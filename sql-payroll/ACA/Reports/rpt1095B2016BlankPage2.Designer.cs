﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt1095B2016BlankPage2.
	/// </summary>
	partial class rpt1095B2016BlankPage2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1095B2016BlankPage2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCoveredName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredDOB12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredAll12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth1_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth2_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth3_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth4_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth5_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth6_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth7_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth8_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth9_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth10_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth11_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoveredMonth12_12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label179 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label188 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label166 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label167 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label189 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label190 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label191 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label192 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape82 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape83 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape84 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape85 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape86 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape87 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape88 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape89 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape90 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape91 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape92 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape93 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape94 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape95 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape96 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape97 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape98 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape99 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape100 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape101 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape102 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape103 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape104 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape105 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape36 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape37 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape38 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape39 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape46 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape47 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape48 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape49 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape50 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape51 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape106 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape107 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape108 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape109 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape110 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape111 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape112 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape113 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape114 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape115 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape116 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape117 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape118 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape119 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape120 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape121 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape122 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape123 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape124 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape125 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape126 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape127 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape128 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape129 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape52 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape53 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape54 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape55 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape56 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape57 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape58 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape59 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape60 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape61 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape62 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape63 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape64 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape65 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape66 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape67 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape68 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape69 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape70 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape71 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape72 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape73 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape74 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape75 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape76 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape77 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape78 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape79 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape80 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape81 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape130 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape131 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape132 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape133 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape134 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape135 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape136 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape137 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape138 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape139 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape140 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape141 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape142 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape143 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape144 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape145 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape146 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape147 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape148 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape149 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape150 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape151 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape152 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape153 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape154 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape155 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape156 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape157 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape158 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape159 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label193 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label194 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label188)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label189)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label190)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label191)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label192)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label193)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label194)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCoveredName1,
				this.txtCoveredSSN1,
				this.txtCoveredDOB1,
				this.txtCoveredName2,
				this.txtCoveredSSN2,
				this.txtCoveredDOB2,
				this.txtCoveredName3,
				this.txtCoveredSSN3,
				this.txtCoveredDOB3,
				this.txtCoveredName4,
				this.txtCoveredSSN4,
				this.txtCoveredDOB4,
				this.txtCoveredName5,
				this.txtCoveredSSN5,
				this.txtCoveredDOB5,
				this.txtCoveredName6,
				this.txtCoveredSSN6,
				this.txtCoveredDOB6,
				this.txtCoveredAll12_1,
				this.txtCoveredMonth1_1,
				this.txtCoveredMonth2_1,
				this.txtCoveredMonth3_1,
				this.txtCoveredMonth4_1,
				this.txtCoveredMonth5_1,
				this.txtCoveredMonth6_1,
				this.txtCoveredMonth7_1,
				this.txtCoveredMonth8_1,
				this.txtCoveredMonth9_1,
				this.txtCoveredMonth10_1,
				this.txtCoveredMonth11_1,
				this.txtCoveredMonth12_1,
				this.txtCoveredAll12_2,
				this.txtCoveredMonth1_2,
				this.txtCoveredMonth2_2,
				this.txtCoveredMonth3_2,
				this.txtCoveredMonth4_2,
				this.txtCoveredMonth5_2,
				this.txtCoveredMonth6_2,
				this.txtCoveredMonth7_2,
				this.txtCoveredMonth8_2,
				this.txtCoveredMonth9_2,
				this.txtCoveredMonth10_2,
				this.txtCoveredMonth11_2,
				this.txtCoveredMonth12_2,
				this.txtCoveredAll12_3,
				this.txtCoveredMonth1_3,
				this.txtCoveredMonth2_3,
				this.txtCoveredMonth3_3,
				this.txtCoveredMonth4_3,
				this.txtCoveredMonth5_3,
				this.txtCoveredMonth6_3,
				this.txtCoveredMonth7_3,
				this.txtCoveredMonth8_3,
				this.txtCoveredMonth9_3,
				this.txtCoveredMonth10_3,
				this.txtCoveredMonth11_3,
				this.txtCoveredMonth12_3,
				this.txtCoveredAll12_4,
				this.txtCoveredMonth1_4,
				this.txtCoveredMonth2_4,
				this.txtCoveredMonth3_4,
				this.txtCoveredMonth4_4,
				this.txtCoveredMonth5_4,
				this.txtCoveredMonth6_4,
				this.txtCoveredMonth7_4,
				this.txtCoveredMonth8_4,
				this.txtCoveredMonth9_4,
				this.txtCoveredMonth10_4,
				this.txtCoveredMonth11_4,
				this.txtCoveredMonth12_4,
				this.txtCoveredAll12_5,
				this.txtCoveredMonth1_5,
				this.txtCoveredMonth2_5,
				this.txtCoveredMonth3_5,
				this.txtCoveredMonth4_5,
				this.txtCoveredMonth5_5,
				this.txtCoveredMonth6_5,
				this.txtCoveredMonth7_5,
				this.txtCoveredMonth8_5,
				this.txtCoveredMonth9_5,
				this.txtCoveredMonth10_5,
				this.txtCoveredMonth11_5,
				this.txtCoveredMonth12_5,
				this.txtCoveredAll12_6,
				this.txtCoveredMonth1_6,
				this.txtCoveredMonth2_6,
				this.txtCoveredMonth3_6,
				this.txtCoveredMonth4_6,
				this.txtCoveredMonth5_6,
				this.txtCoveredMonth6_6,
				this.txtCoveredMonth7_6,
				this.txtCoveredMonth8_6,
				this.txtCoveredMonth9_6,
				this.txtCoveredMonth10_6,
				this.txtCoveredMonth11_6,
				this.txtCoveredMonth12_6,
				this.txtName,
				this.txtSSN,
				this.txtCoveredName7,
				this.txtCoveredSSN7,
				this.txtCoveredDOB7,
				this.txtCoveredName8,
				this.txtCoveredSSN8,
				this.txtCoveredDOB8,
				this.txtCoveredName9,
				this.txtCoveredSSN9,
				this.txtCoveredDOB9,
				this.txtCoveredName10,
				this.txtCoveredSSN10,
				this.txtCoveredDOB10,
				this.txtCoveredName11,
				this.txtCoveredSSN11,
				this.txtCoveredDOB11,
				this.txtCoveredName12,
				this.txtCoveredSSN12,
				this.txtCoveredDOB12,
				this.txtCoveredAll12_7,
				this.txtCoveredMonth1_7,
				this.txtCoveredMonth2_7,
				this.txtCoveredMonth3_7,
				this.txtCoveredMonth4_7,
				this.txtCoveredMonth5_7,
				this.txtCoveredMonth6_7,
				this.txtCoveredMonth7_7,
				this.txtCoveredMonth8_7,
				this.txtCoveredMonth9_7,
				this.txtCoveredMonth10_7,
				this.txtCoveredMonth11_7,
				this.txtCoveredMonth12_7,
				this.txtCoveredAll12_8,
				this.txtCoveredMonth1_8,
				this.txtCoveredMonth2_8,
				this.txtCoveredMonth3_8,
				this.txtCoveredMonth4_8,
				this.txtCoveredMonth5_8,
				this.txtCoveredMonth6_8,
				this.txtCoveredMonth7_8,
				this.txtCoveredMonth8_8,
				this.txtCoveredMonth9_8,
				this.txtCoveredMonth10_8,
				this.txtCoveredMonth11_8,
				this.txtCoveredMonth12_8,
				this.txtCoveredAll12_9,
				this.txtCoveredMonth1_9,
				this.txtCoveredMonth2_9,
				this.txtCoveredMonth3_9,
				this.txtCoveredMonth4_9,
				this.txtCoveredMonth5_9,
				this.txtCoveredMonth6_9,
				this.txtCoveredMonth7_9,
				this.txtCoveredMonth8_9,
				this.txtCoveredMonth9_9,
				this.txtCoveredMonth10_9,
				this.txtCoveredMonth11_9,
				this.txtCoveredMonth12_9,
				this.txtCoveredAll12_10,
				this.txtCoveredMonth1_10,
				this.txtCoveredMonth2_10,
				this.txtCoveredMonth3_10,
				this.txtCoveredMonth4_10,
				this.txtCoveredMonth5_10,
				this.txtCoveredMonth6_10,
				this.txtCoveredMonth7_10,
				this.txtCoveredMonth8_10,
				this.txtCoveredMonth9_10,
				this.txtCoveredMonth10_10,
				this.txtCoveredMonth11_10,
				this.txtCoveredMonth12_10,
				this.txtCoveredAll12_11,
				this.txtCoveredMonth1_11,
				this.txtCoveredMonth2_11,
				this.txtCoveredMonth3_11,
				this.txtCoveredMonth4_11,
				this.txtCoveredMonth5_11,
				this.txtCoveredMonth6_11,
				this.txtCoveredMonth7_11,
				this.txtCoveredMonth8_11,
				this.txtCoveredMonth9_11,
				this.txtCoveredMonth10_11,
				this.txtCoveredMonth11_11,
				this.txtCoveredMonth12_11,
				this.txtCoveredAll12_12,
				this.txtCoveredMonth1_12,
				this.txtCoveredMonth2_12,
				this.txtCoveredMonth3_12,
				this.txtCoveredMonth4_12,
				this.txtCoveredMonth5_12,
				this.txtCoveredMonth6_12,
				this.txtCoveredMonth7_12,
				this.txtCoveredMonth8_12,
				this.txtCoveredMonth9_12,
				this.txtCoveredMonth10_12,
				this.txtCoveredMonth11_12,
				this.txtCoveredMonth12_12,
				this.Label176,
				this.Line13,
				this.Label89,
				this.Label90,
				this.Line15,
				this.Line16,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line20,
				this.Line21,
				this.Line22,
				this.Label130,
				this.Label131,
				this.Label132,
				this.Label133,
				this.Label134,
				this.Label135,
				this.Label3,
				this.Label174,
				this.Label175,
				this.Label178,
				this.Label179,
				this.Label180,
				this.Label181,
				this.Label182,
				this.Label183,
				this.Label184,
				this.Line38,
				this.Label185,
				this.Line39,
				this.Line40,
				this.Line41,
				this.Line42,
				this.Line43,
				this.Line44,
				this.Label34,
				this.Label35,
				this.Label93,
				this.Label186,
				this.Label187,
				this.Label188,
				this.Line46,
				this.Line23,
				this.Line36,
				this.Label118,
				this.Label136,
				this.Label137,
				this.Label165,
				this.Label166,
				this.Label167,
				this.Label168,
				this.Label169,
				this.Label170,
				this.Label171,
				this.Label172,
				this.Label173,
				this.Line37,
				this.Label189,
				this.Label190,
				this.Label191,
				this.Line24,
				this.Line25,
				this.Line26,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Label192,
				this.Line47,
				this.Line45,
				this.Shape4,
				this.Shape5,
				this.Shape6,
				this.Shape7,
				this.Shape8,
				this.Shape9,
				this.Shape10,
				this.Shape11,
				this.Shape12,
				this.Shape13,
				this.Shape14,
				this.Shape15,
				this.Shape16,
				this.Shape17,
				this.Shape18,
				this.Shape19,
				this.Shape20,
				this.Shape21,
				this.Shape22,
				this.Shape23,
				this.Shape24,
				this.Shape25,
				this.Shape26,
				this.Shape27,
				this.Shape82,
				this.Shape83,
				this.Shape84,
				this.Shape85,
				this.Shape86,
				this.Shape87,
				this.Shape88,
				this.Shape89,
				this.Shape90,
				this.Shape91,
				this.Shape92,
				this.Shape93,
				this.Shape94,
				this.Shape95,
				this.Shape96,
				this.Shape97,
				this.Shape98,
				this.Shape99,
				this.Shape100,
				this.Shape101,
				this.Shape102,
				this.Shape103,
				this.Shape104,
				this.Shape105,
				this.Shape28,
				this.Shape29,
				this.Shape30,
				this.Shape32,
				this.Shape31,
				this.Shape33,
				this.Shape34,
				this.Shape35,
				this.Shape36,
				this.Shape37,
				this.Shape38,
				this.Shape39,
				this.Shape40,
				this.Shape41,
				this.Shape42,
				this.Shape43,
				this.Shape44,
				this.Shape45,
				this.Shape46,
				this.Shape47,
				this.Shape48,
				this.Shape49,
				this.Shape50,
				this.Shape51,
				this.Shape106,
				this.Shape107,
				this.Shape108,
				this.Shape109,
				this.Shape110,
				this.Shape111,
				this.Shape112,
				this.Shape113,
				this.Shape114,
				this.Shape115,
				this.Shape116,
				this.Shape117,
				this.Shape118,
				this.Shape119,
				this.Shape120,
				this.Shape121,
				this.Shape122,
				this.Shape123,
				this.Shape124,
				this.Shape125,
				this.Shape126,
				this.Shape127,
				this.Shape128,
				this.Shape129,
				this.Shape52,
				this.Shape53,
				this.Shape54,
				this.Shape55,
				this.Shape56,
				this.Shape57,
				this.Shape58,
				this.Shape59,
				this.Shape60,
				this.Shape61,
				this.Shape62,
				this.Shape63,
				this.Shape64,
				this.Shape65,
				this.Shape66,
				this.Shape67,
				this.Shape68,
				this.Shape69,
				this.Shape70,
				this.Shape71,
				this.Shape72,
				this.Shape73,
				this.Shape74,
				this.Shape75,
				this.Shape76,
				this.Shape77,
				this.Shape78,
				this.Shape79,
				this.Shape80,
				this.Shape81,
				this.Shape130,
				this.Shape131,
				this.Shape132,
				this.Shape133,
				this.Shape134,
				this.Shape135,
				this.Shape136,
				this.Shape137,
				this.Shape138,
				this.Shape139,
				this.Shape140,
				this.Shape141,
				this.Shape142,
				this.Shape143,
				this.Shape144,
				this.Shape145,
				this.Shape146,
				this.Shape147,
				this.Shape148,
				this.Shape149,
				this.Shape150,
				this.Shape151,
				this.Shape152,
				this.Shape153,
				this.Shape154,
				this.Shape155,
				this.Shape156,
				this.Shape157,
				this.Shape158,
				this.Shape159,
				this.Label177,
				this.Label193,
				this.Label194
			});
			this.Detail.Height = 7.614583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtCoveredName1
			// 
			this.txtCoveredName1.Height = 0.1770833F;
			this.txtCoveredName1.Left = 0.1666667F;
			this.txtCoveredName1.Name = "txtCoveredName1";
			this.txtCoveredName1.Text = null;
			this.txtCoveredName1.Top = 1.552083F;
			this.txtCoveredName1.Width = 2.166667F;
			// 
			// txtCoveredSSN1
			// 
			this.txtCoveredSSN1.Height = 0.1770833F;
			this.txtCoveredSSN1.Left = 2.5F;
			this.txtCoveredSSN1.Name = "txtCoveredSSN1";
			this.txtCoveredSSN1.Text = null;
			this.txtCoveredSSN1.Top = 1.552083F;
			this.txtCoveredSSN1.Width = 1F;
			// 
			// txtCoveredDOB1
			// 
			this.txtCoveredDOB1.Height = 0.1770833F;
			this.txtCoveredDOB1.Left = 3.729167F;
			this.txtCoveredDOB1.Name = "txtCoveredDOB1";
			this.txtCoveredDOB1.Text = null;
			this.txtCoveredDOB1.Top = 1.552083F;
			this.txtCoveredDOB1.Width = 0.9166667F;
			// 
			// txtCoveredName2
			// 
			this.txtCoveredName2.Height = 0.1770833F;
			this.txtCoveredName2.Left = 0.1666667F;
			this.txtCoveredName2.Name = "txtCoveredName2";
			this.txtCoveredName2.Text = null;
			this.txtCoveredName2.Top = 2.052083F;
			this.txtCoveredName2.Width = 2.166667F;
			// 
			// txtCoveredSSN2
			// 
			this.txtCoveredSSN2.Height = 0.1770833F;
			this.txtCoveredSSN2.Left = 2.5F;
			this.txtCoveredSSN2.Name = "txtCoveredSSN2";
			this.txtCoveredSSN2.Text = null;
			this.txtCoveredSSN2.Top = 2.052083F;
			this.txtCoveredSSN2.Width = 1F;
			// 
			// txtCoveredDOB2
			// 
			this.txtCoveredDOB2.Height = 0.1770833F;
			this.txtCoveredDOB2.Left = 3.729167F;
			this.txtCoveredDOB2.Name = "txtCoveredDOB2";
			this.txtCoveredDOB2.Text = null;
			this.txtCoveredDOB2.Top = 2.052083F;
			this.txtCoveredDOB2.Width = 0.9166667F;
			// 
			// txtCoveredName3
			// 
			this.txtCoveredName3.Height = 0.1770833F;
			this.txtCoveredName3.Left = 0.1666667F;
			this.txtCoveredName3.Name = "txtCoveredName3";
			this.txtCoveredName3.Text = null;
			this.txtCoveredName3.Top = 2.552083F;
			this.txtCoveredName3.Width = 2.166667F;
			// 
			// txtCoveredSSN3
			// 
			this.txtCoveredSSN3.Height = 0.1770833F;
			this.txtCoveredSSN3.Left = 2.5F;
			this.txtCoveredSSN3.Name = "txtCoveredSSN3";
			this.txtCoveredSSN3.Text = null;
			this.txtCoveredSSN3.Top = 2.552083F;
			this.txtCoveredSSN3.Width = 1F;
			// 
			// txtCoveredDOB3
			// 
			this.txtCoveredDOB3.Height = 0.1770833F;
			this.txtCoveredDOB3.Left = 3.729167F;
			this.txtCoveredDOB3.Name = "txtCoveredDOB3";
			this.txtCoveredDOB3.Text = null;
			this.txtCoveredDOB3.Top = 2.552083F;
			this.txtCoveredDOB3.Width = 0.9166667F;
			// 
			// txtCoveredName4
			// 
			this.txtCoveredName4.Height = 0.1770833F;
			this.txtCoveredName4.Left = 0.1666667F;
			this.txtCoveredName4.Name = "txtCoveredName4";
			this.txtCoveredName4.Text = null;
			this.txtCoveredName4.Top = 3.052083F;
			this.txtCoveredName4.Width = 2.166667F;
			// 
			// txtCoveredSSN4
			// 
			this.txtCoveredSSN4.Height = 0.1770833F;
			this.txtCoveredSSN4.Left = 2.5F;
			this.txtCoveredSSN4.Name = "txtCoveredSSN4";
			this.txtCoveredSSN4.Text = null;
			this.txtCoveredSSN4.Top = 3.052083F;
			this.txtCoveredSSN4.Width = 1F;
			// 
			// txtCoveredDOB4
			// 
			this.txtCoveredDOB4.Height = 0.1770833F;
			this.txtCoveredDOB4.Left = 3.729167F;
			this.txtCoveredDOB4.Name = "txtCoveredDOB4";
			this.txtCoveredDOB4.Text = null;
			this.txtCoveredDOB4.Top = 3.052083F;
			this.txtCoveredDOB4.Width = 0.9166667F;
			// 
			// txtCoveredName5
			// 
			this.txtCoveredName5.Height = 0.1770833F;
			this.txtCoveredName5.Left = 0.1666667F;
			this.txtCoveredName5.Name = "txtCoveredName5";
			this.txtCoveredName5.Text = null;
			this.txtCoveredName5.Top = 3.552083F;
			this.txtCoveredName5.Width = 2.166667F;
			// 
			// txtCoveredSSN5
			// 
			this.txtCoveredSSN5.Height = 0.1770833F;
			this.txtCoveredSSN5.Left = 2.5F;
			this.txtCoveredSSN5.Name = "txtCoveredSSN5";
			this.txtCoveredSSN5.Text = null;
			this.txtCoveredSSN5.Top = 3.552083F;
			this.txtCoveredSSN5.Width = 1F;
			// 
			// txtCoveredDOB5
			// 
			this.txtCoveredDOB5.Height = 0.1770833F;
			this.txtCoveredDOB5.Left = 3.729167F;
			this.txtCoveredDOB5.Name = "txtCoveredDOB5";
			this.txtCoveredDOB5.Text = null;
			this.txtCoveredDOB5.Top = 3.552083F;
			this.txtCoveredDOB5.Width = 0.9166667F;
			// 
			// txtCoveredName6
			// 
			this.txtCoveredName6.Height = 0.1770833F;
			this.txtCoveredName6.Left = 0.1666667F;
			this.txtCoveredName6.Name = "txtCoveredName6";
			this.txtCoveredName6.Text = null;
			this.txtCoveredName6.Top = 4.041667F;
			this.txtCoveredName6.Width = 2.166667F;
			// 
			// txtCoveredSSN6
			// 
			this.txtCoveredSSN6.Height = 0.1770833F;
			this.txtCoveredSSN6.Left = 2.5F;
			this.txtCoveredSSN6.Name = "txtCoveredSSN6";
			this.txtCoveredSSN6.Text = null;
			this.txtCoveredSSN6.Top = 4.041667F;
			this.txtCoveredSSN6.Width = 1F;
			// 
			// txtCoveredDOB6
			// 
			this.txtCoveredDOB6.Height = 0.1770833F;
			this.txtCoveredDOB6.Left = 3.729167F;
			this.txtCoveredDOB6.Name = "txtCoveredDOB6";
			this.txtCoveredDOB6.Text = null;
			this.txtCoveredDOB6.Top = 4.041667F;
			this.txtCoveredDOB6.Width = 0.9166667F;
			// 
			// txtCoveredAll12_1
			// 
			this.txtCoveredAll12_1.Height = 0.1770833F;
			this.txtCoveredAll12_1.Left = 4.885417F;
			this.txtCoveredAll12_1.Name = "txtCoveredAll12_1";
			this.txtCoveredAll12_1.Text = null;
			this.txtCoveredAll12_1.Top = 1.552083F;
			this.txtCoveredAll12_1.Width = 0.25F;
			// 
			// txtCoveredMonth1_1
			// 
			this.txtCoveredMonth1_1.Height = 0.1770833F;
			this.txtCoveredMonth1_1.Left = 5.395833F;
			this.txtCoveredMonth1_1.Name = "txtCoveredMonth1_1";
			this.txtCoveredMonth1_1.Text = null;
			this.txtCoveredMonth1_1.Top = 1.552083F;
			this.txtCoveredMonth1_1.Width = 0.25F;
			// 
			// txtCoveredMonth2_1
			// 
			this.txtCoveredMonth2_1.Height = 0.1770833F;
			this.txtCoveredMonth2_1.Left = 5.789583F;
			this.txtCoveredMonth2_1.Name = "txtCoveredMonth2_1";
			this.txtCoveredMonth2_1.Text = null;
			this.txtCoveredMonth2_1.Top = 1.552083F;
			this.txtCoveredMonth2_1.Width = 0.25F;
			// 
			// txtCoveredMonth3_1
			// 
			this.txtCoveredMonth3_1.Height = 0.1770833F;
			this.txtCoveredMonth3_1.Left = 6.183333F;
			this.txtCoveredMonth3_1.Name = "txtCoveredMonth3_1";
			this.txtCoveredMonth3_1.Text = null;
			this.txtCoveredMonth3_1.Top = 1.552083F;
			this.txtCoveredMonth3_1.Width = 0.25F;
			// 
			// txtCoveredMonth4_1
			// 
			this.txtCoveredMonth4_1.Height = 0.1770833F;
			this.txtCoveredMonth4_1.Left = 6.577083F;
			this.txtCoveredMonth4_1.Name = "txtCoveredMonth4_1";
			this.txtCoveredMonth4_1.Text = null;
			this.txtCoveredMonth4_1.Top = 1.552083F;
			this.txtCoveredMonth4_1.Width = 0.25F;
			// 
			// txtCoveredMonth5_1
			// 
			this.txtCoveredMonth5_1.Height = 0.1770833F;
			this.txtCoveredMonth5_1.Left = 6.970833F;
			this.txtCoveredMonth5_1.Name = "txtCoveredMonth5_1";
			this.txtCoveredMonth5_1.Text = null;
			this.txtCoveredMonth5_1.Top = 1.552083F;
			this.txtCoveredMonth5_1.Width = 0.25F;
			// 
			// txtCoveredMonth6_1
			// 
			this.txtCoveredMonth6_1.Height = 0.1770833F;
			this.txtCoveredMonth6_1.Left = 7.364583F;
			this.txtCoveredMonth6_1.Name = "txtCoveredMonth6_1";
			this.txtCoveredMonth6_1.Text = null;
			this.txtCoveredMonth6_1.Top = 1.552083F;
			this.txtCoveredMonth6_1.Width = 0.25F;
			// 
			// txtCoveredMonth7_1
			// 
			this.txtCoveredMonth7_1.Height = 0.1770833F;
			this.txtCoveredMonth7_1.Left = 7.758333F;
			this.txtCoveredMonth7_1.Name = "txtCoveredMonth7_1";
			this.txtCoveredMonth7_1.Text = null;
			this.txtCoveredMonth7_1.Top = 1.552083F;
			this.txtCoveredMonth7_1.Width = 0.25F;
			// 
			// txtCoveredMonth8_1
			// 
			this.txtCoveredMonth8_1.Height = 0.1770833F;
			this.txtCoveredMonth8_1.Left = 8.152083F;
			this.txtCoveredMonth8_1.Name = "txtCoveredMonth8_1";
			this.txtCoveredMonth8_1.Text = null;
			this.txtCoveredMonth8_1.Top = 1.552083F;
			this.txtCoveredMonth8_1.Width = 0.25F;
			// 
			// txtCoveredMonth9_1
			// 
			this.txtCoveredMonth9_1.Height = 0.1770833F;
			this.txtCoveredMonth9_1.Left = 8.545834F;
			this.txtCoveredMonth9_1.Name = "txtCoveredMonth9_1";
			this.txtCoveredMonth9_1.Text = null;
			this.txtCoveredMonth9_1.Top = 1.552083F;
			this.txtCoveredMonth9_1.Width = 0.25F;
			// 
			// txtCoveredMonth10_1
			// 
			this.txtCoveredMonth10_1.Height = 0.1770833F;
			this.txtCoveredMonth10_1.Left = 8.939584F;
			this.txtCoveredMonth10_1.Name = "txtCoveredMonth10_1";
			this.txtCoveredMonth10_1.Text = null;
			this.txtCoveredMonth10_1.Top = 1.552083F;
			this.txtCoveredMonth10_1.Width = 0.25F;
			// 
			// txtCoveredMonth11_1
			// 
			this.txtCoveredMonth11_1.Height = 0.1770833F;
			this.txtCoveredMonth11_1.Left = 9.333333F;
			this.txtCoveredMonth11_1.Name = "txtCoveredMonth11_1";
			this.txtCoveredMonth11_1.Text = null;
			this.txtCoveredMonth11_1.Top = 1.552083F;
			this.txtCoveredMonth11_1.Width = 0.25F;
			// 
			// txtCoveredMonth12_1
			// 
			this.txtCoveredMonth12_1.Height = 0.1770833F;
			this.txtCoveredMonth12_1.Left = 9.727083F;
			this.txtCoveredMonth12_1.Name = "txtCoveredMonth12_1";
			this.txtCoveredMonth12_1.Text = null;
			this.txtCoveredMonth12_1.Top = 1.552083F;
			this.txtCoveredMonth12_1.Width = 0.1979167F;
			// 
			// txtCoveredAll12_2
			// 
			this.txtCoveredAll12_2.Height = 0.1770833F;
			this.txtCoveredAll12_2.Left = 4.885417F;
			this.txtCoveredAll12_2.Name = "txtCoveredAll12_2";
			this.txtCoveredAll12_2.Text = null;
			this.txtCoveredAll12_2.Top = 2.052083F;
			this.txtCoveredAll12_2.Width = 0.25F;
			// 
			// txtCoveredMonth1_2
			// 
			this.txtCoveredMonth1_2.Height = 0.1770833F;
			this.txtCoveredMonth1_2.Left = 5.395833F;
			this.txtCoveredMonth1_2.Name = "txtCoveredMonth1_2";
			this.txtCoveredMonth1_2.Text = null;
			this.txtCoveredMonth1_2.Top = 2.052083F;
			this.txtCoveredMonth1_2.Width = 0.25F;
			// 
			// txtCoveredMonth2_2
			// 
			this.txtCoveredMonth2_2.Height = 0.1770833F;
			this.txtCoveredMonth2_2.Left = 5.789583F;
			this.txtCoveredMonth2_2.Name = "txtCoveredMonth2_2";
			this.txtCoveredMonth2_2.Text = null;
			this.txtCoveredMonth2_2.Top = 2.052083F;
			this.txtCoveredMonth2_2.Width = 0.25F;
			// 
			// txtCoveredMonth3_2
			// 
			this.txtCoveredMonth3_2.Height = 0.1770833F;
			this.txtCoveredMonth3_2.Left = 6.183333F;
			this.txtCoveredMonth3_2.Name = "txtCoveredMonth3_2";
			this.txtCoveredMonth3_2.Text = null;
			this.txtCoveredMonth3_2.Top = 2.052083F;
			this.txtCoveredMonth3_2.Width = 0.25F;
			// 
			// txtCoveredMonth4_2
			// 
			this.txtCoveredMonth4_2.Height = 0.1770833F;
			this.txtCoveredMonth4_2.Left = 6.577083F;
			this.txtCoveredMonth4_2.Name = "txtCoveredMonth4_2";
			this.txtCoveredMonth4_2.Text = null;
			this.txtCoveredMonth4_2.Top = 2.052083F;
			this.txtCoveredMonth4_2.Width = 0.25F;
			// 
			// txtCoveredMonth5_2
			// 
			this.txtCoveredMonth5_2.Height = 0.1770833F;
			this.txtCoveredMonth5_2.Left = 6.970833F;
			this.txtCoveredMonth5_2.Name = "txtCoveredMonth5_2";
			this.txtCoveredMonth5_2.Text = null;
			this.txtCoveredMonth5_2.Top = 2.052083F;
			this.txtCoveredMonth5_2.Width = 0.25F;
			// 
			// txtCoveredMonth6_2
			// 
			this.txtCoveredMonth6_2.Height = 0.1770833F;
			this.txtCoveredMonth6_2.Left = 7.364583F;
			this.txtCoveredMonth6_2.Name = "txtCoveredMonth6_2";
			this.txtCoveredMonth6_2.Text = null;
			this.txtCoveredMonth6_2.Top = 2.052083F;
			this.txtCoveredMonth6_2.Width = 0.25F;
			// 
			// txtCoveredMonth7_2
			// 
			this.txtCoveredMonth7_2.Height = 0.1770833F;
			this.txtCoveredMonth7_2.Left = 7.758333F;
			this.txtCoveredMonth7_2.Name = "txtCoveredMonth7_2";
			this.txtCoveredMonth7_2.Text = null;
			this.txtCoveredMonth7_2.Top = 2.052083F;
			this.txtCoveredMonth7_2.Width = 0.25F;
			// 
			// txtCoveredMonth8_2
			// 
			this.txtCoveredMonth8_2.Height = 0.1770833F;
			this.txtCoveredMonth8_2.Left = 8.152083F;
			this.txtCoveredMonth8_2.Name = "txtCoveredMonth8_2";
			this.txtCoveredMonth8_2.Text = null;
			this.txtCoveredMonth8_2.Top = 2.052083F;
			this.txtCoveredMonth8_2.Width = 0.25F;
			// 
			// txtCoveredMonth9_2
			// 
			this.txtCoveredMonth9_2.Height = 0.1770833F;
			this.txtCoveredMonth9_2.Left = 8.545834F;
			this.txtCoveredMonth9_2.Name = "txtCoveredMonth9_2";
			this.txtCoveredMonth9_2.Text = null;
			this.txtCoveredMonth9_2.Top = 2.052083F;
			this.txtCoveredMonth9_2.Width = 0.25F;
			// 
			// txtCoveredMonth10_2
			// 
			this.txtCoveredMonth10_2.Height = 0.1770833F;
			this.txtCoveredMonth10_2.Left = 8.939584F;
			this.txtCoveredMonth10_2.Name = "txtCoveredMonth10_2";
			this.txtCoveredMonth10_2.Text = null;
			this.txtCoveredMonth10_2.Top = 2.052083F;
			this.txtCoveredMonth10_2.Width = 0.25F;
			// 
			// txtCoveredMonth11_2
			// 
			this.txtCoveredMonth11_2.Height = 0.1770833F;
			this.txtCoveredMonth11_2.Left = 9.333333F;
			this.txtCoveredMonth11_2.Name = "txtCoveredMonth11_2";
			this.txtCoveredMonth11_2.Text = null;
			this.txtCoveredMonth11_2.Top = 2.052083F;
			this.txtCoveredMonth11_2.Width = 0.25F;
			// 
			// txtCoveredMonth12_2
			// 
			this.txtCoveredMonth12_2.Height = 0.1770833F;
			this.txtCoveredMonth12_2.Left = 9.727083F;
			this.txtCoveredMonth12_2.Name = "txtCoveredMonth12_2";
			this.txtCoveredMonth12_2.Text = null;
			this.txtCoveredMonth12_2.Top = 2.052083F;
			this.txtCoveredMonth12_2.Width = 0.1979167F;
			// 
			// txtCoveredAll12_3
			// 
			this.txtCoveredAll12_3.Height = 0.1770833F;
			this.txtCoveredAll12_3.Left = 4.885417F;
			this.txtCoveredAll12_3.Name = "txtCoveredAll12_3";
			this.txtCoveredAll12_3.Text = null;
			this.txtCoveredAll12_3.Top = 2.552083F;
			this.txtCoveredAll12_3.Width = 0.25F;
			// 
			// txtCoveredMonth1_3
			// 
			this.txtCoveredMonth1_3.Height = 0.1770833F;
			this.txtCoveredMonth1_3.Left = 5.395833F;
			this.txtCoveredMonth1_3.Name = "txtCoveredMonth1_3";
			this.txtCoveredMonth1_3.Text = null;
			this.txtCoveredMonth1_3.Top = 2.552083F;
			this.txtCoveredMonth1_3.Width = 0.25F;
			// 
			// txtCoveredMonth2_3
			// 
			this.txtCoveredMonth2_3.Height = 0.1770833F;
			this.txtCoveredMonth2_3.Left = 5.789583F;
			this.txtCoveredMonth2_3.Name = "txtCoveredMonth2_3";
			this.txtCoveredMonth2_3.Text = null;
			this.txtCoveredMonth2_3.Top = 2.552083F;
			this.txtCoveredMonth2_3.Width = 0.25F;
			// 
			// txtCoveredMonth3_3
			// 
			this.txtCoveredMonth3_3.Height = 0.1770833F;
			this.txtCoveredMonth3_3.Left = 6.183333F;
			this.txtCoveredMonth3_3.Name = "txtCoveredMonth3_3";
			this.txtCoveredMonth3_3.Text = null;
			this.txtCoveredMonth3_3.Top = 2.552083F;
			this.txtCoveredMonth3_3.Width = 0.25F;
			// 
			// txtCoveredMonth4_3
			// 
			this.txtCoveredMonth4_3.Height = 0.1770833F;
			this.txtCoveredMonth4_3.Left = 6.577083F;
			this.txtCoveredMonth4_3.Name = "txtCoveredMonth4_3";
			this.txtCoveredMonth4_3.Text = null;
			this.txtCoveredMonth4_3.Top = 2.552083F;
			this.txtCoveredMonth4_3.Width = 0.25F;
			// 
			// txtCoveredMonth5_3
			// 
			this.txtCoveredMonth5_3.Height = 0.1770833F;
			this.txtCoveredMonth5_3.Left = 6.970833F;
			this.txtCoveredMonth5_3.Name = "txtCoveredMonth5_3";
			this.txtCoveredMonth5_3.Text = null;
			this.txtCoveredMonth5_3.Top = 2.552083F;
			this.txtCoveredMonth5_3.Width = 0.25F;
			// 
			// txtCoveredMonth6_3
			// 
			this.txtCoveredMonth6_3.Height = 0.1770833F;
			this.txtCoveredMonth6_3.Left = 7.364583F;
			this.txtCoveredMonth6_3.Name = "txtCoveredMonth6_3";
			this.txtCoveredMonth6_3.Text = null;
			this.txtCoveredMonth6_3.Top = 2.552083F;
			this.txtCoveredMonth6_3.Width = 0.25F;
			// 
			// txtCoveredMonth7_3
			// 
			this.txtCoveredMonth7_3.Height = 0.1770833F;
			this.txtCoveredMonth7_3.Left = 7.758333F;
			this.txtCoveredMonth7_3.Name = "txtCoveredMonth7_3";
			this.txtCoveredMonth7_3.Text = null;
			this.txtCoveredMonth7_3.Top = 2.552083F;
			this.txtCoveredMonth7_3.Width = 0.25F;
			// 
			// txtCoveredMonth8_3
			// 
			this.txtCoveredMonth8_3.Height = 0.1770833F;
			this.txtCoveredMonth8_3.Left = 8.152083F;
			this.txtCoveredMonth8_3.Name = "txtCoveredMonth8_3";
			this.txtCoveredMonth8_3.Text = null;
			this.txtCoveredMonth8_3.Top = 2.552083F;
			this.txtCoveredMonth8_3.Width = 0.25F;
			// 
			// txtCoveredMonth9_3
			// 
			this.txtCoveredMonth9_3.Height = 0.1770833F;
			this.txtCoveredMonth9_3.Left = 8.545834F;
			this.txtCoveredMonth9_3.Name = "txtCoveredMonth9_3";
			this.txtCoveredMonth9_3.Text = null;
			this.txtCoveredMonth9_3.Top = 2.552083F;
			this.txtCoveredMonth9_3.Width = 0.25F;
			// 
			// txtCoveredMonth10_3
			// 
			this.txtCoveredMonth10_3.Height = 0.1770833F;
			this.txtCoveredMonth10_3.Left = 8.939584F;
			this.txtCoveredMonth10_3.Name = "txtCoveredMonth10_3";
			this.txtCoveredMonth10_3.Text = null;
			this.txtCoveredMonth10_3.Top = 2.552083F;
			this.txtCoveredMonth10_3.Width = 0.25F;
			// 
			// txtCoveredMonth11_3
			// 
			this.txtCoveredMonth11_3.Height = 0.1770833F;
			this.txtCoveredMonth11_3.Left = 9.333333F;
			this.txtCoveredMonth11_3.Name = "txtCoveredMonth11_3";
			this.txtCoveredMonth11_3.Text = null;
			this.txtCoveredMonth11_3.Top = 2.552083F;
			this.txtCoveredMonth11_3.Width = 0.25F;
			// 
			// txtCoveredMonth12_3
			// 
			this.txtCoveredMonth12_3.Height = 0.1770833F;
			this.txtCoveredMonth12_3.Left = 9.727083F;
			this.txtCoveredMonth12_3.Name = "txtCoveredMonth12_3";
			this.txtCoveredMonth12_3.Text = null;
			this.txtCoveredMonth12_3.Top = 2.552083F;
			this.txtCoveredMonth12_3.Width = 0.1979167F;
			// 
			// txtCoveredAll12_4
			// 
			this.txtCoveredAll12_4.Height = 0.1770833F;
			this.txtCoveredAll12_4.Left = 4.885417F;
			this.txtCoveredAll12_4.Name = "txtCoveredAll12_4";
			this.txtCoveredAll12_4.Text = null;
			this.txtCoveredAll12_4.Top = 3.052083F;
			this.txtCoveredAll12_4.Width = 0.25F;
			// 
			// txtCoveredMonth1_4
			// 
			this.txtCoveredMonth1_4.Height = 0.1770833F;
			this.txtCoveredMonth1_4.Left = 5.395833F;
			this.txtCoveredMonth1_4.Name = "txtCoveredMonth1_4";
			this.txtCoveredMonth1_4.Text = null;
			this.txtCoveredMonth1_4.Top = 3.052083F;
			this.txtCoveredMonth1_4.Width = 0.25F;
			// 
			// txtCoveredMonth2_4
			// 
			this.txtCoveredMonth2_4.Height = 0.1770833F;
			this.txtCoveredMonth2_4.Left = 5.789583F;
			this.txtCoveredMonth2_4.Name = "txtCoveredMonth2_4";
			this.txtCoveredMonth2_4.Text = null;
			this.txtCoveredMonth2_4.Top = 3.052083F;
			this.txtCoveredMonth2_4.Width = 0.25F;
			// 
			// txtCoveredMonth3_4
			// 
			this.txtCoveredMonth3_4.Height = 0.1770833F;
			this.txtCoveredMonth3_4.Left = 6.183333F;
			this.txtCoveredMonth3_4.Name = "txtCoveredMonth3_4";
			this.txtCoveredMonth3_4.Text = null;
			this.txtCoveredMonth3_4.Top = 3.052083F;
			this.txtCoveredMonth3_4.Width = 0.25F;
			// 
			// txtCoveredMonth4_4
			// 
			this.txtCoveredMonth4_4.Height = 0.1770833F;
			this.txtCoveredMonth4_4.Left = 6.577083F;
			this.txtCoveredMonth4_4.Name = "txtCoveredMonth4_4";
			this.txtCoveredMonth4_4.Text = null;
			this.txtCoveredMonth4_4.Top = 3.052083F;
			this.txtCoveredMonth4_4.Width = 0.25F;
			// 
			// txtCoveredMonth5_4
			// 
			this.txtCoveredMonth5_4.Height = 0.1770833F;
			this.txtCoveredMonth5_4.Left = 6.970833F;
			this.txtCoveredMonth5_4.Name = "txtCoveredMonth5_4";
			this.txtCoveredMonth5_4.Text = null;
			this.txtCoveredMonth5_4.Top = 3.052083F;
			this.txtCoveredMonth5_4.Width = 0.25F;
			// 
			// txtCoveredMonth6_4
			// 
			this.txtCoveredMonth6_4.Height = 0.1770833F;
			this.txtCoveredMonth6_4.Left = 7.364583F;
			this.txtCoveredMonth6_4.Name = "txtCoveredMonth6_4";
			this.txtCoveredMonth6_4.Text = null;
			this.txtCoveredMonth6_4.Top = 3.052083F;
			this.txtCoveredMonth6_4.Width = 0.25F;
			// 
			// txtCoveredMonth7_4
			// 
			this.txtCoveredMonth7_4.Height = 0.1770833F;
			this.txtCoveredMonth7_4.Left = 7.758333F;
			this.txtCoveredMonth7_4.Name = "txtCoveredMonth7_4";
			this.txtCoveredMonth7_4.Text = null;
			this.txtCoveredMonth7_4.Top = 3.052083F;
			this.txtCoveredMonth7_4.Width = 0.25F;
			// 
			// txtCoveredMonth8_4
			// 
			this.txtCoveredMonth8_4.Height = 0.1770833F;
			this.txtCoveredMonth8_4.Left = 8.152083F;
			this.txtCoveredMonth8_4.Name = "txtCoveredMonth8_4";
			this.txtCoveredMonth8_4.Text = null;
			this.txtCoveredMonth8_4.Top = 3.052083F;
			this.txtCoveredMonth8_4.Width = 0.25F;
			// 
			// txtCoveredMonth9_4
			// 
			this.txtCoveredMonth9_4.Height = 0.1770833F;
			this.txtCoveredMonth9_4.Left = 8.545834F;
			this.txtCoveredMonth9_4.Name = "txtCoveredMonth9_4";
			this.txtCoveredMonth9_4.Text = null;
			this.txtCoveredMonth9_4.Top = 3.052083F;
			this.txtCoveredMonth9_4.Width = 0.25F;
			// 
			// txtCoveredMonth10_4
			// 
			this.txtCoveredMonth10_4.Height = 0.1770833F;
			this.txtCoveredMonth10_4.Left = 8.939584F;
			this.txtCoveredMonth10_4.Name = "txtCoveredMonth10_4";
			this.txtCoveredMonth10_4.Text = null;
			this.txtCoveredMonth10_4.Top = 3.052083F;
			this.txtCoveredMonth10_4.Width = 0.25F;
			// 
			// txtCoveredMonth11_4
			// 
			this.txtCoveredMonth11_4.Height = 0.1770833F;
			this.txtCoveredMonth11_4.Left = 9.333333F;
			this.txtCoveredMonth11_4.Name = "txtCoveredMonth11_4";
			this.txtCoveredMonth11_4.Text = null;
			this.txtCoveredMonth11_4.Top = 3.052083F;
			this.txtCoveredMonth11_4.Width = 0.25F;
			// 
			// txtCoveredMonth12_4
			// 
			this.txtCoveredMonth12_4.Height = 0.1770833F;
			this.txtCoveredMonth12_4.Left = 9.727083F;
			this.txtCoveredMonth12_4.Name = "txtCoveredMonth12_4";
			this.txtCoveredMonth12_4.Text = null;
			this.txtCoveredMonth12_4.Top = 3.052083F;
			this.txtCoveredMonth12_4.Width = 0.1979167F;
			// 
			// txtCoveredAll12_5
			// 
			this.txtCoveredAll12_5.Height = 0.1770833F;
			this.txtCoveredAll12_5.Left = 4.885417F;
			this.txtCoveredAll12_5.Name = "txtCoveredAll12_5";
			this.txtCoveredAll12_5.Text = null;
			this.txtCoveredAll12_5.Top = 3.552083F;
			this.txtCoveredAll12_5.Width = 0.25F;
			// 
			// txtCoveredMonth1_5
			// 
			this.txtCoveredMonth1_5.Height = 0.1770833F;
			this.txtCoveredMonth1_5.Left = 5.395833F;
			this.txtCoveredMonth1_5.Name = "txtCoveredMonth1_5";
			this.txtCoveredMonth1_5.Text = null;
			this.txtCoveredMonth1_5.Top = 3.552083F;
			this.txtCoveredMonth1_5.Width = 0.25F;
			// 
			// txtCoveredMonth2_5
			// 
			this.txtCoveredMonth2_5.Height = 0.1770833F;
			this.txtCoveredMonth2_5.Left = 5.789583F;
			this.txtCoveredMonth2_5.Name = "txtCoveredMonth2_5";
			this.txtCoveredMonth2_5.Text = null;
			this.txtCoveredMonth2_5.Top = 3.552083F;
			this.txtCoveredMonth2_5.Width = 0.25F;
			// 
			// txtCoveredMonth3_5
			// 
			this.txtCoveredMonth3_5.Height = 0.1770833F;
			this.txtCoveredMonth3_5.Left = 6.183333F;
			this.txtCoveredMonth3_5.Name = "txtCoveredMonth3_5";
			this.txtCoveredMonth3_5.Text = null;
			this.txtCoveredMonth3_5.Top = 3.552083F;
			this.txtCoveredMonth3_5.Width = 0.25F;
			// 
			// txtCoveredMonth4_5
			// 
			this.txtCoveredMonth4_5.Height = 0.1770833F;
			this.txtCoveredMonth4_5.Left = 6.577083F;
			this.txtCoveredMonth4_5.Name = "txtCoveredMonth4_5";
			this.txtCoveredMonth4_5.Text = null;
			this.txtCoveredMonth4_5.Top = 3.552083F;
			this.txtCoveredMonth4_5.Width = 0.25F;
			// 
			// txtCoveredMonth5_5
			// 
			this.txtCoveredMonth5_5.Height = 0.1770833F;
			this.txtCoveredMonth5_5.Left = 6.970833F;
			this.txtCoveredMonth5_5.Name = "txtCoveredMonth5_5";
			this.txtCoveredMonth5_5.Text = null;
			this.txtCoveredMonth5_5.Top = 3.552083F;
			this.txtCoveredMonth5_5.Width = 0.25F;
			// 
			// txtCoveredMonth6_5
			// 
			this.txtCoveredMonth6_5.Height = 0.1770833F;
			this.txtCoveredMonth6_5.Left = 7.364583F;
			this.txtCoveredMonth6_5.Name = "txtCoveredMonth6_5";
			this.txtCoveredMonth6_5.Text = null;
			this.txtCoveredMonth6_5.Top = 3.552083F;
			this.txtCoveredMonth6_5.Width = 0.25F;
			// 
			// txtCoveredMonth7_5
			// 
			this.txtCoveredMonth7_5.Height = 0.1770833F;
			this.txtCoveredMonth7_5.Left = 7.758333F;
			this.txtCoveredMonth7_5.Name = "txtCoveredMonth7_5";
			this.txtCoveredMonth7_5.Text = null;
			this.txtCoveredMonth7_5.Top = 3.552083F;
			this.txtCoveredMonth7_5.Width = 0.25F;
			// 
			// txtCoveredMonth8_5
			// 
			this.txtCoveredMonth8_5.Height = 0.1770833F;
			this.txtCoveredMonth8_5.Left = 8.152083F;
			this.txtCoveredMonth8_5.Name = "txtCoveredMonth8_5";
			this.txtCoveredMonth8_5.Text = null;
			this.txtCoveredMonth8_5.Top = 3.552083F;
			this.txtCoveredMonth8_5.Width = 0.25F;
			// 
			// txtCoveredMonth9_5
			// 
			this.txtCoveredMonth9_5.Height = 0.1770833F;
			this.txtCoveredMonth9_5.Left = 8.545834F;
			this.txtCoveredMonth9_5.Name = "txtCoveredMonth9_5";
			this.txtCoveredMonth9_5.Text = null;
			this.txtCoveredMonth9_5.Top = 3.552083F;
			this.txtCoveredMonth9_5.Width = 0.25F;
			// 
			// txtCoveredMonth10_5
			// 
			this.txtCoveredMonth10_5.Height = 0.1770833F;
			this.txtCoveredMonth10_5.Left = 8.939584F;
			this.txtCoveredMonth10_5.Name = "txtCoveredMonth10_5";
			this.txtCoveredMonth10_5.Text = null;
			this.txtCoveredMonth10_5.Top = 3.552083F;
			this.txtCoveredMonth10_5.Width = 0.25F;
			// 
			// txtCoveredMonth11_5
			// 
			this.txtCoveredMonth11_5.Height = 0.1770833F;
			this.txtCoveredMonth11_5.Left = 9.333333F;
			this.txtCoveredMonth11_5.Name = "txtCoveredMonth11_5";
			this.txtCoveredMonth11_5.Text = null;
			this.txtCoveredMonth11_5.Top = 3.552083F;
			this.txtCoveredMonth11_5.Width = 0.25F;
			// 
			// txtCoveredMonth12_5
			// 
			this.txtCoveredMonth12_5.Height = 0.1770833F;
			this.txtCoveredMonth12_5.Left = 9.727083F;
			this.txtCoveredMonth12_5.Name = "txtCoveredMonth12_5";
			this.txtCoveredMonth12_5.Text = null;
			this.txtCoveredMonth12_5.Top = 3.552083F;
			this.txtCoveredMonth12_5.Width = 0.1979167F;
			// 
			// txtCoveredAll12_6
			// 
			this.txtCoveredAll12_6.Height = 0.1770833F;
			this.txtCoveredAll12_6.Left = 4.885417F;
			this.txtCoveredAll12_6.Name = "txtCoveredAll12_6";
			this.txtCoveredAll12_6.Text = null;
			this.txtCoveredAll12_6.Top = 4.041667F;
			this.txtCoveredAll12_6.Width = 0.25F;
			// 
			// txtCoveredMonth1_6
			// 
			this.txtCoveredMonth1_6.Height = 0.1770833F;
			this.txtCoveredMonth1_6.Left = 5.395833F;
			this.txtCoveredMonth1_6.Name = "txtCoveredMonth1_6";
			this.txtCoveredMonth1_6.Text = null;
			this.txtCoveredMonth1_6.Top = 4.041667F;
			this.txtCoveredMonth1_6.Width = 0.25F;
			// 
			// txtCoveredMonth2_6
			// 
			this.txtCoveredMonth2_6.Height = 0.1770833F;
			this.txtCoveredMonth2_6.Left = 5.789583F;
			this.txtCoveredMonth2_6.Name = "txtCoveredMonth2_6";
			this.txtCoveredMonth2_6.Text = null;
			this.txtCoveredMonth2_6.Top = 4.041667F;
			this.txtCoveredMonth2_6.Width = 0.25F;
			// 
			// txtCoveredMonth3_6
			// 
			this.txtCoveredMonth3_6.Height = 0.1770833F;
			this.txtCoveredMonth3_6.Left = 6.183333F;
			this.txtCoveredMonth3_6.Name = "txtCoveredMonth3_6";
			this.txtCoveredMonth3_6.Text = null;
			this.txtCoveredMonth3_6.Top = 4.041667F;
			this.txtCoveredMonth3_6.Width = 0.25F;
			// 
			// txtCoveredMonth4_6
			// 
			this.txtCoveredMonth4_6.Height = 0.1770833F;
			this.txtCoveredMonth4_6.Left = 6.577083F;
			this.txtCoveredMonth4_6.Name = "txtCoveredMonth4_6";
			this.txtCoveredMonth4_6.Text = null;
			this.txtCoveredMonth4_6.Top = 4.041667F;
			this.txtCoveredMonth4_6.Width = 0.25F;
			// 
			// txtCoveredMonth5_6
			// 
			this.txtCoveredMonth5_6.Height = 0.1770833F;
			this.txtCoveredMonth5_6.Left = 6.970833F;
			this.txtCoveredMonth5_6.Name = "txtCoveredMonth5_6";
			this.txtCoveredMonth5_6.Text = null;
			this.txtCoveredMonth5_6.Top = 4.041667F;
			this.txtCoveredMonth5_6.Width = 0.25F;
			// 
			// txtCoveredMonth6_6
			// 
			this.txtCoveredMonth6_6.Height = 0.1770833F;
			this.txtCoveredMonth6_6.Left = 7.364583F;
			this.txtCoveredMonth6_6.Name = "txtCoveredMonth6_6";
			this.txtCoveredMonth6_6.Text = null;
			this.txtCoveredMonth6_6.Top = 4.041667F;
			this.txtCoveredMonth6_6.Width = 0.25F;
			// 
			// txtCoveredMonth7_6
			// 
			this.txtCoveredMonth7_6.Height = 0.1770833F;
			this.txtCoveredMonth7_6.Left = 7.758333F;
			this.txtCoveredMonth7_6.Name = "txtCoveredMonth7_6";
			this.txtCoveredMonth7_6.Text = null;
			this.txtCoveredMonth7_6.Top = 4.041667F;
			this.txtCoveredMonth7_6.Width = 0.25F;
			// 
			// txtCoveredMonth8_6
			// 
			this.txtCoveredMonth8_6.Height = 0.1770833F;
			this.txtCoveredMonth8_6.Left = 8.152083F;
			this.txtCoveredMonth8_6.Name = "txtCoveredMonth8_6";
			this.txtCoveredMonth8_6.Text = null;
			this.txtCoveredMonth8_6.Top = 4.041667F;
			this.txtCoveredMonth8_6.Width = 0.25F;
			// 
			// txtCoveredMonth9_6
			// 
			this.txtCoveredMonth9_6.Height = 0.1770833F;
			this.txtCoveredMonth9_6.Left = 8.545834F;
			this.txtCoveredMonth9_6.Name = "txtCoveredMonth9_6";
			this.txtCoveredMonth9_6.Text = null;
			this.txtCoveredMonth9_6.Top = 4.041667F;
			this.txtCoveredMonth9_6.Width = 0.25F;
			// 
			// txtCoveredMonth10_6
			// 
			this.txtCoveredMonth10_6.Height = 0.1770833F;
			this.txtCoveredMonth10_6.Left = 8.939584F;
			this.txtCoveredMonth10_6.Name = "txtCoveredMonth10_6";
			this.txtCoveredMonth10_6.Text = null;
			this.txtCoveredMonth10_6.Top = 4.041667F;
			this.txtCoveredMonth10_6.Width = 0.25F;
			// 
			// txtCoveredMonth11_6
			// 
			this.txtCoveredMonth11_6.Height = 0.1770833F;
			this.txtCoveredMonth11_6.Left = 9.333333F;
			this.txtCoveredMonth11_6.Name = "txtCoveredMonth11_6";
			this.txtCoveredMonth11_6.Text = null;
			this.txtCoveredMonth11_6.Top = 4.041667F;
			this.txtCoveredMonth11_6.Width = 0.25F;
			// 
			// txtCoveredMonth12_6
			// 
			this.txtCoveredMonth12_6.Height = 0.1770833F;
			this.txtCoveredMonth12_6.Left = 9.727083F;
			this.txtCoveredMonth12_6.Name = "txtCoveredMonth12_6";
			this.txtCoveredMonth12_6.Text = null;
			this.txtCoveredMonth12_6.Top = 4.041667F;
			this.txtCoveredMonth12_6.Width = 0.1979167F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.1666667F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0.4722222F;
			this.txtName.Width = 5.25F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1770833F;
			this.txtSSN.Left = 5.583333F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.4722222F;
			this.txtSSN.Width = 1.729167F;
			// 
			// txtCoveredName7
			// 
			this.txtCoveredName7.Height = 0.1770833F;
			this.txtCoveredName7.Left = 0.1666667F;
			this.txtCoveredName7.Name = "txtCoveredName7";
			this.txtCoveredName7.Text = null;
			this.txtCoveredName7.Top = 4.541667F;
			this.txtCoveredName7.Width = 2.166667F;
			// 
			// txtCoveredSSN7
			// 
			this.txtCoveredSSN7.Height = 0.1770833F;
			this.txtCoveredSSN7.Left = 2.5F;
			this.txtCoveredSSN7.Name = "txtCoveredSSN7";
			this.txtCoveredSSN7.Text = null;
			this.txtCoveredSSN7.Top = 4.541667F;
			this.txtCoveredSSN7.Width = 1F;
			// 
			// txtCoveredDOB7
			// 
			this.txtCoveredDOB7.Height = 0.1770833F;
			this.txtCoveredDOB7.Left = 3.729167F;
			this.txtCoveredDOB7.Name = "txtCoveredDOB7";
			this.txtCoveredDOB7.Text = null;
			this.txtCoveredDOB7.Top = 4.541667F;
			this.txtCoveredDOB7.Width = 0.9166667F;
			// 
			// txtCoveredName8
			// 
			this.txtCoveredName8.Height = 0.1770833F;
			this.txtCoveredName8.Left = 0.1666667F;
			this.txtCoveredName8.Name = "txtCoveredName8";
			this.txtCoveredName8.Text = null;
			this.txtCoveredName8.Top = 5.041667F;
			this.txtCoveredName8.Width = 2.166667F;
			// 
			// txtCoveredSSN8
			// 
			this.txtCoveredSSN8.Height = 0.1770833F;
			this.txtCoveredSSN8.Left = 2.5F;
			this.txtCoveredSSN8.Name = "txtCoveredSSN8";
			this.txtCoveredSSN8.Text = null;
			this.txtCoveredSSN8.Top = 5.041667F;
			this.txtCoveredSSN8.Width = 1F;
			// 
			// txtCoveredDOB8
			// 
			this.txtCoveredDOB8.Height = 0.1770833F;
			this.txtCoveredDOB8.Left = 3.729167F;
			this.txtCoveredDOB8.Name = "txtCoveredDOB8";
			this.txtCoveredDOB8.Text = null;
			this.txtCoveredDOB8.Top = 5.041667F;
			this.txtCoveredDOB8.Width = 0.9166667F;
			// 
			// txtCoveredName9
			// 
			this.txtCoveredName9.Height = 0.1770833F;
			this.txtCoveredName9.Left = 0.1666667F;
			this.txtCoveredName9.Name = "txtCoveredName9";
			this.txtCoveredName9.Text = null;
			this.txtCoveredName9.Top = 5.541667F;
			this.txtCoveredName9.Width = 2.166667F;
			// 
			// txtCoveredSSN9
			// 
			this.txtCoveredSSN9.Height = 0.1770833F;
			this.txtCoveredSSN9.Left = 2.5F;
			this.txtCoveredSSN9.Name = "txtCoveredSSN9";
			this.txtCoveredSSN9.Text = null;
			this.txtCoveredSSN9.Top = 5.541667F;
			this.txtCoveredSSN9.Width = 1F;
			// 
			// txtCoveredDOB9
			// 
			this.txtCoveredDOB9.Height = 0.1770833F;
			this.txtCoveredDOB9.Left = 3.729167F;
			this.txtCoveredDOB9.Name = "txtCoveredDOB9";
			this.txtCoveredDOB9.Text = null;
			this.txtCoveredDOB9.Top = 5.541667F;
			this.txtCoveredDOB9.Width = 0.9166667F;
			// 
			// txtCoveredName10
			// 
			this.txtCoveredName10.Height = 0.1770833F;
			this.txtCoveredName10.Left = 0.1666667F;
			this.txtCoveredName10.Name = "txtCoveredName10";
			this.txtCoveredName10.Text = null;
			this.txtCoveredName10.Top = 6.041667F;
			this.txtCoveredName10.Width = 2.166667F;
			// 
			// txtCoveredSSN10
			// 
			this.txtCoveredSSN10.Height = 0.1770833F;
			this.txtCoveredSSN10.Left = 2.5F;
			this.txtCoveredSSN10.Name = "txtCoveredSSN10";
			this.txtCoveredSSN10.Text = null;
			this.txtCoveredSSN10.Top = 6.041667F;
			this.txtCoveredSSN10.Width = 1F;
			// 
			// txtCoveredDOB10
			// 
			this.txtCoveredDOB10.Height = 0.1770833F;
			this.txtCoveredDOB10.Left = 3.729167F;
			this.txtCoveredDOB10.Name = "txtCoveredDOB10";
			this.txtCoveredDOB10.Text = null;
			this.txtCoveredDOB10.Top = 6.041667F;
			this.txtCoveredDOB10.Width = 0.9166667F;
			// 
			// txtCoveredName11
			// 
			this.txtCoveredName11.Height = 0.1770833F;
			this.txtCoveredName11.Left = 0.1666667F;
			this.txtCoveredName11.Name = "txtCoveredName11";
			this.txtCoveredName11.Text = null;
			this.txtCoveredName11.Top = 6.53125F;
			this.txtCoveredName11.Width = 2.166667F;
			// 
			// txtCoveredSSN11
			// 
			this.txtCoveredSSN11.Height = 0.1770833F;
			this.txtCoveredSSN11.Left = 2.5F;
			this.txtCoveredSSN11.Name = "txtCoveredSSN11";
			this.txtCoveredSSN11.Text = null;
			this.txtCoveredSSN11.Top = 6.53125F;
			this.txtCoveredSSN11.Width = 1F;
			// 
			// txtCoveredDOB11
			// 
			this.txtCoveredDOB11.Height = 0.1770833F;
			this.txtCoveredDOB11.Left = 3.729167F;
			this.txtCoveredDOB11.Name = "txtCoveredDOB11";
			this.txtCoveredDOB11.Text = null;
			this.txtCoveredDOB11.Top = 6.53125F;
			this.txtCoveredDOB11.Width = 0.9166667F;
			// 
			// txtCoveredName12
			// 
			this.txtCoveredName12.Height = 0.1770833F;
			this.txtCoveredName12.Left = 0.1666667F;
			this.txtCoveredName12.Name = "txtCoveredName12";
			this.txtCoveredName12.Text = null;
			this.txtCoveredName12.Top = 7.03125F;
			this.txtCoveredName12.Width = 2.166667F;
			// 
			// txtCoveredSSN12
			// 
			this.txtCoveredSSN12.Height = 0.1770833F;
			this.txtCoveredSSN12.Left = 2.5F;
			this.txtCoveredSSN12.Name = "txtCoveredSSN12";
			this.txtCoveredSSN12.Text = null;
			this.txtCoveredSSN12.Top = 7.03125F;
			this.txtCoveredSSN12.Width = 1F;
			// 
			// txtCoveredDOB12
			// 
			this.txtCoveredDOB12.Height = 0.1770833F;
			this.txtCoveredDOB12.Left = 3.729167F;
			this.txtCoveredDOB12.Name = "txtCoveredDOB12";
			this.txtCoveredDOB12.Text = null;
			this.txtCoveredDOB12.Top = 7.03125F;
			this.txtCoveredDOB12.Width = 0.9166667F;
			// 
			// txtCoveredAll12_7
			// 
			this.txtCoveredAll12_7.Height = 0.1770833F;
			this.txtCoveredAll12_7.Left = 4.885417F;
			this.txtCoveredAll12_7.Name = "txtCoveredAll12_7";
			this.txtCoveredAll12_7.Text = null;
			this.txtCoveredAll12_7.Top = 4.541667F;
			this.txtCoveredAll12_7.Width = 0.25F;
			// 
			// txtCoveredMonth1_7
			// 
			this.txtCoveredMonth1_7.Height = 0.1770833F;
			this.txtCoveredMonth1_7.Left = 5.395833F;
			this.txtCoveredMonth1_7.Name = "txtCoveredMonth1_7";
			this.txtCoveredMonth1_7.Text = null;
			this.txtCoveredMonth1_7.Top = 4.541667F;
			this.txtCoveredMonth1_7.Width = 0.25F;
			// 
			// txtCoveredMonth2_7
			// 
			this.txtCoveredMonth2_7.Height = 0.1770833F;
			this.txtCoveredMonth2_7.Left = 5.789583F;
			this.txtCoveredMonth2_7.Name = "txtCoveredMonth2_7";
			this.txtCoveredMonth2_7.Text = null;
			this.txtCoveredMonth2_7.Top = 4.541667F;
			this.txtCoveredMonth2_7.Width = 0.25F;
			// 
			// txtCoveredMonth3_7
			// 
			this.txtCoveredMonth3_7.Height = 0.1770833F;
			this.txtCoveredMonth3_7.Left = 6.183333F;
			this.txtCoveredMonth3_7.Name = "txtCoveredMonth3_7";
			this.txtCoveredMonth3_7.Text = null;
			this.txtCoveredMonth3_7.Top = 4.541667F;
			this.txtCoveredMonth3_7.Width = 0.25F;
			// 
			// txtCoveredMonth4_7
			// 
			this.txtCoveredMonth4_7.Height = 0.1770833F;
			this.txtCoveredMonth4_7.Left = 6.577083F;
			this.txtCoveredMonth4_7.Name = "txtCoveredMonth4_7";
			this.txtCoveredMonth4_7.Text = null;
			this.txtCoveredMonth4_7.Top = 4.541667F;
			this.txtCoveredMonth4_7.Width = 0.25F;
			// 
			// txtCoveredMonth5_7
			// 
			this.txtCoveredMonth5_7.Height = 0.1770833F;
			this.txtCoveredMonth5_7.Left = 6.970833F;
			this.txtCoveredMonth5_7.Name = "txtCoveredMonth5_7";
			this.txtCoveredMonth5_7.Text = null;
			this.txtCoveredMonth5_7.Top = 4.541667F;
			this.txtCoveredMonth5_7.Width = 0.25F;
			// 
			// txtCoveredMonth6_7
			// 
			this.txtCoveredMonth6_7.Height = 0.1770833F;
			this.txtCoveredMonth6_7.Left = 7.364583F;
			this.txtCoveredMonth6_7.Name = "txtCoveredMonth6_7";
			this.txtCoveredMonth6_7.Text = null;
			this.txtCoveredMonth6_7.Top = 4.541667F;
			this.txtCoveredMonth6_7.Width = 0.25F;
			// 
			// txtCoveredMonth7_7
			// 
			this.txtCoveredMonth7_7.Height = 0.1770833F;
			this.txtCoveredMonth7_7.Left = 7.758333F;
			this.txtCoveredMonth7_7.Name = "txtCoveredMonth7_7";
			this.txtCoveredMonth7_7.Text = null;
			this.txtCoveredMonth7_7.Top = 4.541667F;
			this.txtCoveredMonth7_7.Width = 0.25F;
			// 
			// txtCoveredMonth8_7
			// 
			this.txtCoveredMonth8_7.Height = 0.1770833F;
			this.txtCoveredMonth8_7.Left = 8.152083F;
			this.txtCoveredMonth8_7.Name = "txtCoveredMonth8_7";
			this.txtCoveredMonth8_7.Text = null;
			this.txtCoveredMonth8_7.Top = 4.541667F;
			this.txtCoveredMonth8_7.Width = 0.25F;
			// 
			// txtCoveredMonth9_7
			// 
			this.txtCoveredMonth9_7.Height = 0.1770833F;
			this.txtCoveredMonth9_7.Left = 8.545834F;
			this.txtCoveredMonth9_7.Name = "txtCoveredMonth9_7";
			this.txtCoveredMonth9_7.Text = null;
			this.txtCoveredMonth9_7.Top = 4.541667F;
			this.txtCoveredMonth9_7.Width = 0.25F;
			// 
			// txtCoveredMonth10_7
			// 
			this.txtCoveredMonth10_7.Height = 0.1770833F;
			this.txtCoveredMonth10_7.Left = 8.939584F;
			this.txtCoveredMonth10_7.Name = "txtCoveredMonth10_7";
			this.txtCoveredMonth10_7.Text = null;
			this.txtCoveredMonth10_7.Top = 4.541667F;
			this.txtCoveredMonth10_7.Width = 0.25F;
			// 
			// txtCoveredMonth11_7
			// 
			this.txtCoveredMonth11_7.Height = 0.1770833F;
			this.txtCoveredMonth11_7.Left = 9.333333F;
			this.txtCoveredMonth11_7.Name = "txtCoveredMonth11_7";
			this.txtCoveredMonth11_7.Text = null;
			this.txtCoveredMonth11_7.Top = 4.541667F;
			this.txtCoveredMonth11_7.Width = 0.25F;
			// 
			// txtCoveredMonth12_7
			// 
			this.txtCoveredMonth12_7.Height = 0.1770833F;
			this.txtCoveredMonth12_7.Left = 9.727083F;
			this.txtCoveredMonth12_7.Name = "txtCoveredMonth12_7";
			this.txtCoveredMonth12_7.Text = null;
			this.txtCoveredMonth12_7.Top = 4.541667F;
			this.txtCoveredMonth12_7.Width = 0.1979167F;
			// 
			// txtCoveredAll12_8
			// 
			this.txtCoveredAll12_8.Height = 0.1770833F;
			this.txtCoveredAll12_8.Left = 4.885417F;
			this.txtCoveredAll12_8.Name = "txtCoveredAll12_8";
			this.txtCoveredAll12_8.Text = null;
			this.txtCoveredAll12_8.Top = 5.041667F;
			this.txtCoveredAll12_8.Width = 0.25F;
			// 
			// txtCoveredMonth1_8
			// 
			this.txtCoveredMonth1_8.Height = 0.1770833F;
			this.txtCoveredMonth1_8.Left = 5.395833F;
			this.txtCoveredMonth1_8.Name = "txtCoveredMonth1_8";
			this.txtCoveredMonth1_8.Text = null;
			this.txtCoveredMonth1_8.Top = 5.041667F;
			this.txtCoveredMonth1_8.Width = 0.25F;
			// 
			// txtCoveredMonth2_8
			// 
			this.txtCoveredMonth2_8.Height = 0.1770833F;
			this.txtCoveredMonth2_8.Left = 5.789583F;
			this.txtCoveredMonth2_8.Name = "txtCoveredMonth2_8";
			this.txtCoveredMonth2_8.Text = null;
			this.txtCoveredMonth2_8.Top = 5.041667F;
			this.txtCoveredMonth2_8.Width = 0.25F;
			// 
			// txtCoveredMonth3_8
			// 
			this.txtCoveredMonth3_8.Height = 0.1770833F;
			this.txtCoveredMonth3_8.Left = 6.183333F;
			this.txtCoveredMonth3_8.Name = "txtCoveredMonth3_8";
			this.txtCoveredMonth3_8.Text = null;
			this.txtCoveredMonth3_8.Top = 5.041667F;
			this.txtCoveredMonth3_8.Width = 0.25F;
			// 
			// txtCoveredMonth4_8
			// 
			this.txtCoveredMonth4_8.Height = 0.1770833F;
			this.txtCoveredMonth4_8.Left = 6.577083F;
			this.txtCoveredMonth4_8.Name = "txtCoveredMonth4_8";
			this.txtCoveredMonth4_8.Text = null;
			this.txtCoveredMonth4_8.Top = 5.041667F;
			this.txtCoveredMonth4_8.Width = 0.25F;
			// 
			// txtCoveredMonth5_8
			// 
			this.txtCoveredMonth5_8.Height = 0.1770833F;
			this.txtCoveredMonth5_8.Left = 6.970833F;
			this.txtCoveredMonth5_8.Name = "txtCoveredMonth5_8";
			this.txtCoveredMonth5_8.Text = null;
			this.txtCoveredMonth5_8.Top = 5.041667F;
			this.txtCoveredMonth5_8.Width = 0.25F;
			// 
			// txtCoveredMonth6_8
			// 
			this.txtCoveredMonth6_8.Height = 0.1770833F;
			this.txtCoveredMonth6_8.Left = 7.364583F;
			this.txtCoveredMonth6_8.Name = "txtCoveredMonth6_8";
			this.txtCoveredMonth6_8.Text = null;
			this.txtCoveredMonth6_8.Top = 5.041667F;
			this.txtCoveredMonth6_8.Width = 0.25F;
			// 
			// txtCoveredMonth7_8
			// 
			this.txtCoveredMonth7_8.Height = 0.1770833F;
			this.txtCoveredMonth7_8.Left = 7.758333F;
			this.txtCoveredMonth7_8.Name = "txtCoveredMonth7_8";
			this.txtCoveredMonth7_8.Text = null;
			this.txtCoveredMonth7_8.Top = 5.041667F;
			this.txtCoveredMonth7_8.Width = 0.25F;
			// 
			// txtCoveredMonth8_8
			// 
			this.txtCoveredMonth8_8.Height = 0.1770833F;
			this.txtCoveredMonth8_8.Left = 8.152083F;
			this.txtCoveredMonth8_8.Name = "txtCoveredMonth8_8";
			this.txtCoveredMonth8_8.Text = null;
			this.txtCoveredMonth8_8.Top = 5.041667F;
			this.txtCoveredMonth8_8.Width = 0.25F;
			// 
			// txtCoveredMonth9_8
			// 
			this.txtCoveredMonth9_8.Height = 0.1770833F;
			this.txtCoveredMonth9_8.Left = 8.545834F;
			this.txtCoveredMonth9_8.Name = "txtCoveredMonth9_8";
			this.txtCoveredMonth9_8.Text = null;
			this.txtCoveredMonth9_8.Top = 5.041667F;
			this.txtCoveredMonth9_8.Width = 0.25F;
			// 
			// txtCoveredMonth10_8
			// 
			this.txtCoveredMonth10_8.Height = 0.1770833F;
			this.txtCoveredMonth10_8.Left = 8.939584F;
			this.txtCoveredMonth10_8.Name = "txtCoveredMonth10_8";
			this.txtCoveredMonth10_8.Text = null;
			this.txtCoveredMonth10_8.Top = 5.041667F;
			this.txtCoveredMonth10_8.Width = 0.25F;
			// 
			// txtCoveredMonth11_8
			// 
			this.txtCoveredMonth11_8.Height = 0.1770833F;
			this.txtCoveredMonth11_8.Left = 9.333333F;
			this.txtCoveredMonth11_8.Name = "txtCoveredMonth11_8";
			this.txtCoveredMonth11_8.Text = null;
			this.txtCoveredMonth11_8.Top = 5.041667F;
			this.txtCoveredMonth11_8.Width = 0.25F;
			// 
			// txtCoveredMonth12_8
			// 
			this.txtCoveredMonth12_8.Height = 0.1770833F;
			this.txtCoveredMonth12_8.Left = 9.727083F;
			this.txtCoveredMonth12_8.Name = "txtCoveredMonth12_8";
			this.txtCoveredMonth12_8.Text = null;
			this.txtCoveredMonth12_8.Top = 5.041667F;
			this.txtCoveredMonth12_8.Width = 0.1979167F;
			// 
			// txtCoveredAll12_9
			// 
			this.txtCoveredAll12_9.Height = 0.1770833F;
			this.txtCoveredAll12_9.Left = 4.885417F;
			this.txtCoveredAll12_9.Name = "txtCoveredAll12_9";
			this.txtCoveredAll12_9.Text = null;
			this.txtCoveredAll12_9.Top = 5.541667F;
			this.txtCoveredAll12_9.Width = 0.25F;
			// 
			// txtCoveredMonth1_9
			// 
			this.txtCoveredMonth1_9.Height = 0.1770833F;
			this.txtCoveredMonth1_9.Left = 5.395833F;
			this.txtCoveredMonth1_9.Name = "txtCoveredMonth1_9";
			this.txtCoveredMonth1_9.Text = null;
			this.txtCoveredMonth1_9.Top = 5.541667F;
			this.txtCoveredMonth1_9.Width = 0.25F;
			// 
			// txtCoveredMonth2_9
			// 
			this.txtCoveredMonth2_9.Height = 0.1770833F;
			this.txtCoveredMonth2_9.Left = 5.789583F;
			this.txtCoveredMonth2_9.Name = "txtCoveredMonth2_9";
			this.txtCoveredMonth2_9.Text = null;
			this.txtCoveredMonth2_9.Top = 5.541667F;
			this.txtCoveredMonth2_9.Width = 0.25F;
			// 
			// txtCoveredMonth3_9
			// 
			this.txtCoveredMonth3_9.Height = 0.1770833F;
			this.txtCoveredMonth3_9.Left = 6.183333F;
			this.txtCoveredMonth3_9.Name = "txtCoveredMonth3_9";
			this.txtCoveredMonth3_9.Text = null;
			this.txtCoveredMonth3_9.Top = 5.541667F;
			this.txtCoveredMonth3_9.Width = 0.25F;
			// 
			// txtCoveredMonth4_9
			// 
			this.txtCoveredMonth4_9.Height = 0.1770833F;
			this.txtCoveredMonth4_9.Left = 6.577083F;
			this.txtCoveredMonth4_9.Name = "txtCoveredMonth4_9";
			this.txtCoveredMonth4_9.Text = null;
			this.txtCoveredMonth4_9.Top = 5.541667F;
			this.txtCoveredMonth4_9.Width = 0.25F;
			// 
			// txtCoveredMonth5_9
			// 
			this.txtCoveredMonth5_9.Height = 0.1770833F;
			this.txtCoveredMonth5_9.Left = 6.970833F;
			this.txtCoveredMonth5_9.Name = "txtCoveredMonth5_9";
			this.txtCoveredMonth5_9.Text = null;
			this.txtCoveredMonth5_9.Top = 5.541667F;
			this.txtCoveredMonth5_9.Width = 0.25F;
			// 
			// txtCoveredMonth6_9
			// 
			this.txtCoveredMonth6_9.Height = 0.1770833F;
			this.txtCoveredMonth6_9.Left = 7.364583F;
			this.txtCoveredMonth6_9.Name = "txtCoveredMonth6_9";
			this.txtCoveredMonth6_9.Text = null;
			this.txtCoveredMonth6_9.Top = 5.541667F;
			this.txtCoveredMonth6_9.Width = 0.25F;
			// 
			// txtCoveredMonth7_9
			// 
			this.txtCoveredMonth7_9.Height = 0.1770833F;
			this.txtCoveredMonth7_9.Left = 7.758333F;
			this.txtCoveredMonth7_9.Name = "txtCoveredMonth7_9";
			this.txtCoveredMonth7_9.Text = null;
			this.txtCoveredMonth7_9.Top = 5.541667F;
			this.txtCoveredMonth7_9.Width = 0.25F;
			// 
			// txtCoveredMonth8_9
			// 
			this.txtCoveredMonth8_9.Height = 0.1770833F;
			this.txtCoveredMonth8_9.Left = 8.152083F;
			this.txtCoveredMonth8_9.Name = "txtCoveredMonth8_9";
			this.txtCoveredMonth8_9.Text = null;
			this.txtCoveredMonth8_9.Top = 5.541667F;
			this.txtCoveredMonth8_9.Width = 0.25F;
			// 
			// txtCoveredMonth9_9
			// 
			this.txtCoveredMonth9_9.Height = 0.1770833F;
			this.txtCoveredMonth9_9.Left = 8.545834F;
			this.txtCoveredMonth9_9.Name = "txtCoveredMonth9_9";
			this.txtCoveredMonth9_9.Text = null;
			this.txtCoveredMonth9_9.Top = 5.541667F;
			this.txtCoveredMonth9_9.Width = 0.25F;
			// 
			// txtCoveredMonth10_9
			// 
			this.txtCoveredMonth10_9.Height = 0.1770833F;
			this.txtCoveredMonth10_9.Left = 8.939584F;
			this.txtCoveredMonth10_9.Name = "txtCoveredMonth10_9";
			this.txtCoveredMonth10_9.Text = null;
			this.txtCoveredMonth10_9.Top = 5.541667F;
			this.txtCoveredMonth10_9.Width = 0.25F;
			// 
			// txtCoveredMonth11_9
			// 
			this.txtCoveredMonth11_9.Height = 0.1770833F;
			this.txtCoveredMonth11_9.Left = 9.333333F;
			this.txtCoveredMonth11_9.Name = "txtCoveredMonth11_9";
			this.txtCoveredMonth11_9.Text = null;
			this.txtCoveredMonth11_9.Top = 5.541667F;
			this.txtCoveredMonth11_9.Width = 0.25F;
			// 
			// txtCoveredMonth12_9
			// 
			this.txtCoveredMonth12_9.Height = 0.1770833F;
			this.txtCoveredMonth12_9.Left = 9.727083F;
			this.txtCoveredMonth12_9.Name = "txtCoveredMonth12_9";
			this.txtCoveredMonth12_9.Text = null;
			this.txtCoveredMonth12_9.Top = 5.541667F;
			this.txtCoveredMonth12_9.Width = 0.1979167F;
			// 
			// txtCoveredAll12_10
			// 
			this.txtCoveredAll12_10.Height = 0.1770833F;
			this.txtCoveredAll12_10.Left = 4.885417F;
			this.txtCoveredAll12_10.Name = "txtCoveredAll12_10";
			this.txtCoveredAll12_10.Text = null;
			this.txtCoveredAll12_10.Top = 6.041667F;
			this.txtCoveredAll12_10.Width = 0.25F;
			// 
			// txtCoveredMonth1_10
			// 
			this.txtCoveredMonth1_10.Height = 0.1770833F;
			this.txtCoveredMonth1_10.Left = 5.395833F;
			this.txtCoveredMonth1_10.Name = "txtCoveredMonth1_10";
			this.txtCoveredMonth1_10.Text = null;
			this.txtCoveredMonth1_10.Top = 6.041667F;
			this.txtCoveredMonth1_10.Width = 0.25F;
			// 
			// txtCoveredMonth2_10
			// 
			this.txtCoveredMonth2_10.Height = 0.1770833F;
			this.txtCoveredMonth2_10.Left = 5.789583F;
			this.txtCoveredMonth2_10.Name = "txtCoveredMonth2_10";
			this.txtCoveredMonth2_10.Text = null;
			this.txtCoveredMonth2_10.Top = 6.041667F;
			this.txtCoveredMonth2_10.Width = 0.25F;
			// 
			// txtCoveredMonth3_10
			// 
			this.txtCoveredMonth3_10.Height = 0.1770833F;
			this.txtCoveredMonth3_10.Left = 6.183333F;
			this.txtCoveredMonth3_10.Name = "txtCoveredMonth3_10";
			this.txtCoveredMonth3_10.Text = null;
			this.txtCoveredMonth3_10.Top = 6.041667F;
			this.txtCoveredMonth3_10.Width = 0.25F;
			// 
			// txtCoveredMonth4_10
			// 
			this.txtCoveredMonth4_10.Height = 0.1770833F;
			this.txtCoveredMonth4_10.Left = 6.577083F;
			this.txtCoveredMonth4_10.Name = "txtCoveredMonth4_10";
			this.txtCoveredMonth4_10.Text = null;
			this.txtCoveredMonth4_10.Top = 6.041667F;
			this.txtCoveredMonth4_10.Width = 0.25F;
			// 
			// txtCoveredMonth5_10
			// 
			this.txtCoveredMonth5_10.Height = 0.1770833F;
			this.txtCoveredMonth5_10.Left = 6.970833F;
			this.txtCoveredMonth5_10.Name = "txtCoveredMonth5_10";
			this.txtCoveredMonth5_10.Text = null;
			this.txtCoveredMonth5_10.Top = 6.041667F;
			this.txtCoveredMonth5_10.Width = 0.25F;
			// 
			// txtCoveredMonth6_10
			// 
			this.txtCoveredMonth6_10.Height = 0.1770833F;
			this.txtCoveredMonth6_10.Left = 7.364583F;
			this.txtCoveredMonth6_10.Name = "txtCoveredMonth6_10";
			this.txtCoveredMonth6_10.Text = null;
			this.txtCoveredMonth6_10.Top = 6.041667F;
			this.txtCoveredMonth6_10.Width = 0.25F;
			// 
			// txtCoveredMonth7_10
			// 
			this.txtCoveredMonth7_10.Height = 0.1770833F;
			this.txtCoveredMonth7_10.Left = 7.758333F;
			this.txtCoveredMonth7_10.Name = "txtCoveredMonth7_10";
			this.txtCoveredMonth7_10.Text = null;
			this.txtCoveredMonth7_10.Top = 6.041667F;
			this.txtCoveredMonth7_10.Width = 0.25F;
			// 
			// txtCoveredMonth8_10
			// 
			this.txtCoveredMonth8_10.Height = 0.1770833F;
			this.txtCoveredMonth8_10.Left = 8.152083F;
			this.txtCoveredMonth8_10.Name = "txtCoveredMonth8_10";
			this.txtCoveredMonth8_10.Text = null;
			this.txtCoveredMonth8_10.Top = 6.041667F;
			this.txtCoveredMonth8_10.Width = 0.25F;
			// 
			// txtCoveredMonth9_10
			// 
			this.txtCoveredMonth9_10.Height = 0.1770833F;
			this.txtCoveredMonth9_10.Left = 8.545834F;
			this.txtCoveredMonth9_10.Name = "txtCoveredMonth9_10";
			this.txtCoveredMonth9_10.Text = null;
			this.txtCoveredMonth9_10.Top = 6.041667F;
			this.txtCoveredMonth9_10.Width = 0.25F;
			// 
			// txtCoveredMonth10_10
			// 
			this.txtCoveredMonth10_10.Height = 0.1770833F;
			this.txtCoveredMonth10_10.Left = 8.939584F;
			this.txtCoveredMonth10_10.Name = "txtCoveredMonth10_10";
			this.txtCoveredMonth10_10.Text = null;
			this.txtCoveredMonth10_10.Top = 6.041667F;
			this.txtCoveredMonth10_10.Width = 0.25F;
			// 
			// txtCoveredMonth11_10
			// 
			this.txtCoveredMonth11_10.Height = 0.1770833F;
			this.txtCoveredMonth11_10.Left = 9.333333F;
			this.txtCoveredMonth11_10.Name = "txtCoveredMonth11_10";
			this.txtCoveredMonth11_10.Text = null;
			this.txtCoveredMonth11_10.Top = 6.041667F;
			this.txtCoveredMonth11_10.Width = 0.25F;
			// 
			// txtCoveredMonth12_10
			// 
			this.txtCoveredMonth12_10.Height = 0.1770833F;
			this.txtCoveredMonth12_10.Left = 9.727083F;
			this.txtCoveredMonth12_10.Name = "txtCoveredMonth12_10";
			this.txtCoveredMonth12_10.Text = null;
			this.txtCoveredMonth12_10.Top = 6.041667F;
			this.txtCoveredMonth12_10.Width = 0.1979167F;
			// 
			// txtCoveredAll12_11
			// 
			this.txtCoveredAll12_11.Height = 0.1770833F;
			this.txtCoveredAll12_11.Left = 4.885417F;
			this.txtCoveredAll12_11.Name = "txtCoveredAll12_11";
			this.txtCoveredAll12_11.Text = null;
			this.txtCoveredAll12_11.Top = 6.53125F;
			this.txtCoveredAll12_11.Width = 0.25F;
			// 
			// txtCoveredMonth1_11
			// 
			this.txtCoveredMonth1_11.Height = 0.1770833F;
			this.txtCoveredMonth1_11.Left = 5.395833F;
			this.txtCoveredMonth1_11.Name = "txtCoveredMonth1_11";
			this.txtCoveredMonth1_11.Text = null;
			this.txtCoveredMonth1_11.Top = 6.53125F;
			this.txtCoveredMonth1_11.Width = 0.25F;
			// 
			// txtCoveredMonth2_11
			// 
			this.txtCoveredMonth2_11.Height = 0.1770833F;
			this.txtCoveredMonth2_11.Left = 5.789583F;
			this.txtCoveredMonth2_11.Name = "txtCoveredMonth2_11";
			this.txtCoveredMonth2_11.Text = null;
			this.txtCoveredMonth2_11.Top = 6.53125F;
			this.txtCoveredMonth2_11.Width = 0.25F;
			// 
			// txtCoveredMonth3_11
			// 
			this.txtCoveredMonth3_11.Height = 0.1770833F;
			this.txtCoveredMonth3_11.Left = 6.183333F;
			this.txtCoveredMonth3_11.Name = "txtCoveredMonth3_11";
			this.txtCoveredMonth3_11.Text = null;
			this.txtCoveredMonth3_11.Top = 6.53125F;
			this.txtCoveredMonth3_11.Width = 0.25F;
			// 
			// txtCoveredMonth4_11
			// 
			this.txtCoveredMonth4_11.Height = 0.1770833F;
			this.txtCoveredMonth4_11.Left = 6.577083F;
			this.txtCoveredMonth4_11.Name = "txtCoveredMonth4_11";
			this.txtCoveredMonth4_11.Text = null;
			this.txtCoveredMonth4_11.Top = 6.53125F;
			this.txtCoveredMonth4_11.Width = 0.25F;
			// 
			// txtCoveredMonth5_11
			// 
			this.txtCoveredMonth5_11.Height = 0.1770833F;
			this.txtCoveredMonth5_11.Left = 6.970833F;
			this.txtCoveredMonth5_11.Name = "txtCoveredMonth5_11";
			this.txtCoveredMonth5_11.Text = null;
			this.txtCoveredMonth5_11.Top = 6.53125F;
			this.txtCoveredMonth5_11.Width = 0.25F;
			// 
			// txtCoveredMonth6_11
			// 
			this.txtCoveredMonth6_11.Height = 0.1770833F;
			this.txtCoveredMonth6_11.Left = 7.364583F;
			this.txtCoveredMonth6_11.Name = "txtCoveredMonth6_11";
			this.txtCoveredMonth6_11.Text = null;
			this.txtCoveredMonth6_11.Top = 6.53125F;
			this.txtCoveredMonth6_11.Width = 0.25F;
			// 
			// txtCoveredMonth7_11
			// 
			this.txtCoveredMonth7_11.Height = 0.1770833F;
			this.txtCoveredMonth7_11.Left = 7.758333F;
			this.txtCoveredMonth7_11.Name = "txtCoveredMonth7_11";
			this.txtCoveredMonth7_11.Text = null;
			this.txtCoveredMonth7_11.Top = 6.53125F;
			this.txtCoveredMonth7_11.Width = 0.25F;
			// 
			// txtCoveredMonth8_11
			// 
			this.txtCoveredMonth8_11.Height = 0.1770833F;
			this.txtCoveredMonth8_11.Left = 8.152083F;
			this.txtCoveredMonth8_11.Name = "txtCoveredMonth8_11";
			this.txtCoveredMonth8_11.Text = null;
			this.txtCoveredMonth8_11.Top = 6.53125F;
			this.txtCoveredMonth8_11.Width = 0.25F;
			// 
			// txtCoveredMonth9_11
			// 
			this.txtCoveredMonth9_11.Height = 0.1770833F;
			this.txtCoveredMonth9_11.Left = 8.545834F;
			this.txtCoveredMonth9_11.Name = "txtCoveredMonth9_11";
			this.txtCoveredMonth9_11.Text = null;
			this.txtCoveredMonth9_11.Top = 6.53125F;
			this.txtCoveredMonth9_11.Width = 0.25F;
			// 
			// txtCoveredMonth10_11
			// 
			this.txtCoveredMonth10_11.Height = 0.1770833F;
			this.txtCoveredMonth10_11.Left = 8.939584F;
			this.txtCoveredMonth10_11.Name = "txtCoveredMonth10_11";
			this.txtCoveredMonth10_11.Text = null;
			this.txtCoveredMonth10_11.Top = 6.53125F;
			this.txtCoveredMonth10_11.Width = 0.25F;
			// 
			// txtCoveredMonth11_11
			// 
			this.txtCoveredMonth11_11.Height = 0.1770833F;
			this.txtCoveredMonth11_11.Left = 9.333333F;
			this.txtCoveredMonth11_11.Name = "txtCoveredMonth11_11";
			this.txtCoveredMonth11_11.Text = null;
			this.txtCoveredMonth11_11.Top = 6.53125F;
			this.txtCoveredMonth11_11.Width = 0.25F;
			// 
			// txtCoveredMonth12_11
			// 
			this.txtCoveredMonth12_11.Height = 0.1770833F;
			this.txtCoveredMonth12_11.Left = 9.727083F;
			this.txtCoveredMonth12_11.Name = "txtCoveredMonth12_11";
			this.txtCoveredMonth12_11.Text = null;
			this.txtCoveredMonth12_11.Top = 6.53125F;
			this.txtCoveredMonth12_11.Width = 0.1979167F;
			// 
			// txtCoveredAll12_12
			// 
			this.txtCoveredAll12_12.Height = 0.1770833F;
			this.txtCoveredAll12_12.Left = 4.885417F;
			this.txtCoveredAll12_12.Name = "txtCoveredAll12_12";
			this.txtCoveredAll12_12.Text = null;
			this.txtCoveredAll12_12.Top = 7.03125F;
			this.txtCoveredAll12_12.Width = 0.25F;
			// 
			// txtCoveredMonth1_12
			// 
			this.txtCoveredMonth1_12.Height = 0.1770833F;
			this.txtCoveredMonth1_12.Left = 5.395833F;
			this.txtCoveredMonth1_12.Name = "txtCoveredMonth1_12";
			this.txtCoveredMonth1_12.Text = null;
			this.txtCoveredMonth1_12.Top = 7.03125F;
			this.txtCoveredMonth1_12.Width = 0.25F;
			// 
			// txtCoveredMonth2_12
			// 
			this.txtCoveredMonth2_12.Height = 0.1770833F;
			this.txtCoveredMonth2_12.Left = 5.789583F;
			this.txtCoveredMonth2_12.Name = "txtCoveredMonth2_12";
			this.txtCoveredMonth2_12.Text = null;
			this.txtCoveredMonth2_12.Top = 7.03125F;
			this.txtCoveredMonth2_12.Width = 0.25F;
			// 
			// txtCoveredMonth3_12
			// 
			this.txtCoveredMonth3_12.Height = 0.1770833F;
			this.txtCoveredMonth3_12.Left = 6.183333F;
			this.txtCoveredMonth3_12.Name = "txtCoveredMonth3_12";
			this.txtCoveredMonth3_12.Text = null;
			this.txtCoveredMonth3_12.Top = 7.03125F;
			this.txtCoveredMonth3_12.Width = 0.25F;
			// 
			// txtCoveredMonth4_12
			// 
			this.txtCoveredMonth4_12.Height = 0.1770833F;
			this.txtCoveredMonth4_12.Left = 6.577083F;
			this.txtCoveredMonth4_12.Name = "txtCoveredMonth4_12";
			this.txtCoveredMonth4_12.Text = null;
			this.txtCoveredMonth4_12.Top = 7.03125F;
			this.txtCoveredMonth4_12.Width = 0.25F;
			// 
			// txtCoveredMonth5_12
			// 
			this.txtCoveredMonth5_12.Height = 0.1770833F;
			this.txtCoveredMonth5_12.Left = 6.970833F;
			this.txtCoveredMonth5_12.Name = "txtCoveredMonth5_12";
			this.txtCoveredMonth5_12.Text = null;
			this.txtCoveredMonth5_12.Top = 7.03125F;
			this.txtCoveredMonth5_12.Width = 0.25F;
			// 
			// txtCoveredMonth6_12
			// 
			this.txtCoveredMonth6_12.Height = 0.1770833F;
			this.txtCoveredMonth6_12.Left = 7.364583F;
			this.txtCoveredMonth6_12.Name = "txtCoveredMonth6_12";
			this.txtCoveredMonth6_12.Text = null;
			this.txtCoveredMonth6_12.Top = 7.03125F;
			this.txtCoveredMonth6_12.Width = 0.25F;
			// 
			// txtCoveredMonth7_12
			// 
			this.txtCoveredMonth7_12.Height = 0.1770833F;
			this.txtCoveredMonth7_12.Left = 7.758333F;
			this.txtCoveredMonth7_12.Name = "txtCoveredMonth7_12";
			this.txtCoveredMonth7_12.Text = null;
			this.txtCoveredMonth7_12.Top = 7.03125F;
			this.txtCoveredMonth7_12.Width = 0.25F;
			// 
			// txtCoveredMonth8_12
			// 
			this.txtCoveredMonth8_12.Height = 0.1770833F;
			this.txtCoveredMonth8_12.Left = 8.152083F;
			this.txtCoveredMonth8_12.Name = "txtCoveredMonth8_12";
			this.txtCoveredMonth8_12.Text = null;
			this.txtCoveredMonth8_12.Top = 7.03125F;
			this.txtCoveredMonth8_12.Width = 0.25F;
			// 
			// txtCoveredMonth9_12
			// 
			this.txtCoveredMonth9_12.Height = 0.1770833F;
			this.txtCoveredMonth9_12.Left = 8.545834F;
			this.txtCoveredMonth9_12.Name = "txtCoveredMonth9_12";
			this.txtCoveredMonth9_12.Text = null;
			this.txtCoveredMonth9_12.Top = 7.03125F;
			this.txtCoveredMonth9_12.Width = 0.25F;
			// 
			// txtCoveredMonth10_12
			// 
			this.txtCoveredMonth10_12.Height = 0.1770833F;
			this.txtCoveredMonth10_12.Left = 8.939584F;
			this.txtCoveredMonth10_12.Name = "txtCoveredMonth10_12";
			this.txtCoveredMonth10_12.Text = null;
			this.txtCoveredMonth10_12.Top = 7.03125F;
			this.txtCoveredMonth10_12.Width = 0.25F;
			// 
			// txtCoveredMonth11_12
			// 
			this.txtCoveredMonth11_12.Height = 0.1770833F;
			this.txtCoveredMonth11_12.Left = 9.333333F;
			this.txtCoveredMonth11_12.Name = "txtCoveredMonth11_12";
			this.txtCoveredMonth11_12.Text = null;
			this.txtCoveredMonth11_12.Top = 7.03125F;
			this.txtCoveredMonth11_12.Width = 0.25F;
			// 
			// txtCoveredMonth12_12
			// 
			this.txtCoveredMonth12_12.Height = 0.1770833F;
			this.txtCoveredMonth12_12.Left = 9.727083F;
			this.txtCoveredMonth12_12.Name = "txtCoveredMonth12_12";
			this.txtCoveredMonth12_12.Text = null;
			this.txtCoveredMonth12_12.Top = 7.03125F;
			this.txtCoveredMonth12_12.Width = 0.1979167F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.28125F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 4.697917F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-size: 6.5pt; text-align: center";
			this.Label176.Text = "(d) Covered all 12 months";
			this.Label176.Top = 0.9583333F;
			this.Label176.Width = 0.5833333F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0F;
			this.Line13.LineWeight = 2F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.6875F;
			this.Line13.Width = 10F;
			this.Line13.X1 = 0F;
			this.Line13.X2 = 10F;
			this.Line13.Y1 = 0.6875F;
			this.Line13.Y2 = 0.6875F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1770833F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "background-color: rgb(0,0,0); color: rgb(255,255,255); font-family: \'Arial\'; font" + "-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Label89.Text = "Part III";
			this.Label89.Top = 0.6875F;
			this.Label89.Width = 0.53125F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1770833F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 0.6354167F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-family: \'Arial\'; font-weight: bold";
			this.Label90.Text = "Covered Individuals -";
			this.Label90.Top = 0.6979167F;
			this.Label90.Width = 1.572917F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 1.397917F;
			this.Line15.Width = 10F;
			this.Line15.X1 = 0F;
			this.Line15.X2 = 10F;
			this.Line15.Y1 = 1.397917F;
			this.Line15.Y2 = 1.397917F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 1.895833F;
			this.Line16.Width = 10F;
			this.Line16.X1 = 0F;
			this.Line16.X2 = 10F;
			this.Line16.Y1 = 1.895833F;
			this.Line16.Y2 = 1.895833F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 2.39375F;
			this.Line17.Width = 10F;
			this.Line17.X1 = 0F;
			this.Line17.X2 = 10F;
			this.Line17.Y1 = 2.39375F;
			this.Line17.Y2 = 2.39375F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 0F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 2.891667F;
			this.Line18.Width = 10F;
			this.Line18.X1 = 0F;
			this.Line18.X2 = 10F;
			this.Line18.Y1 = 2.891667F;
			this.Line18.Y2 = 2.891667F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 3.389583F;
			this.Line19.Width = 10F;
			this.Line19.X1 = 0F;
			this.Line19.X2 = 10F;
			this.Line19.Y1 = 3.389583F;
			this.Line19.Y2 = 3.389583F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 0F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 3.8875F;
			this.Line20.Width = 10F;
			this.Line20.X1 = 0F;
			this.Line20.X2 = 10F;
			this.Line20.Y1 = 3.8875F;
			this.Line20.Y2 = 3.8875F;
			// 
			// Line21
			// 
			this.Line21.Height = 6.508333F;
			this.Line21.Left = 2.40625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 0.8645833F;
			this.Line21.Width = 0F;
			this.Line21.X1 = 2.40625F;
			this.Line21.X2 = 2.40625F;
			this.Line21.Y1 = 0.8645833F;
			this.Line21.Y2 = 7.372917F;
			// 
			// Line22
			// 
			this.Line22.Height = 6.508333F;
			this.Line22.Left = 3.583333F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 0.8645833F;
			this.Line22.Width = 0F;
			this.Line22.X1 = 3.583333F;
			this.Line22.X2 = 3.583333F;
			this.Line22.Y1 = 0.8645833F;
			this.Line22.Y2 = 7.372917F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1770833F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label130.Text = "29";
			this.Label130.Top = 1.739583F;
			this.Label130.Width = 0.28125F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1770833F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 0F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label131.Text = "30";
			this.Label131.Top = 2.239583F;
			this.Label131.Width = 0.28125F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1770833F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label132.Text = "31";
			this.Label132.Top = 2.739583F;
			this.Label132.Width = 0.28125F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1770833F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label133.Text = "32";
			this.Label133.Top = 3.239583F;
			this.Label133.Width = 0.28125F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1770833F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 0F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label134.Text = "33";
			this.Label134.Top = 3.739583F;
			this.Label134.Width = 0.28125F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1770833F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 0F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label135.Text = "34";
			this.Label135.Top = 4.229167F;
			this.Label135.Width = 0.28125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.3541667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 6.5pt; text-align: center";
			this.Label3.Text = "(a) Name of covered individual(s)";
			this.Label3.Top = 0.9583333F;
			this.Label3.Width = 1.708333F;
			// 
			// Label174
			// 
			this.Label174.Height = 0.19F;
			this.Label174.HyperLink = null;
			this.Label174.Left = 2.5F;
			this.Label174.Name = "Label174";
			this.Label174.Style = "font-size: 6.5pt; text-align: center";
			this.Label174.Text = "(b) SSN or other TIN";
			this.Label174.Top = 0.9583333F;
			this.Label174.Width = 1.020833F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.28125F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 3.604167F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-size: 7pt; text-align: center";
			this.Label175.Text = "(c) DOB (If SSN or other TIN is not available)";
			this.Label175.Top = 0.9583333F;
			this.Label175.Width = 1.083333F;
			// 
			// Label178
			// 
			this.Label178.Height = 0.19F;
			this.Label178.HyperLink = null;
			this.Label178.Left = 6.729167F;
			this.Label178.Name = "Label178";
			this.Label178.Style = "font-size: 6.5pt; text-align: center";
			this.Label178.Text = "(e) Months of Coverage";
			this.Label178.Top = 0.9583333F;
			this.Label178.Width = 1.708333F;
			// 
			// Label179
			// 
			this.Label179.Height = 0.1770833F;
			this.Label179.HyperLink = null;
			this.Label179.Left = 0F;
			this.Label179.Name = "Label179";
			this.Label179.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label179.Text = "35";
			this.Label179.Top = 4.729167F;
			this.Label179.Width = 0.28125F;
			// 
			// Label180
			// 
			this.Label180.Height = 0.1770833F;
			this.Label180.HyperLink = null;
			this.Label180.Left = 0F;
			this.Label180.Name = "Label180";
			this.Label180.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label180.Text = "36";
			this.Label180.Top = 5.229167F;
			this.Label180.Width = 0.28125F;
			// 
			// Label181
			// 
			this.Label181.Height = 0.1770833F;
			this.Label181.HyperLink = null;
			this.Label181.Left = 0F;
			this.Label181.Name = "Label181";
			this.Label181.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label181.Text = "37";
			this.Label181.Top = 5.729167F;
			this.Label181.Width = 0.28125F;
			// 
			// Label182
			// 
			this.Label182.Height = 0.1770833F;
			this.Label182.HyperLink = null;
			this.Label182.Left = 0F;
			this.Label182.Name = "Label182";
			this.Label182.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label182.Text = "38";
			this.Label182.Top = 6.229167F;
			this.Label182.Width = 0.28125F;
			// 
			// Label183
			// 
			this.Label183.Height = 0.1770833F;
			this.Label183.HyperLink = null;
			this.Label183.Left = 0F;
			this.Label183.Name = "Label183";
			this.Label183.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label183.Text = "39";
			this.Label183.Top = 6.71875F;
			this.Label183.Width = 0.28125F;
			// 
			// Label184
			// 
			this.Label184.Height = 0.1770833F;
			this.Label184.HyperLink = null;
			this.Label184.Left = 0F;
			this.Label184.Name = "Label184";
			this.Label184.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label184.Text = "40";
			this.Label184.Top = 7.21875F;
			this.Label184.Width = 0.28125F;
			// 
			// Line38
			// 
			this.Line38.Height = 0F;
			this.Line38.Left = 0.03125F;
			this.Line38.LineWeight = 2F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 7.375F;
			this.Line38.Width = 9.96875F;
			this.Line38.X1 = 0.03125F;
			this.Line38.X2 = 10F;
			this.Line38.Y1 = 7.375F;
			this.Line38.Y2 = 7.375F;
			// 
			// Label185
			// 
			this.Label185.Height = 0.1770833F;
			this.Label185.HyperLink = null;
			this.Label185.Left = 2.197917F;
			this.Label185.Name = "Label185";
			this.Label185.Style = "font-family: \'Arial\'";
			this.Label185.Text = "Continuation Sheet";
			this.Label185.Top = 0.6979167F;
			this.Label185.Width = 1.572917F;
			// 
			// Line39
			// 
			this.Line39.Height = 0F;
			this.Line39.Left = 0F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 4.385417F;
			this.Line39.Width = 10F;
			this.Line39.X1 = 0F;
			this.Line39.X2 = 10F;
			this.Line39.Y1 = 4.385417F;
			this.Line39.Y2 = 4.385417F;
			// 
			// Line40
			// 
			this.Line40.Height = 0F;
			this.Line40.Left = 0F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 4.883333F;
			this.Line40.Width = 10F;
			this.Line40.X1 = 0F;
			this.Line40.X2 = 10F;
			this.Line40.Y1 = 4.883333F;
			this.Line40.Y2 = 4.883333F;
			// 
			// Line41
			// 
			this.Line41.Height = 0F;
			this.Line41.Left = 0F;
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 5.38125F;
			this.Line41.Width = 10F;
			this.Line41.X1 = 0F;
			this.Line41.X2 = 10F;
			this.Line41.Y1 = 5.38125F;
			this.Line41.Y2 = 5.38125F;
			// 
			// Line42
			// 
			this.Line42.Height = 0F;
			this.Line42.Left = 0F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 5.879167F;
			this.Line42.Width = 10F;
			this.Line42.X1 = 0F;
			this.Line42.X2 = 10F;
			this.Line42.Y1 = 5.879167F;
			this.Line42.Y2 = 5.879167F;
			// 
			// Line43
			// 
			this.Line43.Height = 0F;
			this.Line43.Left = 0F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 6.377083F;
			this.Line43.Width = 10F;
			this.Line43.X1 = 0F;
			this.Line43.X2 = 10F;
			this.Line43.Y1 = 6.377083F;
			this.Line43.Y2 = 6.377083F;
			// 
			// Line44
			// 
			this.Line44.Height = 0F;
			this.Line44.Left = 0F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 6.875F;
			this.Line44.Width = 10F;
			this.Line44.X1 = 0F;
			this.Line44.X2 = 10F;
			this.Line44.Y1 = 6.875F;
			this.Line44.Y2 = 6.875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 9.4375F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 7pt; text-align: left";
			this.Label34.Text = "Page";
			this.Label34.Top = 0.25F;
			this.Label34.Width = 0.3333333F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 9.75F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 10pt; font-weight: bold; text-align: right; vertical-align: bottom";
			this.Label35.Text = "3";
			this.Label35.Top = 0.1736111F;
			this.Label35.Width = 0.2083333F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1979167F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 9.208333F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'OCR A Extended\'; font-size: 12pt; text-align: right";
			this.Label93.Text = "560317";
			this.Label93.Top = 0F;
			this.Label93.Width = 0.75F;
			// 
			// Label186
			// 
			this.Label186.Height = 0.19F;
			this.Label186.HyperLink = null;
			this.Label186.Left = 0F;
			this.Label186.Name = "Label186";
			this.Label186.Style = "font-size: 7pt; text-align: left";
			this.Label186.Text = "Name of responsible individual";
			this.Label186.Top = 0.375F;
			this.Label186.Width = 1.708333F;
			// 
			// Label187
			// 
			this.Label187.Height = 0.19F;
			this.Label187.HyperLink = null;
			this.Label187.Left = 7.75F;
			this.Label187.Name = "Label187";
			this.Label187.Style = "font-size: 7pt; text-align: left";
			this.Label187.Text = "Date of birth (If SSN or other TIN is not available)";
			this.Label187.Top = 0.375F;
			this.Label187.Width = 2.208333F;
			// 
			// Label188
			// 
			this.Label188.Height = 0.19F;
			this.Label188.HyperLink = null;
			this.Label188.Left = 0F;
			this.Label188.Name = "Label188";
			this.Label188.Style = "font-size: 7pt; text-align: left";
			this.Label188.Text = "Form 1095-B (2016)";
			this.Label188.Top = 0.25F;
			this.Label188.Width = 1.208333F;
			// 
			// Line46
			// 
			this.Line46.Height = 0.3125F;
			this.Line46.Left = 7.635417F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 0.375F;
			this.Line46.Width = 0F;
			this.Line46.X1 = 7.635417F;
			this.Line46.X2 = 7.635417F;
			this.Line46.Y1 = 0.375F;
			this.Line46.Y2 = 0.6875F;
			// 
			// Line23
			// 
			this.Line23.Height = 6.508333F;
			this.Line23.Left = 4.697917F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 0.8645833F;
			this.Line23.Width = 0F;
			this.Line23.X1 = 4.697917F;
			this.Line23.X2 = 4.697917F;
			this.Line23.Y1 = 0.8645833F;
			this.Line23.Y2 = 7.372917F;
			// 
			// Line36
			// 
			this.Line36.Height = 0F;
			this.Line36.Left = 0F;
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 0.8645833F;
			this.Line36.Width = 10F;
			this.Line36.X1 = 0F;
			this.Line36.X2 = 10F;
			this.Line36.Y1 = 0.8645833F;
			this.Line36.Y2 = 0.8645833F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1770833F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 5.270833F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-size: 8.5pt; text-align: center";
			this.Label118.Text = "Jan";
			this.Label118.Top = 1.25F;
			this.Label118.Width = 0.3958333F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1770833F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 5.666667F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-size: 8.5pt; text-align: center";
			this.Label136.Text = "Feb";
			this.Label136.Top = 1.25F;
			this.Label136.Width = 0.3958333F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1770833F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 6.0625F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-size: 8.5pt; text-align: center";
			this.Label137.Text = "Mar";
			this.Label137.Top = 1.25F;
			this.Label137.Width = 0.3958333F;
			// 
			// Label165
			// 
			this.Label165.Height = 0.1770833F;
			this.Label165.HyperLink = null;
			this.Label165.Left = 6.447917F;
			this.Label165.Name = "Label165";
			this.Label165.Style = "font-size: 8.5pt; text-align: center";
			this.Label165.Text = "Apr";
			this.Label165.Top = 1.25F;
			this.Label165.Width = 0.3958333F;
			// 
			// Label166
			// 
			this.Label166.Height = 0.1770833F;
			this.Label166.HyperLink = null;
			this.Label166.Left = 6.84375F;
			this.Label166.Name = "Label166";
			this.Label166.Style = "font-size: 8.5pt; text-align: center";
			this.Label166.Text = "May";
			this.Label166.Top = 1.25F;
			this.Label166.Width = 0.3958333F;
			// 
			// Label167
			// 
			this.Label167.Height = 0.1770833F;
			this.Label167.HyperLink = null;
			this.Label167.Left = 7.239583F;
			this.Label167.Name = "Label167";
			this.Label167.Style = "font-size: 8.5pt; text-align: center";
			this.Label167.Text = "June";
			this.Label167.Top = 1.25F;
			this.Label167.Width = 0.39375F;
			// 
			// Label168
			// 
			this.Label168.Height = 0.1770833F;
			this.Label168.HyperLink = null;
			this.Label168.Left = 7.635417F;
			this.Label168.Name = "Label168";
			this.Label168.Style = "font-size: 8.5pt; text-align: center";
			this.Label168.Text = "July";
			this.Label168.Top = 1.25F;
			this.Label168.Width = 0.3958333F;
			// 
			// Label169
			// 
			this.Label169.Height = 0.1770833F;
			this.Label169.HyperLink = null;
			this.Label169.Left = 8.03125F;
			this.Label169.Name = "Label169";
			this.Label169.Style = "font-size: 8.5pt; text-align: center";
			this.Label169.Text = "Aug";
			this.Label169.Top = 1.25F;
			this.Label169.Width = 0.3958333F;
			// 
			// Label170
			// 
			this.Label170.Height = 0.1770833F;
			this.Label170.HyperLink = null;
			this.Label170.Left = 8.416667F;
			this.Label170.Name = "Label170";
			this.Label170.Style = "font-size: 8.5pt; text-align: center";
			this.Label170.Text = "Sept";
			this.Label170.Top = 1.25F;
			this.Label170.Width = 0.3958333F;
			// 
			// Label171
			// 
			this.Label171.Height = 0.1770833F;
			this.Label171.HyperLink = null;
			this.Label171.Left = 8.8125F;
			this.Label171.Name = "Label171";
			this.Label171.Style = "font-size: 8.5pt; text-align: center";
			this.Label171.Text = "Oct";
			this.Label171.Top = 1.25F;
			this.Label171.Width = 0.3958333F;
			// 
			// Label172
			// 
			this.Label172.Height = 0.1770833F;
			this.Label172.HyperLink = null;
			this.Label172.Left = 9.208333F;
			this.Label172.Name = "Label172";
			this.Label172.Style = "font-size: 8.5pt; text-align: center";
			this.Label172.Text = "Nov";
			this.Label172.Top = 1.25F;
			this.Label172.Width = 0.3958333F;
			// 
			// Label173
			// 
			this.Label173.Height = 0.1770833F;
			this.Label173.HyperLink = null;
			this.Label173.Left = 9.604167F;
			this.Label173.Name = "Label173";
			this.Label173.Style = "font-size: 8.5pt; text-align: center";
			this.Label173.Text = "Dec";
			this.Label173.Top = 1.25F;
			this.Label173.Width = 0.3958333F;
			// 
			// Line37
			// 
			this.Line37.Height = 0F;
			this.Line37.Left = 5.270833F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 1.25F;
			this.Line37.Width = 4.729167F;
			this.Line37.X1 = 5.270833F;
			this.Line37.X2 = 10F;
			this.Line37.Y1 = 1.25F;
			this.Line37.Y2 = 1.25F;
			// 
			// Label189
			// 
			this.Label189.Height = 0.1770833F;
			this.Label189.HyperLink = null;
			this.Label189.Left = 8.666667F;
			this.Label189.Name = "Label189";
			this.Label189.Style = "font-size: 8.5pt";
			this.Label189.Text = "Form";
			this.Label189.Top = 7.427083F;
			this.Label189.Width = 0.40625F;
			// 
			// Label190
			// 
			this.Label190.Height = 0.1770833F;
			this.Label190.HyperLink = null;
			this.Label190.Left = 9.489583F;
			this.Label190.Name = "Label190";
			this.Label190.Style = "font-size: 8.5pt";
			this.Label190.Text = "(2016)";
			this.Label190.Top = 7.427083F;
			this.Label190.Width = 0.40625F;
			// 
			// Label191
			// 
			this.Label191.Height = 0.1770833F;
			this.Label191.HyperLink = null;
			this.Label191.Left = 8.989583F;
			this.Label191.Name = "Label191";
			this.Label191.Style = "font-size: 10pt; font-weight: bold";
			this.Label191.Text = "1095-B";
			this.Label191.Top = 7.40625F;
			this.Label191.Width = 0.53125F;
			// 
			// Line24
			// 
			this.Line24.Height = 6.520833F;
			this.Line24.Left = 5.270833F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 0.8645833F;
			this.Line24.Width = 0F;
			this.Line24.X1 = 5.270833F;
			this.Line24.X2 = 5.270833F;
			this.Line24.Y1 = 0.8645833F;
			this.Line24.Y2 = 7.385417F;
			// 
			// Line25
			// 
			this.Line25.Height = 6.135417F;
			this.Line25.Left = 5.664583F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 1.25F;
			this.Line25.Width = 0F;
			this.Line25.X1 = 5.664583F;
			this.Line25.X2 = 5.664583F;
			this.Line25.Y1 = 1.25F;
			this.Line25.Y2 = 7.385417F;
			// 
			// Line26
			// 
			this.Line26.Height = 6.135417F;
			this.Line26.Left = 6.058333F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 1.25F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 6.058333F;
			this.Line26.X2 = 6.058333F;
			this.Line26.Y1 = 1.25F;
			this.Line26.Y2 = 7.385417F;
			// 
			// Line27
			// 
			this.Line27.Height = 6.135417F;
			this.Line27.Left = 6.452083F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 1.25F;
			this.Line27.Width = 0F;
			this.Line27.X1 = 6.452083F;
			this.Line27.X2 = 6.452083F;
			this.Line27.Y1 = 1.25F;
			this.Line27.Y2 = 7.385417F;
			// 
			// Line28
			// 
			this.Line28.Height = 6.135417F;
			this.Line28.Left = 6.845833F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 1.25F;
			this.Line28.Width = 0F;
			this.Line28.X1 = 6.845833F;
			this.Line28.X2 = 6.845833F;
			this.Line28.Y1 = 1.25F;
			this.Line28.Y2 = 7.385417F;
			// 
			// Line29
			// 
			this.Line29.Height = 6.135417F;
			this.Line29.Left = 7.239583F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 1.25F;
			this.Line29.Width = 0F;
			this.Line29.X1 = 7.239583F;
			this.Line29.X2 = 7.239583F;
			this.Line29.Y1 = 1.25F;
			this.Line29.Y2 = 7.385417F;
			// 
			// Line30
			// 
			this.Line30.Height = 6.135417F;
			this.Line30.Left = 7.633333F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 1.25F;
			this.Line30.Width = 0F;
			this.Line30.X1 = 7.633333F;
			this.Line30.X2 = 7.633333F;
			this.Line30.Y1 = 1.25F;
			this.Line30.Y2 = 7.385417F;
			// 
			// Line31
			// 
			this.Line31.Height = 6.135417F;
			this.Line31.Left = 8.027083F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 1.25F;
			this.Line31.Width = 0F;
			this.Line31.X1 = 8.027083F;
			this.Line31.X2 = 8.027083F;
			this.Line31.Y1 = 1.25F;
			this.Line31.Y2 = 7.385417F;
			// 
			// Line32
			// 
			this.Line32.Height = 6.135417F;
			this.Line32.Left = 8.420834F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 1.25F;
			this.Line32.Width = 0F;
			this.Line32.X1 = 8.420834F;
			this.Line32.X2 = 8.420834F;
			this.Line32.Y1 = 1.25F;
			this.Line32.Y2 = 7.385417F;
			// 
			// Line33
			// 
			this.Line33.Height = 6.135417F;
			this.Line33.Left = 8.814584F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 1.25F;
			this.Line33.Width = 0F;
			this.Line33.X1 = 8.814584F;
			this.Line33.X2 = 8.814584F;
			this.Line33.Y1 = 1.25F;
			this.Line33.Y2 = 7.385417F;
			// 
			// Line34
			// 
			this.Line34.Height = 6.135417F;
			this.Line34.Left = 9.208333F;
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 1.25F;
			this.Line34.Width = 0F;
			this.Line34.X1 = 9.208333F;
			this.Line34.X2 = 9.208333F;
			this.Line34.Y1 = 1.25F;
			this.Line34.Y2 = 7.385417F;
			// 
			// Line35
			// 
			this.Line35.Height = 6.135417F;
			this.Line35.Left = 9.602083F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 1.25F;
			this.Line35.Width = 0F;
			this.Line35.X1 = 9.602083F;
			this.Line35.X2 = 9.602083F;
			this.Line35.Y1 = 1.25F;
			this.Line35.Y2 = 7.385417F;
			// 
			// Label192
			// 
			this.Label192.Height = 0.19F;
			this.Label192.HyperLink = null;
			this.Label192.Left = 5.583333F;
			this.Label192.Name = "Label192";
			this.Label192.Style = "font-size: 7pt; text-align: left";
			this.Label192.Text = "Social security number (SSN or other TIN)";
			this.Label192.Top = 0.375F;
			this.Label192.Width = 2.020833F;
			// 
			// Line47
			// 
			this.Line47.Height = 0.3125F;
			this.Line47.Left = 5.5F;
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 0.375F;
			this.Line47.Width = 0F;
			this.Line47.X1 = 5.5F;
			this.Line47.X2 = 5.5F;
			this.Line47.Y1 = 0.375F;
			this.Line47.Y2 = 0.6875F;
			// 
			// Line45
			// 
			this.Line45.Height = 0F;
			this.Line45.Left = 0F;
			this.Line45.LineWeight = 2F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 0.375F;
			this.Line45.Width = 10F;
			this.Line45.X1 = 0F;
			this.Line45.X2 = 10F;
			this.Line45.Y1 = 0.375F;
			this.Line45.Y2 = 0.375F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.1770833F;
			this.Shape4.Left = 4.864583F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 1.552083F;
			this.Shape4.Width = 0.1770833F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.1770833F;
			this.Shape5.Left = 5.375F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 1.552083F;
			this.Shape5.Width = 0.1770833F;
			// 
			// Shape6
			// 
			this.Shape6.Height = 0.1770833F;
			this.Shape6.Left = 4.864583F;
			this.Shape6.Name = "Shape6";
			this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape6.Top = 2.052083F;
			this.Shape6.Width = 0.1770833F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.1770833F;
			this.Shape7.Left = 4.864583F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 2.552083F;
			this.Shape7.Width = 0.1770833F;
			// 
			// Shape8
			// 
			this.Shape8.Height = 0.1770833F;
			this.Shape8.Left = 4.864583F;
			this.Shape8.Name = "Shape8";
			this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape8.Top = 3.052083F;
			this.Shape8.Width = 0.1770833F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1770833F;
			this.Shape9.Left = 4.864583F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 3.552083F;
			this.Shape9.Width = 0.1770833F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1770833F;
			this.Shape10.Left = 4.864583F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 4.041667F;
			this.Shape10.Width = 0.1770833F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.1770833F;
			this.Shape11.Left = 5.375F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 2.052083F;
			this.Shape11.Width = 0.1770833F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.1770833F;
			this.Shape12.Left = 5.375F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 2.552083F;
			this.Shape12.Width = 0.1770833F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 0.1770833F;
			this.Shape13.Left = 5.375F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 3.052083F;
			this.Shape13.Width = 0.1770833F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.1770833F;
			this.Shape14.Left = 5.375F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 3.552083F;
			this.Shape14.Width = 0.1770833F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1770833F;
			this.Shape15.Left = 5.375F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 4.041667F;
			this.Shape15.Width = 0.1770833F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1770833F;
			this.Shape16.Left = 5.770833F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 1.552083F;
			this.Shape16.Width = 0.1770833F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.1770833F;
			this.Shape17.Left = 5.770833F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 2.052083F;
			this.Shape17.Width = 0.1770833F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.1770833F;
			this.Shape18.Left = 5.770833F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 2.552083F;
			this.Shape18.Width = 0.1770833F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.1770833F;
			this.Shape19.Left = 5.770833F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 3.052083F;
			this.Shape19.Width = 0.1770833F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.1770833F;
			this.Shape20.Left = 5.770833F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 3.552083F;
			this.Shape20.Width = 0.1770833F;
			// 
			// Shape21
			// 
			this.Shape21.Height = 0.1770833F;
			this.Shape21.Left = 5.770833F;
			this.Shape21.Name = "Shape21";
			this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape21.Top = 4.041667F;
			this.Shape21.Width = 0.1770833F;
			// 
			// Shape22
			// 
			this.Shape22.Height = 0.1770833F;
			this.Shape22.Left = 6.166667F;
			this.Shape22.Name = "Shape22";
			this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape22.Top = 1.552083F;
			this.Shape22.Width = 0.1770833F;
			// 
			// Shape23
			// 
			this.Shape23.Height = 0.1770833F;
			this.Shape23.Left = 6.166667F;
			this.Shape23.Name = "Shape23";
			this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape23.Top = 2.052083F;
			this.Shape23.Width = 0.1770833F;
			// 
			// Shape24
			// 
			this.Shape24.Height = 0.1770833F;
			this.Shape24.Left = 6.166667F;
			this.Shape24.Name = "Shape24";
			this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape24.Top = 2.552083F;
			this.Shape24.Width = 0.1770833F;
			// 
			// Shape25
			// 
			this.Shape25.Height = 0.1770833F;
			this.Shape25.Left = 6.166667F;
			this.Shape25.Name = "Shape25";
			this.Shape25.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape25.Top = 3.052083F;
			this.Shape25.Width = 0.1770833F;
			// 
			// Shape26
			// 
			this.Shape26.Height = 0.1770833F;
			this.Shape26.Left = 6.166667F;
			this.Shape26.Name = "Shape26";
			this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape26.Top = 3.552083F;
			this.Shape26.Width = 0.1770833F;
			// 
			// Shape27
			// 
			this.Shape27.Height = 0.1770833F;
			this.Shape27.Left = 6.166667F;
			this.Shape27.Name = "Shape27";
			this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape27.Top = 4.041667F;
			this.Shape27.Width = 0.1770833F;
			// 
			// Shape82
			// 
			this.Shape82.Height = 0.1770833F;
			this.Shape82.Left = 4.864583F;
			this.Shape82.Name = "Shape82";
			this.Shape82.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape82.Top = 4.541667F;
			this.Shape82.Width = 0.1770833F;
			// 
			// Shape83
			// 
			this.Shape83.Height = 0.1770833F;
			this.Shape83.Left = 5.375F;
			this.Shape83.Name = "Shape83";
			this.Shape83.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape83.Top = 4.541667F;
			this.Shape83.Width = 0.1770833F;
			// 
			// Shape84
			// 
			this.Shape84.Height = 0.1770833F;
			this.Shape84.Left = 4.864583F;
			this.Shape84.Name = "Shape84";
			this.Shape84.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape84.Top = 5.041667F;
			this.Shape84.Width = 0.1770833F;
			// 
			// Shape85
			// 
			this.Shape85.Height = 0.1770833F;
			this.Shape85.Left = 4.864583F;
			this.Shape85.Name = "Shape85";
			this.Shape85.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape85.Top = 5.541667F;
			this.Shape85.Width = 0.1770833F;
			// 
			// Shape86
			// 
			this.Shape86.Height = 0.1770833F;
			this.Shape86.Left = 4.864583F;
			this.Shape86.Name = "Shape86";
			this.Shape86.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape86.Top = 6.041667F;
			this.Shape86.Width = 0.1770833F;
			// 
			// Shape87
			// 
			this.Shape87.Height = 0.1770833F;
			this.Shape87.Left = 4.864583F;
			this.Shape87.Name = "Shape87";
			this.Shape87.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape87.Top = 6.527083F;
			this.Shape87.Width = 0.1770833F;
			// 
			// Shape88
			// 
			this.Shape88.Height = 0.1770833F;
			this.Shape88.Left = 4.864583F;
			this.Shape88.Name = "Shape88";
			this.Shape88.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape88.Top = 7.03125F;
			this.Shape88.Width = 0.1770833F;
			// 
			// Shape89
			// 
			this.Shape89.Height = 0.1770833F;
			this.Shape89.Left = 5.375F;
			this.Shape89.Name = "Shape89";
			this.Shape89.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape89.Top = 5.041667F;
			this.Shape89.Width = 0.1770833F;
			// 
			// Shape90
			// 
			this.Shape90.Height = 0.1770833F;
			this.Shape90.Left = 5.375F;
			this.Shape90.Name = "Shape90";
			this.Shape90.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape90.Top = 5.541667F;
			this.Shape90.Width = 0.1770833F;
			// 
			// Shape91
			// 
			this.Shape91.Height = 0.1770833F;
			this.Shape91.Left = 5.375F;
			this.Shape91.Name = "Shape91";
			this.Shape91.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape91.Top = 6.041667F;
			this.Shape91.Width = 0.1770833F;
			// 
			// Shape92
			// 
			this.Shape92.Height = 0.1770833F;
			this.Shape92.Left = 5.375F;
			this.Shape92.Name = "Shape92";
			this.Shape92.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape92.Top = 6.53125F;
			this.Shape92.Width = 0.1770833F;
			// 
			// Shape93
			// 
			this.Shape93.Height = 0.1770833F;
			this.Shape93.Left = 5.375F;
			this.Shape93.Name = "Shape93";
			this.Shape93.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape93.Top = 7.03125F;
			this.Shape93.Width = 0.1770833F;
			// 
			// Shape94
			// 
			this.Shape94.Height = 0.1770833F;
			this.Shape94.Left = 5.770833F;
			this.Shape94.Name = "Shape94";
			this.Shape94.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape94.Top = 4.541667F;
			this.Shape94.Width = 0.1770833F;
			// 
			// Shape95
			// 
			this.Shape95.Height = 0.1770833F;
			this.Shape95.Left = 5.770833F;
			this.Shape95.Name = "Shape95";
			this.Shape95.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape95.Top = 5.041667F;
			this.Shape95.Width = 0.1770833F;
			// 
			// Shape96
			// 
			this.Shape96.Height = 0.1770833F;
			this.Shape96.Left = 5.770833F;
			this.Shape96.Name = "Shape96";
			this.Shape96.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape96.Top = 5.541667F;
			this.Shape96.Width = 0.1770833F;
			// 
			// Shape97
			// 
			this.Shape97.Height = 0.1770833F;
			this.Shape97.Left = 5.770833F;
			this.Shape97.Name = "Shape97";
			this.Shape97.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape97.Top = 6.041667F;
			this.Shape97.Width = 0.1770833F;
			// 
			// Shape98
			// 
			this.Shape98.Height = 0.1770833F;
			this.Shape98.Left = 5.770833F;
			this.Shape98.Name = "Shape98";
			this.Shape98.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape98.Top = 6.53125F;
			this.Shape98.Width = 0.1770833F;
			// 
			// Shape99
			// 
			this.Shape99.Height = 0.1770833F;
			this.Shape99.Left = 5.770833F;
			this.Shape99.Name = "Shape99";
			this.Shape99.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape99.Top = 7.03125F;
			this.Shape99.Width = 0.1770833F;
			// 
			// Shape100
			// 
			this.Shape100.Height = 0.1770833F;
			this.Shape100.Left = 6.166667F;
			this.Shape100.Name = "Shape100";
			this.Shape100.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape100.Top = 4.541667F;
			this.Shape100.Width = 0.1770833F;
			// 
			// Shape101
			// 
			this.Shape101.Height = 0.1770833F;
			this.Shape101.Left = 6.166667F;
			this.Shape101.Name = "Shape101";
			this.Shape101.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape101.Top = 5.041667F;
			this.Shape101.Width = 0.1770833F;
			// 
			// Shape102
			// 
			this.Shape102.Height = 0.1770833F;
			this.Shape102.Left = 6.166667F;
			this.Shape102.Name = "Shape102";
			this.Shape102.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape102.Top = 5.541667F;
			this.Shape102.Width = 0.1770833F;
			// 
			// Shape103
			// 
			this.Shape103.Height = 0.1770833F;
			this.Shape103.Left = 6.166667F;
			this.Shape103.Name = "Shape103";
			this.Shape103.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape103.Top = 6.041667F;
			this.Shape103.Width = 0.1770833F;
			// 
			// Shape104
			// 
			this.Shape104.Height = 0.1770833F;
			this.Shape104.Left = 6.166667F;
			this.Shape104.Name = "Shape104";
			this.Shape104.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape104.Top = 6.53125F;
			this.Shape104.Width = 0.1770833F;
			// 
			// Shape105
			// 
			this.Shape105.Height = 0.1770833F;
			this.Shape105.Left = 6.166667F;
			this.Shape105.Name = "Shape105";
			this.Shape105.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape105.Top = 7.03125F;
			this.Shape105.Width = 0.1770833F;
			// 
			// Shape28
			// 
			this.Shape28.Height = 0.1770833F;
			this.Shape28.Left = 6.552083F;
			this.Shape28.Name = "Shape28";
			this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape28.Top = 1.552083F;
			this.Shape28.Width = 0.1770833F;
			// 
			// Shape29
			// 
			this.Shape29.Height = 0.1770833F;
			this.Shape29.Left = 6.552083F;
			this.Shape29.Name = "Shape29";
			this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape29.Top = 2.052083F;
			this.Shape29.Width = 0.1770833F;
			// 
			// Shape30
			// 
			this.Shape30.Height = 0.1770833F;
			this.Shape30.Left = 6.552083F;
			this.Shape30.Name = "Shape30";
			this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape30.Top = 2.552083F;
			this.Shape30.Width = 0.1770833F;
			// 
			// Shape32
			// 
			this.Shape32.Height = 0.1770833F;
			this.Shape32.Left = 6.552083F;
			this.Shape32.Name = "Shape32";
			this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape32.Top = 3.552083F;
			this.Shape32.Width = 0.1770833F;
			// 
			// Shape31
			// 
			this.Shape31.Height = 0.1770833F;
			this.Shape31.Left = 6.552083F;
			this.Shape31.Name = "Shape31";
			this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape31.Top = 3.052083F;
			this.Shape31.Width = 0.1770833F;
			// 
			// Shape33
			// 
			this.Shape33.Height = 0.1770833F;
			this.Shape33.Left = 6.552083F;
			this.Shape33.Name = "Shape33";
			this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape33.Top = 4.041667F;
			this.Shape33.Width = 0.1770833F;
			// 
			// Shape34
			// 
			this.Shape34.Height = 0.1770833F;
			this.Shape34.Left = 6.947917F;
			this.Shape34.Name = "Shape34";
			this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape34.Top = 1.552083F;
			this.Shape34.Width = 0.1770833F;
			// 
			// Shape35
			// 
			this.Shape35.Height = 0.1770833F;
			this.Shape35.Left = 6.947917F;
			this.Shape35.Name = "Shape35";
			this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape35.Top = 2.052083F;
			this.Shape35.Width = 0.1770833F;
			// 
			// Shape36
			// 
			this.Shape36.Height = 0.1770833F;
			this.Shape36.Left = 6.947917F;
			this.Shape36.Name = "Shape36";
			this.Shape36.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape36.Top = 2.552083F;
			this.Shape36.Width = 0.1770833F;
			// 
			// Shape37
			// 
			this.Shape37.Height = 0.1770833F;
			this.Shape37.Left = 6.947917F;
			this.Shape37.Name = "Shape37";
			this.Shape37.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape37.Top = 3.052083F;
			this.Shape37.Width = 0.1770833F;
			// 
			// Shape38
			// 
			this.Shape38.Height = 0.1770833F;
			this.Shape38.Left = 6.947917F;
			this.Shape38.Name = "Shape38";
			this.Shape38.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape38.Top = 3.552083F;
			this.Shape38.Width = 0.1770833F;
			// 
			// Shape39
			// 
			this.Shape39.Height = 0.1770833F;
			this.Shape39.Left = 6.947917F;
			this.Shape39.Name = "Shape39";
			this.Shape39.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape39.Top = 4.041667F;
			this.Shape39.Width = 0.1770833F;
			// 
			// Shape40
			// 
			this.Shape40.Height = 0.1770833F;
			this.Shape40.Left = 7.34375F;
			this.Shape40.Name = "Shape40";
			this.Shape40.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape40.Top = 1.552083F;
			this.Shape40.Width = 0.1770833F;
			// 
			// Shape41
			// 
			this.Shape41.Height = 0.1770833F;
			this.Shape41.Left = 7.34375F;
			this.Shape41.Name = "Shape41";
			this.Shape41.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape41.Top = 2.052083F;
			this.Shape41.Width = 0.1770833F;
			// 
			// Shape42
			// 
			this.Shape42.Height = 0.1770833F;
			this.Shape42.Left = 7.34375F;
			this.Shape42.Name = "Shape42";
			this.Shape42.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape42.Top = 2.552083F;
			this.Shape42.Width = 0.1770833F;
			// 
			// Shape43
			// 
			this.Shape43.Height = 0.1770833F;
			this.Shape43.Left = 7.34375F;
			this.Shape43.Name = "Shape43";
			this.Shape43.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape43.Top = 3.052083F;
			this.Shape43.Width = 0.1770833F;
			// 
			// Shape44
			// 
			this.Shape44.Height = 0.1770833F;
			this.Shape44.Left = 7.34375F;
			this.Shape44.Name = "Shape44";
			this.Shape44.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape44.Top = 3.552083F;
			this.Shape44.Width = 0.1770833F;
			// 
			// Shape45
			// 
			this.Shape45.Height = 0.1770833F;
			this.Shape45.Left = 7.34375F;
			this.Shape45.Name = "Shape45";
			this.Shape45.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape45.Top = 4.041667F;
			this.Shape45.Width = 0.1770833F;
			// 
			// Shape46
			// 
			this.Shape46.Height = 0.1770833F;
			this.Shape46.Left = 7.739583F;
			this.Shape46.Name = "Shape46";
			this.Shape46.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape46.Top = 1.552083F;
			this.Shape46.Width = 0.1770833F;
			// 
			// Shape47
			// 
			this.Shape47.Height = 0.1770833F;
			this.Shape47.Left = 7.739583F;
			this.Shape47.Name = "Shape47";
			this.Shape47.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape47.Top = 2.052083F;
			this.Shape47.Width = 0.1770833F;
			// 
			// Shape48
			// 
			this.Shape48.Height = 0.1770833F;
			this.Shape48.Left = 7.739583F;
			this.Shape48.Name = "Shape48";
			this.Shape48.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape48.Top = 2.552083F;
			this.Shape48.Width = 0.1770833F;
			// 
			// Shape49
			// 
			this.Shape49.Height = 0.1770833F;
			this.Shape49.Left = 7.739583F;
			this.Shape49.Name = "Shape49";
			this.Shape49.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape49.Top = 3.052083F;
			this.Shape49.Width = 0.1770833F;
			// 
			// Shape50
			// 
			this.Shape50.Height = 0.1770833F;
			this.Shape50.Left = 7.739583F;
			this.Shape50.Name = "Shape50";
			this.Shape50.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape50.Top = 3.552083F;
			this.Shape50.Width = 0.1770833F;
			// 
			// Shape51
			// 
			this.Shape51.Height = 0.1770833F;
			this.Shape51.Left = 7.739583F;
			this.Shape51.Name = "Shape51";
			this.Shape51.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape51.Top = 4.041667F;
			this.Shape51.Width = 0.1770833F;
			// 
			// Shape106
			// 
			this.Shape106.Height = 0.1770833F;
			this.Shape106.Left = 6.552083F;
			this.Shape106.Name = "Shape106";
			this.Shape106.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape106.Top = 4.541667F;
			this.Shape106.Width = 0.1770833F;
			// 
			// Shape107
			// 
			this.Shape107.Height = 0.1770833F;
			this.Shape107.Left = 6.552083F;
			this.Shape107.Name = "Shape107";
			this.Shape107.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape107.Top = 5.041667F;
			this.Shape107.Width = 0.1770833F;
			// 
			// Shape108
			// 
			this.Shape108.Height = 0.1770833F;
			this.Shape108.Left = 6.552083F;
			this.Shape108.Name = "Shape108";
			this.Shape108.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape108.Top = 5.541667F;
			this.Shape108.Width = 0.1770833F;
			// 
			// Shape109
			// 
			this.Shape109.Height = 0.1770833F;
			this.Shape109.Left = 6.552083F;
			this.Shape109.Name = "Shape109";
			this.Shape109.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape109.Top = 6.53125F;
			this.Shape109.Width = 0.1770833F;
			// 
			// Shape110
			// 
			this.Shape110.Height = 0.1770833F;
			this.Shape110.Left = 6.552083F;
			this.Shape110.Name = "Shape110";
			this.Shape110.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape110.Top = 6.041667F;
			this.Shape110.Width = 0.1770833F;
			// 
			// Shape111
			// 
			this.Shape111.Height = 0.1770833F;
			this.Shape111.Left = 6.552083F;
			this.Shape111.Name = "Shape111";
			this.Shape111.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape111.Top = 7.03125F;
			this.Shape111.Width = 0.1770833F;
			// 
			// Shape112
			// 
			this.Shape112.Height = 0.1770833F;
			this.Shape112.Left = 6.947917F;
			this.Shape112.Name = "Shape112";
			this.Shape112.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape112.Top = 4.541667F;
			this.Shape112.Width = 0.1770833F;
			// 
			// Shape113
			// 
			this.Shape113.Height = 0.1770833F;
			this.Shape113.Left = 6.947917F;
			this.Shape113.Name = "Shape113";
			this.Shape113.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape113.Top = 5.041667F;
			this.Shape113.Width = 0.1770833F;
			// 
			// Shape114
			// 
			this.Shape114.Height = 0.1770833F;
			this.Shape114.Left = 6.947917F;
			this.Shape114.Name = "Shape114";
			this.Shape114.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape114.Top = 5.541667F;
			this.Shape114.Width = 0.1770833F;
			// 
			// Shape115
			// 
			this.Shape115.Height = 0.1770833F;
			this.Shape115.Left = 6.947917F;
			this.Shape115.Name = "Shape115";
			this.Shape115.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape115.Top = 6.041667F;
			this.Shape115.Width = 0.1770833F;
			// 
			// Shape116
			// 
			this.Shape116.Height = 0.1770833F;
			this.Shape116.Left = 6.947917F;
			this.Shape116.Name = "Shape116";
			this.Shape116.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape116.Top = 6.53125F;
			this.Shape116.Width = 0.1770833F;
			// 
			// Shape117
			// 
			this.Shape117.Height = 0.1770833F;
			this.Shape117.Left = 6.947917F;
			this.Shape117.Name = "Shape117";
			this.Shape117.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape117.Top = 7.03125F;
			this.Shape117.Width = 0.1770833F;
			// 
			// Shape118
			// 
			this.Shape118.Height = 0.1770833F;
			this.Shape118.Left = 7.34375F;
			this.Shape118.Name = "Shape118";
			this.Shape118.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape118.Top = 4.541667F;
			this.Shape118.Width = 0.1770833F;
			// 
			// Shape119
			// 
			this.Shape119.Height = 0.1770833F;
			this.Shape119.Left = 7.34375F;
			this.Shape119.Name = "Shape119";
			this.Shape119.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape119.Top = 5.041667F;
			this.Shape119.Width = 0.1770833F;
			// 
			// Shape120
			// 
			this.Shape120.Height = 0.1770833F;
			this.Shape120.Left = 7.34375F;
			this.Shape120.Name = "Shape120";
			this.Shape120.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape120.Top = 5.541667F;
			this.Shape120.Width = 0.1770833F;
			// 
			// Shape121
			// 
			this.Shape121.Height = 0.1770833F;
			this.Shape121.Left = 7.34375F;
			this.Shape121.Name = "Shape121";
			this.Shape121.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape121.Top = 6.041667F;
			this.Shape121.Width = 0.1770833F;
			// 
			// Shape122
			// 
			this.Shape122.Height = 0.1770833F;
			this.Shape122.Left = 7.34375F;
			this.Shape122.Name = "Shape122";
			this.Shape122.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape122.Top = 6.53125F;
			this.Shape122.Width = 0.1770833F;
			// 
			// Shape123
			// 
			this.Shape123.Height = 0.1770833F;
			this.Shape123.Left = 7.34375F;
			this.Shape123.Name = "Shape123";
			this.Shape123.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape123.Top = 7.03125F;
			this.Shape123.Width = 0.1770833F;
			// 
			// Shape124
			// 
			this.Shape124.Height = 0.1770833F;
			this.Shape124.Left = 7.739583F;
			this.Shape124.Name = "Shape124";
			this.Shape124.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape124.Top = 4.541667F;
			this.Shape124.Width = 0.1770833F;
			// 
			// Shape125
			// 
			this.Shape125.Height = 0.1770833F;
			this.Shape125.Left = 7.739583F;
			this.Shape125.Name = "Shape125";
			this.Shape125.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape125.Top = 5.041667F;
			this.Shape125.Width = 0.1770833F;
			// 
			// Shape126
			// 
			this.Shape126.Height = 0.1770833F;
			this.Shape126.Left = 7.739583F;
			this.Shape126.Name = "Shape126";
			this.Shape126.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape126.Top = 5.541667F;
			this.Shape126.Width = 0.1770833F;
			// 
			// Shape127
			// 
			this.Shape127.Height = 0.1770833F;
			this.Shape127.Left = 7.739583F;
			this.Shape127.Name = "Shape127";
			this.Shape127.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape127.Top = 6.041667F;
			this.Shape127.Width = 0.1770833F;
			// 
			// Shape128
			// 
			this.Shape128.Height = 0.1770833F;
			this.Shape128.Left = 7.739583F;
			this.Shape128.Name = "Shape128";
			this.Shape128.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape128.Top = 6.53125F;
			this.Shape128.Width = 0.1770833F;
			// 
			// Shape129
			// 
			this.Shape129.Height = 0.1770833F;
			this.Shape129.Left = 7.739583F;
			this.Shape129.Name = "Shape129";
			this.Shape129.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape129.Top = 7.03125F;
			this.Shape129.Width = 0.1770833F;
			// 
			// Shape52
			// 
			this.Shape52.Height = 0.1770833F;
			this.Shape52.Left = 8.135417F;
			this.Shape52.Name = "Shape52";
			this.Shape52.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape52.Top = 1.552083F;
			this.Shape52.Width = 0.1770833F;
			// 
			// Shape53
			// 
			this.Shape53.Height = 0.1770833F;
			this.Shape53.Left = 8.135417F;
			this.Shape53.Name = "Shape53";
			this.Shape53.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape53.Top = 2.052083F;
			this.Shape53.Width = 0.1770833F;
			// 
			// Shape54
			// 
			this.Shape54.Height = 0.1770833F;
			this.Shape54.Left = 8.135417F;
			this.Shape54.Name = "Shape54";
			this.Shape54.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape54.Top = 2.552083F;
			this.Shape54.Width = 0.1770833F;
			// 
			// Shape55
			// 
			this.Shape55.Height = 0.1770833F;
			this.Shape55.Left = 8.135417F;
			this.Shape55.Name = "Shape55";
			this.Shape55.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape55.Top = 3.052083F;
			this.Shape55.Width = 0.1770833F;
			// 
			// Shape56
			// 
			this.Shape56.Height = 0.1770833F;
			this.Shape56.Left = 8.135417F;
			this.Shape56.Name = "Shape56";
			this.Shape56.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape56.Top = 3.552083F;
			this.Shape56.Width = 0.1770833F;
			// 
			// Shape57
			// 
			this.Shape57.Height = 0.1770833F;
			this.Shape57.Left = 8.135417F;
			this.Shape57.Name = "Shape57";
			this.Shape57.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape57.Top = 4.041667F;
			this.Shape57.Width = 0.1770833F;
			// 
			// Shape58
			// 
			this.Shape58.Height = 0.1770833F;
			this.Shape58.Left = 8.520833F;
			this.Shape58.Name = "Shape58";
			this.Shape58.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape58.Top = 1.552083F;
			this.Shape58.Width = 0.1770833F;
			// 
			// Shape59
			// 
			this.Shape59.Height = 0.1770833F;
			this.Shape59.Left = 8.520833F;
			this.Shape59.Name = "Shape59";
			this.Shape59.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape59.Top = 2.052083F;
			this.Shape59.Width = 0.1770833F;
			// 
			// Shape60
			// 
			this.Shape60.Height = 0.1770833F;
			this.Shape60.Left = 8.520833F;
			this.Shape60.Name = "Shape60";
			this.Shape60.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape60.Top = 2.552083F;
			this.Shape60.Width = 0.1770833F;
			// 
			// Shape61
			// 
			this.Shape61.Height = 0.1770833F;
			this.Shape61.Left = 8.520833F;
			this.Shape61.Name = "Shape61";
			this.Shape61.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape61.Top = 3.052083F;
			this.Shape61.Width = 0.1770833F;
			// 
			// Shape62
			// 
			this.Shape62.Height = 0.1770833F;
			this.Shape62.Left = 8.520833F;
			this.Shape62.Name = "Shape62";
			this.Shape62.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape62.Top = 3.552083F;
			this.Shape62.Width = 0.1770833F;
			// 
			// Shape63
			// 
			this.Shape63.Height = 0.1770833F;
			this.Shape63.Left = 8.520833F;
			this.Shape63.Name = "Shape63";
			this.Shape63.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape63.Top = 4.041667F;
			this.Shape63.Width = 0.1770833F;
			// 
			// Shape64
			// 
			this.Shape64.Height = 0.1770833F;
			this.Shape64.Left = 8.916667F;
			this.Shape64.Name = "Shape64";
			this.Shape64.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape64.Top = 1.552083F;
			this.Shape64.Width = 0.1770833F;
			// 
			// Shape65
			// 
			this.Shape65.Height = 0.1770833F;
			this.Shape65.Left = 8.916667F;
			this.Shape65.Name = "Shape65";
			this.Shape65.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape65.Top = 2.052083F;
			this.Shape65.Width = 0.1770833F;
			// 
			// Shape66
			// 
			this.Shape66.Height = 0.1770833F;
			this.Shape66.Left = 8.916667F;
			this.Shape66.Name = "Shape66";
			this.Shape66.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape66.Top = 2.552083F;
			this.Shape66.Width = 0.1770833F;
			// 
			// Shape67
			// 
			this.Shape67.Height = 0.1770833F;
			this.Shape67.Left = 8.916667F;
			this.Shape67.Name = "Shape67";
			this.Shape67.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape67.Top = 3.052083F;
			this.Shape67.Width = 0.1770833F;
			// 
			// Shape68
			// 
			this.Shape68.Height = 0.1770833F;
			this.Shape68.Left = 8.916667F;
			this.Shape68.Name = "Shape68";
			this.Shape68.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape68.Top = 3.552083F;
			this.Shape68.Width = 0.1770833F;
			// 
			// Shape69
			// 
			this.Shape69.Height = 0.1770833F;
			this.Shape69.Left = 8.916667F;
			this.Shape69.Name = "Shape69";
			this.Shape69.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape69.Top = 4.041667F;
			this.Shape69.Width = 0.1770833F;
			// 
			// Shape70
			// 
			this.Shape70.Height = 0.1770833F;
			this.Shape70.Left = 9.3125F;
			this.Shape70.Name = "Shape70";
			this.Shape70.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape70.Top = 1.552083F;
			this.Shape70.Width = 0.1770833F;
			// 
			// Shape71
			// 
			this.Shape71.Height = 0.1770833F;
			this.Shape71.Left = 9.3125F;
			this.Shape71.Name = "Shape71";
			this.Shape71.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape71.Top = 2.052083F;
			this.Shape71.Width = 0.1770833F;
			// 
			// Shape72
			// 
			this.Shape72.Height = 0.1770833F;
			this.Shape72.Left = 9.3125F;
			this.Shape72.Name = "Shape72";
			this.Shape72.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape72.Top = 2.552083F;
			this.Shape72.Width = 0.1770833F;
			// 
			// Shape73
			// 
			this.Shape73.Height = 0.1770833F;
			this.Shape73.Left = 9.3125F;
			this.Shape73.Name = "Shape73";
			this.Shape73.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape73.Top = 3.052083F;
			this.Shape73.Width = 0.1770833F;
			// 
			// Shape74
			// 
			this.Shape74.Height = 0.1770833F;
			this.Shape74.Left = 9.3125F;
			this.Shape74.Name = "Shape74";
			this.Shape74.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape74.Top = 3.552083F;
			this.Shape74.Width = 0.1770833F;
			// 
			// Shape75
			// 
			this.Shape75.Height = 0.1770833F;
			this.Shape75.Left = 9.3125F;
			this.Shape75.Name = "Shape75";
			this.Shape75.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape75.Top = 4.041667F;
			this.Shape75.Width = 0.1770833F;
			// 
			// Shape76
			// 
			this.Shape76.Height = 0.1770833F;
			this.Shape76.Left = 9.708333F;
			this.Shape76.Name = "Shape76";
			this.Shape76.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape76.Top = 1.552083F;
			this.Shape76.Width = 0.1770833F;
			// 
			// Shape77
			// 
			this.Shape77.Height = 0.1770833F;
			this.Shape77.Left = 9.708333F;
			this.Shape77.Name = "Shape77";
			this.Shape77.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape77.Top = 2.052083F;
			this.Shape77.Width = 0.1770833F;
			// 
			// Shape78
			// 
			this.Shape78.Height = 0.1770833F;
			this.Shape78.Left = 9.708333F;
			this.Shape78.Name = "Shape78";
			this.Shape78.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape78.Top = 2.552083F;
			this.Shape78.Width = 0.1770833F;
			// 
			// Shape79
			// 
			this.Shape79.Height = 0.1770833F;
			this.Shape79.Left = 9.708333F;
			this.Shape79.Name = "Shape79";
			this.Shape79.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape79.Top = 3.052083F;
			this.Shape79.Width = 0.1770833F;
			// 
			// Shape80
			// 
			this.Shape80.Height = 0.1770833F;
			this.Shape80.Left = 9.708333F;
			this.Shape80.Name = "Shape80";
			this.Shape80.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape80.Top = 3.552083F;
			this.Shape80.Width = 0.1770833F;
			// 
			// Shape81
			// 
			this.Shape81.Height = 0.1770833F;
			this.Shape81.Left = 9.708333F;
			this.Shape81.Name = "Shape81";
			this.Shape81.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape81.Top = 4.041667F;
			this.Shape81.Width = 0.1770833F;
			// 
			// Shape130
			// 
			this.Shape130.Height = 0.1770833F;
			this.Shape130.Left = 8.135417F;
			this.Shape130.Name = "Shape130";
			this.Shape130.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape130.Top = 4.541667F;
			this.Shape130.Width = 0.1770833F;
			// 
			// Shape131
			// 
			this.Shape131.Height = 0.1770833F;
			this.Shape131.Left = 8.135417F;
			this.Shape131.Name = "Shape131";
			this.Shape131.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape131.Top = 5.041667F;
			this.Shape131.Width = 0.1770833F;
			// 
			// Shape132
			// 
			this.Shape132.Height = 0.1770833F;
			this.Shape132.Left = 8.135417F;
			this.Shape132.Name = "Shape132";
			this.Shape132.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape132.Top = 5.541667F;
			this.Shape132.Width = 0.1770833F;
			// 
			// Shape133
			// 
			this.Shape133.Height = 0.1770833F;
			this.Shape133.Left = 8.135417F;
			this.Shape133.Name = "Shape133";
			this.Shape133.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape133.Top = 6.041667F;
			this.Shape133.Width = 0.1770833F;
			// 
			// Shape134
			// 
			this.Shape134.Height = 0.1770833F;
			this.Shape134.Left = 8.135417F;
			this.Shape134.Name = "Shape134";
			this.Shape134.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape134.Top = 6.53125F;
			this.Shape134.Width = 0.1770833F;
			// 
			// Shape135
			// 
			this.Shape135.Height = 0.1770833F;
			this.Shape135.Left = 8.135417F;
			this.Shape135.Name = "Shape135";
			this.Shape135.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape135.Top = 7.03125F;
			this.Shape135.Width = 0.1770833F;
			// 
			// Shape136
			// 
			this.Shape136.Height = 0.1770833F;
			this.Shape136.Left = 8.520833F;
			this.Shape136.Name = "Shape136";
			this.Shape136.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape136.Top = 4.541667F;
			this.Shape136.Width = 0.1770833F;
			// 
			// Shape137
			// 
			this.Shape137.Height = 0.1770833F;
			this.Shape137.Left = 8.520833F;
			this.Shape137.Name = "Shape137";
			this.Shape137.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape137.Top = 5.041667F;
			this.Shape137.Width = 0.1770833F;
			// 
			// Shape138
			// 
			this.Shape138.Height = 0.1770833F;
			this.Shape138.Left = 8.520833F;
			this.Shape138.Name = "Shape138";
			this.Shape138.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape138.Top = 5.541667F;
			this.Shape138.Width = 0.1770833F;
			// 
			// Shape139
			// 
			this.Shape139.Height = 0.1770833F;
			this.Shape139.Left = 8.520833F;
			this.Shape139.Name = "Shape139";
			this.Shape139.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape139.Top = 6.041667F;
			this.Shape139.Width = 0.1770833F;
			// 
			// Shape140
			// 
			this.Shape140.Height = 0.1770833F;
			this.Shape140.Left = 8.520833F;
			this.Shape140.Name = "Shape140";
			this.Shape140.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape140.Top = 6.53125F;
			this.Shape140.Width = 0.1770833F;
			// 
			// Shape141
			// 
			this.Shape141.Height = 0.1770833F;
			this.Shape141.Left = 8.520833F;
			this.Shape141.Name = "Shape141";
			this.Shape141.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape141.Top = 7.03125F;
			this.Shape141.Width = 0.1770833F;
			// 
			// Shape142
			// 
			this.Shape142.Height = 0.1770833F;
			this.Shape142.Left = 8.916667F;
			this.Shape142.Name = "Shape142";
			this.Shape142.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape142.Top = 4.541667F;
			this.Shape142.Width = 0.1770833F;
			// 
			// Shape143
			// 
			this.Shape143.Height = 0.1770833F;
			this.Shape143.Left = 8.916667F;
			this.Shape143.Name = "Shape143";
			this.Shape143.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape143.Top = 5.041667F;
			this.Shape143.Width = 0.1770833F;
			// 
			// Shape144
			// 
			this.Shape144.Height = 0.1770833F;
			this.Shape144.Left = 8.916667F;
			this.Shape144.Name = "Shape144";
			this.Shape144.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape144.Top = 5.541667F;
			this.Shape144.Width = 0.1770833F;
			// 
			// Shape145
			// 
			this.Shape145.Height = 0.1770833F;
			this.Shape145.Left = 8.916667F;
			this.Shape145.Name = "Shape145";
			this.Shape145.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape145.Top = 6.041667F;
			this.Shape145.Width = 0.1770833F;
			// 
			// Shape146
			// 
			this.Shape146.Height = 0.1770833F;
			this.Shape146.Left = 8.916667F;
			this.Shape146.Name = "Shape146";
			this.Shape146.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape146.Top = 6.53125F;
			this.Shape146.Width = 0.1770833F;
			// 
			// Shape147
			// 
			this.Shape147.Height = 0.1770833F;
			this.Shape147.Left = 8.916667F;
			this.Shape147.Name = "Shape147";
			this.Shape147.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape147.Top = 7.03125F;
			this.Shape147.Width = 0.1770833F;
			// 
			// Shape148
			// 
			this.Shape148.Height = 0.1770833F;
			this.Shape148.Left = 9.3125F;
			this.Shape148.Name = "Shape148";
			this.Shape148.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape148.Top = 4.541667F;
			this.Shape148.Width = 0.1770833F;
			// 
			// Shape149
			// 
			this.Shape149.Height = 0.1770833F;
			this.Shape149.Left = 9.3125F;
			this.Shape149.Name = "Shape149";
			this.Shape149.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape149.Top = 5.041667F;
			this.Shape149.Width = 0.1770833F;
			// 
			// Shape150
			// 
			this.Shape150.Height = 0.1770833F;
			this.Shape150.Left = 9.3125F;
			this.Shape150.Name = "Shape150";
			this.Shape150.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape150.Top = 5.541667F;
			this.Shape150.Width = 0.1770833F;
			// 
			// Shape151
			// 
			this.Shape151.Height = 0.1770833F;
			this.Shape151.Left = 9.3125F;
			this.Shape151.Name = "Shape151";
			this.Shape151.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape151.Top = 6.041667F;
			this.Shape151.Width = 0.1770833F;
			// 
			// Shape152
			// 
			this.Shape152.Height = 0.1770833F;
			this.Shape152.Left = 9.3125F;
			this.Shape152.Name = "Shape152";
			this.Shape152.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape152.Top = 6.53125F;
			this.Shape152.Width = 0.1770833F;
			// 
			// Shape153
			// 
			this.Shape153.Height = 0.1770833F;
			this.Shape153.Left = 9.3125F;
			this.Shape153.Name = "Shape153";
			this.Shape153.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape153.Top = 7.03125F;
			this.Shape153.Width = 0.1770833F;
			// 
			// Shape154
			// 
			this.Shape154.Height = 0.1770833F;
			this.Shape154.Left = 9.708333F;
			this.Shape154.Name = "Shape154";
			this.Shape154.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape154.Top = 4.541667F;
			this.Shape154.Width = 0.1770833F;
			// 
			// Shape155
			// 
			this.Shape155.Height = 0.1770833F;
			this.Shape155.Left = 9.708333F;
			this.Shape155.Name = "Shape155";
			this.Shape155.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape155.Top = 5.041667F;
			this.Shape155.Width = 0.1770833F;
			// 
			// Shape156
			// 
			this.Shape156.Height = 0.1770833F;
			this.Shape156.Left = 9.708333F;
			this.Shape156.Name = "Shape156";
			this.Shape156.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape156.Top = 5.541667F;
			this.Shape156.Width = 0.1770833F;
			// 
			// Shape157
			// 
			this.Shape157.Height = 0.1770833F;
			this.Shape157.Left = 9.708333F;
			this.Shape157.Name = "Shape157";
			this.Shape157.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape157.Top = 6.041667F;
			this.Shape157.Width = 0.1770833F;
			// 
			// Shape158
			// 
			this.Shape158.Height = 0.1770833F;
			this.Shape158.Left = 9.708333F;
			this.Shape158.Name = "Shape158";
			this.Shape158.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape158.Top = 6.53125F;
			this.Shape158.Width = 0.1770833F;
			// 
			// Shape159
			// 
			this.Shape159.Height = 0.1770833F;
			this.Shape159.Left = 9.708333F;
			this.Shape159.Name = "Shape159";
			this.Shape159.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape159.Top = 7.03125F;
			this.Shape159.Width = 0.1770833F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1770833F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 0F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-size: 8pt; font-weight: bold";
			this.Label177.Text = "XID #1607";
			this.Label177.Top = 7.427083F;
			this.Label177.Width = 0.65625F;
			// 
			// Label193
			// 
			this.Label193.Height = 0.1770833F;
			this.Label193.HyperLink = null;
			this.Label193.Left = 4.625F;
			this.Label193.Name = "Label193";
			this.Label193.Style = "font-size: 8pt";
			this.Label193.Text = "41-0852411";
			this.Label193.Top = 7.427083F;
			this.Label193.Width = 0.78125F;
			// 
			// Label194
			// 
			this.Label194.Height = 0.1770833F;
			this.Label194.HyperLink = null;
			this.Label194.Left = 7.260417F;
			this.Label194.Name = "Label194";
			this.Label194.Style = "font-size: 8pt";
			this.Label194.Text = "1095BIRSC";
			this.Label194.Top = 7.427083F;
			this.Label194.Visible = false;
			this.Label194.Width = 0.78125F;
			// 
			// rpt1095B2016BlankPage2
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredName12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredSSN12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredDOB12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredAll12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth1_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth2_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth3_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth4_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth5_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth6_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth7_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth8_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth9_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth10_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth11_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoveredMonth12_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label187)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label188)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label189)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label190)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label191)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label192)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label193)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label194)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredName12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredSSN12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredDOB12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredAll12_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth1_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth2_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth3_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth4_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth5_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth6_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth7_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth8_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth9_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth10_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth11_12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoveredMonth12_12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label178;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label179;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label180;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label181;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label182;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label183;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label184;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label185;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label186;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label187;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label188;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label166;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label167;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label171;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label189;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label190;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label191;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label192;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape25;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape82;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape83;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape84;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape85;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape86;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape87;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape88;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape89;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape90;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape91;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape92;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape93;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape94;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape95;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape96;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape97;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape98;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape99;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape100;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape101;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape102;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape103;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape104;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape105;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape36;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape37;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape38;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape39;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape40;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape41;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape42;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape43;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape44;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape45;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape46;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape47;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape48;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape49;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape50;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape51;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape106;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape107;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape108;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape109;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape110;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape111;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape112;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape113;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape114;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape115;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape116;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape117;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape118;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape119;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape120;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape121;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape122;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape123;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape124;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape125;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape126;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape127;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape128;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape129;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape52;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape53;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape54;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape55;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape56;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape57;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape59;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape60;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape61;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape62;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape63;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape64;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape65;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape66;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape67;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape68;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape69;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape70;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape71;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape72;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape73;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape74;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape75;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape76;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape77;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape78;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape79;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape80;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape81;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape130;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape131;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape132;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape133;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape134;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape135;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape136;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape137;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape138;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape139;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape140;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape141;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape142;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape143;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape144;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape145;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape146;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape147;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape148;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape149;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape150;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape151;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape152;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape153;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape154;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape155;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape156;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape157;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape158;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape159;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label193;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label194;
	}
}
