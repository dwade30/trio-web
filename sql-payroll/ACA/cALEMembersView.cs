﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class cALEMembersView
	{
		//=========================================================
		private cGenericCollection membersList = new cGenericCollection();
		private cACAService aService = new cACAService();
		private bool boolChanged;
		private int intTaxYear;

		public int TaxYear
		{
			get
			{
				int TaxYear = 0;
				TaxYear = intTaxYear;
				return TaxYear;
			}
			set
			{
				intTaxYear = value;
			}
		}

		public bool Changed
		{
			get
			{
				bool Changed = false;
				Changed = boolChanged;
				return Changed;
			}
			set
			{
				boolChanged = value;
			}
		}

		public void SetMembers(ref cGenericCollection listOfMembers)
		{
			membersList = listOfMembers;
		}

		public cGenericCollection Members
		{
			get
			{
				cGenericCollection Members = null;
				Members = membersList;
				return Members;
			}
		}

		public void ClearMembers()
		{
			membersList.ClearList();
		}

		public void LoadMembers(int intYear)
		{
			intTaxYear = intYear;
			membersList = aService.LoadALEMembers(intYear);
			boolChanged = false;
		}

		public void SaveMembers()
		{
			aService.SaveALEMembers(membersList, intTaxYear);
		}

		public void AddMember(cAggregatedMember aMember)
		{
			if (!(aMember == null))
			{
				membersList.AddItem(aMember);
				boolChanged = true;
			}
		}

		public void CreateMember(string strName, string strEIN)
		{
			cAggregatedMember aMember = new cAggregatedMember();
			aMember.EIN = strEIN;
			aMember.Name = strName;
			AddMember(aMember);
		}

		public void RemoveMember(int lngID)
		{
			if (lngID > 0)
			{
				membersList.MoveFirst();
				cAggregatedMember aMember;
				while (membersList.IsCurrent())
				{
					aMember = (cAggregatedMember)membersList.GetCurrentItem();
					if (aMember.ID == lngID)
					{
						membersList.RemoveCurrent();
						break;
					}
					membersList.MoveNext();
				}
			}
		}

		public void RemoveMemberByIndex(int intIndex)
		{
			if (intIndex > 0)
			{
				int intCount = 0;
				membersList.MoveFirst();
				intCount = 0;
				while (membersList.IsCurrent())
				{
					intCount += 1;
					if (intCount == intIndex)
					{
						membersList.RemoveCurrent();
						break;
					}
					membersList.MoveNext();
				}
			}
		}
	}
}
