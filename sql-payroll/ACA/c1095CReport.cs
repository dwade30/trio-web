﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class c1095CReport
	{
		//=========================================================
		// Private planList  As cGenericCollection
		private cGenericCollection formsList;
		private cEmployerRecord employerRecord;
		private bool boolMaskSSNs;
		private bool boolTestPrint;
		private int intReportYear;
		private bool boolHasOverFlows;
		private double dblVerticalAlignment;
		private double dblHorizontalAlignment;
		private bool boolIncludeUncoveredEmployees;

        public int OverflowThreshhold { get; set; } = 6;
		public bool IncludeUncoveredEmployees
		{
			set
			{
				boolIncludeUncoveredEmployees = value;
			}
			get
			{
				bool IncludeUncoveredEmployees = false;
				IncludeUncoveredEmployees = boolIncludeUncoveredEmployees;
				return IncludeUncoveredEmployees;
			}
		}

		public double VerticalAlignment
		{
			set
			{
				dblVerticalAlignment = value;
			}
			get
			{
				double VerticalAlignment = 0;
				VerticalAlignment = dblVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public double HorizontalAlignment
		{
			set
			{
				dblHorizontalAlignment = value;
			}
			get
			{
				double HorizontalAlignment = 0;
				HorizontalAlignment = dblHorizontalAlignment;
				return HorizontalAlignment;
			}
		}

		public bool HasOverflows
		{
			set
			{
				boolHasOverFlows = value;
			}
			get
			{
				bool HasOverflows = false;
				HasOverflows = boolHasOverFlows;
				return HasOverflows;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public bool TestPrint
		{
			set
			{
				boolTestPrint = value;
			}
			get
			{
				bool TestPrint = false;
				TestPrint = boolTestPrint;
				return TestPrint;
			}
		}

		public bool MaskSSNs
		{
			set
			{
				boolMaskSSNs = value;
			}
			get
			{
				bool MaskSSNs = false;
				MaskSSNs = boolMaskSSNs;
				return MaskSSNs;
			}
		}

		public cGenericCollection ListOfForms
		{
			set
			{
				formsList = value;
			}
			get
			{
				cGenericCollection ListOfForms = null;
				ListOfForms = formsList;
				return ListOfForms;
			}
		}
		//
		// Public Property Let ListOfPlans(ByRef plansList As cGenericCollection)
		// Set planList = plansList
		// End Property
		//
		// Public Property Get ListOfPlans() As cGenericCollection
		// Set ListOfPlans = planList
		// End Property
		public cEmployerRecord EmployerInfo
		{
			set
			{
				employerRecord = value;
			}
			get
			{
				cEmployerRecord EmployerInfo = null;
				EmployerInfo = employerRecord;
				return EmployerInfo;
			}
		}
	}
}
