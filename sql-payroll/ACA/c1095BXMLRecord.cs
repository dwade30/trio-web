﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class c1095BXMLRecord
	{
		//=========================================================
		private c1095B baseRecord = new c1095B();
		private int lngRecordID;
		private string strScenarioID = string.Empty;
		private cEmployee employeerecord = new cEmployee();
		private string strCorrectedUniqueID = string.Empty;

		public cEmployee Employee
		{
			get
			{
				cEmployee Employee = null;
				Employee = employeerecord;
				return Employee;
			}
		}

		public string ScenarioID
		{
			set
			{
				strScenarioID = value;
			}
			get
			{
				string ScenarioID = "";
				ScenarioID = strScenarioID;
				return ScenarioID;
			}
		}

		public int RecordID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int RecordID = 0;
				RecordID = lngRecordID;
				return RecordID;
			}
		}

		public c1095B BaseInfo
		{
			set
			{
				baseRecord = value;
			}
			get
			{
				c1095B BaseInfo = null;
				BaseInfo = baseRecord;
				return BaseInfo;
			}
		}

		public string CorrectedUniqueID
		{
			set
			{
				strCorrectedUniqueID = value;
			}
			get
			{
				string CorrectedUniqueID = "";
				CorrectedUniqueID = strCorrectedUniqueID;
				return CorrectedUniqueID;
			}
		}
	}
}
