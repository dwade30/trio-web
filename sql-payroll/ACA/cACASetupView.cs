﻿//Fecher vbPorter - Version 1.0.0.59
using Global;
using System;

namespace TWPY0000
{
	public class cACASetupView
	{
		//=========================================================
		private cACAService acaServ = new cACAService();
		private cACAEmployeeSetup currentSetup = new cACAEmployeeSetup();
		private int lngLastError;
		private string strLastError = "";

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public string LastErrorDescription
		{
			get
			{
				string LastErrorDescription = "";
				LastErrorDescription = strLastError;
				return LastErrorDescription;
			}
		}

		public void ClearErrors()
		{
			lngLastError = 0;
			strLastError = "";
		}

		public cGenericCollection CoveragePlans
		{
			get
			{
				cGenericCollection CoveragePlans = null;
				CoveragePlans = acaServ.GetAllProviders();
				return CoveragePlans;
			}
		}

		public cACAEmployeeSetup CurrentRecord
		{
			get
			{
				cACAEmployeeSetup CurrentRecord = null;
				CurrentRecord = currentSetup;
				return CurrentRecord;
			}
		}

		public void LoadSetup(int lngID)
		{
			currentSetup = acaServ.GetACAEmployeeSetup(lngID);
		}

		public void LoadSetupByEmployee(int lngID, int lngYear)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ClearErrors();
				currentSetup = acaServ.GetACAEmployeeSetupByEmployee(lngID, lngYear);
				if (acaServ.LastErrorNumber > 0)
				{
					lngLastError = acaServ.LastErrorNumber;
					strLastError = acaServ.LastErrorDescription;
				}
				if (currentSetup.ID == 0)
				{
					currentSetup.EmployeeID = lngID;
					currentSetup.ReportYear = lngYear;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = fecherFoundation.Information.Err(ex).Number;
				strLastError = fecherFoundation.Information.Err(ex).Description + "  In LoadSetupByEmployee line " + fecherFoundation.Information.Erl();
			}
		}

		public void RemoveCurrent()
		{
			if (!(currentSetup == null))
			{
				acaServ.DeleteACAEmployeeSetup(currentSetup.ID);
				currentSetup = null;
			}
		}

		public void SaveSetup()
		{
			acaServ.SaveACASetup(currentSetup);
		}

		public void NEWRECORD()
		{
			currentSetup = new cACAEmployeeSetup();
		}

		public void UpdateDependent(ref cACAEmployeeDependent dep)
		{
			if (!(dep == null))
			{
				if (!(currentSetup == null))
				{
					cACAEmployeeDependent tdep;
					cACAEmployeeDependent saveDep = null;
					if (dep.ID > 0)
					{
						foreach (cACAEmployeeDependent tdep_foreach in currentSetup.Dependents)
						{
							tdep = (cACAEmployeeDependent)tdep_foreach;
							if (tdep.ID == dep.ID)
							{
								saveDep = tdep;
								break;
							}
							tdep = null;
						}
					}
					if (saveDep == null)
					{
						saveDep = new cACAEmployeeDependent();
						currentSetup.Dependents.Add(saveDep);
					}
					saveDep.DateOfBirth = dep.DateOfBirth;
					saveDep.FirstName = dep.FirstName;
					saveDep.HealthCareTerminationDate = dep.HealthCareTerminationDate;
					saveDep.LastName = dep.LastName;
					saveDep.MiddleName = dep.MiddleName;
					saveDep.SSN = dep.SSN;
					saveDep.Suffix = dep.Suffix;
					saveDep.Deleted = dep.Deleted;
					int x;
					for (x = 1; x <= 12; x++)
					{
						saveDep.SetIsCoveredForMonth(x, dep.GetIsCoveredForMonth(x));
					}
					// x
				}
			}
		}

		public void RemoveDependent(int lngID)
		{
			if (!(currentSetup == null))
			{
				int x;
				for (x = 1; x <= currentSetup.Dependents.Count; x++)
				{
					if (currentSetup.Dependents[x].ID == lngID)
					{
						currentSetup.Dependents.Remove(x);
						return;
					}
				}
			}
		}
	}
}
