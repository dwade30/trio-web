﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using System;

namespace TWPY0000
{
	public class c1095CController
	{
		//=========================================================
		cACAService acaService = new cACAService();
		cEmployerService employerService = new cEmployerService();
		cEmployeeService employeeService = new cEmployeeService();

		public void LoadReport(ref c1095CReport theReport)
		{
			cGenericCollection providers;
			providers = acaService.GetAll1095CProviders();
			cCoverageProvider Provider;
			cEmployerRecord EmployerInfo;
			cGenericCollection setupList;
			cACAEmployeeSetup empSetup;
			c1095C reportRecord;
			cEmployee empRecord;
			cGenericCollection reportRecords = new cGenericCollection();
			string strTemp = "";
			cGenericCollection tempList;
			setupList = acaService.GetAll1095CSetups(theReport.ReportYear);
			if (theReport.IncludeUncoveredEmployees)
			{
				tempList = acaService.GetSetupsForUncoveredEmployees(theReport.ReportYear);
				if (tempList.ItemCount() > 0)
				{
					tempList.MoveFirst();
					while (tempList.IsCurrent())
					{
						setupList.AddItem(tempList.GetCurrentItem());
						tempList.MoveNext();
					}
				}
			}
			// theReport.ListOfPlans = providers
			EmployerInfo = employerService.GetEmployerInfo();
			theReport.EmployerInfo = EmployerInfo;
			setupList.MoveFirst();
			while (setupList.IsCurrent())
			{
				empSetup = (cACAEmployeeSetup)setupList.GetCurrentItem();
				reportRecord = new c1095C();
				reportRecord.ACAEmployee = empSetup;
				empRecord = employeeService.GetEmployeeByID(empSetup.EmployeeID);
				reportRecord.Address = empRecord.Address1;
				reportRecord.City = empRecord.City;
				reportRecord.State = empRecord.State;
				reportRecord.PostalCode = empRecord.Zip;
				if (empSetup.Dependents.Count > theReport.OverflowThreshhold)
				{
					theReport.HasOverflows = true;
				}
				if (empRecord.Zip4.Trim() != "")
				{
					reportRecord.PostalCode = reportRecord.PostalCode + " " + empRecord.Zip4;
				}
				strTemp = empRecord.SSN;
				if (strTemp.Length == 9)
				{
					reportRecord.SSN = Strings.Left(strTemp, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Right(strTemp, 4);
				}
				else
				{
					reportRecord.SSN = strTemp;
				}
				reportRecord.EmployeeName = empRecord.FullName;
				reportRecord.EmployeeFirstName = empRecord.FirstName;
				reportRecord.EmployeeMiddleName = empRecord.MiddleName;
				reportRecord.EmployeeLastName = (empRecord.LastName + " " + empRecord.Designation).Trim();
                if (empRecord.BirthDate.IsDate())
                {
                    var age = new DateTime(theReport.ReportYear, 1, 1).DifferenceInWholeYears(empRecord.BirthDate.ToDate());
                    if (age.IsBetween(1,120))
                    {
                        reportRecord.Age = age;
                    }
                }
				if (empSetup.CoverageProviderID > 0)
				{
					providers.MoveFirst();
					while (providers.IsCurrent())
					{
						Provider = (cCoverageProvider)providers.GetCurrentItem();
						if (Provider.ID == empSetup.CoverageProviderID)
						{
							reportRecord.PlanStartMonth = Provider.PlanMonth;
							break;
						}
						providers.MoveNext();
					}
				}
				reportRecords.AddItem(reportRecord);
				setupList.MoveNext();
			}
			theReport.ListOfForms = reportRecords;
		}
	}
}
