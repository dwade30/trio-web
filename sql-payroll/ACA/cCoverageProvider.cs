﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cCoverageProvider
	{
		//=========================================================
		private int lngID;
		private string strEIN = string.Empty;
		private string strTelephone = string.Empty;
		private string strName = string.Empty;
		private string strAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strPlanName = string.Empty;
		private bool bool1095B;
		private int intPlanMonth;
		private bool boolChanged;

		public bool IsChanged
		{
			set
			{
				boolChanged = value;
			}
			get
			{
				bool IsChanged = false;
				IsChanged = boolChanged;
				return IsChanged;
			}
		}

		public string PlanName
		{
			set
			{
				strPlanName = value;
				IsChanged = true;
			}
			get
			{
				string PlanName = "";
				PlanName = strPlanName;
				return PlanName;
			}
		}

		public bool Use1095B
		{
			set
			{
				bool1095B = value;
				IsChanged = true;
			}
			get
			{
				bool Use1095B = false;
				Use1095B = bool1095B;
				return Use1095B;
			}
		}

		public int PlanMonth
		{
			set
			{
				intPlanMonth = value;
				IsChanged = true;
			}
			get
			{
				int PlanMonth = 0;
				PlanMonth = intPlanMonth;
				return PlanMonth;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string EIN
		{
			set
			{
				strEIN = value;
				IsChanged = true;
			}
			get
			{
				string EIN = "";
				EIN = strEIN;
				return EIN;
			}
		}

		public string Telephone
		{
			set
			{
				strTelephone = value;
				IsChanged = true;
			}
			get
			{
				string Telephone = "";
				Telephone = strTelephone;
				return Telephone;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
				IsChanged = true;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public string Address
		{
			set
			{
				strAddress = value;
				IsChanged = true;
			}
			get
			{
				string Address = "";
				Address = strAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
				IsChanged = true;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
				IsChanged = true;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string PostalCode
		{
			set
			{
				strZip = value;
				IsChanged = true;
			}
			get
			{
				string PostalCode = "";
				PostalCode = strZip;
				return PostalCode;
			}
		}
	}
}
