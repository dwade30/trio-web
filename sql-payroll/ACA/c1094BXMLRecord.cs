﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;

namespace TWPY0000
{
	public class c1094BXMLRecord
	{
		//=========================================================
		private c1094B baseRecord;
		private string strScenario = string.Empty;
		private string strUniqueID = string.Empty;
		private cGenericCollection listDetails = new cGenericCollection();
		private int intSubmissionID;
		private cEmployerRecord employerRecord = new cEmployerRecord();

		public cEmployerRecord Employer
		{
			get
			{
				cEmployerRecord Employer = null;
				Employer = employerRecord;
				return Employer;
			}
		}

		public int SubmissionID
		{
			set
			{
				intSubmissionID = value;
			}
			get
			{
				int SubmissionID = 0;
				SubmissionID = intSubmissionID;
				return SubmissionID;
			}
		}

		public cGenericCollection Details
		{
			set
			{
				listDetails = value;
			}
			get
			{
				return listDetails;
            }
		}

		public string ScenarioID
		{
			set
			{
				strScenario = value;
			}
			get
			{
				string ScenarioID = "";
				ScenarioID = strScenario;
				return ScenarioID;
			}
		}

		public c1094B BaseInfo
		{
			set
			{
				baseRecord = value;
			}
			get
			{
				c1094B BaseInfo = null;
				BaseInfo = baseRecord;
				return BaseInfo;
			}
		}

		public string UNIQUEID
		{
			set
			{
				strUniqueID = value;
			}
			get
			{
				string UNIQUEID = "";
				UNIQUEID = strUniqueID;
				return UNIQUEID;
			}
		}
	}
}
