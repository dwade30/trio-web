﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class cCoverageProviderView
	{
		//=========================================================
		private cCoverageProvider currentProvider;
		private cGenericCollection planList = new cGenericCollection();
		private cACAService aService = new cACAService();

		public bool IsCurrentPlan
		{
			get
			{
				bool IsCurrentPlan = false;
				IsCurrentPlan = !(currentProvider == null);
				return IsCurrentPlan;
			}
		}

		public cCoverageProvider CurrentPlan
		{
			get
			{
				cCoverageProvider CurrentPlan = null;
				CurrentPlan = currentProvider;
				return CurrentPlan;
			}
		}

		public bool Changed
		{
			get
			{
				bool Changed = false;
				if (!(currentProvider == null))
				{
					Changed = currentProvider.IsChanged;
				}
				return Changed;
			}
		}

		public void LoadPlans()
		{
			planList = aService.GetAllProviders();
		}

		public object ListOfPlans
		{
			get
			{
				object ListOfPlans = null;
				ListOfPlans = planList;
				return ListOfPlans;
			}
		}

		public void AddPlan()
		{
			currentProvider = new cCoverageProvider();
			currentProvider.PlanMonth = 1;
		}

		public bool SavePlan()
		{
			bool SavePlan = false;
			if (!(currentProvider == null))
			{
				SavePlan = aService.SaveCoverageProvider(ref currentProvider);
				LoadPlans();
			}
			return SavePlan;
		}

		public void LoadPlan(int lngID)
		{
			currentProvider = aService.LoadPlan(lngID);
		}
	}
}
