﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation.VisualBasicLayer;
using Global;

namespace TWPY0000
{
	public class cACADataImportFormatView
	{
		//=========================================================
		private cACADataImportFormat currentFormat;
		private cACAService aService = new cACAService();
		private cGenericCollection formatsList = new cGenericCollection();
		private string strFileName = string.Empty;
		private cGenericCollection filePreviewList = new cGenericCollection();
		private int intPlanYear;

		public int PlanYear
		{
			set
			{
				intPlanYear = value;
			}
			get
			{
				int PlanYear = 0;
				PlanYear = intPlanYear;
				return PlanYear;
			}
		}

		public cGenericCollection DependentFilePreview
		{
			get
			{
				cGenericCollection DependentFilePreview = null;
				DependentFilePreview = filePreviewList;
				return DependentFilePreview;
			}
		}

		public string FileName
		{
			set
			{
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public cGenericCollection ListOfFormats
		{
			get
			{
				cGenericCollection ListOfFormats = null;
				ListOfFormats = formatsList;
				return ListOfFormats;
			}
		}

		public bool IsCurrentFormat
		{
			get
			{
				bool IsCurrentFormat = false;
				IsCurrentFormat = !(currentFormat == null);
				return IsCurrentFormat;
			}
		}

		public bool Changed
		{
			get
			{
				bool Changed = false;
				if (!(currentFormat == null))
				{
					Changed = currentFormat.IsChanged;
				}
				return Changed;
			}
		}

		public cACADataImportFormat CurrentImportFormat
		{
			get
			{
				cACADataImportFormat CurrentImportFormat = null;
				CurrentImportFormat = currentFormat;
				return CurrentImportFormat;
			}
		}

		public void LoadFormats()
		{
			formatsList = aService.GetImportFormatsList();
		}

		public void AddColumn(string strColumnInfo)
		{
			if (!(currentFormat == null))
			{
				currentFormat.AddColumnName(strColumnInfo);
			}
		}

		public void RemoveColumn(string strColumnName)
		{
			if (!(currentFormat == null))
			{
				currentFormat.RemoveColumn(strColumnName);
			}
		}

		public void AddFormat()
		{
			currentFormat = new cACADataImportFormat();
			currentFormat.SeparatorCharacter = ",";
		}

		public void ClearColumns()
		{
			currentFormat.ClearColumns();
		}

		public void LoadFormat(int lngID)
		{
			currentFormat = aService.LoadImportFormat(lngID);
		}

		public bool SaveFormat()
		{
			bool SaveFormat = false;
			if (!(currentFormat == null))
			{
				SaveFormat = aService.SaveImportFormat(ref currentFormat);
				LoadFormats();
			}
			return SaveFormat;
		}

		public void PreviewFormat(int intLimit)
		{
			if (!(currentFormat == null))
			{
				if (fecherFoundation.Strings.Trim(strFileName) != "")
				{
					FCFileSystem fso = new FCFileSystem();
					if (FCFileSystem.FileExists(strFileName))
					{
						filePreviewList = aService.ImportCoveredDependentsPreview(ref currentFormat, strFileName, intLimit);
					}
				}
			}
		}

		public cGenericCollection ImportFormat()
		{
			cGenericCollection ImportFormat = null;
			if (!(currentFormat == null))
			{
				if (fecherFoundation.Strings.Trim(strFileName) != "")
				{
					FCFileSystem fso = new FCFileSystem();
					if (FCFileSystem.FileExists(strFileName))
					{
						ImportFormat = aService.ImportCoveredDependentsFile(currentFormat, strFileName, intPlanYear);
					}
					else
					{
						fecherFoundation.Information.Err().Raise(43, "", "File not found", null, null);
					}
				}
			}
			return ImportFormat;
		}
	}
}
