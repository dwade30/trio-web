﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cACAEmployeeDependentImported
	{
		//=========================================================
		private int lngID;
		private string strFirstName = string.Empty;
		private string strMiddleName = string.Empty;
		private string strLastName = string.Empty;
		private string strSuffix = string.Empty;
		private string strSSN = string.Empty;
		private string strDateOfBirth = string.Empty;
		private string strHealthCareTerminationDate = string.Empty;
		private bool[] boolForMonth = new bool[12 + 1];
		private int lngParentID;
		private bool boolDeleted;
		private string strEmployeeIdentifier = string.Empty;

		public string EmployeeIdentifier
		{
			set
			{
				strEmployeeIdentifier = value;
			}
			get
			{
				string EmployeeIdentifier = "";
				EmployeeIdentifier = strEmployeeIdentifier;
				return EmployeeIdentifier;
			}
		}

		public bool Deleted
		{
			set
			{
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}

		public int ACAEmployeeSetupID
		{
			set
			{
				lngParentID = value;
			}
			get
			{
				int ACAEmployeeSetupID = 0;
				ACAEmployeeSetupID = lngParentID;
				return ACAEmployeeSetupID;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string FirstName
		{
			set
			{
				strFirstName = value;
			}
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
		}

		public string MiddleName
		{
			set
			{
				strMiddleName = value;
			}
			get
			{
				string MiddleName = "";
				MiddleName = strMiddleName;
				return MiddleName;
			}
		}

		public string LastName
		{
			set
			{
				strLastName = value;
			}
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
		}

		public string Suffix
		{
			set
			{
				strSuffix = value;
			}
			get
			{
				string Suffix = "";
				Suffix = strSuffix;
				return Suffix;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string DateOfBirth
		{
			set
			{
				strDateOfBirth = value;
			}
			get
			{
				string DateOfBirth = "";
				DateOfBirth = strDateOfBirth;
				return DateOfBirth;
			}
		}

		public string HealthCareTerminationDate
		{
			set
			{
				strHealthCareTerminationDate = value;
			}
			get
			{
				string HealthCareTerminationDate = "";
				HealthCareTerminationDate = strHealthCareTerminationDate;
				return HealthCareTerminationDate;
			}
		}

		public bool CoveredAll12Months
		{
			set
			{
				if (value)
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						SetIsCoveredForMonth(x, true);
					}
					// x
				}
			}
			get
			{
				bool CoveredAll12Months = false;
				int x;
				bool boolLastCoverage;
				boolLastCoverage = GetIsCoveredForMonth(1);
				for (x = 1; x <= 12; x++)
				{
					if (GetIsCoveredForMonth(x) != boolLastCoverage)
					{
						boolLastCoverage = false;
						break;
					}
				}
				// x
				CoveredAll12Months = boolLastCoverage;
				return CoveredAll12Months;
			}
		}

		public void SetIsCoveredForMonth(int intMonth, bool boolCovered)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				boolForMonth[intMonth] = boolCovered;
			}
		}

		public bool GetIsCoveredForMonth(int intMonth)
		{
			bool GetIsCoveredForMonth = false;
			if (intMonth > 0 && intMonth < 13)
			{
				GetIsCoveredForMonth = boolForMonth[intMonth];
			}
			return GetIsCoveredForMonth;
		}
	}
}
