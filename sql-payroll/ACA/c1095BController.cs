﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;

namespace TWPY0000
{
	public class c1095BController
	{
		//=========================================================
		cACAService acaService = new cACAService();
		cEmployeeService employeeService = new cEmployeeService();

		public void LoadReport(ref c1095BReport theReport)
		{
			cGenericCollection providers;
			providers = acaService.GetAll1095BProviders();
			cCoverageProvider Provider;
			cGenericCollection setupList;
			cACAEmployeeSetup empSetup;
			c1095B reportRecord;
			cEmployee empRecord;
			cGenericCollection reportRecords = new cGenericCollection();
			string strTemp = "";
			setupList = acaService.GetAll1095BSetups(theReport.ReportYear);
			setupList.MoveFirst();
			while (setupList.IsCurrent())
			{
				empSetup = (cACAEmployeeSetup)setupList.GetCurrentItem();
				reportRecord = new c1095B();
				reportRecord.ACAEmployee = empSetup;
				empRecord = employeeService.GetEmployeeByID(empSetup.EmployeeID);
				reportRecord.Address = empRecord.Address1;
				reportRecord.City = empRecord.City;
				reportRecord.State = empRecord.State;
				reportRecord.PostalCode = empRecord.Zip;
				if (empSetup.Dependents.Count > 6)
				{
					theReport.HasOverflows = true;
				}
				if (fecherFoundation.Strings.Trim(empRecord.Zip4) != "")
				{
					reportRecord.PostalCode = reportRecord.PostalCode + " " + empRecord.Zip4;
				}
				strTemp = empRecord.SSN;
				if (strTemp.Length == 9)
				{
					reportRecord.SSN = Strings.Left(strTemp, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Right(strTemp, 4);
				}
				else
				{
					reportRecord.SSN = strTemp;
				}
				reportRecord.EmployeeName = empRecord.FullName;
				reportRecord.EmployeeFirstName = empRecord.FirstName;
				reportRecord.EmployeeMiddleName = empRecord.MiddleName;
				reportRecord.EmployeeLastName = (empRecord.LastName + " " + empRecord.Designation).Trim();
				
				if (empSetup.CoverageProviderID > 0)
				{
					providers.MoveFirst();
					while (providers.IsCurrent())
					{
						Provider = (cCoverageProvider)providers.GetCurrentItem();
						if (Provider.ID == empSetup.CoverageProviderID)
						{
							reportRecord.CoverageProvider = Provider;
							break;
						}
						providers.MoveNext();
					}
				}
				reportRecords.AddItem(reportRecord);
				setupList.MoveNext();
			}
			theReport.ListOfForms = reportRecords;
		}
	}
}
