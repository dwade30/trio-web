﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class c1094B
	{
		//=========================================================
		private int lngID;
		private string strFilerName = string.Empty;
		private string strEmployerEIN = string.Empty;
		private string strContactPerson = "";
		private string strTelephone = string.Empty;
		private string strAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strPostalCode = string.Empty;
		private int intNumberSubmissions;
		private int intYearCovered;
		private bool boolIsChanged;
		private double dblVerticalAlignment;
		private double dblHorizontalAlignment;
		private string strContactMiddle = string.Empty;
		private string strContactLast = string.Empty;
		private string strContactSuffix = string.Empty;

		public string ContactSuffix
		{
			set
			{
				strContactSuffix = value;
			}
			get
			{
				string ContactSuffix = "";
				ContactSuffix = strContactSuffix;
				return ContactSuffix;
			}
		}

		public string ContactLastName
		{
			set
			{
				strContactLast = value;
			}
			get
			{
				string ContactLastName = "";
				ContactLastName = strContactLast;
				return ContactLastName;
			}
		}

		public string ContactMiddleName
		{
			set
			{
				strContactMiddle = value;
			}
			get
			{
				string ContactMiddleName = "";
				ContactMiddleName = strContactMiddle;
				return ContactMiddleName;
			}
		}

		public string ContactFullName
		{
			get
			{
				string ContactFullName = "";
				ContactFullName = (((strContactPerson + " " + strContactMiddle).Trim() + " " + strContactLast).Trim() + " " + strContactSuffix).Trim();
				return ContactFullName;
			}
		}

		public double VerticalAlignment
		{
			set
			{
				dblVerticalAlignment = value;
			}
			get
			{
				double VerticalAlignment = 0;
				VerticalAlignment = dblVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public double HorizontalAlignment
		{
			set
			{
				dblHorizontalAlignment = value;
			}
			get
			{
				double HorizontalAlignment = 0;
				HorizontalAlignment = dblHorizontalAlignment;
				return HorizontalAlignment;
			}
		}

		public string PostalCode
		{
			set
			{
				strPostalCode = value;
				boolIsChanged = true;
			}
			get
			{
				string PostalCode = "";
				PostalCode = strPostalCode;
				return PostalCode;
			}
		}

		public string State
		{
			set
			{
				strState = value;
				boolIsChanged = true;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public bool IsChanged
		{
			set
			{
				boolIsChanged = value;
			}
			get
			{
				bool IsChanged = false;
				IsChanged = boolIsChanged;
				return IsChanged;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string FilerName
		{
			set
			{
				strFilerName = value;
				boolIsChanged = true;
			}
			get
			{
				string FilerName = "";
				FilerName = strFilerName;
				return FilerName;
			}
		}

		public string EIN
		{
			set
			{
				strEmployerEIN = value;
				boolIsChanged = true;
			}
			get
			{
				string EIN = "";
				EIN = strEmployerEIN;
				return EIN;
			}
		}

		public string ContactName
		{
			set
			{
				strContactPerson = value;
				boolIsChanged = true;
			}
			get
			{
				string ContactName = "";
				ContactName = strContactPerson;
				return ContactName;
			}
		}

		public string ContactPhone
		{
			set
			{
				strTelephone = value;
				boolIsChanged = true;
			}
			get
			{
				string ContactPhone = "";
				ContactPhone = strTelephone;
				return ContactPhone;
			}
		}

		public string Address
		{
			set
			{
				strAddress = value;
				boolIsChanged = true;
			}
			get
			{
				string Address = "";
				Address = strAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
				boolIsChanged = true;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public int NumberOfSubmissions
		{
			set
			{
				intNumberSubmissions = value;
				boolIsChanged = true;
			}
			get
			{
				int NumberOfSubmissions = 0;
				NumberOfSubmissions = intNumberSubmissions;
				return NumberOfSubmissions;
			}
		}

		public int YearCovered
		{
			set
			{
				intYearCovered = value;
				boolIsChanged = true;
			}
			get
			{
				int YearCovered = 0;
				YearCovered = intYearCovered;
				return YearCovered;
			}
		}
	}
}
