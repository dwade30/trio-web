﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class c1094BView
	{
		//=========================================================
		private c1094B current1094b;
		private cACAService aService = new cACAService();
		private cEmployerService eService = new cEmployerService();
		private int intCoverageYear;

		public bool Changed
		{
			get
			{
				bool Changed = false;
				if (IsCurrentForm)
				{
					Changed = current1094b.IsChanged;
				}
				return Changed;
			}
		}

		public bool IsCurrentForm
		{
			get
			{
				bool IsCurrentForm = false;
				IsCurrentForm = !(current1094b == null);
				return IsCurrentForm;
			}
		}

		public int CoverageYear
		{
			set
			{
				intCoverageYear = value;
			}
			get
			{
				int CoverageYear = 0;
				CoverageYear = intCoverageYear;
				return CoverageYear;
			}
		}

		public c1094B GetCurrentForm()
		{
			c1094B GetCurrentForm = null;
			GetCurrentForm = current1094b;
			return GetCurrentForm;
		}

		public void LoadForm(int intYear)
		{
			intCoverageYear = intYear;
			current1094b = aService.Load1094b(intYear);
			if (current1094b.YearCovered == 0)
			{
				current1094b.YearCovered = intYear;
				c1094B old1094B;
				old1094B = aService.Load1094b(intYear - 1);
				if (old1094B.YearCovered == intYear - 1)
				{
					current1094b.Address = old1094B.Address;
					current1094b.City = old1094B.City;
					current1094b.ContactName = old1094B.ContactName;
					current1094b.ContactPhone = old1094B.ContactPhone;
					current1094b.EIN = old1094B.EIN;
					current1094b.FilerName = old1094B.FilerName;
					current1094b.PostalCode = old1094B.PostalCode;
					current1094b.State = old1094B.State;
				}
				else
				{
					cEmployerRecord tEmployer;
					tEmployer = eService.GetEmployerInfo();
					current1094b.Address = tEmployer.Address;
					current1094b.City = tEmployer.City;
					current1094b.ContactPhone = tEmployer.Telephone;
					current1094b.EIN = tEmployer.EIN;
					current1094b.FilerName = tEmployer.Name;
					current1094b.PostalCode = fecherFoundation.Strings.Trim(tEmployer.Zip + " " + tEmployer.Zip4);
					current1094b.State = tEmployer.State;
				}
			}
		}

		public void SaveForm()
		{
			if (IsCurrentForm)
			{
				aService.Save1094B(ref current1094b);
			}
		}
	}
}
