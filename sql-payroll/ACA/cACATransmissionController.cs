//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class cACATransmissionController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strError)
		{
			lngLastError = lngError;
			strLastError = strError;
		}

		private void FillACATransmissionDetailsByTransmission(int lngID, cGenericCollection Details)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				Details.ClearList();
				cACATransmissionDetail aDetail;
				rsLoad.OpenRecordset("Select * from acatransmissiondetails where acatransmissionid = " + FCConvert.ToString(lngID) + " order by recordid", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					aDetail = new cACATransmissionDetail();
					FillACATransmissionDetail(ref aDetail, ref rsLoad);
					Details.AddItem(aDetail);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		private void FillACATransmissionDetail(ref cACATransmissionDetail aDetail, ref clsDRWrapper rsLoad)
		{
			if (!(aDetail == null) && !(rsLoad == null))
			{
				aDetail.ACASetupID = rsLoad.Get_Fields_Int32("ACASetupID");
				aDetail.ACATransmissionID = Conversion.Val(rsLoad.Get_Fields("acatransmissionid"));
				aDetail.RecordID = rsLoad.Get_Fields_Int32("RecordID");
				aDetail.ID = rsLoad.Get_Fields("ID");
				aDetail.IsUpdated = false;
			}
		}

		public cGenericCollection GetFullACATransmissions()
		{
			cGenericCollection GetFullACATransmissions = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACATransmission aTransmission;
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection transmissions = new cGenericCollection();
				rsLoad.OpenRecordset("select * from acatransmissions order by taxyear desc, id desc", "payroll");
				while (!rsLoad.EndOfFile())
				{
					aTransmission = new cACATransmission();
					FillACATransmission(aTransmission, rsLoad);
					FillACATransmissionDetailsByTransmission(aTransmission.ID, aTransmission.Details);
					transmissions.AddItem(aTransmission);
					rsLoad.MoveNext();
				}
				GetFullACATransmissions = transmissions;
				return GetFullACATransmissions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetFullACATransmissions;
		}

		public cACATransmission GetFullACATransmission(int lngID)
		{
			cACATransmission GetFullACATransmission = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACATransmission aTransmission;
				aTransmission = GetACATransmission(lngID);
				FillACATransmissionDetailsByTransmission(aTransmission.ID, aTransmission.Details);
				GetFullACATransmission = aTransmission;
				return GetFullACATransmission;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetFullACATransmission;
		}

		public cACATransmission GetFullACATransmissionByTransmissionID(string strTransmissionID)
		{
			cACATransmission GetFullACATransmissionByTransmissionID = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACATransmission aTransmission;
				aTransmission = GetACATransmissionByTransmissionID(strTransmissionID);
				FillACATransmissionDetailsByTransmission(aTransmission.ID, aTransmission.Details);
				GetFullACATransmissionByTransmissionID = aTransmission;
				return GetFullACATransmissionByTransmissionID;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetFullACATransmissionByTransmissionID;
		}

		public cACATransmission GetACATransmission(int lngID)
		{
			cACATransmission GetACATransmission = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("Select * from ACATransmissions where id = " + FCConvert.ToString(lngID), "Payroll");
				cACATransmission aTransmission = new cACATransmission();
				if (!rsLoad.EndOfFile())
				{
					FillACATransmission(aTransmission, rsLoad);
				}
				else
				{
					fecherFoundation.Information.Err().Raise(9999, "", "ACA Transmission record not found", null, null);
				}
				GetACATransmission = aTransmission;
				return GetACATransmission;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetACATransmission;
		}

		public cACATransmission GetACATransmissionByTransmissionID(string strTransmissionID)
		{
			cACATransmission GetACATransmissionByTransmissionID = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("Select * from acatransmissions where transmissionid = '" + strTransmissionID + "'", "Payroll");
				cACATransmission aTransmission = new cACATransmission();
				if (!rsLoad.EndOfFile())
				{
					FillACATransmission(aTransmission, rsLoad);
				}
				else
				{
					fecherFoundation.Information.Err().Raise(9999, "", "ACA Transmission record not found", null, null);
				}
				GetACATransmissionByTransmissionID = aTransmission;
				return GetACATransmissionByTransmissionID;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetACATransmissionByTransmissionID;
		}

		private void FillACATransmission(cACATransmission trans, clsDRWrapper rsLoad)
		{
			if (!(trans == null) && !(rsLoad == null))
			{
				trans.FileName = rsLoad.Get_Fields_String("FileName");
				trans.ID = rsLoad.Get_Fields("ID");
				trans.ReceiptID = rsLoad.Get_Fields_String("ReceiptID");
				trans.TaxYear = Conversion.Val(rsLoad.Get_Fields("TaxYear"));
				trans.TestFileCode = rsLoad.Get_Fields_String("TestFileCode");
				trans.TransmissionID = rsLoad.Get_Fields_String("TransmissionID");
				trans.TransmissionType = rsLoad.Get_Fields_String("TransmissionType");
				trans.IsUpdated = false;
			}
		}

		public void DeleteACATransmission(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngID > 0)
				{
					clsDRWrapper rs = new clsDRWrapper();
					rs.Execute("delete from ACATransmissions where id = " + FCConvert.ToString(lngID), "Payroll");
					rs.Execute("delete from ACATransmissionDetails where acatransmissionid = " + FCConvert.ToString(lngID), "Payroll");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void SaveACATransmission(ref cACATransmission aTransmission)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(aTransmission == null))
				{
					if (aTransmission.IsDeleted)
					{
						DeleteACATransmission(aTransmission.ID);
						return;
					}
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("Select * from ACATransmissions where id = " + aTransmission.ID, "payroll");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						if (aTransmission.ID > 0)
						{
							fecherFoundation.Information.Err().Raise(9999, "", "ACA Transmission record not found", null, null);
							return;
						}
						rsSave.AddNew();
					}
					rsSave.Set_Fields("FileName", aTransmission.FileName);
					rsSave.Set_Fields("ReceiptID", aTransmission.ReceiptID);
					rsSave.Set_Fields("TaxYear", aTransmission.TaxYear);
					rsSave.Set_Fields("TestFileCode", aTransmission.TestFileCode);
					rsSave.Set_Fields("TransmissionID", aTransmission.TransmissionID);
					rsSave.Set_Fields("TransmissionType", aTransmission.TransmissionType);
					rsSave.Update();
					aTransmission.ID = rsSave.Get_Fields("ID");
					aTransmission.IsUpdated = false;
					aTransmission.Details.MoveFirst();
					cACATransmissionDetail aDetail;
					while (aTransmission.Details.IsCurrent())
					{
						//App.DoEvents();
						aDetail = (cACATransmissionDetail)aTransmission.Details.GetCurrentItem();
						aDetail.ACATransmissionID = aTransmission.ID;
						SaveACATransmissionDetail(ref aDetail);
						aTransmission.Details.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void DeleteACATransmissionDetail(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngID > 0)
				{
					clsDRWrapper rs = new clsDRWrapper();
					rs.Execute("delete from ACATransmissionDetails where id = " + FCConvert.ToString(lngID), "Payroll");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void SaveACATransmissionDetail(ref cACATransmissionDetail aDetail)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(aDetail == null))
				{
					if (aDetail.IsDeleted)
					{
						DeleteACATransmissionDetail(aDetail.ID);
						return;
					}
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from acatransmissiondetails where id = " + aDetail.ID, "Payroll");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						if (aDetail.ID > 0)
						{
							fecherFoundation.Information.Err().Raise(9999, "", "ACA Transmission Detail record not found", null, null);
							return;
						}
						rsSave.AddNew();
					}
					rsSave.Set_Fields("ACASetupID", aDetail.ACASetupID);
					rsSave.Set_Fields("ACATransmissionID", aDetail.ACATransmissionID);
					rsSave.Set_Fields("Recordid", aDetail.RecordID);
					rsSave.Update();
					aDetail.ID = rsSave.Get_Fields("ID");
					aDetail.IsUpdated = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}
	}
}
