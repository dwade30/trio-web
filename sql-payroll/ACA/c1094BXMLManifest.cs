﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class c1094BXMLManifest
	{
		//=========================================================
		private string strTransmissionID = string.Empty;
		private string strTimeStamp = string.Empty;
		private int intPaymentYear;
		private bool boolPriorYearData;
		private string strEIN = string.Empty;
		private string strTransmissionTypeCode = string.Empty;
		private string strTestFileCode = string.Empty;
		private string strTransmitterName = string.Empty;
		private string strCompanyName = string.Empty;
		private string strCompanyAddress = string.Empty;
		private string strCompanyCity = string.Empty;
		private string strCompanyState = string.Empty;
		private string strCompanyPostalCode = string.Empty;
		private string strContactFirstName = string.Empty;
		private string strContactMiddleName = string.Empty;
		private string strContactLastName = string.Empty;
		private string strContactSuffix = string.Empty;
		private string strContactPhone = string.Empty;
		private string strVendorCode = string.Empty;
		private string strVendorContactFirstName = string.Empty;
		private string strVendorContactMiddleName = string.Empty;
		private string strVendorContactLastName = string.Empty;
		private string strVendorContactSuffixName = string.Empty;
		private string strVendorContactPhone = string.Empty;
		private int intTotalPayeeCount;
		private int intTotalPayerRecordCount;
		private string strSoftwareID = string.Empty;
		private string strFormType = string.Empty;
		private string strBinaryFormatCode = string.Empty;
		private string strCheckSum = string.Empty;
		private int lngAttachmentSize;
		private string strRequestFileName = string.Empty;

		public string TransmissionID
		{
			set
			{
				strTransmissionID = value;
			}
			get
			{
				string TransmissionID = "";
				TransmissionID = strTransmissionID;
				return TransmissionID;
			}
		}

		public string FileTimeStamp
		{
			set
			{
				strTimeStamp = value;
			}
			get
			{
				string FileTimeStamp = "";
				FileTimeStamp = strTimeStamp;
				return FileTimeStamp;
			}
		}

		public int PaymentYear
		{
			set
			{
				intPaymentYear = value;
			}
			get
			{
				int PaymentYear = 0;
				PaymentYear = intPaymentYear;
				return PaymentYear;
			}
		}

		public bool IsPriorYearData
		{
			set
			{
				boolPriorYearData = value;
			}
			get
			{
				bool IsPriorYearData = false;
				IsPriorYearData = boolPriorYearData;
				return IsPriorYearData;
			}
		}

		public string EIN
		{
			set
			{
				strEIN = value;
			}
			get
			{
				string EIN = "";
				EIN = strEIN;
				return EIN;
			}
		}

		public string TransmissionTypeCode
		{
			set
			{
				strTransmissionTypeCode = value;
			}
			get
			{
				string TransmissionTypeCode = "";
				TransmissionTypeCode = strTransmissionTypeCode;
				return TransmissionTypeCode;
			}
		}

		public string TestFileCode
		{
			set
			{
				strTestFileCode = value;
			}
			get
			{
				string TestFileCode = "";
				TestFileCode = strTestFileCode;
				return TestFileCode;
			}
		}

		public string TransmitterName
		{
			set
			{
				strTransmitterName = value;
			}
			get
			{
				string TransmitterName = "";
				TransmitterName = strTransmitterName;
				return TransmitterName;
			}
		}

		public string CompanyName
		{
			set
			{
				strCompanyName = value;
			}
			get
			{
				string CompanyName = "";
				CompanyName = strCompanyName;
				return CompanyName;
			}
		}

		public string CompanyAddress
		{
			set
			{
				strCompanyAddress = value;
			}
			get
			{
				string CompanyAddress = "";
				CompanyAddress = strCompanyAddress;
				return CompanyAddress;
			}
		}

		public string CompanyCity
		{
			set
			{
				strCompanyCity = value;
			}
			get
			{
				string CompanyCity = "";
				CompanyCity = strCompanyCity;
				return CompanyCity;
			}
		}

		public string CompanyState
		{
			set
			{
				strCompanyState = value;
			}
			get
			{
				string CompanyState = "";
				CompanyState = strCompanyState;
				return CompanyState;
			}
		}

		public string CompanyPostalCode
		{
			set
			{
				strCompanyPostalCode = value;
			}
			get
			{
				string CompanyPostalCode = "";
				CompanyPostalCode = strCompanyPostalCode;
				return CompanyPostalCode;
			}
		}

		public string ContactFirstName
		{
			set
			{
				strContactFirstName = value;
			}
			get
			{
				string ContactFirstName = "";
				ContactFirstName = strContactFirstName;
				return ContactFirstName;
			}
		}

		public string ContactMiddleName
		{
			set
			{
				strContactMiddleName = value;
			}
			get
			{
				string ContactMiddleName = "";
				ContactMiddleName = strContactMiddleName;
				return ContactMiddleName;
			}
		}

		public string ContactLastName
		{
			set
			{
				strContactLastName = value;
			}
			get
			{
				string ContactLastName = "";
				ContactLastName = strContactLastName;
				return ContactLastName;
			}
		}

		public string ContactSuffix
		{
			set
			{
				strContactSuffix = value;
			}
			get
			{
				string ContactSuffix = "";
				ContactSuffix = strContactSuffix;
				return ContactSuffix;
			}
		}

		public string ContactPhone
		{
			set
			{
				strContactPhone = value;
			}
			get
			{
				string ContactPhone = "";
				ContactPhone = strContactPhone;
				return ContactPhone;
			}
		}

		public string VendorCode
		{
			set
			{
				strVendorCode = value;
			}
			get
			{
				string VendorCode = "";
				VendorCode = strVendorCode;
				return VendorCode;
			}
		}

		public string VendorContactFirstName
		{
			set
			{
				strVendorContactFirstName = value;
			}
			get
			{
				string VendorContactFirstName = "";
				VendorContactFirstName = strVendorContactFirstName;
				return VendorContactFirstName;
			}
		}

		public string VendorContactMiddleName
		{
			set
			{
				strVendorContactMiddleName = value;
			}
			get
			{
				string VendorContactMiddleName = "";
				VendorContactMiddleName = strVendorContactMiddleName;
				return VendorContactMiddleName;
			}
		}

		public string VendorContactLastName
		{
			set
			{
				strVendorContactLastName = value;
			}
			get
			{
				string VendorContactLastName = "";
				VendorContactLastName = strVendorContactLastName;
				return VendorContactLastName;
			}
		}

		public string VendorContactSuffix
		{
			set
			{
				strVendorContactSuffixName = value;
			}
			get
			{
				string VendorContactSuffix = "";
				VendorContactSuffix = strVendorContactSuffixName;
				return VendorContactSuffix;
			}
		}

		public string VendorContactPhone
		{
			set
			{
				strVendorContactPhone = value;
			}
			get
			{
				string VendorContactPhone = "";
				VendorContactPhone = strVendorContactPhone;
				return VendorContactPhone;
			}
		}

		public int TotalPayeeCount
		{
			set
			{
				intTotalPayeeCount = value;
			}
			get
			{
				int TotalPayeeCount = 0;
				TotalPayeeCount = intTotalPayeeCount;
				return TotalPayeeCount;
			}
		}

		public int TotalPayerRecordCount
		{
			set
			{
				intTotalPayerRecordCount = value;
			}
			get
			{
				int TotalPayerRecordCount = 0;
				TotalPayerRecordCount = intTotalPayerRecordCount;
				return TotalPayerRecordCount;
			}
		}

		public string SoftwareID
		{
			set
			{
				strSoftwareID = value;
			}
			get
			{
				string SoftwareID = "";
				SoftwareID = strSoftwareID;
				return SoftwareID;
			}
		}

		public string FormType
		{
			set
			{
				strFormType = value;
			}
			get
			{
				string FormType = "";
				FormType = strFormType;
				return FormType;
			}
		}

		public string BinaryFormatCode
		{
			set
			{
				strBinaryFormatCode = value;
			}
			get
			{
				string BinaryFormatCode = "";
				BinaryFormatCode = strBinaryFormatCode;
				return BinaryFormatCode;
			}
		}

		public string CheckSum
		{
			set
			{
				strCheckSum = value;
			}
			get
			{
				string CheckSum = "";
				CheckSum = strCheckSum;
				return CheckSum;
			}
		}

		public int AttachmentSize
		{
			set
			{
				lngAttachmentSize = value;
			}
			get
			{
				int AttachmentSize = 0;
				AttachmentSize = lngAttachmentSize;
				return AttachmentSize;
			}
		}

		public string RequestFileName
		{
			set
			{
				strRequestFileName = value;
			}
			get
			{
				string RequestFileName = "";
				RequestFileName = strRequestFileName;
				return RequestFileName;
			}
		}

		public c1094BXMLManifest() : base()
		{
			TestFileCode = "P";
		}
	}
}
