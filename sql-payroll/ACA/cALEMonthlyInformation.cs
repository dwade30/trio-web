﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cALEMonthlyInformation
	{
		//=========================================================
		private int intMonth;
		private bool boolMinimumEssentialCoverageOffer;
		private int intFullTimeEmployeeCount;
		private int intTotalEmployeeCount;
		private bool boolAggregatedGroup;
		private string strSection4980HTransitionRelief = string.Empty;
		private int lngID;

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public int CoverageMonth
		{
			set
			{
				intMonth = value;
			}
			get
			{
				int CoverageMonth = 0;
				CoverageMonth = intMonth;
				return CoverageMonth;
			}
		}

		public bool MinimumEssentialCoverageOffer
		{
			set
			{
				boolMinimumEssentialCoverageOffer = value;
			}
			get
			{
				bool MinimumEssentialCoverageOffer = false;
				MinimumEssentialCoverageOffer = boolMinimumEssentialCoverageOffer;
				return MinimumEssentialCoverageOffer;
			}
		}

		public int FullTimeEmployeeCount
		{
			set
			{
				intFullTimeEmployeeCount = value;
			}
			get
			{
				int FullTimeEmployeeCount = 0;
				FullTimeEmployeeCount = intFullTimeEmployeeCount;
				return FullTimeEmployeeCount;
			}
		}

		public int TotalEmployeeCount
		{
			set
			{
				intTotalEmployeeCount = value;
			}
			get
			{
				int TotalEmployeeCount = 0;
				TotalEmployeeCount = intTotalEmployeeCount;
				return TotalEmployeeCount;
			}
		}

		public bool AggregatedGroup
		{
			set
			{
				boolAggregatedGroup = value;
			}
			get
			{
				bool AggregatedGroup = false;
				AggregatedGroup = boolAggregatedGroup;
				return AggregatedGroup;
			}
		}

		public string Section4980HTransitionRelief
		{
			set
			{
				strSection4980HTransitionRelief = value;
			}
			get
			{
				string Section4980HTransitionRelief = "";
				Section4980HTransitionRelief = strSection4980HTransitionRelief;
				return Section4980HTransitionRelief;
			}
		}
	}
}
