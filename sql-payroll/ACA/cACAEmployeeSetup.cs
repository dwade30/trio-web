﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public class cACAEmployeeSetup
	{
		//=========================================================
		private int lngID;
		private int lngEmployeeID;
		private string strEmployeeNumber = string.Empty;
		// vbPorter upgrade warning: boolCoverageDeclined As object	OnWrite(bool)
		private object boolCoverageDeclined;
		private string[] strCoverageRequiredForMonth = new string[12 + 1];
		private double[] dblEmployeeShareLowestPremiumForMonth = new double[12 + 1];
		private string[] strSafeHarborCodeForMonth = new string[12 + 1];
        private string[] zipCodesByMonth = new string[12];
		private int lngYear;
		private FCCollection collDependents = new FCCollection();
		private string strOriginOfPolicy = string.Empty;
		private int lngCoverageProviderID;

		public int CoverageProviderID
		{
			set
			{
				lngCoverageProviderID = value;
			}
			get
			{
				int CoverageProviderID = 0;
				CoverageProviderID = lngCoverageProviderID;
				return CoverageProviderID;
			}
		}

		public string OriginOfPolicy
		{
			set
			{
				strOriginOfPolicy = value;
			}
			get
			{
				string OriginOfPolicy = "";
				OriginOfPolicy = strOriginOfPolicy;
				return OriginOfPolicy;
			}
		}

		public int ReportYear
		{
			set
			{
				lngYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = lngYear;
				return ReportYear;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public int EmployeeID
		{
			set
			{
				lngEmployeeID = value;
			}
			get
			{
				int EmployeeID = 0;
				EmployeeID = lngEmployeeID;
				return EmployeeID;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public bool CoverageDeclined
		{
			set
			{
				boolCoverageDeclined = value;
			}
			get
			{
				bool CoverageDeclined = false;
				CoverageDeclined = FCConvert.ToBoolean(boolCoverageDeclined);
				return CoverageDeclined;
			}
		}

		public bool CoverageRequiredCodeSameAll12Months
		{
			get
			{
				bool CoverageRequiredCodeSameAll12Months = false;
				int x;
				string lastCode;
				lastCode = GetCoverageRequiredCodeForMonth(1);
				bool boolSame;
				boolSame = true;
				for (x = 1; x <= 12; x++)
				{
					if (GetCoverageRequiredCodeForMonth(x) != lastCode)
					{
						lastCode = "";
						boolSame = false;
						break;
					}
				}
				// x
				CoverageRequiredCodeSameAll12Months = boolSame;
				return CoverageRequiredCodeSameAll12Months;
			}
		}

		public string CoverageRequiredCodeAll12Months
		{
			set
			{
				if (fecherFoundation.Strings.Trim(value) != "")
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						SetCoverageRequiredCodeForMonth(x, value);
					}
					// x
				}
			}
		}

		public string CoverageCodeRequiredAll12Months
		{
			get
			{
				string CoverageCodeRequiredAll12Months = "";
				int x;
				string lastCode;
				lastCode = GetCoverageRequiredCodeForMonth(1);
				for (x = 1; x <= 12; x++)
				{
					if (GetCoverageRequiredCodeForMonth(x) != lastCode)
					{
						lastCode = "";
						break;
					}
				}
				// x
				CoverageCodeRequiredAll12Months = lastCode;
				return CoverageCodeRequiredAll12Months;
			}
		}

		public void SetCoverageRequiredCodeForMonth(int intMonth, string strCode)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				strCoverageRequiredForMonth[intMonth] = strCode;
			}
		}

		public string GetCoverageRequiredCodeForMonth(int intMonth)
		{
			string GetCoverageRequiredCodeForMonth = "";
			if (intMonth > 0 && intMonth < 13)
			{
				GetCoverageRequiredCodeForMonth = strCoverageRequiredForMonth[intMonth];
			}
			return GetCoverageRequiredCodeForMonth;
		}

		public bool SafeHarborCodeSameAll12Months
		{
			get
			{
				bool SafeHarborCodeSameAll12Months = false;
				int x;
				string lastCode;
				bool boolSame;
				boolSame = true;
				lastCode = GetSafeHarborCodeForMonth(1);
				for (x = 1; x <= 12; x++)
				{
					if (GetSafeHarborCodeForMonth(x) != lastCode)
					{
						lastCode = "";
						boolSame = false;
						break;
					}
				}
				// x
				SafeHarborCodeSameAll12Months = boolSame;
				return SafeHarborCodeSameAll12Months;
			}
		}

		public string SafeHarborCodeAll12Months
		{
			get
			{
				string SafeHarborCodeAll12Months = "";
				int x;
				string lastCode;
				lastCode = GetSafeHarborCodeForMonth(1);
				for (x = 1; x <= 12; x++)
				{
					if (GetSafeHarborCodeForMonth(x) != lastCode)
					{
						lastCode = "";
						break;
					}
				}
				// x
				SafeHarborCodeAll12Months = lastCode;
				return SafeHarborCodeAll12Months;
			}
			set
			{
				if (fecherFoundation.Strings.Trim(value) != "")
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						SetSafeHarborCodeForMonth(x, value);
					}
					// x
				}
			}
		}

        public bool ZipCodeSameAll12Months
        {
            get
            {
                var lastCode = zipCodesByMonth[0];
                foreach (var code in zipCodesByMonth)
                {
                    if (code != lastCode)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public string ZipCodeAll12Months
        {
            get
            {
                if (ZipCodeSameAll12Months)
                {
                    return GetZipCodeForMonth(1);
                }
                return "";
            }
            set
            {
                if (!value.IsNullOrWhiteSpace())
                {
                    for(int x = 1; x < 13; x++)
                    {
                        SetZipCodeForMonth(x, value);
                    }
                }
            }
        }
		public FCCollection Dependents
		{
			get
			{
				return collDependents;
			}
		}

		public bool EmployeeLowestPremiumSameForAll12Months
		{
			get
			{
				bool EmployeeLowestPremiumSameForAll12Months = false;
				int x;
				bool boolSame;
				boolSame = true;
				double dblTemp;
				dblTemp = GetEmployeeShareLowestPremiumForMonth(1);
				for (x = 2; x <= 12; x++)
				{
					if (GetEmployeeShareLowestPremiumForMonth(x) != dblTemp)
					{
						boolSame = false;
						break;
					}
				}
				// x
				EmployeeLowestPremiumSameForAll12Months = boolSame;
				return EmployeeLowestPremiumSameForAll12Months;
			}
		}
		// vbPorter upgrade warning: intMonth As object	OnWrite
		public void SetEmployeeShareLowestPremiumForMonth(int intMonth, double dblPremium)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				dblEmployeeShareLowestPremiumForMonth[intMonth] = dblPremium;
			}
		}

		public double GetEmployeeShareLowestPremiumForMonth(int intMonth)
		{
			double GetEmployeeShareLowestPremiumForMonth = 0;
			if (intMonth > 0 && intMonth < 13)
			{
				GetEmployeeShareLowestPremiumForMonth = dblEmployeeShareLowestPremiumForMonth[intMonth];
			}
			return GetEmployeeShareLowestPremiumForMonth;
		}

		public void SetSafeHarborCodeForMonth(int intMonth, string strCode)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				strSafeHarborCodeForMonth[intMonth] = strCode;
			}
		}

		public string GetSafeHarborCodeForMonth(int intMonth)
		{			
			if (intMonth.IsBetween(1,12))
			{
				return strSafeHarborCodeForMonth[intMonth];
			}
			return "";
		}

        public string GetZipCodeForMonth(int month)
        {
            if (month.IsBetween(1,12))
            {
                return zipCodesByMonth[month - 1];
            }
            return "";
        }

        public void SetZipCodeForMonth(int month, string code)
        {
            if (month.IsBetween(1,12))
            {
                zipCodesByMonth[month - 1] = code;
            }
        }

		public double EmployeeShareLowestPremiumAll12Months
		{
			set
			{
				if (value > 0)
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						SetEmployeeShareLowestPremiumForMonth(x, value);
					}
					// x
				}
			}
			get
			{
				double EmployeeShareLowestPremiumAll12Months = 0;
				int x;
				double lastPremium;
				lastPremium = GetEmployeeShareLowestPremiumForMonth(1);
				for (x = 2; x <= 12; x++)
				{
					if (GetEmployeeShareLowestPremiumForMonth(x) != lastPremium)
					{
						lastPremium = 0;
						break;
					}
				}
				// x
				EmployeeShareLowestPremiumAll12Months = lastPremium;
				return EmployeeShareLowestPremiumAll12Months;
			}
		}
	}
}
