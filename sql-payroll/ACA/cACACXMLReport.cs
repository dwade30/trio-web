﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cACACXMLReport
	{
		//=========================================================
		private string strFileName = string.Empty;
		private cGenericCollection headersList = new cGenericCollection();
		private string strManifestName = string.Empty;
		private c1094CXMLManifest xmlManifest = new c1094CXMLManifest();
		private int intReportYear;
		private DateTime dtTimeStamp;
		private string strTransmissionType = string.Empty;
		private bool boolIsCorrection;

		public bool IsCorrection
		{
			set
			{
				boolIsCorrection = value;
			}
			get
			{
				bool IsCorrection = false;
				IsCorrection = boolIsCorrection;
				return IsCorrection;
			}
		}

		private string TransmissionType
		{
			set
			{
				strTransmissionType = value;
			}
			get
			{
				string TransmissionType = "";
				TransmissionType = strTransmissionType;
				return TransmissionType;
			}
		}

		public DateTime TimeStamp
		{
			set
			{
				dtTimeStamp = value;
			}
			get
			{
				DateTime TimeStamp = System.DateTime.Now;
				TimeStamp = dtTimeStamp;
				return TimeStamp;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public cGenericCollection List1094s
		{
			get
			{
				cGenericCollection List1094s = null;
				List1094s = headersList;
				return List1094s;
			}
		}

		public string FileName
		{
			set
			{
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public string ManifestName
		{
			set
			{
				strManifestName = value;
			}
			get
			{
				string ManifestName = "";
				ManifestName = strManifestName;
				return ManifestName;
			}
		}

		public c1094CXMLManifest Manifest
		{
			get
			{
				c1094CXMLManifest Manifest = null;
				Manifest = xmlManifest;
				return Manifest;
			}
		}

		public cACACXMLReport() : base()
		{
			dtTimeStamp = DateTime.Now;
			strTransmissionType = "O";
		}
	}
}
