﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cACA2017TestController : IcACATestController
	{
		//=========================================================
		public cACABXMLReport GetTestScenario1(int intYear)
		{
			cACABXMLReport GetTestScenario1 = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario1Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "1-0";
			header.Employer.Name = "Hidetestone";
			header.Employer.Address = "975 Alder Lane Suite 312";
			header.Employer.City = "New York";
			header.Employer.State = "NY";
			header.Employer.EIN = "000000151";
			header.Employer.Telephone = "5551352468";
			header.Employer.Zip = "10023";
			header.BaseInfo = headerbase;
			header.Details = FillScenario1Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario1 = theReport;
			return GetTestScenario1;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario1Header(int intYear)
		{
			c1094B GetScenario1Header = null;
			c1094B header = new c1094B();
			header.FilerName = "Hidetestone";
			header.Address = "975 Alder lane Suite 312";
			header.City = "New York";
			header.ContactName = "Bertha";
			header.ContactLastName = "Logan";
			header.ContactPhone = "5551352468";
			header.EIN = "000000151";
			header.NumberOfSubmissions = 2;
			header.PostalCode = "10023";
			header.State = "NY";
			header.YearCovered = intYear;
			GetScenario1Header = header;
			return GetScenario1Header;
		}

		private c1095BXMLRecord GetScenario1_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario1_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "1-1";
			detxml.RecordID = 1;
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "D";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "4435 Chestnut Avenue";
			detxml.BaseInfo.City = "Madison";
			detxml.BaseInfo.PostalCode = "27025";
			// detXML.BaseInfo.ReportYear = intYear
			// detXML.BaseInfo.OriginOfPolicy = "D"
			detxml.BaseInfo.SSN = "000000101";
			detxml.BaseInfo.State = "NC";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "975 Alder Lane Suite 312";
			detxml.BaseInfo.CoverageProvider.City = "New York";
			detxml.BaseInfo.CoverageProvider.EIN = "000000151";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Hidetestone";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "10023";
			detxml.BaseInfo.CoverageProvider.State = "NY";
			detxml.BaseInfo.CoverageProvider.Telephone = "5551352468";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.CoveredAll12Months = true;
			covIndividual.FirstName = "Hanna";
			covIndividual.ID = 1;
			covIndividual.LastName = "Martin";
			// covIndividual.MiddleName = "Winnona"
			covIndividual.SSN = "000000101";
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.CoveredAll12Months = true;
			covIndividual.FirstName = "Isaias";
			covIndividual.ID = 2;
			covIndividual.LastName = "Martin";
			covIndividual.SSN = "000000102";
			empSetup.Dependents.Add(covIndividual);
			// Set detXML.Employee = New cEmployee
			detxml.Employee.Address1 = "4435 Chestnut Avenue";
			detxml.Employee.City = "Madison";
			detxml.Employee.State = "NC";
			detxml.Employee.Zip = "27025";
			detxml.Employee.Zip4 = "";
			detxml.Employee.SSN = "000000101";
			detxml.Employee.FirstName = "Hanna";
			// detxml.Employee.MiddleName = "Winnona"
			detxml.Employee.LastName = "Martin";
			GetScenario1_1Record = detxml;
			return GetScenario1_1Record;
		}

		private c1095BXMLRecord GetScenario1_2Record(int intYear)
		{
			c1095BXMLRecord GetScenario1_2Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "1-2";
			detxml.RecordID = 2;
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "002";
			empSetup.ID = 2;
			empSetup.OriginOfPolicy = "D";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "1313 Buckthorn Lane";
			detxml.BaseInfo.City = "Washington";
			detxml.BaseInfo.PostalCode = "20026";
			detxml.BaseInfo.SSN = "";
			detxml.BaseInfo.State = "DC";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "975 Alder Lane Suite 312";
			detxml.BaseInfo.CoverageProvider.City = "New York";
			detxml.BaseInfo.CoverageProvider.EIN = "000000151";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Hidetestone";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "10023";
			detxml.BaseInfo.CoverageProvider.State = "NY";
			detxml.BaseInfo.CoverageProvider.Telephone = "5551352468";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.FirstName = "Dolly";
			covIndividual.ID = 3;
			covIndividual.LastName = "Martinez";
			covIndividual.DateOfBirth = "1973-02-06";
			covIndividual.SetIsCoveredForMonth(6, true);
			covIndividual.SetIsCoveredForMonth(7, true);
			covIndividual.SetIsCoveredForMonth(8, true);
			covIndividual.SetIsCoveredForMonth(9, true);
			covIndividual.SetIsCoveredForMonth(10, true);
			covIndividual.SetIsCoveredForMonth(11, true);
			covIndividual.SetIsCoveredForMonth(12, true);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.FirstName = "Edward";
			covIndividual.ID = 4;
			covIndividual.LastName = "Martinez";
			covIndividual.SSN = "000000120";
			covIndividual.SetIsCoveredForMonth(6, true);
			covIndividual.SetIsCoveredForMonth(7, true);
			covIndividual.SetIsCoveredForMonth(8, true);
			covIndividual.SetIsCoveredForMonth(9, true);
			covIndividual.SetIsCoveredForMonth(10, true);
			covIndividual.SetIsCoveredForMonth(11, true);
			covIndividual.SetIsCoveredForMonth(12, true);
			empSetup.Dependents.Add(covIndividual);
			// Set detXML.Employee = New cEmployee
			detxml.Employee.Address1 = "1313 Buckthorn Lane";
			detxml.Employee.City = "Washington";
			detxml.Employee.State = "DC";
			detxml.Employee.Zip = "20026";
			detxml.Employee.Zip4 = "";
			detxml.Employee.SSN = "";
			detxml.Employee.BirthDate = "1973-02-06";
			detxml.Employee.FirstName = "Dolly";
			detxml.Employee.LastName = "Martinez";
			GetScenario1_2Record = detxml;
			return GetScenario1_2Record;
		}

		private cGenericCollection FillScenario1Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario1_2Record(intYear));
				listDetails.AddItem(GetScenario1_1Record(intYear));
			}
			return listDetails;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario2Header(int  intYear)
		{
			c1094B GetScenario2Header = null;
			c1094B header = new c1094B();
			header.FilerName = "Worktesttwo";
			header.Address = "2277 Holly Place";
			header.City = "Washington";
			header.State = "DC";
			header.ContactLastName = "Lincoln";
			header.ContactName = "Fred";
			header.ContactPhone = "5555372511";
			header.EIN = "000000215";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "20022";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario2Header = header;
			return GetScenario2Header;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario2CHeader(int  intYear)
		{
			c1094B GetScenario2CHeader = null;
			c1094B header = new c1094B();
			header.FilerName = "Worktesttwo";
			header.Address = "2277 Holly Place";
			header.City = "Washington";
			header.State = "DC";
			header.ContactLastName = "Lincoln";
			header.ContactName = "Fred";
			header.ContactPhone = "5555372511";
			header.EIN = "000000215";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "20022";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario2CHeader = header;
			return GetScenario2CHeader;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario6CHeader(int  intYear)
		{
			c1094B GetScenario6CHeader = null;
			c1094B header = new c1094B();
			header.FilerName = "Parktestsix Medicaid";
			header.Address = "65 Health Avenue";
			header.City = "Austin";
			header.State = "TX";
			header.ContactLastName = "Koop";
			header.ContactName = "Elias";
			header.ContactPhone = "5554052543";
			header.EIN = "000000631";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "78741";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario6CHeader = header;
			return GetScenario6CHeader;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario6Header(int  intYear)
		{
			c1094B GetScenario6Header = null;
			c1094B header = new c1094B();
			header.FilerName = "Parktestsix Medicaid";
			header.Address = "65 Health Avenue";
			header.City = "Austin";
			header.State = "TX";
			header.ContactLastName = "Koop";
			header.ContactName = "Elias";
			header.ContactPhone = "5554052543";
			header.EIN = "000000631";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "78741";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario6Header = header;
			return GetScenario6Header;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario8CHeader(int  intYear)
		{
			c1094B GetScenario8CHeader = null;
			c1094B header = new c1094B();
			header.FilerName = "Patttesteight Medicare";
			header.Address = "65 Willow Lane";
			header.City = "Baltimore";
			header.State = "MD";
			header.ContactLastName = "Santanova";
			header.ContactName = "Elizabeth";
			header.ContactPhone = "5556332273";
			header.EIN = "000000810";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "21244";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario8CHeader = header;
			return GetScenario8CHeader;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094B GetScenario8Header(int  intYear)
		{
			c1094B GetScenario8Header = null;
			c1094B header = new c1094B();
			header.FilerName = "Patttesteight Medicare";
			header.Address = "65 Willow Lane";
			header.City = "Baltimore";
			header.State = "MD";
			header.ContactLastName = "Santanova";
			header.ContactName = "Elizabeth";
			header.ContactPhone = "5556332273";
			header.EIN = "000000810";
			header.ID = 1;
			header.NumberOfSubmissions = 1;
			header.PostalCode = "21244";
			header.YearCovered = FCConvert.ToInt16(intYear);
			GetScenario8Header = header;
			return GetScenario8Header;
		}

		private c1094C GetScenario7Header(int intYear)
		{
			c1094C GetScenario7Header = null;
			c1094C header = new c1094C();
			header.EmployerName = "Carrtestseven";
			header.Address = "109 Cypress Cove";
			header.City = "Wimberley";
			header.State = "TX";
			header.PostalCode = "78676";
			header.EIN = "000000710";
			header.ContactName = "Carla";
			header.ContactLastName = "Hayes";
			header.ContactTelephone = "5551552899";
			header.Total1095CTransmitted = 1;
			header.IsAuthoritativeTransmittal = true;
			header.Total1095CForMember = 103;
			header.MemberOfAggregatedGroup = false;
			header.NinetyEightPercentOfferMethod = true;
			header.SetAll12MonthsMinimumCoverage(true);
			header.SetAll12MonthsTotalEmployeeCount(103);
			header.SetAll12MonthsAggregatedGroup(false);
			header.YearCovered = intYear;
			GetScenario7Header = header;
			return GetScenario7Header;
		}

		private c1094C GetScenario7CHeader(int intYear)
		{
			c1094C GetScenario7CHeader = null;
			c1094C header = new c1094C();
			header.EmployerName = "Carrtestseven";
			header.Address = "109 Cypress Cove";
			header.City = "Wimberley";
			header.State = "TX";
			header.PostalCode = "78676";
			header.EIN = "000000710";
			header.IsCorrected = true;
			header.ContactName = "Carla";
			header.ContactLastName = "Hayes";
			header.ContactTelephone = "5551552899";
			header.Total1095CTransmitted = 0;
			header.IsAuthoritativeTransmittal = true;
			header.Total1095CForMember = 110;
			header.MemberOfAggregatedGroup = false;
			header.NinetyEightPercentOfferMethod = true;
			header.SetAll12MonthsMinimumCoverage(true);
			header.SetAll12MonthsTotalEmployeeCount(110);
			header.SetAll12MonthsAggregatedGroup(false);
			header.YearCovered = intYear;
			string strReceiptID;
			strReceiptID = "";
			GetScenario7CHeader = header;
			return GetScenario7CHeader;
		}

		private c1094C GetScenario5Header(int intYear)
		{
			c1094C GetScenario5Header = null;
			c1094C header = new c1094C();
			header.EmployerName = "Darrtestfive";
			header.Address = "4689 Redwood Avenue";
			header.City = "Austin";
			header.State = "TX";
			header.PostalCode = "78755";
			header.EIN = "000000599";
			header.ContactName = "Susan";
			header.ContactLastName = "Williamson";
			header.ContactTelephone = "5551234567";
			header.Total1095CTransmitted = 2;
			header.IsAuthoritativeTransmittal = true;
			header.Total1095CForMember = 322;
			header.MemberOfAggregatedGroup = true;
			header.QualifyingOfferMethod = true;
			header.Section4980HRelief = false;
			header.SetAll12MonthsAggregatedGroup(true);
			header.SetAll12MonthsMinimumCoverage(true);
			header.SetAll12MonthsSection4980H("");
			header.YearCovered = intYear;
			header.SetFullTimeEmployeeCountForMonth(1, 315);
			header.SetFullTimeEmployeeCountForMonth(2, 316);
			header.SetFullTimeEmployeeCountForMonth(3, 316);
			header.SetFullTimeEmployeeCountForMonth(4, 316);
			header.SetFullTimeEmployeeCountForMonth(5, 316);
			header.SetFullTimeEmployeeCountForMonth(6, 316);
			header.SetFullTimeEmployeeCountForMonth(7, 318);
			header.SetFullTimeEmployeeCountForMonth(8, 318);
			header.SetFullTimeEmployeeCountForMonth(9, 318);
			header.SetFullTimeEmployeeCountForMonth(10, 318);
			header.SetFullTimeEmployeeCountForMonth(11, 318);
			header.SetFullTimeEmployeeCountForMonth(12, 318);
			header.SetTotalEmployeeCountForMonth(1, 330);
			header.SetTotalEmployeeCountForMonth(2, 335);
			header.SetTotalEmployeeCountForMonth(3, 335);
			header.SetTotalEmployeeCountForMonth(4, 335);
			header.SetTotalEmployeeCountForMonth(5, 335);
			header.SetTotalEmployeeCountForMonth(6, 335);
			header.SetTotalEmployeeCountForMonth(7, 335);
			header.SetTotalEmployeeCountForMonth(8, 333);
			header.SetTotalEmployeeCountForMonth(9, 333);
			header.SetTotalEmployeeCountForMonth(10, 333);
			header.SetTotalEmployeeCountForMonth(11, 333);
			header.SetTotalEmployeeCountForMonth(12, 333);
			cAggregatedMember aMember;
			aMember = new cAggregatedMember();
			aMember.EIN = "000000600";
			aMember.ID = 1;
			aMember.Name = "Darrtestfive Subsidiary One";
			header.AggregatedMembers.AddItem(aMember);
			GetScenario5Header = header;
			return GetScenario5Header;
		}

		private c1094C GetScenario5CHeader(int intYear)
		{
			c1094C GetScenario5CHeader = null;
			c1094C header = new c1094C();
			header.EmployerName = "Darrtestfive";
			header.Address = "4689 Redwood Avenue";
			header.City = "Austin";
			header.State = "TX";
			header.PostalCode = "78755";
			header.EIN = "000000599";
			header.ContactName = "Susan";
			header.ContactLastName = "Williamson";
			header.ContactTelephone = "5551234567";
			header.Total1095CTransmitted = 1;
			header.IsAuthoritativeTransmittal = false;
			header.Total1095CForMember = 0;
			header.MemberOfAggregatedGroup = false;
			// header.IsCorrected = True
			header.YearCovered = intYear;
			GetScenario5CHeader = header;
			return GetScenario5CHeader;
		}

		private c1094C GetScenario4Header(int intYear)
		{
			c1094C GetScenario4Header = null;
			c1094C header = new c1094C();
			header.EmployerName = "Gammtestfour County";
			header.Address = "2946 Pear Street";
			header.City = "West Bend";
			header.State = "WI";
			header.PostalCode = "53095";
			header.EIN = "000000401";
			header.ContactName = "Danny";
			header.ContactLastName = "Whitney";
			header.ContactTelephone = "5551452365";
			header.Total1095CTransmitted = 2;
			header.IsAuthoritativeTransmittal = false;
			header.YearCovered = intYear;
			header.DesignatedGovnernmentEntity = "Gammtestfour State Government";
			header.EntityAddress = "1155 Alder Avenue";
			header.EntityCity = "Madison";
			header.EntityState = "WI";
			header.EntityPostalCode = "53703";
			header.EntityEIN = "000000407";
			header.EntityContact = "Sam";
			header.EntityContactLast = "Castle";
			header.EntityTelephone = "5551115555";
			GetScenario4Header = header;
			return GetScenario4Header;
		}

		private c1094C GetScenario4CHeader(int intYear)
		{
			c1094C GetScenario4CHeader = null;
			c1094C header = new c1094C();
			header.EmployerName = "Gammtestfour County";
			header.Address = "2946 Pear Street";
			header.City = "West Bend";
			header.State = "WI";
			header.PostalCode = "53095";
			header.EIN = "000000401";
			header.ContactName = "Danny";
			header.ContactLastName = "Whitney";
			header.ContactTelephone = "5551452365";
			header.Total1095CTransmitted = 1;
			header.IsAuthoritativeTransmittal = false;
			header.YearCovered = intYear;
			header.DesignatedGovnernmentEntity = "Gammtestfour State Government";
			header.EntityAddress = "1155 Alder Avenue";
			header.EntityCity = "Madison";
			header.EntityState = "WI";
			header.EntityPostalCode = "53703";
			header.EntityEIN = "000000407";
			header.EntityContact = "Sam";
			header.EntityContactLast = "Castle";
			header.EntityTelephone = "5551115555";
			GetScenario4CHeader = header;
			return GetScenario4CHeader;
		}
		// vbPorter upgrade warning: intYear As object	OnWrite
		private c1094C GetScenario3Header(int intYear)
		{
			c1094C GetScenario3Header = null;
			c1094C header = new c1094C();
			header.EmployerName = "Selitestthree";
			header.Address = "6689 Willow Court";
			header.City = "Beverly Hills";
			header.State = "CA";
			header.PostalCode = "90211";
			header.EIN = "000000301";
			header.ContactName = "Rose";
			header.ContactLastName = "Lincoln";
			header.ContactTelephone = "5559876543";
			header.Total1095CTransmitted = 1;
			header.IsAuthoritativeTransmittal = true;
			header.Total1095CForMember = 455;
			header.MemberOfAggregatedGroup = true;
			header.Section4980HRelief = true;
			header.YearCovered = intYear;
			header.SetAll12MonthsMinimumCoverage(true);
			header.SetFullTimeEmployeeCountForMonth(1, 312);
			header.SetFullTimeEmployeeCountForMonth(2, 312);
			header.SetFullTimeEmployeeCountForMonth(3, 315);
			header.SetFullTimeEmployeeCountForMonth(4, 320);
			header.SetFullTimeEmployeeCountForMonth(5, 322);
			header.SetFullTimeEmployeeCountForMonth(6, 325);
			header.SetFullTimeEmployeeCountForMonth(7, 329);
			header.SetFullTimeEmployeeCountForMonth(8, 333);
			header.SetFullTimeEmployeeCountForMonth(9, 341);
			header.SetFullTimeEmployeeCountForMonth(10, 344);
			header.SetFullTimeEmployeeCountForMonth(11, 361);
			header.SetFullTimeEmployeeCountForMonth(12, 372);
			header.SetTotalEmployeeCountForMonth(1, 351);
			header.SetTotalEmployeeCountForMonth(2, 352);
			header.SetTotalEmployeeCountForMonth(3, 358);
			header.SetTotalEmployeeCountForMonth(4, 365);
			header.SetTotalEmployeeCountForMonth(5, 369);
			header.SetTotalEmployeeCountForMonth(6, 376);
			header.SetTotalEmployeeCountForMonth(7, 372);
			header.SetTotalEmployeeCountForMonth(8, 369);
			header.SetTotalEmployeeCountForMonth(9, 366);
			header.SetTotalEmployeeCountForMonth(10, 363);
			header.SetTotalEmployeeCountForMonth(11, 377);
			header.SetTotalEmployeeCountForMonth(12, 385);
			header.SetAll12MonthsAggregatedGroup(true);
			// Call header.SetAll12MonthsSection4980H("B")
			// Call header.Set4980HForMonth(1, "B")
			// Call header.Set4980HForMonth(2, "B")
			// Call header.Set4980HForMonth(3, "B")
			// Call header.Set4980HForMonth(4, "B")
			// Call header.Set4980HForMonth(5, "B")
			header.Set4980HForMonth(6, "");
			header.Set4980HForMonth(7, "");
			header.Set4980HForMonth(8, "");
			header.Set4980HForMonth(9, "");
			header.Set4980HForMonth(10, "");
			header.Set4980HForMonth(11, "");
			header.Set4980HForMonth(12, "");
			cAggregatedMember aMember;
			aMember = new cAggregatedMember();
			aMember.EIN = "000000302";
			aMember.ID = 1;
			aMember.Name = "Selitestthree Subsidiary One";
			header.AggregatedMembers.AddItem(aMember);
			aMember = new cAggregatedMember();
			aMember.EIN = "000000303";
			aMember.ID = 2;
			aMember.Name = "Selitestthree Subsidiary Two";
			header.AggregatedMembers.AddItem(aMember);
			GetScenario3Header = header;
			aMember = new cAggregatedMember();
			aMember.EIN = "000000304";
			aMember.Name = "Selitestthree Subsidiary Three";
			aMember.ID = 3;
			header.AggregatedMembers.AddItem(aMember);
			aMember = new cAggregatedMember();
			aMember.EIN = "000000305";
			aMember.Name = "Selitestthree Subsidiary Four";
			aMember.ID = 4;
			header.AggregatedMembers.AddItem(aMember);
			GetScenario3Header = header;
			return GetScenario3Header;
		}

		private cGenericCollection FillScenario2Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario2_1Record(intYear));
				// Call listDetails.AddItem(GetScenario2_2Record(intYear))
				// Call listDetails.AddItem(GetScenario2_3Record(intYear))
			}
			return listDetails;
		}

		private cGenericCollection FillScenario2CDetails(cGenericCollection listDetails, int intYear)
		{
			object FillScenario2CDetails = null;
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario2C_1Record(intYear));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario6CDetails(cGenericCollection listDetails, int intYear)
		{
			object FillScenario6CDetails = null;
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario6C_1Record(intYear));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario6Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				// Call listDetails.AddItem(GetScenario6_2Record(intYear))
				listDetails.AddItem(GetScenario6_1Record(intYear));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario8CDetails(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario8C_1Record(intYear));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario8Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario8_1Record(intYear));
				// Call listDetails.AddItem(GetScenario8_2Record(intYear))
			}
			return listDetails;
		}

		private cGenericCollection FillScenario3Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario3_1Record(intYear, 1));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario4Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario4_1Record(intYear, 1));
				listDetails.AddItem(GetScenario4_2Record(intYear, 2));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario4CDetails(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario4C_1Record(intYear, 1));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario5Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario5_1Record(intYear, 1));
				listDetails.AddItem(GetScenario5_2Record(intYear, 2));
				// Call listDetails.AddItem(GetScenario5_3Record(intYear, 3))
			}
			return listDetails;
		}

		private cGenericCollection FillScenario5CDetails(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario5C_2Record(intYear, 1));
			}
			return listDetails;
		}

		private cGenericCollection FillScenario7Details(cGenericCollection listDetails, int intYear)
		{
			if (!(listDetails == null))
			{
				listDetails.AddItem(GetScenario7_1Record(intYear, 1));
			}
			return listDetails;
		}

		private c1095CXMLRecord GetScenario7_1Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario7_1Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "7-1";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1C";
			empSetup.EmployeeShareLowestPremiumAll12Months = 115;
			empSetup.SafeHarborCodeAll12Months = "2C";
			detxml.BaseInfo.Address = "420 Falcon Lane";
			detxml.BaseInfo.City = "San Juan Capistrano";
			detxml.BaseInfo.State = "CA";
			detxml.BaseInfo.PostalCode = "92693";
			detxml.BaseInfo.SSN = "000000701";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "420 Falcon Lane";
			detxml.Employee.City = "San Juan Capistrano";
			detxml.Employee.State = "CA";
			detxml.Employee.Zip = "92693";
			detxml.Employee.SSN = "000000701";
			detxml.Employee.FirstName = "Scarlett";
			detxml.Employee.LastName = "Camen";
			GetScenario7_1Record = detxml;
			return GetScenario7_1Record;
		}

		private c1095CXMLRecord GetScenario5_2Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario5_2Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "5-2";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "003";
			empSetup.ID = 2;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1G";
			detxml.BaseInfo.Address = "847 Walnut Avenue";
			detxml.BaseInfo.City = "Roy";
			detxml.BaseInfo.State = "NM";
			detxml.BaseInfo.PostalCode = "877743";
			detxml.BaseInfo.SSN = "000000577";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "847 Walnut Avenue";
			detxml.Employee.City = "Roy";
			detxml.Employee.State = "NM";
			detxml.Employee.Zip = "87743";
			detxml.Employee.SSN = "000000577";
			detxml.Employee.FirstName = "Rose";
			detxml.Employee.LastName = "Davichi";
			cACAEmployeeDependent covIndividual;
			covIndividual = GetNewDependent("Erica", "", "Davichi", "", "", "2005-12-05");
			covIndividual.SetIsCoveredForMonth(7, true);
			covIndividual.SetIsCoveredForMonth(8, true);
			covIndividual.SetIsCoveredForMonth(9, true);
			covIndividual.SetIsCoveredForMonth(10, true);
			covIndividual.SetIsCoveredForMonth(11, true);
			covIndividual.SetIsCoveredForMonth(12, true);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Omar", "", "Davichi", "", "000000578", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Rose", "", "Davichi", "", "000000577", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Same", "", "Davichi", "", "000000579", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			GetScenario5_2Record = detxml;
			return GetScenario5_2Record;
		}

		private c1095CXMLRecord GetScenario5C_2Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario5C_2Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "5C-2";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			detxml.BaseInfo.Corrected = true;
			string strReceiptID;
			strReceiptID = "1095C-16-00060349";
			strReceiptID += "|1|2";
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			detxml.CorrectedUniqueID = strReceiptID;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "003";
			empSetup.ID = 2;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1G";
			detxml.BaseInfo.Address = "847 Walnut Avenue";
			detxml.BaseInfo.City = "Roy";
			detxml.BaseInfo.State = "NM";
			detxml.BaseInfo.PostalCode = "87743";
			detxml.BaseInfo.SSN = "000000577";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "847 Walnut Avenue";
			detxml.Employee.City = "Roy";
			detxml.Employee.State = "NM";
			detxml.Employee.Zip = "87743";
			detxml.Employee.SSN = "000000577";
			detxml.Employee.FirstName = "Rose";
			detxml.Employee.LastName = "Davichi";
			cACAEmployeeDependent covIndividual;
			covIndividual = GetNewDependent("Rose", "", "Davichi", "", "000000577", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Erica", "", "Davichi", "", "", "2006-12-05");
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(6, false);
			covIndividual.SetIsCoveredForMonth(7, false);
			covIndividual.SetIsCoveredForMonth(8, false);
			covIndividual.SetIsCoveredForMonth(9, false);
			covIndividual.SetIsCoveredForMonth(10, false);
			covIndividual.SetIsCoveredForMonth(11, false);
			covIndividual.SetIsCoveredForMonth(12, false);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Omar", "", "Davichi", "", "000000578", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Sam", "", "Davichi", "", "000000579", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			GetScenario5C_2Record = detxml;
			return GetScenario5C_2Record;
		}
		// Private Function GetScenario5_2Record(ByVal intYear As Integer, ByVal intRecordID As Integer) As c1095CXMLRecord
		// Dim detxml As New c1095CXMLRecord
		// detxml.ScenarioID = "5-2"
		// detxml.RecordID = intRecordID
		// Set detxml.BaseInfo = New c1095C
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 2
		// empSetup.EmployeeNumber = "002"
		// empSetup.ID = 2
		// empSetup.ReportYear = intYear
		// empSetup.CoverageRequiredCodeAll12Months = "1A"
		// detxml.BaseInfo.Address = "5991 Sycamore Lane"
		// detxml.BaseInfo.City = "Sandy"
		// detxml.BaseInfo.State = "UT"
		// detxml.BaseInfo.PostalCode = "84094"
		// detxml.BaseInfo.SSN = "000000581"
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.PlanStartMonth = 1
		// detxml.Employee.Address1 = "5991 Sycamore Lane"
		// detxml.Employee.City = "Sandy"
		// detxml.Employee.State = "UT"
		// detxml.Employee.Zip = "84094"
		// detxml.Employee.SSN = "000000581"
		// detxml.Employee.FirstName = "Peter"
		// detxml.Employee.LastName = "Davignon"
		//
		// Dim covIndividual As cACAEmployeeDependent
		// Set covIndividual = GetNewDependent("Peter", "", "Davignon", "", "000000581", "")
		// covIndividual.CoveredAll12Months = True
		// Call empSetup.Dependents.add(covIndividual)
		//
		// Set covIndividual = GetNewDependent("Rene", "", "Davignon", "", "000000585", "")
		// covIndividual.CoveredAll12Months = True
		// Call empSetup.Dependents.add(covIndividual)
		//
		// Set covIndividual = GetNewDependent("Sally", "", "Davignon", "", "000000583", "")
		// covIndividual.CoveredAll12Months = True
		// Call empSetup.Dependents.add(covIndividual)
		//
		// Set covIndividual = GetNewDependent("Teddy", "", "Davignon", "", "000000589", "")
		// Call covIndividual.SetIsCoveredForMonth(6, True)
		// Call covIndividual.SetIsCoveredForMonth(7, True)
		// Call covIndividual.SetIsCoveredForMonth(8, True)
		// Call covIndividual.SetIsCoveredForMonth(9, True)
		// Call covIndividual.SetIsCoveredForMonth(10, True)
		// Call covIndividual.SetIsCoveredForMonth(11, True)
		// Call covIndividual.SetIsCoveredForMonth(12, True)
		// Call empSetup.Dependents.add(covIndividual)
		//
		// Set GetScenario5_2Record = detxml
		// End Function
		private c1095CXMLRecord GetScenario5_1Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario5_1Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "5-1";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1A";
			detxml.BaseInfo.Address = "2993 Spruce Lane";
			detxml.BaseInfo.City = "Fort Collins";
			detxml.BaseInfo.State = "CO";
			detxml.BaseInfo.PostalCode = "80522";
			detxml.BaseInfo.SSN = "000000533";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "2993 Spruce Lane";
			detxml.Employee.City = "Fort Collins";
			detxml.Employee.State = "CO";
			detxml.Employee.Zip = "80522";
			detxml.Employee.SSN = "000000533";
			detxml.Employee.FirstName = "Odette";
			detxml.Employee.MiddleName = "Cloudy";
			detxml.Employee.LastName = "Davidson";
			cACAEmployeeDependent covIndividual;
			covIndividual = GetNewDependent("Mindy", "", "Davidson", "", "000000534", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Nicolas", "", "Davidson", "", "000000535", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Odette", "", "Davidson", "", "000000533", "");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = GetNewDependent("Peter", "", "Davidson", "", "", "02/06/1970");
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			GetScenario5_1Record = detxml;
			return GetScenario5_1Record;
		}

		private cACAEmployeeDependent GetNewDependent(string strFirstName, string strMiddleName, string strLastName, string strSuffix, string strSSN, string strBirthDate)
		{
			cACAEmployeeDependent GetNewDependent = null;
			cACAEmployeeDependent covIndividual = new cACAEmployeeDependent();
			covIndividual.FirstName = strFirstName;
			covIndividual.MiddleName = strMiddleName;
			covIndividual.LastName = strLastName;
			covIndividual.Suffix = strSuffix;
			covIndividual.SSN = strSSN;
			covIndividual.DateOfBirth = strBirthDate;
			GetNewDependent = covIndividual;
			return GetNewDependent;
		}

		private c1095CXMLRecord GetScenario4_1Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario4_1Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "4-1";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1C";
			empSetup.EmployeeShareLowestPremiumAll12Months = 105;
			empSetup.SafeHarborCodeAll12Months = "2C";
			detxml.BaseInfo.Address = "1919 Pine Avenue";
			detxml.BaseInfo.City = "Germantown";
			detxml.BaseInfo.State = "WI";
			detxml.BaseInfo.PostalCode = "53022";
			detxml.BaseInfo.SSN = "000000411";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "1919 Pine Avenue";
			detxml.Employee.City = "Germantown";
			detxml.Employee.State = "WI";
			detxml.Employee.Zip = "53022";
			detxml.Employee.SSN = "000000411";
			detxml.Employee.FirstName = "Erika";
			detxml.Employee.LastName = "Gaviton";
			GetScenario4_1Record = detxml;
			return GetScenario4_1Record;
		}

		private c1095CXMLRecord GetScenario4C_1Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario4C_1Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "4C-1";
			detxml.RecordID = intRecordID;
			string strUniqueID;
			strUniqueID = "1095C-16-00060348";
			strUniqueID += "|1|1";
			detxml.CorrectedUniqueID = strUniqueID;
			detxml.BaseInfo = new c1095C();
			detxml.BaseInfo.Corrected = true;
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1C";
			empSetup.EmployeeShareLowestPremiumAll12Months = 108;
			empSetup.SafeHarborCodeAll12Months = "2C";
			detxml.BaseInfo.Address = "1919 Pine Avenue";
			detxml.BaseInfo.City = "Germantown";
			detxml.BaseInfo.State = "WI";
			detxml.BaseInfo.PostalCode = "53022";
			detxml.BaseInfo.SSN = "000000411";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "1919 Pine Avenue";
			detxml.Employee.City = "Germantown";
			detxml.Employee.State = "WI";
			detxml.Employee.Zip = "53022";
			detxml.Employee.SSN = "000000411";
			detxml.Employee.FirstName = "Erika";
			detxml.Employee.LastName = "Gaviton";
			GetScenario4C_1Record = detxml;
			return GetScenario4C_1Record;
		}

		private c1095CXMLRecord GetScenario4_2Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario4_2Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "4-2";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "002";
			empSetup.ID = 2;
			empSetup.ReportYear = intYear;
			empSetup.CoverageRequiredCodeAll12Months = "1E";
			empSetup.SetCoverageRequiredCodeForMonth(8, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(9, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(10, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(11, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(12, "1H");
			empSetup.SetEmployeeShareLowestPremiumForMonth(1, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(2, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(3, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(4, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(5, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(6, 152);
			empSetup.SetEmployeeShareLowestPremiumForMonth(7, 152);
			empSetup.SafeHarborCodeAll12Months = "2C";
			empSetup.SetSafeHarborCodeForMonth(8, "2A");
			empSetup.SetSafeHarborCodeForMonth(9, "2A");
			empSetup.SetSafeHarborCodeForMonth(10, "2A");
			empSetup.SetSafeHarborCodeForMonth(11, "2A");
			empSetup.SetSafeHarborCodeForMonth(12, "2A");
			detxml.BaseInfo.Address = "2845 Plum Street";
			detxml.BaseInfo.City = "West Bend";
			detxml.BaseInfo.State = "WI";
			detxml.BaseInfo.PostalCode = "53095";
			detxml.BaseInfo.SSN = "000000422";
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.PlanStartMonth = 1;
			detxml.Employee.Address1 = "2845 Plum Street";
			detxml.Employee.City = "West Bend";
			detxml.Employee.State = "WI";
			detxml.Employee.Zip = "53095";
			detxml.Employee.SSN = "000000422";
			detxml.Employee.FirstName = "Ida";
			detxml.Employee.LastName = "Gavitas";
			GetScenario4_2Record = detxml;
			return GetScenario4_2Record;
		}
		// Private Function GetScenario4_3Record(ByVal intYear As Integer, ByVal intRecordID As Integer) As c1095CXMLRecord
		// Dim detxml As New c1095CXMLRecord
		// detxml.ScenarioID = "4-3"
		// detxml.RecordID = intRecordID
		// Set detxml.BaseInfo = New c1095C
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 3
		// empSetup.EmployeeNumber = "003"
		// empSetup.ID = 3
		// empSetup.ReportYear = intYear
		// empSetup.CoverageRequiredCodeAll12Months = "1H"
		// Call empSetup.SetCoverageRequiredCodeForMonth(8, "1C")
		// Call empSetup.SetCoverageRequiredCodeForMonth(9, "1C")
		// Call empSetup.SetCoverageRequiredCodeForMonth(10, "1C")
		// Call empSetup.SetCoverageRequiredCodeForMonth(11, "1C")
		// Call empSetup.SetCoverageRequiredCodeForMonth(12, "1C")
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(8, 205)
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(9, 205)
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(10, 205)
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(11, 205)
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(12, 205)
		// empSetup.SafeHarborCodeAll12Months = "2A"
		// Call empSetup.SetSafeHarborCodeForMonth(8, "2C")
		// Call empSetup.SetSafeHarborCodeForMonth(9, "2C")
		// Call empSetup.SetSafeHarborCodeForMonth(10, "2C")
		// Call empSetup.SetSafeHarborCodeForMonth(11, "2C")
		// Call empSetup.SetSafeHarborCodeForMonth(12, "2C")
		// detxml.BaseInfo.Address = "2546 Red Cedar Lane"
		// detxml.BaseInfo.City = "Germantown"
		// detxml.BaseInfo.State = "WI"
		// detxml.BaseInfo.PostalCode = "53022"
		// detxml.BaseInfo.SSN = "000000433"
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.PlanStartMonth = 1
		// detxml.Employee.Address1 = "2546 Red Cedar Lane"
		// detxml.Employee.City = "Germantown"
		// detxml.Employee.State = "WI"
		// detxml.Employee.Zip = "53022"
		// detxml.Employee.SSN = "000000433"
		// detxml.Employee.FirstName = "Larry"
		// detxml.Employee.LastName = "Gavizonlas"
		// Set GetScenario4_3Record = detxml
		// End Function
		//
		// Private Function GetScenario4_4Record(ByVal intYear As Integer, ByVal intRecordID As Integer) As c1095CXMLRecord
		// Dim detxml As New c1095CXMLRecord
		// detxml.ScenarioID = "4-4"
		// detxml.RecordID = intRecordID
		// Set detxml.BaseInfo = New c1095C
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 4
		// empSetup.EmployeeNumber = "004"
		// empSetup.ID = 4
		// empSetup.ReportYear = intYear
		// empSetup.CoverageRequiredCodeAll12Months = "1E"
		// Call empSetup.SetCoverageRequiredCodeForMonth(1, "1H")
		// empSetup.EmployeeShareLowestPremiumAll12Months = 205
		// Call empSetup.SetEmployeeShareLowestPremiumForMonth(1, 0)
		// empSetup.SafeHarborCodeAll12Months = "2C"
		// Call empSetup.SetSafeHarborCodeForMonth(1, "2A")
		// detxml.BaseInfo.Address = "355 Maple Lane"
		// detxml.BaseInfo.City = "West Bend"
		// detxml.BaseInfo.State = "WI"
		// detxml.BaseInfo.PostalCode = "53095"
		// detxml.BaseInfo.SSN = "000000444"
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.PlanStartMonth = 1
		// detxml.Employee.Address1 = "355 Maple Lane"
		// detxml.Employee.City = "West Bend"
		// detxml.Employee.State = "WI"
		// detxml.Employee.Zip = "53095"
		// detxml.Employee.FirstName = "Kyle"
		// detxml.Employee.LastName = "Gaviblont"
		// Set GetScenario4_4Record = detxml
		// End Function
		private c1095CXMLRecord GetScenario3_1Record(int intYear, int intRecordID)
		{
			c1095CXMLRecord GetScenario3_1Record = null;
			c1095CXMLRecord detxml = new c1095CXMLRecord();
			detxml.ScenarioID = "3-1";
			detxml.RecordID = intRecordID;
			detxml.BaseInfo = new c1095C();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.ReportYear = intYear;
			empSetup.SetCoverageRequiredCodeForMonth(1, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(2, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(3, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(4, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(5, "1H");
			empSetup.SetCoverageRequiredCodeForMonth(6, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(7, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(8, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(9, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(10, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(11, "1E");
			empSetup.SetCoverageRequiredCodeForMonth(12, "1E");
			empSetup.SetEmployeeShareLowestPremiumForMonth(6, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(7, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(8, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(9, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(10, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(11, 139);
			empSetup.SetEmployeeShareLowestPremiumForMonth(12, 139);
			// Call empSetup.SetSafeHarborCodeForMonth(6, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(7, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(8, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(9, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(10, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(11, "2C")
			// Call empSetup.SetSafeHarborCodeForMonth(12, "2C")
			detxml.BaseInfo.Address = "342 Ash Avenue";
			detxml.BaseInfo.City = "Seattle";
			detxml.BaseInfo.PostalCode = "98104";
			detxml.BaseInfo.State = "WA";
			detxml.BaseInfo.SSN = "000000350";
			// detXML.BaseInfo.CoverageProviderID = 1
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.PlanMonth = 6;
			detxml.BaseInfo.PlanStartMonth = 6;
			detxml.Employee.Address1 = "342 Ash Avenue";
			detxml.Employee.SSN = "000000350";
			detxml.Employee.City = "Seattle";
			detxml.Employee.State = "WA";
			detxml.Employee.Zip = "98104";
			detxml.Employee.FirstName = "Teresa";
			detxml.Employee.LastName = "Southern";
			GetScenario3_1Record = detxml;
			return GetScenario3_1Record;
		}

		private c1095BXMLRecord GetScenario8_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario8_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "8-1";
			detxml.RecordID = FCConvert.ToInt32("1");
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "C";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "1724 Hurst Street";
			detxml.BaseInfo.City = "San Marcos";
			detxml.BaseInfo.PostalCode = "78666";
			detxml.BaseInfo.SSN = "000000821";
			detxml.BaseInfo.State = "TX";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "65 Willow Lane";
			detxml.BaseInfo.CoverageProvider.City = "Baltimore";
			detxml.BaseInfo.CoverageProvider.EIN = "000000810";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Patttesteight Medicare";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "21244";
			detxml.BaseInfo.CoverageProvider.State = "MD";
			detxml.BaseInfo.CoverageProvider.Telephone = "5556332273";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Kathy";
			// covIndividual.MiddleName = "Noble"
			covIndividual.LastName = "Jones";
			covIndividual.ID = 1;
			covIndividual.SSN = "000000821";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(1, false);
			covIndividual.SetIsCoveredForMonth(2, false);
			covIndividual.FirstName = "John";
			covIndividual.LastName = "Jones";
			covIndividual.SSN = "000000829";
			covIndividual.ID = 2;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.CoveredAll12Months = true;
			covIndividual.FirstName = "Grace";
			covIndividual.LastName = "Jones";
			covIndividual.ID = 3;
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "1724 Hurst Street";
			detxml.Employee.City = "San Marcos";
			detxml.Employee.State = "TX";
			detxml.Employee.Zip = "78666";
			detxml.Employee.SSN = "000000821";
			detxml.Employee.FirstName = "Kathy";
			// detxml.Employee.MiddleName = "Noble"
			detxml.Employee.LastName = "Jones";
			GetScenario8_1Record = detxml;
			return GetScenario8_1Record;
		}

		private c1095BXMLRecord GetScenario8C_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario8C_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "8C-1";
			detxml.RecordID = FCConvert.ToInt32("1");
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "002";
			empSetup.ID = 2;
			empSetup.OriginOfPolicy = "C";
			empSetup.ReportYear = intYear;
			string strReceiptID;
			strReceiptID = "1095B-16-00030025";
			detxml.CorrectedUniqueID = strReceiptID + "|1|2";
			detxml.BaseInfo.Corrected = true;
			detxml.BaseInfo.Address = "1724 Hurst Street";
			detxml.BaseInfo.City = "San Marcos";
			detxml.BaseInfo.PostalCode = "74467";
			detxml.BaseInfo.SSN = "000000821";
			detxml.BaseInfo.State = "TX";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "65 Willow Lane";
			detxml.BaseInfo.CoverageProvider.City = "Baltimore";
			detxml.BaseInfo.CoverageProvider.State = "MD";
			detxml.BaseInfo.CoverageProvider.EIN = "000000810";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Patttesteight Medicare";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "21244";
			detxml.BaseInfo.CoverageProvider.Telephone = "5556332273";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.FirstName = "Kathy";
			covIndividual.LastName = "Jones";
			covIndividual.ID = 3;
			covIndividual.SSN = "000-00-0821";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.FirstName = "John";
			covIndividual.LastName = "Jones";
			covIndividual.SSN = "000-00-0829";
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(1, false);
			covIndividual.SetIsCoveredForMonth(2, false);
			covIndividual.SetIsCoveredForMonth(3, false);
			covIndividual.SetIsCoveredForMonth(4, false);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.CoveredAll12Months = true;
			covIndividual.FirstName = "Grace";
			covIndividual.LastName = "Jones";
			covIndividual.DateOfBirth = "2014-03-01";
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "1724 Hurst Street";
			detxml.Employee.City = "San Marcos";
			detxml.Employee.State = "TX";
			detxml.Employee.Zip = "78666";
			detxml.Employee.SSN = "000000821";
			detxml.Employee.FirstName = "Kathy";
			detxml.Employee.LastName = "Jones";
			GetScenario8C_1Record = detxml;
			return GetScenario8C_1Record;
		}

		private c1095BXMLRecord GetScenario8_2Record(int intYear)
		{
			c1095BXMLRecord GetScenario8_2Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "8-2";
			detxml.RecordID = FCConvert.ToInt32("2");
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 2;
			empSetup.EmployeeNumber = "002";
			empSetup.ID = 2;
			empSetup.OriginOfPolicy = "C";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "1963 Hardin Avenue";
			detxml.BaseInfo.City = "Wagoner";
			detxml.BaseInfo.PostalCode = "74467";
			detxml.BaseInfo.SSN = "000000829";
			detxml.BaseInfo.State = "OK";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "65 Willow Lane";
			detxml.BaseInfo.CoverageProvider.City = "Baltimore";
			detxml.BaseInfo.CoverageProvider.State = "MD";
			detxml.BaseInfo.CoverageProvider.EIN = "000000810";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Patttesteight Medicare";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "21244";
			detxml.BaseInfo.CoverageProvider.Telephone = "5556332273";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 2;
			covIndividual.FirstName = "Sarah";
			covIndividual.LastName = "Jonezin";
			covIndividual.ID = 3;
			covIndividual.SSN = "000000829";
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(1, false);
			covIndividual.SetIsCoveredForMonth(2, false);
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "1963 Hardin Avenue";
			detxml.Employee.City = "Wagoner";
			detxml.Employee.State = "OK";
			detxml.Employee.Zip = "74467";
			detxml.Employee.SSN = "000000829";
			detxml.Employee.FirstName = "Sarah";
			detxml.Employee.LastName = "Jonezin";
			GetScenario8_2Record = detxml;
			return GetScenario8_2Record;
		}

		private c1095BXMLRecord GetScenario6_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario6_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "6-1";
			detxml.RecordID = FCConvert.ToInt32("2");
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "C";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "1724 Hurst Street";
			detxml.BaseInfo.City = "San Marcos";
			detxml.BaseInfo.PostalCode = "78666";
			detxml.BaseInfo.SSN = "000000601";
			detxml.BaseInfo.State = "TX";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "65 Health Avenue";
			detxml.BaseInfo.CoverageProvider.City = "Austin";
			detxml.BaseInfo.CoverageProvider.EIN = "000000631";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Parktestsix Medicaid";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "78741";
			detxml.BaseInfo.CoverageProvider.State = "TX";
			detxml.BaseInfo.CoverageProvider.Telephone = "5554052543";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Maria";
			// covIndividual.MiddleName = "Candice"
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 1;
			covIndividual.SSN = "000000601";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Jane";
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 2;
			// covIndividual.SSN = "000000603"
			covIndividual.DateOfBirth = "2016-11-05";
			covIndividual.SetIsCoveredForMonth(11, true);
			covIndividual.SetIsCoveredForMonth(12, true);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Max";
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 3;
			covIndividual.SSN = "000000602";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "1724 Hurst Street";
			detxml.Employee.City = "San Marcos";
			detxml.Employee.State = "TX";
			detxml.Employee.Zip = "78666";
			detxml.Employee.Zip4 = "";
			detxml.Employee.SSN = "000000601";
			detxml.Employee.FirstName = "Maria";
			detxml.Employee.LastName = "Nichols";
			// detxml.Employee.MiddleName = "Candice"
			GetScenario6_1Record = detxml;
			return GetScenario6_1Record;
		}

		private c1095BXMLRecord GetScenario6C_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario6C_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "6C-1";
			detxml.RecordID = FCConvert.ToInt32("1");
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			string strReceiptID;
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "001";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "C";
			empSetup.ReportYear = intYear;
			strReceiptID = "1095B-16-00030213";
			detxml.CorrectedUniqueID = strReceiptID + "|1|2";
			detxml.BaseInfo.Corrected = true;
			detxml.BaseInfo.Address = "1724 Hurst Street";
			detxml.BaseInfo.City = "San Marcos";
			detxml.BaseInfo.PostalCode = "78666";
			detxml.BaseInfo.SSN = "000000601";
			detxml.BaseInfo.State = "TX";
			detxml.BaseInfo.EmployerSponsoredCoverage = false;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "65 Health Avenue";
			detxml.BaseInfo.CoverageProvider.City = "Austin";
			detxml.BaseInfo.CoverageProvider.EIN = "000000631";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Parktestsix Medicaid";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "78741";
			detxml.BaseInfo.CoverageProvider.State = "TX";
			detxml.BaseInfo.CoverageProvider.Telephone = "5554052543";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Maria";
			// covIndividual.MiddleName = "Candice"
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 1;
			covIndividual.SSN = "000000601";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Jane";
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 2;
			// covIndividual.SSN = "000000603"
			covIndividual.DateOfBirth = "2016-09-05";
			covIndividual.SetIsCoveredForMonth(9, true);
			covIndividual.SetIsCoveredForMonth(10, true);
			covIndividual.SetIsCoveredForMonth(11, true);
			covIndividual.SetIsCoveredForMonth(12, true);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Max";
			covIndividual.LastName = "Nichols";
			covIndividual.ID = 3;
			covIndividual.SSN = "000000602";
			covIndividual.CoveredAll12Months = true;
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "1724 Hurst Street";
			detxml.Employee.City = "San Marcos";
			detxml.Employee.State = "TX";
			detxml.Employee.Zip = "78666";
			detxml.Employee.Zip4 = "";
			detxml.Employee.SSN = "000000601";
			detxml.Employee.FirstName = "Maria";
			detxml.Employee.LastName = "Nichols";
			// detxml.Employee.MiddleName = "Candice"
			GetScenario6C_1Record = detxml;
			return GetScenario6C_1Record;
		}
		// Private Function GetScenario6_2Record(ByVal intYear As Integer) As c1095BXMLRecord
		// Dim detxml As New c1095BXMLRecord
		// detxml.ScenarioID = "6-2"
		// detxml.RecordID = 1
		//
		// Set detxml.BaseInfo = New c1095B
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 2
		// empSetup.EmployeeNumber = "002"
		// empSetup.ID = 2
		// empSetup.OriginOfPolicy = "C"
		// empSetup.ReportYear = intYear
		//
		// detxml.BaseInfo.Corrected = True
		// detxml.BaseInfo.Address = "1963 Hardin Avenue"
		// detxml.BaseInfo.City = "Dallas"
		// detxml.BaseInfo.PostalCode = "75201"
		// detxml.BaseInfo.SSN = "000000604"
		// detxml.BaseInfo.State = "TX"
		// detxml.BaseInfo.EmployerSponsoredCoverage = False
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.Address = "65 Health Avenue"
		// detxml.BaseInfo.CoverageProvider.City = "Austin"
		// detxml.BaseInfo.CoverageProvider.EIN = "000000631"
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.Name = "Parktestsix Medicaid"
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.CoverageProvider.PostalCode = "78741"
		// detxml.BaseInfo.CoverageProvider.State = "TX"
		// detxml.BaseInfo.CoverageProvider.Telephone = "5554052543"
		// detxml.BaseInfo.CoverageProvider.Use1095B = True
		//
		// Dim covIndividual As New cACAEmployeeDependent
		// covIndividual.ACAEmployeeSetupID = 2
		// covIndividual.FirstName = "Ben"
		// covIndividual.LastName = "Nichas"
		// covIndividual.SSN = "000000604"
		// covIndividual.ID = 4
		// covIndividual.CoveredAll12Months = True
		// Call covIndividual.SetIsCoveredForMonth(1, False)
		// Call covIndividual.SetIsCoveredForMonth(2, False)
		// Call empSetup.Dependents.add(covIndividual)
		//
		// detxml.Employee.Address1 = "1963 Hardin Avenue"
		// detxml.Employee.City = "Dallas"
		// detxml.Employee.State = "TX"
		// detxml.Employee.Zip = "75201"
		// detxml.Employee.SSN = "000000604"
		// detxml.Employee.FirstName = "Ben"
		// detxml.Employee.LastName = "Nichas"
		//
		// Set GetScenario6_2Record = detxml
		// End Function
		// Private Function GetScenario2_1Record(ByVal intYear As Integer) As c1095BXMLRecord
		// Dim detxml As New c1095BXMLRecord
		// detxml.ScenarioID = "2-1"
		// detxml.RecordID = 1
		//
		// Set detxml.BaseInfo = New c1095B
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 1
		// empSetup.EmployeeNumber = "001"
		// empSetup.ID = 1
		// empSetup.OriginOfPolicy = "D"
		// empSetup.ReportYear = intYear
		//
		// detxml.BaseInfo.Address = "4639 Fir Street"
		// detxml.BaseInfo.City = "Lake City"
		// detxml.BaseInfo.PostalCode = "32055"
		// detxml.BaseInfo.SSN = "000000207"
		// detxml.BaseInfo.State = "FL"
		// detxml.BaseInfo.EmployerSponsoredCoverage = False
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.Address = "2277 Holly Place"
		// detxml.BaseInfo.CoverageProvider.City = "Washington"
		// detxml.BaseInfo.CoverageProvider.EIN = "000000215"
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.Name = "Worktesttwo"
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.CoverageProvider.PostalCode = "20022"
		// detxml.BaseInfo.CoverageProvider.State = "DC"
		// detxml.BaseInfo.CoverageProvider.Telephone = "5555372511"
		// detxml.BaseInfo.CoverageProvider.Use1095B = True
		//
		// Dim covIndividual As cACAEmployeeDependent
		// Set covIndividual = New cACAEmployeeDependent
		// covIndividual.ACAEmployeeSetupID = 1
		// covIndividual.FirstName = "Marco"
		// covIndividual.LastName = "Williams"
		// covIndividual.ID = 1
		// covIndividual.SSN = "000000207"
		// covIndividual.CoveredAll12Months = True
		// Call empSetup.Dependents.add(covIndividual)
		// Set covIndividual = New cACAEmployeeDependent
		// covIndividual.ACAEmployeeSetupID = 1
		// covIndividual.CoveredAll12Months = True
		// covIndividual.FirstName = "Nana"
		// covIndividual.ID = 2
		// covIndividual.LastName = "Williams"
		// covIndividual.SSN = "000000208"
		// Call empSetup.Dependents.add(covIndividual)
		// detxml.Employee.Address1 = "4639 Fir Street"
		// detxml.Employee.City = "Lake City"
		// detxml.Employee.State = "FL"
		// detxml.Employee.Zip = "32055"
		// detxml.Employee.Zip4 = ""
		// detxml.Employee.SSN = "000000207"
		// detxml.Employee.FirstName = "Marco"
		// detxml.Employee.LastName = "Williams"
		//
		// Set GetScenario2_1Record = detxml
		// End Function
		private c1095BXMLRecord GetScenario2C_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario2C_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "2C-1";
			detxml.RecordID = 1;
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			string strReceiptID;
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "003";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "A";
			empSetup.ReportYear = intYear;
			strReceiptID = "1095B-16-00030021";
			detxml.CorrectedUniqueID = strReceiptID + "|1|2";
			detxml.BaseInfo.Corrected = true;
			detxml.BaseInfo.Address = "2255 Oak Avenue";
			detxml.BaseInfo.City = "Dublin";
			detxml.BaseInfo.PostalCode = "43016";
			detxml.BaseInfo.SSN = "000000211";
			detxml.BaseInfo.State = "OH";
			detxml.BaseInfo.EmployerSponsoredCoverage = true;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "2277 Holly Place";
			detxml.BaseInfo.CoverageProvider.City = "Washington";
			detxml.BaseInfo.CoverageProvider.EIN = "000000215";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Worktesttwo";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "20022";
			detxml.BaseInfo.CoverageProvider.State = "DC";
			detxml.BaseInfo.CoverageProvider.Telephone = "5555372511";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Vicky";
			covIndividual.LastName = "Willhelm";
			covIndividual.ID = 1;
			covIndividual.SSN = "000000211";
			covIndividual.SetIsCoveredForMonth(1, true);
			covIndividual.SetIsCoveredForMonth(2, true);
			covIndividual.SetIsCoveredForMonth(3, true);
			covIndividual.SetIsCoveredForMonth(4, true);
			covIndividual.SetIsCoveredForMonth(5, true);
			covIndividual.SetIsCoveredForMonth(6, true);
			covIndividual.SetIsCoveredForMonth(7, true);
			covIndividual.SetIsCoveredForMonth(8, true);
			covIndividual.SetIsCoveredForMonth(9, false);
			covIndividual.SetIsCoveredForMonth(10, false);
			covIndividual.SetIsCoveredForMonth(11, false);
			covIndividual.SetIsCoveredForMonth(12, false);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Wilfred";
			covIndividual.LastName = "Willhelm";
			covIndividual.ID = 4;
			covIndividual.SSN = "000000212";
			covIndividual.SetIsCoveredForMonth(1, true);
			covIndividual.SetIsCoveredForMonth(2, true);
			covIndividual.SetIsCoveredForMonth(3, true);
			covIndividual.SetIsCoveredForMonth(4, true);
			covIndividual.SetIsCoveredForMonth(5, true);
			covIndividual.SetIsCoveredForMonth(6, true);
			covIndividual.SetIsCoveredForMonth(7, true);
			covIndividual.SetIsCoveredForMonth(8, true);
			covIndividual.SetIsCoveredForMonth(9, false);
			covIndividual.SetIsCoveredForMonth(10, false);
			covIndividual.SetIsCoveredForMonth(11, false);
			covIndividual.SetIsCoveredForMonth(12, false);
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "914 Maple Street";
			detxml.Employee.City = "Paducah";
			detxml.Employee.State = "KY";
			detxml.Employee.Zip = "42003";
			detxml.Employee.SSN = "000000209";
			detxml.Employee.FirstName = "Paulette";
			detxml.Employee.LastName = "Willbert";
			GetScenario2C_1Record = detxml;
			return GetScenario2C_1Record;
		}
		// Private Function GetScenario2_2Record(ByVal intYear As Integer) As c1095BXMLRecord
		// Dim detxml As New c1095BXMLRecord
		// detxml.ScenarioID = "2-2"
		// detxml.RecordID = 2
		//
		// Set detxml.BaseInfo = New c1095B
		// Dim empSetup As New cACAEmployeeSetup
		// detxml.BaseInfo.ACAEmployee = empSetup
		// empSetup.CoverageProviderID = 1
		// empSetup.EmployeeID = 2
		// empSetup.EmployeeNumber = "002"
		// empSetup.ID = 2
		// empSetup.OriginOfPolicy = "D"
		// empSetup.ReportYear = intYear
		//
		// detxml.BaseInfo.Address = "914 Maple Street"
		// detxml.BaseInfo.City = "Paducah"
		// detxml.BaseInfo.PostalCode = "42003"
		// detxml.BaseInfo.SSN = "000000209"
		// detxml.BaseInfo.State = "KY"
		// detxml.BaseInfo.EmployerSponsoredCoverage = False
		// detxml.BaseInfo.CoverageProvider = New cCoverageProvider
		// detxml.BaseInfo.CoverageProvider.Address = "2277 Holly Place"
		// detxml.BaseInfo.CoverageProvider.City = "Washington"
		// detxml.BaseInfo.CoverageProvider.EIN = "000000215"
		// detxml.BaseInfo.CoverageProvider.ID = 1
		// detxml.BaseInfo.CoverageProvider.Name = "Worktesttwo"
		// detxml.BaseInfo.CoverageProvider.PlanMonth = 1
		// detxml.BaseInfo.CoverageProvider.PostalCode = "20022"
		// detxml.BaseInfo.CoverageProvider.State = "DC"
		// detxml.BaseInfo.CoverageProvider.Telephone = "5555372511"
		// detxml.BaseInfo.CoverageProvider.Use1095B = True
		//
		// Dim covIndividual As cACAEmployeeDependent
		// Set covIndividual = New cACAEmployeeDependent
		// covIndividual.ACAEmployeeSetupID = 2
		// covIndividual.FirstName = "Paulette"
		// covIndividual.LastName = "Willbert"
		// covIndividual.ID = 3
		// covIndividual.SSN = "000000209"
		// Call covIndividual.SetIsCoveredForMonth(1, False)
		// Call covIndividual.SetIsCoveredForMonth(2, False)
		// Call covIndividual.SetIsCoveredForMonth(3, False)
		// Call covIndividual.SetIsCoveredForMonth(4, False)
		// Call covIndividual.SetIsCoveredForMonth(5, False)
		// Call covIndividual.SetIsCoveredForMonth(6, True)
		// Call covIndividual.SetIsCoveredForMonth(7, True)
		// Call covIndividual.SetIsCoveredForMonth(8, True)
		// Call covIndividual.SetIsCoveredForMonth(9, True)
		// Call covIndividual.SetIsCoveredForMonth(10, True)
		// Call covIndividual.SetIsCoveredForMonth(11, True)
		// Call covIndividual.SetIsCoveredForMonth(12, True)
		// Call empSetup.Dependents.add(covIndividual)
		//
		// Set covIndividual = New cACAEmployeeDependent
		// covIndividual.ACAEmployeeSetupID = 2
		// covIndividual.FirstName = "Rene"
		// covIndividual.LastName = "Willbert"
		// covIndividual.ID = 4
		// covIndividual.SSN = "000000210"
		// Call covIndividual.SetIsCoveredForMonth(1, False)
		// Call covIndividual.SetIsCoveredForMonth(2, False)
		// Call covIndividual.SetIsCoveredForMonth(3, False)
		// Call covIndividual.SetIsCoveredForMonth(4, False)
		// Call covIndividual.SetIsCoveredForMonth(5, False)
		// Call covIndividual.SetIsCoveredForMonth(6, True)
		// Call covIndividual.SetIsCoveredForMonth(7, True)
		// Call covIndividual.SetIsCoveredForMonth(8, True)
		// Call covIndividual.SetIsCoveredForMonth(9, True)
		// Call covIndividual.SetIsCoveredForMonth(10, True)
		// Call covIndividual.SetIsCoveredForMonth(11, True)
		// Call covIndividual.SetIsCoveredForMonth(12, True)
		// Call empSetup.Dependents.add(covIndividual)
		//
		// detxml.Employee.Address1 = "914 Maple Street"
		// detxml.Employee.City = "Paducah"
		// detxml.Employee.State = "KY"
		// detxml.Employee.Zip = "42003"
		// detxml.Employee.SSN = "000000209"
		// detxml.Employee.FirstName = "Paulette"
		// detxml.Employee.LastName = "Willbert"
		//
		// Set GetScenario2_2Record = detxml
		// End Function
		private c1095BXMLRecord GetScenario2_1Record(int intYear)
		{
			c1095BXMLRecord GetScenario2_1Record = null;
			c1095BXMLRecord detxml = new c1095BXMLRecord();
			detxml.ScenarioID = "2-1";
			detxml.RecordID = 1;
			detxml.BaseInfo = new c1095B();
			cACAEmployeeSetup empSetup = new cACAEmployeeSetup();
			detxml.BaseInfo.ACAEmployee = empSetup;
			empSetup.CoverageProviderID = 1;
			empSetup.EmployeeID = 1;
			empSetup.EmployeeNumber = "003";
			empSetup.ID = 1;
			empSetup.OriginOfPolicy = "A";
			empSetup.ReportYear = intYear;
			detxml.BaseInfo.Address = "2255 Oak Avenue";
			detxml.BaseInfo.City = "Dublin";
			detxml.BaseInfo.State = "OH";
			detxml.BaseInfo.PostalCode = "43016";
			detxml.BaseInfo.SSN = "000000211";
			detxml.BaseInfo.EmployerSponsoredCoverage = true;
			detxml.BaseInfo.CoverageProvider = new cCoverageProvider();
			detxml.BaseInfo.CoverageProvider.Address = "2277 Holly Place";
			detxml.BaseInfo.CoverageProvider.City = "Washington";
			detxml.BaseInfo.CoverageProvider.EIN = "000000215";
			detxml.BaseInfo.CoverageProvider.ID = 1;
			detxml.BaseInfo.CoverageProvider.Name = "Worktesttwo";
			detxml.BaseInfo.CoverageProvider.PlanMonth = 1;
			detxml.BaseInfo.CoverageProvider.PostalCode = "20022";
			detxml.BaseInfo.CoverageProvider.State = "DC";
			detxml.BaseInfo.CoverageProvider.Telephone = "5555372511";
			detxml.BaseInfo.CoverageProvider.Use1095B = true;
			cACAEmployeeDependent covIndividual;
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Vicky";
			covIndividual.LastName = "Willhelm";
			covIndividual.ID = 5;
			covIndividual.SSN = "000000211";
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(10, false);
			covIndividual.SetIsCoveredForMonth(11, false);
			covIndividual.SetIsCoveredForMonth(12, false);
			empSetup.Dependents.Add(covIndividual);
			covIndividual = new cACAEmployeeDependent();
			covIndividual.ACAEmployeeSetupID = 1;
			covIndividual.FirstName = "Wilfred";
			covIndividual.LastName = "Willhelm";
			covIndividual.ID = 6;
			covIndividual.SSN = "000000212";
			covIndividual.CoveredAll12Months = true;
			covIndividual.SetIsCoveredForMonth(10, false);
			covIndividual.SetIsCoveredForMonth(11, false);
			covIndividual.SetIsCoveredForMonth(12, false);
			empSetup.Dependents.Add(covIndividual);
			detxml.Employee.Address1 = "2255 Oak Avenue";
			detxml.Employee.City = "Dublin";
			detxml.Employee.State = "OH";
			detxml.Employee.Zip = "43016";
			detxml.Employee.SSN = "000000211";
			detxml.Employee.FirstName = "Vicky";
			detxml.Employee.LastName = "Willhelm";
			GetScenario2_1Record = detxml;
			return GetScenario2_1Record;
		}

		public cACABXMLReport GetTestScenario2(int intYear)
		{
			cACABXMLReport GetTestScenario2 = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario2Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "2-0";
			header.Employer.Name = "Workshoptwo";
			header.Employer.Address = "1095 Cedar Lane";
			header.Employer.City = "Westerville";
			header.Employer.EIN = "000000250";
			header.Employer.Zip = "43081";
			header.Employer.State = "OH";
			header.Employer.Telephone = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario2Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario2 = theReport;
			return GetTestScenario2;
		}

		public cACABXMLReport GetTestScenario2C(int intYear)
		{
			cACABXMLReport GetTestScenario2C = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario2CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "2C-0";
			header.Employer.Name = "Workshoptwo";
			header.Employer.Address = "1095 Cedar Lane";
			header.Employer.City = "Westerville";
			header.Employer.EIN = "000000250";
			header.Employer.Zip = "43081";
			header.Employer.State = "OH";
			header.Employer.Telephone = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario2CDetails(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario2C = theReport;
			return GetTestScenario2C;
		}

		public cACABXMLReport GetTestScenario6C(int intYear)
		{
			cACABXMLReport GetTestScenario6C = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario6CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "6C-0";
			header.Employer.Name = "";
			header.Employer.Address = "";
			header.Employer.City = "";
			header.Employer.State = "";
			header.Employer.EIN = "";
			header.Employer.Telephone = "";
			header.Employer.Zip = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario6CDetails(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario6C = theReport;
			return GetTestScenario6C;
		}

		public cACABXMLReport GetTestScenario6(int intYear)
		{
			cACABXMLReport GetTestScenario6 = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario6Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "6-0";
			header.Employer.Name = "";
			header.Employer.Address = "";
			header.Employer.City = "";
			header.Employer.State = "";
			header.Employer.EIN = "";
			header.Employer.Telephone = "";
			header.Employer.Zip = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario6Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario6 = theReport;
			return GetTestScenario6;
		}

		public cACABXMLReport GetTestScenario8C(int intYear)
		{
			cACABXMLReport GetTestScenario8C = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario8CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "8C-0";
			header.Employer.Name = "";
			header.Employer.Address = "";
			header.Employer.City = "";
			header.Employer.State = "";
			header.Employer.EIN = "";
			header.Employer.Telephone = "";
			header.Employer.Zip = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario8CDetails(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario8C = theReport;
			return GetTestScenario8C;
		}

		public cACABXMLReport GetTestScenario8(int intYear)
		{
			cACABXMLReport GetTestScenario8 = null;
			cACABXMLReport theReport = new cACABXMLReport();
			c1094BXMLRecord header = new c1094BXMLRecord();
			c1094B headerbase;
			cGenericCollection emps;
			c1095BXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario8Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "8-0";
			header.Employer.Name = "";
			header.Employer.Address = "";
			header.Employer.City = "";
			header.Employer.State = "";
			header.Employer.EIN = "";
			header.Employer.Telephone = "";
			header.Employer.Zip = "";
			header.BaseInfo = headerbase;
			header.Details = FillScenario8Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario8 = theReport;
			return GetTestScenario8;
		}

		public cACACXMLReport GetTestScenario3(int intYear)
		{
			cACACXMLReport GetTestScenario3 = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario3Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "3-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario3Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario3 = theReport;
			return GetTestScenario3;
		}

		public cACACXMLReport GetTestScenario4(int intYear)
		{
			cACACXMLReport GetTestScenario4 = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario4Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "4-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario4Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario4 = theReport;
			return GetTestScenario4;
		}

		public cACACXMLReport GetTestScenario4C(int intYear)
		{
			cACACXMLReport GetTestScenario4C = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario4CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "4C-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario4CDetails(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario4C = theReport;
			return GetTestScenario4C;
		}

		public cACACXMLReport GetTestScenario5C(int intYear)
		{
			cACACXMLReport GetTestScenario5C = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario5CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "5C-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario5CDetails(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario5C = theReport;
			return GetTestScenario5C;
		}

		public cACACXMLReport GetTestScenario5(int intYear)
		{
			cACACXMLReport GetTestScenario5 = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario5Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "5-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario5Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario5 = theReport;
			return GetTestScenario5;
		}

		public cACACXMLReport GetTestScenario7C(int intYear)
		{
			cACACXMLReport GetTestScenario7C = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			theReport.IsCorrection = true;
			headerbase = GetScenario7CHeader(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "7C-0";
			header.BaseInfo = headerbase;
			string strReceiptID;
			strReceiptID = "1095C-16-00060350|1";
			header.CorrectedUniqueID = strReceiptID;
			// Call FillScenario7cDetails(header.Details, intYear)
			theReport.List1094s.AddItem(header);
			GetTestScenario7C = theReport;
			return GetTestScenario7C;
		}

		public cACACXMLReport GetTestScenario7(int intYear)
		{
			cACACXMLReport GetTestScenario7 = null;
			cACACXMLReport theReport = new cACACXMLReport();
			c1094CXMLRecord header = new c1094CXMLRecord();
			c1094C headerbase;
			cGenericCollection emps;
			c1095CXMLRecord empXML;
			theReport.ReportYear = intYear;
			headerbase = GetScenario7Header(intYear);
			header.SubmissionID = 1;
			header.ScenarioID = "7-0";
			header.BaseInfo = headerbase;
			header.Details = FillScenario7Details(header.Details, intYear);
			theReport.List1094s.AddItem(header);
			GetTestScenario7 = theReport;
			return GetTestScenario7;
		}
	}
}
