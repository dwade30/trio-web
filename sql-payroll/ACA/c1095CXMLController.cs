﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public abstract class c1095CXMLController
	{
		//=========================================================
		cEmployeeService employeeService = new cEmployeeService();
		protected c1095CXMLManifestController manController;

		public void SetManifestController(c1095CXMLManifestController manifestCont)
		{
			manController = manifestCont;
		}

		protected string StripBadChars(string strOrig)
		{
			string StripBadChars = "";
			string strReturn;
			strReturn = strOrig.Replace("'", "");
			strReturn = strReturn.Replace("&", "");
			strReturn = strReturn.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
			strReturn = strReturn.Replace("<", "");
			StripBadChars = strReturn;
			return StripBadChars;
		}
		// vbPorter upgrade warning: strInnerXML As string	OnWrite(string, int)
		protected string GetSimpleTag(string strTag, string strInnerXML)
		{
			string GetSimpleTag = "";
			if (fecherFoundation.Strings.Trim(strInnerXML) != "")
			{
				GetSimpleTag = "<" + strTag + ">" + fecherFoundation.Strings.Trim(strInnerXML) + "</" + strTag + ">";
			}
			return GetSimpleTag;
		}

		protected string GetBooleanTag(string strTag, bool boolValue)
		{
			string GetBooleanTag = "";
			string strTemp;
			strTemp = "<" + strTag + ">";
			if (boolValue)
			{
				strTemp += "1";
			}
			else
			{
				strTemp += "0";
			}
			strTemp += "</" + strTag + ">";
			GetBooleanTag = strTemp;
			return GetBooleanTag;
		}

		protected string GetYesNoTag(string strTag, bool boolValue)
		{
			string GetYesNoTag = "";
			string strTemp;
			strTemp = "<" + strTag + ">";
			if (boolValue)
			{
				strTemp += "1";
			}
			else
			{
				strTemp += "2";
			}
			strTemp += "</" + strTag + ">";
			GetYesNoTag = strTemp;
			return GetYesNoTag;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public object ALEMonthlyInformationToXML(cALEMonthlyInformation monthlyInfo)
		//{
		//	object ALEMonthlyInformationToXML = null;
		//	string strXML = "";
		//	if (!(monthlyInfo == null))
		//	{
		//		string strMonth = "";
		//		strMonth = GetMonthAbbrev(monthlyInfo.CoverageMonth);
		//		strXML = "<" + strMonth + "ALEMonthlyInfoGrp>";
		//		strXML += GetSimpleTag("MinEssentialCvrOffrCd>", FCConvert.ToString(monthlyInfo.MinimumEssentialCoverageOffer));
		//		strXML += GetSimpleTag("ALEMemberFTECnt", FCConvert.ToString(monthlyInfo.FullTimeEmployeeCount));
		//		strXML += GetSimpleTag("TotalEmployeeCnt", FCConvert.ToString(monthlyInfo.TotalEmployeeCount));
		//		strXML += "</" + strMonth + "ALEMonthlyInfoGrp>";
		//	}
		//	ALEMonthlyInformationToXML = strXML;
		//	return ALEMonthlyInformationToXML;
		//}

		protected string GetMonthAbbrev(int intMonth)
		{
			string GetMonthAbbrev = "";
			string strMonth = "";
			switch (intMonth)
			{
				case 1:
					{
						strMonth = "Jan";
						break;
					}
				case 2:
					{
						strMonth = "Feb";
						break;
					}
				case 3:
					{
						strMonth = "Mar";
						break;
					}
				case 4:
					{
						strMonth = "Apr";
						break;
					}
				case 5:
					{
						strMonth = "May";
						break;
					}
				case 6:
					{
						strMonth = "Jun";
						break;
					}
				case 7:
					{
						strMonth = "Jul";
						break;
					}
				case 8:
					{
						strMonth = "Aug";
						break;
					}
				case 9:
					{
						strMonth = "Sep";
						break;
					}
				case 10:
					{
						strMonth = "Oct";
						break;
					}
				case 11:
					{
						strMonth = "Nov";
						break;
					}
				case 12:
					{
						strMonth = "Dec";
						break;
					}
			}
			//end switch
			GetMonthAbbrev = strMonth;
			return GetMonthAbbrev;
		}

		protected string GetMonthName(int intMonth)
		{
			string GetMonthName = "";
			string strMonth = "";
			switch (intMonth)
			{
				case 1:
					{
						strMonth = "January";
						break;
					}
				case 2:
					{
						strMonth = "February";
						break;
					}
				case 3:
					{
						strMonth = "March";
						break;
					}
				case 4:
					{
						strMonth = "April";
						break;
					}
				case 5:
					{
						strMonth = "May";
						break;
					}
				case 6:
					{
						strMonth = "June";
						break;
					}
				case 7:
					{
						strMonth = "July";
						break;
					}
				case 8:
					{
						strMonth = "August";
						break;
					}
				case 9:
					{
						strMonth = "September";
						break;
					}
				case 10:
					{
						strMonth = "October";
						break;
					}
				case 11:
					{
						strMonth = "November";
						break;
					}
				case 12:
					{
						strMonth = "December";
						break;
					}
			}
			//end switch
			GetMonthName = strMonth;
			return GetMonthName;
		}

		//public string GetPersonNameXML(string strTag, string strFirstName, string strMiddleName, string strLastName, string strSuffix)
		//{
		//	string GetPersonNameXML = "";
		//	string strXML = "";
		//	string strTemp = "";
		//	if (strFirstName != "" || strLastName != "")
		//	{
		//		if (strTag != "")
		//		{
		//			strXML = "<" + strTag + ">";
		//		}
		//		if (strFirstName != "")
		//		{
		//			strXML += GetSimpleTag("PersonFirstNm", StripBadChars(strFirstName));
		//		}
		//		if (strMiddleName != "")
		//		{
		//			strXML += GetSimpleTag("PersonMiddleNm", StripBadChars(strMiddleName));
		//		}
		//		if (strLastName != "")
		//		{
		//			strXML += GetSimpleTag("PersonLastNm", StripBadChars(strLastName));
		//		}
		//		if (strSuffix != "")
		//		{
		//			strXML += GetSimpleTag("SuffixNm", StripBadChars(strSuffix));
		//		}
		//		if (strTag != "")
		//		{
		//			strXML += "</" + strTag + ">";
		//		}
		//	}
		//	GetPersonNameXML = strXML;
		//	return GetPersonNameXML;
		//}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public object GetMailingAddressGroupXML(string strAddress1, string strCity, string strState, string strZip, string strZip4)
		//{
		//	object GetMailingAddressGroupXML = null;
		//	string strXML = "";
		//	string strTemp = "";
		//	string strZip5;
		//	string strZipExt = "";
		//	strZip = strZip.Replace("-", " ");
		//	strZip5 = fecherFoundation.Strings.Trim(Strings.Left(strZip + "     ", 5));
		//	if (strZip4 != "")
		//	{
		//		strZipExt = strZip4;
		//	}
		//	else
		//	{
		//		if (strZip.Length > 6)
		//		{
		//			strZipExt = fecherFoundation.Strings.Trim(Strings.Mid(strZip, 7));
		//		}
		//	}
		//	if (strAddress1 != "" || strCity != "" || strState != "" || strZip != "")
		//	{
		//		strXML = "<MailingAddressGrp>";
		//		strXML += "<USAddressGrp>";
		//		if (strAddress1 != "")
		//		{
		//			strXML += "<AddressLine1Txt>";
		//			strTemp = strAddress1;
		//			strTemp = strTemp.Replace("'", "");
		//			strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
		//			strTemp = strTemp.Replace("/", "");
		//			strTemp = strTemp.Replace("\\", "");
		//			strTemp = strTemp.Replace("#", " ");
		//			strXML += fecherFoundation.Strings.Trim(strTemp);
		//			strXML += "</AddressLine1Txt>";
		//		}
		//		if (strCity != "")
		//		{
		//			strXML += "<irs:CityNm>";
		//			strTemp = strCity.Replace("'", "");
		//			strTemp = strCity.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
		//			strXML += fecherFoundation.Strings.Trim(strTemp);
		//			strXML += "</irs:CityNm>";
		//		}
		//		strXML += "<USStateCd>";
		//		strXML += strState;
		//		strXML += "</USStateCd>";
		//		strXML += "<irs:USZIPCd>";
		//		strXML += strZip5.Replace(" ", "");
		//		strXML += "</irs:USZIPCd>";
		//		if (strZipExt != "")
		//		{
		//			strXML += "<irs:USZIPExtensionCd>";
		//			strXML += strZipExt;
		//			strXML += "</irs:USZIPExtensionCd>";
		//		}
		//		strXML += "</USAddressGrp>";
		//		strXML += "</MailingAddressGrp>";
		//	}
		//	GetMailingAddressGroupXML = strXML;
		//	return GetMailingAddressGroupXML;
		//}

        public abstract string ACACXMLReportToXML(ref cACACXMLReport theReport);
		//{
		//	string ACACXMLReportToXML = "";
		//	string strXML = "";
		//	string strTemp = "";
		//	int intCount = 0;
		//	if (!(theReport == null))
		//	{
		//		theReport.List1094s.MoveFirst();
		//		strXML = "<?xml version=" + FCConvert.ToString(Convert.ToChar(34)) + "1.0" + FCConvert.ToString(Convert.ToChar(34)) + " encoding=" + FCConvert.ToString(Convert.ToChar(34)) + "utf-8" + FCConvert.ToString(Convert.ToChar(34)) + "?>";
		//		strXML += "<form109495C:Form109495CTransmittalUpstream xmlns:irs=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:common" + FCConvert.ToString(Convert.ToChar(34));
		//		strXML += " xmlns:form109495C=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:msg:form1094-1095Ctransmitterupstreammessage" + FCConvert.ToString(Convert.ToChar(34)) + ">";
		//		c1094CXMLRecord header;
		//		intCount = 0;
		//		while (theReport.List1094s.IsCurrent())
		//		{
		//			//App.DoEvents();
		//			intCount += 1;
		//			header = (c1094CXMLRecord)theReport.List1094s.GetCurrentItem();
		//			header.SubmissionID = intCount;
		//			strTemp = c1094CXMLRecordToXML(ref header);
		//			strXML += strTemp;
		//			theReport.List1094s.MoveNext();
		//		}
		//		strXML += "</form109495C:Form109495CTransmittalUpstream>";
		//	}
		//	ACACXMLReportToXML = strXML;
		//	return ACACXMLReportToXML;
		//}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
       // public abstract object c1095CXMLRecordToXML(c1095CXMLRecord acaEmp, string strEmployerPhone);
		//{
		//	object c1095CXMLRecordToXML = null;
		//	string strXML = "";
		//	if (!(acaEmp == null))
		//	{
		//		c1095C baseRecord;
		//		cEmployee emp;
		//		int intindex;
		//		string strMonth = "";
		//		double dblTemp = 0;
		//		string strTemp = "";
		//		emp = acaEmp.Employee;
		//		strXML = "<Form1095CUpstreamDetail recordType=" + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + " lineNum=" + FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ">";
		//		strXML += GetSimpleTag("RecordId", FCConvert.ToString(acaEmp.RecordID));
		//		if (acaEmp.ScenarioID != "")
		//		{
		//			strXML += GetSimpleTag("TestScenarioId", acaEmp.ScenarioID);
		//		}
		//		baseRecord = acaEmp.BaseInfo;
		//		if (baseRecord.Corrected)
		//		{
		//			strXML += GetSimpleTag("CorrectedInd", "1");
		//			strXML += "<CorrectedRecordInfoGrp>";
		//			strXML += GetSimpleTag("CorrectedUniqueRecordId", acaEmp.CorrectedUniqueID);
		//			strXML += GetPersonNameXML("CorrectedRecordPayeeName", emp.FirstName, emp.MiddleName, emp.LastName, emp.Designation);
		//			strTemp = emp.SSN.Replace("-", "");
		//			strTemp = strTemp.Replace(" ", "");
		//			strXML += GetSimpleTag("CorrectedRecordPayeeTIN", strTemp);
		//			strXML += "</CorrectedRecordInfoGrp>";
		//		}
		//		else
		//		{
		//			strXML += GetSimpleTag("CorrectedInd", "0");
		//		}
		//		strXML += GetSimpleTag("irs:TaxYr", FCConvert.ToString(baseRecord.ReportYear));
		//		strXML += "<EmployeeInfoGrp>";
		//		strXML += GetPersonNameXML("OtherCompletePersonName", emp.FirstName, emp.MiddleName, emp.LastName, emp.Designation);
		//		strXML += GetSimpleTag("irs:TINRequestTypeCd", "INDIVIDUAL_TIN");
		//		if (baseRecord.SSN != "")
		//		{
		//			strXML += GetSimpleTag("irs:SSN", fecherFoundation.Strings.Trim(baseRecord.SSN.Replace("-", "")));
		//		}
		//		else
		//		{
		//		}
		//		strXML += FCConvert.ToString(GetMailingAddressGroupXML(baseRecord.Address, baseRecord.City, baseRecord.State, baseRecord.PostalCode, ""));
		//		strXML += "</EmployeeInfoGrp>";
		//		if (strEmployerPhone != "")
		//		{
		//			strXML += GetSimpleTag("ALEContactPhoneNum", strEmployerPhone.Replace("(", "").Replace(")", "").Replace("-", ""));
		//		}
		//		if (baseRecord.PlanStartMonth > 0)
		//		{
		//			strXML += GetSimpleTag("StartMonthNumberCd", Strings.Right("0" + FCConvert.ToString(baseRecord.PlanStartMonth), 2));
		//		}
		//		strXML += "<EmployeeOfferAndCoverageGrp>";
		//		strTemp = baseRecord.CoverageCodeRequiredAll12Months;
		//		if (strTemp != "")
		//		{
		//			strXML += GetSimpleTag("AnnualOfferOfCoverageCd", strTemp);
		//		}
		//		else
		//		{
		//			strXML += "<MonthlyOfferCoverageGrp>";
		//			for (intindex = 1; intindex <= 12; intindex++)
		//			{
		//				strTemp = baseRecord.GetCoverageRequiredCodeForMonth(intindex);
		//				if (strTemp != "")
		//				{
		//					strMonth = GetMonthAbbrev(intindex);
		//					strXML += GetSimpleTag(strMonth + "OfferCd", strTemp);
		//				}
		//			}
		//			strXML += "</MonthlyOfferCoverageGrp>";
		//		}
		//		if (baseRecord.EmployeeShareLowestPremiumSameAll12Months)
		//		{
		//			dblTemp = baseRecord.EmployeeShareLowestPremiumAll12Months;
		//			if (dblTemp > 0)
		//			{
		//				strXML += GetSimpleTag("AnnlShrLowestCostMthlyPremAmt", Strings.Format(dblTemp, "0.00"));
		//			}
		//		}
		//		else
		//		{
		//			strXML += "<MonthlyShareOfLowestCostMonthlyPremGrp>";
		//			for (intindex = 1; intindex <= 12; intindex++)
		//			{
		//				dblTemp = baseRecord.GetEmployeeShareLowestPremiumForMonth(intindex);
		//				if (dblTemp > 0)
		//				{
		//					strMonth = GetMonthName(intindex);
		//					strXML += GetSimpleTag(strMonth + "Amt", Strings.Format(dblTemp, "0.00"));
		//				}
		//			}
		//			strXML += "</MonthlyShareOfLowestCostMonthlyPremGrp>";
		//		}
		//		strTemp = baseRecord.SafeHarborCodeAll12Months;
		//		if (strTemp != "")
		//		{
		//			strXML += GetSimpleTag("AnnualSafeHarborCd", strTemp);
		//		}
		//		else
		//		{
		//			if (!baseRecord.SafeHarborCodeSameAll12Months)
		//			{
		//				strXML += "<MonthlySafeHarborGrp>";
		//				for (intindex = 1; intindex <= 12; intindex++)
		//				{
		//					strTemp = baseRecord.GetSafeHarborCodeForMonth(intindex);
		//					if (strTemp != "")
		//					{
		//						strMonth = GetMonthAbbrev(intindex);
		//						strXML += GetSimpleTag(strMonth + "SafeHarborCd", strTemp);
		//					}
		//				}
		//				strXML += "</MonthlySafeHarborGrp>";
		//			}
		//		}
		//		strXML += "</EmployeeOfferAndCoverageGrp>";
		//		if (baseRecord.coveredIndividuals.Count > 0)
		//		{
		//			strXML += GetSimpleTag("CoveredIndividualInd", "1");
		//			strXML += GetCoveredIndividualsXML(baseRecord.coveredIndividuals);
		//		}
		//		else
		//		{
		//			strXML += GetSimpleTag("CoveredIndividualInd", "0");
		//		}
		//		strXML += "</Form1095CUpstreamDetail>";
		//	}
		//	c1095CXMLRecordToXML = strXML;
		//	return c1095CXMLRecordToXML;
		//}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public object GetCoveredIndMonthlyXML(ref cACAEmployeeDependent dep)
		//{
		//	object GetCoveredIndMonthlyXML = null;
		//	string strXML = "";
		//	int x;
		//	string strMonth = "";
		//	int intBool;
		//	if (!(dep == null))
		//	{
		//		if (dep.CoveredAll12Months)
		//		{
		//			strXML = GetSimpleTag("CoveredIndividualAnnualInd", "1");
		//		}
		//		else
		//		{
		//			strXML = "<CoveredIndividualMonthlyIndGrp>";
		//			for (x = 1; x <= 12; x++)
		//			{
		//				strMonth = GetMonthName(x);
		//				if (dep.GetIsCoveredForMonth(x))
		//				{
		//					strXML += GetSimpleTag(strMonth + "Ind", FCConvert.ToString(1));
		//				}
		//				else
		//				{
		//					strXML += GetSimpleTag(strMonth + "Ind", FCConvert.ToString(0));
		//				}
		//			}
		//			strXML += "</CoveredIndividualMonthlyIndGrp>";
		//		}
		//	}
		//	GetCoveredIndMonthlyXML = strXML;
		//	return GetCoveredIndMonthlyXML;
		//}

		//public string GetCoveredIndividualsXML(FCCollection coveredIndividuals)
		//{
		//	string GetCoveredIndividualsXML = "";
		//	string strXML = "";
		//	if (!(coveredIndividuals == null))
		//	{
		//		if (coveredIndividuals.Count > 0)
		//		{
		//			string strTemp = "";
		//			int x;
		//			cACAEmployeeDependent dep;
		//			for (x = 1; x <= coveredIndividuals.Count; x++)
		//			{
		//				//App.DoEvents();
		//				dep = coveredIndividuals[x];
		//				if (dep.FullName != "")
		//				{
		//					strXML += "<CoveredIndividualGrp>";
		//					strXML += GetPersonNameXML("CoveredIndividualName", dep.FirstName, dep.MiddleName, dep.LastName, dep.Suffix);
		//					strXML += GetSimpleTag("irs:TINRequestTypeCd", "INDIVIDUAL_TIN");
		//					if (dep.SSN != "")
		//					{
		//						strXML += "<irs:SSN>";
		//						strTemp = dep.SSN;
		//						strTemp = strTemp.Replace("-", "");
		//						strTemp = strTemp.Replace(" ", "");
		//						strXML += strTemp;
		//						strXML += "</irs:SSN>";
		//					}
		//					else
		//					{
		//						if (Information.IsDate(dep.DateOfBirth))
		//						{
		//							strXML += GetSimpleTag("irs:BirthDt", FCConvert.ToString(FCConvert.ToDateTime(dep.DateOfBirth).Year) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(dep.DateOfBirth).Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(dep.DateOfBirth).Day), 2));
		//						}
		//					}
		//					strXML += FCConvert.ToString(GetCoveredIndMonthlyXML(ref dep));
		//					strXML += "</CoveredIndividualGrp>";
		//				}
		//			}
		//		}
		//	}
		//	GetCoveredIndividualsXML = strXML;
		//	return GetCoveredIndividualsXML;
		//}

		//public string c1094CXMLRecordToXML(ref c1094CXMLRecord acaRec)
		//{
		//	string c1094CXMLRecordToXML = "";
		//	string strXML = "";
		//	string strTemp = "";
		//	if (!(acaRec == null))
		//	{
		//		strXML = "<Form1094CUpstreamDetail recordType=" + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + " lineNum=" + FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + " xmlns=" + FCConvert.ToString(Convert.ToChar(34)) + "urn:us:gov:treasury:irs:ext:aca:air:7.0" + FCConvert.ToString(Convert.ToChar(34)) + ">";
		//		strXML += GetSimpleTag("SubmissionId", FCConvert.ToString(acaRec.SubmissionID));
		//		strXML += GetSimpleTag("OriginalUniqueSubmissionId", acaRec.UNIQUEID);
		//		strXML += GetSimpleTag("TestScenarioId", acaRec.ScenarioID);
		//		strXML += GetSimpleTag("irs:TaxYr", FCConvert.ToString(acaRec.BaseInfo.YearCovered));
		//		strXML += GetBooleanTag("CorrectedInd", acaRec.BaseInfo.IsCorrected);
		//		if (acaRec.BaseInfo.IsCorrected && acaRec.CorrectedUniqueID != "")
		//		{
		//			strXML += "<CorrectedSubmissionInfoGrp>";
		//			strXML += GetSimpleTag("CorrectedUniqueSubmissionId", acaRec.CorrectedUniqueID);
		//			strXML += "<CorrectedSubmissionPayerName>";
		//			strXML += GetSimpleTag("BusinessNameLine1Txt", StripBadChars(acaRec.BaseInfo.EmployerName));
		//			strXML += "</CorrectedSubmissionPayerName>";
		//			strXML += GetSimpleTag("CorrectedSubmissionPayerTIN", acaRec.BaseInfo.EIN.Replace("-", "").Replace(" ", ""));
		//			strXML += "</CorrectedSubmissionInfoGrp>";
		//		}
		//		strXML += "<EmployerInformationGrp>";
		//		strXML += "<BusinessName>";
		//		strXML += GetSimpleTag("BusinessNameLine1Txt", StripBadChars(acaRec.BaseInfo.EmployerName));
		//		strXML += "</BusinessName>";
		//		strXML += GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN");
		//		strXML += GetSimpleTag("irs:EmployerEIN", acaRec.BaseInfo.EIN.Replace("-", "").Replace(" ", ""));
		//		strXML += FCConvert.ToString(GetMailingAddressGroupXML(acaRec.BaseInfo.Address, acaRec.BaseInfo.City, acaRec.BaseInfo.State, acaRec.BaseInfo.PostalCode, ""));
		//		if (acaRec.BaseInfo.ContactName != "")
		//		{
		//			strXML += GetPersonNameXML("ContactNameGrp", acaRec.BaseInfo.ContactName, acaRec.BaseInfo.ContactMiddleName, acaRec.BaseInfo.ContactLastName, acaRec.BaseInfo.ContactSuffix);
		//		}
		//		if (acaRec.BaseInfo.ContactTelephone != "")
		//		{
		//			strTemp = StripBadChars(acaRec.BaseInfo.ContactTelephone);
		//			strTemp = strTemp.Replace("(", "");
		//			strTemp = strTemp.Replace(")", "");
		//			strTemp = strTemp.Replace("-", "");
		//			strXML += GetSimpleTag("ContactPhoneNum", strTemp);
		//		}
		//		strXML += "</EmployerInformationGrp>";
		//		if (acaRec.BaseInfo.DesignatedGovnernmentEntity != "")
		//		{
		//			strXML += "<GovtEntityEmployerInfoGrp>";
		//			strXML += "<BusinessName>";
		//			strXML += GetSimpleTag("BusinessNameLine1Txt", StripBadChars(acaRec.BaseInfo.DesignatedGovnernmentEntity));
		//			strXML += "</BusinessName>";
		//			// strXML = strXML & GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN")
		//			if (acaRec.BaseInfo.EntityEIN != "")
		//			{
		//				strXML += GetSimpleTag("irs:EmployerEIN", acaRec.BaseInfo.EntityEIN.Replace("-", "").Replace(" ", ""));
		//			}
		//			strXML += FCConvert.ToString(GetMailingAddressGroupXML(acaRec.BaseInfo.EntityAddress, acaRec.BaseInfo.EntityCity, acaRec.BaseInfo.EntityState, acaRec.BaseInfo.EntityPostalCode, ""));
		//			if (acaRec.BaseInfo.EntityContact != "")
		//			{
		//				strXML += GetPersonNameXML("ContactNameGrp", acaRec.BaseInfo.EntityContact, acaRec.BaseInfo.EntityContactMiddle, acaRec.BaseInfo.EntityContactLast, acaRec.BaseInfo.EntityContactSuffix);
		//			}
		//			if (acaRec.BaseInfo.EntityTelephone != "")
		//			{
		//				strXML += GetSimpleTag("ContactPhoneNum", acaRec.BaseInfo.EntityTelephone.Replace("(", "").Replace(")", "").Replace("-", ""));
		//			}
		//			strXML += "</GovtEntityEmployerInfoGrp>";
		//		}
		//		strXML += GetSimpleTag("Form1095CAttachedCnt", FCConvert.ToString(acaRec.Details.ItemCount()));
		//		strXML += GetBooleanTag("AuthoritativeTransmittalInd", acaRec.BaseInfo.IsAuthoritativeTransmittal);
		//		if (acaRec.BaseInfo.IsAuthoritativeTransmittal)
		//		{
		//			strXML += GetSimpleTag("TotalForm1095CALEMemberCnt", FCConvert.ToString(acaRec.BaseInfo.Total1095CForMember));
		//			// If acaRec.BaseInfo.MemberOfAggregatedGroup Then
		//			// strXML = strXML & GetSimpleTag("AggregatedGroupMemberCd", "1")
		//			// Else
		//			// strXML = strXML & GetSimpleTag("AggregatedGroupMemberCd", "0")
		//			// End If
		//			strXML += GetYesNoTag("AggregatedGroupMemberCd", acaRec.BaseInfo.MemberOfAggregatedGroup);
		//			strXML += GetBooleanTag("QualifyingOfferMethodInd", acaRec.BaseInfo.QualifyingOfferMethod);
		//			strXML += GetBooleanTag("QlfyOfferMethodTrnstReliefInd", acaRec.BaseInfo.QualifyingOfferTransitionRelief);
		//			strXML += GetBooleanTag("Section4980HReliefInd", acaRec.BaseInfo.Section4980HRelief);
		//			strXML += GetBooleanTag("NinetyEightPctOfferMethodInd", acaRec.BaseInfo.NinetyEightPercentOfferMethod);
		//			strXML += GetALEMemberInformationXML(ref acaRec);
		//			strXML += GetOtherALEMembersXML(acaRec.BaseInfo.AggregatedMembers);
		//		}
		//		acaRec.Details.MoveFirst();
		//		while (acaRec.Details.IsCurrent())
		//		{
		//			//App.DoEvents();
		//			strXML += FCConvert.ToString(c1095CXMLRecordToXML((c1095CXMLRecord)acaRec.Details.GetCurrentItem(), acaRec.BaseInfo.ContactTelephone));
		//			// , acaRec)
		//			acaRec.Details.MoveNext();
		//		}
		//		strXML += "</Form1094CUpstreamDetail>";
		//	}
		//	c1094CXMLRecordToXML = strXML;
		//	return c1094CXMLRecordToXML;
		//}

		//public string GetOtherALEMembersXML(cGenericCollection membersList)
		//{
		//	string GetOtherALEMembersXML = "";
		//	string strXML;
		//	string strMemberXML = "";
		//	strXML = "";
		//	cAggregatedMember gMember;
		//	if (!(membersList == null))
		//	{
		//		if (membersList.ItemCount() > 0)
		//		{
		//			membersList.MoveFirst();
		//			while (membersList.IsCurrent())
		//			{
		//				//App.DoEvents();
		//				gMember = (cAggregatedMember)membersList.GetCurrentItem();
		//				strMemberXML = "<OtherALEMembersGrp>";
		//				strMemberXML += "<BusinessName>";
		//				strMemberXML += GetSimpleTag("BusinessNameLine1Txt", StripBadChars(gMember.Name));
		//				strMemberXML += "</BusinessName>";
		//				strMemberXML += GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN");
		//				strMemberXML += GetSimpleTag("irs:EIN", gMember.EIN.Replace("-", "").Replace(" ", ""));
		//				strMemberXML += "</OtherALEMembersGrp>";
		//				strXML += strMemberXML;
		//				membersList.MoveNext();
		//			}
		//		}
		//	}
		//	GetOtherALEMembersXML = strXML;
		//	return GetOtherALEMembersXML;
		//}

		//public string GetALEMemberInformationXML(ref c1094CXMLRecord acaRec)
		//{
		//	string GetALEMemberInformationXML = "";
		//	string strXML;
		//	string strTemp = "";
		//	bool boolAll12MinCoverage = false;
		//	bool boolAll12FTCount = false;
		//	bool boolAll12TotalCount = false;
		//	bool boolAll12Group = false;
		//	bool boolAll12Section4980H = false;
		//	if (!(acaRec.ScenarioID == "5-0"))
		//	{
		//		boolAll12MinCoverage = acaRec.BaseInfo.MinimumCoverageSameAll12Months;
		//		boolAll12FTCount = acaRec.BaseInfo.FullTimeCountSameAll12Months;
		//		boolAll12TotalCount = acaRec.BaseInfo.TotalEmployeeCountSameAll12Months;
		//		boolAll12Section4980H = acaRec.BaseInfo.Section4980HSameAll12Months;
		//		boolAll12Group = acaRec.BaseInfo.InAggregatedGroupSameAll12Months;
		//	}
		//	int x;
		//	cALEMonthlyInformation minfo;
		//	strXML = "<ALEMemberInformationGrp>";
		//	if (boolAll12FTCount || boolAll12MinCoverage || boolAll12TotalCount || boolAll12Group || boolAll12Section4980H)
		//	{
		//		strXML += "<YearlyALEMemberDetail>";
		//		if (boolAll12MinCoverage)
		//		{
		//			strXML += GetYesNoTag("MinEssentialCvrOffrCd", acaRec.BaseInfo.GetMonthlyInformation(1).MinimumEssentialCoverageOffer);
		//		}
		//		if (boolAll12FTCount && acaRec.BaseInfo.GetMonthlyInformation(1).FullTimeEmployeeCount > 0)
		//		{
		//			strXML += GetSimpleTag("ALEMemberFTECnt", acaRec.BaseInfo.GetMonthlyInformation(1).FullTimeEmployeeCount.ToString());
		//		}
		//		if (boolAll12TotalCount && acaRec.BaseInfo.GetMonthlyInformation(1).TotalEmployeeCount > 0)
		//		{
		//			strXML += GetSimpleTag("TotalEmployeeCnt", acaRec.BaseInfo.GetMonthlyInformation(1).TotalEmployeeCount.ToString());
		//		}
		//		if (boolAll12Group && acaRec.BaseInfo.GetMonthlyInformation(1).AggregatedGroup)
		//		{
		//			strXML += GetBooleanTag("AggregatedGroupInd", acaRec.BaseInfo.GetMonthlyInformation(1).AggregatedGroup);
		//		}
		//		if (boolAll12Section4980H)
		//		{
		//			strXML += GetSimpleTag("ALESect4980HTrnstReliefCd", acaRec.BaseInfo.GetMonthlyInformation(1).Section4980HTransitionRelief);
		//		}
		//		strXML += "</YearlyALEMemberDetail>";
		//	}
		//	if (!(boolAll12FTCount && boolAll12MinCoverage && boolAll12TotalCount && boolAll12Group && boolAll12Section4980H))
		//	{
		//		for (x = 1; x <= 12; x++)
		//		{
		//			strTemp = GetMonthAbbrev(x);
		//			if (strTemp == "Sep")
		//			{
		//				strTemp = "Sept";
		//			}
		//			minfo = acaRec.BaseInfo.GetMonthlyInformation(x);
		//			strXML += "<" + strTemp + "ALEMonthlyInfoGrp>";
		//			if (!boolAll12MinCoverage)
		//			{
		//				strXML += GetYesNoTag("MinEssentialCvrOffrCd", minfo.MinimumEssentialCoverageOffer);
		//			}
		//			if (!boolAll12FTCount && minfo.FullTimeEmployeeCount > 0)
		//			{
		//				strXML += GetSimpleTag("ALEMemberFTECnt", FCConvert.ToString(minfo.FullTimeEmployeeCount));
		//			}
		//			if (!boolAll12TotalCount && minfo.TotalEmployeeCount > 0)
		//			{
		//				strXML += GetSimpleTag("TotalEmployeeCnt", FCConvert.ToString(minfo.TotalEmployeeCount));
		//			}
		//			if (!boolAll12Group)
		//			{
		//				strXML += GetBooleanTag("AggregatedGroupInd", minfo.AggregatedGroup);
		//			}
		//			if (!boolAll12Section4980H)
		//			{
		//				strXML += GetSimpleTag("ALESect4980HTrnstReliefCd", minfo.Section4980HTransitionRelief);
		//			}
		//			strXML += "</" + strTemp + "ALEMonthlyInfoGrp>";
		//		}
		//	}
		//	strXML += "</ALEMemberInformationGrp>";
		//	GetALEMemberInformationXML = strXML;
		//	return GetALEMemberInformationXML;
		//}

		public string ACACXMLReportToManifestXML(cACACXMLReport theReport)
		{
            return manController.ACACXMLReportToManifestXML(theReport);
        }
		
	}
}
