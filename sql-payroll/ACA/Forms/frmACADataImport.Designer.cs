//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmACADataImport.
	/// </summary>
	partial class frmACADataImport
	{
		public fecherFoundation.FCComboBox cmbSSN;
		public fecherFoundation.FCLabel lblSSN;
		public fecherFoundation.FCComboBox cmbPlanYear;
		public fecherFoundation.FCButton cmdImport;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCComboBox cmbFormats;
		public FCGrid GridFormat;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdPlaceHolder;
		public fecherFoundation.FCCheckBox chkMonths;
		public fecherFoundation.FCCheckBox chkAll12;
		public fecherFoundation.FCCheckBox chkSuffix;
		public fecherFoundation.FCCheckBox chkLast;
		public fecherFoundation.FCCheckBox chkMiddle;
		public fecherFoundation.FCCheckBox chkFirst;
		public fecherFoundation.FCCheckBox chkDOB;
		public fecherFoundation.FCCheckBox chkSSN;
		public fecherFoundation.FCCheckBox chkColEmployeeSSN;
		public fecherFoundation.FCComboBox cmbSeparator;
		public fecherFoundation.FCCheckBox chkHeader;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblFileName;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdAddNewFormat;
		public fecherFoundation.FCButton cmdSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbSSN = new fecherFoundation.FCComboBox();
			this.lblSSN = new fecherFoundation.FCLabel();
			this.cmbPlanYear = new fecherFoundation.FCComboBox();
			this.cmdImport = new fecherFoundation.FCButton();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.cmbFormats = new fecherFoundation.FCComboBox();
			this.GridFormat = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmdPlaceHolder = new fecherFoundation.FCButton();
			this.chkMonths = new fecherFoundation.FCCheckBox();
			this.chkAll12 = new fecherFoundation.FCCheckBox();
			this.chkSuffix = new fecherFoundation.FCCheckBox();
			this.chkLast = new fecherFoundation.FCCheckBox();
			this.chkMiddle = new fecherFoundation.FCCheckBox();
			this.chkFirst = new fecherFoundation.FCCheckBox();
			this.chkDOB = new fecherFoundation.FCCheckBox();
			this.chkSSN = new fecherFoundation.FCCheckBox();
			this.chkColEmployeeSSN = new fecherFoundation.FCCheckBox();
			this.cmbSeparator = new fecherFoundation.FCComboBox();
			this.chkHeader = new fecherFoundation.FCCheckBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.lblFileName = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdAddNewFormat = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridFormat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPlaceHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAll12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuffix)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkColEmployeeSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewFormat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(907, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPlanYear);
			this.ClientArea.Controls.Add(this.cmdImport);
			this.ClientArea.Controls.Add(this.cmdPreview);
			this.ClientArea.Controls.Add(this.cmbSSN);
			this.ClientArea.Controls.Add(this.lblSSN);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.cmbFormats);
			this.ClientArea.Controls.Add(this.GridFormat);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmbSeparator);
			this.ClientArea.Controls.Add(this.chkHeader);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.lblFileName);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(907, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAddNewFormat);
			this.TopPanel.Size = new System.Drawing.Size(907, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddNewFormat, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(198, 30);
			this.HeaderText.Text = "ACA Data Import";
			// 
			// cmbSSN
			// 
			this.cmbSSN.AutoSize = false;
			this.cmbSSN.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSSN.FormattingEnabled = true;
			this.cmbSSN.Items.AddRange(new object[] {
            "SSN",
            "Employee Number"});
			this.cmbSSN.Location = new System.Drawing.Point(226, 217);
			this.cmbSSN.Name = "cmbSSN";
			this.cmbSSN.Size = new System.Drawing.Size(289, 40);
			this.cmbSSN.TabIndex = 28;
			this.cmbSSN.SelectedIndexChanged += new System.EventHandler(this.optSSN_CheckedChanged);
			// 
			// lblSSN
			// 
			this.lblSSN.AutoSize = true;
			this.lblSSN.Location = new System.Drawing.Point(30, 231);
			this.lblSSN.Name = "lblSSN";
			this.lblSSN.Size = new System.Drawing.Size(147, 15);
			this.lblSSN.TabIndex = 29;
			this.lblSSN.Text = "EMPLOYEE IDENTIFIER";
			// 
			// cmbPlanYear
			// 
			this.cmbPlanYear.AutoSize = false;
			this.cmbPlanYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPlanYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPlanYear.FormattingEnabled = true;
			this.cmbPlanYear.Location = new System.Drawing.Point(160, 661);
			this.cmbPlanYear.Name = "cmbPlanYear";
			this.cmbPlanYear.Size = new System.Drawing.Size(126, 40);
			this.cmbPlanYear.TabIndex = 27;
			this.cmbPlanYear.SelectedIndexChanged += new System.EventHandler(this.cmbPlanYear_SelectedIndexChanged);
			// 
			// cmdImport
			// 
			this.cmdImport.AppearanceKey = "actionButton";
			this.cmdImport.Location = new System.Drawing.Point(276, 711);
			this.cmdImport.Name = "cmdImport";
			this.cmdImport.Size = new System.Drawing.Size(91, 40);
			this.cmdImport.TabIndex = 26;
			this.cmdImport.Text = "Import";
			this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "actionButton";
			this.cmdPreview.Location = new System.Drawing.Point(152, 711);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Size = new System.Drawing.Size(104, 40);
			this.cmdPreview.TabIndex = 25;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(30, 711);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(102, 40);
			this.cmdBrowse.TabIndex = 19;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// cmbFormats
			// 
			this.cmbFormats.AutoSize = false;
			this.cmbFormats.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFormats.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormats.FormattingEnabled = true;
			this.cmbFormats.Location = new System.Drawing.Point(226, 30);
			this.cmbFormats.Name = "cmbFormats";
			this.cmbFormats.Size = new System.Drawing.Size(289, 40);
			this.cmbFormats.TabIndex = 17;
			this.cmbFormats.SelectedIndexChanged += new System.EventHandler(this.cmbFormats_SelectedIndexChanged);
			// 
			// GridFormat
			// 
			this.GridFormat.AllowSelection = false;
			this.GridFormat.AllowUserToResizeColumns = false;
			this.GridFormat.AllowUserToResizeRows = false;
			this.GridFormat.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridFormat.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridFormat.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridFormat.BackColorBkg = System.Drawing.Color.Empty;
			this.GridFormat.BackColorFixed = System.Drawing.Color.Empty;
			this.GridFormat.BackColorSel = System.Drawing.Color.Empty;
			this.GridFormat.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridFormat.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridFormat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridFormat.ColumnHeadersHeight = 30;
			this.GridFormat.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridFormat.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridFormat.DragIcon = null;
			this.GridFormat.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridFormat.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.GridFormat.FixedCols = 0;
			this.GridFormat.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridFormat.FrozenCols = 0;
			this.GridFormat.GridColor = System.Drawing.Color.Empty;
			this.GridFormat.GridColorFixed = System.Drawing.Color.Empty;
			this.GridFormat.Location = new System.Drawing.Point(30, 472);
			this.GridFormat.Name = "GridFormat";
			this.GridFormat.OutlineCol = 0;
			this.GridFormat.ReadOnly = true;
			this.GridFormat.RowHeadersVisible = false;
			this.GridFormat.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridFormat.RowHeightMin = 0;
			this.GridFormat.Rows = 3;
			this.GridFormat.ScrollTipText = null;
			this.GridFormat.ShowColumnVisibilityMenu = false;
			this.GridFormat.ShowFocusCell = false;
			this.GridFormat.Size = new System.Drawing.Size(849, 144);
			this.GridFormat.StandardTab = true;
			this.GridFormat.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridFormat.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridFormat.TabIndex = 6;
            this.GridFormat.AfterMoveColumn += new EventHandler<AfterMoveColumnEventArgs>(GridFormat_AfterMoveColumn);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.cmdPlaceHolder);
			this.Frame1.Controls.Add(this.chkMonths);
			this.Frame1.Controls.Add(this.chkAll12);
			this.Frame1.Controls.Add(this.chkSuffix);
			this.Frame1.Controls.Add(this.chkLast);
			this.Frame1.Controls.Add(this.chkMiddle);
			this.Frame1.Controls.Add(this.chkFirst);
			this.Frame1.Controls.Add(this.chkDOB);
			this.Frame1.Controls.Add(this.chkSSN);
			this.Frame1.Controls.Add(this.chkColEmployeeSSN);
			this.Frame1.Location = new System.Drawing.Point(30, 267);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(568, 188);
			this.Frame1.TabIndex = 5;
			// 
			// cmdPlaceHolder
			// 
			this.cmdPlaceHolder.AppearanceKey = "actionButton";
			this.cmdPlaceHolder.Location = new System.Drawing.Point(0, 151);
			this.cmdPlaceHolder.Name = "cmdPlaceHolder";
			this.cmdPlaceHolder.Size = new System.Drawing.Size(223, 40);
			this.cmdPlaceHolder.TabIndex = 16;
			this.cmdPlaceHolder.Text = "Add Place Holder Field";
			this.cmdPlaceHolder.Click += new System.EventHandler(this.cmdPlaceHolder_Click);
			// 
			// chkMonths
			// 
			this.chkMonths.Location = new System.Drawing.Point(355, 37);
			this.chkMonths.Name = "chkMonths";
			this.chkMonths.Size = new System.Drawing.Size(97, 27);
			this.chkMonths.TabIndex = 15;
			this.chkMonths.Text = "Jan - Dec";
			this.chkMonths.CheckedChanged += new System.EventHandler(this.chkMonths_CheckedChanged);
			// 
			// chkAll12
			// 
			this.chkAll12.Location = new System.Drawing.Point(355, 0);
			this.chkAll12.Name = "chkAll12";
			this.chkAll12.Size = new System.Drawing.Size(126, 27);
			this.chkAll12.TabIndex = 14;
			this.chkAll12.Text = "All 12 Months";
			this.chkAll12.CheckedChanged += new System.EventHandler(this.chkAll12_CheckedChanged);
			// 
			// chkSuffix
			// 
			this.chkSuffix.Location = new System.Drawing.Point(188, 111);
			this.chkSuffix.Name = "chkSuffix";
			this.chkSuffix.Size = new System.Drawing.Size(68, 27);
			this.chkSuffix.TabIndex = 13;
			this.chkSuffix.Text = "Suffix";
			this.chkSuffix.CheckedChanged += new System.EventHandler(this.chkSuffix_CheckedChanged);
			// 
			// chkLast
			// 
			this.chkLast.Location = new System.Drawing.Point(188, 74);
			this.chkLast.Name = "chkLast";
			this.chkLast.Size = new System.Drawing.Size(106, 27);
			this.chkLast.TabIndex = 12;
			this.chkLast.Text = "Last Name";
			this.chkLast.CheckedChanged += new System.EventHandler(this.chkLast_CheckedChanged);
			// 
			// chkMiddle
			// 
			this.chkMiddle.Location = new System.Drawing.Point(188, 37);
			this.chkMiddle.Name = "chkMiddle";
			this.chkMiddle.Size = new System.Drawing.Size(124, 27);
			this.chkMiddle.TabIndex = 11;
			this.chkMiddle.Text = "Middle Name";
			this.chkMiddle.CheckedChanged += new System.EventHandler(this.chkMiddle_CheckedChanged);
			// 
			// chkFirst
			// 
			this.chkFirst.Location = new System.Drawing.Point(188, 0);
			this.chkFirst.Name = "chkFirst";
			this.chkFirst.Size = new System.Drawing.Size(107, 27);
			this.chkFirst.TabIndex = 10;
			this.chkFirst.Text = "First Name";
			this.chkFirst.CheckedChanged += new System.EventHandler(this.chkFirst_CheckedChanged);
			// 
			// chkDOB
			// 
			this.chkDOB.Location = new System.Drawing.Point(0, 74);
			this.chkDOB.Name = "chkDOB";
			this.chkDOB.Size = new System.Drawing.Size(118, 27);
			this.chkDOB.TabIndex = 9;
			this.chkDOB.Text = "Date of Birth";
			this.chkDOB.CheckedChanged += new System.EventHandler(this.chkDOB_CheckedChanged);
			// 
			// chkSSN
			// 
			this.chkSSN.Location = new System.Drawing.Point(0, 37);
			this.chkSSN.Name = "chkSSN";
			this.chkSSN.Size = new System.Drawing.Size(127, 27);
			this.chkSSN.TabIndex = 8;
			this.chkSSN.Text = "Covered SSN";
			this.chkSSN.CheckedChanged += new System.EventHandler(this.chkSSN_CheckedChanged);
			// 
			// chkColEmployeeSSN
			// 
			this.chkColEmployeeSSN.Location = new System.Drawing.Point(0, 0);
			this.chkColEmployeeSSN.Name = "chkColEmployeeSSN";
			this.chkColEmployeeSSN.Size = new System.Drawing.Size(168, 27);
			this.chkColEmployeeSSN.TabIndex = 7;
			this.chkColEmployeeSSN.Text = "Employee Identifier";
			this.chkColEmployeeSSN.CheckedChanged += new System.EventHandler(this.chkColEmployeeSSN_CheckedChanged);
			// 
			// cmbSeparator
			// 
			this.cmbSeparator.AutoSize = false;
			this.cmbSeparator.BackColor = System.Drawing.SystemColors.Window;
			this.cmbSeparator.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSeparator.FormattingEnabled = true;
			this.cmbSeparator.Location = new System.Drawing.Point(226, 167);
			this.cmbSeparator.Name = "cmbSeparator";
			this.cmbSeparator.Size = new System.Drawing.Size(289, 40);
			this.cmbSeparator.TabIndex = 3;
			this.cmbSeparator.SelectedIndexChanged += new System.EventHandler(this.cmbSeparator_SelectedIndexChanged);
			// 
			// chkHeader
			// 
			this.chkHeader.Location = new System.Drawing.Point(30, 130);
			this.chkHeader.Name = "chkHeader";
			this.chkHeader.Size = new System.Drawing.Size(384, 27);
			this.chkHeader.TabIndex = 1;
			this.chkHeader.Text = "First line is column headers or non-readable data";
			this.chkHeader.CheckedChanged += new System.EventHandler(this.chkHeader_CheckedChanged);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(226, 80);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(289, 40);
			this.txtDescription.TabIndex = 0;
			this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 675);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(71, 15);
			this.Label5.TabIndex = 28;
			this.Label5.Text = "PLAN YEAR";
			// 
			// lblFileName
			// 
			this.lblFileName.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblFileName.Location = new System.Drawing.Point(160, 636);
			this.lblFileName.Name = "lblFileName";
			this.lblFileName.Size = new System.Drawing.Size(719, 25);
			this.lblFileName.TabIndex = 24;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 636);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(71, 15);
			this.Label3.TabIndex = 23;
			this.Label3.Text = "FILE NAME";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 44);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(79, 16);
			this.Label4.TabIndex = 18;
			this.Label4.Text = "FILE FORMAT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 181);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(71, 16);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "SEPARATOR";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 94);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(136, 15);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "FORMAT DESCRIPTION";
			// 
			// cmdAddNewFormat
			// 
			this.cmdAddNewFormat.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddNewFormat.AppearanceKey = "toolbarButton";
			this.cmdAddNewFormat.Location = new System.Drawing.Point(759, 29);
			this.cmdAddNewFormat.Name = "cmdAddNewFormat";
			this.cmdAddNewFormat.Size = new System.Drawing.Size(120, 24);
			this.cmdAddNewFormat.TabIndex = 0;
			this.cmdAddNewFormat.Text = "Add New Format";
			this.cmdAddNewFormat.Click += new System.EventHandler(this.mnuAddNewFormat_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(276, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmACADataImport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(907, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmACADataImport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "ACA Data Import";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmACADataImport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmACADataImport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridFormat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPlaceHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAll12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuffix)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkColEmployeeSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewFormat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}

        private void GridFormat_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}