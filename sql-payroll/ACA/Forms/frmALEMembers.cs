﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmALEMembers : BaseForm
	{
		public frmALEMembers()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmALEMembers InstancePtr
		{
			get
			{
				return (frmALEMembers)Sys.GetInstance(typeof(frmALEMembers));
			}
		}

		protected frmALEMembers _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cGenericCollection memberList;
		private cALEMembersView theView;
		private bool boolRefreshing;

		public void Init(cGenericCollection groupMembers)
		{
			memberList = groupMembers;
			theView = new cALEMembersView();
			theView.SetMembers(ref memberList);
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmALEMembers_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmALEMembers_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmALEMembers properties;
			//frmALEMembers.FillStyle	= 0;
			//frmALEMembers.ScaleWidth	= 5880;
			//frmALEMembers.ScaleHeight	= 4305;
			//frmALEMembers.LinkTopic	= "Form2";
			//frmALEMembers.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			RefreshFromView();
		}

		private void SetupGrid()
		{
			boolRefreshing = true;
			gridMembers.Cols = 3;
			gridMembers.ColHidden(0, true);
			gridMembers.Rows = 1;
			gridMembers.TextMatrix(0, 1, "Name");
			gridMembers.TextMatrix(0, 2, "EIN");
			boolRefreshing = false;
		}

		private void ResizeGrid()
		{
			int lngGridWidth;
			lngGridWidth = gridMembers.WidthOriginal;
			gridMembers.ColWidth(1, FCConvert.ToInt32(lngGridWidth * 0.85));
		}

		private void RefreshFromView()
		{
			boolRefreshing = true;
			gridMembers.Rows = 1;
			theView.Members.MoveFirst();
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			cAggregatedMember aMember;
			while (theView.Members.IsCurrent())
			{
				gridMembers.Rows += 1;
				intRow = gridMembers.Rows - 1;
				aMember = (cAggregatedMember)theView.Members.GetCurrentItem();
				gridMembers.TextMatrix(intRow, 0, FCConvert.ToString(aMember.ID));
				gridMembers.TextMatrix(intRow, 1, aMember.Name);
				gridMembers.TextMatrix(intRow, 2, aMember.EIN);
				theView.Members.MoveNext();
			}
			boolRefreshing = false;
		}

		private void frmALEMembers_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}
		// vbPorter upgrade warning: Row As int	OnRead
		private void gridMembers_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (!boolRefreshing)
			{
				if (gridMembers.Row > 0)
				{
					cAggregatedMember aMember;
					// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
					int intRow = 0;
					intRow = gridMembers.Row;
					aMember = (cAggregatedMember)theView.Members.GetItemByIndex(intRow);
					if (gridMembers.Col == 1)
					{
						aMember.Name = gridMembers.TextMatrix(gridMembers.Row, 1);
					}
					else if (gridMembers.Col == 2)
					{
						aMember.EIN = gridMembers.TextMatrix(gridMembers.Row, 2);
					}
				}
			}
		}

		private void gridMembers_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				AddMember();
			}
			else if (e.KeyCode == Keys.Delete)
			{
				RemoveMember();
			}
		}

		private void mnuAddMember_Click(object sender, System.EventArgs e)
		{
			AddMember();
		}

		private void AddMember()
		{
			theView.CreateMember("", "");
			RefreshFromView();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuRemoveMember_Click(object sender, System.EventArgs e)
		{
			RemoveMember();
		}

		private void RemoveMember()
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			intRow = gridMembers.Row;
			if (intRow > 0)
			{
				theView.RemoveMemberByIndex(intRow);
			}
			RefreshFromView();
		}
	}
}
