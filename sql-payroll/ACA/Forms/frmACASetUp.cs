﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmACASetUp : BaseForm
	{
		public frmACASetUp()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            GridZip.CurrentCellChanged += GridZip_CurrentCellChanged;
            GridZip.CellValidating += GridZip_CellValidating;
		}

        private void GridZip_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (GridZip.Col == 0)
            {
                if (GridZip.EditText.Trim() != "")
                {
                    int x;
                    for (x = 1; x <= 12; x++)
                    {
                        GridZip.TextMatrix(0, x, GridZip.EditText);
                    }
                    // x
                }
            }
        }

        private void GridZip_CurrentCellChanged(object sender, EventArgs e)
        {
            if (GridZip.Col == GridZip.Cols - 1)
            {
                GridZip.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                GridZip.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmACASetUp InstancePtr
		{
			get
			{
				return (frmACASetUp)Sys.GetInstance(typeof(frmACASetUp));
			}
		}

		protected frmACASetUp _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cACASetupView theView = new cACASetupView();
		const int CNSTDepGridColID = 0;
		const int CNSTDepGridColFirstName = 1;
		const int CNSTDepGridColMiddleName = 2;
		const int CNSTDepGridColLastName = 3;
		const int CNSTDepGridColSuffix = 4;
		const int CNSTDepGridColSSN = 5;
		const int CNSTDepGridColDOB = 6;
		const int CNSTDepGridColTerminationDate = 7;
		const int CNSTDepGridColAll12 = 8;
		private int intReportYear;
		private bool boolRefreshing;
		// vbPorter upgrade warning: intYearToShow As int	OnWriteFCConvert.ToInt32(
		public void Init(int intYearToShow)
		{
			intReportYear = intYearToShow;
			this.Text = "Employee Health Care - " + FCConvert.ToString(intReportYear);
			this.Show(App.MainForm);
			ChooseEmployee();
			if (theView.CurrentRecord == null)
			{
				Close();
			}
			else
			{
				if (theView.CurrentRecord.EmployeeID < 1)
				{
					Close();
				}
			}
		}

		private void cmdNewDependent_Click()
		{
			AddDependent();
		}

		private void AddDependent()
		{
			gridDependents.Rows += 1;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			intRow = (gridDependents.Rows - 1);
			gridDependents.TextMatrix(intRow, CNSTDepGridColID, FCConvert.ToString(0));
			gridDependents.TextMatrix(intRow, CNSTDepGridColFirstName, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColDOB, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColLastName, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColMiddleName, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColSSN, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColSuffix, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColTerminationDate, "");
			gridDependents.TextMatrix(intRow, CNSTDepGridColAll12, FCConvert.ToString(false));
			int x;
			for (x = 1; x <= 12; x++)
			{
				gridDependents.TextMatrix(intRow, x + 8, FCConvert.ToString(false));
			}
            // x
            //FC:FINAL:BSE:#4243 increase grid size 
            gridDependents.Height += 34;
		}

		private void DeleteDependent()
		{
			int lngRow;
			lngRow = gridDependents.Row;
			if (lngRow > 0)
			{
				int lngID = 0;
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridDependents.TextMatrix(lngRow, CNSTDepGridColID))));
				cACAEmployeeDependent dep;
				if (lngID > 0)
				{
					dep = new cACAEmployeeDependent();
					dep.ID = lngID;
					dep.Deleted = true;
					theView.UpdateDependent(ref dep);
				}
				gridDependents.RemoveItem(lngRow);
			}
		}

		private void Command1_Click()
		{
			theView.LoadSetupByEmployee(1, intReportYear);
			ShowCurrent();
		}

		private void Command2_Click()
		{
			SaveCurrent();
		}

		private void frmACASetUp_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmACASetUp_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrids();
			SetupCombo();
			SetupPlanCombo();
			ResizeGrids();
		}

		private void SetupCombo()
		{
			cmbOriginOfPolicy.Clear();
			cmbOriginOfPolicy.AddItem("A. Small Business Health Options Program (SHOP)");
			cmbOriginOfPolicy.AddItem("B. Employer-sponsored coverage");
			cmbOriginOfPolicy.AddItem("C. Government-sponsored program");
			cmbOriginOfPolicy.AddItem("D. Individual market insurance");
			cmbOriginOfPolicy.AddItem("E. Multiemployer plan");
			cmbOriginOfPolicy.AddItem("F. Other designated minimum essential coverage");
			cmbOriginOfPolicy.AddItem("G. Individual Coverage Health Reimbursement Arrangement (ICHRA)");
		}

		private void SetupPlanCombo()
		{
			cmbPlan.Clear();
			cGenericCollection planList;
			planList = theView.CoveragePlans;
			planList.MoveFirst();
			cmbPlan.AddItem("None");
			cmbPlan.ItemData(0, 0);
			cCoverageProvider CoverageProvider;
			while (planList.IsCurrent())
			{
				CoverageProvider = (cCoverageProvider)planList.GetCurrentItem();
				cmbPlan.AddItem(CoverageProvider.PlanName);
				cmbPlan.ItemData(cmbPlan.NewIndex, CoverageProvider.ID);
				planList.MoveNext();
			}
		}

		private void frmACASetUp_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void gridBox14_RowColChange(object sender, System.EventArgs e)
		{
			if (gridBox14.Col == gridBox14.Cols - 1)
			{
				gridBox14.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				gridBox14.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void gridBox14_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (gridBox14.Col == 0)
			{
				if (fecherFoundation.Strings.Trim(gridBox14.EditText) != "")
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						gridBox14.TextMatrix(0, x, gridBox14.EditText);
					}
					// x
				}
			}
		}

		private void gridBox15_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (gridBox15.Col == 0)
			{
				if (fecherFoundation.Strings.Trim(gridBox15.EditText) != "")
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						gridBox15.TextMatrix(0, x, gridBox15.EditText);
					}
					// x
				}
			}
		}

		private void gridBox16_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (gridBox16.Col == 0)
			{
				if (fecherFoundation.Strings.Trim(gridBox16.EditText) != "")
				{
					int x;
					for (x = 1; x <= 12; x++)
					{
						gridBox16.TextMatrix(0, x, gridBox16.EditText);
					}
					// x
				}
			}
		}

		private void gridBox15_RowColChange(object sender, System.EventArgs e)
		{
			if (gridBox15.Col == gridBox15.Cols - 1)
			{
				gridBox15.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				gridBox15.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void gridBox16_RowColChange(object sender, System.EventArgs e)
		{
			if (gridBox16.Col == gridBox16.Cols - 1)
			{
				gridBox16.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				gridBox16.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void gridDependents_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (gridDependents.Col == CNSTDepGridColAll12 && !boolRefreshing && gridDependents.Row > 0)
			{
				bool boolValue;
				boolRefreshing = true;
				int x;
                //FC:FINAL:AM:#4115 - TextMatrix doesn't return the updated value on CellChanged
				//if (FCConvert.CBool(gridDependents.TextMatrix(gridDependents.Row, CNSTDepGridColAll12)))
                if(FCConvert.CBool(gridDependents[e.ColumnIndex, e.RowIndex].Value))
				{
					for (x = 1; x <= 12; x++)
					{
						gridDependents.TextMatrix(gridDependents.Row, x + 8, FCConvert.ToString(true));
					}
					// x
				}
				else
				{
					for (x = 1; x <= 12; x++)
					{
						gridDependents.TextMatrix(gridDependents.Row, x + 8, FCConvert.ToString(false));
					}
					// x
				}
				boolRefreshing = false;
			}
		}

		private void gridDependents_KeyUpEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						AddDependent();
						break;
					}
				case Keys.Delete:
					{
						DeleteDependent();
						break;
					}
			}
			//end switch
		}

		private void mnuAddDependent_Click(object sender, System.EventArgs e)
		{
			AddDependent();
		}

		private void mnuDeleteDependent_Click(object sender, System.EventArgs e)
		{
			DeleteDependent();
		}

		private void mnuDeleteRecord_Click(object sender, System.EventArgs e)
		{
			DeleteCurrent();
			ShowCurrent();
		}

		private void mnuEmployee_Click(object sender, System.EventArgs e)
		{
			ChooseEmployee();
		}

		private void ChooseEmployee()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				int lngID;
				lngID = frmEmployeeSearch.InstancePtr.Init(true);
				/*- frmEmployeeSearch = null; */
				if (lngID > 0)
				{
					LoadSetupByEmployee(lngID, intReportYear);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ChooseEmployee line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadSetup(int lngID)
		{
			theView.LoadSetup(lngID);
			ShowCurrent();
		}

		private void LoadSetupByEmployee(int lngID, int lngYear)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				theView.LoadSetupByEmployee(lngID, lngYear);
				if (theView.LastErrorNumber > 0)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				ShowCurrent();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadSetupByEmployee line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowCurrent()
		{
			// VB6 Bad Scope Dim:
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolRefreshing = true;
				cACAEmployeeSetup tempRec;
				tempRec = theView.CurrentRecord;
				if (!(tempRec == null))
				{
					if (tempRec.EmployeeID > 0)
					{
						gridDependents.Rows = 1;
						cEmployee emp;
						cEmployeeService empServ = new cEmployeeService();
						emp = empServ.GetEmployeeByID(theView.CurrentRecord.EmployeeID);
						lblEmployeeName.Text = emp.FullName;
						lblEmployeeNumber.Text = emp.EmployeeNumber;
						theView.CurrentRecord.EmployeeNumber = emp.EmployeeNumber;
						if (theView.CurrentRecord.CoverageDeclined)
						{
							chkCoverageDeclined.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkCoverageDeclined.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "a")
						{
							cmbOriginOfPolicy.SelectedIndex = 0;
						}
						else if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "b")
						{
							cmbOriginOfPolicy.SelectedIndex = 1;
						}
						else if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "c")
						{
							cmbOriginOfPolicy.SelectedIndex = 2;
						}
						else if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "d")
						{
							cmbOriginOfPolicy.SelectedIndex = 3;
						}
						else if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "e")
						{
							cmbOriginOfPolicy.SelectedIndex = 4;
						}
						else if (fecherFoundation.Strings.LCase(theView.CurrentRecord.OriginOfPolicy) == "f")
						{
							cmbOriginOfPolicy.SelectedIndex = 5;
						}
						else
						{
							cmbOriginOfPolicy.SelectedIndex = -1;
						}
						cmbPlan.SelectedIndex = 0;
						for (x = 0; x <= cmbPlan.Items.Count - 1; x++)
						{
							if (cmbPlan.ItemData(x) == theView.CurrentRecord.CoverageProviderID)
							{
								cmbPlan.SelectedIndex = x;
								break;
							}
						}
						// x
						for (x = 1; x <= 12; x++)
						{
							gridBox14.TextMatrix(0, x, theView.CurrentRecord.GetCoverageRequiredCodeForMonth(x));
							gridBox15.TextMatrix(0, x, FCConvert.ToString(theView.CurrentRecord.GetEmployeeShareLowestPremiumForMonth(x)));
							gridBox16.TextMatrix(0, x, theView.CurrentRecord.GetSafeHarborCodeForMonth(x));
                            GridZip.TextMatrix(0, x, theView.CurrentRecord.GetZipCodeForMonth(x));
						}
						// x
						// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
						int intRow;
						foreach (cACAEmployeeDependent dep in theView.CurrentRecord.Dependents)
						{
							if (!dep.Deleted)
							{
								gridDependents.Rows += 1;
								intRow = (gridDependents.Rows - 1);
								gridDependents.TextMatrix(intRow, CNSTDepGridColID, FCConvert.ToString(dep.ID));
								gridDependents.TextMatrix(intRow, CNSTDepGridColDOB, dep.DateOfBirth);
								gridDependents.TextMatrix(intRow, CNSTDepGridColFirstName, dep.FirstName);
								gridDependents.TextMatrix(intRow, CNSTDepGridColLastName, dep.LastName);
								gridDependents.TextMatrix(intRow, CNSTDepGridColMiddleName, dep.MiddleName);
								gridDependents.TextMatrix(intRow, CNSTDepGridColSSN, dep.SSN);
								gridDependents.TextMatrix(intRow, CNSTDepGridColSuffix, dep.Suffix);
								gridDependents.TextMatrix(intRow, CNSTDepGridColTerminationDate, dep.HealthCareTerminationDate);
								if (dep.CoveredAll12Months)
								{
									gridDependents.TextMatrix(intRow, CNSTDepGridColAll12, FCConvert.ToString(true));
								}
								else
								{
									gridDependents.TextMatrix(intRow, CNSTDepGridColAll12, FCConvert.ToString(false));
								}
								for (x = 1; x <= 12; x++)
								{
									gridDependents.TextMatrix(intRow, x + 8, FCConvert.ToString(dep.GetIsCoveredForMonth(x)));
								}
								// x
							}
						}
						boolRefreshing = false;
						return;
					}
				}
				gridDependents.Rows = 1;
				for (x = 1; x <= 12; x++)
				{
					gridBox14.TextMatrix(0, x, "");
					gridBox15.TextMatrix(0, x, "");
					gridBox16.TextMatrix(0, x, "");
                    GridZip.TextMatrix(0, x, "");
				}
				// x
				lblEmployeeName.Text = "";
				lblEmployeeNumber.Text = "";
				boolRefreshing = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ShowCurrent line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void DeleteCurrent()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(theView.CurrentRecord == null))
				{
					theView.RemoveCurrent();
					ChooseEmployee();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveCurrent()
		{
			bool SaveCurrent = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				gridBox14.Row = -1;
				gridBox16.Row = -1;
                GridZip.Row = -1;
				gridDependents.Row = -1;
				if (!(theView.CurrentRecord == null))
				{
					UpdateViewModel();
					theView.SaveSetup();
				}
				SaveCurrent = true;
				MessageBox.Show("Information Saved", "Save Health Care Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveCurrent;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCurrent;
		}

		private void UpdateViewModel()
		{
			if (!(theView.CurrentRecord == null))
			{
				cACAEmployeeSetup curRec;
				string strTemp = "";
				curRec = theView.CurrentRecord;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				for (x = 1; x <= 12; x++)
				{
					curRec.SetCoverageRequiredCodeForMonth(x, fecherFoundation.Strings.Trim(gridBox14.TextMatrix(0, x)));
					curRec.SetEmployeeShareLowestPremiumForMonth(x, Conversion.Val(fecherFoundation.Strings.Trim(gridBox15.TextMatrix(0, x))));
					curRec.SetSafeHarborCodeForMonth(x, fecherFoundation.Strings.Trim(gridBox16.TextMatrix(0, x)));
                    curRec.SetZipCodeForMonth(x, GridZip.TextMatrix(0, x).Trim());
				}
				// x
				curRec.CoverageDeclined = chkCoverageDeclined.CheckState == Wisej.Web.CheckState.Checked;
				curRec.CoverageProviderID = cmbPlan.ItemData(cmbPlan.SelectedIndex);
				cACAEmployeeDependent dep;
				int Y;
				if (cmbOriginOfPolicy.SelectedIndex >= 0)
				{
					curRec.OriginOfPolicy = Strings.Left(cmbOriginOfPolicy.Text, 1);
				}
				else
				{
					curRec.OriginOfPolicy = "";
				}
				for (x = 1; x <= (gridDependents.Rows - 1); x++)
				{
					dep = new cACAEmployeeDependent();
					dep.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridDependents.TextMatrix(x, CNSTDepGridColID))));
					dep.FirstName = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColFirstName));
					dep.MiddleName = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColMiddleName));
					dep.LastName = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColLastName));
					dep.Suffix = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColSuffix));
					strTemp = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColSSN));
					if (strTemp.Length != 11)
					{
						strTemp = "";
					}
					dep.SSN = strTemp;
					strTemp = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColDOB));
					if (strTemp != "")
					{
						if (Information.IsDate(strTemp))
						{
							dep.DateOfBirth = strTemp;
						}
					}
					strTemp = fecherFoundation.Strings.Trim(gridDependents.TextMatrix(x, CNSTDepGridColTerminationDate));
					if (strTemp != "")
					{
						if (Information.IsDate(strTemp))
						{
							dep.HealthCareTerminationDate = strTemp;
						}
					}
					for (Y = 1; Y <= 12; Y++)
					{
						dep.SetIsCoveredForMonth(Y, FCConvert.CBool(gridDependents.TextMatrix(x, Y + 8)));
					}
					// Y
					theView.UpdateDependent(ref dep);
				}
				// x
			}
		}

		private void SetupGrids()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strBox14Combo;
			string strBox16Combo;
			strBox14Combo = "|1A|1B|1C|1D|1E|1F|1G|1H|1I|1L|1M|1N|1O|1P|1Q|1R|1S";
			strBox16Combo = "|2A|2B|2C|2D|2E|2F|2G|2H|2I";
			gridBox14.ComboList = strBox14Combo;
			gridBox16.ComboList = strBox16Combo;
			for (x = 0; x <= (gridBox15.Cols - 1); x++)
			{
				gridBox15.ColDataType(x, FCGrid.DataTypeSettings.flexDTDouble);
                gridBox15.ColAlignment(x, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                GridZip.ColAlignment(x, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
            
			// x
			SetupDependentsGrid();
		}

		private void SetupDependentsGrid()
		{
			int x;
			gridDependents.TextMatrix(0, CNSTDepGridColFirstName, "First Name");
			gridDependents.TextMatrix(0, CNSTDepGridColMiddleName, "Middle Name");
			gridDependents.TextMatrix(0, CNSTDepGridColLastName, "Last Name");
			gridDependents.TextMatrix(0, CNSTDepGridColSSN, "SSN");
			gridDependents.TextMatrix(0, CNSTDepGridColSuffix, "Sfx");
			gridDependents.TextMatrix(0, CNSTDepGridColDOB, "DOB");
			gridDependents.TextMatrix(0, CNSTDepGridColTerminationDate, "Term Date");
			gridDependents.ColEditMask(CNSTDepGridColDOB, "90/90/0000");
			gridDependents.ColEditMask(CNSTDepGridColTerminationDate, "90/90/0000");
			gridDependents.ColEditMask(CNSTDepGridColSSN, "###-##-####");
			gridDependents.ColDataType(CNSTDepGridColAll12, FCGrid.DataTypeSettings.flexDTBoolean);
			gridDependents.ColAlignment(CNSTDepGridColAll12, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			gridDependents.TextMatrix(0, CNSTDepGridColAll12, "All 12");
			gridDependents.ColHidden(CNSTDepGridColTerminationDate, true);
			for (x = 1; x <= 12; x++)
			{
				gridDependents.ColDataType(x + 8, FCGrid.DataTypeSettings.flexDTBoolean);
				gridDependents.ColAlignment(x + 8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				gridDependents.TextMatrix(0, x + 8, FCConvert.ToString(MonthDescription(x)));
			}
			// x
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object MonthDescription(int intMonth)
		{
			object MonthDescription = null;
			string strReturn = "";
			switch (intMonth)
			{
				case 0:
					{
						break;
					}
				case 1:
					{
						strReturn = "Jan";
						break;
					}
				case 2:
					{
						strReturn = "Feb";
						break;
					}
				case 3:
					{
						strReturn = "Mar";
						break;
					}
				case 4:
					{
						strReturn = "Apr";
						break;
					}
				case 5:
					{
						strReturn = "May";
						break;
					}
				case 6:
					{
						strReturn = "Jun";
						break;
					}
				case 7:
					{
						strReturn = "Jul";
						break;
					}
				case 8:
					{
						strReturn = "Aug";
						break;
					}
				case 9:
					{
						strReturn = "Sep";
						break;
					}
				case 10:
					{
						strReturn = "Oct";
						break;
					}
				case 11:
					{
						strReturn = "Nov";
						break;
					}
				case 12:
					{
						strReturn = "Dec";
						break;
					}
			}
			//end switch
			MonthDescription = strReturn;
			return MonthDescription;
		}

		private void ResizeGrids()
		{
			int x;
			double GridWidth;
			GridWidth = gridBox14.WidthOriginal;
			for (x = 0; x <= 12; x++)
			{
				gridBox14.ColWidth(x, FCConvert.ToInt32(0.0765 * GridWidth));
				gridBox15.ColWidth(x, FCConvert.ToInt32(0.0765 * GridWidth));
				gridBox16.ColWidth(x, FCConvert.ToInt32(0.0765 * GridWidth));
                GridZip.ColWidth(x, FCConvert.ToInt32(0.0765 * GridWidth));

            }

			int lngLeft = 0;
			int Y;
			for (x = 1; x <= 12; x++)
			{
				foreach (Control obj in this.GetAllControls())
				{
					if (fecherFoundation.Strings.LCase(obj.Name) == "lblmonth" + FCConvert.ToString(x))
					{
						lngLeft = gridBox14.LeftOriginal;
						for (Y = 0; Y <= x - 1; Y++)
						{
							lngLeft += gridBox14.ColWidth(Y);
						}
						// Y
						(obj as FCLabel).LeftOriginal = lngLeft;
						(obj as FCLabel).WidthOriginal = gridBox14.ColWidth(x);
					}
				}
			}
			// x
			lblAll12.LeftOriginal = gridBox14.LeftOriginal;
			lblAll12.WidthOriginal = gridBox14.ColWidth(0);
			ResizeDependentsGrid();
		}

		private void ResizeDependentsGrid()
		{
			double GridWidth;
			GridWidth = gridDependents.WidthOriginal;
			int x;
			gridDependents.ColWidth(0, (false ? -1 : 0));
			gridDependents.ColWidth(CNSTDepGridColFirstName, FCConvert.ToInt32(0.108 * GridWidth));
			gridDependents.ColWidth(CNSTDepGridColMiddleName, FCConvert.ToInt32(0.107 * GridWidth));
			gridDependents.ColWidth(CNSTDepGridColLastName, FCConvert.ToInt32(0.108 * GridWidth));
			gridDependents.ColWidth(CNSTDepGridColSuffix, FCConvert.ToInt32(0.03 * GridWidth));
			gridDependents.ColWidth(CNSTDepGridColSSN, FCConvert.ToInt32(0.09 * GridWidth));
			gridDependents.ColWidth(CNSTDepGridColDOB, FCConvert.ToInt32(0.09 * GridWidth));
			// gridDependents.ColWidth(CNSTDepGridColTerminationDate) = 0.071 * GridWidth
			gridDependents.ColWidth(CNSTDepGridColAll12, FCConvert.ToInt32(0.04 * GridWidth));
			for (x = 1; x <= 12; x++)
			{
				gridDependents.ColWidth(x + 8, FCConvert.ToInt32(0.033 * GridWidth));
			}
			// x
			// gridDependents.ColWidth(8) = 0
		}

		private void mnuPrintAll_Click(object sender, System.EventArgs e)
		{
			cACASetupList lSetup;
			cACAService sACA = new cACAService();
			lSetup = sACA.GetAll(intReportYear);
			if (!(lSetup == null))
			{
				if (!(lSetup.ItemCount() < 1))
				{
					rptHealthCareSetup.InstancePtr.Init(ref lSetup);
					return;
				}
			}
			MessageBox.Show("No set up records were found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		private void mnuPrintSetup_Click(object sender, System.EventArgs e)
		{
			cACASetupList lSetup = new cACASetupList();
			if (!(theView.CurrentRecord == null))
			{
				lSetup.AddItem(theView.CurrentRecord);
			}
			rptHealthCareSetup.InstancePtr.Init(ref lSetup);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveCurrent();
			ShowCurrent();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveCurrent())
			{
				Close();
			}
			else
			{
				ShowCurrent();
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
