﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frm1094C : BaseForm
	{
		public frm1094C()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frm1094C InstancePtr
		{
			get
			{
				return (frm1094C)Sys.GetInstance(typeof(frm1094C));
			}
		}

		protected frm1094C _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private c1094CView theView;
		private bool boolRefreshing;
		private cSettingsController setCont = new cSettingsController();

		private void chk4980HRelief_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().Section4980HRelief = chk4980HRelief.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chk98PercentMethod_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().NinetyEightPercentOfferMethod = chk98PercentMethod.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkAuthoritative_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().IsAuthoritativeTransmittal = chkAuthoritative.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkInGroup_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().MemberOfAggregatedGroup = chkInGroup.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkQualifyingOfferMethod_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().QualifyingOfferMethod = chkQualifyingOfferMethod.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkQualifyingOfferTransition_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().QualifyingOfferTransitionRelief = chkQualifyingOfferTransition.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void cmdALEMembers_Click(object sender, System.EventArgs e)
		{
			frmALEMembers.InstancePtr.Init(theView.GetCurrentForm().AggregatedMembers);
		}

		private void cmdMonthly_Click(object sender, System.EventArgs e)
		{
			framMonthly.Visible = true;
			//Support.ZOrder(framMonthly, 0);
		}

		private void cmdMonthlyOK_Click(object sender, System.EventArgs e)
		{
			framMonthly.Visible = false;
		}

		private void frm1094C_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frm1094C_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frm1094C properties;
			//frm1094C.FillStyle	= 0;
			//frm1094C.ScaleWidth	= 9300;
			//frm1094C.ScaleHeight	= 7830;
			//frm1094C.LinkTopic	= "Form2";
			//frm1094C.LockControls	= -1  'True;
			//frm1094C.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public void Init(int intYearToShow)
		{
			theView = new c1094CView();
			theView.LoadForm(intYearToShow);
			SetupGrid();
			RefreshFromView();
			this.Show(App.MainForm);
		}

		private void SetupGrid()
		{
			gridMonthly.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
			gridMonthly.ColDataType(2, FCGrid.DataTypeSettings.flexDTLong);
			gridMonthly.ColDataType(3, FCGrid.DataTypeSettings.flexDTLong);
			gridMonthly.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
			gridMonthly.TextMatrix(0, 1, "Minimum Coverage Offer");
			gridMonthly.TextMatrix(0, 2, "Full Time Count");
			gridMonthly.TextMatrix(0, 3, "Total Employees");
			gridMonthly.TextMatrix(0, 4, "Aggregated Group");
			gridMonthly.TextMatrix(0, 5, "4980H Transition Relief");
			gridMonthly.TextMatrix(1, 0, "All 12");
			gridMonthly.TextMatrix(2, 0, "Jan");
			gridMonthly.TextMatrix(3, 0, "Feb");
			gridMonthly.TextMatrix(4, 0, "Mar");
			gridMonthly.TextMatrix(5, 0, "Apr");
			gridMonthly.TextMatrix(6, 0, "May");
			gridMonthly.TextMatrix(7, 0, "Jun");
			gridMonthly.TextMatrix(8, 0, "Jul");
			gridMonthly.TextMatrix(9, 0, "Aug");
			gridMonthly.TextMatrix(10, 0, "Sep");
			gridMonthly.TextMatrix(11, 0, "Oct");
			gridMonthly.TextMatrix(12, 0, "Nov");
			gridMonthly.TextMatrix(13, 0, "Dec");
			gridMonthly.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridMonthly.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			int lngGridWidth;
			lngGridWidth = gridMonthly.WidthOriginal;
			gridMonthly.ColWidth(0, FCConvert.ToInt32(0.1 * lngGridWidth));
			gridMonthly.ColWidth(1, FCConvert.ToInt32(0.2 * lngGridWidth));
			gridMonthly.ColWidth(2, FCConvert.ToInt32(0.16 * lngGridWidth));
			gridMonthly.ColWidth(3, FCConvert.ToInt32(0.16 * lngGridWidth));
			gridMonthly.ColWidth(4, FCConvert.ToInt32(0.16 * lngGridWidth));
			//int lngHeight;
			//lngHeight = gridMonthly.RowHeight(0);
			//gridMonthly.Height = lngHeight * gridMonthly.Rows + 5;
		}

		private void RefreshFromView()
		{
			string strTemp;
			strTemp = setCont.GetSettingValue("1094cVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSVertical.Text = strTemp;
			}
			else
			{
				txtIRSVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1094cHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSHorizontal.Text = strTemp;
			}
			else
			{
				txtIRSHorizontal.Text = "0";
			}
			if (theView.IsCurrentForm)
			{
				boolRefreshing = true;
				c1094C theForm;
				theForm = theView.GetCurrentForm();
				this.Text = "1094C - " + FCConvert.ToString(theForm.YearCovered);
				txtAddress.Text = theForm.Address;
				txtCity.Text = theForm.City;
				txtContact.Text = theForm.ContactName;
				txtContactLast.Text = theForm.ContactLastName;
				txtContactMiddle.Text = theForm.ContactMiddleName;
				txtSuffix.Text = theForm.ContactSuffix;
				txtEIN.Text = theForm.EIN;
				txtEmployer.Text = theForm.EmployerName;
				txtPostalCode.Text = theForm.PostalCode;
				txtState.Text = theForm.State;
				txtTelephone.Text = theForm.ContactTelephone;
				txtTransmittedForms.Text = FCConvert.ToString(theForm.Total1095CTransmitted);
				txtFiled.Text = FCConvert.ToString(theForm.Total1095CForMember);
				if (theForm.Section4980HRelief)
				{
					chk4980HRelief.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chk4980HRelief.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (theForm.QualifyingOfferMethod)
				{
					chkQualifyingOfferMethod.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkQualifyingOfferMethod.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (theForm.QualifyingOfferTransitionRelief)
				{
					chkQualifyingOfferTransition.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkQualifyingOfferTransition.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (theForm.NinetyEightPercentOfferMethod)
				{
					chk98PercentMethod.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chk98PercentMethod.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (theForm.IsAuthoritativeTransmittal)
				{
					chkAuthoritative.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkAuthoritative.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (theForm.MemberOfAggregatedGroup)
				{
					chkInGroup.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkInGroup.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				FillGrid();
				boolRefreshing = false;
			}
		}

		private void FillGrid()
		{
			cALEMonthlyInformation minfo;
			int x;
			// gridMonthly.TextMatrix(1, 1) = False
			// gridMonthly.TextMatrix(1, 2) = ""
			// gridMonthly.TextMatrix(1, 3) = ""
			// gridMonthly.TextMatrix(1, 4) = False
			// gridMonthly.TextMatrix(1, 5) = ""
			for (x = 1; x <= 12; x++)
			{
				minfo = theView.GetCurrentForm().GetMonthlyInformation(x);
				gridMonthly.TextMatrix(x + 1, 1, FCConvert.ToString(minfo.MinimumEssentialCoverageOffer));
				gridMonthly.TextMatrix(x + 1, 2, FCConvert.ToString(minfo.FullTimeEmployeeCount));
				gridMonthly.TextMatrix(x + 1, 3, FCConvert.ToString(minfo.TotalEmployeeCount));
				gridMonthly.TextMatrix(x + 1, 4, FCConvert.ToString(minfo.AggregatedGroup));
				gridMonthly.TextMatrix(x + 1, 5, minfo.Section4980HTransitionRelief);
			}
			// x
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(DialogResult)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!OkToLeaveForm())
			{
				e.Cancel = true;
			}
		}

		private void frm1094C_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}
		// vbPorter upgrade warning: Row As int	OnRead
		// vbPorter upgrade warning: Col As int	OnRead
		private void gridMonthly_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (!boolRefreshing)
			{
				// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
				int intCol = 0;
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow = 0;
				bool boolTemp = false;
				intRow = gridMonthly.Row;
				intCol = gridMonthly.Col;
				if (intRow == 1)
				{
					boolRefreshing = true;
					int x;
					FCGrid.DataTypeSettings vbPorterVar = gridMonthly.ColDataType(intCol);
					if (vbPorterVar == FCGrid.DataTypeSettings.flexDTBoolean)
					{
						if (FCConvert.ToDouble(gridMonthly.EditText) == 1)
						{
							boolTemp = false;
						}
						else
						{
							boolTemp = true;
						}
						if (intCol == 1)
						{
							theView.GetCurrentForm().SetAll12MonthsMinimumCoverage(boolTemp);
						}
						else if (intCol == 4)
						{
							theView.GetCurrentForm().SetAll12MonthsAggregatedGroup(boolTemp);
						}
					}
					else if (vbPorterVar == FCGrid.DataTypeSettings.flexDTLong)
					{
						if (intCol == 2)
						{
							theView.GetCurrentForm().SetAll12MonthsFullTimeEmployeeCount(FCConvert.ToInt32(Conversion.Val(gridMonthly.EditText)));
						}
						else if (intCol == 3)
						{
							theView.GetCurrentForm().SetAll12MonthsTotalEmployeeCount(FCConvert.ToInt32(Conversion.Val(gridMonthly.EditText)));
						}
					}
					else
					{
						if (intCol == 5)
						{
							theView.GetCurrentForm().SetAll12MonthsSection4980H(gridMonthly.EditText);
						}
					}
					FillGrid();
					boolRefreshing = false;
				}
				else
				{
					switch (intCol)
					{
						case 1:
							{
								if (FCConvert.ToDouble(gridMonthly.EditText) == 2)
								{
									boolTemp = false;
								}
								else
								{
									boolTemp = true;
								}
								theView.GetCurrentForm().SetMinimumCoverageForMonth(intRow - 1, boolTemp);
								break;
							}
						case 2:
							{
								theView.GetCurrentForm().SetFullTimeEmployeeCountForMonth(intRow - 1, FCConvert.ToInt32(Conversion.Val(gridMonthly.EditText)));
								break;
							}
						case 3:
							{
								theView.GetCurrentForm().SetTotalEmployeeCountForMonth(intRow - 1, FCConvert.ToInt32(Conversion.Val(gridMonthly.EditText)));
								break;
							}
						case 4:
							{
								if (FCConvert.ToDouble(gridMonthly.EditText) == 2)
								{
									boolTemp = false;
								}
								else
								{
									boolTemp = true;
								}
								theView.GetCurrentForm().SetAggregatedGroupForMonth(intRow - 1, boolTemp);
								break;
							}
						case 5:
							{
								theView.GetCurrentForm().Set4980HForMonth(intRow - 1, gridMonthly.EditText);
								break;
							}
					}
					//end switch
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport(theView.GetCurrentForm(), false);
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			PrintReport(theView.GetCurrentForm(), true);
		}

		private void PrintReport(c1094C theForm, bool boolTest)
		{
			theForm.HorizontalAlignment = 0;
			theForm.VerticalAlignment = 0;
			if (Information.IsNumeric(txtIRSHorizontal.Text))
			{
				setCont.SaveSetting(txtIRSHorizontal.Text, "1094CHorizontalAlignment", "Payroll", "", "", "");
				theForm.HorizontalAlignment = FCConvert.ToDouble(txtIRSHorizontal.Text);
			}
			else
			{
				setCont.SaveSetting("0", "1094CHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSVertical.Text))
			{
				setCont.SaveSetting(txtIRSVertical.Text, "1094CVerticalAlignment", "Payroll", "", "", "");
				theForm.VerticalAlignment = FCConvert.ToDouble(txtIRSVertical.Text);
			}
			else
			{
				setCont.SaveSetting("0", "1094CVerticalAlignment", "Payroll", "", "", "");
			}
			switch (theForm.YearCovered)
			{
				case 2016:
				case 2017:
				case 2018:
				case 2019:
				case 2020:
                {
						rpt1094C2016.InstancePtr.Init(theForm, boolTest);
						break;
                }
				default:
					{
						MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveForm();
            //FC:FINAL:BSE #4261 add save successful message 
            MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveForm())
			{
				Close();
			}
		}

		private void txtAddress_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().Address = txtAddress.Text;
				}
			}
		}

		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().City = txtCity.Text;
				}
			}
		}

		private void txtContact_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactName = txtContact.Text;
				}
			}
		}

		private void txtContactLast_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactLastName = txtContactLast.Text;
				}
			}
		}

		private void txtContactMiddle_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactMiddleName = txtContactMiddle.Text;
				}
			}
		}

		private void txtEIN_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().EIN = txtEIN.Text;
				}
			}
		}

		private void txtEmployer_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().EmployerName = txtEmployer.Text;
				}
			}
		}

		private void txtFiled_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().Total1095CForMember = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFiled.Text)));
				}
			}
		}

		private void txtFiled_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPostalCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().PostalCode = txtPostalCode.Text;
				}
			}
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().State = txtState.Text;
				}
			}
		}

		private void txtSuffix_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactSuffix = txtSuffix.Text;
				}
			}
		}

		private void txtTelephone_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactTelephone = txtTelephone.Text;
				}
			}
		}

		private void txtTransmittedForms_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().Total1095CTransmitted = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTransmittedForms.Text)));
				}
			}
		}

		private void txtTransmittedForms_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool OkToLeaveForm()
		{
			bool OkToLeaveForm = false;
			bool boolReturn;
			boolReturn = true;
			if (theView.IsCurrentForm)
			{
				if (theView.Changed)
				{
					// vbPorter upgrade warning: intReturn As int	OnWrite(DialogResult)
					DialogResult intReturn = 0;
					intReturn = MessageBox.Show("Changes have been made." + "\r\n" + "Save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
					if (intReturn == DialogResult.Cancel)
					{
						boolReturn = false;
					}
					else if (intReturn == DialogResult.Yes)
					{
						if (!SaveForm())
						{
							boolReturn = false;
						}
					}
				}
			}
			OkToLeaveForm = boolReturn;
			return OkToLeaveForm;
		}

		private bool SaveForm()
		{
			bool SaveForm = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (theView.IsCurrentForm)
				{
					if (Information.IsNumeric(txtIRSHorizontal.Text))
					{
						setCont.SaveSetting(txtIRSHorizontal.Text, "1094CHorizontalAlignment", "Payroll", "", "", "");
					}
					else
					{
						setCont.SaveSetting("0", "1094CHorizontalAlignment", "Payroll", "", "", "");
					}
					if (Information.IsNumeric(txtIRSVertical.Text))
					{
						setCont.SaveSetting(txtIRSVertical.Text, "1094CVerticalAlignment", "Payroll", "", "", "");
					}
					else
					{
						setCont.SaveSetting("0", "1094CVerticalAlignment", "Payroll", "", "", "");
					}
					theView.SaveForm();
				}
				SaveForm = true;
				return SaveForm;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveForm;
		}
	}
}
