﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frm1094B : BaseForm
	{
		public frm1094B()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frm1094B InstancePtr
		{
			get
			{
				return (frm1094B)Sys.GetInstance(typeof(frm1094B));
			}
		}

		protected frm1094B _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private c1094BView theView;
		private bool boolRefreshing;
		private cSettingsController setCont = new cSettingsController();

		public void Init(int intYearToShow)
		{
			theView = new c1094BView();
			theView.LoadForm(intYearToShow);
			RefreshFromView();
			this.Show(App.MainForm);
		}

		private void frm1094B_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frm1094B_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frm1094B properties;
			//frm1094B.FillStyle	= 0;
			//frm1094B.ScaleWidth	= 9300;
			//frm1094B.ScaleHeight	= 7890;
			//frm1094B.LinkTopic	= "Form2";
			//frm1094B.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);

            txtSubmissions.AllowOnlyNumericInput();
            txtIRSVertical.AllowOnlyNumericInput();
            txtIRSHorizontal.AllowOnlyNumericInput();
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(DialogResult)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!OkToLeaveForm())
			{
				e.Cancel = true;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveForm();
            MessageBox.Show("Save Successful");
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveForm())
			{
				Close();
			}
		}

		private void txtAddress_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().Address = txtAddress.Text;
				}
			}
		}

		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().City = txtCity.Text;
				}
			}
		}

		private void txtContact_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactName = txtContact.Text;
				}
			}
		}

		private void txtContactLast_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactLastName = txtContactLast.Text;
				}
			}
		}

		private void txtContactMiddle_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactMiddleName = txtContactMiddle.Text;
				}
			}
		}

		private void txtEIN_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().EIN = txtEIN.Text;
				}
			}
		}

		private void txtFilerName_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().FilerName = txtFilerName.Text;
				}
			}
		}

		private void txtPhone_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactPhone = txtPhone.Text;
				}
			}
		}

		private void txtPostalCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().PostalCode = txtPostalCode.Text;
				}
			}
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().State = txtState.Text;
				}
			}
		}

		private void txtSubmissions_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().NumberOfSubmissions = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSubmissions.Text)));
				}
			}
		}

		private void RefreshFromView()
		{
			string strTemp;
			boolRefreshing = true;
			strTemp = setCont.GetSettingValue("1094BVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSVertical.Text = strTemp;
			}
			else
			{
				txtIRSVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1094BHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSHorizontal.Text = strTemp;
			}
			else
			{
				txtIRSHorizontal.Text = "0";
			}
			if (theView.IsCurrentForm)
			{
				c1094B theForm;
				theForm = theView.GetCurrentForm();
				this.Text = "1094B - " + FCConvert.ToString(theForm.YearCovered);
				txtAddress.Text = theForm.Address;
				txtCity.Text = theForm.City;
				txtContact.Text = theForm.ContactName;
				txtSuffix.Text = theForm.ContactSuffix;
				txtContactMiddle.Text = theForm.ContactMiddleName;
				txtContactLast.Text = theForm.ContactLastName;
				txtEIN.Text = theForm.EIN;
				txtFilerName.Text = theForm.FilerName;
				txtPhone.Text = theForm.ContactPhone;
				txtPostalCode.Text = theForm.PostalCode;
				txtState.Text = theForm.State;
				txtSubmissions.Text = FCConvert.ToString(theForm.NumberOfSubmissions);
			}
			boolRefreshing = false;
		}

		private void txtSubmissions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool OkToLeaveForm()
		{
			bool OkToLeaveForm = false;
			bool boolReturn;
			boolReturn = true;
			if (theView.IsCurrentForm)
			{
				if (theView.Changed)
				{
					// vbPorter upgrade warning: intReturn As int	OnWrite(DialogResult)
					DialogResult intReturn = 0;
					intReturn = MessageBox.Show("Changes have been made." + "\r\n" + "Save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
					if (intReturn == DialogResult.Cancel)
					{
						boolReturn = false;
					}
					else if (intReturn == DialogResult.Yes)
					{
						if (!SaveForm())
						{
							boolReturn = false;
						}
					}
				}
			}
			OkToLeaveForm = boolReturn;
			return OkToLeaveForm;
		}

		private bool SaveForm()
		{
			bool SaveForm = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (theView.IsCurrentForm)
				{
					theView.SaveForm();
				}
				SaveForm = true;
				return SaveForm;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveForm;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport(theView.GetCurrentForm(), false);
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			PrintReport(theView.GetCurrentForm(), true);
		}

		private void PrintReport(c1094B theForm, bool boolTest)
		{
			theForm.HorizontalAlignment = 0;
			theForm.VerticalAlignment = 0;
			if (Information.IsNumeric(txtIRSHorizontal.Text))
			{
				setCont.SaveSetting(txtIRSHorizontal.Text, "1094BHorizontalAlignment", "Payroll", "", "", "");
				theForm.HorizontalAlignment = FCConvert.ToDouble(txtIRSHorizontal.Text);
			}
			else
			{
				setCont.SaveSetting("0", "1094BHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSVertical.Text))
			{
				setCont.SaveSetting(txtIRSVertical.Text, "1094BVerticalAlignment", "Payroll", "", "", "");
				theForm.VerticalAlignment = FCConvert.ToDouble(txtIRSVertical.Text);
			}
			else
			{
				setCont.SaveSetting("0", "1094BVerticalAlignment", "Payroll", "", "", "");
			}
			switch (theForm.YearCovered)
			{
				case 2016:
				case 2017:
				case 2018:
				case 2019:
				case 2020:
					{
						rpt1094B2016.InstancePtr.Init(theForm, boolTest);
						break;
					}
				default:
					{
						MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						break;
					}
			}
			//end switch
		}

		private void txtIRSHorizontal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIRSVertical_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSuffix_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentForm)
				{
					theView.GetCurrentForm().ContactSuffix = txtSuffix.Text;
				}
			}
		}
	}
}
