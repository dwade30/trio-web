﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frm1095Cs : BaseForm
	{
		public frm1095Cs()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

            //FC:FINAL:AKV:#4255 - Print 1095-Cs - Alignment fields should only allow numeric values
            txtIRSVertical.AllowOnlyNumericInput(true);
            txtIRSHorizontal.AllowOnlyNumericInput(true);
            txtEmployeeVertical.AllowOnlyNumericInput(true);
            txtEmployeeHorizontal.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frm1095Cs InstancePtr
		{
			get
			{
				return (frm1095Cs)Sys.GetInstance(typeof(frm1095Cs));
			}
		}

		protected frm1095Cs _InstancePtr = null;
		//=========================================================
		private cSettingsController setCont = new cSettingsController();
		private int intReportYear;
		private cACAService aService = new cACAService();
		private cEmployerService eService = new cEmployerService();

		public void Init(int intYear)
		{
			intReportYear = intYear;
			this.CenterToScreen();
            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
            //this.Show(FCForm.FormShowEnum.Modeless);
            this.Show(App.MainForm);
        }

		private void frm1095Cs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frm1095Cs_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			RefreshInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void RefreshInfo()
		{
			string strFormType;
			string strTemp;
			strFormType = setCont.GetSettingValue("1095EmployeeFormType", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "portrait")
			{
				cmbLandscape.Text = "Portrait Pre-Printed (for windowed envelopes)";
			}
			else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "blankportrait")
			{
				cmbLandscape.Text = "Portrait Blank (for windowed envelopes)";
			}
			else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "blanklandscape")
			{
				cmbLandscape.Text = "Landscape Blank";
			}
			else
			{
				cmbLandscape.Text = "Landscape Pre-Printed";
			}
			strTemp = setCont.GetSettingValue("1095CIRSVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSVertical.Text = strTemp;
			}
			else
			{
				txtIRSVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095CIRSHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSHorizontal.Text = strTemp;
			}
			else
			{
				txtIRSHorizontal.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095CEmployeeVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtEmployeeVertical.Text = strTemp;
			}
			else
			{
				txtEmployeeVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095CEmployeeHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtEmployeeHorizontal.Text = strTemp;
			}
			else
			{
				txtEmployeeHorizontal.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095CIncludeUncoveredEmployees", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				if (FCConvert.CBool(strTemp))
				{
					chkIncludeUncovered.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkIncludeUncovered.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkIncludeUncovered.CheckState = Wisej.Web.CheckState.Checked;
			}
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			PrintTest();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			Print1095Cs();
		}

		private void Print1095Cs()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				c1095CController cont1095C = new c1095CController();
				c1095CReport the1095CReport = new c1095CReport();
				the1095CReport.ReportYear = intReportYear;
				if (cmbEmployeeCopies.Text == "Print Employee Copies")
				{
					the1095CReport.MaskSSNs = true;
				}
				if (chkIncludeUncovered.CheckState == Wisej.Web.CheckState.Checked)
				{
					setCont.SaveSetting("True", "1095CIncludeUncoveredEmployees", "Payroll", "", "", "");
					the1095CReport.IncludeUncoveredEmployees = true;
				}
				else
				{
					setCont.SaveSetting("False", "1095CIncludeUncoveredEmployees", "Payroll", "", "", "");
					the1095CReport.IncludeUncoveredEmployees = false;
				}
                var overflowThreshhold = 6;

                if (cmbLandscape.Text == "Landscape Blank")
                {
                    if (the1095CReport.ReportYear > 2019)
                    {
                        overflowThreshhold = 0;
                    }
                }
                else
                {                 
                    if (the1095CReport.ReportYear > 2019)
                    {
                        overflowThreshhold = 0;
                    }
                }
                the1095CReport.OverflowThreshhold = overflowThreshhold;

                cont1095C.LoadReport(ref the1095CReport);
				if (the1095CReport.ListOfForms.ItemCount() < 1)
				{
					MessageBox.Show("No 1095-Cs to print", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					PrintForms(ref the1095CReport);
					Update1094C(the1095CReport.ListOfForms.ItemCount());
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Update1094C(int intCount)
		{
			c1094C current1094C;
			current1094C = aService.Load1094C(intReportYear);
			if (current1094C.YearCovered == 0)
			{
				current1094C.YearCovered = intReportYear;
				c1094C old1094C;
				old1094C = aService.Load1094C(intReportYear - 1);
				if (old1094C.YearCovered == intReportYear - 1)
				{
					current1094C.Address = old1094C.Address;
					current1094C.City = old1094C.City;
					current1094C.ContactName = old1094C.ContactName;
					current1094C.ContactTelephone = old1094C.ContactTelephone;
					current1094C.DesignatedGovnernmentEntity = old1094C.DesignatedGovnernmentEntity;
					current1094C.EIN = old1094C.EIN;
					current1094C.EmployerName = old1094C.EmployerName;
					current1094C.EntityAddress = old1094C.EntityAddress;
					current1094C.EntityCity = old1094C.EntityCity;
					current1094C.EntityContact = old1094C.EntityContact;
					current1094C.EntityEIN = old1094C.EntityEIN;
					current1094C.EntityPostalCode = old1094C.EntityPostalCode;
					current1094C.EntityState = old1094C.EntityState;
					current1094C.EntityTelephone = old1094C.EntityTelephone;
					current1094C.IsAuthoritativeTransmittal = old1094C.IsAuthoritativeTransmittal;
					current1094C.MemberOfAggregatedGroup = old1094C.MemberOfAggregatedGroup;
					current1094C.PostalCode = old1094C.PostalCode;
					current1094C.State = old1094C.State;
				}
				else
				{
					cEmployerRecord tEmployer;
					tEmployer = eService.GetEmployerInfo();
					current1094C.Address = tEmployer.Address;
					current1094C.City = tEmployer.City;
					current1094C.EIN = tEmployer.EIN;
					current1094C.ContactTelephone = tEmployer.Telephone;
					current1094C.EmployerName = tEmployer.Name;
					current1094C.State = tEmployer.State;
					current1094C.PostalCode = fecherFoundation.Strings.Trim(tEmployer.Zip + " " + tEmployer.Zip4);
				}
				current1094C.Total1095CForMember = intCount;
				current1094C.Total1095CTransmitted = intCount;
				aService.Save1094C(ref current1094C);
			}
			else
			{
				if (MessageBox.Show("Update 1094-C counts?", "Update Counts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					current1094C.Total1095CForMember = intCount;
					current1094C.Total1095CTransmitted = intCount;
					aService.Save1094C(ref current1094C);
				}
			}
		}

		private void PrintForms(ref c1095CReport the1095CReport)
		{
			// vbPorter upgrade warning: dblHorizontal As double	OnWrite(int, string)
			double dblHorizontal;
			// vbPorter upgrade warning: dblVertical As double	OnWrite(int, string)
			double dblVertical;
			bool boolEmployeeCopy;
			dblHorizontal = 0;
			dblVertical = 0;
			boolEmployeeCopy = false;
            var overflowThreshhold = 6;
			if (cmbEmployeeCopies.Text == "Print Employee Copies")
			{
				boolEmployeeCopy = true;
			}
			if (cmbLandscape.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				setCont.SaveSetting("Portrait", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			else if (cmbLandscape.Text == "Portrait Blank (for windowed envelopes)")
			{
				setCont.SaveSetting("BlankPortrait", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			else if (cmbLandscape.Text == "Landscape Blank")
			{
				setCont.SaveSetting("BlankLandscape", "1095EmployeeFormType", "Payroll", "", "", "");
                if (the1095CReport.ReportYear > 2019)
                {
                    overflowThreshhold = 0;
                }
			}
			else
			{
				setCont.SaveSetting("Landscape", "1095EmployeeFormType", "Payroll", "", "", "");
                if (the1095CReport.ReportYear > 2019)
                {
                    overflowThreshhold = 0;
                }
            }
            //the1095CReport.OverflowThreshhold = overflowThreshhold;
            //the1095CReport.HasOverflows = the1095CReport.
      

            if (Information.IsNumeric(txtEmployeeHorizontal.Text))
			{
				setCont.SaveSetting(txtEmployeeHorizontal.Text, "1095CEmployeeHorizontalAlignment", "Payroll", "", "", "");
				if (boolEmployeeCopy)
				{
					dblHorizontal = FCConvert.ToDouble(txtEmployeeHorizontal.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095CEmployeeHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtEmployeeVertical.Text))
			{
				setCont.SaveSetting(txtEmployeeVertical.Text, "1095CEmployeeVerticalAlignment", "Payroll", "", "", "");
				if (boolEmployeeCopy)
				{
					dblVertical = FCConvert.ToDouble(txtEmployeeVertical.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095CEmployeeVerticalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSHorizontal.Text))
			{
				setCont.SaveSetting(txtIRSHorizontal.Text, "1095CIRSHorizontalAlignment", "Payroll", "", "", "");
				if (!boolEmployeeCopy)
				{
					dblHorizontal = FCConvert.ToDouble(txtIRSHorizontal.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095CIRSHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSVertical.Text))
			{
				setCont.SaveSetting(txtIRSVertical.Text, "1095CIRSVerticalAlignment", "Payroll", "", "", "");
				if (!boolEmployeeCopy)
				{
					dblVertical = FCConvert.ToDouble(txtIRSVertical.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095CIRSVerticalAlignment", "Payroll", "", "", "");
			}
			the1095CReport.HorizontalAlignment = dblHorizontal;
			the1095CReport.VerticalAlignment = dblVertical;
            if (!the1095CReport.TestPrint && !StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
            {
				var extraProperties = new Dictionary<string, string>();
				extraProperties.Add("Report Year",the1095CReport.ReportYear.ToString());
				extraProperties.Add("Report Format", cmbLandscape.Text);
                extraProperties.Add("Employee Copy", boolEmployeeCopy.ToString());
				Telemetry.TrackEvent("Printed 1095C", extraProperties);
            }
			PrintPage1(ref the1095CReport);
			if (the1095CReport.HasOverflows)
			{
				if (MessageBox.Show("Place 1095-C overflow page(s) in the printer", "Print Overflow", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
				{
					PrintOverflow(ref the1095CReport);
				}
			}
		}

		private void PrintPage1(ref c1095CReport the1095CReport)
		{
			if (cmbLandscape.Text == "Landscape Pre-Printed")
			{
				switch (the1095CReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095C2016Page1.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095C2018Page1.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095C2020Page1.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbLandscape.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				switch (the1095CReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095C2016PortraitPage1.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095C2018PortraitPage1.InstancePtr.Init(ref the1095CReport,true);
                            break;
                        }
                    case 2020:
                        rpt1095C2020PortraitPage1.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbLandscape.Text == "Landscape Blank")
			{
				switch (the1095CReport.ReportYear)
				{
                    case 2020:
                        rpt1095C2020BlankPage1.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095C2018BlankPage1.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095C2017BlankPage1.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (the1095CReport.ReportYear)
				{
                    case 2020:
                        rpt1095C2020BlankPortraitPage1.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095C2018BlankPortraitPage1.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095C2017BlankPortraitPage1.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
		}

		private void PrintOverflow(ref c1095CReport the1095CReport)
		{
			if (cmbLandscape.Text == "Landscape Pre-Printed")
			{
				switch (the1095CReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095C2016Page2.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095C2018Page2.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095C2020Page2.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbLandscape.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				switch (the1095CReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095C2016PortraitPage2.InstancePtr.Init(ref the1095CReport,true);
							break;
						}
                    case 2018:
                        {
                            rpt1095C2018PortraitPage2.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095C2020PortraitPage2.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbLandscape.Text == "Landscape Blank")
			{
				switch (the1095CReport.ReportYear)
				{
                    case 2020:
                        rpt1095C2020BlankPage2.InstancePtr.Init(ref the1095CReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095C2018BlankPage2.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095C2017BlankPage2.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (the1095CReport.ReportYear)
				{
                    case 2020:
                        rpt1095C2020BlankPortraitPage2.InstancePtr.Init(ref the1095CReport,true);
                        break;
                    case 2018:
                        {
                            rpt1095C2018BlankPortraitPage2.InstancePtr.Init(ref the1095CReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095C2017BlankPortraitPage2.InstancePtr.Init(ref the1095CReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
		}

		private void PrintTest()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				c1095CController cont1095C = new c1095CController();
				c1095CReport the1095CReport = new c1095CReport();
				the1095CReport.ReportYear = intReportYear;
				if (cmbEmployeeCopies.Text == "Print Employee Copies")
				{
					the1095CReport.MaskSSNs = true;
				}
				the1095CReport.TestPrint = true;
				the1095CReport.HasOverflows = true;
				PrintForms(ref the1095CReport);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtEmployeeHorizontal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtEmployeeVertical_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIRSHorizontal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIRSVertical_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
