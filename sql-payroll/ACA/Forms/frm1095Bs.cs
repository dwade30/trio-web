﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frm1095Bs : BaseForm
	{
		public frm1095Bs()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
            //FC:FINAL:BSE #2529 Allow only numbers
            this.txtIRSHorizontal.AllowOnlyNumericInput(true);
            this.txtIRSVertical.AllowOnlyNumericInput(true);
            this.txtEmployeeHorizontal.AllowOnlyNumericInput(true);
            this.txtEmployeeVertical.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frm1095Bs InstancePtr
		{
			get
			{
				return (frm1095Bs)Sys.GetInstance(typeof(frm1095Bs));
			}
		}

		protected frm1095Bs _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int intReportYear;
		private cSettingsController setCont = new cSettingsController();
		private cACAService aService = new cACAService();
		private cEmployerService eService = new cEmployerService();

		public void Init(int intYear)
		{
			intReportYear = intYear;
            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
            this.Show(FCForm.FormShowEnum.Modeless);
            this.Show(App.MainForm);
        }

		private void RefreshInfo()
		{
			string strFormType;
			string strTemp;
			strFormType = setCont.GetSettingValue("1095EmployeeFormType", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "portrait")
			{
				cmbPortrait.Text = "Portrait Pre-Printed (for windowed envelopes)";
			}
			else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "blankportrait")
			{
				cmbPortrait.Text = "Portrait Blank (for windowed envelopes)";
			}
			else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(strFormType)) == "blanklandscape")
			{
				cmbPortrait.Text = "Landscape Blank";
			}
			else
			{
				cmbPortrait.Text = "Landscape Pre-Printed";
			}
			strTemp = setCont.GetSettingValue("1095BIRSVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSVertical.Text = strTemp;
			}
			else
			{
				txtIRSVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095BIRSHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtIRSHorizontal.Text = strTemp;
			}
			else
			{
				txtIRSHorizontal.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095BEmployeeVerticalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtEmployeeVertical.Text = strTemp;
			}
			else
			{
				txtEmployeeVertical.Text = "0";
			}
			strTemp = setCont.GetSettingValue("1095BEmployeeHorizontalAlignment", "Payroll", "", "", "");
			if (fecherFoundation.Strings.Trim(strTemp) != "")
			{
				txtEmployeeHorizontal.Text = strTemp;
			}
			else
			{
				txtEmployeeHorizontal.Text = "0";
			}
		}

		private void frm1095Bs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frm1095Bs_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			RefreshInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			PrintTest();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			Print1095Bs();
		}

		private void Print1095Bs()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				c1095BController cont1095B = new c1095BController();
				c1095BReport the1095bReport = new c1095BReport();
				the1095bReport.ReportYear = intReportYear;
				if (cmbIRSCopies.Text == "Print Employee Copies")
				{
					the1095bReport.MaskSSNs = true;
				}
				cont1095B.LoadReport(ref the1095bReport);
				if (the1095bReport.ListOfForms.ItemCount() < 1)
				{
					MessageBox.Show("No 1095-Bs to print", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				PrintForms(ref the1095bReport);
				Update1094b(the1095bReport.ListOfForms.ItemCount());
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintForms(ref c1095BReport the1095bReport)
		{
			// vbPorter upgrade warning: dblHorizontal As double	OnWrite(int, string)
			double dblHorizontal;
			// vbPorter upgrade warning: dblVertical As double	OnWrite(int, string)
			double dblVertical;
			bool boolEmployeeCopy;
			dblHorizontal = 0;
			dblVertical = 0;
			boolEmployeeCopy = false;
			if (cmbIRSCopies.Text == "Print Employee Copies")
			{
				boolEmployeeCopy = true;
			}
			if (cmbPortrait.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				setCont.SaveSetting("Portrait", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			else if (cmbPortrait.Text == "Portrait Blank (for windowed envelopes)")
			{
				setCont.SaveSetting("BlankPortrait", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			else if (cmbPortrait.Text == "Landscape Blank")
			{
				setCont.SaveSetting("BlankLandscape", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			else
			{
				setCont.SaveSetting("Landscape", "1095EmployeeFormType", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtEmployeeHorizontal.Text))
			{
				setCont.SaveSetting(txtEmployeeHorizontal.Text, "1095BEmployeeHorizontalAlignment", "Payroll", "", "", "");
				if (boolEmployeeCopy)
				{
					dblHorizontal = FCConvert.ToDouble(txtEmployeeHorizontal.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095BEmployeeHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtEmployeeVertical.Text))
			{
				setCont.SaveSetting(txtEmployeeVertical.Text, "1095BEmployeeVerticalAlignment", "Payroll", "", "", "");
				if (boolEmployeeCopy)
				{
					dblVertical = FCConvert.ToDouble(txtEmployeeVertical.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095BEmployeeVerticalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSHorizontal.Text))
			{
				setCont.SaveSetting(txtIRSHorizontal.Text, "1095BIRSHorizontalAlignment", "Payroll", "", "", "");
				if (!boolEmployeeCopy)
				{
					dblHorizontal = FCConvert.ToDouble(txtIRSHorizontal.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095BIRSHorizontalAlignment", "Payroll", "", "", "");
			}
			if (Information.IsNumeric(txtIRSVertical.Text))
			{
				setCont.SaveSetting(txtIRSVertical.Text, "1095BIRSVerticalAlignment", "Payroll", "", "", "");
				if (!boolEmployeeCopy)
				{
					dblVertical = FCConvert.ToDouble(txtIRSVertical.Text);
				}
			}
			else
			{
				setCont.SaveSetting("0", "1095BIRSVerticalAlignment", "Payroll", "", "", "");
			}
			the1095bReport.HorizontalAlignment = dblHorizontal;
			the1095bReport.VerticalAlignment = dblVertical;
            if (!the1095bReport.TestPrint && !StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
            {
                var extraProperties = new Dictionary<string, string>();
                extraProperties.Add("Report Year", the1095bReport.ReportYear.ToString());
                extraProperties.Add("Report Format", cmbPortrait.Text);
				extraProperties.Add("Employee Copy",boolEmployeeCopy.ToString());
                Telemetry.TrackEvent("Printed 1095B", extraProperties);
            }
			PrintPage1(ref the1095bReport);
			if (the1095bReport.HasOverflows)
			{
				if (MessageBox.Show("Place 1095-B overflow page(s) in the printer", "Print Overflow", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
				{
					PrintOverflow(ref the1095bReport);
				}
			}
		}

		private void PrintPage1(ref c1095BReport the1095bReport)
		{
			if (cmbPortrait.Text == "Landscape Pre-Printed")
			{
				switch (the1095bReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095B2016Page1.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095B2018Page1.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095B2020Page1.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbPortrait.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				switch (the1095bReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095B2016PortraitPage1.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095B2018PortraitPage1.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095B2020PortraitPage1.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbPortrait.Text == "Landscape Blank")
			{
				switch (the1095bReport.ReportYear)
				{
                    case 2020:
                        rpt1095B2020BlankPage1.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095B2018BlankPage1.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095B2017BlankPage1.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (the1095bReport.ReportYear)
				{
                    case 2020:
                        rpt1095B2020BlankPortraitPage1.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095B2018BlankPortraitPage1.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095B2017BlankPortraitPage1.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
		}

		private void PrintOverflow(ref c1095BReport the1095bReport)
		{
			if (cmbPortrait.Text == "Landscape Pre-Printed")
			{
				switch (the1095bReport.ReportYear)
				{
					case 2016:
					case 2017:
						{
							rpt1095B2016Page2.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095B2018Page2.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095B2020Page2.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbPortrait.Text == "Portrait Pre-Printed (for windowed envelopes)")
			{
				switch (the1095bReport.ReportYear)
				{
				// Case 2015
				// Call rpt1095BPortraitPage2.Init(the1095bReport)
					case 2016:
					case 2017:
						{
							rpt1095B2016PortraitPage2.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
                    case 2018:
                        {
                            rpt1095B2018PortraitPage2.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2020:
                        rpt1095B2020PortraitPage2.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else if (cmbPortrait.Text == "Landscape Blank")
			{
				switch (the1095bReport.ReportYear)
				{
                    case 2020:
                        rpt1095B2020BlankPage2.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095B2018BlankPage2.InstancePtr.Init(ref the1095bReport, true);
                            break;
                        }
                    case 2017:
						{
							rpt1095B2017BlankPage2.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (the1095bReport.ReportYear)
				{
                    case 2020:
                        rpt1095B2020BlankPortraitPage2.InstancePtr.Init(ref the1095bReport, true);
                        break;
                    case 2018:
                        {
                            rpt1095B2018BlankPortraitPage2.InstancePtr.Init(ref the1095bReport,true);
                            break;
                        }
                    case 2017:
						{
							rpt1095B2017BlankPortraitPage2.InstancePtr.Init(ref the1095bReport, true);
							break;
						}
					default:
						{
							MessageBox.Show("Not Implemented", "Not Implemented", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							break;
						}
				}
				//end switch
			}
		}

		private void PrintTest()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				c1095BReport the1095bReport = new c1095BReport();
				the1095bReport.ReportYear = intReportYear;
				if (cmbIRSCopies.Text == "Print Employee Copies")
				{
					the1095bReport.MaskSSNs = true;
				}
				the1095bReport.TestPrint = true;
				the1095bReport.HasOverflows = true;
				PrintForms(ref the1095bReport);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Update1094b(int intCount)
		{
			c1094B current1094b;
			current1094b = aService.Load1094b(intReportYear);
			if (current1094b.YearCovered == 0)
			{
				current1094b.YearCovered = intReportYear;
				c1094B old1094B;
				old1094B = aService.Load1094b(intReportYear - 1);
				if (old1094B.YearCovered == intReportYear - 1)
				{
					current1094b.Address = old1094B.Address;
					current1094b.City = old1094B.City;
					current1094b.ContactName = old1094B.ContactName;
					current1094b.ContactPhone = old1094B.ContactPhone;
					current1094b.EIN = old1094B.EIN;
					current1094b.FilerName = old1094B.FilerName;
					current1094b.PostalCode = old1094B.PostalCode;
					current1094b.State = old1094B.State;
				}
				else
				{
					cEmployerRecord tEmployer;
					tEmployer = eService.GetEmployerInfo();
					current1094b.Address = tEmployer.Address;
					current1094b.City = tEmployer.City;
					current1094b.EIN = tEmployer.EIN;
					current1094b.ContactPhone = tEmployer.Telephone;
					current1094b.FilerName = tEmployer.Name;
					current1094b.State = tEmployer.State;
					current1094b.PostalCode = fecherFoundation.Strings.Trim(tEmployer.Zip + " " + tEmployer.Zip4);
				}
				current1094b.NumberOfSubmissions = intCount;
				aService.Save1094B(ref current1094b);
			}
			else
			{
				if (MessageBox.Show("Update 1094-B counts?", "Update Counts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					current1094b.NumberOfSubmissions = intCount;
					aService.Save1094B(ref current1094b);
				}
			}
		}

		private void txtEmployeeHorizontal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtEmployeeVertical_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIRSHorizontal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIRSVertical_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
