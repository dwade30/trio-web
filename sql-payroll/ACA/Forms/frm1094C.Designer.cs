//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frm1094C.
	/// </summary>
	partial class frm1094C
	{
		public fecherFoundation.FCTextBox txtContactMiddle;
		public fecherFoundation.FCTextBox txtContactLast;
		public fecherFoundation.FCTextBox txtSuffix;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtIRSVertical;
		public fecherFoundation.FCTextBox txtIRSHorizontal;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCButton cmdALEMembers;
		public fecherFoundation.FCButton cmdMonthly;
		public fecherFoundation.FCTextBox txtTelephone;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chk98PercentMethod;
		public fecherFoundation.FCCheckBox chk4980HRelief;
		public fecherFoundation.FCCheckBox chkQualifyingOfferTransition;
		public fecherFoundation.FCCheckBox chkQualifyingOfferMethod;
		public fecherFoundation.FCCheckBox chkInGroup;
		public fecherFoundation.FCTextBox txtFiled;
		public fecherFoundation.FCCheckBox chkAuthoritative;
		public fecherFoundation.FCTextBox txtTransmittedForms;
		public fecherFoundation.FCTextBox txtPostalCode;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtEmployer;
		public fecherFoundation.FCTextBox txtEIN;
		public fecherFoundation.FCFrame framMonthly;
		public FCGrid gridMonthly;
		public fecherFoundation.FCButton cmdMonthlyOK;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdPrintTest;
		public fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.txtContactMiddle = new fecherFoundation.FCTextBox();
            this.txtContactLast = new fecherFoundation.FCTextBox();
            this.txtSuffix = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtIRSVertical = new fecherFoundation.FCTextBox();
            this.txtIRSHorizontal = new fecherFoundation.FCTextBox();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.cmdALEMembers = new fecherFoundation.FCButton();
            this.cmdMonthly = new fecherFoundation.FCButton();
            this.txtTelephone = new fecherFoundation.FCTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chk98PercentMethod = new fecherFoundation.FCCheckBox();
            this.chk4980HRelief = new fecherFoundation.FCCheckBox();
            this.chkQualifyingOfferTransition = new fecherFoundation.FCCheckBox();
            this.chkQualifyingOfferMethod = new fecherFoundation.FCCheckBox();
            this.chkInGroup = new fecherFoundation.FCCheckBox();
            this.txtFiled = new fecherFoundation.FCTextBox();
            this.chkAuthoritative = new fecherFoundation.FCCheckBox();
            this.txtTransmittedForms = new fecherFoundation.FCTextBox();
            this.txtPostalCode = new fecherFoundation.FCTextBox();
            this.txtContact = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtEmployer = new fecherFoundation.FCTextBox();
            this.txtEIN = new fecherFoundation.FCTextBox();
            this.framMonthly = new fecherFoundation.FCFrame();
            this.gridMonthly = new fecherFoundation.FCGrid();
            this.cmdMonthlyOK = new fecherFoundation.FCButton();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintTest = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdALEMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk98PercentMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4980HRelief)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQualifyingOfferTransition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQualifyingOfferMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthoritative)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framMonthly)).BeginInit();
            this.framMonthly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMonthlyOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framMonthly);
            this.ClientArea.Controls.Add(this.txtContactMiddle);
            this.ClientArea.Controls.Add(this.txtContactLast);
            this.ClientArea.Controls.Add(this.chkAuthoritative);
            this.ClientArea.Controls.Add(this.txtSuffix);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmdALEMembers);
            this.ClientArea.Controls.Add(this.cmdMonthly);
            this.ClientArea.Controls.Add(this.txtTelephone);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.chkInGroup);
            this.ClientArea.Controls.Add(this.txtFiled);
            this.ClientArea.Controls.Add(this.txtTransmittedForms);
            this.ClientArea.Controls.Add(this.txtPostalCode);
            this.ClientArea.Controls.Add(this.txtContact);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtAddress);
            this.ClientArea.Controls.Add(this.txtEmployer);
            this.ClientArea.Controls.Add(this.txtEIN);
            this.ClientArea.Controls.Add(this.Label18);
            this.ClientArea.Controls.Add(this.Label17);
            this.ClientArea.Controls.Add(this.Label16);
            this.ClientArea.Controls.Add(this.Label15);
            this.ClientArea.Controls.Add(this.Label12);
            this.ClientArea.Controls.Add(this.Label11);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdPrintTest);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintTest, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(93, 30);
            this.HeaderText.Text = "1094-C";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // txtContactMiddle
            // 
            this.txtContactMiddle.AutoSize = false;
            this.txtContactMiddle.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactMiddle.LinkItem = null;
            this.txtContactMiddle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactMiddle.LinkTopic = null;
            this.txtContactMiddle.Location = new System.Drawing.Point(179, 330);
            this.txtContactMiddle.Name = "txtContactMiddle";
            this.txtContactMiddle.Size = new System.Drawing.Size(359, 40);
            this.txtContactMiddle.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtContactMiddle, null);
            this.txtContactMiddle.TextChanged += new System.EventHandler(this.txtContactMiddle_TextChanged);
            // 
            // txtContactLast
            // 
            this.txtContactLast.AutoSize = false;
            this.txtContactLast.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactLast.LinkItem = null;
            this.txtContactLast.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactLast.LinkTopic = null;
            this.txtContactLast.Location = new System.Drawing.Point(179, 380);
            this.txtContactLast.Name = "txtContactLast";
            this.txtContactLast.Size = new System.Drawing.Size(359, 40);
            this.txtContactLast.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtContactLast, null);
            this.txtContactLast.TextChanged += new System.EventHandler(this.txtContactLast_TextChanged);
            // 
            // txtSuffix
            // 
            this.txtSuffix.AutoSize = false;
            this.txtSuffix.BackColor = System.Drawing.SystemColors.Window;
            this.txtSuffix.LinkItem = null;
            this.txtSuffix.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSuffix.LinkTopic = null;
            this.txtSuffix.Location = new System.Drawing.Point(179, 430);
            this.txtSuffix.Name = "txtSuffix";
            this.txtSuffix.Size = new System.Drawing.Size(359, 40);
            this.txtSuffix.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtSuffix, null);
            this.txtSuffix.TextChanged += new System.EventHandler(this.txtSuffix_TextChanged);
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.txtIRSVertical);
            this.Frame2.Controls.Add(this.txtIRSHorizontal);
            this.Frame2.Controls.Add(this.Label13);
            this.Frame2.Controls.Add(this.Label14);
            this.Frame2.Location = new System.Drawing.Point(428, 810);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(220, 108);
            this.Frame2.TabIndex = 37;
            this.Frame2.Text = "Alignment Adjustment";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtIRSVertical
            // 
            this.txtIRSVertical.AutoSize = false;
            this.txtIRSVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSVertical.LinkItem = null;
            this.txtIRSVertical.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSVertical.LinkTopic = null;
            this.txtIRSVertical.Location = new System.Drawing.Point(20, 48);
            this.txtIRSVertical.Name = "txtIRSVertical";
            this.txtIRSVertical.Size = new System.Drawing.Size(80, 40);
            this.txtIRSVertical.TabIndex = 39;
            this.txtIRSVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtIRSVertical, null);
            // 
            // txtIRSHorizontal
            // 
            this.txtIRSHorizontal.AutoSize = false;
            this.txtIRSHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSHorizontal.LinkItem = null;
            this.txtIRSHorizontal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSHorizontal.LinkTopic = null;
            this.txtIRSHorizontal.Location = new System.Drawing.Point(120, 48);
            this.txtIRSHorizontal.Name = "txtIRSHorizontal";
            this.txtIRSHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtIRSHorizontal.TabIndex = 38;
            this.txtIRSHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtIRSHorizontal, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 30);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(63, 15);
            this.Label13.TabIndex = 41;
            this.Label13.Text = "VERTICAL";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(120, 30);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(77, 15);
            this.Label14.TabIndex = 40;
            this.Label14.Text = "HORIZONTAL";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // cmdALEMembers
            // 
            this.cmdALEMembers.AppearanceKey = "actionButton";
            this.cmdALEMembers.Location = new System.Drawing.Point(254, 809);
            this.cmdALEMembers.Name = "cmdALEMembers";
            this.cmdALEMembers.Size = new System.Drawing.Size(156, 40);
            this.cmdALEMembers.TabIndex = 36;
            this.cmdALEMembers.Text = "ALE Members";
            this.ToolTip1.SetToolTip(this.cmdALEMembers, null);
            this.cmdALEMembers.Click += new System.EventHandler(this.cmdALEMembers_Click);
            // 
            // cmdMonthly
            // 
            this.cmdMonthly.AppearanceKey = "actionButton";
            this.cmdMonthly.Location = new System.Drawing.Point(30, 809);
            this.cmdMonthly.Name = "cmdMonthly";
            this.cmdMonthly.Size = new System.Drawing.Size(204, 40);
            this.cmdMonthly.TabIndex = 19;
            this.cmdMonthly.Text = "Monthly Information";
            this.ToolTip1.SetToolTip(this.cmdMonthly, null);
            this.cmdMonthly.Click += new System.EventHandler(this.cmdMonthly_Click);
            // 
            // txtTelephone
            // 
            this.txtTelephone.AutoSize = false;
            this.txtTelephone.BackColor = System.Drawing.SystemColors.Window;
            this.txtTelephone.LinkItem = null;
            this.txtTelephone.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTelephone.LinkTopic = null;
            this.txtTelephone.Location = new System.Drawing.Point(179, 480);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(359, 40);
            this.txtTelephone.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtTelephone, "Contact telephone number");
            this.txtTelephone.TextChanged += new System.EventHandler(this.txtTelephone_TextChanged);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.chk98PercentMethod);
            this.Frame1.Controls.Add(this.chk4980HRelief);
            this.Frame1.Controls.Add(this.chkQualifyingOfferTransition);
            this.Frame1.Controls.Add(this.chkQualifyingOfferMethod);
            this.Frame1.Location = new System.Drawing.Point(30, 690);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(627, 100);
            this.Frame1.TabIndex = 26;
            this.Frame1.Text = "Certifications Of Eligibility";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chk98PercentMethod
            // 
            this.chk98PercentMethod.Location = new System.Drawing.Point(349, 63);
            this.chk98PercentMethod.Name = "chk98PercentMethod";
            this.chk98PercentMethod.Size = new System.Drawing.Size(160, 27);
            this.chk98PercentMethod.TabIndex = 18;
            this.chk98PercentMethod.Text = "98% Offer Method";
            this.ToolTip1.SetToolTip(this.chk98PercentMethod, null);
            this.chk98PercentMethod.CheckedChanged += new System.EventHandler(this.chk98PercentMethod_CheckedChanged);
            // 
            // chk4980HRelief
            // 
            this.chk4980HRelief.Location = new System.Drawing.Point(349, 30);
            this.chk4980HRelief.Name = "chk4980HRelief";
            this.chk4980HRelief.Size = new System.Drawing.Size(258, 27);
            this.chk4980HRelief.TabIndex = 17;
            this.chk4980HRelief.Text = "Section 4980H Transition Relief";
            this.ToolTip1.SetToolTip(this.chk4980HRelief, null);
            this.chk4980HRelief.CheckedChanged += new System.EventHandler(this.chk4980HRelief_CheckedChanged);
            // 
            // chkQualifyingOfferTransition
            // 
            this.chkQualifyingOfferTransition.Location = new System.Drawing.Point(20, 63);
            this.chkQualifyingOfferTransition.Name = "chkQualifyingOfferTransition";
            this.chkQualifyingOfferTransition.Size = new System.Drawing.Size(324, 27);
            this.chkQualifyingOfferTransition.TabIndex = 16;
            this.chkQualifyingOfferTransition.Text = "Qualifying Offer Method Transition Relief";
            this.ToolTip1.SetToolTip(this.chkQualifyingOfferTransition, null);
            this.chkQualifyingOfferTransition.CheckedChanged += new System.EventHandler(this.chkQualifyingOfferTransition_CheckedChanged);
            // 
            // chkQualifyingOfferMethod
            // 
            this.chkQualifyingOfferMethod.Location = new System.Drawing.Point(20, 30);
            this.chkQualifyingOfferMethod.Name = "chkQualifyingOfferMethod";
            this.chkQualifyingOfferMethod.Size = new System.Drawing.Size(201, 27);
            this.chkQualifyingOfferMethod.TabIndex = 15;
            this.chkQualifyingOfferMethod.Text = "Qualifying Offer Method";
            this.ToolTip1.SetToolTip(this.chkQualifyingOfferMethod, null);
            this.chkQualifyingOfferMethod.CheckedChanged += new System.EventHandler(this.chkQualifyingOfferMethod_CheckedChanged);
            // 
            // chkInGroup
            // 
            this.chkInGroup.Location = new System.Drawing.Point(344, 650);
            this.chkInGroup.Name = "chkInGroup";
            this.chkInGroup.Size = new System.Drawing.Size(22, 26);
            this.chkInGroup.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.chkInGroup, null);
            this.chkInGroup.CheckedChanged += new System.EventHandler(this.chkInGroup_CheckedChanged);
            // 
            // txtFiled
            // 
            this.txtFiled.AutoSize = false;
            this.txtFiled.BackColor = System.Drawing.SystemColors.Window;
            this.txtFiled.LinkItem = null;
            this.txtFiled.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFiled.LinkTopic = null;
            this.txtFiled.Location = new System.Drawing.Point(344, 605);
            this.txtFiled.Name = "txtFiled";
            this.txtFiled.Size = new System.Drawing.Size(194, 40);
            this.txtFiled.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtFiled, null);
            this.txtFiled.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFiled_KeyPress);
            this.txtFiled.TextChanged += new System.EventHandler(this.txtFiled_TextChanged);
            // 
            // chkAuthoritative
            // 
            this.chkAuthoritative.Location = new System.Drawing.Point(344, 575);
            this.chkAuthoritative.Name = "chkAuthoritative";
            this.chkAuthoritative.Size = new System.Drawing.Size(22, 26);
            this.chkAuthoritative.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.chkAuthoritative, null);
            this.chkAuthoritative.CheckedChanged += new System.EventHandler(this.chkAuthoritative_CheckedChanged);
            // 
            // txtTransmittedForms
            // 
            this.txtTransmittedForms.AutoSize = false;
            this.txtTransmittedForms.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransmittedForms.LinkItem = null;
            this.txtTransmittedForms.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTransmittedForms.LinkTopic = null;
            this.txtTransmittedForms.Location = new System.Drawing.Point(344, 530);
            this.txtTransmittedForms.Name = "txtTransmittedForms";
            this.txtTransmittedForms.Size = new System.Drawing.Size(194, 40);
            this.txtTransmittedForms.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtTransmittedForms, null);
            this.txtTransmittedForms.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTransmittedForms_KeyPress);
            this.txtTransmittedForms.TextChanged += new System.EventHandler(this.txtTransmittedForms_TextChanged);
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.AutoSize = false;
            this.txtPostalCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPostalCode.LinkItem = null;
            this.txtPostalCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPostalCode.LinkTopic = null;
            this.txtPostalCode.Location = new System.Drawing.Point(179, 230);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(359, 40);
            this.txtPostalCode.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtPostalCode, "Country and ZIP or foreign postal code");
            this.txtPostalCode.TextChanged += new System.EventHandler(this.txtPostalCode_TextChanged);
            // 
            // txtContact
            // 
            this.txtContact.AutoSize = false;
            this.txtContact.BackColor = System.Drawing.SystemColors.Window;
            this.txtContact.LinkItem = null;
            this.txtContact.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContact.LinkTopic = null;
            this.txtContact.Location = new System.Drawing.Point(179, 280);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(359, 40);
            this.txtContact.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtContact, null);
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // txtState
            // 
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(179, 180);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(359, 40);
            this.txtState.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtState, "State or Province");
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(179, 130);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(359, 40);
            this.txtCity.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtCity, null);
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.LinkItem = null;
            this.txtAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress.LinkTopic = null;
            this.txtAddress.Location = new System.Drawing.Point(179, 80);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(359, 40);
            this.txtAddress.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtAddress, "Street address (including room or suite no.)");
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
            // 
            // txtEmployer
            // 
            this.txtEmployer.AutoSize = false;
            this.txtEmployer.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployer.LinkItem = null;
            this.txtEmployer.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployer.LinkTopic = null;
            this.txtEmployer.Location = new System.Drawing.Point(179, 30);
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Size = new System.Drawing.Size(359, 40);
            this.txtEmployer.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtEmployer, null);
            this.txtEmployer.TextChanged += new System.EventHandler(this.txtEmployer_TextChanged);
            // 
            // txtEIN
            // 
            this.txtEIN.AutoSize = false;
            this.txtEIN.BackColor = System.Drawing.SystemColors.Window;
            this.txtEIN.LinkItem = null;
            this.txtEIN.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEIN.LinkTopic = null;
            this.txtEIN.Location = new System.Drawing.Point(654, 30);
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Size = new System.Drawing.Size(180, 40);
            this.txtEIN.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtEIN, null);
            this.txtEIN.TextChanged += new System.EventHandler(this.txtEIN_TextChanged);
            // 
            // framMonthly
            // 
            this.framMonthly.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framMonthly.AppearanceKey = "groupBoxNoBorders";
            this.framMonthly.BackColor = System.Drawing.Color.White;
            this.framMonthly.Controls.Add(this.gridMonthly);
            this.framMonthly.Controls.Add(this.cmdMonthlyOK);
            this.framMonthly.Location = new System.Drawing.Point(30, 0);
            this.framMonthly.Name = "framMonthly";
            this.framMonthly.Size = new System.Drawing.Size(1010, 918);
            this.framMonthly.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.framMonthly, null);
            this.framMonthly.Visible = false;
            // 
            // gridMonthly
            // 
            this.gridMonthly.AllowSelection = false;
            this.gridMonthly.AllowUserToResizeColumns = false;
            this.gridMonthly.AllowUserToResizeRows = false;
            this.gridMonthly.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridMonthly.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridMonthly.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridMonthly.BackColorBkg = System.Drawing.Color.Empty;
            this.gridMonthly.BackColorFixed = System.Drawing.Color.Empty;
            this.gridMonthly.BackColorSel = System.Drawing.Color.Empty;
            this.gridMonthly.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridMonthly.Cols = 6;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridMonthly.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridMonthly.ColumnHeadersHeight = 30;
            this.gridMonthly.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridMonthly.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridMonthly.DragIcon = null;
            this.gridMonthly.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridMonthly.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridMonthly.ExtendLastCol = true;
            this.gridMonthly.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridMonthly.FrozenCols = 0;
            this.gridMonthly.GridColor = System.Drawing.Color.Empty;
            this.gridMonthly.GridColorFixed = System.Drawing.Color.Empty;
            this.gridMonthly.Location = new System.Drawing.Point(0, 30);
            this.gridMonthly.Name = "gridMonthly";
            this.gridMonthly.OutlineCol = 0;
            this.gridMonthly.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridMonthly.RowHeightMin = 0;
            this.gridMonthly.Rows = 14;
            this.gridMonthly.ScrollTipText = null;
            this.gridMonthly.ShowColumnVisibilityMenu = false;
            this.gridMonthly.ShowFocusCell = false;
            this.gridMonthly.Size = new System.Drawing.Size(990, 552);
            this.gridMonthly.StandardTab = true;
            this.gridMonthly.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridMonthly.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridMonthly.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.gridMonthly, null);
            this.gridMonthly.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridMonthly_ValidateEdit);
            // 
            // cmdMonthlyOK
            // 
            this.cmdMonthlyOK.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdMonthlyOK.AppearanceKey = "actionButton";
            this.cmdMonthlyOK.Location = new System.Drawing.Point(0, 602);
            this.cmdMonthlyOK.Name = "cmdMonthlyOK";
            this.cmdMonthlyOK.Size = new System.Drawing.Size(64, 40);
            this.cmdMonthlyOK.TabIndex = 34;
            this.cmdMonthlyOK.Text = "Ok";
            this.ToolTip1.SetToolTip(this.cmdMonthlyOK, null);
            this.cmdMonthlyOK.Click += new System.EventHandler(this.cmdMonthlyOK_Click);
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(568, 294);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(79, 19);
            this.Label18.TabIndex = 45;
            this.Label18.Text = "FIRST";
            this.ToolTip1.SetToolTip(this.Label18, null);
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(568, 344);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(79, 19);
            this.Label17.TabIndex = 44;
            this.Label17.Text = "MIDDLE";
            this.ToolTip1.SetToolTip(this.Label17, null);
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(568, 394);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(79, 19);
            this.Label16.TabIndex = 43;
            this.Label16.Text = "LAST";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(568, 444);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(79, 19);
            this.Label15.TabIndex = 42;
            this.Label15.Text = "SUFFIX";
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(30, 656);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(245, 16);
            this.Label12.TabIndex = 32;
            this.Label12.Text = "MEMBER OF AN AGGREGATED ALE GROUP";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(30, 582);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(245, 21);
            this.Label11.TabIndex = 31;
            this.Label11.Text = "THIS IS THE AUTHORITATIVE TRANSMITTAL";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 494);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(82, 17);
            this.Label10.TabIndex = 30;
            this.Label10.Text = "TELEPHONE";
            this.ToolTip1.SetToolTip(this.Label10, "Contact telephone number");
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 294);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(82, 17);
            this.Label9.TabIndex = 29;
            this.Label9.Text = "CONTACT";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 244);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(82, 17);
            this.Label8.TabIndex = 28;
            this.Label8.Text = "POSTAL CODE";
            this.ToolTip1.SetToolTip(this.Label8, "Country and ZIP or foreign postal code");
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 194);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(82, 17);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "STATE";
            this.ToolTip1.SetToolTip(this.Label7, "State or Province");
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 619);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(177, 17);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "TOTAL FORMS 1095-C FILED";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 544);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(193, 17);
            this.Label5.TabIndex = 24;
            this.Label5.Text = "TOTAL FORMS 1095-C SUBMITTED";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(568, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(29, 17);
            this.Label4.TabIndex = 23;
            this.Label4.Text = "EIN";
            this.ToolTip1.SetToolTip(this.Label4, "Employer Identification Number");
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(82, 17);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "CITY";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 17);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label2, "Street address (including room or suite no.)");
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(82, 17);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "EMPLOYER";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(925, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(45, 24);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdPrintTest
            // 
            this.cmdPrintTest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintTest.AppearanceKey = "toolbarButton";
            this.cmdPrintTest.Location = new System.Drawing.Point(976, 29);
            this.cmdPrintTest.Name = "cmdPrintTest";
            this.cmdPrintTest.Size = new System.Drawing.Size(74, 24);
            this.cmdPrintTest.TabIndex = 2;
            this.cmdPrintTest.Text = "Print Test";
            this.ToolTip1.SetToolTip(this.cmdPrintTest, null);
            this.cmdPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(474, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frm1094C
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frm1094C";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "1094-C";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frm1094C_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frm1094C_KeyDown);
            this.Resize += new System.EventHandler(this.frm1094C_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdALEMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk98PercentMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4980HRelief)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQualifyingOfferTransition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQualifyingOfferMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthoritative)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framMonthly)).EndInit();
            this.framMonthly.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMonthlyOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}