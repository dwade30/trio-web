//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmACAElectronicFiling.
	/// </summary>
	partial class frmACAElectronicFiling
	{
		public fecherFoundation.FCButton Command3;
		public fecherFoundation.FCButton Command2;
		public fecherFoundation.FCTextBox txtTCC;
		public fecherFoundation.FCTextBox txtPath;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenariosB;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario1;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario2;
		public fecherFoundation.FCToolStripMenuItem mnuScenario2C;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario6;
		public fecherFoundation.FCToolStripMenuItem mnuScenario6Correction;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario8;
		public fecherFoundation.FCToolStripMenuItem mnuScenario8C;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenariosC;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario3;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario4;
		public fecherFoundation.FCToolStripMenuItem mnuScenario4C;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario5;
		public fecherFoundation.FCToolStripMenuItem mnuScenario5C;
		public fecherFoundation.FCToolStripMenuItem mnuTestScenario7;
		public fecherFoundation.FCToolStripMenuItem mnuScenario7C;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Command3 = new fecherFoundation.FCButton();
			this.Command2 = new fecherFoundation.FCButton();
			this.txtTCC = new fecherFoundation.FCTextBox();
			this.txtPath = new fecherFoundation.FCTextBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuTestScenariosB = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario2C = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario6 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario6Correction = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario8 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario8C = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenariosC = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario4C = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario5C = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTestScenario7 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuScenario7C = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Command2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 224);
			this.BottomPanel.Size = new System.Drawing.Size(496, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Command3);
			this.ClientArea.Controls.Add(this.Command2);
			this.ClientArea.Controls.Add(this.txtTCC);
			this.ClientArea.Controls.Add(this.txtPath);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(496, 164);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(496, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(244, 30);
			this.HeaderText.Text = "ACA Electronic Filing";
			// 
			// Command3
			// 
			this.Command3.AppearanceKey = "actionButton";
			this.Command3.Location = new System.Drawing.Point(260, 115);
			this.Command3.Name = "Command3";
			this.Command3.Size = new System.Drawing.Size(208, 40);
			this.Command3.TabIndex = 6;
			this.Command3.Text = "Create 94/95 C XML";
			this.Command3.Click += new System.EventHandler(this.Command3_Click);
			// 
			// Command2
			// 
			this.Command2.AppearanceKey = "actionButton";
			this.Command2.Location = new System.Drawing.Point(30, 115);
			this.Command2.Name = "Command2";
			this.Command2.Size = new System.Drawing.Size(210, 40);
			this.Command2.TabIndex = 5;
			this.Command2.Text = "Create 94/95 B XML";
			this.Command2.Click += new System.EventHandler(this.Command2_Click);
			// 
			// txtTCC
			// 
			this.txtTCC.BackColor = System.Drawing.SystemColors.Window;
			this.txtTCC.Location = new System.Drawing.Point(30, 55);
			this.txtTCC.Name = "txtTCC";
			this.txtTCC.Size = new System.Drawing.Size(310, 40);
			this.txtTCC.TabIndex = 3;
			this.txtTCC.TextChanged += new System.EventHandler(this.txtTCC_TextChanged);
			this.txtTCC.Validating += new System.ComponentModel.CancelEventHandler(this.txtTCC_Validating);
			// 
			// txtPath
			// 
			this.txtPath.BackColor = System.Drawing.SystemColors.Window;
			this.txtPath.Location = new System.Drawing.Point(30, 55);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(310, 40);
			this.txtPath.TabIndex = 1;
			this.txtPath.Visible = false;
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(360, 54);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(108, 40);
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Visible = false;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(104, 15);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "TCC NUMBER";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(104, 15);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "SAVE TO";
			this.Label1.Visible = false;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTestScenariosB,
            this.mnuTestScenariosC});
			this.MainMenu1.Name = null;
			// 
			// mnuTestScenariosB
			// 
			this.mnuTestScenariosB.Index = 0;
			this.mnuTestScenariosB.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTestScenario1,
            this.mnuTestScenario2,
            this.mnuScenario2C,
            this.mnuTestScenario6,
            this.mnuScenario6Correction,
            this.mnuTestScenario8,
            this.mnuScenario8C});
			this.mnuTestScenariosB.Name = "mnuTestScenariosB";
			this.mnuTestScenariosB.Text = "Test Scenarios 94/95 B";
			// 
			// mnuTestScenario1
			// 
			this.mnuTestScenario1.Index = 0;
			this.mnuTestScenario1.Name = "mnuTestScenario1";
			this.mnuTestScenario1.Text = "Scenario 1";
			this.mnuTestScenario1.Click += new System.EventHandler(this.mnuTestScenario1_Click);
			// 
			// mnuTestScenario2
			// 
			this.mnuTestScenario2.Index = 1;
			this.mnuTestScenario2.Name = "mnuTestScenario2";
			this.mnuTestScenario2.Text = "Scenario 2";
			this.mnuTestScenario2.Click += new System.EventHandler(this.mnuTestScenario2_Click);
			// 
			// mnuScenario2C
			// 
			this.mnuScenario2C.Index = 2;
			this.mnuScenario2C.Name = "mnuScenario2C";
			this.mnuScenario2C.Text = "Scenario 2C";
			this.mnuScenario2C.Visible = false;
			this.mnuScenario2C.Click += new System.EventHandler(this.mnuScenario2C_Click);
			// 
			// mnuTestScenario6
			// 
			this.mnuTestScenario6.Index = 3;
			this.mnuTestScenario6.Name = "mnuTestScenario6";
			this.mnuTestScenario6.Text = "Scenario 6";
			this.mnuTestScenario6.Click += new System.EventHandler(this.mnuTestScenario6_Click);
			// 
			// mnuScenario6Correction
			// 
			this.mnuScenario6Correction.Index = 4;
			this.mnuScenario6Correction.Name = "mnuScenario6Correction";
			this.mnuScenario6Correction.Text = "Scenario 6C";
			this.mnuScenario6Correction.Visible = false;
			this.mnuScenario6Correction.Click += new System.EventHandler(this.mnuScenario6Correction_Click);
			// 
			// mnuTestScenario8
			// 
			this.mnuTestScenario8.Index = 5;
			this.mnuTestScenario8.Name = "mnuTestScenario8";
			this.mnuTestScenario8.Text = "Scenario 8";
			this.mnuTestScenario8.Click += new System.EventHandler(this.mnuTestScenario8_Click);
			// 
			// mnuScenario8C
			// 
			this.mnuScenario8C.Index = 6;
			this.mnuScenario8C.Name = "mnuScenario8C";
			this.mnuScenario8C.Text = "Scenario 8C";
			this.mnuScenario8C.Visible = false;
			this.mnuScenario8C.Click += new System.EventHandler(this.mnuScenario8C_Click);
			// 
			// mnuTestScenariosC
			// 
			this.mnuTestScenariosC.Index = 1;
			this.mnuTestScenariosC.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTestScenario3,
            this.mnuTestScenario4,
            this.mnuScenario4C,
            this.mnuTestScenario5,
            this.mnuScenario5C,
            this.mnuTestScenario7,
            this.mnuScenario7C});
			this.mnuTestScenariosC.Name = "mnuTestScenariosC";
			this.mnuTestScenariosC.Text = "Test Scenarios 94/95 C";
			// 
			// mnuTestScenario3
			// 
			this.mnuTestScenario3.Index = 0;
			this.mnuTestScenario3.Name = "mnuTestScenario3";
			this.mnuTestScenario3.Text = "Scenario 3";
			this.mnuTestScenario3.Click += new System.EventHandler(this.mnuTestScenario3_Click);
			// 
			// mnuTestScenario4
			// 
			this.mnuTestScenario4.Index = 1;
			this.mnuTestScenario4.Name = "mnuTestScenario4";
			this.mnuTestScenario4.Text = "Scenario 4";
			this.mnuTestScenario4.Click += new System.EventHandler(this.mnuTestScenario4_Click);
			// 
			// mnuScenario4C
			// 
			this.mnuScenario4C.Index = 2;
			this.mnuScenario4C.Name = "mnuScenario4C";
			this.mnuScenario4C.Text = "Scenario 4C";
			this.mnuScenario4C.Visible = false;
			this.mnuScenario4C.Click += new System.EventHandler(this.mnuScenario4C_Click);
			// 
			// mnuTestScenario5
			// 
			this.mnuTestScenario5.Index = 3;
			this.mnuTestScenario5.Name = "mnuTestScenario5";
			this.mnuTestScenario5.Text = "Scenario 5";
			this.mnuTestScenario5.Click += new System.EventHandler(this.mnuTestScenario5_Click);
			// 
			// mnuScenario5C
			// 
			this.mnuScenario5C.Index = 4;
			this.mnuScenario5C.Name = "mnuScenario5C";
			this.mnuScenario5C.Text = "Scenario 5C";
			this.mnuScenario5C.Visible = false;
			this.mnuScenario5C.Click += new System.EventHandler(this.mnuScenario5C_Click);
			// 
			// mnuTestScenario7
			// 
			this.mnuTestScenario7.Index = 5;
			this.mnuTestScenario7.Name = "mnuTestScenario7";
			this.mnuTestScenario7.Text = "Scenario 7";
			this.mnuTestScenario7.Click += new System.EventHandler(this.mnuTestScenario7_Click);
			// 
			// mnuScenario7C
			// 
			this.mnuScenario7C.Index = 6;
			this.mnuScenario7C.Name = "mnuScenario7C";
			this.mnuScenario7C.Text = "Scenario 7C";
			this.mnuScenario7C.Visible = false;
			this.mnuScenario7C.Click += new System.EventHandler(this.mnuScenario7C_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = -1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// Seperator
			// 
			this.Seperator.Index = -1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmACAElectronicFiling
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(496, 332);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmACAElectronicFiling";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "ACA Electronic Filing";
			this.Load += new System.EventHandler(this.frmACAElectronicFiling_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmACAElectronicFiling_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Command2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}