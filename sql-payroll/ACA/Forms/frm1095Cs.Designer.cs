//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frm1095Cs.
	/// </summary>
	partial class frm1095Cs
	{
		public fecherFoundation.FCComboBox cmbLandscape;
		public fecherFoundation.FCLabel lblLandscape;
		public fecherFoundation.FCComboBox cmbEmployeeCopies;
		public fecherFoundation.FCLabel lblEmployeeCopies;
		public fecherFoundation.FCCheckBox chkIncludeUncovered;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtIRSVertical;
		public fecherFoundation.FCTextBox txtEmployeeVertical;
		public fecherFoundation.FCTextBox txtIRSHorizontal;
		public fecherFoundation.FCTextBox txtEmployeeHorizontal;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCButton cmdSaveContinue;
		public fecherFoundation.FCButton cmdPrintTest;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbLandscape = new fecherFoundation.FCComboBox();
            this.lblLandscape = new fecherFoundation.FCLabel();
            this.cmbEmployeeCopies = new fecherFoundation.FCComboBox();
            this.lblEmployeeCopies = new fecherFoundation.FCLabel();
            this.chkIncludeUncovered = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtIRSVertical = new fecherFoundation.FCTextBox();
            this.txtEmployeeVertical = new fecherFoundation.FCTextBox();
            this.txtIRSHorizontal = new fecherFoundation.FCTextBox();
            this.txtEmployeeHorizontal = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.cmdPrintTest = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeUncovered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 375);
            this.BottomPanel.Size = new System.Drawing.Size(659, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkIncludeUncovered);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbLandscape);
            this.ClientArea.Controls.Add(this.lblLandscape);
            this.ClientArea.Controls.Add(this.cmbEmployeeCopies);
            this.ClientArea.Controls.Add(this.lblEmployeeCopies);
            this.ClientArea.Size = new System.Drawing.Size(679, 491);
            this.ClientArea.Controls.SetChildIndex(this.lblEmployeeCopies, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbEmployeeCopies, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblLandscape, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbLandscape, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkIncludeUncovered, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintTest);
            this.TopPanel.Size = new System.Drawing.Size(679, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintTest, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(151, 28);
            this.HeaderText.Text = "Print 1095-Cs";
            // 
            // cmbLandscape
            // 
            this.cmbLandscape.Items.AddRange(new object[] {
            "Landscape Pre-Printed",
            "Landscape Blank",
            "Portrait Pre-Printed (for windowed envelopes)",
            "Portrait Blank (for windowed envelopes)"});
            this.cmbLandscape.Location = new System.Drawing.Point(269, 90);
            this.cmbLandscape.Name = "cmbLandscape";
            this.cmbLandscape.Size = new System.Drawing.Size(380, 40);
            this.cmbLandscape.TabIndex = 14;
            this.cmbLandscape.Text = "Landscape Pre-Printed";
            // 
            // lblLandscape
            // 
            this.lblLandscape.AutoSize = true;
            this.lblLandscape.Location = new System.Drawing.Point(30, 104);
            this.lblLandscape.Name = "lblLandscape";
            this.lblLandscape.Size = new System.Drawing.Size(203, 15);
            this.lblLandscape.TabIndex = 15;
            this.lblLandscape.Text = "EMPLOYEE COPY FORM STYLE";
            // 
            // cmbEmployeeCopies
            // 
            this.cmbEmployeeCopies.Items.AddRange(new object[] {
            "Print Employee Copies",
            "Print IRS Copies"});
            this.cmbEmployeeCopies.Location = new System.Drawing.Point(269, 30);
            this.cmbEmployeeCopies.Name = "cmbEmployeeCopies";
            this.cmbEmployeeCopies.Size = new System.Drawing.Size(380, 40);
            this.cmbEmployeeCopies.TabIndex = 16;
            this.cmbEmployeeCopies.Text = "Print Employee Copies";
            // 
            // lblEmployeeCopies
            // 
            this.lblEmployeeCopies.AutoSize = true;
            this.lblEmployeeCopies.Location = new System.Drawing.Point(30, 44);
            this.lblEmployeeCopies.Name = "lblEmployeeCopies";
            this.lblEmployeeCopies.Size = new System.Drawing.Size(53, 15);
            this.lblEmployeeCopies.TabIndex = 17;
            this.lblEmployeeCopies.Text = "COPIES";
            // 
            // chkIncludeUncovered
            // 
            this.chkIncludeUncovered.Location = new System.Drawing.Point(30, 150);
            this.chkIncludeUncovered.Name = "chkIncludeUncovered";
            this.chkIncludeUncovered.Size = new System.Drawing.Size(277, 22);
            this.chkIncludeUncovered.TabIndex = 7;
            this.chkIncludeUncovered.Text = "Include employees with no coverage plans";
            this.ToolTip1.SetToolTip(this.chkIncludeUncovered, "Include employees with no coverage plan (If they will not be included on 1095-B\'s" +
        ")");
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtIRSVertical);
            this.Frame2.Controls.Add(this.txtEmployeeVertical);
            this.Frame2.Controls.Add(this.txtIRSHorizontal);
            this.Frame2.Controls.Add(this.txtEmployeeHorizontal);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Location = new System.Drawing.Point(30, 197);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(349, 178);
            this.Frame2.TabIndex = 13;
            this.Frame2.Text = "Alignment Adjustment";
            // 
            // txtIRSVertical
            // 
            this.txtIRSVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSVertical.Location = new System.Drawing.Point(149, 58);
            this.txtIRSVertical.Name = "txtIRSVertical";
            this.txtIRSVertical.Size = new System.Drawing.Size(80, 40);
            this.txtIRSVertical.TabIndex = 8;
            this.txtIRSVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSVertical.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSVertical_KeyPress);
            // 
            // txtEmployeeVertical
            // 
            this.txtEmployeeVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeVertical.Location = new System.Drawing.Point(149, 118);
            this.txtEmployeeVertical.Name = "txtEmployeeVertical";
            this.txtEmployeeVertical.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeVertical.TabIndex = 10;
            this.txtEmployeeVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtEmployeeVertical.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmployeeVertical_KeyPress);
            // 
            // txtIRSHorizontal
            // 
            this.txtIRSHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSHorizontal.Location = new System.Drawing.Point(249, 58);
            this.txtIRSHorizontal.Name = "txtIRSHorizontal";
            this.txtIRSHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtIRSHorizontal.TabIndex = 9;
            this.txtIRSHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSHorizontal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSHorizontal_KeyPress);
            // 
            // txtEmployeeHorizontal
            // 
            this.txtEmployeeHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeHorizontal.Location = new System.Drawing.Point(249, 118);
            this.txtEmployeeHorizontal.Name = "txtEmployeeHorizontal";
            this.txtEmployeeHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeHorizontal.TabIndex = 11;
            this.txtEmployeeHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtEmployeeHorizontal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmployeeHorizontal_KeyPress);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 72);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(74, 19);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "IRS";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 132);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(67, 16);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "EMPLOYEE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(149, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(63, 19);
            this.Label3.TabIndex = 15;
            this.Label3.Text = "VERTICAL";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(249, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(77, 19);
            this.Label4.TabIndex = 14;
            this.Label4.Text = "HORIZONTAL";
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(270, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(80, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Print";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // cmdPrintTest
            // 
            this.cmdPrintTest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintTest.Location = new System.Drawing.Point(574, 29);
            this.cmdPrintTest.Name = "cmdPrintTest";
            this.cmdPrintTest.Size = new System.Drawing.Size(75, 24);
            this.cmdPrintTest.TabIndex = 1;
            this.cmdPrintTest.Text = "Print Test";
            this.cmdPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // frm1095Cs
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(679, 551);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frm1095Cs";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print 1095-Cs";
            this.Load += new System.EventHandler(this.frm1095Cs_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frm1095Cs_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeUncovered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}