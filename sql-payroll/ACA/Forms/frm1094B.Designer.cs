//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frm1094B.
	/// </summary>
	partial class frm1094B
	{
		public fecherFoundation.FCTextBox txtSuffix;
		public fecherFoundation.FCTextBox txtContactLast;
		public fecherFoundation.FCTextBox txtContactMiddle;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtIRSVertical;
		public fecherFoundation.FCTextBox txtIRSHorizontal;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCTextBox txtSubmissions;
		public fecherFoundation.FCTextBox txtPostalCode;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtPhone;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtEIN;
		public fecherFoundation.FCTextBox txtFilerName;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdPrintTest;
		public fecherFoundation.FCButton cmdSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtSuffix = new fecherFoundation.FCTextBox();
            this.txtContactLast = new fecherFoundation.FCTextBox();
            this.txtContactMiddle = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtIRSVertical = new fecherFoundation.FCTextBox();
            this.txtIRSHorizontal = new fecherFoundation.FCTextBox();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.txtSubmissions = new fecherFoundation.FCTextBox();
            this.txtPostalCode = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtPhone = new fecherFoundation.FCTextBox();
            this.txtContact = new fecherFoundation.FCTextBox();
            this.txtEIN = new fecherFoundation.FCTextBox();
            this.txtFilerName = new fecherFoundation.FCTextBox();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintTest = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(581, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtSuffix);
            this.ClientArea.Controls.Add(this.txtContactLast);
            this.ClientArea.Controls.Add(this.txtContactMiddle);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.txtSubmissions);
            this.ClientArea.Controls.Add(this.txtPostalCode);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtAddress);
            this.ClientArea.Controls.Add(this.txtPhone);
            this.ClientArea.Controls.Add(this.txtContact);
            this.ClientArea.Controls.Add(this.txtEIN);
            this.ClientArea.Controls.Add(this.txtFilerName);
            this.ClientArea.Controls.Add(this.Label15);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.Label13);
            this.ClientArea.Controls.Add(this.Label12);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(581, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdPrintTest);
            this.TopPanel.Size = new System.Drawing.Size(581, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintTest, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(91, 30);
            this.HeaderText.Text = "1094-B";
            // 
            // txtSuffix
            // 
            this.txtSuffix.AutoSize = false;
            this.txtSuffix.BackColor = System.Drawing.SystemColors.Window;
            this.txtSuffix.LinkItem = null;
            this.txtSuffix.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSuffix.LinkTopic = null;
            this.txtSuffix.Location = new System.Drawing.Point(183, 280);
            this.txtSuffix.Name = "txtSuffix";
            this.txtSuffix.Size = new System.Drawing.Size(284, 40);
            this.txtSuffix.TabIndex = 5;
            this.txtSuffix.TextChanged += new System.EventHandler(this.txtSuffix_TextChanged);
            // 
            // txtContactLast
            // 
            this.txtContactLast.AutoSize = false;
            this.txtContactLast.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactLast.LinkItem = null;
            this.txtContactLast.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactLast.LinkTopic = null;
            this.txtContactLast.Location = new System.Drawing.Point(183, 230);
            this.txtContactLast.Name = "txtContactLast";
            this.txtContactLast.Size = new System.Drawing.Size(284, 40);
            this.txtContactLast.TabIndex = 4;
            this.txtContactLast.TextChanged += new System.EventHandler(this.txtContactLast_TextChanged);
            // 
            // txtContactMiddle
            // 
            this.txtContactMiddle.AutoSize = false;
            this.txtContactMiddle.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactMiddle.LinkItem = null;
            this.txtContactMiddle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactMiddle.LinkTopic = null;
            this.txtContactMiddle.Location = new System.Drawing.Point(183, 180);
            this.txtContactMiddle.Name = "txtContactMiddle";
            this.txtContactMiddle.Size = new System.Drawing.Size(284, 40);
            this.txtContactMiddle.TabIndex = 3;
            this.txtContactMiddle.TextChanged += new System.EventHandler(this.txtContactMiddle_TextChanged);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtIRSVertical);
            this.Frame2.Controls.Add(this.txtIRSHorizontal);
            this.Frame2.Controls.Add(this.Label11);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(23, 640);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(444, 109);
            this.Frame2.TabIndex = 21;
            this.Frame2.Text = "Alignment Adjustment";
            // 
            // txtIRSVertical
            // 
            this.txtIRSVertical.AutoSize = false;
            this.txtIRSVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSVertical.LinkItem = null;
            this.txtIRSVertical.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSVertical.LinkTopic = null;
            this.txtIRSVertical.Location = new System.Drawing.Point(20, 49);
            this.txtIRSVertical.Name = "txtIRSVertical";
            this.txtIRSVertical.Size = new System.Drawing.Size(80, 40);
            this.txtIRSVertical.TabIndex = 23;
            this.txtIRSVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSVertical.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSVertical_KeyPress);
            // 
            // txtIRSHorizontal
            // 
            this.txtIRSHorizontal.AutoSize = false;
            this.txtIRSHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSHorizontal.LinkItem = null;
            this.txtIRSHorizontal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSHorizontal.LinkTopic = null;
            this.txtIRSHorizontal.Location = new System.Drawing.Point(120, 51);
            this.txtIRSHorizontal.Name = "txtIRSHorizontal";
            this.txtIRSHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtIRSHorizontal.TabIndex = 22;
            this.txtIRSHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSHorizontal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSHorizontal_KeyPress);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 30);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(63, 18);
            this.Label11.TabIndex = 25;
            this.Label11.Text = "VERTICAL";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(120, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(81, 18);
            this.Label10.TabIndex = 24;
            this.Label10.Text = "HORIZONTAL";
            // 
            // txtSubmissions
            // 
            this.txtSubmissions.AutoSize = false;
            this.txtSubmissions.BackColor = System.Drawing.SystemColors.Window;
            this.txtSubmissions.LinkItem = null;
            this.txtSubmissions.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSubmissions.LinkTopic = null;
            this.txtSubmissions.Location = new System.Drawing.Point(183, 580);
            this.txtSubmissions.Name = "txtSubmissions";
            this.txtSubmissions.Size = new System.Drawing.Size(284, 40);
            this.txtSubmissions.TabIndex = 11;
            this.txtSubmissions.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSubmissions_KeyPress);
            this.txtSubmissions.TextChanged += new System.EventHandler(this.txtSubmissions_TextChanged);
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.AutoSize = false;
            this.txtPostalCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPostalCode.LinkItem = null;
            this.txtPostalCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPostalCode.LinkTopic = null;
            this.txtPostalCode.Location = new System.Drawing.Point(183, 530);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(284, 40);
            this.txtPostalCode.TabIndex = 10;
            this.txtPostalCode.TextChanged += new System.EventHandler(this.txtPostalCode_TextChanged);
            // 
            // txtState
            // 
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(183, 480);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(284, 40);
            this.txtState.TabIndex = 9;
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(183, 430);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(284, 40);
            this.txtCity.TabIndex = 8;
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.LinkItem = null;
            this.txtAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress.LinkTopic = null;
            this.txtAddress.Location = new System.Drawing.Point(183, 380);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(284, 40);
            this.txtAddress.TabIndex = 7;
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
            // 
            // txtPhone
            // 
            this.txtPhone.AutoSize = false;
            this.txtPhone.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhone.LinkItem = null;
            this.txtPhone.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPhone.LinkTopic = null;
            this.txtPhone.Location = new System.Drawing.Point(183, 330);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(284, 40);
            this.txtPhone.TabIndex = 6;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // txtContact
            // 
            this.txtContact.AutoSize = false;
            this.txtContact.BackColor = System.Drawing.SystemColors.Window;
            this.txtContact.LinkItem = null;
            this.txtContact.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContact.LinkTopic = null;
            this.txtContact.Location = new System.Drawing.Point(183, 130);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(284, 40);
            this.txtContact.TabIndex = 2;
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // txtEIN
            // 
            this.txtEIN.AutoSize = false;
            this.txtEIN.BackColor = System.Drawing.SystemColors.Window;
            this.txtEIN.LinkItem = null;
            this.txtEIN.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEIN.LinkTopic = null;
            this.txtEIN.Location = new System.Drawing.Point(183, 80);
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Size = new System.Drawing.Size(284, 40);
            this.txtEIN.TabIndex = 1;
            this.txtEIN.TextChanged += new System.EventHandler(this.txtEIN_TextChanged);
            // 
            // txtFilerName
            // 
            this.txtFilerName.AutoSize = false;
            this.txtFilerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilerName.LinkItem = null;
            this.txtFilerName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFilerName.LinkTopic = null;
            this.txtFilerName.Location = new System.Drawing.Point(183, 30);
            this.txtFilerName.Name = "txtFilerName";
            this.txtFilerName.Size = new System.Drawing.Size(284, 40);
            this.txtFilerName.TabIndex = 0;
            this.txtFilerName.TextChanged += new System.EventHandler(this.txtFilerName_TextChanged);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(501, 294);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(49, 19);
            this.Label15.TabIndex = 29;
            this.Label15.Text = "SUFFIX";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(501, 244);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(49, 19);
            this.Label14.TabIndex = 28;
            this.Label14.Text = "LAST";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(501, 194);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(49, 19);
            this.Label13.TabIndex = 27;
            this.Label13.Text = "MIDDLE";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(501, 144);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(49, 19);
            this.Label12.TabIndex = 26;
            this.Label12.Text = "FIRST";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 594);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(123, 19);
            this.Label9.TabIndex = 20;
            this.Label9.Text = "TOTAL SUBMISSIONS";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 544);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(88, 19);
            this.Label8.TabIndex = 19;
            this.Label8.Text = "POSTAL CODE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 494);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(79, 19);
            this.Label7.TabIndex = 18;
            this.Label7.Text = "STATE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 444);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(79, 19);
            this.Label6.TabIndex = 17;
            this.Label6.Text = "CITY";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 394);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(79, 19);
            this.Label5.TabIndex = 16;
            this.Label5.Text = "ADDRESS";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 344);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(107, 19);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "CONTACT PHONE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(107, 19);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "CONTACT NAME";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 19);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "EIN";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(79, 19);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "FILER\'S NAME";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(427, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(46, 24);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdPrintTest
            // 
            this.cmdPrintTest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintTest.AppearanceKey = "toolbarButton";
            this.cmdPrintTest.Location = new System.Drawing.Point(479, 29);
            this.cmdPrintTest.Name = "cmdPrintTest";
            this.cmdPrintTest.Size = new System.Drawing.Size(74, 24);
            this.cmdPrintTest.TabIndex = 3;
            this.cmdPrintTest.Text = "Print Test";
            this.cmdPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(246, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frm1094B
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(581, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frm1094B";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "1094-B";
            this.Load += new System.EventHandler(this.frm1094B_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frm1094B_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}