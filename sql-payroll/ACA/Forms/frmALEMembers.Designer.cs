//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmALEMembers.
	/// </summary>
	partial class frmALEMembers
	{
		public fecherFoundation.FCButton cmdOK;
		public FCGrid gridMembers;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddMember;
		public fecherFoundation.FCToolStripMenuItem mnuRemoveMember;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdOK = new fecherFoundation.FCButton();
            this.gridMembers = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddMember = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRemoveMember = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAddMember = new fecherFoundation.FCButton();
            this.cmdRemoveMember = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveMember)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 438);
            this.BottomPanel.Size = new System.Drawing.Size(722, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.gridMembers);
            this.ClientArea.Size = new System.Drawing.Size(742, 460);
            this.ClientArea.Controls.SetChildIndex(this.gridMembers, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdOK, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRemoveMember);
            this.TopPanel.Controls.Add(this.cmdAddMember);
            this.TopPanel.Size = new System.Drawing.Size(742, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddMember, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveMember, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(156, 28);
            this.HeaderText.Text = "ALE Members";
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "acceptButton";
            this.cmdOK.Location = new System.Drawing.Point(360, 390);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(60, 48);
            this.cmdOK.TabIndex = 1;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // gridMembers
            // 
            this.gridMembers.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridMembers.Cols = 3;
            this.gridMembers.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridMembers.ExtendLastCol = true;
            this.gridMembers.Location = new System.Drawing.Point(30, 30);
            this.gridMembers.Name = "gridMembers";
            this.gridMembers.ReadOnly = false;
            this.gridMembers.Rows = 1;
            this.gridMembers.Size = new System.Drawing.Size(700, 341);
            this.gridMembers.StandardTab = false;
            this.gridMembers.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridMembers.TabIndex = 0;
            this.gridMembers.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridMembers_CellChanged);
            this.gridMembers.KeyDown += new Wisej.Web.KeyEventHandler(this.gridMembers_KeyDownEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddMember,
            this.mnuRemoveMember,
            this.mnuSepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddMember
            // 
            this.mnuAddMember.Index = 0;
            this.mnuAddMember.Name = "mnuAddMember";
            this.mnuAddMember.Text = "Add Member";
            this.mnuAddMember.Click += new System.EventHandler(this.mnuAddMember_Click);
            // 
            // mnuRemoveMember
            // 
            this.mnuRemoveMember.Index = 1;
            this.mnuRemoveMember.Name = "mnuRemoveMember";
            this.mnuRemoveMember.Text = "Remove Member";
            this.mnuRemoveMember.Click += new System.EventHandler(this.mnuRemoveMember_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 2;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            this.mnuSepar2.Visible = false;
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Exit";
            this.mnuSaveExit.Visible = false;
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddMember
            // 
            this.cmdAddMember.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddMember.Location = new System.Drawing.Point(499, 29);
            this.cmdAddMember.Name = "cmdAddMember";
            this.cmdAddMember.Size = new System.Drawing.Size(95, 24);
            this.cmdAddMember.TabIndex = 1;
            this.cmdAddMember.Text = "Add Member";
            this.cmdAddMember.Click += new System.EventHandler(this.mnuAddMember_Click);
            // 
            // cmdRemoveMember
            // 
            this.cmdRemoveMember.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveMember.Location = new System.Drawing.Point(600, 29);
            this.cmdRemoveMember.Name = "cmdRemoveMember";
            this.cmdRemoveMember.Size = new System.Drawing.Size(115, 24);
            this.cmdRemoveMember.TabIndex = 2;
            this.cmdRemoveMember.Text = "Remove Member";
            this.cmdRemoveMember.Click += new System.EventHandler(this.mnuRemoveMember_Click);
            // 
            // frmALEMembers
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(742, 520);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmALEMembers";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ALE Members";
            this.Load += new System.EventHandler(this.frmALEMembers_Load);
            this.Resize += new System.EventHandler(this.frmALEMembers_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmALEMembers_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveMember)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdRemoveMember;
		private FCButton cmdAddMember;
	}
}