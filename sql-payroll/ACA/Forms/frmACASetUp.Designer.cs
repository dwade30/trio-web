﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmACASetUp.
	/// </summary>
	partial class frmACASetUp
	{
		public fecherFoundation.FCComboBox cmbPlan;
		public fecherFoundation.FCComboBox cmbOriginOfPolicy;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid gridDependents;
		public fecherFoundation.FCCheckBox chkCoverageDeclined;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid gridBox15;
		public FCGrid gridBox16;
		public FCGrid gridBox14;
		public fecherFoundation.FCLabel lblAll12;
		public fecherFoundation.FCLabel lblMonth12;
		public fecherFoundation.FCLabel lblMonth11;
		public fecherFoundation.FCLabel lblMonth10;
		public fecherFoundation.FCLabel lblMonth9;
		public fecherFoundation.FCLabel lblMonth8;
		public fecherFoundation.FCLabel lblMonth7;
		public fecherFoundation.FCLabel lblMonth6;
		public fecherFoundation.FCLabel lblMonth5;
		public fecherFoundation.FCLabel lblMonth4;
		public fecherFoundation.FCLabel lblMonth3;
		public fecherFoundation.FCLabel lblMonth2;
		public fecherFoundation.FCLabel lblMonth1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblEmployeeName;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuAddDependent;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteDependent;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSetup;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAll;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar5;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRecord;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbPlan = new fecherFoundation.FCComboBox();
            this.cmbOriginOfPolicy = new fecherFoundation.FCComboBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.gridDependents = new fecherFoundation.FCGrid();
            this.chkCoverageDeclined = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.GridZip = new fecherFoundation.FCGrid();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.gridBox15 = new fecherFoundation.FCGrid();
            this.gridBox16 = new fecherFoundation.FCGrid();
            this.gridBox14 = new fecherFoundation.FCGrid();
            this.lblAll12 = new fecherFoundation.FCLabel();
            this.lblMonth12 = new fecherFoundation.FCLabel();
            this.lblMonth11 = new fecherFoundation.FCLabel();
            this.lblMonth10 = new fecherFoundation.FCLabel();
            this.lblMonth9 = new fecherFoundation.FCLabel();
            this.lblMonth8 = new fecherFoundation.FCLabel();
            this.lblMonth7 = new fecherFoundation.FCLabel();
            this.lblMonth6 = new fecherFoundation.FCLabel();
            this.lblMonth5 = new fecherFoundation.FCLabel();
            this.lblMonth4 = new fecherFoundation.FCLabel();
            this.lblMonth3 = new fecherFoundation.FCLabel();
            this.lblMonth2 = new fecherFoundation.FCLabel();
            this.lblMonth1 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblEmployeeName = new fecherFoundation.FCLabel();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddDependent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteDependent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSetup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRecord = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdEmployee = new fecherFoundation.FCButton();
            this.cmdAddDependent = new fecherFoundation.FCButton();
            this.cmdDeleteDependent = new fecherFoundation.FCButton();
            this.cmdPrintSetup = new fecherFoundation.FCButton();
            this.cmdPrintAll = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDeleteRecord = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDependents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCoverageDeclined)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDependent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDependent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRecord)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 704);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbPlan);
            this.ClientArea.Controls.Add(this.cmbOriginOfPolicy);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.chkCoverageDeclined);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.lblEmployeeName);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblEmployeeNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblEmployeeName, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkCoverageDeclined, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbOriginOfPolicy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPlan, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdEmployee);
            this.TopPanel.Controls.Add(this.cmdAddDependent);
            this.TopPanel.Controls.Add(this.cmdDeleteDependent);
            this.TopPanel.Controls.Add(this.cmdPrintSetup);
            this.TopPanel.Controls.Add(this.cmdPrintAll);
            this.TopPanel.Controls.Add(this.cmdDeleteRecord);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRecord, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintSetup, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteDependent, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddDependent, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEmployee, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(245, 28);
            this.HeaderText.Text = "Employee Health Care";
            // 
            // cmbPlan
            // 
            this.cmbPlan.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPlan.Location = new System.Drawing.Point(601, 94);
            this.cmbPlan.Name = "cmbPlan";
            this.cmbPlan.Size = new System.Drawing.Size(314, 40);
            this.cmbPlan.TabIndex = 28;
            // 
            // cmbOriginOfPolicy
            // 
            this.cmbOriginOfPolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cmbOriginOfPolicy.Location = new System.Drawing.Point(267, 94);
            this.cmbOriginOfPolicy.Name = "cmbOriginOfPolicy";
            this.cmbOriginOfPolicy.Size = new System.Drawing.Size(314, 40);
            this.cmbOriginOfPolicy.TabIndex = 26;
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.gridDependents);
            this.Frame2.Location = new System.Drawing.Point(30, 471);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(1020, 233);
            this.Frame2.TabIndex = 25;
            this.Frame2.Text = "Covered Individuals";
            // 
            // gridDependents
            // 
            this.gridDependents.Cols = 21;
            this.gridDependents.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridDependents.ExtendLastCol = true;
            this.gridDependents.Location = new System.Drawing.Point(0, 30);
            this.gridDependents.Name = "gridDependents";
            this.gridDependents.ReadOnly = false;
            this.gridDependents.Rows = 1;
            this.gridDependents.Size = new System.Drawing.Size(1020, 109);
            this.gridDependents.StandardTab = false;
            this.gridDependents.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridDependents.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.gridDependents, "Use Ins and Del keys to add and remove dependents");
            this.gridDependents.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridDependents_CellChanged);
            this.gridDependents.KeyUp += new Wisej.Web.KeyEventHandler(this.gridDependents_KeyUpEvent);
            // 
            // chkCoverageDeclined
            // 
            this.chkCoverageDeclined.Location = new System.Drawing.Point(30, 94);
            this.chkCoverageDeclined.Name = "chkCoverageDeclined";
            this.chkCoverageDeclined.Size = new System.Drawing.Size(143, 22);
            this.chkCoverageDeclined.TabIndex = 1;
            this.chkCoverageDeclined.Text = "Coverage Declined";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.GridZip);
            this.Frame1.Controls.Add(this.fcLabel1);
            this.Frame1.Controls.Add(this.gridBox15);
            this.Frame1.Controls.Add(this.gridBox16);
            this.Frame1.Controls.Add(this.gridBox14);
            this.Frame1.Controls.Add(this.lblAll12);
            this.Frame1.Controls.Add(this.lblMonth12);
            this.Frame1.Controls.Add(this.lblMonth11);
            this.Frame1.Controls.Add(this.lblMonth10);
            this.Frame1.Controls.Add(this.lblMonth9);
            this.Frame1.Controls.Add(this.lblMonth8);
            this.Frame1.Controls.Add(this.lblMonth7);
            this.Frame1.Controls.Add(this.lblMonth6);
            this.Frame1.Controls.Add(this.lblMonth5);
            this.Frame1.Controls.Add(this.lblMonth4);
            this.Frame1.Controls.Add(this.lblMonth3);
            this.Frame1.Controls.Add(this.lblMonth2);
            this.Frame1.Controls.Add(this.lblMonth1);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.FormatCaption = false;
            this.Frame1.Location = new System.Drawing.Point(30, 154);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1020, 311);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "Section II";
            // 
            // GridZip
            // 
            this.GridZip.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridZip.Cols = 13;
            this.GridZip.ColumnHeadersVisible = false;
            this.GridZip.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridZip.ExtendLastCol = true;
            this.GridZip.FixedCols = 0;
            this.GridZip.FixedRows = 0;
            this.GridZip.Location = new System.Drawing.Point(127, 254);
            this.GridZip.Name = "GridZip";
            this.GridZip.ReadOnly = false;
            this.GridZip.RowHeadersVisible = false;
            this.GridZip.Rows = 1;
            this.GridZip.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.GridZip.Size = new System.Drawing.Size(873, 42);
            this.GridZip.StandardTab = false;
            this.GridZip.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridZip.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.GridZip, "Zip Code");
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(20, 268);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(48, 17);
            this.fcLabel1.TabIndex = 26;
            this.fcLabel1.Text = "BOX 17";
            this.ToolTip1.SetToolTip(this.fcLabel1, "Zip Code");
            // 
            // gridBox15
            // 
            this.gridBox15.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridBox15.Cols = 13;
            this.gridBox15.ColumnHeadersVisible = false;
            this.gridBox15.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridBox15.ExtendLastCol = true;
            this.gridBox15.FixedCols = 0;
            this.gridBox15.FixedRows = 0;
            this.gridBox15.Location = new System.Drawing.Point(127, 139);
            this.gridBox15.Name = "gridBox15";
            this.gridBox15.ReadOnly = false;
            this.gridBox15.RowHeadersVisible = false;
            this.gridBox15.Rows = 1;
            this.gridBox15.Size = new System.Drawing.Size(873, 42);
            this.gridBox15.StandardTab = false;
            this.gridBox15.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridBox15.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.gridBox15, "Employee share of lowest cost monthly premium, for self-only minimum value covera" +
        "ge");
            this.gridBox15.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridBox15_ValidateEdit);
            this.gridBox15.CurrentCellChanged += new System.EventHandler(this.gridBox15_RowColChange);
            // 
            // gridBox16
            // 
            this.gridBox16.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridBox16.Cols = 13;
            this.gridBox16.ColumnHeadersVisible = false;
            this.gridBox16.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridBox16.ExtendLastCol = true;
            this.gridBox16.FixedCols = 0;
            this.gridBox16.FixedRows = 0;
            this.gridBox16.Location = new System.Drawing.Point(127, 197);
            this.gridBox16.Name = "gridBox16";
            this.gridBox16.ReadOnly = false;
            this.gridBox16.RowHeadersVisible = false;
            this.gridBox16.Rows = 1;
            this.gridBox16.Size = new System.Drawing.Size(873, 42);
            this.gridBox16.StandardTab = false;
            this.gridBox16.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridBox16.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.gridBox16, "Applicable section 4980H safe harbor");
            this.gridBox16.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridBox16_ValidateEdit);
            this.gridBox16.CurrentCellChanged += new System.EventHandler(this.gridBox16_RowColChange);
            // 
            // gridBox14
            // 
            this.gridBox14.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridBox14.Cols = 13;
            this.gridBox14.ColumnHeadersVisible = false;
            this.gridBox14.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridBox14.ExtendLastCol = true;
            this.gridBox14.FixedCols = 0;
            this.gridBox14.FixedRows = 0;
            this.gridBox14.Location = new System.Drawing.Point(127, 80);
            this.gridBox14.Name = "gridBox14";
            this.gridBox14.ReadOnly = false;
            this.gridBox14.RowHeadersVisible = false;
            this.gridBox14.Rows = 1;
            this.gridBox14.Size = new System.Drawing.Size(873, 42);
            this.gridBox14.StandardTab = false;
            this.gridBox14.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridBox14.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.gridBox14, "Offer of coverage required code");
            this.gridBox14.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridBox14_ValidateEdit);
            this.gridBox14.CurrentCellChanged += new System.EventHandler(this.gridBox14_RowColChange);
            // 
            // lblAll12
            // 
            this.lblAll12.Location = new System.Drawing.Point(127, 34);
            this.lblAll12.Name = "lblAll12";
            this.lblAll12.Size = new System.Drawing.Size(86, 30);
            this.lblAll12.TabIndex = 24;
            this.lblAll12.Text = "ALL 12 MONTHS";
            // 
            // lblMonth12
            // 
            this.lblMonth12.Location = new System.Drawing.Point(788, 44);
            this.lblMonth12.Name = "lblMonth12";
            this.lblMonth12.Size = new System.Drawing.Size(33, 15);
            this.lblMonth12.TabIndex = 23;
            this.lblMonth12.Text = "DEC";
            // 
            // lblMonth11
            // 
            this.lblMonth11.Location = new System.Drawing.Point(749, 44);
            this.lblMonth11.Name = "lblMonth11";
            this.lblMonth11.Size = new System.Drawing.Size(33, 15);
            this.lblMonth11.TabIndex = 22;
            this.lblMonth11.Text = "NOV";
            // 
            // lblMonth10
            // 
            this.lblMonth10.Location = new System.Drawing.Point(703, 44);
            this.lblMonth10.Name = "lblMonth10";
            this.lblMonth10.Size = new System.Drawing.Size(28, 15);
            this.lblMonth10.TabIndex = 21;
            this.lblMonth10.Text = "OCT";
            // 
            // lblMonth9
            // 
            this.lblMonth9.Location = new System.Drawing.Point(645, 44);
            this.lblMonth9.Name = "lblMonth9";
            this.lblMonth9.Size = new System.Drawing.Size(33, 15);
            this.lblMonth9.TabIndex = 20;
            this.lblMonth9.Text = "SEP";
            // 
            // lblMonth8
            // 
            this.lblMonth8.Location = new System.Drawing.Point(585, 44);
            this.lblMonth8.Name = "lblMonth8";
            this.lblMonth8.Size = new System.Drawing.Size(32, 15);
            this.lblMonth8.TabIndex = 19;
            this.lblMonth8.Text = "AUG";
            // 
            // lblMonth7
            // 
            this.lblMonth7.Location = new System.Drawing.Point(531, 44);
            this.lblMonth7.Name = "lblMonth7";
            this.lblMonth7.Size = new System.Drawing.Size(25, 15);
            this.lblMonth7.TabIndex = 18;
            this.lblMonth7.Text = "JUL";
            // 
            // lblMonth6
            // 
            this.lblMonth6.Location = new System.Drawing.Point(478, 44);
            this.lblMonth6.Name = "lblMonth6";
            this.lblMonth6.Size = new System.Drawing.Size(29, 15);
            this.lblMonth6.TabIndex = 17;
            this.lblMonth6.Text = "JUN";
            // 
            // lblMonth5
            // 
            this.lblMonth5.Location = new System.Drawing.Point(424, 44);
            this.lblMonth5.Name = "lblMonth5";
            this.lblMonth5.Size = new System.Drawing.Size(34, 15);
            this.lblMonth5.TabIndex = 16;
            this.lblMonth5.Text = "MAY";
            // 
            // lblMonth4
            // 
            this.lblMonth4.Location = new System.Drawing.Point(372, 44);
            this.lblMonth4.Name = "lblMonth4";
            this.lblMonth4.Size = new System.Drawing.Size(29, 15);
            this.lblMonth4.TabIndex = 15;
            this.lblMonth4.Text = "APR";
            // 
            // lblMonth3
            // 
            this.lblMonth3.Location = new System.Drawing.Point(319, 44);
            this.lblMonth3.Name = "lblMonth3";
            this.lblMonth3.Size = new System.Drawing.Size(31, 15);
            this.lblMonth3.TabIndex = 14;
            this.lblMonth3.Text = "MAR";
            // 
            // lblMonth2
            // 
            this.lblMonth2.Location = new System.Drawing.Point(272, 44);
            this.lblMonth2.Name = "lblMonth2";
            this.lblMonth2.Size = new System.Drawing.Size(32, 15);
            this.lblMonth2.TabIndex = 13;
            this.lblMonth2.Text = "FEB";
            // 
            // lblMonth1
            // 
            this.lblMonth1.Location = new System.Drawing.Point(223, 44);
            this.lblMonth1.Name = "lblMonth1";
            this.lblMonth1.Size = new System.Drawing.Size(30, 15);
            this.lblMonth1.TabIndex = 12;
            this.lblMonth1.Text = "JAN";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 211);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(48, 17);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "BOX 16";
            this.ToolTip1.SetToolTip(this.Label4, "Applicable section 4980H safe harbor");
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 153);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(48, 17);
            this.Label3.TabIndex = 10;
            this.Label3.Text = "BOX 15";
            this.ToolTip1.SetToolTip(this.Label3, "Employee share of lowest cost monthly premium, for self-only minimum value covera" +
        "ge");
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(51, 15);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "BOX 14 ";
            this.ToolTip1.SetToolTip(this.Label2, "Offer of coverage required code");
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(601, 66);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(181, 15);
            this.Label6.TabIndex = 29;
            this.Label6.Text = "PLAN";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(267, 65);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(181, 15);
            this.Label5.TabIndex = 27;
            this.Label5.Text = "ORIGIN OF POLICY";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(357, 30);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(448, 15);
            this.lblEmployeeName.TabIndex = 8;
            this.lblEmployeeName.Text = "EMPLOYEE NAME";
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(163, 30);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(124, 15);
            this.lblEmployeeNumber.TabIndex = 7;
            this.lblEmployeeNumber.Text = "00000";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(89, 15);
            this.Label1.TabIndex = 30;
            this.Label1.Text = "EMPLOYEE";
            this.Label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEmployee,
            this.mnuSepar3,
            this.mnuAddDependent,
            this.mnuDeleteDependent,
            this.mnuSepar4,
            this.mnuPrintSetup,
            this.mnuPrintAll,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar5,
            this.mnuDeleteRecord,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuEmployee
            // 
            this.mnuEmployee.Index = 0;
            this.mnuEmployee.Name = "mnuEmployee";
            this.mnuEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuEmployee.Text = "Select Employee";
            this.mnuEmployee.Click += new System.EventHandler(this.mnuEmployee_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuAddDependent
            // 
            this.mnuAddDependent.Index = 2;
            this.mnuAddDependent.Name = "mnuAddDependent";
            this.mnuAddDependent.Text = "Add Dependent";
            this.mnuAddDependent.Click += new System.EventHandler(this.mnuAddDependent_Click);
            // 
            // mnuDeleteDependent
            // 
            this.mnuDeleteDependent.Index = 3;
            this.mnuDeleteDependent.Name = "mnuDeleteDependent";
            this.mnuDeleteDependent.Text = "Delete Dependent";
            this.mnuDeleteDependent.Click += new System.EventHandler(this.mnuDeleteDependent_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = 4;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuPrintSetup
            // 
            this.mnuPrintSetup.Index = 5;
            this.mnuPrintSetup.Name = "mnuPrintSetup";
            this.mnuPrintSetup.Text = "Print Setup";
            this.mnuPrintSetup.Click += new System.EventHandler(this.mnuPrintSetup_Click);
            // 
            // mnuPrintAll
            // 
            this.mnuPrintAll.Index = 6;
            this.mnuPrintAll.Name = "mnuPrintAll";
            this.mnuPrintAll.Text = "Print All";
            this.mnuPrintAll.Click += new System.EventHandler(this.mnuPrintAll_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 7;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 8;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 9;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar5
            // 
            this.mnuSepar5.Index = 10;
            this.mnuSepar5.Name = "mnuSepar5";
            this.mnuSepar5.Text = "-";
            // 
            // mnuDeleteRecord
            // 
            this.mnuDeleteRecord.Index = 11;
            this.mnuDeleteRecord.Name = "mnuDeleteRecord";
            this.mnuDeleteRecord.Text = "Delete Record";
            this.mnuDeleteRecord.Click += new System.EventHandler(this.mnuDeleteRecord_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 12;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 13;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdEmployee
            // 
            this.cmdEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEmployee.Location = new System.Drawing.Point(455, 29);
            this.cmdEmployee.Name = "cmdEmployee";
            this.cmdEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdEmployee.Size = new System.Drawing.Size(124, 24);
            this.cmdEmployee.TabIndex = 1;
            this.cmdEmployee.Text = "Select Employee";
            this.cmdEmployee.Click += new System.EventHandler(this.mnuEmployee_Click);
            // 
            // cmdAddDependent
            // 
            this.cmdAddDependent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddDependent.Location = new System.Drawing.Point(585, 29);
            this.cmdAddDependent.Name = "cmdAddDependent";
            this.cmdAddDependent.Size = new System.Drawing.Size(120, 24);
            this.cmdAddDependent.TabIndex = 2;
            this.cmdAddDependent.Text = "Add Dependent";
            this.cmdAddDependent.Click += new System.EventHandler(this.mnuAddDependent_Click);
            // 
            // cmdDeleteDependent
            // 
            this.cmdDeleteDependent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteDependent.Location = new System.Drawing.Point(711, 29);
            this.cmdDeleteDependent.Name = "cmdDeleteDependent";
            this.cmdDeleteDependent.Size = new System.Drawing.Size(130, 24);
            this.cmdDeleteDependent.TabIndex = 3;
            this.cmdDeleteDependent.Text = "Delete Dependent";
            this.cmdDeleteDependent.Click += new System.EventHandler(this.mnuDeleteDependent_Click);
            // 
            // cmdPrintSetup
            // 
            this.cmdPrintSetup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintSetup.Location = new System.Drawing.Point(847, 29);
            this.cmdPrintSetup.Name = "cmdPrintSetup";
            this.cmdPrintSetup.Size = new System.Drawing.Size(84, 24);
            this.cmdPrintSetup.TabIndex = 4;
            this.cmdPrintSetup.Text = "Print Setup";
            this.cmdPrintSetup.Click += new System.EventHandler(this.mnuPrintSetup_Click);
            // 
            // cmdPrintAll
            // 
            this.cmdPrintAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintAll.Location = new System.Drawing.Point(937, 29);
            this.cmdPrintAll.Name = "cmdPrintAll";
            this.cmdPrintAll.Size = new System.Drawing.Size(66, 24);
            this.cmdPrintAll.TabIndex = 5;
            this.cmdPrintAll.Text = "Print All";
            this.cmdPrintAll.Click += new System.EventHandler(this.mnuPrintAll_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(470, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(81, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdDeleteRecord
            // 
            this.cmdDeleteRecord.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteRecord.Location = new System.Drawing.Point(1009, 29);
            this.cmdDeleteRecord.Name = "cmdDeleteRecord";
            this.cmdDeleteRecord.Size = new System.Drawing.Size(105, 24);
            this.cmdDeleteRecord.TabIndex = 6;
            this.cmdDeleteRecord.Text = "Delete Record";
            this.cmdDeleteRecord.Click += new System.EventHandler(this.mnuDeleteRecord_Click);
            // 
            // frmACASetUp
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmACASetUp";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Employee Health Care";
            this.Load += new System.EventHandler(this.frmACASetUp_Load);
            this.Resize += new System.EventHandler(this.frmACASetUp_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmACASetUp_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDependents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCoverageDeclined)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDependent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDependent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRecord)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdPrintAll;
        private FCButton cmdPrintSetup;
        private FCButton cmdDeleteDependent;
        private FCButton cmdAddDependent;
        private FCButton cmdEmployee;
        private FCButton cmdSave;
        private FCButton cmdDeleteRecord;
        public FCGrid GridZip;
        public FCLabel fcLabel1;
    }
}
