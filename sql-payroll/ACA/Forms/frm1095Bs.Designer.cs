//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frm1095Bs.
	/// </summary>
	partial class frm1095Bs
	{
		public fecherFoundation.FCComboBox cmbIRSCopies;
		public fecherFoundation.FCLabel lblIRSCopies;
		public fecherFoundation.FCComboBox cmbPortrait;
		public fecherFoundation.FCLabel lblPortrait;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtEmployeeHorizontal;
		public fecherFoundation.FCTextBox txtIRSHorizontal;
		public fecherFoundation.FCTextBox txtEmployeeVertical;
		public fecherFoundation.FCTextBox txtIRSVertical;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSaveContinue;
		public fecherFoundation.FCButton cmdPrintTest;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbIRSCopies = new fecherFoundation.FCComboBox();
            this.lblIRSCopies = new fecherFoundation.FCLabel();
            this.cmbPortrait = new fecherFoundation.FCComboBox();
            this.lblPortrait = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtEmployeeHorizontal = new fecherFoundation.FCTextBox();
            this.txtIRSHorizontal = new fecherFoundation.FCTextBox();
            this.txtEmployeeVertical = new fecherFoundation.FCTextBox();
            this.txtIRSVertical = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.cmdPrintTest = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 400);
            this.BottomPanel.Size = new System.Drawing.Size(659, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbIRSCopies);
            this.ClientArea.Controls.Add(this.lblIRSCopies);
            this.ClientArea.Controls.Add(this.cmbPortrait);
            this.ClientArea.Controls.Add(this.lblPortrait);
            this.ClientArea.Size = new System.Drawing.Size(659, 340);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintTest);
            this.TopPanel.Size = new System.Drawing.Size(659, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintTest, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(161, 30);
            this.HeaderText.Text = "Print 1095-Bs";
            // 
            // cmbIRSCopies
            // 
            this.cmbIRSCopies.AutoSize = false;
            this.cmbIRSCopies.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbIRSCopies.FormattingEnabled = true;
            this.cmbIRSCopies.Items.AddRange(new object[] {
            "Print Employee Copies",
            "Print IRS Copies"});
            this.cmbIRSCopies.Location = new System.Drawing.Point(269, 30);
            this.cmbIRSCopies.Name = "cmbIRSCopies";
            this.cmbIRSCopies.Size = new System.Drawing.Size(365, 40);
            this.cmbIRSCopies.TabIndex = 8;
            this.cmbIRSCopies.Text = "Print Employee Copies";
            // 
            // lblIRSCopies
            // 
            this.lblIRSCopies.AutoSize = true;
            this.lblIRSCopies.Location = new System.Drawing.Point(30, 44);
            this.lblIRSCopies.Name = "lblIRSCopies";
            this.lblIRSCopies.Size = new System.Drawing.Size(54, 15);
            this.lblIRSCopies.TabIndex = 9;
            this.lblIRSCopies.Text = "COPIES";
            // 
            // cmbPortrait
            // 
            this.cmbPortrait.AutoSize = false;
            this.cmbPortrait.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPortrait.FormattingEnabled = true;
            this.cmbPortrait.Items.AddRange(new object[] {
            "Landscape Pre-Printed",
            "Landscape Blank",
            "Portrait Pre-Printed (for windowed envelopes)",
            "Portrait Blank (for windowed envelopes)"});
            this.cmbPortrait.Location = new System.Drawing.Point(269, 90);
            this.cmbPortrait.Name = "cmbPortrait";
            this.cmbPortrait.Size = new System.Drawing.Size(365, 40);
            this.cmbPortrait.TabIndex = 10;
            // 
            // lblPortrait
            // 
            this.lblPortrait.AutoSize = true;
            this.lblPortrait.Location = new System.Drawing.Point(30, 104);
            this.lblPortrait.Name = "lblPortrait";
            this.lblPortrait.Size = new System.Drawing.Size(196, 15);
            this.lblPortrait.TabIndex = 11;
            this.lblPortrait.Text = "EMPLOYEE COPY FORM STYLE";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtEmployeeHorizontal);
            this.Frame2.Controls.Add(this.txtIRSHorizontal);
            this.Frame2.Controls.Add(this.txtEmployeeVertical);
            this.Frame2.Controls.Add(this.txtIRSVertical);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Location = new System.Drawing.Point(30, 150);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(349, 178);
            this.Frame2.TabIndex = 7;
            this.Frame2.Text = "Alignment Adjustment";
            // 
            // txtEmployeeHorizontal
            // 
            this.txtEmployeeHorizontal.AutoSize = false;
            this.txtEmployeeHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeHorizontal.LinkItem = null;
            this.txtEmployeeHorizontal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployeeHorizontal.LinkTopic = null;
            this.txtEmployeeHorizontal.Location = new System.Drawing.Point(249, 118);
            this.txtEmployeeHorizontal.Name = "txtEmployeeHorizontal";
            this.txtEmployeeHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeHorizontal.TabIndex = 13;
            this.txtEmployeeHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtEmployeeHorizontal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmployeeHorizontal_KeyPress);
            // 
            // txtIRSHorizontal
            // 
            this.txtIRSHorizontal.AutoSize = false;
            this.txtIRSHorizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSHorizontal.LinkItem = null;
            this.txtIRSHorizontal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSHorizontal.LinkTopic = null;
            this.txtIRSHorizontal.Location = new System.Drawing.Point(249, 58);
            this.txtIRSHorizontal.Name = "txtIRSHorizontal";
            this.txtIRSHorizontal.Size = new System.Drawing.Size(80, 40);
            this.txtIRSHorizontal.TabIndex = 11;
            this.txtIRSHorizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSHorizontal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSHorizontal_KeyPress);
            // 
            // txtEmployeeVertical
            // 
            this.txtEmployeeVertical.AutoSize = false;
            this.txtEmployeeVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeVertical.LinkItem = null;
            this.txtEmployeeVertical.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployeeVertical.LinkTopic = null;
            this.txtEmployeeVertical.Location = new System.Drawing.Point(149, 118);
            this.txtEmployeeVertical.Name = "txtEmployeeVertical";
            this.txtEmployeeVertical.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeVertical.TabIndex = 12;
            this.txtEmployeeVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtEmployeeVertical.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmployeeVertical_KeyPress);
            // 
            // txtIRSVertical
            // 
            this.txtIRSVertical.AutoSize = false;
            this.txtIRSVertical.BackColor = System.Drawing.SystemColors.Window;
            this.txtIRSVertical.LinkItem = null;
            this.txtIRSVertical.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIRSVertical.LinkTopic = null;
            this.txtIRSVertical.Location = new System.Drawing.Point(149, 58);
            this.txtIRSVertical.Name = "txtIRSVertical";
            this.txtIRSVertical.Size = new System.Drawing.Size(80, 40);
            this.txtIRSVertical.TabIndex = 10;
            this.txtIRSVertical.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtIRSVertical.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtIRSVertical_KeyPress);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(249, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(77, 19);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "HORIZONTAL";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(149, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(63, 19);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "VERTICAL";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 132);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(67, 16);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "EMPLOYEE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 72);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(74, 19);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "IRS";
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(271, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(80, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Print";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // cmdPrintTest
            // 
            this.cmdPrintTest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintTest.AppearanceKey = "toolbarButton";
            this.cmdPrintTest.Location = new System.Drawing.Point(555, 29);
            this.cmdPrintTest.Name = "cmdPrintTest";
            this.cmdPrintTest.Size = new System.Drawing.Size(75, 24);
            this.cmdPrintTest.TabIndex = 1;
            this.cmdPrintTest.Text = "Print Test";
            this.cmdPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // frm1095Bs
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(659, 508);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frm1095Bs";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print 1095-Bs";
            this.Load += new System.EventHandler(this.frm1095Bs_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frm1095Bs_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintTest)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}