﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmACAElectronicFiling : BaseForm
	{
		public frmACAElectronicFiling()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmACAElectronicFiling InstancePtr
		{
			get
			{
				return (frmACAElectronicFiling)Sys.GetInstance(typeof(frmACAElectronicFiling));
			}
		}

		protected frmACAElectronicFiling _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cACAElectronicFilingView theView;
		private bool boolUpdating;

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			theView.Browse();
		}

		private void Command2_Click(object sender, System.EventArgs e)
		{
			SaveACABFile();
		}

		private void SaveACABFile()
		{
			SaveSettings();
			theView.Save1095Bs();
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveACACFile()
		{
			SaveSettings();
			theView.Save1095Cs();
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Command3_Click(object sender, System.EventArgs e)
		{
			SaveACACFile();
		}

		private void frmACAElectronicFiling_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmACAElectronicFiling_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public void Init(int intYear)
		{
			this.Show(App.MainForm);
			theView = new cACAElectronicFilingView();
            theView.FileSaved += theView_FileSaved;
            theView.ViewUpdated += theView_ViewUpdated;
			theView.Load(intYear);
			mnuScenario6Correction.Visible = theView.AllowScenario6Correction;
			mnuScenario2C.Visible = theView.AllowScenario2Correction;
			mnuScenario8C.Visible = theView.AllowScenario8Correction;
			mnuScenario4C.Visible = theView.AllowScenario4Correction;
			mnuScenario5C.Visible = theView.AllowScenario5Correction;
			mnuScenario7C.Visible = theView.AllowScenario7Correction;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SaveSettings()
		{
			boolUpdating = true;
			theView.TCCCode = txtTCC.Text;            
            theView.SaveFilePath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;            
            boolUpdating = false;
			theView.Save();
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				//MessageBox.Show("Settings saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FCMessageBox.Show("Settings saved", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Saved");
			}
		}

		private void mnuScenario2C_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario2C();
		}

		private void mnuScenario4C_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario4C();
		}

		private void mnuScenario5C_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario5C();
		}

		private void mnuScenario6Correction_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario6C();
		}

		private void mnuScenario7C_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario7C();
		}

		private void mnuScenario8C_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario8C();
		}

		private void mnuTestScenario1_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario1();
		}

		private void SaveTestScenario1()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(1));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario2()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(2));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario2C()
		{
			SaveSettings();
			theView.SaveScenario("2C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario3()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(3));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario4()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(4));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario5()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(5));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario7()
		{
			SaveSettings();
			theView.SaveScenario(FCConvert.ToString(7));
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario6C()
		{
			SaveSettings();
			theView.SaveScenario("6C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario7C()
		{
			SaveSettings();
			theView.SaveScenario("7C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario4C()
		{
			SaveSettings();
			theView.SaveScenario("4C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario5C()
		{
			SaveSettings();
			theView.SaveScenario("5C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario8C()
		{
			SaveSettings();
			theView.SaveScenario("8C");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario6()
		{
			SaveSettings();
			theView.SaveScenario("6");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveTestScenario8()
		{
			SaveSettings();
			theView.SaveScenario("8");
			if (theView.LastErrorNumber > 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + "  " + theView.LastErrorDescription, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuTestScenario2_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario2();
		}

		private void mnuTestScenario3_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario3();
		}

		private void mnuTestScenario4_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario4();
		}

		private void mnuTestScenario5_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario5();
		}

		private void mnuTestScenario6_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario6();
		}

		private void mnuTestScenario7_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario7();
		}

		private void mnuTestScenario8_Click(object sender, System.EventArgs e)
		{
			SaveTestScenario8();
		}

		private void theView_DestinationPathChanged()
		{
			if (!boolUpdating)
			{
				txtPath.Text = theView.SaveFilePath;
			}
		}

		private void theView_FileSaved(string strFileName)
		{
			MessageBox.Show("File " + strFileName + " and manifest_" + strFileName + " saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void theView_ViewUpdated()
		{
			if (!boolUpdating)
			{
				boolUpdating = true;
				txtPath.Text = theView.SaveFilePath;
				txtTCC.Text = theView.TCCCode;
				boolUpdating = false;
			}
		}

		private void txtTCC_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				boolUpdating = true;
				theView.TCCCode = txtTCC.Text;
				boolUpdating = false;
			}
		}

		private void txtTCC_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolUpdating)
			{
				boolUpdating = true;
				theView.TCCCode = txtTCC.Text;
				boolUpdating = false;
			}
		}
	}
}
