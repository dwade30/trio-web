﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmACADataImport : BaseForm
	{
		public frmACADataImport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmACADataImport InstancePtr
		{
			get
			{
				return (frmACADataImport)Sys.GetInstance(typeof(frmACADataImport));
			}
		}

		protected frmACADataImport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cACADataImportFormatView theView = new cACADataImportFormatView();
		private bool boolRefreshing;
		const int cnstSeparComma = 0;
		const int cnstSeparPipe = 1;
		const int cnstSeparSemiColon = 2;
		const int cnstSeparTab = 3;
		private int intDefaultImportYear;
		// vbPorter upgrade warning: intDefaultYear As int	OnWriteFCConvert.ToInt32(
		public void Init(int intDefaultYear)
		{
			intDefaultImportYear = intDefaultYear;
			this.Show(App.MainForm);
		}

		private string MonthAbbrev(int intMonth)
		{
			string MonthAbbrev = "";
			string strMonth;
			strMonth = "";
			switch (intMonth)
			{
				case 1:
					{
						strMonth = "Jan";
						break;
					}
				case 2:
					{
						strMonth = "Feb";
						break;
					}
				case 3:
					{
						strMonth = "Mar";
						break;
					}
				case 4:
					{
						strMonth = "Apr";
						break;
					}
				case 5:
					{
						strMonth = "May";
						break;
					}
				case 6:
					{
						strMonth = "Jun";
						break;
					}
				case 7:
					{
						strMonth = "Jul";
						break;
					}
				case 8:
					{
						strMonth = "Aug";
						break;
					}
				case 9:
					{
						strMonth = "Sep";
						break;
					}
				case 10:
					{
						strMonth = "Oct";
						break;
					}
				case 11:
					{
						strMonth = "Nov";
						break;
					}
				case 12:
					{
						strMonth = "Dec";
						break;
					}
			}
			//end switch
			MonthAbbrev = strMonth;
			return MonthAbbrev;
		}

		private void chkAll12_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkAll12.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("All 12");
				}
				else
				{
					theView.RemoveColumn("All 12");
				}
				RefreshGrid();
			}
		}

		private void chkColEmployeeSSN_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkColEmployeeSSN.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("Emp SSN");
				}
				else
				{
					theView.RemoveColumn("Emp SSN");
				}
				RefreshGrid();
			}
		}

		private void chkDOB_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkDOB.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("DOB");
				}
				else
				{
					theView.RemoveColumn("DOB");
				}
				RefreshGrid();
			}
		}

		private void chkFirst_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkFirst.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("First");
				}
				else
				{
					theView.RemoveColumn("First");
				}
				RefreshGrid();
			}
		}
		// Private Sub chkFullName_Click()
		// If Not boolRefreshing Then
		// If chkFullName.Value = vbChecked Then
		// Call theView.AddColumn("Full Name")
		// Else
		// Call theView.RemoveColumn("Full Name")
		// End If
		// RefreshGrid
		// End If
		// End Sub
		private void chkHeader_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentFormat)
				{
					theView.CurrentImportFormat.FirstLineUnreadable = chkHeader.CheckState == Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void chkLast_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkLast.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("Last");
				}
				else
				{
					theView.RemoveColumn("Last");
				}
				RefreshGrid();
			}
		}

		private void chkMiddle_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkMiddle.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("Middle");
				}
				else
				{
					theView.RemoveColumn("Middle");
				}
				RefreshGrid();
			}
		}

		private void chkMonths_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				int x;
				if (chkMonths.CheckState == Wisej.Web.CheckState.Checked)
				{
					for (x = 1; x <= 12; x++)
					{
						theView.AddColumn(MonthAbbrev(x));
					}
					// x
				}
				else
				{
					for (x = 1; x <= 12; x++)
					{
						theView.RemoveColumn(MonthAbbrev(x));
					}
					// x
				}
				RefreshGrid();
			}
		}

		private void chkSSN_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkSSN.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("SSN");
				}
				else
				{
					theView.RemoveColumn("SSN");
				}
				RefreshGrid();
			}
		}

		private void chkSuffix_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (chkSuffix.CheckState == Wisej.Web.CheckState.Checked)
				{
					theView.AddColumn("Sfx");
				}
				else
				{
					theView.RemoveColumn("Sfx");
				}
				RefreshGrid();
			}
		}

		private void cmbFormats_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				int lngID = 0;
				lngID = cmbFormats.ItemData(cmbFormats.SelectedIndex);
				if (theView.IsCurrentFormat)
				{
					if (theView.CurrentImportFormat.ID != lngID)
					{
						if (theView.Changed)
						{
							if (OkToChangeFormats())
							{
							}
						}
					}
				}
				if (lngID > 0)
				{
					theView.LoadFormat(lngID);
				}
				else
				{
					theView.AddFormat();
				}
				ShowView();
			}
		}

		private void cmbPlanYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				theView.PlanYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbPlanYear.Text)));
			}
		}

		private void cmbSeparator_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentFormat)
				{
					switch (cmbSeparator.SelectedIndex)
					{
						case cnstSeparPipe:
							{
								theView.CurrentImportFormat.SeparatorCharacter = "|";
								break;
							}
						case cnstSeparSemiColon:
							{
								theView.CurrentImportFormat.SeparatorCharacter = ";";
								break;
							}
						case cnstSeparTab:
							{
								theView.CurrentImportFormat.SeparatorCharacter = "\t";
								break;
							}
						default:
							{
								theView.CurrentImportFormat.SeparatorCharacter = ",";
								break;
							}
					}
					//end switch
				}
			}
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			BrowseForFile();
		}

		private void BrowseForFile()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Select import file";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
                //FC:FINAL:BSE:#4246 display the file name instead of directory path 
                //lblFileName.Text = MDIParent.InstancePtr.CommonDialog1.FileName;
                lblFileName.Text = System.IO.Path.GetFileName(MDIParent.InstancePtr.CommonDialog1.FileName);
                theView.FileName = lblFileName.Text;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		private void cmdImport_Click(object sender, System.EventArgs e)
		{
			theView.PlanYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbPlanYear.Text)));
			ImportFormat();
		}

		private void ImportFormat()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cGenericCollection collBadEmployees;
				bool boolSuccess = false;
				if (theView.IsCurrentFormat)
				{
					if (fecherFoundation.Strings.Trim(lblFileName.Text) == "")
					{
						MessageBox.Show("You must choose a file before importing", "No File Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					collBadEmployees = theView.ImportFormat();
					boolSuccess = true;
					if (!(collBadEmployees == null))
					{
						if (collBadEmployees.ItemCount() > 0)
						{
							boolSuccess = false;
							rptGenericList.InstancePtr.Init(ref collBadEmployees, "Data Import Errors", "Import Errors");
						}
					}
					if (boolSuccess)
					{
						MessageBox.Show("Data imported for plan year " + FCConvert.ToString(theView.PlanYear), "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdPlaceHolder_Click(object sender, System.EventArgs e)
		{
			if (!(theView.CurrentImportFormat == null))
			{
				string strName = "";
				// vbPorter upgrade warning: strNum As string	OnWrite(string, int)
				string strNum = "";
				int intMax = 0;
				int intTemp = 0;
				intMax = 0;
				foreach (cACAImportFormatColumn tCol in theView.CurrentImportFormat.Fields)
				{
					strName = tCol.ColumnName;
					strNum = "";
					if (strName.Length > 3)
					{
						if (fecherFoundation.Strings.LCase(Strings.Left(strName, 4)) == "misc")
						{
							if (strName.Length > 4)
							{
								strNum = Strings.Mid(strName, 5);
							}
							else
							{
								strNum = FCConvert.ToString(1);
							}
						}
					}
					if (strNum != "")
					{
						intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strNum)));
						if (intTemp > intMax)
							intMax = intTemp;
					}
				}
				strName = "Misc";
				if (intMax > 0)
				{
					strName += FCConvert.ToString(intMax + 1);
				}
				theView.AddColumn(strName);
				RefreshGrid();
			}
		}

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			PreviewFormat();
		}

		private void PreviewFormat()
		{
			if (theView.IsCurrentFormat)
			{
				if (fecherFoundation.Strings.Trim(lblFileName.Text) == "")
				{
					MessageBox.Show("You must choose a file to preview", "No File Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				theView.PreviewFormat(3);
				RefreshPreview();
			}
		}

		private void Form_Initialize()
		{
			intDefaultImportYear = 0;
		}

		private void frmACADataImport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmACADataImport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmACADataImport properties;
			//frmACADataImport.FillStyle	= 0;
			//frmACADataImport.ScaleWidth	= 9300;
			//frmACADataImport.ScaleHeight	= 7650;
			//frmACADataImport.LinkTopic	= "Form2";
			//frmACADataImport.LockControls	= -1  'True;
			//frmACADataImport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupForm();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(DialogResult)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (theView.IsCurrentFormat)
			{
				if (theView.Changed)
				{
					if (!OkToChangeFormats())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void GridFormat_AfterMoveColumn(object sender, EventArgs e)
		{
			if (!boolRefreshing)
			{
				theView.ClearColumns();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				for (x = 0; x <= (GridFormat.Cols - 1); x++)
				{
					theView.AddColumn(GridFormat.TextMatrix(0, x));
				}
				// x
				// RefreshGrid
			}
		}

		private void mnuAddNewFormat_Click(object sender, System.EventArgs e)
		{
			AddNewFormat();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void AddNewFormat()
		{
			if (OkToChangeFormats())
			{
				theView.AddFormat();
				ShowView();
			}
		}

		private bool SaveFormat()
		{
			bool SaveFormat = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn = false;
				if (fecherFoundation.Strings.Trim(txtDescription.Text) == "")
				{
					MessageBox.Show("You must enter a description before saving", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveFormat;
				}
				if (!(theView.CurrentImportFormat == null))
				{
					// theView.CurrentImportFormat.Description = txtDescription.Text
					// theView.CurrentImportFormat.FirstLineUnreadable = chkHeader.Value = vbChecked
					// Select Case cmbSeparator.ListIndex
					// Case cnstSeparPipe
					// theView.CurrentImportFormat.SeparatorCharacter = "|"
					// Case cnstSeparSemiColon
					// theView.CurrentImportFormat.SeparatorCharacter = ";"
					// Case cnstSeparTab
					// theView.CurrentImportFormat.SeparatorCharacter = vbTab
					// Case Else
					// theView.CurrentImportFormat.SeparatorCharacter = ","
					// End Select
					boolReturn = theView.SaveFormat();
					if (boolReturn)
					{
						RefreshFormatList(theView.CurrentImportFormat.ID);
						MessageBox.Show("Format Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				SaveFormat = boolReturn;
				return SaveFormat;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveFormat;
		}

		private bool OkToChangeFormats()
		{
			bool OkToChangeFormats = false;
			bool boolReturn;
			boolReturn = true;
			if (!(theView.CurrentImportFormat == null))
			{
				if (theView.Changed)
				{
					// vbPorter upgrade warning: intReturn As int	OnWrite(DialogResult)
					DialogResult intReturn = 0;
					intReturn = MessageBox.Show("Changes have been made to the current format." + "\r\n" + "Save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
					if (intReturn == DialogResult.Cancel)
					{
						boolReturn = false;
					}
					else if (intReturn == DialogResult.Yes)
					{
						if (!SaveFormat())
						{
							boolReturn = false;
						}
					}
				}
			}
			OkToChangeFormats = boolReturn;
			return OkToChangeFormats;
		}

		private void SetupForm()
		{
			boolRefreshing = true;
			cmbSeparator.AddItem(", Comma");
			cmbSeparator.AddItem("| Pipe");
			cmbSeparator.AddItem("; Semicolon");
			cmbSeparator.AddItem("Tab");
			cmbPlanYear.Clear();
			cmbPlanYear.AddItem((DateTime.Today.Year - 1).ToString());
			cmbPlanYear.AddItem((DateTime.Today.Year).ToString());
			cmbPlanYear.AddItem((DateTime.Today.Year + 1).ToString());
			int x;
			for (x = 0; x <= cmbPlanYear.Items.Count - 1; x++)
			{
				if (FCConvert.ToDouble(cmbPlanYear.Items[x].ToString()) == intDefaultImportYear)
				{
					cmbPlanYear.SelectedIndex = x;
					break;
				}
			}
			// x
			if (cmbPlanYear.SelectedIndex < 0)
			{
				cmbPlanYear.SelectedIndex = 1;
			}
			theView.LoadFormats();
			RefreshFormatList(0);
		}

		private void RefreshFormatList(int lngID)
		{
			cGenericCollection tList;
			tList = theView.ListOfFormats;
			cACADataImportFormat tFormat;
			boolRefreshing = true;
			cmbFormats.Clear();
			cmbFormats.AddItem("New");
			cmbFormats.ItemData(cmbFormats.NewIndex, 0);
			if (!(tList == null))
			{
				tList.MoveFirst();
				while (tList.IsCurrent())
				{
					tFormat = (cACADataImportFormat)tList.GetCurrentItem();
					cmbFormats.AddItem(tFormat.Description);
					cmbFormats.ItemData(cmbFormats.NewIndex, tFormat.ID);
					tList.MoveNext();
				}
			}
			boolRefreshing = false;
			if (cmbFormats.Items.Count > 1)
			{
				if (lngID > 0)
				{
					int x;
					for (x = 0; x <= cmbFormats.Items.Count - 1; x++)
					{
						if (cmbFormats.ItemData(x) == lngID)
						{
							cmbFormats.SelectedIndex = x;
							return;
						}
					}
					// x
				}
				else
				{
					cmbFormats.SelectedIndex = 1;
				}
			}
			else
			{
				cmbFormats.SelectedIndex = 0;
			}
			if (cmbFormats.SelectedIndex < 0)
				cmbFormats.SelectedIndex = 0;
			boolRefreshing = false;
		}

		private void ShowView()
		{
			boolRefreshing = true;
			cACADataImportFormat impFormat;
			impFormat = theView.CurrentImportFormat;
			if (!(impFormat == null))
			{
				if (impFormat.FirstLineUnreadable)
				{
					chkHeader.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkHeader.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if ((impFormat.SeparatorCharacter) == "|")
				{
					cmbSeparator.SelectedIndex = cnstSeparPipe;
				}
				else if ((impFormat.SeparatorCharacter) == ";")
				{
					cmbSeparator.SelectedIndex = cnstSeparSemiColon;
				}
				else if ((impFormat.SeparatorCharacter) == "\t")
				{
					cmbSeparator.SelectedIndex = cnstSeparTab;
				}
				else
				{
					cmbSeparator.SelectedIndex = cnstSeparComma;
				}
				txtDescription.Text = impFormat.Description;
			}
			if (impFormat.EmployeeIdentifierType == modGlobalVariables.ACAEmployeeIdentifierType.SSN)
			{
				cmbSSN.Text = "SSN";
			}
			else
			{
				cmbSSN.Text = "Employee Number";
			}
			RefreshGrid();
			boolRefreshing = false;
		}

		private void RefreshGrid()
		{
			boolRefreshing = true;
			cACADataImportFormat impFormat;
			impFormat = theView.CurrentImportFormat;
			GridFormat.Cols = 0;
			GridFormat.Rows = 1;
			chkColEmployeeSSN.CheckState = Wisej.Web.CheckState.Unchecked;
			chkDOB.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSSN.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAll12.CheckState = Wisej.Web.CheckState.Unchecked;
			chkFirst.CheckState = Wisej.Web.CheckState.Unchecked;
			// chkFullName.Value = vbUnchecked
			chkMiddle.CheckState = Wisej.Web.CheckState.Unchecked;
			chkLast.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSuffix.CheckState = Wisej.Web.CheckState.Unchecked;
			chkMonths.CheckState = Wisej.Web.CheckState.Unchecked;
			if (!(impFormat == null))
			{
				foreach (cACAImportFormatColumn Col in impFormat.Fields)
				{
					GridFormat.Cols += 1;
					GridFormat.TextMatrix(0, GridFormat.Cols - 1, Col.ColumnName);
					if (fecherFoundation.Strings.LCase(Col.ColumnName) == "emp ssn")
					{
						chkColEmployeeSSN.CheckState = Wisej.Web.CheckState.Checked;
						// Case "full name"
						// chkFullName.Value = vbChecked
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "first")
					{
						chkFirst.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "middle")
					{
						chkMiddle.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "last")
					{
						chkLast.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "sfx")
					{
						chkSuffix.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "ssn")
					{
						chkSSN.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "dob")
					{
						chkDOB.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if (fecherFoundation.Strings.LCase(Col.ColumnName) == "all 12")
					{
						chkAll12.CheckState = Wisej.Web.CheckState.Checked;
					}
					else if ((fecherFoundation.Strings.LCase(Col.ColumnName) == "jan") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "feb") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "mar") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "apr") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "may") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "june") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "july") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "aug") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "sep") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "oct") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "nov") || (fecherFoundation.Strings.LCase(Col.ColumnName) == "dec"))
					{
						chkMonths.CheckState = Wisej.Web.CheckState.Checked;
					}
				}
			}
			boolRefreshing = false;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveFormat();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveFormat())
			{
				Close();
			}
		}

		private void optSSN_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentFormat)
				{
					if (cmbSSN.Text == "SSN")
					{
						theView.CurrentImportFormat.EmployeeIdentifierType = modGlobalVariables.ACAEmployeeIdentifierType.SSN;
					}
                    else if (cmbSSN.Text == "Employee Number")
                    {
                        theView.CurrentImportFormat.EmployeeIdentifierType = modGlobalVariables.ACAEmployeeIdentifierType.EmployeeNumber;
                    }
				}
			}
		}

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				if (theView.IsCurrentFormat)
				{
					theView.CurrentImportFormat.Description = txtDescription.Text;
				}
			}
		}

		private void RefreshPreview()
		{
			// If Trim(lblFileName.Text) = "" Then
			// MsgBox "You must select a file before previewing", vbExclamation, "No File Selected"
			// Exit Sub
			// End If
			// Dim fso As New FCFCFileSystem
			// If Not FCFileSystem.FileExists(lblFileName.Text) Then
			// MsgBox "The selected file could not be found", vbExclamation, "File Not Found"
			// Exit Sub
			// End If
			GridFormat.Rows = 1;
			int lngRow;
			lngRow = 0;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			if (theView.IsCurrentFormat)
			{
				cACAEmployeeDependentImported previewRecord;
				theView.DependentFilePreview.MoveFirst();
				while (theView.DependentFilePreview.IsCurrent())
				{
					// For Each previewRecord In theView.DependentFilePreview
					previewRecord = (cACAEmployeeDependentImported)theView.DependentFilePreview.GetCurrentItem();
					GridFormat.Rows += 1;
					lngRow = GridFormat.Rows - 1;
					for (intCol = 0; intCol <= (GridFormat.Cols - 1); intCol++)
					{
						if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "emp ssn")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.EmployeeIdentifier);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "first")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.FirstName);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "middle")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.MiddleName);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "last")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.LastName);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "sfx")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.Suffix);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "ssn")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.SSN);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "dob")
						{
							GridFormat.TextMatrix(lngRow, intCol, previewRecord.DateOfBirth);
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "all 12")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.CoveredAll12Months));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "jan")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(1)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "feb")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(2)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "mar")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(3)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "apr")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(4)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "may")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(5)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "jun")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(6)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "jul")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(7)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "aug")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(8)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "sep")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(9)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "oct")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(10)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "nov")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(11)));
						}
						else if (fecherFoundation.Strings.LCase(GridFormat.TextMatrix(0, intCol)) == "dec")
						{
							GridFormat.TextMatrix(lngRow, intCol, FCConvert.ToString(previewRecord.GetIsCoveredForMonth(12)));
						}
					}
					theView.DependentFilePreview.MoveNext();
				}
			}
		}
	}
}
