﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	public class cACAElectronicFilingView
	{
		//=========================================================
		private int intReportYear;
		private string strTCCCode = string.Empty;
		private string strSavePath = string.Empty;
		private cSettingsController setCont = new cSettingsController();
		private cACAService acaServ = new cACAService();
		// Private contBXML As New c1095BXMLController
		public delegate void DestinationPathChangedEventHandler();

		public event DestinationPathChangedEventHandler DestinationPathChanged;

		public delegate void ViewUpdatedEventHandler();

		public event ViewUpdatedEventHandler ViewUpdated;

		public delegate void FileSavedEventHandler(string strFileName);

		public event FileSavedEventHandler FileSaved;

		private bool boolIsTRIOWorkstation;
		private int lngLastError;
		private string strLastError = "";

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public string LastErrorDescription
		{
			get
			{
				string LastErrorDescription = "";
				LastErrorDescription = strLastError;
				return LastErrorDescription;
			}
		}

		public void ClearErrors()
		{
			lngLastError = 0;
			strLastError = "";
		}

		private void SetError(int lngError, string strErrorMessage)
		{
			lngLastError = lngError;
			strLastError = strErrorMessage;
		}

		public string SaveFilePath
		{
			set
			{
				strSavePath = value;
			}
			get
			{
				string SaveFilePath = "";
				SaveFilePath = strSavePath;
				return SaveFilePath;
			}
		}

		public string TCCCode
		{
			set
			{
				strTCCCode = value;
			}
			get
			{
				string TCCCode = "";
				TCCCode = strTCCCode;
				return TCCCode;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public void Browse()
		{
			cBrowseForFolder bff = new cBrowseForFolder();
			if (strSavePath != "")
			{
				bff.Directory = strSavePath;
			}
			bff.Title = "Select Path";
			bff.ShowDialog();
			if (bff.Directory != "")
			{
				strSavePath = bff.Directory;
				if (this.DestinationPathChanged != null)
					this.DestinationPathChanged();
			}
		}

		public void Save()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				setCont.SaveSetting(strSavePath, "LastACASaveDirectory", "Payroll", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "");
				setCont.SaveSetting(strTCCCode, "ACATCCCode", "Payroll", "", "", "");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void Load(int intYear)
		{
			intReportYear = intYear;
			strSavePath = setCont.GetSettingValue("LastACASaveDirectory", "Payroll", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "");
			strTCCCode = setCont.GetSettingValue("ACATCCCode", "Payroll", "", "", "");
			
			if (StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
			{
				boolIsTRIOWorkstation = true;
			}
			if (this.ViewUpdated != null)
				this.ViewUpdated();
		}

		private string GetTimeStamp(DateTime dtDate)
		{
			string GetTimeStamp = "";
			string strTimeStamp;
			cDateTimeUtility dtu = new cDateTimeUtility();
			DateTime TempDate;
			TempDate = dtu.LocalTimeToUTC(dtDate);
			strTimeStamp = Strings.Format(TempDate, "hh:mm:ss");
			strTimeStamp = strTimeStamp.Replace(":", "");
			strTimeStamp += "000";
			GetTimeStamp = strTimeStamp;
			return GetTimeStamp;
		}

		private string GetFileName(bool boolB, DateTime dtTimeStamp)
		{
			string GetFileName = "";
			string strReturn;
			string strTimeStamp;
			// Dim dtu As New cDateTimeUtility
			strReturn = "1094";
			if (boolB)
			{
				strReturn += "B";
			}
			else
			{
				strReturn += "C";
			}
			strReturn += "_Request_" + strTCCCode;
			strReturn += "_" + FCConvert.ToString(DateTime.Now.Year) + Strings.Right("00" + FCConvert.ToString(DateTime.Now.Month), 2) + Strings.Right("00" + FCConvert.ToString(DateTime.Now.Day), 2);
			strReturn += "T";
			strTimeStamp = GetTimeStamp(dtTimeStamp);
			strReturn += strTimeStamp;
			strReturn += "Z.xml";
			GetFileName = strReturn;
			return GetFileName;
		}

		public void Save1095Bs()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACABXMLReport reportB;
				reportB = acaServ.LoadACABXMLReport(ReportYear);
				DateTime dtTimeStamp;
				string strTemp = "";
				cCreateGUID guidCreator = new cCreateGUID();
				string strPath = "";
				if (strTCCCode == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "No TCC code specified", null, null);
				}
				if (!(reportB == null))
				{
					reportB.TimeStamp = DateTime.Now;
					dtTimeStamp = reportB.TimeStamp;
					reportB.FileName = GetFileName(true, dtTimeStamp);
					reportB.Manifest.RequestFileName = reportB.FileName;
					reportB.ManifestName = "manifest_" + reportB.FileName;
					// reportB.Manifest.FileTimeStamp = Year(dtTimeStamp) & "-" & Right("00" & Month(dtTimeStamp), 2) & "-" & Right("00" & Day(dtTimeStamp), 2) & "T" & Format(dtTimeStamp, "HH:MM:SS") & "Z"
					reportB.Manifest.FileTimeStamp = FCConvert.ToString(reportB.TimeStamp.Year) + "-" + Strings.Right("00" + FCConvert.ToString(reportB.TimeStamp.Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(reportB.TimeStamp.Day), 2) + "T" + Strings.Format(reportB.TimeStamp, "hh:mm:ss") + "Z";
                    //strPath = SaveFilePath;
                    strPath = FCFileSystem.Statics.UserDataFolder;
					if (strPath != "")
					{
						if (Strings.Right(" " + strPath, 1) != "\\" && Strings.Right(" " + strPath, 1) != "/")
						{
							strPath += "\\";
						}
					}                    
					strTemp = guidCreator.CreateGUID(true);
					strTemp += ":SYS12:" + strTCCCode + "::T";
					reportB.Manifest.TransmissionID = strTemp;
					acaServ.SaveACABXMLReport(ref reportB, strPath);
					if (!(acaServ.LastErrorNumber > 0))
					{
                        ZipAndSendFile(ref reportB, strPath);
                        if (this.FileSaved != null)
							this.FileSaved(reportB.FileName);
					}
					else
					{
						SetError(acaServ.LastErrorNumber, acaServ.LastErrorDescription);
					}
					reportB = null;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void Save1095Cs()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cACACXMLReport reportC;
				reportC = acaServ.LoadACACXMLReport(ReportYear);
				DateTime dtTimeStamp;
				string strTemp = "";
				cCreateGUID guidCreator = new cCreateGUID();
				string strPath = "";
				if (strTCCCode == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "No TCC code specified", null, null);
				}
				if (!(reportC == null))
				{
					reportC.TimeStamp = DateTime.Now;
					dtTimeStamp = reportC.TimeStamp;
					reportC.FileName = GetFileName(false, dtTimeStamp);
					reportC.Manifest.RequestFileName = reportC.FileName;
					reportC.ManifestName = "manifest_" + reportC.FileName;
					reportC.Manifest.FileTimeStamp = FCConvert.ToString(reportC.TimeStamp.Year) + "-" + Strings.Right("00" + FCConvert.ToString(reportC.TimeStamp.Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(reportC.TimeStamp.Day), 2) + "T" + Strings.Format(reportC.TimeStamp, "hh:mm:ss") + "Z";
                    strPath = FCFileSystem.Statics.UserDataFolder;
                    if (strPath != "")
					{
						if (Strings.Right(" " + strPath, 1) != "\\" && Strings.Right(" " + strPath, 1) != "/")
						{
							strPath += "\\";
						}
					}
					strTemp = guidCreator.CreateGUID(true);
					strTemp += ":SYS12:" + strTCCCode + "::T";
					reportC.Manifest.TransmissionID = strTemp;
					acaServ.SaveACACXMLReport(ref reportC, strPath);
					if (!(acaServ.LastErrorNumber > 0))
					{
                        ZipAndSendFile(ref reportC, strPath);
						if (this.FileSaved != null)
							this.FileSaved(reportC.FileName);
					}
					else
					{
						SetError(acaServ.LastErrorNumber, acaServ.LastErrorDescription);
					}
					reportC = null;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

        private void ZipAndSendFile(ref cACACXMLReport theReport, string strPath)
        {
            string zipFilePath = strPath + System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetTempFileName()) + ".zip";
            if (System.IO.File.Exists(zipFilePath))
            {
                System.IO.File.Delete(zipFilePath);
            }
            var zip = ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
            zip.CreateEntryFromFile(strPath + theReport.FileName, theReport.FileName, System.IO.Compression.CompressionLevel.Optimal);
            zip.CreateEntryFromFile(strPath + theReport.ManifestName, theReport.ManifestName, System.IO.Compression.CompressionLevel.Optimal);
            zip.Dispose();
            FCUtils.Download(zipFilePath, System.IO.Path.GetFileNameWithoutExtension(theReport.FileName) + ".zip");
        }

        private void ZipAndSendFile(ref cACABXMLReport theReport, string strPath)
        {
            string zipFilePath = strPath + System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetTempFileName()) + ".zip";
            if (System.IO.File.Exists(zipFilePath))
            {
                System.IO.File.Delete(zipFilePath);
            }
            var zip = ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
            zip.CreateEntryFromFile(strPath + theReport.FileName, theReport.FileName, System.IO.Compression.CompressionLevel.Optimal);
            zip.CreateEntryFromFile(strPath + theReport.ManifestName, theReport.ManifestName, System.IO.Compression.CompressionLevel.Optimal);
            zip.Dispose();
            FCUtils.Download(zipFilePath, System.IO.Path.GetFileNameWithoutExtension(theReport.FileName) + ".zip");
        }

        // vbPorter upgrade warning: strScenario As string	OnWrite(int, string)
        public void SaveScenario(string strScenario)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
                cACABXMLReport reportB = null;//= new cACABXMLReport();
                cACACXMLReport reportC = null;// = new cACACXMLReport();
				FCFileSystem fso = new FCFileSystem();
				// Dim reportC As cacacxmlreport
				if (strTCCCode == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "No TCC code specified", null, null);
				}
				if (strScenario == "1")
				{
					reportB = acaServ.GetTestScenario1(ReportYear);
				}
				else if (strScenario == "2")
				{
					reportB = acaServ.GetTestScenario2(ReportYear);
				}
				else if (strScenario == "2C")
				{
					reportB = acaServ.GetTestScenario2C(ReportYear);
				}
				else if (strScenario == "3")
				{
					reportC = acaServ.GetTestScenario3(ReportYear);
				}
				else if (strScenario == "4")
				{
					reportC = acaServ.GetTestScenario4(ReportYear);
				}
				else if (strScenario == "4C")
				{
					reportC = acaServ.GetTestScenario4C(ReportYear);
				}
				else if (strScenario == "5")
				{
					reportC = acaServ.GetTestScenario5(ReportYear);
				}
				else if (strScenario == "5C")
				{
					reportC = acaServ.GetTestScenario5C(ReportYear);
				}
				else if (strScenario == "6")
				{
					reportB = acaServ.GetTestScenario6(ReportYear);
				}
				else if (strScenario == "6C")
				{
					reportB = acaServ.GetTestScenario6C(ReportYear);
				}
				else if (strScenario == "7")
				{
					reportC = acaServ.GetTestScenario7(ReportYear);
				}
				else if (strScenario == "7C")
				{
					reportC = acaServ.GetTestScenario7C(ReportYear);
				}
				else if (strScenario == "8")
				{
					reportB = acaServ.GetTestScenario8(ReportYear);
				}
				else if (strScenario == "8C")
				{
					reportB = acaServ.GetTestScenario8C(ReportYear);
				}
				DateTime dtTimeStamp;
				string strTemp = "";
				cCreateGUID guidCreator = new cCreateGUID();
				string strPath = "";
				if (!(reportB == null))
				{
					reportB.Manifest.TestFileCode = "T";
					reportB.TimeStamp = DateTime.Now;
					dtTimeStamp = reportB.TimeStamp;
					reportB.FileName = GetFileName(true, dtTimeStamp);
					reportB.Manifest.RequestFileName = reportB.FileName;
					reportB.ManifestName = "manifest_" + reportB.FileName;
					// reportB.Manifest.FileTimeStamp = Year(dtTimeStamp) & "-" & Right("00" & Month(dtTimeStamp), 2) & "-" & Right("00" & Day(dtTimeStamp), 2) & "T" & Format(dtTimeStamp, "HH:MM:SS") & "Z"
					reportB.Manifest.FileTimeStamp = FCConvert.ToString(reportB.TimeStamp.Year) + "-" + Strings.Right("00" + FCConvert.ToString(reportB.TimeStamp.Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(reportB.TimeStamp.Day), 2) + "T" + Strings.Format(reportB.TimeStamp, "hh:mm:ss") + "Z";
                    strPath = FCFileSystem.Statics.UserDataFolder;
                    if (strPath != "")
					{
						if (Strings.Right(" " + strPath, 1) != "\\" && Strings.Right(" " + strPath, 1) != "/")
						{
							strPath += "\\";
						}
					}
					strTemp = guidCreator.CreateGUID(true);
					strTemp += ":SYS12:" + strTCCCode + "::T";
					reportB.Manifest.TransmissionID = strTemp;
					acaServ.SaveACABXMLReport(ref reportB, strPath);
					if (!(acaServ.LastErrorNumber > 0))
					{
                        ZipAndSendFile(ref reportB, strPath);
                        if (this.FileSaved != null)
							this.FileSaved(reportB.FileName);
					}
					else
					{
						SetError(acaServ.LastErrorNumber, acaServ.LastErrorDescription);
					}
					reportB = null;
				}
				if (!(reportC == null))
				{
					reportC.Manifest.TestFileCode = "T";
					reportC.TimeStamp = DateTime.Now;
					dtTimeStamp = reportC.TimeStamp;
					reportC.FileName = GetFileName(false, dtTimeStamp);
					reportC.Manifest.RequestFileName = reportC.FileName;
					reportC.ManifestName = "manifest_" + reportC.FileName;
					reportC.Manifest.FileTimeStamp = FCConvert.ToString(reportC.TimeStamp.Year) + "-" + Strings.Right("00" + FCConvert.ToString(reportC.TimeStamp.Month), 2) + "-" + Strings.Right("00" + FCConvert.ToString(reportC.TimeStamp.Day), 2) + "T" + Strings.Format(reportC.TimeStamp, "hh:mm:ss") + "Z";
					strPath = SaveFilePath;
					if (strPath != "")
					{
						if (Strings.Right(" " + strPath, 1) != "\\" && Strings.Right(" " + strPath, 1) != "/")
						{
							strPath += "\\";
						}
					}
					strTemp = guidCreator.CreateGUID(true);
					strTemp += ":SYS12:" + strTCCCode + "::T";
					reportC.Manifest.TransmissionID = strTemp;
					acaServ.SaveACACXMLReport(ref reportC, strPath);
					if (!(acaServ.LastErrorNumber > 0))
					{
                        ZipAndSendFile(ref reportC, strPath);
						if (this.FileSaved != null)
							this.FileSaved(reportC.FileName);
					}
					else
					{
						SetError(acaServ.LastErrorNumber, acaServ.LastErrorDescription);
					}
					reportC = null;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public bool AllowScenario6Correction
		{
			get
			{
				bool AllowScenario6Correction = false;
				AllowScenario6Correction = boolIsTRIOWorkstation;
				return AllowScenario6Correction;
			}
		}

		public bool AllowScenario2Correction
		{
			get
			{
				bool AllowScenario2Correction = false;
				AllowScenario2Correction = boolIsTRIOWorkstation;
				return AllowScenario2Correction;
			}
		}

		public bool AllowScenario8Correction
		{
			get
			{
				bool AllowScenario8Correction = false;
				AllowScenario8Correction = boolIsTRIOWorkstation;
				return AllowScenario8Correction;
			}
		}

		public bool AllowScenario4Correction
		{
			get
			{
				bool AllowScenario4Correction = false;
				AllowScenario4Correction = boolIsTRIOWorkstation;
				return AllowScenario4Correction;
			}
		}

		public bool AllowScenario5Correction
		{
			get
			{
				bool AllowScenario5Correction = false;
				AllowScenario5Correction = boolIsTRIOWorkstation;
				return AllowScenario5Correction;
			}
		}

		public bool AllowScenario7Correction
		{
			get
			{
				bool AllowScenario7Correction = false;
				AllowScenario7Correction = boolIsTRIOWorkstation;
				return AllowScenario7Correction;
			}
		}
	}
}
