﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class cACADataImportFormat
	{
		//=========================================================
		private bool boolFirstLineUnreadable;
		private string strSeparatorChar = string.Empty;
		private string strColumnSetup = "";
		private int lngID;
		private FCCollection Columns = new FCCollection();
		private string strFormatDescription = string.Empty;
		private bool boolChanged;
		// vbPorter upgrade warning: intIdentifierType As int	OnWrite(ACAEmployeeIdentifierType)
		private modGlobalVariables.ACAEmployeeIdentifierType intIdentifierType;

		public modGlobalVariables.ACAEmployeeIdentifierType EmployeeIdentifierType
		{
			set
			{
				intIdentifierType = value;
				boolChanged = true;
			}
			get
			{
				modGlobalVariables.ACAEmployeeIdentifierType EmployeeIdentifierType = 0;
				EmployeeIdentifierType = intIdentifierType;
				return EmployeeIdentifierType;
			}
		}

		public bool IsChanged
		{
			set
			{
				boolChanged = value;
			}
			get
			{
				bool IsChanged = false;
				IsChanged = boolChanged;
				return IsChanged;
			}
		}

		public string Description
		{
			set
			{
				strFormatDescription = value;
				boolChanged = true;
			}
			get
			{
				string Description = "";
				Description = strFormatDescription;
				return Description;
			}
		}

		public FCCollection Fields
		{
			get
			{
				return Columns;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string ColumnSetup
		{
			get
			{
				string ColumnSetup = "";
				ColumnSetup = strColumnSetup;
				return ColumnSetup;
			}
		}

		public string SeparatorCharacter
		{
			set
			{
				strSeparatorChar = value;
				boolChanged = true;
			}
			get
			{
				string SeparatorCharacter = "";
				SeparatorCharacter = strSeparatorChar;
				return SeparatorCharacter;
			}
		}

		public bool FirstLineUnreadable
		{
			set
			{
				boolFirstLineUnreadable = value;
				boolChanged = true;
			}
			get
			{
				bool FirstLineUnreadable = false;
				FirstLineUnreadable = boolFirstLineUnreadable;
				return FirstLineUnreadable;
			}
		}

		public void ClearColumns()
		{
			int x;
			for (x = Columns.Count; x >= 1; x--)
			{
				Columns.Remove(x);
			}
			// x
			boolChanged = true;
		}

		public void AddColumn(cACAImportFormatColumn Col)
		{
			string cName = "";
			bool boolFound;
			boolFound = false;
			foreach (cACAImportFormatColumn tCol in Columns)
			{
				if (tCol.ColumnName == Col.ColumnName)
				{
					tCol.Width = Col.Width;
					boolFound = true;
				}
			}
			if (!boolFound)
			{
				Columns.Add(Col);
				boolChanged = true;
			}
		}

		public void AddColumnName(string strColumnName)
		{
			cACAImportFormatColumn Col = new cACAImportFormatColumn();
			Col.ColumnName = strColumnName;
			AddColumn(Col);
		}

		public void RemoveColumn(string strColumnName)
		{
			int x;
			// vbPorter upgrade warning: Col As cACAImportFormatColumn	OnWrite(SSDataWidgets_B_OLEDB.Columns)
			cACAImportFormatColumn Col;
			for (x = 1; x <= Columns.Count; x++)
			{
				Col = Columns[x];
				if (fecherFoundation.Strings.LCase(Col.ColumnName) == fecherFoundation.Strings.LCase(strColumnName))
				{
					Columns.Remove(x);
					boolChanged = true;
					break;
				}
			}
			// x
		}
	}
}
