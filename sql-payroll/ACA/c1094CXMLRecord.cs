﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;

namespace TWPY0000
{
	public class c1094CXMLRecord
	{
		//=========================================================
		private c1094C baseRecord;
		private string strScenario = string.Empty;
		private string strUniqueID = string.Empty;
		private cGenericCollection listDetails = new cGenericCollection();
		private int intSubmissionID;
		private string strCorrectedUniqueID = string.Empty;

		public string CorrectedUniqueID
		{
			set
			{
				strCorrectedUniqueID = value;
			}
			get
			{
				string CorrectedUniqueID = "";
				CorrectedUniqueID = strCorrectedUniqueID;
				return CorrectedUniqueID;
			}
		}

		public int SubmissionID
		{
			set
			{
				intSubmissionID = value;
			}
			get
			{
				int SubmissionID = 0;
				SubmissionID = intSubmissionID;
				return SubmissionID;
			}
		}

		public cGenericCollection Details
		{
			set
			{
				listDetails = value;
			}
			get
			{
				cGenericCollection Details = null;
				Details = listDetails;
				return Details;
			}
		}

		public string ScenarioID
		{
			set
			{
				strScenario = value;
			}
			get
			{
				string ScenarioID = "";
				ScenarioID = strScenario;
				return ScenarioID;
			}
		}

		public c1094C BaseInfo
		{
			set
			{
				baseRecord = value;
			}
			get
			{
				c1094C BaseInfo = null;
				BaseInfo = baseRecord;
				return BaseInfo;
			}
		}

		public string UNIQUEID
		{
			set
			{
				strUniqueID = value;
			}
			get
			{
				string UNIQUEID = "";
				UNIQUEID = strUniqueID;
				return UNIQUEID;
			}
		}
	}
}
