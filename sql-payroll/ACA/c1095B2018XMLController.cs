//Fecher vbPorter - Version 1.0.0.93
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class c1095B2018XMLController : c1095BXMLController
	{

		//=========================================================



		//private c1095BXMLManifestController manController;

		//public void SetManifestController(ref c1095BXMLManifestController manifestCont)
		//{
		//	manController = manifestCont;
		//}

		//private string StripBadChars(string strOrig)
		//{
		//	string StripBadChars = "";
		//	string strReturn;
		//	strReturn = strOrig.Replace("'", "");
		//	strReturn = strReturn.Replace("&", "");
		//	strReturn = strReturn.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
		//	strReturn = strReturn.Replace("<", "");
		//	StripBadChars = strReturn;
		//	return StripBadChars;
		//}

		//public  string ACABXMLReportToManifestXML(ref cACABXMLReport theReport)
		//{
		//	string ACABXMLReportToManifestXML = "";
		//	ACABXMLReportToManifestXML = manController.ACABXMLReportToManifestXML(ref theReport);
		//	return ACABXMLReportToManifestXML;
		//}

		//private string GetSimpleTag(string strTag, string strInnerXML)
		//{
		//	string GetSimpleTag = "";
		//	GetSimpleTag = "<"+strTag+">"+strInnerXML+"</"+strTag+">";
		//	return GetSimpleTag;
		//}

		public override string ACABXMLReportToXML(ref cACABXMLReport theReport)
		{
			string ACABXMLReportToXML = "";
			string strXML = "";
			string strTemp = "";
			if (!(theReport==null)) {
				theReport.List1094s.MoveFirst();
				strXML = "<?xml version="+FCConvert.ToString(Convert.ToChar(34))+"1.0"+FCConvert.ToString(Convert.ToChar(34))+" encoding="+FCConvert.ToString(Convert.ToChar(34))+"utf-8"+FCConvert.ToString(Convert.ToChar(34))+"?>";
				strXML += "<form109495B:Form109495BTransmittalUpstream xmlns:irs="+FCConvert.ToString(Convert.ToChar(34))+"urn:us:gov:treasury:irs:common"+FCConvert.ToString(Convert.ToChar(34));
				strXML += " xmlns:form109495B="+FCConvert.ToString(Convert.ToChar(34))+"urn:us:gov:treasury:irs:msg:form1094-1095Btransmitterupstreammessage"+FCConvert.ToString(Convert.ToChar(34))+">";
				c1094BXMLRecord header;
				while (theReport.List1094s.IsCurrent()) {
					//Application.DoEvents();
					header = (c1094BXMLRecord)theReport.List1094s.GetCurrentItem();
					strTemp = c1094XMLRecordToXML(ref header);
					strXML += strTemp;
					theReport.List1094s.MoveNext();
				}
				strXML += "</form109495B:Form109495BTransmittalUpstream>";
			}
			ACABXMLReportToXML = strXML;
			return ACABXMLReportToXML;
		}

		private string c1094XMLRecordToXML(ref c1094BXMLRecord acaRec)
		{
			string c1094XMLRecordToXML = "";
			string strXML = "";
			string strTemp = "";

			if (!(acaRec==null)) {
				strXML = "<Form1094BUpstreamDetail xmlns="+FCConvert.ToString(Convert.ToChar(34))+"urn:us:gov:treasury:irs:ext:aca:air:ty18"+FCConvert.ToString(Convert.ToChar(34))+" recordType = "+FCConvert.ToString(Convert.ToChar(34))+FCConvert.ToString(Convert.ToChar(34))+" lineNum="+FCConvert.ToString(Convert.ToChar(34))+"0"+FCConvert.ToString(Convert.ToChar(34))+">";
				strXML += GetSimpleTag("SubmissionId", "1");
				if (acaRec.UNIQUEID!="") {
					strXML += GetSimpleTag("OriginalUniqueSubmissionId", acaRec.UNIQUEID);
				}
				if (acaRec.ScenarioID!="") {
					strXML += GetSimpleTag("TestScenarioId", acaRec.ScenarioID);
				}
				strXML += GetSimpleTag("TaxYr", acaRec.BaseInfo.YearCovered.ToString());
				strTemp = acaRec.BaseInfo.FilerName;
				strTemp = strTemp.Replace("'", "");
				strXML += "<BusinessName>";
				strXML += "<BusinessNameLine1Txt>";
				strXML += strTemp;
				strXML += "</BusinessNameLine1Txt>";
				strXML += "</BusinessName>";
				strXML += GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN");
				strXML += "<irs:EmployerEIN>";
				strTemp = acaRec.BaseInfo.EIN;
				strTemp = strTemp.Replace(" ", "");
				strTemp = strTemp.Replace("-", "");
				strXML += strTemp;
				strXML += "</irs:EmployerEIN>";
				strXML += ContactNameGroupToXML(acaRec.BaseInfo.ContactName, acaRec.BaseInfo.ContactMiddleName, acaRec.BaseInfo.ContactLastName, acaRec.BaseInfo.ContactSuffix);

				if (acaRec.BaseInfo.ContactPhone!="") {
					strXML += "<ContactPhoneNum>";
					strTemp = acaRec.BaseInfo.ContactPhone;
					strTemp = fecherFoundation.Strings.Trim(strTemp);
					strTemp = strTemp.Replace("(", "");
					strTemp = strTemp.Replace(")", "");
					strTemp = strTemp.Replace("-", "");
					if (strTemp.Length==7) {
						strTemp = "207"+strTemp;
					}
					strXML += strTemp;
					strXML += "</ContactPhoneNum>";
				}
				strXML += FCConvert.ToString(GetMailingAddressGroupXML(acaRec.BaseInfo.Address, acaRec.BaseInfo.City, acaRec.BaseInfo.State, acaRec.BaseInfo.PostalCode, ""));
				strXML += "<Form1095BAttachedCnt>";
				strXML += acaRec.Details.ItemCount();
				strXML += "</Form1095BAttachedCnt>";
				acaRec.Details.MoveFirst();
				while (acaRec.Details.IsCurrent()) {
					//Application.DoEvents();
					strXML += c1095BXMLRecordToXML((c1095BXMLRecord)acaRec.Details.GetCurrentItem(), acaRec);
					acaRec.Details.MoveNext();
				}
				strXML += "</Form1094BUpstreamDetail>";
			}
			c1094XMLRecordToXML = strXML;
			return c1094XMLRecordToXML;
		}

		private string ContactNameGroupToXML(string strFirstName, string strMiddleName, string strLastName, string strSuffix)
		{
			string ContactNameGroupToXML = "";
			string strXML = "";
			if (strFirstName!="" && strLastName!="") {
				string strTemp = "";
				strXML = "<ContactNameGrp>";
				strXML += GetSimpleTag("PersonFirstNm", StripBadChars(strFirstName));
				if (strMiddleName!="") {
					strXML += GetSimpleTag("PersonMiddleNm", StripBadChars(strMiddleName));
				}
				strXML += GetSimpleTag("PersonLastNm", StripBadChars(strLastName));
				if (strSuffix!="") {
					strXML += "<SuffixNm>";
					strTemp = strSuffix.Replace(".", "");
					strXML += strTemp;
					strXML += "</SuffixNm>";
				}
				strXML += "</ContactNameGrp>";
			}
			ContactNameGroupToXML = strXML;
			return ContactNameGroupToXML;
		}

		private string c1095BXMLRecordToXML( c1095BXMLRecord acaEmp,  c1094BXMLRecord acaRec)
		{
			string c1095BXMLRecordToXML = "";
			string strXML = "";
			string strTemp = "";
			short intindex;
			string strMonth = "";
			cEmployee emp;

			if (!(acaEmp==null)) {
				emp = acaEmp.Employee;
				strXML = "<Form1095BUpstreamDetail recordType = "+FCConvert.ToString(Convert.ToChar(34))+FCConvert.ToString(Convert.ToChar(34))+" lineNum = "+FCConvert.ToString(Convert.ToChar(34))+"0"+FCConvert.ToString(Convert.ToChar(34))+">";
				strXML += "<RecordId>";
				strXML += acaEmp.RecordID;
				strXML += "</RecordId>";
				if (acaEmp.ScenarioID!="") {
					strXML += GetSimpleTag("TestScenarioId", acaEmp.ScenarioID);
				}
				if (acaEmp.BaseInfo.Corrected) {
					strTemp = "1";
				} else {
					strTemp = "0";
				}
				strXML += GetSimpleTag("CorrectedInd", strTemp);
				if (acaEmp.BaseInfo.Corrected) {
					strXML += "<CorrectedRecordRecipientGrp>";
					if (acaEmp.CorrectedUniqueID!="") {
						strXML += GetSimpleTag("CorrectedUniqueRecordId", acaEmp.CorrectedUniqueID);
					}
					strXML += GetPersonNameXML("CorrectedRecRecipientPrsnName", emp.FirstName, emp.MiddleName, emp.LastName, emp.Designation);
					if (emp.SSN!="") {
						strTemp = emp.SSN;
						strTemp = strTemp.Replace("-", "");
						strTemp = strTemp.Replace(" ", "");
						strXML += GetSimpleTag("CorrectedRecRecipientTIN", strTemp);
					}
					strXML += "</CorrectedRecordRecipientGrp>";
				}
				strXML += GetSimpleTag("TaxYr", acaEmp.BaseInfo.ReportYear.ToString());
				strXML += "<ResponsibleIndividualGrp>";
				strXML += GetPersonNameXML("ResponsibleIndividualName", emp.FirstName, emp.MiddleName, emp.LastName, emp.Designation);
				strXML += GetSimpleTag("irs:TINRequestTypeCd", "INDIVIDUAL_TIN");
				if (emp.SSN!="") {
					strTemp = emp.SSN;
					strTemp = strTemp.Replace("-", "");
					strTemp = strTemp.Replace(" ", "");
					strXML += GetSimpleTag("irs:TIN", strTemp);
				} else {
					if (fecherFoundation.Information.IsDate(emp.BirthDate)) {
						strXML += GetSimpleTag("BirthDt", FCConvert.ToString(DateTime.Parse(emp.BirthDate).Year)+"-"+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(emp.BirthDate).Month), 2)+"-"+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(emp.BirthDate).Day), 2));
					}
				}
				strXML += FCConvert.ToString(GetMailingAddressGroupXML(emp.Address1, emp.City, emp.State, emp.Zip, emp.Zip4));
				strXML += GetSimpleTag("HealthCoverageOriginCd", fecherFoundation.Strings.UCase(acaEmp.BaseInfo.OriginOfPolicy));
				strXML += "</ResponsibleIndividualGrp>";
				if (acaEmp.BaseInfo.EmployerSponsoredCoverage) {
					strXML += GetEmployerSponsoredXML(ref acaRec);
				}
				if (!(acaEmp.BaseInfo.CoverageProvider==null)) {
					strXML += GetProviderXML(acaEmp.BaseInfo.CoverageProvider);
				}
				if (!(acaEmp.BaseInfo.coveredIndividuals==null)) {
					if (acaEmp.BaseInfo.coveredIndividuals.Count>0) {
						strXML += GetCoveredIndividualsXML(acaEmp.BaseInfo.coveredIndividuals);
					}
				}
				strXML += "</Form1095BUpstreamDetail>";
			}
			c1095BXMLRecordToXML = strXML;
			return c1095BXMLRecordToXML;
		}

		private string GetEmployerSponsoredXML(ref c1094BXMLRecord acaRec)
		{
			string GetEmployerSponsoredXML = "";
			cEmployerRecord employerRecord;
			string strXML = "";
			string strTemp = "";

			if (!(acaRec.Employer==null)) {
				strXML = "<SponsoringEmployerInfoGrp>";
				strXML += "<BusinessName>";
				strXML += GetSimpleTag("BusinessNameLine1Txt", acaRec.Employer.Name);
				strXML += "</BusinessName>";
				strXML += "<irs:EIN>";
				strTemp = acaRec.Employer.EIN;
				strTemp = strTemp.Replace(" ", "");
				strTemp = strTemp.Replace("-", "");
				strXML += strTemp;
				strXML += "</irs:EIN>";
				strXML += GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN");
				strXML += FCConvert.ToString(GetMailingAddressGroupXML(acaRec.Employer.Address, acaRec.Employer.City, acaRec.Employer.State, acaRec.Employer.Zip, acaRec.Employer.Zip4));
				strXML += "</SponsoringEmployerInfoGrp>";
			}
			GetEmployerSponsoredXML = strXML;
			return GetEmployerSponsoredXML;
		}

		private string GetCoveredIndividualsXML( FCCollection origCoveredIndividuals)
		{
			string GetCoveredIndividualsXML = "";
			string strXML = "";
			if (!(origCoveredIndividuals==null)) {
				if (origCoveredIndividuals.Count>0) {
					FCCollection coveredIndividuals = new/*AsNew*/ FCCollection();

					string strTemp = "";
					short x;
					cACAEmployeeDependent dep;
					GetReOrderedIndividuals(ref origCoveredIndividuals, ref coveredIndividuals);
					for(x=1; x<=coveredIndividuals.Count; x++) {
						//Application.DoEvents();
						dep = coveredIndividuals[x];
						if (dep.FullName!="") {
							strXML += "<CoveredIndividualGrp>";
							strXML += GetPersonNameXML("CoveredIndividualName", dep.FirstName, dep.MiddleName, dep.LastName, dep.Suffix);
							strXML += GetSimpleTag("irs:TINRequestTypeCd", "INDIVIDUAL_TIN");
							if (dep.SSN!="") {
								strXML += "<irs:SSN>";
								strTemp = dep.SSN;
								strTemp = strTemp.Replace("-", "");
								strTemp = strTemp.Replace(" ", "");
								strXML += strTemp;
								strXML += "</irs:SSN>";
							} else {
								if (fecherFoundation.Information.IsDate(dep.DateOfBirth)) {
									strXML += GetSimpleTag("BirthDt", FCConvert.ToString(DateTime.Parse(dep.DateOfBirth).Year)+"-"+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(dep.DateOfBirth).Month), 2)+"-"+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(dep.DateOfBirth).Day), 2));
								}
							}
							strXML += FCConvert.ToString(GetCoveredIndMonthlyXML(ref dep));
							strXML += "</CoveredIndividualGrp>";
						}
					}
				}
			}
			GetCoveredIndividualsXML = strXML;
			return GetCoveredIndividualsXML;
		}

		private void GetReOrderedIndividuals(ref FCCollection orig, ref FCCollection newlist)
		{
			// vbPorter upgrade warning: x As short	OnWrite(int)
			short x;
			short Y;
			cACAEmployeeDependent dep;
			Dictionary<string, cACAEmployeeDependent> tempList = new Dictionary<string, cACAEmployeeDependent>();
			
			for(x=1; x<=orig.Count; x++) {
				dep = orig[x];
				tempList.Add(dep.FullName+" "+FCConvert.ToString(dep.ID), dep);
			}

			cACAEmployeeDependent[] arList = new cACAEmployeeDependent[tempList.Count];
			int i = 0;
			foreach (var item in tempList.Values)
			{
				arList[i] = item;
				i++;
			}
			bool boolAdded = false;
			cACAEmployeeDependent tempDep;
			for(x=0; x<=(short)(fecherFoundation.Information.UBound(arList, 1)); x++) {
				boolAdded = false;
				dep = arList[x];

				for (Y = 1; Y <= newlist.Count; Y++)
				{
					tempDep = newlist[Y];
					if (string.Compare(dep.FullName + " " + FCConvert.ToString(dep.ID), tempDep.FullName + " " + FCConvert.ToString(dep.ID)) <= 0)
					{
						newlist.Add(dep, null, Y);
						boolAdded = true;
						break;
					}
				}
				if (!boolAdded)
				{
					newlist.Add(dep);
				}
			}

		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetCoveredIndMonthlyXML(ref cACAEmployeeDependent dep)
		{
			object GetCoveredIndMonthlyXML = null;
			string strXML = "";
			short x;
			string strMonth = "";
			short intBool;
			if (!(dep==null)) {
				if (dep.CoveredAll12Months) {
					strXML = GetSimpleTag("CoveredIndividualAnnualInd", "1");
				} else {
					strXML = "<CoveredIndividualMonthlyIndGrp>";
					for(x=1; x<=12; x++) {
						strMonth = GetMonthName(x);
						if (dep.GetIsCoveredForMonth(x)) {
							strXML += GetSimpleTag(strMonth+"Ind", FCConvert.ToString(1));
						} else {
							strXML += GetSimpleTag(strMonth+"Ind", FCConvert.ToString(0));
						}
					}
					strXML += "</CoveredIndividualMonthlyIndGrp>";
				}
			}
			GetCoveredIndMonthlyXML = strXML;
			return GetCoveredIndMonthlyXML;
		}

		private string GetPersonNameXML(string strTag, string strFirstName, string strMiddleName, string strLastName, string strSuffix)
		{
			string GetPersonNameXML = "";
			string strXML = "";
			string strTemp = "";
			if (strFirstName!="" || strLastName!="") {
				if (strTag!="") {
					strXML = "<"+strTag+">";
				}
				if (strFirstName!="") {
					strXML += GetSimpleTag("PersonFirstNm", StripBadChars(strFirstName));
				}
				if (strMiddleName!="") {
					strXML += GetSimpleTag("PersonMiddleNm", StripBadChars(strMiddleName));
				}
				if (strLastName!="") {
					strXML += GetSimpleTag("PersonLastNm", StripBadChars(strLastName));
				}
				if (strSuffix!="") {
					strXML += GetSimpleTag("SuffixNm", StripBadChars(strSuffix));
				}
				if (strTag!="") {
					strXML += "</"+strTag+">";
				}
			}
			GetPersonNameXML = strXML;
			return GetPersonNameXML;
		}

		private string GetProviderXML( cCoverageProvider theProvider)
		{
			string GetProviderXML = "";
			string strXML = "";
			string strTemp = "";
			if (!(theProvider==null)) {
				if (fecherFoundation.Strings.Trim(theProvider.Name)!="") {
					strXML = "<IssuerInfoGrp>";
					strTemp = theProvider.Name.Replace("'", "");
					strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
					strTemp = strTemp.Replace("/", "");
					strTemp = strTemp.Replace("\\", "");
					strXML += "<BusinessName>";
					strXML += GetSimpleTag("BusinessNameLine1Txt", strTemp);
					strXML += "</BusinessName>";
					strXML += GetSimpleTag("irs:TINRequestTypeCd", "BUSINESS_TIN");
					strXML += "<irs:EIN>";
					strTemp = theProvider.EIN.Replace(" ", "");
					strTemp = strTemp.Replace("-", "");
					strXML += strTemp;
					strXML += "</irs:EIN>";
					if (theProvider.Telephone!="") {
						strTemp = theProvider.Telephone;
						strTemp = strTemp.Replace("(", "");
						strTemp = strTemp.Replace(")", "");
						strTemp = strTemp.Replace("-", "");
						strXML += "<ContactPhoneNum>";
						strXML += strTemp;
						strXML += "</ContactPhoneNum>";
					}
					strXML += FCConvert.ToString(GetMailingAddressGroupXML(theProvider.Address, theProvider.City, theProvider.State, theProvider.PostalCode, ""));
					strXML += "</IssuerInfoGrp>";
				}
			}
			GetProviderXML = strXML;
			return GetProviderXML;
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetMailingAddressGroupXML(string strAddress1, string strCity, string strState, string strZip, string strZip4)
		{
			object GetMailingAddressGroupXML = null;
			string strXML = "";
			string strTemp = "";
			string strZip5;
			string strZipExt = "";
			strZip = strZip.Replace("-", " ");
			strZip5 = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Left(strZip+"     ", 5));
			if (strZip4!="") {
				strZipExt = strZip4;
			} else {
				if (strZip.Length>6) {
					strZipExt = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strZip, 7));
				}
			}
			if (strAddress1!="" || strCity!="" || strState!="" || strZip!="") {
				strXML = "<MailingAddressGrp>";
				strXML += "<USAddressGrp>";
				if (strAddress1!="") {
					strXML += "<AddressLine1Txt>";
					strTemp = strAddress1;
					strTemp = strTemp.Replace("'", "");
					strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
					strTemp = strTemp.Replace("/", "");
					strTemp = strTemp.Replace("\\", "");
					strTemp = strTemp.Replace("#", " ");
					strXML += strTemp;
					strXML += "</AddressLine1Txt>";
				}
				if (strCity!="") {
					strXML += "<irs:CityNm>";
					strTemp = strCity.Replace("'", "");
					strTemp = strCity.Replace(FCConvert.ToString(Convert.ToChar(34)), "");
					strXML += strTemp;
					strXML += "</irs:CityNm>";
				}
				strXML += "<USStateCd>";
				strXML += strState;
				strXML += "</USStateCd>";
				strXML += "<irs:USZIPCd>";
				strXML += strZip5.Replace(" ", "");
				strXML += "</irs:USZIPCd>";
				if (strZipExt!="") {
					strXML += "<irs:USZIPExtensionCd>";
					strXML += strZipExt;
					strXML += "</irs:USZIPExtensionCd>";
				}
				strXML += "</USAddressGrp>";
				strXML += "</MailingAddressGrp>";
			}
			GetMailingAddressGroupXML = strXML;
			return GetMailingAddressGroupXML;
		}

		private string GetMonthAbbrev(short intMonth)
		{
			string GetMonthAbbrev = "";
			string strMonth = "";
			switch (intMonth) {
				
				case 1:
				{
					strMonth = "Jan";
					break;
				}
				case 2:
				{
					strMonth = "Feb";
					break;
				}
				case 3:
				{
					strMonth = "Mar";
					break;
				}
				case 4:
				{
					strMonth = "Apr";
					break;
				}
				case 5:
				{
					strMonth = "May";
					break;
				}
				case 6:
				{
					strMonth = "Jun";
					break;
				}
				case 7:
				{
					strMonth = "Jul";
					break;
				}
				case 8:
				{
					strMonth = "Aug";
					break;
				}
				case 9:
				{
					strMonth = "Sep";
					break;
				}
				case 10:
				{
					strMonth = "Oct";
					break;
				}
				case 11:
				{
					strMonth = "Nov";
					break;
				}
				case 12:
				{
					strMonth = "Dec";
					break;
				}
			} //end switch
			GetMonthAbbrev = strMonth;
			return GetMonthAbbrev;
		}
		private string GetMonthName(short intMonth)
		{
			string GetMonthName = "";
			string strMonth = "";
			switch (intMonth) {
				
				case 1:
				{
					strMonth = "January";
					break;
				}
				case 2:
				{
					strMonth = "February";
					break;
				}
				case 3:
				{
					strMonth = "March";
					break;
				}
				case 4:
				{
					strMonth = "April";
					break;
				}
				case 5:
				{
					strMonth = "May";
					break;
				}
				case 6:
				{
					strMonth = "June";
					break;
				}
				case 7:
				{
					strMonth = "July";
					break;
				}
				case 8:
				{
					strMonth = "August";
					break;
				}
				case 9:
				{
					strMonth = "September";
					break;
				}
				case 10:
				{
					strMonth = "October";
					break;
				}
				case 11:
				{
					strMonth = "November";
					break;
				}
				case 12:
				{
					strMonth = "December";
					break;
				}
			} //end switch
			GetMonthName = strMonth;
			return GetMonthName;
		}


	}
}
