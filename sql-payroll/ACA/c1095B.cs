﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class c1095B
	{
		//=========================================================
		private string strEmployeeName = string.Empty;
		private string strEmployeeFirstName = string.Empty;
		private string strEmployeeMiddleName = string.Empty;
		private string strEmployeeLastName = string.Empty;
		private string strSSN = string.Empty;
		private string strAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strPostalCode = string.Empty;
		private bool boolEmployerSponsoredCoverage;
		private bool boolVoid;
		private bool boolCorrected;
		private cACAEmployeeSetup EmployeeSetup;
		private cCoverageProvider planProvider;

		public bool EmployerSponsoredCoverage
		{
			set
			{
				boolEmployerSponsoredCoverage = value;
			}
			get
			{
				bool EmployerSponsoredCoverage = false;
				EmployerSponsoredCoverage = boolEmployerSponsoredCoverage;
				return EmployerSponsoredCoverage;
			}
		}

		public cCoverageProvider CoverageProvider
		{
			set
			{
				planProvider = value;
			}
			get
			{
				cCoverageProvider CoverageProvider = null;
				CoverageProvider = planProvider;
				return CoverageProvider;
			}
		}

		public string OriginOfPolicy
		{
			set
			{
				EmployeeSetup.OriginOfPolicy = value;
			}
			get
			{
				string OriginOfPolicy = "";
				OriginOfPolicy = EmployeeSetup.OriginOfPolicy;
				return OriginOfPolicy;
			}
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToInt32(
		public int ReportYear
		{
			get
			{
				int ReportYear = 0;
				ReportYear = EmployeeSetup.ReportYear;
				return ReportYear;
			}
		}

		public FCCollection coveredIndividuals
		{
			get
			{
				return EmployeeSetup.Dependents;
			}
		}

		public string EmployeeNumber
		{
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = EmployeeSetup.EmployeeNumber;
				return EmployeeNumber;
			}
		}

		public int EmployeeID
		{
			get
			{
				int EmployeeID = 0;
				EmployeeID = EmployeeSetup.EmployeeID;
				return EmployeeID;
			}
		}

		public int CoverageProviderID
		{
			get
			{
				int CoverageProviderID = 0;
				CoverageProviderID = EmployeeSetup.CoverageProviderID;
				return CoverageProviderID;
			}
		}

		public cACAEmployeeSetup ACAEmployee
		{
			set
			{
				EmployeeSetup = value;
			}
			get
			{
				cACAEmployeeSetup ACAEmployee = null;
				ACAEmployee = EmployeeSetup;
				return ACAEmployee;
			}
		}

		public string EmployeeName
		{
			set
			{
				strEmployeeName = value;
			}
			get
			{
				return strEmployeeName;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string Address
		{
			set
			{
				strAddress = value;
			}
			get
			{
				string Address = "";
				Address = strAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string PostalCode
		{
			set
			{
				strPostalCode = value;
			}
			get
			{
				string PostalCode = "";
				PostalCode = strPostalCode;
				return PostalCode;
			}
		}

		public bool Void
		{
			set
			{
				boolVoid = value;
			}
			get
			{
				bool Void = false;
				Void = boolVoid;
				return Void;
			}
		}

		public bool Corrected
		{
			set
			{
				boolCorrected = value;
			}
			get
			{
				bool Corrected = false;
				Corrected = boolCorrected;
				return Corrected;
			}
		}

		public string EmployeeFirstName
		{
			set
			{
				strEmployeeFirstName = value;
			}
			get
			{
				return strEmployeeFirstName;
			}
		}

		public string EmployeeMiddleName
		{
			set
			{
				strEmployeeMiddleName = value;
			}
			get
			{
				return strEmployeeMiddleName;
			}
		}

		public string EmployeeLastName
		{
			set
			{
				strEmployeeLastName = value;
			}
			get
			{
				return strEmployeeLastName;
			}
		}
	}
}
