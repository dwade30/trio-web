﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cAggregatedMember
	{
		//=========================================================
		private string strName = string.Empty;
		private string strEIN = string.Empty;
		private int lngID;

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public string EIN
		{
			set
			{
				strEIN = value;
			}
			get
			{
				string EIN = "";
				EIN = strEIN;
				return EIN;
			}
		}
	}
}
