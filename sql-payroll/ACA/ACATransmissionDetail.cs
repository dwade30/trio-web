﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cACATransmissionDetail
	{
		//=========================================================
		private int mvarID;
		// local copy
		private int mvarACATransmissionID;
		// local copy
		private int mvarRecordID;
		// local copy
		private int mvarACASetupID;
		// local copy
		private bool mvarIsUpdated;
		// local copy
		private bool mvarIsDeleted;
		// local copy
		public bool IsDeleted
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.IsDeleted = 5
				mvarIsDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.IsDeleted
				IsDeleted = mvarIsDeleted;
				return IsDeleted;
			}
		}

		public bool IsUpdated
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.IsUpdated = 5
				mvarIsUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.IsUpdated
				IsUpdated = mvarIsUpdated;
				return IsUpdated;
			}
		}

		public int ACASetupID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.ACASetupID = 5
				mvarACASetupID = value;
				IsUpdated = true;
			}
			get
			{
				int ACASetupID = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.ACASetupID
				ACASetupID = mvarACASetupID;
				return ACASetupID;
			}
		}

		public int RecordID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.RecordID = 5
				mvarRecordID = value;
				IsUpdated = true;
			}
			get
			{
				int RecordID = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.RecordID
				RecordID = mvarRecordID;
				return RecordID;
			}
		}

		public int ACATransmissionID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.ACATransmissionID = 5
				mvarACATransmissionID = value;
				IsUpdated = true;
			}
			get
			{
				int ACATransmissionID = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.ACATransmissionID
				ACATransmissionID = mvarACATransmissionID;
				return ACATransmissionID;
			}
		}

		public int ID
		{
			set
			{
				// used when assigning a value to the property, on the left side of an assignment.
				// Syntax: X.ID = 5
				mvarID = value;
			}
			get
			{
				int ID = 0;
				// used when retrieving value of a property, on the right side of an assignment.
				// Syntax: Debug.Print X.ID
				ID = mvarID;
				return ID;
			}
		}
	}
}
