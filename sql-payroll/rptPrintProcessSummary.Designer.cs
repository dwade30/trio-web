﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessSummary.
	/// </summary>
	partial class rptPrintProcessSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPrintProcessSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeductions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSickAccrued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSickUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacAccrued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherAccrued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetPayTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEMatchTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistHoursTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeductionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSickAccrTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSickUsedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacAccrTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacUsedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherAccrTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherUsedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalEmployees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickAccrued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacAccrued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAccrued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPayTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistHoursTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickAccrTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickUsedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacAccrTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAccrTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUsedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee,
				this.txtGross,
				this.txtFederal,
				this.txtFICA,
				this.txtMedicare,
				this.txtDeductions,
				this.txtNetPay,
				this.txtMatch,
				this.txtDistHours,
				this.txtSickAccrued,
				this.txtSickUsed,
				this.txtVacAccrued,
				this.txtVacUsed,
				this.txtOtherAccrued,
				this.txtOtherUsed,
				this.txtState
			});
			this.Detail.Height = 0.2F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNetPayTotal,
				this.txtEMatchTotal,
				this.txtDistHoursTotal,
				this.txtGrossTotal,
				this.txtFedTotal,
				this.txtStateTotal,
				this.txtFICATotal,
				this.txtMedTotal,
				this.txtTotalTaxes,
				this.Field23,
				this.Field24,
				this.txtDeductionTotal,
				this.txtSickAccrTotal,
				this.txtSickUsedTotal,
				this.txtVacAccrTotal,
				this.txtVacUsedTotal,
				this.txtOtherAccrTotal,
				this.txtOtherUsedTotal,
				this.Line2,
				this.txtTotalEmployees,
				this.Field26
			});
			this.ReportFooter.Height = 0.6666667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Line1,
				this.txtTitle,
				this.txtDate,
				this.txtPage,
				this.txtMuniName,
				this.txtTime,
				this.txtPayDate
			});
			this.PageHeader.Height = 1.052083F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 0.09375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = "Employee";
			this.Field1.Top = 0.8125F;
			this.Field1.Width = 0.6875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2333333F;
			this.Field2.Left = 2.066667F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field2.Text = "Gross";
			this.Field2.Top = 0.8F;
			this.Field2.Width = 0.5333334F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2333333F;
			this.Field3.Left = 2.633333F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field3.Text = "Federal";
			this.Field3.Top = 0.8F;
			this.Field3.Width = 0.5666667F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2333333F;
			this.Field4.Left = 3.2F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field4.Text = "FICA";
			this.Field4.Top = 0.8F;
			this.Field4.Width = 0.5666667F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2333333F;
			this.Field5.Left = 3.766667F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field5.Text = "Medicare";
			this.Field5.Top = 0.8F;
			this.Field5.Width = 0.5666667F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2333333F;
			this.Field6.Left = 4.933333F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field6.Text = "Ded";
			this.Field6.Top = 0.8F;
			this.Field6.Width = 0.4333333F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.2333333F;
			this.Field8.Left = 5.441667F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field8.Text = "Net Pay";
			this.Field8.Top = 0.8F;
			this.Field8.Width = 0.7F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2333333F;
			this.Field9.Left = 6.1375F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field9.Text = "E/Match";
			this.Field9.Top = 0.8F;
			this.Field9.Width = 0.6333333F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.2333333F;
			this.Field10.Left = 6.508333F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field10.Text = "Hours";
			this.Field10.Top = 0.8F;
			this.Field10.Width = 0.6666667F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.2333333F;
			this.Field11.Left = 7.241667F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field11.Text = "Accr";
			this.Field11.Top = 0.8F;
			this.Field11.Width = 0.525F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.2333333F;
			this.Field12.Left = 7.766667F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field12.Text = "Used";
			this.Field12.Top = 0.8F;
			this.Field12.Width = 0.525F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.2333333F;
			this.Field13.Left = 8.325F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field13.Text = "Accr";
			this.Field13.Top = 0.8F;
			this.Field13.Width = 0.4958333F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.2333333F;
			this.Field14.Left = 8.820833F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field14.Text = "Used";
			this.Field14.Top = 0.8F;
			this.Field14.Width = 0.525F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.2333333F;
			this.Field15.Left = 9.4125F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field15.Text = "Accr";
			this.Field15.Top = 0.8F;
			this.Field15.Width = 0.4625F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.2333333F;
			this.Field16.Left = 9.875F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field16.Text = "Used";
			this.Field16.Top = 0.8F;
			this.Field16.Width = 0.525F;
			// 
			// Field17
			// 
			this.Field17.CanGrow = false;
			this.Field17.Height = 0.1666667F;
			this.Field17.Left = 2.633333F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field17.Text = "- - - - - - - -   T   A   X   E   S   - - - - - - - - -";
			this.Field17.Top = 0.6333333F;
			this.Field17.Width = 2.266667F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.2333333F;
			this.Field18.Left = 4.3F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field18.Text = "State";
			this.Field18.Top = 0.8F;
			this.Field18.Width = 0.5666667F;
			// 
			// Field19
			// 
			this.Field19.CanGrow = false;
			this.Field19.Height = 0.1666667F;
			this.Field19.Left = 7.241667F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field19.Text = "- - S I C K - -";
			this.Field19.Top = 0.6333333F;
			this.Field19.Width = 1.020833F;
			// 
			// Field20
			// 
			this.Field20.CanGrow = false;
			this.Field20.Height = 0.1666667F;
			this.Field20.Left = 8.325F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field20.Text = "- - - V A C - - -";
			this.Field20.Top = 0.6333333F;
			this.Field20.Width = 0.9916667F;
			// 
			// Field21
			// 
			this.Field21.CanGrow = false;
			this.Field21.Height = 0.1666667F;
			this.Field21.Left = 9.441667F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field21.Text = "- O T H E R -";
			this.Field21.Top = 0.6333333F;
			this.Field21.Width = 0.9583333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.033333F;
			this.Line1.Width = 10.43333F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10.43333F;
			this.Line1.Y1 = 1.033333F;
			this.Line1.Y2 = 1.033333F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.2333333F;
			this.txtTitle.Left = 0.03333334F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.03333334F;
			this.txtTitle.Width = 10.43333F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 8.3F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.03333334F;
			this.txtDate.Width = 2.166667F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2333333F;
			this.txtPage.Left = 9.333333F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.2F;
			this.txtPage.Width = 1.133333F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// txtPayDate
			// 
			this.txtPayDate.Height = 0.2F;
			this.txtPayDate.Left = 0.03333334F;
			this.txtPayDate.MultiLine = false;
			this.txtPayDate.Name = "txtPayDate";
			this.txtPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtPayDate.Text = "Title";
			this.txtPayDate.Top = 0.2666667F;
			this.txtPayDate.Width = 10.43333F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2F;
			this.txtEmployee.Left = 0.03333334F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmployee.Text = "Employee";
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 2F;
			// 
			// txtGross
			// 
			this.txtGross.Height = 0.2F;
			this.txtGross.Left = 2.066667F;
			this.txtGross.Name = "txtGross";
			this.txtGross.OutputFormat = resources.GetString("txtGross.OutputFormat");
			this.txtGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtGross.Text = "Gross";
			this.txtGross.Top = 0F;
			this.txtGross.Width = 0.5666667F;
			// 
			// txtFederal
			// 
			this.txtFederal.Height = 0.2F;
			this.txtFederal.Left = 2.633333F;
			this.txtFederal.Name = "txtFederal";
			this.txtFederal.OutputFormat = resources.GetString("txtFederal.OutputFormat");
			this.txtFederal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFederal.Text = "Federal";
			this.txtFederal.Top = 0F;
			this.txtFederal.Width = 0.5666667F;
			// 
			// txtFICA
			// 
			this.txtFICA.Height = 0.2F;
			this.txtFICA.Left = 3.2F;
			this.txtFICA.Name = "txtFICA";
			this.txtFICA.OutputFormat = resources.GetString("txtFICA.OutputFormat");
			this.txtFICA.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICA.Text = "FICA";
			this.txtFICA.Top = 0F;
			this.txtFICA.Width = 0.5666667F;
			// 
			// txtMedicare
			// 
			this.txtMedicare.Height = 0.2F;
			this.txtMedicare.Left = 3.766667F;
			this.txtMedicare.Name = "txtMedicare";
			this.txtMedicare.OutputFormat = resources.GetString("txtMedicare.OutputFormat");
			this.txtMedicare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedicare.Text = "Medicare";
			this.txtMedicare.Top = 0F;
			this.txtMedicare.Width = 0.5666667F;
			// 
			// txtDeductions
			// 
			this.txtDeductions.Height = 0.2F;
			this.txtDeductions.Left = 4.9F;
			this.txtDeductions.Name = "txtDeductions";
			this.txtDeductions.OutputFormat = resources.GetString("txtDeductions.OutputFormat");
			this.txtDeductions.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDeductions.Text = "Deductions";
			this.txtDeductions.Top = 0F;
			this.txtDeductions.Width = 0.5333334F;
			// 
			// txtNetPay
			// 
			this.txtNetPay.Height = 0.2F;
			this.txtNetPay.Left = 5.441667F;
			this.txtNetPay.Name = "txtNetPay";
			this.txtNetPay.OutputFormat = resources.GetString("txtNetPay.OutputFormat");
			this.txtNetPay.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtNetPay.Text = "Net Pay";
			this.txtNetPay.Top = 0F;
			this.txtNetPay.Width = 0.7F;
			// 
			// txtMatch
			// 
			this.txtMatch.Height = 0.2F;
			this.txtMatch.Left = 6.141667F;
			this.txtMatch.Name = "txtMatch";
			this.txtMatch.OutputFormat = resources.GetString("txtMatch.OutputFormat");
			this.txtMatch.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMatch.Text = "E/Match";
			this.txtMatch.Top = 0F;
			this.txtMatch.Width = 0.5666667F;
			// 
			// txtDistHours
			// 
			this.txtDistHours.Height = 0.2F;
			this.txtDistHours.Left = 6.741667F;
			this.txtDistHours.Name = "txtDistHours";
			this.txtDistHours.OutputFormat = resources.GetString("txtDistHours.OutputFormat");
			this.txtDistHours.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDistHours.Text = "Dist Hours";
			this.txtDistHours.Top = 0F;
			this.txtDistHours.Width = 0.4333333F;
			// 
			// txtSickAccrued
			// 
			this.txtSickAccrued.Height = 0.2F;
			this.txtSickAccrued.Left = 7.241667F;
			this.txtSickAccrued.Name = "txtSickAccrued";
			this.txtSickAccrued.OutputFormat = resources.GetString("txtSickAccrued.OutputFormat");
			this.txtSickAccrued.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSickAccrued.Text = "Accr";
			this.txtSickAccrued.Top = 0F;
			this.txtSickAccrued.Width = 0.525F;
			// 
			// txtSickUsed
			// 
			this.txtSickUsed.Height = 0.2F;
			this.txtSickUsed.Left = 7.766667F;
			this.txtSickUsed.Name = "txtSickUsed";
			this.txtSickUsed.OutputFormat = resources.GetString("txtSickUsed.OutputFormat");
			this.txtSickUsed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSickUsed.Text = "Used";
			this.txtSickUsed.Top = 0F;
			this.txtSickUsed.Width = 0.525F;
			// 
			// txtVacAccrued
			// 
			this.txtVacAccrued.Height = 0.2F;
			this.txtVacAccrued.Left = 8.325F;
			this.txtVacAccrued.Name = "txtVacAccrued";
			this.txtVacAccrued.OutputFormat = resources.GetString("txtVacAccrued.OutputFormat");
			this.txtVacAccrued.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtVacAccrued.Text = "Accr";
			this.txtVacAccrued.Top = 0F;
			this.txtVacAccrued.Width = 0.4958333F;
			// 
			// txtVacUsed
			// 
			this.txtVacUsed.Height = 0.2F;
			this.txtVacUsed.Left = 8.820833F;
			this.txtVacUsed.Name = "txtVacUsed";
			this.txtVacUsed.OutputFormat = resources.GetString("txtVacUsed.OutputFormat");
			this.txtVacUsed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtVacUsed.Text = "Used";
			this.txtVacUsed.Top = 0F;
			this.txtVacUsed.Width = 0.525F;
			// 
			// txtOtherAccrued
			// 
			this.txtOtherAccrued.Height = 0.2F;
			this.txtOtherAccrued.Left = 9.4125F;
			this.txtOtherAccrued.Name = "txtOtherAccrued";
			this.txtOtherAccrued.OutputFormat = resources.GetString("txtOtherAccrued.OutputFormat");
			this.txtOtherAccrued.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtOtherAccrued.Text = "Accr";
			this.txtOtherAccrued.Top = 0F;
			this.txtOtherAccrued.Width = 0.4625F;
			// 
			// txtOtherUsed
			// 
			this.txtOtherUsed.Height = 0.2F;
			this.txtOtherUsed.Left = 9.875F;
			this.txtOtherUsed.Name = "txtOtherUsed";
			this.txtOtherUsed.OutputFormat = resources.GetString("txtOtherUsed.OutputFormat");
			this.txtOtherUsed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtOtherUsed.Text = "Used";
			this.txtOtherUsed.Top = 0F;
			this.txtOtherUsed.Width = 0.525F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.2F;
			this.txtState.Left = 4.3F;
			this.txtState.Name = "txtState";
			this.txtState.OutputFormat = resources.GetString("txtState.OutputFormat");
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtState.Text = "State";
			this.txtState.Top = 0F;
			this.txtState.Width = 0.5666667F;
			// 
			// txtNetPayTotal
			// 
			this.txtNetPayTotal.Height = 0.2F;
			this.txtNetPayTotal.Left = 5.441667F;
			this.txtNetPayTotal.Name = "txtNetPayTotal";
			this.txtNetPayTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtNetPayTotal.Text = null;
			this.txtNetPayTotal.Top = 0.1F;
			this.txtNetPayTotal.Width = 0.7F;
			// 
			// txtEMatchTotal
			// 
			this.txtEMatchTotal.Height = 0.2F;
			this.txtEMatchTotal.Left = 6.141667F;
			this.txtEMatchTotal.Name = "txtEMatchTotal";
			this.txtEMatchTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtEMatchTotal.Text = null;
			this.txtEMatchTotal.Top = 0.1F;
			this.txtEMatchTotal.Width = 0.5666667F;
			// 
			// txtDistHoursTotal
			// 
			this.txtDistHoursTotal.Height = 0.2F;
			this.txtDistHoursTotal.Left = 6.708333F;
			this.txtDistHoursTotal.Name = "txtDistHoursTotal";
			this.txtDistHoursTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDistHoursTotal.Text = null;
			this.txtDistHoursTotal.Top = 0.1F;
			this.txtDistHoursTotal.Width = 0.4666667F;
			// 
			// txtGrossTotal
			// 
			this.txtGrossTotal.Height = 0.2F;
			this.txtGrossTotal.Left = 2.066667F;
			this.txtGrossTotal.Name = "txtGrossTotal";
			this.txtGrossTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtGrossTotal.Text = null;
			this.txtGrossTotal.Top = 0.1F;
			this.txtGrossTotal.Width = 0.5666667F;
			// 
			// txtFedTotal
			// 
			this.txtFedTotal.Height = 0.2F;
			this.txtFedTotal.Left = 2.633333F;
			this.txtFedTotal.Name = "txtFedTotal";
			this.txtFedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFedTotal.Text = null;
			this.txtFedTotal.Top = 0.1F;
			this.txtFedTotal.Width = 0.5666667F;
			// 
			// txtStateTotal
			// 
			this.txtStateTotal.Height = 0.2F;
			this.txtStateTotal.Left = 4.3F;
			this.txtStateTotal.Name = "txtStateTotal";
			this.txtStateTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateTotal.Text = null;
			this.txtStateTotal.Top = 0.1F;
			this.txtStateTotal.Width = 0.5666667F;
			// 
			// txtFICATotal
			// 
			this.txtFICATotal.Height = 0.2F;
			this.txtFICATotal.Left = 3.2F;
			this.txtFICATotal.Name = "txtFICATotal";
			this.txtFICATotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICATotal.Text = null;
			this.txtFICATotal.Top = 0.1F;
			this.txtFICATotal.Width = 0.5666667F;
			// 
			// txtMedTotal
			// 
			this.txtMedTotal.Height = 0.2F;
			this.txtMedTotal.Left = 3.766667F;
			this.txtMedTotal.Name = "txtMedTotal";
			this.txtMedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedTotal.Text = null;
			this.txtMedTotal.Top = 0.1F;
			this.txtMedTotal.Width = 0.5666667F;
			// 
			// txtTotalTaxes
			// 
			this.txtTotalTaxes.Height = 0.1666667F;
			this.txtTotalTaxes.Left = 2.7F;
			this.txtTotalTaxes.Name = "txtTotalTaxes";
			this.txtTotalTaxes.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalTaxes.Text = null;
			this.txtTotalTaxes.Top = 0.4333333F;
			this.txtTotalTaxes.Width = 0.5666667F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1666667F;
			this.Field23.Left = 0.06666667F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field23.Text = "Totals:";
			this.Field23.Top = 0.1333333F;
			this.Field23.Width = 0.6F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1666667F;
			this.Field24.Left = 1.766667F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field24.Text = "Total Taxes:";
			this.Field24.Top = 0.4333333F;
			this.Field24.Width = 0.8666667F;
			// 
			// txtDeductionTotal
			// 
			this.txtDeductionTotal.Height = 0.2F;
			this.txtDeductionTotal.Left = 4.9F;
			this.txtDeductionTotal.Name = "txtDeductionTotal";
			this.txtDeductionTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDeductionTotal.Text = null;
			this.txtDeductionTotal.Top = 0.1F;
			this.txtDeductionTotal.Width = 0.5333334F;
			// 
			// txtSickAccrTotal
			// 
			this.txtSickAccrTotal.Height = 0.2F;
			this.txtSickAccrTotal.Left = 7.241667F;
			this.txtSickAccrTotal.Name = "txtSickAccrTotal";
			this.txtSickAccrTotal.OutputFormat = resources.GetString("txtSickAccrTotal.OutputFormat");
			this.txtSickAccrTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSickAccrTotal.Text = null;
			this.txtSickAccrTotal.Top = 0.1F;
			this.txtSickAccrTotal.Width = 0.525F;
			// 
			// txtSickUsedTotal
			// 
			this.txtSickUsedTotal.Height = 0.2F;
			this.txtSickUsedTotal.Left = 7.766667F;
			this.txtSickUsedTotal.Name = "txtSickUsedTotal";
			this.txtSickUsedTotal.OutputFormat = resources.GetString("txtSickUsedTotal.OutputFormat");
			this.txtSickUsedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSickUsedTotal.Text = null;
			this.txtSickUsedTotal.Top = 0.1F;
			this.txtSickUsedTotal.Width = 0.525F;
			// 
			// txtVacAccrTotal
			// 
			this.txtVacAccrTotal.Height = 0.2F;
			this.txtVacAccrTotal.Left = 8.325F;
			this.txtVacAccrTotal.Name = "txtVacAccrTotal";
			this.txtVacAccrTotal.OutputFormat = resources.GetString("txtVacAccrTotal.OutputFormat");
			this.txtVacAccrTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtVacAccrTotal.Text = null;
			this.txtVacAccrTotal.Top = 0.1F;
			this.txtVacAccrTotal.Width = 0.4958333F;
			// 
			// txtVacUsedTotal
			// 
			this.txtVacUsedTotal.Height = 0.2F;
			this.txtVacUsedTotal.Left = 8.820833F;
			this.txtVacUsedTotal.Name = "txtVacUsedTotal";
			this.txtVacUsedTotal.OutputFormat = resources.GetString("txtVacUsedTotal.OutputFormat");
			this.txtVacUsedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtVacUsedTotal.Text = null;
			this.txtVacUsedTotal.Top = 0.1F;
			this.txtVacUsedTotal.Width = 0.525F;
			// 
			// txtOtherAccrTotal
			// 
			this.txtOtherAccrTotal.Height = 0.1979167F;
			this.txtOtherAccrTotal.Left = 9.375F;
			this.txtOtherAccrTotal.Name = "txtOtherAccrTotal";
			this.txtOtherAccrTotal.OutputFormat = resources.GetString("txtOtherAccrTotal.OutputFormat");
			this.txtOtherAccrTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtOtherAccrTotal.Text = null;
			this.txtOtherAccrTotal.Top = 0.1041667F;
			this.txtOtherAccrTotal.Width = 0.5F;
			// 
			// txtOtherUsedTotal
			// 
			this.txtOtherUsedTotal.Height = 0.2F;
			this.txtOtherUsedTotal.Left = 9.875F;
			this.txtOtherUsedTotal.Name = "txtOtherUsedTotal";
			this.txtOtherUsedTotal.OutputFormat = resources.GetString("txtOtherUsedTotal.OutputFormat");
			this.txtOtherUsedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtOtherUsedTotal.Text = null;
			this.txtOtherUsedTotal.Top = 0.1F;
			this.txtOtherUsedTotal.Width = 0.525F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.666667F;
			this.Line2.LineWeight = 2F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.1F;
			this.Line2.Width = 8.733333F;
			this.Line2.X1 = 1.666667F;
			this.Line2.X2 = 10.4F;
			this.Line2.Y1 = 0.1F;
			this.Line2.Y2 = 0.1F;
			// 
			// txtTotalEmployees
			// 
			this.txtTotalEmployees.Height = 0.1666667F;
			this.txtTotalEmployees.Left = 4.933333F;
			this.txtTotalEmployees.Name = "txtTotalEmployees";
			this.txtTotalEmployees.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalEmployees.Text = null;
			this.txtTotalEmployees.Top = 0.4333333F;
			this.txtTotalEmployees.Width = 0.5666667F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1666667F;
			this.Field26.Left = 3.766667F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field26.Text = "Total Employees:";
			this.Field26.Top = 0.4333333F;
			this.Field26.Width = 1.1F;
			// 
			// rptPrintProcessSummary
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.45833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickAccrued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacAccrued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAccrued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPayTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistHoursTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickAccrTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickUsedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacAccrTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAccrTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUsedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSickAccrued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSickUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacAccrued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAccrued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetPayTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEMatchTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistHoursTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeductionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSickAccrTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSickUsedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacAccrTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacUsedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAccrTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherUsedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalEmployees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
