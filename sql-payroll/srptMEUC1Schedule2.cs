﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMEUC1Schedule2.
	/// </summary>
	public partial class srptMEUC1Schedule2 : FCSectionReport
	{
		public srptMEUC1Schedule2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptMEUC1Schedule2 InstancePtr
		{
			get
			{
				return (srptMEUC1Schedule2)Sys.GetInstance(typeof(srptMEUC1Schedule2));
			}
		}

		protected srptMEUC1Schedule2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMEUC1Schedule2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//clsDRWrapper clsMonth1 = new clsDRWrapper();
		//clsDRWrapper clsMonth2 = new clsDRWrapper();
		//clsDRWrapper clsMonth3 = new clsDRWrapper();
		//modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		//clsDRWrapper clsC1 = new clsDRWrapper();
		bool boolLastPage;
		double dblTotalWages;
		//double dblTotalWH;
		//double dblTotalExcess;
		//int lngM1Workers;
		//int lngM2Workers;
		//int lngM3Workers;
		//int lngM1Females;
		//int lngM2Females;
		//int lngM3Females;
		//double dblLimit;
		//clsDRWrapper rsUnemployment = new clsDRWrapper();
		int intpage;
		bool boolTestPrint;
		string strQTDWhere = "";
		string strYTDWhere = "";
		int intNumRecs;
		cUC1Report theReport;
		int lngCurrentRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear;
			strYear = FCConvert.ToString(theReport.YearCovered);
			eArgs.EOF = !theReport.Details.IsCurrent();
			if (!eArgs.EOF)
			{
				// Barcode1.Caption = Right(strYear, 2) & "0850" & Format(intpage, "0")
				Barcode1.Text = "200640" + Strings.Format(intpage, "0");
				if (intpage > 9)
					Barcode1.Left = 8040 / 1440F;
				intpage += 1;
			}
			else if (intpage == 2 && boolTestPrint)
			{
				// Barcode1.Caption = Right(strYear, 2) & "0850" & Format(intpage, "0")
				Barcode1.Text = "200640" + Strings.Format(intpage, "0");
				eArgs.EOF = false;
				intpage += 1;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			theReport = (this.ParentReport as rptMEUC1).UCReportObject;
			boolTestPrint = (this.ParentReport as rptMEUC1).IsTestPrint;
			intNumRecs = 18;
			lngCurrentRecord = 1;
			intpage = 2;
			FillBoxes();
			theReport.Details.MoveFirst();
		}

		private void FillBoxes()
		{
			if (!boolTestPrint)
			{
				int intQuarterCovered;
				// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
				string strYear = "";
				txtName.Text = fecherFoundation.Strings.UCase(theReport.Summary.Name);
				txtFederalEmployerID.Text = Strings.Mid(theReport.Summary.FederalID, 1, 2) + " " + Strings.Mid(theReport.Summary.FederalID, 3);
				txtUCAccount.Text = theReport.Summary.UCAccountID;
				intQuarterCovered = theReport.Quarter;
				strYear = FCConvert.ToString(theReport.YearCovered);
				Label9.Text = "SCHEDULE 2 (FORM ME UC-1) " + strYear;
				txtYearStart.Text = strYear;
				txtYearEnd.Text = strYear;
				switch (intQuarterCovered)
				{
					case 1:
						{
							txtPeriodStart.Text = "01 01";
							txtPeriodEnd.Text = "03 31";
							break;
						}
					case 2:
						{
							txtPeriodStart.Text = "04 01";
							txtPeriodEnd.Text = "06 30";
							break;
						}
					case 3:
						{
							txtPeriodStart.Text = "07 01";
							txtPeriodEnd.Text = "09 30";
							break;
						}
					case 4:
						{
							txtPeriodStart.Text = "10 01";
							txtPeriodEnd.Text = "12 31";
							break;
						}
				}
				//end switch
			}
		}

		private void ClearBoxes()
		{
			int x;
			for (x = 1; x <= intNumRecs; x++)
			{
				(this.Detail.Controls["txtName" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtSSN" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtWage" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
            }
			// x
			txtWagePageTotal.Text = "";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblPageWage = 0;
			double dblWage = 0;
			double dblGross;
			double dblExcess;
			double dblQTDDed;
			double dblYTDDed;
			int intCurrentLine;
			string strTemp = "";
			cUC1Schedule2Detail det;
			if (!boolTestPrint)
			{
				if (theReport.Details.IsCurrent())
				{
					ClearBoxes();
					// up to intnumrecs on this page
					dblPageWage = 0;
					dblExcess = 0;
					intCurrentLine = 1;
					if (intpage > 9)
						Barcode1.Left = 8040 / 1440F;
					cUC1Schedule2BarCodeInfo bcInfo = new cUC1Schedule2BarCodeInfo();
					for (intCurrentLine = 1; intCurrentLine <= intNumRecs; intCurrentLine++)
					{
						if (!theReport.Details.IsCurrent())
						{
							boolLastPage = true;
							break;
						}
						det = theReport.Details.GetCurrentDetail();
						(Detail.Controls["txtName" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = det.LastFirst;
						(Detail.Controls["txtSSN" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = det.SSN;
						dblWage = det.UCWages;
						// ADD T
						dblPageWage += dblWage;
						(Detail.Controls["txtWage" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblWage, "0.00");
						
						bcInfo.Clear();
						bcInfo.Set_IsSeasonal(intCurrentLine, det.IsSeasonal);
						bcInfo.Set_SSN(intCurrentLine, det.SSN);
						bcInfo.Set_UCGrossWages(intCurrentLine, dblWage);
						theReport.Details.MoveNext();
					}
					// intCurrentLine
					// make this check in case the last record is the intNumRecsth on this page
					if (!theReport.Details.IsCurrent())
						boolLastPage = true;
					// keep track of grandtotals
					dblTotalWages += dblPageWage;
					txtWagePageTotal.Text = Strings.Format(dblPageWage, "0.00");
					txtWageTotal.Text = Strings.Format(theReport.Summary.UCWages, "0.00");
					bcInfo.ReportYear = FCConvert.ToString(theReport.YearCovered);
					bcInfo.UCGrossPageTotal = dblPageWage;
					bcInfo.StartDate = theReport.Summary.StartDate;
					bcInfo.EndDate = theReport.Summary.EndDate;
					bcInfo.FederalID = theReport.Summary.FederalID;
					// for now don't show the 2d barcode
					// Dim strCode As String
					// strCode = bcInfo.Get2DBarCodeString
					// Dim hBC As Long
					// Dim errCode As Integer
					// hBC = CreateBarCode(BC_PDF417, 0, 0&, 4, 150, 1, "Arial", 24, 0, 0, 30, 0, 4)
					// errCode = DrawBarCodeToClipboard(300, 300, hBC, strCode, "")
					// If errCode = 0 Then
					// Image1.Picture = Clipboard.GetData
					// End If
					// Call DeleteBarCode(hBC)
				}
			}
		}

		
	}
}
