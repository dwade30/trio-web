﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditLockedForm.
	/// </summary>
	partial class frmEditLockedForm
	{
		public fecherFoundation.FCComboBox cmbEdit;
		public fecherFoundation.FCButton cmdProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbEdit = new fecherFoundation.FCComboBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 140);
            this.BottomPanel.Size = new System.Drawing.Size(298, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbEdit);
            this.ClientArea.Size = new System.Drawing.Size(298, 80);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(298, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // cmbEdit
            // 
            this.cmbEdit.AutoSize = false;
            this.cmbEdit.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbEdit.FormattingEnabled = true;
            this.cmbEdit.Items.AddRange(new object[] {
            "Add new record",
            "Delete record",
            "Unlock Grid"});
            this.cmbEdit.Location = new System.Drawing.Point(30, 30);
            this.cmbEdit.Name = "cmbEdit";
            this.cmbEdit.Size = new System.Drawing.Size(240, 40);
            this.cmbEdit.TabIndex = 0;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(98, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 3;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmEditLockedForm
            // 
            this.AcceptButton = this.cmdProcess;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(298, 248);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEditLockedForm";
            this.Text = "Edit Locked Form";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEditLockedForm_Load);
            this.Activated += new System.EventHandler(this.frmEditLockedForm_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditLockedForm_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
