﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptAddressStub.
	/// </summary>
	public partial class srptAddressStub : FCSectionReport
	{
		public srptAddressStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptAddressStub InstancePtr
		{
			get
			{
				return (srptAddressStub)Sys.GetInstance(typeof(srptAddressStub));
			}
		}

		protected srptAddressStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAddressStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
				if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais")
				{
					Detail.Height = 5220 / 1440F;
					Detail.CanShrink = false;
					// txtCheckNo.Left = 9000
					// txtCheckNo.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left
					// txtCheckNo.Top = 240
					// txtDate.Top = 240
				}
                if (FCConvert.ToString(this.UserData) != string.Empty)
                {
                    int X;
                    if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HALLOWELL")
                    {
                    }
                    else
                    {
                        clsPrt.SetReportFontsByTag(this, "Text", FCConvert.ToString(this.UserData));
                        clsPrt.SetReportFontsByTag(this, "bold", FCConvert.ToString(this.UserData), true);
                    }
                }
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Address Stub ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				txtAddressName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
				txtAddress1.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 19);
				txtAddress2.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 20);
				txtCityStateZip.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 21);
				txtCheckNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12);
				lblReturnAddress.Text = rptLaserCheck1.InstancePtr.strReturnAddress;
				FCFileSystem fso = new FCFileSystem();
				if (rptLaserCheck1.InstancePtr.strLogoPath != string.Empty)
				{
					if (FCFileSystem.FileExists(rptLaserCheck1.InstancePtr.strLogoPath))
					{
						TXTlOGO.Image = FCUtils.LoadPicture(rptLaserCheck1.InstancePtr.strLogoPath);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Address Stub DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
