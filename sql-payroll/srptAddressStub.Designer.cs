﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptAddressStub.
	/// </summary>
	partial class srptAddressStub
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAddressStub));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAddressName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.TXTlOGO = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAddressName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTlOGO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAddressName,
				this.txtAddress1,
				this.txtCityStateZip,
				this.txtEmployeeNumCheck,
				this.txtAddress2,
				this.txtCheckNo,
				this.TXTlOGO,
				this.lblReturnAddress
			});
			this.Detail.Height = 3.625F;
			this.Detail.Name = "Detail";
			// 
			// txtAddressName
			// 
			this.txtAddressName.Height = 0.19F;
			this.txtAddressName.HyperLink = null;
			this.txtAddressName.Left = 2.25F;
			this.txtAddressName.Name = "txtAddressName";
			this.txtAddressName.Style = "";
			this.txtAddressName.Tag = "text";
			this.txtAddressName.Text = "EMPLOYEE";
			this.txtAddressName.Top = 2.71875F;
			this.txtAddressName.Width = 3.25F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.2F;
			this.txtAddress1.HyperLink = null;
			this.txtAddress1.Left = 2.25F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "";
			this.txtAddress1.Tag = "TEXT";
			this.txtAddress1.Text = "TRIOVILLE";
			this.txtAddress1.Top = 2.875F;
			this.txtAddress1.Width = 4.366667F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.Height = 0.21875F;
			this.txtCityStateZip.HyperLink = null;
			this.txtCityStateZip.Left = 2.25F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Style = "";
			this.txtCityStateZip.Tag = "TEXT";
			this.txtCityStateZip.Text = "TRIOVILLE";
			this.txtCityStateZip.Top = 3.208333F;
			this.txtCityStateZip.Width = 4.364583F;
			// 
			// txtEmployeeNumCheck
			// 
			this.txtEmployeeNumCheck.Height = 0.1666667F;
			this.txtEmployeeNumCheck.HyperLink = null;
			this.txtEmployeeNumCheck.Left = 5.5625F;
			this.txtEmployeeNumCheck.Name = "txtEmployeeNumCheck";
			this.txtEmployeeNumCheck.Style = "text-align: right";
			this.txtEmployeeNumCheck.Tag = "TEXT";
			this.txtEmployeeNumCheck.Text = null;
			this.txtEmployeeNumCheck.Top = 2.708333F;
			this.txtEmployeeNumCheck.Width = 0.65625F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1333333F;
			this.txtAddress2.HyperLink = null;
			this.txtAddress2.Left = 2.25F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "";
			this.txtAddress2.Tag = "TEXT";
			this.txtAddress2.Text = "TRIOVILLE";
			this.txtAddress2.Top = 3.075F;
			this.txtAddress2.Width = 4.366667F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.28125F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 4.75F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "font-size: 14.5pt; text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = null;
			this.txtCheckNo.Top = 1.875F;
			this.txtCheckNo.Width = 1.864583F;
			// 
			// TXTlOGO
			// 
			this.TXTlOGO.Height = 0.75F;
			this.TXTlOGO.HyperLink = null;
			this.TXTlOGO.ImageData = null;
			this.TXTlOGO.Left = 0.0625F;
			this.TXTlOGO.LineWeight = 1F;
			this.TXTlOGO.Name = "TXTlOGO";
			this.TXTlOGO.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.TXTlOGO.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.TXTlOGO.Top = 1.3125F;
			this.TXTlOGO.Visible = false;
			this.TXTlOGO.Width = 0.75F;
			// 
			// lblReturnAddress
			// 
			this.lblReturnAddress.CanGrow = false;
			this.lblReturnAddress.Height = 0.78125F;
			this.lblReturnAddress.Left = 0.875F;
			this.lblReturnAddress.Name = "lblReturnAddress";
			this.lblReturnAddress.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 1";
			this.lblReturnAddress.Tag = "Large";
			this.lblReturnAddress.Text = null;
			this.lblReturnAddress.Top = 1.28125F;
			this.lblReturnAddress.Width = 3.125F;
			// 
			// srptAddressStub
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtAddressName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTlOGO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddressName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNumCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Picture TXTlOGO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress;
	}
}
