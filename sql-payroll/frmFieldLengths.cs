//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmFieldLengths : BaseForm
	{
		public frmFieldLengths()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:BSE #2659 Allow only numeric values
            this.txtEmployeeNumber.AllowOnlyNumericInput();
            this.Label4 = new System.Collections.Generic.List<FCLabel>();
			this.Label4.AddControlArrayElement(Label4_0, 0);
			this.Label4.AddControlArrayElement(Label4_1, 1);
			this.Label4.AddControlArrayElement(Label4_2, 2);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFieldLengths InstancePtr
		{
			get
			{
				return (frmFieldLengths)Sys.GetInstance(typeof(frmFieldLengths));
			}
		}

		protected frmFieldLengths _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 30,2001
		//
		// NOTES:  Modified by Dan Soltesz, May 29, 2001
		// Aug 10, 2001
		//
		//
		// **************************************************
		// private local variables
		// Private dbLengths       As DAO.Database
		private clsDRWrapper rsLengths = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		private int intCounter;
		private int intDataChanged;
		private bool boolLoading;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cboMonth_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cboType_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// record that this form is now dirty and there is data to save
				intDataChanged = 1;
				// chkRequired.Enabled = cboType = "Numeric"
				// If cboType <> "Numeric" Then chkRequired = 0
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdDelete_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Delete the field length information from the database
				if (MessageBox.Show("This action will delete Field length infomation. Continue?", "Payroll Field Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsLengths.Execute("Delete from tblFieldLengths", "Payroll");
					cmdNew_Click();
					MessageBox.Show("Delete of record was successful", "Payroll Field Lengths", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void chkBiWeekly_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
			if (!boolLoading)
			{
				boolLoading = true;
				chkWeekly.CheckState = Wisej.Web.CheckState.Unchecked;
				boolLoading = false;
			}
		}

		private void chkRequired_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
		}

		private void chkWeekly_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
			if (!boolLoading)
			{
				boolLoading = true;
				chkBiWeekly.CheckState = Wisej.Web.CheckState.Unchecked;
				boolLoading = false;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Employee Lengths/Format", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// clear out the controls but do nothing to the database
				txtEmployeeNumber.Text = FCConvert.ToString(0);
				cboType.SelectedIndex = 0;
				txtDeptLength.Text = FCConvert.ToString(0);
				txtDivLength.Text = FCConvert.ToString(0);
				// form is now dirty
				intDataChanged = 1;
				txtEmployeeNumber.Focus();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			int x;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (Conversion.Val(txtEmployeeNumber.Text) == 0)
				{
					MessageBox.Show("Employee Number length must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtEmployeeNumber.Focus();
					return;
				}
				else if (Conversion.Val(fecherFoundation.Strings.Trim(txtEmployeeNumber.Text)) > 50)
				{
					MessageBox.Show("Employee Number length must be less then 50 characters.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtEmployeeNumber.Text = "";
					txtEmployeeNumber.Focus();
					return;
				}
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Employee Lengths", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				// get all of the records from the database
				rsLengths.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (rsLengths.EndOfFile())
				{
					rsLengths.AddNew();
				}
				else
				{
					rsLengths.Edit();
				}
				rsLengths.SetData("EmployeeNumber", Conversion.Val(txtEmployeeNumber.Text));
				rsLengths.SetData("EmployeeType", cboType.Text);
				rsLengths.SetData("DeptLength", Conversion.Val(txtDeptLength.Text));
				rsLengths.SetData("DivLength", Conversion.Val(txtDivLength.Text));
				rsLengths.SetData("LengthRequired", FCConvert.ToInt32(chkRequired.CheckState));
				rsLengths.Set_Fields("PayFreqWeekly", chkWeekly.CheckState);
				rsLengths.Set_Fields("PayFreqBiWeekly", chkBiWeekly.CheckState);
				// UPDATE THE DESCRIPTION FOR THE FREQUENCY CODE "T"
				// 
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblFrequencyCodes where ID = 11");
				if (!rsData.EndOfFile())
				{
					rsData.Edit();
					if (chkWeekly.CheckState == CheckState.Checked)
					{
						rsData.Set_Fields("Description", "Week 1-4 NOT 5");
					}
					else
					{
						rsData.Set_Fields("Description", "Week 1-2 NOT 3");
					}
					rsData.Update();
				}
				if (cboMonth.SelectedIndex < 0)
				{
					rsLengths.Set_Fields("FirstFiscalMonth", 1);
				}
				else
				{
					rsLengths.Set_Fields("FirstFiscalMonth", cboMonth.ItemData(cboMonth.SelectedIndex));
				}
				rsLengths.Update();
				rsLengths.OpenRecordset("Select * from tblFrequencyCodes where FrequencyCode = 'T'", "Twpy0000.vb1");
				if (!rsLengths.EndOfFile())
				{
					rsLengths.Edit();
					// IF THIS IS A
					if (chkBiWeekly.CheckState == CheckState.Checked)
					{
						rsLengths.Set_Fields("Description", "Week 1-2, Not 3");
						rsLengths.Set_Fields("PeriodsPerYear", 24);
					}
					if (chkWeekly.CheckState == CheckState.Checked)
					{
						rsLengths.Set_Fields("Description", "Week 1-4, Not 5");
						rsLengths.Set_Fields("PeriodsPerYear", 48);
					}
					rsLengths.Update();
				}
				// all changes have been updated
				intDataChanged = 0;
				clsHistoryClass.Compare();
				MessageBox.Show("Save was successful", "Payroll Field Lengths", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmFieldLengths_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmFieldLengths_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			if (KeyAscii == Keys.Escape)
			{
				cmdExit_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFieldLengths_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFieldLengths properties;
			//frmFieldLengths.ScaleWidth	= 5880;
			//frmFieldLengths.ScaleHeight	= 4410;
			//frmFieldLengths.LinkTopic	= "Form1";
			//frmFieldLengths.LockControls	= -1  'True;
			//End Unmaped Properties
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				rsLengths.DefaultDB = "TWPY0000.vb1";
				// this is for
				LoadMonthCombo();
				cboMonth.Enabled /*Locked*/ = modGlobalVariables.Statics.gboolBudgetary;
				cboMonth.Enabled = !modGlobalVariables.Statics.gboolBudgetary;
				// Get the data from the database
				boolLoading = true;
				ShowData();
				boolLoading = false;
				intDataChanged = 0;
				clsHistoryClass.SetGridIDColumn("PY");
				Frame3.ForeColor = Color.Red;
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intMonth = 0;
				// get all of the records from the database
				rsLengths.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsLengths.EndOfFile())
				{
					txtEmployeeNumber.Text = FCConvert.ToString(Conversion.Val(rsLengths.Get_Fields("EmployeeNumber") + " "));
					txtDeptLength.Text = FCConvert.ToString(Conversion.Val(rsLengths.Get_Fields_Int32("DeptLength") + " "));
					txtDivLength.Text = FCConvert.ToString(Conversion.Val(rsLengths.Get_Fields_Int32("DivLength") + " "));
					chkRequired.CheckState = rsLengths.Get_Fields_Boolean("LengthRequired") ? CheckState.Checked : CheckState.Unchecked;
					chkWeekly.CheckState = rsLengths.Get_Fields_Boolean("PayFreqWeekly") ? CheckState.Checked : CheckState.Unchecked;
					chkBiWeekly.CheckState = rsLengths.Get_Fields_Boolean("PayFreqBiWeekly") ? CheckState.Checked : CheckState.Unchecked;
					for (intCounter = 0; intCounter <= cboType.Items.Count - 1; intCounter++)
					{
						if (cboType.Items[intCounter].ToString() == fecherFoundation.Strings.Trim(rsLengths.Get_Fields_String("EmployeeType") + " "))
						{
							cboType.SelectedIndex = intCounter;
							break;
						}
					}
				}
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
					if (rsData.EndOfFile())
					{
						intMonth = 1;
					}
					else
					{
						txtDeptLength.Text = Strings.Left(fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Expenditure") + " "), 2);
						txtDeptLength.Enabled = false;
						txtDivLength.Text = Strings.Mid(fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Expenditure") + " "), 3, 2);
						txtDivLength.Enabled = false;
						intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_String("FiscalStart"))));
					}
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLengths.Get_Fields_Int16("FirstFiscalMonth"))));
				}
				for (intCounter = 0; intCounter <= 11; intCounter++)
				{
					if (cboMonth.ItemData(intCounter) == intMonth)
					{
						cboMonth.SelectedIndex = intCounter;
						break;
					}
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadMonthCombo()
		{
			cboMonth.Clear();
			cboMonth.AddItem("January");
			cboMonth.ItemData(cboMonth.NewIndex, 1);
			cboMonth.AddItem("February");
			cboMonth.ItemData(cboMonth.NewIndex, 2);
			cboMonth.AddItem("March");
			cboMonth.ItemData(cboMonth.NewIndex, 3);
			cboMonth.AddItem("April");
			cboMonth.ItemData(cboMonth.NewIndex, 4);
			cboMonth.AddItem("May");
			cboMonth.ItemData(cboMonth.NewIndex, 5);
			cboMonth.AddItem("June");
			cboMonth.ItemData(cboMonth.NewIndex, 6);
			cboMonth.AddItem("July");
			cboMonth.ItemData(cboMonth.NewIndex, 7);
			cboMonth.AddItem("August");
			cboMonth.ItemData(cboMonth.NewIndex, 8);
			cboMonth.AddItem("September");
			cboMonth.ItemData(cboMonth.NewIndex, 9);
			cboMonth.AddItem("October");
			cboMonth.ItemData(cboMonth.NewIndex, 10);
			cboMonth.AddItem("November");
			cboMonth.ItemData(cboMonth.NewIndex, 11);
			cboMonth.AddItem("December");
			cboMonth.ItemData(cboMonth.NewIndex, 12);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void txtDeptLength_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtDeptLength_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtDeptLength.SelectionStart = 0;
				txtDeptLength.SelectionLength = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtDeptLength_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtDeptLength_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Do not allow the entry of a character
				if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = 1;
				}
				if (txtDeptLength.Text.Length > 9 && KeyAscii != Keys.Back)
				{
					MessageBox.Show("Max Length Of 10", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					KeyAscii = (Keys)0;
					return;
				}
				if (txtDeptLength.Text.Length > 3 && KeyAscii != Keys.Back)
				{
					txtDeptLength.WidthOriginal = txtDeptLength.WidthOriginal + 100;
				}
				if (txtDeptLength.Text.Length > 4 && KeyAscii == Keys.Back)
				{
					txtDeptLength.WidthOriginal = txtDeptLength.WidthOriginal - 100;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtDeptLength_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtDeptLength.Text) < 1)
			{
				MessageBox.Show("Value Must Be Greater Than 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void txtDivLength_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtDivLength_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtDivLength.SelectionStart = 0;
				txtDivLength.SelectionLength = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtDivLength_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtDivLength_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Do not allow the entry of a character
				if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = 1;
				}
				if (txtDivLength.Text.Length > 9 && KeyAscii != Keys.Back)
				{
					MessageBox.Show("Max Length Of 10", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					KeyAscii = (Keys)0;
					return;
				}
				if (txtDivLength.Text.Length > 3 && KeyAscii != Keys.Back)
				{
					txtDivLength.WidthOriginal = txtDivLength.WidthOriginal + 100;
				}
				if (txtDivLength.Text.Length > 4 && KeyAscii == Keys.Back)
				{
					txtDivLength.WidthOriginal = txtDivLength.WidthOriginal - 100;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtEmployeeNumber_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtEmployeeNumber_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtEmployeeNumber.SelectionStart = 0;
				txtEmployeeNumber.SelectionLength = fecherFoundation.Strings.Trim(txtEmployeeNumber.Text).Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtEmployeeNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtEmployeeNumber_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Do not allow the entry of a character
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
					intDataChanged = 1;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtEmployeeNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Employee Number Length";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Employee Number Format";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkRequired";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Employee Number Length Required";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeptLength";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Department Length";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtDivLength";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Division Length";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboMonth";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "First Fiscal Month";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkWeekly";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Weekly Pay Frequency";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkBiWeekly";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Bi-Weekly Pay Frequency";
			intTotalNumberOfControls += 1;
		}
	}
}
