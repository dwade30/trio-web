﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptGenericList.
	/// </summary>
	public partial class rptGenericList : BaseSectionReport
	{
		public rptGenericList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptGenericList InstancePtr
		{
			get
			{
				return (rptGenericList)Sys.GetInstance(typeof(rptGenericList));
			}
		}

		protected rptGenericList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptGenericList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cGenericCollection reportList;

		public void Init(ref cGenericCollection coll, string strTitle, string strCaption)
		{
			if (!(coll == null))
			{
				reportList = coll;
			}
			else
			{
				reportList = new cGenericCollection();
			}
			txtCaption.Text = strTitle;
			this.Name = strCaption;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !reportList.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			reportList.MoveFirst();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (reportList.IsCurrent())
			{
				txtDetail.Text = FCConvert.ToString(reportList.GetCurrentItem());
				reportList.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
