//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeDeductions.
	/// </summary>
	public partial class srptEmployeeDeductions : FCSectionReport
	{
		public srptEmployeeDeductions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "sub Employee Deduction Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptEmployeeDeductions InstancePtr
		{
			get
			{
				return (srptEmployeeDeductions)Sys.GetInstance(typeof(srptEmployeeDeductions));
			}
		}

		protected srptEmployeeDeductions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsProcessing?.Dispose();
                rsProcessing = null;
				rsDeductions?.Dispose();
                rsDeductions = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptEmployeeDeductions	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// private local variables
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		private clsDRWrapper rsDeductions = new clsDRWrapper();
		
		private string strEmployee = "";
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object IsDefaultDedution()
		{
			object IsDefaultDedution = null;
			
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("Description"))) == string.Empty && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("TaxStatusCode")))) == 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("Amount")))) == 0 && fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields_String("AmountType"))) == string.Empty && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("FrequencyCode")))) == 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields_Double("Limit")))) == 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("Priority")))) == 0)
			{
				IsDefaultDedution = true;
			}
			else
			{
				IsDefaultDedution = false;
			}
			return IsDefaultDedution;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: txtAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txtFreqTax As object	OnWrite(string)
			// vbPorter upgrade warning: txtLimit As object	OnWrite(string)
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType = "";
				string strAmount = "";
				double dblTemp;
				if (rsProcessing.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				if (FCConvert.ToBoolean(IsDefaultDedution()))
				{
					if (FCConvert.ToInt32(rsProcessing.Get_Fields_Int32("DeductionNumber")) == 0)
					{
					}
					else
					{
						txtAmount.Text = Strings.Format(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].Amount, "#,##0.00");
						// txtAmountDP = Format(aryDefautlDeduction(rsProcessing.Fields("DeductionNumber")).AmountType, "$#,##0.00")
						string vbPorterVar = modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].AmountType;
						if (vbPorterVar == "Dollars")
						{
							txtAmountDP.Text = "$";
						}
						else if (vbPorterVar == "Percent")
						{
							txtAmountDP.Text = "%";
						}
						else
						{
							txtAmountDP.Text = modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].AmountType;
						}
						txtFreqTax.Text = FCConvert.ToString(GetFrequency(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].FreqCode))) + "/" + FCConvert.ToString(GetTaxCode(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].TaxStatusCode)));
					}
				}
				else
				{
					if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("Amount")))) == 0)
					{
						txtAmount.Text = Strings.Format(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].Amount, "#,##0.00");
					}
					else
					{
						txtAmount.Text = Strings.Format(rsProcessing.Get_Fields("Amount"), "#,##0.00");
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields_String("AmountType"))) == string.Empty)
					{
						string vbPorterVar1 = modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].AmountType;
						if (vbPorterVar1 == "Dollars")
						{
							txtAmountDP.Text = "$";
						}
						else if (vbPorterVar1 == "Percent")
						{
							txtAmountDP.Text = "%";
						}
						else
						{
							txtAmountDP.Text = modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].AmountType;
						}
						// txtAmountDP = Format(aryDefautlDeduction(rsProcessing.Fields("DeductionNumber")).AmountType, "$#,##0.00")
					}
					else
					{
						// txtAmountDP = Format(rsProcessing.Fields("AmountType"), "$#,##0.00")
						string vbPorterVar2 = rsProcessing.Get_Fields_String("AmountType");
						if (vbPorterVar2 == "Dollars")
						{
							txtAmountDP.Text = "$";
						}
						else if (vbPorterVar2 == "Percent")
						{
							txtAmountDP.Text = "%";
						}
						else
						{
							txtAmountDP.Text = rsProcessing.Get_Fields_String("AmountType");
						}
					}
					if (fecherFoundation.Strings.UCase(txtAmountDP.Text) == "DOLLARS")
						txtAmountDP.Text = "$";
					if (fecherFoundation.Strings.UCase(txtAmountDP.Text) == "PERCENT")
						txtAmountDP.Text = "%";
					if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("FrequencyCode")))) == 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("TaxStatusCode")))) == 0)
					{
						// get both from default screen
						txtFreqTax.Text = FCConvert.ToString(GetFrequency(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].FreqCode))) + "/" + FCConvert.ToString(GetTaxCode(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].TaxStatusCode)));
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("FrequencyCode")))) == 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("TaxStatusCode")))) != 0)
					{
						// get frequency from default screen and NOT tax status
						txtFreqTax.Text = FCConvert.ToString(GetFrequency(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].FreqCode))) + "/" + FCConvert.ToString(GetTaxCode(rsProcessing.Get_Fields("TaxStatusCode")));
					}
					else if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("FrequencyCode")))) != 0 && Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields("TaxStatusCode")))) == 0)
					{
						// get tax status from default screen and NOT frequency
						txtFreqTax.Text = string.Empty;
						if (FCConvert.ToInt32(rsProcessing.Get_Fields_Int32("DeductionNumber")) != 0)
						{
							txtFreqTax.Text = FCConvert.ToString(GetFrequency(rsProcessing.Get_Fields("FrequencyCode"))) + "/" + FCConvert.ToString(GetTaxCode(FCConvert.ToInt32(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields_Int32("DeductionNumber")].TaxStatusCode)));
						}
					}
					else
					{
						// get both from Employee Deductions screen
						txtFreqTax.Text = FCConvert.ToString(GetFrequency(rsProcessing.Get_Fields("FrequencyCode"))) + "/" + FCConvert.ToString(GetTaxCode(rsProcessing.Get_Fields("TaxStatusCode")));
					}
				}
				// txtDedCode = GetDeductionNumber(rsProcessing.Fields("DeductionCode"))
				if (Conversion.Val(rsProcessing.Get_Fields("limit")) == 0)
				{
					txtLimit.Text = Strings.Format(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].Limit, "$#,###,##0.00");
					if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "LIFE")
					{
						txtPer.Text = "LIF";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "FISCAL")
					{
						txtPer.Text = "FIS";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "CALENDAR")
					{
						txtPer.Text = "CAL";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "QUARTER")
					{
						txtPer.Text = "QTR";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "MONTH")
					{
						txtPer.Text = "MON";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].LimitType) == "PAY-PERIOD")
					{
						txtPer.Text = "PER";
					}
				}
				else
				{
					txtLimit.Text = Strings.Format(rsProcessing.Get_Fields_Double("Limit"), "$#,##0.00");
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "LIFE")
					{
						txtPer.Text = "LIF";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "FISCAL")
					{
						txtPer.Text = "FIS";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "CALENDAR")
					{
						txtPer.Text = "CAL";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "QUARTER")
					{
						txtPer.Text = "QTR";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "MONTH")
					{
						txtPer.Text = "MON";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsProcessing.Get_Fields("Limittype"))) == "PAY-PERIOD")
					{
						txtPer.Text = "PER";
					}
				}
				dblTemp = modCoreysSweeterCode.GetCurrentDeductionTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtCurrent.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblTemp = modCoreysSweeterCode.GetMTDDeductionTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtMTD.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblTemp = modCoreysSweeterCode.GetQTDDeductionTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtQTD.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblTemp = modCoreysSweeterCode.GetFYTDDeductionTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtFiscal.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblTemp = modCoreysSweeterCode.GetCYTDDeductionTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtCalendar.Text = Strings.Format(dblTemp, "$#,##0.00");
				txtLTD.Text = Strings.Format(rsProcessing.Get_Fields_Double("LTDTotal"), "$#,##0.00");
				txtDedCode.Text = modGlobalVariables.Statics.aryDefautlDeduction[rsProcessing.Get_Fields("deductionnumber")].des;
				rsProcessing.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: intID As int	OnWrite(string)
		private object GetFrequency(int intID)
		{
			object GetFrequency = null;
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                rsTemp.OpenRecordset("Select FrequencyCode from tblFrequencyCodes where ID = " +
                                     FCConvert.ToString(intID));
                if (!rsTemp.EndOfFile())
                {
                    GetFrequency = rsTemp.Get_Fields("FrequencyCode");
                }

                return GetFrequency;
            }
        }
		// vbPorter upgrade warning: intID As int	OnWrite(string)
		private object GetTaxCode(int intID)
		{
			object GetTaxCode = null;
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                rsTemp.OpenRecordset("Select TaxStatusCode from tblTaxStatusCodes where ID = " +
                                     FCConvert.ToString(intID));
                if (!rsTemp.EndOfFile())
                {
                    GetTaxCode = rsTemp.Get_Fields("TaxStatusCode");
                }
            }

            return GetTaxCode;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Call rsProcessing.OpenRecordset("Select * from tblEmployeeDeductions Where EmployeeNumber = '" & gstrEmployeeToGet & "' Order by DeductionCode", "TWPY0000.vb1")
			strEmployee = FCConvert.ToString(this.UserData);
			rsProcessing.OpenRecordset("Select * from tblEmployeeDeductions Where EmployeeNumber = '" + this.UserData + "' Order by DeductionCode", "TWPY0000.vb1");
			Array.Resize(ref modGlobalVariables.Statics.aryDefautlDeduction, 0 + 1);
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
			while (!rsDeductions.EndOfFile())
			{
				if (Information.UBound(modGlobalVariables.Statics.aryDefautlDeduction, 1) < rsDeductions.Get_Fields_Int32("DeductionNumber"))
				{
					Array.Resize(ref modGlobalVariables.Statics.aryDefautlDeduction, FCConvert.ToInt32(rsDeductions.Get_Fields_Int32("DeductionNumber")) + 1);
				}
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].DeductionNumber = FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"));
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].des = rsDeductions.Get_Fields("Description");
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].TaxStatusCode = FCConvert.ToString(rsDeductions.Get_Fields_Int32("TaxStatusCode"));
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].Amount = FCConvert.ToString(rsDeductions.Get_Fields_Double("Amount"));
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].AmountType = rsDeductions.Get_Fields_String("AmountType");
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].FreqCode = FCConvert.ToString(rsDeductions.Get_Fields_Int32("FrequencyCode"));
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].Limit = FCConvert.ToString(rsDeductions.Get_Fields_Double("Limit"));
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields("deductionnumber")].LimitType = rsDeductions.Get_Fields_String("LimitType");
				modGlobalVariables.Statics.aryDefautlDeduction[rsDeductions.Get_Fields_Int32("DeductionNumber")].Priority = FCConvert.ToString(rsDeductions.Get_Fields_Int32("Priority"));
				rsDeductions.MoveNext();
			}
		}

		
	}
}
