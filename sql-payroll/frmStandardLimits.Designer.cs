//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmStandardLimits.
	/// </summary>
	partial class frmStandardLimits
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCComboBox cbo940;
		public fecherFoundation.FCComboBox cbo941;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtCSSFRate;
		public fecherFoundation.FCTextBox txtUPAFRate;
		public fecherFoundation.FCTextBox txtUnemploymentRate;
		public fecherFoundation.FCTextBox txtUnemploymentBase;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel lblSchoolRate;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtFutaBase;
		public fecherFoundation.FCTextBox txtFutaActual;
		public fecherFoundation.FCTextBox txtMaxCredit;
		public fecherFoundation.FCTextBox txtFutaRate;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cbo940 = new fecherFoundation.FCComboBox();
            this.cbo941 = new fecherFoundation.FCComboBox();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtCSSFRate = new fecherFoundation.FCTextBox();
            this.txtUPAFRate = new fecherFoundation.FCTextBox();
            this.txtUnemploymentRate = new fecherFoundation.FCTextBox();
            this.txtUnemploymentBase = new fecherFoundation.FCTextBox();
            this.Label20 = new fecherFoundation.FCLabel();
            this.lblSchoolRate = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtFutaBase = new fecherFoundation.FCTextBox();
            this.txtFutaActual = new fecherFoundation.FCTextBox();
            this.txtMaxCredit = new fecherFoundation.FCTextBox();
            this.txtFutaRate = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 448);
            this.BottomPanel.Size = new System.Drawing.Size(842, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(862, 628);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(862, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 28);
            this.HeaderText.Text = "Standard Limits / Rates";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.Frame5);
            this.Frame1.Controls.Add(this.Frame7);
            this.Frame1.Controls.Add(this.cmdDelete);
            this.Frame1.Controls.Add(this.cmdNew);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(844, 448);
            this.Frame1.TabIndex = 29;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.cbo940);
            this.Frame3.Controls.Add(this.cbo941);
            this.Frame3.Controls.Add(this.Label17);
            this.Frame3.Controls.Add(this.Label18);
            this.Frame3.Location = new System.Drawing.Point(30, 336);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(516, 90);
            this.Frame3.TabIndex = 6;
            this.Frame3.Text = "Reporting";
            // 
            // cbo940
            // 
            this.cbo940.BackColor = System.Drawing.SystemColors.Window;
            this.cbo940.Items.AddRange(new object[] {
            "Weekly",
            "Monthly",
            "Quarterly"});
            this.cbo940.Location = new System.Drawing.Point(376, 30);
            this.cbo940.Name = "cbo940";
            this.cbo940.TabIndex = 20;
            // 
            // cbo941
            // 
            this.cbo941.BackColor = System.Drawing.SystemColors.Window;
            this.cbo941.Items.AddRange(new object[] {
            "Weekly",
            "Monthly"});
            this.cbo941.Location = new System.Drawing.Point(123, 30);
            this.cbo941.Name = "cbo941";
            this.cbo941.TabIndex = 19;
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(20, 44);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(80, 21);
            this.Label17.TabIndex = 52;
            this.Label17.Text = "FTD ( 941 )";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(263, 44);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(68, 21);
            this.Label18.TabIndex = 51;
            this.Label18.Text = "FTD ( 940 )";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtCSSFRate);
            this.Frame5.Controls.Add(this.txtUPAFRate);
            this.Frame5.Controls.Add(this.txtUnemploymentRate);
            this.Frame5.Controls.Add(this.txtUnemploymentBase);
            this.Frame5.Controls.Add(this.Label20);
            this.Frame5.Controls.Add(this.lblSchoolRate);
            this.Frame5.Controls.Add(this.Label11);
            this.Frame5.Controls.Add(this.Label12);
            this.Frame5.Location = new System.Drawing.Point(30, 186);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(516, 140);
            this.Frame5.TabIndex = 5;
            this.Frame5.Text = "State Unemployment";
            // 
            // txtCSSFRate
            // 
            this.txtCSSFRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtCSSFRate.Location = new System.Drawing.Point(376, 30);
            this.txtCSSFRate.Name = "txtCSSFRate";
            this.txtCSSFRate.Size = new System.Drawing.Size(120, 40);
            this.txtCSSFRate.TabIndex = 17;
            // 
            // txtUPAFRate
            // 
            this.txtUPAFRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtUPAFRate.Location = new System.Drawing.Point(376, 80);
            this.txtUPAFRate.Name = "txtUPAFRate";
            this.txtUPAFRate.Size = new System.Drawing.Size(120, 40);
            this.txtUPAFRate.TabIndex = 18;
            // 
            // txtUnemploymentRate
            // 
            this.txtUnemploymentRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtUnemploymentRate.Location = new System.Drawing.Point(123, 30);
            this.txtUnemploymentRate.Name = "txtUnemploymentRate";
            this.txtUnemploymentRate.Size = new System.Drawing.Size(120, 40);
            this.txtUnemploymentRate.TabIndex = 15;
            this.txtUnemploymentRate.Enter += new System.EventHandler(this.txtUnemploymentRate_Enter);
            this.txtUnemploymentRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUnemploymentRate_KeyPress);
            // 
            // txtUnemploymentBase
            // 
            this.txtUnemploymentBase.BackColor = System.Drawing.SystemColors.Window;
            this.txtUnemploymentBase.Location = new System.Drawing.Point(123, 80);
            this.txtUnemploymentBase.Name = "txtUnemploymentBase";
            this.txtUnemploymentBase.Size = new System.Drawing.Size(120, 40);
            this.txtUnemploymentBase.TabIndex = 16;
            this.txtUnemploymentBase.Enter += new System.EventHandler(this.txtUnemploymentBase_Enter);
            this.txtUnemploymentBase.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUnemploymentBase_KeyPress);
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(263, 44);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(107, 21);
            this.Label20.TabIndex = 56;
            this.Label20.Text = "CSSF RATE (%)";
            // 
            // lblSchoolRate
            // 
            this.lblSchoolRate.Location = new System.Drawing.Point(263, 94);
            this.lblSchoolRate.Name = "lblSchoolRate";
            this.lblSchoolRate.Size = new System.Drawing.Size(107, 21);
            this.lblSchoolRate.TabIndex = 55;
            this.lblSchoolRate.Text = "UPAF RATE (%)";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 44);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(78, 21);
            this.Label11.TabIndex = 44;
            this.Label11.Text = "RATE (%)";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 94);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(58, 21);
            this.Label12.TabIndex = 43;
            this.Label12.Text = "BASE";
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtFutaBase);
            this.Frame7.Controls.Add(this.txtFutaActual);
            this.Frame7.Controls.Add(this.txtMaxCredit);
            this.Frame7.Controls.Add(this.txtFutaRate);
            this.Frame7.Controls.Add(this.Label14);
            this.Frame7.Controls.Add(this.Label13);
            this.Frame7.Controls.Add(this.Label10);
            this.Frame7.Controls.Add(this.Label9);
            this.Frame7.FormatCaption = false;
            this.Frame7.Location = new System.Drawing.Point(30, 36);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(516, 140);
            this.Frame7.TabIndex = 4;
            this.Frame7.Text = "FUTA";
            // 
            // txtFutaBase
            // 
            this.txtFutaBase.BackColor = System.Drawing.SystemColors.Window;
            this.txtFutaBase.Location = new System.Drawing.Point(376, 80);
            this.txtFutaBase.Name = "txtFutaBase";
            this.txtFutaBase.Size = new System.Drawing.Size(120, 40);
            this.txtFutaBase.TabIndex = 14;
            this.txtFutaBase.Enter += new System.EventHandler(this.txtFutaBase_Enter);
            this.txtFutaBase.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFutaBase_KeyPress);
            // 
            // txtFutaActual
            // 
            this.txtFutaActual.BackColor = System.Drawing.SystemColors.Window;
            this.txtFutaActual.Location = new System.Drawing.Point(376, 30);
            this.txtFutaActual.Name = "txtFutaActual";
            this.txtFutaActual.Size = new System.Drawing.Size(120, 40);
            this.txtFutaActual.TabIndex = 13;
            this.txtFutaActual.Enter += new System.EventHandler(this.txtFutaActual_Enter);
            this.txtFutaActual.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFutaActual_KeyPress);
            // 
            // txtMaxCredit
            // 
            this.txtMaxCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxCredit.Location = new System.Drawing.Point(123, 80);
            this.txtMaxCredit.Name = "txtMaxCredit";
            this.txtMaxCredit.Size = new System.Drawing.Size(120, 40);
            this.txtMaxCredit.TabIndex = 12;
            this.txtMaxCredit.Enter += new System.EventHandler(this.txtMaxCredit_Enter);
            this.txtMaxCredit.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMaxCredit_KeyPress);
            // 
            // txtFutaRate
            // 
            this.txtFutaRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtFutaRate.Location = new System.Drawing.Point(123, 30);
            this.txtFutaRate.Name = "txtFutaRate";
            this.txtFutaRate.Size = new System.Drawing.Size(120, 40);
            this.txtFutaRate.TabIndex = 11;
            this.txtFutaRate.Enter += new System.EventHandler(this.txtFutaRate_Enter);
            this.txtFutaRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFutaRate_KeyPress);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(263, 94);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(48, 21);
            this.Label14.TabIndex = 41;
            this.Label14.Text = "BASE";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(263, 44);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(68, 21);
            this.Label13.TabIndex = 40;
            this.Label13.Text = "ACTUAL";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 94);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(97, 21);
            this.Label10.TabIndex = 39;
            this.Label10.Text = "MAX CREDIT";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(60, 21);
            this.Label9.TabIndex = 38;
            this.Label9.Text = "RATE(%)";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = Wisej.Web.AnchorStyles.Top;
            this.cmdDelete.Location = new System.Drawing.Point(634, 6);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 26;
            this.cmdDelete.TabStop = false;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Visible = false;
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = Wisej.Web.AnchorStyles.Top;
            this.cmdNew.Location = new System.Drawing.Point(545, 6);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 25;
            this.cmdNew.TabStop = false;
            this.cmdNew.Text = "New";
            this.cmdNew.Visible = false;
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = 1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                 ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                       ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 5;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(362, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmStandardLimits
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(862, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmStandardLimits";
            this.Text = "Standard Limits/Rates";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmStandardLimits_Load);
            this.Activated += new System.EventHandler(this.frmStandardLimits_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmStandardLimits_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
    }
}