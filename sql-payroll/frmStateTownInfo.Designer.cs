//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmStateTownInfo.
	/// </summary>
	partial class frmStateTownInfo
	{
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCGrid StateGrid;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCGrid TownGrid;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCGrid CountyGrid;
		public fecherFoundation.FCLabel lblNotes;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.StateGrid = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.TownGrid = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.CountyGrid = new fecherFoundation.FCGrid();
            this.lblNotes = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StateGrid)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TownGrid)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CountyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 529);
            this.BottomPanel.Size = new System.Drawing.Size(644, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.lblNotes);
            this.ClientArea.Size = new System.Drawing.Size(644, 469);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(644, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(341, 30);
            this.HeaderText.Text = "Edit States / Towns / Counties";
            // 
            // SSTab1
            // 
            this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Location = new System.Drawing.Point(30, 57);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.ShowFocusRect = false;
            this.SSTab1.Size = new System.Drawing.Size(577, 404);
            this.SSTab1.TabIndex = 3;
            this.SSTab1.TabsPerRow = 0;
            this.SSTab1.TabStop = false;
            this.SSTab1.Text = "States";
            this.SSTab1.WordWrap = false;
            this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.StateGrid);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "States";
            // 
            // StateGrid
            // 
            this.StateGrid.AllowSelection = false;
            this.StateGrid.AllowUserToResizeColumns = false;
            this.StateGrid.AllowUserToResizeRows = false;
            this.StateGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.StateGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.StateGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.StateGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.StateGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.StateGrid.BackColorSel = System.Drawing.Color.Empty;
            this.StateGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.StateGrid.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.StateGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.StateGrid.ColumnHeadersHeight = 30;
            this.StateGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.StateGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.StateGrid.DragIcon = null;
            this.StateGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.StateGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.StateGrid.FrozenCols = 0;
            this.StateGrid.GridColor = System.Drawing.Color.Empty;
            this.StateGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.StateGrid.Location = new System.Drawing.Point(20, 20);
            this.StateGrid.Name = "StateGrid";
            this.StateGrid.OutlineCol = 0;
            this.StateGrid.ReadOnly = true;
            this.StateGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StateGrid.RowHeightMin = 0;
            this.StateGrid.Rows = 50;
            this.StateGrid.ScrollTipText = null;
            this.StateGrid.ShowColumnVisibilityMenu = false;
            this.StateGrid.Size = new System.Drawing.Size(535, 318);
            this.StateGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.StateGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.StateGrid.TabIndex = 0;
            this.StateGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.StateGrid_KeyPressEdit);
            this.StateGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(StateGrid_EditingControlShowing);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.TownGrid);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Text = "Towns";
            // 
            // TownGrid
            // 
            this.TownGrid.AllowSelection = false;
            this.TownGrid.AllowUserToResizeColumns = false;
            this.TownGrid.AllowUserToResizeRows = false;
            this.TownGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.TownGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.TownGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.TownGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.TownGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.TownGrid.BackColorSel = System.Drawing.Color.Empty;
            this.TownGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.TownGrid.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.TownGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TownGrid.ColumnHeadersHeight = 30;
            this.TownGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.TownGrid.DefaultCellStyle = dataGridViewCellStyle4;
            this.TownGrid.DragIcon = null;
            this.TownGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.TownGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.TownGrid.FrozenCols = 0;
            this.TownGrid.GridColor = System.Drawing.Color.Empty;
            this.TownGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.TownGrid.Location = new System.Drawing.Point(20, 20);
            this.TownGrid.Name = "TownGrid";
            this.TownGrid.OutlineCol = 0;
            this.TownGrid.ReadOnly = true;
            this.TownGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TownGrid.RowHeightMin = 0;
            this.TownGrid.Rows = 50;
            this.TownGrid.ScrollTipText = null;
            this.TownGrid.ShowColumnVisibilityMenu = false;
            this.TownGrid.Size = new System.Drawing.Size(535, 318);
            this.TownGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.TownGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.TownGrid.TabIndex = 1;
            this.TownGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.TownGrid_KeyPressEdit);
            this.TownGrid.ComboDropDown += new System.EventHandler(this.TownGrid_ComboDropDown);
            this.TownGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(TownGrid_EditingControlShowing);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.CountyGrid);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Text = "Counties";
            // 
            // CountyGrid
            // 
            this.CountyGrid.AllowSelection = false;
            this.CountyGrid.AllowUserToResizeColumns = false;
            this.CountyGrid.AllowUserToResizeRows = false;
            this.CountyGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.CountyGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.CountyGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.CountyGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.CountyGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.CountyGrid.BackColorSel = System.Drawing.Color.Empty;
            this.CountyGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.CountyGrid.Cols = 10;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.CountyGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.CountyGrid.ColumnHeadersHeight = 30;
            this.CountyGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.CountyGrid.DefaultCellStyle = dataGridViewCellStyle6;
            this.CountyGrid.DragIcon = null;
            this.CountyGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.CountyGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.CountyGrid.FrozenCols = 0;
            this.CountyGrid.GridColor = System.Drawing.Color.Empty;
            this.CountyGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.CountyGrid.Location = new System.Drawing.Point(20, 20);
            this.CountyGrid.Name = "CountyGrid";
            this.CountyGrid.OutlineCol = 0;
            this.CountyGrid.ReadOnly = true;
            this.CountyGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.CountyGrid.RowHeightMin = 0;
            this.CountyGrid.Rows = 50;
            this.CountyGrid.ScrollTipText = null;
            this.CountyGrid.ShowColumnVisibilityMenu = false;
            this.CountyGrid.Size = new System.Drawing.Size(535, 318);
            this.CountyGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.CountyGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.CountyGrid.TabIndex = 2;
            this.CountyGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.CountyGrid_KeyPressEdit);
            this.CountyGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(CountyGrid_EditingControlShowing);
            // 
            // lblNotes
            // 
            this.lblNotes.Location = new System.Drawing.Point(30, 30);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(576, 16);
            this.lblNotes.TabIndex = 4;
            this.lblNotes.Text = "MENU OPTIONS ARE ONLY FUNCTIONAL WITH THE ACTIVE TAB";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 2;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                        ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit          ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 5;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(502, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(48, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(556, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(60, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(276, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(82, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmStateTownInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(644, 637);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmStateTownInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit States / Towns / Counties";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmStateTownInfo_Load);
            this.Activated += new System.EventHandler(this.frmStateTownInfo_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmStateTownInfo_KeyPress);
            this.Resize += new System.EventHandler(this.frmStateTownInfo_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StateGrid)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TownGrid)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CountyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdSave;
	}
}