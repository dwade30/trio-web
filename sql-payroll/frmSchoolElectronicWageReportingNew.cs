//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmSchoolElectronicWageReportingNew : BaseForm
	{
		public frmSchoolElectronicWageReportingNew()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_9, 9);
			this.Label1.AddControlArrayElement(Label1_10, 10);
			this.Label1.AddControlArrayElement(Label1_11, 11);
			this.Label1.AddControlArrayElement(Label1_12, 12);
			this.Label1.AddControlArrayElement(Label1_13, 13);
			this.Label1.AddControlArrayElement(Label1_14, 14);
			this.Label1.AddControlArrayElement(Label1_15, 15);
			this.cmbEndDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_0, 0);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_1, 1);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_2, 2);
			this.cmbStartDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_0, 0);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_1, 1);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_2, 2);
			this.txtStartDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtStartDate.AddControlArrayElement(txtStartDate_0, 0);
			this.txtStartDate.AddControlArrayElement(txtStartDate_1, 1);
			this.txtStartDate.AddControlArrayElement(txtStartDate_2, 2);
			this.txtEndDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtEndDate.AddControlArrayElement(txtEndDate_0, 0);
			this.txtEndDate.AddControlArrayElement(txtEndDate_1, 1);
			this.txtEndDate.AddControlArrayElement(txtEndDate_2, 2);
			this.lblTo = new System.Collections.Generic.List<FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblFrom = new System.Collections.Generic.List<FCLabel>();
			this.lblFrom.AddControlArrayElement(lblFrom_0, 0);
			this.lblFrom.AddControlArrayElement(lblFrom_1, 1);
			this.lblFrom.AddControlArrayElement(lblFrom_2, 2);
			this.lblMonth = new System.Collections.Generic.List<FCLabel>();
			this.lblMonth.AddControlArrayElement(lblMonth_0, 0);
			this.lblMonth.AddControlArrayElement(lblMonth_1, 1);
			this.lblMonth.AddControlArrayElement(lblMonth_2, 2);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSchoolElectronicWageReportingNew InstancePtr
		{
			get
			{
				return (frmSchoolElectronicWageReportingNew)Sys.GetInstance(typeof(frmSchoolElectronicWageReportingNew));
			}
		}

		protected frmSchoolElectronicWageReportingNew _InstancePtr = null;
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		int lngYearCovered;
		int intQuarterCovered;
		int intLastMonthInQuarter;
		clsDRWrapper clsEmployees = new clsDRWrapper();
		clsDRWrapper clsPayments = new clsDRWrapper();
		double dblCredit;
		double dblTotalExcess;
		double dblTotalWages;
		double dblTotalWH;
		double dblTotalGrossWages;
		double dblLimit;
		double dblRate;
		clsDRWrapper clsMonth1 = new clsDRWrapper();
		clsDRWrapper clsMonth2 = new clsDRWrapper();
		clsDRWrapper clsMonth3 = new clsDRWrapper();
		clsDRWrapper clsSchoolMonth1 = new clsDRWrapper();
		clsDRWrapper clsSchoolMonth2 = new clsDRWrapper();
		clsDRWrapper clsSchoolMonth3 = new clsDRWrapper();
		int lngSchoolFemsM1;
		int lngSchoolFemsM2;
		int lngSchoolFemsM3;
		int lngSchoolM1;
		int lngSchoolM2;
		int lngSchoolM3;
		double dblSchoolRemitted;
		double dblSchoolCredit;
		double dblTotalSchoolGrossWages;
		double dblTotalSchoolWages;
		double dblTotalSchoolExcess;
		int lngFemsM1;
		int lngFemsM2;
		int lngFemsM3;
		int lngM1;
		int lngM2;
		int lngM3;
		double dblRemitted;
		string strSequence;
		bool boolReportNotElectronic;
		int lngTotWithholdableEmployees;
		string strLastEmployee;
		string strSchEmployeeNumber = "";
		string strLastSchSSN = "";
		double dblCSSFRate;
		double dblSchoolRate;

		private void frmSchoolElectronicWageReportingNew_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmSchoolElectronicWageReportingNew_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		public void Init(ref int intQCovered, ref int lngYCovered, string strSeq = "", bool boolNotElectronic = false)
		{
			// if boolnotelectronic then we are doing the printout for the state, not the file
			// THIS
			boolReportNotElectronic = boolNotElectronic;
			strSequence = strSeq;
			lngYearCovered = lngYCovered;
			intQuarterCovered = intQCovered;
			switch (intQuarterCovered)
			{
				case 1:
					{
						intLastMonthInQuarter = 3;
						break;
					}
				case 2:
					{
						intLastMonthInQuarter = 6;
						break;
					}
				case 3:
					{
						intLastMonthInQuarter = 9;
						break;
					}
				case 4:
					{
						intLastMonthInQuarter = 12;
						break;
					}
			}
			//end switch
			// get payment info
			frmC1Payments.InstancePtr.Init(intLastMonthInQuarter, lngYearCovered, strSeq);
			if (modGlobalVariables.Statics.gboolCancelSelected)
			{
			}
			else
			{
				if (boolReportNotElectronic)
				{
					framFile.Visible = false;
				}
				this.Show(FCForm.FormShowEnum.Modal);
				// Me.Show , MDIParent
			}
		}

		private void frmSchoolElectronicWageReportingNew_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSchoolElectronicWageReportingNew properties;
			//frmSchoolElectronicWageReportingNew.ScaleWidth	= 9225;
			//frmSchoolElectronicWageReportingNew.ScaleHeight	= 8325;
			//frmSchoolElectronicWageReportingNew.LinkTopic	= "Form1";
			//frmSchoolElectronicWageReportingNew.LockControls	= -1  'True;
			//End Unmaped Properties
			int intTemp;
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, true);
			if (boolReportNotElectronic)
			{
				this.Text = "Wage Reporting";
				mnuCopy.Enabled = false;
			}
			// lngYearcovered = Year(Date)
			// Select Case Month(Date)
			// Case 1 To 3
			// intQuarterCovered = 1
			// Case 4 To 6
			// intQuarterCovered = 2
			// Case 7 To 9
			// intQuarterCovered = 3
			// Case 10 To 12
			// intQuarterCovered = 4
			// End Select
			// 
			// Call frmSelectDateInfo.Init("Select quarter and year for report", lngYearcovered, intQuarterCovered)
			// Select Case intQuarterCovered
			// Case 1
			// intLastMonthInQuarter = 3
			// Case 2
			// intLastMonthInQuarter = 6
			// Case 3
			// intLastMonthInQuarter = 9
			// Case 4
			// intLastMonthInQuarter = 12
			// End Select
			// 
			// get payment info
			// Call frmC1Payments.Init(intLastMonthInQuarter, lngYearcovered)
			FillDateCombos();
			LoadInfo();
			ShowInfo();
		}

		private void mnuBLS3020_Click(object sender, System.EventArgs e)
		{
			// frmMultipleWorksitSetup.Show vbModal, MDIParent
		}

		private void mnuCopy_Click(object sender, System.EventArgs e)
		{
			txtFilename.Text = Strings.Replace(txtFilename.Text, "/", "", 1, -1, CompareConstants.vbTextCompare);
			CopyEWRFile2(fecherFoundation.Strings.Trim(txtFilename.Text), false);
		}

		private void mnuCopySchool_Click(object sender, System.EventArgs e)
		{
			txtFilename.Text = Strings.Replace(txtFilename.Text, "/", "", 1, -1, CompareConstants.vbTextCompare);
			CopyEWRFile2(fecherFoundation.Strings.Trim(txtFilename.Text), true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			// gboolCancelSelected = True
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			clsLoad.OpenRecordset("select SCHOOLUNEMPLOYMENTRATE,unemploymentrate,unemploymentbase,cssfrate from tblStandardLimits", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				dblRate = Conversion.Val(clsLoad.Get_Fields("unemploymentrate")) / 100;
				dblLimit = Conversion.Val(clsLoad.Get_Fields("unemploymentbase"));
				dblSchoolRate = Conversion.Val(clsLoad.Get_Fields("schoolunemploymentrate")) / 100;
				dblCSSFRate = Conversion.Val(clsLoad.Get_Fields("cssfrate")) / 100;
			}
			else
			{
				dblRate = 0;
				dblLimit = 7000;
				dblCSSFRate = 0;
				dblSchoolRate = 0;
			}
			// If UCase(MuniName) = "JAY" Then
			// dblSchoolRate = 0
			// dblCSSFRate = 0
			// End If
			strSQL = "select * from tblemployerinfo";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
				EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
				EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
				EWRRecord.SchoolMRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields_String("SchoolMRSWithholdingID"));
				EWRRecord.SchoolStateUCAccount = FCConvert.ToString(clsLoad.Get_Fields("schoolstateucaccount"));
				EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
				EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
				EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
				EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				EWRRecord.ProcessorLicenseCode = FCConvert.ToString(clsLoad.Get_Fields_String("ProcessorLicenseCode"));
			}
			else
			{
				EWRRecord.EmployerAddress = "";
				EWRRecord.EmployerCity = "";
				EWRRecord.EmployerName = "";
				EWRRecord.EmployerState = "ME";
				EWRRecord.EmployerStateCode = 23;
				EWRRecord.EmployerZip = "";
				EWRRecord.EmployerZip4 = "";
				EWRRecord.FederalEmployerID = "";
				EWRRecord.MRSWithholdingID = "";
				EWRRecord.SchoolMRSWithholdingID = "";
				EWRRecord.StateUCAccount = "";
				EWRRecord.SchoolStateUCAccount = "";
				EWRRecord.TransmitterExt = "";
				EWRRecord.TransmitterPhone = "0000000000";
				EWRRecord.TransmitterTitle = "";
			}
			txtFilename.Text = "rtnwage.txt";
			if (!clsLoad.EndOfFile())
			{
				txtSeasonalCode.Text = FCConvert.ToString(clsLoad.Get_Fields("seasonalcode"));
				if (Information.IsDate(clsLoad.Get_Fields("seasonalfrom")))
				{
					T2KSeasonalFrom.Text = Strings.Format(clsLoad.Get_Fields("seasonalfrom"), "MM/dd/yyyy");
				}
				if (Information.IsDate(clsLoad.Get_Fields("seasonalTO")))
				{
					T2KSeasonalTo.Text = Strings.Format(clsLoad.Get_Fields("seasonalTo"), "MM/dd/yyyy");
				}
			}
		}

		private void ShowInfo()
		{
			string strTemp = "";
			object strPhone = "";
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtTemp;
			// try to fill in initial range boxes
			// get the paydates found in tblcheckdetail that cover the 12th for each month in quarter
			for (x = 0; x <= 2; x++)
			{
				strSQL = "select * from tblemployerinfo";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "startdate")) && Convert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "startdate")).ToOADate() != 0)
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "startdate");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								// txtStartDate(x).Text = Format(dtTemp, "MM/dd/yyyy")
								cmbStartDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "EndDate")) && Convert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "enddate")).ToOADate() != 0)
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "enddate");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								// txtEndDate(x).Text = Format(dtTemp, "MM/dd/yyyy")
								cmbEndDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
				}
				// If Not (IsDate(txtStartDate(x).Text)) Or Not (IsDate(txtEndDate(x).Text)) Then
				// strSQL = "Select paydate from tblcheckdetail where month(paydate) = " & intLastMonthInQuarter + x - 2 & " and year(paydate) = " & lngYearCovered & " group by paydate order by paydate"
				// Call clsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// Do While Not clsLoad.EndOfFile
				// If Day(clsLoad.Fields("paydate")) < 12 Then
				// txtStartDate(x).Text = Format(intLastMonthInQuarter + x - 2, "00") & "/" & Format(Day(clsLoad.Fields("paydate")), "00") & "/" & lngYearCovered
				// ElseIf Day(clsLoad.Fields("paydate")) > 12 Then
				// txtEndDate(x).Text = Format(intLastMonthInQuarter + x - 2, "00") & "/" & Format(Day(clsLoad.Fields("paydate")), "00") & "/" & lngYearCovered
				// Exit Do
				// Else
				// it is the 12th
				// txtStartDate(x).Text = Format(intLastMonthInQuarter + x - 2, "00") & "/12/" & lngYearCovered
				// txtEndDate(x).Text = txtStartDate(x).Text
				// Exit Do
				// End If
				// clsLoad.MoveNext
				// Loop
				// Else
				// txtStartDate(x).Text = Format(intLastMonthInQuarter + x - 2, "00") & "/12/" & lngYearCovered
				// txtEndDate(x).Text = Format(intLastMonthInQuarter + x - 2, "00") & "/12/" & lngYearCovered
				// End If
				// End If
			}
			// x
			txtCity.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerCity);
			txtEmployerName.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerName);
			txtExtension.Text = EWRRecord.TransmitterExt;
			txtFederalID.Text = EWRRecord.FederalEmployerID;
			txtMRSID.Text = EWRRecord.MRSWithholdingID;
			txtSchoolMRSId.Text = EWRRecord.SchoolMRSWithholdingID;
			txtState.Text = EWRRecord.EmployerState;
			txtStreetAddress.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerAddress);
			txttitle.Text = fecherFoundation.Strings.Trim(EWRRecord.TransmitterTitle);
			txtUCAccount.Text = EWRRecord.StateUCAccount;
			txtSchoolUCAccount.Text = EWRRecord.SchoolStateUCAccount;
			txtZip.Text = EWRRecord.EmployerZip;
			txtZip4.Text = EWRRecord.EmployerZip4;
			strPhone = EWRRecord.TransmitterPhone;
			strPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strPhone), 10));
			txtPhone.Text = "(" + Strings.Mid(strPhone.ToStringES(), 1, 3) + ")" + Strings.Mid(strPhone.ToStringES(), 4, 3) + "-" + Strings.Mid(strPhone.ToStringES(), 7);
			txtProcessorLicenseCode.Text = EWRRecord.ProcessorLicenseCode;
		}

		private bool CheckRange()
		{
			bool CheckRange = false;
			int x;
			CheckRange = false;
			// For x = 0 To 2
			// If Not IsDate(txtStartDate(x).Text) Then
			// MsgBox "You must enter a valid date in each box", vbExclamation, "Invalid Date"
			// txtStartDate(x).SetFocus
			// Exit Function
			// End If
			// If Not IsDate(txtEndDate(x).Text) Then
			// MsgBox "You must enter a valid date in each box", vbExclamation, "Invalid Date"
			// txtEndDate(x).SetFocus
			// Exit Function
			// End If
			// If Day(txtStartDate(x).Text) > 12 Then
			// MsgBox "You must include the 12th day of the month in your date range", vbExclamation, "Invalid Range"
			// txtStartDate(x).SetFocus
			// Exit Function
			// End If
			// If Day(txtEndDate(x).Text) < 12 Then
			// MsgBox "You must include the 12th day of the month in your date range", vbExclamation, "Invalid Range"
			// txtEndDate(x).SetFocus
			// Exit Function
			// End If
			// Next x
			for (x = 0; x <= 2; x++)
			{
				if (!Information.IsDate(cmbStartDate[x].Text))
				{
					MessageBox.Show("You must enter a valid start date for each month", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cmbStartDate[x].Focus();
					return CheckRange;
				}
				// If Not IsDate(cmbEndDate(x).Text) Then
				// MsgBox "You must enter a valid date in each box", vbExclamation, "Invalid Date"
				// cmbEndDate(x).SetFocus
				// Exit Function
				// End If
				// If Day(cmbStartDate(x).Text) > 12 Then
				// MsgBox "You must include the 12th day of the month in your date range", vbExclamation, "Invalid Range"
				// cmbStartDate(x).SetFocus
				// Exit Function
				// End If
				// If Day(cmbEndDate(x).Text) < 12 Then
				// MsgBox "You must include the 12th day of the month in your date range", vbExclamation, "Invalid Range"
				// cmbEndDate(x).SetFocus
				// Exit Function
				// End If
			}
			// x
			CheckRange = true;
			return CheckRange;
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			dblCredit = Conversion.Val(txtCredit.Text);
			dblRemitted = Conversion.Val(txtRemitted.Text);
			if (!CheckRange())
			{
				return;
			}
			if (!SaveInfo())
			{
				return;
			}
			rptC1Laser.InstancePtr.Init(dblRate, dblLimit, ref intQuarterCovered, ref lngYearCovered, ref strSequence, false, dblCSSFRate);
		}

		private void mnuPrintSchoolC1Override_Click(object sender, System.EventArgs e)
		{
			object dblOVRate;
			double dblTemp;
			dblTemp = Conversion.Val(modRegistry.GetRegistryKey("UCOverrideRate", "PY", "0"));
			dblOVRate = dblTemp;
			if (!frmInput.InstancePtr.Init(ref dblOVRate, "UC Rate", "Enter the percentage rate", 1440, false, modGlobalConstants.InputDTypes.idtNumber, dblTemp.ToString()))
			{
				return;
			}
			modRegistry.SaveRegistryKey("UCOverrideRate", FCConvert.ToString(dblOVRate), "PY");
			rptElectronicC1.InstancePtr.Init("rtnwagesch.txt", this.Modal, true, FCConvert.ToDouble(dblOVRate));
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			rptC1Laser.InstancePtr.Init(dblRate, dblLimit, ref intQuarterCovered, ref lngYearCovered, ref strSequence, false, dblCSSFRate, true);
		}

		private void mnuReprintNonSchool_Click(object sender, System.EventArgs e)
		{
			rptElectronicC1.InstancePtr.Init("rtnwage.txt", this.Modal);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			txtFilename.Text = Strings.Replace(txtFilename.Text, "/", "", 1, -1, CompareConstants.vbTextCompare);
			dblCredit = Conversion.Val(txtCredit.Text);
			dblRemitted = Conversion.Val(txtRemitted.Text);
			if (!CheckRange())
			{
				return;
			}
			if (!SaveInfo())
			{
				return;
			}
			if (!boolReportNotElectronic)
			{
				// *********************************************************************************
				// *********************************************************************************
				// MATTHEW 10/05/2004
				// THIS IS HERE JUST FOR THE MULTIPLE WORKSITE REPORT THAT IS NEEDED BY THE
				// STATE FOR RESEARCH PURPOSES.
				string strUnEmpQuery1 = "";
				string strUnEmpQuery2 = "";
				string strRange = "";
				string strQuery1 = "";
				string strQuery2 = "";
				string strQuery3 = "";
				string strQuery4 = "";
				DateTime dtStartDate = DateTime.FromOADate(0);
				DateTime dtEndDate = DateTime.FromOADate(0);
				string strSQL = "";
				// strSQL = "select employeenumber from tblcheckdetail where paydate between '" & txtStartDate(0).Text & "' and #" & txtEndDate(0).Text & "# group by employeenumber"
				// corey 11/14/2005
				// strSQL = "select employeenumber from tblcheckdetail where paydate between '" & cmbStartDate(0).Text & "'and '" & cmbEndDate(0).Text & "'and checkvoid = 0 group by employeenumber"
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between '" + cmbStartDate[0].Text + "'and '" + cmbEndDate[0].Text + "'group by employeenumber";
				clsMonth1.OpenRecordset(strSQL, "twpy0000.vb1");
				// strSQL = "select employeenumber from tblcheckdetail where paydate between #" & txtStartDate(1).Text & "# and #" & txtEndDate(1).Text & "# group by employeenumber"
				// corey 11/14/2005
				// strSQL = "select employeenumber from tblcheckdetail where paydate between '" & cmbStartDate(1).Text & "'and '" & cmbEndDate(1).Text & "'and checkvoid = 0 group by employeenumber"
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') AND paydate between '" + cmbStartDate[1].Text + "'and '" + cmbEndDate[1].Text + "'group by employeenumber";
				clsMonth2.OpenRecordset(strSQL, "twpy0000.vb1");
				// strSQL = "select employeenumber from tblcheckdetail where paydate between #" & txtStartDate(2).Text & "# and #" & txtEndDate(2).Text & "# group by employeenumber"
				// corey 11/14/2005
				// strSQL = "select employeenumber from tblcheckdetail where paydate between '" & cmbStartDate(2).Text & "'and '" & cmbEndDate(2).Text & "'and checkvoid = 0 group by employeenumber"
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between '" + cmbStartDate[2].Text + "'and '" + cmbEndDate[2].Text + "'group by employeenumber";
				clsMonth3.OpenRecordset(strSQL, "twpy0000.vb1");
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarterCovered, lngYearCovered);
				// If intQuarterCovered = 1 Then
				// ytd and qtd are same
				// strSQL = "select employeenumber,sum(distGrossPay) as qtdGrossPay,sum(distGrossPay) as YTDGrossPay from tblCheckDetail where Paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 AND (distu <> 'N' and distu <> '') group by employeenumber having ( sum(convert(int, isnull(distgrosspay, 0))) > 0 )"
				// strUnEmpQuery1 = "(" & strSQL & ") as UnEmpQuery1 "
				// strSQL = "select employeenumber as employeenum,sum(grosspay) as qtdTotalGross,sum(grosspay) as YTDTotalGross,sum(StateTaxWH) as qtdStateWH from tblCheckDetail where PayDate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 group by employeenumber having (sum(val(grosspay & '')) > 0 or sum(convert(int, isnull(statetaxwh, 0))) > 0)"
				// strUnEmpQuery2 = "(" & strSQL & ") as UnEmpQuery2 "
				// strQuery1 = "(select * from " & strUnEmpQuery2 & " left join " & strUnEmpQuery1 & " on (UnEmpquery1.employeenumber = UnEmpquery2.employeenum)) as C1Query1"
				// strQuery2 = "(select * from " & strQuery1 & " left join (select employeenumber as enum,unemployment as seasonal from tblMiscUpdate) as C1Query2 on (c1query2.enum = c1query1.employeenum) where (qtdgrosspay > 0 or qtdstatewh > 0) ) as C1Query3"
				// strSQL = "select *,TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " & strQuery2 & " on (tblemployeemaster.employeenumber = c1query3.employeenum)   " & strRange & " order by tblemployeemaster.lastname,tblemployeemaster.firstname"
				// Else
				// YTD is not same as qtd
				strSQL = "select employeenumber as enum,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
				strUnEmpQuery1 = "(" + strSQL + ") as UnEmpQuery1 ";
				strSQL = "select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where paydate between '" + "1/1/" + FCConvert.ToString(lngYearCovered) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber ";
				strUnEmpQuery2 = "(" + strSQL + ") as UnEmpQuery2 ";
				strQuery1 = "(Select * from " + strUnEmpQuery2 + " left join " + strUnEmpQuery1 + " on (unempquery1.enum = unempquery2.employeenum)) as C1Query1";
				// strQuery2 = "(select employeenumber,sum(grosspay) as qtdTotalGross,sum(stateTaxWH) as qtdStateWH from tblcheckdetail where paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 group by employeenumber having (sum(grosspay) > 0 or sum(statetaxwh) > 0) ) as C1Query2 "
				strQuery2 = "(select employeenumber,sum(statetaxgross) as qtdTotalGross,sum(stateTaxWH) as qtdStateWH from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber having (sum(grosspay) > 0 or sum(statetaxwh) > 0) ) as C1Query2 ";
				strQuery3 = "(select * from " + strQuery2 + " left join " + strQuery1 + " on (c1query1.employeeNUM = c1query2.employeenumBER) WHERE (qtdgrosspay > 0 or qtdstatewh > 0)) as C1Query3";
				strQuery4 = "(select * from " + strQuery3 + " left join (select employeenumber as enum,unemployment as seasonal from tblMiscUpdate) as C1Query4 on (c1query4.enum = c1query3.employeenumber)) as C1Query5 ";
				strSQL = "select *,TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery4 + " on (tblemployeemaster.employeenumber = c1query5.employeenumber)   " + strRange + "  order by tblemployeemaster.lastname,tblemployeemaster.firstname";
				// End If
				clsDRWrapper rsUnemployment = new clsDRWrapper();
				rsUnemployment.OpenRecordset("Select * from tblMiscUpdate", "TWPY0000.vb1");
				// Call SetBLS3020Data(strSQL, Month(cmbStartDate(0).Text), clsMonth1, clsMonth2, clsMonth3, rsUnemployment)
				// *********************************************************************************
				// *********************************************************************************
				if (!CreateEWRFile())
				{
					return;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//App.DoEvents();
				MessageBox.Show("File(s) created", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// If Not CopyEWRFile(Trim(txtFilename.Text), False) Then
				// Exit Sub
				// End If
				// If Not CopyEWRFile(Trim(txtFilename.Text), True) Then
				// Exit Sub
				// End If
				rptElectronicC1.InstancePtr.Init("rtnwage.txt", true);
				rptElectronicC1.InstancePtr.Init("rtnwagesch.txt", true);
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalRoutines.Statics.gstrReportID)) == "NONE")
				{
				}
				else
				{
					rptBLS.InstancePtr.Init(ref intQuarterCovered, ref lngYearCovered, this.Modal, strSequence, true);
				}
			}
			else
			{
				rptC1Laser.InstancePtr.Init(dblRate, dblLimit, ref intQuarterCovered, ref lngYearCovered, ref strSequence, false, dblCSSFRate);
				mnuExit_Click();
			}
		}

		private void ReprintSchoolSummary_Click(object sender, System.EventArgs e)
		{
			rptElectronicC1.InstancePtr.Init("rtnwagesch.txt", this.Modal);
		}

		private void txtExtension_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFilename_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Decimal)
			{
				KeyCode = (Keys)0;
			}
		}

		private void txtMRSID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtUCAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: KeyAscii As int	OnWrite(Keys)
		private bool KeyIsNumber(int KeyAscii)
		{
			bool KeyIsNumber = false;
			KeyIsNumber = false;
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 27 || KeyAscii == 127 || KeyAscii == 8))
			{
				KeyIsNumber = true;
			}
			return KeyIsNumber;
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			object strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				if (fecherFoundation.Strings.Trim(txtFederalID.Text).Length != modCoreysSweeterCode.EWRFederalIDLen)
				{
					MessageBox.Show("The federal ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRFederalIDLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFederalID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtMRSID.Text).Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
				{
					MessageBox.Show("The MRS Withholding Account must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRMRSWithholdingLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtMRSID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtSchoolMRSId.Text).Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
				{
					MessageBox.Show("The MRS Withholding Account for school must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRMRSWithholdingLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtSchoolMRSId.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtUCAccount.Text).Length != modCoreysSweeterCode.EWRStateUCLen)
				{
					MessageBox.Show("The State UC ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRStateUCLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtUCAccount.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtSchoolUCAccount.Text).Length != modCoreysSweeterCode.EWRStateUCLen)
				{
					MessageBox.Show("The State UC ID for school must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRStateUCLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtSchoolUCAccount.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtEmployerName.Text) == string.Empty)
				{
					MessageBox.Show("You must provide an employer name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerName.Focus();
					return SaveInfo;
				}
				// If Trim(txtProcessorLicenseCode.Text) = vbNullString Then
				// MsgBox "You must provide a Processor License Code", vbExclamation, "No License Code"
				// txtProcessorLicenseCode.SetFocus
				// Exit Function
				// End If
				EWRRecord.EmployerAddress = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtStreetAddress.Text), modCoreysSweeterCode.EWRReturnAddressLen));
				EWRRecord.EmployerCity = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtCity.Text), modCoreysSweeterCode.EWRReturnCityLen));
				EWRRecord.EmployerName = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtEmployerName.Text), modCoreysSweeterCode.EWRReturnNameLen));
				EWRRecord.EmployerState = Strings.Mid(fecherFoundation.Strings.Trim(txtState.Text), 1, 2);
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = fecherFoundation.Strings.Trim(txtZip.Text);
				EWRRecord.EmployerZip4 = fecherFoundation.Strings.Trim(txtZip4.Text);
				EWRRecord.FederalEmployerID = txtFederalID.Text;
				EWRRecord.MRSWithholdingID = txtMRSID.Text;
				EWRRecord.SchoolMRSWithholdingID = txtSchoolMRSId.Text;
				EWRRecord.StateUCAccount = txtUCAccount.Text;
				EWRRecord.SchoolStateUCAccount = txtSchoolUCAccount.Text;
				EWRRecord.TransmitterExt = txtExtension.Text;
				strTemp = "";
				for (x = 1; x <= (txtPhone.Text.Length); x++)
				{
					if (Information.IsNumeric(Strings.Mid(txtPhone.Text, x, 1)))
					{
						strTemp += Strings.Mid(txtPhone.Text, x, 1);
					}
				}
				// x
				EWRRecord.TransmitterPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strTemp), 10));
				EWRRecord.TransmitterTitle = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txttitle.Text), modCoreysSweeterCode.EWRTransmitterTitleLen));
				EWRRecord.ProcessorLicenseCode = txtProcessorLicenseCode.Text;
				// bug id 5532 matthew 11/01/05
				// .EmployerAddress = Mid(Trim(txtStreetAddress.Text), 1, EWRReturnAddressLen)
				// .EmployerCity = Mid(Trim(txtCity.Text), 1, EWRReturnCityLen)
				// .EmployerName = Mid(Trim(txtEmployerName.Text), 1, EWRReturnNameLen)
				// .EmployerState = Mid(Trim(txtState.Text), 1, 2)
				// .EmployerStateCode = GetStateCode(.EmployerState)
				// .EmployerZip = Trim(txtZip.Text)
				// .EmployerZip4 = Trim(txtZip4.Text)
				// .FederalEmployerID = txtFederalID.Text
				// .MRSWithholdingID = txtMRSID.Text
				// .StateUCAccount = txtUCAccount.Text
				// .TransmitterExt = txtExtension.Text
				// strTemp = ""
				// For X = 1 To Len(txtPhone.Text)
				// If IsNumeric(Mid(txtPhone.Text, X, 1)) Then
				// strTemp = strTemp & Mid(txtPhone.Text, X, 1)
				// End If
				// Next X
				// .TransmitterPhone = PadToString(strTemp, 10)
				// .TransmitterTitle = Trim(txtTitle.Text)
				modCoreysSweeterCode.Statics.EWRWageTotals.M1Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[0].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M2Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[1].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M3Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[2].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M1End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[0].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M2End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[1].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M3End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[2].Text, "MM/dd/yyyy"));
				clsSave.OpenRecordset("select * from tblemployerinfo", "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("employername", EWRRecord.EmployerName);
				clsSave.Set_Fields("Address1", EWRRecord.EmployerAddress);
				clsSave.Set_Fields("City", EWRRecord.EmployerCity);
				clsSave.Set_Fields("State", EWRRecord.EmployerState);
				clsSave.Set_Fields("zip", EWRRecord.EmployerZip);
				clsSave.Set_Fields("zip4", EWRRecord.EmployerZip4);
				clsSave.Set_Fields("TransmitterTitle", EWRRecord.TransmitterTitle);
				clsSave.Set_Fields("transmitterextension", EWRRecord.TransmitterExt);
				clsSave.Set_Fields("transmitterphone", EWRRecord.TransmitterPhone);
				clsSave.Set_Fields("FederalEmployerID", EWRRecord.FederalEmployerID);
				clsSave.Set_Fields("StateUCAccount", EWRRecord.StateUCAccount);
				clsSave.Set_Fields("SchoolStateUCAccount", EWRRecord.SchoolStateUCAccount);
				clsSave.Set_Fields("MRSWithholdingID", EWRRecord.MRSWithholdingID);
				clsSave.Set_Fields("SchoolMRSWithholdingID", EWRRecord.SchoolMRSWithholdingID);
				clsSave.Set_Fields("ProcessorLicenseCode", EWRRecord.ProcessorLicenseCode);
				if (txtSeasonalCode.Text != string.Empty)
				{
					clsSave.Set_Fields("seasonalcode", FCConvert.ToString(Conversion.Val(txtSeasonalCode.Text)));
				}
				else
				{
					clsSave.Set_Fields("seasonalcode", null);
				}
				if (Information.IsDate(T2KSeasonalFrom.Text))
				{
					clsSave.Set_Fields("SeasonalFrom", Strings.Format(T2KSeasonalFrom.Text, "MM/dd/yy"));
				}
				else
				{
					clsSave.Set_Fields("SeasonalFrom", "");
				}
				if (Information.IsDate(T2KSeasonalTo.Text))
				{
					clsSave.Set_Fields("SeasonalTo", Strings.Format(T2KSeasonalTo.Text, "MM/dd/yy"));
				}
				else
				{
					clsSave.Set_Fields("seasonalto", "");
				}
				// If IsDate(txtStartDate(0).Text) Then
				// clsSave.Fields("Month1StartDate") = txtStartDate(0).Text
				// End If
				// If IsDate(txtStartDate(1).Text) Then
				// clsSave.Fields("Month2startdate") = txtStartDate(1).Text
				// End If
				// If IsDate(txtStartDate(2).Text) Then
				// clsSave.Fields("Month3startdate") = txtStartDate(2).Text
				// End If
				// If IsDate(txtEndDate(0).Text) Then
				// clsSave.Fields("Month1EndDate") = txtEndDate(0).Text
				// End If
				// If IsDate(txtEndDate(1).Text) Then
				// clsSave.Fields("Month2enddate") = txtEndDate(1).Text
				// End If
				// If IsDate(txtEndDate(2).Text) Then
				// clsSave.Fields("month3enddate") = txtEndDate(2).Text
				// End If
				if (Information.IsDate(cmbStartDate[0].Text))
				{
					clsSave.Set_Fields("Month1StartDate", cmbStartDate[0].Text);
				}
				if (Information.IsDate(cmbStartDate[1].Text))
				{
					clsSave.Set_Fields("Month2startdate", cmbStartDate[1].Text);
				}
				if (Information.IsDate(cmbStartDate[2].Text))
				{
					clsSave.Set_Fields("Month3startdate", cmbStartDate[2].Text);
				}
				if (Information.IsDate(cmbEndDate[0].Text))
				{
					clsSave.Set_Fields("Month1EndDate", cmbEndDate[0].Text);
				}
				else
				{
					clsSave.Set_Fields("month1enddate", cmbStartDate[0].Text);
					cmbEndDate[0].Text = cmbStartDate[0].Text;
				}
				if (Information.IsDate(cmbEndDate[1].Text))
				{
					clsSave.Set_Fields("Month2enddate", cmbEndDate[1].Text);
				}
				else
				{
					clsSave.Set_Fields("month2enddate", cmbStartDate[1].Text);
					cmbEndDate[1].Text = cmbStartDate[1].Text;
				}
				if (Information.IsDate(cmbEndDate[2].Text))
				{
					clsSave.Set_Fields("month3enddate", cmbEndDate[2].Text);
				}
				else
				{
					clsSave.Set_Fields("month3enddate", cmbStartDate[2].Text);
					cmbEndDate[2].Text = cmbStartDate[2].Text;
				}
				clsSave.Update();
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private bool CopyEWRFile(ref string strFName, bool boolForSchool)
		{
			bool CopyEWRFile = false;
			FCFileSystem fso = new FCFileSystem();
			StreamReader tsSrc;
			StreamWriter tsDest;
			int intCurVolume;
			bool boolDone;
			// vbPorter upgrade warning: intResp As int	OnWrite(DialogResult)
			DialogResult intResp = 0;
			DriveInfo dr;
			int lngNumBytes = 0;
			// vbPorter upgrade warning: lngNumRecords As int	OnWriteFCConvert.ToDouble(
			int lngNumRecords;
			int lngNumWritten = 0;
			int lngNumTotalRecords;
			int lngNumTotalWritten;
			int lngNumTotalBytes;
			FileInfo fl;
			string strFlName = "";
			string strRecord = "";
			string strSrcName = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CopyEWRFile = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (fecherFoundation.Strings.Trim(strFName) == string.Empty)
					strFName = "rtnwage.txt";
				if (!boolForSchool)
				{
					strSrcName = "rtnWage.txt";
				}
				else
				{
					strSrcName = "rtnWageSch.txt";
				}
				if (!FCFileSystem.FileExists(strSrcName))
				{
					MessageBox.Show("File " + Environment.CurrentDirectory + "\\" + strSrcName + " not found.  Cannot copy.", "File not found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CopyEWRFile;
				}
				fl = new FileInfo(strSrcName);
				lngNumTotalBytes = FCConvert.ToInt32(fl.Length);
				if (lngNumTotalBytes < 277)
				{
					MessageBox.Show("File " + Environment.CurrentDirectory + "\\" + strSrcName + " is too small.  It is not a valid file.", "Invalid Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CopyEWRFile;
				}
				fl = null;
				tsSrc = FCFileSystem.OpenText(strSrcName);
				lngNumTotalRecords = FCConvert.ToInt32(Conversion.Int(lngNumTotalBytes / 277.0));
				// 275 + CRLF
				boolDone = false;
				intCurVolume = 0;
				lngNumTotalWritten = 0;
				while (!boolDone)
				{
					//App.DoEvents();
					// Unload frmWait
					intCurVolume += 1;
					DiskInsert:
					;
					if (!boolForSchool)
					{
						intResp = MessageBox.Show("Insert disk " + FCConvert.ToString(intCurVolume) + " in drive A: and press OK when ready", "Insert Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
					}
					else
					{
						intResp = MessageBox.Show("Insert disk " + FCConvert.ToString(intCurVolume) + " to copy school file in drive A: and press OK when ready", "Insert Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
					}
					if (intResp == DialogResult.Cancel)
					{
						MessageBox.Show("Copy cancelled by user", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return CopyEWRFile;
					}
					dr = fso.GetDrive("A:");
					if (!dr.IsReady)
					{
						MessageBox.Show("Drive not ready", "Not Ready", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto DiskInsert;
					}
					lngNumWritten = 0;
					//lngNumBytes = dr.FreeSpace;
					if (lngNumBytes < 277 * 2 + 1)
					{
						MessageBox.Show("Not enough free space. Delete some files or use another disk.", "Disk Full", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto DiskInsert;
					}
					// Call frmWait.Init("Copying volume " & intCurVolume)
					lngNumRecords = FCConvert.ToInt32((lngNumBytes / 277.0) - 2);
					// leave a little extra room to be sure
					if (intCurVolume == 1)
					{
						strFlName = strFName;
					}
					else
					{
						strFlName = strFName + FCConvert.ToString(intCurVolume);
					}
					if (FCFileSystem.FileExists("a:\\" + strFlName))
						FCFileSystem.DeleteFile("a:\\" + strFlName);
					tsDest = FCFileSystem.CreateTextFile("a:\\" + strFlName);
					// Do While lngNumWritten < lngNumRecords And Not boolDone
					// strRecord = tsSrc.ReadLine
					// tsDest.WriteLine strRecord
					// tsDest.Write
					// lngNumWritten = lngNumWritten + 1
					// lngNumTotalWritten = lngNumTotalWritten + 1
					// If lngNumWritten >= lngNumTotalRecords Then boolDone = True
					// Loop
					while (lngNumWritten < lngNumBytes && !boolDone)
					{
						//strRecord = tsSrc.Read(FCConvert.ToInt32(lngNumBytes / 277.0) - 2);
						tsDest.Write(strRecord);
						lngNumWritten += strRecord.Length;
						lngNumTotalWritten += strRecord.Length;
						if (lngNumTotalWritten >= lngNumTotalBytes)
							boolDone = true;
					}
					tsDest.Close();
				}
				tsSrc.Close();
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("File Copy Completed", "File Copied", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CopyEWRFile = true;
				return CopyEWRFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CopyEWRFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CopyEWRFile;
		}

		private bool CopyEWRFile2(string strFName, bool boolForSchool)
		{
			return CopyEWRFile(ref strFName, boolForSchool);
		}

		private bool CreateEWRFile()
		{
			bool CreateEWRFile = false;
			// creates the file "rtnwage" in the data directory
			FCFileSystem fso = new FCFileSystem();
			StreamWriter ts = null;
			StreamWriter SchoolTS = null;
			bool boolFileOpened = false;
			string strRecord;
			int lngNumEmployeeRecords;
			int lngNumSchoolEmployeeRecords;
			int intCurrentQuarter;
			int lngCurrentYear;
			string strSQL;
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			string strQuery1 = "";
			string strQuery2 = "";
			string strQuery3 = "";
			string strQuery4 = "";
			string[] strAry = null;
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			string strRange = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLastEmployee = "";
				dblTotalWages = 0;
				dblTotalExcess = 0;
				lngFemsM1 = 0;
				lngFemsM2 = 0;
				lngFemsM3 = 0;
				dblTotalWH = 0;
				dblTotalGrossWages = 0;
				lngM1 = 0;
				lngM2 = 0;
				lngM3 = 0;
				dblTotalSchoolExcess = 0;
				dblTotalSchoolGrossWages = 0;
				dblTotalSchoolWages = 0;
				lngSchoolFemsM1 = 0;
				lngSchoolFemsM2 = 0;
				lngSchoolFemsM3 = 0;
				lngSchoolM1 = 0;
				lngSchoolM2 = 0;
				lngSchoolM3 = 0;
				lngCurrentYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
				if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
				{
					intCurrentQuarter = 1;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
				{
					intCurrentQuarter = 2;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
				{
					intCurrentQuarter = 3;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
				{
					intCurrentQuarter = 4;
				}
				strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) < 1)
				{
					// all or single
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				}
				else
				{
					// range
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
				}
				if (lngSeqStart >= 0)
				{
					// strRange = " and seqnumber between " & lngSeqStart & " and " & lngSeqEnd
					strRange = " where seqnumber between " + FCConvert.ToString(lngSeqStart) + " and " + FCConvert.ToString(lngSeqEnd);
				}
				else
				{
					strRange = "";
				}
				CreateEWRFile = false;
				boolFileOpened = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// Call frmWait.Init("Please wait while the file is created", True)
				// strSQL = "select employeenumber from tblcheckdetail where paydate between '" & txtStartDate(0).Text & "' and #" & txtEndDate(0).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and (distu <> 'N' and distu <> '') and  paydate between '" + cmbStartDate[0].Text + "'and '" + cmbEndDate[0].Text + "'group by ssn";
				clsMonth1.OpenRecordset(strSQL, "twpy0000.vb1");
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where fromschool = 1 and checkvoid = 0 and (distu <> 'N' and distu <> '') and  paydate between '" + cmbStartDate[0].Text + "'and '" + cmbEndDate[0].Text + "'group by ssn";
				clsSchoolMonth1.OpenRecordset(strSQL, "twpy0000.vb1");
				// strSQL = "select employeenumber from tblcheckdetail where paydate between #" & txtStartDate(1).Text & "# and #" & txtEndDate(1).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + cmbStartDate[1].Text + "'and '" + cmbEndDate[1].Text + "'group by ssn";
				clsMonth2.OpenRecordset(strSQL, "twpy0000.vb1");
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber ) where fromschool = 1 and checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + cmbStartDate[1].Text + "'and '" + cmbEndDate[1].Text + "'group by ssn";
				clsSchoolMonth2.OpenRecordset(strSQL, "twpy0000.vb1");
				// strSQL = "select employeenumber from tblcheckdetail where paydate between #" & txtStartDate(2).Text & "# and #" & txtEndDate(2).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + cmbStartDate[2].Text + "'and '" + cmbEndDate[2].Text + "'group by ssn";
				clsMonth3.OpenRecordset(strSQL, "twpy0000.vb1");
				strSQL = "select ssn from tblcheckdetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where fromschool = 1 and checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + cmbStartDate[2].Text + "'and '" + cmbEndDate[2].Text + "'group by ssn";
				clsSchoolMonth3.OpenRecordset(strSQL, "twpy0000.vb1");
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarterCovered, lngYearCovered);
				strSQL = "select count(employeenumber) as thecount from (SELECT EMPLOYEENUMBER FROM tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber having  sum(statetaxwh) > 0)";
				clsEmployees.OpenRecordset(strSQL, "twpy0000.vb1");
				lngTotWithholdableEmployees = 0;
				if (!clsEmployees.EndOfFile())
				{
					lngTotWithholdableEmployees = FCConvert.ToInt32(Math.Round(Conversion.Val(clsEmployees.Get_Fields("thecount"))));
				}
				// YTD is not same as qtd
				// strSQL = "select employeenumber as enum,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where Paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )"
				// Call clsEmployees.CreateStoredProcedure("StateUnemploymentQuery2", strSQL, "twpy0000.vb1")
				// strSQL = "select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where paydate between '" & "1/1/" & lngYearCovered & "' and '" & dtEndDate & "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber "
				// Call clsEmployees.CreateStoredProcedure("StateUnemploymentQuery3", strSQL, "twpy0000.vb1")
				// strQuery1 = "(Select * from stateunemploymentquery3 inner join stateunemploymentquery2 on (stateunemploymentquery2.enum = stateunemploymentquery3.employeenum)) as C1Query1"
				// strQuery2 = "(select employeenumber,sum(statetaxgross) as qtdTotalGross,sum(stateTaxWH) as qtdStateWH from tblcheckdetail where paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 group by employeenumber having (sum(grosspay) > 0 or sum(statetaxwh) > 0) ) as C1Query2 "
				// strQuery3 = "(select * from " & strQuery2 & " left join " & strQuery1 & " on (c1query1.eNUM = c1query2.employeenumBER)) as C1Query3"
				// strQuery4 = "(select * from " & strQuery3 & " left join (select employeenumber as enum,unemployment as seasonal from tblMiscUpdate) as C1Query4 on (c1query4.enum = c1query3.employeenumber)) as C1Query5 "
				// strSQL = "select ssn from tblemployeemaster where not unemploymentexempt " & strRange & " group by ssn"
				strSQL = "select ssn from tblemployeemaster  " + strRange + " group by ssn";
				// End If
				// 
				clsEmployees.OpenRecordset(strSQL, "twpy0000.vb1");
				// get rid of any existing file
				if (FCFileSystem.FileExists("rtnwage.txt"))
				{
					FCFileSystem.DeleteFile("rtnwage.txt");
				}
				if (FCFileSystem.FileExists("rtnWageSch.txt"))
				{
					FCFileSystem.DeleteFile("rtnWageSch.txt");
				}
				// create the file
				ts = FCFileSystem.CreateTextFile("rtnwage.txt");
				SchoolTS = FCFileSystem.CreateTextFile("rtnWageSch.txt");
				// write the transmitter record (first record)
				strRecord = MakeTransmitterRecord();
				if (strRecord == "")
				{
					ts.Close();
					SchoolTS.Close();
					return CreateEWRFile;
				}
				ts.WriteLine(strRecord);
				SchoolTS.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write the authorization record
				strRecord = MakeAuthorizationRecord();
				if (strRecord == "")
				{
					ts.Close();
					SchoolTS.Close();
					return CreateEWRFile;
				}
				ts.WriteLine(strRecord);
				SchoolTS.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write employer record
				strRecord = MakeEmployerRecord();
				if (strRecord == "")
				{
					ts.Close();
					SchoolTS.Close();
					return CreateEWRFile;
				}
				ts.WriteLine(strRecord);
				strRecord = MakeEmployerRecord(true);
				SchoolTS.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write employee records
				lngNumEmployeeRecords = 0;
				lngNumSchoolEmployeeRecords = 0;
				while (!clsEmployees.EndOfFile())
				{
					//App.DoEvents();
					strSchEmployeeNumber = "";
					strLastSchSSN = "";
					strRecord = MakeEmployeeRecord(true);
					if (strRecord != string.Empty)
					{
						SchoolTS.WriteLine(strRecord);
						lngNumSchoolEmployeeRecords += 1;
					}
					strRecord = MakeEmployeeRecord();
					// If strRecord = "" Then
					// ts.Close
					// SchoolTS.Close
					// Exit Function
					// End If
					if (strRecord != string.Empty)
					{
						ts.WriteLine(strRecord);
						lngNumEmployeeRecords += 1;
					}
					clsEmployees.MoveNext();
					// frmWait.IncrementProgress
					//App.DoEvents();
				}
				// write total record
				strRecord = MakeTotalRecord(lngNumEmployeeRecords);
				if (strRecord == "")
				{
					ts.Close();
					SchoolTS.Close();
					return CreateEWRFile;
				}
				ts.WriteLine(strRecord);
				strRecord = MakeTotalRecord(lngNumSchoolEmployeeRecords, true);
				SchoolTS.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write reconciliation record(s)
				// strSQL = "select * from tblStatePayments where paid > 0 order by datepaid"
				strSQL = "select * from tblStatePayments where paid > 0 or withheld > 0 order by datepaid";
				clsPayments.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsPayments.EndOfFile())
				{
					//App.DoEvents();
					strRecord = MakeReconciliationRecord();
					if (strRecord == "")
					{
						ts.Close();
						return CreateEWRFile;
					}
					ts.WriteLine(strRecord);
					clsPayments.MoveNext();
					// frmWait.IncrementProgress
					//App.DoEvents();
				}
				// write final record
				strRecord = MakeFinalRecord(ref lngNumEmployeeRecords);
				if (strRecord == "")
				{
					ts.Close();
					SchoolTS.Close();
					return CreateEWRFile;
				}
				ts.WriteLine(strRecord);
				strRecord = MakeFinalRecord(ref lngNumSchoolEmployeeRecords, true);
				SchoolTS.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// close the file
				ts.Close();
				SchoolTS.Close();
				boolFileOpened = false;
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				ts = null;
				CreateEWRFile = true;
				return CreateEWRFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				// Unload frmWait
				if (boolFileOpened)
				{
					ts.Close();
					SchoolTS.Close();
				}
				if (fecherFoundation.Information.Err(ex).Number == 52)
				{
					MessageBox.Show("Bad file name" + "\r\n" + "Make sure there are no illegal characters in the name such as / or *", "Bad File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateEWRFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return CreateEWRFile;
		}

		private string MakeTransmitterRecord()
		{
			string MakeTransmitterRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeTransmitterRecord = "";
				strOut = "A";
				strOut += Strings.Format(lngYearCovered, "0000");
				// four digit year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += "UTAX";
				strOut += Strings.StrDup(5, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				// transmitting company name
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRAddressLen, " "), 1, modCoreysSweeterCode.EWRAddressLen);
				// transmitting company street address
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRCityLen, " "), 1, modCoreysSweeterCode.EWRCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + "  ", 1, 2);
				strOut += Strings.StrDup(13, " ");
				// spaces
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					object intValue = EWRRecord.EmployerZip4;
					strOut += "-" + FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(intValue), 4));
					EWRRecord.EmployerZip4 = FCConvert.ToString(intValue);
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.Mid(EWRRecord.TransmitterTitle + Strings.StrDup(modCoreysSweeterCode.EWRTransmitterTitleLen, " "), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				// person responsible for accuracy of this report
				strOut += Strings.Format(EWRRecord.TransmitterPhone, "0000000000");
				object intValue2 = EWRRecord.TransmitterExt;
				strOut += FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(intValue2), 4));
				EWRRecord.TransmitterExt = FCConvert.ToString(intValue2);
				strOut += Strings.StrDup(68, " ");
				MakeTransmitterRecord = strOut;
				return MakeTransmitterRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeTransmitterRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeTransmitterRecord;
		}

		private string MakeAuthorizationRecord()
		{
			string MakeAuthorizationRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeAuthorizationRecord = "";
				strOut = "B";
				// this is an authorization record
				strOut += Strings.Format(lngYearCovered, "0000");
				// current year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += Strings.StrDup(8, " ");
				// used on tapes and magnetic reels only
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += " ";
				// space
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += "ASC";
				// in ascii format not ebcdic
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += "UTAX";
				strOut += Strings.StrDup(108, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRReturnNameLen, " "), 1, modCoreysSweeterCode.EWRReturnNameLen);
				// company to return to
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRReturnAddressLen, " "), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRReturnCityLen, " "), 1, modCoreysSweeterCode.EWRReturnCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + "  ", 1, 2);
				strOut += Strings.StrDup(5, " ");
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					strOut += "-" + Strings.Format(EWRRecord.EmployerZip4, "0000");
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.StrDup(13, " ");
				MakeAuthorizationRecord = strOut;
				return MakeAuthorizationRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeAuthorizationRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeAuthorizationRecord;
		}

		private string MakeEmployerRecord(bool boolRecForSchool = false)
		{
			string MakeEmployerRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeEmployerRecord = "";
				strOut = "E";
				// employer record
				strOut += Strings.Format(lngYearCovered, "0000");
				// current year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += Strings.StrDup(9, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRAddressLen, " "), 1, modCoreysSweeterCode.EWRAddressLen);
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRCityLen, " "), 1, modCoreysSweeterCode.EWRCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + Strings.StrDup(2, " "), 1, 2);
				strOut += Strings.StrDup(8, " ");
				// spaces
				// zip and zip4 are backwards in this record.  Is this a typo in the specs?
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					strOut += "-" + Strings.Format(EWRRecord.EmployerZip4, "0000");
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				strOut += Strings.StrDup(8, " ");
				// spaces
				strOut += "UTAX";
				strOut += Strings.Format(EWRRecord.EmployerStateCode, "00");
				if (!boolRecForSchool)
				{
					strOut += EWRRecord.StateUCAccount;
					// already forced to 10
				}
				else
				{
					strOut += EWRRecord.SchoolStateUCAccount;
				}
				strOut += Strings.StrDup(5, " ");
				// spaces
				strOut += Strings.Format(intLastMonthInQuarter, "00");
				// last month in period covered
				if (!clsEmployees.EndOfFile())
				{
					strOut += "1";
					// 0 is not to be followed by employee records
				}
				else
				{
					strOut += "0";
				}
				// corey 11/16/05 additions to specs was just 67 blanks
				// strOut = strOut & String(67, " ")   'spaces
				strOut += Strings.StrDup(18, " ");
				// spaces
				strOut += Strings.StrDup(9, "0");
				// preparer EIN, only if using a paid preparer
				strOut += Strings.Left(EWRRecord.ProcessorLicenseCode + Strings.StrDup(7, " "), 7);
				// processor license code
				if (!boolRecForSchool)
				{
					strOut += Strings.Format(lngTotWithholdableEmployees, "0000");
					// total # of employees subject to withholding
				}
				else
				{
					strOut += "0000";
				}
				strOut += Strings.StrDup(29, " ");
				// spaces
				if (!boolRecForSchool)
				{
					strOut += EWRRecord.MRSWithholdingID;
					// already forced to 11
				}
				else
				{
					strOut += EWRRecord.SchoolMRSWithholdingID;
				}
				strOut += Strings.StrDup(7, " ");
				// spaces
				MakeEmployerRecord = strOut;
				return MakeEmployerRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeEmployerRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeEmployerRecord;
		}

		private string MakeEmployeeRecord(bool boolRecForSchool = false)
		{
			string MakeEmployeeRecord = "";
			string strOut;
			double dblTotal;
			// vbPorter upgrade warning: dblExcess As double	OnWriteFCConvert.ToDecimal(
			double dblExcess;
			double dblTGross;
			double dblStateGross;
			double dblStateTax;
			double dblYTDUnemployment;
			double dblTempTax;
			double dblTemp;
			string strTemp;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(string, DateTime)
			DateTime dtEndDate;
			clsDRWrapper clsSchoolTemp = new clsDRWrapper();
			string strSeasonal = "";
			string strSex = "";
			string strEmpSeqNum = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeEmployeeRecord = "";
				// get last day of last month
				dtEndDate = FCConvert.ToDateTime(FCConvert.ToString(intLastMonthInQuarter) + "/1/" + FCConvert.ToString(lngYearCovered));
				dtEndDate = fecherFoundation.DateAndTime.DateAdd("m", 1, dtEndDate);
				dtEndDate = fecherFoundation.DateAndTime.DateAdd("d", -1, dtEndDate);
				strOut = "S";
				// employeerecord
				strOut += Strings.Format(FCConvert.ToString(clsEmployees.Get_Fields("ssn")).Replace("-", ""), "000000000");
				// employees social security number
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
				{
					// UCase(MuniName) <> "JAY" And
					clsTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" + clsEmployees.Get_Fields("ssn") + "' order by seqnumber", "twpy0000.vb1");
				}
				else if (!boolRecForSchool)
				{
					// If UCase(MuniName) = "JAY" Then
					// Call clsTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" & clsEmployees.Fields("ssn") & "' and seqnumber = 1 ", "twpy0000.vb1")
					// Call clsSchoolTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" & clsEmployees.Fields("ssn") & "' and seqnumber <> 1 order by seqnumber ", "twpy0000.vb1")
					// Else
					clsTemp.OpenRecordset("select top 1 * from tblemployeemaster where seqnumber = -1000", "twpy0000.vb1");
					clsSchoolTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" + clsEmployees.Get_Fields("ssn") + "'  order by seqnumber ", "twpy0000.vb1");
					// End If
				}
				else
				{
					// If UCase(MuniName) = "JAY" Then
					// Call clsTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" & clsEmployees.Fields("ssn") & "' and seqnumber <> 1 order by seqnumber", "twpy0000.vb1")
					// Else
					clsTemp.OpenRecordset("select * ,tblemployeemaster.employeenumber as employeenumber, tblemployeemaster.seqnumber as employeeseqnumber,tblmiscupdate.unemployment as seasonal from tblemployeemaster left join tblmiscupdate on (tblmiscupdate.employeenumber = tblemployeemaster.employeenumber) where ssn = '" + clsEmployees.Get_Fields("ssn") + "' order by seqnumber", "twpy0000.vb1");
					// End If
					if (clsTemp.EndOfFile())
					{
						// no school employee
						return MakeEmployeeRecord;
					}
				}
				if (clsTemp.EndOfFile() && clsSchoolTemp.EndOfFile())
					return MakeEmployeeRecord;
				if (!clsTemp.EndOfFile())
				{
					strOut += Strings.Mid(clsTemp.Get_Fields("lastname") + Strings.StrDup(20, " "), 1, 20);
					strOut += Strings.Mid(clsTemp.Get_Fields("firstname") + Strings.StrDup(12, " "), 1, 12);
					strLastEmployee = FCConvert.ToString(clsTemp.Get_Fields("employeenumber"));
					strSeasonal = FCConvert.ToString(clsTemp.Get_Fields_Boolean("seasonal"));
					strSex = FCConvert.ToString(clsTemp.Get_Fields("sex"));
					strEmpSeqNum = FCConvert.ToString(clsTemp.Get_Fields("employeeseqnumber"));
				}
				else
				{
					strOut += Strings.Mid(clsSchoolTemp.Get_Fields("lastname") + Strings.StrDup(20, " "), 1, 20);
					strOut += Strings.Mid(clsSchoolTemp.Get_Fields("firstname") + Strings.StrDup(12, " "), 1, 12);
					strLastEmployee = FCConvert.ToString(clsSchoolTemp.Get_Fields("employeenumber"));
					strSeasonal = FCConvert.ToString(clsSchoolTemp.Get_Fields_Boolean("seasonal"));
					strSex = FCConvert.ToString(clsSchoolTemp.Get_Fields("sex"));
					strEmpSeqNum = FCConvert.ToString(clsSchoolTemp.Get_Fields("employeeseqnumber"));
				}
				if (boolRecForSchool)
					strSchEmployeeNumber = FCConvert.ToString(clsTemp.Get_Fields("employeenumber"));
				strOut += " ";
				// middle initial
				strOut += Strings.Format(EWRRecord.EmployerStateCode, "00");
				// state code
				// MATTHEW 10/08/2003
				strOut += Strings.Format(intLastMonthInQuarter, "00") + FCConvert.ToString(lngYearCovered);
				// reporting year and quarter
				strOut += Strings.StrDup(12, " ");
				dblTGross = 0;
				dblTemp = 0;
				dblStateGross = 0;
				dblStateTax = 0;
				dblYTDUnemployment = 0;
				dblTotal = 0;
				dblTempTax = 0;
				if (!boolRecForSchool)
				{
					while (!clsTemp.EndOfFile())
					{
						dblTotal += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNTOWN);
						dblYTDUnemployment += modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNTOWN);
						dblTemp += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNTOWN);
						dblTempTax += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNTOWN);
						clsTemp.MoveNext();
					}
					while (!clsSchoolTemp.EndOfFile())
					{
						dblTemp += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, clsSchoolTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNSCHOOL);
						dblTempTax += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, clsSchoolTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNSCHOOL);
						clsSchoolTemp.MoveNext();
					}
					dblTotalWH += dblTempTax;
					dblStateTax += dblTempTax;
					dblStateGross += dblTemp;
					dblTotalGrossWages += dblStateGross;
				}
				else
				{
					while (!clsTemp.EndOfFile())
					{
						dblTotal += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNSCHOOL);
						dblYTDUnemployment += modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, clsTemp.Get_Fields("employeenumber"), dtEndDate, modCoreysSweeterCode.CNSTSCHOOLORTOWNSCHOOL);
						clsTemp.MoveNext();
					}
				}
				dblTGross += dblTotal;
				dblExcess = FCConvert.ToDouble(modDavesSweetCode.CalculateExcess(FCConvert.ToDecimal(dblYTDUnemployment), FCConvert.ToDecimal(dblTotal), FCConvert.ToDecimal(dblLimit)));
				if (!boolRecForSchool)
				{
					dblTotalExcess += dblExcess;
					dblTotalWages += dblTotal;
				}
				else
				{
					dblTotalSchoolExcess += dblExcess;
					dblTotalSchoolWages += dblTotal;
				}
				if (dblStateGross == 0 && dblTotal == 0)
				{
					// this employee didn't work this quarter
					return MakeEmployeeRecord;
				}
				strOut += Strings.Format(dblTotal * 100, "00000000000000");
				strOut += Strings.Format(dblExcess * 100, "00000000000000");
				strOut += Strings.Format((dblTotal - dblExcess) * 100, "00000000000000");
				strOut += Strings.StrDup(37, " ");
				// spaces
				strOut += "UTAX";
				if (!boolRecForSchool)
				{
					strOut += EWRRecord.StateUCAccount;
				}
				else
				{
					strOut += EWRRecord.SchoolStateUCAccount;
				}
				strOut += Strings.StrDup(20, " ");
				// spaces
				// dblTemp = 0
				// If Not boolRecForSchool Then
				// 
				// 
				// Else
				// 
				// dblTotalSchoolGrossWages = dblTotalSchoolGrossWages + dblTemp
				// End If
				// Dave 10/11/06 School employees in the town file must show a State Gross of 0
				if ((!boolRecForSchool && strEmpSeqNum != "1") || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "WISCASSET SCHOOLS" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "WESTPORT SCHOOLS" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ALNA SCHOOLS")
				{
					strOut += Strings.Format(0, "00000000000000");
				}
				else
				{
					strOut += Strings.Format(dblStateGross * 100, "00000000000000");
				}
				// If Not boolRecForSchool Then
				// 
				// Else
				// dblTempTax = 0
				// End If
				strOut += Strings.Format(dblStateTax * 100, "00000000000000");
				if (strSeasonal == "S")
				{
					strOut += "S";
				}
				else
				{
					strOut += "N";
				}
				strOut += Strings.StrDup(5, " ");
				strOut += "0";
				// not mandatory
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(strSex + " ", 1));
				if (!boolRecForSchool)
				{
					if (!clsMonth1.EndOfFile())
					{
						if (clsMonth1.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If Trim(strLastEmployee) <> Trim(clsTemp.Fields("employeenumber")) Then
							lngM1 += 1;
							if (strTemp == "F")
								lngFemsM1 += 1;
							// End If
						}
						else
						{
							clsMonth1.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsMonth2.EndOfFile())
					{
						if (clsMonth2.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If strLastEmployee <> clsTemp.Fields("employeenumber") Then
							lngM2 += 1;
							if (strTemp == "F")
								lngFemsM2 += 1;
							// End If
						}
						else
						{
							clsMonth2.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsMonth3.EndOfFile())
					{
						if (clsMonth3.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If strLastEmployee <> clsTemp.Fields("employeenumber") Then
							lngM3 += 1;
							if (strTemp == "F")
								lngFemsM3 += 1;
							// End If
						}
						else
						{
							clsMonth3.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
				}
				else
				{
					if (!clsMonth1.EndOfFile())
					{
						if (clsSchoolMonth1.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If Trim(strLastEmployee) <> Trim(clsTemp.Fields("employeenumber")) Then
							lngSchoolM1 += 1;
							if (strTemp == "F")
								lngSchoolFemsM1 += 1;
							// End If
						}
						else
						{
							clsSchoolMonth1.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsSchoolMonth2.EndOfFile())
					{
						if (clsSchoolMonth2.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If strLastEmployee <> clsTemp.Fields("employeenumber") Then
							lngSchoolM2 += 1;
							if (strTemp == "F")
								lngSchoolFemsM2 += 1;
							// End If
						}
						else
						{
							clsSchoolMonth2.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsSchoolMonth3.EndOfFile())
					{
						if (clsSchoolMonth3.FindFirstRecord("ssn", clsEmployees.Get_Fields("ssn")))
						{
							strOut += "1";
							// If strLastEmployee <> clsTemp.Fields("employeenumber") Then
							lngSchoolM3 += 1;
							if (strTemp == "F")
								lngSchoolFemsM3 += 1;
							// End If
						}
						else
						{
							clsSchoolMonth3.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
				}
				if (!boolRecForSchool)
				{
					strOut += EWRRecord.MRSWithholdingID;
				}
				else
				{
					strOut += EWRRecord.SchoolMRSWithholdingID;
				}
				if (strTemp == "F")
				{
					strOut += "1";
				}
				else
				{
					strOut += "0";
				}
				strOut += Strings.StrDup(8, "0");
				strOut += Strings.StrDup(8, "0");
				strOut += Strings.StrDup(33, " ");
				MakeEmployeeRecord = strOut;
				return MakeEmployeeRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeEmployeeRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeEmployeeRecord;
		}
		// vbPorter upgrade warning: lngNumEmployeeRecords As object	OnWriteFCConvert.ToInt32(
		private string MakeTotalRecord(object lngNumEmployeeRecords, bool boolRecForSchool = false)
		{
			string MakeTotalRecord = "";
			string strOut;
			clsDRWrapper clsLoad = new clsDRWrapper();
			double dblIncomeDue = 0;
			// vbPorter upgrade warning: dblUCContributionsDue As double	OnWrite(string)
			double dblUCContributionsDue = 0;
			double dblVoucherPayments = 0;
			double dblTotalContributionsDue;
			// vbPorter upgrade warning: dblCSSFAssessment As double	OnWrite(string)
			double dblCSSFAssessment = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeTotalRecord = "";
				strOut = "T";
				// total record
				strOut += Strings.Format(lngNumEmployeeRecords, "0000000");
				strOut += "UTAX";
				if (!boolRecForSchool)
				{
					strOut += EWRRecord.StateUCAccount;
				}
				else
				{
					strOut += EWRRecord.SchoolStateUCAccount;
				}
				strOut += Strings.StrDup(4, " ");
				// spaces
				if (!boolRecForSchool)
				{
					strOut += Strings.Format(dblTotalWages * 100, "00000000000000");
					strOut += Strings.Format(dblTotalExcess * 100, "00000000000000");
					strOut += Strings.Format((dblTotalWages - dblTotalExcess) * 100, "00000000000000");
				}
				else
				{
					strOut += Strings.Format(dblTotalSchoolWages * 100, "00000000000000");
					strOut += Strings.Format(dblTotalSchoolExcess * 100, "00000000000000");
					strOut += Strings.Format((dblTotalSchoolWages - dblTotalSchoolExcess) * 100, "00000000000000");
				}
				strOut += Strings.StrDup(19, " ");
				if (!boolRecForSchool)
				{
					strOut += Strings.Format(FCConvert.ToInt32(Strings.Format((dblTotalWages - dblTotalExcess) * dblRate, "0.00")) * 100, "0000000000000");
					// total due
					dblUCContributionsDue = FCConvert.ToDouble(Strings.Format((dblTotalWages - dblTotalExcess) * dblRate, "0.00"));
					dblCSSFAssessment = FCConvert.ToDouble(Strings.Format((dblTotalWages - dblTotalExcess) * dblCSSFRate, "0.00"));
				}
				else
				{
					strOut += Strings.Format(FCConvert.ToInt32(Strings.Format((dblTotalSchoolWages - dblTotalSchoolExcess) * dblSchoolRate, "0.00")) * 100, "0000000000000");
					// total due
					dblUCContributionsDue = FCConvert.ToDouble(Strings.Format((dblTotalSchoolWages - dblTotalSchoolExcess) * dblSchoolRate, "0.00"));
					dblCSSFAssessment = FCConvert.ToDouble(Strings.Format((dblTotalSchoolWages - dblTotalExcess) * dblCSSFRate, "0.00"));
				}
				dblTotalContributionsDue = dblUCContributionsDue + dblCSSFAssessment;
				// strOut = strOut & String(11, " ")
				strOut += Strings.Format(dblCSSFAssessment * 100, "00000000000");
				// payments made
				if (!boolRecForSchool)
				{
					clsLoad.OpenRecordset("select sum(paid) as totalpaid from tblStatePayments", "twpy0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						dblVoucherPayments = Conversion.Val(clsLoad.Get_Fields("totalpaid"));
					}
					else
					{
						dblVoucherPayments = 0;
					}
					strOut += Strings.Format(dblVoucherPayments * 100, "00000000000");
					dblIncomeDue = dblTotalWH - (dblCredit + dblVoucherPayments);
				}
				else
				{
					strOut += "00000000000";
					// voucher payments
					dblIncomeDue = 0;
				}
				if (dblIncomeDue < 0)
				{
					strOut += "00000000000";
				}
				else
				{
					strOut += Strings.Format(dblIncomeDue * 100, "00000000000");
				}
				strOut += Strings.StrDup(11, " ");
				// strOut = strOut & String(4, "0")
				if (!boolRecForSchool)
				{
					strOut += Strings.Format(dblRate * 10000, "0000");
				}
				else
				{
					strOut += Strings.Format(dblSchoolRate * 10000, "0000");
				}
				// strOut = strOut & String(4, "0")
				strOut += Strings.Format(dblCSSFRate * 10000, "0000");
				strOut += Strings.StrDup(22, " ");
				// amount due
				if (dblIncomeDue + dblTotalContributionsDue < 0)
				{
					strOut += "00000000000";
				}
				else
				{
					strOut += Strings.Format((dblIncomeDue + dblTotalContributionsDue) * 100, "00000000000");
				}
				// corey 02082005
				// state doesn't want remitted anymore, just zeroes
				strOut += "0000000000000";
				// strOut = strOut & Format(dblRemitted * 100, "0000000000000")
				if (!boolRecForSchool)
				{
					strOut += Strings.Format(dblTotalGrossWages * 100, "00000000000000");
					strOut += Strings.Format(dblTotalWH * 100, "00000000000000");
					strOut += Strings.Format(lngM1, "0000000");
					strOut += Strings.Format(lngM2, "0000000");
					strOut += Strings.Format(lngM3, "0000000");
					strOut += Strings.Format(lngFemsM1, "0000000");
					strOut += Strings.Format(lngFemsM2, "0000000");
					strOut += Strings.Format(lngFemsM3, "0000000");
				}
				else
				{
					strOut += Strings.Format(dblTotalSchoolGrossWages * 100, "00000000000000");
					strOut += "00000000000000";
					strOut += Strings.Format(lngSchoolM1, "0000000");
					strOut += Strings.Format(lngSchoolM2, "0000000");
					strOut += Strings.Format(lngSchoolM3, "0000000");
					strOut += Strings.Format(lngSchoolFemsM1, "0000000");
					strOut += Strings.Format(lngSchoolFemsM2, "0000000");
					strOut += Strings.Format(lngSchoolFemsM3, "0000000");
				}
				strOut += Strings.StrDup(7, " ");
				MakeTotalRecord = strOut;
				return MakeTotalRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeTotalRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeTotalRecord;
		}

		private string MakeReconciliationRecord()
		{
			string MakeReconciliationRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeReconciliationRecord = "";
				strOut = "R";
				// reconciliation record
				strOut += Strings.Format(clsPayments.Get_Fields_DateTime("DatePaid"), "MMddyyyy");
				strOut += Strings.Format((clsPayments.Get_Fields("withheld") * 100), "000000000");
				strOut += Strings.Format(Conversion.Val(clsPayments.Get_Fields("paid")) * 100, "000000000");
				strOut += EWRRecord.StateUCAccount;
				strOut += Strings.StrDup(238, " ");
				MakeReconciliationRecord = strOut;
				return MakeReconciliationRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeReconciliationRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeReconciliationRecord;
		}

		private string MakeFinalRecord(ref int lngNumEmployeeRecords, bool boolRecForSchool = false)
		{
			string MakeFinalRecord = "";
			string strOut;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeFinalRecord = "";
				strOut = "F";
				// final record
				strOut += Strings.Format(lngNumEmployeeRecords, "0000000000");
				strOut += Strings.Format(1, "0000000000");
				// total number of E records
				strOut += "UTAX";
				strOut += Strings.StrDup(15, " ");
				if (!boolRecForSchool)
				{
					dblTemp = dblTotalWages * 100;
				}
				else
				{
					dblTemp = dblTotalSchoolWages * 100;
				}
				strOut += Strings.Format(dblTemp, "000000000000000");
				strOut += Strings.StrDup(220, " ");
				MakeFinalRecord = strOut;
				return MakeFinalRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeFinalRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeFinalRecord;
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillDateCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			int[] intM = new int[3 + 1];
			DateTime dtDateTemp;
			// vbPorter upgrade warning: dtDate1 As DateTime	OnWrite(string)
			DateTime dtDate1;
			DateTime dtDate2;
			int x;
			intM[2] = intLastMonthInQuarter;
			intM[1] = intM[2] - 1;
			intM[0] = intM[2] - 2;
			// first months combos
			for (x = 0; x <= 2; x++)
			{
				dtDate1 = FCConvert.ToDateTime(FCConvert.ToString(intM[x]) + "/1/" + FCConvert.ToString(lngYearCovered));
				dtDateTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtDate1);
				dtDate2 = fecherFoundation.DateAndTime.DateAdd("d", -1, dtDateTemp);
				strSQL = "select paydate from tblcheckdetail where paydate between '" + FCConvert.ToString(dtDate1) + "' and '" + FCConvert.ToString(dtDate2) + "' and checkvoid = 0 group by paydate order by paydate";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				cmbStartDate[x].Clear();
				cmbEndDate[x].Clear();
				cmbStartDate[x].AddItem("");
				cmbEndDate[x].AddItem("");
				while (!clsLoad.EndOfFile())
				{
					cmbStartDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					cmbEndDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					clsLoad.MoveNext();
				}
			}
			// x
		}
	}
}
