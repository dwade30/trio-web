﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modDateRoutines
	{
		////=========================================================
		//// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public static object LongDate(object MyDate)
		//{
		//	object LongDate = null;
		//	if (FCConvert.ToString(MyDate).Length == 8)
		//		return LongDate;
		//	LongDate = Strings.Format(MyDate, "MM/dd/yyyy");
		//	// LongDate = ConvertDateToHaveSlashes(LongDate)
		//	return LongDate;
		//}
		//// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public static object GetPlaceOfDeath(string intValue)
		//{
		//	object GetPlaceOfDeath = null;
		//	if (intValue == "0")
		//	{
		//		GetPlaceOfDeath = "DOA";
		//	}
		//	else if (intValue == "1")
		//	{
		//		GetPlaceOfDeath = "Inpatient";
		//	}
		//	else if (intValue == "2")
		//	{
		//		GetPlaceOfDeath = "ER/Outpatient";
		//	}
		//	else if (intValue == "3")
		//	{
		//		GetPlaceOfDeath = "Nursing Home";
		//	}
		//	else if (intValue == "4")
		//	{
		//		GetPlaceOfDeath = "Residence";
		//	}
		//	else if (intValue == "5")
		//	{
		//		GetPlaceOfDeath = "Other";
		//	}
		//	return GetPlaceOfDeath;
		//}
		// vbPorter upgrade warning: MyDate As object	OnWrite(string, object, VB.TextBox)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object StripDateSlashes(object MyDate)
		{
			object StripDateSlashes = null;
			// strips out the dash marks if the number came from a masked
			// edit box such as a sub-account number
			// vbPorter upgrade warning: I As int	OnWriteFCConvert.ToInt32(
			int I;
			string strTempNumber;
			strTempNumber = string.Empty;
			//FC:FINAL:DDU:#i2054 - fixed length of for
			int length = FCConvert.ToString(MyDate).Length;
			for (I = 0; I <= length; I++)
			{
				if (Strings.Left(FCConvert.ToString(MyDate), 1) != "/" && Strings.Left(FCConvert.ToString(MyDate), 1) != "_")
				{
					strTempNumber += Strings.Left(FCConvert.ToString(MyDate), 1);
				}
				if (!(FCConvert.ToString(MyDate) == string.Empty))
				{
					MyDate = Strings.Mid(FCConvert.ToString(MyDate), 2, FCConvert.ToString(MyDate).Length - 1);
				}
			}
			// I
			// return the new number
			StripDateSlashes = fecherFoundation.Strings.Trim(strTempNumber);
			return StripDateSlashes;
		}
		// vbPorter upgrade warning: ControlName As object	OnWrite(string)
		//public static void DateMask(T2KDateBox ControlName)
		//{
		//	string TempDate;
		//	TempDate = fecherFoundation.Strings.Trim(FCConvert.ToString(StripDateSlashes(ControlName)));
		//	if (TempDate != string.Empty)
		//	{
		//		if (TempDate.Length == 1)
		//		{
		//			if (FCConvert.ToDouble(TempDate) > 1)
		//			{
		//				ControlName.Text = "0" + TempDate;
		//				ControlName.SelStart = 3;
		//			}
		//		}
		//		else if (TempDate.Length == 2)
		//		{
		//			if (FCConvert.ToDouble(Strings.Left(TempDate, 1)) == 1 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 2)
		//			{
		//				ControlName.Text = Strings.Left(TempDate, 1);
		//				ControlName.SelStart = 1;
		//			}
		//		}
		//		else if (TempDate.Length == 3)
		//		{
		//			if (FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 3)
		//			{
		//				ControlName.Text = Strings.Left(TempDate, 2) + "0" + Strings.Right(TempDate, 1);
		//				ControlName.SelStart = 6;
		//			}
		//		}
		//		else if (TempDate.Length == 4)
		//		{
		//			if (FCConvert.ToDouble(Strings.Mid(TempDate, 3, 1)) == 3 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 1)
		//			{
		//				ControlName.Text = Strings.Left(TempDate, 3);
		//				ControlName.SelStart = 4;
		//			}
		//			// No need to check 5th digit
		//		}
		//		else if (TempDate.Length == 6)
		//		{
		//			if ((FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 19) && (FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 20))
		//			{
		//				if (FCConvert.ToDouble(Strings.Right(TempDate, 2)) > 60)
		//				{
		//					ControlName.Text = Strings.Left(TempDate, 4) + "19" + Strings.Right(TempDate, 2);
		//				}
		//				else
		//				{
		//					ControlName.Text = Strings.Left(TempDate, 4) + "20" + Strings.Right(TempDate, 2);
		//				}
		//			}
		//		}
		//	}
		//}

		public static string ConvertDateToHaveSlashes(string DateValue)
		{
			string ConvertDateToHaveSlashes = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				DateValue = CurrentDate();
			}
			str_date = fecherFoundation.Strings.Trim(DateValue);
			ConvertDateToHaveSlashes = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
			return ConvertDateToHaveSlashes;
		}

		//public static string ConvertDate(string DateValue)
		//{
		//	string ConvertDate = "";
		//	string str_date;
		//	if (DateValue == string.Empty)
		//	{
		//		return ConvertDate;
		//	}
		//	str_date = fecherFoundation.Strings.Trim(DateValue);
		//	ConvertDate = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
		//	return ConvertDate;
		//}
		//// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		//public static object MakeFullDate(string DateValue)
		//{
		//	object MakeFullDate = null;
		//	MakeFullDate = Strings.Format(DateValue, "MM/dd/yyyy");
		//	return MakeFullDate;
		//}

		public static string CurrentDate()
		{
			string CurrentDate = "";
			string TempDate;
			TempDate = FCConvert.ToString(Conversion.Val(Strings.Format(DateTime.Now, "MMddyyyy")));
			if (TempDate.Length == 7)
			{
				CurrentDate = 0 + TempDate;
			}
			else
			{
				CurrentDate = TempDate;
			}
			return CurrentDate;
		}
		// vbPorter upgrade warning: DateOne As object	OnWrite(string)
		// vbPorter upgrade warning: DateTwo As object	OnWrite(string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		////public static bool CompareDates(string DateOne, string DateTwo, string Symbol)
		////{
		////	// vbPorter upgrade warning: SubtractedValue As object	OnWriteFCConvert.ToDouble(
		////	double SubtractedValue = 0;
		////	DateOne = Strings.Format(ConvertDateToHaveSlashes(DateOne), "yyyyMMdd");
		////	DateTwo = Strings.Format(ConvertDateToHaveSlashes(DateTwo), "yyyyMMdd");
		////	if (Symbol == ">")
		////	{
		////		SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
		////		return (SubtractedValue > 0 ? true : false);
		////	}
		////	else if (Symbol == "<")
		////	{
		////		SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
		////		return (SubtractedValue > 0 ? false : true);
		////	}
		////	else if (Symbol == "=")
		////	{
		////		SubtractedValue = FCConvert.ToDouble(DateOne) - FCConvert.ToDouble(DateTwo);
		////		return (FCConvert.ToInt32(SubtractedValue) == 0 ? true : false);
		////	}
		////	return false;
		////}
		////// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		////public static object GetDateWithoutSlashes(string DateValue)
		////{
		////	object GetDateWithoutSlashes = null;
		////	string str_date;
		////	str_date = fecherFoundation.Strings.Trim(DateValue);
		////	GetDateWithoutSlashes = Strings.Left(str_date, 2) + Strings.Mid(str_date, 3, 2) + Strings.Right(str_date, 4);
		////	return GetDateWithoutSlashes;
		////}

		//public static bool InvalidMaskedDate(ref Wisej.Web.MaskedTextBox mskTbx, ref string strFieldName)
		//{
		//	bool InvalidMaskedDate = false;
		//	try
		//	{
		//		fecherFoundation.Information.Err().Clear();
		//		// vbPorter upgrade warning: strDate As object	OnWrite(string)
		//		object strDate;
		//		InvalidMaskedDate = false;
		//		// Anticipate no problems
		//		strDate = ConvertDateToHaveSlashes(mskTbx.Text);
		//		if (!(Information.IsDate(strDate)) || ((DateTime)strDate).Year < 1800 || ((DateTime)strDate).Year > 2200)
		//		{
		//			InvalidMaskedDate = true;
		//			// Problem!
		//			MessageBox.Show("Invalid Entry for " + strFieldName, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//			mskTbx.Focus();
		//			mskTbx.SelectionStart = 0;
		//			mskTbx.SelectionLength = 10;
		//		}
		//		return InvalidMaskedDate;
		//	}
		//	catch (Exception ex)
		//	{
		//		return InvalidMaskedDate;
		//	}
		//}
		//// Routine For Selecting a Text Box value for Change
		//public static void Reselect(ref FCTextBox txtTbx)
		//{
		//	txtTbx.SelectionStart = 0;
		//	txtTbx.SelectionLength = txtTbx.Text.Length;
		//}
		//// Common subroutine to complain and highlight the bad text.
		//private static void CussAndReselect(ref FCTextBox txtTbx, ref string strTbxType)
		//{
		//	MessageBox.Show("Invalid Entry for " + strTbxType + ": " + txtTbx.GetName(), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//	txtTbx.SelectionStart = 0;
		//	txtTbx.SelectionLength = txtTbx.Text.Length;
		//}
	}
}
