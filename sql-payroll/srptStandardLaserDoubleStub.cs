﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptStandardLaserDoubleStub.
	/// </summary>
	public partial class srptStandardLaserDoubleStub : FCSectionReport
	{
		public srptStandardLaserDoubleStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptStandardLaserDoubleStub InstancePtr
		{
			get
			{
				return (srptStandardLaserDoubleStub)Sys.GetInstance(typeof(srptStandardLaserDoubleStub));
			}
		}

		protected srptStandardLaserDoubleStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptStandardLaserDoubleStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolShowPayPeriod;
		private cEmployeeCheck eCheck;
		private cEarnedTimeService earnService = new cEarnedTimeService();
		const int CNSTMAXSTUBLINES = 23;
		const int CNSTMAXBenLines = 12;
		const int CNSTMaxDedLines = 20;
		const int CNSTMaxPayLines = 12;
		const int CNSTMaxBankLines = 7;
		private int intMaxPayLine;
		private int intMaxDedLine;
		private bool boolTestPrint;
		const int CNSTFICACOL = 46;
		const int CNSTMEDCOL = 47;
		const int CNSTYTDFICACOL = 48;
		const int CNSTYTDMEDCOL = 49;
		const int CNSTDEPTDIVDESCCOL = 50;
		const int CNSTYTDFedGrossCol = 51;
		const int CNSTYTDFicaGrossCol = 52;
		const int CNSTYTDMedGrossCol = 53;
		const int CNSTYTDStateGrossCol = 54;
		const int CNSTMatchDescCol = 8;
		const int CNSTMatchAmountCol = 9;
		const int CNSTMatchYTDAmountCol = 10;
		const int CNSTBankAmountCol = 6;
		const int CNSTBankDescCol = 7;
		private bool boolShowDistRate;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			eCheck = (this.ParentReport as rptEmailPayrollCheck).GetCheckByIndex(this.UserData.ToString().ToIntegerValue());
			boolShowDistRate = (this.ParentReport as rptEmailPayrollCheck).checksReport.checkFormat.ShowDistributionRate;
            boolShowPayPeriod = (this.ParentReport as rptEmailPayrollCheck).PrintPayPeriod;
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= CNSTMaxDedLines; x++)
			{
				(Detail.Controls["txtDedDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMAXBenLines; x++)
			{
				(Detail.Controls["lblBenefit" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblBenAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblBenYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMaxPayLines; x++)
			{
				(Detail.Controls["txtPayDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtHours" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtPayAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtRate" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMaxBankLines; x++)
			{
				(Detail.Controls["txtBank" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtBankAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}

            lblOther.Text = "";
            txtOtherBalance.Text = "";
            txtOtherAccrued.Text = "";
            txtOtherTaken.Text = "";
			// x
			for (x = 1; x <= 5; x++)
			{
				(Detail.Controls["lblCode" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Accrued"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Taken"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			txtTotalHours.Text = "0.00";
			txtTotalAmount.Text = "0.00";
			txtEMatchCurrent.Text = "0.00";
			txtEmatchYTD.Text = "0.00";
			txtCurrentDeductions.Text = "0.00";
			txtTotalYTDDeductions.Text = "0.00";
			txtCheckNo.Text = "";
			txtDirectDeposit.Text = "";
			txtDirectDepSav.Text = "";
			txtChkAmount.Text = "";
			txtEmployeeNo.Text = "";
			txtPayRate.Text = "0.00";
			txtDepartment.Text = "";
			txtDate.Text = "";
			txtName.Text = "";
			lblIndRate.Visible = boolShowDistRate;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			bool boolPrintTest;
			// vbPorter upgrade warning: tempTax As cTax	OnWrite(object)
			cTax tempTax;
			int intMaxPayLines;
			int intCurrentLine = 0;
			int intMaxDeductionLines;
			double dblTotalHours = 0;
			double dblTotalPayAmount = 0;
			int intMaxDepositLines;
			int lngVacID = 0;
			int lngSickID;
			cVacSickItem earnItem;
			int intMaxVacSickLines;
			int intMaxBenefitLines;
			intMaxBenefitLines = CNSTMAXBenLines;
			intMaxPayLines = CNSTMaxPayLines;
			intMaxDeductionLines = CNSTMaxDedLines;
			intMaxDepositLines = CNSTMaxBankLines;
			intMaxVacSickLines = 5;
			ClearFields();
			if (eCheck == null)
				return;
			if (!boolTestPrint)
			{
				// Line1.Visible = False
				cPayRun pRun;
				pRun = eCheck.PayRun;
				if (!boolShowPayPeriod)
				{
					lblCheckMessage.Text = eCheck.CheckMessage;
				}
				else
				{
					if (!(pRun == null))
					{
						if (Information.IsDate(pRun.PeriodStart) && Information.IsDate(pRun.PeriodEnd))
						{
							lblCheckMessage.Text = "Pay Period: " + Strings.Format(pRun.PeriodStart, "MM/dd/yyyy") + " to " + Strings.Format(pRun.PeriodEnd, "MM/dd/yyyy");
						}
					}
				}
				lngVacID = earnService.GetVacationID();
				lngSickID = earnService.GetSickID();
				intCurrentLine = 0;
				cPayStubItem payItem;
				double dblPayExcess = 0;
				double dblExcessHours = 0;
				dblTotalHours = 0;
				dblTotalPayAmount = 0;
				eCheck.PayItems.MoveFirst();
				while (eCheck.PayItems.IsCurrent())
				{
					payItem = (cPayStubItem)eCheck.PayItems.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalPayAmount += payItem.Amount;
					if (intCurrentLine < intMaxPayLines)
					{
						(Detail.Controls["txtPayDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = payItem.Description;
						(Detail.Controls["txtPayAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(payItem.Amount, "0.00");
						if (payItem.PayRate > 0)
						{
							(Detail.Controls["txtRate" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(payItem.PayRate, "0.00");
						}
						else
						{
							(Detail.Controls["txtRate" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
						}
						if (!payItem.IsDollars)
						{
							(Detail.Controls["txtHours" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(payItem.Hours, "0.00");
							dblTotalHours += payItem.Hours;
							if (payItem.Hours < 0)
							{
							}
							else
							{
							}
						}
						else
						{
						}
					}
					else
					{
						dblPayExcess += payItem.Amount;
						(Detail.Controls["txtRate" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
						if (!payItem.IsDollars)
						{
							dblExcessHours += payItem.Hours;
							dblTotalHours += payItem.Hours;
							(Detail.Controls["txtHours" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblExcessHours, "0.00");
						}
						(Detail.Controls["txtPayAmount" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblPayExcess, "0.00");
						if (intCurrentLine == intMaxPayLines)
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = payItem.Description;
						}
						else
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "Other";
						}
					}
					eCheck.PayItems.MoveNext();
				}
				cPayStubItem dedItem;
				double dblDedExcess = 0;
				double dblDedYTDExcess = 0;
				double dblTotalDeductions = 0;
				eCheck.DEDUCTIONS.MoveFirst();
				intCurrentLine = 0;
				while (eCheck.DEDUCTIONS.IsCurrent())
				{
					dedItem = (cPayStubItem)eCheck.DEDUCTIONS.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalDeductions += dedItem.Amount;
					if (intCurrentLine < intMaxDeductionLines)
					{
						(Detail.Controls["txtDedDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = dedItem.Description;
						(Detail.Controls["txtDedAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dedItem.Amount, "0.00");
						(Detail.Controls["txtDedYTD" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dedItem.total, "0.00");
					}
					else
					{
						dblDedExcess += dedItem.Amount;
						dblDedYTDExcess += dedItem.total;
						(Detail.Controls["txtDedAmount" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblDedExcess, "0.00");
						(Detail.Controls["txtDedYTD" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblDedYTDExcess, "0.00");
						if (intCurrentLine == intMaxDeductionLines)
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = dedItem.Description;
						}
						else
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "Other";
						}
					}
					eCheck.DEDUCTIONS.MoveNext();
				}
				txtTotalYTDDeductions.Text = Strings.Format(eCheck.YearToDateDeductions, "0.00");
				cPayStubItem benItem;
				intCurrentLine = 0;
				eCheck.Benefits.MoveFirst();
				double dblBenExcess = 0;
				double dblBenYTDExcess = 0;
				while (eCheck.Benefits.IsCurrent())
				{
					benItem = (cPayStubItem)eCheck.Benefits.GetCurrentItem();
					intCurrentLine += 1;
					if (intCurrentLine < intMaxBenefitLines)
					{
						(Detail.Controls["lblBenefit" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = benItem.Description;
						(Detail.Controls["lblBenAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(benItem.Amount, "0.00");
						(Detail.Controls["lblBenYTD" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(benItem.total, "0.00");
					}
					else
					{
						dblBenExcess += benItem.Amount;
						dblBenYTDExcess += benItem.total;
						(Detail.Controls["lblBenAmount" + intMaxBenefitLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblBenExcess, "0.00");
						(Detail.Controls["lblBenYTD" + intMaxBenefitLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblBenYTDExcess, "0.00");
						if (intCurrentLine == intMaxBenefitLines)
						{
							(Detail.Controls["lblBenefit" + intMaxBenefitLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = benItem.Description;
						}
						else
						{
							(Detail.Controls["lblBenefit" + intMaxBenefitLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "Other";
						}
					}
					eCheck.Benefits.MoveNext();
				}
				eCheck.Deposits.MoveFirst();
				intCurrentLine = 0;
				cCheckDeposit depItem;
				double dblExcessDeposit = 0;
				while (eCheck.Deposits.IsCurrent())
				{
					depItem = (cCheckDeposit)eCheck.Deposits.GetCurrentItem();
					intCurrentLine += 1;
					if (intCurrentLine < intMaxDepositLines)
					{
						(Detail.Controls["txtBank" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						(Detail.Controls["txtBankAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(depItem.Amount, "#,###,##0.00");
					}
					else
					{
						dblExcessDeposit += depItem.Amount;
						if (intCurrentLine == intMaxDepositLines)
						{
							(Detail.Controls["txtBank" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						}
						else
						{
							(Detail.Controls["txtBank" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = FCConvert.ToString(depItem.Bank == "Other");
						}
						(Detail.Controls["txtBankAmount" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblExcessDeposit, "#,###,##0.00");
					}
					eCheck.Deposits.MoveNext();
				}
				// With rptLaserCheck1.GridMisc
				dblDirectDepChk = eCheck.CheckingAmount;
				dblDirectDepSav = eCheck.SavingsAmount;
				dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
				txtDate.Text = "Date " + Strings.Format(eCheck.PayRun.PayDate, "MM/dd/yyyy");
				txtCurrentGross.Text = Strings.Format(eCheck.CurrentGross, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["federal"];
				txtCurrentFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["fica"];
				txtCurrentFica.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["medicare"];
				txtCurrentMedicare.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["state"];
				txtCurrentState.Text = Strings.Format(tempTax.Tax, "0.00");
				txtCurrentDeductions.Text = Strings.Format(dblTotalDeductions, "0.00");
				// txtYTDDeds.Caption = Format(eCheck.YearToDateDeductions, "0.00")
				txtCurrentNet.Text = Strings.Format(eCheck.Net, "0.00");
				eCheck.EarnedTime.MoveFirst();
				intCurrentLine = 0;
				double dblOtherEarned = 0;
				double dblOtherAccrued = 0;
				double dblOtherUsed = 0;
				dblOtherEarned = 0;
				dblOtherAccrued = 0;
				dblOtherUsed = 0;
				while (eCheck.EarnedTime.IsCurrent())
				{
					earnItem = (cVacSickItem)eCheck.EarnedTime.GetCurrentItem();
					if (earnItem.TypeID == lngVacID)
					{
						txtVacationBalance.Text = Strings.Format(earnItem.Balance, "0.00");
						txtVacationAccrued.Text = Strings.Format(earnItem.Accrued, "0.00");
						txtVacationUsed.Text = Strings.Format(earnItem.Used, "0.00");
					}
					else if (earnItem.TypeID == lngSickID)
					{
						txtSickBalance.Text = Strings.Format(earnItem.Balance, "0.00");
						txtSickAccrued.Text = Strings.Format(earnItem.Accrued, "0.00");
						txtSickTaken.Text = Strings.Format(earnItem.Used, "0.00");
					}
					else
					{
						intCurrentLine += 1;
						if (intCurrentLine <= intMaxVacSickLines)
						{
							(Detail.Controls["lblCode" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = earnItem.Name;
							(Detail.Controls["lblCode" + intCurrentLine + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(earnItem.Balance, "0.00");
							(Detail.Controls["lblCode" + intCurrentLine + "Taken"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(earnItem.Used, "0.00");
							(Detail.Controls["lblCode" + intCurrentLine + "Accrued"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(earnItem.Accrued, "0.00");
						}
						else
						{
							dblOtherEarned += earnItem.Balance;
							dblOtherAccrued += earnItem.Accrued;
							dblOtherUsed += earnItem.Used;
                            lblOther.Text = "Other";
							txtOtherBalance.Text = Strings.Format(dblOtherEarned, "0.00");
							txtOtherAccrued.Text = Strings.Format(dblOtherAccrued, "0.00");
							txtOtherTaken.Text = Strings.Format(dblOtherUsed, "0.00");
						}
					}
					eCheck.EarnedTime.MoveNext();
				}
				txtYTDGross.Text = Strings.Format(eCheck.YearToDateGross, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfederal"];
				txtYTDFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfica"];
				txtYTDFICA.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdmedicare"];
				txtYTDMedicare.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdstate"];
				txtYTDState.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfederalgross"];
				txtYTDFedTaxable.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdstategross"];
				txtYTDStateTaxable.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdficagross"];
				txtYTDFicaTaxable.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdmedicaregross"];
				txtYTDMedicareTaxable.Text = Strings.Format(tempTax.Tax, "0.00");
				txtYTDNet.Text = Strings.Format(eCheck.YearToDateNet, "0.00");
				txtTotalHours.Text = Strings.Format(dblTotalHours, "0.00");
				txtTotalAmount.Text = Strings.Format(dblTotalPayAmount, "0.00");
				txtEMatchCurrent.Text = Strings.Format(eCheck.BenefitsTotal, "0.00");
				txtEmatchYTD.Text = Strings.Format(eCheck.YearToDateBenefitsTotal, "0.00");
				txtEmployeeNo.Text = eCheck.EmployeeNumber;
				txtName.Text = eCheck.EmployeeName;
				txtCheckNo.Text = FCConvert.ToString(eCheck.CheckNumber);
				if (dblDirectDeposit > 0)
				{
					// If LCase(MuniName) <> "rangeley" Then
					txtDirectDeposit.Visible = true;
					// txtDirectDepositLabel.Visible = True
					txtChkAmount.Visible = true;
					lblCheckAmount.Visible = true;
					txtDirectDepChkLabel.Visible = true;
					txtDirectDepositSavLabel.Visible = true;
					txtDirectDepSav.Visible = true;
					txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
					txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
					// End If
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				else
				{
					txtDirectDeposit.Visible = false;
					// txtDirectDepositLabel.Visible = False
					txtChkAmount.Visible = false;
					lblCheckAmount.Visible = false;
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				if (eCheck.BasePayrate == 0)
				{
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
				}
				else
				{
					lblPayRate.Visible = true;
					txtPayRate.Visible = true;
					txtPayRate.Text = Strings.Format(eCheck.BasePayrate, "0.00");
				}
				if (fecherFoundation.Strings.Trim(eCheck.DepartmentDivision) == string.Empty)
				{
					lblDepartmentLabel.Visible = false;
					txtDepartment.Text = "";
				}
				else
				{
					lblDepartmentLabel.Visible = true;
					txtDepartment.Text = fecherFoundation.Strings.Trim(eCheck.DepartmentDivision);
				}
			}
		}

		
	}
}
