﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEUC1TotalRecord
	{
		//=========================================================
		private int intSRecords;
		private string strUCAccount = string.Empty;
		private double dblTotalUCWages;
		private double dblTotalExcess;
		private double dblTotalTaxable;
		private double dblUCDue;
		private double dblCSSFAssessment;
		private bool boolDirectReimburser;
		private double dblUCRate;
		private double dblCSSFRate;
		private double dblTotalDue;
		private int[] intMonthlyEmployment = new int[3 + 1];
		private int[] intFemaleEmployment = new int[3 + 1];

		public int NumberEmployeeRecords
		{
			set
			{
				intSRecords = value;
			}
			get
			{
				int NumberEmployeeRecords = 0;
				NumberEmployeeRecords = intSRecords;
				return NumberEmployeeRecords;
			}
		}

		public string UCAccount
		{
			set
			{
				strUCAccount = value;
			}
			get
			{
				string UCAccount = "";
				UCAccount = strUCAccount;
				return UCAccount;
			}
		}

		public double UCWages
		{
			set
			{
				dblTotalUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblTotalUCWages;
				return UCWages;
			}
		}

		public double ExcessWages
		{
			set
			{
				dblTotalExcess = value;
			}
			get
			{
				double ExcessWages = 0;
				ExcessWages = dblTotalExcess;
				return ExcessWages;
			}
		}

		public double TaxableWages
		{
			set
			{
				dblTotalTaxable = value;
			}
			get
			{
				double TaxableWages = 0;
				TaxableWages = dblTotalTaxable;
				return TaxableWages;
			}
		}

		public double UCDue
		{
			set
			{
				dblUCDue = value;
			}
			get
			{
				double UCDue = 0;
				UCDue = dblUCDue;
				return UCDue;
			}
		}

		public double CSSFAssessment
		{
			set
			{
				dblCSSFAssessment = value;
			}
			get
			{
				double CSSFAssessment = 0;
				CSSFAssessment = dblCSSFAssessment;
				return CSSFAssessment;
			}
		}

		public bool IsDirectReimburser
		{
			set
			{
				boolDirectReimburser = value;
			}
			get
			{
				bool IsDirectReimburser = false;
				IsDirectReimburser = boolDirectReimburser;
				return IsDirectReimburser;
			}
		}

		public double UCRate
		{
			set
			{
				dblUCRate = value;
			}
			get
			{
				double UCRate = 0;
				UCRate = dblUCRate;
				return UCRate;
			}
		}

		public double CSSFRate
		{
			set
			{
				dblCSSFRate = value;
			}
			get
			{
				double CSSFRate = 0;
				CSSFRate = dblCSSFRate;
				return CSSFRate;
			}
		}

		public double TotalContCSSFDue
		{
			set
			{
				dblTotalDue = value;
			}
			get
			{
				double TotalContCSSFDue = 0;
				TotalContCSSFDue = dblTotalDue;
				return TotalContCSSFDue;
			}
		}

		public void Set_MonthlyEmployment(int intindex, int intCount)
		{
			if (intindex > 0 && intindex < 4)
			{
				intMonthlyEmployment[intindex - 1] = intCount;
			}
		}

		public int Get_MonthlyEmployment(int intindex)
		{
			int MonthlyEmployment = 0;
			if (intindex > 0 && intindex < 4)
			{
				MonthlyEmployment = intMonthlyEmployment[intindex - 1];
			}
			return MonthlyEmployment;
		}

		public void Set_FemaleCount(int intindex, int intCount)
		{
			if (intindex > 0 && intindex < 4)
			{
				intFemaleEmployment[intindex - 1] = intCount;
			}
		}

		public int Get_FemaleCount(int intindex)
		{
			int FemaleCount = 0;
			if (intindex > 0 && intindex < 4)
			{
				FemaleCount = intFemaleEmployment[intindex - 1];
			}
			return FemaleCount;
		}

		public string ToString()
		{
			string ToString = "";
			string strReturn;
			int x;
			strReturn = "T";
			strReturn += Strings.Format(intSRecords, Strings.StrDup(7, "0"));
			strReturn += "WAGE";
			strReturn += strUCAccount;
			strReturn += Strings.StrDup(4, " ");
			// not used by maine
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblTotalUCWages, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblTotalExcess, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblTotalTaxable, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.StrDup(19, " ");
			// not used by maine
			if (!boolDirectReimburser)
			{
				strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblUCDue, "0.00")) * 100, Strings.StrDup(13, "0"));
				strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblCSSFAssessment, "0.00")) * 100, Strings.StrDup(11, "0"));
				strReturn += Strings.StrDup(33, " ");
				// not used by maine
				strReturn += Conversion.Val(Strings.Format(dblUCRate * 100 * 100, "0"));
				if (dblUCRate == 0)
					dblCSSFRate = 0;
				strReturn += Conversion.Val(Strings.Format(dblCSSFRate * 100 * 100, "0"));
				strReturn += Strings.StrDup(22, " ");
				// not used by maine
			}
			else
			{
				strReturn += Strings.StrDup(13, "0");
				strReturn += Strings.StrDup(11, "0");
				strReturn += Strings.StrDup(33, " ");
				// not used by maine
				strReturn += "0000";
				strReturn += "0000";
				strReturn += Strings.StrDup(22, " ");
				// not used by maine
			}
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblTotalDue, "0.00")) * 100, Strings.StrDup(11, "0"));
			strReturn += Strings.StrDup(41, " ");
			// not used by maine
			for (x = 1; x <= 3; x++)
			{
				strReturn += Strings.Format(Get_MonthlyEmployment(x), Strings.StrDup(7, "0"));
			}
			// x
			for (x = 1; x <= 3; x++)
			{
				strReturn += Strings.Format(Get_FemaleCount(x), Strings.StrDup(7, "0"));
			}
			// x
			strReturn += Strings.StrDup(7, " ");
			// not used by maine
			ToString = strReturn;
			return ToString;
		}
	}
}
