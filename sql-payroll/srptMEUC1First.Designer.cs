﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMEUC1First.
	/// </summary>
	partial class srptMEUC1First
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMEUC1First));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUCEmployerAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFederalEmployerID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtQuarterNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUnemploymentCompensationGrossWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUnemploymentCompensationGrossCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExcessWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExcessWagesCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTaxableWagesPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTaxableWagesPaidCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContributionsDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContributionsDueCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCSSFAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCSSFAssessmentCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalDueCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLine9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCSSFRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEMail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFirstMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSecMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtThirdMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFemalesFirstMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFemalesSecMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFemalesThirdMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label148 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMaineLicense = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label149 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line195 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line196 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label150 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line197 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line198 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line199 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Line200 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUPAFAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUPAFAssessmentCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUPAFRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEmployerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnemploymentCompensationGrossWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnemploymentCompensationGrossCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcessWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcessWagesCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableWagesPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableWagesPaidCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContributionsDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContributionsDueCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFAssessmentCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDueCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesFirstMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesSecMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesThirdMo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaineLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFAssessment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFAssessmentCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape17,
            this.Barcode1,
            this.Label1,
            this.Shape18,
            this.Shape19,
            this.Label2,
            this.Label4,
            this.Label5,
            this.txtYear,
            this.Label7,
            this.txtUCEmployerAccount,
            this.txtFederalEmployerID,
            this.Label8,
            this.Label24,
            this.txtQuarterNum,
            this.txtUnemploymentCompensationGrossWages,
            this.txtUnemploymentCompensationGrossCents,
            this.txtExcessWages,
            this.txtExcessWagesCents,
            this.txtTaxableWagesPaid,
            this.txtTaxableWagesPaidCents,
            this.txtContributionsDue,
            this.txtContributionsDueCents,
            this.txtCSSFAssessment,
            this.txtCSSFAssessmentCents,
            this.txtTotalDue,
            this.txtTotalDueCents,
            this.Label27,
            this.txtLine9,
            this.Label62,
            this.txtCSSFRate,
            this.Label112,
            this.Label113,
            this.Label117,
            this.Field33,
            this.Label26,
            this.Label73,
            this.txtPhone,
            this.txtEMail,
            this.txtDate,
            this.Line3,
            this.Label119,
            this.Line29,
            this.Label120,
            this.Label121,
            this.txtPeriodStart,
            this.txtPeriodEnd,
            this.txtName,
            this.txtAddress,
            this.txtCity,
            this.txtState,
            this.txtZip,
            this.txtFirstMo,
            this.txtSecMo,
            this.txtThirdMo,
            this.txtFemalesFirstMo,
            this.txtFemalesSecMo,
            this.txtFemalesThirdMo,
            this.Label18,
            this.Label48,
            this.Label66,
            this.Label74,
            this.Label75,
            this.Label76,
            this.Label9,
            this.Label19,
            this.Label49,
            this.Label67,
            this.Label20,
            this.Label50,
            this.Label59,
            this.Label130,
            this.Label29,
            this.Label30,
            this.Label51,
            this.Label60,
            this.Label21,
            this.Label52,
            this.Label61,
            this.Label53,
            this.Label110,
            this.Label131,
            this.Label132,
            this.Label37,
            this.Label123,
            this.Label124,
            this.Label125,
            this.Label129,
            this.Label145,
            this.Label146,
            this.Label147,
            this.Label148,
            this.txtEIN,
            this.Label54,
            this.Label106,
            this.txtMaineLicense,
            this.Label116,
            this.Label149,
            this.Line195,
            this.Label118,
            this.Line196,
            this.Label150,
            this.Line197,
            this.Label151,
            this.Line198,
            this.Label152,
            this.Line199,
            this.Image1,
            this.Line200,
            this.Field34,
            this.Label153,
            this.Field35,
            this.Label154,
            this.Label155,
            this.label3,
            this.txtUPAFAssessment,
            this.txtUPAFAssessmentCents,
            this.label6,
            this.txtUPAFRate,
            this.label10,
            this.label11,
            this.label12,
            this.label13});
            this.Detail.Height = 9.90625F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Shape17
            // 
            this.Shape17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape17.Height = 0.125F;
            this.Shape17.Left = 0.01388889F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 0.01388889F;
            this.Shape17.Width = 0.125F;
            // 
            // Barcode1
            // 
            this.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
            this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode1.Height = 0.625F;
            this.Barcode1.Left = 5.636F;
            this.Barcode1.Name = "Barcode1";
            this.Barcode1.QuietZoneBottom = 0F;
            this.Barcode1.QuietZoneLeft = 0F;
            this.Barcode1.QuietZoneRight = 0F;
            this.Barcode1.QuietZoneTop = 0F;
            this.Barcode1.Text = "2006400";
            this.Barcode1.Top = 0F;
            this.Barcode1.Width = 1.583333F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.25F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 7.167F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 12pt; font-weight: bold;" +
    " text-align: right";
            this.Label1.Text = "15";
            this.Label1.Top = 0.174F;
            this.Label1.Width = 0.3333335F;
            // 
            // Shape18
            // 
            this.Shape18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape18.Height = 0.125F;
            this.Shape18.Left = 0F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 9.777778F;
            this.Shape18.Width = 0.125F;
            // 
            // Shape19
            // 
            this.Shape19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape19.Height = 0.125F;
            this.Shape19.Left = 7.361111F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 9.777778F;
            this.Shape19.Width = 0.125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2.375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
            this.Label2.Text = "UNEMPLOYMENT";
            this.Label2.Top = 0F;
            this.Label2.Width = 2.333333F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1666667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 2.208333F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: center; vertical-align: top";
            this.Label4.Text = "CONTRIBUTIONS";
            this.Label4.Top = 0.125F;
            this.Label4.Width = 2.666667F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1666667F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 2.208333F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 8.5pt; font-weight: bold; text-align: center; vertical-align: top";
            this.Label5.Text = "REPORT";
            this.Label5.Top = 0.25F;
            this.Label5.Width = 2.666667F;
            // 
            // txtYear
            // 
            this.txtYear.Height = 0.3125F;
            this.txtYear.HyperLink = null;
            this.txtYear.Left = 0.75F;
            this.txtYear.Name = "txtYear";
            this.txtYear.Style = "font-family: \\000027Arial\\000027; font-size: 18pt; font-weight: bold";
            this.txtYear.Text = "2021";
            this.txtYear.Top = 0.3333333F;
            this.txtYear.Width = 0.625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1770833F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 3.833F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \\000027Arial\\000027; font-size: 8.5pt; font-weight: bold; text-align" +
    ": left; vertical-align: top";
            this.Label7.Text = "UC Employer Account No:";
            this.Label7.Top = 1.042F;
            this.Label7.Width = 1.6875F;
            // 
            // txtUCEmployerAccount
            // 
            this.txtUCEmployerAccount.Height = 0.1666667F;
            this.txtUCEmployerAccount.Left = 6.417F;
            this.txtUCEmployerAccount.Name = "txtUCEmployerAccount";
            this.txtUCEmployerAccount.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: mi" +
    "ddle";
            this.txtUCEmployerAccount.Text = "9999999999";
            this.txtUCEmployerAccount.Top = 1.002F;
            this.txtUCEmployerAccount.Width = 1.0625F;
            // 
            // txtFederalEmployerID
            // 
            this.txtFederalEmployerID.Height = 0.1666667F;
            this.txtFederalEmployerID.Left = 6.417F;
            this.txtFederalEmployerID.Name = "txtFederalEmployerID";
            this.txtFederalEmployerID.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: mi" +
    "ddle";
            this.txtFederalEmployerID.Text = "99 999999999";
            this.txtFederalEmployerID.Top = 1.315F;
            this.txtFederalEmployerID.Width = 1.125F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1770833F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 3.833F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \\000027Arial\\000027; font-size: 8.5pt; font-weight: bold; text-align" +
    ": left; vertical-align: top";
            this.Label8.Text = "Federal Employer ID No:";
            this.Label8.Top = 1.335F;
            this.Label8.Width = 1.6875F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1666667F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 3.145833F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-size: 10pt; text-align: left; vertical-align: top";
            this.Label24.Text = "QUARTER #";
            this.Label24.Top = 0.53125F;
            this.Label24.Width = 0.9375F;
            // 
            // txtQuarterNum
            // 
            this.txtQuarterNum.Height = 0.1666667F;
            this.txtQuarterNum.HyperLink = null;
            this.txtQuarterNum.Left = 4.302083F;
            this.txtQuarterNum.Name = "txtQuarterNum";
            this.txtQuarterNum.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: bottom";
            this.txtQuarterNum.Text = "9";
            this.txtQuarterNum.Top = 0.53125F;
            this.txtQuarterNum.Width = 0.25F;
            // 
            // txtUnemploymentCompensationGrossWages
            // 
            this.txtUnemploymentCompensationGrossWages.Height = 0.1666667F;
            this.txtUnemploymentCompensationGrossWages.Left = 4.979167F;
            this.txtUnemploymentCompensationGrossWages.Name = "txtUnemploymentCompensationGrossWages";
            this.txtUnemploymentCompensationGrossWages.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtUnemploymentCompensationGrossWages.Text = "9999999999999";
            this.txtUnemploymentCompensationGrossWages.Top = 3.5F;
            this.txtUnemploymentCompensationGrossWages.Width = 1.75F;
            // 
            // txtUnemploymentCompensationGrossCents
            // 
            this.txtUnemploymentCompensationGrossCents.Height = 0.1666667F;
            this.txtUnemploymentCompensationGrossCents.Left = 6.875F;
            this.txtUnemploymentCompensationGrossCents.Name = "txtUnemploymentCompensationGrossCents";
            this.txtUnemploymentCompensationGrossCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtUnemploymentCompensationGrossCents.Text = "99";
            this.txtUnemploymentCompensationGrossCents.Top = 3.5F;
            this.txtUnemploymentCompensationGrossCents.Width = 0.4375F;
            // 
            // txtExcessWages
            // 
            this.txtExcessWages.Height = 0.1666667F;
            this.txtExcessWages.Left = 4.979F;
            this.txtExcessWages.Name = "txtExcessWages";
            this.txtExcessWages.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtExcessWages.Text = "9999999999999";
            this.txtExcessWages.Top = 3.833F;
            this.txtExcessWages.Width = 1.75F;
            // 
            // txtExcessWagesCents
            // 
            this.txtExcessWagesCents.Height = 0.1666667F;
            this.txtExcessWagesCents.Left = 6.874833F;
            this.txtExcessWagesCents.Name = "txtExcessWagesCents";
            this.txtExcessWagesCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtExcessWagesCents.Text = "99";
            this.txtExcessWagesCents.Top = 3.833F;
            this.txtExcessWagesCents.Width = 0.4375F;
            // 
            // txtTaxableWagesPaid
            // 
            this.txtTaxableWagesPaid.Height = 0.1666667F;
            this.txtTaxableWagesPaid.Left = 4.979F;
            this.txtTaxableWagesPaid.Name = "txtTaxableWagesPaid";
            this.txtTaxableWagesPaid.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtTaxableWagesPaid.Text = "9999999999999";
            this.txtTaxableWagesPaid.Top = 4.157F;
            this.txtTaxableWagesPaid.Width = 1.75F;
            // 
            // txtTaxableWagesPaidCents
            // 
            this.txtTaxableWagesPaidCents.Height = 0.1666667F;
            this.txtTaxableWagesPaidCents.Left = 6.874833F;
            this.txtTaxableWagesPaidCents.Name = "txtTaxableWagesPaidCents";
            this.txtTaxableWagesPaidCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtTaxableWagesPaidCents.Text = "99";
            this.txtTaxableWagesPaidCents.Top = 4.157F;
            this.txtTaxableWagesPaidCents.Width = 0.4375F;
            // 
            // txtContributionsDue
            // 
            this.txtContributionsDue.Height = 0.1666667F;
            this.txtContributionsDue.Left = 4.979F;
            this.txtContributionsDue.Name = "txtContributionsDue";
            this.txtContributionsDue.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtContributionsDue.Text = "9999999999999";
            this.txtContributionsDue.Top = 4.49F;
            this.txtContributionsDue.Width = 1.75F;
            // 
            // txtContributionsDueCents
            // 
            this.txtContributionsDueCents.Height = 0.1666667F;
            this.txtContributionsDueCents.Left = 6.874833F;
            this.txtContributionsDueCents.Name = "txtContributionsDueCents";
            this.txtContributionsDueCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtContributionsDueCents.Text = "99";
            this.txtContributionsDueCents.Top = 4.49F;
            this.txtContributionsDueCents.Width = 0.4375F;
            // 
            // txtCSSFAssessment
            // 
            this.txtCSSFAssessment.Height = 0.1666667F;
            this.txtCSSFAssessment.Left = 4.979F;
            this.txtCSSFAssessment.Name = "txtCSSFAssessment";
            this.txtCSSFAssessment.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtCSSFAssessment.Text = "9999999999999";
            this.txtCSSFAssessment.Top = 4.803F;
            this.txtCSSFAssessment.Width = 1.75F;
            // 
            // txtCSSFAssessmentCents
            // 
            this.txtCSSFAssessmentCents.Height = 0.1666667F;
            this.txtCSSFAssessmentCents.Left = 6.875F;
            this.txtCSSFAssessmentCents.Name = "txtCSSFAssessmentCents";
            this.txtCSSFAssessmentCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtCSSFAssessmentCents.Text = "99";
            this.txtCSSFAssessmentCents.Top = 4.803F;
            this.txtCSSFAssessmentCents.Width = 0.4375F;
            // 
            // txtTotalDue
            // 
            this.txtTotalDue.Height = 0.1666667F;
            this.txtTotalDue.Left = 4.979F;
            this.txtTotalDue.Name = "txtTotalDue";
            this.txtTotalDue.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtTotalDue.Text = "9999999999999";
            this.txtTotalDue.Top = 5.657F;
            this.txtTotalDue.Width = 1.75F;
            // 
            // txtTotalDueCents
            // 
            this.txtTotalDueCents.Height = 0.1666667F;
            this.txtTotalDueCents.Left = 6.874833F;
            this.txtTotalDueCents.Name = "txtTotalDueCents";
            this.txtTotalDueCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtTotalDueCents.Text = "99";
            this.txtTotalDueCents.Top = 5.657F;
            this.txtTotalDueCents.Width = 0.4375F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0.2289998F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label27.Text = "UC Contribution rate";
            this.Label27.Top = 4.49F;
            this.Label27.Width = 1.0625F;
            // 
            // txtLine9
            // 
            this.txtLine9.Height = 0.1666667F;
            this.txtLine9.Left = 1.124833F;
            this.txtLine9.Name = "txtLine9";
            this.txtLine9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtLine9.Text = ".9999";
            this.txtLine9.Top = 4.49F;
            this.txtLine9.Width = 0.5625F;
            // 
            // Label62
            // 
            this.Label62.Height = 0.1666667F;
            this.Label62.HyperLink = null;
            this.Label62.Left = 4.385F;
            this.Label62.Name = "Label62";
            this.Label62.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: middle";
            this.Label62.Text = "7b.";
            this.Label62.Top = 4.803F;
            this.Label62.Width = 0.25F;
            // 
            // txtCSSFRate
            // 
            this.txtCSSFRate.Height = 0.1666667F;
            this.txtCSSFRate.Left = 0.625F;
            this.txtCSSFRate.Name = "txtCSSFRate";
            this.txtCSSFRate.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; font-weight: bold" +
    "; text-align: right; vertical-align: middle";
            this.txtCSSFRate.Text = ".0006";
            this.txtCSSFRate.Top = 4.813F;
            this.txtCSSFRate.Width = 0.5625F;
            // 
            // Label112
            // 
            this.Label112.Height = 0.1875F;
            this.Label112.HyperLink = null;
            this.Label112.Left = 0.2495F;
            this.Label112.Name = "Label112";
            this.Label112.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label112.Text = "CSSF rate";
            this.Label112.Top = 4.803F;
            this.Label112.Width = 0.5625F;
            // 
            // Label113
            // 
            this.Label113.Height = 0.1875F;
            this.Label113.HyperLink = null;
            this.Label113.Left = 0.062F;
            this.Label113.Name = "Label113";
            this.Label113.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label113.Text = "7a.";
            this.Label113.Top = 4.803F;
            this.Label113.Width = 0.25F;
            // 
            // Label117
            // 
            this.Label117.Height = 0.1875F;
            this.Label117.HyperLink = null;
            this.Label117.Left = 1.937083F;
            this.Label117.Name = "Label117";
            this.Label117.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label117.Text = "CSSF Assessment due (line 5 times line 7a)";
            this.Label117.Top = 4.803F;
            this.Label117.Width = 2.0625F;
            // 
            // Field33
            // 
            this.Field33.Height = 0.2670004F;
            this.Field33.Left = 0.25F;
            this.Field33.Name = "Field33";
            this.Field33.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: middle";
            this.Field33.Text = "Note: The CSSF and UPAF Assessment does not apply to direct reimbursable employer" +
    "s. See instructions.";
            this.Field33.Top = 5.352F;
            this.Field33.Width = 4.5625F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1666667F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label26.Text = "Signature";
            this.Label26.Top = 6.488F;
            this.Label26.Width = 0.5416667F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.1666667F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 4.552F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: right; vertical-ali" +
    "gn: middle";
            this.Label73.Text = "Contact person email:";
            this.Label73.Top = 6.793F;
            this.Label73.Width = 1.354F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1875F;
            this.txtPhone.Left = 3.604167F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: top";
            this.txtPhone.Text = "999 999 9999";
            this.txtPhone.Top = 6.773F;
            this.txtPhone.Width = 1.125F;
            // 
            // txtEMail
            // 
            this.txtEMail.CanGrow = false;
            this.txtEMail.Height = 0.1875F;
            this.txtEMail.Left = 5.987F;
            this.txtEMail.MultiLine = false;
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: top; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.txtEMail.Text = "XXXXXXXXXXXXXXXX";
            this.txtEMail.Top = 6.773F;
            this.txtEMail.Width = 1.4875F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 5.8125F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: top";
            this.txtDate.Text = "99 99  9999";
            this.txtDate.Top = 6.47F;
            this.txtDate.Width = 1.3125F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.6020833F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 6.633834F;
            this.Line3.Width = 4.15F;
            this.Line3.X1 = 0.6020833F;
            this.Line3.X2 = 4.752083F;
            this.Line3.Y1 = 6.633834F;
            this.Line3.Y2 = 6.633834F;
            // 
            // Label119
            // 
            this.Label119.Height = 0.1666667F;
            this.Label119.HyperLink = null;
            this.Label119.Left = 5.375F;
            this.Label119.Name = "Label119";
            this.Label119.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: right; vertical-ali" +
    "gn: middle";
            this.Label119.Text = "Date:";
            this.Label119.Top = 6.488F;
            this.Label119.Width = 0.3541665F;
            // 
            // Line29
            // 
            this.Line29.Height = 0F;
            this.Line29.Left = 5.777778F;
            this.Line29.LineWeight = 1F;
            this.Line29.Name = "Line29";
            this.Line29.Top = 6.633834F;
            this.Line29.Visible = false;
            this.Line29.Width = 1.524305F;
            this.Line29.X1 = 5.777778F;
            this.Line29.X2 = 7.302083F;
            this.Line29.Y1 = 6.633834F;
            this.Line29.Y2 = 6.633834F;
            // 
            // Label120
            // 
            this.Label120.Height = 0.1666667F;
            this.Label120.HyperLink = null;
            this.Label120.Left = 0F;
            this.Label120.Name = "Label120";
            this.Label120.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label120.Text = "Print Name:";
            this.Label120.Top = 6.793F;
            this.Label120.Width = 0.6041667F;
            // 
            // Label121
            // 
            this.Label121.Height = 0.1666667F;
            this.Label121.HyperLink = null;
            this.Label121.Left = 3.0625F;
            this.Label121.Name = "Label121";
            this.Label121.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label121.Text = "Telephone:";
            this.Label121.Top = 6.793F;
            this.Label121.Width = 0.6041667F;
            // 
            // txtPeriodStart
            // 
            this.txtPeriodStart.Height = 0.1666667F;
            this.txtPeriodStart.Left = 4.99F;
            this.txtPeriodStart.Name = "txtPeriodStart";
            this.txtPeriodStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodStart.Text = "99 99   9999";
            this.txtPeriodStart.Top = 1.648F;
            this.txtPeriodStart.Width = 1.052083F;
            // 
            // txtPeriodEnd
            // 
            this.txtPeriodEnd.Height = 0.1666667F;
            this.txtPeriodEnd.Left = 6.323F;
            this.txtPeriodEnd.Name = "txtPeriodEnd";
            this.txtPeriodEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodEnd.Text = "99 99   9999";
            this.txtPeriodEnd.Top = 1.648F;
            this.txtPeriodEnd.Width = 1.09375F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1666667F;
            this.txtName.Left = 0.083F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: to" +
    "p";
            this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName.Top = 0.845F;
            this.txtName.Width = 4.25F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1666667F;
            this.txtAddress.Left = 0.083F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: to" +
    "p";
            this.txtAddress.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtAddress.Top = 1.313F;
            this.txtAddress.Width = 3.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1666667F;
            this.txtCity.Left = 0.083F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: to" +
    "p";
            this.txtCity.Text = "XXXXXXXXXXXXXXXXXXXXXX";
            this.txtCity.Top = 1.783F;
            this.txtCity.Width = 2.583333F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1666667F;
            this.txtState.Left = 2.292F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: to" +
    "p";
            this.txtState.Text = "ME";
            this.txtState.Top = 1.783F;
            this.txtState.Width = 0.25F;
            // 
            // txtZip
            // 
            this.txtZip.Height = 0.1666667F;
            this.txtZip.Left = 2.802416F;
            this.txtZip.Name = "txtZip";
            this.txtZip.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; vertical-align: to" +
    "p";
            this.txtZip.Text = "99999";
            this.txtZip.Top = 1.783F;
            this.txtZip.Width = 0.5F;
            // 
            // txtFirstMo
            // 
            this.txtFirstMo.Height = 0.1666667F;
            this.txtFirstMo.Left = 4.666667F;
            this.txtFirstMo.Name = "txtFirstMo";
            this.txtFirstMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtFirstMo.Text = "999999";
            this.txtFirstMo.Top = 2.667F;
            this.txtFirstMo.Width = 0.8333333F;
            // 
            // txtSecMo
            // 
            this.txtSecMo.Height = 0.1666667F;
            this.txtSecMo.Left = 5.583333F;
            this.txtSecMo.Name = "txtSecMo";
            this.txtSecMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtSecMo.Text = "999999";
            this.txtSecMo.Top = 2.667F;
            this.txtSecMo.Width = 0.8333333F;
            // 
            // txtThirdMo
            // 
            this.txtThirdMo.Height = 0.1666667F;
            this.txtThirdMo.Left = 6.5625F;
            this.txtThirdMo.Name = "txtThirdMo";
            this.txtThirdMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtThirdMo.Text = "999999";
            this.txtThirdMo.Top = 2.667F;
            this.txtThirdMo.Width = 0.75F;
            // 
            // txtFemalesFirstMo
            // 
            this.txtFemalesFirstMo.Height = 0.1666667F;
            this.txtFemalesFirstMo.Left = 4.666667F;
            this.txtFemalesFirstMo.Name = "txtFemalesFirstMo";
            this.txtFemalesFirstMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtFemalesFirstMo.Text = "999999";
            this.txtFemalesFirstMo.Top = 3F;
            this.txtFemalesFirstMo.Width = 0.8333333F;
            // 
            // txtFemalesSecMo
            // 
            this.txtFemalesSecMo.Height = 0.1666667F;
            this.txtFemalesSecMo.Left = 5.583333F;
            this.txtFemalesSecMo.Name = "txtFemalesSecMo";
            this.txtFemalesSecMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtFemalesSecMo.Text = "999999";
            this.txtFemalesSecMo.Top = 3F;
            this.txtFemalesSecMo.Width = 0.8333333F;
            // 
            // txtFemalesThirdMo
            // 
            this.txtFemalesThirdMo.Height = 0.1666667F;
            this.txtFemalesThirdMo.Left = 6.5625F;
            this.txtFemalesThirdMo.Name = "txtFemalesThirdMo";
            this.txtFemalesThirdMo.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtFemalesThirdMo.Text = "999999";
            this.txtFemalesThirdMo.Top = 3F;
            this.txtFemalesThirdMo.Width = 0.75F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.3333333F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 0.229F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \\000027Arial\\000027; font-size: 6pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label18.Text = resources.GetString("Label18.Text");
            this.Label18.Top = 2.504F;
            this.Label18.Width = 3.833333F;
            // 
            // Label48
            // 
            this.Label48.Height = 0.1666667F;
            this.Label48.HyperLink = null;
            this.Label48.Left = 0.06233332F;
            this.Label48.Name = "Label48";
            this.Label48.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label48.Text = "1.";
            this.Label48.Top = 2.504F;
            this.Label48.Width = 0.25F;
            // 
            // Label66
            // 
            this.Label66.Height = 0.1666667F;
            this.Label66.HyperLink = null;
            this.Label66.Left = 4.385F;
            this.Label66.Name = "Label66";
            this.Label66.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: top";
            this.Label66.Text = "1.";
            this.Label66.Top = 2.667F;
            this.Label66.Width = 0.239583F;
            // 
            // Label74
            // 
            this.Label74.Height = 0.1875F;
            this.Label74.HyperLink = null;
            this.Label74.Left = 4.937F;
            this.Label74.Name = "Label74";
            this.Label74.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: top";
            this.Label74.Text = "1st Month";
            this.Label74.Top = 2.376F;
            this.Label74.Width = 0.75F;
            // 
            // Label75
            // 
            this.Label75.Height = 0.1875F;
            this.Label75.HyperLink = null;
            this.Label75.Left = 5.832833F;
            this.Label75.Name = "Label75";
            this.Label75.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: top";
            this.Label75.Text = "2nd Month";
            this.Label75.Top = 2.376F;
            this.Label75.Width = 0.75F;
            // 
            // Label76
            // 
            this.Label76.Height = 0.1875F;
            this.Label76.HyperLink = null;
            this.Label76.Left = 6.791167F;
            this.Label76.Name = "Label76";
            this.Label76.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: top";
            this.Label76.Text = "3rd Month";
            this.Label76.Top = 2.376F;
            this.Label76.Width = 0.625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.5F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \\000027Arial\\000027; font-size: 10pt; font-weight: bold; text-align:" +
    " left; vertical-align: middle";
            this.Label9.Text = "Form ME UC-1";
            this.Label9.Top = 0F;
            this.Label9.Width = 1.5625F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1666667F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0.2286667F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label19.Text = "Number of female employees included on line 1.  If none, enter zero (0)";
            this.Label19.Top = 3.004F;
            this.Label19.Width = 3.583333F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.1666667F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 0.062F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label49.Text = "2.";
            this.Label49.Top = 3.004F;
            this.Label49.Width = 0.25F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.1666667F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 4.385417F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: middle";
            this.Label67.Text = "2.";
            this.Label67.Top = 3F;
            this.Label67.Width = 0.25F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1666667F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0.2291667F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label20.Text = "Total unemployment contributions gross wages paid this quarter";
            this.Label20.Top = 3.333333F;
            this.Label20.Width = 3.583333F;
            // 
            // Label50
            // 
            this.Label50.Height = 0.1666667F;
            this.Label50.HyperLink = null;
            this.Label50.Left = 0.0625F;
            this.Label50.Name = "Label50";
            this.Label50.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label50.Text = "3.";
            this.Label50.Top = 3.333333F;
            this.Label50.Width = 0.25F;
            // 
            // Label59
            // 
            this.Label59.Height = 0.1666667F;
            this.Label59.HyperLink = null;
            this.Label59.Left = 4.385417F;
            this.Label59.Name = "Label59";
            this.Label59.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: top";
            this.Label59.Text = "3.";
            this.Label59.Top = 3.5F;
            this.Label59.Width = 0.25F;
            // 
            // Label130
            // 
            this.Label130.Height = 0.1666667F;
            this.Label130.HyperLink = null;
            this.Label130.Left = 0.2291667F;
            this.Label130.Name = "Label130";
            this.Label130.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label130.Text = "(from Schedule 2 line 15)";
            this.Label130.Top = 3.5F;
            this.Label130.Width = 3.583333F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.125F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0.2498331F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label29.Text = "EXCESS WAGES (SEE INSTRUCTIONS)";
            this.Label29.Top = 3.838F;
            this.Label29.Width = 3.5F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.125F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 0.2498331F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: top";
            this.Label30.Text = "NOTE: THE TAXABLE WAGE BASE IS $12,000 FOR EACH EMPLOYEE.";
            this.Label30.Top = 4.002998F;
            this.Label30.Width = 3.5F;
            // 
            // Label51
            // 
            this.Label51.Height = 0.1875F;
            this.Label51.HyperLink = null;
            this.Label51.Left = 0.06233311F;
            this.Label51.Name = "Label51";
            this.Label51.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label51.Text = "4.";
            this.Label51.Top = 3.838F;
            this.Label51.Width = 0.25F;
            // 
            // Label60
            // 
            this.Label60.Height = 0.1666667F;
            this.Label60.HyperLink = null;
            this.Label60.Left = 4.38525F;
            this.Label60.Name = "Label60";
            this.Label60.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: top";
            this.Label60.Text = "4.";
            this.Label60.Top = 3.838F;
            this.Label60.Width = 0.25F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 0.25F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label21.Text = "Taxable wages paid in this quarter (line 3 minus line 4)";
            this.Label21.Top = 4.168F;
            this.Label21.Width = 3.5625F;
            // 
            // Label52
            // 
            this.Label52.Height = 0.1875F;
            this.Label52.HyperLink = null;
            this.Label52.Left = 0.0625F;
            this.Label52.Name = "Label52";
            this.Label52.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label52.Text = "5.";
            this.Label52.Top = 4.168F;
            this.Label52.Width = 0.25F;
            // 
            // Label61
            // 
            this.Label61.Height = 0.1666667F;
            this.Label61.HyperLink = null;
            this.Label61.Left = 4.38525F;
            this.Label61.Name = "Label61";
            this.Label61.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: top";
            this.Label61.Text = "5.";
            this.Label61.Top = 4.157F;
            this.Label61.Width = 0.25F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.1875F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 0.06233311F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.Label53.Text = "6a.";
            this.Label53.Top = 4.49F;
            this.Label53.Width = 0.25F;
            // 
            // Label110
            // 
            this.Label110.Height = 0.1666667F;
            this.Label110.HyperLink = null;
            this.Label110.Left = 4.38525F;
            this.Label110.Name = "Label110";
            this.Label110.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: middle";
            this.Label110.Text = "6b.";
            this.Label110.Top = 4.49F;
            this.Label110.Width = 0.25F;
            // 
            // Label131
            // 
            this.Label131.Height = 0.1875F;
            this.Label131.HyperLink = null;
            this.Label131.Left = 0.25F;
            this.Label131.Name = "Label131";
            this.Label131.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label131.Text = "Total contributions, CSSF and UPAF assessment due (Add lines 6b, 7b, and 7d)";
            this.Label131.Top = 5.667F;
            this.Label131.Width = 3.5625F;
            // 
            // Label132
            // 
            this.Label132.Height = 0.1875F;
            this.Label132.HyperLink = null;
            this.Label132.Left = 0.0625F;
            this.Label132.Name = "Label132";
            this.Label132.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label132.Text = "8.";
            this.Label132.Top = 5.667F;
            this.Label132.Width = 0.25F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.1666667F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 4.135F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; text-decorati" +
    "on: underline; vertical-align: top";
            this.Label37.Text = "Treasurer, State of Maine";
            this.Label37.Top = 9.044001F;
            this.Label37.Width = 1.166667F;
            // 
            // Label123
            // 
            this.Label123.Height = 0.1666667F;
            this.Label123.HyperLink = null;
            this.Label123.Left = 3.780833F;
            this.Label123.Name = "Label123";
            this.Label123.Style = "font-size: 7pt; font-weight: bold";
            this.Label123.Text = "If enclosing a check, make check payable to:";
            this.Label123.Top = 8.929417F;
            this.Label123.Width = 2.25F;
            // 
            // Label124
            // 
            this.Label124.Height = 0.4166667F;
            this.Label124.HyperLink = null;
            this.Label124.Left = 4.135F;
            this.Label124.Name = "Label124";
            this.Label124.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label124.Text = "MAINE REVENUE SERVICES        P.O. Box 1065                  Augusta, ME 04332-10" +
    "65";
            this.Label124.Top = 9.304417F;
            this.Label124.Width = 1.40625F;
            // 
            // Label125
            // 
            this.Label125.Height = 0.1666667F;
            this.Label125.HyperLink = null;
            this.Label125.Left = 4.082916F;
            this.Label125.Name = "Label125";
            this.Label125.Style = "font-size: 7pt; font-weight: bold";
            this.Label125.Text = "and MAIL WITH RETURN TO:";
            this.Label125.Top = 9.175944F;
            this.Label125.Width = 1.541667F;
            // 
            // Label129
            // 
            this.Label129.Height = 0.1666667F;
            this.Label129.HyperLink = null;
            this.Label129.Left = 6.04125F;
            this.Label129.Name = "Label129";
            this.Label129.Style = "font-size: 7pt; font-weight: bold";
            this.Label129.Text = "If not enclosing a check,";
            this.Label129.Top = 8.929417F;
            this.Label129.Width = 1.375F;
            // 
            // Label145
            // 
            this.Label145.Height = 0.1666667F;
            this.Label145.HyperLink = null;
            this.Label145.Left = 6.124583F;
            this.Label145.Name = "Label145";
            this.Label145.Style = "font-size: 7pt; font-weight: bold";
            this.Label145.Text = "MAIL RETURN TO:";
            this.Label145.Top = 9.044001F;
            this.Label145.Width = 1.229167F;
            // 
            // Label146
            // 
            this.Label146.Height = 0.4166667F;
            this.Label146.HyperLink = null;
            this.Label146.Left = 6.04125F;
            this.Label146.Name = "Label146";
            this.Label146.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label146.Text = "MAINE REVENUE SERVICES        P.O. Box 1064                  Augusta, ME 04332-10" +
    "64";
            this.Label146.Top = 9.241917F;
            this.Label146.Width = 1.40625F;
            // 
            // Label147
            // 
            this.Label147.Height = 0.1666667F;
            this.Label147.HyperLink = null;
            this.Label147.Left = 4.22875F;
            this.Label147.Name = "Label147";
            this.Label147.Style = "font-size: 7pt; font-weight: bold";
            this.Label147.Text = "Maine Revenue Services processes returns on behalf of the";
            this.Label147.Top = 8.564834F;
            this.Label147.Width = 3F;
            // 
            // Label148
            // 
            this.Label148.Height = 0.1666667F;
            this.Label148.HyperLink = null;
            this.Label148.Left = 4.16625F;
            this.Label148.Name = "Label148";
            this.Label148.Style = "font-size: 7pt; font-weight: bold";
            this.Label148.Text = "Maine Department of Labor -- (207) 621-5120 or (844) 754-3508";
            this.Label148.Top = 8.731501F;
            this.Label148.Width = 3F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1666667F;
            this.txtEIN.Left = 6.396F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtEIN.Text = "99  9999999";
            this.txtEIN.Top = 7.793F;
            this.txtEIN.Width = 1.020833F;
            // 
            // Label54
            // 
            this.Label54.Height = 0.1666667F;
            this.Label54.HyperLink = null;
            this.Label54.Left = 4.062F;
            this.Label54.Name = "Label54";
            this.Label54.Style = "font-size: 7pt; vertical-align: middle";
            this.Label54.Text = "Paid preparer EIN:";
            this.Label54.Top = 7.812F;
            this.Label54.Width = 0.9166667F;
            // 
            // Label106
            // 
            this.Label106.Height = 0.259999F;
            this.Label106.HyperLink = null;
            this.Label106.Left = 4.667F;
            this.Label106.Name = "Label106";
            this.Label106.Style = "font-size: 7pt; vertical-align: middle";
            this.Label106.Text = "Maine Payroll Processor License Number:";
            this.Label106.Top = 8.08F;
            this.Label106.Width = 1.319667F;
            // 
            // txtMaineLicense
            // 
            this.txtMaineLicense.Height = 0.1666667F;
            this.txtMaineLicense.Left = 6.479F;
            this.txtMaineLicense.Name = "txtMaineLicense";
            this.txtMaineLicense.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtMaineLicense.Text = "999999999";
            this.txtMaineLicense.Top = 8.147F;
            this.txtMaineLicense.Width = 1.020833F;
            // 
            // Label116
            // 
            this.Label116.Height = 0.2087504F;
            this.Label116.HyperLink = null;
            this.Label116.Left = 2.771F;
            this.Label116.Name = "Label116";
            this.Label116.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt; font-weight: bold; text-align" +
    ": center";
            this.Label116.Text = "For Paid Preparers Only";
            this.Label116.Top = 7.096F;
            this.Label116.Width = 2.125F;
            // 
            // Label149
            // 
            this.Label149.Height = 0.1666667F;
            this.Label149.HyperLink = null;
            this.Label149.Left = 0.104F;
            this.Label149.Name = "Label149";
            this.Label149.Style = "font-size: 7pt; vertical-align: middle";
            this.Label149.Text = "Address:";
            this.Label149.Top = 8.175F;
            this.Label149.Width = 0.5833333F;
            // 
            // Line195
            // 
            this.Line195.Height = 0F;
            this.Line195.Left = 0.5415F;
            this.Line195.LineWeight = 1F;
            this.Line195.Name = "Line195";
            this.Line195.Top = 8.306944F;
            this.Line195.Width = 3.375F;
            this.Line195.X1 = 0.5415F;
            this.Line195.X2 = 3.9165F;
            this.Line195.Y1 = 8.306944F;
            this.Line195.Y2 = 8.306944F;
            // 
            // Label118
            // 
            this.Label118.Height = 0.1666667F;
            this.Label118.HyperLink = null;
            this.Label118.Left = 0.104F;
            this.Label118.Name = "Label118";
            this.Label118.Style = "font-size: 7pt; vertical-align: middle";
            this.Label118.Text = "Paid Preparers\'s Signature:";
            this.Label118.Top = 7.475F;
            this.Label118.Width = 1.270833F;
            // 
            // Line196
            // 
            this.Line196.Height = 0F;
            this.Line196.Left = 1.354F;
            this.Line196.LineWeight = 1F;
            this.Line196.Name = "Line196";
            this.Line196.Top = 7.620833F;
            this.Line196.Width = 2.375F;
            this.Line196.X1 = 1.354F;
            this.Line196.X2 = 3.729F;
            this.Line196.Y1 = 7.620833F;
            this.Line196.Y2 = 7.620833F;
            // 
            // Label150
            // 
            this.Label150.Height = 0.1666667F;
            this.Label150.HyperLink = null;
            this.Label150.Left = 3.812333F;
            this.Label150.Name = "Label150";
            this.Label150.Style = "font-size: 7pt; vertical-align: middle";
            this.Label150.Text = "Date:";
            this.Label150.Top = 7.475F;
            this.Label150.Width = 0.3333335F;
            // 
            // Line197
            // 
            this.Line197.Height = 0F;
            this.Line197.Left = 4.104F;
            this.Line197.LineWeight = 1F;
            this.Line197.Name = "Line197";
            this.Line197.Top = 7.620833F;
            this.Line197.Width = 0.9375F;
            this.Line197.X1 = 4.104F;
            this.Line197.X2 = 5.0415F;
            this.Line197.Y1 = 7.620833F;
            this.Line197.Y2 = 7.620833F;
            // 
            // Label151
            // 
            this.Label151.Height = 0.1666667F;
            this.Label151.HyperLink = null;
            this.Label151.Left = 5.062333F;
            this.Label151.Name = "Label151";
            this.Label151.Style = "font-size: 7pt; vertical-align: middle";
            this.Label151.Text = "Telephone:";
            this.Label151.Top = 7.475F;
            this.Label151.Width = 0.5833334F;
            // 
            // Line198
            // 
            this.Line198.Height = 0F;
            this.Line198.Left = 5.656083F;
            this.Line198.LineWeight = 1F;
            this.Line198.Name = "Line198";
            this.Line198.Top = 7.620833F;
            this.Line198.Width = 1.8125F;
            this.Line198.X1 = 5.656083F;
            this.Line198.X2 = 7.468583F;
            this.Line198.Y1 = 7.620833F;
            this.Line198.Y2 = 7.620833F;
            // 
            // Label152
            // 
            this.Label152.Height = 0.1666667F;
            this.Label152.HyperLink = null;
            this.Label152.Left = 0.103667F;
            this.Label152.Name = "Label152";
            this.Label152.Style = "font-size: 7pt; vertical-align: middle";
            this.Label152.Text = "Firm\'s Name (or yours, if self-employed):";
            this.Label152.Top = 7.808528F;
            this.Label152.Width = 1.833333F;
            // 
            // Line199
            // 
            this.Line199.Height = 0F;
            this.Line199.Left = 1.916167F;
            this.Line199.LineWeight = 1F;
            this.Line199.Name = "Line199";
            this.Line199.Top = 7.961305F;
            this.Line199.Width = 2F;
            this.Line199.X1 = 1.916167F;
            this.Line199.X2 = 3.916167F;
            this.Line199.Y1 = 7.961305F;
            this.Line199.Y2 = 7.961305F;
            // 
            // Image1
            // 
            this.Image1.Height = 1.15625F;
            this.Image1.HyperLink = null;
            this.Image1.ImageData = null;
            this.Image1.Left = 0.1349999F;
            this.Image1.LineWeight = 1F;
            this.Image1.Name = "Image1";
            this.Image1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
            this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image1.Top = 8.564834F;
            this.Image1.Width = 3.947917F;
            // 
            // Line200
            // 
            this.Line200.Height = 0F;
            this.Line200.Left = 3.72529E-09F;
            this.Line200.LineWeight = 1F;
            this.Line200.Name = "Line200";
            this.Line200.Top = 6F;
            this.Line200.Width = 7.4375F;
            this.Line200.X1 = 3.72529E-09F;
            this.Line200.X2 = 7.4375F;
            this.Line200.Y1 = 6F;
            this.Line200.Y2 = 6F;
            // 
            // Field34
            // 
            this.Field34.Height = 0.1666667F;
            this.Field34.Left = 0.125F;
            this.Field34.Name = "Field34";
            this.Field34.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: middle";
            this.Field34.Text = "Under penalties of perjury, I certify that the information contained on this retu" +
    "rn, report and attachment(s) is true and correct.";
            this.Field34.Top = 6.167F;
            this.Field34.Width = 6.375F;
            // 
            // Label153
            // 
            this.Label153.Height = 0.1666667F;
            this.Label153.HyperLink = null;
            this.Label153.Left = 4.385417F;
            this.Label153.Name = "Label153";
            this.Label153.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: middle";
            this.Label153.Text = "8.";
            this.Label153.Top = 5.667F;
            this.Label153.Width = 0.25F;
            // 
            // Field35
            // 
            this.Field35.Height = 0.1666667F;
            this.Field35.Left = 2.03125F;
            this.Field35.Name = "Field35";
            this.Field35.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; font-weight: bold; text-align: " +
    "left; vertical-align: middle";
            this.Field35.Text = "See page 6 for electronic filing and payment requirements and options";
            this.Field35.Top = 2.152778F;
            this.Field35.Visible = false;
            this.Field35.Width = 3.4375F;
            // 
            // Label154
            // 
            this.Label154.Height = 0.1666667F;
            this.Label154.HyperLink = null;
            this.Label154.Left = 3.833F;
            this.Label154.Name = "Label154";
            this.Label154.Style = "font-family: \\000027Arial\\000027; font-size: 8.5pt; font-weight: bold; text-align" +
    ": left; vertical-align: top";
            this.Label154.Text = "Quarterly";
            this.Label154.Top = 1.535167F;
            this.Label154.Width = 0.7708333F;
            // 
            // Label155
            // 
            this.Label155.Height = 0.1666667F;
            this.Label155.HyperLink = null;
            this.Label155.Left = 3.833F;
            this.Label155.Name = "Label155";
            this.Label155.Style = "font-family: \\000027Arial\\000027; font-size: 8.5pt; font-weight: bold; text-align" +
    ": left; vertical-align: top";
            this.Label155.Text = "Period Covered";
            this.Label155.Top = 1.681F;
            this.Label155.Width = 1.020833F;
            // 
            // label3
            // 
            this.label3.Height = 0.1875F;
            this.label3.HyperLink = null;
            this.label3.Left = 1.937333F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.label3.Text = "UC contributions due (line 5 times line 6a)";
            this.label3.Top = 4.49F;
            this.label3.Width = 2.0625F;
            // 
            // txtUPAFAssessment
            // 
            this.txtUPAFAssessment.DataField = "";
            this.txtUPAFAssessment.DistinctField = "";
            this.txtUPAFAssessment.Height = 0.1666667F;
            this.txtUPAFAssessment.Left = 4.979F;
            this.txtUPAFAssessment.Name = "txtUPAFAssessment";
            this.txtUPAFAssessment.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtUPAFAssessment.SummaryGroup = "";
            this.txtUPAFAssessment.Text = "9999999999999";
            this.txtUPAFAssessment.Top = 5.147F;
            this.txtUPAFAssessment.Width = 1.75F;
            // 
            // txtUPAFAssessmentCents
            // 
            this.txtUPAFAssessmentCents.DataField = "";
            this.txtUPAFAssessmentCents.DistinctField = "";
            this.txtUPAFAssessmentCents.Height = 0.1666667F;
            this.txtUPAFAssessmentCents.Left = 6.874834F;
            this.txtUPAFAssessmentCents.Name = "txtUPAFAssessmentCents";
            this.txtUPAFAssessmentCents.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right;" +
    " vertical-align: middle";
            this.txtUPAFAssessmentCents.SummaryGroup = "";
            this.txtUPAFAssessmentCents.Text = "99";
            this.txtUPAFAssessmentCents.Top = 5.147F;
            this.txtUPAFAssessmentCents.Width = 0.4375F;
            // 
            // label6
            // 
            this.label6.Height = 0.1666667F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.38525F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: middle";
            this.label6.Text = "7d.";
            this.label6.Top = 5.157F;
            this.label6.Width = 0.25F;
            // 
            // txtUPAFRate
            // 
            this.txtUPAFRate.DataField = "";
            this.txtUPAFRate.DistinctField = "";
            this.txtUPAFRate.Height = 0.1666667F;
            this.txtUPAFRate.Left = 0.6248335F;
            this.txtUPAFRate.Name = "txtUPAFRate";
            this.txtUPAFRate.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; font-weight: bold" +
    "; text-align: right; vertical-align: middle";
            this.txtUPAFRate.SummaryGroup = "";
            this.txtUPAFRate.Text = ".0013";
            this.txtUPAFRate.Top = 5.157F;
            this.txtUPAFRate.Width = 0.5625F;
            // 
            // label10
            // 
            this.label10.Height = 0.1875F;
            this.label10.HyperLink = null;
            this.label10.Left = 0.2498336F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.label10.Text = "UPAF rate";
            this.label10.Top = 5.157F;
            this.label10.Width = 0.5625F;
            // 
            // label11
            // 
            this.label11.Height = 0.1875F;
            this.label11.HyperLink = null;
            this.label11.Left = 0.06233358F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.label11.Text = "7c.";
            this.label11.Top = 5.157F;
            this.label11.Width = 0.25F;
            // 
            // label12
            // 
            this.label12.Height = 0.1875F;
            this.label12.HyperLink = null;
            this.label12.Left = 1.937334F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: middle";
            this.label12.Text = "UPAF Assessment due (line 5 times line 7c)";
            this.label12.Top = 5.157F;
            this.label12.Width = 2.0625F;
            // 
            // label13
            // 
            this.label13.Height = 0.1875F;
            this.label13.HyperLink = null;
            this.label13.Left = 0.659F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: \\000027Arial\\000027; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; vertical-align: middle";
            this.label13.Text = "(CSSF UPAF)";
            this.label13.Top = 0.144F;
            this.label13.Width = 1.5625F;
            // 
            // srptMEUC1First
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.541667F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEmployerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnemploymentCompensationGrossWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnemploymentCompensationGrossCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcessWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcessWagesCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableWagesPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableWagesPaidCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContributionsDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContributionsDueCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFAssessmentCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDueCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCSSFRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesFirstMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesSecMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFemalesThirdMo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaineLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFAssessment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFAssessmentCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPAFRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label txtYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCEmployerAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEmployerID;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label txtQuarterNum;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnemploymentCompensationGrossWages;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnemploymentCompensationGrossCents;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcessWages;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcessWagesCents;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxableWagesPaid;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxableWagesPaidCents;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContributionsDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContributionsDueCents;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCSSFAssessment;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCSSFAssessmentCents;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDueCents;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCSSFRate;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEMail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstMo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecMo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThirdMo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFemalesFirstMo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFemalesSecMo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFemalesThirdMo;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaineLicense;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label149;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line195;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line196;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label150;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line197;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label151;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line198;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line199;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line200;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUPAFAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUPAFAssessmentCents;
		private GrapeCity.ActiveReports.SectionReportModel.Label label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUPAFRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
    }
}
