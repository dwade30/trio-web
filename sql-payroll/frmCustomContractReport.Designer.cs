﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCustomContractReport.
	/// </summary>
	partial class frmCustomContractReport
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid txtEmployee;
		public Global.T2KDateBox t2kStartStart;
		public fecherFoundation.FCTextBox txtSequence;
		public fecherFoundation.FCTextBox txtGroup;
		public fecherFoundation.FCComboBox cmbStatus;
		public Global.T2KDateBox t2kStartEnd;
		public Global.T2KDateBox t2kEndStart;
		public Global.T2KDateBox t2kEndEnd;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public Global.T2KDateBox t2kAsOf;
		public fecherFoundation.FCLabel Label9;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtEmployee = new fecherFoundation.FCGrid();
            this.t2kStartStart = new Global.T2KDateBox();
            this.txtSequence = new fecherFoundation.FCTextBox();
            this.txtGroup = new fecherFoundation.FCTextBox();
            this.cmbStatus = new fecherFoundation.FCComboBox();
            this.t2kStartEnd = new Global.T2KDateBox();
            this.t2kEndStart = new Global.T2KDateBox();
            this.t2kEndEnd = new Global.T2KDateBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.t2kAsOf = new Global.T2KDateBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kAsOf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(665, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.t2kAsOf);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(665, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(665, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(277, 30);
            this.HeaderText.Text = "Custom Contract Report";
            // 
            // cmbType
            // 
            this.cmbType.AutoSize = false;
            this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Contract Summary",
            "Contract Detail",
            "Distribution Detail"});
            this.cmbType.Location = new System.Drawing.Point(176, 500);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(248, 40);
			this.cmbType.SelectedIndex = 0;
            this.cmbType.TabIndex = 0;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 514);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(95, 15);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "REPORT TYPE";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtEmployee);
            this.Frame1.Controls.Add(this.t2kStartStart);
            this.Frame1.Controls.Add(this.txtSequence);
            this.Frame1.Controls.Add(this.txtGroup);
            this.Frame1.Controls.Add(this.cmbStatus);
            this.Frame1.Controls.Add(this.t2kStartEnd);
            this.Frame1.Controls.Add(this.t2kEndStart);
            this.Frame1.Controls.Add(this.t2kEndEnd);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(597, 390);
            this.Frame1.TabIndex = 12;
            this.Frame1.Text = "Include";
            // 
            // txtEmployee
            // 
            this.txtEmployee.AllowSelection = false;
            this.txtEmployee.AllowUserToResizeColumns = false;
            this.txtEmployee.AllowUserToResizeRows = false;
            this.txtEmployee.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.txtEmployee.BackColorAlternate = System.Drawing.Color.Empty;
            this.txtEmployee.BackColorBkg = System.Drawing.Color.Empty;
            this.txtEmployee.BackColorFixed = System.Drawing.Color.Empty;
            this.txtEmployee.BackColorSel = System.Drawing.Color.Empty;
            this.txtEmployee.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.txtEmployee.Cols = 2;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.txtEmployee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.txtEmployee.ColumnHeadersHeight = 30;
            this.txtEmployee.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.txtEmployee.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.txtEmployee.DefaultCellStyle = dataGridViewCellStyle4;
            this.txtEmployee.DragIcon = null;
            this.txtEmployee.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.txtEmployee.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.txtEmployee.ExtendLastCol = true;
            this.txtEmployee.FixedCols = 0;
            this.txtEmployee.FixedRows = 0;
            this.txtEmployee.ForeColorFixed = System.Drawing.Color.Empty;
            this.txtEmployee.FrozenCols = 0;
            this.txtEmployee.GridColor = System.Drawing.Color.Empty;
            this.txtEmployee.GridColorFixed = System.Drawing.Color.Empty;
            this.txtEmployee.Location = new System.Drawing.Point(160, 90);
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.OutlineCol = 0;
            this.txtEmployee.RowHeadersVisible = false;
            this.txtEmployee.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.txtEmployee.RowHeightMin = 0;
            this.txtEmployee.Rows = 1;
            this.txtEmployee.ScrollTipText = null;
            this.txtEmployee.ShowColumnVisibilityMenu = false;
            this.txtEmployee.Size = new System.Drawing.Size(381, 42);
            this.txtEmployee.StandardTab = true;
            this.txtEmployee.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.txtEmployee.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.txtEmployee.TabIndex = 1;
            // 
            // t2kStartStart
            // 
            this.t2kStartStart.Location = new System.Drawing.Point(160, 270);
            this.t2kStartStart.Mask = "00/00/0000";
            this.t2kStartStart.MaxLength = 10;
            this.t2kStartStart.Name = "t2kStartStart";
            this.t2kStartStart.Size = new System.Drawing.Size(115, 40);
            this.t2kStartStart.TabIndex = 4;
            // 
            // txtSequence
            // 
            this.txtSequence.AutoSize = false;
            this.txtSequence.BackColor = System.Drawing.SystemColors.Window;
            this.txtSequence.LinkItem = null;
            this.txtSequence.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSequence.LinkTopic = null;
            this.txtSequence.Location = new System.Drawing.Point(160, 210);
            this.txtSequence.Name = "txtSequence";
            this.txtSequence.Size = new System.Drawing.Size(381, 40);
            this.txtSequence.TabIndex = 3;
            // 
            // txtGroup
            // 
            this.txtGroup.AutoSize = false;
            this.txtGroup.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroup.LinkItem = null;
            this.txtGroup.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtGroup.LinkTopic = null;
            this.txtGroup.Location = new System.Drawing.Point(160, 150);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(381, 40);
            this.txtGroup.TabIndex = 2;
            // 
            // cmbStatus
            // 
            this.cmbStatus.AutoSize = false;
            this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(160, 30);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(381, 40);
            this.cmbStatus.TabIndex = 0;
            // 
            // t2kStartEnd
            // 
            this.t2kStartEnd.Location = new System.Drawing.Point(428, 270);
            this.t2kStartEnd.Mask = "00/00/0000";
            this.t2kStartEnd.MaxLength = 10;
            this.t2kStartEnd.Name = "t2kStartEnd";
            this.t2kStartEnd.Size = new System.Drawing.Size(115, 40);
            this.t2kStartEnd.TabIndex = 5;
            // 
            // t2kEndStart
            // 
            this.t2kEndStart.Location = new System.Drawing.Point(160, 330);
            this.t2kEndStart.Mask = "00/00/0000";
            this.t2kEndStart.MaxLength = 10;
            this.t2kEndStart.Name = "t2kEndStart";
            this.t2kEndStart.Size = new System.Drawing.Size(115, 40);
            this.t2kEndStart.TabIndex = 6;
            // 
            // t2kEndEnd
            // 
            this.t2kEndEnd.Location = new System.Drawing.Point(428, 330);
            this.t2kEndEnd.Mask = "00/00/0000";
            this.t2kEndEnd.MaxLength = 10;
            this.t2kEndEnd.Name = "t2kEndEnd";
            this.t2kEndEnd.Size = new System.Drawing.Size(115, 40);
            this.t2kEndEnd.TabIndex = 7;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(374, 344);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(19, 17);
            this.Label7.TabIndex = 21;
            this.Label7.Text = "TO";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 344);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(64, 17);
            this.Label6.TabIndex = 20;
            this.Label6.Text = "END DATE";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(45, 17);
            this.Label8.TabIndex = 19;
            this.Label8.Text = "STATUS";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(374, 284);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(19, 17);
            this.Label5.TabIndex = 18;
            this.Label5.Text = "TO";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 284);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(78, 17);
            this.Label4.TabIndex = 17;
            this.Label4.Text = "START DATE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 224);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(70, 17);
            this.Label3.TabIndex = 16;
            this.Label3.Text = "SEQUENCE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 164);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(45, 17);
            this.Label2.TabIndex = 15;
            this.Label2.Text = "GROUP";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(70, 17);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "EMPLOYEE";
            // 
            // t2kAsOf
            // 
            this.t2kAsOf.Location = new System.Drawing.Point(176, 440);
            this.t2kAsOf.Mask = "00/00/0000";
            this.t2kAsOf.MaxLength = 10;
            this.t2kAsOf.Name = "t2kAsOf";
            this.t2kAsOf.Size = new System.Drawing.Size(115, 40);
            this.t2kAsOf.TabIndex = 8;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 454);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(36, 23);
            this.Label9.TabIndex = 22;
            this.Label9.Text = "AS OF";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(281, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(82, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // frmCustomContractReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(665, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCustomContractReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Custom Contract Report";
            this.Load += new System.EventHandler(this.frmCustomContractReport_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomContractReport_KeyDown);
            this.Resize += new System.EventHandler(this.frmCustomContractReport_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kAsOf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdPrint;
    }
}
