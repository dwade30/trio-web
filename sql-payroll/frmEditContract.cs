//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEditContract : BaseForm
	{
		public frmEditContract()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_3, 1);
			this.Label1.AddControlArrayElement(Label1_4, 2);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditContract InstancePtr
		{
			get
			{
				return (frmEditContract)Sys.GetInstance(typeof(frmEditContract));
			}
		}

		protected frmEditContract _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngContract;
		private clsContract conCurContract = new clsContract();
		const int CNSTGRIDACCOUNTSCOLID = 0;
		const int CNSTGRIDACCOUNTSCOLCONTRACTID = 1;
		const int CNSTGRIDACCOUNTSCOLACCOUNT = 2;
		const int CNSTGRIDACCOUNTSCOLORIGINALAMOUNT = 3;
		const int CNSTGRIDACCOUNTSCOLPAID = 4;
		const int CNSTGRIDACCOUNTSCOLLEFT = 5;
		private clsGridAccount clsGA = new clsGridAccount();
		private bool boolJournalChanges;
		private bool boolEncumber;

		public void Init(ref int lngContractID)
		{
			boolEncumber = false;
			boolJournalChanges = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				if (modGlobalConstants.Statics.gboolBD && rsLoad.Get_Fields_Boolean("EncumberContracts"))
				{
					boolEncumber = true;
				}
			}
			lngContract = lngContractID;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmEditContract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditContract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditContract properties;
			//frmEditContract.FillStyle	= 0;
			//frmEditContract.ScaleWidth	= 9300;
			//frmEditContract.ScaleHeight	= 7770;
			//frmEditContract.LinkTopic	= "Form2";
			//frmEditContract.LockControls	= -1  'True;
			//frmEditContract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridAccounts();
			LoadContract();
			clsGA.GRID7Light = GridAccounts;

				clsGA.DefaultAccountType = "E";

			clsGA.OnlyAllowDefaultType = false;
			clsGA.AccountCol = CNSTGRIDACCOUNTSCOLACCOUNT;
			clsGA.Validation = true;
			clsGA.AllowSplits = modRegionalTown.IsRegionalTown();
		}

		private void SetupGridAccounts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridAccounts.Cols = 6;
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLID, true);
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLCONTRACTID, true);
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLACCOUNT, "Account");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "Amount");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, "Paid");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "Remaining");
				GridTotals.Cols = GridAccounts.Cols;
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLACCOUNT, "Total");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, "0.00");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
				GridTotals.ColHidden(CNSTGRIDACCOUNTSCOLID, true);
				GridTotals.ColHidden(CNSTGRIDACCOUNTSCOLCONTRACTID, true);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadContract()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngARow;
				if (conCurContract.LoadContract(lngContract))
				{
					GridAccounts.Rows = 1;
					lblEmployee.Text = "Employee #" + conCurContract.EmployeeNumber;
					txtDescription.Text = conCurContract.Description;
					if (Information.IsDate(conCurContract.StartDate))
					{
						if (conCurContract.StartDate.ToOADate() != 0)
						{
							t2kStartDate.Text = Strings.Format(conCurContract.StartDate, "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(conCurContract.EndDate))
					{
						if (conCurContract.EndDate.ToOADate() != 0)
						{
							t2kEndDate.Text = Strings.Format(conCurContract.EndDate, "MM/dd/yyyy");
						}
					}
					while (!conCurContract.EndOfAccounts())
					{
						GridAccounts.Rows += 1;
						lngARow = GridAccounts.Rows - 1;
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLID, FCConvert.ToString(conCurContract.CurAccountID));
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLACCOUNT, conCurContract.CurAccount);
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLCONTRACTID, FCConvert.ToString(conCurContract.ContractID));
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.CurAccountOriginalAmount, "0.00"));
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLPAID, Strings.Format(conCurContract.CurAccountPaid, "0.00"));
						if (conCurContract.CurAccountID >= 0)
						{
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, Strings.Format(conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid, "###,###,##0.00"));
						}
						else
						{
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
						}
						conCurContract.MoveNextAccount();
					}
					ReAddAccounts();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadContract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridAccounts()
		{
			int GridWidth = 0;
			GridWidth = GridAccounts.WidthOriginal;
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLACCOUNT, FCConvert.ToInt32(0.4 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLACCOUNT, FCConvert.ToInt32(0.4 * GridWidth));
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.2 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.2 * GridWidth));
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToInt32(0.2 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToInt32(0.2 * GridWidth));
			//GridTotals.Height = GridTotals.RowHeight(0) + 60;
		}

		private void frmEditContract_Resize(object sender, System.EventArgs e)
		{
			ResizeGridAccounts();
		}

		private void GridAccounts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridAccounts[e.ColumnIndex, e.RowIndex];
            int lngRow;
			lngRow = GridAccounts.GetFlexRowIndex(e.RowIndex);
			if (lngRow != CNSTGRIDACCOUNTSCOLACCOUNT)
			{
				//ToolTip1.SetToolTip(GridAccounts, "Use Insert and Delete to add and remove accounts");
				cell.ToolTipText = "Use Insert and Delete to add and remove accounts";
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ReAddAccounts()
		{
			int lngRow;
			double dblOriginal;
			double dblLeft;
			double dblPaid;
			//App.DoEvents();
			dblOriginal = 0;
			dblLeft = 0;
			dblPaid = 0;
			for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
			{
				dblOriginal += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
				dblPaid += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID));
			}
			// lngRow
			dblLeft = dblOriginal - dblPaid;
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToString(dblOriginal));
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblPaid));
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, FCConvert.ToString(dblLeft));
		}

		private void GridAccounts_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = GridAccounts.Row;
			if (lngRow > 0)
			{
				if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLID)) > 0)
				{
					if (GridAccounts.Col == CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						clsGA.AccountCol = CNSTGRIDACCOUNTSCOLACCOUNT;
					}
					else
					{
						clsGA.AccountCol = FCConvert.ToInt16(GridAccounts.Cols + 1);
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (GridAccounts.Col == CNSTGRIDACCOUNTSCOLORIGINALAMOUNT || GridAccounts.Col == CNSTGRIDACCOUNTSCOLACCOUNT)
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						clsGA.AccountCol = CNSTGRIDACCOUNTSCOLACCOUNT;
					}
					else
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
			}
		}

		private void GridAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridAccounts.Row < 1)
				return;
			double dblPaid = 0;
			double dblOriginal = 0;
			switch (GridAccounts.Col)
			{
				case CNSTGRIDACCOUNTSCOLORIGINALAMOUNT:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.EditText);
						GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToString(dblOriginal));
						boolJournalChanges = true;
						break;
					}
				case CNSTGRIDACCOUNTSCOLPAID:
					{
						dblPaid = Conversion.Val(GridAccounts.EditText);
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblPaid));
						break;
					}
				case CNSTGRIDACCOUNTSCOLLEFT:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						GridAccounts.EditText = FCConvert.ToString(dblOriginal - dblPaid);
						break;
					}
				default:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						break;
					}
			}
			//end switch
			GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLLEFT, FCConvert.ToString(dblOriginal - dblPaid));
			ReAddAccounts();
		}

		private void GridAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int lngRow = 0;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						GridAccounts.Rows += 1;
						lngRow = GridAccounts.Rows - 1;
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID, "0.00");
						GridAccounts.RowData(lngRow, false);
						GridAccounts.TopRow = lngRow;
						boolJournalChanges = true;
						break;
					}
				case Keys.Delete:
					{
						if (Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLID)) == 0)
						{
							KeyCode = 0;
							GridAccounts.RemoveItem(GridAccounts.Row);
							ReAddAccounts();
						}
						else if (Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID)) == 0)
						{
							KeyCode = 0;
							// must edit encumbrance record
							boolJournalChanges = true;
							gridDelete.Rows += 1;
							lngRow = gridDelete.Rows - 1;
							gridDelete.TextMatrix(lngRow, 0, FCConvert.ToString(Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLID))));
							gridDelete.TextMatrix(lngRow, 1, FCConvert.ToString(Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID))));
							GridAccounts.RemoveItem(GridAccounts.Row);
						}
						else
						{
							KeyCode = 0;
							MessageBox.Show("Cannot delete accounts that have already been paid out of", "Invalid Operation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						break;
					}
			}
			//end switch
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			if (SaveData())
			{
				Close();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strLastAccount;
				//App.DoEvents();
				GridAccounts.Row = 1;
				GridAccounts.Col = CNSTGRIDACCOUNTSCOLACCOUNT;
				GridAccounts.Sort = FCGrid.SortSettings.flexSortStringAscending;
				int lngRow;
				int lngEncumbranceID = 0;
				int lngEncumbranceJournal;
				double dblTotalOriginal;
				strLastAccount = "";
				dblTotalOriginal = 0;
				for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
				{
					if (fecherFoundation.Strings.Trim(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT)).Length < 3)
					{
						MessageBox.Show("All entries must contain valid accounts", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)) <= 0)
					{
						MessageBox.Show("Beginning amounts must be greater than 0", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID)) > Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)))
					{
						MessageBox.Show("Amounts paid cannot be greater than original amounts", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					if (strLastAccount == GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT))
					{
						MessageBox.Show("Cannot have more than one instance of an account in the same contract", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					else
					{
						strLastAccount = GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT);
					}
					dblTotalOriginal += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
				}
				// lngRow
				int lngJournal = 0;
				clsDRWrapper rsEncumbrance = new clsDRWrapper();
				clsDRWrapper rsJournal = new clsDRWrapper();
				double dblJournalTotal;
				dblJournalTotal = 0;
				if (boolJournalChanges && boolEncumber)
				{
					// create general journal and adjust encumbrance journal
					lngEncumbranceJournal = conCurContract.EncumbranceJournal;
					lngEncumbranceID = conCurContract.EncumbranceID;
					lngJournal = modCoreysSweeterCode.CreateAJournal("PY Edit Contract Emp " + conCurContract.EmployeeNumber, 0, "GJ");
					if (lngJournal > 0)
					{
						rsJournal.OpenRecordset("select * from journalentries where journalnumber = " + FCConvert.ToString(lngJournal), "twbd0000.vb1");
					}
					else
					{
						MessageBox.Show("Cannot create journal" + "\r\n" + "Cannot continue", "Error Creating Journal", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
				}
				int lngAcctRow;
				double dblTemp = 0;
				for (lngRow = 0; lngRow <= gridDelete.Rows - 1; lngRow++)
				{
					if (Conversion.Val(gridDelete.TextMatrix(lngRow, 0)) > 0)
					{
						conCurContract.MoveFirstAccount();
						while (!conCurContract.EndOfAccounts())
						{
							if (conCurContract.CurAccountID == Conversion.Val(gridDelete.TextMatrix(lngRow, 0)))
							{
								if (boolEncumber)
								{
									dblTemp = conCurContract.CurAccountOriginalAmount * -1;
									rsEncumbrance.OpenRecordset("select * from encumbrancedetail where recordnumber = " + FCConvert.ToString(lngEncumbranceID) + " and account = '" + conCurContract.CurAccount + "'", "twbd0000.vb1");
									if (!rsEncumbrance.EndOfFile())
									{
										dblJournalTotal += dblTemp;
										rsEncumbrance.Edit();
										rsEncumbrance.Set_Fields("adjustments", dblTemp);
										rsEncumbrance.Update();
										rsJournal.AddNew();
										rsJournal.Set_Fields("Date", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
										rsJournal.Set_Fields("Type", "G");
										rsJournal.Set_Fields("Description", fecherFoundation.Strings.Trim(Strings.Left("PY Edit Contract Emp " + conCurContract.EmployeeNumber + Strings.StrDup(25, " "), 25)));
										rsJournal.Set_Fields("vendornumber", 0);
										rsJournal.Set_Fields("journalnumber", lngJournal);
										rsJournal.Set_Fields("account", conCurContract.CurAccount);
										rsJournal.Set_Fields("amount", dblTemp);
										rsJournal.Set_Fields("warrantnumber", 0);
										rsJournal.Set_Fields("period", DateTime.Today.Month);
										rsJournal.Set_Fields("rcb", "E");
										rsJournal.Set_Fields("status", "E");
										rsJournal.Set_Fields("po", conCurContract.ContractID);
										rsJournal.Update();
									}
								}
								conCurContract.DeleteCurAccount();
								break;
							}
							conCurContract.MoveNextAccount();
						}
					}
				}
				// lngRow
				gridDelete.Rows = 0;
				double dblEncumber = 0;
				for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLID)) > 0)
					{
						conCurContract.MoveFirstAccount();
						while (!conCurContract.EndOfAccounts())
						{
							if (conCurContract.CurAccountID == Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLID)))
							{
								conCurContract.CurAccountOriginalAmount = Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
								if (boolEncumber)
								{
									dblEncumber = conCurContract.CurAccountEncumbrance;
									dblTemp = conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid;
									if (dblTemp != dblEncumber)
									{
										dblTemp -= dblEncumber;
										rsEncumbrance.OpenRecordset("select * from encumbrancedetail where EncumbranceID = " + FCConvert.ToString(lngEncumbranceID) + " and account = '" + conCurContract.CurAccount + "'", "twbd0000.vb1");
										if (!rsEncumbrance.EndOfFile())
										{
											dblJournalTotal += dblTemp;
											rsEncumbrance.Edit();
											rsEncumbrance.Set_Fields("adjustments", Conversion.Val(rsEncumbrance.Get_Fields("adjustments")) + dblTemp);
											rsEncumbrance.Update();
											rsJournal.AddNew();
											rsJournal.Set_Fields("date", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
											rsJournal.Set_Fields("type", "G");
											rsJournal.Set_Fields("description", fecherFoundation.Strings.Trim(Strings.Left("PY Edit Contract Emp " + conCurContract.EmployeeNumber + Strings.StrDup(25, " "), 25)));
											rsJournal.Set_Fields("amount", dblTemp);
											rsJournal.Set_Fields("journalnumber", lngJournal);
											rsJournal.Set_Fields("account", conCurContract.CurAccount);
											rsJournal.Set_Fields("period", DateTime.Today.Month);
											rsJournal.Set_Fields("RCB", "E");
											rsJournal.Set_Fields("status", "E");
											rsJournal.Update();
										}
									}
								}
								break;
							}
							conCurContract.MoveNextAccount();
						}
					}
					else
					{
						// create a new one
						conCurContract.AddAccount(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT), Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)));
						if (boolEncumber)
						{
							rsEncumbrance.OpenRecordset("select * from encumbrancedetail where EncumbranceID = 0", "twbd0000.vb1");
							rsEncumbrance.AddNew();
							dblTemp = Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
							dblJournalTotal += dblTemp;
							rsEncumbrance.Set_Fields("EncumbranceID", lngEncumbranceID);
							rsEncumbrance.Set_Fields("account", GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT));
							rsEncumbrance.Set_Fields("amount", 0);
							rsEncumbrance.Set_Fields("adjustments", dblTemp);
							rsEncumbrance.Set_Fields("liquidated", 0);
							rsEncumbrance.Set_Fields("description", "PY Contract " + FCConvert.ToString(conCurContract.ContractID));
							rsEncumbrance.Update();
							rsJournal.AddNew();
							rsJournal.Set_Fields("date", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							rsJournal.Set_Fields("type", "G");
							rsJournal.Set_Fields("description", fecherFoundation.Strings.Trim(Strings.Left("PY Edit Contract Emp " + conCurContract.EmployeeNumber + Strings.StrDup(25, " "), 25)));
							rsJournal.Set_Fields("amount", dblTemp);
							rsJournal.Set_Fields("journalnumber", lngJournal);
							rsJournal.Set_Fields("account", GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT));
							rsJournal.Set_Fields("period", DateTime.Today.Month);
							rsJournal.Set_Fields("rcb", "E");
							rsJournal.Set_Fields("status", "E");
							rsJournal.Update();
						}
					}
				}
				// lngRow
				if (boolEncumber && dblJournalTotal != 0)
				{
					rsJournal.Execute("update journalmaster set totalamount = " + FCConvert.ToString(dblJournalTotal) + " where journalnumber = " + FCConvert.ToString(lngJournal), "twbd0000.vb1");
					rsEncumbrance.Execute("update encumbrances set adjustments = adjustments + " + FCConvert.ToString(dblJournalTotal) + " where ID = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
					// Call rsJournal.Execute("update journalmaster set totalamount = totalamount + " & dblJournalTotal & " where journalnumber = " & lngEncumbranceJournal, "twbd0000.vb1")
					MessageBox.Show("Created journal number " + FCConvert.ToString(lngJournal), "Created Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				boolJournalChanges = false;
				conCurContract.OriginalAmount = dblTotalOriginal;
				if (txtDescription.Text != string.Empty)
				{
					conCurContract.Description = txtDescription.Text;
				}
				if (Information.IsDate(t2kEndDate.Text) && Information.IsDate(t2kStartDate.Text))
				{
					if (fecherFoundation.DateAndTime.DateDiff("d", fecherFoundation.DateAndTime.DateValue(t2kStartDate.Text), fecherFoundation.DateAndTime.DateValue(t2kEndDate.Text)) >= 0)
					{
						conCurContract.StartDate = FCConvert.ToDateTime(t2kStartDate.Text);
						conCurContract.EndDate = FCConvert.ToDateTime(t2kEndDate.Text);
					}
				}
				conCurContract.SaveContract();
				LoadContract();
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(cmdSave, EventArgs.Empty);
        }
    }
}
