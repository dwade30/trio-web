//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2.
	/// </summary>
	partial class frmW2
	{
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCProgressBar pbrStatus;
		public fecherFoundation.FCLabel lblStatus;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.vsData = new fecherFoundation.FCGrid();
			this.pbrStatus = new fecherFoundation.FCProgressBar();
			this.lblStatus = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 407);
			this.BottomPanel.Size = new System.Drawing.Size(685, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.pbrStatus);
			this.ClientArea.Controls.Add(this.lblStatus);
			this.ClientArea.Controls.Add(this.vsData);
			this.ClientArea.Size = new System.Drawing.Size(685, 347);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(685, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(242, 30);
			this.HeaderText.Text = "Save W2 Information";
			// 
			// vsData
			// 
			this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsData.Cols = 23;
			this.vsData.ColumnHeadersVisible = false;
			this.vsData.FixedCols = 0;
			this.vsData.FixedRows = 0;
			this.vsData.Location = new System.Drawing.Point(30, 123);
			this.vsData.Name = "vsData";
			this.vsData.RowHeadersVisible = false;
			this.vsData.Rows = 50;
			this.vsData.Size = new System.Drawing.Size(627, 221);
			// 
			// pbrStatus
			// 
			this.pbrStatus.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.pbrStatus.Location = new System.Drawing.Point(30, 72);
			this.pbrStatus.Name = "pbrStatus";
			this.pbrStatus.Size = new System.Drawing.Size(627, 30);
			this.pbrStatus.TabIndex = 1;
			// 
			// lblStatus
			// 
			this.lblStatus.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblStatus.Location = new System.Drawing.Point(30, 30);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(627, 20);
			this.lblStatus.TabIndex = 2;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuProcess,
            this.mnuSP2,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 0;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print Edit Report";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 1;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 2;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcess.Text = "Process                     ";
			this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 3;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(285, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(106, 48);
			this.cmdProcess.Text = "Process";
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Location = new System.Drawing.Point(535, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(120, 24);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print Edit Report";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// frmW2
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(685, 515);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmW2";
			this.Text = "Save W2 Information";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmW2_Load);
			this.Activated += new System.EventHandler(this.frmW2_Activated);
			this.Resize += new System.EventHandler(this.frmW2_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW2_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
		private FCButton cmdPrint;
	}
}