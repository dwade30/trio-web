﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Schedule1.
	/// </summary>
	partial class srptC1Schedule1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1Schedule1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUCEmployerAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtX = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line121 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line122 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtAmount43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaymentSub3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaymentSub1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDatePaid22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaymentSub2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDatePaid43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line109 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line111 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line112 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line113 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line114 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line124 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line125 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line126 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line127 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line128 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line130 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line131 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line133 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line134 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line136 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line137 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line143 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line145 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line146 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line147 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line148 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line150 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line151 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line152 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line154 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line155 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line157 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line158 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line160 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line166 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtAmount13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line167 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line168 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line169 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line170 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line171 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line172 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line173 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line174 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line175 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDatePaid34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line176 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line177 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line178 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line179 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line180 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line181 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line182 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line183 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line184 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line185 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line186 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line187 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line188 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line189 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line190 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line191 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line192 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line193 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape15,
            this.Shape14,
            this.Shape13,
            this.Label1,
            this.Label3,
            this.Line13,
            this.Label4,
            this.Label13,
            this.Label14,
            this.Label15,
            this.txtLine13,
            this.Line45,
            this.Line46,
            this.Label17,
            this.Label18,
            this.Label20,
            this.Label21,
            this.Label23,
            this.Barcode1,
            this.Label24,
            this.Line75,
            this.Label26,
            this.Label27,
            this.txtName,
            this.txtUCEmployerAccount,
            this.txtPeriodStart,
            this.txtPeriodEnd,
            this.Field25,
            this.Label42,
            this.Label43,
            this.txtWithholdingAccount,
            this.Label44,
            this.Label45,
            this.Label74,
            this.txtX,
            this.Line121,
            this.Line122,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.txtAmount43,
            this.txtAmount44,
            this.txtAmount45,
            this.txtAmount46,
            this.txtAmount47,
            this.txtAmount48,
            this.txtAmount49,
            this.txtAmount50,
            this.txtAmount51,
            this.txtAmount52,
            this.txtAmount53,
            this.txtAmount54,
            this.txtPaymentSub3,
            this.txtAmount1,
            this.txtAmount2,
            this.txtAmount3,
            this.txtAmount4,
            this.txtAmount5,
            this.txtAmount6,
            this.txtAmount7,
            this.txtAmount8,
            this.txtAmount9,
            this.txtAmount10,
            this.txtAmount11,
            this.txtAmount12,
            this.txtPaymentSub1,
            this.txtDatePaid1,
            this.txtDatePaid2,
            this.txtDatePaid3,
            this.txtDatePaid4,
            this.txtDatePaid5,
            this.txtDatePaid6,
            this.txtDatePaid7,
            this.txtDatePaid8,
            this.txtDatePaid9,
            this.txtDatePaid10,
            this.txtDatePaid11,
            this.txtDatePaid12,
            this.Label85,
            this.txtDatePaid22,
            this.txtDatePaid23,
            this.txtDatePaid24,
            this.txtDatePaid25,
            this.txtDatePaid26,
            this.txtDatePaid27,
            this.txtDatePaid28,
            this.txtDatePaid29,
            this.txtDatePaid30,
            this.txtDatePaid31,
            this.txtDatePaid32,
            this.txtDatePaid33,
            this.txtAmount22,
            this.txtAmount23,
            this.txtAmount24,
            this.txtAmount25,
            this.txtAmount26,
            this.txtAmount27,
            this.txtAmount28,
            this.txtAmount29,
            this.txtAmount30,
            this.txtAmount31,
            this.txtAmount32,
            this.txtAmount33,
            this.txtPaymentSub2,
            this.Label91,
            this.txtDatePaid43,
            this.txtDatePaid44,
            this.txtDatePaid45,
            this.txtDatePaid46,
            this.txtDatePaid47,
            this.txtDatePaid48,
            this.txtDatePaid49,
            this.txtDatePaid50,
            this.txtDatePaid51,
            this.txtDatePaid52,
            this.txtDatePaid53,
            this.txtDatePaid54,
            this.Label95,
            this.Line100,
            this.Line101,
            this.Line102,
            this.Line103,
            this.Line104,
            this.Line106,
            this.Line108,
            this.Line109,
            this.Line111,
            this.Line112,
            this.Line113,
            this.Line114,
            this.Line123,
            this.Line124,
            this.Line125,
            this.Line126,
            this.Line127,
            this.Line128,
            this.Line130,
            this.Line131,
            this.Line133,
            this.Line134,
            this.Line136,
            this.Line137,
            this.Line143,
            this.Line145,
            this.Line146,
            this.Line147,
            this.Line148,
            this.Line150,
            this.Line151,
            this.Line152,
            this.Line154,
            this.Line155,
            this.Line157,
            this.Line158,
            this.Line160,
            this.Line166,
            this.txtAmount13,
            this.txtDatePaid13,
            this.txtAmount14,
            this.txtDatePaid14,
            this.txtAmount15,
            this.txtDatePaid15,
            this.txtAmount16,
            this.txtDatePaid16,
            this.txtAmount17,
            this.txtDatePaid17,
            this.txtAmount18,
            this.txtDatePaid18,
            this.txtAmount19,
            this.txtDatePaid19,
            this.txtAmount20,
            this.txtDatePaid20,
            this.txtAmount21,
            this.txtDatePaid21,
            this.Line167,
            this.Line168,
            this.Line169,
            this.Line170,
            this.Line171,
            this.Line172,
            this.Line173,
            this.Line174,
            this.Line175,
            this.Line99,
            this.txtDatePaid34,
            this.txtAmount34,
            this.txtDatePaid35,
            this.txtAmount35,
            this.txtDatePaid36,
            this.txtAmount36,
            this.txtDatePaid37,
            this.txtAmount37,
            this.txtDatePaid38,
            this.txtAmount38,
            this.txtDatePaid39,
            this.txtAmount39,
            this.txtDatePaid40,
            this.txtAmount40,
            this.txtDatePaid41,
            this.txtAmount41,
            this.txtDatePaid42,
            this.txtAmount42,
            this.txtAmount55,
            this.txtDatePaid55,
            this.txtAmount56,
            this.txtDatePaid56,
            this.txtAmount57,
            this.txtDatePaid57,
            this.txtAmount58,
            this.txtDatePaid58,
            this.txtAmount59,
            this.txtDatePaid59,
            this.txtAmount60,
            this.txtDatePaid60,
            this.txtAmount61,
            this.txtDatePaid61,
            this.txtAmount62,
            this.txtDatePaid62,
            this.txtAmount63,
            this.txtDatePaid63,
            this.Line176,
            this.Line177,
            this.Line178,
            this.Line179,
            this.Line180,
            this.Line181,
            this.Line182,
            this.Line183,
            this.Line184,
            this.Line185,
            this.Line186,
            this.Line187,
            this.Line188,
            this.Line189,
            this.Line190,
            this.Line191,
            this.Line192,
            this.Line193});
			this.Detail.Height = 10.09375F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Shape15
			// 
			this.Shape15.Height = 5.5F;
			this.Shape15.Left = 5.190972F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 2.666667F;
			this.Shape15.Visible = false;
			this.Shape15.Width = 2.298611F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 5.5F;
			this.Shape14.Left = 2.59375F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 2.666667F;
			this.Shape14.Visible = false;
			this.Shape14.Width = 2.298611F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 5.5F;
			this.Shape13.Left = 0.01388889F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 2.666667F;
			this.Shape13.Visible = false;
			this.Shape13.Width = 2.291667F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.1458333F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label1.Text = "Date Wages or Non-wages Paid";
			this.Label1.Top = 2.3125F;
			this.Label1.Width = 0.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.25F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.416667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label3.Text = "Payment Amount";
			this.Label3.Top = 2.3125F;
			this.Label3.Width = 0.6666667F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 1.708333F;
			this.Line13.Visible = false;
			this.Line13.Width = 7.5F;
			this.Line13.X1 = 0F;
			this.Line13.X2 = 7.5F;
			this.Line13.Y1 = 1.708333F;
			this.Line13.Y2 = 1.708333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.25F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label4.Text = "SCHEDULE 1/C1 (Form 941/C1-ME) ";
			this.Label4.Top = 0F;
			this.Label4.Width = 3.166667F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.4166667F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label13.Text = "Reconciliation of 900ME Voucher Payments or EFT Payments of Income Tax Withholdin" +
    "g";
			this.Label13.Top = 1.729167F;
			this.Label13.Width = 6.75F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.4166667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Label14.Text = "For employers or non-payroll filers required to remit withholding taxes on a semi" +
    "weekly basis (see instructions).";
			this.Label14.Top = 1.958333F;
			this.Label14.Width = 7.083333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left";
			this.Label15.Text = "12. Total (Enter on form 941/C1-ME, line 2)";
			this.Label15.Top = 8.496528F;
			this.Label15.Width = 2.145833F;
			// 
			// txtLine13
			// 
			this.txtLine13.Height = 0.1666667F;
			this.txtLine13.Left = 5.53125F;
			this.txtLine13.Name = "txtLine13";
			this.txtLine13.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtLine13.Text = "9999999999.99";
			this.txtLine13.Top = 8.496528F;
			this.txtLine13.Width = 1.6875F;
			// 
			// Line45
			// 
			this.Line45.Height = 0F;
			this.Line45.Left = 0.07291666F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 8.8125F;
			this.Line45.Visible = false;
			this.Line45.Width = 7.416667F;
			this.Line45.X1 = 0.07291666F;
			this.Line45.X2 = 7.489583F;
			this.Line45.Y1 = 8.8125F;
			this.Line45.Y2 = 8.8125F;
			// 
			// Line46
			// 
			this.Line46.Height = 0F;
			this.Line46.Left = 1.375F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 9.916667F;
			this.Line46.Width = 2.8375F;
			this.Line46.X1 = 1.375F;
			this.Line46.X2 = 4.2125F;
			this.Line46.Y1 = 9.916667F;
			this.Line46.Y2 = 9.916667F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.3125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left";
			this.Label17.Text = "For Field Advisor Use:";
			this.Label17.Top = 9.791667F;
			this.Label17.Width = 1.0625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.25F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.708333F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label18.Text = "Date Wages or Non-wages Paid";
			this.Label18.Top = 2.3125F;
			this.Label18.Width = 0.875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.25F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.0625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label20.Text = "Payment Amount";
			this.Label20.Top = 2.3125F;
			this.Label20.Width = 0.6666667F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.25F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.322917F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label21.Text = "Date Wages or Non-wages Paid";
			this.Label21.Top = 2.3125F;
			this.Label21.Width = 0.8125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.25F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 6.625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label23.Text = "Payment Amount";
			this.Label23.Top = 2.3125F;
			this.Label23.Width = 0.6666667F;
			// 
			// Barcode1
			// 
			this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.Barcode1.Height = 0.6458333F;
			this.Barcode1.Left = 5.666667F;
			this.Barcode1.Name = "Barcode1";
			this.Barcode1.QuietZoneBottom = 0F;
			this.Barcode1.QuietZoneLeft = 0F;
			this.Barcode1.QuietZoneRight = 0F;
			this.Barcode1.QuietZoneTop = 0F;
			this.Barcode1.Top = 0F;
			this.Barcode1.Width = 1.583333F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1666667F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 7.166667F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: right" +
    "";
			this.Label24.Text = "15";
			this.Label24.Top = 0.1666667F;
			this.Label24.Width = 0.3333333F;
			// 
			// Line75
			// 
			this.Line75.Height = 0F;
			this.Line75.Left = 0.08333334F;
			this.Line75.LineWeight = 1F;
			this.Line75.Name = "Line75";
			this.Line75.Top = 8.166667F;
			this.Line75.Visible = false;
			this.Line75.Width = 7.416667F;
			this.Line75.X1 = 0.08333334F;
			this.Line75.X2 = 7.5F;
			this.Line75.Y1 = 8.166667F;
			this.Line75.Y2 = 8.166667F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.3125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: underline";
			this.Label26.Text = "For the Third quarter Only: all employers or non-payroll filers, please check if " +
    "applicable:";
			this.Label26.Top = 8.9375F;
			this.Label26.Visible = false;
			this.Label26.Width = 6.75F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1666667F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.4583333F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: center";
			this.Label27.Text = "I file my return electronically or my return is prepared by a tax preparer and I " +
    "do not need Maine tax forms mailed to me next year.";
			this.Label27.Top = 9.277778F;
			this.Label27.Visible = false;
			this.Label27.Width = 6.5625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 1.09375F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier New\'";
			this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtName.Top = 0.3159722F;
			this.txtName.Width = 3.479167F;
			// 
			// txtUCEmployerAccount
			// 
			this.txtUCEmployerAccount.Height = 0.1666667F;
			this.txtUCEmployerAccount.Left = 1.09375F;
			this.txtUCEmployerAccount.Name = "txtUCEmployerAccount";
			this.txtUCEmployerAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: middle";
			this.txtUCEmployerAccount.Text = "9999999999";
			this.txtUCEmployerAccount.Top = 0.9895833F;
			this.txtUCEmployerAccount.Width = 1.083333F;
			// 
			// txtPeriodStart
			// 
			this.txtPeriodStart.Height = 0.1666667F;
			this.txtPeriodStart.Left = 0.9722222F;
			this.txtPeriodStart.Name = "txtPeriodStart";
			this.txtPeriodStart.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtPeriodStart.Text = "99 99 9999";
			this.txtPeriodStart.Top = 1.347222F;
			this.txtPeriodStart.Width = 1F;
			// 
			// txtPeriodEnd
			// 
			this.txtPeriodEnd.Height = 0.1666667F;
			this.txtPeriodEnd.Left = 2.260417F;
			this.txtPeriodEnd.Name = "txtPeriodEnd";
			this.txtPeriodEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtPeriodEnd.Text = "99 99 9999";
			this.txtPeriodEnd.Top = 1.34375F;
			this.txtPeriodEnd.Width = 1.041667F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1666667F;
			this.Field25.Left = 2.03125F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; vertical-align:" +
    " top";
			this.Field25.Text = "-";
			this.Field25.Top = 1.34375F;
			this.Field25.Visible = false;
			this.Field25.Width = 0.25F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1666667F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.1875F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Arial\'; font-size: 7pt";
			this.Label42.Text = "Name";
			this.Label42.Top = 0.3159722F;
			this.Label42.Width = 0.4583333F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.2916667F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.1875F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Arial\'; font-size: 7pt";
			this.Label43.Text = "UC Employer Account No:";
			this.Label43.Top = 0.90625F;
			this.Label43.Width = 0.9166667F;
			// 
			// txtWithholdingAccount
			// 
			this.txtWithholdingAccount.Height = 0.1666667F;
			this.txtWithholdingAccount.Left = 1.09375F;
			this.txtWithholdingAccount.Name = "txtWithholdingAccount";
			this.txtWithholdingAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: middle";
			this.txtWithholdingAccount.Text = "99 999999999";
			this.txtWithholdingAccount.Top = 0.6666667F;
			this.txtWithholdingAccount.Width = 1.166667F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.2916667F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0.1875F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Arial\'; font-size: 7pt";
			this.Label44.Text = "Withholding Account No:";
			this.Label44.Top = 0.5729167F;
			this.Label44.Width = 0.8958333F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1666667F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.1875F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Arial\'; font-size: 7pt";
			this.Label45.Text = "Period Covered:";
			this.Label45.Top = 1.347222F;
			this.Label45.Width = 1.25F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1666667F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Courier New\'";
			this.Label74.Text = "TSC";
			this.Label74.Top = 9.927083F;
			this.Label74.Visible = false;
			this.Label74.Width = 0.4166667F;
			// 
			// txtX
			// 
			this.txtX.Height = 0.1666667F;
			this.txtX.Left = 0.09722222F;
			this.txtX.Name = "txtX";
			this.txtX.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" +
    "p";
			this.txtX.Text = "X";
			this.txtX.Top = 9.208333F;
			this.txtX.Visible = false;
			this.txtX.Width = 0.25F;
			// 
			// Line121
			// 
			this.Line121.Height = 0F;
			this.Line121.Left = 0.65625F;
			this.Line121.LineWeight = 1F;
			this.Line121.Name = "Line121";
			this.Line121.Top = 1.875F;
			this.Line121.Width = 6.28125F;
			this.Line121.X1 = 0.65625F;
			this.Line121.X2 = 6.9375F;
			this.Line121.Y1 = 1.875F;
			this.Line121.Y2 = 1.875F;
			// 
			// Line122
			// 
			this.Line122.Height = 0F;
			this.Line122.Left = 0.9791667F;
			this.Line122.LineWeight = 1F;
			this.Line122.Name = "Line122";
			this.Line122.Top = 8.666667F;
			this.Line122.Visible = false;
			this.Line122.Width = 5.375F;
			this.Line122.X1 = 0.9791667F;
			this.Line122.X2 = 6.354167F;
			this.Line122.Y1 = 8.666667F;
			this.Line122.Y2 = 8.666667F;
			// 
			// Shape17
			// 
			this.Shape17.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape17.Height = 0.125F;
			this.Shape17.Left = 0.01388889F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 0.01388889F;
			this.Shape17.Width = 0.125F;
			// 
			// Shape18
			// 
			this.Shape18.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape18.Height = 0.125F;
			this.Shape18.Left = 0.01041667F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 9.875F;
			this.Shape18.Width = 0.125F;
			// 
			// Shape19
			// 
			this.Shape19.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape19.Height = 0.125F;
			this.Shape19.Left = 7.361111F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 9.875F;
			this.Shape19.Width = 0.125F;
			// 
			// txtAmount43
			// 
			this.txtAmount43.CanGrow = false;
			this.txtAmount43.Height = 0.1666667F;
			this.txtAmount43.Left = 6.548611F;
			this.txtAmount43.Name = "txtAmount43";
			this.txtAmount43.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount43.Text = "999999.99";
			this.txtAmount43.Top = 2.6875F;
			this.txtAmount43.Width = 0.8333333F;
			// 
			// txtAmount44
			// 
			this.txtAmount44.CanGrow = false;
			this.txtAmount44.Height = 0.1666667F;
			this.txtAmount44.Left = 6.552083F;
			this.txtAmount44.Name = "txtAmount44";
			this.txtAmount44.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount44.Text = "999999.99";
			this.txtAmount44.Top = 2.9375F;
			this.txtAmount44.Width = 0.8333333F;
			// 
			// txtAmount45
			// 
			this.txtAmount45.CanGrow = false;
			this.txtAmount45.Height = 0.1666667F;
			this.txtAmount45.Left = 6.552083F;
			this.txtAmount45.Name = "txtAmount45";
			this.txtAmount45.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount45.Text = "999999.99";
			this.txtAmount45.Top = 3.1875F;
			this.txtAmount45.Width = 0.8333333F;
			// 
			// txtAmount46
			// 
			this.txtAmount46.CanGrow = false;
			this.txtAmount46.Height = 0.1666667F;
			this.txtAmount46.Left = 6.552083F;
			this.txtAmount46.Name = "txtAmount46";
			this.txtAmount46.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount46.Text = "999999.99";
			this.txtAmount46.Top = 3.4375F;
			this.txtAmount46.Width = 0.8333333F;
			// 
			// txtAmount47
			// 
			this.txtAmount47.CanGrow = false;
			this.txtAmount47.Height = 0.1666667F;
			this.txtAmount47.Left = 6.552083F;
			this.txtAmount47.Name = "txtAmount47";
			this.txtAmount47.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount47.Text = "999999.99";
			this.txtAmount47.Top = 3.6875F;
			this.txtAmount47.Width = 0.8333333F;
			// 
			// txtAmount48
			// 
			this.txtAmount48.CanGrow = false;
			this.txtAmount48.Height = 0.1666667F;
			this.txtAmount48.Left = 6.552083F;
			this.txtAmount48.Name = "txtAmount48";
			this.txtAmount48.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount48.Text = "999999.99";
			this.txtAmount48.Top = 3.9375F;
			this.txtAmount48.Width = 0.8333333F;
			// 
			// txtAmount49
			// 
			this.txtAmount49.CanGrow = false;
			this.txtAmount49.Height = 0.1666667F;
			this.txtAmount49.Left = 6.552083F;
			this.txtAmount49.Name = "txtAmount49";
			this.txtAmount49.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount49.Text = "999999.99";
			this.txtAmount49.Top = 4.1875F;
			this.txtAmount49.Width = 0.8333333F;
			// 
			// txtAmount50
			// 
			this.txtAmount50.CanGrow = false;
			this.txtAmount50.Height = 0.1666667F;
			this.txtAmount50.Left = 6.552083F;
			this.txtAmount50.Name = "txtAmount50";
			this.txtAmount50.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount50.Text = "999999.99";
			this.txtAmount50.Top = 4.4375F;
			this.txtAmount50.Width = 0.8333333F;
			// 
			// txtAmount51
			// 
			this.txtAmount51.CanGrow = false;
			this.txtAmount51.Height = 0.1666667F;
			this.txtAmount51.Left = 6.552083F;
			this.txtAmount51.Name = "txtAmount51";
			this.txtAmount51.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount51.Text = "999999.99";
			this.txtAmount51.Top = 4.6875F;
			this.txtAmount51.Width = 0.8333333F;
			// 
			// txtAmount52
			// 
			this.txtAmount52.CanGrow = false;
			this.txtAmount52.Height = 0.1666667F;
			this.txtAmount52.Left = 6.552083F;
			this.txtAmount52.Name = "txtAmount52";
			this.txtAmount52.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount52.Text = "999999.99";
			this.txtAmount52.Top = 4.9375F;
			this.txtAmount52.Width = 0.8333333F;
			// 
			// txtAmount53
			// 
			this.txtAmount53.CanGrow = false;
			this.txtAmount53.Height = 0.1666667F;
			this.txtAmount53.Left = 6.552083F;
			this.txtAmount53.Name = "txtAmount53";
			this.txtAmount53.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount53.Text = "999999.99";
			this.txtAmount53.Top = 5.1875F;
			this.txtAmount53.Width = 0.8333333F;
			// 
			// txtAmount54
			// 
			this.txtAmount54.CanGrow = false;
			this.txtAmount54.Height = 0.1666667F;
			this.txtAmount54.Left = 6.552083F;
			this.txtAmount54.Name = "txtAmount54";
			this.txtAmount54.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount54.Text = "999999.99";
			this.txtAmount54.Top = 5.4375F;
			this.txtAmount54.Width = 0.8333333F;
			// 
			// txtPaymentSub3
			// 
			this.txtPaymentSub3.CanGrow = false;
			this.txtPaymentSub3.Height = 0.1666667F;
			this.txtPaymentSub3.Left = 6.552083F;
			this.txtPaymentSub3.Name = "txtPaymentSub3";
			this.txtPaymentSub3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtPaymentSub3.Text = "999999.99";
			this.txtPaymentSub3.Top = 7.9375F;
			this.txtPaymentSub3.Width = 0.8333333F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.1666667F;
			this.txtAmount1.Left = 1.368056F;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount1.Text = "999999.99";
			this.txtAmount1.Top = 2.6875F;
			this.txtAmount1.Width = 0.8333333F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.1666667F;
			this.txtAmount2.Left = 1.364583F;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount2.Text = "999999.99";
			this.txtAmount2.Top = 2.9375F;
			this.txtAmount2.Width = 0.8333333F;
			// 
			// txtAmount3
			// 
			this.txtAmount3.CanGrow = false;
			this.txtAmount3.Height = 0.1666667F;
			this.txtAmount3.Left = 1.364583F;
			this.txtAmount3.Name = "txtAmount3";
			this.txtAmount3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount3.Text = "999999.99";
			this.txtAmount3.Top = 3.1875F;
			this.txtAmount3.Width = 0.8333333F;
			// 
			// txtAmount4
			// 
			this.txtAmount4.CanGrow = false;
			this.txtAmount4.Height = 0.1666667F;
			this.txtAmount4.Left = 1.364583F;
			this.txtAmount4.Name = "txtAmount4";
			this.txtAmount4.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount4.Text = "999999.99";
			this.txtAmount4.Top = 3.4375F;
			this.txtAmount4.Width = 0.8333333F;
			// 
			// txtAmount5
			// 
			this.txtAmount5.CanGrow = false;
			this.txtAmount5.Height = 0.1666667F;
			this.txtAmount5.Left = 1.364583F;
			this.txtAmount5.Name = "txtAmount5";
			this.txtAmount5.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount5.Text = "999999.99";
			this.txtAmount5.Top = 3.6875F;
			this.txtAmount5.Width = 0.8333333F;
			// 
			// txtAmount6
			// 
			this.txtAmount6.CanGrow = false;
			this.txtAmount6.Height = 0.1666667F;
			this.txtAmount6.Left = 1.364583F;
			this.txtAmount6.Name = "txtAmount6";
			this.txtAmount6.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount6.Text = "999999.99";
			this.txtAmount6.Top = 3.9375F;
			this.txtAmount6.Width = 0.8333333F;
			// 
			// txtAmount7
			// 
			this.txtAmount7.CanGrow = false;
			this.txtAmount7.Height = 0.1666667F;
			this.txtAmount7.Left = 1.364583F;
			this.txtAmount7.Name = "txtAmount7";
			this.txtAmount7.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount7.Text = "999999.99";
			this.txtAmount7.Top = 4.1875F;
			this.txtAmount7.Width = 0.8333333F;
			// 
			// txtAmount8
			// 
			this.txtAmount8.CanGrow = false;
			this.txtAmount8.Height = 0.1666667F;
			this.txtAmount8.Left = 1.364583F;
			this.txtAmount8.Name = "txtAmount8";
			this.txtAmount8.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount8.Text = "999999.99";
			this.txtAmount8.Top = 4.4375F;
			this.txtAmount8.Width = 0.8333333F;
			// 
			// txtAmount9
			// 
			this.txtAmount9.CanGrow = false;
			this.txtAmount9.Height = 0.1666667F;
			this.txtAmount9.Left = 1.364583F;
			this.txtAmount9.Name = "txtAmount9";
			this.txtAmount9.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount9.Text = "999999.99";
			this.txtAmount9.Top = 4.6875F;
			this.txtAmount9.Width = 0.8333333F;
			// 
			// txtAmount10
			// 
			this.txtAmount10.CanGrow = false;
			this.txtAmount10.Height = 0.1666667F;
			this.txtAmount10.Left = 1.364583F;
			this.txtAmount10.Name = "txtAmount10";
			this.txtAmount10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount10.Text = "999999.99";
			this.txtAmount10.Top = 4.9375F;
			this.txtAmount10.Width = 0.8333333F;
			// 
			// txtAmount11
			// 
			this.txtAmount11.CanGrow = false;
			this.txtAmount11.Height = 0.1666667F;
			this.txtAmount11.Left = 1.364583F;
			this.txtAmount11.Name = "txtAmount11";
			this.txtAmount11.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount11.Text = "999999.99";
			this.txtAmount11.Top = 5.1875F;
			this.txtAmount11.Width = 0.8333333F;
			// 
			// txtAmount12
			// 
			this.txtAmount12.CanGrow = false;
			this.txtAmount12.Height = 0.1666667F;
			this.txtAmount12.Left = 1.364583F;
			this.txtAmount12.Name = "txtAmount12";
			this.txtAmount12.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount12.Text = "999999.99";
			this.txtAmount12.Top = 5.4375F;
			this.txtAmount12.Width = 0.8333333F;
			// 
			// txtPaymentSub1
			// 
			this.txtPaymentSub1.CanGrow = false;
			this.txtPaymentSub1.Height = 0.1666667F;
			this.txtPaymentSub1.Left = 1.364583F;
			this.txtPaymentSub1.Name = "txtPaymentSub1";
			this.txtPaymentSub1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtPaymentSub1.Text = "999999.99";
			this.txtPaymentSub1.Top = 7.9375F;
			this.txtPaymentSub1.Width = 0.8333333F;
			// 
			// txtDatePaid1
			// 
			this.txtDatePaid1.CanGrow = false;
			this.txtDatePaid1.Height = 0.1666667F;
			this.txtDatePaid1.Left = 0.1354167F;
			this.txtDatePaid1.Name = "txtDatePaid1";
			this.txtDatePaid1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid1.Text = "99 99 99";
			this.txtDatePaid1.Top = 2.691667F;
			this.txtDatePaid1.Width = 0.75F;
			// 
			// txtDatePaid2
			// 
			this.txtDatePaid2.CanGrow = false;
			this.txtDatePaid2.Height = 0.1666667F;
			this.txtDatePaid2.Left = 0.1354167F;
			this.txtDatePaid2.Name = "txtDatePaid2";
			this.txtDatePaid2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid2.Text = "99 99 99";
			this.txtDatePaid2.Top = 2.941667F;
			this.txtDatePaid2.Width = 0.75F;
			// 
			// txtDatePaid3
			// 
			this.txtDatePaid3.CanGrow = false;
			this.txtDatePaid3.Height = 0.1666667F;
			this.txtDatePaid3.Left = 0.1354167F;
			this.txtDatePaid3.Name = "txtDatePaid3";
			this.txtDatePaid3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid3.Text = "99 99 99";
			this.txtDatePaid3.Top = 3.191667F;
			this.txtDatePaid3.Width = 0.75F;
			// 
			// txtDatePaid4
			// 
			this.txtDatePaid4.CanGrow = false;
			this.txtDatePaid4.Height = 0.1666667F;
			this.txtDatePaid4.Left = 0.1354167F;
			this.txtDatePaid4.Name = "txtDatePaid4";
			this.txtDatePaid4.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid4.Text = "99 99 99";
			this.txtDatePaid4.Top = 3.441667F;
			this.txtDatePaid4.Width = 0.75F;
			// 
			// txtDatePaid5
			// 
			this.txtDatePaid5.CanGrow = false;
			this.txtDatePaid5.Height = 0.1666667F;
			this.txtDatePaid5.Left = 0.1354167F;
			this.txtDatePaid5.Name = "txtDatePaid5";
			this.txtDatePaid5.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid5.Text = "99 99 99";
			this.txtDatePaid5.Top = 3.691667F;
			this.txtDatePaid5.Width = 0.75F;
			// 
			// txtDatePaid6
			// 
			this.txtDatePaid6.CanGrow = false;
			this.txtDatePaid6.Height = 0.1666667F;
			this.txtDatePaid6.Left = 0.1354167F;
			this.txtDatePaid6.Name = "txtDatePaid6";
			this.txtDatePaid6.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid6.Text = "99 99 99";
			this.txtDatePaid6.Top = 3.941667F;
			this.txtDatePaid6.Width = 0.75F;
			// 
			// txtDatePaid7
			// 
			this.txtDatePaid7.CanGrow = false;
			this.txtDatePaid7.Height = 0.1666667F;
			this.txtDatePaid7.Left = 0.1354167F;
			this.txtDatePaid7.Name = "txtDatePaid7";
			this.txtDatePaid7.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid7.Text = "99 99 99";
			this.txtDatePaid7.Top = 4.191667F;
			this.txtDatePaid7.Width = 0.75F;
			// 
			// txtDatePaid8
			// 
			this.txtDatePaid8.CanGrow = false;
			this.txtDatePaid8.Height = 0.1666667F;
			this.txtDatePaid8.Left = 0.1354167F;
			this.txtDatePaid8.Name = "txtDatePaid8";
			this.txtDatePaid8.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid8.Text = "99 99 99";
			this.txtDatePaid8.Top = 4.441667F;
			this.txtDatePaid8.Width = 0.75F;
			// 
			// txtDatePaid9
			// 
			this.txtDatePaid9.CanGrow = false;
			this.txtDatePaid9.Height = 0.1666667F;
			this.txtDatePaid9.Left = 0.1354167F;
			this.txtDatePaid9.Name = "txtDatePaid9";
			this.txtDatePaid9.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid9.Text = "99 99 99";
			this.txtDatePaid9.Top = 4.691667F;
			this.txtDatePaid9.Width = 0.75F;
			// 
			// txtDatePaid10
			// 
			this.txtDatePaid10.CanGrow = false;
			this.txtDatePaid10.Height = 0.1666667F;
			this.txtDatePaid10.Left = 0.1354167F;
			this.txtDatePaid10.Name = "txtDatePaid10";
			this.txtDatePaid10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid10.Text = "99 99 99";
			this.txtDatePaid10.Top = 4.941667F;
			this.txtDatePaid10.Width = 0.75F;
			// 
			// txtDatePaid11
			// 
			this.txtDatePaid11.CanGrow = false;
			this.txtDatePaid11.Height = 0.1666667F;
			this.txtDatePaid11.Left = 0.1354167F;
			this.txtDatePaid11.Name = "txtDatePaid11";
			this.txtDatePaid11.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid11.Text = "99 99 99";
			this.txtDatePaid11.Top = 5.191667F;
			this.txtDatePaid11.Width = 0.75F;
			// 
			// txtDatePaid12
			// 
			this.txtDatePaid12.CanGrow = false;
			this.txtDatePaid12.Height = 0.1666667F;
			this.txtDatePaid12.Left = 0.1354167F;
			this.txtDatePaid12.Name = "txtDatePaid12";
			this.txtDatePaid12.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid12.Text = "99 99 99";
			this.txtDatePaid12.Top = 5.441667F;
			this.txtDatePaid12.Width = 0.75F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.2395833F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "background-color: rgb(255,255,255); font-size: 8.5pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle";
			this.Label85.Text = "Subtotal A";
			this.Label85.Top = 7.895833F;
			this.Label85.Width = 0.75F;
			// 
			// txtDatePaid22
			// 
			this.txtDatePaid22.CanGrow = false;
			this.txtDatePaid22.Height = 0.1666667F;
			this.txtDatePaid22.Left = 2.708333F;
			this.txtDatePaid22.Name = "txtDatePaid22";
			this.txtDatePaid22.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid22.Text = "99 99 99";
			this.txtDatePaid22.Top = 2.6875F;
			this.txtDatePaid22.Width = 0.75F;
			// 
			// txtDatePaid23
			// 
			this.txtDatePaid23.CanGrow = false;
			this.txtDatePaid23.Height = 0.1666667F;
			this.txtDatePaid23.Left = 2.708333F;
			this.txtDatePaid23.Name = "txtDatePaid23";
			this.txtDatePaid23.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid23.Text = "99 99 99";
			this.txtDatePaid23.Top = 2.9375F;
			this.txtDatePaid23.Width = 0.75F;
			// 
			// txtDatePaid24
			// 
			this.txtDatePaid24.CanGrow = false;
			this.txtDatePaid24.Height = 0.1666667F;
			this.txtDatePaid24.Left = 2.708333F;
			this.txtDatePaid24.Name = "txtDatePaid24";
			this.txtDatePaid24.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid24.Text = "99 99 99";
			this.txtDatePaid24.Top = 3.1875F;
			this.txtDatePaid24.Width = 0.75F;
			// 
			// txtDatePaid25
			// 
			this.txtDatePaid25.CanGrow = false;
			this.txtDatePaid25.Height = 0.1666667F;
			this.txtDatePaid25.Left = 2.708333F;
			this.txtDatePaid25.Name = "txtDatePaid25";
			this.txtDatePaid25.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid25.Text = "99 99 99";
			this.txtDatePaid25.Top = 3.4375F;
			this.txtDatePaid25.Width = 0.75F;
			// 
			// txtDatePaid26
			// 
			this.txtDatePaid26.CanGrow = false;
			this.txtDatePaid26.Height = 0.1666667F;
			this.txtDatePaid26.Left = 2.708333F;
			this.txtDatePaid26.Name = "txtDatePaid26";
			this.txtDatePaid26.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid26.Text = "99 99 99";
			this.txtDatePaid26.Top = 3.6875F;
			this.txtDatePaid26.Width = 0.75F;
			// 
			// txtDatePaid27
			// 
			this.txtDatePaid27.CanGrow = false;
			this.txtDatePaid27.Height = 0.1666667F;
			this.txtDatePaid27.Left = 2.708333F;
			this.txtDatePaid27.Name = "txtDatePaid27";
			this.txtDatePaid27.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid27.Text = "99 99 99";
			this.txtDatePaid27.Top = 3.9375F;
			this.txtDatePaid27.Width = 0.75F;
			// 
			// txtDatePaid28
			// 
			this.txtDatePaid28.CanGrow = false;
			this.txtDatePaid28.Height = 0.1666667F;
			this.txtDatePaid28.Left = 2.708333F;
			this.txtDatePaid28.Name = "txtDatePaid28";
			this.txtDatePaid28.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid28.Text = "99 99 99";
			this.txtDatePaid28.Top = 4.1875F;
			this.txtDatePaid28.Width = 0.75F;
			// 
			// txtDatePaid29
			// 
			this.txtDatePaid29.CanGrow = false;
			this.txtDatePaid29.Height = 0.1666667F;
			this.txtDatePaid29.Left = 2.708333F;
			this.txtDatePaid29.Name = "txtDatePaid29";
			this.txtDatePaid29.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid29.Text = "99 99 99";
			this.txtDatePaid29.Top = 4.4375F;
			this.txtDatePaid29.Width = 0.75F;
			// 
			// txtDatePaid30
			// 
			this.txtDatePaid30.CanGrow = false;
			this.txtDatePaid30.Height = 0.1666667F;
			this.txtDatePaid30.Left = 2.708333F;
			this.txtDatePaid30.Name = "txtDatePaid30";
			this.txtDatePaid30.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid30.Text = "99 99 99";
			this.txtDatePaid30.Top = 4.6875F;
			this.txtDatePaid30.Width = 0.75F;
			// 
			// txtDatePaid31
			// 
			this.txtDatePaid31.CanGrow = false;
			this.txtDatePaid31.Height = 0.1666667F;
			this.txtDatePaid31.Left = 2.708333F;
			this.txtDatePaid31.Name = "txtDatePaid31";
			this.txtDatePaid31.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid31.Text = "99 99 99";
			this.txtDatePaid31.Top = 4.9375F;
			this.txtDatePaid31.Width = 0.75F;
			// 
			// txtDatePaid32
			// 
			this.txtDatePaid32.CanGrow = false;
			this.txtDatePaid32.Height = 0.1666667F;
			this.txtDatePaid32.Left = 2.708333F;
			this.txtDatePaid32.Name = "txtDatePaid32";
			this.txtDatePaid32.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid32.Text = "99 99 99";
			this.txtDatePaid32.Top = 5.1875F;
			this.txtDatePaid32.Width = 0.75F;
			// 
			// txtDatePaid33
			// 
			this.txtDatePaid33.CanGrow = false;
			this.txtDatePaid33.Height = 0.1666667F;
			this.txtDatePaid33.Left = 2.708333F;
			this.txtDatePaid33.Name = "txtDatePaid33";
			this.txtDatePaid33.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid33.Text = "99 99 99";
			this.txtDatePaid33.Top = 5.4375F;
			this.txtDatePaid33.Width = 0.75F;
			// 
			// txtAmount22
			// 
			this.txtAmount22.CanGrow = false;
			this.txtAmount22.Height = 0.1666667F;
			this.txtAmount22.Left = 3.954861F;
			this.txtAmount22.Name = "txtAmount22";
			this.txtAmount22.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount22.Text = "999999.99";
			this.txtAmount22.Top = 2.6875F;
			this.txtAmount22.Width = 0.8333333F;
			// 
			// txtAmount23
			// 
			this.txtAmount23.CanGrow = false;
			this.txtAmount23.Height = 0.1666667F;
			this.txtAmount23.Left = 3.954861F;
			this.txtAmount23.Name = "txtAmount23";
			this.txtAmount23.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount23.Text = "999999.99";
			this.txtAmount23.Top = 2.9375F;
			this.txtAmount23.Width = 0.8333333F;
			// 
			// txtAmount24
			// 
			this.txtAmount24.CanGrow = false;
			this.txtAmount24.Height = 0.1666667F;
			this.txtAmount24.Left = 3.954861F;
			this.txtAmount24.Name = "txtAmount24";
			this.txtAmount24.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount24.Text = "999999.99";
			this.txtAmount24.Top = 3.1875F;
			this.txtAmount24.Width = 0.8333333F;
			// 
			// txtAmount25
			// 
			this.txtAmount25.CanGrow = false;
			this.txtAmount25.Height = 0.1666667F;
			this.txtAmount25.Left = 3.954861F;
			this.txtAmount25.Name = "txtAmount25";
			this.txtAmount25.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount25.Text = "999999.99";
			this.txtAmount25.Top = 3.4375F;
			this.txtAmount25.Width = 0.8333333F;
			// 
			// txtAmount26
			// 
			this.txtAmount26.CanGrow = false;
			this.txtAmount26.Height = 0.1666667F;
			this.txtAmount26.Left = 3.954861F;
			this.txtAmount26.Name = "txtAmount26";
			this.txtAmount26.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount26.Text = "999999.99";
			this.txtAmount26.Top = 3.6875F;
			this.txtAmount26.Width = 0.8333333F;
			// 
			// txtAmount27
			// 
			this.txtAmount27.CanGrow = false;
			this.txtAmount27.Height = 0.1666667F;
			this.txtAmount27.Left = 3.954861F;
			this.txtAmount27.Name = "txtAmount27";
			this.txtAmount27.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount27.Text = "999999.99";
			this.txtAmount27.Top = 3.9375F;
			this.txtAmount27.Width = 0.8333333F;
			// 
			// txtAmount28
			// 
			this.txtAmount28.CanGrow = false;
			this.txtAmount28.Height = 0.1666667F;
			this.txtAmount28.Left = 3.954861F;
			this.txtAmount28.Name = "txtAmount28";
			this.txtAmount28.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount28.Text = "999999.99";
			this.txtAmount28.Top = 4.1875F;
			this.txtAmount28.Width = 0.8333333F;
			// 
			// txtAmount29
			// 
			this.txtAmount29.CanGrow = false;
			this.txtAmount29.Height = 0.1666667F;
			this.txtAmount29.Left = 3.954861F;
			this.txtAmount29.Name = "txtAmount29";
			this.txtAmount29.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount29.Text = "999999.99";
			this.txtAmount29.Top = 4.4375F;
			this.txtAmount29.Width = 0.8333333F;
			// 
			// txtAmount30
			// 
			this.txtAmount30.CanGrow = false;
			this.txtAmount30.Height = 0.1666667F;
			this.txtAmount30.Left = 3.954861F;
			this.txtAmount30.Name = "txtAmount30";
			this.txtAmount30.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount30.Text = "999999.99";
			this.txtAmount30.Top = 4.6875F;
			this.txtAmount30.Width = 0.8333333F;
			// 
			// txtAmount31
			// 
			this.txtAmount31.CanGrow = false;
			this.txtAmount31.Height = 0.1666667F;
			this.txtAmount31.Left = 3.954861F;
			this.txtAmount31.Name = "txtAmount31";
			this.txtAmount31.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount31.Text = "999999.99";
			this.txtAmount31.Top = 4.9375F;
			this.txtAmount31.Width = 0.8333333F;
			// 
			// txtAmount32
			// 
			this.txtAmount32.CanGrow = false;
			this.txtAmount32.Height = 0.1666667F;
			this.txtAmount32.Left = 3.954861F;
			this.txtAmount32.Name = "txtAmount32";
			this.txtAmount32.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount32.Text = "999999.99";
			this.txtAmount32.Top = 5.1875F;
			this.txtAmount32.Width = 0.8333333F;
			// 
			// txtAmount33
			// 
			this.txtAmount33.CanGrow = false;
			this.txtAmount33.Height = 0.1666667F;
			this.txtAmount33.Left = 3.954861F;
			this.txtAmount33.Name = "txtAmount33";
			this.txtAmount33.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount33.Text = "999999.99";
			this.txtAmount33.Top = 5.4375F;
			this.txtAmount33.Width = 0.8333333F;
			// 
			// txtPaymentSub2
			// 
			this.txtPaymentSub2.CanGrow = false;
			this.txtPaymentSub2.Height = 0.1666667F;
			this.txtPaymentSub2.Left = 3.958333F;
			this.txtPaymentSub2.Name = "txtPaymentSub2";
			this.txtPaymentSub2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtPaymentSub2.Text = "999999.99";
			this.txtPaymentSub2.Top = 7.9375F;
			this.txtPaymentSub2.Width = 0.8333333F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.2395833F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 2.583333F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "background-color: rgb(128,128,128); font-size: 8.5pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle";
			this.Label91.Text = "Subtotal B";
			this.Label91.Top = 7.895833F;
			this.Label91.Width = 0.75F;
			// 
			// txtDatePaid43
			// 
			this.txtDatePaid43.CanGrow = false;
			this.txtDatePaid43.Height = 0.1666667F;
			this.txtDatePaid43.Left = 5.302083F;
			this.txtDatePaid43.Name = "txtDatePaid43";
			this.txtDatePaid43.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid43.Text = "99 99 99";
			this.txtDatePaid43.Top = 2.6875F;
			this.txtDatePaid43.Width = 0.75F;
			// 
			// txtDatePaid44
			// 
			this.txtDatePaid44.CanGrow = false;
			this.txtDatePaid44.Height = 0.1666667F;
			this.txtDatePaid44.Left = 5.302083F;
			this.txtDatePaid44.Name = "txtDatePaid44";
			this.txtDatePaid44.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid44.Text = "99 99 99";
			this.txtDatePaid44.Top = 2.9375F;
			this.txtDatePaid44.Width = 0.75F;
			// 
			// txtDatePaid45
			// 
			this.txtDatePaid45.CanGrow = false;
			this.txtDatePaid45.Height = 0.1666667F;
			this.txtDatePaid45.Left = 5.302083F;
			this.txtDatePaid45.Name = "txtDatePaid45";
			this.txtDatePaid45.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid45.Text = "99 99 99";
			this.txtDatePaid45.Top = 3.1875F;
			this.txtDatePaid45.Width = 0.75F;
			// 
			// txtDatePaid46
			// 
			this.txtDatePaid46.CanGrow = false;
			this.txtDatePaid46.Height = 0.1666667F;
			this.txtDatePaid46.Left = 5.302083F;
			this.txtDatePaid46.Name = "txtDatePaid46";
			this.txtDatePaid46.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid46.Text = "99 99 99";
			this.txtDatePaid46.Top = 3.4375F;
			this.txtDatePaid46.Width = 0.75F;
			// 
			// txtDatePaid47
			// 
			this.txtDatePaid47.CanGrow = false;
			this.txtDatePaid47.Height = 0.1666667F;
			this.txtDatePaid47.Left = 5.302083F;
			this.txtDatePaid47.Name = "txtDatePaid47";
			this.txtDatePaid47.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid47.Text = "99 99 99";
			this.txtDatePaid47.Top = 3.6875F;
			this.txtDatePaid47.Width = 0.75F;
			// 
			// txtDatePaid48
			// 
			this.txtDatePaid48.CanGrow = false;
			this.txtDatePaid48.Height = 0.1666667F;
			this.txtDatePaid48.Left = 5.302083F;
			this.txtDatePaid48.Name = "txtDatePaid48";
			this.txtDatePaid48.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid48.Text = "99 99 99";
			this.txtDatePaid48.Top = 3.9375F;
			this.txtDatePaid48.Width = 0.75F;
			// 
			// txtDatePaid49
			// 
			this.txtDatePaid49.CanGrow = false;
			this.txtDatePaid49.Height = 0.1666667F;
			this.txtDatePaid49.Left = 5.302083F;
			this.txtDatePaid49.Name = "txtDatePaid49";
			this.txtDatePaid49.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid49.Text = "99 99 99";
			this.txtDatePaid49.Top = 4.1875F;
			this.txtDatePaid49.Width = 0.75F;
			// 
			// txtDatePaid50
			// 
			this.txtDatePaid50.CanGrow = false;
			this.txtDatePaid50.Height = 0.1666667F;
			this.txtDatePaid50.Left = 5.302083F;
			this.txtDatePaid50.Name = "txtDatePaid50";
			this.txtDatePaid50.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid50.Text = "99 99 99";
			this.txtDatePaid50.Top = 4.4375F;
			this.txtDatePaid50.Width = 0.75F;
			// 
			// txtDatePaid51
			// 
			this.txtDatePaid51.CanGrow = false;
			this.txtDatePaid51.Height = 0.1666667F;
			this.txtDatePaid51.Left = 5.302083F;
			this.txtDatePaid51.Name = "txtDatePaid51";
			this.txtDatePaid51.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid51.Text = "99 99 99";
			this.txtDatePaid51.Top = 4.6875F;
			this.txtDatePaid51.Width = 0.75F;
			// 
			// txtDatePaid52
			// 
			this.txtDatePaid52.CanGrow = false;
			this.txtDatePaid52.Height = 0.1666667F;
			this.txtDatePaid52.Left = 5.302083F;
			this.txtDatePaid52.Name = "txtDatePaid52";
			this.txtDatePaid52.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid52.Text = "99 99 99";
			this.txtDatePaid52.Top = 4.9375F;
			this.txtDatePaid52.Width = 0.75F;
			// 
			// txtDatePaid53
			// 
			this.txtDatePaid53.CanGrow = false;
			this.txtDatePaid53.Height = 0.1666667F;
			this.txtDatePaid53.Left = 5.302083F;
			this.txtDatePaid53.Name = "txtDatePaid53";
			this.txtDatePaid53.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid53.Text = "99 99 99";
			this.txtDatePaid53.Top = 5.1875F;
			this.txtDatePaid53.Width = 0.75F;
			// 
			// txtDatePaid54
			// 
			this.txtDatePaid54.CanGrow = false;
			this.txtDatePaid54.Height = 0.1666667F;
			this.txtDatePaid54.Left = 5.302083F;
			this.txtDatePaid54.Name = "txtDatePaid54";
			this.txtDatePaid54.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid54.Text = "99 99 99";
			this.txtDatePaid54.Top = 5.4375F;
			this.txtDatePaid54.Width = 0.75F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.2395833F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 5.208333F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "background-color: rgb(255,255,255); font-size: 8.5pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle";
			this.Label95.Text = "Subtotal C";
			this.Label95.Top = 7.895833F;
			this.Label95.Width = 0.75F;
			// 
			// Line100
			// 
			this.Line100.Height = 0F;
			this.Line100.Left = 0.01388889F;
			this.Line100.LineWeight = 1F;
			this.Line100.Name = "Line100";
			this.Line100.Top = 2.9F;
			this.Line100.Visible = false;
			this.Line100.Width = 2.291667F;
			this.Line100.X1 = 0.01388889F;
			this.Line100.X2 = 2.305556F;
			this.Line100.Y1 = 2.9F;
			this.Line100.Y2 = 2.9F;
			// 
			// Line101
			// 
			this.Line101.Height = 0F;
			this.Line101.Left = 0.01388889F;
			this.Line101.LineWeight = 1F;
			this.Line101.Name = "Line101";
			this.Line101.Top = 3.15F;
			this.Line101.Visible = false;
			this.Line101.Width = 2.291667F;
			this.Line101.X1 = 0.01388889F;
			this.Line101.X2 = 2.305556F;
			this.Line101.Y1 = 3.15F;
			this.Line101.Y2 = 3.15F;
			// 
			// Line102
			// 
			this.Line102.Height = 0F;
			this.Line102.Left = 0.01388889F;
			this.Line102.LineWeight = 1F;
			this.Line102.Name = "Line102";
			this.Line102.Top = 3.4F;
			this.Line102.Visible = false;
			this.Line102.Width = 2.291667F;
			this.Line102.X1 = 0.01388889F;
			this.Line102.X2 = 2.305556F;
			this.Line102.Y1 = 3.4F;
			this.Line102.Y2 = 3.4F;
			// 
			// Line103
			// 
			this.Line103.Height = 0F;
			this.Line103.Left = 0.01388889F;
			this.Line103.LineWeight = 1F;
			this.Line103.Name = "Line103";
			this.Line103.Top = 3.65F;
			this.Line103.Visible = false;
			this.Line103.Width = 2.291667F;
			this.Line103.X1 = 0.01388889F;
			this.Line103.X2 = 2.305556F;
			this.Line103.Y1 = 3.65F;
			this.Line103.Y2 = 3.65F;
			// 
			// Line104
			// 
			this.Line104.Height = 0F;
			this.Line104.Left = 0.01388889F;
			this.Line104.LineWeight = 1F;
			this.Line104.Name = "Line104";
			this.Line104.Top = 3.9F;
			this.Line104.Visible = false;
			this.Line104.Width = 2.291667F;
			this.Line104.X1 = 0.01388889F;
			this.Line104.X2 = 2.305556F;
			this.Line104.Y1 = 3.9F;
			this.Line104.Y2 = 3.9F;
			// 
			// Line106
			// 
			this.Line106.Height = 0F;
			this.Line106.Left = 0.01388889F;
			this.Line106.LineWeight = 1F;
			this.Line106.Name = "Line106";
			this.Line106.Top = 4.15F;
			this.Line106.Visible = false;
			this.Line106.Width = 2.291667F;
			this.Line106.X1 = 0.01388889F;
			this.Line106.X2 = 2.305556F;
			this.Line106.Y1 = 4.15F;
			this.Line106.Y2 = 4.15F;
			// 
			// Line108
			// 
			this.Line108.Height = 0F;
			this.Line108.Left = 0.01388889F;
			this.Line108.LineWeight = 1F;
			this.Line108.Name = "Line108";
			this.Line108.Top = 4.4F;
			this.Line108.Visible = false;
			this.Line108.Width = 2.291667F;
			this.Line108.X1 = 0.01388889F;
			this.Line108.X2 = 2.305556F;
			this.Line108.Y1 = 4.4F;
			this.Line108.Y2 = 4.4F;
			// 
			// Line109
			// 
			this.Line109.Height = 0F;
			this.Line109.Left = 0.01388889F;
			this.Line109.LineWeight = 1F;
			this.Line109.Name = "Line109";
			this.Line109.Top = 4.65F;
			this.Line109.Visible = false;
			this.Line109.Width = 2.291667F;
			this.Line109.X1 = 0.01388889F;
			this.Line109.X2 = 2.305556F;
			this.Line109.Y1 = 4.65F;
			this.Line109.Y2 = 4.65F;
			// 
			// Line111
			// 
			this.Line111.Height = 0F;
			this.Line111.Left = 0.01388889F;
			this.Line111.LineWeight = 1F;
			this.Line111.Name = "Line111";
			this.Line111.Top = 4.9F;
			this.Line111.Visible = false;
			this.Line111.Width = 2.291667F;
			this.Line111.X1 = 0.01388889F;
			this.Line111.X2 = 2.305556F;
			this.Line111.Y1 = 4.9F;
			this.Line111.Y2 = 4.9F;
			// 
			// Line112
			// 
			this.Line112.Height = 0F;
			this.Line112.Left = 0.01388889F;
			this.Line112.LineWeight = 1F;
			this.Line112.Name = "Line112";
			this.Line112.Top = 5.15F;
			this.Line112.Visible = false;
			this.Line112.Width = 2.291667F;
			this.Line112.X1 = 0.01388889F;
			this.Line112.X2 = 2.305556F;
			this.Line112.Y1 = 5.15F;
			this.Line112.Y2 = 5.15F;
			// 
			// Line113
			// 
			this.Line113.Height = 0F;
			this.Line113.Left = 0.01388889F;
			this.Line113.LineWeight = 1F;
			this.Line113.Name = "Line113";
			this.Line113.Top = 5.4F;
			this.Line113.Visible = false;
			this.Line113.Width = 2.291667F;
			this.Line113.X1 = 0.01388889F;
			this.Line113.X2 = 2.305556F;
			this.Line113.Y1 = 5.4F;
			this.Line113.Y2 = 5.4F;
			// 
			// Line114
			// 
			this.Line114.Height = 0F;
			this.Line114.Left = 0.01388889F;
			this.Line114.LineWeight = 1F;
			this.Line114.Name = "Line114";
			this.Line114.Top = 5.65F;
			this.Line114.Visible = false;
			this.Line114.Width = 2.291667F;
			this.Line114.X1 = 0.01388889F;
			this.Line114.X2 = 2.305556F;
			this.Line114.Y1 = 5.65F;
			this.Line114.Y2 = 5.65F;
			// 
			// Line123
			// 
			this.Line123.Height = 5.5F;
			this.Line123.Left = 3.732639F;
			this.Line123.LineWeight = 1F;
			this.Line123.Name = "Line123";
			this.Line123.Top = 2.666667F;
			this.Line123.Visible = false;
			this.Line123.Width = 0F;
			this.Line123.X1 = 3.732639F;
			this.Line123.X2 = 3.732639F;
			this.Line123.Y1 = 2.666667F;
			this.Line123.Y2 = 8.166667F;
			// 
			// Line124
			// 
			this.Line124.Height = 0F;
			this.Line124.Left = 2.59375F;
			this.Line124.LineWeight = 1F;
			this.Line124.Name = "Line124";
			this.Line124.Top = 2.895833F;
			this.Line124.Visible = false;
			this.Line124.Width = 2.302083F;
			this.Line124.X1 = 2.59375F;
			this.Line124.X2 = 4.895833F;
			this.Line124.Y1 = 2.895833F;
			this.Line124.Y2 = 2.895833F;
			// 
			// Line125
			// 
			this.Line125.Height = 0F;
			this.Line125.Left = 2.59375F;
			this.Line125.LineWeight = 1F;
			this.Line125.Name = "Line125";
			this.Line125.Top = 3.145833F;
			this.Line125.Visible = false;
			this.Line125.Width = 2.302083F;
			this.Line125.X1 = 2.59375F;
			this.Line125.X2 = 4.895833F;
			this.Line125.Y1 = 3.145833F;
			this.Line125.Y2 = 3.145833F;
			// 
			// Line126
			// 
			this.Line126.Height = 0F;
			this.Line126.Left = 2.59375F;
			this.Line126.LineWeight = 1F;
			this.Line126.Name = "Line126";
			this.Line126.Top = 3.395833F;
			this.Line126.Visible = false;
			this.Line126.Width = 2.302083F;
			this.Line126.X1 = 2.59375F;
			this.Line126.X2 = 4.895833F;
			this.Line126.Y1 = 3.395833F;
			this.Line126.Y2 = 3.395833F;
			// 
			// Line127
			// 
			this.Line127.Height = 0F;
			this.Line127.Left = 2.59375F;
			this.Line127.LineWeight = 1F;
			this.Line127.Name = "Line127";
			this.Line127.Top = 3.645833F;
			this.Line127.Visible = false;
			this.Line127.Width = 2.302083F;
			this.Line127.X1 = 2.59375F;
			this.Line127.X2 = 4.895833F;
			this.Line127.Y1 = 3.645833F;
			this.Line127.Y2 = 3.645833F;
			// 
			// Line128
			// 
			this.Line128.Height = 0F;
			this.Line128.Left = 2.59375F;
			this.Line128.LineWeight = 1F;
			this.Line128.Name = "Line128";
			this.Line128.Top = 3.895833F;
			this.Line128.Visible = false;
			this.Line128.Width = 2.302083F;
			this.Line128.X1 = 2.59375F;
			this.Line128.X2 = 4.895833F;
			this.Line128.Y1 = 3.895833F;
			this.Line128.Y2 = 3.895833F;
			// 
			// Line130
			// 
			this.Line130.Height = 0F;
			this.Line130.Left = 2.59375F;
			this.Line130.LineWeight = 1F;
			this.Line130.Name = "Line130";
			this.Line130.Top = 4.145833F;
			this.Line130.Visible = false;
			this.Line130.Width = 2.302083F;
			this.Line130.X1 = 2.59375F;
			this.Line130.X2 = 4.895833F;
			this.Line130.Y1 = 4.145833F;
			this.Line130.Y2 = 4.145833F;
			// 
			// Line131
			// 
			this.Line131.Height = 0F;
			this.Line131.Left = 2.59375F;
			this.Line131.LineWeight = 1F;
			this.Line131.Name = "Line131";
			this.Line131.Top = 4.645833F;
			this.Line131.Visible = false;
			this.Line131.Width = 2.302083F;
			this.Line131.X1 = 2.59375F;
			this.Line131.X2 = 4.895833F;
			this.Line131.Y1 = 4.645833F;
			this.Line131.Y2 = 4.645833F;
			// 
			// Line133
			// 
			this.Line133.Height = 0F;
			this.Line133.Left = 2.59375F;
			this.Line133.LineWeight = 1F;
			this.Line133.Name = "Line133";
			this.Line133.Top = 4.895833F;
			this.Line133.Visible = false;
			this.Line133.Width = 2.302083F;
			this.Line133.X1 = 2.59375F;
			this.Line133.X2 = 4.895833F;
			this.Line133.Y1 = 4.895833F;
			this.Line133.Y2 = 4.895833F;
			// 
			// Line134
			// 
			this.Line134.Height = 0F;
			this.Line134.Left = 2.59375F;
			this.Line134.LineWeight = 1F;
			this.Line134.Name = "Line134";
			this.Line134.Top = 5.145833F;
			this.Line134.Visible = false;
			this.Line134.Width = 2.302083F;
			this.Line134.X1 = 2.59375F;
			this.Line134.X2 = 4.895833F;
			this.Line134.Y1 = 5.145833F;
			this.Line134.Y2 = 5.145833F;
			// 
			// Line136
			// 
			this.Line136.Height = 0F;
			this.Line136.Left = 2.59375F;
			this.Line136.LineWeight = 1F;
			this.Line136.Name = "Line136";
			this.Line136.Top = 5.395833F;
			this.Line136.Visible = false;
			this.Line136.Width = 2.302083F;
			this.Line136.X1 = 2.59375F;
			this.Line136.X2 = 4.895833F;
			this.Line136.Y1 = 5.395833F;
			this.Line136.Y2 = 5.395833F;
			// 
			// Line137
			// 
			this.Line137.Height = 0F;
			this.Line137.Left = 2.59375F;
			this.Line137.LineWeight = 1F;
			this.Line137.Name = "Line137";
			this.Line137.Top = 5.645833F;
			this.Line137.Visible = false;
			this.Line137.Width = 2.302083F;
			this.Line137.X1 = 2.59375F;
			this.Line137.X2 = 4.895833F;
			this.Line137.Y1 = 5.645833F;
			this.Line137.Y2 = 5.645833F;
			// 
			// Line143
			// 
			this.Line143.Height = 0F;
			this.Line143.Left = 2.59375F;
			this.Line143.LineWeight = 1F;
			this.Line143.Name = "Line143";
			this.Line143.Top = 4.395833F;
			this.Line143.Visible = false;
			this.Line143.Width = 2.302083F;
			this.Line143.X1 = 2.59375F;
			this.Line143.X2 = 4.895833F;
			this.Line143.Y1 = 4.395833F;
			this.Line143.Y2 = 4.395833F;
			// 
			// Line145
			// 
			this.Line145.Height = 5.5F;
			this.Line145.Left = 6.329861F;
			this.Line145.LineWeight = 1F;
			this.Line145.Name = "Line145";
			this.Line145.Top = 2.666667F;
			this.Line145.Visible = false;
			this.Line145.Width = 0F;
			this.Line145.X1 = 6.329861F;
			this.Line145.X2 = 6.329861F;
			this.Line145.Y1 = 2.666667F;
			this.Line145.Y2 = 8.166667F;
			// 
			// Line146
			// 
			this.Line146.Height = 0F;
			this.Line146.Left = 5.197917F;
			this.Line146.LineWeight = 1F;
			this.Line146.Name = "Line146";
			this.Line146.Top = 2.895833F;
			this.Line146.Visible = false;
			this.Line146.Width = 2.291666F;
			this.Line146.X1 = 5.197917F;
			this.Line146.X2 = 7.489583F;
			this.Line146.Y1 = 2.895833F;
			this.Line146.Y2 = 2.895833F;
			// 
			// Line147
			// 
			this.Line147.Height = 0F;
			this.Line147.Left = 5.197917F;
			this.Line147.LineWeight = 1F;
			this.Line147.Name = "Line147";
			this.Line147.Top = 3.145833F;
			this.Line147.Visible = false;
			this.Line147.Width = 2.291666F;
			this.Line147.X1 = 5.197917F;
			this.Line147.X2 = 7.489583F;
			this.Line147.Y1 = 3.145833F;
			this.Line147.Y2 = 3.145833F;
			// 
			// Line148
			// 
			this.Line148.Height = 0F;
			this.Line148.Left = 5.197917F;
			this.Line148.LineWeight = 1F;
			this.Line148.Name = "Line148";
			this.Line148.Top = 3.395833F;
			this.Line148.Visible = false;
			this.Line148.Width = 2.291666F;
			this.Line148.X1 = 5.197917F;
			this.Line148.X2 = 7.489583F;
			this.Line148.Y1 = 3.395833F;
			this.Line148.Y2 = 3.395833F;
			// 
			// Line150
			// 
			this.Line150.Height = 0F;
			this.Line150.Left = 5.197917F;
			this.Line150.LineWeight = 1F;
			this.Line150.Name = "Line150";
			this.Line150.Top = 3.645833F;
			this.Line150.Visible = false;
			this.Line150.Width = 2.291666F;
			this.Line150.X1 = 5.197917F;
			this.Line150.X2 = 7.489583F;
			this.Line150.Y1 = 3.645833F;
			this.Line150.Y2 = 3.645833F;
			// 
			// Line151
			// 
			this.Line151.Height = 0F;
			this.Line151.Left = 5.197917F;
			this.Line151.LineWeight = 1F;
			this.Line151.Name = "Line151";
			this.Line151.Top = 3.895833F;
			this.Line151.Visible = false;
			this.Line151.Width = 2.291666F;
			this.Line151.X1 = 5.197917F;
			this.Line151.X2 = 7.489583F;
			this.Line151.Y1 = 3.895833F;
			this.Line151.Y2 = 3.895833F;
			// 
			// Line152
			// 
			this.Line152.Height = 0F;
			this.Line152.Left = 5.197917F;
			this.Line152.LineWeight = 1F;
			this.Line152.Name = "Line152";
			this.Line152.Top = 4.145833F;
			this.Line152.Visible = false;
			this.Line152.Width = 2.291666F;
			this.Line152.X1 = 5.197917F;
			this.Line152.X2 = 7.489583F;
			this.Line152.Y1 = 4.145833F;
			this.Line152.Y2 = 4.145833F;
			// 
			// Line154
			// 
			this.Line154.Height = 0F;
			this.Line154.Left = 5.197917F;
			this.Line154.LineWeight = 1F;
			this.Line154.Name = "Line154";
			this.Line154.Top = 4.645833F;
			this.Line154.Visible = false;
			this.Line154.Width = 2.291666F;
			this.Line154.X1 = 5.197917F;
			this.Line154.X2 = 7.489583F;
			this.Line154.Y1 = 4.645833F;
			this.Line154.Y2 = 4.645833F;
			// 
			// Line155
			// 
			this.Line155.Height = 0F;
			this.Line155.Left = 5.197917F;
			this.Line155.LineWeight = 1F;
			this.Line155.Name = "Line155";
			this.Line155.Top = 4.895833F;
			this.Line155.Visible = false;
			this.Line155.Width = 2.291666F;
			this.Line155.X1 = 5.197917F;
			this.Line155.X2 = 7.489583F;
			this.Line155.Y1 = 4.895833F;
			this.Line155.Y2 = 4.895833F;
			// 
			// Line157
			// 
			this.Line157.Height = 0F;
			this.Line157.Left = 5.197917F;
			this.Line157.LineWeight = 1F;
			this.Line157.Name = "Line157";
			this.Line157.Top = 5.145833F;
			this.Line157.Visible = false;
			this.Line157.Width = 2.291666F;
			this.Line157.X1 = 5.197917F;
			this.Line157.X2 = 7.489583F;
			this.Line157.Y1 = 5.145833F;
			this.Line157.Y2 = 5.145833F;
			// 
			// Line158
			// 
			this.Line158.Height = 0F;
			this.Line158.Left = 5.197917F;
			this.Line158.LineWeight = 1F;
			this.Line158.Name = "Line158";
			this.Line158.Top = 5.395833F;
			this.Line158.Visible = false;
			this.Line158.Width = 2.291666F;
			this.Line158.X1 = 5.197917F;
			this.Line158.X2 = 7.489583F;
			this.Line158.Y1 = 5.395833F;
			this.Line158.Y2 = 5.395833F;
			// 
			// Line160
			// 
			this.Line160.Height = 0F;
			this.Line160.Left = 5.197917F;
			this.Line160.LineWeight = 1F;
			this.Line160.Name = "Line160";
			this.Line160.Top = 5.645833F;
			this.Line160.Visible = false;
			this.Line160.Width = 2.291666F;
			this.Line160.X1 = 5.197917F;
			this.Line160.X2 = 7.489583F;
			this.Line160.Y1 = 5.645833F;
			this.Line160.Y2 = 5.645833F;
			// 
			// Line166
			// 
			this.Line166.Height = 0F;
			this.Line166.Left = 5.197917F;
			this.Line166.LineWeight = 1F;
			this.Line166.Name = "Line166";
			this.Line166.Top = 4.395833F;
			this.Line166.Visible = false;
			this.Line166.Width = 2.291666F;
			this.Line166.X1 = 5.197917F;
			this.Line166.X2 = 7.489583F;
			this.Line166.Y1 = 4.395833F;
			this.Line166.Y2 = 4.395833F;
			// 
			// txtAmount13
			// 
			this.txtAmount13.CanGrow = false;
			this.txtAmount13.Height = 0.1666667F;
			this.txtAmount13.Left = 1.364583F;
			this.txtAmount13.Name = "txtAmount13";
			this.txtAmount13.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount13.Text = "999999.99";
			this.txtAmount13.Top = 5.6875F;
			this.txtAmount13.Width = 0.8333333F;
			// 
			// txtDatePaid13
			// 
			this.txtDatePaid13.CanGrow = false;
			this.txtDatePaid13.Height = 0.1666667F;
			this.txtDatePaid13.Left = 0.1354167F;
			this.txtDatePaid13.Name = "txtDatePaid13";
			this.txtDatePaid13.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid13.Text = "99 99 99";
			this.txtDatePaid13.Top = 5.691667F;
			this.txtDatePaid13.Width = 0.75F;
			// 
			// txtAmount14
			// 
			this.txtAmount14.CanGrow = false;
			this.txtAmount14.Height = 0.1666667F;
			this.txtAmount14.Left = 1.364583F;
			this.txtAmount14.Name = "txtAmount14";
			this.txtAmount14.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount14.Text = "999999.99";
			this.txtAmount14.Top = 5.941667F;
			this.txtAmount14.Width = 0.8333333F;
			// 
			// txtDatePaid14
			// 
			this.txtDatePaid14.CanGrow = false;
			this.txtDatePaid14.Height = 0.1666667F;
			this.txtDatePaid14.Left = 0.1354167F;
			this.txtDatePaid14.Name = "txtDatePaid14";
			this.txtDatePaid14.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid14.Text = "99 99 99";
			this.txtDatePaid14.Top = 5.941667F;
			this.txtDatePaid14.Width = 0.75F;
			// 
			// txtAmount15
			// 
			this.txtAmount15.CanGrow = false;
			this.txtAmount15.Height = 0.1666667F;
			this.txtAmount15.Left = 1.364583F;
			this.txtAmount15.Name = "txtAmount15";
			this.txtAmount15.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount15.Text = "999999.99";
			this.txtAmount15.Top = 6.191667F;
			this.txtAmount15.Width = 0.8333333F;
			// 
			// txtDatePaid15
			// 
			this.txtDatePaid15.CanGrow = false;
			this.txtDatePaid15.Height = 0.1666667F;
			this.txtDatePaid15.Left = 0.1354167F;
			this.txtDatePaid15.Name = "txtDatePaid15";
			this.txtDatePaid15.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid15.Text = "99 99 99";
			this.txtDatePaid15.Top = 6.191667F;
			this.txtDatePaid15.Width = 0.75F;
			// 
			// txtAmount16
			// 
			this.txtAmount16.CanGrow = false;
			this.txtAmount16.Height = 0.1666667F;
			this.txtAmount16.Left = 1.364583F;
			this.txtAmount16.Name = "txtAmount16";
			this.txtAmount16.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount16.Text = "999999.99";
			this.txtAmount16.Top = 6.441667F;
			this.txtAmount16.Width = 0.8333333F;
			// 
			// txtDatePaid16
			// 
			this.txtDatePaid16.CanGrow = false;
			this.txtDatePaid16.Height = 0.1666667F;
			this.txtDatePaid16.Left = 0.1354167F;
			this.txtDatePaid16.Name = "txtDatePaid16";
			this.txtDatePaid16.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid16.Text = "99 99 99";
			this.txtDatePaid16.Top = 6.441667F;
			this.txtDatePaid16.Width = 0.75F;
			// 
			// txtAmount17
			// 
			this.txtAmount17.CanGrow = false;
			this.txtAmount17.Height = 0.1666667F;
			this.txtAmount17.Left = 1.364583F;
			this.txtAmount17.Name = "txtAmount17";
			this.txtAmount17.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount17.Text = "999999.99";
			this.txtAmount17.Top = 6.691667F;
			this.txtAmount17.Width = 0.8333333F;
			// 
			// txtDatePaid17
			// 
			this.txtDatePaid17.CanGrow = false;
			this.txtDatePaid17.Height = 0.1666667F;
			this.txtDatePaid17.Left = 0.1354167F;
			this.txtDatePaid17.Name = "txtDatePaid17";
			this.txtDatePaid17.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid17.Text = "99 99 99";
			this.txtDatePaid17.Top = 6.691667F;
			this.txtDatePaid17.Width = 0.75F;
			// 
			// txtAmount18
			// 
			this.txtAmount18.CanGrow = false;
			this.txtAmount18.Height = 0.1666667F;
			this.txtAmount18.Left = 1.364583F;
			this.txtAmount18.Name = "txtAmount18";
			this.txtAmount18.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount18.Text = "999999.99";
			this.txtAmount18.Top = 6.941667F;
			this.txtAmount18.Width = 0.8333333F;
			// 
			// txtDatePaid18
			// 
			this.txtDatePaid18.CanGrow = false;
			this.txtDatePaid18.Height = 0.1666667F;
			this.txtDatePaid18.Left = 0.1354167F;
			this.txtDatePaid18.Name = "txtDatePaid18";
			this.txtDatePaid18.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid18.Text = "99 99 99";
			this.txtDatePaid18.Top = 6.941667F;
			this.txtDatePaid18.Width = 0.75F;
			// 
			// txtAmount19
			// 
			this.txtAmount19.CanGrow = false;
			this.txtAmount19.Height = 0.1666667F;
			this.txtAmount19.Left = 1.364583F;
			this.txtAmount19.Name = "txtAmount19";
			this.txtAmount19.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount19.Text = "999999.99";
			this.txtAmount19.Top = 7.191667F;
			this.txtAmount19.Width = 0.8333333F;
			// 
			// txtDatePaid19
			// 
			this.txtDatePaid19.CanGrow = false;
			this.txtDatePaid19.Height = 0.1666667F;
			this.txtDatePaid19.Left = 0.1354167F;
			this.txtDatePaid19.Name = "txtDatePaid19";
			this.txtDatePaid19.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid19.Text = "99 99 99";
			this.txtDatePaid19.Top = 7.191667F;
			this.txtDatePaid19.Width = 0.75F;
			// 
			// txtAmount20
			// 
			this.txtAmount20.CanGrow = false;
			this.txtAmount20.Height = 0.1666667F;
			this.txtAmount20.Left = 1.364583F;
			this.txtAmount20.Name = "txtAmount20";
			this.txtAmount20.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount20.Text = "999999.99";
			this.txtAmount20.Top = 7.441667F;
			this.txtAmount20.Width = 0.8333333F;
			// 
			// txtDatePaid20
			// 
			this.txtDatePaid20.CanGrow = false;
			this.txtDatePaid20.Height = 0.1666667F;
			this.txtDatePaid20.Left = 0.1354167F;
			this.txtDatePaid20.Name = "txtDatePaid20";
			this.txtDatePaid20.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid20.Text = "99 99 99";
			this.txtDatePaid20.Top = 7.441667F;
			this.txtDatePaid20.Width = 0.75F;
			// 
			// txtAmount21
			// 
			this.txtAmount21.CanGrow = false;
			this.txtAmount21.Height = 0.1666667F;
			this.txtAmount21.Left = 1.364583F;
			this.txtAmount21.Name = "txtAmount21";
			this.txtAmount21.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount21.Text = "999999.99";
			this.txtAmount21.Top = 7.691667F;
			this.txtAmount21.Width = 0.8333333F;
			// 
			// txtDatePaid21
			// 
			this.txtDatePaid21.CanGrow = false;
			this.txtDatePaid21.Height = 0.1666667F;
			this.txtDatePaid21.Left = 0.15625F;
			this.txtDatePaid21.Name = "txtDatePaid21";
			this.txtDatePaid21.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid21.Text = "99 99 99";
			this.txtDatePaid21.Top = 7.6875F;
			this.txtDatePaid21.Width = 0.75F;
			// 
			// Line167
			// 
			this.Line167.Height = 0F;
			this.Line167.Left = 0.01388889F;
			this.Line167.LineWeight = 1F;
			this.Line167.Name = "Line167";
			this.Line167.Top = 5.9F;
			this.Line167.Visible = false;
			this.Line167.Width = 2.291667F;
			this.Line167.X1 = 0.01388889F;
			this.Line167.X2 = 2.305556F;
			this.Line167.Y1 = 5.9F;
			this.Line167.Y2 = 5.9F;
			// 
			// Line168
			// 
			this.Line168.Height = 0F;
			this.Line168.Left = 0.01041667F;
			this.Line168.LineWeight = 1F;
			this.Line168.Name = "Line168";
			this.Line168.Top = 6.15F;
			this.Line168.Visible = false;
			this.Line168.Width = 2.291666F;
			this.Line168.X1 = 0.01041667F;
			this.Line168.X2 = 2.302083F;
			this.Line168.Y1 = 6.15F;
			this.Line168.Y2 = 6.15F;
			// 
			// Line169
			// 
			this.Line169.Height = 0F;
			this.Line169.Left = 0.01041667F;
			this.Line169.LineWeight = 1F;
			this.Line169.Name = "Line169";
			this.Line169.Top = 6.4F;
			this.Line169.Visible = false;
			this.Line169.Width = 2.291666F;
			this.Line169.X1 = 0.01041667F;
			this.Line169.X2 = 2.302083F;
			this.Line169.Y1 = 6.4F;
			this.Line169.Y2 = 6.4F;
			// 
			// Line170
			// 
			this.Line170.Height = 0F;
			this.Line170.Left = 0.01041667F;
			this.Line170.LineWeight = 1F;
			this.Line170.Name = "Line170";
			this.Line170.Top = 6.65F;
			this.Line170.Visible = false;
			this.Line170.Width = 2.291666F;
			this.Line170.X1 = 0.01041667F;
			this.Line170.X2 = 2.302083F;
			this.Line170.Y1 = 6.65F;
			this.Line170.Y2 = 6.65F;
			// 
			// Line171
			// 
			this.Line171.Height = 0F;
			this.Line171.Left = 0.01041667F;
			this.Line171.LineWeight = 1F;
			this.Line171.Name = "Line171";
			this.Line171.Top = 6.9F;
			this.Line171.Visible = false;
			this.Line171.Width = 2.291666F;
			this.Line171.X1 = 0.01041667F;
			this.Line171.X2 = 2.302083F;
			this.Line171.Y1 = 6.9F;
			this.Line171.Y2 = 6.9F;
			// 
			// Line172
			// 
			this.Line172.Height = 0F;
			this.Line172.Left = 0.01041667F;
			this.Line172.LineWeight = 1F;
			this.Line172.Name = "Line172";
			this.Line172.Top = 7.15F;
			this.Line172.Visible = false;
			this.Line172.Width = 2.291666F;
			this.Line172.X1 = 0.01041667F;
			this.Line172.X2 = 2.302083F;
			this.Line172.Y1 = 7.15F;
			this.Line172.Y2 = 7.15F;
			// 
			// Line173
			// 
			this.Line173.Height = 0F;
			this.Line173.Left = 0.01041667F;
			this.Line173.LineWeight = 1F;
			this.Line173.Name = "Line173";
			this.Line173.Top = 7.4F;
			this.Line173.Visible = false;
			this.Line173.Width = 2.291666F;
			this.Line173.X1 = 0.01041667F;
			this.Line173.X2 = 2.302083F;
			this.Line173.Y1 = 7.4F;
			this.Line173.Y2 = 7.4F;
			// 
			// Line174
			// 
			this.Line174.Height = 0F;
			this.Line174.Left = 0.01041667F;
			this.Line174.LineWeight = 1F;
			this.Line174.Name = "Line174";
			this.Line174.Top = 7.65F;
			this.Line174.Visible = false;
			this.Line174.Width = 2.291666F;
			this.Line174.X1 = 0.01041667F;
			this.Line174.X2 = 2.302083F;
			this.Line174.Y1 = 7.65F;
			this.Line174.Y2 = 7.65F;
			// 
			// Line175
			// 
			this.Line175.Height = 0F;
			this.Line175.Left = 0.01041667F;
			this.Line175.LineWeight = 1F;
			this.Line175.Name = "Line175";
			this.Line175.Top = 7.9F;
			this.Line175.Visible = false;
			this.Line175.Width = 2.291666F;
			this.Line175.X1 = 0.01041667F;
			this.Line175.X2 = 2.302083F;
			this.Line175.Y1 = 7.9F;
			this.Line175.Y2 = 7.9F;
			// 
			// Line99
			// 
			this.Line99.Height = 5.5F;
			this.Line99.Left = 1.125F;
			this.Line99.LineWeight = 1F;
			this.Line99.Name = "Line99";
			this.Line99.Top = 2.666667F;
			this.Line99.Visible = false;
			this.Line99.Width = 0F;
			this.Line99.X1 = 1.125F;
			this.Line99.X2 = 1.125F;
			this.Line99.Y1 = 2.666667F;
			this.Line99.Y2 = 8.166667F;
			// 
			// txtDatePaid34
			// 
			this.txtDatePaid34.CanGrow = false;
			this.txtDatePaid34.Height = 0.1666667F;
			this.txtDatePaid34.Left = 2.708333F;
			this.txtDatePaid34.Name = "txtDatePaid34";
			this.txtDatePaid34.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid34.Text = "99 99 99";
			this.txtDatePaid34.Top = 5.6875F;
			this.txtDatePaid34.Width = 0.75F;
			// 
			// txtAmount34
			// 
			this.txtAmount34.CanGrow = false;
			this.txtAmount34.Height = 0.1666667F;
			this.txtAmount34.Left = 3.958333F;
			this.txtAmount34.Name = "txtAmount34";
			this.txtAmount34.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount34.Text = "999999.99";
			this.txtAmount34.Top = 5.6875F;
			this.txtAmount34.Width = 0.8333333F;
			// 
			// txtDatePaid35
			// 
			this.txtDatePaid35.CanGrow = false;
			this.txtDatePaid35.Height = 0.1666667F;
			this.txtDatePaid35.Left = 2.708333F;
			this.txtDatePaid35.Name = "txtDatePaid35";
			this.txtDatePaid35.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid35.Text = "99 99 99";
			this.txtDatePaid35.Top = 5.9375F;
			this.txtDatePaid35.Width = 0.75F;
			// 
			// txtAmount35
			// 
			this.txtAmount35.CanGrow = false;
			this.txtAmount35.Height = 0.1666667F;
			this.txtAmount35.Left = 3.958333F;
			this.txtAmount35.Name = "txtAmount35";
			this.txtAmount35.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount35.Text = "999999.99";
			this.txtAmount35.Top = 5.9375F;
			this.txtAmount35.Width = 0.8333333F;
			// 
			// txtDatePaid36
			// 
			this.txtDatePaid36.CanGrow = false;
			this.txtDatePaid36.Height = 0.1666667F;
			this.txtDatePaid36.Left = 2.708333F;
			this.txtDatePaid36.Name = "txtDatePaid36";
			this.txtDatePaid36.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid36.Text = "99 99 99";
			this.txtDatePaid36.Top = 6.1875F;
			this.txtDatePaid36.Width = 0.75F;
			// 
			// txtAmount36
			// 
			this.txtAmount36.CanGrow = false;
			this.txtAmount36.Height = 0.1666667F;
			this.txtAmount36.Left = 3.958333F;
			this.txtAmount36.Name = "txtAmount36";
			this.txtAmount36.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount36.Text = "999999.99";
			this.txtAmount36.Top = 6.1875F;
			this.txtAmount36.Width = 0.8333333F;
			// 
			// txtDatePaid37
			// 
			this.txtDatePaid37.CanGrow = false;
			this.txtDatePaid37.Height = 0.1666667F;
			this.txtDatePaid37.Left = 2.708333F;
			this.txtDatePaid37.Name = "txtDatePaid37";
			this.txtDatePaid37.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid37.Text = "99 99 99";
			this.txtDatePaid37.Top = 6.4375F;
			this.txtDatePaid37.Width = 0.75F;
			// 
			// txtAmount37
			// 
			this.txtAmount37.CanGrow = false;
			this.txtAmount37.Height = 0.1666667F;
			this.txtAmount37.Left = 3.958333F;
			this.txtAmount37.Name = "txtAmount37";
			this.txtAmount37.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount37.Text = "999999.99";
			this.txtAmount37.Top = 6.4375F;
			this.txtAmount37.Width = 0.8333333F;
			// 
			// txtDatePaid38
			// 
			this.txtDatePaid38.CanGrow = false;
			this.txtDatePaid38.Height = 0.1666667F;
			this.txtDatePaid38.Left = 2.708333F;
			this.txtDatePaid38.Name = "txtDatePaid38";
			this.txtDatePaid38.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid38.Text = "99 99 99";
			this.txtDatePaid38.Top = 6.6875F;
			this.txtDatePaid38.Width = 0.75F;
			// 
			// txtAmount38
			// 
			this.txtAmount38.CanGrow = false;
			this.txtAmount38.Height = 0.1666667F;
			this.txtAmount38.Left = 3.958333F;
			this.txtAmount38.Name = "txtAmount38";
			this.txtAmount38.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount38.Text = "999999.99";
			this.txtAmount38.Top = 6.6875F;
			this.txtAmount38.Width = 0.8333333F;
			// 
			// txtDatePaid39
			// 
			this.txtDatePaid39.CanGrow = false;
			this.txtDatePaid39.Height = 0.1666667F;
			this.txtDatePaid39.Left = 2.708333F;
			this.txtDatePaid39.Name = "txtDatePaid39";
			this.txtDatePaid39.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid39.Text = "99 99 99";
			this.txtDatePaid39.Top = 6.9375F;
			this.txtDatePaid39.Width = 0.75F;
			// 
			// txtAmount39
			// 
			this.txtAmount39.CanGrow = false;
			this.txtAmount39.Height = 0.1666667F;
			this.txtAmount39.Left = 3.958333F;
			this.txtAmount39.Name = "txtAmount39";
			this.txtAmount39.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount39.Text = "999999.99";
			this.txtAmount39.Top = 6.9375F;
			this.txtAmount39.Width = 0.8333333F;
			// 
			// txtDatePaid40
			// 
			this.txtDatePaid40.CanGrow = false;
			this.txtDatePaid40.Height = 0.1666667F;
			this.txtDatePaid40.Left = 2.708333F;
			this.txtDatePaid40.Name = "txtDatePaid40";
			this.txtDatePaid40.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid40.Text = "99 99 99";
			this.txtDatePaid40.Top = 7.1875F;
			this.txtDatePaid40.Width = 0.75F;
			// 
			// txtAmount40
			// 
			this.txtAmount40.CanGrow = false;
			this.txtAmount40.Height = 0.1666667F;
			this.txtAmount40.Left = 3.958333F;
			this.txtAmount40.Name = "txtAmount40";
			this.txtAmount40.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount40.Text = "999999.99";
			this.txtAmount40.Top = 7.1875F;
			this.txtAmount40.Width = 0.8333333F;
			// 
			// txtDatePaid41
			// 
			this.txtDatePaid41.CanGrow = false;
			this.txtDatePaid41.Height = 0.1666667F;
			this.txtDatePaid41.Left = 2.708333F;
			this.txtDatePaid41.Name = "txtDatePaid41";
			this.txtDatePaid41.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid41.Text = "99 99 99";
			this.txtDatePaid41.Top = 7.4375F;
			this.txtDatePaid41.Width = 0.75F;
			// 
			// txtAmount41
			// 
			this.txtAmount41.CanGrow = false;
			this.txtAmount41.Height = 0.1666667F;
			this.txtAmount41.Left = 3.958333F;
			this.txtAmount41.Name = "txtAmount41";
			this.txtAmount41.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount41.Text = "999999.99";
			this.txtAmount41.Top = 7.4375F;
			this.txtAmount41.Width = 0.8333333F;
			// 
			// txtDatePaid42
			// 
			this.txtDatePaid42.CanGrow = false;
			this.txtDatePaid42.Height = 0.1666667F;
			this.txtDatePaid42.Left = 2.708333F;
			this.txtDatePaid42.Name = "txtDatePaid42";
			this.txtDatePaid42.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid42.Text = "99 99 99";
			this.txtDatePaid42.Top = 7.6875F;
			this.txtDatePaid42.Width = 0.75F;
			// 
			// txtAmount42
			// 
			this.txtAmount42.CanGrow = false;
			this.txtAmount42.Height = 0.1666667F;
			this.txtAmount42.Left = 3.958333F;
			this.txtAmount42.Name = "txtAmount42";
			this.txtAmount42.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount42.Text = "999999.99";
			this.txtAmount42.Top = 7.6875F;
			this.txtAmount42.Width = 0.8333333F;
			// 
			// txtAmount55
			// 
			this.txtAmount55.CanGrow = false;
			this.txtAmount55.Height = 0.1666667F;
			this.txtAmount55.Left = 6.552083F;
			this.txtAmount55.Name = "txtAmount55";
			this.txtAmount55.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount55.Text = "999999.99";
			this.txtAmount55.Top = 5.6875F;
			this.txtAmount55.Width = 0.8333333F;
			// 
			// txtDatePaid55
			// 
			this.txtDatePaid55.CanGrow = false;
			this.txtDatePaid55.Height = 0.1666667F;
			this.txtDatePaid55.Left = 5.302083F;
			this.txtDatePaid55.Name = "txtDatePaid55";
			this.txtDatePaid55.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid55.Text = "99 99 99";
			this.txtDatePaid55.Top = 5.6875F;
			this.txtDatePaid55.Width = 0.75F;
			// 
			// txtAmount56
			// 
			this.txtAmount56.CanGrow = false;
			this.txtAmount56.Height = 0.1666667F;
			this.txtAmount56.Left = 6.552083F;
			this.txtAmount56.Name = "txtAmount56";
			this.txtAmount56.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount56.Text = "999999.99";
			this.txtAmount56.Top = 5.9375F;
			this.txtAmount56.Width = 0.8333333F;
			// 
			// txtDatePaid56
			// 
			this.txtDatePaid56.CanGrow = false;
			this.txtDatePaid56.Height = 0.1666667F;
			this.txtDatePaid56.Left = 5.302083F;
			this.txtDatePaid56.Name = "txtDatePaid56";
			this.txtDatePaid56.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid56.Text = "99 99 99";
			this.txtDatePaid56.Top = 5.9375F;
			this.txtDatePaid56.Width = 0.75F;
			// 
			// txtAmount57
			// 
			this.txtAmount57.CanGrow = false;
			this.txtAmount57.Height = 0.1666667F;
			this.txtAmount57.Left = 6.552083F;
			this.txtAmount57.Name = "txtAmount57";
			this.txtAmount57.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount57.Text = "999999.99";
			this.txtAmount57.Top = 6.1875F;
			this.txtAmount57.Width = 0.8333333F;
			// 
			// txtDatePaid57
			// 
			this.txtDatePaid57.CanGrow = false;
			this.txtDatePaid57.Height = 0.1666667F;
			this.txtDatePaid57.Left = 5.302083F;
			this.txtDatePaid57.Name = "txtDatePaid57";
			this.txtDatePaid57.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid57.Text = "99 99 99";
			this.txtDatePaid57.Top = 6.1875F;
			this.txtDatePaid57.Width = 0.75F;
			// 
			// txtAmount58
			// 
			this.txtAmount58.CanGrow = false;
			this.txtAmount58.Height = 0.1666667F;
			this.txtAmount58.Left = 6.552083F;
			this.txtAmount58.Name = "txtAmount58";
			this.txtAmount58.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount58.Text = "999999.99";
			this.txtAmount58.Top = 6.4375F;
			this.txtAmount58.Width = 0.8333333F;
			// 
			// txtDatePaid58
			// 
			this.txtDatePaid58.CanGrow = false;
			this.txtDatePaid58.Height = 0.1666667F;
			this.txtDatePaid58.Left = 5.302083F;
			this.txtDatePaid58.Name = "txtDatePaid58";
			this.txtDatePaid58.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid58.Text = "99 99 99";
			this.txtDatePaid58.Top = 6.4375F;
			this.txtDatePaid58.Width = 0.75F;
			// 
			// txtAmount59
			// 
			this.txtAmount59.CanGrow = false;
			this.txtAmount59.Height = 0.1666667F;
			this.txtAmount59.Left = 6.552083F;
			this.txtAmount59.Name = "txtAmount59";
			this.txtAmount59.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount59.Text = "999999.99";
			this.txtAmount59.Top = 6.6875F;
			this.txtAmount59.Width = 0.8333333F;
			// 
			// txtDatePaid59
			// 
			this.txtDatePaid59.CanGrow = false;
			this.txtDatePaid59.Height = 0.1666667F;
			this.txtDatePaid59.Left = 5.302083F;
			this.txtDatePaid59.Name = "txtDatePaid59";
			this.txtDatePaid59.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid59.Text = "99 99 99";
			this.txtDatePaid59.Top = 6.6875F;
			this.txtDatePaid59.Width = 0.75F;
			// 
			// txtAmount60
			// 
			this.txtAmount60.CanGrow = false;
			this.txtAmount60.Height = 0.1666667F;
			this.txtAmount60.Left = 6.552083F;
			this.txtAmount60.Name = "txtAmount60";
			this.txtAmount60.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount60.Text = "999999.99";
			this.txtAmount60.Top = 6.9375F;
			this.txtAmount60.Width = 0.8333333F;
			// 
			// txtDatePaid60
			// 
			this.txtDatePaid60.CanGrow = false;
			this.txtDatePaid60.Height = 0.1666667F;
			this.txtDatePaid60.Left = 5.302083F;
			this.txtDatePaid60.Name = "txtDatePaid60";
			this.txtDatePaid60.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid60.Text = "99 99 99";
			this.txtDatePaid60.Top = 6.9375F;
			this.txtDatePaid60.Width = 0.75F;
			// 
			// txtAmount61
			// 
			this.txtAmount61.CanGrow = false;
			this.txtAmount61.Height = 0.1666667F;
			this.txtAmount61.Left = 6.552083F;
			this.txtAmount61.Name = "txtAmount61";
			this.txtAmount61.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount61.Text = "999999.99";
			this.txtAmount61.Top = 7.1875F;
			this.txtAmount61.Width = 0.8333333F;
			// 
			// txtDatePaid61
			// 
			this.txtDatePaid61.CanGrow = false;
			this.txtDatePaid61.Height = 0.1666667F;
			this.txtDatePaid61.Left = 5.302083F;
			this.txtDatePaid61.Name = "txtDatePaid61";
			this.txtDatePaid61.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid61.Text = "99 99 99";
			this.txtDatePaid61.Top = 7.1875F;
			this.txtDatePaid61.Width = 0.75F;
			// 
			// txtAmount62
			// 
			this.txtAmount62.CanGrow = false;
			this.txtAmount62.Height = 0.1666667F;
			this.txtAmount62.Left = 6.552083F;
			this.txtAmount62.Name = "txtAmount62";
			this.txtAmount62.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount62.Text = "999999.99";
			this.txtAmount62.Top = 7.4375F;
			this.txtAmount62.Width = 0.8333333F;
			// 
			// txtDatePaid62
			// 
			this.txtDatePaid62.CanGrow = false;
			this.txtDatePaid62.Height = 0.1666667F;
			this.txtDatePaid62.Left = 5.302083F;
			this.txtDatePaid62.Name = "txtDatePaid62";
			this.txtDatePaid62.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid62.Text = "99 99 99";
			this.txtDatePaid62.Top = 7.4375F;
			this.txtDatePaid62.Width = 0.75F;
			// 
			// txtAmount63
			// 
			this.txtAmount63.CanGrow = false;
			this.txtAmount63.Height = 0.1666667F;
			this.txtAmount63.Left = 6.552083F;
			this.txtAmount63.Name = "txtAmount63";
			this.txtAmount63.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtAmount63.Text = "999999.99";
			this.txtAmount63.Top = 7.6875F;
			this.txtAmount63.Width = 0.8333333F;
			// 
			// txtDatePaid63
			// 
			this.txtDatePaid63.CanGrow = false;
			this.txtDatePaid63.Height = 0.1666667F;
			this.txtDatePaid63.Left = 5.302083F;
			this.txtDatePaid63.Name = "txtDatePaid63";
			this.txtDatePaid63.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center";
			this.txtDatePaid63.Text = "99 99 99";
			this.txtDatePaid63.Top = 7.6875F;
			this.txtDatePaid63.Width = 0.75F;
			// 
			// Line176
			// 
			this.Line176.Height = 0F;
			this.Line176.Left = 2.59375F;
			this.Line176.LineWeight = 1F;
			this.Line176.Name = "Line176";
			this.Line176.Top = 5.895833F;
			this.Line176.Visible = false;
			this.Line176.Width = 2.291667F;
			this.Line176.X1 = 2.59375F;
			this.Line176.X2 = 4.885417F;
			this.Line176.Y1 = 5.895833F;
			this.Line176.Y2 = 5.895833F;
			// 
			// Line177
			// 
			this.Line177.Height = 0F;
			this.Line177.Left = 2.59375F;
			this.Line177.LineWeight = 1F;
			this.Line177.Name = "Line177";
			this.Line177.Top = 6.145833F;
			this.Line177.Visible = false;
			this.Line177.Width = 2.291667F;
			this.Line177.X1 = 2.59375F;
			this.Line177.X2 = 4.885417F;
			this.Line177.Y1 = 6.145833F;
			this.Line177.Y2 = 6.145833F;
			// 
			// Line178
			// 
			this.Line178.Height = 0F;
			this.Line178.Left = 2.59375F;
			this.Line178.LineWeight = 1F;
			this.Line178.Name = "Line178";
			this.Line178.Top = 6.395833F;
			this.Line178.Visible = false;
			this.Line178.Width = 2.291667F;
			this.Line178.X1 = 2.59375F;
			this.Line178.X2 = 4.885417F;
			this.Line178.Y1 = 6.395833F;
			this.Line178.Y2 = 6.395833F;
			// 
			// Line179
			// 
			this.Line179.Height = 0F;
			this.Line179.Left = 2.59375F;
			this.Line179.LineWeight = 1F;
			this.Line179.Name = "Line179";
			this.Line179.Top = 6.645833F;
			this.Line179.Visible = false;
			this.Line179.Width = 2.291667F;
			this.Line179.X1 = 2.59375F;
			this.Line179.X2 = 4.885417F;
			this.Line179.Y1 = 6.645833F;
			this.Line179.Y2 = 6.645833F;
			// 
			// Line180
			// 
			this.Line180.Height = 0F;
			this.Line180.Left = 2.59375F;
			this.Line180.LineWeight = 1F;
			this.Line180.Name = "Line180";
			this.Line180.Top = 6.895833F;
			this.Line180.Visible = false;
			this.Line180.Width = 2.291667F;
			this.Line180.X1 = 2.59375F;
			this.Line180.X2 = 4.885417F;
			this.Line180.Y1 = 6.895833F;
			this.Line180.Y2 = 6.895833F;
			// 
			// Line181
			// 
			this.Line181.Height = 0F;
			this.Line181.Left = 2.59375F;
			this.Line181.LineWeight = 1F;
			this.Line181.Name = "Line181";
			this.Line181.Top = 7.145833F;
			this.Line181.Visible = false;
			this.Line181.Width = 2.291667F;
			this.Line181.X1 = 2.59375F;
			this.Line181.X2 = 4.885417F;
			this.Line181.Y1 = 7.145833F;
			this.Line181.Y2 = 7.145833F;
			// 
			// Line182
			// 
			this.Line182.Height = 0F;
			this.Line182.Left = 2.59375F;
			this.Line182.LineWeight = 1F;
			this.Line182.Name = "Line182";
			this.Line182.Top = 7.395833F;
			this.Line182.Visible = false;
			this.Line182.Width = 2.291667F;
			this.Line182.X1 = 2.59375F;
			this.Line182.X2 = 4.885417F;
			this.Line182.Y1 = 7.395833F;
			this.Line182.Y2 = 7.395833F;
			// 
			// Line183
			// 
			this.Line183.Height = 0F;
			this.Line183.Left = 2.59375F;
			this.Line183.LineWeight = 1F;
			this.Line183.Name = "Line183";
			this.Line183.Top = 7.645833F;
			this.Line183.Visible = false;
			this.Line183.Width = 2.291667F;
			this.Line183.X1 = 2.59375F;
			this.Line183.X2 = 4.885417F;
			this.Line183.Y1 = 7.645833F;
			this.Line183.Y2 = 7.645833F;
			// 
			// Line184
			// 
			this.Line184.Height = 0F;
			this.Line184.Left = 2.59375F;
			this.Line184.LineWeight = 1F;
			this.Line184.Name = "Line184";
			this.Line184.Top = 7.895833F;
			this.Line184.Visible = false;
			this.Line184.Width = 2.291667F;
			this.Line184.X1 = 2.59375F;
			this.Line184.X2 = 4.885417F;
			this.Line184.Y1 = 7.895833F;
			this.Line184.Y2 = 7.895833F;
			// 
			// Line185
			// 
			this.Line185.Height = 0F;
			this.Line185.Left = 5.1875F;
			this.Line185.LineWeight = 1F;
			this.Line185.Name = "Line185";
			this.Line185.Top = 5.895833F;
			this.Line185.Visible = false;
			this.Line185.Width = 2.291667F;
			this.Line185.X1 = 5.1875F;
			this.Line185.X2 = 7.479167F;
			this.Line185.Y1 = 5.895833F;
			this.Line185.Y2 = 5.895833F;
			// 
			// Line186
			// 
			this.Line186.Height = 0F;
			this.Line186.Left = 5.1875F;
			this.Line186.LineWeight = 1F;
			this.Line186.Name = "Line186";
			this.Line186.Top = 6.145833F;
			this.Line186.Visible = false;
			this.Line186.Width = 2.291667F;
			this.Line186.X1 = 5.1875F;
			this.Line186.X2 = 7.479167F;
			this.Line186.Y1 = 6.145833F;
			this.Line186.Y2 = 6.145833F;
			// 
			// Line187
			// 
			this.Line187.Height = 0F;
			this.Line187.Left = 5.1875F;
			this.Line187.LineWeight = 1F;
			this.Line187.Name = "Line187";
			this.Line187.Top = 6.395833F;
			this.Line187.Visible = false;
			this.Line187.Width = 2.291667F;
			this.Line187.X1 = 5.1875F;
			this.Line187.X2 = 7.479167F;
			this.Line187.Y1 = 6.395833F;
			this.Line187.Y2 = 6.395833F;
			// 
			// Line188
			// 
			this.Line188.Height = 0F;
			this.Line188.Left = 5.1875F;
			this.Line188.LineWeight = 1F;
			this.Line188.Name = "Line188";
			this.Line188.Top = 6.645833F;
			this.Line188.Visible = false;
			this.Line188.Width = 2.291667F;
			this.Line188.X1 = 5.1875F;
			this.Line188.X2 = 7.479167F;
			this.Line188.Y1 = 6.645833F;
			this.Line188.Y2 = 6.645833F;
			// 
			// Line189
			// 
			this.Line189.Height = 0F;
			this.Line189.Left = 5.1875F;
			this.Line189.LineWeight = 1F;
			this.Line189.Name = "Line189";
			this.Line189.Top = 6.895833F;
			this.Line189.Visible = false;
			this.Line189.Width = 2.291667F;
			this.Line189.X1 = 5.1875F;
			this.Line189.X2 = 7.479167F;
			this.Line189.Y1 = 6.895833F;
			this.Line189.Y2 = 6.895833F;
			// 
			// Line190
			// 
			this.Line190.Height = 0F;
			this.Line190.Left = 5.1875F;
			this.Line190.LineWeight = 1F;
			this.Line190.Name = "Line190";
			this.Line190.Top = 7.145833F;
			this.Line190.Visible = false;
			this.Line190.Width = 2.291667F;
			this.Line190.X1 = 5.1875F;
			this.Line190.X2 = 7.479167F;
			this.Line190.Y1 = 7.145833F;
			this.Line190.Y2 = 7.145833F;
			// 
			// Line191
			// 
			this.Line191.Height = 0F;
			this.Line191.Left = 5.1875F;
			this.Line191.LineWeight = 1F;
			this.Line191.Name = "Line191";
			this.Line191.Top = 7.395833F;
			this.Line191.Visible = false;
			this.Line191.Width = 2.291667F;
			this.Line191.X1 = 5.1875F;
			this.Line191.X2 = 7.479167F;
			this.Line191.Y1 = 7.395833F;
			this.Line191.Y2 = 7.395833F;
			// 
			// Line192
			// 
			this.Line192.Height = 0F;
			this.Line192.Left = 5.1875F;
			this.Line192.LineWeight = 1F;
			this.Line192.Name = "Line192";
			this.Line192.Top = 7.645833F;
			this.Line192.Visible = false;
			this.Line192.Width = 2.291667F;
			this.Line192.X1 = 5.1875F;
			this.Line192.X2 = 7.479167F;
			this.Line192.Y1 = 7.645833F;
			this.Line192.Y2 = 7.645833F;
			// 
			// Line193
			// 
			this.Line193.Height = 0F;
			this.Line193.Left = 5.1875F;
			this.Line193.LineWeight = 1F;
			this.Line193.Name = "Line193";
			this.Line193.Top = 7.895833F;
			this.Line193.Visible = false;
			this.Line193.Width = 2.291667F;
			this.Line193.X1 = 5.1875F;
			this.Line193.X2 = 7.479167F;
			this.Line193.Y1 = 7.895833F;
			this.Line193.Y2 = 7.895833F;
			// 
			// srptC1Schedule1
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.541667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCEmployerAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtX;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line121;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line122;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount54;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line100;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line101;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line102;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line103;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line104;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line106;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line108;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line109;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line111;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line112;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line113;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line114;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line123;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line124;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line125;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line126;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line127;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line128;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line130;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line131;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line133;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line134;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line136;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line137;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line143;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line145;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line146;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line147;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line148;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line150;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line151;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line152;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line154;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line155;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line157;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line158;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line160;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line166;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line167;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line168;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line169;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line170;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line171;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line172;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line173;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line174;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line175;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line99;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount58;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid58;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid63;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line176;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line177;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line178;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line179;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line180;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line181;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line182;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line183;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line184;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line185;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line186;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line187;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line188;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line189;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line190;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line191;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line192;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line193;
	}
}
