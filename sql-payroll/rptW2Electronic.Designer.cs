﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Electronic.
	/// </summary>
	partial class rptW2Electronic
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2Electronic));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblMuni2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompany = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossSocSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossMed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossFed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSocTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBox12L = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupGrossSocSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupGrossMed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupGrossFed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupSocTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupBox12L = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGroupCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotGrossSocSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotGrossMed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotGrossFed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSocTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBox12L = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTot401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTot403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTot408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTot457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTot501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFinalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuni2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossSocSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSocTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox12L)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt401)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt403)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt408)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt457)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt501)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossSocSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossFed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupSocTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupMedTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupFedTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupBox12L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup401)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup403)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup408)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup457)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup501)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossSocSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSocTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBox12L)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot401)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot403)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot408)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot457)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot501)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label6,
            this.txtSSN,
            this.txtName,
            this.txtEmployeeAddress1,
            this.txtEmployeeAddress2,
            this.txtEmployeeCityStateZip,
            this.txtGrossSocSec,
            this.txtGrossMed,
            this.txtGrossFed,
            this.txtSocTax,
            this.txtMedTax,
            this.txtFedTax,
            this.txtBox12L,
            this.txtPen,
            this.txt401,
            this.txt403,
            this.txt408,
            this.txt457,
            this.txt501,
            this.Label34,
            this.Label35,
            this.txtStateWages,
            this.Label36,
				this.txtStateTax
			});
			this.Detail.Height = 1.666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMuni2,
				this.Label8,
				this.lblDate2,
				this.Label10,
				this.lblTime2
			});
			this.ReportHeader.Height = 0.46875F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Visible = false;
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotGrossSocSec,
				this.txtTotGrossMed,
				this.txtTotGrossFed,
				this.txtTotSocTax,
				this.txtTotMedTax,
				this.txtTotFedTax,
				this.txtTotBox12L,
				this.txtTot401,
				this.txtTot403,
				this.txtTot408,
				this.txtTot457,
				this.txtTot501,
				this.Label21,
				this.Label22,
				this.Label23,
				this.txtFinalCount
			});
			this.ReportFooter.Height = 1.25F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMuni,
				this.Label1,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20
			});
			this.PageHeader.Height = 0.8854167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.Label3,
				this.txtYear,
				this.Label4,
				this.Label5,
				this.txtID,
				this.txtCompany,
				this.txtAddress1,
				this.txtAddress2,
				this.txtCityStateZip,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33
			});
			this.GroupHeader1.Height = 0.8020833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupGrossSocSec,
				this.txtGroupGrossMed,
				this.txtGroupGrossFed,
				this.txtGroupSocTax,
				this.txtGroupMedTax,
				this.txtGroupFedTax,
				this.txtGroupBox12L,
				this.txtGroup401,
				this.txtGroup403,
				this.txtGroup408,
				this.txtGroup457,
				this.txtGroup501,
				this.Label37,
				this.Label38,
				this.Label39,
				this.txtGroupCount
			});
			this.GroupFooter1.Height = 1.21875F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
            // lblMuni2
            // 
            this.lblMuni2.Height = 0.19F;
            this.lblMuni2.HyperLink = null;
            this.lblMuni2.Left = 0F;
            this.lblMuni2.Name = "lblMuni2";
            this.lblMuni2.Style = "";
            this.lblMuni2.Text = "Label1";
            this.lblMuni2.Top = 0F;
            this.lblMuni2.Width = 1.5625F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.21875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 1.625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.Label8.Text = "Payroll - W2 Disk Audit";
            this.Label8.Top = 0F;
            this.Label8.Width = 4.25F;
            // 
            // lblDate2
            // 
            this.lblDate2.Height = 0.19F;
            this.lblDate2.HyperLink = null;
            this.lblDate2.Left = 6.25F;
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Style = "text-align: right";
            this.lblDate2.Text = "Label2";
            this.lblDate2.Top = 0F;
            this.lblDate2.Width = 1.1875F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 6.25F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "text-align: right";
            this.Label10.Text = "Page 1";
            this.Label10.Top = 0.15625F;
            this.Label10.Width = 1.1875F;
            // 
            // lblTime2
            // 
            this.lblTime2.Height = 0.19F;
            this.lblTime2.HyperLink = null;
            this.lblTime2.Left = 0F;
            this.lblTime2.Name = "lblTime2";
            this.lblTime2.Style = "";
            this.lblTime2.Text = "Label1";
            this.lblTime2.Top = 0.15625F;
            this.lblTime2.Width = 1.5625F;
            // 
            // lblMuni
            // 
            this.lblMuni.Height = 0.219F;
            this.lblMuni.HyperLink = null;
            this.lblMuni.Left = 0F;
            this.lblMuni.Name = "lblMuni";
            this.lblMuni.Style = "";
            this.lblMuni.Text = "Label1";
            this.lblMuni.Top = 0F;
            this.lblMuni.Width = 1.5625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Payroll - W2 Disk Audit";
            this.Label1.Top = 0F;
            this.Label1.Width = 4.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.219F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.25F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "text-align: right";
            this.lblDate.Text = "Label2";
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.1875F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.31275F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.25F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "text-align: right";
            this.lblPage.Text = "Label2";
            this.lblPage.Top = 0.15625F;
            this.lblPage.Width = 1.1875F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.31275F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "";
            this.lblTime.Text = "Label1";
            this.lblTime.Top = 0.15625F;
            this.lblTime.Width = 1.5625F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1975001F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0.3125F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold; text-align: right";
            this.Label11.Text = "Social Sec";
            this.Label11.Top = 0.6875F;
            this.Label11.Width = 0.9375F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1975001F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 1.3125F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold; text-align: right";
            this.Label12.Text = "Medicare";
            this.Label12.Top = 0.6875F;
            this.Label12.Width = 0.9375F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1975001F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 2.3125F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-weight: bold; text-align: right";
            this.Label13.Text = "Federal";
            this.Label13.Top = 0.6875F;
            this.Label13.Width = 0.9375F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1975001F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 3.375F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; text-align: right";
            this.Label14.Text = "Social Sec";
            this.Label14.Top = 0.6875F;
            this.Label14.Width = 0.9375F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1975001F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 4.375F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-weight: bold; text-align: right";
            this.Label15.Text = "Medicare";
            this.Label15.Top = 0.6875F;
            this.Label15.Width = 0.9375F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1975001F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 5.375F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold; text-align: right";
            this.Label16.Text = "Federal";
            this.Label16.Top = 0.6875F;
            this.Label16.Width = 0.9375F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1975001F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 6.4375F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-weight: bold; text-align: right";
            this.Label17.Text = "Box 12L";
            this.Label17.Top = 0.6875F;
            this.Label17.Width = 0.6875F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1975001F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 7.1875F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-weight: bold; text-align: center";
            this.Label18.Text = "Pen";
            this.Label18.Top = 0.6875F;
            this.Label18.Width = 0.3125F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.21825F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0.25F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-weight: bold; text-align: center";
            this.Label19.Text = "------------- G R O S S   P A Y S -------------";
            this.Label19.Top = 0.46875F;
            this.Label19.Width = 3.0625F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.21825F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 3.375F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-weight: bold; text-align: center";
            this.Label20.Text = "-------- T A X E S   W I T H H E L D -------";
            this.Label20.Top = 0.46875F;
            this.Label20.Width = 3.0625F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.19F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "";
            this.Label2.Text = "RE";
            this.Label2.Top = 0F;
            this.Label2.Width = 0.25F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.3125F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold";
            this.Label3.Text = "Year:";
            this.Label3.Top = 0F;
            this.Label3.Width = 0.375F;
            // 
            // txtYear
            // 
            this.txtYear.Height = 0.1875F;
            this.txtYear.Left = 0.75F;
            this.txtYear.Name = "txtYear";
            this.txtYear.Text = null;
            this.txtYear.Top = 0F;
            this.txtYear.Width = 0.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 1.375F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "ID:";
            this.Label4.Top = 0F;
            this.Label4.Width = 0.3125F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 2.8125F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "Employer:";
            this.Label5.Top = 0F;
            this.Label5.Width = 0.75F;
            // 
            // txtID
            // 
            this.txtID.Height = 0.1875F;
            this.txtID.Left = 1.75F;
            this.txtID.Name = "txtID";
            this.txtID.Text = null;
            this.txtID.Top = 0F;
            this.txtID.Width = 1F;
            // 
            // txtCompany
            // 
            this.txtCompany.Height = 0.1875F;
            this.txtCompany.Left = 3.625F;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Text = "Field2";
            this.txtCompany.Top = 0F;
            this.txtCompany.Width = 3.75F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Height = 0.1875F;
            this.txtAddress1.Left = 3.625F;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Text = "Field3";
            this.txtAddress1.Top = 0.1875F;
            this.txtAddress1.Width = 3.75F;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Height = 0.1875F;
            this.txtAddress2.Left = 3.625F;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Text = "Field4";
            this.txtAddress2.Top = 0.375F;
            this.txtAddress2.Width = 3.75F;
            // 
            // txtCityStateZip
            // 
            this.txtCityStateZip.Height = 0.1875F;
            this.txtCityStateZip.Left = 3.625F;
            this.txtCityStateZip.Name = "txtCityStateZip";
            this.txtCityStateZip.Text = "Field5";
            this.txtCityStateZip.Top = 0.5625F;
            this.txtCityStateZip.Width = 3.75F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.19F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0.3125F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-weight: bold; text-align: right";
            this.Label24.Text = "Social Sec";
            this.Label24.Top = 1.0625F;
            this.Label24.Width = 0.9375F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.19F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 1.3125F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-weight: bold; text-align: right";
            this.Label25.Text = "Medicare";
            this.Label25.Top = 1.0625F;
            this.Label25.Width = 0.9375F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.19F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 2.3125F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-weight: bold; text-align: right";
            this.Label26.Text = "Federal";
            this.Label26.Top = 1.0625F;
            this.Label26.Width = 0.9375F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.19F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 3.375F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-weight: bold; text-align: right";
            this.Label27.Text = "Social Sec";
            this.Label27.Top = 1.0625F;
            this.Label27.Width = 0.9375F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.19F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 4.375F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-weight: bold; text-align: right";
            this.Label28.Text = "Medicare";
            this.Label28.Top = 1.0625F;
            this.Label28.Width = 0.9375F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.19F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 5.375F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-weight: bold; text-align: right";
            this.Label29.Text = "Federal";
            this.Label29.Top = 1.0625F;
            this.Label29.Width = 0.9375F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.19F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 6.4375F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-weight: bold; text-align: right";
            this.Label30.Text = "Box 12L";
            this.Label30.Top = 1.0625F;
            this.Label30.Width = 0.6875F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.19F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 7.1875F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-weight: bold; text-align: center";
            this.Label31.Text = "Pen";
            this.Label31.Top = 1.0625F;
            this.Label31.Width = 0.3125F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.19F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 0.25F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-weight: bold; text-align: center";
            this.Label32.Text = "------------- G R O S S   P A Y S -------------";
            this.Label32.Top = 0.84375F;
            this.Label32.Width = 3.0625F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.19F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 3.375F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-weight: bold; text-align: center";
            this.Label33.Text = "-------- T A X E S   W I T H H E L D -------";
            this.Label33.Top = 0.84375F;
            this.Label33.Width = 3.0625F;
            // 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "";
			this.Label6.Text = "RW";
			this.Label6.Top = 0F;
			this.Label6.Width = 0.25F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1875F;
			this.txtSSN.Left = 0.3125F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0F;
			this.txtSSN.Width = 0.8125F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.1875F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field2";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.75F;
			// 
			// txtEmployeeAddress1
			// 
			this.txtEmployeeAddress1.Height = 0.1875F;
			this.txtEmployeeAddress1.Left = 1.1875F;
			this.txtEmployeeAddress1.Name = "txtEmployeeAddress1";
			this.txtEmployeeAddress1.Text = "Field2";
			this.txtEmployeeAddress1.Top = 0.1875F;
			this.txtEmployeeAddress1.Width = 3.75F;
			// 
			// txtEmployeeAddress2
			// 
			this.txtEmployeeAddress2.Height = 0.1875F;
			this.txtEmployeeAddress2.Left = 1.1875F;
			this.txtEmployeeAddress2.Name = "txtEmployeeAddress2";
			this.txtEmployeeAddress2.Text = "Field2";
			this.txtEmployeeAddress2.Top = 0.375F;
			this.txtEmployeeAddress2.Width = 3.75F;
			// 
			// txtEmployeeCityStateZip
			// 
			this.txtEmployeeCityStateZip.Height = 0.1875F;
			this.txtEmployeeCityStateZip.Left = 1.1875F;
			this.txtEmployeeCityStateZip.Name = "txtEmployeeCityStateZip";
			this.txtEmployeeCityStateZip.Text = "Field2";
			this.txtEmployeeCityStateZip.Top = 0.5625F;
			this.txtEmployeeCityStateZip.Width = 3.75F;
			// 
			// txtGrossSocSec
			// 
			this.txtGrossSocSec.Height = 0.1875F;
			this.txtGrossSocSec.Left = 0.3125F;
			this.txtGrossSocSec.Name = "txtGrossSocSec";
			this.txtGrossSocSec.Style = "text-align: right";
			this.txtGrossSocSec.Text = null;
			this.txtGrossSocSec.Top = 0.84375F;
			this.txtGrossSocSec.Width = 0.9375F;
			// 
			// txtGrossMed
			// 
			this.txtGrossMed.Height = 0.1875F;
			this.txtGrossMed.Left = 1.3125F;
			this.txtGrossMed.Name = "txtGrossMed";
			this.txtGrossMed.Style = "text-align: right";
			this.txtGrossMed.Text = null;
			this.txtGrossMed.Top = 0.84375F;
			this.txtGrossMed.Width = 0.9375F;
			// 
			// txtGrossFed
			// 
			this.txtGrossFed.Height = 0.1875F;
			this.txtGrossFed.Left = 2.3125F;
			this.txtGrossFed.Name = "txtGrossFed";
			this.txtGrossFed.Style = "text-align: right";
			this.txtGrossFed.Text = null;
			this.txtGrossFed.Top = 0.84375F;
			this.txtGrossFed.Width = 0.9375F;
			// 
			// txtSocTax
			// 
			this.txtSocTax.Height = 0.1875F;
			this.txtSocTax.Left = 3.375F;
			this.txtSocTax.Name = "txtSocTax";
			this.txtSocTax.Style = "text-align: right";
			this.txtSocTax.Text = null;
			this.txtSocTax.Top = 0.84375F;
			this.txtSocTax.Width = 0.9375F;
			// 
			// txtMedTax
			// 
			this.txtMedTax.Height = 0.1875F;
			this.txtMedTax.Left = 4.375F;
			this.txtMedTax.Name = "txtMedTax";
			this.txtMedTax.Style = "text-align: right";
			this.txtMedTax.Text = null;
			this.txtMedTax.Top = 0.84375F;
			this.txtMedTax.Width = 0.9375F;
			// 
			// txtFedTax
			// 
			this.txtFedTax.Height = 0.1875F;
			this.txtFedTax.Left = 5.375F;
			this.txtFedTax.Name = "txtFedTax";
			this.txtFedTax.Style = "text-align: right";
			this.txtFedTax.Text = null;
			this.txtFedTax.Top = 0.84375F;
			this.txtFedTax.Width = 0.9375F;
			// 
			// txtBox12L
			// 
			this.txtBox12L.Height = 0.1875F;
			this.txtBox12L.Left = 6.375F;
			this.txtBox12L.Name = "txtBox12L";
			this.txtBox12L.Style = "text-align: right";
			this.txtBox12L.Text = null;
			this.txtBox12L.Top = 0.84375F;
			this.txtBox12L.Width = 0.75F;
			// 
			// txtPen
			// 
			this.txtPen.Height = 0.1875F;
			this.txtPen.Left = 7.1875F;
			this.txtPen.Name = "txtPen";
			this.txtPen.Style = "text-align: center";
			this.txtPen.Text = null;
			this.txtPen.Top = 0.84375F;
			this.txtPen.Width = 0.25F;
			// 
			// txt401
			// 
			this.txt401.Height = 0.1666667F;
			this.txt401.Left = 0.4166667F;
			this.txt401.Name = "txt401";
			this.txt401.Style = "text-align: right";
			this.txt401.Text = null;
			this.txt401.Top = 1.083333F;
			this.txt401.Width = 1.083333F;
			// 
			// txt403
			// 
			this.txt403.Height = 0.1666667F;
			this.txt403.Left = 1.666667F;
			this.txt403.Name = "txt403";
			this.txt403.Style = "text-align: right";
			this.txt403.Text = null;
			this.txt403.Top = 1.083333F;
			this.txt403.Width = 1F;
			// 
			// txt408
			// 
			this.txt408.Height = 0.1666667F;
			this.txt408.Left = 2.833333F;
			this.txt408.Name = "txt408";
			this.txt408.Style = "text-align: right";
			this.txt408.Text = null;
			this.txt408.Top = 1.083333F;
			this.txt408.Width = 1.083333F;
			// 
			// txt457
			// 
			this.txt457.Height = 0.1666667F;
			this.txt457.Left = 4F;
			this.txt457.Name = "txt457";
			this.txt457.Style = "text-align: right";
			this.txt457.Text = null;
			this.txt457.Top = 1.083333F;
			this.txt457.Width = 1.083333F;
			// 
			// txt501
			// 
			this.txt501.Height = 0.1666667F;
			this.txt501.Left = 5.166667F;
			this.txt501.Name = "txt501";
			this.txt501.Style = "text-align: right";
			this.txt501.Text = null;
			this.txt501.Top = 1.083333F;
			this.txt501.Width = 1.083333F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1666667F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "";
			this.Label34.Text = "RS";
			this.Label34.Top = 1.333333F;
			this.Label34.Width = 0.25F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.25F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.4166667F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-weight: bold";
			this.Label35.Text = "State Taxable Wages";
			this.Label35.Top = 1.333333F;
			this.Label35.Width = 1.583333F;
			// 
			// txtStateWages
			// 
			this.txtStateWages.Height = 0.1666667F;
			this.txtStateWages.Left = 1.916667F;
			this.txtStateWages.Name = "txtStateWages";
			this.txtStateWages.Style = "text-align: right";
			this.txtStateWages.Text = null;
			this.txtStateWages.Top = 1.333333F;
			this.txtStateWages.Width = 1.166667F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1666667F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.166667F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-weight: bold";
			this.Label36.Text = "State Tax Withheld";
			this.Label36.Top = 1.333333F;
			this.Label36.Width = 1.5F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1666667F;
			this.txtStateTax.Left = 4.666667F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 1.333333F;
			this.txtStateTax.Width = 1.166667F;
			// 
            // txtGroupGrossSocSec
            // 
            this.txtGroupGrossSocSec.Height = 0.1875F;
            this.txtGroupGrossSocSec.Left = 0.3125F;
            this.txtGroupGrossSocSec.Name = "txtGroupGrossSocSec";
            this.txtGroupGrossSocSec.Style = "text-align: right";
            this.txtGroupGrossSocSec.Text = null;
            this.txtGroupGrossSocSec.Top = 0.4375F;
            this.txtGroupGrossSocSec.Width = 0.9375F;
            // 
            // txtGroupGrossMed
            // 
            this.txtGroupGrossMed.Height = 0.1875F;
            this.txtGroupGrossMed.Left = 1.3125F;
            this.txtGroupGrossMed.Name = "txtGroupGrossMed";
            this.txtGroupGrossMed.Style = "text-align: right";
            this.txtGroupGrossMed.Text = null;
            this.txtGroupGrossMed.Top = 0.4375F;
            this.txtGroupGrossMed.Width = 0.9375F;
            // 
            // txtGroupGrossFed
            // 
            this.txtGroupGrossFed.Height = 0.1875F;
            this.txtGroupGrossFed.Left = 2.3125F;
            this.txtGroupGrossFed.Name = "txtGroupGrossFed";
            this.txtGroupGrossFed.Style = "text-align: right";
            this.txtGroupGrossFed.Text = null;
            this.txtGroupGrossFed.Top = 0.4375F;
            this.txtGroupGrossFed.Width = 0.9375F;
            // 
            // txtGroupSocTax
            // 
            this.txtGroupSocTax.Height = 0.1875F;
            this.txtGroupSocTax.Left = 3.375F;
            this.txtGroupSocTax.Name = "txtGroupSocTax";
            this.txtGroupSocTax.Style = "text-align: right";
            this.txtGroupSocTax.Text = null;
            this.txtGroupSocTax.Top = 0.4375F;
            this.txtGroupSocTax.Width = 0.9375F;
            // 
            // txtGroupMedTax
            // 
            this.txtGroupMedTax.Height = 0.1875F;
            this.txtGroupMedTax.Left = 4.375F;
            this.txtGroupMedTax.Name = "txtGroupMedTax";
            this.txtGroupMedTax.Style = "text-align: right";
            this.txtGroupMedTax.Text = null;
            this.txtGroupMedTax.Top = 0.4375F;
            this.txtGroupMedTax.Width = 0.9375F;
            // 
            // txtGroupFedTax
            // 
            this.txtGroupFedTax.Height = 0.1875F;
            this.txtGroupFedTax.Left = 5.375F;
            this.txtGroupFedTax.Name = "txtGroupFedTax";
            this.txtGroupFedTax.Style = "text-align: right";
            this.txtGroupFedTax.Text = null;
            this.txtGroupFedTax.Top = 0.4375F;
            this.txtGroupFedTax.Width = 0.9375F;
            // 
            // txtGroupBox12L
            // 
            this.txtGroupBox12L.Height = 0.1875F;
            this.txtGroupBox12L.Left = 6.375F;
            this.txtGroupBox12L.Name = "txtGroupBox12L";
            this.txtGroupBox12L.Style = "text-align: right";
            this.txtGroupBox12L.Text = null;
            this.txtGroupBox12L.Top = 0.4375F;
            this.txtGroupBox12L.Width = 0.75F;
            // 
            // txtGroup401
            // 
            this.txtGroup401.Height = 0.1875F;
            this.txtGroup401.Left = 0.4375F;
            this.txtGroup401.Name = "txtGroup401";
            this.txtGroup401.Style = "text-align: right";
            this.txtGroup401.Text = null;
            this.txtGroup401.Top = 0.65625F;
            this.txtGroup401.Width = 1.0625F;
            // 
            // txtGroup403
            // 
            this.txtGroup403.Height = 0.1875F;
            this.txtGroup403.Left = 1.625F;
            this.txtGroup403.Name = "txtGroup403";
            this.txtGroup403.Style = "text-align: right";
            this.txtGroup403.Text = null;
            this.txtGroup403.Top = 0.65625F;
            this.txtGroup403.Width = 1.0625F;
            // 
            // txtGroup408
            // 
            this.txtGroup408.Height = 0.1875F;
            this.txtGroup408.Left = 2.8125F;
            this.txtGroup408.Name = "txtGroup408";
            this.txtGroup408.Style = "text-align: right";
            this.txtGroup408.Text = null;
            this.txtGroup408.Top = 0.65625F;
            this.txtGroup408.Width = 1.0625F;
            // 
            // txtGroup457
            // 
            this.txtGroup457.Height = 0.1875F;
            this.txtGroup457.Left = 4F;
            this.txtGroup457.Name = "txtGroup457";
            this.txtGroup457.Style = "text-align: right";
            this.txtGroup457.Text = null;
            this.txtGroup457.Top = 0.65625F;
            this.txtGroup457.Width = 1.0625F;
            // 
            // txtGroup501
            // 
            this.txtGroup501.Height = 0.1875F;
            this.txtGroup501.Left = 5.1875F;
            this.txtGroup501.Name = "txtGroup501";
            this.txtGroup501.Style = "text-align: right";
            this.txtGroup501.Text = null;
            this.txtGroup501.Top = 0.65625F;
            this.txtGroup501.Width = 1.0625F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.1875F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 0F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-weight: bold";
            this.Label37.Text = "Totals";
            this.Label37.Top = 0.21875F;
            this.Label37.Width = 0.9375F;
            // 
            // Label38
            // 
            this.Label38.Height = 0.1875F;
            this.Label38.HyperLink = null;
            this.Label38.Left = 0F;
            this.Label38.Name = "Label38";
            this.Label38.Style = "";
            this.Label38.Text = "RT";
            this.Label38.Top = 0.4375F;
            this.Label38.Width = 0.25F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1875F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 0.4375F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "";
            this.Label39.Text = "Record Count:";
            this.Label39.Top = 0.96875F;
            this.Label39.Width = 1F;
            // 
            // txtGroupCount
            // 
            this.txtGroupCount.Height = 0.1875F;
            this.txtGroupCount.Left = 1.5F;
            this.txtGroupCount.Name = "txtGroupCount";
            this.txtGroupCount.Text = null;
            this.txtGroupCount.Top = 0.96875F;
            this.txtGroupCount.Width = 0.625F;
            // 
			// txtTotGrossSocSec
			// 
			this.txtTotGrossSocSec.Height = 0.1875F;
			this.txtTotGrossSocSec.Left = 0.3125F;
			this.txtTotGrossSocSec.Name = "txtTotGrossSocSec";
			this.txtTotGrossSocSec.Style = "text-align: right";
			this.txtTotGrossSocSec.Text = null;
			this.txtTotGrossSocSec.Top = 0.4375F;
			this.txtTotGrossSocSec.Width = 0.9375F;
			// 
			// txtTotGrossMed
			// 
			this.txtTotGrossMed.Height = 0.1875F;
			this.txtTotGrossMed.Left = 1.3125F;
			this.txtTotGrossMed.Name = "txtTotGrossMed";
			this.txtTotGrossMed.Style = "text-align: right";
			this.txtTotGrossMed.Text = null;
			this.txtTotGrossMed.Top = 0.4375F;
			this.txtTotGrossMed.Width = 0.9375F;
			// 
			// txtTotGrossFed
			// 
			this.txtTotGrossFed.Height = 0.1875F;
			this.txtTotGrossFed.Left = 2.3125F;
			this.txtTotGrossFed.Name = "txtTotGrossFed";
			this.txtTotGrossFed.Style = "text-align: right";
			this.txtTotGrossFed.Text = null;
			this.txtTotGrossFed.Top = 0.4375F;
			this.txtTotGrossFed.Width = 0.9375F;
			// 
			// txtTotSocTax
			// 
			this.txtTotSocTax.Height = 0.1875F;
			this.txtTotSocTax.Left = 3.375F;
			this.txtTotSocTax.Name = "txtTotSocTax";
			this.txtTotSocTax.Style = "text-align: right";
			this.txtTotSocTax.Text = null;
			this.txtTotSocTax.Top = 0.4375F;
			this.txtTotSocTax.Width = 0.9375F;
			// 
			// txtTotMedTax
			// 
			this.txtTotMedTax.Height = 0.1875F;
			this.txtTotMedTax.Left = 4.375F;
			this.txtTotMedTax.Name = "txtTotMedTax";
			this.txtTotMedTax.Style = "text-align: right";
			this.txtTotMedTax.Text = null;
			this.txtTotMedTax.Top = 0.4375F;
			this.txtTotMedTax.Width = 0.9375F;
			// 
			// txtTotFedTax
			// 
			this.txtTotFedTax.Height = 0.1875F;
			this.txtTotFedTax.Left = 5.375F;
			this.txtTotFedTax.Name = "txtTotFedTax";
			this.txtTotFedTax.Style = "text-align: right";
			this.txtTotFedTax.Text = null;
			this.txtTotFedTax.Top = 0.4375F;
			this.txtTotFedTax.Width = 0.9375F;
			// 
			// txtTotBox12L
			// 
			this.txtTotBox12L.Height = 0.1875F;
			this.txtTotBox12L.Left = 6.375F;
			this.txtTotBox12L.Name = "txtTotBox12L";
			this.txtTotBox12L.Style = "text-align: right";
			this.txtTotBox12L.Text = null;
			this.txtTotBox12L.Top = 0.4375F;
			this.txtTotBox12L.Width = 0.75F;
			// 
			// txtTot401
			// 
			this.txtTot401.Height = 0.1875F;
			this.txtTot401.Left = 0.4375F;
			this.txtTot401.Name = "txtTot401";
			this.txtTot401.Style = "text-align: right";
			this.txtTot401.Text = null;
			this.txtTot401.Top = 0.65625F;
			this.txtTot401.Width = 1.0625F;
			// 
			// txtTot403
			// 
			this.txtTot403.Height = 0.1875F;
			this.txtTot403.Left = 1.625F;
			this.txtTot403.Name = "txtTot403";
			this.txtTot403.Style = "text-align: right";
			this.txtTot403.Text = null;
			this.txtTot403.Top = 0.65625F;
			this.txtTot403.Width = 1.0625F;
			// 
			// txtTot408
			// 
			this.txtTot408.Height = 0.1875F;
			this.txtTot408.Left = 2.8125F;
			this.txtTot408.Name = "txtTot408";
			this.txtTot408.Style = "text-align: right";
			this.txtTot408.Text = null;
			this.txtTot408.Top = 0.65625F;
			this.txtTot408.Width = 1.0625F;
			// 
			// txtTot457
			// 
			this.txtTot457.Height = 0.1875F;
			this.txtTot457.Left = 4F;
			this.txtTot457.Name = "txtTot457";
			this.txtTot457.Style = "text-align: right";
			this.txtTot457.Text = null;
			this.txtTot457.Top = 0.65625F;
			this.txtTot457.Width = 1.0625F;
			// 
			// txtTot501
			// 
			this.txtTot501.Height = 0.1875F;
			this.txtTot501.Left = 5.1875F;
			this.txtTot501.Name = "txtTot501";
			this.txtTot501.Style = "text-align: right";
			this.txtTot501.Text = null;
			this.txtTot501.Top = 0.65625F;
			this.txtTot501.Width = 1.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "Final Totals";
			this.Label21.Top = 0.21875F;
			this.Label21.Width = 0.9375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "";
			this.Label22.Text = null;
			this.Label22.Top = 0.4375F;
			this.Label22.Width = 0.25F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.4375F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "";
			this.Label23.Text = "Final Record Count:";
			this.Label23.Top = 0.96875F;
			this.Label23.Width = 1.3125F;
			// 
			// txtFinalCount
			// 
			this.txtFinalCount.Height = 0.1875F;
			this.txtFinalCount.Left = 1.8125F;
			this.txtFinalCount.Name = "txtFinalCount";
			this.txtFinalCount.Text = null;
			this.txtFinalCount.Top = 0.96875F;
			this.txtFinalCount.Width = 0.625F;
			// 
			// rptW2Electronic
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblMuni2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompany)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossSocSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSocTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox12L)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt401)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt403)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt408)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt457)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt501)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossSocSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGrossFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupSocTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupBox12L)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup401)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup403)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup408)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup457)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup501)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossSocSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGrossFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSocTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBox12L)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot401)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot403)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot408)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot457)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTot501)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossSocSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossMed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossFed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSocTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox12L;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt401;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt403;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt408;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt457;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt501;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGrossSocSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGrossMed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGrossFed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSocTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBox12L;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTot401;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTot403;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTot408;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTot457;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTot501;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinalCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompany;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupGrossSocSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupGrossMed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupGrossFed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupSocTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupBox12L;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup401;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup403;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup408;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup457;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup501;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupCount;
	}
}
