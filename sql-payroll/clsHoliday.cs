﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsHoliday
	{
		//=========================================================
		public enum HolidayType
		{
			NotUsed = 0,
			SpecificDate = 1,
			NthWeekdayOfMonth = 2,
			LastWeekdayOfMonth = 3,
			Easter = 4,
		}

		private int intDay;
		private int intMonth;
		private int intWeekDay;
		private string strName = string.Empty;
		private HolidayType intType;
		private int lngID;
		private int lngShiftID;

		public int ShiftType
		{
			set
			{
				lngShiftID = value;
			}
			get
			{
				int ShiftType = 0;
				ShiftType = lngShiftID;
				return ShiftType;
			}
		}

		public int ID
		{
			set
			{
				value = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string HolidayName
		{
			set
			{
				strName = value;
			}
			get
			{
				string HolidayName = "";
				HolidayName = strName;
				return HolidayName;
			}
		}

		public int DayOfWeek
		{
			set
			{
				// If intDay < vbSunday Or intDay > vbSaturday Then
				// MsgBox "Invalid day of week specified in DayOfWeek", vbCritical, "Error"
				// Exit Property
				// End If
				intWeekDay = value;
			}
			get
			{
				int DayOfWeek = 0;
				DayOfWeek = intWeekDay;
				return DayOfWeek;
			}
		}

		public HolidayType TypeofHoliday
		{
			set
			{
				intType = value;
			}
			get
			{
				HolidayType TypeofHoliday = (HolidayType)0;
				TypeofHoliday = intType;
				return TypeofHoliday;
			}
		}

		public int DayNum
		{
			set
			{
				intDay = value;
			}
			get
			{
				int DayNum = 0;
				DayNum = intDay;
				return DayNum;
			}
		}

		public int MonthNum
		{
			set
			{
				intMonth = value;
			}
			get
			{
				int MonthNum = 0;
				MonthNum = intMonth;
				return MonthNum;
			}
		}
		// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, DateTime)
		public DateTime Get_DateFallsOn(int intYear = 0)
		{
			DateTime DateFallsOn = System.DateTime.Now;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(int, string, DateTime)
			DateTime dtDate;
			// vbPorter upgrade warning: intLastDay As int	OnWriteFCConvert.ToInt32(
			int intLastDay = 0;
			int X;
			int intCount = 0;
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, DateTime)
			DateTime dtTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				DateFallsOn = DateTime.FromOADate(0);
				dtDate = DateTime.FromOADate(0);
				switch (intType)
				{
					case HolidayType.SpecificDate:
						{
							if (intDay < 1 || intYear < 1900)
							{
								MessageBox.Show("Invalid date in DateFallsOn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return DateFallsOn;
							}
							dtDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(intYear));
							break;
						}
					case HolidayType.NthWeekdayOfMonth:
						{
							if (intDay < 1 || intYear < 1900)
							{
								MessageBox.Show("Invalid date in DateFallsOn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return DateFallsOn;
							}
							dtTemp = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(intYear));
							dtTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtTemp);
							dtTemp = FCConvert.ToDateTime(FCConvert.ToString(dtTemp.Month) + "/1/" + FCConvert.ToString(dtTemp.Year));
							dtTemp = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp);
							intLastDay = dtTemp.Day;
							intCount = 0;
							for (X = 1; X <= intLastDay; X++)
							{
								if (fecherFoundation.DateAndTime.Weekday(Convert.ToDateTime(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(X) + "/" + FCConvert.ToString(intYear)), System.DayOfWeek.Sunday) == intWeekDay)
								{
									intCount += 1;
									if (intCount == intDay)
									{
										dtDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(X) + "/" + FCConvert.ToString(intYear));
										break;
									}
								}
							}
							// X
							break;
						}
					case HolidayType.LastWeekdayOfMonth:
						{
							if (intDay < 1 || intYear < 1900)
							{
								MessageBox.Show("Invalid date in DateFallsOn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return DateFallsOn;
							}
							dtTemp = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(intYear));
							dtTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtTemp);
							dtTemp = FCConvert.ToDateTime(FCConvert.ToString(dtTemp.Month) + "/1/" + FCConvert.ToString(dtTemp.Year));
							dtTemp = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp);
							intLastDay = dtTemp.Day;
							intCount = 0;
							for (X = 1; X <= intLastDay; X++)
							{
								if (fecherFoundation.DateAndTime.Weekday(Convert.ToDateTime(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(X) + "/" + FCConvert.ToString(intYear)), System.DayOfWeek.Sunday) == intWeekDay)
								{
									dtDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(X) + "/" + FCConvert.ToString(intYear));
								}
							}
							// X
							break;
						}
					case HolidayType.Easter:
						{
							if (intYear < 1900)
							{
								MessageBox.Show("Invalid date in DateFallsOn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return DateFallsOn;
							}
							dtDate = modCoreysSweeterCode.GetEaster(ref intYear);
							break;
						}
				}
				//end switch
				DateFallsOn = dtDate;
				return DateFallsOn;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DateFallsOn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DateFallsOn;
		}

		public void CopyFrom(ref clsHoliday thol)
		{
			intMonth = thol.MonthNum;
			intDay = thol.DayNum;
			intWeekDay = thol.DayOfWeek;
			strName = thol.HolidayName;
			lngID = thol.ID;
			intType = thol.TypeofHoliday;
			lngShiftID = thol.ShiftType;
		}
	}
}
