﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDistributionSetup.
	/// </summary>
	public partial class rptCustomDistributionSetup : BaseSectionReport
	{
		public rptCustomDistributionSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Distribution Setup";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomDistributionSetup InstancePtr
		{
			get
			{
				return (rptCustomDistributionSetup)Sys.GetInstance(typeof(rptCustomDistributionSetup));
			}
		}

		protected rptCustomDistributionSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomDistributionSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intpage;
		bool boolShade = true;
		private cPYDistributionSetupReport theReport;

		public void Init(ref cPYDistributionSetupReport repObj)
		{
			theReport = repObj;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!theReport.Employees.IsCurrent())
			{
				eArgs.EOF = true;
				return;
			}
			cEmployee currEmployee;
			// vbPorter upgrade warning: currDistribution As cPYDistributionReportItem	OnRead(cPayrollDistribution)
			cPYDistributionReportItem currDistribution;
			// vbPorter upgrade warning: currDist As cPayrollDistribution	OnWrite(cPYDistributionReportItem)
			cPayrollDistribution currDist;
			currEmployee = (cEmployee)theReport.Employees.GetCurrentItem();
			if (FCConvert.ToString(this.Fields["grpHeader"].Value) != currEmployee.EmployeeNumber)
			{
				this.Fields["grpHeader"].Value = currEmployee.EmployeeNumber;
				// currEmployee.DistributionRecords.MoveFirst
				txtEmployee.Text = currEmployee.EmployeeNumber + "   " + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(currEmployee.FirstName + " " + currEmployee.MiddleName) + " " + currEmployee.LastName);
				boolShade = !boolShade;
			}
			currDistribution = (cPYDistributionReportItem)currEmployee.DistributionRecords.GetCurrentItem();
			currDist = currDistribution;
			txtDistNumber.Text = FCConvert.ToString(currDist.RecordNumber);
			// 1=N, 2=W, 3=1 week only, 4=S1, 5=S2, 6=S3   (Hard Coded)
			switch (currDist.AccountCode)
			{
				case "1":
					{
						txtCode.Text = "Hold";
						break;
					}
				case "2":
					{
						txtCode.Text = "Weekly";
						break;
					}
				case "3":
					{
						txtCode.Text = "1 Week Only";
						break;
					}
				case "4":
					{
						txtCode.Text = "Separate Check (1)";
						break;
					}
				case "5":
					{
						txtCode.Text = "Separate Check (2)";
						break;
					}
				case "6":
					{
						txtCode.Text = "Separate Check (3)";
						break;
					}
				default:
					{
						txtCode.Text = string.Empty;
						break;
					}
			}
			//end switch
			txtAccount.Text = FCConvert.ToString(currDist.AccountNumber);
			txtPayCat.Text = currDistribution.CategoryDescription;
			txtTaxStatus.Text = currDistribution.TaxStatusDescription;
			txtM.Text = currDist.MSRS;
			txtU.Text = currDist.DistU;
			txtWC.Text = currDist.WorkComp;
			txtPayCD.Text = currDistribution.PayCodeDescription;
			txtBaseRate.Text = Strings.Format(currDist.BaseRate, "0.00");
			txtTaxNumPer.Text = Strings.Format(currDist.NumberWeeks, "0.00");
			txtDefaultHrs.Text = Strings.Format(currDist.DefaultHours, "0.00");
			txtHrsThisWeek.Text = Strings.Format(currDist.HoursWeek, "0.00");
			currEmployee.DistributionRecords.MoveNext();
			if (!currEmployee.DistributionRecords.IsCurrent())
			{
				theReport.Employees.MoveNext();
				if (theReport.Employees.IsCurrent())
				{
					currEmployee = (cEmployee)theReport.Employees.GetCurrentItem();
					currEmployee.DistributionRecords.MoveFirst();
				}
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			theReport.Employees.MoveFirst();
			cEmployee tempEmp;
			if (theReport.Employees.IsCurrent())
			{
				tempEmp = (cEmployee)theReport.Employees.GetCurrentItem();
				tempEmp.DistributionRecords.MoveFirst();
				this.Fields["grpHeader"].Value = tempEmp.EmployeeNumber;
				txtEmployee.Text = tempEmp.EmployeeNumber + "   " + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(tempEmp.FirstName + " " + tempEmp.MiddleName) + " " + tempEmp.LastName);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolShade)
			{
				Detail.BackColor = ColorTranslator.FromOle(0xF3F3F2);
				this.GroupHeader1.BackColor = Color.White;
			}
			else
			{
				Detail.BackColor = Color.White;
				this.GroupHeader1.BackColor = ColorTranslator.FromOle(0xF3F3F2);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		
		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
