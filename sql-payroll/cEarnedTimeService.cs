//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEarnedTimeService
	{
		//=========================================================
		public cGenericCollection GetCodeTypes()
		{
            cGenericCollection colTypes = new cGenericCollection();
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                cEarnedTimeType tempItem;
                rsLoad.OpenRecordset("select * from tblCodeTypes order by id", "Payroll");
                while (!rsLoad.EndOfFile())
                {
                    tempItem = new cEarnedTimeType();
                    tempItem.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
                    tempItem.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
                    colTypes.AddItem(tempItem);
                    rsLoad.MoveNext();
                }

                
            }

            return colTypes;
        }

		public int GetVacationID()
		{
			int GetVacationID = 0;
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                int lngID = 0;
                rsLoad.OpenRecordset("select * from tblcodetypes where description = 'vacation'", "Payroll");
                if (!rsLoad.EndOfFile())
                {
                    lngID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
                }

                GetVacationID = lngID;
            }

            return GetVacationID;
		}

		public int GetSickID()
		{
			int GetSickID = 0;
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                int lngID = 0;
                rsLoad.OpenRecordset("select * from tblcodetypes where description = 'sick'", "Payroll");
                if (!rsLoad.EndOfFile())
                {
                    lngID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
                }

                GetSickID = lngID;
            }

            return GetSickID;
		}
	}
}
