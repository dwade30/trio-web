﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt941Wage.
	/// </summary>
	partial class rpt941Wage
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt941Wage));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMCWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSMCTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMCTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMCPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTaxGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateIncomeWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCalcSSMCTaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSMCTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateIncomeWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcSSMCTaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSSN,
				this.txtName,
				this.txtStateGross,
				this.txtFedGross,
				this.txtFICAGross,
				this.txtMedGross
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.txtStateTotal,
				this.txtFederalTotal,
				this.txtFICATotal,
				this.txtMedTotal,
				this.Line2,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.txtTotalWage,
				this.txtFedTax,
				this.txtSSWage,
				this.txtMCWage,
				this.txtSSMCTax,
				this.txtTotNetTax,
				this.txtSSTax,
				this.txtMCTax,
				this.txtSSPercent,
				this.txtMCPercent,
				this.txtStateTaxGross,
				this.txtStateIncomeWH,
				this.Label22,
				this.txtCalcSSMCTaxWH
			});
			this.ReportFooter.Height = 3.760417F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.Label4,
				this.Label5,
				this.Line1,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.lblDate,
				this.Label6,
				this.Label7,
				this.Label8
			});
			this.PageHeader.Height = 1.020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.125F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label3.Text = "SSN";
			this.Label3.Top = 0.8333333F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.15625F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label4.Text = "Employee Name";
			this.Label4.Top = 0.8333333F;
			this.Label4.Width = 1.125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.125F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label5.Text = "State Gross";
			this.Label5.Top = 0.8333333F;
			this.Label5.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.09375F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.3125F;
			this.Line1.X1 = 0.09375F;
			this.Line1.X2 = 7.40625F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL - ( 941 Quarterly )";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.21875F;
			this.lblDate.Width = 4.625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.2083333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.21875F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label6.Text = "Federal Gross";
			this.Label6.Top = 0.8333333F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2083333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.28125F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label7.Text = "FICA Gross";
			this.Label7.Top = 0.8333333F;
			this.Label7.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.2083333F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.34375F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label8.Text = "MED Gross";
			this.Label8.Top = 0.8333333F;
			this.Label8.Width = 1F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1666667F;
			this.txtSSN.Left = 0.125F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0F;
			this.txtSSN.Width = 1F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 1.15625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 1.84375F;
			// 
			// txtStateGross
			// 
			this.txtStateGross.Height = 0.1666667F;
			this.txtStateGross.Left = 3.125F;
			this.txtStateGross.Name = "txtStateGross";
			this.txtStateGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateGross.Text = null;
			this.txtStateGross.Top = 0F;
			this.txtStateGross.Width = 1F;
			// 
			// txtFedGross
			// 
			this.txtFedGross.Height = 0.1666667F;
			this.txtFedGross.Left = 4.21875F;
			this.txtFedGross.Name = "txtFedGross";
			this.txtFedGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFedGross.Text = null;
			this.txtFedGross.Top = 0F;
			this.txtFedGross.Width = 1F;
			// 
			// txtFICAGross
			// 
			this.txtFICAGross.Height = 0.1666667F;
			this.txtFICAGross.Left = 5.28125F;
			this.txtFICAGross.Name = "txtFICAGross";
			this.txtFICAGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFICAGross.Text = null;
			this.txtFICAGross.Top = 0F;
			this.txtFICAGross.Width = 1F;
			// 
			// txtMedGross
			// 
			this.txtMedGross.Height = 0.1666667F;
			this.txtMedGross.Left = 6.34375F;
			this.txtMedGross.Name = "txtMedGross";
			this.txtMedGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMedGross.Text = null;
			this.txtMedGross.Top = 0F;
			this.txtMedGross.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label9.Text = "Totals    ";
			this.Label9.Top = 0.1666667F;
			this.Label9.Width = 1F;
			// 
			// txtStateTotal
			// 
			this.txtStateTotal.Height = 0.1666667F;
			this.txtStateTotal.Left = 3.125F;
			this.txtStateTotal.Name = "txtStateTotal";
			this.txtStateTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateTotal.Text = null;
			this.txtStateTotal.Top = 0.1666667F;
			this.txtStateTotal.Width = 1F;
			// 
			// txtFederalTotal
			// 
			this.txtFederalTotal.Height = 0.1666667F;
			this.txtFederalTotal.Left = 4.21875F;
			this.txtFederalTotal.Name = "txtFederalTotal";
			this.txtFederalTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFederalTotal.Text = null;
			this.txtFederalTotal.Top = 0.1666667F;
			this.txtFederalTotal.Width = 1F;
			// 
			// txtFICATotal
			// 
			this.txtFICATotal.Height = 0.1666667F;
			this.txtFICATotal.Left = 5.28125F;
			this.txtFICATotal.Name = "txtFICATotal";
			this.txtFICATotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFICATotal.Text = null;
			this.txtFICATotal.Top = 0.1666667F;
			this.txtFICATotal.Width = 1F;
			// 
			// txtMedTotal
			// 
			this.txtMedTotal.Height = 0.1666667F;
			this.txtMedTotal.Left = 6.34375F;
			this.txtMedTotal.Name = "txtMedTotal";
			this.txtMedTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMedTotal.Text = null;
			this.txtMedTotal.Top = 0.1666667F;
			this.txtMedTotal.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.3125F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 5.1875F;
			this.Line2.X1 = 2.3125F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.2083333F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.59375F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label10.Text = "Category";
			this.Label10.Top = 0.7916667F;
			this.Label10.Width = 1.125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.2083333F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.25F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label11.Text = "Wage";
			this.Label11.Top = 0.7916667F;
			this.Label11.Width = 0.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.2083333F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.71875F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label12.Text = "Tax";
			this.Label12.Top = 0.7916667F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.2083333F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.46875F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label13.Text = "Percent";
			this.Label13.Top = 0.7916667F;
			this.Label13.Width = 1F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.2083333F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.59375F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label14.Text = "Total Wages";
			this.Label14.Top = 1F;
			this.Label14.Width = 1.125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.2083333F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 1.59375F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label15.Text = "Fed Tax Withheld";
			this.Label15.Top = 1.291667F;
			this.Label15.Width = 1.1875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.2F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.6F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label16.Text = "SS Wages/Tax (Calc)";
			this.Label16.Top = 1.6F;
			this.Label16.Width = 1.433333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.2333333F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.6F;
			this.Label17.MultiLine = false;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label17.Text = "MC Wages/Tax (Calc)";
			this.Label17.Top = 1.866667F;
			this.Label17.Width = 1.466667F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.2F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 1.6F;
			this.Label18.MultiLine = false;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label18.Text = "SS + MC Tax (Actual)";
			this.Label18.Top = 2.466667F;
			this.Label18.Width = 1.7F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.2F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.6F;
			this.Label19.MultiLine = false;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label19.Text = "Tot/Net Tax";
			this.Label19.Top = 2.766667F;
			this.Label19.Width = 1.133333F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.2F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.333333F;
			this.Label20.MultiLine = false;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label20.Text = "State Taxable Gross";
			this.Label20.Top = 3.133333F;
			this.Label20.Width = 1.833333F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.2333333F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 2.333333F;
			this.Label21.MultiLine = false;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label21.Text = "State income Tax Withheld";
			this.Label21.Top = 3.366667F;
			this.Label21.Width = 1.833333F;
			// 
			// txtTotalWage
			// 
			this.txtTotalWage.Height = 0.2083333F;
			this.txtTotalWage.Left = 3.09375F;
			this.txtTotalWage.Name = "txtTotalWage";
			this.txtTotalWage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalWage.Text = null;
			this.txtTotalWage.Top = 1F;
			this.txtTotalWage.Width = 1F;
			// 
			// txtFedTax
			// 
			this.txtFedTax.Height = 0.2083333F;
			this.txtFedTax.Left = 4.4375F;
			this.txtFedTax.Name = "txtFedTax";
			this.txtFedTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFedTax.Text = null;
			this.txtFedTax.Top = 1.291667F;
			this.txtFedTax.Width = 1F;
			// 
			// txtSSWage
			// 
			this.txtSSWage.Height = 0.2083333F;
			this.txtSSWage.Left = 3.09375F;
			this.txtSSWage.Name = "txtSSWage";
			this.txtSSWage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtSSWage.Text = null;
			this.txtSSWage.Top = 1.583333F;
			this.txtSSWage.Width = 1F;
			// 
			// txtMCWage
			// 
			this.txtMCWage.Height = 0.2083333F;
			this.txtMCWage.Left = 3.09375F;
			this.txtMCWage.Name = "txtMCWage";
			this.txtMCWage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMCWage.Text = null;
			this.txtMCWage.Top = 1.875F;
			this.txtMCWage.Width = 1F;
			// 
			// txtSSMCTax
			// 
			this.txtSSMCTax.Height = 0.2F;
			this.txtSSMCTax.Left = 4.433333F;
			this.txtSSMCTax.Name = "txtSSMCTax";
			this.txtSSMCTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtSSMCTax.Text = null;
			this.txtSSMCTax.Top = 2.466667F;
			this.txtSSMCTax.Width = 1F;
			// 
			// txtTotNetTax
			// 
			this.txtTotNetTax.Height = 0.2F;
			this.txtTotNetTax.Left = 4.433333F;
			this.txtTotNetTax.Name = "txtTotNetTax";
			this.txtTotNetTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotNetTax.Text = null;
			this.txtTotNetTax.Top = 2.766667F;
			this.txtTotNetTax.Width = 1F;
			// 
			// txtSSTax
			// 
			this.txtSSTax.Height = 0.2083333F;
			this.txtSSTax.Left = 4.4375F;
			this.txtSSTax.Name = "txtSSTax";
			this.txtSSTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtSSTax.Text = null;
			this.txtSSTax.Top = 1.583333F;
			this.txtSSTax.Width = 1F;
			// 
			// txtMCTax
			// 
			this.txtMCTax.Height = 0.2083333F;
			this.txtMCTax.Left = 4.4375F;
			this.txtMCTax.Name = "txtMCTax";
			this.txtMCTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMCTax.Text = null;
			this.txtMCTax.Top = 1.875F;
			this.txtMCTax.Width = 1F;
			// 
			// txtSSPercent
			// 
			this.txtSSPercent.Height = 0.2083333F;
			this.txtSSPercent.Left = 5.65625F;
			this.txtSSPercent.Name = "txtSSPercent";
			this.txtSSPercent.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtSSPercent.Text = null;
			this.txtSSPercent.Top = 1.583333F;
			this.txtSSPercent.Width = 0.875F;
			// 
			// txtMCPercent
			// 
			this.txtMCPercent.Height = 0.2083333F;
			this.txtMCPercent.Left = 5.65625F;
			this.txtMCPercent.Name = "txtMCPercent";
			this.txtMCPercent.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtMCPercent.Text = null;
			this.txtMCPercent.Top = 1.875F;
			this.txtMCPercent.Width = 0.875F;
			// 
			// txtStateTaxGross
			// 
			this.txtStateTaxGross.Height = 0.2F;
			this.txtStateTaxGross.Left = 4.433333F;
			this.txtStateTaxGross.Name = "txtStateTaxGross";
			this.txtStateTaxGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateTaxGross.Text = null;
			this.txtStateTaxGross.Top = 3.133333F;
			this.txtStateTaxGross.Width = 1F;
			// 
			// txtStateIncomeWH
			// 
			this.txtStateIncomeWH.Height = 0.2333333F;
			this.txtStateIncomeWH.Left = 4.433333F;
			this.txtStateIncomeWH.Name = "txtStateIncomeWH";
			this.txtStateIncomeWH.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtStateIncomeWH.Text = null;
			this.txtStateIncomeWH.Top = 3.366667F;
			this.txtStateIncomeWH.Width = 1F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.2F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 1.6F;
			this.Label22.MultiLine = false;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label22.Text = "SS + MC Tax (Calc)";
			this.Label22.Top = 2.166667F;
			this.Label22.Width = 1.7F;
			// 
			// txtCalcSSMCTaxWH
			// 
			this.txtCalcSSMCTaxWH.Height = 0.2F;
			this.txtCalcSSMCTaxWH.Left = 4.433333F;
			this.txtCalcSSMCTaxWH.Name = "txtCalcSSMCTaxWH";
			this.txtCalcSSMCTaxWH.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtCalcSSMCTaxWH.Text = null;
			this.txtCalcSSMCTaxWH.Top = 2.166667F;
			this.txtCalcSSMCTaxWH.Width = 1F;
			// 
			// rpt941Wage
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSMCTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMCPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateIncomeWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalcSSMCTaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMCWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSMCTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMCTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSPercent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMCPercent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateIncomeWH;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalcSSMCTaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
