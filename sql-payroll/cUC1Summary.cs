﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cUC1Summary
	{
		//=========================================================
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string straddress = string.Empty;
		private double dblContributionRate;
		// vbPorter upgrade warning: strMaineVendorCode As int	OnWrite(string)
		private int strMaineVendorCode;
		private string strStateCode = string.Empty;
		private string strYear = string.Empty;
		private DateTime dtStart;
		private DateTime dtEnd;
		private string strAccountID = string.Empty;
		private string strFedId = string.Empty;
		private string strName = string.Empty;
		private int intEmployeeCountMonth1;
		private int intEmployeeCountMonth2;
		private int intEmployeeCountMonth3;
		private int intFemaleCountMonth1;
		private int intFemaleCountMonth2;
		private int intFemaleCountMonth3;
		private double dblUCWages;
		private double dblExcessWages;
		private double dblTaxableWages;
		private double dblUCDue;
		private double dblCSSFAssess;
        private double dblUPAFAssess;
		private double dblTotalContCSSFUPAFDue;
		private string strPreparerEIN = string.Empty;
		private string strPayrollProcessor = string.Empty;
		private string strZipExt = string.Empty;

		public string ZipExtension
		{
			set
			{
				strZipExt = value;
			}
			get
			{
				string ZipExtension = "";
				ZipExtension = strZipExt;
				return ZipExtension;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string zip = "";
				zip = strZip;
				return zip;
			}
		}

		public string Address
		{
			set
			{
				straddress = value;
			}
			get
			{
				string Address = "";
				Address = straddress;
				return Address;
			}
		}

		public double UCRate
		{
			set
			{
				dblContributionRate = value;
			}
			get
			{
				double UCRate = 0;
				UCRate = dblContributionRate;
				return UCRate;
			}
		}
		// vbPorter upgrade warning: strCode As string	OnRead
		public string MaineVendorCode
		{
			set
			{
				strMaineVendorCode = FCConvert.ToInt32(value);
			}
			// vbPorter upgrade warning: 'Return' As string	OnWrite
			get
			{
				string MaineVendorCode = "";
				MaineVendorCode = FCConvert.ToString(strMaineVendorCode);
				return MaineVendorCode;
			}
		}

		public string StateCode
		{
			set
			{
				strStateCode = value;
			}
			get
			{
				string StateCode = "";
				StateCode = strStateCode;
				return StateCode;
			}
		}

		public string ReportYear
		{
			set
			{
				strYear = value;
			}
			get
			{
				string ReportYear = "";
				ReportYear = strYear;
				return ReportYear;
			}
		}

		public DateTime StartDate
		{
			set
			{
				dtStart = value;
			}
			get
			{
				DateTime StartDate = System.DateTime.Now;
				StartDate = dtStart;
				return StartDate;
			}
		}

		public DateTime EndDate
		{
			set
			{
				dtEnd = value;
			}
			get
			{
				DateTime EndDate = System.DateTime.Now;
				EndDate = dtEnd;
				return EndDate;
			}
		}

		public string UCAccountID
		{
			set
			{
				strAccountID = value;
			}
			get
			{
				string UCAccountID = "";
				UCAccountID = strAccountID;
				return UCAccountID;
			}
		}

		public string FederalID
		{
			set
			{
				strFedId = value;
			}
			get
			{
				string FederalID = "";
				FederalID = strFedId;
				return FederalID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public int EmployeeCountMonth1
		{
			set
			{
				intEmployeeCountMonth1 = value;
			}
			get
			{
				int EmployeeCountMonth1 = 0;
				EmployeeCountMonth1 = intEmployeeCountMonth1;
				return EmployeeCountMonth1;
			}
		}

		public int EmployeeCountMonth2
		{
			set
			{
				intEmployeeCountMonth2 = value;
			}
			get
			{
				int EmployeeCountMonth2 = 0;
				EmployeeCountMonth2 = intEmployeeCountMonth2;
				return EmployeeCountMonth2;
			}
		}

		public int EmployeeCountMonth3
		{
			set
			{
				intEmployeeCountMonth3 = value;
			}
			get
			{
				int EmployeeCountMonth3 = 0;
				EmployeeCountMonth3 = intEmployeeCountMonth3;
				return EmployeeCountMonth3;
			}
		}

		public int FemaleCountMonth1
		{
			set
			{
				intFemaleCountMonth1 = value;
			}
			get
			{
				int FemaleCountMonth1 = 0;
				FemaleCountMonth1 = intFemaleCountMonth1;
				return FemaleCountMonth1;
			}
		}

		public int FemaleCountMonth2
		{
			set
			{
				intFemaleCountMonth2 = value;
			}
			get
			{
				int FemaleCountMonth2 = 0;
				FemaleCountMonth2 = intFemaleCountMonth2;
				return FemaleCountMonth2;
			}
		}

		public int FemaleCountMonth3
		{
			set
			{
				intFemaleCountMonth3 = value;
			}
			get
			{
				int FemaleCountMonth3 = 0;
				FemaleCountMonth3 = intFemaleCountMonth3;
				return FemaleCountMonth3;
			}
		}

		public double UCWages
		{
			set
			{
				dblUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblUCWages;
				return UCWages;
			}
		}

		public double ExcessWages
		{
			set
			{
				dblExcessWages = value;
			}
			get
			{
				double ExcessWages = 0;
				ExcessWages = dblExcessWages;
				return ExcessWages;
			}
		}

		public double TaxableWages
		{
			set
			{
				dblTaxableWages = value;
			}
			get
			{
				double TaxableWages = 0;
				TaxableWages = dblTaxableWages;
				return TaxableWages;
			}
		}

		public double UCDue
		{
			set
			{
				dblUCDue = value;
			}
			get
			{
				double UCDue = 0;
				UCDue = dblUCDue;
				return UCDue;
			}
		}

		public double CSSFAssess
		{
			set
			{
				dblCSSFAssess = value;
			}
			get
			{
				double CSSFAssess = 0;
				CSSFAssess = dblCSSFAssess;
				return CSSFAssess;
			}
		}

        public double UPAFAssess
        {
            set
            {
                dblUPAFAssess = value;
            }
            get
            {
                double UPAFAssess = 0;
                UPAFAssess = dblUPAFAssess;
                return UPAFAssess;
            }
        }

		public double TotalContCSSFUPAFDue
		{
			set
			{
				dblTotalContCSSFUPAFDue = value;
			}
			get
			{
				double TotalContCSSFUPAFDue = 0;
				TotalContCSSFUPAFDue = dblTotalContCSSFUPAFDue;
				return TotalContCSSFUPAFDue;
			}
		}

		public string PreparerEIN
		{
			set
			{
				strPreparerEIN = value;
			}
			get
			{
				string PreparerEIN = "";
				PreparerEIN = strPreparerEIN;
				return PreparerEIN;
			}
		}

		public string PayrollProcessor
		{
			set
			{
				strPayrollProcessor = value;
			}
			get
			{
				string PayrollProcessor = "";
				PayrollProcessor = strPayrollProcessor;
				return PayrollProcessor;
			}
		}

		public cUC1Summary() : base()
		{
			strStateCode = "ME";
			strMaineVendorCode = FCConvert.ToInt32("15");
		}
	}
}
