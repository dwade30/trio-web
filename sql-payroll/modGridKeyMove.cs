﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWPY0000
{
	public class modGridKeyMove
	{
		//=========================================================
		// vbPorter upgrade warning: GridName As object	OnWrite(FCGridCtl.vsFlexGrid)
		// vbPorter upgrade warning: KeyCode As int	OnWrite(int, Keys)
		public static void MoveGridProsition(ref FCGrid GridName, int intStartCol, int intEndCol, Keys KeyCode = 0, int KeyAsii = 9999, bool AddNewRecord = false)
		{
			Statics.gboolSkipKeyMove = false;
			if (KeyAsii == 9999)
			{
				if (KeyCode == Keys.Tab || KeyCode == Keys.Return)
				{
					KeyCode = Keys.Tab;
				}
				Statics.intKeyValue = FCConvert.ToInt32(KeyCode);
			}
			else
			{
				Statics.intKeyValue = KeyAsii;
			}
			switch (Statics.intKeyValue)
			{
				case 37:
					{
						MoveLeftAgain:
						;
						// left key
						if (GridName.Col == intStartCol)
						{
							if (GridName.Row > GridName.FixedRows)
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row - 1, intEndCol) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (intEndCol == intStartCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Row -= 1;
										GridName.Col = intEndCol;
										goto MoveLeftAgain;
									}
								}
								else
								{
									GridName.Row -= 1;
									GridName.Col = intEndCol;
									KeyCode = 0;
								}
							}
						}
						else
						{
							if (GridName.Col - 1 == 1)
							{
								Statics.intColNumber = 1;
							}
							else
							{
								for (Statics.intCounter = GridName.Col - 1; Statics.intCounter >= intStartCol; Statics.intCounter--)
								{
									if (GridName.ColHidden(Statics.intCounter) || GridName.ColWidth(Statics.intCounter) == 0)
									{
									}
									else
									{
										Statics.intColNumber = Statics.intCounter;
										break;
									}
								}
							}
							if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, Statics.intColNumber) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
							{
								// the next column we want to be disabled
								if (Statics.intColNumber < intStartCol)
								{
									// there is no more columns to go to so
									// stay where you are
									KeyCode = 0;
								}
								else
								{
									Statics.gboolSkipKeyMove = true;
									GridName.Col = Statics.intColNumber;
									goto MoveLeftAgain;
								}
							}
						}
						break;
					}
				case 38:
					{
						MoveUpAgain:
						;
						// Up key
						if (GridName.Row > 0)
						{
							if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row - 1, GridName.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
							{
								// the previous row we want to be disabled
								if (GridName.Row - 1 < GridName.FixedRows)
								{
									// there is no more rows to go to so
									// stay where you are
									KeyCode = 0;
								}
								else
								{
									Statics.gboolSkipKeyMove = true;
									GridName.Row -= 1;
									// GoTo MoveUpAgain
								}
							}
						}
						break;
					}
				case 39:
					{
						MoveRightAgain:
						;
						// right key
						if (GridName.Col == intEndCol)
						{
							if (GridName.Row < GridName.Rows - 1)
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row + 1, intStartCol) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (intEndCol == intStartCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Row += 1;
										GridName.Col = intStartCol;
										goto MoveRightAgain;
									}
								}
								else
								{
									GridName.Row += 1;
									GridName.Col = intStartCol;
									KeyCode = 0;
								}
							}
							else
							{
								if (AddNewRecord)
								{
									FCUtils.CallByName(GridName.FindForm(), "AddNewGridRecord", CallType.Method);
									KeyCode = 0;
								}
								else
								{
									PreviousRow:
									;
									// find the last row that was legal
									for (Statics.intCounter = GridName.Col; Statics.intCounter >= intStartCol; Statics.intCounter--)
									{
										if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, Statics.intCounter) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
										{
											GridName.Col = Statics.intCounter - 1;
											break;
										}
									}
									if (Statics.intCounter == 1)
									{
										if (GridName.Row > GridName.FixedRows)
										{
											GridName.Row -= 1;
											GridName.Col = intEndCol;
											goto PreviousRow;
										}
									}
								}
							}
						}
						else
						{
							if (GridName.ColHidden(GridName.Col + 1))
							{
								Statics.gboolSkipKeyMove = true;
								GridName.Col += 1;
								goto MoveRightAgain;
							}
							else
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, GridName.Col + 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (GridName.Col + 1 > intEndCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Col += 1;
										goto MoveRightAgain;
									}
								}
							}
						}
						break;
					}
				case 40:
					{
						MoveDownAgain:
						;
						// down key
						if (GridName.Row + 1 >= GridName.Rows)
						{
							if (AddNewRecord)
							{
								FCUtils.CallByName(GridName.FindForm(), "AddNewGridRecord", CallType.Method);
							}
							else
							{
								// find the last row that was legal
								for (Statics.intCounter = GridName.Row; Statics.intCounter >= GridName.FixedRows; Statics.intCounter--)
								{
									if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Statics.intCounter, GridName.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
									{
										GridName.Row = Statics.intCounter - 1;
										break;
									}
								}
							}
						}
						else if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row + 1, GridName.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							// the next row we want to be disabled
							if (GridName.Row + 1 >= GridName.Rows)
							{
								// there is no more rows to go to so
								// stay where you are
								KeyCode = 0;
							}
							else
							{
								Statics.gboolSkipKeyMove = true;
								GridName.Row += 1;
								goto MoveDownAgain;
							}
						}
						break;
					}
			}
			//end switch
		}

		public class StaticVariables
		{
			public int intCounter;
			public int intKeyValue;
			public int intColNumber;
			public bool gboolSkipKeyMove;
			// not working yet
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
