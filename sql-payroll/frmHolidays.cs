﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmHolidays : BaseForm
	{
		public frmHolidays()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmHolidays InstancePtr
		{
			get
			{
				return (frmHolidays)Sys.GetInstance(typeof(frmHolidays));
			}
		}

		protected frmHolidays _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsHolidaysList HolList = new clsHolidaysList();
		const int cnstgridcolID = 0;
		const int CNSTGridColName = 1;
		const int CNSTGRIDCOLTYPE = 2;
		const int CNSTGRIDCOLDATE = 3;
		const int CNSTGRIDCOLMONTH = 4;
		const int CNSTGRIDCOLDAY = 5;
		const int CNSTGRIDCOLWEEKDAY = 6;
		const int CNSTGRIDCOLSHIFT = 7;
		const int CNSTGRIDCOLINDEX = 8;
		private bool boolDataChanged;

		private void frmHolidays_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmHolidays_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmHolidays properties;
			//frmHolidays.FillStyle	= 0;
			//frmHolidays.ScaleWidth	= 5880;
			//frmHolidays.ScaleHeight	= 4110;
			//frmHolidays.LinkTopic	= "Form2";
			//frmHolidays.LockControls	= -1  'True;
			//frmHolidays.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadData();
			boolDataChanged = false;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				// vbPorter upgrade warning: lngReturn As int	OnWrite(DialogResult)
				DialogResult lngReturn = 0;
				lngReturn = MessageBox.Show("Data has been changed but not saved" + "\r\n" + "Save changes now?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (lngReturn == DialogResult.Cancel)
				{
					e.Cancel = true;
					return;
				}
				else if (lngReturn == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmHolidays_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			clsHoliday thol;
			clsHoliday sHol;
			int lngRow;
			lngRow = Grid.MouseRow;
			thol = new clsHoliday();
			sHol = HolList.GetHolidayByIndex(FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX)));
			if (!(sHol == null))
			{
				thol.CopyFrom(ref sHol);
				if (frmEditHolidays.InstancePtr.Init(ref thol))
				{
					boolDataChanged = true;
					sHol.CopyFrom(ref thol);
					ShowRow(ref sHol, ref lngRow);
				}
			}
		}

		private object InsertHoliday()
		{
			object InsertHoliday = null;
			// VB6 Bad Scope Dim:
			int lngRow = 0;
			clsHoliday thol = new clsHoliday();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				thol.TypeofHoliday = clsHoliday.HolidayType.NotUsed;
				if (frmEditHolidays.InstancePtr.Init(ref thol))
				{
					if (HolList.InsertHoliday(ref thol))
					{
						boolDataChanged = true;
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(HolList.GetCurrentIndex()));
						ShowRow(ref thol, ref lngRow);
					}
				}
				return InsertHoliday;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertHoliday", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return InsertHoliday;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						InsertHoliday();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteHoliday();
						break;
					}
			}
			//end switch
		}

		private void DeleteHoliday()
		{
			int lngRow;
			lngRow = Grid.Row;
			if (lngRow < 1)
				return;
			boolDataChanged = true;
			HolList.DeleteHolidayByIndex(FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX)));
			Grid.RemoveItem(lngRow);
		}

		private void mnuAddHoliday_Click(object sender, System.EventArgs e)
		{
			InsertHoliday();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = Grid.Row;
			if (lngRow > 0)
			{
				DeleteHoliday();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
                //FC:FINAL:BSE #4268 leave form open after saving is complete
				//Close();
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGridColName, FCConvert.ToInt32(0.4 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLTYPE, FCConvert.ToInt32(0.37 * GridWidth));
		}

		private void SetupGrid()
		{
			Grid.Cols = 9;
			Grid.ColHidden(cnstgridcolID, true);
			Grid.ColHidden(CNSTGRIDCOLMONTH, true);
			Grid.ColHidden(CNSTGRIDCOLDAY, true);
			Grid.ColHidden(CNSTGRIDCOLWEEKDAY, true);
			Grid.ColHidden(CNSTGRIDCOLSHIFT, true);
			Grid.ColHidden(CNSTGRIDCOLINDEX, true);
			string strTemp = "";
			Grid.ColDataType(CNSTGRIDCOLDATE, FCGrid.DataTypeSettings.flexDTDate);
			strTemp = "#" + FCConvert.ToString(FCConvert.ToInt32(clsHoliday.HolidayType.SpecificDate)) + ";Specific Date|#" + FCConvert.ToString(FCConvert.ToInt32(clsHoliday.HolidayType.NthWeekdayOfMonth)) + ";Nth Weekday of Month|#" + FCConvert.ToString(FCConvert.ToInt32(clsHoliday.HolidayType.LastWeekdayOfMonth)) + ";Last Weekday of Month|#" + FCConvert.ToString(FCConvert.ToInt32(clsHoliday.HolidayType.Easter)) + ";Easter";
			Grid.ColComboList(CNSTGRIDCOLTYPE, strTemp);
			Grid.TextMatrix(0, CNSTGridColName, "Holiday");
			Grid.TextMatrix(0, CNSTGRIDCOLTYPE, "Type");
			Grid.TextMatrix(0, CNSTGRIDCOLDATE, "This Year");
			Grid.TextMatrix(0, CNSTGRIDCOLSHIFT, "Shift");
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				HolList.LoadHolidays();
				int lngRow = 0;
				Grid.Rows = 1;
				clsHoliday thol;
				while (!HolList.EndOfHolidays())
				{
					thol = HolList.GetCurrentHoliday();
					if (!(thol == null))
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(HolList.GetCurrentIndex()));
						ShowRow(ref thol, ref lngRow);
					}
					HolList.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowRow(ref clsHoliday thol, ref int lngRow)
		{
			Grid.TextMatrix(lngRow, cnstgridcolID, thol.ID);
			Grid.TextMatrix(lngRow, CNSTGRIDCOLDATE, thol.Get_DateFallsOn(DateTime.Today.Year));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLDAY, FCConvert.ToString(Conversion.Val(thol.DayNum)));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLMONTH, FCConvert.ToString(Conversion.Val(thol.MonthNum)));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, thol.TypeofHoliday);
			Grid.TextMatrix(lngRow, CNSTGridColName, thol.HolidayName);
			Grid.TextMatrix(lngRow, CNSTGRIDCOLSHIFT, thol.ShiftType);
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				if (HolList.SaveHolidays())
				{
					SaveData = true;
					LoadData();
					boolDataChanged = false;
					MessageBox.Show("Save successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
