﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptStateUnemploymentC1.
	/// </summary>
	partial class rptStateUnemploymentC1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptStateUnemploymentC1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSocialSecurityNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossWagesPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTaxWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotaGrossWagesPd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExcess = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalQTDPayroll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcessOver7000 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSocialSecurityNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossWagesPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxWithheld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotaGrossWagesPd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWithheld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTDPayroll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcessOver7000)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSocialSecurityNum,
				this.txtName,
				this.txtTotalGross,
				this.txtGrossWagesPaid,
				this.txtStateTaxWithheld
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label6,
				this.txtTotalTotalGross,
				this.txtTotaGrossWagesPd,
				this.txtTotalWithheld,
				this.Line1,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.lblExcess,
				this.txtTotalQTDPayroll,
				this.txtExcessOver7000,
				this.Line2,
				this.Label12,
				this.txtTaxable,
				this.Label13,
				this.txtRate,
				this.Label14,
				this.txtDue,
				this.Line3
			});
			this.ReportFooter.Height = 2.552083F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtTitle,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.txtTitle3
			});
			this.PageHeader.Height = 0.53125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle2
			});
			this.PageFooter.Height = 0.1666667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.GroupHeader1.Height = 0.4166667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0.0625F;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0.03125F;
			this.txtMuniname.Width = 1.9375F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 2F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "State Unemployment (C-1)";
			this.txtTitle.Top = 0.03125F;
			this.txtTitle.Width = 3.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 5.8125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field3";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.9375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 5.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.9375F;
			// 
			// txtTitle3
			// 
			this.txtTitle3.Height = 0.19F;
			this.txtTitle3.Left = 2F;
			this.txtTitle3.Name = "txtTitle3";
			this.txtTitle3.Style = "text-align: center";
			this.txtTitle3.Text = "Pay Date";
			this.txtTitle3.Top = 0.25F;
			this.txtTitle3.Width = 3.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Social Security #";
			this.Label1.Top = 0.1875F;
			this.Label1.Width = 1.25F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 2.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Eligible Gross";
			this.Label3.Top = 0.2083333F;
			this.Label3.Width = 1.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Gross Wages Pd.";
			this.Label4.Top = 0.2083333F;
			this.Label4.Width = 1.3125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.3333333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.4375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "State Tax Withheld";
			this.Label5.Top = 0.04166667F;
			this.Label5.Visible = false;
			this.Label5.Width = 0.9375F;
			// 
			// txtSocialSecurityNum
			// 
			this.txtSocialSecurityNum.Height = 0.1875F;
			this.txtSocialSecurityNum.Left = 0.0625F;
			this.txtSocialSecurityNum.Name = "txtSocialSecurityNum";
			this.txtSocialSecurityNum.Text = "Field1";
			this.txtSocialSecurityNum.Top = 0F;
			this.txtSocialSecurityNum.Width = 1.25F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.375F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.625F;
			// 
			// txtTotalGross
			// 
			this.txtTotalGross.Height = 0.2083333F;
			this.txtTotalGross.Left = 5.34375F;
			this.txtTotalGross.Name = "txtTotalGross";
			this.txtTotalGross.Style = "text-align: right";
			this.txtTotalGross.Text = "Field1";
			this.txtTotalGross.Top = 0F;
			this.txtTotalGross.Width = 0.9375F;
			// 
			// txtGrossWagesPaid
			// 
			this.txtGrossWagesPaid.Height = 0.2083333F;
			this.txtGrossWagesPaid.Left = 3.96875F;
			this.txtGrossWagesPaid.Name = "txtGrossWagesPaid";
			this.txtGrossWagesPaid.Style = "text-align: right";
			this.txtGrossWagesPaid.Text = "Field1";
			this.txtGrossWagesPaid.Top = 0F;
			this.txtGrossWagesPaid.Width = 1.125F;
			// 
			// txtStateTaxWithheld
			// 
			this.txtStateTaxWithheld.Height = 0.2083333F;
			this.txtStateTaxWithheld.Left = 6.34375F;
			this.txtStateTaxWithheld.Name = "txtStateTaxWithheld";
			this.txtStateTaxWithheld.Style = "text-align: right";
			this.txtStateTaxWithheld.Text = "Field1";
			this.txtStateTaxWithheld.Top = 0F;
			this.txtStateTaxWithheld.Visible = false;
			this.txtStateTaxWithheld.Width = 1.0625F;
			// 
			// txtTitle2
			// 
			this.txtTitle2.Height = 0.19F;
			this.txtTitle2.Left = 1.9375F;
			this.txtTitle2.Name = "txtTitle2";
			this.txtTitle2.Style = "text-align: center";
			this.txtTitle2.Text = "Unemployment Exempt Employees Omitted";
			this.txtTitle2.Top = 0F;
			this.txtTitle2.Width = 3.5F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Total";
			this.Label6.Top = 0.0625F;
			this.Label6.Width = 0.5F;
			// 
			// txtTotalTotalGross
			// 
			this.txtTotalTotalGross.Height = 0.1666667F;
			this.txtTotalTotalGross.Left = 5.21875F;
			this.txtTotalTotalGross.Name = "txtTotalTotalGross";
			this.txtTotalTotalGross.Style = "text-align: right";
			this.txtTotalTotalGross.Text = "Field1";
			this.txtTotalTotalGross.Top = 0.04166667F;
			this.txtTotalTotalGross.Width = 1.0625F;
			// 
			// txtTotaGrossWagesPd
			// 
			this.txtTotaGrossWagesPd.Height = 0.1666667F;
			this.txtTotaGrossWagesPd.Left = 3.96875F;
			this.txtTotaGrossWagesPd.Name = "txtTotaGrossWagesPd";
			this.txtTotaGrossWagesPd.Style = "text-align: right";
			this.txtTotaGrossWagesPd.Text = "Field2";
			this.txtTotaGrossWagesPd.Top = 0.04166667F;
			this.txtTotaGrossWagesPd.Width = 1.125F;
			// 
			// txtTotalWithheld
			// 
			this.txtTotalWithheld.Height = 0.1666667F;
			this.txtTotalWithheld.Left = 6.34375F;
			this.txtTotalWithheld.Name = "txtTotalWithheld";
			this.txtTotalWithheld.Style = "text-align: right";
			this.txtTotalWithheld.Text = "Field3";
			this.txtTotalWithheld.Top = 0.04166667F;
			this.txtTotalWithheld.Visible = false;
			this.txtTotalWithheld.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.4375F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.4375F;
			this.Line1.Width = 6.5625F;
			this.Line1.X1 = 0.4375F;
			this.Line1.X2 = 7F;
			this.Line1.Y1 = 0.4375F;
			this.Line1.Y2 = 0.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: left";
			this.Label7.Text = "Type of Tax = C-1";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 1.375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.3125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: left";
			this.Label8.Text = "Category";
			this.Label8.Top = 0.84375F;
			this.Label8.Width = 0.9375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Amount";
			this.Label9.Top = 0.84375F;
			this.Label9.Width = 0.9375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.3125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "text-align: left";
			this.Label10.Text = "Total QTD Payroll";
			this.Label10.Top = 1.09375F;
			this.Label10.Width = 1.3125F;
			// 
			// lblExcess
			// 
			this.lblExcess.Height = 0.1875F;
			this.lblExcess.HyperLink = null;
			this.lblExcess.Left = 2.3125F;
			this.lblExcess.Name = "lblExcess";
			this.lblExcess.Style = "text-align: left";
			this.lblExcess.Text = "Excess Over ";
			this.lblExcess.Top = 1.28125F;
			this.lblExcess.Width = 1.3125F;
			// 
			// txtTotalQTDPayroll
			// 
			this.txtTotalQTDPayroll.Height = 0.1875F;
			this.txtTotalQTDPayroll.Left = 3.9375F;
			this.txtTotalQTDPayroll.Name = "txtTotalQTDPayroll";
			this.txtTotalQTDPayroll.Style = "text-align: right";
			this.txtTotalQTDPayroll.Text = "Field1";
			this.txtTotalQTDPayroll.Top = 1.09375F;
			this.txtTotalQTDPayroll.Width = 1.125F;
			// 
			// txtExcessOver7000
			// 
			this.txtExcessOver7000.Height = 0.1875F;
			this.txtExcessOver7000.Left = 3.9375F;
			this.txtExcessOver7000.Name = "txtExcessOver7000";
			this.txtExcessOver7000.Style = "text-align: right";
			this.txtExcessOver7000.Text = "Field2";
			this.txtExcessOver7000.Top = 1.28125F;
			this.txtExcessOver7000.Width = 1.125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.5F;
			this.Line2.LineWeight = 2F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.625F;
			this.Line2.Width = 4.5F;
			this.Line2.X1 = 1.5F;
			this.Line2.X2 = 6F;
			this.Line2.Y1 = 1.625F;
			this.Line2.Y2 = 1.625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.3125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: left";
			this.Label12.Text = "Taxable";
			this.Label12.Top = 1.6875F;
			this.Label12.Width = 0.9375F;
			// 
			// txtTaxable
			// 
			this.txtTaxable.Height = 0.1875F;
			this.txtTaxable.Left = 3.9375F;
			this.txtTaxable.Name = "txtTaxable";
			this.txtTaxable.Style = "text-align: right";
			this.txtTaxable.Text = "Field1";
			this.txtTaxable.Top = 1.6875F;
			this.txtTaxable.Width = 1.125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.3125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: left";
			this.Label13.Text = "Rate (%)";
			this.Label13.Top = 1.9375F;
			this.Label13.Width = 0.6875F;
			// 
			// txtRate
			// 
			this.txtRate.Height = 0.1875F;
			this.txtRate.Left = 3.9375F;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Text = "Field1";
			this.txtRate.Top = 1.9375F;
			this.txtRate.Width = 1.125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.3125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: left";
			this.Label14.Text = "Due";
			this.Label14.Top = 2.28125F;
			this.Label14.Width = 0.4375F;
			// 
			// txtDue
			// 
			this.txtDue.Height = 0.1875F;
			this.txtDue.Left = 3.9375F;
			this.txtDue.Name = "txtDue";
			this.txtDue.Style = "text-align: right";
			this.txtDue.Text = "Field1";
			this.txtDue.Top = 2.28125F;
			this.txtDue.Width = 1.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.5F;
			this.Line3.LineWeight = 2F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.21875F;
			this.Line3.Width = 4.5F;
			this.Line3.X1 = 1.5F;
			this.Line3.X2 = 6F;
			this.Line3.Y1 = 2.21875F;
			this.Line3.Y2 = 2.21875F;
			// 
			// rptStateUnemploymentC1
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSocialSecurityNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossWagesPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxWithheld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotaGrossWagesPd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWithheld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTDPayroll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcessOver7000)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSocialSecurityNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossWagesPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotaGrossWagesPd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalQTDPayroll;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcessOver7000;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
