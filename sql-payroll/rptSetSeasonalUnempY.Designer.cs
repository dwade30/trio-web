﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSetSeasonalUnempY.
	/// </summary>
	partial class rptSetSeasonalUnempY
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSetSeasonalUnempY));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.Label2,
				this.Line1,
				this.txtTime,
				this.lblPage
			});
			this.PageHeader.Height = 1.09375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.2F;
			this.txtCaption.Left = 0.06666667F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.06666667F;
			this.txtCaption.Width = 7.3F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.366667F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.03333334F;
			this.txtDate.Width = 1.066667F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label2.Text = "Employee Name";
			this.Label2.Top = 0.8125F;
			this.Label2.Width = 1.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.433333F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.433333F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtTime.Text = "MuniName";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 2F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.2F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.366667F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.2F;
			this.lblPage.Width = 1.066667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.2F;
			this.txtName.Left = 0.06666667F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 5.6F;
			// 
			// rptSetSeasonalUnempY
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.4861111F;
			this.PageSettings.Margins.Right = 0.4861111F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
