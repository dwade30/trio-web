//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cPayrollDB
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "Payroll");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
			return GetVersion;
		}

		public void SetVersion(cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "Payroll");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;
				// default to 1.0.0.0
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddPeriodStartFields())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 1;
				if (tVer.IsNewer(cVer))
				{
					if (AddLogoOnNonNegotiableOnly())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 2;
				if (tVer.IsNewer(cVer))
				{
					if (AddACATables())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateACATables())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddACAFileFormatTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 4;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddMoreACATables())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 5;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdatePayFrequenciesTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 6;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (NewACATableUpdates())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 7;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateTblCodes())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 8;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (FCConvert.ToBoolean(AddPayRateToCustomChecks()))
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 9;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (FCConvert.ToBoolean(ChangeWorkCompToText()))
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 10;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddStateStandardDeductions())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 11;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddCheckDetailStats())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 12;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateStateStandardDeductions())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 13;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (Add2020W4NewFieldsAndFederalTaxTables())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 14;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (ConvertPayStatusesFromSQL())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 15;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddNewSSNPermission())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 16;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
                    if (AddNewBankPermission())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 17;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (RemoveLocalEICFromStandardPayTotals())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 18;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
                    if (RemoveBankAccountsFromAudit())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 19;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
                    if (RemoveSSNFromAuditHistory())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 20;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (AddMovedToClientSettingsFieldToReportTitlesTable())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 21;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (RemoveEICFromtblPayStatuses())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 22;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (RemoveLocalTaxWithheldFromtblPayrollAccounts())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 23;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (RemoveLocalTaxFromtblCategories())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 24;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (RemoveLocalTaxFromtblTaxStatusCodes())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 25;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (CorrectAuditTablePayDateFormat())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 26;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
                    if (CheckAndW2Formats())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 27;
                tVer.Build = 0;
                if (tVer.IsNewer((cVer)))
                {
	                if (AddNewFieldsToFed941())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 28;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCategoryTags())
                    {
						nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
							SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 29;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddThirdPartySickPayToW2EditTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 30;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddZipMonth())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 31;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddUPAFRateToLimits())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

				CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
			return CheckVersion;
		}

        private bool AddUPAFRateToLimits()
        {
            try
            {
                clsDRWrapper rsSave = new clsDRWrapper();
                string strSQL = "";
                if (!FieldExists("tblStandardLimits", "UPAFRate", "Payroll"))
                {
                    strSQL = "alter table tblStandardLimits Add UPAFRate [Float] NULL";
                    rsSave.Execute(strSQL, "Payroll");
                    strSQL = "update tblStandardLimits set UPAFRate = .13";
                    rsSave.Execute(strSQL, "Payroll");
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

		private bool AddNewFieldsToFed941()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			string strsql = "";

			try
			{
				if (!FieldExists("tblFed941", "QualifiedSickLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedSickLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll"); 
				}

				if (!FieldExists("tblFed941", "QualifiedFamilyLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedFamilyLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "NonRefundableCreditforLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add NonRefundableCreditforLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "NonRefundableEmplRetentionCredit", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add NonRefundableEmplRetentionCredit float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "DeferredEmployerSocSecTax", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add DeferredEmployerSocSecTax float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "RefundableCreditforLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add RefundableCreditforLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "RefundableEmplRetentionCredit", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add RefundableEmplRetentionCredit float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "TotalAdvancesFromForm7200", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add TotalAdvancesFromForm7200 float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedExpensesAllocableToSickLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedExpensesAllocableToSickLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedExpensesAllocableToFamilyLeaveWages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedExpensesAllocableToFamilyLeaveWages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedWagesForEmplRetentionCredit", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedWagesForEmplRetentionCredit float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedExpensesAllocableToLine21Wages", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedExpensesAllocableToLine21Wages float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "CreditFromForm5884C", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add CreditFromForm5884C float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedWagesPaidForEmplRetentionMar2020", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedWagesPaidForEmplRetentionMar2020 float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				if (!FieldExists("tblFed941", "QualifiedExpensesAllocableToWagesPaidForEmplRetentionMar2020", "Payroll"))
				{
					strsql = "Alter Table tblFed941 Add QualifiedExpensesAllocableToWagesPaidForEmplRetentionMar2020 float null";
					rsUpdate.Execute(strsql, "Payroll");
				}

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private bool CheckAndW2Formats()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("Select * from tblDefaultInformation", "Payroll");
                if (!rsSave.EndOfFile())
                {
                    var checkType = rsSave.Get_Fields_Int32("CheckType");
                    var w2Type = rsSave.Get_Fields_Int32("W2CheckType");
                    if (checkType == 0 || (w2Type > 0 && w2Type < 4))
                    {
                        rsSave.Edit();
                        if (checkType == 0)
                        {
                            rsSave.Set_Fields("CheckType",1); //standard laser
                        }

                        if (w2Type > 0 && w2Type < 4)
                        {
                            rsSave.Set_Fields("W2CheckType",0); // laser w2

                        }
                        rsSave.Update();
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

		private bool CorrectAuditTablePayDateFormat()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();

				rsTemp.Execute("UPDATE AuditChanges SET UserField2 = FORMAT(convert(datetime, UserField2), 'M/d/yyyy') WHERE isdate(userfield2) = 1", "Payroll");
				rsTemp.Execute("UPDATE AuditChangesArchive SET UserField2 = FORMAT(convert(datetime, UserField2), 'M/d/yyyy') WHERE isdate(userfield2) = 1", "Payroll");

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool RemoveLocalTaxFromtblTaxStatusCodes()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();
				
				UpdateTaxStatusCode("4", "R");
				UpdateTaxStatusCode("5", "8");
				UpdateTaxStatusCode("6", "7");
				UpdateTaxStatusCode("9", "R");
				UpdateTaxStatusCode("D", "J");
				UpdateTaxStatusCode("E", "B");
				UpdateTaxStatusCode("F", "G");
				UpdateTaxStatusCode("I", "3");
				UpdateTaxStatusCode("K", "L");
				UpdateTaxStatusCode("M", "C");
				UpdateTaxStatusCode("S", "R");
				UpdateTaxStatusCode("V", "3");
				
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/ST/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/FICA/MED/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/FICA/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/MED/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/MED/ST/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FED/ST/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FICA/MED/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FICA/MED/ST/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'FICA/ST/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'MED/LOC'", "Payroll");
				rsTemp.Execute("DELETE FROM tblTaxStatusCodes WHERE Description = 'ST/LOC'", "Payroll");

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private void UpdateTaxStatusCode(string originalCode, string newCode)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			int originalId;
			int newId;

			rsTemp.OpenRecordset("SELECT * FROM tblTaxStatusCodes WHERE TaxStatusCode = '" + originalCode + "'", "Payroll");
			if (!rsTemp.BeginningOfFile() && !rsTemp.EndOfFile())
			{
				originalId = rsTemp.Get_Fields_Int32("ID");

				rsTemp.OpenRecordset("SELECT * FROM tblTaxStatusCodes WHERE TaxStatusCode = '" + newCode + "'", "Payroll");
				if (!rsTemp.BeginningOfFile() && !rsTemp.EndOfFile())
				{
					newId = rsTemp.Get_Fields_Int32("ID");

					rsTemp.Execute("UPDATE tblDeductionSetup SET TaxStatusCode = " + newId + " WHERE TaxStatusCode = " + originalId, "Payroll");
					rsTemp.Execute("UPDATE tblPayCategories SET TaxStatus = " + newId + " WHERE TaxStatus = " + originalId, "Payroll");
				}
			}
		}
		private bool RemoveLocalTaxFromtblCategories()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();

				rsTemp.Execute("DELETE FROM tblCategories WHERE Description = 'Local Tax'", "Payroll");

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool RemoveLocalTaxWithheldFromtblPayrollAccounts()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();

				rsTemp.Execute("DELETE FROM tblPayrollAccounts WHERE Code = 'LT'", "Payroll");

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool RemoveEICFromtblPayStatuses()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();

				rsTemp.Execute("DELETE FROM tblPayStatuses WHERE Type = 'EIC'", "Payroll");

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool AddMovedToClientSettingsFieldToReportTitlesTable()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			string strsql = "";

			try
			{
				if (!FieldExists("PayrollPermissions", "MovedToClientSettings", "Payroll"))
				{
					strsql = "Alter Table PayrollPermissions Add MovedToClientSettings bit null";
					rsUpdate.Execute(strsql, "Payroll");
					rsUpdate.Execute("Update PayrollPermissions set MovedToClientSettings = 0", "Payroll");
				}

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private bool RemoveLocalEICFromStandardPayTotals()
		{
			try
			{
				clsDRWrapper rsTemp = new clsDRWrapper();
				
				rsTemp.Execute("DELETE FROM tblStandardPayTotals WHERE Description = 'Local Tax'", "Payroll");
				rsTemp.Execute("DELETE FROM tblStandardPayTotals WHERE Description = 'Local Wage'", "Payroll");
				rsTemp.Execute("DELETE FROM tblStandardPayTotals WHERE Description = 'EIC'", "Payroll");
				
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			bool FieldExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					FieldExists = true;
				}
				return FieldExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return FieldExists;
		}

		private bool TableExists(string strTable, string strDB)
		{
			bool TableExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					TableExists = true;
				}
				return TableExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return TableExists;
		}

		private bool AddPeriodStartFields()
		{
			bool AddPeriodStartFields = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PeriodStartDate' AND TABLE_NAME = 'tblPayrollProcessSetup'", "Payroll");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("Alter Table.[dbo].[tblPayrollProcessSetup] Add [PeriodStartDate] datetime null", "Payroll");
				}
				rsTemp.OpenRecordset("Select Column_Name from information_Schema.Columns where column_Name = 'PeriodStartDate' and Table_Name = 'tblPayrollSteps'", "Payroll");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("Alter Table.[dbo].[tblPayrollSteps] Add [PeriodStartDate] datetime null", "Payroll");
				}
				rsTemp.OpenRecordset("Select Column_Name from information_Schema.COlumns where column_Name = 'ShowPayPeriodOnStub' and Table_Name = 'tblDefaultInformation'", "Payroll");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("Alter Table.[dbo].[tblDefaultInformation] Add [ShowPayPeriodOnStub] bit null", "Payroll");
				}
				AddPeriodStartFields = true;
				return AddPeriodStartFields;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddPeriodStartFields;
		}

		private bool AddLogoOnNonNegotiableOnly()
		{
			bool AddLogoOnNonNegotiableOnly = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("select column_name from information_schema.columns where column_name = 'LogoOnNonNegotiableOnly' and table_name = 'tblCheckFormat'", "Payroll");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("Alter table.[dbo].[tblCheckFormat] Add [LogoOnNonNegotiableOnly] bit null", "Payroll");
				}
				AddLogoOnNonNegotiableOnly = true;
				return AddLogoOnNonNegotiableOnly;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddLogoOnNonNegotiableOnly;
		}

		private bool AddACATables()
		{
			bool AddACATables = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				strSQL = "If not exists(select * from information_schema.tables where table_name = N'ACAEmployeeSetup') " + "\r\n";
				strSQL += "CREATE TABLE [dbo].[ACAEmployeeSetup](" + "[ID] [int] IDENTITY(1,1) NOT NULL," + "[ReportYear] [int] NULL," + "[EmployeeID] [int] NULL," + "[EmployeeNumber] [nvarchar](255) NULL," + "[CoverageDeclined] [bit] NULL,";
				int x;
				for (x = 1; x <= 12; x++)
				{
					strSQL += "[CoverageRequiredMonth" + FCConvert.ToString(x) + "] [nvarchar](50) NULL,";
				}
				// x
				for (x = 1; x <= 12; x++)
				{
					strSQL += "[EmployeeLowestPremiumMonth" + FCConvert.ToString(x) + "] [Decimal] (18,2) NULL,";
				}
				// x
				for (x = 1; x <= 12; x++)
				{
					strSQL += "[SafeHarborCodeMonth" + FCConvert.ToString(x) + "] [nvarchar](50) NULL,";
				}
				// x
				strSQL += " Constraint [PK_ACAEmployeeSetup] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				strSQL = "If not exists(select * from information_schema.tables where table_name = N'ACAEmployeeDependents') " + "\r\n";
				strSQL += "Create Table [dbo].[ACAEmployeeDependents] (";
				strSQL += "[ID] [int] IDENTITY(1,1) NOT NULL,";
				strSQL += "[ACAEmployeeSetupID] [int] NULL,";
				strSQL += "[DateOfBirth] [nvarchar](50) NULL,";
				strSQL += "[TerminationDate] [nvarchar] (50) NULL,";
				strSQL += "[FirstName][nvarchar] (255) NULL,";
				strSQL += "[MiddleName][nvarchar] (255) NULL,";
				strSQL += "[LastName][nvarchar] (255) NULL,";
				strSQL += "[Suffix][nvarchar](50) NULL,";
				strSQL += "[SSN] [nvarchar](50) NULL,";
				for (x = 1; x <= 12; x++)
				{
					strSQL += "[CoveredMonth" + FCConvert.ToString(x) + "] [bit] NULL,";
				}
				// x
				strSQL += " Constraint [PK_ACAEmployeeDependents] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				AddACATables = true;
				return AddACATables;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddACATables;
		}

		private bool UpdateACATables()
		{
			bool UpdateACATables = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				if (!FieldExists("ACAEmployeeSetup", "OriginOfPolicy", "Payroll"))
				{
					strSQL = "Alter table.[dbo].[ACAEmployeeSetup] Add [OriginOfPolicy] nvarchar(50) null";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "If not exists( select * from information_schema.tables where table_name = N'CoverageProviders') " + "\r\n";
				strSQL += "Create Table [dbo].[CoverageProviders] (";
				strSQL += "[ID] [int] IDENTITY(1,1) NOT NULL,";
				strSQL += "[PlanName] [nvarchar](255) NULL,";
				strSQL += "[PlanStartMonth] [int] NULL,";
				strSQL += "[Name] [nvarchar] (255) NULL,";
				strSQL += "[Address] [nvarchar] (255) NULL,";
				strSQL += "[City] [nvarchar] (255) NULL,";
				strSQL += "[State] [nvarchar] (255) NULL,";
				strSQL += "[PostalCode] [nvarchar] (255) NULL,";
				strSQL += "[EIN] [nvarchar] (255) NULL,";
				strSQL += "[Telephone] [nvarchar] (255) NULL,";
				strSQL += "[Use1095B] [bit] NULL,";
				strSQL += " Constraint [PK_CoverageProviders] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				UpdateACATables = true;
				return UpdateACATables;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateACATables;
		}

		public bool AddACAFileFormatTable()
		{
			bool AddACAFileFormatTable = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				rsSave.ClearErrors();
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACAFileFormat') " + "\r\n";
				strSQL += "Create Table [dbo].[ACAFileFormat] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[FormatName] [nvarchar] (255) NULL,";
				strSQL += "[FirstLineUnreadable] [bit] NULL,";
				strSQL += "[SeparatorChar] [nvarchar] (50) NULL,";
				strSQL += "[EmployeeIdentifierType] int NULL,";
				strSQL += " Constraint [PK_ACAFileFormat] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				if (rsSave.ErrorNumber != 0)
				{
					return AddACAFileFormatTable;
				}
				rsSave.ClearErrors();
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACAFileFormatColumns') " + "\r\n";
				strSQL += "Create Table [dbo].[ACAFileFormatColumns] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[FileFormatID] [int] NULL,";
				strSQL += "[ColumnName] [nvarchar] (255) NULL,";
				strSQL += "[ColumnWidth] [int] NULL,";
				strSQL += "[ColumnOrder] [int] NULL,";
				strSQL += " Constraint [PK_ACAFileFormatColumns] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				if (rsSave.ErrorNumber != 0)
				{
					return AddACAFileFormatTable;
				}
				AddACAFileFormatTable = true;
				return AddACAFileFormatTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddACAFileFormatTable;
		}

		private bool AddCloseoutByDeptOption()
		{
			bool AddCloseoutByDeptOption = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsTemp = new clsDRWrapper();
				// kk 120412 trout-348  Add Close Out by Department option
				rsTemp.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'DeptDiv' AND TABLE_NAME = 'Operators'", "SystemSettings");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("ALTER TABLE.[dbo].[Operators] ADD [DeptDiv] nvarchar(50) NULL", "SystemSettings");
				}
				AddCloseoutByDeptOption = true;
				return AddCloseoutByDeptOption;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddCloseoutByDeptOption;
		}

		private bool UpdateStats()
		{
			bool UpdateStats = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsExec = new clsDRWrapper();
				string strSQL;
				strSQL = "Exec sp_updatestats";
				rsExec.Execute(strSQL, "Payroll");
				UpdateStats = true;
				return UpdateStats;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateStats;
		}

		public bool DoMaintenance()
		{
			bool DoMaintenance = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn;
				boolReturn = true;
				boolReturn = boolReturn && UpdateStats();
				DoMaintenance = boolReturn;
				return DoMaintenance;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return DoMaintenance;
		}

		private bool AddMoreACATables()
		{
			bool AddMoreACATables = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				if (!FieldExists("ACAEmployeeSetup", "CoverageProviderID", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACAEmployeeSetup] ADD [CoverageProviderID] int NULL", "Payroll");
				}
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACA1094B') " + "\r\n";
				strSQL += "Create Table [dbo].[ACA1094B] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[YearCovered] [int] NULL,";
				strSQL += "[FilerName] [nvarchar] (255) NULL,";
				strSQL += "[EIN] [nvarchar] (50) NULL,";
				strSQL += "[ContactName] [nvarchar] (255) NULL,";
				strSQL += "[ContactTelephone] [nvarchar] (50) NULL,";
				strSQL += "[Address] [nvarchar] (255) NULL,";
				strSQL += "[City] [nvarchar] (255) NULL,";
				strSQL += "[State] [nvarchar] (50) NULL,";
				strSQL += "[PostalCode] [nvarchar] (50) NULL,";
				strSQL += "[NumberOfSubmissions] [int] NULL,";
				strSQL += " Constraint [PK_ACA1094B] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACA1094C') " + "\r\n";
				strSQL += "Create Table [dbo].[ACA1094C] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[YearCovered] [int] NULL,";
				strSQL += "[IsCorrected] [bit] NULL,";
				strSQL += "[EmployerName] [nvarchar] (255) NULL,";
				strSQL += "[EmployerEIN] [nvarchar] (50) NULL,";
				strSQL += "[EmployerAddress] [nvarchar] (255) NULL,";
				strSQL += "[EmployerCity] [nvarchar] (255) NULL,";
				strSQL += "[EmployerState] [nvarchar] (50) NULL,";
				strSQL += "[EmployerPostalCode] [nvarchar] (50) NULL,";
				strSQL += "[EmployerContact] [nvarchar] (255) NULL,";
				strSQL += "[ContactTelephone] [nvarchar] (50) NULL,";
				strSQL += "[DesignatedEntityName] [nvarchar] (255) NULL,";
				strSQL += "[DesignatedEntityEIN] [nvarchar] (50) NULL,";
				strSQL += "[Entityaddress] [nvarchar] (255) NULL,";
				strSQL += "[EntityCity] [nvarchar] (255) NULL,";
				strSQL += "[EntityState] [nvarchar] (50) NULL,";
				strSQL += "[EntityPostalCode] [nvarchar] (50) NULL,";
				strSQL += "[EntityContact] [nvarchar] (255) NULL,";
				strSQL += "[EntityContactTelephone] [nvarchar] (50) NULL,";
				strSQL += "[TotalForms1095C] [int] NULL,";
				strSQL += "[IsAuthoritative] [bit] NULL,";
				strSQL += "[TotalMember1095C] [int] NULL,";
				strSQL += "[IsInAggregatedGroup] [bit] NULL,";
				strSQL += "[QualifyingOfferMethod] [bit] NULL,";
				strSQL += "[QualifyingOfferTransitionRelief] [bit] NULL,";
				strSQL += "[Section4980HRelief] [bit] NULL,";
				strSQL += "[NinetyEightPercentOfferMethod] [bit] NULL,";
				strSQL += " Constraint [PK_ACA1094C] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACA1094CMonthlyDetail') " + "\r\n";
				strSQL += "Create Table [dbo].[ACA1094CMonthlyDetail] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[ParentID] [int] NULL,";
				strSQL += "[CoveredMonth] [int] NULL,";
				strSQL += "[MinimumEssentialCoverageOffer] [bit] NULL,";
				strSQL += "[FullTimeEmployeeCount] [int] NULL,";
				strSQL += "[TotalEmployeeCount] [int] NULL,";
				strSQL += "[AggregatedGroup] [bit] NULL,";
				strSQL += "[Section4980HTransitionRelief] [nvarchar] (255) NULL,";
				strSQL += " Constraint [PK_ACA1094CMonthlyDetail] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'AggregatedALEMembers') " + "\r\n";
				strSQL += "Create Table [dbo].[AggregatedALEMembers] (";
				strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL += "[YearCovered] [int] NULL,";
				strSQL += "[MemberName] [nvarchar] (255) NULL,";
				strSQL += "[EIN] [nvarchar] (50) NULL,";
				strSQL += " Constraint [PK_AggregatedALEMembers] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "Payroll", false);
				AddMoreACATables = true;
				return AddMoreACATables;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddMoreACATables;
		}

		private bool UpdatePayFrequenciesTable()
		{
			bool UpdatePayFrequenciesTable = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strTemp = "";
				rsSave.OpenRecordset("select * from tblfrequencycodes", "Payroll");
				while (!rsSave.EndOfFile())
				{
					rsSave.Edit();
					strTemp = FCConvert.ToString(rsSave.Get_Fields("Description"));
					strTemp = strTemp.Replace(" (when coded)", "");
					strTemp = strTemp.Replace(" (When Coded)", "");
					rsSave.Set_Fields("Description", strTemp);
					rsSave.Update();
					rsSave.MoveNext();
				}
				UpdatePayFrequenciesTable = true;
				return UpdatePayFrequenciesTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdatePayFrequenciesTable;
		}

		private bool NewACATableUpdates()
		{
			bool NewACATableUpdates = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				if (!FieldExists("aca1094B", "ContactMiddleName", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094B] ADD [ContactMiddleName] nvarchar (255) NULL", "Payroll");
				}
				if (!FieldExists("ACA1094B", "ContactLastName", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094B] ADD [ContactLastName] nvarchar (255) NULL", "Payroll");
				}
				if (!FieldExists("ACA1094B", "ContactSuffix", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094B] ADD [ContactSuffix] nvarchar (255) NULL", "Payroll");
				}
				if (!FieldExists("ACA1094C", "EmployerContactMiddle", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094C] ADD [EmployerContactMiddle] nvarchar (255) NULL", "Payroll");
				}
				if (!FieldExists("ACA1094C", "EmployerContactLast", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094C] ADD [EmployerContactLast] nvarchar (255) NULL", "Payroll");
				}
				if (!FieldExists("ACA1094C", "EmployerContactSuffix", "Payroll"))
				{
					rsSave.Execute("ALTER Table.[dbo].[ACA1094C] ADD [EmployerContactSuffix] nvarchar (255) NULL", "Payroll");
				}
				if (!TableExists("ACATransmissions", "Payroll"))
				{
					strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACATransmissions') ";
					strSQL += "Create Table [dbo].[ACATransmissions] (";
					strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
					strSQL += "[TaxYear] [int] NULL,";
					strSQL += "[TransmissionType] [nvarchar] (255) NULL,";
					strSQL += "[ReceiptID] [nvarchar] (255) NULL,";
					strSQL += "[FileName] [nvarchar] (255) NULL,";
					strSQL += "[TransmissionID] [nvarchar] (255) NULL,";
					strSQL += "[TestFileCode] [nvarchar] (50) NULL,";
					strSQL += " Constraint [PK_ACATransmissions] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
					rsSave.Execute(strSQL, "Payroll");
				}
				if (!TableExists("ACATransmissionDetails", "Payroll"))
				{
					strSQL = "if not exists (select * from information_schema.tables where table_name = N'ACATransmissionDetails') ";
					strSQL += "Create Table [dbo].[ACATransmissionDetails] (";
					strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
					strSQL += "[ACATransmissionID] [int] NULL,";
					strSQL += "[RecordID] [int] NULL,";
					strSQL += "[ACASetupID] [int] NULL,";
					strSQL += " Constraint [PK_ACATransmissionDetails] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
					rsSave.Execute(strSQL, "Payroll");
				}
				NewACATableUpdates = true;
				return NewACATableUpdates;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return NewACATableUpdates;
		}

		private bool UpdateTblCodes()
		{
			bool UpdateTblCodes = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("update tblcodes set cleared = '2' where ltrim(rtrim([cleared])) = '4'", "Payroll");
				UpdateTblCodes = true;
				return UpdateTblCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateTblCodes;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object AddPayRateToCustomChecks()
		{
			object AddPayRateToCustomChecks = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!FieldExists("CustomChecks", "ShowIndividualPayRates", "Payroll"))
				{
					rsSave.Execute("alter Table.[dbo].[CustomChecks] Add [ShowIndividualPayRates] bit NULL", "Payroll");
				}
				AddPayRateToCustomChecks = true;
				return AddPayRateToCustomChecks;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddPayRateToCustomChecks;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object ChangeWorkCompToText()
		{
			object ChangeWorkCompToText = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				strSQL = "select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS IC where TABLE_NAME = 'tblemployeemaster' and COLUMN_NAME = 'WorkCompCode'";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (fecherFoundation.Strings.LCase(FCConvert.ToString(rsSave.Get_Fields("Data_Type"))) == "int")
				{
					strSQL = "alter table tblemployeemaster alter column WorkCompCode nVarChar(50) NULL";
					rsSave.Execute(strSQL, "Payroll");
				}
				ChangeWorkCompToText = true;
				return ChangeWorkCompToText;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return ChangeWorkCompToText;
		}

		private bool AddStateStandardDeductions()
		{
			bool AddStateStandardDeductions = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				if (!FieldExists("tblStandardLimits", "StateSingleDeduction", "Payroll"))
				{
					strSQL = "alter table tblStandardLimits Add StateSingleDeduction [Decimal] (18,2) NULL";
					rsSave.Execute(strSQL, "Payroll");
					strSQL = "update tblStandardLimits set StateSingleDeduction = 8750";
					rsSave.Execute(strSQL, "Payroll");
				}
				if (!FieldExists("tblStandardLimits", "StateMarriedDeduction", "Payroll"))
				{
					strSQL = "alter table tblStandardLimits Add StateMarriedDeduction [Decimal] (18,2) NULL";
					rsSave.Execute(strSQL, "Payroll");
					strSQL = "update tblStandardLimits set StateMarriedDeduction = 20350";
					rsSave.Execute(strSQL, "Payroll");
				}
				AddStateStandardDeductions = true;
				return AddStateStandardDeductions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddStateStandardDeductions;
		}

		private bool AddCheckDetailStats()
		{
			bool AddCheckDetailStats = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				strSQL = "select * from sys.indexes where object_id = object_id('tblCheckDetail') and name = '_dta_index_tblCheckDetail_14_1835153583__K2_K6_K61_K1_5_11_12_14_15_21_24_34_35_37_39_44_57_58_59_60_62_63_70_75_80_105'";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE NONCLUSTERED INDEX [_dta_index_tblCheckDetail_14_1835153583__K2_K6_K61_K1_5_11_12_14_15_21_24_34_35_37_39_44_57_58_59_60_62_63_70_75_80_105] ON [dbo].[tblCheckDetail]";
					strSQL += "([EmployeeNumber] ASC,[PayDate] ASC,[TotalRecord] ASC,[ID] Asc)";
					strSQL += " INCLUDE (   [CheckNumber],[FederalTaxWH],[StateTaxWH],[FICATaxWH],[MedicareTaxWH],[GrossPay],[DistPayCategory],";
					strSQL += " [DistHours],[DistGrossPay],[DedDeductionNumber],[DedAmount],[MatchAmount],[MasterRecord],[DeductionRecord],";
					strSQL += "[MatchRecord],[BankRecord],[DistributionRecord],[TrustRecord],[PayRunID],[CheckVoid],[EmpDeductionID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_57] ON [dbo].[tblCheckDetail]([CheckVoid], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_70] ON [dbo].[tblCheckDetail]([CheckNumber], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_60_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_60_1] ON [dbo].[tblCheckDetail]([BankRecord], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_47_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_47_60] ON [dbo].[tblCheckDetail]([DDBankNumber], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_1] ON [dbo].[tblCheckDetail]([CheckNumber], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_60_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_60_80] ON [dbo].[tblCheckDetail]([BankRecord], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_2] ON [dbo].[tblCheckDetail]([CheckVoid], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_64_63' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_64_63] ON [dbo].[tblCheckDetail]([TrustRecipientID], [TrustRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_59' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_59] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [MatchRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_61] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_47_48' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_47_48] ON [dbo].[tblCheckDetail]([ID], [DDBankNumber], [DDAccountNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_63' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_63] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [TrustRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_2_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_2_80] ON [dbo].[tblCheckDetail]([PayRunID], [EmployeeNumber], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_37_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_37_80] ON [dbo].[tblCheckDetail]([EmployeeNumber], [DedDeductionNumber], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_2_6' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_2_6] ON [dbo].[tblCheckDetail]([ID], [EmployeeNumber], [PayDate])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_60_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_60_80] ON [dbo].[tblCheckDetail]([ID], [BankRecord], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_57_2_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_57_2_61] ON [dbo].[tblCheckDetail]([MasterRecord], [EmployeeNumber], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_60_47' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_60_47] ON [dbo].[tblCheckDetail]([ID], [BankRecord], [DDBankNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_61_1_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_61_1_2] ON [dbo].[tblCheckDetail]([TotalRecord], [ID], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_63_64' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_63_64] ON [dbo].[tblCheckDetail]([ID], [TrustRecord], [TrustRecipientID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_2_57_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_2_57_60] ON [dbo].[tblCheckDetail]([ID], [EmployeeNumber], [MasterRecord], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_64_6_5_66' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_64_6_5_66] ON [dbo].[tblCheckDetail]([TrustRecipientID], [PayDate], [CheckNumber], [TrustCategoryID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_6_80_37' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_6_80_37] ON [dbo].[tblCheckDetail]([EmployeeNumber], [PayDate], [CheckVoid], [DedDeductionNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_24_62' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_24_62] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [DistPayCategory], [DistributionRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_47_48_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_47_48_2] ON [dbo].[tblCheckDetail]([CheckVoid], [DDBankNumber], [DDAccountNumber], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_1_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_1_60] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [ID], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_57_5_80_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_57_5_80_61] ON [dbo].[tblCheckDetail]([MasterRecord], [CheckNumber], [CheckVoid], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_64_6_70_63' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_64_6_70_63] ON [dbo].[tblCheckDetail]([TrustRecipientID], [PayDate], [PayRunID], [TrustRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_64_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_64_6_70] ON [dbo].[tblCheckDetail]([ID], [TrustRecipientID], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_2_37_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_2_37_80] ON [dbo].[tblCheckDetail]([PayRunID], [EmployeeNumber], [DedDeductionNumber], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_2_6_5' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_2_6_5] ON [dbo].[tblCheckDetail]([PayRunID], [EmployeeNumber], [PayDate], [CheckNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_6_1_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_6_1_61] ON [dbo].[tblCheckDetail]([EmployeeNumber], [PayDate], [ID], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_62' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_62] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [DistributionRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_47_6_70_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_47_6_70_60] ON [dbo].[tblCheckDetail]([DDBankNumber], [PayDate], [PayRunID], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_59_2_1_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_59_2_1_57] ON [dbo].[tblCheckDetail]([MatchRecord], [EmployeeNumber], [ID], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_57_6_70_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_57_6_70_1] ON [dbo].[tblCheckDetail]([MasterRecord], [PayDate], [PayRunID], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_47_48_2_6' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_47_48_2_6] ON [dbo].[tblCheckDetail]([CheckNumber], [DDBankNumber], [DDAccountNumber], [EmployeeNumber], [PayDate])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_66_1_64_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_66_1_64_6_70] ON [dbo].[tblCheckDetail]([TrustCategoryID], [ID], [TrustRecipientID], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_1_61_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_1_61_57] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [ID], [TotalRecord], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_60_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_60_80] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [BankRecord], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_66_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_66_1] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [TrustCategoryID], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_64_66_6_70_5' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_64_66_6_70_5] ON [dbo].[tblCheckDetail]([TrustRecipientID], [TrustCategoryID], [PayDate], [PayRunID], [CheckNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_60_63_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_60_63_1] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [BankRecord], [TrustRecord], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_1_63_64' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_1_63_64] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [ID], [TrustRecord], [TrustRecipientID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_57_2_6_70_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_57_2_6_70_61] ON [dbo].[tblCheckDetail]([MasterRecord], [EmployeeNumber], [PayDate], [PayRunID], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_6_70_1_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_6_70_1_60] ON [dbo].[tblCheckDetail]([CheckNumber], [PayDate], [PayRunID], [ID], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_37_57_105_58' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_37_57_105_58] ON [dbo].[tblCheckDetail]([EmployeeNumber], [DedDeductionNumber], [MasterRecord], [EmpDeductionID], [DeductionRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_2_80_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_2_80_6_70] ON [dbo].[tblCheckDetail]([ID], [EmployeeNumber], [CheckVoid], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_60_1_47' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_60_1_47] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [BankRecord], [ID], [DDBankNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_62_1_2_57_24' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_62_1_2_57_24] ON [dbo].[tblCheckDetail]([DistributionRecord], [ID], [EmployeeNumber], [MasterRecord], [DistPayCategory])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_57_70_5_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_57_70_5_80] ON [dbo].[tblCheckDetail]([ID], [MasterRecord], [PayRunID], [CheckNumber], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_47_48_2_1_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_47_48_2_1_6_70] ON [dbo].[tblCheckDetail]([DDBankNumber], [DDAccountNumber], [EmployeeNumber], [ID], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_37_1_80_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_37_1_80_6_70] ON [dbo].[tblCheckDetail]([EmployeeNumber], [DedDeductionNumber], [ID], [CheckVoid], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_2_57_60_63' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_2_57_60_63] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [EmployeeNumber], [MasterRecord], [BankRecord], [TrustRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_1_57_6_70_5' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_1_57_6_70_5] ON [dbo].[tblCheckDetail]([EmployeeNumber], [ID], [MasterRecord], [PayDate], [PayRunID], [CheckNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_58_2_37_1_57_105' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_58_2_37_1_57_105] ON [dbo].[tblCheckDetail]([DeductionRecord], [EmployeeNumber], [DedDeductionNumber], [ID], [MasterRecord], [EmpDeductionID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_6_70_5_64_66' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_6_70_5_64_66] ON [dbo].[tblCheckDetail]([ID], [PayDate], [PayRunID], [CheckNumber], [TrustRecipientID], [TrustCategoryID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_1_6_70_5_60' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_1_6_70_5_60] ON [dbo].[tblCheckDetail]([CheckVoid], [ID], [PayDate], [PayRunID], [CheckNumber], [BankRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_6_70_5_59_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_6_70_5_59_2] ON [dbo].[tblCheckDetail]([CheckVoid], [PayDate], [PayRunID], [CheckNumber], [MatchRecord], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_5_70_1_47_48' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_5_70_1_47_48] ON [dbo].[tblCheckDetail]([PayDate], [CheckNumber], [PayRunID], [ID], [DDBankNumber], [DDAccountNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_61_2_6_70_5_1' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_61_2_6_70_5_1] ON [dbo].[tblCheckDetail]([TotalRecord], [EmployeeNumber], [PayDate], [PayRunID], [CheckNumber], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_6_5_70_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_6_5_70_80] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [PayDate], [CheckNumber], [PayRunID], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_6_70_1_2_37' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_6_70_1_2_37] ON [dbo].[tblCheckDetail]([CheckNumber], [PayDate], [PayRunID], [ID], [EmployeeNumber], [DedDeductionNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_61_1_57_2_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_61_1_57_2_6_70] ON [dbo].[tblCheckDetail]([TotalRecord], [ID], [MasterRecord], [EmployeeNumber], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_80_61_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_80_61_2] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [CheckVoid], [TotalRecord], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_6_70_5_2_59_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_6_70_5_2_59_57] ON [dbo].[tblCheckDetail]([ID], [PayDate], [PayRunID], [CheckNumber], [EmployeeNumber], [MatchRecord], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_80_1_47_48_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_80_1_47_48_6_70] ON [dbo].[tblCheckDetail]([EmployeeNumber], [CheckVoid], [ID], [DDBankNumber], [DDAccountNumber], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_80_6_70_5_58_2_37' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_80_6_70_5_58_2_37] ON [dbo].[tblCheckDetail]([CheckVoid], [PayDate], [PayRunID], [CheckNumber], [DeductionRecord], [EmployeeNumber], [DedDeductionNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_1_80_6_70_5' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_1_80_6_70_5] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [ID], [CheckVoid], [PayDate], [PayRunID], [CheckNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_1_6_70_2_57_60_63' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_1_6_70_2_57_60_63] ON [dbo].[tblCheckDetail]([ID], [PayDate], [PayRunID], [EmployeeNumber], [MasterRecord], [BankRecord], [TrustRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_80_47_48_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_80_47_48_2] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [CheckVoid], [DDBankNumber], [DDAccountNumber], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_5_80_61_70_57_1_2' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_5_80_61_70_57_1_2] ON [dbo].[tblCheckDetail]([CheckNumber], [CheckVoid], [TotalRecord], [PayRunID], [MasterRecord], [ID], [EmployeeNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_5_80_61_1_57_6' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_5_80_61_1_57_6] ON [dbo].[tblCheckDetail]([PayRunID], [CheckNumber], [CheckVoid], [TotalRecord], [ID], [MasterRecord], [PayDate])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_5_6_70_59_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_5_6_70_59_80] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [CheckNumber], [PayDate], [PayRunID], [MatchRecord], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_24_6_5_70_62' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_24_6_5_70_62] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [DistPayCategory], [PayDate], [CheckNumber], [PayRunID], [DistributionRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_6_1_70_5_80_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_6_1_70_5_80_61] ON [dbo].[tblCheckDetail]([EmployeeNumber], [PayDate], [ID], [PayRunID], [CheckNumber], [CheckVoid], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_57_2_5_70_80_61_6' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_57_2_5_70_80_61_6] ON [dbo].[tblCheckDetail]([MasterRecord], [EmployeeNumber], [CheckNumber], [PayRunID], [CheckVoid], [TotalRecord], [PayDate])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_80_1_47_48_6_70' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_80_1_47_48_6_70] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [CheckVoid], [ID], [DDBankNumber], [DDAccountNumber], [PayDate], [PayRunID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_80_6_70_5_1_59_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_80_6_70_5_1_59_57] ON [dbo].[tblCheckDetail]([EmployeeNumber], [CheckVoid], [PayDate], [PayRunID], [CheckNumber], [ID], [MatchRecord], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_57_1_37_80_6_70_5' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_57_1_37_80_6_70_5] ON [dbo].[tblCheckDetail]([EmployeeNumber], [MasterRecord], [ID], [DedDeductionNumber], [CheckVoid], [PayDate], [PayRunID], [CheckNumber])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_80_57_1_6_70_5_61' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_80_57_1_6_70_5_61] ON [dbo].[tblCheckDetail]([EmployeeNumber], [CheckVoid], [MasterRecord], [ID], [PayDate], [PayRunID], [CheckNumber], [TotalRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_6_5_1_62_2_57_24' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_6_5_1_62_2_57_24] ON [dbo].[tblCheckDetail]([PayRunID], [PayDate], [CheckNumber], [ID], [DistributionRecord], [EmployeeNumber], [MasterRecord], [DistPayCategory])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_70_47_48_2_6_5_1_80_57' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_70_47_48_2_6_5_1_80_57] ON [dbo].[tblCheckDetail]([PayRunID], [DDBankNumber], [DDAccountNumber], [EmployeeNumber], [PayDate], [CheckNumber], [ID], [CheckVoid], [MasterRecord])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_37_57_105_5_6_70_58_80' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_37_57_105_5_6_70_58_80] ON [dbo].[tblCheckDetail]([EmployeeNumber], [DedDeductionNumber], [MasterRecord], [EmpDeductionID], [CheckNumber], [PayDate], [PayRunID], [DeductionRecord], [CheckVoid])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_6_70_5_1_2_37_58_57_105' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_6_70_5_1_2_37_58_57_105] ON [dbo].[tblCheckDetail]([PayDate], [PayRunID], [CheckNumber], [ID], [EmployeeNumber], [DedDeductionNumber], [DeductionRecord], [MasterRecord], [EmpDeductionID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1835153583_2_37_6_80_70_5_1_58_57_105' and  object_id = OBJECT_ID('tblCheckDetail')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1835153583_2_37_6_80_70_5_1_58_57_105] ON [dbo].[tblCheckDetail]([EmployeeNumber], [DedDeductionNumber], [PayDate], [CheckVoid], [PayRunID], [CheckNumber], [ID], [DeductionRecord], [MasterRecord], [EmpDeductionID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				strSQL = "select * from sys.stats where name = '_dta_stat_1138103095_2_1' and  object_id = OBJECT_ID('tblEmployeeDeductions')";
				rsSave.OpenRecordset(strSQL, "Payroll");
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE STATISTICS [_dta_stat_1138103095_2_1] ON [dbo].[tblEmployeeDeductions]([RecordNumber], [ID])";
					rsSave.Execute(strSQL, "Payroll");
				}
				AddCheckDetailStats = true;
				return AddCheckDetailStats;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddCheckDetailStats;
		}

		private bool UpdateStateStandardDeductions()
		{
			//bool UpdateStateStandardDeductions = false;
			//clsDRWrapper rsSave = new clsDRWrapper();
			//string strSQL;
			//try
			//{
			//	// On Error GoTo ErrorHandler
			//	fecherFoundation.Information.Err().Clear();
			//	strSQL = "update tblstandardlimits set statesinglededuction = 8950,statemarrieddeduction = 20750";
			//	rsSave.Execute(strSQL, "Payroll");
			//}
			//catch (Exception ex)
			//{
			//	// ErrorHandler:
			//	UpdateStateStandardDeductions = true;
			//}
			return true;
		}

        private bool Add2020W4NewFieldsAndFederalTaxTables()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                if (!FieldExists("tblEmployeeMaster", "W4Date", "Payroll"))
                {
                    rsSave.Execute("alter Table.[dbo].[tblEmployeeMaster] Add [W4Date] datetime NULL", "Payroll");
                }

                if (!FieldExists("tblEmployeeMaster", "W4MultipleJobs", "Payroll"))
                {
                    rsSave.Execute("alter Table.[dbo].[tblEmployeeMaster] Add [W4MultipleJobs] bit NULL", "Payroll");
                }

                if (!FieldExists("tblEmployeeMaster", "FederalOtherDependents", "Payroll"))
                {
                    rsSave.Execute("alter Table.[dbo].[tblEmployeeMaster] Add [FederalOtherDependents] int NULL",
                        "Payroll");
                }

                if (!FieldExists("tblEmployeeMaster", "AddFederalDeduction", "Payroll"))
                {
                    rsSave.Execute("alter Table.[dbo].[tblEmployeeMaster] Add [AddFederalDeduction] float NULL",
                        "Payroll");
                }

                if (!FieldExists("tblEmployeeMaster", "FederalOtherIncome", "Payroll"))
                {
                    rsSave.Execute("alter Table.[dbo].[tblEmployeeMaster] Add [FederalOtherIncome] float NULL",
                        "Payroll");
                }

                if (!FieldExists("tblstandardlimits", "FederalStdMarriedDeduction", "Payroll"))
                {
                    rsSave.Execute(
                        "alter Table.[dbo].[tblstandardlimits] Add [FederalStdMarriedDeduction] float NULL", "Payroll");
                }

                if (!FieldExists("tblstandardlimits", "FederalStdSingleDeduction", "Payroll"))
                {
                    rsSave.Execute(
                        "alter Table.[dbo].[tblstandardlimits] Add [FederalStdSingleDeduction] float NULL", "Payroll");
                }

                if (!FieldExists("tblstandardlimits", "FederalQualifyingDependentCreditAmount", "Payroll"))
                {
                    rsSave.Execute(
                        "alter Table.[dbo].[tblstandardlimits] Add [FederalQualifyingDependentCreditAmount] float NULL",
                        "Payroll");
                }

                if (!FieldExists("tblstandardlimits", "FederalOtherDependentCreditAmount", "Payroll"))
                {
                    rsSave.Execute(
                        "alter Table.[dbo].[tblstandardlimits] Add [FederalOtherDependentCreditAmount] float NULL",
                        "Payroll");
                }

                var strSQL =
                    "update tblstandardlimits set FederalStdMarriedDeduction = 12600,FederalStdSingleDeduction = 8400, FederalQualifyingDependentCreditAmount = 2000, FederalOtherDependentCreditAmount = 500";
                rsSave.Execute(strSQL, "Payroll");

                int hhTableId = 0;
                int s2TableId = 0;
                int m2TableId = 0;
                int h2TableId = 0;

                rsSave.OpenRecordset("Select * from tblPayStatuses where StatusCode = '4' and [Type] = 'Federal'", "Payroll");
                if (rsSave.EndOfFile())
                {
                    rsSave.AddNew();
                    rsSave.Set_Fields("StatusCode", "4");
                    rsSave.Set_Fields("Description", "Head of Household");
                    rsSave.Set_Fields("UseTable", "HEAD OF HOUSEHOLD");
                    rsSave.Set_Fields("Type", "FEDERAL");
                    rsSave.Set_Fields("LastUpdate", DateTime.Now);
                    rsSave.Update();
                    hhTableId = rsSave.Get_Fields_Int32("Id");
                }
                

                if (!TableExists("tblFederalHeadOfHouseholdTaxTable", "Payroll"))
                {
                    strSQL =
                        "if not exists (select * from information_schema.tables where table_name = N'tblFederalHeadOfHouseholdTaxTable') ";
                    strSQL = strSQL + "Create Table [dbo].[tblFederalHeadOfHouseholdTaxTable] ("
                                    + "[ID] [int] IDENTITY (1,1) NOT NULL,"
                                    + "[RecordNumber] [int] NULL,"
                                    + "[PayMin] [float] NULL,"
                                    + "[PayMax] [float]  NULL,"
                                    + "[Tax] [float] NULL,"
                                    + "[TaxPercent] [float]  NULL,"
                                    + "[AmountOver] [float]  NULL,"
                                    + "[LastUserID] [nvarchar](25)  NULL,"
                                    + "[LastUpdate] [datetime]  NULL,"
                                    + " Constraint [PK_tblFederalHeadOfHouseholdTaxTable] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    rsSave.Execute(strSQL, "Payroll");
                    strSQL = "select * from tblFederalHeadOfHouseholdTaxTable where id = 0";
                    rsSave.OpenRecordset(strSQL, "Payroll");
                    AddTaxTableRecords(strSQL, hhTableId, 0, 10050, 0, 0, 0);
                    AddTaxTableRecords(strSQL, hhTableId, 10050, 24150, 0, 10, 10050);
                    AddTaxTableRecords(strSQL, hhTableId, 24150, 63750, 1410, 12, 24150);
                    AddTaxTableRecords(strSQL, hhTableId, 63750, 95550, 6162, 22, 63750);
                    AddTaxTableRecords(strSQL, hhTableId, 95550, 173350, 13158, 24, 95550);
                    AddTaxTableRecords(strSQL, hhTableId, 173350, 217400, 31830, 32, 173350);
                    AddTaxTableRecords(strSQL, hhTableId, 217400, 528450, 45926, 35, 217400);
                    AddTaxTableRecords(strSQL, hhTableId, 528450, 99999999, 154793.5, 37, 528450);
                }

                rsSave.OpenRecordset("select top 1 * from tblFederalHeadOfHouseholdTaxTable", "Payroll");

                //   Multiple Jobs Tables (W-4, Step 2c checked)


                if (!TableExists("tblFederalMarriedMultiJobsTaxTable", "Payroll"))
                {
                    strSQL =
                        "if not exists (select * from information_schema.tables where table_name = N'tblFederalMarriedMultiJobsTaxTable') "
                        + "Create Table [dbo].[tblFederalMarriedMultiJobsTaxTable] ("
                        + "[ID] [int] IDENTITY (1,1) NOT NULL,"
                        + "[RecordNumber] [int] NULL,"
                        + "[PayMin] [float] NULL,"
                        + "[PayMax] [float]  NULL,"
                        + "[Tax] [float] NULL,"
                        + "[TaxPercent] [float]  NULL,"
                        + "[AmountOver] [float]  NULL,"
                        + "[LastUserID] [nvarchar](25)  NULL,"
                        + "[LastUpdate] [datetime]  NULL,"
                        + " Constraint [PK_tblFederalMarriedMultiJobsTaxTable] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    rsSave.Execute(strSQL, "Payroll");
                    strSQL = "select * from tblFederalMarriedMultiJobsTaxTable where id = 0";
                    AddTaxTableRecords(strSQL, m2TableId, 0, 12400, 0, 0, 0);
                    AddTaxTableRecords(strSQL, m2TableId, 12400, 22275, 0, 10, 12400);
                    AddTaxTableRecords(strSQL, m2TableId, 22275, 52525, 987.5, 12, 21900);
                    AddTaxTableRecords(strSQL, m2TableId, 52525, 97925, 4617.5, 22, 52525);
                    AddTaxTableRecords(strSQL, m2TableId, 97925, 175700, 14605.5, 24, 97925);
                    AddTaxTableRecords(strSQL, m2TableId, 175700, 219750, 33271.5, 32, 175700);
                    AddTaxTableRecords(strSQL, m2TableId, 219750, 323425, 47367.5, 35, 219750);
                    AddTaxTableRecords(strSQL, m2TableId, 323425, 99999999, 83653.75, 37, 323425);
                }

                if (!TableExists("tblFederalSingleMultiJobsTaxTable", "Payroll"))
                {
                    strSQL =
                        "if not exists (select * from information_schema.tables where table_name = N'tblFederalSingleMultiJobsTaxTable') "
                        + "Create Table [dbo].[tblFederalSingleMultiJobsTaxTable] ("
                        + "[ID] [int] IDENTITY (1,1) NOT NULL,"
                        + "[RecordNumber] [int] NULL,"
                        + "[PayMin] [float] NULL,"
                        + "[PayMax] [float]  NULL,"
                        + "[Tax] [float] NULL,"
                        + "[TaxPercent] [float]  NULL,"
                        + "[AmountOver] [float]  NULL,"
                        + "[LastUserID] [nvarchar](25)  NULL,"
                        + "[LastUpdate] [datetime]  NULL,"
                        + " Constraint [PK_tblFederalSingleMultiJobsTaxTable] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    rsSave.Execute(strSQL, "Payroll");
                    strSQL = "select * from tblFederalSingleMultiJobsTaxTable where id = 0";

                    AddTaxTableRecords(strSQL, s2TableId, 0, 6200, 0, 0, 0);
                    AddTaxTableRecords(strSQL, s2TableId, 6200, 11138, 0, 10, 6200);
                    AddTaxTableRecords(strSQL, s2TableId, 11138, 26263, 493.75, 12, 11138);
                    AddTaxTableRecords(strSQL, s2TableId, 26263, 48963, 2308.75, 22, 26263);
                    AddTaxTableRecords(strSQL, s2TableId, 48963, 87850, 7302.75, 24, 48963);
                    AddTaxTableRecords(strSQL, s2TableId, 87850, 109875, 16635.75, 32, 87850);
                    AddTaxTableRecords(strSQL, s2TableId, 109875, 265400, 23683.75, 35, 109875);
                    AddTaxTableRecords(strSQL, s2TableId, 265400, 99999999, 78117.5, 37, 265400);

                }

                if (!TableExists("tblFederalHeadOfHouseholdMultiJobsTaxTable", "Payroll"))
                {
                    strSQL =
                        "if not exists (select * from information_schema.tables where table_name = N'tblFederalHeadOfHouseholdMultiJobsTaxTable') "
                        + "Create Table [dbo].[tblFederalHeadOfHouseholdMultiJobsTaxTable] ("
                        + "[ID] [int] IDENTITY (1,1) NOT NULL,"
                        + "[RecordNumber] [int] NULL,"
                        + "[PayMin] [float] NULL,"
                        + "[PayMax] [float]  NULL,"
                        + "[Tax] [float] NULL,"
                        + "[TaxPercent] [float]  NULL,"
                        + "[AmountOver] [float]  NULL,"
                        + "[LastUserID] [nvarchar](25)  NULL,"
                        + "[LastUpdate] [datetime]  NULL,"
                        + " Constraint [PK_tblFederalHeadOfHouseholdMultiJobsTaxTable] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                        rsSave.Execute(strSQL, "Payroll");
                        strSQL = "select * from tblFederalHeadOfHouseholdMultiJobsTaxTable where id = 0";

                        AddTaxTableRecords(strSQL, h2TableId, 0, 9325, 0, 0, 0);
                        AddTaxTableRecords(strSQL, h2TableId, 9325, 16375, 0, 10, 9325);
                        AddTaxTableRecords(strSQL, h2TableId, 16375, 36175, 705, 12, 16375);
                        AddTaxTableRecords(strSQL, h2TableId, 36175, 52075, 3081, 22, 36175);
                        AddTaxTableRecords(strSQL, h2TableId, 52075, 90975, 6579, 24, 52075);
                        AddTaxTableRecords(strSQL, h2TableId, 90975, 113000, 15915, 32, 90975);
                        AddTaxTableRecords(strSQL, h2TableId, 113000, 268525, 22963, 35, 113000);
                        AddTaxTableRecords(strSQL, h2TableId, 268525, 99999999, 77396.75, 37, 268525);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool ConvertPayStatusesFromSQL()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var rsLoad = new clsDRWrapper();
                int headOfHouseholdId = 0;
                int singleId = 0;
                int marriedId = 0;
                int marriedFilingSingleId = 0;
                rsSave.OpenRecordset("select * from tblPayStatuses where [Type] = 'FEDERAL' and statuscode = 4",
                    "Payroll");
                if (!rsSave.EndOfFile())
                {
                    headOfHouseholdId = rsSave.Get_Fields_Int32("ID");
                }

                rsSave.OpenRecordset("Select * from tblPayStatuses where [Type] = 'FEDERAL' and statuscode = 3",
                    "Payroll");
                if (!rsSave.EndOfFile())
                {
                    marriedId = rsSave.Get_Fields_Int32("ID");
                }

                rsSave.OpenRecordset("Select * from tblPayStatuses where [Type] = 'FEDERAL' and statuscode = 2",
                    "Payroll");
                if (!rsSave.EndOfFile())
                {
                    singleId = rsSave.Get_Fields_Int32("ID");
                }
                rsSave.OpenRecordset("Select * from tblPayStatuses where [Type] = 'FEDERAL' and statuscode = 1",
                    "Payroll");
                if (!rsSave.EndOfFile())
                {
                    marriedFilingSingleId = rsSave.Get_Fields_Int32("ID");
                }
                rsLoad.OpenRecordset("Select * from tblPayStatuses where [Type] = 'FEDERAL' and StatusCode > 4",
                    "Payroll");                
                while (!rsLoad.EndOfFile())
                {
                    var statusCode = rsLoad.Get_Fields_Int32("StatusCode");
                    var useTable = rsLoad.Get_Fields_String("UseTable").ToLower();
                    var id = rsLoad.Get_Fields_Int32("ID");
                    if (useTable.StartsWith("head"))
                    {
                        rsSave.Execute(
                            "Update tblEmployeeMaster set FedFilingStatusId = " + headOfHouseholdId.ToString() + " where fedfilingstatusid = " + id.ToString(),
                            "Payroll");                        
                    }
                    else if (useTable.StartsWith("single"))
                    {
                        rsSave.Execute(
                            "Update tblEmployeeMaster set FedFilingStatusId = " + singleId.ToString() + " where fedfilingstatusid = " + id.ToString(),
                            "Payroll");                        
                    }
                    else if (useTable.StartsWith("married"))
                    {
                        rsSave.Execute(
                            "Update tblEmployeeMaster set FedFilingStatusId = " + marriedId.ToString() + " where fedfilingstatusid = " + id.ToString(),
                            "Payroll");                       
                    }
                    rsSave.Execute("Delete from tblPayStatuses where id = " + id.ToString(), "Payroll");
                    rsLoad.MoveNext();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void AddTaxTableRecords(string strSql, int recordNumber, double min, double max, double tax, double percent, double over)
        {
            var rsTax = new clsDRWrapper();
            rsTax.OpenRecordset(strSql, "Payroll");
            rsTax.AddNew();
            rsTax.Set_Fields("RecordNumber",recordNumber);
            rsTax.Set_Fields("PayMin",min);
            rsTax.Set_Fields("PayMax",max);
            rsTax.Set_Fields("Tax",tax);
            rsTax.Set_Fields("TaxPercent",percent);
            rsTax.Set_Fields("AmountOver",over);
            rsTax.Set_Fields("LastUpdate",DateTime.Now);
            rsTax.Update();
        }

        private bool AddNewSSNPermission()
        {
            try
            {
                var rsLoad = new clsDRWrapper();
                var rsSave = new clsDRWrapper();
                rsLoad.OpenRecordset(
                    "Select * from permissionstable where functionid = 1 and modulename = 'PY' and permission = 'F'",
                    "SystemSettings");
                while (!rsLoad.EndOfFile())
                {
                    rsSave.OpenRecordset(
                        "Select * from permissionstable where modulename = 'PY' and functionid = 72 and userid = " +
                        rsLoad.Get_Fields_Int32("UserID"), "SystemSettings");
                    if (rsSave.EndOfFile())
                    {
                        rsSave.Execute(
                            "Insert into permissionstable (UserID,ModuleName,FunctionID,Permission) Values (" +
                            rsLoad.Get_Fields_Int32("UserID") + ",'PY',72,'F')", "SystemSettings");
                    }

                    rsLoad.MoveNext();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool AddNewBankPermission()
        {
            try
            {
                var rsLoad = new clsDRWrapper();
                var rsSave = new clsDRWrapper();
                rsLoad.OpenRecordset(
                    "Select * from permissionstable where functionid = 1 and modulename = 'PY' and permission = 'F'",
                    "SystemSettings");
                while (!rsLoad.EndOfFile())
                {
                    rsSave.OpenRecordset(
                        "Select * from permissionstable where modulename = 'PY' and functionid = 73 and userid = " +
                        rsLoad.Get_Fields_Int32("UserID"), "SystemSettings");
                    if (rsSave.EndOfFile())
                    {
                        rsSave.Execute(
                            "Insert into permissionstable (UserID,ModuleName,FunctionID,Permission) Values (" +
                            rsLoad.Get_Fields_Int32("UserID") + ",'PY',73,'F')", "SystemSettings");
                    }

                    rsLoad.MoveNext();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool RemoveBankAccountsFromAudit()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var strSql = "delete from AuditChangesArchive where Location = 'Direct Deposit' and ChangeDescription like '%Account changed%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditChanges where Location = 'Direct Deposit' and ChangeDescription like '%Account changed%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditHistory where Description3 like 'Account%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditArchive where Description3 like 'Account%'";
                rsSave.Execute(strSql, "Payroll");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private bool RemoveSSNFromAuditHistory()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var strSql = "delete from AuditHistory where Description1 like '%txtSSN%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditArchive where Description1 like '%txtSSN%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditChangesArchive where  ChangeDescription like '%Social Security Number%'";
                rsSave.Execute(strSql, "Payroll");
                strSql = "delete from AuditChanges where  ChangeDescription like '%Social Security Number%'";
                rsSave.Execute(strSql, "Payroll");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private string GetTableCreationHeaderSQL(string strTableName)
        {
            string GetTableCreationHeaderSQL = "";
            string strSQL;
            strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
            strSQL += "Create Table [dbo].[" + strTableName + "] (";
            strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
            GetTableCreationHeaderSQL = strSQL;
            return GetTableCreationHeaderSQL;
        }

        private string GetTablePKSql(string strTableName)
        {
            string GetTablePKSql = "";
            string strSQL;
            strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
            GetTablePKSql = strSQL;
            return GetTablePKSql;
        }

		private bool AddCategoryTags()
        {
            try
            {
                if (AddCategoryTagsTable())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

     //   private bool AddTags()
     //   {
     //       try
     //       {
     //           if (!TableExists("Tags", "Payroll"))
     //           {
     //               var strsql =  GetTableCreationHeaderSQL("Tags");
     //               strsql = strsql + " [Name] nvarchar(255) NULL,";
     //               strsql = strsql + " [Description] nvarchar(255) NULL,";
     //               strsql = strsql + GetTablePKSql("Tags");
					//var rs = new clsDRWrapper();
     //               rs.Execute(strsql, "Payroll");
     //               rs.Execute(
     //                   "insert into Tags (Name,Description) values ('WorkedHours','Counts towards hours worked')","Payroll");
     //           }

     //           return true;
     //       }
     //       catch (Exception ex)
     //       {
     //           return false;
     //       }
     //   }

        private bool AddCategoryTagsTable()
        {
            try
            {
				var tableName = "CategoryTags";

				if (!TableExists(tableName, "Payroll"))
                {
                    var strsql = GetTableCreationHeaderSQL(tableName);
                    strsql += " [PayCategory] int NULL,";
                    strsql += " [Tag] nvarchar(255) NULL, ";
                    strsql += GetTablePKSql(tableName);
					var rs = new clsDRWrapper();
                    if (!rs.Execute(strsql, "Payroll"))
                    {
                        return false;
                    }

                    DefaultCategoryTags();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void DefaultCategoryTags()
        {
			var rsLoad = new clsDRWrapper();
			var rsSave = new clsDRWrapper();
            rsLoad.OpenRecordset("select * from tblPayCategories where [type] = 'Hours'", "Payroll");
            while (!rsLoad.EndOfFile())
            {
                rsSave.OpenRecordset("select * from tblAssociateCodes where PayCatID =" + rsLoad.Get_Fields_Int32("ID"),
                    "Payroll");
                if (rsSave.EndOfFile())
                {
					rsSave.Execute("insert into CategoryTags (PayCategory,Tag) values (" + rsLoad.Get_Fields_Int32("ID") + ",'Worked Hours')","Payroll");
                }
                rsLoad.MoveNext();
            }
        }

        private bool AddThirdPartySickPayToW2EditTable()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var strSQL = "";
                if (!FieldExists("tblW2EditTable","W23rdPartySickPay","Payroll"))
                {
                    strSQL = "alter table tblw2edittable add W23rdPartySickPay [Bit] NULL";
                    rsSave.Execute(strSQL, "Payroll");
                }
                if (!FieldExists("tblW2Original", "W23rdPartySickPay","Payroll"))
                {
                    strSQL = "alter table tblw2original add W23rdPartySickPay [Bit] NULL";
                    rsSave.Execute(strSQL, "Payroll");
                }
                if (!FieldExists("tblw2Archive","W23rdPartySickPay","Payroll"))
                {
                    strSQL = "alter table tblW2Archive add W23rdPartySickPay [Bit] NULL";
                    rsSave.Execute(strSQL, "Payroll");
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool AddZipMonth()
        {
            try
            {
                for(int x = 1; x < 13; x++)
                {
                    if  (!AddFieldIfDoesntExist("ZipCodeMonth" + x, "ACAEmployeeSetup", "Payroll", "NVarchar(50) NULL"))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            return false;
        }

        private bool AddFieldIfDoesntExist(string fieldName, string tableName, string dbName,string creationCommand)
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var sql = "If not exists(Select column_name from information_schema.columns where column_name = '" + fieldName + "' and table_name = '" + tableName + "') BEGIN ";
                sql += " Alter table [" + tableName + "] add [" + fieldName + "] " + creationCommand + ";";
                sql += " END";
                return rsSave.Execute(sql, dbName);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}