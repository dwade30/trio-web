//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmVacationSick.
	/// </summary>
	partial class frmVacationSick
	{
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCLabel txtCode;
		public fecherFoundation.FCLabel txtEmployee;
		public fecherFoundation.FCComboBox cboType;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtUsedBalance;
		public fecherFoundation.FCTextBox txtUsedSickBalance;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtUsedSickCalendar;
		public fecherFoundation.FCTextBox txtUsedCalendar;
		public fecherFoundation.FCTextBox txtUsedSickFiscal;
		public fecherFoundation.FCTextBox txtUsedFiscal;
		public fecherFoundation.FCTextBox txtUsedSickCurrent;
		public fecherFoundation.FCTextBox txtUsedCurrent;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtAccruedSickCalendar;
		public fecherFoundation.FCTextBox txtAccruedCalendar;
		public fecherFoundation.FCTextBox txtAccruedSickFiscal;
		public fecherFoundation.FCTextBox txtAccruedFiscal;
		public fecherFoundation.FCTextBox txtAccruedSickCurrent;
		public fecherFoundation.FCTextBox txtAccruedCurrent;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public FCGrid vsCodes;
		public fecherFoundation.FCLabel lblCurrentEmployee;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuCodes;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuMultiPays;
		public fecherFoundation.FCToolStripMenuItem mnuMultiPay;
		public fecherFoundation.FCToolStripMenuItem mnuCaption1;
		public fecherFoundation.FCToolStripMenuItem mnuCaption2;
		public fecherFoundation.FCToolStripMenuItem mnuCaption3;
		public fecherFoundation.FCToolStripMenuItem mnuCaption4;
		public fecherFoundation.FCToolStripMenuItem mnuCaption5;
		public fecherFoundation.FCToolStripMenuItem mnuCaption6;
		public fecherFoundation.FCToolStripMenuItem mnuCaption7;
		public fecherFoundation.FCToolStripMenuItem mnuCaption8;
		public fecherFoundation.FCToolStripMenuItem mnuCaption9;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtCode = new fecherFoundation.FCLabel();
            this.txtEmployee = new fecherFoundation.FCLabel();
            this.cboType = new fecherFoundation.FCComboBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtUsedBalance = new fecherFoundation.FCTextBox();
            this.txtUsedSickBalance = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtUsedSickCalendar = new fecherFoundation.FCTextBox();
            this.txtUsedCalendar = new fecherFoundation.FCTextBox();
            this.txtUsedSickFiscal = new fecherFoundation.FCTextBox();
            this.txtUsedFiscal = new fecherFoundation.FCTextBox();
            this.txtUsedSickCurrent = new fecherFoundation.FCTextBox();
            this.txtUsedCurrent = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtAccruedSickCalendar = new fecherFoundation.FCTextBox();
            this.txtAccruedCalendar = new fecherFoundation.FCTextBox();
            this.txtAccruedSickFiscal = new fecherFoundation.FCTextBox();
            this.txtAccruedFiscal = new fecherFoundation.FCTextBox();
            this.txtAccruedSickCurrent = new fecherFoundation.FCTextBox();
            this.txtAccruedCurrent = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsCodes = new fecherFoundation.FCGrid();
            this.lblCurrentEmployee = new fecherFoundation.FCLabel();
            this.lblType = new fecherFoundation.FCLabel();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultiPays = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultiPay = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption8 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption9 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCodes = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdEdit = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsCodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 641);
            this.BottomPanel.Size = new System.Drawing.Size(1023, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cboType);
            this.ClientArea.Controls.Add(this.vsCodes);
            this.ClientArea.Controls.Add(this.lblCurrentEmployee);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Size = new System.Drawing.Size(1043, 682);
            this.ClientArea.Controls.SetChildIndex(this.lblType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCurrentEmployee, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsCodes, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboType, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdEdit);
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Size = new System.Drawing.Size(1043, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEdit, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(163, 28);
            this.HeaderText.Text = "Vacation / Sick";
            // 
            // Frame4
            // 
            this.Frame4.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.Frame4.BackColor = System.Drawing.Color.White;
            this.Frame4.Controls.Add(this.txtCode);
            this.Frame4.Controls.Add(this.txtEmployee);
            this.Frame4.Location = new System.Drawing.Point(30, 497);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(200, 144);
            this.Frame4.TabIndex = 29;
            this.Frame4.Text = "Status";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(20, 60);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(160, 40);
            this.txtCode.TabIndex = 33;
            this.txtCode.Tag = "Code #";
            // 
            // txtEmployee
            // 
            this.txtEmployee.Location = new System.Drawing.Point(20, 30);
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.Size = new System.Drawing.Size(160, 20);
            this.txtEmployee.TabIndex = 32;
            this.txtEmployee.Tag = "Employee #";
            // 
            // cboType
            // 
            this.cboType.BackColor = System.Drawing.SystemColors.Window;
            this.cboType.Location = new System.Drawing.Point(92, 30);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(308, 40);
            this.cboType.TabIndex = 30;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.Enter += new System.EventHandler(this.cboType_Enter);
            // 
            // Frame3
            // 
            this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.Frame3.AppearanceKey = "groupBoxNoBorders";
            this.Frame3.Controls.Add(this.txtUsedBalance);
            this.Frame3.Controls.Add(this.txtUsedSickBalance);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Location = new System.Drawing.Point(885, 497);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(121, 144);
            this.Frame3.TabIndex = 25;
            // 
            // txtUsedBalance
            // 
            this.txtUsedBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedBalance.Location = new System.Drawing.Point(1, 84);
            this.txtUsedBalance.MaxLength = 25;
            this.txtUsedBalance.Name = "txtUsedBalance";
            this.txtUsedBalance.Size = new System.Drawing.Size(120, 40);
            this.txtUsedBalance.TabIndex = 8;
            this.txtUsedBalance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedBalance.Enter += new System.EventHandler(this.txtUsedBalance_Enter);
            this.txtUsedBalance.TextChanged += new System.EventHandler(this.txtUsedBalance_TextChanged);
            // 
            // txtUsedSickBalance
            // 
            this.txtUsedSickBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedSickBalance.Location = new System.Drawing.Point(1, 159);
            this.txtUsedSickBalance.MaxLength = 25;
            this.txtUsedSickBalance.Name = "txtUsedSickBalance";
            this.txtUsedSickBalance.Size = new System.Drawing.Size(120, 40);
            this.txtUsedSickBalance.TabIndex = 26;
            this.txtUsedSickBalance.TabStop = false;
            this.txtUsedSickBalance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedSickBalance.TextChanged += new System.EventHandler(this.txtUsedSickBalance_TextChanged);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(1, 57);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(60, 19);
            this.Label9.TabIndex = 27;
            this.Label9.Text = "BALANCE";
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.txtUsedSickCalendar);
            this.Frame1.Controls.Add(this.txtUsedCalendar);
            this.Frame1.Controls.Add(this.txtUsedSickFiscal);
            this.Frame1.Controls.Add(this.txtUsedFiscal);
            this.Frame1.Controls.Add(this.txtUsedSickCurrent);
            this.Frame1.Controls.Add(this.txtUsedCurrent);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Location = new System.Drawing.Point(568, 497);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(298, 144);
            this.Frame1.TabIndex = 9;
            this.Frame1.Text = "Used";
            // 
            // txtUsedSickCalendar
            // 
            this.txtUsedSickCalendar.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedSickCalendar.Location = new System.Drawing.Point(200, 156);
            this.txtUsedSickCalendar.MaxLength = 25;
            this.txtUsedSickCalendar.Name = "txtUsedSickCalendar";
            this.txtUsedSickCalendar.Size = new System.Drawing.Size(80, 40);
            this.txtUsedSickCalendar.TabIndex = 20;
            this.txtUsedSickCalendar.TabStop = false;
            this.txtUsedSickCalendar.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedSickCalendar.Enter += new System.EventHandler(this.txtUsedSickCalendar_Enter);
            this.txtUsedSickCalendar.TextChanged += new System.EventHandler(this.txtUsedSickCalendar_TextChanged);
            // 
            // txtUsedCalendar
            // 
            this.txtUsedCalendar.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedCalendar.Location = new System.Drawing.Point(200, 84);
            this.txtUsedCalendar.MaxLength = 25;
            this.txtUsedCalendar.Name = "txtUsedCalendar";
            this.txtUsedCalendar.Size = new System.Drawing.Size(80, 40);
            this.txtUsedCalendar.TabIndex = 7;
            this.txtUsedCalendar.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedCalendar.Enter += new System.EventHandler(this.txtUsedCalendar_Enter);
            this.txtUsedCalendar.TextChanged += new System.EventHandler(this.txtUsedCalendar_TextChanged);
            this.txtUsedCalendar.Validating += new System.ComponentModel.CancelEventHandler(this.txtUsedCalendar_Validating);
            // 
            // txtUsedSickFiscal
            // 
            this.txtUsedSickFiscal.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedSickFiscal.Location = new System.Drawing.Point(110, 156);
            this.txtUsedSickFiscal.MaxLength = 25;
            this.txtUsedSickFiscal.Name = "txtUsedSickFiscal";
            this.txtUsedSickFiscal.Size = new System.Drawing.Size(80, 40);
            this.txtUsedSickFiscal.TabIndex = 19;
            this.txtUsedSickFiscal.TabStop = false;
            this.txtUsedSickFiscal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedSickFiscal.Enter += new System.EventHandler(this.txtUsedSickFiscal_Enter);
            this.txtUsedSickFiscal.TextChanged += new System.EventHandler(this.txtUsedSickFiscal_TextChanged);
            // 
            // txtUsedFiscal
            // 
            this.txtUsedFiscal.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedFiscal.Location = new System.Drawing.Point(110, 84);
            this.txtUsedFiscal.MaxLength = 25;
            this.txtUsedFiscal.Name = "txtUsedFiscal";
            this.txtUsedFiscal.Size = new System.Drawing.Size(80, 40);
            this.txtUsedFiscal.TabIndex = 6;
            this.txtUsedFiscal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedFiscal.Enter += new System.EventHandler(this.txtUsedFiscal_Enter);
            this.txtUsedFiscal.TextChanged += new System.EventHandler(this.txtUsedFiscal_TextChanged);
            this.txtUsedFiscal.Validating += new System.ComponentModel.CancelEventHandler(this.txtUsedFiscal_Validating);
            // 
            // txtUsedSickCurrent
            // 
            this.txtUsedSickCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedSickCurrent.Location = new System.Drawing.Point(20, 156);
            this.txtUsedSickCurrent.MaxLength = 25;
            this.txtUsedSickCurrent.Name = "txtUsedSickCurrent";
            this.txtUsedSickCurrent.Size = new System.Drawing.Size(80, 40);
            this.txtUsedSickCurrent.TabIndex = 18;
            this.txtUsedSickCurrent.TabStop = false;
            this.txtUsedSickCurrent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedSickCurrent.Enter += new System.EventHandler(this.txtUsedSickCurrent_Enter);
            this.txtUsedSickCurrent.TextChanged += new System.EventHandler(this.txtUsedSickCurrent_TextChanged);
            // 
            // txtUsedCurrent
            // 
            this.txtUsedCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsedCurrent.Location = new System.Drawing.Point(20, 84);
            this.txtUsedCurrent.MaxLength = 25;
            this.txtUsedCurrent.Name = "txtUsedCurrent";
            this.txtUsedCurrent.Size = new System.Drawing.Size(80, 40);
            this.txtUsedCurrent.TabIndex = 5;
            this.txtUsedCurrent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUsedCurrent.Enter += new System.EventHandler(this.txtUsedCurrent_Enter);
            this.txtUsedCurrent.TextChanged += new System.EventHandler(this.txtUsedCurrent_TextChanged);
            this.txtUsedCurrent.Validating += new System.ComponentModel.CancelEventHandler(this.txtUsedCurrent_Validating);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(110, 57);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 19);
            this.Label1.TabIndex = 28;
            this.Label1.Text = "FISCAL";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 30);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(260, 19);
            this.Label11.TabIndex = 24;
            this.Label11.Text = " - - - - - - - - - - - - - - - - - Y T D - - - - - - - - - - - - - - - - - ";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(200, 57);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(80, 19);
            this.Label8.TabIndex = 23;
            this.Label8.Text = "CALENDAR";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(110, 57);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(50, 19);
            this.Label7.TabIndex = 22;
            this.Label7.Text = "FISCAL";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 57);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(80, 19);
            this.Label6.TabIndex = 21;
            this.Label6.Text = "CURRENT";
            // 
            // Frame2
            // 
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.txtAccruedSickCalendar);
            this.Frame2.Controls.Add(this.txtAccruedCalendar);
            this.Frame2.Controls.Add(this.txtAccruedSickFiscal);
            this.Frame2.Controls.Add(this.txtAccruedFiscal);
            this.Frame2.Controls.Add(this.txtAccruedSickCurrent);
            this.Frame2.Controls.Add(this.txtAccruedCurrent);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(250, 497);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(298, 141);
            this.Frame2.TabIndex = 10;
            this.Frame2.Text = "Accrued";
            // 
            // txtAccruedSickCalendar
            // 
            this.txtAccruedSickCalendar.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedSickCalendar.Location = new System.Drawing.Point(200, 157);
            this.txtAccruedSickCalendar.MaxLength = 25;
            this.txtAccruedSickCalendar.Name = "txtAccruedSickCalendar";
            this.txtAccruedSickCalendar.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedSickCalendar.TabIndex = 13;
            this.txtAccruedSickCalendar.TabStop = false;
            this.txtAccruedSickCalendar.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedSickCalendar.Enter += new System.EventHandler(this.txtAccruedSickCalendar_Enter);
            this.txtAccruedSickCalendar.TextChanged += new System.EventHandler(this.txtAccruedSickCalendar_TextChanged);
            this.txtAccruedSickCalendar.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedSickCalendar_Validating);
            // 
            // txtAccruedCalendar
            // 
            this.txtAccruedCalendar.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedCalendar.Location = new System.Drawing.Point(200, 84);
            this.txtAccruedCalendar.MaxLength = 25;
            this.txtAccruedCalendar.Name = "txtAccruedCalendar";
            this.txtAccruedCalendar.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedCalendar.TabIndex = 4;
            this.txtAccruedCalendar.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedCalendar.Enter += new System.EventHandler(this.txtAccruedCalendar_Enter);
            this.txtAccruedCalendar.TextChanged += new System.EventHandler(this.txtAccruedCalendar_TextChanged);
            this.txtAccruedCalendar.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedCalendar_Validating);
            // 
            // txtAccruedSickFiscal
            // 
            this.txtAccruedSickFiscal.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedSickFiscal.Location = new System.Drawing.Point(110, 157);
            this.txtAccruedSickFiscal.MaxLength = 25;
            this.txtAccruedSickFiscal.Name = "txtAccruedSickFiscal";
            this.txtAccruedSickFiscal.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedSickFiscal.TabIndex = 12;
            this.txtAccruedSickFiscal.TabStop = false;
            this.txtAccruedSickFiscal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedSickFiscal.Enter += new System.EventHandler(this.txtAccruedSickFiscal_Enter);
            this.txtAccruedSickFiscal.TextChanged += new System.EventHandler(this.txtAccruedSickFiscal_TextChanged);
            this.txtAccruedSickFiscal.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedSickFiscal_Validating);
            // 
            // txtAccruedFiscal
            // 
            this.txtAccruedFiscal.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedFiscal.Location = new System.Drawing.Point(110, 84);
            this.txtAccruedFiscal.MaxLength = 25;
            this.txtAccruedFiscal.Name = "txtAccruedFiscal";
            this.txtAccruedFiscal.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedFiscal.TabIndex = 3;
            this.txtAccruedFiscal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedFiscal.Enter += new System.EventHandler(this.txtAccruedFiscal_Enter);
            this.txtAccruedFiscal.TextChanged += new System.EventHandler(this.txtAccruedFiscal_TextChanged);
            this.txtAccruedFiscal.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedFiscal_Validating);
            // 
            // txtAccruedSickCurrent
            // 
            this.txtAccruedSickCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedSickCurrent.Location = new System.Drawing.Point(20, 157);
            this.txtAccruedSickCurrent.MaxLength = 25;
            this.txtAccruedSickCurrent.Name = "txtAccruedSickCurrent";
            this.txtAccruedSickCurrent.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedSickCurrent.TabIndex = 11;
            this.txtAccruedSickCurrent.TabStop = false;
            this.txtAccruedSickCurrent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedSickCurrent.Enter += new System.EventHandler(this.txtAccruedSickCurrent_Enter);
            this.txtAccruedSickCurrent.TextChanged += new System.EventHandler(this.txtAccruedSickCurrent_TextChanged);
            this.txtAccruedSickCurrent.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedSickCurrent_Validating);
            // 
            // txtAccruedCurrent
            // 
            this.txtAccruedCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccruedCurrent.Location = new System.Drawing.Point(20, 84);
            this.txtAccruedCurrent.MaxLength = 25;
            this.txtAccruedCurrent.Name = "txtAccruedCurrent";
            this.txtAccruedCurrent.Size = new System.Drawing.Size(80, 40);
            this.txtAccruedCurrent.TabIndex = 2;
            this.txtAccruedCurrent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccruedCurrent.Enter += new System.EventHandler(this.txtAccruedCurrent_Enter);
            this.txtAccruedCurrent.TextChanged += new System.EventHandler(this.txtAccruedCurrent_TextChanged);
            this.txtAccruedCurrent.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccruedCurrent_Validating);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(260, 19);
            this.Label10.TabIndex = 17;
            this.Label10.Text = " - - - - - - - - - - - - - - - - - Y T D - - - - - - - - - - - - - - - - - ";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(200, 57);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(65, 19);
            this.Label5.TabIndex = 16;
            this.Label5.Text = "CALENDAR";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(110, 57);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(60, 19);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "FISCAL";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 57);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 19);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "CURRENT";
            // 
            // vsCodes
            // 
            this.vsCodes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsCodes.Cols = 10;
            this.vsCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsCodes.Location = new System.Drawing.Point(30, 80);
            this.vsCodes.Name = "vsCodes";
            this.vsCodes.ReadOnly = false;
            this.vsCodes.Rows = 50;
            this.vsCodes.Size = new System.Drawing.Size(983, 407);
            this.vsCodes.TabIndex = 1;
            this.vsCodes.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsCodes_KeyDownEdit);
            this.vsCodes.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsCodes_AfterEdit);
            this.vsCodes.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCodes_ChangeEdit);
            this.vsCodes.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsCodes_MouseMoveEvent);
            this.vsCodes.CurrentCellChanged += new System.EventHandler(this.vsCodes_RowColChange);
            this.vsCodes.Enter += new System.EventHandler(this.vsCodes_Enter);
            this.vsCodes.Click += new System.EventHandler(this.vsCodes_ClickEvent);
            // 
            // lblCurrentEmployee
            // 
            this.lblCurrentEmployee.Location = new System.Drawing.Point(420, 44);
            this.lblCurrentEmployee.Name = "lblCurrentEmployee";
            this.lblCurrentEmployee.Size = new System.Drawing.Size(636, 22);
            this.lblCurrentEmployee.TabIndex = 31;
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(35, 17);
            this.lblType.TabIndex = 30;
            this.lblType.Text = "TYPE";
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = -1;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuMultiPays
            // 
            this.mnuMultiPays.Index = -1;
            this.mnuMultiPays.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuMultiPay,
            this.mnuCaption1,
            this.mnuCaption2,
            this.mnuCaption3,
            this.mnuCaption4,
            this.mnuCaption5,
            this.mnuCaption6,
            this.mnuCaption7,
            this.mnuCaption8,
            this.mnuCaption9});
            this.mnuMultiPays.Name = "mnuMultiPays";
            this.mnuMultiPays.Text = "Multi Pays";
            this.mnuMultiPays.Visible = false;
            // 
            // mnuMultiPay
            // 
            this.mnuMultiPay.Enabled = false;
            this.mnuMultiPay.Index = 0;
            this.mnuMultiPay.Name = "mnuMultiPay";
            this.mnuMultiPay.Text = "Add/Edit Additional Pay Master";
            // 
            // mnuCaption1
            // 
            this.mnuCaption1.Index = 1;
            this.mnuCaption1.Name = "mnuCaption1";
            this.mnuCaption1.Text = "Caption1";
            this.mnuCaption1.Click += new System.EventHandler(this.mnuCaption1_Click);
            // 
            // mnuCaption2
            // 
            this.mnuCaption2.Index = 2;
            this.mnuCaption2.Name = "mnuCaption2";
            this.mnuCaption2.Text = "Caption2";
            this.mnuCaption2.Click += new System.EventHandler(this.mnuCaption2_Click);
            // 
            // mnuCaption3
            // 
            this.mnuCaption3.Index = 3;
            this.mnuCaption3.Name = "mnuCaption3";
            this.mnuCaption3.Text = "Caption3";
            this.mnuCaption3.Click += new System.EventHandler(this.mnuCaption3_Click);
            // 
            // mnuCaption4
            // 
            this.mnuCaption4.Index = 4;
            this.mnuCaption4.Name = "mnuCaption4";
            this.mnuCaption4.Text = "Caption4";
            this.mnuCaption4.Click += new System.EventHandler(this.mnuCaption4_Click);
            // 
            // mnuCaption5
            // 
            this.mnuCaption5.Index = 5;
            this.mnuCaption5.Name = "mnuCaption5";
            this.mnuCaption5.Text = "Caption5";
            this.mnuCaption5.Click += new System.EventHandler(this.mnuCaption5_Click);
            // 
            // mnuCaption6
            // 
            this.mnuCaption6.Index = 6;
            this.mnuCaption6.Name = "mnuCaption6";
            this.mnuCaption6.Text = "Caption6";
            this.mnuCaption6.Click += new System.EventHandler(this.mnuCaption6_Click);
            // 
            // mnuCaption7
            // 
            this.mnuCaption7.Index = 7;
            this.mnuCaption7.Name = "mnuCaption7";
            this.mnuCaption7.Text = "Caption7";
            this.mnuCaption7.Click += new System.EventHandler(this.mnuCaption7_Click);
            // 
            // mnuCaption8
            // 
            this.mnuCaption8.Index = 8;
            this.mnuCaption8.Name = "mnuCaption8";
            this.mnuCaption8.Text = "Caption8";
            this.mnuCaption8.Click += new System.EventHandler(this.mnuCaption8_Click);
            // 
            // mnuCaption9
            // 
            this.mnuCaption9.Index = 9;
            this.mnuCaption9.Name = "mnuCaption9";
            this.mnuCaption9.Text = "Caption9";
            this.mnuCaption9.Click += new System.EventHandler(this.mnuCaption9_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = -1;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuCodes
            // 
            this.mnuCodes.Index = -1;
            this.mnuCodes.Name = "mnuCodes";
            this.mnuCodes.Text = "Edit Codes";
            this.mnuCodes.Click += new System.EventHandler(this.mnuCodes_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                     ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit              ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.Location = new System.Drawing.Point(805, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(120, 24);
            this.cmdSelect.TabIndex = 1;
            this.cmdSelect.Text = "Select Employee";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdEdit
            // 
            this.cmdEdit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEdit.Location = new System.Drawing.Point(931, 29);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(84, 24);
            this.cmdEdit.TabIndex = 2;
            this.cmdEdit.Text = "Edit Codes";
            this.cmdEdit.Click += new System.EventHandler(this.mnuCodes_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(468, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmVacationSick
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1043, 742);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmVacationSick";
            this.Text = "Vacation / Sick";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmVacationSick_Load);
            this.Activated += new System.EventHandler(this.frmVacationSick_Activated);
            this.Resize += new System.EventHandler(this.frmVacationSick_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVacationSick_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsCodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSelect;
		private FCButton cmdSave;
		private FCButton cmdEdit;
	}
}
