﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditCheckDetail.
	/// </summary>
	partial class frmEditCheckDetail
	{
		public fecherFoundation.FCComboBox cmbPayDate;
		public FCGrid GridEmployees;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddPayDate;
		public fecherFoundation.FCToolStripMenuItem mnusepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbPayDate = new fecherFoundation.FCComboBox();
            this.GridEmployees = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddPayDate = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAddPayDate = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPayDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdAddPayDate);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(748, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbPayDate);
            this.ClientArea.Controls.Add(this.GridEmployees);
            this.ClientArea.Size = new System.Drawing.Size(748, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(748, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(202, 30);
            this.HeaderText.Text = "Edit Pay Records";
            // 
            // cmbPayDate
            // 
            this.cmbPayDate.AutoSize = false;
            this.cmbPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPayDate.FormattingEnabled = true;
            this.cmbPayDate.Location = new System.Drawing.Point(30, 30);
            this.cmbPayDate.Name = "cmbPayDate";
            this.cmbPayDate.Size = new System.Drawing.Size(241, 40);
            this.cmbPayDate.TabIndex = 1;
            // 
            // GridEmployees
            // 
            this.GridEmployees.AllowSelection = false;
            this.GridEmployees.AllowUserToResizeColumns = false;
            this.GridEmployees.AllowUserToResizeRows = false;
            this.GridEmployees.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridEmployees.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridEmployees.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorBkg = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorSel = System.Drawing.Color.Empty;
            this.GridEmployees.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridEmployees.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridEmployees.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridEmployees.ColumnHeadersHeight = 30;
            this.GridEmployees.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridEmployees.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridEmployees.DragIcon = null;
            this.GridEmployees.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridEmployees.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridEmployees.ExtendLastCol = true;
            this.GridEmployees.FixedCols = 0;
            this.GridEmployees.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.FrozenCols = 0;
            this.GridEmployees.GridColor = System.Drawing.Color.Empty;
            this.GridEmployees.GridColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.Location = new System.Drawing.Point(30, 90);
            this.GridEmployees.Name = "GridEmployees";
            this.GridEmployees.OutlineCol = 0;
            this.GridEmployees.ReadOnly = true;
            this.GridEmployees.RowHeadersVisible = false;
            this.GridEmployees.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridEmployees.RowHeightMin = 0;
            this.GridEmployees.Rows = 1;
            this.GridEmployees.ScrollTipText = null;
            this.GridEmployees.ShowColumnVisibilityMenu = false;
            this.GridEmployees.Size = new System.Drawing.Size(680, 439);
            this.GridEmployees.StandardTab = true;
            this.GridEmployees.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridEmployees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridEmployees.TabIndex = 0;
            this.GridEmployees.DoubleClick += new System.EventHandler(this.GridEmployees_DblClick);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddPayDate,
            this.mnusepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddPayDate
            // 
            this.mnuAddPayDate.Index = 0;
            this.mnuAddPayDate.Name = "mnuAddPayDate";
            this.mnuAddPayDate.Text = "Add pay date";
            this.mnuAddPayDate.Click += new System.EventHandler(this.mnuAddPayDate_Click);
            // 
            // mnusepar2
            // 
            this.mnusepar2.Index = 1;
            this.mnusepar2.Name = "mnusepar2";
            this.mnusepar2.Text = "-";
            this.mnusepar2.Visible = false;
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 2;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Visible = false;
            // 
            // Seperator
            // 
            this.Seperator.Index = 3;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddPayDate
            // 
            this.cmdAddPayDate.AppearanceKey = "acceptButton";
            this.cmdAddPayDate.Location = new System.Drawing.Point(283, 30);
            this.cmdAddPayDate.Name = "cmdAddPayDate";
            this.cmdAddPayDate.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdAddPayDate.Size = new System.Drawing.Size(152, 48);
            this.cmdAddPayDate.TabIndex = 1;
            this.cmdAddPayDate.Text = "Add Pay Date";
            this.cmdAddPayDate.Click += new System.EventHandler(this.mnuAddPayDate_Click);
            // 
            // frmEditCheckDetail
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(748, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditCheckDetail";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Pay Records";
            this.Load += new System.EventHandler(this.frmEditCheckDetail_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditCheckDetail_KeyDown);
            this.Resize += new System.EventHandler(this.frmEditCheckDetail_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPayDate)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdAddPayDate;
    }
}
