﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMEUC1First.
	/// </summary>
	public partial class srptMEUC1First : FCSectionReport
	{
		public srptMEUC1First()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptMEUC1First InstancePtr
		{
			get
			{
				return (srptMEUC1First)Sys.GetInstance(typeof(srptMEUC1First));
			}
		}

		protected srptMEUC1First _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                theReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMEUC1First	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolPrintTest;
		double dblRate;
		double dblLimit;
		string strSeq = "";
		int intQuarterCovered;
		int lngYearCovered;
		cUC1Report theReport;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intQuarterCovered = 0;
			theReport = (this.ParentReport as rptMEUC1).UCReportObject;
			boolPrintTest = (this.ParentReport as rptMEUC1).IsTestPrint;
			string strTemp = "";
			if (!boolPrintTest)
			{
				txtMaineLicense.Visible = false;
				txtEIN.Visible = false;
				txtDate.Text = Strings.Format(DateTime.Today, "mm dd  yyyy");
				txtAddress.Text = fecherFoundation.Strings.UCase(theReport.Summary.Address);
				txtCity.Text = fecherFoundation.Strings.UCase(theReport.Summary.City);
				txtState.Text = fecherFoundation.Strings.UCase(theReport.Summary.State);
				txtZip.Text = theReport.Summary.zip;
				txtName.Text = fecherFoundation.Strings.UCase(theReport.Summary.Name);
				txtUCEmployerAccount.Text = theReport.Summary.UCAccountID;
				txtFederalEmployerID.Text = theReport.Summary.FederalID;
				txtEMail.Text = theReport.Transmitter.Email;
				strTemp = Strings.Mid(strTemp, 1, 3) + " " + Strings.Mid(strTemp, 4, 3) + " " + Strings.Mid(strTemp, 7);
				txtPhone.Text = strTemp;
				intQuarterCovered = theReport.Quarter;
				Barcode1.Text = "2006400";
				txtQuarterNum.Text = intQuarterCovered.ToString();
				switch (intQuarterCovered)
				{
					case 1:
						{
							txtPeriodStart.Text = "01 01";
							txtPeriodEnd.Text = "03 31";
							break;
						}
					case 2:
						{
							txtPeriodStart.Text = "04 01";
							txtPeriodEnd.Text = "06 30";
							break;
						}
					case 3:
						{
							txtPeriodStart.Text = "07 01";
							txtPeriodEnd.Text = "09 30";
							break;
						}
					case 4:
						{
							txtPeriodStart.Text = "10 01";
							txtPeriodEnd.Text = "12 31";
							break;
						}
				}
				//end switch
				txtPeriodStart.Text = txtPeriodStart.Text + "   " + FCConvert.ToString(theReport.YearCovered);
				txtPeriodEnd.Text = txtPeriodEnd.Text + "   " + FCConvert.ToString(theReport.YearCovered);
				cUC1BarCodeInfo bcInfo;
				cUC1Controller ucCont = new cUC1Controller();
				bcInfo = ucCont.GetBarCodeInfoFromSummary(theReport.Summary);
				bcInfo.CSSFAssess = theReport.Summary.CSSFAssess;
                bcInfo.UPAFAssess = theReport.Summary.UPAFAssess;
				// for now don't show the 2d barcode
				// Dim strCode As String
				// strCode = bcInfo.Get2DBarCodeString
				// Dim hBC As Long
				// Dim errCode As Integer
				// hBC = CreateBarCode(BC_PDF417, 0, 0&, 4, 150, 1, "Arial", 24, 0, 0, 30, 0, 4)
				// errCode = DrawBarCodeToClipboard(300, 300, hBC, strCode, "")
				// If errCode = 0 Then
				// Image1.Picture = Clipboard.GetData
				// End If
				// Call DeleteBarCode(hBC)
				txtYear.Text = FCConvert.ToString(theReport.YearCovered);
				GetTotalsFromRecords();
                ucCont = null;
            }
			else
			{
				txtState.Text = "XX";
                txtUPAFRate.Text = ".9999";
                txtCSSFRate.Text = ".9999";
            }
		}

		private void GetTotalsFromRecords()
		{
			double dblTotStateWH;
			// Dim dblPayments As Double
			//clsDRWrapper clsTemp = new clsDRWrapper();
			double dblWHDue;
			string strWhere = "";
			double dblTotalGross;
			double dblTotalExcess;
			double dblTaxWagePaid;
			double dblRate;
			double dblContributionsDue;
			double dblCSSFRate;
            double dblUPAFRate;
			double dblUCDue;
			double dblCSSFAssessment;
            double dblUPAFAssessment;
			double lngWhole;
			int lngWholeTaxWagePaid;
			double dblCentsTaxWagePaid;
			double dblCents;
			double dblTemp;
			txtFirstMo.Text = theReport.Summary.EmployeeCountMonth1.ToString();
			txtSecMo.Text = theReport.Summary.EmployeeCountMonth2.ToString();
			txtThirdMo.Text = theReport.Summary.EmployeeCountMonth3.ToString();
			txtFemalesFirstMo.Text = theReport.Summary.FemaleCountMonth1.ToString();
			txtFemalesSecMo.Text = theReport.Summary.FemaleCountMonth2.ToString();
			txtFemalesThirdMo.Text = theReport.Summary.FemaleCountMonth3.ToString();
			dblTotalGross = theReport.Summary.UCWages;
			dblTotalExcess = theReport.Summary.ExcessWages;
			dblRate = theReport.Summary.UCRate;
			dblCSSFRate = theReport.CSSFRate;
            dblUPAFRate = theReport.UPAFRate;
			// dblPayments = Val(.dblTotalPayments)
			dblUCDue = theReport.Summary.UCDue;
			dblCSSFAssessment = theReport.Summary.CSSFAssess;
            dblUPAFAssessment = theReport.Summary.UPAFAssess;
			dblContributionsDue = theReport.Summary.TotalContCSSFUPAFDue;
			dblTaxWagePaid = theReport.Summary.TaxableWages;
			lngWhole = Conversion.Int(dblTotalGross);
			dblCents = (dblTotalGross - lngWhole) * 100;
			if (dblCents < 1)
				dblCents = 0;
			txtUnemploymentCompensationGrossWages.Text = FCConvert.ToString(lngWhole);
			txtUnemploymentCompensationGrossCents.Text = Strings.Format(dblCents, "00");
			lngWhole = Conversion.Int(dblTotalExcess);
			dblCents = (dblTotalExcess - lngWhole) * 100;
			if (dblCents < 1)
				dblCents = 0;
			txtExcessWagesCents.Text = Strings.Format(dblCents, "00");
			txtExcessWages.Text = FCConvert.ToString(lngWhole);
			lngWhole = Conversion.Int(dblTaxWagePaid);
			dblCents = (dblTaxWagePaid - lngWhole) * 100;
			if (dblCents < 1)
			{
				dblCents = 0;
			}
			txtTaxableWagesPaid.Text = FCConvert.ToString(lngWhole);
			txtTaxableWagesPaidCents.Text = Strings.Format(dblCents, "00");
			lngWhole = Conversion.Int(dblUCDue);
			dblCents = (dblUCDue - lngWhole) * 100;
			if (dblCents < 1)
				dblCents = 0;
			txtContributionsDue.Text = FCConvert.ToString(lngWhole);
			txtContributionsDueCents.Text = Strings.Format(dblCents, "00");
			lngWhole = Conversion.Int(dblCSSFAssessment);
			dblCents = (dblCSSFAssessment - lngWhole) * 100;
			if (dblCents < 1)
				dblCents = 0;
			txtCSSFAssessment.Text = FCConvert.ToString(lngWhole);
			txtCSSFAssessmentCents.Text = Strings.Format(dblCents, "00");

            lngWhole = Conversion.Int(dblUPAFAssessment);
            dblCents = (dblUPAFAssessment - lngWhole) * 100;
            if (dblCents < 1)
                dblCents = 0;
            txtUPAFAssessment.Text = FCConvert.ToString(lngWhole);
            txtUPAFAssessmentCents.Text = Strings.Format(dblCents, "00");
			if (Strings.Right(Strings.Format(dblRate, ".00000"), 1) != "0")
			{
				txtLine9.Text = Strings.Format(dblRate, ".00000");
			}
			else
			{
				txtLine9.Text = Strings.Format(dblRate, ".0000");
			}
			if (Strings.Right(Strings.Format(dblCSSFRate, ".00000"), 1) != "0")
			{
				txtCSSFRate.Text = Strings.Format(dblCSSFRate, ".00000");
			}
			else
			{
				txtCSSFRate.Text = Strings.Format(dblCSSFRate, ".0000");
			}

            if (Strings.Right(Strings.Format(dblUPAFRate, ".00000"), 1) != "0")
            {
                txtUPAFRate.Text = Strings.Format(dblUPAFRate, ".00000");
            }
            else
            {
                txtUPAFRate.Text = Strings.Format(dblUPAFRate, ".0000");
            }
			lngWhole = Conversion.Int(dblContributionsDue);
			dblCents = (dblContributionsDue - lngWhole) * 100;
			if (dblCents < 1)
				dblCents = 0;
			txtTotalDue.Text = FCConvert.ToString(lngWhole);
			txtTotalDueCents.Text = Strings.Format(dblCents, "00");
		}

        private void Detail_Format(object sender, EventArgs e)
        {

        }
    }
}
