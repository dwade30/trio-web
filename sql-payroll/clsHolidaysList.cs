//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsHolidaysList
	{
		//=========================================================
		private clsHoliday[] HolList = null;
		private int intCurrentHoliday;
		// vbPorter upgrade warning: hType As clsHoliday.HolidayType	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intWeekDay As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngShiftID As int	OnWriteFCConvert.ToDouble(
		public bool AddHoliday(string strName, clsHoliday.HolidayType hType, int intMonth = 0, int intDay = 0, int intWeekDay = 0, int lngID = 0, int lngShiftID = 0)
		{
			bool AddHoliday = false;
			// if not type of easter then intMonth must be specified as does intDay.  intWeekDay is also required if it is not a type specific date
			AddHoliday = false;
			// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
			int intindex = 0;
			if (!FCUtils.IsEmpty(HolList))
			{
				intindex = Information.UBound(HolList, 1);
				if (intindex < 0)
				{
					intindex = 0;
				}
				Array.Resize(ref HolList, intindex + 1 + 1);
				intindex = Information.UBound(HolList, 1);
			}
			else
			{
				HolList = new clsHoliday[1 + 1];
				intindex = 0;
			}
			HolList[intindex] = new clsHoliday();
			HolList[intindex].ID = lngID;
			HolList[intindex].DayNum = intDay;
			HolList[intindex].MonthNum = intMonth;
			HolList[intindex].TypeofHoliday = hType;
			HolList[intindex].HolidayName = strName;
			HolList[intindex].ShiftType = lngShiftID;
			intCurrentHoliday = intindex;
			if (intWeekDay > 0)
			{
				HolList[intindex].DayOfWeek = intWeekDay;
			}
			AddHoliday = true;
			return AddHoliday;
		}

		public bool InsertHoliday(ref clsHoliday thol)
		{
			bool InsertHoliday = false;
			InsertHoliday = false;
			if (AddHoliday(thol.HolidayName, thol.TypeofHoliday, thol.MonthNum, thol.DayNum, thol.DayOfWeek, thol.ID, thol.ShiftType))
			{
				InsertHoliday = true;
			}
			return InsertHoliday;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentHoliday;
			return GetCurrentIndex;
		}

		public bool LoadHolidays()
		{
			bool LoadHolidays = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadHolidays = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select * from Holidays";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				FCUtils.EraseSafe(HolList);
				while (!rsLoad.EndOfFile())
				{
					if (!AddHoliday(rsLoad.Get_Fields_String("Name"), (clsHoliday.HolidayType)rsLoad.Get_Fields_Int16("HolidayType"), FCConvert.ToInt16(Conversion.Val(rsLoad.Get_Fields_Int16("MonthOf"))), FCConvert.ToInt16(Conversion.Val(rsLoad.Get_Fields_Int16("DayOf"))), FCConvert.ToInt16(Conversion.Val(rsLoad.Get_Fields_Int16("WeekDay"))), rsLoad.Get_Fields("ID"), FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("shift")))))
					{
						MessageBox.Show("Error loading holiday " + rsLoad.Get_Fields_String("Name"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return LoadHolidays;
					}
					rsLoad.MoveNext();
				}
				MoveFirst();
				LoadHolidays = true;
				return LoadHolidays;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadHolidays", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadHolidays;
		}

		public bool SaveHolidays()
		{
			bool SaveHolidays = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveHolidays = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				if (!FCUtils.IsEmpty(HolList))
				{
					// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
					int X;
					for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
					{
						if (!(HolList[X] == null))
						{
							if (HolList[X].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
							{
								// save it
								if (HolList[X].ID > 0)
								{
									strSQL = "select * from holidays where ID = " + FCConvert.ToString(HolList[X].ID);
									rsSave.OpenRecordset(strSQL, "TWPY0000.vb1");
									if (!rsSave.EndOfFile())
									{
										rsSave.Edit();
									}
									else
									{
										rsSave.AddNew();
									}
								}
								else
								{
									strSQL = "select * from holidays where ID = -1";
									rsSave.OpenRecordset(strSQL, "TWPY0000.vb1");
									rsSave.AddNew();
								}
								rsSave.Set_Fields("Name", HolList[X].HolidayName);
								rsSave.Set_Fields("HolidayType", HolList[X].TypeofHoliday);
								rsSave.Set_Fields("MonthOf", HolList[X].MonthNum);
								rsSave.Set_Fields("DayOf", HolList[X].DayNum);
								rsSave.Set_Fields("WeekDay", HolList[X].DayOfWeek);
								rsSave.Set_Fields("Shift", HolList[X].ShiftType);
								rsSave.Update();
								HolList[X].ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							}
							else
							{
								// delete it
								if (HolList[X].ID > 0)
								{
									rsSave.Execute("DELETE FROM holidays where ID = " + FCConvert.ToString(HolList[X].ID), "twpy0000.vb1");
								}
								HolList[X].ID = 0;
							}
						}
					}
					// X
				}
				MoveFirst();
				SaveHolidays = true;
				return SaveHolidays;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveHolidays", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveHolidays;
		}

		public bool IsAHoliday(DateTime dtDate)
		{
			bool IsAHoliday = false;
			IsAHoliday = false;
			if (!FCUtils.IsEmpty(HolList))
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (HolList[X].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
						{
							if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, HolList[X].Get_DateFallsOn(dtDate.Year)) == 0)
							{
								IsAHoliday = true;
								break;
							}
						}
					}
				}
				// X
			}
			else
			{
			}
			return IsAHoliday;
		}

		public clsHoliday GetHolidayByDate(DateTime dtDate)
		{
			clsHoliday GetHolidayByDate = null;
			clsHoliday thol;
			thol = null;
			if (!FCUtils.IsEmpty(HolList))
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (HolList[X].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
						{
							if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, HolList[X].Get_DateFallsOn(dtDate.Year)) == 0)
							{
								thol = HolList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetHolidayByDate = thol;
			return GetHolidayByDate;
		}

		public void DeleteHolidayByID(ref int lngID)
		{
			if (!FCUtils.IsEmpty(HolList) && lngID > 0)
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (HolList[X].ID == lngID)
						{
							HolList[X].TypeofHoliday = clsHoliday.HolidayType.NotUsed;
							break;
						}
					}
				}
				// X
			}
		}

		public clsHoliday GetHolidayByID(ref int lngID)
		{
			clsHoliday GetHolidayByID = null;
			clsHoliday thol;
			thol = null;
			if (!FCUtils.IsEmpty(HolList) && lngID > 0)
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (HolList[X].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
						{
							if (HolList[X].ID == lngID)
							{
								thol = HolList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetHolidayByID = thol;
			return GetHolidayByID;
		}

		public void DeleteHolidayByName(ref string strName)
		{
			if (!FCUtils.IsEmpty(HolList) && strName != "")
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (fecherFoundation.Strings.UCase(HolList[X].HolidayName) == fecherFoundation.Strings.UCase(strName))
						{
							HolList[X].TypeofHoliday = clsHoliday.HolidayType.NotUsed;
							break;
						}
					}
				}
				// X
			}
		}

		public clsHoliday GetHolidayByName(ref string strName)
		{
			clsHoliday GetHolidayByName = null;
			clsHoliday thol;
			thol = null;
			if (!FCUtils.IsEmpty(HolList) && strName != "")
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(HolList, 1)); X++)
				{
					if (!(HolList[X] == null))
					{
						if (HolList[X].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
						{
							if (fecherFoundation.Strings.UCase(HolList[X].HolidayName) == fecherFoundation.Strings.UCase(strName))
							{
								thol = HolList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetHolidayByName = thol;
			return GetHolidayByName;
		}

		public clsHoliday GetCurrentHoliday()
		{
			clsHoliday GetCurrentHoliday = null;
			clsHoliday thol;
			thol = null;
			if (!FCUtils.IsEmpty(HolList))
			{
				if (intCurrentHoliday >= 0)
				{
					if (!(HolList[intCurrentHoliday] == null))
					{
						thol = HolList[intCurrentHoliday];
					}
				}
			}
			GetCurrentHoliday = thol;
			return GetCurrentHoliday;
		}
		// vbPorter upgrade warning: intindex As int	OnWrite(string)
		public clsHoliday GetHolidayByIndex(int intindex)
		{
			clsHoliday GetHolidayByIndex = null;
			clsHoliday thol;
			thol = null;
			if (!FCUtils.IsEmpty(HolList) && intindex >= 0)
			{
				if (intindex <= Information.UBound(HolList, 1))
				{
					if (!(HolList[intindex] == null))
					{
						if (HolList[intindex].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
						{
							thol = HolList[intindex];
						}
					}
				}
			}
			GetHolidayByIndex = thol;
			return GetHolidayByIndex;
		}
		// vbPorter upgrade warning: intindex As int	OnWrite(string)
		public void DeleteHolidayByIndex(int intindex)
		{
			if (!FCUtils.IsEmpty(HolList) && intindex >= 0)
			{
				if (intindex <= Information.UBound(HolList, 1))
				{
					if (!(HolList[intindex] == null))
					{
						HolList[intindex].TypeofHoliday = clsHoliday.HolidayType.NotUsed;
					}
				}
			}
		}

		public bool MoveFirst()
		{
			bool MoveFirst = false;
			// returns false if there is no first record
			MoveFirst = false;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp;
			intCurrentHoliday = -1;
			if (!FCUtils.IsEmpty(HolList))
			{
				for (intTemp = 0; intTemp <= (Information.UBound(HolList, 1)); intTemp++)
				{
					if (!(HolList[intTemp] == null))
					{
						intCurrentHoliday = intTemp;
						MoveFirst = true;
						break;
					}
				}
				// intTemp
			}
			return MoveFirst;
		}

		public bool MoveNext()
		{
			bool MoveNext = false;
			MoveNext = false;
			if (!FCUtils.IsEmpty(HolList))
			{
				int intTemp = 0;
				if (intCurrentHoliday < Information.UBound(HolList, 1))
				{
					intTemp = intCurrentHoliday + 1;
					while (HolList[intTemp] == null)
					{
						intTemp = intCurrentHoliday + 1;
						if (intTemp > Information.UBound(HolList, 1))
						{
							intCurrentHoliday = -1;
							return MoveNext;
						}
					}
					intCurrentHoliday = intTemp;
					MoveNext = true;
				}
				else
				{
					intCurrentHoliday = -1;
					return MoveNext;
				}
			}
			return MoveNext;
		}

		public bool MoveTo(ref int intindex)
		{
			bool MoveTo = false;
			MoveTo = false;
			if (!FCUtils.IsEmpty(HolList))
			{
				if (intindex >= 0)
				{
					if (intindex <= Information.UBound(HolList, 1))
					{
						if (!(HolList[intindex] == null))
						{
							if (HolList[intindex].TypeofHoliday != clsHoliday.HolidayType.NotUsed)
							{
								intCurrentHoliday = intindex;
								MoveTo = true;
							}
						}
					}
				}
			}
			return MoveTo;
		}

		public bool EndOfHolidays()
		{
			bool EndOfHolidays = false;
			if (!FCUtils.IsEmpty(HolList))
			{
				if (intCurrentHoliday >= 0)
				{
					EndOfHolidays = false;
				}
				else
				{
					EndOfHolidays = true;
				}
			}
			else
			{
				EndOfHolidays = true;
			}
			return EndOfHolidays;
		}
	}
}
