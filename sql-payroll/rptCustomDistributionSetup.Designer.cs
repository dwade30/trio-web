﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDistributionSetup.
	/// </summary>
	partial class rptCustomDistributionSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomDistributionSetup));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayCat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaseRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxNumPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDefaultHrs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHrsThisWeek = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtU = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayCat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxNumPer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultHrs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHrsThisWeek)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtWC,
            this.txtDistNumber,
            this.txtPayCat,
            this.txtTaxStatus,
            this.txtPayCD,
            this.txtBaseRate,
            this.txtTaxNumPer,
            this.txtDefaultHrs,
            this.txtHrsThisWeek,
            this.txtU,
            this.txtCode,
            this.txtM});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 1.6875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtAccount.Text = "Field1";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.1875F;
			// 
			// txtWC
			// 
			this.txtWC.Height = 0.1666667F;
			this.txtWC.Left = 5.875F;
			this.txtWC.Name = "txtWC";
			this.txtWC.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtWC.Text = "Field1";
			this.txtWC.Top = 0F;
			this.txtWC.Width = 0.4375F;
			// 
			// txtDistNumber
			// 
			this.txtDistNumber.Height = 0.1666667F;
			this.txtDistNumber.Left = 0.21875F;
			this.txtDistNumber.Name = "txtDistNumber";
			this.txtDistNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDistNumber.Text = "Field1";
			this.txtDistNumber.Top = 0F;
			this.txtDistNumber.Width = 0.3125F;
			// 
			// txtPayCat
			// 
			this.txtPayCat.Height = 0.19F;
			this.txtPayCat.Left = 2.90625F;
			this.txtPayCat.Name = "txtPayCat";
			this.txtPayCat.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtPayCat.Text = "Field1";
			this.txtPayCat.Top = 0F;
			this.txtPayCat.Width = 0.90625F;
			// 
			// txtTaxStatus
			// 
			this.txtTaxStatus.Height = 0.19F;
			this.txtTaxStatus.Left = 3.875F;
			this.txtTaxStatus.Name = "txtTaxStatus";
			this.txtTaxStatus.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtTaxStatus.Text = "Field1";
			this.txtTaxStatus.Top = 0F;
			this.txtTaxStatus.Width = 0.4375F;
			// 
			// txtPayCD
			// 
			this.txtPayCD.Height = 0.1666667F;
			this.txtPayCD.Left = 6.5F;
			this.txtPayCD.Name = "txtPayCD";
			this.txtPayCD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtPayCD.Text = "Field1";
			this.txtPayCD.Top = 0F;
			this.txtPayCD.Width = 0.6875F;
			// 
			// txtBaseRate
			// 
			this.txtBaseRate.Height = 0.1666667F;
			this.txtBaseRate.Left = 7.1875F;
			this.txtBaseRate.Name = "txtBaseRate";
			this.txtBaseRate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtBaseRate.Text = "Field1";
			this.txtBaseRate.Top = 0F;
			this.txtBaseRate.Width = 0.6875F;
			// 
			// txtTaxNumPer
			// 
			this.txtTaxNumPer.Height = 0.1666667F;
			this.txtTaxNumPer.Left = 7.9375F;
			this.txtTaxNumPer.Name = "txtTaxNumPer";
			this.txtTaxNumPer.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTaxNumPer.Text = "Field1";
			this.txtTaxNumPer.Top = 0F;
			this.txtTaxNumPer.Width = 0.5F;
			// 
			// txtDefaultHrs
			// 
			this.txtDefaultHrs.Height = 0.1666667F;
			this.txtDefaultHrs.Left = 8.46875F;
			this.txtDefaultHrs.Name = "txtDefaultHrs";
			this.txtDefaultHrs.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDefaultHrs.Text = "Field1";
			this.txtDefaultHrs.Top = 0F;
			this.txtDefaultHrs.Width = 0.53125F;
			// 
			// txtHrsThisWeek
			// 
			this.txtHrsThisWeek.Height = 0.1666667F;
			this.txtHrsThisWeek.Left = 9.125F;
			this.txtHrsThisWeek.Name = "txtHrsThisWeek";
			this.txtHrsThisWeek.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtHrsThisWeek.Text = "Field1";
			this.txtHrsThisWeek.Top = 0F;
			this.txtHrsThisWeek.Width = 0.59375F;
			// 
			// txtU
			// 
			this.txtU.Height = 0.1666667F;
			this.txtU.Left = 5.375F;
			this.txtU.Name = "txtU";
			this.txtU.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtU.Text = "Field1";
			this.txtU.Top = 0F;
			this.txtU.Width = 0.40625F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.1666667F;
			this.txtCode.Left = 0.5625F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCode.Text = "Field1";
			this.txtCode.Top = 0F;
			this.txtCode.Width = 1.0625F;
			// 
			// txtM
			// 
			this.txtM.Height = 0.1666667F;
			this.txtM.Left = 4.46875F;
			this.txtM.Name = "txtM";
			this.txtM.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtM.Text = "Field1";
			this.txtM.Top = 0F;
			this.txtM.Width = 0.5F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtTime,
            this.txtPage,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field11,
            this.Line1,
            this.Field13,
            this.Field14,
            this.Field15});
			this.PageHeader.Height = 0.875F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: center";
			this.Label1.Text = "PAYROLL -  DISTRIBUTION SETUP REPORT";
			this.Label1.Top = 0F;
			this.Label1.Width = 9.6875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 8.375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.3125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.3125F;
			this.Field1.Left = 3.84375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Tax Status";
			this.Field1.Top = 0.53125F;
			this.Field1.Width = 0.53125F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 1.6875F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field2.Text = "Account";
			this.Field2.Top = 0.6666667F;
			this.Field2.Width = 0.53125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 2.90625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field3.Text = "Pay Cat";
			this.Field3.Top = 0.65625F;
			this.Field3.Width = 0.65625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1666667F;
			this.Field4.Left = 4.4375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field4.Text = "M";
			this.Field4.Top = 0.6666667F;
			this.Field4.Width = 0.28125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1666667F;
			this.Field5.Left = 6.46875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field5.Text = "Pay CD";
			this.Field5.Top = 0.6666667F;
			this.Field5.Width = 0.53125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2916667F;
			this.Field6.Left = 7.3125F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field6.Text = "Base Rate";
			this.Field6.Top = 0.5416667F;
			this.Field6.Width = 0.53125F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.2916667F;
			this.Field7.Left = 7.90625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field7.Text = "Tax # Per";
			this.Field7.Top = 0.5416667F;
			this.Field7.Width = 0.5F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.2916667F;
			this.Field8.Left = 8.46875F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field8.Text = "Default Hrs.";
			this.Field8.Top = 0.5416667F;
			this.Field8.Width = 0.53125F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2916667F;
			this.Field9.Left = 9.125F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field9.Text = "Hrs. This Week";
			this.Field9.Top = 0.5416667F;
			this.Field9.Width = 0.59375F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1666667F;
			this.Field11.Left = 4.4375F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field11.Text = "- - - - - - Include for - - - - - - ";
			this.Field11.Top = 0.5F;
			this.Field11.Width = 1.78125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.53125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.8333333F;
			this.Line1.Width = 9.21875F;
			this.Line1.X1 = 0.53125F;
			this.Line1.X2 = 9.75F;
			this.Line1.Y1 = 0.8333333F;
			this.Line1.Y2 = 0.8333333F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1666667F;
			this.Field13.Left = 5.34375F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field13.Text = "U";
			this.Field13.Top = 0.6666667F;
			this.Field13.Width = 0.28125F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1666667F;
			this.Field14.Left = 5.875F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field14.Text = "W/C";
			this.Field14.Top = 0.6666667F;
			this.Field14.Width = 0.34375F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1666667F;
			this.Field15.Left = 0.5625F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field15.Text = "Code";
			this.Field15.Top = 0.6666667F;
			this.Field15.Width = 0.53125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployee});
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0.1770833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.1666667F;
			this.txtEmployee.Left = 0.0625F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtEmployee.Text = "Field1";
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 4.65625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptCustomDistributionSetup
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 8.5F;
			this.PageSettings.PaperWidth = 11F;
			this.PrintWidth = 9.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayCat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxNumPer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultHrs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHrsThisWeek)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayCat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaseRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxNumPer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDefaultHrs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHrsThisWeek;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtU;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
