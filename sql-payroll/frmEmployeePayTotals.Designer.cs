//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeePayTotals.
	/// </summary>
	partial class frmEmployeePayTotals
	{
		public FCGrid vsPayTotals;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuMultiPays;
		public fecherFoundation.FCToolStripMenuItem mnuMultiAdd;
		public fecherFoundation.FCToolStripMenuItem mnuCaption1;
		public fecherFoundation.FCToolStripMenuItem mnuCaption2;
		public fecherFoundation.FCToolStripMenuItem mnuCaption3;
		public fecherFoundation.FCToolStripMenuItem mnuCaption4;
		public fecherFoundation.FCToolStripMenuItem mnuCaption5;
		public fecherFoundation.FCToolStripMenuItem mnuCaption6;
		public fecherFoundation.FCToolStripMenuItem mnuCaption7;
		public fecherFoundation.FCToolStripMenuItem mnuCaption8;
		public fecherFoundation.FCToolStripMenuItem mnuCaption9;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsPayTotals = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.mnuMultiAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption8 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCaption9 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMultiPays = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 515);
            this.BottomPanel.Size = new System.Drawing.Size(909, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsPayTotals);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Controls.Add(this.cmdNew);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Size = new System.Drawing.Size(909, 455);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Size = new System.Drawing.Size(909, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Pay Totals";
            // 
            // vsPayTotals
            // 
            this.vsPayTotals.AllowSelection = false;
            this.vsPayTotals.AllowUserToResizeColumns = false;
            this.vsPayTotals.AllowUserToResizeRows = false;
            this.vsPayTotals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayTotals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsPayTotals.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsPayTotals.BackColorBkg = System.Drawing.Color.Empty;
            this.vsPayTotals.BackColorFixed = System.Drawing.Color.Empty;
            this.vsPayTotals.BackColorSel = System.Drawing.Color.Empty;
            this.vsPayTotals.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsPayTotals.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsPayTotals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsPayTotals.ColumnHeadersHeight = 30;
            this.vsPayTotals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsPayTotals.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsPayTotals.DragIcon = null;
            this.vsPayTotals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsPayTotals.ExtendLastCol = true;
            this.vsPayTotals.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsPayTotals.FrozenCols = 0;
            this.vsPayTotals.GridColor = System.Drawing.Color.Empty;
            this.vsPayTotals.GridColorFixed = System.Drawing.Color.Empty;
            this.vsPayTotals.Location = new System.Drawing.Point(30, 59);
            this.vsPayTotals.Name = "vsPayTotals";
            this.vsPayTotals.OutlineCol = 0;
            this.vsPayTotals.ReadOnly = true;
            this.vsPayTotals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsPayTotals.RowHeightMin = 0;
            this.vsPayTotals.Rows = 50;
            this.vsPayTotals.ScrollTipText = null;
            this.vsPayTotals.ShowColumnVisibilityMenu = false;
            this.vsPayTotals.Size = new System.Drawing.Size(851, 384);
            this.vsPayTotals.StandardTab = true;
            this.vsPayTotals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsPayTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsPayTotals.TabIndex = 7;
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(414, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(80, 48);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(640, 67);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Visible = false;
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(572, 64);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 0;
            this.cmdNew.Text = "New";
            this.cmdNew.Visible = false;
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRefresh.AppearanceKey = "toolbarButton";
            this.cmdRefresh.Location = new System.Drawing.Point(726, 79);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
            this.cmdRefresh.TabIndex = 3;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Visible = false;
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 30);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(575, 15);
            this.lblEmployeeNumber.TabIndex = 6;
            this.lblEmployeeNumber.Tag = "Employee #";
            this.lblEmployeeNumber.Text = "EMPLOYEE #";
            // 
            // mnuMultiAdd
            // 
            this.mnuMultiAdd.Enabled = false;
            this.mnuMultiAdd.Index = 0;
            this.mnuMultiAdd.Name = "mnuMultiAdd";
            this.mnuMultiAdd.Text = "Add/Edit Additional Pay Master";
            // 
            // mnuCaption1
            // 
            this.mnuCaption1.Index = 1;
            this.mnuCaption1.Name = "mnuCaption1";
            this.mnuCaption1.Text = "Caption1";
            this.mnuCaption1.Click += new System.EventHandler(this.mnuCaption1_Click);
            // 
            // mnuCaption2
            // 
            this.mnuCaption2.Index = 2;
            this.mnuCaption2.Name = "mnuCaption2";
            this.mnuCaption2.Text = "Caption2";
            this.mnuCaption2.Click += new System.EventHandler(this.mnuCaption2_Click);
            // 
            // mnuCaption3
            // 
            this.mnuCaption3.Index = 3;
            this.mnuCaption3.Name = "mnuCaption3";
            this.mnuCaption3.Text = "Caption3";
            this.mnuCaption3.Click += new System.EventHandler(this.mnuCaption3_Click);
            // 
            // mnuCaption4
            // 
            this.mnuCaption4.Index = 4;
            this.mnuCaption4.Name = "mnuCaption4";
            this.mnuCaption4.Text = "Caption4";
            this.mnuCaption4.Click += new System.EventHandler(this.mnuCaption4_Click);
            // 
            // mnuCaption5
            // 
            this.mnuCaption5.Index = 5;
            this.mnuCaption5.Name = "mnuCaption5";
            this.mnuCaption5.Text = "Caption5";
            this.mnuCaption5.Click += new System.EventHandler(this.mnuCaption5_Click);
            // 
            // mnuCaption6
            // 
            this.mnuCaption6.Index = 6;
            this.mnuCaption6.Name = "mnuCaption6";
            this.mnuCaption6.Text = "Caption6";
            this.mnuCaption6.Click += new System.EventHandler(this.mnuCaption6_Click);
            // 
            // mnuCaption7
            // 
            this.mnuCaption7.Index = 7;
            this.mnuCaption7.Name = "mnuCaption7";
            this.mnuCaption7.Text = "Caption7";
            this.mnuCaption7.Click += new System.EventHandler(this.mnuCaption7_Click);
            // 
            // mnuCaption8
            // 
            this.mnuCaption8.Index = 8;
            this.mnuCaption8.Name = "mnuCaption8";
            this.mnuCaption8.Text = "Caption8";
            this.mnuCaption8.Click += new System.EventHandler(this.mnuCaption8_Click);
            // 
            // mnuCaption9
            // 
            this.mnuCaption9.Index = 9;
            this.mnuCaption9.Name = "mnuCaption9";
            this.mnuCaption9.Text = "Caption9";
            this.mnuCaption9.Click += new System.EventHandler(this.mnuCaption9_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelectEmployee,
            this.mnuSP1,
            this.mnuPrint,
            this.mnuSP3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = 0;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employees";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 3;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuMultiPays
            // 
            this.mnuMultiPays.Index = -1;
            this.mnuMultiPays.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuMultiAdd,
            this.mnuCaption1,
            this.mnuCaption2,
            this.mnuCaption3,
            this.mnuCaption4,
            this.mnuCaption5,
            this.mnuCaption6,
            this.mnuCaption7,
            this.mnuCaption8,
            this.mnuCaption9});
            this.mnuMultiPays.Name = "mnuMultiPays";
            this.mnuMultiPays.Text = "Multi Pays";
            this.mnuMultiPays.Visible = false;
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.AppearanceKey = "toolbarButton";
            this.cmdSelect.Location = new System.Drawing.Point(751, 26);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(130, 24);
            this.cmdSelect.TabIndex = 5;
            this.cmdSelect.Text = "Select Employees";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // frmEmployeePayTotals
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(909, 623);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeePayTotals";
            this.Text = "Pay Totals";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeePayTotals_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeePayTotals_KeyPress);
            this.Resize += new System.EventHandler(this.frmEmployeePayTotals_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdSelect;
	}
}