﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cFed941
	{
		//=========================================================
		private int lngRecordID;
		private int intQuarter;
		private int intReportYear;
		private double dblSickPayAdjustment;
		private double dblTipsAndLifeInsuranceAdjustment;
		private double dblCurrentYearIncomeWithholdingAdjust;
		private double dblPriorYearSSAndMedAdjustment;
		private double dblDeposits;
		private double dblCobraPayments;
		private int intCobraIndividuals;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int Quarter
		{
			set
			{
				intQuarter = value;
			}
			get
			{
				int Quarter = 0;
				Quarter = intQuarter;
				return Quarter;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public double SickPayAdjustments
		{
			set
			{
				dblSickPayAdjustment = value;
			}
			get
			{
				double SickPayAdjustments = 0;
				SickPayAdjustments = dblSickPayAdjustment;
				return SickPayAdjustments;
			}
		}

		public double TipsAndLifeAdjustments
		{
			set
			{
				dblTipsAndLifeInsuranceAdjustment = value;
			}
			get
			{
				double TipsAndLifeAdjustments = 0;
				TipsAndLifeAdjustments = dblTipsAndLifeInsuranceAdjustment;
				return TipsAndLifeAdjustments;
			}
		}

		public double CurrentYearIncomeTaxWHAdjust
		{
			set
			{
				dblCurrentYearIncomeWithholdingAdjust = value;
			}
			get
			{
				double CurrentYearIncomeTaxWHAdjust = 0;
				CurrentYearIncomeTaxWHAdjust = dblCurrentYearIncomeWithholdingAdjust;
				return CurrentYearIncomeTaxWHAdjust;
			}
		}

		public double PriorYearSSAndMedAdjustments
		{
			set
			{
				dblPriorYearSSAndMedAdjustment = value;
			}
			get
			{
				double PriorYearSSAndMedAdjustments = 0;
				PriorYearSSAndMedAdjustments = dblPriorYearSSAndMedAdjustment;
				return PriorYearSSAndMedAdjustments;
			}
		}

		public double Deposits
		{
			set
			{
				dblDeposits = value;
			}
			get
			{
				double Deposits = 0;
				Deposits = dblDeposits;
				return Deposits;
			}
		}

		public double CobraPayments
		{
			set
			{
				dblCobraPayments = value;
			}
			get
			{
				double CobraPayments = 0;
				CobraPayments = dblCobraPayments;
				return CobraPayments;
			}
		}

		public int CobraIndividuals
		{
			set
			{
				intCobraIndividuals = value;
			}
			get
			{
				int CobraIndividuals = 0;
				CobraIndividuals = intCobraIndividuals;
				return CobraIndividuals;
			}
		}
	}
}
