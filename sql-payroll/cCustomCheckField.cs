﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cCustomCheckField
	{
		//=========================================================
		private int lngRecordID;
		private string strDescription = string.Empty;
		private string strControlName = string.Empty;
		private string strCheckType = string.Empty;
		// vbPorter upgrade warning: boolInclude As string	OnWrite(bool)
		private string boolInclude = string.Empty;
		private double dblXPosition;
		private double dblYPosition;
		private double dblDefaultXPositon;
		private double dblDefaultYPosition;
		private int intFieldHeight;
		private int intFieldWidth;
		private int lngFormatID;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string ControlName
		{
			set
			{
				strControlName = value;
			}
			get
			{
				string ControlName = "";
				ControlName = strControlName;
				return ControlName;
			}
		}

		public string CheckType
		{
			set
			{
				strCheckType = value;
			}
			get
			{
				string CheckType = "";
				CheckType = strCheckType;
				return CheckType;
			}
		}
		// vbPorter upgrade warning: boolUse As bool	OnRead(string)
		public bool IncludeField
		{
			set
			{
				boolInclude = FCConvert.ToString(value);
			}
			// vbPorter upgrade warning: 'Return' As bool	OnWrite(string)
			get
			{
				bool IncludeField = false;
				IncludeField = FCConvert.CBool(boolInclude);
				return IncludeField;
			}
		}

		public double XPosition
		{
			set
			{
				dblXPosition = value;
			}
			get
			{
				double XPosition = 0;
				XPosition = dblXPosition;
				return XPosition;
			}
		}

		public double YPosition
		{
			set
			{
				dblYPosition = value;
			}
			get
			{
				double YPosition = 0;
				YPosition = dblYPosition;
				return YPosition;
			}
		}

		public double DefaultXPosition
		{
			set
			{
				dblDefaultXPositon = value;
			}
			get
			{
				double DefaultXPosition = 0;
				DefaultXPosition = dblDefaultXPositon;
				return DefaultXPosition;
			}
		}

		public double DefaultYPosition
		{
			set
			{
				dblDefaultYPosition = value;
			}
			get
			{
				double DefaultYPosition = 0;
				DefaultYPosition = dblDefaultYPosition;
				return DefaultYPosition;
			}
		}

		public int FieldHeight
		{
			set
			{
				intFieldHeight = value;
			}
			get
			{
				int FieldHeight = 0;
				FieldHeight = intFieldHeight;
				return FieldHeight;
			}
		}

		public int FieldWidth
		{
			set
			{
				intFieldWidth = value;
			}
			get
			{
				int FieldWidth = 0;
				FieldWidth = intFieldWidth;
				return FieldWidth;
			}
		}

		public int FormatID
		{
			set
			{
				lngFormatID = value;
			}
			get
			{
				int FormatID = 0;
				FormatID = lngFormatID;
				return FormatID;
			}
		}
	}
}
