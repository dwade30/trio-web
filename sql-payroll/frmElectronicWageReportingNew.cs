//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmElectronicWageReporting : BaseForm
	{
		public frmElectronicWageReporting()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_9, 9);
			this.Label1.AddControlArrayElement(Label1_10, 10);
			this.Label1.AddControlArrayElement(Label1_11, 11);
			this.Label1.AddControlArrayElement(Label1_12, 12);
			this.Label1.AddControlArrayElement(Label1_13, 13);
			this.Label1.AddControlArrayElement(Label1_14, 14);
			this.cmbEndDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_0, 0);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_1, 1);
			this.cmbEndDate.AddControlArrayElement(cmbEndDate_2, 2);
			this.cmbStartDate = new System.Collections.Generic.List<FCComboBox>();
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_0, 0);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_1, 1);
			this.cmbStartDate.AddControlArrayElement(cmbStartDate_2, 2);
			this.txtStartDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtStartDate.AddControlArrayElement(txtStartDate_0, 0);
			this.txtStartDate.AddControlArrayElement(txtStartDate_1, 1);
			this.txtStartDate.AddControlArrayElement(txtStartDate_2, 2);
			this.txtEndDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtEndDate.AddControlArrayElement(txtEndDate_0, 0);
			this.txtEndDate.AddControlArrayElement(txtEndDate_1, 1);
			this.txtEndDate.AddControlArrayElement(txtEndDate_2, 2);
			this.lblTo = new System.Collections.Generic.List<FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblFrom = new System.Collections.Generic.List<FCLabel>();
			this.lblFrom.AddControlArrayElement(lblFrom_0, 0);
			this.lblFrom.AddControlArrayElement(lblFrom_1, 1);
			this.lblFrom.AddControlArrayElement(lblFrom_2, 2);
			this.lblMonth = new System.Collections.Generic.List<FCLabel>();
			this.lblMonth.AddControlArrayElement(lblMonth_0, 0);
			this.lblMonth.AddControlArrayElement(lblMonth_1, 1);
			this.lblMonth.AddControlArrayElement(lblMonth_2, 2);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmElectronicWageReporting InstancePtr
		{
			get
			{
				return (frmElectronicWageReporting)Sys.GetInstance(typeof(frmElectronicWageReporting));
			}
		}

		protected frmElectronicWageReporting _InstancePtr = null;
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		int lngYearCovered;
		int intQuarterCovered;
		int intLastMonthInQuarter;
		clsDRWrapper clsEmployees = new clsDRWrapper();
		clsDRWrapper clsPayments = new clsDRWrapper();
		double dblCredit;
		double dblTotalExcess;
		double dblTotalWages;
		double dblTotalWH;
		double dblTotalGrossWages;
		double dblLimit;
		double dblRate;
		double dblSchoolRate;
		clsDRWrapper clsMonth1 = new clsDRWrapper();
		clsDRWrapper clsMonth2 = new clsDRWrapper();
		clsDRWrapper clsMonth3 = new clsDRWrapper();
		int lngFemsM1;
		int lngFemsM2;
		int lngFemsM3;
		int lngM1;
		int lngM2;
		int lngM3;
		double dblRemitted;
		string strSequence;
		bool boolReportNotElectronic;
		int lngTotWithholdableEmployees;
		string strLastEmployee = "";
		string strQTDWhere = "";
		string strYTDWhere = "";
		double dblCSSFRate;
        double dblUPAFRate;
		private string strMatch1List = string.Empty;
        internal List<string> batchReports;

        private void frmElectronicWageReporting_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmElectronicWageReporting_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
		}

		public void Init(int intQCovered, int lngYCovered, string strSeq = "", bool boolNotElectronic = false)
		{
			boolReportNotElectronic = boolNotElectronic;
			strMatch1List = modCoreysSweeterCode.GetBox1Matches();
			strSequence = strSeq;
			lngYearCovered = lngYCovered;
			intQuarterCovered = intQCovered;
			switch (intQuarterCovered)
			{
				case 1:
					{
						intLastMonthInQuarter = 3;
						break;
					}
				case 2:
					{
						intLastMonthInQuarter = 6;
						break;
					}
				case 3:
					{
						intLastMonthInQuarter = 9;
						break;
					}
				case 4:
					{
						intLastMonthInQuarter = 12;
						break;
					}
			}

			if (boolReportNotElectronic)
			{
				framFile.Visible = false;
			}

            this.Show(FCForm.FormShowEnum.Modal);

        }

		private void frmElectronicWageReporting_Load(object sender, System.EventArgs e)
		{
			int intTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, true);
			if (boolReportNotElectronic)
			{
				this.Text = "Wage Reporting";
			}
			FillDateCombos();
			LoadInfo();
			ShowInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			clsLoad.OpenRecordset("select upafrate,unemploymentrate,unemploymentbase,cssfrate from tblStandardLimits", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				dblRate = Conversion.Val(clsLoad.Get_Fields("unemploymentrate")) / 100;
				dblCSSFRate = Conversion.Val(clsLoad.Get_Fields("cssfrate")) / 100;
                dblUPAFRate = Conversion.Val(clsLoad.Get_Fields("upafrate")) / 100;
				dblSchoolRate = dblRate;
				dblLimit = Conversion.Val(clsLoad.Get_Fields("unemploymentbase"));
			}
			else
			{
				dblRate = 0;
				dblSchoolRate = 0;
				dblCSSFRate = 0;
                dblUPAFRate = 0;
				dblLimit = 7000;
			}
			strSQL = "select * from tblemployerinfo";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
				EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
				EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
				EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
				EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
				EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
				EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				EWRRecord.ProcessorLicenseCode = FCConvert.ToString(clsLoad.Get_Fields_String("ProcessorLicenseCode"));
				EWRRecord.TransmitterEmail = FCConvert.ToString(clsLoad.Get_Fields("transmitteremail"));
			}
			else
			{
				EWRRecord.EmployerAddress = "";
				EWRRecord.EmployerCity = "";
				EWRRecord.EmployerName = "";
				EWRRecord.EmployerState = "ME";
				EWRRecord.EmployerStateCode = 23;
				EWRRecord.EmployerZip = "";
				EWRRecord.EmployerZip4 = "";
				EWRRecord.FederalEmployerID = "";
				EWRRecord.MRSWithholdingID = "";
				EWRRecord.StateUCAccount = "";
				EWRRecord.TransmitterExt = "";
				EWRRecord.TransmitterPhone = "0000000000";
				EWRRecord.TransmitterTitle = "";
				EWRRecord.TransmitterEmail = "";
			}
			txtFilename.Text = "rtnwageUC1";
			if (!clsLoad.EndOfFile())
			{
				txtSeasonalCode.Text = FCConvert.ToString(clsLoad.Get_Fields("seasonalcode"));
				if (Information.IsDate(clsLoad.Get_Fields("seasonalfrom")))
				{
					T2KSeasonalFrom.Text = Strings.Format(clsLoad.Get_Fields("seasonalfrom"), "MM/dd/yyyy");
				}
				if (Information.IsDate(clsLoad.Get_Fields("seasonalTO")))
				{
					T2KSeasonalTo.Text = Strings.Format(clsLoad.Get_Fields("seasonalTo"), "MM/dd/yyyy");
				}
			}
		}

		private void ShowInfo()
		{
			string strTemp = "";
			object strPhone = "";
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtTemp;
			// try to fill in initial range boxes
			// get the paydates found in tblcheckdetail that cover the 12th for each month in quarter
			for (x = 0; x <= 2; x++)
			{
				strSQL = "select * from tblemployerinfo";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "startdate")) && Convert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "startdate")).ToOADate() != 0)
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "startdate");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								// txtStartDate(x).Text = Format(dtTemp, "MM/dd/yyyy")
								cmbStartDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
					if (Information.IsDate(clsLoad.Get_Fields("Month" + FCConvert.ToString(x + 1) + "EndDate")) && Convert.ToDateTime(clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "enddate")).ToOADate() != 0)
					{
						dtTemp = (DateTime)clsLoad.Get_Fields("month" + FCConvert.ToString(x + 1) + "enddate");
						if (dtTemp.Month == intLastMonthInQuarter + x - 2)
						{
							if (dtTemp.Year == lngYearCovered)
							{
								// txtEndDate(x).Text = Format(dtTemp, "MM/dd/yyyy")
								cmbEndDate[x].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
							}
						}
					}
				}
			}
			// x
			txtCity.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerCity);
			txtEmployerName.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerName);
			txtExtension.Text = EWRRecord.TransmitterExt;
			txtFederalID.Text = EWRRecord.FederalEmployerID;
			txtMRSID.Text = EWRRecord.MRSWithholdingID;
			txtState.Text = EWRRecord.EmployerState;
			txtStreetAddress.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerAddress);
			txtTitle.Text = fecherFoundation.Strings.Trim(EWRRecord.TransmitterTitle);
			txtUCAccount.Text = EWRRecord.StateUCAccount;
			txtZip.Text = EWRRecord.EmployerZip;
			txtZip4.Text = EWRRecord.EmployerZip4;
			strPhone = EWRRecord.TransmitterPhone;
			strPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strPhone), 10));
			txtPhone.Text = "(" + Strings.Mid(strPhone.ToStringES(), 1, 3) + ")" + Strings.Mid(strPhone.ToStringES(), 4, 3) + "-" + Strings.Mid(strPhone.ToStringES(), 7);
			txtProcessorLicenseCode.Text = EWRRecord.ProcessorLicenseCode;
			txtEmail.Text = EWRRecord.TransmitterEmail;
		}

		private bool CheckRange()
		{
			bool CheckRange = false;
			int x;
			CheckRange = false;
			for (x = 0; x <= 2; x++)
			{
				if (!Information.IsDate(cmbStartDate[x].Text))
				{
					MessageBox.Show("You must enter a valid start date for each month", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cmbStartDate[x].Focus();
					return CheckRange;
				}
			}
			// x
			CheckRange = true;
			return CheckRange;
		}

		private cUC1Report GetReportInstance()
		{
			cUC1Report GetReportInstance = null;
			cUC1Report ucRep = new cUC1Report();
			int x;
			dblCredit = Conversion.Val(txtCredit.Text);
			dblRemitted = Conversion.Val(txtRemitted.Text);
			ucRep.CSSFRate = dblCSSFRate;
            ucRep.UPAFRate = dblUPAFRate;
			ucRep.Limit = dblLimit;
			ucRep.Quarter = intQuarterCovered;
			ucRep.UCRate = dblRate;
			ucRep.YearCovered = lngYearCovered;
			ucRep.Summary.Name = fecherFoundation.Strings.UCase(txtEmployerName.Text);
			ucRep.Summary.Address = fecherFoundation.Strings.UCase(txtStreetAddress.Text);
			ucRep.Summary.City = fecherFoundation.Strings.UCase(txtCity.Text);
			ucRep.Summary.State = fecherFoundation.Strings.UCase(txtState.Text);
			ucRep.Summary.zip = fecherFoundation.Strings.Trim(txtZip.Text);
			ucRep.Summary.ZipExtension = fecherFoundation.Strings.Trim(txtZip4.Text);
			ucRep.Transmitter.Email = txtEmail.Text;
			ucRep.Transmitter.Title = txtTitle.Text;
			ucRep.Transmitter.Phone = txtPhone.Text;
			ucRep.Transmitter.Extension = txtExtension.Text;
			for (x = 1; x <= 3; x++)
			{
				ucRep.Set_EmploymentMonthStartDate(x, cmbStartDate[(x - 1)].Text);
				ucRep.Set_EmploymentMonthEndDate(x, cmbEndDate[(x - 1)].Text);
			}
			// x
			cUC1Controller uc1Cont = new cUC1Controller();
			uc1Cont.LoadReport(ref ucRep, strSequence);
			GetReportInstance = ucRep;
			return GetReportInstance;
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			dblCredit = Conversion.Val(txtCredit.Text);
			dblRemitted = Conversion.Val(txtRemitted.Text);
			if (!CheckRange())
			{
				return;
			}
			if (!SaveInfo())
			{
				return;
			}
			// Call rptMEUC1.Init(dblRate, dblLimit, intQuarterCovered, lngYearCovered, strSequence, dblCSSFRate)
			cUC1Report uc1rep;
			uc1rep = GetReportInstance();
			rptMEUC1.InstancePtr.Init(uc1rep, this.Modal);
		}

		private void mnuPrintTest_Click(object sender, System.EventArgs e)
		{
			// Call rptMEUC1.Init(dblRate, dblLimit, intQuarterCovered, lngYearCovered, strSequence, dblCSSFRate, True)
			cUC1Report uc1rep = new cUC1Report();
			rptMEUC1.InstancePtr.Init(uc1rep, this.Modal, true);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			string strFile = "";

			dblCredit = Conversion.Val(txtCredit.Text);
			dblRemitted = Conversion.Val(txtRemitted.Text);
			if (!CheckRange())
			{
				return;
			}
			if (!SaveInfo())
			{
				return;
			}
			cUC1Report ucRep;
			ucRep = GetReportInstance();
			if (!boolReportNotElectronic)
			{
				cEUC1Controller eCont = new cEUC1Controller();

				strFile = "meUC1Wages_Q" + ucRep.Quarter + "_" + ucRep.YearCovered + ".xls";

				if (File.Exists(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile)))
				{
					File.Delete(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile));
				}
				if (!eCont.CreateFile(strFile, ref ucRep))
				{
					return;
				}
                
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile), strFile);
                if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "jay")
				{
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalRoutines.Statics.gstrReportID)) == "NONE")
					{
					}
					else
					{
						rptBLS.InstancePtr.Init(ref intQuarterCovered, ref lngYearCovered, this.Modal, strSequence, true);
					}
				}
			}
			else
			{
				rptMEUC1.InstancePtr.Init(ucRep, this.Modal);
				mnuExit_Click();
			}
		}

		private void txtExtension_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFilename_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Decimal)
			{
				KeyCode = (Keys)0;
			}
		}

		private void txtMRSID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtUCAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: KeyAscii As int	OnWrite(Keys)
		private bool KeyIsNumber(int KeyAscii)
		{
			bool KeyIsNumber = false;
			KeyIsNumber = false;
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 27 || KeyAscii == 127 || KeyAscii == 8))
			{
				KeyIsNumber = true;
			}
			return KeyIsNumber;
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			object strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				if (fecherFoundation.Strings.Trim(txtFederalID.Text).Length != modCoreysSweeterCode.EWRFederalIDLen)
				{
					MessageBox.Show("The federal ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRFederalIDLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFederalID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtMRSID.Text).Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
				{
					MessageBox.Show("The MRS Withholding Account must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRMRSWithholdingLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtMRSID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtUCAccount.Text).Length != modCoreysSweeterCode.EWRStateUCLen)
				{
					MessageBox.Show("The State UC ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRStateUCLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtUCAccount.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtEmployerName.Text) == string.Empty)
				{
					MessageBox.Show("You must provide an employer name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerName.Focus();
					return SaveInfo;
				}
				EWRRecord.EmployerAddress = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtStreetAddress.Text), modCoreysSweeterCode.EWRReturnAddressLen));
				EWRRecord.EmployerCity = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtCity.Text), modCoreysSweeterCode.EWRReturnCityLen));
				EWRRecord.EmployerName = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtEmployerName.Text), modCoreysSweeterCode.EWRReturnNameLen));
				EWRRecord.EmployerState = Strings.Mid(fecherFoundation.Strings.Trim(txtState.Text), 1, 2);
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = fecherFoundation.Strings.Trim(txtZip.Text);
				EWRRecord.EmployerZip4 = fecherFoundation.Strings.Trim(txtZip4.Text);
				EWRRecord.FederalEmployerID = txtFederalID.Text;
				EWRRecord.MRSWithholdingID = txtMRSID.Text;
				EWRRecord.StateUCAccount = txtUCAccount.Text;
				EWRRecord.TransmitterExt = txtExtension.Text;
				EWRRecord.TransmitterEmail = txtEmail.Text;
				strTemp = "";
				for (x = 1; x <= (txtPhone.Text.Length); x++)
				{
					if (Information.IsNumeric(Strings.Mid(txtPhone.Text, x, 1)))
					{
						strTemp += Strings.Mid(txtPhone.Text, x, 1);
					}
				}
				// x
				EWRRecord.TransmitterPhone = FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(strTemp), 10));
				EWRRecord.TransmitterTitle = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtTitle.Text), modCoreysSweeterCode.EWRTransmitterTitleLen));
				EWRRecord.ProcessorLicenseCode = txtProcessorLicenseCode.Text;
				if (!Information.IsDate(cmbEndDate[0].Text))
				{
					cmbEndDate[0].Text = cmbStartDate[0].Text;
				}
				if (!Information.IsDate(cmbEndDate[1].Text))
				{
					cmbEndDate[1].Text = cmbStartDate[1].Text;
				}
				if (!Information.IsDate(cmbEndDate[2].Text))
				{
					cmbEndDate[2].Text = cmbStartDate[2].Text;
				}
				modCoreysSweeterCode.Statics.EWRWageTotals.M1Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[0].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M2Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[1].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M3Start = FCConvert.ToDateTime(Strings.Format(cmbStartDate[2].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M1End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[0].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M2End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[1].Text, "MM/dd/yyyy"));
				modCoreysSweeterCode.Statics.EWRWageTotals.M3End = FCConvert.ToDateTime(Strings.Format(cmbEndDate[2].Text, "MM/dd/yyyy"));
				clsSave.OpenRecordset("select * from tblemployerinfo", "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("employername", EWRRecord.EmployerName);
				clsSave.Set_Fields("Address1", EWRRecord.EmployerAddress);
				clsSave.Set_Fields("City", EWRRecord.EmployerCity);
				clsSave.Set_Fields("State", EWRRecord.EmployerState);
				clsSave.Set_Fields("zip", EWRRecord.EmployerZip);
				clsSave.Set_Fields("zip4", EWRRecord.EmployerZip4);
				clsSave.Set_Fields("TransmitterTitle", EWRRecord.TransmitterTitle);
				clsSave.Set_Fields("transmitterextension", EWRRecord.TransmitterExt);
				clsSave.Set_Fields("transmitterphone", EWRRecord.TransmitterPhone);
				clsSave.Set_Fields("FederalEmployerID", EWRRecord.FederalEmployerID);
				clsSave.Set_Fields("StateUCAccount", EWRRecord.StateUCAccount);
				clsSave.Set_Fields("MRSWithholdingID", EWRRecord.MRSWithholdingID);
				clsSave.Set_Fields("ProcessorLicenseCode", EWRRecord.ProcessorLicenseCode);
				clsSave.Set_Fields("transmitteremail", EWRRecord.TransmitterEmail);
				if (txtSeasonalCode.Text != string.Empty)
				{
					clsSave.Set_Fields("seasonalcode", FCConvert.ToString(Conversion.Val(txtSeasonalCode.Text)));
				}
				else
				{
					clsSave.Set_Fields("seasonalcode", null);
				}
				if (Information.IsDate(T2KSeasonalFrom.Text))
				{
					clsSave.Set_Fields("SeasonalFrom", Strings.Format(T2KSeasonalFrom.Text, "MM/dd/yy"));
				}
				else
				{
					clsSave.Set_Fields("SeasonalFrom", "");
				}
				if (Information.IsDate(T2KSeasonalTo.Text))
				{
					clsSave.Set_Fields("SeasonalTo", Strings.Format(T2KSeasonalTo.Text, "MM/dd/yy"));
				}
				else
				{
					clsSave.Set_Fields("seasonalto", "");
				}
				if (Information.IsDate(cmbStartDate[0].Text))
				{
					clsSave.Set_Fields("Month1StartDate", cmbStartDate[0].Text);
				}
				if (Information.IsDate(cmbStartDate[1].Text))
				{
					clsSave.Set_Fields("Month2startdate", cmbStartDate[1].Text);
				}
				if (Information.IsDate(cmbStartDate[2].Text))
				{
					clsSave.Set_Fields("Month3startdate", cmbStartDate[2].Text);
				}
				if (Information.IsDate(cmbEndDate[0].Text))
				{
					clsSave.Set_Fields("Month1EndDate", cmbEndDate[0].Text);
				}
				else
				{
					clsSave.Set_Fields("month1enddate", cmbStartDate[0].Text);
					cmbEndDate[0].Text = cmbStartDate[0].Text;
				}
				if (Information.IsDate(cmbEndDate[1].Text))
				{
					clsSave.Set_Fields("Month2enddate", cmbEndDate[1].Text);
				}
				else
				{
					clsSave.Set_Fields("month2enddate", cmbStartDate[1].Text);
					cmbEndDate[1].Text = cmbStartDate[1].Text;
				}
				if (Information.IsDate(cmbEndDate[2].Text))
				{
					clsSave.Set_Fields("month3enddate", cmbEndDate[2].Text);
				}
				else
				{
					clsSave.Set_Fields("month3enddate", cmbStartDate[2].Text);
					cmbEndDate[2].Text = cmbStartDate[2].Text;
				}
				clsSave.Update();
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillDateCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			int[] intM = new int[3 + 1];
			DateTime dtDateTemp;
			// vbPorter upgrade warning: dtDate1 As DateTime	OnWrite(string)
			DateTime dtDate1;
			DateTime dtDate2;
			int x;
			intM[2] = intLastMonthInQuarter;
			intM[1] = intM[2] - 1;
			intM[0] = intM[2] - 2;
			// first months combos
			for (x = 0; x <= 2; x++)
			{
				dtDate1 = FCConvert.ToDateTime(FCConvert.ToString(intM[x]) + "/1/" + FCConvert.ToString(lngYearCovered));
				dtDateTemp = fecherFoundation.DateAndTime.DateAdd("m", 1, dtDate1);
				dtDate2 = fecherFoundation.DateAndTime.DateAdd("d", -1, dtDateTemp);
				strSQL = "select paydate from tblcheckdetail where paydate between '" + FCConvert.ToString(dtDate1) + "' and '" + FCConvert.ToString(dtDate2) + "' and checkvoid = 0 group by paydate order by paydate";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				cmbStartDate[x].Clear();
				cmbEndDate[x].Clear();
				cmbStartDate[x].AddItem("");
				cmbEndDate[x].AddItem("");
				while (!clsLoad.EndOfFile())
				{
					cmbStartDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					cmbEndDate[x].AddItem(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					clsLoad.MoveNext();
				}
			}
			// x
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
