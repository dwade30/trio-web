﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsTimeClockPlusImportRecord
	{
		//=========================================================
		private string strSSN = "";
		private string strExportCode = "";
		private string strCustomEmployeeNumber = "";
		private string strEmployeeNum = "";
		private double dblHours;
		private string strLastName = "";
		private string strDepartment = "";
		private bool boolSalary;
		private string strJobCode = "";
		private string strPayType = "";
		private string strExpenseCode = "";
		private string strJobCodeDesc = "";
		private double dblPayrate;

		public string SSN
		{
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
			set
			{
				strSSN = value;
			}
		}

		public string ExportCode
		{
			get
			{
				string ExportCode = "";
				ExportCode = strExportCode;
				return ExportCode;
			}
			set
			{
				strExportCode = value;
			}
		}

		public string CustomEmployeeNumber
		{
			get
			{
				string CustomEmployeeNumber = "";
				CustomEmployeeNumber = strCustomEmployeeNumber;
				return CustomEmployeeNumber;
			}
			set
			{
				strCustomEmployeeNumber = value;
			}
		}

		public string EmployeeNum
		{
			get
			{
				string EmployeeNum = "";
				EmployeeNum = strEmployeeNum;
				return EmployeeNum;
			}
			set
			{
				strEmployeeNum = value;
			}
		}

		public double Hours
		{
			get
			{
				double Hours = 0;
				Hours = dblHours;
				return Hours;
			}
			set
			{
				dblHours = value;
			}
		}

		public string LastName
		{
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
			set
			{
				strLastName = value;
			}
		}

		public string Department
		{
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
			set
			{
				strDepartment = value;
			}
		}

		public bool Salary
		{
			get
			{
				bool Salary = false;
				Salary = boolSalary;
				return Salary;
			}
			set
			{
				boolSalary = value;
			}
		}

		public string JobCode
		{
			get
			{
				string JobCode = "";
				JobCode = strJobCode;
				return JobCode;
			}
			set
			{
				strJobCode = value;
			}
		}

		public string PayType
		{
			get
			{
				string PayType = "";
				PayType = strPayType;
				return PayType;
			}
			set
			{
				strPayType = value;
			}
		}

		public string ExpenseCode
		{
			get
			{
				string ExpenseCode = "";
				ExpenseCode = strExpenseCode;
				return ExpenseCode;
			}
			set
			{
				strExpenseCode = value;
			}
		}

		public string JobCodeDescription
		{
			get
			{
				string JobCodeDescription = "";
				JobCodeDescription = strJobCodeDesc;
				return JobCodeDescription;
			}
			set
			{
				strJobCodeDesc = value;
			}
		}

		public double PayRate
		{
			set
			{
				dblPayrate = value;
			}
			get
			{
				double PayRate = 0;
				PayRate = dblPayrate;
				return PayRate;
			}
		}
	}
}
