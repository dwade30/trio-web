﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3Regular.
	/// </summary>
	partial class rptW3Regular
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW3Regular));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtFederalEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAllocationTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersStateEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtThirdPartySick = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeferredCompensation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalW2s = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFaxNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtContactPerson = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chk941 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkThirdParty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chk943 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkMed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkMilitary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkHshld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkCT1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartySick)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeferredCompensation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalW2s)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPerson)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk941)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkThirdParty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk943)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMilitary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHshld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCT1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFederalEIN,
            this.txtEmployersName,
            this.txtTotalWages,
            this.txtFederalTax,
            this.txtFICATax,
            this.txtMedTaxes,
            this.txtAllocationTips,
            this.txtFICAWages,
            this.txtMedWages,
            this.txtSSTips,
            this.txtDependentCare,
            this.txtNonQualifiedPlans,
            this.txtState,
            this.txtEmployersStateEIN,
            this.txtStateWages,
            this.txtStateTax,
            this.txtThirdPartySick,
            this.txtDeferredCompensation,
            this.txtTotalW2s,
            this.txtPhoneNumber,
            this.txtFaxNumber,
            this.txtContactPerson,
            this.txtEmail,
            this.chk941,
            this.chkThirdParty,
            this.chk943,
            this.chkMed,
            this.chkMilitary,
            this.chkHshld,
            this.chkCT1});
			this.Detail.Height = 5.34375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtFederalEIN
			// 
			this.txtFederalEIN.Height = 0.1666667F;
			this.txtFederalEIN.Left = 0.15625F;
			this.txtFederalEIN.Name = "txtFederalEIN";
			this.txtFederalEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFederalEIN.Text = null;
			this.txtFederalEIN.Top = 1.125F;
			this.txtFederalEIN.Width = 3.28125F;
			// 
			// txtEmployersName
			// 
			this.txtEmployersName.Height = 1.416667F;
			this.txtEmployersName.Left = 0.15625F;
			this.txtEmployersName.Name = "txtEmployersName";
			this.txtEmployersName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmployersName.Text = null;
			this.txtEmployersName.Top = 1.5F;
			this.txtEmployersName.Width = 3.28125F;
			// 
			// txtTotalWages
			// 
			this.txtTotalWages.Height = 0.1666667F;
			this.txtTotalWages.Left = 4F;
			this.txtTotalWages.Name = "txtTotalWages";
			this.txtTotalWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalWages.Text = null;
			this.txtTotalWages.Top = 0.125F;
			this.txtTotalWages.Width = 1.15625F;
			// 
			// txtFederalTax
			// 
			this.txtFederalTax.Height = 0.1666667F;
			this.txtFederalTax.Left = 6.3125F;
			this.txtFederalTax.Name = "txtFederalTax";
			this.txtFederalTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFederalTax.Text = null;
			this.txtFederalTax.Top = 0.125F;
			this.txtFederalTax.Width = 1.15625F;
			// 
			// txtFICATax
			// 
			this.txtFICATax.Height = 0.1666667F;
			this.txtFICATax.Left = 6.3125F;
			this.txtFICATax.Name = "txtFICATax";
			this.txtFICATax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICATax.Text = null;
			this.txtFICATax.Top = 0.4583333F;
			this.txtFICATax.Width = 1.15625F;
			// 
			// txtMedTaxes
			// 
			this.txtMedTaxes.Height = 0.1666667F;
			this.txtMedTaxes.Left = 6.3125F;
			this.txtMedTaxes.Name = "txtMedTaxes";
			this.txtMedTaxes.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedTaxes.Text = null;
			this.txtMedTaxes.Top = 0.7916667F;
			this.txtMedTaxes.Width = 1.15625F;
			// 
			// txtAllocationTips
			// 
			this.txtAllocationTips.Height = 0.1666667F;
			this.txtAllocationTips.Left = 6.3125F;
			this.txtAllocationTips.Name = "txtAllocationTips";
			this.txtAllocationTips.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAllocationTips.Text = null;
			this.txtAllocationTips.Top = 1.125F;
			this.txtAllocationTips.Width = 1.15625F;
			// 
			// txtFICAWages
			// 
			this.txtFICAWages.Height = 0.1666667F;
			this.txtFICAWages.Left = 4F;
			this.txtFICAWages.Name = "txtFICAWages";
			this.txtFICAWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICAWages.Text = null;
			this.txtFICAWages.Top = 0.4583333F;
			this.txtFICAWages.Width = 1.15625F;
			// 
			// txtMedWages
			// 
			this.txtMedWages.Height = 0.1666667F;
			this.txtMedWages.Left = 4F;
			this.txtMedWages.Name = "txtMedWages";
			this.txtMedWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedWages.Text = null;
			this.txtMedWages.Top = 0.7916667F;
			this.txtMedWages.Width = 1.15625F;
			// 
			// txtSSTips
			// 
			this.txtSSTips.Height = 0.1666667F;
			this.txtSSTips.Left = 4F;
			this.txtSSTips.Name = "txtSSTips";
			this.txtSSTips.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSSTips.Text = null;
			this.txtSSTips.Top = 1.125F;
			this.txtSSTips.Width = 1.15625F;
			// 
			// txtDependentCare
			// 
			this.txtDependentCare.Height = 0.1666667F;
			this.txtDependentCare.Left = 6.3125F;
			this.txtDependentCare.Name = "txtDependentCare";
			this.txtDependentCare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDependentCare.Text = null;
			this.txtDependentCare.Top = 1.458333F;
			this.txtDependentCare.Width = 1.15625F;
			// 
			// txtNonQualifiedPlans
			// 
			this.txtNonQualifiedPlans.Height = 0.1666667F;
			this.txtNonQualifiedPlans.Left = 4F;
			this.txtNonQualifiedPlans.Name = "txtNonQualifiedPlans";
			this.txtNonQualifiedPlans.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtNonQualifiedPlans.Text = null;
			this.txtNonQualifiedPlans.Top = 1.791667F;
			this.txtNonQualifiedPlans.Width = 1.15625F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1666667F;
			this.txtState.Left = 0.15625F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtState.Text = null;
			this.txtState.Top = 3.166667F;
			this.txtState.Width = 0.59375F;
			// 
			// txtEmployersStateEIN
			// 
			this.txtEmployersStateEIN.Height = 0.1666667F;
			this.txtEmployersStateEIN.Left = 0.8125F;
			this.txtEmployersStateEIN.Name = "txtEmployersStateEIN";
			this.txtEmployersStateEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmployersStateEIN.Text = null;
			this.txtEmployersStateEIN.Top = 3.166667F;
			this.txtEmployersStateEIN.Width = 2.625F;
			// 
			// txtStateWages
			// 
			this.txtStateWages.Height = 0.1666667F;
			this.txtStateWages.Left = 4.0625F;
			this.txtStateWages.Name = "txtStateWages";
			this.txtStateWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateWages.Text = null;
			this.txtStateWages.Top = 3.083333F;
			this.txtStateWages.Width = 1.28125F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1666667F;
			this.txtStateTax.Left = 6.3125F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 3.083333F;
			this.txtStateTax.Width = 1.125F;
			// 
			// txtThirdPartySick
			// 
			this.txtThirdPartySick.Height = 0.1666667F;
			this.txtThirdPartySick.Left = 4.40625F;
			this.txtThirdPartySick.Name = "txtThirdPartySick";
			this.txtThirdPartySick.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtThirdPartySick.Text = null;
			this.txtThirdPartySick.Top = 2.458333F;
			this.txtThirdPartySick.Width = 2.53125F;
			// 
			// txtDeferredCompensation
			// 
			this.txtDeferredCompensation.Height = 0.1666667F;
			this.txtDeferredCompensation.Left = 6.3125F;
			this.txtDeferredCompensation.Name = "txtDeferredCompensation";
			this.txtDeferredCompensation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDeferredCompensation.Text = null;
			this.txtDeferredCompensation.Top = 1.802083F;
			this.txtDeferredCompensation.Width = 1.166667F;
			// 
			// txtTotalW2s
			// 
			this.txtTotalW2s.Height = 0.1666667F;
			this.txtTotalW2s.Left = 0.1666667F;
			this.txtTotalW2s.Name = "txtTotalW2s";
			this.txtTotalW2s.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTotalW2s.Text = null;
			this.txtTotalW2s.Top = 0.8333333F;
			this.txtTotalW2s.Width = 3.266667F;
			// 
			// txtPhoneNumber
			// 
			this.txtPhoneNumber.Height = 0.1666667F;
			this.txtPhoneNumber.Left = 4F;
			this.txtPhoneNumber.Name = "txtPhoneNumber";
			this.txtPhoneNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPhoneNumber.Text = null;
			this.txtPhoneNumber.Top = 3.866667F;
			this.txtPhoneNumber.Width = 1.333333F;
			// 
			// txtFaxNumber
			// 
			this.txtFaxNumber.Height = 0.1875F;
			this.txtFaxNumber.Left = 0.25F;
			this.txtFaxNumber.Name = "txtFaxNumber";
			this.txtFaxNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFaxNumber.Text = null;
			this.txtFaxNumber.Top = 4.1875F;
			this.txtFaxNumber.Width = 1.3125F;
			// 
			// txtContactPerson
			// 
			this.txtContactPerson.Height = 0.1666667F;
			this.txtContactPerson.Left = 0.2333333F;
			this.txtContactPerson.Name = "txtContactPerson";
			this.txtContactPerson.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtContactPerson.Text = null;
			this.txtContactPerson.Top = 3.866667F;
			this.txtContactPerson.Width = 3.2F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.1875F;
			this.txtEmail.Left = 4F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 4.1875F;
			this.txtEmail.Width = 3.1875F;
			// 
			// chk941
			// 
			this.chk941.Height = 0.19F;
			this.chk941.Left = 1.2F;
			this.chk941.Name = "chk941";
			this.chk941.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chk941.Text = "X";
			this.chk941.Top = 0.1666667F;
			this.chk941.Width = 0.15625F;
			// 
			// chkThirdParty
			// 
			this.chkThirdParty.Height = 0.19F;
			this.chkThirdParty.Left = 2.866667F;
			this.chkThirdParty.Name = "chkThirdParty";
			this.chkThirdParty.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkThirdParty.Text = "X";
			this.chkThirdParty.Top = 0.5333334F;
			this.chkThirdParty.Width = 0.15625F;
			// 
			// chk943
			// 
			this.chk943.Height = 0.19F;
			this.chk943.Left = 2.3F;
			this.chk943.Name = "chk943";
			this.chk943.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chk943.Text = "X";
			this.chk943.Top = 0.1666667F;
			this.chk943.Width = 0.15625F;
			// 
			// chkMed
			// 
			this.chkMed.Height = 0.19F;
			this.chkMed.Left = 2.3F;
			this.chkMed.Name = "chkMed";
			this.chkMed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkMed.Text = "X";
			this.chkMed.Top = 0.5333334F;
			this.chkMed.Width = 0.15625F;
			// 
			// chkMilitary
			// 
			this.chkMilitary.Height = 0.19F;
			this.chkMilitary.Left = 1.766667F;
			this.chkMilitary.Name = "chkMilitary";
			this.chkMilitary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkMilitary.Text = "X";
			this.chkMilitary.Top = 0.1666667F;
			this.chkMilitary.Width = 0.15625F;
			// 
			// chkHshld
			// 
			this.chkHshld.Height = 0.19F;
			this.chkHshld.Left = 1.766667F;
			this.chkHshld.Name = "chkHshld";
			this.chkHshld.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkHshld.Text = "X";
			this.chkHshld.Top = 0.5333334F;
			this.chkHshld.Width = 0.15625F;
			// 
			// chkCT1
			// 
			this.chkCT1.Height = 0.19F;
			this.chkCT1.Left = 1.2F;
			this.chkCT1.Name = "chkCT1";
			this.chkCT1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkCT1.Text = "X";
			this.chkCT1.Top = 0.5333334F;
			this.chkCT1.Width = 0.15625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptW3Regular
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.947917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartySick)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeferredCompensation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalW2s)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPerson)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk941)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkThirdParty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk943)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMilitary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHshld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCT1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllocationTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersStateEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThirdPartySick;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeferredCompensation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalW2s;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFaxNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactPerson;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chk941;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkThirdParty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chk943;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkMed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkMilitary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkHshld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkCT1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
