﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptFUTA940Report.
	/// </summary>
	partial class rptFUTA940Report
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFUTA940Report));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFUTABase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGRTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMXCreditRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTDPayroll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGRTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMXCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAdditionalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldActualTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldActualTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExemptPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFUTABase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGRTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXCreditRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDPayroll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGRTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdditionalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label42,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label49,
				this.fldFUTABase,
				this.fldGRTaxRate,
				this.fldMXCreditRate,
				this.fldSTTaxRate,
				this.fldYTDPayroll,
				this.fldExcess,
				this.fldTaxable,
				this.fldGRTax,
				this.fldMXCredit,
				this.fldSTTax,
				this.fldAdditionalCredit,
				this.fldActualTax,
				this.fldActualTaxRate,
				this.Label50,
				this.txtExemptPayments,
				this.Line3,
				this.Line4
			});
			this.Detail.Height = 2.71875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblTaxYear,
				this.Label37,
				this.Label41,
				this.Line2
			});
			this.PageHeader.Height = 0.71875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "FUTA Report (940)";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblTaxYear
			// 
			this.lblTaxYear.Height = 0.1875F;
			this.lblTaxYear.HyperLink = null;
			this.lblTaxYear.Left = 0F;
			this.lblTaxYear.Name = "lblTaxYear";
			this.lblTaxYear.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblTaxYear.Text = "Report Summary";
			this.lblTaxYear.Top = 0.21875F;
			this.lblTaxYear.Width = 6.5F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 1.875F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label37.Text = "Category ";
			this.Label37.Top = 0.5F;
			this.Label37.Width = 0.625F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 3.84375F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label41.Text = "Amount";
			this.Label41.Top = 0.5F;
			this.Label41.Width = 0.90625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.84375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.6875F;
			this.Line2.Width = 2.9375F;
			this.Line2.X1 = 1.84375F;
			this.Line2.X2 = 4.78125F;
			this.Line2.Y1 = 0.6875F;
			this.Line2.Y2 = 0.6875F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 1.875F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label42.Text = "YTD Payroll";
			this.Label42.Top = 0.03125F;
			this.Label42.Width = 1.5625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.2083333F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 1.875F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label43.Text = "Excess Over";
			this.Label43.Top = 0.5F;
			this.Label43.Width = 0.875F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.2083333F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 1.875F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label44.Text = "Taxable";
			this.Label44.Top = 0.7916667F;
			this.Label44.Width = 1.5625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1666667F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 1.875F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label45.Text = "Gross Tax @";
			this.Label45.Top = 1.291667F;
			this.Label45.Width = 0.875F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1666667F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 1.875F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label46.Text = "Max Credit @";
			this.Label46.Top = 1.541667F;
			this.Label46.Width = 0.875F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.2083333F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 1.875F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label47.Text = "State Tax @";
			this.Label47.Top = 1.791667F;
			this.Label47.Width = 0.875F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.2083333F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 1.875F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label48.Text = "Additional Credit";
			this.Label48.Top = 2.041667F;
			this.Label48.Width = 1.5625F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.2083333F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 1.875F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label49.Text = "Actual Tax";
			this.Label49.Top = 2.375F;
			this.Label49.Width = 0.875F;
			// 
			// fldFUTABase
			// 
			this.fldFUTABase.Height = 0.2083333F;
			this.fldFUTABase.Left = 2.8125F;
			this.fldFUTABase.Name = "fldFUTABase";
			this.fldFUTABase.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldFUTABase.Text = "Field1";
			this.fldFUTABase.Top = 0.5F;
			this.fldFUTABase.Width = 0.625F;
			// 
			// fldGRTaxRate
			// 
			this.fldGRTaxRate.Height = 0.1666667F;
			this.fldGRTaxRate.Left = 2.8125F;
			this.fldGRTaxRate.Name = "fldGRTaxRate";
			this.fldGRTaxRate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldGRTaxRate.Text = "Field1";
			this.fldGRTaxRate.Top = 1.291667F;
			this.fldGRTaxRate.Width = 0.625F;
			// 
			// fldMXCreditRate
			// 
			this.fldMXCreditRate.Height = 0.1666667F;
			this.fldMXCreditRate.Left = 2.8125F;
			this.fldMXCreditRate.Name = "fldMXCreditRate";
			this.fldMXCreditRate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldMXCreditRate.Text = "Field2";
			this.fldMXCreditRate.Top = 1.541667F;
			this.fldMXCreditRate.Width = 0.625F;
			// 
			// fldSTTaxRate
			// 
			this.fldSTTaxRate.Height = 0.2083333F;
			this.fldSTTaxRate.Left = 2.8125F;
			this.fldSTTaxRate.Name = "fldSTTaxRate";
			this.fldSTTaxRate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSTTaxRate.Text = "Field3";
			this.fldSTTaxRate.Top = 1.791667F;
			this.fldSTTaxRate.Width = 0.625F;
			// 
			// fldYTDPayroll
			// 
			this.fldYTDPayroll.Height = 0.1875F;
			this.fldYTDPayroll.Left = 3.8125F;
			this.fldYTDPayroll.Name = "fldYTDPayroll";
			this.fldYTDPayroll.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldYTDPayroll.Text = "Field1";
			this.fldYTDPayroll.Top = 0.03125F;
			this.fldYTDPayroll.Width = 0.96875F;
			// 
			// fldExcess
			// 
			this.fldExcess.Height = 0.2083333F;
			this.fldExcess.Left = 3.8125F;
			this.fldExcess.Name = "fldExcess";
			this.fldExcess.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldExcess.Text = "Field2";
			this.fldExcess.Top = 0.5F;
			this.fldExcess.Width = 0.96875F;
			// 
			// fldTaxable
			// 
			this.fldTaxable.Height = 0.2083333F;
			this.fldTaxable.Left = 3.8125F;
			this.fldTaxable.Name = "fldTaxable";
			this.fldTaxable.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTaxable.Text = "Field3";
			this.fldTaxable.Top = 0.7916667F;
			this.fldTaxable.Width = 0.96875F;
			// 
			// fldGRTax
			// 
			this.fldGRTax.Height = 0.1666667F;
			this.fldGRTax.Left = 3.8125F;
			this.fldGRTax.Name = "fldGRTax";
			this.fldGRTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldGRTax.Text = "Field4";
			this.fldGRTax.Top = 1.291667F;
			this.fldGRTax.Width = 0.96875F;
			// 
			// fldMXCredit
			// 
			this.fldMXCredit.Height = 0.1666667F;
			this.fldMXCredit.Left = 3.8125F;
			this.fldMXCredit.Name = "fldMXCredit";
			this.fldMXCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldMXCredit.Text = "Field5";
			this.fldMXCredit.Top = 1.541667F;
			this.fldMXCredit.Width = 0.96875F;
			// 
			// fldSTTax
			// 
			this.fldSTTax.Height = 0.2083333F;
			this.fldSTTax.Left = 3.8125F;
			this.fldSTTax.Name = "fldSTTax";
			this.fldSTTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSTTax.Text = "Field6";
			this.fldSTTax.Top = 1.791667F;
			this.fldSTTax.Width = 0.96875F;
			// 
			// fldAdditionalCredit
			// 
			this.fldAdditionalCredit.Height = 0.2083333F;
			this.fldAdditionalCredit.Left = 3.8125F;
			this.fldAdditionalCredit.Name = "fldAdditionalCredit";
			this.fldAdditionalCredit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAdditionalCredit.Text = "Field7";
			this.fldAdditionalCredit.Top = 2.041667F;
			this.fldAdditionalCredit.Width = 0.96875F;
			// 
			// fldActualTax
			// 
			this.fldActualTax.Height = 0.2083333F;
			this.fldActualTax.Left = 3.8125F;
			this.fldActualTax.Name = "fldActualTax";
			this.fldActualTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldActualTax.Text = "Field8";
			this.fldActualTax.Top = 2.375F;
			this.fldActualTax.Width = 0.96875F;
			// 
			// fldActualTaxRate
			// 
			this.fldActualTaxRate.Height = 0.2083333F;
			this.fldActualTaxRate.Left = 2.8125F;
			this.fldActualTaxRate.Name = "fldActualTaxRate";
			this.fldActualTaxRate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldActualTaxRate.Text = "Field3";
			this.fldActualTaxRate.Top = 2.375F;
			this.fldActualTaxRate.Width = 0.625F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.2083333F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 1.875F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label50.Text = "Exempt Payments";
			this.Label50.Top = 0.25F;
			this.Label50.Width = 1.5625F;
			// 
			// txtExemptPayments
			// 
			this.txtExemptPayments.Height = 0.2083333F;
			this.txtExemptPayments.Left = 3.8125F;
			this.txtExemptPayments.Name = "txtExemptPayments";
			this.txtExemptPayments.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.txtExemptPayments.Text = "Field3";
			this.txtExemptPayments.Top = 0.25F;
			this.txtExemptPayments.Width = 0.96875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.84375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.7083333F;
			this.Line3.Width = 2.9375F;
			this.Line3.X1 = 1.84375F;
			this.Line3.X2 = 4.78125F;
			this.Line3.Y1 = 0.7083333F;
			this.Line3.Y2 = 0.7083333F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.84375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.375F;
			this.Line4.Width = 2.9375F;
			this.Line4.X1 = 1.84375F;
			this.Line4.X2 = 4.78125F;
			this.Line4.Y1 = 2.375F;
			this.Line4.Y2 = 2.375F;
			// 
			// rptFUTA940Report
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFUTABase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGRTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXCreditRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDPayroll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGRTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdditionalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFUTABase;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGRTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMXCreditRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDPayroll;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGRTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMXCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdditionalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldActualTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldActualTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptPayments;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
