//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeSchedule.
	/// </summary>
	partial class frmEmployeeSchedule
	{
		public System.Collections.Generic.List<FCGrid> GridDays;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCPanel framGrids;
		public FCGrid GridDays_0;
		public FCGrid GridDays_1;
		public FCGrid GridDays_2;
		public FCGrid GridDays_3;
		public FCGrid GridDays_4;
		public FCGrid GridDays_5;
		public FCGrid GridDays_6;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		public FCGrid GridTotals;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
			this.Frame1 = new fecherFoundation.FCPanel();
			this.framGrids = new fecherFoundation.FCPanel();
			this.GridDays_0 = new fecherFoundation.FCGrid();
			this.GridDays_1 = new fecherFoundation.FCGrid();
			this.GridDays_2 = new fecherFoundation.FCGrid();
			this.GridDays_3 = new fecherFoundation.FCGrid();
			this.GridDays_4 = new fecherFoundation.FCGrid();
			this.GridDays_5 = new fecherFoundation.FCGrid();
			this.GridDays_6 = new fecherFoundation.FCGrid();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_5 = new fecherFoundation.FCLabel();
			this.Label1_6 = new fecherFoundation.FCLabel();
			this.GridTotals = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framGrids)).BeginInit();
			this.framGrids.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 528);
			this.BottomPanel.Size = new System.Drawing.Size(954, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.GridTotals);
			this.ClientArea.Size = new System.Drawing.Size(954, 468);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(954, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(231, 30);
			this.HeaderText.Text = "Employee Schedule";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.framGrids);
			this.Frame1.Location = new System.Drawing.Point(0, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(2540, 270);
			this.Frame1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// framGrids
			// 
			this.framGrids.AppearanceKey = "groupBoxNoBorders";
			this.framGrids.Controls.Add(this.GridDays_0);
			this.framGrids.Controls.Add(this.GridDays_1);
			this.framGrids.Controls.Add(this.GridDays_2);
			this.framGrids.Controls.Add(this.GridDays_3);
			this.framGrids.Controls.Add(this.GridDays_4);
			this.framGrids.Controls.Add(this.GridDays_5);
			this.framGrids.Controls.Add(this.GridDays_6);
			this.framGrids.Controls.Add(this.Label1_0);
			this.framGrids.Controls.Add(this.Label1_1);
			this.framGrids.Controls.Add(this.Label1_2);
			this.framGrids.Controls.Add(this.Label1_3);
			this.framGrids.Controls.Add(this.Label1_4);
			this.framGrids.Controls.Add(this.Label1_5);
			this.framGrids.Controls.Add(this.Label1_6);
			this.framGrids.Location = new System.Drawing.Point(30, 30);
			this.framGrids.Name = "framGrids";
			this.framGrids.Size = new System.Drawing.Size(2510, 248);
			this.framGrids.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.framGrids, null);
			// 
			// GridDays_0
			// 
			this.GridDays_0.AllowSelection = false;
			this.GridDays_0.AllowUserToResizeColumns = false;
			this.GridDays_0.AllowUserToResizeRows = false;
			this.GridDays_0.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_0.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_0.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_0.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_0.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_0.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_0.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_0.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDays_0.ColumnHeadersHeight = 60;
			this.GridDays_0.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_0.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDays_0.DragIcon = null;
			this.GridDays_0.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_0.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_0.ExtendLastCol = true;
			this.GridDays_0.FixedCols = 0;
			this.GridDays_0.FixedRows = 2;
			this.GridDays_0.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_0.FrozenCols = 0;
			this.GridDays_0.GridColor = System.Drawing.Color.Empty;
			this.GridDays_0.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_0.Location = new System.Drawing.Point(0, 28);
			this.GridDays_0.Name = "GridDays_0";
			this.GridDays_0.OutlineCol = 0;
			this.GridDays_0.RowHeadersVisible = false;
			this.GridDays_0.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_0.RowHeightMin = 0;
			this.GridDays_0.Rows = 2;
			this.GridDays_0.ScrollTipText = null;
			this.GridDays_0.ShowColumnVisibilityMenu = false;
			this.GridDays_0.Size = new System.Drawing.Size(350, 200);
			this.GridDays_0.StandardTab = true;
			this.GridDays_0.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_0.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_0.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.GridDays_0, "Use Insert and Delete to add and delete shifts");
			this.GridDays_0.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_0.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_1
			// 
			this.GridDays_1.AllowSelection = false;
			this.GridDays_1.AllowUserToResizeColumns = false;
			this.GridDays_1.AllowUserToResizeRows = false;
			this.GridDays_1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_1.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_1.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_1.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_1.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_1.Cols = 6;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridDays_1.ColumnHeadersHeight = 60;
			this.GridDays_1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_1.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridDays_1.DragIcon = null;
			this.GridDays_1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_1.ExtendLastCol = true;
			this.GridDays_1.FixedCols = 0;
			this.GridDays_1.FixedRows = 2;
			this.GridDays_1.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_1.FrozenCols = 0;
			this.GridDays_1.GridColor = System.Drawing.Color.Empty;
			this.GridDays_1.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_1.Location = new System.Drawing.Point(360, 28);
			this.GridDays_1.Name = "GridDays_1";
			this.GridDays_1.OutlineCol = 0;
			this.GridDays_1.RowHeadersVisible = false;
			this.GridDays_1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_1.RowHeightMin = 0;
			this.GridDays_1.Rows = 2;
			this.GridDays_1.ScrollTipText = null;
			this.GridDays_1.ShowColumnVisibilityMenu = false;
			this.GridDays_1.Size = new System.Drawing.Size(350, 200);
			this.GridDays_1.StandardTab = true;
			this.GridDays_1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_1.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.GridDays_1, "Use Insert and Delete to add and delete shifts");
			this.GridDays_1.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_1.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_2
			// 
			this.GridDays_2.AllowSelection = false;
			this.GridDays_2.AllowUserToResizeColumns = false;
			this.GridDays_2.AllowUserToResizeRows = false;
			this.GridDays_2.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_2.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_2.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_2.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_2.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_2.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_2.Cols = 6;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridDays_2.ColumnHeadersHeight = 60;
			this.GridDays_2.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_2.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridDays_2.DragIcon = null;
			this.GridDays_2.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_2.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_2.ExtendLastCol = true;
			this.GridDays_2.FixedCols = 0;
			this.GridDays_2.FixedRows = 2;
			this.GridDays_2.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_2.FrozenCols = 0;
			this.GridDays_2.GridColor = System.Drawing.Color.Empty;
			this.GridDays_2.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_2.Location = new System.Drawing.Point(720, 28);
			this.GridDays_2.Name = "GridDays_2";
			this.GridDays_2.OutlineCol = 0;
			this.GridDays_2.RowHeadersVisible = false;
			this.GridDays_2.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_2.RowHeightMin = 0;
			this.GridDays_2.Rows = 2;
			this.GridDays_2.ScrollTipText = null;
			this.GridDays_2.ShowColumnVisibilityMenu = false;
			this.GridDays_2.Size = new System.Drawing.Size(350, 200);
			this.GridDays_2.StandardTab = true;
			this.GridDays_2.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_2.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.GridDays_2, "Use Insert and Delete to add and delete shifts");
			this.GridDays_2.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_2.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_3
			// 
			this.GridDays_3.AllowSelection = false;
			this.GridDays_3.AllowUserToResizeColumns = false;
			this.GridDays_3.AllowUserToResizeRows = false;
			this.GridDays_3.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_3.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_3.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_3.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_3.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_3.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_3.Cols = 6;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GridDays_3.ColumnHeadersHeight = 60;
			this.GridDays_3.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_3.DefaultCellStyle = dataGridViewCellStyle8;
			this.GridDays_3.DragIcon = null;
			this.GridDays_3.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_3.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_3.ExtendLastCol = true;
			this.GridDays_3.FixedCols = 0;
			this.GridDays_3.FixedRows = 2;
			this.GridDays_3.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_3.FrozenCols = 0;
			this.GridDays_3.GridColor = System.Drawing.Color.Empty;
			this.GridDays_3.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_3.Location = new System.Drawing.Point(1080, 28);
			this.GridDays_3.Name = "GridDays_3";
			this.GridDays_3.OutlineCol = 0;
			this.GridDays_3.RowHeadersVisible = false;
			this.GridDays_3.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_3.RowHeightMin = 0;
			this.GridDays_3.Rows = 2;
			this.GridDays_3.ScrollTipText = null;
			this.GridDays_3.ShowColumnVisibilityMenu = false;
			this.GridDays_3.Size = new System.Drawing.Size(350, 200);
			this.GridDays_3.StandardTab = true;
			this.GridDays_3.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_3.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_3.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.GridDays_3, "Use Insert and Delete to add and delete shifts");
			this.GridDays_3.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_3.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_4
			// 
			this.GridDays_4.AllowSelection = false;
			this.GridDays_4.AllowUserToResizeColumns = false;
			this.GridDays_4.AllowUserToResizeRows = false;
			this.GridDays_4.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_4.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_4.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_4.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_4.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_4.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_4.Cols = 6;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.GridDays_4.ColumnHeadersHeight = 60;
			this.GridDays_4.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_4.DefaultCellStyle = dataGridViewCellStyle10;
			this.GridDays_4.DragIcon = null;
			this.GridDays_4.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_4.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_4.ExtendLastCol = true;
			this.GridDays_4.FixedCols = 0;
			this.GridDays_4.FixedRows = 2;
			this.GridDays_4.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_4.FrozenCols = 0;
			this.GridDays_4.GridColor = System.Drawing.Color.Empty;
			this.GridDays_4.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_4.Location = new System.Drawing.Point(1440, 28);
			this.GridDays_4.Name = "GridDays_4";
			this.GridDays_4.OutlineCol = 0;
			this.GridDays_4.RowHeadersVisible = false;
			this.GridDays_4.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_4.RowHeightMin = 0;
			this.GridDays_4.Rows = 2;
			this.GridDays_4.ScrollTipText = null;
			this.GridDays_4.ShowColumnVisibilityMenu = false;
			this.GridDays_4.Size = new System.Drawing.Size(350, 200);
			this.GridDays_4.StandardTab = true;
			this.GridDays_4.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_4.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_4.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.GridDays_4, "Use Insert and Delete to add and delete shifts");
			this.GridDays_4.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_4.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_5
			// 
			this.GridDays_5.AllowSelection = false;
			this.GridDays_5.AllowUserToResizeColumns = false;
			this.GridDays_5.AllowUserToResizeRows = false;
			this.GridDays_5.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_5.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_5.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_5.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_5.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_5.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_5.Cols = 6;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.GridDays_5.ColumnHeadersHeight = 60;
			this.GridDays_5.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_5.DefaultCellStyle = dataGridViewCellStyle12;
			this.GridDays_5.DragIcon = null;
			this.GridDays_5.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_5.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_5.ExtendLastCol = true;
			this.GridDays_5.FixedCols = 0;
			this.GridDays_5.FixedRows = 2;
			this.GridDays_5.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_5.FrozenCols = 0;
			this.GridDays_5.GridColor = System.Drawing.Color.Empty;
			this.GridDays_5.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_5.Location = new System.Drawing.Point(1800, 28);
			this.GridDays_5.Name = "GridDays_5";
			this.GridDays_5.OutlineCol = 0;
			this.GridDays_5.RowHeadersVisible = false;
			this.GridDays_5.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_5.RowHeightMin = 0;
			this.GridDays_5.Rows = 2;
			this.GridDays_5.ScrollTipText = null;
			this.GridDays_5.ShowColumnVisibilityMenu = false;
			this.GridDays_5.Size = new System.Drawing.Size(350, 200);
			this.GridDays_5.StandardTab = true;
			this.GridDays_5.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_5.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_5.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.GridDays_5, "Use Insert and Delete to add and delete shifts");
			this.GridDays_5.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_5.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// GridDays_6
			// 
			this.GridDays_6.AllowSelection = false;
			this.GridDays_6.AllowUserToResizeColumns = false;
			this.GridDays_6.AllowUserToResizeRows = false;
			this.GridDays_6.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDays_6.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDays_6.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDays_6.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDays_6.BackColorSel = System.Drawing.Color.Empty;
			this.GridDays_6.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDays_6.Cols = 6;
			dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDays_6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.GridDays_6.ColumnHeadersHeight = 60;
			this.GridDays_6.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDays_6.DefaultCellStyle = dataGridViewCellStyle14;
			this.GridDays_6.DragIcon = null;
			this.GridDays_6.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDays_6.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDays_6.ExtendLastCol = true;
			this.GridDays_6.FixedCols = 0;
			this.GridDays_6.FixedRows = 2;
			this.GridDays_6.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDays_6.FrozenCols = 0;
			this.GridDays_6.GridColor = System.Drawing.Color.Empty;
			this.GridDays_6.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDays_6.Location = new System.Drawing.Point(2160, 28);
			this.GridDays_6.Name = "GridDays_6";
			this.GridDays_6.OutlineCol = 0;
			this.GridDays_6.RowHeadersVisible = false;
			this.GridDays_6.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDays_6.RowHeightMin = 0;
			this.GridDays_6.Rows = 2;
			this.GridDays_6.ScrollTipText = null;
			this.GridDays_6.ShowColumnVisibilityMenu = false;
			this.GridDays_6.Size = new System.Drawing.Size(350, 200);
			this.GridDays_6.StandardTab = true;
			this.GridDays_6.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDays_6.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDays_6.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.GridDays_6, "Use Insert and Delete to add and delete shifts");
			this.GridDays_6.CellButtonClick += new System.EventHandler(this.GridDays_CellButtonClick);
			this.GridDays_6.ComboCloseUp += new System.EventHandler(this.GridDays_ComboCloseUp);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(0, 0);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(80, 16);
			this.Label1_0.TabIndex = 16;
			this.Label1_0.Text = "SUNDAY";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(360, 0);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(80, 16);
			this.Label1_1.TabIndex = 15;
			this.Label1_1.Text = "MONDAY";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(720, 0);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(80, 16);
			this.Label1_2.TabIndex = 14;
			this.Label1_2.Text = "TUESDAY";
			this.ToolTip1.SetToolTip(this.Label1_2, null);
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(1080, 0);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(80, 16);
			this.Label1_3.TabIndex = 13;
			this.Label1_3.Text = "WEDNESDAY";
			this.ToolTip1.SetToolTip(this.Label1_3, null);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(1440, 0);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(80, 16);
			this.Label1_4.TabIndex = 12;
			this.Label1_4.Text = "THURSDAY";
			this.ToolTip1.SetToolTip(this.Label1_4, null);
			// 
			// Label1_5
			// 
			this.Label1_5.Location = new System.Drawing.Point(1800, 0);
			this.Label1_5.Name = "Label1_5";
			this.Label1_5.Size = new System.Drawing.Size(80, 16);
			this.Label1_5.TabIndex = 11;
			this.Label1_5.Text = "FRIDAY";
			this.ToolTip1.SetToolTip(this.Label1_5, null);
			// 
			// Label1_6
			// 
			this.Label1_6.Location = new System.Drawing.Point(2160, 0);
			this.Label1_6.Name = "Label1_6";
			this.Label1_6.Size = new System.Drawing.Size(80, 16);
			this.Label1_6.TabIndex = 10;
			this.Label1_6.Text = "SATURDAY";
			this.ToolTip1.SetToolTip(this.Label1_6, null);
			// 
			// GridTotals
			// 
			this.GridTotals.AllowSelection = false;
			this.GridTotals.AllowUserToResizeColumns = false;
			this.GridTotals.AllowUserToResizeRows = false;
			this.GridTotals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridTotals.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridTotals.BackColorBkg = System.Drawing.Color.Empty;
			this.GridTotals.BackColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.BackColorSel = System.Drawing.Color.Empty;
			this.GridTotals.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridTotals.Cols = 6;
			dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridTotals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.GridTotals.ColumnHeadersHeight = 30;
			this.GridTotals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridTotals.DefaultCellStyle = dataGridViewCellStyle16;
			this.GridTotals.DragIcon = null;
			this.GridTotals.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridTotals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridTotals.ExtendLastCol = true;
			this.GridTotals.FixedCols = 0;
			this.GridTotals.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.FrozenCols = 0;
			this.GridTotals.GridColor = System.Drawing.Color.Empty;
			this.GridTotals.GridColorFixed = System.Drawing.Color.Empty;
			this.GridTotals.Location = new System.Drawing.Point(30, 276);
			this.GridTotals.Name = "GridTotals";
			this.GridTotals.OutlineCol = 0;
			this.GridTotals.RowHeadersVisible = false;
			this.GridTotals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridTotals.RowHeightMin = 0;
			this.GridTotals.Rows = 1;
			this.GridTotals.ScrollTipText = null;
			this.GridTotals.ShowColumnVisibilityMenu = false;
			this.GridTotals.Size = new System.Drawing.Size(569, 235);
			this.GridTotals.StandardTab = true;
			this.GridTotals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridTotals.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.GridTotals, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 3;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(366, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmEmployeeSchedule
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEmployeeSchedule";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Employee Schedule";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmEmployeeSchedule_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmployeeSchedule_KeyDown);
			this.Resize += new System.EventHandler(this.frmEmployeeSchedule_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framGrids)).EndInit();
			this.framGrids.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridDays_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDays_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}