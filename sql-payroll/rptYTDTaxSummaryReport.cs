//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptYTDTaxSumaryReport.
	/// </summary>
	public partial class rptYDTaxSumaryReport : BaseSectionReport
	{
		public rptYDTaxSumaryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "YTD Tax Summary Report";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsData = new FCGrid();
			vsData.Rows = 1;
			vsData.Cols = 10;
			vsHeader = new FCGrid();
			vsHeader.Rows = 2;
			vsHeader.Cols = 10;
		}

		public static rptYDTaxSumaryReport InstancePtr
		{
			get
			{
				return (rptYDTaxSumaryReport)Sys.GetInstance(typeof(rptYDTaxSumaryReport));
			}
		}

		protected rptYDTaxSumaryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs?.Dispose();
				rsData?.Dispose();
                rs = null;
                rsData = null;
				vsData?.Dispose();
				vsHeader?.Dispose();
                vsHeader = null;
                vsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptYTDTaxSumaryReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		int intRow;
		int intCol;
		int intPageNumber;
		bool boolDoubleHeader;
		string strEmployeeNumber = "";
		double dblTotal;
		int intCounter;
		double dblTotalPayTotal;
		double dblFederalTotal;
		double dblFICATotal;
		double dblMedicareTotal;
		double dblStateTotal;
		double dblDeductsTotal;
		double dblNetTotal;
		int intEmployeeTotal;
		int intSetTotals;
		int lngRegType;
		int lngOVType;
		FCGrid vsData;
		FCGrid vsHeader;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// On Error Resume Next
			double dblTemp = 0;
			double dblRegular = 0;
			double dblOvertime = 0;
			double dblOther = 0;
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			NextRecord:
			;
			if (rsData.EndOfFile())
			{
				intSetTotals += 1;
				vsData.TextMatrix(0, 0, string.Empty);
				vsData.TextMatrix(0, 1, string.Empty);
				vsData.TextMatrix(0, 2, string.Empty);
				vsData.TextMatrix(0, 3, string.Empty);
				vsData.TextMatrix(0, 4, string.Empty);
				vsData.TextMatrix(0, 5, string.Empty);
				vsData.TextMatrix(0, 6, string.Empty);
				vsData.TextMatrix(0, 7, string.Empty);
				vsData.TextMatrix(0, 8, string.Empty);
				if (intSetTotals == 1)
				{
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 2)
				{
					vsData.TextMatrix(0, 0, "Final");
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 3)
				{
					vsData.TextMatrix(0, 0, "Totals");
					vsData.TextMatrix(0, 1, "Number Employees: " + FCConvert.ToString(intEmployeeTotal));
					vsData.TextMatrix(0, 2, Strings.Format(dblTotalPayTotal, "0.00"));
					vsData.TextMatrix(0, 3, Strings.Format(dblFederalTotal, "0.00"));
					vsData.TextMatrix(0, 4, Strings.Format(dblFICATotal, "0.00"));
					vsData.TextMatrix(0, 5, Strings.Format(dblMedicareTotal, "0.00"));
					vsData.TextMatrix(0, 6, Strings.Format(dblStateTotal, "0.00"));
					vsData.TextMatrix(0, 7, Strings.Format(dblDeductsTotal, "0.00"));
					vsData.TextMatrix(0, 8, Strings.Format(dblNetTotal, "0.00"));
					eArgs.EOF = false;
					return;
				}
				else if (intSetTotals == 4)
				{
					eArgs.EOF = true;
					return;
				}
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			// If strEmployeeNumber = Trim(rsData.Fields("EmployeeNumber")) Then
			// If Not rsData.EndOfFile Then rsData.MoveNext
			// GoTo NextRecord
			// End If
			dblRegular = 0;
			dblOvertime = 0;
			dblOther = 0;
			strEmployeeNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
			vsData.TextMatrix(0, 0, fecherFoundation.Strings.Trim(strEmployeeNumber));
			vsData.TextMatrix(0, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("LastName"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FirstName"))));
			intEmployeeTotal += 1;
			dblTotal = 0;
			// calculate the total Pay
			dblRegular = modCoreysSweeterCode.GetFYTDPayTotal(lngRegType, ref strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			dblTotal += dblRegular;
			dblOvertime = modCoreysSweeterCode.GetFYTDPayTotal(lngOVType, ref strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			dblTotal += dblOvertime;
			dblOther = modCoreysSweeterCode.GetFYTDPayTotal(-1, ref strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			dblOther += -dblRegular - dblOvertime;
			dblTotal += dblOther;
			vsData.TextMatrix(0, 2, Strings.Format(Conversion.Val(dblTotal), "0.00"));
			dblTotalPayTotal += dblTotal;
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 3, Strings.Format(dblTemp, "0.00"));
			dblFederalTotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 4, Strings.Format(dblTemp, "0.00"));
			dblFICATotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 5, Strings.Format(dblTemp, "0.00"));
			dblMedicareTotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 6, Strings.Format(dblTemp, "0.00"));
			dblStateTotal += dblTemp;
			// get the employee dedutions
			dblTemp = modCoreysSweeterCode.GetFYTDDeductionTotal(-1, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 7, Strings.Format(dblTemp, "0.00"));
			dblDeductsTotal += dblTemp;
			vsData.TextMatrix(0, 8, Strings.Format(FCConvert.ToDouble(vsData.TextMatrix(0, 2)) - (Conversion.Val(vsData.TextMatrix(0, 3)) + Conversion.Val(vsData.TextMatrix(0, 4)) + Conversion.Val(vsData.TextMatrix(0, 5)) + Conversion.Val(vsData.TextMatrix(0, 6)) + Conversion.Val(vsData.TextMatrix(0, 7)) + Conversion.Val(vsData.TextMatrix(0, 8))), "0.00"));
			dblNetTotal += Conversion.Val(vsData.TextMatrix(0, 8));
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intSetTotals = 0;
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy") + "  " + Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			rsData.OpenRecordset("select * from tblpaycategories where description = 'Regular'", "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				lngRegType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("ID"))));
			}
			else
			{
				lngRegType = 0;
			}
			rsData.OpenRecordset("select * from tblpaycategories where description = 'Overtime'", "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				lngOVType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("ID"))));
			}
			else
			{
				lngOVType = 0;
			}
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			intRow = 0;
			boolDoubleHeader = false;
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "Total");
			vsHeader.TextMatrix(0, 3, "- - - - - - - -");
			vsHeader.TextMatrix(0, 4, "- - - - - - - -");
			vsHeader.TextMatrix(0, 5, "Taxes");
			vsHeader.TextMatrix(0, 6, "- - - - - - - -");
			vsHeader.TextMatrix(1, 0, "ID");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "Name");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "- Pay -");
			vsHeader.ColWidth(2, 800);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Federal");
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "FICA");
			vsHeader.ColWidth(4, 800);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Medicare");
			vsHeader.ColWidth(5, 800);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "State");
			vsHeader.ColWidth(6, 800);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Deducts");
			vsHeader.ColWidth(7, 800);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "NET");
			vsHeader.ColWidth(8, 800);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColWidth(4, vsHeader.ColWidth(4));
			vsData.ColWidth(5, vsHeader.ColWidth(5));
			vsData.ColWidth(6, vsHeader.ColWidth(6));
			vsData.ColWidth(7, vsHeader.ColWidth(7));
			vsData.ColWidth(8, vsHeader.ColWidth(8));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsData.ColAlignment(4, vsHeader.ColAlignment(4));
			vsData.ColAlignment(5, vsHeader.ColAlignment(5));
			vsData.ColAlignment(6, vsHeader.ColAlignment(6));
			vsData.ColAlignment(7, vsHeader.ColAlignment(7));
			vsData.ColAlignment(8, vsHeader.ColAlignment(8));
			// vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsData.Rows - 1, intNumberOfSQLFields) = flexAlignLeftCenter
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			// ADD A LINE SEPERATOR BETWEEN THE CAPTIONS AND THE DATA THAT
			// IS DISPLAYED
			Line1.X1 = vsHeader.Left;
			Line1.X2 = this.PrintWidth;
			Line1.Y1 = vsHeader.Top + vsHeader.Height / 1440F + 20 / 1440F;
			Line1.Y2 = vsHeader.Top + vsHeader.Height / 1440F + 20 / 1440F;
			PageHeader.Height += 100 / 1440F;
			Detail.Height = vsData.Height / 1440F + 30 / 1440F;
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void PageHeader_BeforePrint(object sender, EventArgs e)
		{
			vsHeader.TextMatrix(0, 0, "Employee");
			vsHeader.TextMatrix(0, 1, "Employee");
			vsHeader.TextMatrix(0, 2, "Total");
			vsHeader.TextMatrix(0, 3, "- - - - - - - -");
			vsHeader.TextMatrix(0, 4, "- - - - - - - -");
			vsHeader.TextMatrix(0, 5, "Taxes");
			vsHeader.TextMatrix(0, 6, "- - - - - - - -");
			vsHeader.TextMatrix(1, 0, "ID");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 1, "Name");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(1, 2, "- Pay -");
			vsHeader.ColWidth(2, 800);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 3, "Federal");
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 4, "FICA");
			vsHeader.ColWidth(4, 800);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 5, "Medicare");
			vsHeader.ColWidth(5, 800);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 6, "State");
			vsHeader.ColWidth(6, 800);
			vsHeader.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 7, "Deducts");
			vsHeader.ColWidth(7, 800);
			vsHeader.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(1, 8, "NET");
			vsHeader.ColWidth(8, 800);
			vsHeader.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
		}
	}
}
