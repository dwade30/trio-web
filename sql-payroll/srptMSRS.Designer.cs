﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMSRS.
	/// </summary>
	partial class srptMSRS
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMSRS));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFullName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInclude = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLifeInsCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedCompAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMonthlyHD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlanCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPosition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStatusCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRateFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRateOfPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMonthlyFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsLevel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsSched = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRateHD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWorkWeeks = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFedCompDP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnempInclude = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNatureCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInclude)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedCompAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonthlyHD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPosition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatusCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateOfPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonthlyFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsLevel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsSched)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRateHD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkWeeks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedCompDP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnempInclude)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNatureCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFullName,
				this.txtInclude,
				this.txtLifeInsCode,
				this.txtFedCompAmount,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.txtMonthlyHD,
				this.txtPlanCode,
				this.txtPosition,
				this.Label14,
				this.txtStatusCode,
				this.Label15,
				this.Label46,
				this.txtRateFactor,
				this.txtRateOfPay,
				this.Label47,
				this.txtMonthlyFactor,
				this.Label48,
				this.Label49,
				this.txtLifeInsLevel,
				this.txtPayPeriod,
				this.Label50,
				this.txtLifeInsSched,
				this.Label51,
				this.Label52,
				this.txtPayRateHD,
				this.txtWorkWeeks,
				this.Label53,
				this.txtFedCompDP,
				this.Label54,
				this.Shape1,
				this.Label55,
				this.txtUnempInclude,
				this.txtNatureCode,
				this.Label56,
				this.Label57
			});
			this.Detail.Height = 1.40625F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0.19F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape5,
				this.Label45
			});
			this.GroupHeader1.Height = 0.3854167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Shape5
			// 
			this.Shape5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape5.Height = 0.3125F;
			this.Shape5.Left = 0F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 0.03125F;
			this.Shape5.Width = 7.125F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.19F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-size: 9pt; font-weight: bold";
			this.Label45.Text = "MainePERS";
			this.Label45.Top = 0.03125F;
			this.Label45.Width = 1.3125F;
			// 
			// lblFullName
			// 
			this.lblFullName.Height = 0.1875F;
			this.lblFullName.HyperLink = null;
			this.lblFullName.Left = 0F;
			this.lblFullName.Name = "lblFullName";
			this.lblFullName.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblFullName.Text = "Include";
			this.lblFullName.Top = 0.0625F;
			this.lblFullName.Width = 0.6875F;
			// 
			// txtInclude
			// 
			this.txtInclude.Height = 0.1875F;
			this.txtInclude.Left = 0.875F;
			this.txtInclude.Name = "txtInclude";
			this.txtInclude.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtInclude.Text = null;
			this.txtInclude.Top = 0.0625F;
			this.txtInclude.Width = 0.625F;
			// 
			// txtLifeInsCode
			// 
			this.txtLifeInsCode.Height = 0.1875F;
			this.txtLifeInsCode.Left = 0.875F;
			this.txtLifeInsCode.Name = "txtLifeInsCode";
			this.txtLifeInsCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLifeInsCode.Text = null;
			this.txtLifeInsCode.Top = 0.25F;
			this.txtLifeInsCode.Width = 0.625F;
			// 
			// txtFedCompAmount
			// 
			this.txtFedCompAmount.Height = 0.1875F;
			this.txtFedCompAmount.Left = 3.625F;
			this.txtFedCompAmount.Name = "txtFedCompAmount";
			this.txtFedCompAmount.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFedCompAmount.Text = null;
			this.txtFedCompAmount.Top = 1F;
			this.txtFedCompAmount.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label3.Text = "Life Ins. Code";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.3125F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label4.Text = "Fed Comp Amount";
			this.Label4.Top = 1F;
			this.Label4.Width = 1.125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label5.Text = "Monthly H/D";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label6.Text = "Plan Code";
			this.Label6.Top = 0.4375F;
			this.Label6.Width = 0.6875F;
			// 
			// txtMonthlyHD
			// 
			this.txtMonthlyHD.Height = 0.1875F;
			this.txtMonthlyHD.Left = 0.875F;
			this.txtMonthlyHD.Name = "txtMonthlyHD";
			this.txtMonthlyHD.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMonthlyHD.Text = null;
			this.txtMonthlyHD.Top = 0.625F;
			this.txtMonthlyHD.Width = 0.625F;
			// 
			// txtPlanCode
			// 
			this.txtPlanCode.Height = 0.1875F;
			this.txtPlanCode.Left = 0.875F;
			this.txtPlanCode.Name = "txtPlanCode";
			this.txtPlanCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPlanCode.Text = null;
			this.txtPlanCode.Top = 0.4375F;
			this.txtPlanCode.Width = 0.625F;
			// 
			// txtPosition
			// 
			this.txtPosition.Height = 0.1875F;
			this.txtPosition.Left = 6.5F;
			this.txtPosition.Name = "txtPosition";
			this.txtPosition.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPosition.Text = null;
			this.txtPosition.Top = 0.0625F;
			this.txtPosition.Width = 0.625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.1875F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label14.Text = "Position Code";
			this.Label14.Top = 0.0625F;
			this.Label14.Width = 0.875F;
			// 
			// txtStatusCode
			// 
			this.txtStatusCode.Height = 0.1875F;
			this.txtStatusCode.Left = 3.625F;
			this.txtStatusCode.Name = "txtStatusCode";
			this.txtStatusCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtStatusCode.Text = null;
			this.txtStatusCode.Top = 0.0625F;
			this.txtStatusCode.Width = 0.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.3125F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label15.Text = "Status Code";
			this.Label15.Top = 0.0625F;
			this.Label15.Width = 0.8125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0F;
			this.Label46.MultiLine = false;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label46.Text = "Factor";
			this.Label46.Top = 1.1875F;
			this.Label46.Width = 0.6875F;
			// 
			// txtRateFactor
			// 
			this.txtRateFactor.Height = 0.1875F;
			this.txtRateFactor.Left = 0.875F;
			this.txtRateFactor.Name = "txtRateFactor";
			this.txtRateFactor.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtRateFactor.Text = null;
			this.txtRateFactor.Top = 1.1875F;
			this.txtRateFactor.Width = 0.625F;
			// 
			// txtRateOfPay
			// 
			this.txtRateOfPay.Height = 0.1875F;
			this.txtRateOfPay.Left = 0.875F;
			this.txtRateOfPay.Name = "txtRateOfPay";
			this.txtRateOfPay.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtRateOfPay.Text = null;
			this.txtRateOfPay.Top = 1F;
			this.txtRateOfPay.Width = 0.625F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0F;
			this.Label47.MultiLine = false;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label47.Text = "Rate of Pay";
			this.Label47.Top = 1F;
			this.Label47.Width = 0.75F;
			// 
			// txtMonthlyFactor
			// 
			this.txtMonthlyFactor.Height = 0.1875F;
			this.txtMonthlyFactor.Left = 0.875F;
			this.txtMonthlyFactor.Name = "txtMonthlyFactor";
			this.txtMonthlyFactor.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMonthlyFactor.Text = null;
			this.txtMonthlyFactor.Top = 0.8125F;
			this.txtMonthlyFactor.Width = 0.625F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 0F;
			this.Label48.MultiLine = false;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label48.Text = "Factor";
			this.Label48.Top = 0.8125F;
			this.Label48.Width = 0.6875F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 2.3125F;
			this.Label49.MultiLine = false;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label49.Text = "Life Insurance Level";
			this.Label49.Top = 0.25F;
			this.Label49.Width = 1.25F;
			// 
			// txtLifeInsLevel
			// 
			this.txtLifeInsLevel.Height = 0.1875F;
			this.txtLifeInsLevel.Left = 3.625F;
			this.txtLifeInsLevel.Name = "txtLifeInsLevel";
			this.txtLifeInsLevel.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLifeInsLevel.Text = null;
			this.txtLifeInsLevel.Top = 0.25F;
			this.txtLifeInsLevel.Width = 0.75F;
			// 
			// txtPayPeriod
			// 
			this.txtPayPeriod.Height = 0.1875F;
			this.txtPayPeriod.Left = 3.625F;
			this.txtPayPeriod.Name = "txtPayPeriod";
			this.txtPayPeriod.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPayPeriod.Text = null;
			this.txtPayPeriod.Top = 0.625F;
			this.txtPayPeriod.Width = 0.75F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 2.3125F;
			this.Label50.MultiLine = false;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label50.Text = "Pay Period Code";
			this.Label50.Top = 0.625F;
			this.Label50.Width = 1.1875F;
			// 
			// txtLifeInsSched
			// 
			this.txtLifeInsSched.Height = 0.1875F;
			this.txtLifeInsSched.Left = 3.625F;
			this.txtLifeInsSched.Name = "txtLifeInsSched";
			this.txtLifeInsSched.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLifeInsSched.Text = null;
			this.txtLifeInsSched.Top = 0.4375F;
			this.txtLifeInsSched.Width = 0.75F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 2.3125F;
			this.Label51.MultiLine = false;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label51.Text = "Life Ins. Sched. Code";
			this.Label51.Top = 0.4375F;
			this.Label51.Width = 1.25F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 2.3125F;
			this.Label52.MultiLine = false;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label52.Text = "Pay Rate Code H/D";
			this.Label52.Top = 0.8125F;
			this.Label52.Width = 1.125F;
			// 
			// txtPayRateHD
			// 
			this.txtPayRateHD.Height = 0.1875F;
			this.txtPayRateHD.Left = 3.625F;
			this.txtPayRateHD.Name = "txtPayRateHD";
			this.txtPayRateHD.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPayRateHD.Text = null;
			this.txtPayRateHD.Top = 0.8125F;
			this.txtPayRateHD.Width = 0.75F;
			// 
			// txtWorkWeeks
			// 
			this.txtWorkWeeks.Height = 0.1875F;
			this.txtWorkWeeks.Left = 6.5F;
			this.txtWorkWeeks.Name = "txtWorkWeeks";
			this.txtWorkWeeks.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtWorkWeeks.Text = null;
			this.txtWorkWeeks.Top = 0.25F;
			this.txtWorkWeeks.Width = 0.625F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 5.1875F;
			this.Label53.MultiLine = false;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label53.Text = "Work Weeks Per Year";
			this.Label53.Top = 0.25F;
			this.Label53.Width = 1.25F;
			// 
			// txtFedCompDP
			// 
			this.txtFedCompDP.Height = 0.1875F;
			this.txtFedCompDP.Left = 3.625F;
			this.txtFedCompDP.Name = "txtFedCompDP";
			this.txtFedCompDP.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFedCompDP.Text = null;
			this.txtFedCompDP.Top = 1.1875F;
			this.txtFedCompDP.Width = 0.75F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 2.3125F;
			this.Label54.MultiLine = false;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label54.Text = "Fed Comp D/%";
			this.Label54.Top = 1.1875F;
			this.Label54.Width = 1F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.5F;
			this.Shape1.Left = 5.1875F;
			this.Shape1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Shape1.LineWeight = 4F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.78125F;
			this.Shape1.Width = 1.9375F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 5.3125F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label55.Text = "Include";
			this.Label55.Top = 0.84375F;
			this.Label55.Width = 0.8125F;
			// 
			// txtUnempInclude
			// 
			this.txtUnempInclude.Height = 0.1875F;
			this.txtUnempInclude.Left = 6.1875F;
			this.txtUnempInclude.Name = "txtUnempInclude";
			this.txtUnempInclude.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtUnempInclude.Text = null;
			this.txtUnempInclude.Top = 0.84375F;
			this.txtUnempInclude.Width = 0.625F;
			// 
			// txtNatureCode
			// 
			this.txtNatureCode.Height = 0.1875F;
			this.txtNatureCode.Left = 6.1875F;
			this.txtNatureCode.Name = "txtNatureCode";
			this.txtNatureCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtNatureCode.Text = null;
			this.txtNatureCode.Top = 1.03125F;
			this.txtNatureCode.Width = 0.625F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 5.3125F;
			this.Label56.MultiLine = false;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label56.Text = "Nature Code";
			this.Label56.Top = 1.03125F;
			this.Label56.Width = 0.8125F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 5.1875F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label57.Text = "Unemployment Information";
			this.Label57.Top = 0.59375F;
			this.Label57.Width = 1.9375F;
			// 
			// srptMSRS
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.302083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInclude)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedCompAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonthlyHD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPosition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatusCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateOfPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonthlyFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsLevel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsSched)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRateHD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkWeeks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedCompDP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnempInclude)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNatureCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFullName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInclude;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLifeInsCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedCompAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthlyHD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlanCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPosition;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatusCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRateFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRateOfPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthlyFactor;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLifeInsLevel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLifeInsSched;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayRateHD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWorkWeeks;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedCompDP;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnempInclude;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNatureCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
