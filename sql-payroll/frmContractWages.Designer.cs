//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmContractWages.
	/// </summary>
	partial class frmContractWages
	{
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCLabel lblPayRun;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsGrid = new fecherFoundation.FCGrid();
            this.lblPayRun = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 560);
            this.BottomPanel.Size = new System.Drawing.Size(741, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblPayRun);
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Size = new System.Drawing.Size(741, 500);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(741, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(200, 30);
            this.HeaderText.Text = "Employee Listing";
            // 
            // vsGrid
            // 
            this.vsGrid.AllowSelection = false;
            this.vsGrid.AllowUserToResizeColumns = false;
            this.vsGrid.AllowUserToResizeRows = false;
            this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
            this.vsGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsGrid.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsGrid.ColumnHeadersHeight = 30;
            this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsGrid.DragIcon = null;
            this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.FrozenCols = 0;
            this.vsGrid.GridColor = System.Drawing.Color.Empty;
            this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.Location = new System.Drawing.Point(30, 69);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.OutlineCol = 0;
            this.vsGrid.ReadOnly = true;
            this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsGrid.RowHeightMin = 0;
            this.vsGrid.Rows = 1;
            this.vsGrid.ScrollTipText = null;
            this.vsGrid.ShowColumnVisibilityMenu = false;
            this.vsGrid.Size = new System.Drawing.Size(683, 419);
            this.vsGrid.StandardTab = true;
            this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsGrid.TabIndex = 0;
            this.vsGrid.TabStop = false;
            this.vsGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsGrid_KeyPressEdit);
            this.vsGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrid_ValidateEdit);
            this.vsGrid.CurrentCellChanged += new System.EventHandler(this.vsGrid_RowColChange);
            this.vsGrid.Click += new System.EventHandler(this.vsGrid_ClickEvent);
            // 
            // lblPayRun
            // 
            this.lblPayRun.Location = new System.Drawing.Point(30, 30);
            this.lblPayRun.Name = "lblPayRun";
            this.lblPayRun.Size = new System.Drawing.Size(678, 25);
            this.lblPayRun.TabIndex = 1;
            this.lblPayRun.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                  ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                 ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(321, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(81, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmContractWages
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(741, 668);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmContractWages";
            this.Text = "Employee Listing";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmContractWages_Load);
            this.Activated += new System.EventHandler(this.frmContractWages_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmContractWages_KeyPress);
            this.Resize += new System.EventHandler(this.frmContractWages_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSave;
    }
}