﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeInformationReport.
	/// </summary>
	partial class rptEmployeeInformationReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmployeeInformationReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.srptEmployeeMaster = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.srptEmployeeMatch = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptEmployeeDeductions = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptEmployeePayTotals = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.subDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptVacSick = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subMSRS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubDistribution = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape4,
				this.srptEmployeeMaster,
				this.lblHeader,
				this.Line1,
				this.srptEmployeeMatch,
				this.srptEmployeeDeductions,
				this.srptEmployeePayTotals,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Label43,
				this.Label44,
				this.subDirectDeposit,
				this.srptVacSick,
				this.subMSRS,
				this.SubDistribution
			});
			this.Detail.Height = 1.197917F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.Detail.Format += new System.EventHandler(Detail_Format);
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime
			});
			this.PageHeader.Height = 0.5104167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "EMPLOYEE INFORMATION REPORT";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 7.1875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.125F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Shape4
			// 
			this.Shape4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape4.Height = 0.3125F;
			this.Shape4.Left = 0.125F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 0.71875F;
			this.Shape4.Width = 7.125F;
			// 
			// srptEmployeeMaster
			// 
			this.srptEmployeeMaster.CloseBorder = false;
			this.srptEmployeeMaster.Height = 0.0625F;
			this.srptEmployeeMaster.Left = 0.125F;
			this.srptEmployeeMaster.Name = "srptEmployeeMaster";
			this.srptEmployeeMaster.Report = null;
			this.srptEmployeeMaster.Top = 0.3125F;
			this.srptEmployeeMaster.Width = 7.25F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0.125F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-weight: bold; text-align: center";
			this.lblHeader.Text = "EMPLOYEE DATA";
			this.lblHeader.Top = 0.09375F;
			this.lblHeader.Width = 7.1875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.125F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.28125F;
			this.Line1.Width = 7.1875F;
			this.Line1.X1 = 0.125F;
			this.Line1.X2 = 7.3125F;
			this.Line1.Y1 = 0.28125F;
			this.Line1.Y2 = 0.28125F;
			// 
			// srptEmployeeMatch
			// 
			this.srptEmployeeMatch.CloseBorder = false;
			this.srptEmployeeMatch.Height = 0.0625F;
			this.srptEmployeeMatch.Left = 0.125F;
			this.srptEmployeeMatch.Name = "srptEmployeeMatch";
			this.srptEmployeeMatch.Report = null;
			this.srptEmployeeMatch.Top = 0.53125F;
			this.srptEmployeeMatch.Width = 7.1875F;
			// 
			// srptEmployeeDeductions
			// 
			this.srptEmployeeDeductions.CloseBorder = false;
			this.srptEmployeeDeductions.Height = 0.09375F;
			this.srptEmployeeDeductions.Left = 0.125F;
			this.srptEmployeeDeductions.Name = "srptEmployeeDeductions";
			this.srptEmployeeDeductions.Report = null;
			this.srptEmployeeDeductions.Top = 0.4375F;
			this.srptEmployeeDeductions.Width = 7.1875F;
			// 
			// srptEmployeePayTotals
			// 
			this.srptEmployeePayTotals.CloseBorder = false;
			this.srptEmployeePayTotals.Height = 0.0625F;
			this.srptEmployeePayTotals.Left = 0.125F;
			this.srptEmployeePayTotals.Name = "srptEmployeePayTotals";
			this.srptEmployeePayTotals.Report = null;
			this.srptEmployeePayTotals.Top = 1.0625F;
			this.srptEmployeePayTotals.Width = 7.1875F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 9pt; font-weight: bold";
			this.Label33.Text = "Pay Totals";
			this.Label33.Top = 0.71875F;
			this.Label33.Width = 1.625F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.21875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.1875F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label34.Text = "Description";
			this.Label34.Top = 0.875F;
			this.Label34.Width = 1.125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.21875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 2.0625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label35.Text = "Current";
			this.Label35.Top = 0.875F;
			this.Label35.Width = 0.5625F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.21875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label36.Text = "MTD";
			this.Label36.Top = 0.875F;
			this.Label36.Width = 0.5625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.21875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 5F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label37.Text = "Calendar";
			this.Label37.Top = 0.875F;
			this.Label37.Width = 0.5625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.21875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 4.4375F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label43.Text = "Fiscal";
			this.Label43.Top = 0.875F;
			this.Label43.Width = 0.375F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.21875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 3.75F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label44.Text = "QTD";
			this.Label44.Top = 0.875F;
			this.Label44.Width = 0.625F;
			// 
			// subDirectDeposit
			// 
			this.subDirectDeposit.CloseBorder = false;
			this.subDirectDeposit.Height = 0.0625F;
			this.subDirectDeposit.Left = 0.125F;
			this.subDirectDeposit.Name = "subDirectDeposit";
			this.subDirectDeposit.Report = null;
			this.subDirectDeposit.Top = 0.375F;
			this.subDirectDeposit.Width = 7.1875F;
			// 
			// srptVacSick
			// 
			this.srptVacSick.CloseBorder = false;
			this.srptVacSick.Height = 0.0625F;
			this.srptVacSick.Left = 0.125F;
			this.srptVacSick.Name = "srptVacSick";
			this.srptVacSick.Report = null;
			this.srptVacSick.Top = 1.1875F;
			this.srptVacSick.Width = 7.1875F;
			// 
			// subMSRS
			// 
			this.subMSRS.CloseBorder = false;
			this.subMSRS.Height = 0.0625F;
			this.subMSRS.Left = 0.125F;
			this.subMSRS.Name = "subMSRS";
			this.subMSRS.Report = null;
			this.subMSRS.Top = 1.25F;
			this.subMSRS.Width = 7.1875F;
			// 
			// SubDistribution
			// 
			this.SubDistribution.CloseBorder = false;
			this.SubDistribution.Height = 0.0625F;
			this.SubDistribution.Left = 0.125F;
			this.SubDistribution.Name = "SubDistribution";
			this.SubDistribution.Report = null;
			this.SubDistribution.Top = 1.125F;
			this.SubDistribution.Width = 7.1875F;
			// 
			// rptEmployeeInformationReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.Script = "\r\nSub ActiveReport_ReportStart\r\n\r\nEnd Sub\r\n\r\nSub ActiveReport_FetchData(eof)\r\n\r\nE" + "nd Sub\r\n";
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}

        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEmployeeMaster;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEmployeeMatch;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEmployeeDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEmployeePayTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptVacSick;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubDistribution;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
