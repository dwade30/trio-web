//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPayrollDistribution.
	/// </summary>
	partial class frmPayrollDistribution
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkDD;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkMatches;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkDeductions;
		public FCGrid Grid;
		public FCGrid GridDistM;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCTextBox txtDed;
		public fecherFoundation.FCLabel lblTaxes;
		public fecherFoundation.FCLabel lblDeds;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkDD_0;
		public fecherFoundation.FCCheckBox chkDD_1;
		public fecherFoundation.FCCheckBox chkMatches_2;
		public fecherFoundation.FCCheckBox chkMatches_1;
		public fecherFoundation.FCCheckBox chkDeductions_1;
		public fecherFoundation.FCCheckBox chkVacation;
		public fecherFoundation.FCCheckBox chkMatches_0;
		public fecherFoundation.FCCheckBox chkDeductions_0;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public FCGrid GridTotals;
		public fecherFoundation.FCLabel lblAccountDesc;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuUpdateBaseRate;
		public fecherFoundation.FCToolStripMenuItem mnuSP5;
		public fecherFoundation.FCToolStripMenuItem mnuEmployeeSchedule;
		public fecherFoundation.FCToolStripMenuItem menusepar5;
		public fecherFoundation.FCToolStripMenuItem mnuDistributionScreen;
		public fecherFoundation.FCToolStripMenuItem mnuOpenDataEntryScreen;
		public fecherFoundation.FCToolStripMenuItem mnuLongAccountScreen;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuMultPays;
		public fecherFoundation.FCToolStripMenuItem mnuMultAdd;
		public fecherFoundation.FCToolStripMenuItem mnuCaption1;
		public fecherFoundation.FCToolStripMenuItem mnuCaption2;
		public fecherFoundation.FCToolStripMenuItem mnuCaption3;
		public fecherFoundation.FCToolStripMenuItem mnuCaption4;
		public fecherFoundation.FCToolStripMenuItem mnuCaption5;
		public fecherFoundation.FCToolStripMenuItem mnuCaption6;
		public fecherFoundation.FCToolStripMenuItem mnuCaption7;
		public fecherFoundation.FCToolStripMenuItem mnuCaption8;
		public fecherFoundation.FCToolStripMenuItem mnuCaption9;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Grid = new fecherFoundation.FCGrid();
			this.GridDistM = new fecherFoundation.FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtTax = new fecherFoundation.FCTextBox();
			this.txtDed = new fecherFoundation.FCTextBox();
			this.lblTaxes = new fecherFoundation.FCLabel();
			this.lblDeds = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkMatches_2 = new fecherFoundation.FCCheckBox();
			this.chkDD_0 = new fecherFoundation.FCCheckBox();
			this.chkDD_1 = new fecherFoundation.FCCheckBox();
			this.chkMatches_1 = new fecherFoundation.FCCheckBox();
			this.chkDeductions_1 = new fecherFoundation.FCCheckBox();
			this.chkVacation = new fecherFoundation.FCCheckBox();
			this.chkMatches_0 = new fecherFoundation.FCCheckBox();
			this.chkDeductions_0 = new fecherFoundation.FCCheckBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.GridTotals = new fecherFoundation.FCGrid();
			this.lblAccountDesc = new fecherFoundation.FCLabel();
			this.lblEmployeeNumber = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuDistributionScreen = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOpenDataEntryScreen = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLongAccountScreen = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMultPays = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMultAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption6 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption7 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption8 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCaption9 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUpdateBaseRate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmployeeSchedule = new fecherFoundation.FCToolStripMenuItem();
			this.menusepar5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdNewLine = new fecherFoundation.FCButton();
			this.cmdDeleteLine = new fecherFoundation.FCButton();
			this.cmdSelectEmployee = new fecherFoundation.FCButton();
			this.cmdUpdateBaseRates = new fecherFoundation.FCButton();
			this.cmdEmployeeSchedule = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdSaveExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDistM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDD_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDD_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVacation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdateBaseRates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmployeeSchedule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 752);
			this.BottomPanel.Size = new System.Drawing.Size(935, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.GridDistM);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.GridTotals);
			this.ClientArea.Controls.Add(this.lblAccountDesc);
			this.ClientArea.Controls.Add(this.lblEmployeeNumber);
			this.ClientArea.Size = new System.Drawing.Size(955, 606);
			this.ClientArea.Controls.SetChildIndex(this.lblEmployeeNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblAccountDesc, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridTotals, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDistM, 0);
			this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Controls.Add(this.cmdNewLine);
			this.TopPanel.Controls.Add(this.cmdDeleteLine);
			this.TopPanel.Controls.Add(this.cmdSelectEmployee);
			this.TopPanel.Controls.Add(this.cmdUpdateBaseRates);
			this.TopPanel.Controls.Add(this.cmdEmployeeSchedule);
			this.TopPanel.Size = new System.Drawing.Size(955, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdEmployeeSchedule, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdUpdateBaseRates, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSelectEmployee, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteLine, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNewLine, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(204, 28);
			this.HeaderText.Text = "Payroll Distribution";
			// 
			// Grid
			// 
			this.Grid.AllowDrag = true;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.Cols = 5;
			this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
			this.Grid.Location = new System.Drawing.Point(30, 390);
			this.Grid.Name = "Grid";
			this.Grid.Rows = 5;
			this.Grid.Size = new System.Drawing.Size(778, 203);
			this.Grid.TabIndex = 1;
			this.Grid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEdit);
			this.Grid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.Grid_KeyPressEdit);
			this.Grid.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
			this.Grid.AfterMoveRow += new System.EventHandler<fecherFoundation.AfterMoveRowEventArgs>(this.Grid_AfterMoveRow);
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			this.Grid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_ChangeEdit);
			this.Grid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.Grid_MouseMoveEvent);
			this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
			this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
			this.Grid.Enter += new System.EventHandler(this.Grid_Enter);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			// 
			// GridDistM
			// 
			this.GridDistM.Cols = 10;
			this.GridDistM.ColumnHeadersVisible = false;
			this.GridDistM.FixedCols = 0;
			this.GridDistM.FixedRows = 0;
			this.GridDistM.Location = new System.Drawing.Point(558, 682);
			this.GridDistM.Name = "GridDistM";
			this.GridDistM.RowHeadersVisible = false;
			this.GridDistM.Rows = 0;
			this.GridDistM.Size = new System.Drawing.Size(41, 70);
			this.GridDistM.TabIndex = 16;
			this.GridDistM.Visible = false;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtTax);
			this.Frame2.Controls.Add(this.txtDed);
			this.Frame2.Controls.Add(this.lblTaxes);
			this.Frame2.Controls.Add(this.lblDeds);
			this.Frame2.Location = new System.Drawing.Point(30, 75);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(354, 90);
			this.Frame2.TabIndex = 13;
			this.Frame2.Text = "Number Of Pay Periods";
			// 
			// txtTax
			// 
			this.txtTax.BackColor = System.Drawing.SystemColors.Window;
			this.txtTax.Location = new System.Drawing.Point(90, 30);
			this.txtTax.MaxLength = 3;
			this.txtTax.Name = "txtTax";
			this.txtTax.Size = new System.Drawing.Size(80, 40);
			this.txtTax.TabIndex = 1;
			this.txtTax.TextChanged += new System.EventHandler(this.txtTax_TextChanged);
			this.txtTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
			this.txtTax.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTax_KeyPress);
			// 
			// txtDed
			// 
			this.txtDed.BackColor = System.Drawing.SystemColors.Window;
			this.txtDed.Location = new System.Drawing.Point(254, 30);
			this.txtDed.MaxLength = 2;
			this.txtDed.Name = "txtDed";
			this.txtDed.Size = new System.Drawing.Size(80, 40);
			this.txtDed.TabIndex = 2;
			this.txtDed.TextChanged += new System.EventHandler(this.txtDed_TextChanged);
			this.txtDed.Validating += new System.ComponentModel.CancelEventHandler(this.txtDed_Validating);
			this.txtDed.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDed_KeyPress);
			// 
			// lblTaxes
			// 
			this.lblTaxes.Location = new System.Drawing.Point(20, 44);
			this.lblTaxes.Name = "lblTaxes";
			this.lblTaxes.Size = new System.Drawing.Size(50, 16);
			this.lblTaxes.TabIndex = 15;
			this.lblTaxes.Text = "TAXES";
			// 
			// lblDeds
			// 
			this.lblDeds.Location = new System.Drawing.Point(190, 44);
			this.lblDeds.Name = "lblDeds";
			this.lblDeds.Size = new System.Drawing.Size(45, 16);
			this.lblDeds.TabIndex = 14;
			this.lblDeds.Text = "DEDS";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkMatches_2);
			this.Frame1.Controls.Add(this.chkDD_0);
			this.Frame1.Controls.Add(this.chkDD_1);
			this.Frame1.Controls.Add(this.chkMatches_1);
			this.Frame1.Controls.Add(this.chkDeductions_1);
			this.Frame1.Controls.Add(this.chkVacation);
			this.Frame1.Controls.Add(this.chkMatches_0);
			this.Frame1.Controls.Add(this.chkDeductions_0);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(404, 75);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(299, 262);
			this.Frame1.TabIndex = 12;
			this.Frame1.Text = "Apply S(X) Checks Like Primary For";
			// 
			// chkMatches_2
			// 
			this.chkMatches_2.Location = new System.Drawing.Point(137, 117);
			this.chkMatches_2.Name = "chkMatches_2";
			this.chkMatches_2.Size = new System.Drawing.Size(72, 22);
			this.chkMatches_2.TabIndex = 7;
			this.chkMatches_2.Text = "Dollars";
			// 
			// chkDD_0
			// 
			this.chkDD_0.Checked = true;
			this.chkDD_0.CheckState = Wisej.Web.CheckState.Checked;
			this.chkDD_0.Location = new System.Drawing.Point(20, 178);
			this.chkDD_0.Name = "chkDD_0";
			this.chkDD_0.Size = new System.Drawing.Size(76, 22);
			this.chkDD_0.TabIndex = 20;
			this.chkDD_0.Text = "Percent";
			this.ToolTip1.SetToolTip(this.chkDD_0, "Percent of Gross Pay");
			// 
			// chkDD_1
			// 
			this.chkDD_1.Location = new System.Drawing.Point(137, 178);
			this.chkDD_1.Name = "chkDD_1";
			this.chkDD_1.Size = new System.Drawing.Size(72, 22);
			this.chkDD_1.TabIndex = 19;
			this.chkDD_1.Text = "Dollars";
			// 
			// chkMatches_1
			// 
			this.chkMatches_1.Checked = true;
			this.chkMatches_1.CheckState = Wisej.Web.CheckState.Checked;
			this.chkMatches_1.Location = new System.Drawing.Point(137, 117);
			this.chkMatches_1.Name = "chkMatches_1";
			this.chkMatches_1.Size = new System.Drawing.Size(56, 22);
			this.chkMatches_1.TabIndex = 6;
			this.chkMatches_1.Text = "% D";
			this.ToolTip1.SetToolTip(this.chkMatches_1, "Percent of Deduction");
			this.chkMatches_1.Visible = false;
			// 
			// chkDeductions_1
			// 
			this.chkDeductions_1.Location = new System.Drawing.Point(137, 55);
			this.chkDeductions_1.Name = "chkDeductions_1";
			this.chkDeductions_1.Size = new System.Drawing.Size(72, 22);
			this.chkDeductions_1.TabIndex = 4;
			this.chkDeductions_1.Text = "Dollars";
			// 
			// chkVacation
			// 
			this.chkVacation.Location = new System.Drawing.Point(20, 215);
			this.chkVacation.Name = "chkVacation";
			this.chkVacation.Size = new System.Drawing.Size(161, 22);
			this.chkVacation.TabIndex = 8;
			this.chkVacation.Text = "Vacation / Sick / Other";
			// 
			// chkMatches_0
			// 
			this.chkMatches_0.Checked = true;
			this.chkMatches_0.CheckState = Wisej.Web.CheckState.Checked;
			this.chkMatches_0.Location = new System.Drawing.Point(20, 117);
			this.chkMatches_0.Name = "chkMatches_0";
			this.chkMatches_0.Size = new System.Drawing.Size(76, 22);
			this.chkMatches_0.TabIndex = 5;
			this.chkMatches_0.Text = "Percent";
			this.ToolTip1.SetToolTip(this.chkMatches_0, "Percent of Gross Pay");
			// 
			// chkDeductions_0
			// 
			this.chkDeductions_0.Checked = true;
			this.chkDeductions_0.CheckState = Wisej.Web.CheckState.Checked;
			this.chkDeductions_0.Location = new System.Drawing.Point(20, 55);
			this.chkDeductions_0.Name = "chkDeductions_0";
			this.chkDeductions_0.Size = new System.Drawing.Size(76, 22);
			this.chkDeductions_0.TabIndex = 3;
			this.chkDeductions_0.Text = "Percent";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 154);
			this.Label3.Name = "Label3";
			this.Label3.TabIndex = 21;
			this.Label3.Text = "DIRECT DEPOSIT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 92);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(90, 15);
			this.Label2.TabIndex = 18;
			this.Label2.Text = "MATCHES";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(90, 15);
			this.Label1.TabIndex = 17;
			this.Label1.Text = "DEDUCTIONS";
			// 
			// GridTotals
			// 
			this.GridTotals.Cols = 3;
			this.GridTotals.ColumnHeadersHeight = 90;
			this.GridTotals.ColumnHeadersVisible = false;
			this.GridTotals.FixedCols = 0;
			this.GridTotals.FixedRows = 0;
			this.GridTotals.Location = new System.Drawing.Point(30, 612);
			this.GridTotals.Name = "GridTotals";
			this.GridTotals.RowHeadersVisible = false;
			this.GridTotals.Rows = 3;
			this.GridTotals.Size = new System.Drawing.Size(466, 82);
			this.GridTotals.TabIndex = 9;
			this.GridTotals.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridTotals_MouseMoveEvent);
			// 
			// lblAccountDesc
			// 
			this.lblAccountDesc.Location = new System.Drawing.Point(88, 357);
			this.lblAccountDesc.Name = "lblAccountDesc";
			this.lblAccountDesc.Size = new System.Drawing.Size(673, 32);
			this.lblAccountDesc.TabIndex = 11;
			// 
			// lblEmployeeNumber
			// 
			this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 30);
			this.lblEmployeeNumber.Name = "lblEmployeeNumber";
			this.lblEmployeeNumber.Size = new System.Drawing.Size(673, 45);
			this.lblEmployeeNumber.TabIndex = 10;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDistributionScreen,
            this.mnuOpenDataEntryScreen,
            this.mnuLongAccountScreen,
            this.mnuMultPays});
			this.MainMenu1.Name = null;
			// 
			// mnuDistributionScreen
			// 
			this.mnuDistributionScreen.Index = 0;
			this.mnuDistributionScreen.Name = "mnuDistributionScreen";
			this.mnuDistributionScreen.Text = "Distribution Screen";
			this.mnuDistributionScreen.Click += new System.EventHandler(this.mnuDistributionScreen_Click);
			// 
			// mnuOpenDataEntryScreen
			// 
			this.mnuOpenDataEntryScreen.Index = 1;
			this.mnuOpenDataEntryScreen.Name = "mnuOpenDataEntryScreen";
			this.mnuOpenDataEntryScreen.Text = "Data Entry Screen";
			this.mnuOpenDataEntryScreen.Click += new System.EventHandler(this.mnuOpenDataEntryScreen_Click);
			// 
			// mnuLongAccountScreen
			// 
			this.mnuLongAccountScreen.Index = 2;
			this.mnuLongAccountScreen.Name = "mnuLongAccountScreen";
			this.mnuLongAccountScreen.Text = "Long Account Screen";
			this.mnuLongAccountScreen.Click += new System.EventHandler(this.mnuLongAccountScreen_Click);
			// 
			// mnuMultPays
			// 
			this.mnuMultPays.Index = 3;
			this.mnuMultPays.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuMultAdd,
            this.mnuCaption1,
            this.mnuCaption2,
            this.mnuCaption3,
            this.mnuCaption4,
            this.mnuCaption5,
            this.mnuCaption6,
            this.mnuCaption7,
            this.mnuCaption8,
            this.mnuCaption9});
			this.mnuMultPays.Name = "mnuMultPays";
			this.mnuMultPays.Text = "Mult Pays";
			this.mnuMultPays.Visible = false;
			// 
			// mnuMultAdd
			// 
			this.mnuMultAdd.Enabled = false;
			this.mnuMultAdd.Index = 0;
			this.mnuMultAdd.Name = "mnuMultAdd";
			this.mnuMultAdd.Text = "Add/Edit Additional Pay Master";
			this.mnuMultAdd.Click += new System.EventHandler(this.mnuMultAdd_Click);
			// 
			// mnuCaption1
			// 
			this.mnuCaption1.Index = 1;
			this.mnuCaption1.Name = "mnuCaption1";
			this.mnuCaption1.Text = "Caption1";
			this.mnuCaption1.Click += new System.EventHandler(this.mnuCaption1_Click);
			// 
			// mnuCaption2
			// 
			this.mnuCaption2.Index = 2;
			this.mnuCaption2.Name = "mnuCaption2";
			this.mnuCaption2.Text = "Caption2";
			this.mnuCaption2.Click += new System.EventHandler(this.mnuCaption2_Click);
			// 
			// mnuCaption3
			// 
			this.mnuCaption3.Index = 3;
			this.mnuCaption3.Name = "mnuCaption3";
			this.mnuCaption3.Text = "Caption3";
			this.mnuCaption3.Click += new System.EventHandler(this.mnuCaption3_Click);
			// 
			// mnuCaption4
			// 
			this.mnuCaption4.Index = 4;
			this.mnuCaption4.Name = "mnuCaption4";
			this.mnuCaption4.Text = "Caption4";
			this.mnuCaption4.Click += new System.EventHandler(this.mnuCaption4_Click);
			// 
			// mnuCaption5
			// 
			this.mnuCaption5.Index = 5;
			this.mnuCaption5.Name = "mnuCaption5";
			this.mnuCaption5.Text = "Caption5";
			this.mnuCaption5.Click += new System.EventHandler(this.mnuCaption5_Click);
			// 
			// mnuCaption6
			// 
			this.mnuCaption6.Index = 6;
			this.mnuCaption6.Name = "mnuCaption6";
			this.mnuCaption6.Text = "Caption6";
			this.mnuCaption6.Click += new System.EventHandler(this.mnuCaption6_Click);
			// 
			// mnuCaption7
			// 
			this.mnuCaption7.Index = 7;
			this.mnuCaption7.Name = "mnuCaption7";
			this.mnuCaption7.Text = "Caption7";
			this.mnuCaption7.Click += new System.EventHandler(this.mnuCaption7_Click);
			// 
			// mnuCaption8
			// 
			this.mnuCaption8.Index = 8;
			this.mnuCaption8.Name = "mnuCaption8";
			this.mnuCaption8.Text = "Caption8";
			this.mnuCaption8.Click += new System.EventHandler(this.mnuCaption8_Click);
			// 
			// mnuCaption9
			// 
			this.mnuCaption9.Index = 9;
			this.mnuCaption9.Name = "mnuCaption9";
			this.mnuCaption9.Text = "Caption9";
			this.mnuCaption9.Click += new System.EventHandler(this.mnuCaption9_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = -1;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuNew.Text = "New Line";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = -1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Line";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = -1;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuSelectEmployee
			// 
			this.mnuSelectEmployee.Index = -1;
			this.mnuSelectEmployee.Name = "mnuSelectEmployee";
			this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuSelectEmployee.Text = "Select Employee";
			this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
			// 
			// mnuUpdateBaseRate
			// 
			this.mnuUpdateBaseRate.Index = -1;
			this.mnuUpdateBaseRate.Name = "mnuUpdateBaseRate";
			this.mnuUpdateBaseRate.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuUpdateBaseRate.Text = "Multiple Update Base Rates";
			this.mnuUpdateBaseRate.Click += new System.EventHandler(this.mnuUpdateBaseRate_Click);
			// 
			// mnuSP5
			// 
			this.mnuSP5.Index = -1;
			this.mnuSP5.Name = "mnuSP5";
			this.mnuSP5.Text = "-";
			// 
			// mnuEmployeeSchedule
			// 
			this.mnuEmployeeSchedule.Index = -1;
			this.mnuEmployeeSchedule.Name = "mnuEmployeeSchedule";
			this.mnuEmployeeSchedule.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuEmployeeSchedule.Text = "Employee Schedule";
			this.mnuEmployeeSchedule.Click += new System.EventHandler(this.mnuEmployeeSchedule_Click);
			// 
			// menusepar5
			// 
			this.menusepar5.Index = -1;
			this.menusepar5.Name = "menusepar5";
			this.menusepar5.Text = "-";
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = -1;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = -1;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = -1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = -1;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdNewLine
			// 
			this.cmdNewLine.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNewLine.Location = new System.Drawing.Point(283, 29);
			this.cmdNewLine.Name = "cmdNewLine";
			this.cmdNewLine.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdNewLine.Size = new System.Drawing.Size(78, 24);
			this.cmdNewLine.TabIndex = 1;
			this.cmdNewLine.Text = "New Line";
			this.cmdNewLine.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// cmdDeleteLine
			// 
			this.cmdDeleteLine.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteLine.Location = new System.Drawing.Point(367, 29);
			this.cmdDeleteLine.Name = "cmdDeleteLine";
			this.cmdDeleteLine.Size = new System.Drawing.Size(88, 24);
			this.cmdDeleteLine.TabIndex = 2;
			this.cmdDeleteLine.Text = "Delete Line";
			this.cmdDeleteLine.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdSelectEmployee
			// 
			this.cmdSelectEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSelectEmployee.Location = new System.Drawing.Point(461, 29);
			this.cmdSelectEmployee.Name = "cmdSelectEmployee";
			this.cmdSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdSelectEmployee.Size = new System.Drawing.Size(122, 24);
			this.cmdSelectEmployee.TabIndex = 3;
			this.cmdSelectEmployee.Text = "Select Employee";
			this.cmdSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
			// 
			// cmdUpdateBaseRates
			// 
			this.cmdUpdateBaseRates.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdUpdateBaseRates.Location = new System.Drawing.Point(589, 29);
			this.cmdUpdateBaseRates.Name = "cmdUpdateBaseRates";
			this.cmdUpdateBaseRates.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdUpdateBaseRates.Size = new System.Drawing.Size(190, 24);
			this.cmdUpdateBaseRates.TabIndex = 4;
			this.cmdUpdateBaseRates.Text = "Multiple Update Base Rates";
			this.cmdUpdateBaseRates.Click += new System.EventHandler(this.mnuUpdateBaseRate_Click);
			// 
			// cmdEmployeeSchedule
			// 
			this.cmdEmployeeSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEmployeeSchedule.Location = new System.Drawing.Point(785, 29);
			this.cmdEmployeeSchedule.Name = "cmdEmployeeSchedule";
			this.cmdEmployeeSchedule.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdEmployeeSchedule.Size = new System.Drawing.Size(142, 24);
			this.cmdEmployeeSchedule.TabIndex = 5;
			this.cmdEmployeeSchedule.Text = "Employee Schedule";
			this.cmdEmployeeSchedule.Click += new System.EventHandler(this.mnuEmployeeSchedule_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Enabled = false;
			this.cmdSave.Location = new System.Drawing.Point(218, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSave.Size = new System.Drawing.Size(60, 24);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save";
			this.cmdSave.Visible = false;
			// 
			// cmdSaveExit
			// 
			this.cmdSaveExit.AppearanceKey = "acceptButton";
			this.cmdSaveExit.Location = new System.Drawing.Point(405, 30);
			this.cmdSaveExit.Name = "cmdSaveExit";
			this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveExit.Size = new System.Drawing.Size(80, 48);
			this.cmdSaveExit.TabIndex = 2;
			this.cmdSaveExit.Text = "Save";
			this.cmdSaveExit.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmPayrollDistribution
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(955, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmPayrollDistribution";
			this.Text = "Payroll Distribution";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmPayrollDistribution_Load);
			this.Activated += new System.EventHandler(this.frmPayrollDistribution_Activated);
			this.Resize += new System.EventHandler(this.frmPayrollDistribution_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPayrollDistribution_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPayrollDistribution_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDistM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDD_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDD_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVacation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdateBaseRates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmployeeSchedule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdEmployeeSchedule;
		private FCButton cmdUpdateBaseRates;
		private FCButton cmdSelectEmployee;
		private FCButton cmdDeleteLine;
		private FCButton cmdNewLine;
		private FCButton cmdSave;
		private FCButton cmdSaveExit;
	}
}
