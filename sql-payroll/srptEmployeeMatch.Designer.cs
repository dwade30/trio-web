﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeMatch.
	/// </summary>
	partial class srptEmployeeMatch
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptEmployeeMatch));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountDP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCalender = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaxDH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountDP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalender)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxDH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDedCode,
				this.txtAmount,
				this.txtAmountDP,
				this.txtCurrent,
				this.txtMTD,
				this.txtCalender,
				this.txtFiscal,
				this.txtMax,
				this.txtMaxDH,
				this.txtAccount
			});
			this.Detail.Height = 0.1354167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape2,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20
			});
			this.ReportHeader.Height = 0.4791667F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Shape2
			// 
			this.Shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape2.Height = 0.3125F;
			this.Shape2.Left = 0F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.15625F;
			this.Shape2.Width = 7.125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label10.Text = "Employer Match";
			this.Label10.Top = 0.15625F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.21875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "Deduction";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.21875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label12.Text = "Amount";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 0.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.21875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.6875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label13.Text = "Type";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.21875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label14.Text = "Current";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.21875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label15.Text = "MTD";
			this.Label15.Top = 0.3125F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.21875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label16.Text = "Calendar";
			this.Label16.Top = 0.3125F;
			this.Label16.Width = 0.5625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.21875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label17.Text = "Fiscal";
			this.Label17.Top = 0.3125F;
			this.Label17.Width = 0.5625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.21875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 4.875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label18.Text = "Account #";
			this.Label18.Top = 0.3125F;
			this.Label18.Width = 0.875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.21875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.9375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label19.Text = "Maximum";
			this.Label19.Top = 0.3125F;
			this.Label19.Width = 0.625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.21875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 6.625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label20.Text = "Type";
			this.Label20.Top = 0.3125F;
			this.Label20.Width = 0.375F;
			// 
			// txtDedCode
			// 
			this.txtDedCode.Height = 0.19F;
			this.txtDedCode.Left = 0.0625F;
			this.txtDedCode.Name = "txtDedCode";
			this.txtDedCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDedCode.Text = "rtyhdfghdf";
			this.txtDedCode.Top = 0F;
			this.txtDedCode.Width = 0.875F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.19F;
			this.txtAmount.Left = 0.9375F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat");
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.6875F;
			// 
			// txtAmountDP
			// 
			this.txtAmountDP.Height = 0.19F;
			this.txtAmountDP.Left = 1.6875F;
			this.txtAmountDP.Name = "txtAmountDP";
			this.txtAmountDP.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAmountDP.Text = null;
			this.txtAmountDP.Top = 0F;
			this.txtAmountDP.Width = 0.375F;
			// 
			// txtCurrent
			// 
			this.txtCurrent.Height = 0.19F;
			this.txtCurrent.Left = 2.125F;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.OutputFormat = resources.GetString("txtCurrent.OutputFormat");
			this.txtCurrent.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtCurrent.Text = null;
			this.txtCurrent.Top = 0F;
			this.txtCurrent.Width = 0.6875F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.19F;
			this.txtMTD.Left = 2.8125F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.OutputFormat = resources.GetString("txtMTD.OutputFormat");
			this.txtMTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMTD.Text = null;
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.75F;
			// 
			// txtCalender
			// 
			this.txtCalender.Height = 0.19F;
			this.txtCalender.Left = 3.5625F;
			this.txtCalender.Name = "txtCalender";
			this.txtCalender.OutputFormat = resources.GetString("txtCalender.OutputFormat");
			this.txtCalender.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtCalender.Text = null;
			this.txtCalender.Top = 0F;
			this.txtCalender.Width = 0.625F;
			// 
			// txtFiscal
			// 
			this.txtFiscal.Height = 0.19F;
			this.txtFiscal.Left = 4.1875F;
			this.txtFiscal.Name = "txtFiscal";
			this.txtFiscal.OutputFormat = resources.GetString("txtFiscal.OutputFormat");
			this.txtFiscal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFiscal.Text = null;
			this.txtFiscal.Top = 0F;
			this.txtFiscal.Width = 0.625F;
			// 
			// txtMax
			// 
			this.txtMax.Height = 0.19F;
			this.txtMax.Left = 5.71875F;
			this.txtMax.Name = "txtMax";
			this.txtMax.OutputFormat = resources.GetString("txtMax.OutputFormat");
			this.txtMax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMax.Text = null;
			this.txtMax.Top = 0F;
			this.txtMax.Width = 0.875F;
			// 
			// txtMaxDH
			// 
			this.txtMaxDH.Height = 0.19F;
			this.txtMaxDH.Left = 6.625F;
			this.txtMaxDH.Name = "txtMaxDH";
			this.txtMaxDH.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMaxDH.Text = null;
			this.txtMaxDH.Top = 0F;
			this.txtMaxDH.Width = 0.875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 4.875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.875F;
			// 
			// srptEmployeeMatch
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountDP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalender)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxDH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountDP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalender;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaxDH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
