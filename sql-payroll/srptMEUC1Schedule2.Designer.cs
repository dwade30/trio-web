﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMEUC1Schedule2.
	/// </summary>
	partial class srptMEUC1Schedule2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMEUC1Schedule2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUCAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWage18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtWagePageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYearStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFederalEmployerID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWagePageTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWageTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEmployerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Image1,
            this.Barcode1,
            this.Label73,
            this.Label74,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Label6,
            this.txtName,
            this.Label8,
            this.txtUCAccount,
            this.Label9,
            this.Label12,
            this.Label14,
            this.Label15,
            this.Label16,
            this.txtSSN1,
            this.txtName1,
            this.txtWage1,
            this.Label18,
            this.txtSSN2,
            this.txtName2,
            this.txtWage2,
            this.txtSSN3,
            this.txtName3,
            this.txtWage3,
            this.txtSSN4,
            this.txtName4,
            this.txtWage4,
            this.txtSSN5,
            this.txtName5,
            this.txtWage5,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.txtSSN6,
            this.txtName6,
            this.txtWage6,
            this.Label29,
            this.txtSSN7,
            this.txtName7,
            this.txtWage7,
            this.Label31,
            this.txtSSN8,
            this.txtName8,
            this.txtWage8,
            this.Label33,
            this.txtSSN9,
            this.txtName9,
            this.txtWage9,
            this.Label35,
            this.txtSSN10,
            this.txtName10,
            this.txtWage10,
            this.Label37,
            this.txtSSN11,
            this.txtName11,
            this.txtWage11,
            this.Label39,
            this.txtSSN12,
            this.txtName12,
            this.txtWage12,
            this.Label41,
            this.txtSSN13,
            this.txtName13,
            this.txtWage13,
            this.Label43,
            this.txtSSN14,
            this.txtName14,
            this.txtWage14,
            this.Label45,
            this.txtSSN15,
            this.txtName15,
            this.txtWage15,
            this.Label47,
            this.txtSSN16,
            this.txtName16,
            this.txtWage16,
            this.Label49,
            this.txtSSN17,
            this.txtName17,
            this.txtWage17,
            this.Label51,
            this.txtSSN18,
            this.txtName18,
            this.txtWage18,
            this.Label53,
            this.Label66,
            this.Label67,
            this.txtWagePageTotal,
            this.txtWageTotal,
            this.txtPeriodStart,
            this.txtPeriodEnd,
            this.Field25,
            this.Line3,
            this.Label11,
            this.txtYearStart,
            this.txtYearEnd,
            this.txtFederalEmployerID,
            this.Label75});
            this.Detail.Height = 10.10417F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Image1
            // 
            this.Image1.Height = 1.447917F;
            this.Image1.HyperLink = null;
            this.Image1.ImageData = null;
            this.Image1.Left = 0.1145833F;
            this.Image1.LineWeight = 1F;
            this.Image1.Name = "Image1";
            this.Image1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
            this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image1.Top = 8.322917F;
            this.Image1.Width = 2.96875F;
            // 
            // Barcode1
            // 
            this.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
            this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode1.Height = 0.6875F;
            this.Barcode1.Left = 5.526F;
            this.Barcode1.Name = "Barcode1";
            this.Barcode1.QuietZoneBottom = 0F;
            this.Barcode1.QuietZoneLeft = 0F;
            this.Barcode1.QuietZoneRight = 0F;
            this.Barcode1.QuietZoneTop = 0F;
            this.Barcode1.Text = "1506402";
            this.Barcode1.Top = 0F;
            this.Barcode1.Width = 1.666667F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.1666667F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 7.25F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 12pt; font-weight: bold;" +
    " text-align: right";
            this.Label73.Text = "15";
            this.Label73.Top = 0.2083333F;
            this.Label73.Width = 0.25F;
            // 
            // Label74
            // 
            this.Label74.Height = 0.1666667F;
            this.Label74.HyperLink = null;
            this.Label74.Left = 0F;
            this.Label74.Name = "Label74";
            this.Label74.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.Label74.Text = "TSC";
            this.Label74.Top = 9.9375F;
            this.Label74.Visible = false;
            this.Label74.Width = 0.4166667F;
            // 
            // Shape17
            // 
            this.Shape17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape17.Height = 0.125F;
            this.Shape17.Left = 0.01388889F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 0.01388889F;
            this.Shape17.Width = 0.125F;
            // 
            // Shape18
            // 
            this.Shape18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape18.Height = 0.125F;
            this.Shape18.Left = 0.01041667F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 9.875F;
            this.Shape18.Width = 0.125F;
            // 
            // Shape19
            // 
            this.Shape19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape19.Height = 0.125F;
            this.Shape19.Left = 7.361111F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 9.875F;
            this.Shape19.Width = 0.125F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1666667F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.1875F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \\000027Tahoma\\000027; font-size: 6pt; vertical-align: middle";
            this.Label6.Text = "Name";
            this.Label6.Top = 0.3784722F;
            this.Label6.Width = 0.4166667F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1666667F;
            this.txtName.Left = 0.5833333F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName.Top = 0.3333334F;
            this.txtName.Width = 3.583333F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2916667F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.1980838F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \\000027Tahoma\\000027; font-size: 6pt; vertical-align: middle";
            this.Label8.Text = "UC Employer Account No:";
            this.Label8.Top = 0.6150278F;
            this.Label8.Width = 0.75F;
            // 
            // txtUCAccount
            // 
            this.txtUCAccount.Height = 0.1666667F;
            this.txtUCAccount.Left = 1.896F;
            this.txtUCAccount.Name = "txtUCAccount";
            this.txtUCAccount.Style = "font-family: \\000027Courier\\000020New\\000027; vertical-align: middle";
            this.txtUCAccount.Text = "9999999999";
            this.txtUCAccount.Top = 0.6810001F;
            this.txtUCAccount.Width = 1.416667F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1666667F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.25F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \\000027Tahoma\\000027; font-size: 9pt";
            this.Label9.Text = "SCHEDULE 2 (FORM ME UC-1) 2020";
            this.Label9.Top = 0F;
            this.Label9.Width = 3.25F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.228833F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.687F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt; font-weight: bold; text-align" +
    ": center; text-decoration: underline";
            this.Label12.Text = "Unemployment Contributions Wages Listing";
            this.Label12.Top = 1.449F;
            this.Label12.Width = 6F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1666667F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.127F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label14.Text = "11. Employee Name (Last, First, MI)";
            this.Label14.Top = 2.065F;
            this.Label14.Width = 2F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1666667F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 2.977F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label15.Text = "12. Social Security Number";
            this.Label15.Top = 2.065F;
            this.Label15.Width = 1.416667F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1666667F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 5.4F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: Tahoma; font-size: 7pt";
            this.Label16.Text = "13. UC Gross Wages Paid";
            this.Label16.Top = 2.065F;
            this.Label16.Width = 1.333333F;
            // 
            // txtSSN1
            // 
            this.txtSSN1.Height = 0.1979167F;
            this.txtSSN1.Left = 3.469F;
            this.txtSSN1.Name = "txtSSN1";
            this.txtSSN1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN1.Text = "999999999";
            this.txtSSN1.Top = 2.344F;
            this.txtSSN1.Width = 1.541778F;
            // 
            // txtName1
            // 
            this.txtName1.CanGrow = false;
            this.txtName1.Height = 0.1979167F;
            this.txtName1.Left = 0.3231668F;
            this.txtName1.Name = "txtName1";
            this.txtName1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName1.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName1.Top = 2.344F;
            this.txtName1.Width = 1.833333F;
            // 
            // txtWage1
            // 
            this.txtWage1.Height = 0.1979167F;
            this.txtWage1.Left = 5.5315F;
            this.txtWage1.Name = "txtWage1";
            this.txtWage1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage1.Text = "9999999999.99";
            this.txtWage1.Top = 2.340527F;
            this.txtWage1.Width = 1.416667F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1979167F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 0.04191677F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \\000027Tahoma\\000027";
            this.Label18.Text = "a.";
            this.Label18.Top = 2.344F;
            this.Label18.Width = 0.1666667F;
            // 
            // txtSSN2
            // 
            this.txtSSN2.Height = 0.1979167F;
            this.txtSSN2.Left = 3.469F;
            this.txtSSN2.Name = "txtSSN2";
            this.txtSSN2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN2.Text = "999999999";
            this.txtSSN2.Top = 2.677333F;
            this.txtSSN2.Width = 1.541778F;
            // 
            // txtName2
            // 
            this.txtName2.CanGrow = false;
            this.txtName2.Height = 0.1979167F;
            this.txtName2.Left = 0.3231668F;
            this.txtName2.Name = "txtName2";
            this.txtName2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName2.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName2.Top = 2.677333F;
            this.txtName2.Width = 1.833333F;
            // 
            // txtWage2
            // 
            this.txtWage2.Height = 0.1979167F;
            this.txtWage2.Left = 5.5315F;
            this.txtWage2.Name = "txtWage2";
            this.txtWage2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage2.Text = "9999999999.99";
            this.txtWage2.Top = 2.677333F;
            this.txtWage2.Width = 1.416667F;
            // 
            // txtSSN3
            // 
            this.txtSSN3.Height = 0.1979167F;
            this.txtSSN3.Left = 3.469F;
            this.txtSSN3.Name = "txtSSN3";
            this.txtSSN3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN3.Text = "999999999";
            this.txtSSN3.Top = 3.010666F;
            this.txtSSN3.Width = 1.541778F;
            // 
            // txtName3
            // 
            this.txtName3.CanGrow = false;
            this.txtName3.Height = 0.1979167F;
            this.txtName3.Left = 0.3231668F;
            this.txtName3.Name = "txtName3";
            this.txtName3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName3.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName3.Top = 3.010666F;
            this.txtName3.Width = 1.833333F;
            // 
            // txtWage3
            // 
            this.txtWage3.Height = 0.1979167F;
            this.txtWage3.Left = 5.5315F;
            this.txtWage3.Name = "txtWage3";
            this.txtWage3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage3.Text = "9999999999.99";
            this.txtWage3.Top = 3.010666F;
            this.txtWage3.Width = 1.416667F;
            // 
            // txtSSN4
            // 
            this.txtSSN4.Height = 0.1979167F;
            this.txtSSN4.Left = 3.469F;
            this.txtSSN4.Name = "txtSSN4";
            this.txtSSN4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN4.Text = "999999999";
            this.txtSSN4.Top = 3.344F;
            this.txtSSN4.Width = 1.541778F;
            // 
            // txtName4
            // 
            this.txtName4.CanGrow = false;
            this.txtName4.Height = 0.1979167F;
            this.txtName4.Left = 0.3231668F;
            this.txtName4.Name = "txtName4";
            this.txtName4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName4.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName4.Top = 3.344F;
            this.txtName4.Width = 1.833333F;
            // 
            // txtWage4
            // 
            this.txtWage4.Height = 0.1979167F;
            this.txtWage4.Left = 5.5315F;
            this.txtWage4.Name = "txtWage4";
            this.txtWage4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage4.Text = "9999999999.99";
            this.txtWage4.Top = 3.344F;
            this.txtWage4.Width = 1.416667F;
            // 
            // txtSSN5
            // 
            this.txtSSN5.Height = 0.1979167F;
            this.txtSSN5.Left = 3.469F;
            this.txtSSN5.Name = "txtSSN5";
            this.txtSSN5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN5.Text = "999999999";
            this.txtSSN5.Top = 3.677333F;
            this.txtSSN5.Width = 1.541778F;
            // 
            // txtName5
            // 
            this.txtName5.CanGrow = false;
            this.txtName5.Height = 0.1979167F;
            this.txtName5.Left = 0.3231668F;
            this.txtName5.Name = "txtName5";
            this.txtName5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName5.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName5.Top = 3.677333F;
            this.txtName5.Width = 1.8125F;
            // 
            // txtWage5
            // 
            this.txtWage5.Height = 0.1979167F;
            this.txtWage5.Left = 5.5315F;
            this.txtWage5.Name = "txtWage5";
            this.txtWage5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage5.Text = "9999999999.99";
            this.txtWage5.Top = 3.677333F;
            this.txtWage5.Width = 1.416667F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1979167F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0.04191677F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \\000027Tahoma\\000027";
            this.Label24.Text = "b.";
            this.Label24.Top = 2.677333F;
            this.Label24.Width = 0.1666667F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1979167F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 0.04191677F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \\000027Tahoma\\000027";
            this.Label25.Text = "c.";
            this.Label25.Top = 3.010666F;
            this.Label25.Width = 0.1666667F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1979167F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0.04191677F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \\000027Tahoma\\000027";
            this.Label26.Text = "d.";
            this.Label26.Top = 3.344F;
            this.Label26.Width = 0.1666667F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1979167F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0.04191677F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \\000027Tahoma\\000027";
            this.Label27.Text = "e.";
            this.Label27.Top = 3.677333F;
            this.Label27.Width = 0.1875F;
            // 
            // txtSSN6
            // 
            this.txtSSN6.Height = 0.1979167F;
            this.txtSSN6.Left = 3.46875F;
            this.txtSSN6.Name = "txtSSN6";
            this.txtSSN6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN6.Text = "999999999";
            this.txtSSN6.Top = 3.989583F;
            this.txtSSN6.Width = 1.541778F;
            // 
            // txtName6
            // 
            this.txtName6.CanGrow = false;
            this.txtName6.Height = 0.1979167F;
            this.txtName6.Left = 0.3229167F;
            this.txtName6.Name = "txtName6";
            this.txtName6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName6.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName6.Top = 4.020833F;
            this.txtName6.Width = 1.833333F;
            // 
            // txtWage6
            // 
            this.txtWage6.Height = 0.1979167F;
            this.txtWage6.Left = 5.53125F;
            this.txtWage6.Name = "txtWage6";
            this.txtWage6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage6.Text = "9999999999.99";
            this.txtWage6.Top = 3.989583F;
            this.txtWage6.Width = 1.416667F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1979167F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0.04166667F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \\000027Tahoma\\000027";
            this.Label29.Text = "f.";
            this.Label29.Top = 4.020833F;
            this.Label29.Width = 0.1666667F;
            // 
            // txtSSN7
            // 
            this.txtSSN7.Height = 0.1979167F;
            this.txtSSN7.Left = 3.46875F;
            this.txtSSN7.Name = "txtSSN7";
            this.txtSSN7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN7.Text = "999999999";
            this.txtSSN7.Top = 4.322917F;
            this.txtSSN7.Width = 1.541778F;
            // 
            // txtName7
            // 
            this.txtName7.CanGrow = false;
            this.txtName7.Height = 0.1979167F;
            this.txtName7.Left = 0.3229167F;
            this.txtName7.Name = "txtName7";
            this.txtName7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName7.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName7.Top = 4.354167F;
            this.txtName7.Width = 1.833333F;
            // 
            // txtWage7
            // 
            this.txtWage7.Height = 0.1979167F;
            this.txtWage7.Left = 5.53125F;
            this.txtWage7.Name = "txtWage7";
            this.txtWage7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage7.Text = "9999999999.99";
            this.txtWage7.Top = 4.322917F;
            this.txtWage7.Width = 1.416667F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1979167F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 0.04166667F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \\000027Tahoma\\000027";
            this.Label31.Text = "g.";
            this.Label31.Top = 4.354167F;
            this.Label31.Width = 0.1666667F;
            // 
            // txtSSN8
            // 
            this.txtSSN8.Height = 0.1979167F;
            this.txtSSN8.Left = 3.46875F;
            this.txtSSN8.Name = "txtSSN8";
            this.txtSSN8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN8.Text = "999999999";
            this.txtSSN8.Top = 4.65625F;
            this.txtSSN8.Width = 1.541778F;
            // 
            // txtName8
            // 
            this.txtName8.CanGrow = false;
            this.txtName8.Height = 0.1979167F;
            this.txtName8.Left = 0.3229167F;
            this.txtName8.Name = "txtName8";
            this.txtName8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName8.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName8.Top = 4.6875F;
            this.txtName8.Width = 1.833333F;
            // 
            // txtWage8
            // 
            this.txtWage8.Height = 0.1979167F;
            this.txtWage8.Left = 5.53125F;
            this.txtWage8.Name = "txtWage8";
            this.txtWage8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage8.Text = "9999999999.99";
            this.txtWage8.Top = 4.65625F;
            this.txtWage8.Width = 1.416667F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1979167F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 0.04166667F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \\000027Tahoma\\000027";
            this.Label33.Text = "h.";
            this.Label33.Top = 4.6875F;
            this.Label33.Width = 0.1666667F;
            // 
            // txtSSN9
            // 
            this.txtSSN9.Height = 0.1979167F;
            this.txtSSN9.Left = 3.46875F;
            this.txtSSN9.Name = "txtSSN9";
            this.txtSSN9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN9.Text = "999999999";
            this.txtSSN9.Top = 4.989583F;
            this.txtSSN9.Width = 1.541778F;
            // 
            // txtName9
            // 
            this.txtName9.CanGrow = false;
            this.txtName9.Height = 0.1979167F;
            this.txtName9.Left = 0.3229167F;
            this.txtName9.Name = "txtName9";
            this.txtName9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName9.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName9.Top = 5.020833F;
            this.txtName9.Width = 1.833333F;
            // 
            // txtWage9
            // 
            this.txtWage9.Height = 0.1979167F;
            this.txtWage9.Left = 5.53125F;
            this.txtWage9.Name = "txtWage9";
            this.txtWage9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage9.Text = "9999999999.99";
            this.txtWage9.Top = 4.989583F;
            this.txtWage9.Width = 1.416667F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.1979167F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 0.04166667F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \\000027Tahoma\\000027";
            this.Label35.Text = "i.";
            this.Label35.Top = 5.020833F;
            this.Label35.Width = 0.1666667F;
            // 
            // txtSSN10
            // 
            this.txtSSN10.Height = 0.1979167F;
            this.txtSSN10.Left = 3.46875F;
            this.txtSSN10.Name = "txtSSN10";
            this.txtSSN10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN10.Text = "999999999";
            this.txtSSN10.Top = 5.322917F;
            this.txtSSN10.Width = 1.541778F;
            // 
            // txtName10
            // 
            this.txtName10.CanGrow = false;
            this.txtName10.Height = 0.1979167F;
            this.txtName10.Left = 0.3229167F;
            this.txtName10.Name = "txtName10";
            this.txtName10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName10.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName10.Top = 5.364583F;
            this.txtName10.Width = 1.833333F;
            // 
            // txtWage10
            // 
            this.txtWage10.Height = 0.1979167F;
            this.txtWage10.Left = 5.53125F;
            this.txtWage10.Name = "txtWage10";
            this.txtWage10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage10.Text = "9999999999.99";
            this.txtWage10.Top = 5.322917F;
            this.txtWage10.Width = 1.416667F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.1979167F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 0.04166667F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \\000027Tahoma\\000027";
            this.Label37.Text = "j.";
            this.Label37.Top = 5.364583F;
            this.Label37.Width = 0.1666667F;
            // 
            // txtSSN11
            // 
            this.txtSSN11.Height = 0.1979167F;
            this.txtSSN11.Left = 3.46875F;
            this.txtSSN11.Name = "txtSSN11";
            this.txtSSN11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN11.Text = "999999999";
            this.txtSSN11.Top = 5.65625F;
            this.txtSSN11.Width = 1.541778F;
            // 
            // txtName11
            // 
            this.txtName11.CanGrow = false;
            this.txtName11.Height = 0.1979167F;
            this.txtName11.Left = 0.3229167F;
            this.txtName11.Name = "txtName11";
            this.txtName11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName11.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName11.Top = 5.697917F;
            this.txtName11.Width = 1.833333F;
            // 
            // txtWage11
            // 
            this.txtWage11.Height = 0.1979167F;
            this.txtWage11.Left = 5.53125F;
            this.txtWage11.Name = "txtWage11";
            this.txtWage11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage11.Text = "9999999999.99";
            this.txtWage11.Top = 5.65625F;
            this.txtWage11.Width = 1.416667F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1979167F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 0.04166667F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-family: \\000027Tahoma\\000027";
            this.Label39.Text = "k.";
            this.Label39.Top = 5.697917F;
            this.Label39.Width = 0.1666667F;
            // 
            // txtSSN12
            // 
            this.txtSSN12.Height = 0.1979167F;
            this.txtSSN12.Left = 3.46875F;
            this.txtSSN12.Name = "txtSSN12";
            this.txtSSN12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN12.Text = "999999999";
            this.txtSSN12.Top = 5.989583F;
            this.txtSSN12.Width = 1.541778F;
            // 
            // txtName12
            // 
            this.txtName12.CanGrow = false;
            this.txtName12.Height = 0.1979167F;
            this.txtName12.Left = 0.3229167F;
            this.txtName12.Name = "txtName12";
            this.txtName12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName12.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName12.Top = 6.03125F;
            this.txtName12.Width = 1.833333F;
            // 
            // txtWage12
            // 
            this.txtWage12.Height = 0.1979167F;
            this.txtWage12.Left = 5.53125F;
            this.txtWage12.Name = "txtWage12";
            this.txtWage12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage12.Text = "9999999999.99";
            this.txtWage12.Top = 5.989583F;
            this.txtWage12.Width = 1.416667F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.1979167F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 0.04166667F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-family: \\000027Tahoma\\000027";
            this.Label41.Text = "l.";
            this.Label41.Top = 6.03125F;
            this.Label41.Width = 0.1666667F;
            // 
            // txtSSN13
            // 
            this.txtSSN13.Height = 0.1979167F;
            this.txtSSN13.Left = 3.46875F;
            this.txtSSN13.Name = "txtSSN13";
            this.txtSSN13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN13.Text = "999999999";
            this.txtSSN13.Top = 6.322917F;
            this.txtSSN13.Width = 1.541778F;
            // 
            // txtName13
            // 
            this.txtName13.CanGrow = false;
            this.txtName13.Height = 0.1979167F;
            this.txtName13.Left = 0.3229167F;
            this.txtName13.Name = "txtName13";
            this.txtName13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName13.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName13.Top = 6.364583F;
            this.txtName13.Width = 1.833333F;
            // 
            // txtWage13
            // 
            this.txtWage13.Height = 0.1979167F;
            this.txtWage13.Left = 5.53125F;
            this.txtWage13.Name = "txtWage13";
            this.txtWage13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage13.Text = "9999999999.99";
            this.txtWage13.Top = 6.322917F;
            this.txtWage13.Width = 1.416667F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.1979167F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 0.04166667F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \\000027Tahoma\\000027";
            this.Label43.Text = "m.";
            this.Label43.Top = 6.364583F;
            this.Label43.Width = 0.1666667F;
            // 
            // txtSSN14
            // 
            this.txtSSN14.Height = 0.1979167F;
            this.txtSSN14.Left = 3.46875F;
            this.txtSSN14.Name = "txtSSN14";
            this.txtSSN14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN14.Text = "999999999";
            this.txtSSN14.Top = 6.65625F;
            this.txtSSN14.Width = 1.541778F;
            // 
            // txtName14
            // 
            this.txtName14.CanGrow = false;
            this.txtName14.Height = 0.1979167F;
            this.txtName14.Left = 0.3229167F;
            this.txtName14.Name = "txtName14";
            this.txtName14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName14.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName14.Top = 6.697917F;
            this.txtName14.Width = 1.833333F;
            // 
            // txtWage14
            // 
            this.txtWage14.Height = 0.1979167F;
            this.txtWage14.Left = 5.53125F;
            this.txtWage14.Name = "txtWage14";
            this.txtWage14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage14.Text = "9999999999.99";
            this.txtWage14.Top = 6.65625F;
            this.txtWage14.Width = 1.416667F;
            // 
            // Label45
            // 
            this.Label45.Height = 0.1979167F;
            this.Label45.HyperLink = null;
            this.Label45.Left = 0.04166667F;
            this.Label45.Name = "Label45";
            this.Label45.Style = "font-family: \\000027Tahoma\\000027";
            this.Label45.Text = "n.";
            this.Label45.Top = 6.697917F;
            this.Label45.Width = 0.1666667F;
            // 
            // txtSSN15
            // 
            this.txtSSN15.Height = 0.1979167F;
            this.txtSSN15.Left = 3.46875F;
            this.txtSSN15.Name = "txtSSN15";
            this.txtSSN15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN15.Text = "999999999";
            this.txtSSN15.Top = 6.989583F;
            this.txtSSN15.Width = 1.541778F;
            // 
            // txtName15
            // 
            this.txtName15.CanGrow = false;
            this.txtName15.Height = 0.1979167F;
            this.txtName15.Left = 0.3229167F;
            this.txtName15.Name = "txtName15";
            this.txtName15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName15.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName15.Top = 6.989583F;
            this.txtName15.Width = 1.833333F;
            // 
            // txtWage15
            // 
            this.txtWage15.Height = 0.1979167F;
            this.txtWage15.Left = 5.53125F;
            this.txtWage15.Name = "txtWage15";
            this.txtWage15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage15.Text = "9999999999.99";
            this.txtWage15.Top = 6.989583F;
            this.txtWage15.Width = 1.416667F;
            // 
            // Label47
            // 
            this.Label47.Height = 0.1979167F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 0.04166667F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-family: \\000027Tahoma\\000027";
            this.Label47.Text = "o.";
            this.Label47.Top = 7.03125F;
            this.Label47.Width = 0.1666667F;
            // 
            // txtSSN16
            // 
            this.txtSSN16.Height = 0.1979167F;
            this.txtSSN16.Left = 3.46875F;
            this.txtSSN16.Name = "txtSSN16";
            this.txtSSN16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN16.Text = "999999999";
            this.txtSSN16.Top = 7.322917F;
            this.txtSSN16.Width = 1.541778F;
            // 
            // txtName16
            // 
            this.txtName16.CanGrow = false;
            this.txtName16.Height = 0.1979167F;
            this.txtName16.Left = 0.3229167F;
            this.txtName16.Name = "txtName16";
            this.txtName16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName16.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName16.Top = 7.322917F;
            this.txtName16.Width = 1.833333F;
            // 
            // txtWage16
            // 
            this.txtWage16.Height = 0.1979167F;
            this.txtWage16.Left = 5.53125F;
            this.txtWage16.Name = "txtWage16";
            this.txtWage16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage16.Text = "9999999999.99";
            this.txtWage16.Top = 7.322917F;
            this.txtWage16.Width = 1.416667F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.1979167F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 0.04166667F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-family: \\000027Tahoma\\000027";
            this.Label49.Text = "p.";
            this.Label49.Top = 7.364583F;
            this.Label49.Width = 0.1666667F;
            // 
            // txtSSN17
            // 
            this.txtSSN17.Height = 0.1979167F;
            this.txtSSN17.Left = 3.46875F;
            this.txtSSN17.Name = "txtSSN17";
            this.txtSSN17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN17.Text = "999999999";
            this.txtSSN17.Top = 7.65625F;
            this.txtSSN17.Width = 1.541778F;
            // 
            // txtName17
            // 
            this.txtName17.CanGrow = false;
            this.txtName17.Height = 0.1979167F;
            this.txtName17.Left = 0.3229167F;
            this.txtName17.Name = "txtName17";
            this.txtName17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName17.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName17.Top = 7.65625F;
            this.txtName17.Width = 1.833333F;
            // 
            // txtWage17
            // 
            this.txtWage17.Height = 0.1979167F;
            this.txtWage17.Left = 5.53125F;
            this.txtWage17.Name = "txtWage17";
            this.txtWage17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage17.Text = "9999999999.99";
            this.txtWage17.Top = 7.65625F;
            this.txtWage17.Width = 1.416667F;
            // 
            // Label51
            // 
            this.Label51.Height = 0.1979167F;
            this.Label51.HyperLink = null;
            this.Label51.Left = 0.04166667F;
            this.Label51.Name = "Label51";
            this.Label51.Style = "font-family: \\000027Tahoma\\000027";
            this.Label51.Text = "q.";
            this.Label51.Top = 7.697917F;
            this.Label51.Width = 0.1666667F;
            // 
            // txtSSN18
            // 
            this.txtSSN18.Height = 0.1979167F;
            this.txtSSN18.Left = 3.472222F;
            this.txtSSN18.Name = "txtSSN18";
            this.txtSSN18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN18.Text = "999999999";
            this.txtSSN18.Top = 7.989584F;
            this.txtSSN18.Width = 1.541778F;
            // 
            // txtName18
            // 
            this.txtName18.CanGrow = false;
            this.txtName18.Height = 0.1979167F;
            this.txtName18.Left = 0.3229167F;
            this.txtName18.Name = "txtName18";
            this.txtName18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName18.Text = "XXXXXXXXXXXXXXXXXXX";
            this.txtName18.Top = 7.989584F;
            this.txtName18.Width = 1.833333F;
            // 
            // txtWage18
            // 
            this.txtWage18.Height = 0.1979167F;
            this.txtWage18.Left = 5.53125F;
            this.txtWage18.Name = "txtWage18";
            this.txtWage18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWage18.Text = "9999999999.99";
            this.txtWage18.Top = 7.989584F;
            this.txtWage18.Width = 1.416667F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.1979167F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 0.04166667F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-family: \\000027Tahoma\\000027";
            this.Label53.Text = "r.";
            this.Label53.Top = 8.041667F;
            this.Label53.Width = 0.1666667F;
            // 
            // Label66
            // 
            this.Label66.Height = 0.1666667F;
            this.Label66.HyperLink = null;
            this.Label66.Left = 3.229F;
            this.Label66.Name = "Label66";
            this.Label66.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label66.Text = "14. Total of column 13 on this page";
            this.Label66.Top = 8.521001F;
            this.Label66.Width = 2.1875F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.1666667F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 3.229F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label67.Text = "15. Total of columns 13 for ALL pages";
            this.Label67.Top = 8.854334F;
            this.Label67.Width = 2.3125F;
            // 
            // txtWagePageTotal
            // 
            this.txtWagePageTotal.Height = 0.1979167F;
            this.txtWagePageTotal.Left = 5.902611F;
            this.txtWagePageTotal.Name = "txtWagePageTotal";
            this.txtWagePageTotal.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWagePageTotal.Text = "9999999999.99";
            this.txtWagePageTotal.Top = 8.521001F;
            this.txtWagePageTotal.Width = 1.416667F;
            // 
            // txtWageTotal
            // 
            this.txtWageTotal.Height = 0.1979167F;
            this.txtWageTotal.Left = 5.906083F;
            this.txtWageTotal.Name = "txtWageTotal";
            this.txtWageTotal.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWageTotal.Text = "9999999999.99";
            this.txtWageTotal.Top = 8.854334F;
            this.txtWageTotal.Width = 1.416667F;
            // 
            // txtPeriodStart
            // 
            this.txtPeriodStart.Height = 0.1666667F;
            this.txtPeriodStart.Left = 4.91F;
            this.txtPeriodStart.Name = "txtPeriodStart";
            this.txtPeriodStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodStart.Text = "99 99";
            this.txtPeriodStart.Top = 1.031F;
            this.txtPeriodStart.Width = 0.9375F;
            // 
            // txtPeriodEnd
            // 
            this.txtPeriodEnd.Height = 0.1666667F;
            this.txtPeriodEnd.Left = 6.403057F;
            this.txtPeriodEnd.Name = "txtPeriodEnd";
            this.txtPeriodEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodEnd.Text = "99 99";
            this.txtPeriodEnd.Top = 1.027527F;
            this.txtPeriodEnd.Width = 0.9166667F;
            // 
            // Field25
            // 
            this.Field25.Height = 0.1666667F;
            this.Field25.Left = 5.951667F;
            this.Field25.Name = "Field25";
            this.Field25.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: cente" +
    "r; vertical-align: middle";
            this.Field25.Text = "-";
            this.Field25.Top = 1.031F;
            this.Field25.Visible = false;
            this.Field25.Width = 0.25F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 1.354167F;
            this.Line3.Width = 7.5F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.5F;
            this.Line3.Y1 = 1.354167F;
            this.Line3.Y2 = 1.354167F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1666667F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 3.541667F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \\000027Tahoma\\000027; font-size: 6pt; vertical-align: middle";
            this.Label11.Text = "Quarterly Period Covered";
            this.Label11.Top = 1.041667F;
            this.Label11.Width = 1.1875F;
            // 
            // txtYearStart
            // 
            this.txtYearStart.Height = 0.1666667F;
            this.txtYearStart.Left = 5.541945F;
            this.txtYearStart.Name = "txtYearStart";
            this.txtYearStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearStart.Text = "9999";
            this.txtYearStart.Top = 1.031F;
            this.txtYearStart.Width = 0.5F;
            // 
            // txtYearEnd
            // 
            this.txtYearEnd.Height = 0.1666667F;
            this.txtYearEnd.Left = 7.000278F;
            this.txtYearEnd.Name = "txtYearEnd";
            this.txtYearEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearEnd.Text = "9999";
            this.txtYearEnd.Top = 1.031F;
            this.txtYearEnd.Width = 0.4375F;
            // 
            // txtFederalEmployerID
            // 
            this.txtFederalEmployerID.Height = 0.1666667F;
            this.txtFederalEmployerID.Left = 2.062778F;
            this.txtFederalEmployerID.Name = "txtFederalEmployerID";
            this.txtFederalEmployerID.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.txtFederalEmployerID.Text = "99 999999999";
            this.txtFederalEmployerID.Top = 1.031F;
            this.txtFederalEmployerID.Width = 1.75F;
            // 
            // Label75
            // 
            this.Label75.Height = 0.1666667F;
            this.Label75.HyperLink = null;
            this.Label75.Left = 0.1979167F;
            this.Label75.Name = "Label75";
            this.Label75.Style = "font-family: \\000027Tahoma\\000027; font-size: 6pt; vertical-align: middle";
            this.Label75.Text = "Federal Employer ID No:";
            this.Label75.Top = 1.041667F;
            this.Label75.Width = 1.395833F;
            // 
            // srptMEUC1Schedule2
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWagePageTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWageTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEmployerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWagePageTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWageTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEmployerID;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
        private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
    }
}
