//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Schedule1.
	/// </summary>
	public partial class srptC1Schedule1 : FCSectionReport
	{
		public srptC1Schedule1()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1Schedule1 InstancePtr
		{
			get
			{
				return (srptC1Schedule1)Sys.GetInstance(typeof(srptC1Schedule1));
			}
		}

		protected srptC1Schedule1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1Schedule1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolTestPrint;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL = "";
                int X;
                double dblTotalWithheld = 0;
                double dblTotalPaid = 0;
                double dblWithheldSubtotalA = 0;
                double dblWithheldSubtotalB = 0;
                double dblWithheldSubtotalC = 0;
                double dblPaymentSubtotalA = 0;
                double dblPaymentSubtotalB = 0;
                double dblPaymentSubtotalC = 0;
                // vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, DateTime)
                DateTime dtTemp;
                int intQuarterCovered;
                int intMonthOne = 0;
                // vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
                string strYear = "";
                int intTemp;
                boolTestPrint = modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest;
                LoadInfo();
                if (!boolTestPrint)
                {
                    txtX.Text = "";
                    txtName.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerName);
                    txtWithholdingAccount.Text = Strings.Mid(EWRRecord.MRSWithholdingID, 1, 2) + " " +
                                                 Strings.Mid(EWRRecord.MRSWithholdingID, 3);
                    txtUCEmployerAccount.Text = EWRRecord.StateUCAccount;
                    intQuarterCovered = modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter;
                    switch (intQuarterCovered)
                    {
                        case 1:
                        {
                            intMonthOne = 1;
                            txtPeriodStart.Text =
                                "01 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "03 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            dtTemp = FCConvert.ToDateTime(Strings.Format(
                                "05/01/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear),
                                "MM/dd/yyyy"));
                            dtTemp = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp);
                            // txtFileBefore.Text = "04-" & Day(dtTemp) & "-" & Right(Year(dtTemp), 2)
                            break;
                        }
                        case 2:
                        {
                            intMonthOne = 4;
                            txtPeriodStart.Text =
                                "04 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "06 30 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            // txtFileBefore.Text = "07-31-" & Right(EWRWageTotals.lngYear, 2)
                            break;
                        }
                        case 3:
                        {
                            intMonthOne = 7;
                            txtPeriodStart.Text =
                                "07 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "09 30 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            // txtFileBefore.Text = "10-31-" & Right(EWRWageTotals.lngYear, 2)
                            break;
                        }
                        case 4:
                        {
                            intMonthOne = 10;
                            txtPeriodStart.Text =
                                "10 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "12 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            // txtFileBefore.Text = "01-31-" & Right(EWRWageTotals.lngYear + 1, 2)
                            break;
                        }
                    }

                    //end switch
                    strYear = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                    dblWithheldSubtotalA = 0;
                    dblWithheldSubtotalB = 0;
                    dblWithheldSubtotalC = 0;
                    dblPaymentSubtotalA = 0;
                    dblPaymentSubtotalB = 0;
                    dblPaymentSubtotalC = 0;
                    Label4.Text = Label4.Text + " " + strYear;
                    dblTotalWithheld = 0;
                    dblTotalPaid = 0;
                    for (X = 1; X <= 36; X++)
                    {
                        (this.Detail.Controls["txtDatePaid" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = "";
                        // Me.Detail.Controls("txtwithheld" & X).Text = ""
                        (this.Detail.Controls["txtAmount" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = "";
                    }

                    // X
                    // strSQL = "select * from tblStatePayments where paid > 0 order by datepaid"
                    strSQL =
                        "select * from tblStatePayments WHERE convert(float, isnull(withheld, 0)) > 0 or convert(float, isnull(paid, 0)) > 0 order by datepaid";
                    clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    X = 1;
                    while (!clsLoad.EndOfFile())
                    {
                        if (X > 63)
                            break;
                        strYear = FCConvert.ToString((DateTime) clsLoad.Get_Fields("datepaid").Year);
                        if (Convert.ToDateTime(clsLoad.Get_Fields("datepaid")).Month > intMonthOne)
                        {
                            if (Convert.ToDateTime(clsLoad.Get_Fields("datepaid")).Month == intMonthOne + 1)
                            {
                                if (X < 22)
                                    X = 22;
                            }
                            else
                            {
                                if (X < 43)
                                    X = 43;
                            }
                        }

                        (this.Detail.Controls["txtDatePaid" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = Strings.Format(clsLoad.Get_Fields("datepaid"), "mm dd yy");
                        // Me.Detail.Controls("txtwithheld" & X).Text = Format(Val(clsLoad.Fields("withheld")), "0.00")
                        (this.Detail.Controls["txtAmount" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("paid")), "0.00");
                        dblTotalWithheld += Conversion.Val(clsLoad.Get_Fields("withheld"));
                        dblTotalPaid += Conversion.Val(clsLoad.Get_Fields("paid"));
                        if (X < 22)
                        {
                            dblWithheldSubtotalA += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalA += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }
                        else if (X < 43)
                        {
                            dblWithheldSubtotalB += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalB += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }
                        else
                        {
                            dblWithheldSubtotalC += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalC += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }

                        X += 1;
                        clsLoad.MoveNext();
                    }

                    Barcode1.Text = Strings.Right(strYear, 2) + "08501";
                    // txtLine12.Text = Format(dblTotalWithheld, "0.00")
                    txtLine13.Text = Strings.Format(dblTotalPaid, "0.00");
                    // NOT USED BECAUSE OF CHECK BOX ON VOUCHER SCREEN.
                    // ***********************************************************************************************
                    // get the frequency of state tax
                    // MATTHEW 8/3/2005 CALL ID 74280
                    // Dim rsFreq As New clsDRWrapper
                    // Call rsFreq.OpenRecordset("SELECT tblCategories.ID, tblRecipients.Freq FROM tblRecipients INNER JOIN tblCategories ON tblRecipients.RecptNumber = tblCategories.Include where tblCategories.ID = 4")
                    // If Not rsFreq.EndOfFile Then
                    // If rsFreq.Fields("Freq") = "Quarterly" Then
                    // dblPaymentSubtotalA = 0
                    // dblPaymentSubtotalB = 0
                    // dblPaymentSubtotalC = 0
                    // txtLine13.Text = Format(0, "0.00")
                    // End If
                    // End If
                    // ***********************************************************************************************
                    // 
                    txtPaymentSub1.Text = Strings.Format(dblPaymentSubtotalA, "0.00");
                    // txtPaymentSubA.Text = Format(dblPaymentSubtotalA, "0.00")
                    // txtPaymentSubB.Text = Format(dblPaymentSubtotalB, "0.00")
                    if (dblPaymentSubtotalB > 0)
                    {
                        txtPaymentSub2.Text = Strings.Format(dblPaymentSubtotalB, "0.00");
                    }
                    else
                    {
                        txtPaymentSub2.Text = "";
                    }

                    // txtPaymentSubC.Text = Format(dblPaymentSubtotalC, "0.00")
                    if (dblPaymentSubtotalC > 0)
                    {
                        txtPaymentSub3.Text = Strings.Format(dblPaymentSubtotalC, "0.00");
                    }
                    else
                    {
                        txtPaymentSub3.Text = "";
                    }

                    // txtWithheldSubA.Text = Format(dblWithheldSubtotalA, "0.00")
                    // txtWithheldSub1.Text = Format(dblWithheldSubtotalA, "0.00")
                    // txtWithheldSubB.Text = Format(dblWithheldSubtotalB, "0.00")
                    // If dblWithheldSubtotalB > 0 Then
                    // txtWithheldSub2.Text = Format(dblWithheldSubtotalB, "0.00")
                    // Else
                    // txtWithheldSub2.Text = ""
                    // End If
                    // txtWithheldSubC.Text = Format(dblWithheldSubtotalC, "0.00")
                    // If dblWithheldSubtotalC > 0 Then
                    // txtWithheldSub3.Text = Format(dblWithheldSubtotalC, "0.00")
                    // Else
                    // txtWithheldSub3.Text = ""
                    // End If
                    // save the info so it can be used by other subreports
                    modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalPayments = dblTotalPaid;
                }
                else
                {
                    Label4.Text = Label4.Text + " " + FCConvert.ToString(DateTime.Today.Year);
                }
            }
        }

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

		
	}
}
