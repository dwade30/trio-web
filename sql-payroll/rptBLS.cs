//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBLS.
	/// </summary>
	public partial class rptBLS : BaseSectionReport
	{
		public rptBLS()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Multiple Worksite Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBLS InstancePtr
		{
			get
			{
				return (rptBLS)Sys.GetInstance(typeof(rptBLS));
			}
		}

		protected rptBLS _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBLS	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		// vbPorter upgrade warning: dtM1Start As DateTime	OnWrite(string)
		DateTime dtM1Start;
		// vbPorter upgrade warning: dtM2Start As DateTime	OnWrite(string)
		DateTime dtM2Start;
		// vbPorter upgrade warning: dtM3Start As DateTime	OnWrite(string)
		DateTime dtM3Start;
		// vbPorter upgrade warning: dtM1End As DateTime	OnWrite(string)
		DateTime dtM1End;
		// vbPorter upgrade warning: dtM2End As DateTime	OnWrite(DateTime, string)
		DateTime dtM2End;
		// vbPorter upgrade warning: dtM3End As DateTime	OnWrite(string)
		DateTime dtM3End;
		// vbPorter upgrade warning: lngM1Sub As int	OnWrite(int, double)
		int lngM1Sub;
		// vbPorter upgrade warning: lngM2Sub As int	OnWrite(int, double)
		int lngM2Sub;
		// vbPorter upgrade warning: lngM3Sub As int	OnWrite(int, double)
		int lngM3Sub;
		// vbPorter upgrade warning: lngM1Total As int	OnWrite(int, double)
		int lngM1Total;
		// vbPorter upgrade warning: lngM2Total As int	OnWrite(int, double)
		int lngM2Total;
		// vbPorter upgrade warning: lngM3Total As int	OnWrite(int, double)
		int lngM3Total;
		double dblQTD;
		double dblTotQTD;

		public void Init(ref int intQuarter, ref int lngYear, bool modalDialog, string strSequence = "", bool boolModal = false, bool boolHideMessages = true)
		{
			string strSQL;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			lngM1Total = 0;
			lngM2Total = 0;
			lngM3Total = 0;
			lngM1Sub = 0;
			lngM2Sub = 0;
			lngM3Sub = 0;
			dblQTD = 0;
			dblTotQTD = 0;
			modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStart, ref dtEnd, intQuarter, lngYear);
			strSQL = "select blsworksiteid,sum(distgrosspay) as qtdgross from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and rtrim(isnull(blsworksiteid, '')) <> '' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' group by blsworksiteid";
			rsReport.OpenRecordset(strSQL, "twpy0000.vb1");
			if (rsReport.EndOfFile())
			{
				if (!boolHideMessages)
				{
					MessageBox.Show("No worksite information to report", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				this.Close();
				return;
			}
			switch (intQuarter)
			{
				case 1:
					{
						dtM1Start = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
						dtM2Start = FCConvert.ToDateTime("2/1/" + FCConvert.ToString(lngYear));
						dtM3Start = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(lngYear));
						dtM1End = FCConvert.ToDateTime("1/31/" + FCConvert.ToString(lngYear));
						dtM2End = fecherFoundation.DateAndTime.DateAdd("d", -1, dtM3Start);
						dtM3End = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(lngYear));
						txtMonth1Label.Text = "January";
						txtMonth2Label.Text = "February";
						txtMonth3Label.Text = "March";
						break;
					}
				case 2:
					{
						dtM1Start = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(lngYear));
						dtM2Start = FCConvert.ToDateTime("5/1/" + FCConvert.ToString(lngYear));
						dtM3Start = FCConvert.ToDateTime("6/1/" + FCConvert.ToString(lngYear));
						dtM1End = FCConvert.ToDateTime("4/30/" + FCConvert.ToString(lngYear));
						dtM2End = FCConvert.ToDateTime("5/31/" + FCConvert.ToString(lngYear));
						dtM3End = FCConvert.ToDateTime("6/30/" + FCConvert.ToString(lngYear));
						txtMonth1Label.Text = "April";
						txtMonth2Label.Text = "May";
						txtMonth3Label.Text = "June";
						break;
					}
				case 3:
					{
						dtM1Start = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(lngYear));
						dtM2Start = FCConvert.ToDateTime("8/1/" + FCConvert.ToString(lngYear));
						dtM3Start = FCConvert.ToDateTime("9/1/" + FCConvert.ToString(lngYear));
						dtM1End = FCConvert.ToDateTime("7/31/" + FCConvert.ToString(lngYear));
						dtM2End = FCConvert.ToDateTime("8/31/" + FCConvert.ToString(lngYear));
						dtM3End = FCConvert.ToDateTime("9/30/" + FCConvert.ToString(lngYear));
						txtMonth1Label.Text = "July";
						txtMonth2Label.Text = "August";
						txtMonth3Label.Text = "September";
						break;
					}
				case 4:
					{
						dtM1Start = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(lngYear));
						dtM2Start = FCConvert.ToDateTime("11/1/" + FCConvert.ToString(lngYear));
						dtM3Start = FCConvert.ToDateTime("12/1/" + FCConvert.ToString(lngYear));
						dtM1End = FCConvert.ToDateTime("10/31/" + FCConvert.ToString(lngYear));
						dtM2End = FCConvert.ToDateTime("11/30/" + FCConvert.ToString(lngYear));
						dtM3End = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
						txtMonth1Label.Text = "October";
						txtMonth2Label.Text = "November";
						txtMonth3Label.Text = "December";
						break;
					}
			}
			//end switch
			if (!boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "BLSSummary", showModal: modalDialog);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "BLSSummary", showModal: modalDialog);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				this.Fields["grpHeader"].Value = Strings.Left(FCConvert.ToString(rsReport.Get_Fields("blsworksiteid")), 1);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				string[] strAry = null;
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    int x;
                    string strSQL = "";
                    dblQTD += Conversion.Val(rsReport.Get_Fields("qtdgross"));
                    dblTotQTD += Conversion.Val(rsReport.Get_Fields("qtdgross"));
                    txtQTD.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("qtdgross")), "#,###,##0.00");
                    strAry = Strings.Split(FCConvert.ToString(rsReport.Get_Fields("blsworksiteid")), "-", -1,
                        CompareConstants.vbTextCompare);
                    if (Information.UBound(strAry, 1) > 0)
                    {
                        txtWorksite.Text = fecherFoundation.Strings.Trim(strAry[1]);
                    }
                    else
                    {
                        // txtWorksite.Text = ""
                        txtWorksite.Text = FCConvert.ToString(rsReport.Get_Fields("blsworksiteid"));
                    }

                    // strSQL = "select count(enum) as thecount from (select tblemployeemaster.employeenumber as enum from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" & rsReport.Fields("blsworksiteid") & "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" & dtM1Start & "' and '" & dtM1End & "' group by tblemployeemaster.employeenumber)"
                    strSQL =
                        "select count(snum) as thecount from (select tblemployeemaster.ssn as snum from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" +
                        rsReport.Get_Fields("blsworksiteid") +
                        "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" +
                        FCConvert.ToString(dtM1Start) + "' and '" + FCConvert.ToString(dtM1End) +
                        "' group by tblemployeemaster.ssn) tbl1";
                    rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    if (!rsLoad.EndOfFile())
                    {
                        txtMonth1.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM1Sub += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM1Total += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                    }
                    else
                    {
                        txtMonth1.Text = "0";
                    }

                    // strSQL = "select count(enum) as thecount from (select tblemployeemaster.employeenumber as enum from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" & rsReport.Fields("blsworksiteid") & "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" & dtM2Start & "' and '" & dtM2End & "' group by tblemployeemaster.employeenumber)"
                    strSQL =
                        "select count(snum) as thecount from (select tblemployeemaster.ssn as snum from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" +
                        rsReport.Get_Fields("blsworksiteid") +
                        "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" +
                        FCConvert.ToString(dtM2Start) + "' and '" + FCConvert.ToString(dtM2End) +
                        "' group by tblemployeemaster.ssn) tbl1";
                    rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    if (!rsLoad.EndOfFile())
                    {
                        txtMonth2.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM2Sub += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM2Total += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                    }
                    else
                    {
                        txtMonth2.Text = "0";
                    }

                    // strSQL = "select count(enum) as thecount from (select tblemployeemaster.employeenumber as ENUM from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" & rsReport.Fields("blsworksiteid") & "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" & dtM3Start & "' and '" & dtM3End & "' group by tblemployeemaster.employeenumber)"
                    strSQL =
                        "select count(snum) as thecount from (select tblemployeemaster.ssn as sNUM from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and blsworksiteid = '" +
                        rsReport.Get_Fields("blsworksiteid") +
                        "' and (distu <> 'N' and distu <> '') and convert(int, isnull(distgrosspay, 0)) > 0 and distributionrecord = 1 and paydate between '" +
                        FCConvert.ToString(dtM3Start) + "' and '" + FCConvert.ToString(dtM3End) +
                        "' group by tblemployeemaster.ssn) tbl1";
                    rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    if (!rsLoad.EndOfFile())
                    {
                        txtMonth3.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM3Sub += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                        lngM3Total += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("thecount")));
                    }
                    else
                    {
                        txtMonth3.Text = "0";
                    }

                    rsReport.MoveNext();
                }
            }
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtM1Sub.Text = Strings.Format(lngM1Sub, "#,##0");
			txtM2Sub.Text = Strings.Format(lngM2Sub, "#,##0");
			txtM3Sub.Text = Strings.Format(lngM3Sub, "#,##0");
			txtQTDSub.Text = Strings.Format(dblQTD, "#,###,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtReport.Text = FCConvert.ToString(this.Fields["grpHeader"].Value);
			lngM1Sub = 0;
			lngM2Sub = 0;
			lngM3Sub = 0;
			dblQTD = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
