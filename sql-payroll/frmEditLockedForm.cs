﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEditLockedForm : BaseForm
	{
		public frmEditLockedForm()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditLockedForm InstancePtr
		{
			get
			{
				return (frmEditLockedForm)Sys.GetInstance(typeof(frmEditLockedForm));
			}
		}

		protected frmEditLockedForm _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 15,2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		// Private dbEdits         As DAO.Database
		private clsDRWrapper rsEdits = new clsDRWrapper();
		private int intCounter;
		private int intDataChanged;
		private bool boolUnload;

		private void cmdSelect_Click()
		{
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdProcess_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				/*? On Error Resume Next  */
				if (this.cmbEdit.Text == "Add new record")
				{
					FCUtils.CallByName(modGlobalVariables.Statics.gobjEditFormName, "NewRecord", CallType.Method);
				}
				else if (this.cmbEdit.Text == "Delete record")
				{
					FCUtils.CallByName(modGlobalVariables.Statics.gobjEditFormName, "DeleteRecord", CallType.Method);
				}
				else if (this.cmbEdit.Text == "Unlock Grid")
				{
					FCUtils.CallByName(modGlobalVariables.Statics.gobjEditFormName, "LockGridCells", CallType.Method, 0);
				}
				(modGlobalVariables.Statics.gobjEditFormName as FCForm).Show();
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEditLockedForm_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolUnload)
				{
					Close();
					return;
				}
                //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                //this.Show(FCForm.FormShowEnum.Modeless);
                this.Show(App.MainForm);
                if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEditLockedForm_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEditLockedForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditLockedForm properties;
			//frmEditLockedForm.ScaleWidth	= 4215;
			//frmEditLockedForm.ScaleHeight	= 3180;
			//frmEditLockedForm.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				this.Hide();
				boolUnload = false;
				if (Interaction.InputBox("Enter password to edit locked form.", "Security Password", "TRIO2001") != modGlobalConstants.DATABASEPASSWORD)
				{
					MessageBox.Show("Invalid password. User cannot access this functionality.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolUnload = true;
					return;
				}
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, FCConvert.ToInt16(true));
				rsEdits.DefaultDB = "TWPY0000.vb1";
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						// Call cmdSave_Click
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			(modGlobalVariables.Statics.gobjEditFormName as FCForm).Show();
			// set focus back to the menu options of the MDIParent
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}
	}
}
