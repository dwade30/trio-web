//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTACheckDetail.
	/// </summary>
	public partial class rptTACheckDetail : BaseSectionReport
	{
		public rptTACheckDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "TA Check Detail";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTACheckDetail InstancePtr
		{
			get
			{
				return (rptTACheckDetail)Sys.GetInstance(typeof(rptTACheckDetail));
			}
		}

		protected rptTACheckDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMaster?.Dispose();
				rsMain?.Dispose();
				rsDeduction?.Dispose();
                rsMaster = null;
                rsMain = null;
                rsDeduction = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTACheckDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsMaster = new clsDRWrapper();
		private clsDRWrapper rsDeduction = new clsDRWrapper();
		private int intPageNumber;
		private double dblGroupAmount;
		private bool boolClearGroupTotals;
		private bool boolDone;
		private int intDone;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				NextRecord:
				;
				if (rsMaster.EndOfFile())
				{
					this.Fields["grpHeader"].Value = "0";
					boolDone = true;
					frmWait.InstancePtr.Unload();
					eArgs.EOF = true;
					return;
				}
				this.Fields["grpHeader"].Value = rsMaster.Get_Fields("CheckNumber") + "-" + rsMaster.Get_Fields("paydate");
				txtDateField.Text = FCConvert.ToString(rsMaster.Get_Fields("PayDate"));
				txtPayRun.Text = rsMaster.Get_Fields_String("PayRunID");
				txtVoid.Text = (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("CheckVoid")) ? "  Y" : "  N");
				txtCheckNumber.Text = rsMaster.Get_Fields_String("CheckNumber");
				txtAmount.Text = Strings.Format(rsMaster.Get_Fields_Double("TrustAmount"), "0.00");
				dblGroupAmount += Conversion.Val(rsMaster.Get_Fields_Double("TrustAmount"));
				txtDescription.Text = rsMaster.Get_Fields_String("TrustDeductionDescription");
				if (rsMain.FindFirstRecord("ID", rsMaster.Get_Fields_Int32("TrustRecipientID")))
				{
					txtRecipient.Text = rsMain.Get_Fields_String("Name") + "(" + rsMain.Get_Fields("Freq") + ")";
				}
				else
				{
					txtRecipient.Text = string.Empty;
				}
				if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TAAdjustRecord")))
				{
					txtDeduction.Text = "TA Adjustment Record";
				}
				else
				{
					txtDeduction.Text = string.Empty;
				}
				if (!rsMaster.EndOfFile())
					rsMaster.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intPageNumber = 0;
			rsMaster.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, modGlobalVariables.DEFAULTDATABASE);
			rsMain.OpenRecordset("Select * from tblRecipients");
			rsDeduction.OpenRecordset("Select * from tblDeductionSetup");
			lblSort.Text = modGlobalVariables.Statics.gstrCheckListingSort;
			boolDone = false;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolClearGroupTotals = false;
			if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}
		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtGroupAmount As object	OnWrite(string)
			if (boolDone)
			{
				txtGroupAmount.Text = Strings.Format(dblGroupAmount, "0.00");
			}
			else
			{
				txtGroupAmount.Text = Strings.Format(dblGroupAmount + (FCConvert.ToDouble(txtAmount.Text) * -1), "0.00");
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (boolClearGroupTotals)
			{
				dblGroupAmount = FCConvert.ToDouble(txtAmount.Text);
			}
			else
			{
				boolClearGroupTotals = true;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
