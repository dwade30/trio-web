﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptC1Laser.
	/// </summary>
	public partial class rptC1Laser : BaseSectionReport
	{
		public rptC1Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "State Unemployment C-1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptC1Laser InstancePtr
		{
			get
			{
				return (rptC1Laser)Sys.GetInstance(typeof(rptC1Laser));
			}
		}

		protected rptC1Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptC1Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double dblRate;
		double dblLimit;
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		string strSeq;
		int intQuarterCovered;
		int lngYearCovered;
		bool boolPlain;
		double dblCSSFRate;

		public void Init(double dblRt, double dblLmt, ref int intQuarter, ref int lngYear, ref string strSequence, bool boolJust941 = false, double dblCRate = 0, bool boolPrintTest = false)
		{
			dblRate = dblRt;
			dblCSSFRate = dblCRate;
			dblLimit = dblLmt;
			intQuarterCovered = intQuarter;
			lngYearCovered = lngYear;
			strSeq = strSequence;
			modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter = intQuarter;
			modCoreysSweeterCode.Statics.EWRWageTotals.lngYear = lngYear;
			modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest = boolPrintTest;
			boolPlain = boolJust941;
			if (boolPlain)
			{
				this.Name = "941";
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName);
				// Me.PrintReport (False)
			}
			else
			{
				// Me.Show vbModal, MDIParent
				// Call frmReportViewer.Init(Me, , 1)
				modCoreysSweeterCode.CheckDefaultPrint(this, "", 1, boolAllowEmail: false);
			}
		}

		private void LoadData()
		{
			string strUnEmpQuery1 = "";
			string strUnEmpQuery2 = "";
			string strSQL2 = "";
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			string[] strAry = null;
			string strRange = "";
			string strQuery1 = "";
			string strQuery2 = "";
			string strQuery3 = "";
			string strQuery4 = "";
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			string strSQL = "";
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modCoreysSweeterCode.Statics.EWRWageTotals.dblExcessWages = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblLimit = dblLimit;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblRate = dblRate;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblCSSFRate = dblCSSFRate;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalGrossReportableWages = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalStateWithheld = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthWorkers = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthWorkers = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthFemales = 0;
				modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthWorkers = 0;
				strAry = Strings.Split(strSeq, ",", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) < 1)
				{
					// all or single
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				}
				else
				{
					// range
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
				}
				if (lngSeqStart >= 0)
				{
					// strRange = " and seqnumber between " & lngSeqStart & " and " & lngSeqEnd
					strRange = " where seqnumber between " + FCConvert.ToString(lngSeqStart) + " and " + FCConvert.ToString(lngSeqEnd);
				}
				else
				{
					strRange = "";
				}
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarterCovered, lngYearCovered);
				if (!boolPlain)
				{
					// YTD is not same as qtd
					strSQL = "select employeenumber as enum,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where distributionrecord = 1 and Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and not msrsadjustrecord = 1 and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
					strUnEmpQuery1 = "(" + strSQL + ") as UnEmpQuery1 ";
					strSQL = "select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where distributionrecord = 1 and paydate between '" + "1/1/" + FCConvert.ToString(lngYearCovered) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and (distu <> 'N' and distu <> '') and not msrsadjustrecord = 1 group by employeenumber ";
					strUnEmpQuery2 = "(" + strSQL + ") as UnEmpQuery2 ";
					strQuery1 = "(Select * from " + strUnEmpQuery2 + " left join " + strUnEmpQuery1 + " on (unempquery1.enum = unempquery2.employeenum)) as C1Query1";
					strQuery2 = "(select employeenumber,sum(grosspay) as qtdTotalGross,sum(stateTaxWH) as qtdStateWH from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and not msrsadjustrecord = 1 group by employeenumber having (sum(grosspay) > 0 or sum(statetaxwh) > 0) ) as C1Query2 ";
					// corey 06/04/2004   'added a where clause so we don't get people with no state tax and no gross pay
					strQuery3 = "(select * from " + strQuery2 + " left join " + strQuery1 + " on (c1query1.employeeNUM = c1query2.employeenumBER) WHERE (qtdgrosspay > 0 or qtdstatewh > 0)) as C1Query3";
					strQuery4 = "(select * from " + strQuery3 + " left join (select employeenumber as e4num,unemployment as seasonal from tblMiscUpdate) as C1Query4 on (c1query4.e4num = c1query3.employeenumber)) as C1Query5 ";
					strSQL = "select *,TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery4 + " on (tblemployeemaster.employeenumber = c1query5.employeenumber)   " + strRange + " order by tblemployeemaster.lastname,tblemployeemaster.firstname";
					// End If
					SubReport1.Report = new srptC1Schedule1();
					SubReport2.Report = new srptC1Part4();
					SubReport2.Report.UserData = strSQL;
					SubReport3.Report = new srptNewC1LaserFirst();
				}
				else
				{
					// sql is much simpler since we don't need any unemployment info, just statetaxwh
					// query1 sums up state tax wh for each employee
					// the main sql statement just inner joins that with employeemaster table
					// MATTHEW 6/30/2005 CALL ID 71812
					// strQuery1 = "(Select employeenumber,sum(statetaxWH) as qtdStateWH,SUM(DISTGROSSPAY) AS qtdGrossPay from tblcheckdetail where distributionrecord = 1 and paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 group by employeenumber having sum(statetaxwh) > 0) as Query941"
					// strQuery1 = "(Select employeenumber,sum(statetaxWH) as qtdStateWH,SUM(DISTGROSSPAY) AS qtdGrossPay from tblcheckdetail where paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and checkvoid = 0 and not msrsadjustrecord group by employeenumber having sum(statetaxwh) > 0) as Query941"
					strQuery1 = "(Select employeenumber,sum(statetaxWH) as qtdStateWH,SUM(statetaxgross) AS qtdGrossPay from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and not msrsadjustrecord = 1 group by employeenumber having sum(statetaxwh) > 0) as Query941";
					strSQL = "Select *,tblemployeemaster.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery1 + "  on (tblemployeemaster.employeenumber = query941.employeenumber) " + strRange + " order by tblemployeemaster.lastname,tblemployeemaster.firstname,tblemployeemaster.employeenumber";
					SubReport1.Report = new srptC1Plain();
					SubReport2.Report = new srptC1Part4Plan();
					SubReport2.Report.UserData = strSQL;
					ReportFooter.Visible = false;
					// Set SubReport3 = srptC1Plain
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			LoadData();
			// Call SetFixedSizeReport(Me, MDIParent.Grid)
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			srptC1Schedule1.InstancePtr.Unload();
			srptC1Part4.InstancePtr.Unload();
			srptNewC1LaserFirst.InstancePtr.Unload();
			srptC1Plain.InstancePtr.Unload();
			srptC1Part4Plan.InstancePtr.Unload();
		}
	}
}
