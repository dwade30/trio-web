//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmCheckSequence : BaseForm
	{
		public frmCheckSequence()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckSequence InstancePtr
		{
			get
			{
				return (frmCheckSequence)Sys.GetInstance(typeof(frmCheckSequence));
			}
		}

		protected frmCheckSequence _InstancePtr = null;
		//=========================================================
		int lngWarrant;
		int lngJournal;
		int lngReprintWarrant;
		bool boolMustUnload;
		private bool boolPrintValidChecksSeparate;
		private cBankService bankService = new cBankService();
		private cSettingsController setCont = new cSettingsController();

		private void frmCheckSequence_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (boolMustUnload)
				Close();
		}

		private void frmCheckSequence_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCheckSequence_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vsElasticLight1.Enabled = True
			boolMustUnload = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			lblPayDate.Text = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			lblRunNumber.Text = FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
			txtStartReprint.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			txtEndReprint.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			txtCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
			txtPayDate.Text = FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate);
			txtPayRunID.Text = FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
			boolPrintValidChecksSeparate = false;
			clsLoad.OpenRecordset("select PrintInvalidSeparate from tbldefaultINFORMATION", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				boolPrintValidChecksSeparate = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("printinvalidseparate"));
				if (boolPrintValidChecksSeparate)
				{
					txtStartingNonNegotiable.Visible = true;
					Label9.Visible = true;
				}
				else
				{
					txtStartingNonNegotiable.Visible = false;
					Label9.Visible = false;
				}
			}
			clsLoad.OpenRecordset("Select Top 1 WarrantNumber from tblCheckDetail where  PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "TWPY0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (Conversion.Val(clsLoad.Get_Fields_Int32("WarrantNumber")) > 0)
				{
					lngWarrant = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("WarrantNumber"))));
					lblWarrant.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("WarrantNumber")));
				}
			}
			clsLoad.OpenRecordset("Select Top 1 JournalNumber from tblCheckDetail where  PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "TWPY0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (Conversion.Val(clsLoad.Get_Fields("JournalNumber")) > 0)
				{
					lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("JournalNumber"))));
					lblJournal.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("JournalNumber")));
				}
			}
			// check for checks to print
			clsLoad.OpenRecordset("Select Top 1 PayRunID from tblCheckDetail where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and convert(int, isnull(checknumber, 0)) = 0", "TWPY0000.vb1");
			if (clsLoad.EndOfFile())
			{
				if (cmbReprint.Items.Contains("Initial Run"))
				{
					cmbReprint.Items.Remove("Initial Run");

                    if (cmbReprint.Items.Count > 0)
                    {
                        cmbReprint.SelectedIndex = 0;
                    }
                }
			}
			// check for checks to reprint
			clsLoad.OpenRecordset("Select Top 1 CheckNumber,WarrantNumber from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and convert(int, isnull(checknumber, 0)) > 0 order by checknumber", "twpy0000.vb1");
			if (clsLoad.EndOfFile())
			{
				if (cmbReprint.Items.Contains("Reprint"))
				{
					cmbReprint.Items.Remove("Reprint");

                    if (cmbReprint.Items.Count > 0)
                    {
                        cmbReprint.SelectedIndex = 0;
                    }
                }
			}
			else
			{
				txtStartReprint.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("CheckNumber")));
				lngReprintWarrant = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("WarrantNumber"))));
				clsLoad.OpenRecordset("Select Top 1 CheckNumber from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and convert(int, isnull(checknumber, 0)) > 0  order by   checknumber desc", "twpy0000.vb1");
				txtEndReprint.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("CheckNumber")));
			}
			if (!cmbReprint.Items.Contains("Initial Run"))
			{
				if (!cmbReprint.Items.Contains("Reprint"))
				{
					// there is nothing to print at all
					MessageBox.Show("There are no checks to print or reprint for Paydate " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " and Payrun " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "No Checks", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					boolMustUnload = true;
				}
				else
				{
					cmbReprint.Text = "Reprint";
					lblWarrant.Text = FCConvert.ToString(lngReprintWarrant);
				}
			}
			string strTemp = "";
			if (modGlobalConstants.Statics.gboolBD)
			{
				strTemp = fecherFoundation.Strings.LCase(setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", ""));
				if (strTemp == "bank" || strTemp == "check type")
				{
					// If CBool(strTemp) Then
					int lngStartingCheckNumber = 0;
					lngStartingCheckNumber = GetStartingCheckNumber();
					if (lngStartingCheckNumber > 0)
					{
						txtStartingCheck.Text = FCConvert.ToString(lngStartingCheckNumber);
					}
					else
					{
						txtStartingCheck.Text = "";
					}
					int lngNNStartingCheckNumber = 0;
					lngNNStartingCheckNumber = GetStartingNonNegotiableCheckNumber();
					if (lngNNStartingCheckNumber > 0)
					{
						txtStartingNonNegotiable.Text = FCConvert.ToString(lngNNStartingCheckNumber);
					}
					else
					{
						txtStartingNonNegotiable.Text = "";
					}
					// End If
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
 
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modDavesSweetCode.UnlockWarrant();
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			bool boolPrintSig;
			string strEnteredPassword = "";
			// vbPorter upgrade warning: intResponse As int	OnWrite(DialogResult)
			DialogResult intResponse = 0;
			int lngNumChecks;
			int lngStart;
			int lngEnd;
			bool boolACH;
			string strSQL = "";
			int lngID = 0;
			int lngBank = 0;
			int lngNonNegStart = 0;
			
			if (Conversion.Val(txtStartingCheck.Text) < 1)
			{
				MessageBox.Show("You must enter a valid starting check number to continue.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolPrintValidChecksSeparate)
			{
				if (Conversion.Val(txtStartingNonNegotiable.Text) < 1)
				{
					MessageBox.Show("You must enter a valid starting check number for non-negotiable checks", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			modGlobalVariables.Statics.gstrCheckMessage = fecherFoundation.Strings.Trim(txtCheckMessage.Text);
			boolPrintSig = false;
			clsLoad.OpenRecordset("select PRINTsignature from tbldefaultinformation", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PrintSignature")))
				{
					// must get password
					UsePassword:
					;
					intResponse = MessageBox.Show("Do you want a signature to print on the checks?", "Print Signature?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResponse == DialogResult.Yes)
					{
						TryPassword:
						;
						lngID = frmSignaturePassword.InstancePtr.Init(modSignatureFile.PAYROLLSIG);
						if (lngID == 0)
						{
							intResponse = MessageBox.Show("Incorrect Password. Re-enter password?", "Incorrect Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intResponse == DialogResult.Yes)
								goto TryPassword;
						}
						else if (lngID == -1)
						{
							// cancelled
							boolPrintSig = false;
							if (MessageBox.Show("Print checks without signature?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
							{
								return;
							}
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_6("PY", "Electronic Signature Used", modGlobalVariables.Statics.gstrUser, DateTime.Today.ToShortDateString(), Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "MM/dd/yyyy"));
							boolPrintSig = true;
							if (!modSignatureFile.SigFileExists(ref lngID))
							{
								boolPrintSig = false;
								if (MessageBox.Show("Signature file is missing.  Print checks without signature?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
								{
									return;
								}
							}
						}
						// SetupSigInformation
						// 
						// strEnteredPassword = UCase(frmSigPassword.Init)
						// If strEnteredPassword = vbNullString Then GoTo UsePassword
						// If UCase(gstrPayrollSigPassword) = strEnteredPassword Then
						// AddCYAEntry "PY", "Electronic Signature Used", gstrUser, Date, Time
						// 
						// boolPrintSig = True
						// Else
						// intResponse = MsgBox("Incorrect Password. Re-enter password?", vbQuestion + vbYesNo, "Incorrect Password")
						// If intResponse = vbYes Then GoTo TryPassword
						// End If
					}
				}
			}
			clsDRWrapper rsData = new clsDRWrapper();
			if (cmbReprint.Items.Contains("Initial Run") && cmbReprint.Text == "Initial Run")
			{
				lngWarrant = lngReprintWarrant;
				lngNonNegStart = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStartingNonNegotiable.Text)));
				MessageBox.Show("Load Checks.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				rsData.OpenRecordset("select banknumber from tblpayrollsteps where paydate = '" + this.lblPayDate.Text + "' and payrunid = " + this.lblRunNumber.Text, "twpy0000.vb1");
				lngBank = rsData.EndOfFile() 
                    ? 0 
                    : (int) FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("banknumber"))));
				if (!modCoreysSweeterCode.PrintPayrollChecks(false, FCConvert.ToInt32(Conversion.Val(txtStartingCheck.Text)), lngWarrant, lngJournal, boolPrintSig, lngJournal, 0, lngID, lngBank, lngNonNegStart))
				{
					Close();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
			}
			else if (cmbReprint.Text == "Reprint" && cmbReprint.Items.Contains("Reprint"))
			{
				// MATTHEW 9/28/2004
				// CALL #55969 HAS MADE IT SO THAT WE NEED TO CLEAR OUT THE CHECK REGISTER
				// AND WARRANT FLAGS WHEN THE USER DOES A REPRINT OF CHECKS SO THAT THE CHECK
				// NUMBERS IN PY EQUAL THAT OF CHECK RECONCILIATION IN BD.
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + this.lblPayDate.Text + "' AND PayRunID = " + this.lblRunNumber.Text, "TWPY0000.vb1");
				if (rsData.EndOfFile())
				{
					lngBank = 0;
				}
				else
				{
					lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("BankNumber"))));
					rsData.Edit();
					rsData.Set_Fields("CheckRegister", false);
					rsData.Set_Fields("CheckRegisterUserID", " ");
					rsData.Set_Fields("CheckRegisterDateTime", null);
					rsData.Set_Fields("Warrant", false);
					rsData.Set_Fields("WarrantUserID", " ");
					rsData.Set_Fields("WarrantDateTime", null);
					rsData.Update();
				}
				MessageBox.Show("Load Checks.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				lngNonNegStart = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStartingNonNegotiable.Text)));
				if (!modCoreysSweeterCode.PrintPayrollChecks(true, FCConvert.ToInt32(Conversion.Val(txtStartingCheck.Text)), lngReprintWarrant, lngJournal, boolPrintSig, FCConvert.ToInt32(Conversion.Val(txtStartReprint.Text)), FCConvert.ToInt32(Conversion.Val(txtEndReprint.Text)), lngID, lngBank, lngNonNegStart))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
			}
			else
			{
				// should never be able to get here
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("You must select a valid print option.");
				return;
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			Close();
		}

		private void optReprint_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						lblEndReprint.Enabled = false;
						lblStartReprint.Enabled = false;
						txtStartReprint.Enabled = false;
						txtEndReprint.Enabled = false;
						// fraPayDate.Visible = False
						txtStartReprint.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						txtEndReprint.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						break;
					}
				case 1:
					{
						lblEndReprint.Enabled = true;
						lblStartReprint.Enabled = true;
						txtStartReprint.Enabled = true;
						txtEndReprint.Enabled = true;
						// fraPayDate.Visible = True
						txtStartReprint.BackColor = Color.White;
						txtEndReprint.BackColor = Color.White;
						break;
					}
			}
			//end switch
		}

		private void optReprint_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReprint.SelectedIndex;
			//FC:FINAL:DSE:#i2139 Set index based on the text (items from combo are dynamicaly removed)
			switch(cmbReprint.Text)
			{
				case "Initial Run":
					index = 0;
					break;
				case "Reprint":
					index = 1;
					break;
				default:
					index = -1;
					break;
			}
			optReprint_CheckedChanged(index, sender, e);
		}

		private int GetStartingCheckNumber()
		{
			int GetStartingCheckNumber = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			int lngBank = 0;
			int lngStartingCheckNumber;
			lngStartingCheckNumber = 0;
			rsData.OpenRecordset("select banknumber from tblpayrollsteps where paydate = '" + this.lblPayDate.Text + "' and payrunid = " + this.lblRunNumber.Text, "twpy0000.vb1");
			if (rsData.EndOfFile())
			{
				lngBank = 0;
			}
			else
			{
				lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("banknumber"))));
			}
			if (lngBank > 0)
			{
				string strTemp = "";
				string strCheckType = "";
				strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
				if (fecherFoundation.Strings.LCase(strTemp) == "no")
				{
					strCheckType = "";
				}
				else if (fecherFoundation.Strings.LCase(strTemp) == "bank")
				{
					strCheckType = "";
				}
				else if (fecherFoundation.Strings.LCase(strTemp) == "check type")
				{
					strCheckType = "Payroll";
				}
				lngStartingCheckNumber = bankService.GetLastCheckNumberForBankCheckType(lngBank, strCheckType);
				if (lngStartingCheckNumber > 0)
				{
					lngStartingCheckNumber += 1;
				}
			}
			GetStartingCheckNumber = lngStartingCheckNumber;
			return GetStartingCheckNumber;
		}

		private int GetStartingNonNegotiableCheckNumber()
		{
			int GetStartingNonNegotiableCheckNumber = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			int lngBank = 0;
			int lngStartingCheckNumber;
			lngStartingCheckNumber = 0;
			rsData.OpenRecordset("select banknumber from tblpayrollsteps where paydate = '" + this.lblPayDate.Text + "' and payrunid = " + this.lblRunNumber.Text, "twpy0000.vb1");
			if (rsData.EndOfFile())
			{
				lngBank = 0;
			}
			else
			{
				lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("banknumber"))));
			}
			if (lngBank > 0)
			{
				string strCheckType = "";
				strCheckType = "Payroll NonNegotiable";
				lngStartingCheckNumber = bankService.GetLastCheckNumberForBankCheckType(lngBank, strCheckType);
				if (lngStartingCheckNumber > 0)
				{
					lngStartingCheckNumber += 1;
				}
			}
			GetStartingNonNegotiableCheckNumber = lngStartingCheckNumber;
			return GetStartingNonNegotiableCheckNumber;
		}       
    }
}
