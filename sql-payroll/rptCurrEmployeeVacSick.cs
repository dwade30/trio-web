//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCurrEmployeeVacSick.
	/// </summary>
	public partial class rptCurrEmployeeVacSick : BaseSectionReport
	{
		public rptCurrEmployeeVacSick()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Current Employee Vacation / Sick";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCurrEmployeeVacSick InstancePtr
		{
			get
			{
				return (rptCurrEmployeeVacSick)Sys.GetInstance(typeof(rptCurrEmployeeVacSick));
			}
		}

		protected rptCurrEmployeeVacSick _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCurrEmployeeVacSick	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		
		private string strTemp = "";
		private int intEmployee;
		private double dblAccruedCurrent;
		private double dblAccruedFiscal;
		private double dblAccruedCalendar;
		private double dblUsedCurrent;
		private double dblUsedFiscal;
		private double dblUsedCalendar;
		private double dblBalance;
		bool boolGroups;
		string strDeptDiv = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			if (!eArgs.EOF && boolGroups)
			{
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
				strDeptDiv = FCConvert.ToString(rsData.Get_Fields("deptdiv"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: intRow As object	OnWrite
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			// intSetTotals = 0
			int intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// & "  " & Format(Time, "hh:mm tt")
			txtSecondTitle.Text = "Pay Date " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			// Call rsdata.OpenRecordset(strCustomSQL, DEFAULTDATABASE)
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			int intRow = 0;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// Call SetPrintProperties(Me)
		}

		public bool Init(int intVacSickCode, bool boolShowMsgBox = true, bool boolSeparatePages = false, List<string> batchReports = null)
		{
			bool Init = false;
			string strWhere;
			string strSQL;
			string strOrderBy = "";
			rsData.OpenRecordset("select reportsequence from tbldefaultinformation", "twpy0000.vb1");
			switch (rsData.Get_Fields_Int32("reportsequence"))
			{
				case 0:
					{
						// name
						strOrderBy = " order by lastname,firstname,tblemployeemaster.employeenumber ";
						break;
					}
				case 1:
					{
						// employee
						strOrderBy = " order by tblemployeemaster.employeenumber ";
						break;
					}
				case 2:
					{
						// sequence
						strOrderBy = " order by seqnumber,lastname,firstname,tblemployeemaster.employeenumber ";
						break;
					}
				case 3:
					{
						// dept div
						strOrderBy = " order by deptdiv,lastname,firstname,tblemployeemaster.employeenumber ";
						boolGroups = true;
						this.GroupHeader1.Visible = true;
						if (boolSeparatePages)
						{
							this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
						}
						else
						{
							this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
						}
						break;
					}
			}
			//end switch
			rsData.OpenRecordset("select * from tblcodetypes where ID = " + FCConvert.ToString(intVacSickCode), "twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				txtTitle.Text = "Employee " + rsData.Get_Fields_String("description") + " Report";
			}
			else
			{
				txtTitle.Text = "Employee Unknown Code Type Report";
			}
			// Matthew 1/23/04
			// strWhere = " where tblvacationsick.typeid = " & intVacSickCode & " and (accruedcurrent <> 0 or usedcurrent <> 0 or usedbalance <> 0)"
			// Commented out the current <> 0 and etc for Vassalboro as they wanted to see employee 26
			// who did not have current but had fiscal data
			strWhere = " where selected = 1 and tblvacationsick.typeid = " + FCConvert.ToString(intVacSickCode) + " and (accruedcurrent <> 0 or accruedFiscal <> 0 or accruedCalendar <> 0 or usedcurrent <> 0 or usedFiscal <> 0 or usedCalendar <> 0 or usedbalance <> 0)";
			// now open the recordset
			// corey 06/08/2004 changed to left join so manual codes will work
			strSQL = "select * from tblemployeemaster inner join (tblvacationsick left join tblcodes on (tblvacationsick.codeid = tblcodes.ID)) on (tblvacationsick.employeenumber = tblemployeemaster.employeenumber) " + strWhere + " " + strOrderBy;
			rsData.OpenRecordset(strSQL, "twpy0000.vb1");
			if (rsData.EndOfFile())
			{
				Init = false;
				if (boolShowMsgBox)
				{
					MessageBox.Show("No data found.");
				}
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 1)
					{
						modDavesSweetCode.Statics.blnReportCompleted = true;
					}
				}
				else
				{
					if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 1)
					{
						// sick
						modGlobalRoutines.UpdatePayrollStepTable("SickReport");
					}
					else if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 2)
					{
						// vacation
						modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
					}
					else
					{
						// other
						modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
					}
				}
				this.Close();
				return Init;
			}
			Init = true;
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// Me.PrintReport (False)
			}
			else
			{
				// frmReportViewer.Init Me, , 1
				modCoreysSweeterCode.CheckDefaultPrint(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "EmployeeVacSick");
			}
			return Init;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 1)
				{
					modDavesSweetCode.Statics.blnReportCompleted = true;
					// thhi
				}
			}
			else
			{
				if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 1)
				{
					// sick
					modGlobalRoutines.UpdatePayrollStepTable("SickReport");
				}
				else if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == 2)
				{
					// vacation
					modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
				}
				else if (Conversion.Val(modGlobalVariables.Statics.gstrVacType) == -1000)
				{
					modGlobalRoutines.UpdatePayrollStepTable("SickReport");
					modGlobalRoutines.UpdatePayrollStepTable("VacationReport");
					modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
				}
				else
				{
					// other
					modGlobalRoutines.UpdatePayrollStepTable("OtherCodeTypes");
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields_String("Desig");
			intEmployee += 1;
			// txtCode.Text = Trim(rsData.Fields("Code"))
			if (FCConvert.ToInt32(rsData.Get_Fields("codeid")) == 0)
			{
				// corey 06/08/2004
				txtUM.Text = rsData.Get_Fields_String("UM");
				txtPER.Text = rsData.Get_Fields_String("tblvacationsick.type");
				txtRate.Text = Strings.Format(rsData.Get_Fields("rates"), "0.0000");
			}
			else
			{
				txtRate.Text = Strings.Format(rsData.Get_Fields("Rate"), "0.0000");
				txtUM.Text = rsData.Get_Fields_String("DHW");
				txtPER.Text = rsData.Get_Fields_String("tblcodes.Type");
			}
			txtAccruedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("accruedcurrent")), "0.0000");
			dblAccruedCurrent += Conversion.Val(rsData.Get_Fields("Accruedcurrent"));
			txtAccruedFiscal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("AccruedFiscal")), "0.0000");
			dblAccruedFiscal += Conversion.Val(rsData.Get_Fields_Double("AccruedFiscal"));
			txtAccruedCalendar.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("AccruedCalendar")), "0.0000");
			dblAccruedCalendar += Conversion.Val(rsData.Get_Fields_Double("AccruedCalendar"));
			txtUsedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedCurrent")), "0.00");
			dblUsedCurrent += Conversion.Val(rsData.Get_Fields_Double("UsedCurrent"));
			txtUsedFiscal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedFiscal")), "0.00");
			dblUsedFiscal += Conversion.Val(rsData.Get_Fields_Double("UsedFiscal"));
			txtUsedCalendar.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedCalendar")), "0.00");
			dblUsedCalendar += Conversion.Val(rsData.Get_Fields_Double("UsedCalendar"));
			txtBalance.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedBalance")), "0.0000");
			dblBalance += Conversion.Val(rsData.Get_Fields_Double("UsedBalance"));
			rsData.MoveNext();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtDeptDiv.Text = FCConvert.ToString(this.Fields["grpHeader"].Value);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// TH
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtEmployeeTotal As object	OnWrite
			// vbPorter upgrade warning: txtAccruedCurrentTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtAccruedFiscalTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtAccruedCalendarTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtUsedCurrentTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtUsedFiscalTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtUsedCalendarTotal As object	OnWrite(string)
			// vbPorter upgrade warning: txtBalanceTotal As object	OnWrite(string)
			txtEmployeeTotal.Text = intEmployee.ToString();
			txtAccruedCurrentTotal.Text = Strings.Format(dblAccruedCurrent, "0.0000");
			txtAccruedFiscalTotal.Text = Strings.Format(dblAccruedFiscal, "0.0000");
			txtAccruedCalendarTotal.Text = Strings.Format(dblAccruedCalendar, "0.0000");
			txtUsedCurrentTotal.Text = Strings.Format(dblUsedCurrent, "0.00");
			txtUsedFiscalTotal.Text = Strings.Format(dblUsedFiscal, "0.00");
			txtUsedCalendarTotal.Text = Strings.Format(dblUsedCalendar, "0.00");
			txtBalanceTotal.Text = Strings.Format(dblBalance, "0.0000");
		}

		

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
