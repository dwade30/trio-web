﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingTax.
	/// </summary>
	partial class rptCheckListingTax
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckListingTax));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtFICAWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVoid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalFederal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalFICA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblSort = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupFederal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupFICA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtGroupGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFICAWH,
            this.txtMedWH,
            this.txtStateWH,
            this.txtAmount,
            this.txtRegular,
            this.txtOther,
            this.txtFederalWH,
            this.txtEmployee,
            this.txtDateField,
            this.txtType,
            this.txtVoid,
            this.txtCheckNumber,
            this.txtGross});
			this.Detail.Height = 0.2708333F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtFICAWH
			// 
			this.txtFICAWH.Height = 0.2083333F;
			this.txtFICAWH.Left = 7.09375F;
			this.txtFICAWH.Name = "txtFICAWH";
			this.txtFICAWH.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFICAWH.Text = null;
			this.txtFICAWH.Top = 0.04166667F;
			this.txtFICAWH.Width = 0.6875F;
			// 
			// txtMedWH
			// 
			this.txtMedWH.Height = 0.2083333F;
			this.txtMedWH.Left = 7.78125F;
			this.txtMedWH.Name = "txtMedWH";
			this.txtMedWH.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMedWH.Text = null;
			this.txtMedWH.Top = 0.04166667F;
			this.txtMedWH.Width = 0.6875F;
			// 
			// txtStateWH
			// 
			this.txtStateWH.Height = 0.2083333F;
			this.txtStateWH.Left = 8.46875F;
			this.txtStateWH.Name = "txtStateWH";
			this.txtStateWH.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStateWH.Text = null;
			this.txtStateWH.Top = 0.04166667F;
			this.txtStateWH.Width = 0.6875F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2083333F;
			this.txtAmount.Left = 4.34375F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAmount.Text = "Amount";
			this.txtAmount.Top = 0.04166667F;
			this.txtAmount.Width = 0.6875F;
			// 
			// txtRegular
			// 
			this.txtRegular.Height = 0.2083333F;
			this.txtRegular.Left = 5.03125F;
			this.txtRegular.Name = "txtRegular";
			this.txtRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRegular.Text = "Regular";
			this.txtRegular.Top = 0.04166667F;
			this.txtRegular.Width = 0.6875F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.2083333F;
			this.txtOther.Left = 5.71875F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOther.Text = "Other";
			this.txtOther.Top = 0.04166667F;
			this.txtOther.Width = 0.6875F;
			// 
			// txtFederalWH
			// 
			this.txtFederalWH.Height = 0.2083333F;
			this.txtFederalWH.Left = 6.40625F;
			this.txtFederalWH.Name = "txtFederalWH";
			this.txtFederalWH.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFederalWH.Text = null;
			this.txtFederalWH.Top = 0.04166667F;
			this.txtFederalWH.Width = 0.6875F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2083333F;
			this.txtEmployee.Left = 0.0625F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'";
			this.txtEmployee.Text = "Employee";
			this.txtEmployee.Top = 0.04166667F;
			this.txtEmployee.Width = 1.96875F;
			// 
			// txtDateField
			// 
			this.txtDateField.Height = 0.2083333F;
			this.txtDateField.Left = 2.09375F;
			this.txtDateField.Name = "txtDateField";
			this.txtDateField.Style = "font-family: \'Tahoma\'";
			this.txtDateField.Text = "Date";
			this.txtDateField.Top = 0.04166667F;
			this.txtDateField.Width = 0.8125F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.2083333F;
			this.txtType.Left = 2.90625F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'";
			this.txtType.Text = "Type";
			this.txtType.Top = 0.04166667F;
			this.txtType.Width = 0.40625F;
			// 
			// txtVoid
			// 
			this.txtVoid.Height = 0.2083333F;
			this.txtVoid.Left = 3.3125F;
			this.txtVoid.Name = "txtVoid";
			this.txtVoid.Style = "font-family: \'Tahoma\'";
			this.txtVoid.Text = "Void";
			this.txtVoid.Top = 0.04166667F;
			this.txtVoid.Width = 0.375F;
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Height = 0.2083333F;
			this.txtCheckNumber.Left = 3.8125F;
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCheckNumber.Text = "Check #";
			this.txtCheckNumber.Top = 0.04166667F;
			this.txtCheckNumber.Width = 0.5625F;
			// 
			// txtGross
			// 
			this.txtGross.Height = 0.2083333F;
			this.txtGross.Left = 4.989583F;
			this.txtGross.Name = "txtGross";
			this.txtGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGross.Text = "Gross";
			this.txtGross.Top = 0.04166667F;
			this.txtGross.Visible = false;
			this.txtGross.Width = 1.416667F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalAmount,
            this.txtTotalRegular,
            this.txtTotalOther,
            this.Field17,
            this.Line13,
            this.txtTotalFederal,
            this.txtTotalFICA,
            this.txtTotalMedicare,
            this.txtTotalState,
            this.txtTotGross});
			this.ReportFooter.Height = 0.5104167F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.Left = 4.366667F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalAmount.Text = "Field15";
			this.txtTotalAmount.Top = 0.3F;
			this.txtTotalAmount.Width = 0.6666667F;
			// 
			// txtTotalRegular
			// 
			this.txtTotalRegular.Height = 0.1666667F;
			this.txtTotalRegular.Left = 5.033333F;
			this.txtTotalRegular.Name = "txtTotalRegular";
			this.txtTotalRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalRegular.Text = "Field15";
			this.txtTotalRegular.Top = 0.3F;
			this.txtTotalRegular.Width = 0.7333333F;
			// 
			// txtTotalOther
			// 
			this.txtTotalOther.Height = 0.1666667F;
			this.txtTotalOther.Left = 5.766667F;
			this.txtTotalOther.Name = "txtTotalOther";
			this.txtTotalOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalOther.Text = "Field15";
			this.txtTotalOther.Top = 0.3F;
			this.txtTotalOther.Width = 0.6333333F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.2F;
			this.Field17.Left = 3.1F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field17.Text = "Grand Totals";
			this.Field17.Top = 0.3F;
			this.Field17.Width = 1F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 3.1F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.2666667F;
			this.Line13.Width = 6.7F;
			this.Line13.X1 = 3.1F;
			this.Line13.X2 = 9.8F;
			this.Line13.Y1 = 0.2666667F;
			this.Line13.Y2 = 0.2666667F;
			// 
			// txtTotalFederal
			// 
			this.txtTotalFederal.Height = 0.1666667F;
			this.txtTotalFederal.Left = 6.466667F;
			this.txtTotalFederal.Name = "txtTotalFederal";
			this.txtTotalFederal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalFederal.Text = "Field15";
			this.txtTotalFederal.Top = 0.3F;
			this.txtTotalFederal.Width = 0.6333333F;
			// 
			// txtTotalFICA
			// 
			this.txtTotalFICA.Height = 0.1666667F;
			this.txtTotalFICA.Left = 7.133333F;
			this.txtTotalFICA.Name = "txtTotalFICA";
			this.txtTotalFICA.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalFICA.Text = "Field15";
			this.txtTotalFICA.Top = 0.3F;
			this.txtTotalFICA.Width = 0.6333333F;
			// 
			// txtTotalMedicare
			// 
			this.txtTotalMedicare.Height = 0.1666667F;
			this.txtTotalMedicare.Left = 7.833333F;
			this.txtTotalMedicare.Name = "txtTotalMedicare";
			this.txtTotalMedicare.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalMedicare.Text = "Field15";
			this.txtTotalMedicare.Top = 0.3F;
			this.txtTotalMedicare.Width = 0.6333333F;
			// 
			// txtTotalState
			// 
			this.txtTotalState.Height = 0.1666667F;
			this.txtTotalState.Left = 8.5F;
			this.txtTotalState.Name = "txtTotalState";
			this.txtTotalState.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalState.Text = "Field15";
			this.txtTotalState.Top = 0.3F;
			this.txtTotalState.Width = 0.6333333F;
			// 
			// txtTotGross
			// 
			this.txtTotGross.Height = 0.1666667F;
			this.txtTotGross.Left = 5.041667F;
			this.txtTotGross.Name = "txtTotGross";
			this.txtTotGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotGross.Text = "Field15";
			this.txtTotGross.Top = 0.3020833F;
			this.txtTotGross.Visible = false;
			this.txtTotGross.Width = 1.364583F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSort,
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.Label3,
            this.txtTime,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6,
            this.lblRegular,
            this.lblOther,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Field12,
            this.Field14,
            this.Line12,
            this.Field15,
            this.lblGross});
			this.PageHeader.Height = 1.458333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblSort
			// 
			this.lblSort.Height = 0.2083333F;
			this.lblSort.HyperLink = null;
			this.lblSort.Left = 0.0625F;
			this.lblSort.Name = "lblSort";
			this.lblSort.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 8.5pt; font" +
    "-weight: bold; text-align: center";
			this.lblSort.Text = null;
			this.lblSort.Top = 0.25F;
			this.lblSort.Width = 9.78125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 10pt; font-" +
    "weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL CHECK LISTING";
			this.Label1.Top = 0.04166667F;
			this.Label1.Width = 9.78125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.08333334F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.40625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.40625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 8.5pt; font" +
    "-weight: bold; text-align: center";
			this.Label3.Text = "Complete File";
			this.Label3.Top = 0.4583333F;
			this.Label3.Width = 9.78125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'";
			this.Field1.Text = "Employee";
			this.Field1.Top = 1.208333F;
			this.Field1.Width = 0.9375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2083333F;
			this.Field2.Left = 2.09375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Field2.Text = "Date";
			this.Field2.Top = 1.208333F;
			this.Field2.Width = 0.8125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 2.90625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'";
			this.Field3.Text = "Type";
			this.Field3.Top = 1.208333F;
			this.Field3.Width = 0.40625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 3.3125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'";
			this.Field4.Text = "Void";
			this.Field4.Top = 1.208333F;
			this.Field4.Width = 0.375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 3.8125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: left";
			this.Field5.Text = "Check #";
			this.Field5.Top = 1.208333F;
			this.Field5.Width = 0.65625F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 4.34375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = "Amount";
			this.Field6.Top = 1.208333F;
			this.Field6.Width = 0.65625F;
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.2083333F;
			this.lblRegular.Left = 5.03125F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 1.208333F;
			this.lblRegular.Width = 0.6875F;
			// 
			// lblOther
			// 
			this.lblOther.Height = 0.2083333F;
			this.lblOther.Left = 5.71875F;
			this.lblOther.Name = "lblOther";
			this.lblOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblOther.Text = "Other";
			this.lblOther.Top = 1.208333F;
			this.lblOther.Width = 0.6875F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2083333F;
			this.Field9.Left = 6.40625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field9.Text = "Federal";
			this.Field9.Top = 1.208333F;
			this.Field9.Width = 0.6875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.2083333F;
			this.Field10.Left = 7.09375F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field10.Text = "FICA";
			this.Field10.Top = 1.208333F;
			this.Field10.Width = 0.6875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.2083333F;
			this.Field11.Left = 7.78125F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field11.Text = "Medicare";
			this.Field11.Top = 1.208333F;
			this.Field11.Width = 0.6875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.2083333F;
			this.Field12.Left = 8.46875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field12.Text = "State";
			this.Field12.Top = 1.208333F;
			this.Field12.Width = 0.6875F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.2083333F;
			this.Field14.Left = 5.0625F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Field14.Text = "- - - W a g e s - - -";
			this.Field14.Top = 1F;
			this.Field14.Width = 1.34375F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 1.416667F;
			this.Line12.Width = 9.875F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 9.875F;
			this.Line12.Y1 = 1.416667F;
			this.Line12.Y2 = 1.416667F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.2083333F;
			this.Field15.Left = 6.46875F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field15.Text = "-- - ---- - - - - T a x e s -  - ---- - - - - - - -";
			this.Field15.Top = 1F;
			this.Field15.Width = 2.698251F;
			// 
			// lblGross
			// 
			this.lblGross.Height = 0.2083333F;
			this.lblGross.Left = 5.71875F;
			this.lblGross.Name = "lblGross";
			this.lblGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblGross.Text = "Gross";
			this.lblGross.Top = 1.208333F;
			this.lblGross.Visible = false;
			this.lblGross.Width = 0.6875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field16,
            this.txtGroupFederal,
            this.txtGroupFICA,
            this.txtGroupMedicare,
            this.txtGroupState,
            this.txtGroupAmount,
            this.txtGroupRegular,
            this.txtGroupOther,
            this.Line14,
            this.txtGroupGross});
			this.GroupFooter1.Height = 0.5520833F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Field16
			// 
			this.Field16.Height = 0.2F;
			this.Field16.Left = 3.166667F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field16.Text = "Group Totals";
			this.Field16.Top = 0.06666667F;
			this.Field16.Width = 1F;
			// 
			// txtGroupFederal
			// 
			this.txtGroupFederal.Height = 0.1666667F;
			this.txtGroupFederal.Left = 6.466667F;
			this.txtGroupFederal.Name = "txtGroupFederal";
			this.txtGroupFederal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupFederal.Text = "Field15";
			this.txtGroupFederal.Top = 0.06666667F;
			this.txtGroupFederal.Width = 0.6333333F;
			// 
			// txtGroupFICA
			// 
			this.txtGroupFICA.Height = 0.1666667F;
			this.txtGroupFICA.Left = 7.133333F;
			this.txtGroupFICA.Name = "txtGroupFICA";
			this.txtGroupFICA.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupFICA.Text = "Field15";
			this.txtGroupFICA.Top = 0.06666667F;
			this.txtGroupFICA.Width = 0.6333333F;
			// 
			// txtGroupMedicare
			// 
			this.txtGroupMedicare.Height = 0.1666667F;
			this.txtGroupMedicare.Left = 7.833333F;
			this.txtGroupMedicare.Name = "txtGroupMedicare";
			this.txtGroupMedicare.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupMedicare.Text = "Field15";
			this.txtGroupMedicare.Top = 0.06666667F;
			this.txtGroupMedicare.Width = 0.6333333F;
			// 
			// txtGroupState
			// 
			this.txtGroupState.Height = 0.1666667F;
			this.txtGroupState.Left = 8.533334F;
			this.txtGroupState.Name = "txtGroupState";
			this.txtGroupState.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupState.Text = "Field15";
			this.txtGroupState.Top = 0.06666667F;
			this.txtGroupState.Width = 0.6333333F;
			// 
			// txtGroupAmount
			// 
			this.txtGroupAmount.Height = 0.1666667F;
			this.txtGroupAmount.Left = 4.366667F;
			this.txtGroupAmount.Name = "txtGroupAmount";
			this.txtGroupAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupAmount.Text = "Field15";
			this.txtGroupAmount.Top = 0.06666667F;
			this.txtGroupAmount.Width = 0.6666667F;
			// 
			// txtGroupRegular
			// 
			this.txtGroupRegular.Height = 0.1666667F;
			this.txtGroupRegular.Left = 5F;
			this.txtGroupRegular.Name = "txtGroupRegular";
			this.txtGroupRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupRegular.Text = "Field15";
			this.txtGroupRegular.Top = 0.06666667F;
			this.txtGroupRegular.Width = 0.7333333F;
			// 
			// txtGroupOther
			// 
			this.txtGroupOther.Height = 0.1666667F;
			this.txtGroupOther.Left = 5.766667F;
			this.txtGroupOther.Name = "txtGroupOther";
			this.txtGroupOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupOther.Text = "Field15";
			this.txtGroupOther.Top = 0.06666667F;
			this.txtGroupOther.Width = 0.6333333F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 3.166667F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0.06666667F;
			this.Line14.Width = 6.7F;
			this.Line14.X1 = 3.166667F;
			this.Line14.X2 = 9.866667F;
			this.Line14.Y1 = 0.06666667F;
			this.Line14.Y2 = 0.06666667F;
			// 
			// txtGroupGross
			// 
			this.txtGroupGross.Height = 0.1666667F;
			this.txtGroupGross.Left = 5.041667F;
			this.txtGroupGross.Name = "txtGroupGross";
			this.txtGroupGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupGross.Text = "Field15";
			this.txtGroupGross.Top = 0.0625F;
			this.txtGroupGross.Visible = false;
			this.txtGroupGross.Width = 1.364583F;
			// 
			// rptCheckListingTax
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 8.5F;
			this.PageSettings.PaperWidth = 11F;
			this.PrintWidth = 9.916667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVoid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGross;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFederal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFICA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGross;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSort;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblGross;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupFederal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupFICA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupOther;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupGross;
	}
}
