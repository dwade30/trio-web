//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmployeeSchedule : BaseForm
	{
		public frmEmployeeSchedule()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.GridDays = new System.Collections.Generic.List<FCGrid>();
			this.GridDays.AddControlArrayElement(GridDays_0, 0);
			this.GridDays.AddControlArrayElement(GridDays_1, 1);
			this.GridDays.AddControlArrayElement(GridDays_2, 2);
			this.GridDays.AddControlArrayElement(GridDays_3, 3);
			this.GridDays.AddControlArrayElement(GridDays_4, 4);
			this.GridDays.AddControlArrayElement(GridDays_5, 5);
			this.GridDays.AddControlArrayElement(GridDays_6, 6);
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeeSchedule InstancePtr
		{
			get
			{
				return (frmEmployeeSchedule)Sys.GetInstance(typeof(frmEmployeeSchedule));
			}
		}

		protected frmEmployeeSchedule _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDTOTALSCOLCAT = 0;
		const int CNSTGRIDTOTALSCOLDESC = 1;
		const int CNSTGRIDTOTALSCOLTYPE = 2;
		const int CNSTGRIDTOTALSCOLSCHEDULED = 3;
		const int CNSTGRIDTOTALSCOLACTUAL = 4;
		const int CNSTGRIDTOTALSCOLPAID = 5;
		const int CNSTGRIDTOTALSCOLDistAutoID = 6;
		const int CNSTGRIDTOTALSCOLACCOUNT = 7;
		const int CNSTGRIDDAYSCOLID = 0;
		const int CNSTGRIDDAYSCOLSHIFT = 1;
		const int CNSTGRIDDAYSCOLSCHEDULEDSTART = 2;
		const int CNSTGRIDDAYSCOLSCHEDULEDEND = 3;
		const int CNSTGRIDDAYSCOLACTUALSTART = 4;
		const int CNSTGRIDDAYSCOLACTUALEND = 5;
		const int CNSTGRIDDAYSCOLINDEX = 6;
		const int CNSTGRIDDAYSCOLLUNCH = 7;
		const int CNSTGRIDDAYSCOLACCOUNT = 8;
		private DateTime dtFirstDayOfWeek;
		private string strEmployeeNumber = string.Empty;
		private clsScheduleWeek wkWorkWeek;
		private int lngRegularShiftID;
		private bool boolCurrentWeek;
		private bool boolFromDistribution;
		private bool boolReturn;
		clsShiftTypeList ShiftList = new clsShiftTypeList();

		public bool Init(ref DateTime dtDate, ref string strEmployee, bool boolFromDistributionScreen = false)
		{
			bool Init = false;
			// dtdate is week of.  Need to find the first day of that week
			// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToInt32(
			int intDay;
			Init = false;
			wkWorkWeek = new clsScheduleWeek();
			boolFromDistribution = boolFromDistributionScreen;
			boolReturn = false;
			strEmployeeNumber = strEmployee;
			intDay = fecherFoundation.DateAndTime.Weekday(dtDate, DayOfWeek.Sunday);
			if (intDay != FCConvert.ToInt32(DayOfWeek.Sunday))
			{
				dtFirstDayOfWeek = fecherFoundation.DateAndTime.DateAdd("d", FCConvert.ToInt32(DayOfWeek.Sunday) - intDay, dtDate);
			}
			else
			{
				dtFirstDayOfWeek = dtDate;
			}
			DateTime dtTemp;
			dtTemp = modSchedule.GetWeekBeginningDate(dtDate);
			if (modSchedule.GetWeekBeginningDate(modGlobalVariables.Statics.gdatCurrentWeekEndingDate).ToOADate() == dtTemp.ToOADate())
			{
				boolCurrentWeek = true;
			}
			else
			{
				boolCurrentWeek = false;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = boolReturn;
			return Init;
		}

		private void frmEmployeeSchedule_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEmployeeSchedule_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmployeeSchedule properties;
			//frmEmployeeSchedule.FillStyle	= 0;
			//frmEmployeeSchedule.ScaleWidth	= 9300;
			//frmEmployeeSchedule.ScaleHeight	= 7545;
			//frmEmployeeSchedule.LinkTopic	= "Form2";
			//frmEmployeeSchedule.LockControls	= -1  'True;
			//frmEmployeeSchedule.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridDays();
			SetupGridTotals();
			LoadWeek(ref dtFirstDayOfWeek, ref strEmployeeNumber);
		}

		private void LoadWeek(ref DateTime dtDate, ref string strEmployee)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				// if nothing is found then calculate the shifts based on the default schedule
				wkWorkWeek.EmployeeNumber = strEmployee;
				wkWorkWeek.LoadEmployeeWeek(dtDate);
				wkWorkWeek.LoadSettings();
				wkWorkWeek.InitPayTots();
				wkWorkWeek.ReCalcWeek();
				LoadGrids();
				FillGridTotals();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGrids()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				clsScheduleDay tDay;
				clsWorkShift tShift;
				int lngRow;
				for (x = 0; x <= 6; x++)
				{
					GridDays[x].Rows = 2;
					tDay = wkWorkWeek.GetDay(x + 1);
					if (!(tDay == null))
					{
						tDay.MoveFirst();
						while (tDay.GetCurrentIndex() >= 0)
						{
							tShift = tDay.GetCurrentShift();
							if (!(tShift == null))
							{
								GridDays[x].Rows += 1;
								lngRow = GridDays[x].Rows - 1;
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLID, FCConvert.ToString(tShift.ID));
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALEND, tShift.ActualEnd);
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALSTART, tShift.ActualStart);
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDEND, tShift.ScheduledEnd);
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDSTART, tShift.ScheduledStart);
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLSHIFT, FCConvert.ToString(tShift.ShiftType));
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLINDEX, FCConvert.ToString(tDay.GetCurrentIndex()));
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(tShift.LunchTime));
								GridDays[x].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, tShift.Account);
							}
							tDay.MoveNext();
						}
					}
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrids", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridDays()
		{
			// vbPorter upgrade warning: GridWidth As object	OnWriteFCConvert.ToInt32(
			int x;
			int lngColWidth;
			for (x = 0; x <= 6; x++)
			{
				int GridWidth = GridDays[x].WidthOriginal;
				GridDays[x].ColWidth(CNSTGRIDDAYSCOLSHIFT, FCConvert.ToInt32(0.26 * GridWidth));
				GridDays[x].ColWidth(CNSTGRIDDAYSCOLSCHEDULEDSTART, FCConvert.ToInt32(0.195 * GridWidth));
				GridDays[x].ColWidth(CNSTGRIDDAYSCOLSCHEDULEDEND, FCConvert.ToInt32(0.195 * GridWidth));
				GridDays[x].ColWidth(CNSTGRIDDAYSCOLACTUALSTART, FCConvert.ToInt32(0.16 * GridWidth));
			}
			// x
		}

		private void SetupGridDays()
		{
			DateTime dtDate;
			int x;
			int intDay;
			int intindex = 0;
			clsShiftType tShift;
			string strTemp;
			ShiftList.LoadTypes();
			strTemp = "";
			ShiftList.MoveFirst();
			// strTemp = "#0;None"
			if (ShiftList.GetCurrentIndex() < 0)
			{
				strTemp = "#0;None";
			}
			else
			{
				intindex = ShiftList.GetCurrentIndex();
				while (intindex >= 0)
				{
					tShift = ShiftList.GetCurrentShiftType();
					if (tShift.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Regular))
					{
						lngRegularShiftID = tShift.ShiftID;
					}
					if (!(tShift == null))
					{
						strTemp += "#" + FCConvert.ToString(tShift.ShiftID) + ";" + tShift.ShiftName + "|";
					}
					ShiftList.MoveNext();
					intindex = ShiftList.GetCurrentIndex();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					// get rid of last |
				}
				else
				{
					strTemp = "#0;None";
				}
			}
			dtDate = dtFirstDayOfWeek;
			for (intDay = 0; intDay <= 6; intDay++)
			{
				GridDays[intDay].Rows = 2;
				GridDays[intDay].Cols = 9;
				GridDays[intDay].ColHidden(CNSTGRIDDAYSCOLINDEX, true);
				GridDays[intDay].ColHidden(CNSTGRIDDAYSCOLLUNCH, true);
				GridDays[intDay].ColHidden(CNSTGRIDDAYSCOLACCOUNT, true);
				GridDays[intDay].Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				GridDays[intDay].MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
				GridDays[intDay].MergeRow(0, true);
				GridDays[intDay].MergeRow(1, true);
				Label1[intDay].Text = (DayOfWeek)(intDay + 1) + " " + Strings.Format(dtDate, "MM/dd/yyyy");
				GridDays[intDay].ColHidden(CNSTGRIDDAYSCOLID, true);
				GridDays[intDay].TextMatrix(1, CNSTGRIDDAYSCOLSHIFT, "Shift");
				GridDays[intDay].ColComboList(CNSTGRIDDAYSCOLSHIFT, strTemp);
				GridDays[intDay].TextMatrix(1, CNSTGRIDDAYSCOLSCHEDULEDSTART, "From");
				GridDays[intDay].TextMatrix(1, CNSTGRIDDAYSCOLSCHEDULEDEND, "To");
				GridDays[intDay].TextMatrix(0, CNSTGRIDDAYSCOLSCHEDULEDSTART, "Scheduled");
				GridDays[intDay].TextMatrix(0, CNSTGRIDDAYSCOLSCHEDULEDEND, "Scheduled");
				GridDays[intDay].ColComboList(CNSTGRIDDAYSCOLSCHEDULEDSTART, "...");
				GridDays[intDay].ColComboList(CNSTGRIDDAYSCOLSCHEDULEDEND, "...");
				GridDays[intDay].TextMatrix(1, CNSTGRIDDAYSCOLACTUALSTART, "From");
				GridDays[intDay].TextMatrix(1, CNSTGRIDDAYSCOLACTUALEND, "To");
				GridDays[intDay].TextMatrix(0, CNSTGRIDDAYSCOLACTUALSTART, "Actual");
				GridDays[intDay].TextMatrix(0, CNSTGRIDDAYSCOLACTUALEND, "Actual");
				GridDays[intDay].ColComboList(CNSTGRIDDAYSCOLACTUALSTART, "...");
				GridDays[intDay].ColComboList(CNSTGRIDDAYSCOLACTUALEND, "...");
				dtDate = fecherFoundation.DateAndTime.DateAdd("d", 1, dtDate);
			}
			// intDay
		}

		private void SetupGridTotals()
		{
			string strTemp = "";
			GridTotals.Cols = 8;
			GridTotals.Rows = 1;
			GridTotals.FixedRows = 1;
			GridTotals.FixedCols = 2;
			GridTotals.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			GridTotals.ColHidden(CNSTGRIDTOTALSCOLDistAutoID, true);
			GridTotals.ColHidden(CNSTGRIDTOTALSCOLCAT, true);
			GridTotals.ColHidden(CNSTGRIDTOTALSCOLACCOUNT, true);
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLCAT, "Pay Category");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLDESC, "Pay Category");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLACTUAL, "Actual");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLSCHEDULED, "Scheduled");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLTYPE, "Type");
			GridTotals.ColHidden(CNSTGRIDTOTALSCOLSCHEDULED, true);
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLPAID, "Paid");
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from TBLpaycategories order by ID", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (!(strTemp == string.Empty))
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridTotals.ColComboList(CNSTGRIDTOTALSCOLCAT, strTemp);
		}

		private void FillGridTotals()
		{
			clsPayTotalsByCat PayTots;
			clsPayCatTotal tCat;
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow = 0;
			clsPayCatTotal tBreak;
			int x;
			GridTotals.Rows = 1;
			PayTots = wkWorkWeek.GetPayTotals();
			rsLoad.OpenRecordset("select * from TBLpaycategories order by ID", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				tCat = PayTots.Get_CategoryTotals(rsLoad.Get_Fields("ID"));
				if (!(tCat == null))
				{
					if (tCat.ActualHours > 0 || tCat.AmountPaid > 0)
					{
						if (tCat.BreakDownCount < 1)
						{
							GridTotals.Rows += 1;
							lngRow = GridTotals.Rows - 1;
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLCAT, FCConvert.ToString(tCat.PayCat));
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLDESC, FCConvert.ToString(GridTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDTOTALSCOLCAT)));
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID, FCConvert.ToString(tCat.AmountPaid));
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLACTUAL, FCConvert.ToString(tCat.ActualHours));
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLTYPE, FCConvert.ToString(rsLoad.Get_Fields("type")));
							GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLACCOUNT, "");
						}
						else
						{
							for (x = 1; x <= tCat.BreakDownCount; x++)
							{
								tBreak = tCat.Get_BreakdownByIndex(x);
								if (!(tBreak == null))
								{
									GridTotals.Rows += 1;
									lngRow = GridTotals.Rows - 1;
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLCAT, FCConvert.ToString(tCat.PayCat));
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLDESC, GridTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDTOTALSCOLCAT) + " " + tBreak.Account);
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLACCOUNT, tBreak.Account);
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID, FCConvert.ToString(tBreak.AmountPaid));
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLACTUAL, FCConvert.ToString(tBreak.ActualHours));
									GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLTYPE, FCConvert.ToString(rsLoad.Get_Fields("type")));
								}
							}
							// x
						}
					}
				}
				rsLoad.MoveNext();
			}
		}

		private void frmEmployeeSchedule_Resize(object sender, System.EventArgs e)
		{
			ResizeGridDays();
			ResizeGridTotals();
		}

		private void ResizeGridTotals()
		{
			int GridWidth = 0;
			GridWidth = GridTotals.WidthOriginal;
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLDESC, FCConvert.ToInt32(0.4 * GridWidth));
			// .ColWidth(CNSTGRIDTOTALSCOLSCHEDULED) = 0.2 * GridWidth
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLTYPE, FCConvert.ToInt32(0.2 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLACTUAL, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void InsertRow(int intindex)
		{
			int lngRow;
			GridDays[intindex].Rows += 1;
			lngRow = GridDays[intindex].Rows - 1;
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLID, FCConvert.ToString(0));
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDSTART, "Unscheduled");
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDEND, "Unscheduled");
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALSTART, "1:00 AM");
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALEND, "1:00 AM");
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(0));
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, "");
			if (lngRegularShiftID > 0)
			{
				clsShiftType tShift;
				GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLSHIFT, FCConvert.ToString(lngRegularShiftID));
				tShift = ShiftList.GetShiftTypeByType(lngRegularShiftID);
				if (!(tShift == null))
				{
					GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(tShift.LunchTime));
					GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, tShift.Account);
				}
			}
			// griddays(intindex).TextMatrix(lngrow,CNSTGRIDDAYSCOLSHIFT) = 0
			ResizeGridDays();
			clsScheduleDay tDay;
			tDay = wkWorkWeek.GetDay(intindex + 1);
			GridDays[intindex].TextMatrix(lngRow, CNSTGRIDDAYSCOLINDEX, FCConvert.ToString(tDay.AddShift(0, "Unscheduled", "Unscheduled", "Unworked", "Unworked", 0, 0, "")));
		}

		private void DeleteRow(int intindex)
		{
			if (GridDays[intindex].Row > 1)
			{
				// vbPorter upgrade warning: intI As int	OnWrite(string)
				int intI = 0;
				intI = FCConvert.ToInt32(GridDays[intindex].TextMatrix(GridDays[intindex].Row, CNSTGRIDDAYSCOLINDEX));
				clsScheduleDay tDay;
				tDay = wkWorkWeek.GetDay(intindex + 1);
				if (!(tDay == null))
				{
					tDay.DeleteShift(intI);
				}
				GridDays[intindex].RemoveItem(GridDays[intindex].Row);
				wkWorkWeek.ReCalcWeek();
				LoadGrids();
				FillGridTotals();
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (boolFromDistribution)
			{
				frmPayrollDistribution.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
		}

		private void GridDays_CellButtonClick(int Index, object sender, EventArgs e)
		{
			int lngCol;
			int lngRow;
			lngRow = GridDays[Index].Row;
			lngCol = GridDays[Index].Col;
			string strSend = "";
			string strFrom;
			string strTo;
			string strReturn;
			bool boolScheduled;
			clsScheduleDay tDay;
			clsWorkShift tShift;
			boolScheduled = true;
			if (fecherFoundation.Strings.UCase(GridDays[Index].TextMatrix(1, lngCol)) == "FROM")
			{
			}
			else if (fecherFoundation.Strings.UCase(GridDays[Index].TextMatrix(1, lngCol)) == "TO")
			{
				lngCol -= 1;
			}
			if (fecherFoundation.Strings.UCase(GridDays[Index].TextMatrix(0, lngCol)) == "ACTUAL")
			{
				boolScheduled = false;
			}
			strFrom = GridDays[Index].TextMatrix(lngRow, lngCol);
			strTo = GridDays[Index].TextMatrix(lngRow, lngCol + 1);
			if (fecherFoundation.Strings.UCase(strFrom) == "UNSCHEDULED" || fecherFoundation.Strings.UCase(strFrom) == "UNWORKED" || fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(strFrom)) == "")
			{
				strSend = "UNSCHEDULED";
			}
			else
			{
				strSend = strFrom + "," + strTo + "," + GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH) + "," + GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT);
			}
			strReturn = frmGetScheduleTime.InstancePtr.Init(ref strSend);
			string[] strAry = null;
			strAry = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
			strFrom = strAry[0];
			if (fecherFoundation.Strings.UCase(strFrom) == "UNSCHEDULED" || fecherFoundation.Strings.Trim(strFrom) == "")
			{
				if (boolScheduled)
				{
					strFrom = "Unscheduled";
					strTo = "Unscheduled";
				}
				else
				{
					strFrom = "Unworked";
					strTo = "Unworked";
				}
			}
			else
			{
				strFrom = strAry[0];
				strTo = strAry[1];
			}
			GridDays[Index].TextMatrix(lngRow, lngCol, strFrom);
			GridDays[Index].TextMatrix(lngRow, lngCol + 1, strTo);
			if (Information.UBound(strAry, 1) >= 2)
			{
				GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(Conversion.Val(strAry[2])));
			}
			else
			{
				GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(0));
			}
			if (Information.UBound(strAry, 1) >= 3)
			{
				GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, strAry[3]);
			}
			else
			{
				GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, "");
			}
			tDay = wkWorkWeek.GetDay(Index + 1);
			tShift = tDay.GetShiftByIndex(FCConvert.ToInt32(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLINDEX)));
			tShift.ActualStart = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALSTART);
			tShift.ActualEnd = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALEND);
			tShift.ScheduledStart = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDSTART);
			tShift.ScheduledEnd = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDEND);
			tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSHIFT))));
			tShift.LunchTime = Conversion.Val(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH));
			tShift.Account = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT);
			wkWorkWeek.ReCalcWeek();
			LoadGrids();
			FillGridTotals();
		}

		private void GridDays_CellButtonClick(object sender, EventArgs e)
		{
			int index = GridDays.GetIndex((FCGrid)sender);
			GridDays_CellButtonClick(index, sender, e);
		}

		private void GridDays_ComboCloseUp(int Index, object sender, EventArgs e)
		{
			clsScheduleDay tDay;
			clsWorkShift tShift;
			int lngCol;
			int lngRow;
			lngRow = GridDays[Index].Row;
			lngCol = GridDays[Index].Col;
			tDay = wkWorkWeek.GetDay(Index + 1);
			tShift = tDay.GetShiftByIndex(FCConvert.ToInt32(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLINDEX)));
			if (lngCol != CNSTGRIDDAYSCOLSHIFT)
			{
				tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSHIFT))));
			}
			else
			{
				tShift.ShiftType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDays[Index].ComboData())));
				if (tShift.ShiftType > 0)
				{
					clsShiftType tType;
					tType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
					if (!(tType == null))
					{
						GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH, FCConvert.ToString(tType.LunchTime));
						GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT, tType.Account);
					}
				}
			}
			tShift.ActualStart = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALSTART);
			tShift.ActualEnd = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACTUALEND);
			tShift.ScheduledStart = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDSTART);
			tShift.ScheduledEnd = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLSCHEDULEDEND);
			tShift.LunchTime = Conversion.Val(GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLLUNCH));
			tShift.Account = GridDays[Index].TextMatrix(lngRow, CNSTGRIDDAYSCOLACCOUNT);
			wkWorkWeek.ReCalcWeek();
			LoadGrids();
			FillGridTotals();
		}

		private void GridDays_ComboCloseUp(object sender, EventArgs e)
		{
			int index = GridDays.GetIndex((FCGrid)sender);
			GridDays_ComboCloseUp(index, sender, e);
		}

		private void GridDays_KeyDownEvent(int Index, object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						InsertRow(Index);
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteRow(Index);
						break;
					}
			}
			//end switch
		}

		private void GridDays_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int index = GridDays.GetIndex((FCGrid)sender);
			GridDays_KeyDownEvent(index, sender, e);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			ReshowFrame();
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			ReshowFrame();
		}

		private void ReshowFrame()
		{
			int lngWidth;
			int lngOffset;
			int lngPos;
			int lngMax;
			double dblPct = 0;
			// vbPorter upgrade warning: lngLeft As int	OnWriteFCConvert.ToDouble(
			int lngLeft = 0;
			lngOffset = 75;
			//lngWidth = framGrids.WidthOriginal;
			//lngPos = HScroll1.Value;
			//lngMax = lngWidth - (Frame1.WidthOriginal - lngOffset);
			//if (lngMax > 0)
			//{
			//	dblPct = lngPos / 100.0;
			//	lngLeft = FCConvert.ToInt32(-(dblPct * lngMax));
			//	framGrids.LeftOriginal = lngLeft;
			//}
			//else
			//{
			//	framGrids.LeftOriginal = lngOffset;
			//}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			int lngRow;
			int lngCat = 0;
			string defaultWorkComp = "";
			string defaultMSRS = "";
			string defaultStatus = "";
			string defaultWC = "";
			double dblBaseRate = 0;
			string defaultDistU = "";
			int lngLastNum = 0;
			bool boolFound = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strAccount = "";
				string dAccount = "";
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				bool boolSoFar = false;
				SaveData = wkWorkWeek.SaveEmployeeWeek();
				if (boolCurrentWeek)
				{
					// must update the distribution screen
					boolReturn = true;
					clsDRWrapper rsPayCat = new clsDRWrapper();
					clsDRWrapper rsSave = new clsDRWrapper();
					rsPayCat.OpenRecordset("select * from tblpaycategories order by ID", "twpy0000.vb1");
					rsSave.Execute("update tblpayrolldistribution set hoursweek = 0 where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
					rsSave.OpenRecordset("Select * from tblpayrolldistribution where employeenumber = '" + strEmployeeNumber + "' order by recordnumber", "twpy0000.vb1");
					lngLastNum = 0;
					if (!rsSave.EndOfFile())
					{
						defaultWorkComp = FCConvert.ToString(rsSave.Get_Fields("workcomp"));
						defaultMSRS = FCConvert.ToString(rsSave.Get_Fields("msrs"));
						defaultStatus = FCConvert.ToString(rsSave.Get_Fields("statuscode"));
						defaultWC = FCConvert.ToString(rsSave.Get_Fields("wc"));
						dblBaseRate = Conversion.Val(rsSave.Get_Fields("baserate"));
						defaultDistU = FCConvert.ToString(rsSave.Get_Fields("distu"));
						rsSave.MoveLast();
						lngLastNum = FCConvert.ToInt32(rsSave.Get_Fields("recordnumber"));
					}
					for (lngRow = 1; lngRow <= GridTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID)) > 0)
						{
							lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLCAT))));
							strAccount = GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLACCOUNT);
							boolFound = false;
							if (rsSave.FindFirstRecord("cat", lngCat))
							{
								if (fecherFoundation.Strings.Trim(strAccount) == "")
								{
									boolFound = true;
								}
								else
								{
									dAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("accountnumber")));
									if (dAccount.Length > 0)
									{
										boolSoFar = true;
										if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
										{
											for (x = 3; x <= (strAccount.Length); x++)
											{
												if (dAccount.Length >= x)
												{
													if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
													{
														boolSoFar = false;
													}
												}
												else
												{
													boolSoFar = false;
												}
											}
											// x
											if (boolSoFar)
											{
												boolFound = true;
											}
											else
											{
												while (rsSave.FindNextRecord("cat", lngCat) && !boolFound)
												{
													dAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields("accountnumber")));
													if (dAccount.Length > 0)
													{
														boolSoFar = true;
														if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
														{
															for (x = 3; x <= (strAccount.Length); x++)
															{
																if (dAccount.Length >= x)
																{
																	if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
																	{
																		boolSoFar = false;
																	}
																}
																else
																{
																	boolSoFar = false;
																}
															}
															// x
															if (boolSoFar)
															{
																boolFound = true;
																break;
															}
														}
													}
												}
											}
										}
									}
								}
							}
							if (boolFound)
							{
								rsSave.Edit();
								rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID))));
								rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID)) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
								rsSave.Update();
							}
							if (!boolFound)
							{
								// must make new one
								rsSave.AddNew();
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("cat", lngCat);
								lngLastNum += 1;
								rsSave.Set_Fields("recordnumber", lngLastNum);
								if (rsPayCat.FindFirstRecord("ID", lngCat))
								{
									rsSave.Set_Fields("factor", FCConvert.ToString(Conversion.Val(rsPayCat.Get_Fields("multi"))));
									rsSave.Set_Fields("taxcode", rsPayCat.Get_Fields("taxstatus"));
									rsSave.Set_Fields("wc", rsPayCat.Get_Fields_Boolean("workerscomp"));
									rsSave.Set_Fields("accountcode", 2);
									rsSave.Set_Fields("CD", 1);
									rsSave.Set_Fields("numberweeks", 0);
									rsSave.Set_Fields("defaulthours", 0);
									rsSave.Set_Fields("statuscode", "");
									rsSave.Set_Fields("grantfunded", 0);
									rsSave.Set_Fields("msrsid", -3);
									rsSave.Set_Fields("project", 0);
									rsSave.Set_Fields("weekstaxwithheld", 0);
									rsSave.Set_Fields("contractid", 0);
									rsSave.Set_Fields("workcomp", defaultWorkComp);
									rsSave.Set_Fields("msrs", defaultMSRS);
									rsSave.Set_Fields("statuscode", defaultStatus);
									rsSave.Set_Fields("wc", defaultWC);
									rsSave.Set_Fields("baserate", dblBaseRate);
									rsSave.Set_Fields("distu", defaultDistU);
									rsSave.Set_Fields("hoursweek", FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID))));
									rsSave.Set_Fields("gross", Strings.Format(FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID)) * Conversion.Val(rsSave.Get_Fields("factor")) * Conversion.Val(rsSave.Get_Fields("baserate"))), "0.00"));
									if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "DOLLARS")
									{
										rsSave.Set_Fields("baserate", 1);
										rsSave.Set_Fields("gross", FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(lngRow, CNSTGRIDTOTALSCOLPAID))));
									}
									else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsPayCat.Get_Fields("type"))) == "NON-PAY (HOURS)")
									{
										rsSave.Set_Fields("baserate", 0);
										rsSave.Set_Fields("gross", 0);
									}
									MessageBox.Show("Created new distribution line for pay category " + rsPayCat.Get_Fields_String("description") + "\r\n" + "Check to make sure all entries for this line are correct", "New Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								rsSave.Update();
							}
						}
					}
					// lngRow
				}
				MessageBox.Show("Schedule Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
			LoadGrids();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				Close();
			}
			else
			{
				LoadGrids();
			}
		}
	}
}
