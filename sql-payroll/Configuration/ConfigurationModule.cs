﻿using Autofac;
using SharedApplication;
using SharedApplication.Payroll;

namespace TWPY0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmTaxSetup>().As<IView<ITaxTableViewModel>>();
        }
    }
}