//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditPayRecords.
	/// </summary>
	partial class frmEditPayRecords
	{
		public FCGrid gridCheck;
		public fecherFoundation.FCTextBox txtCheckNumber;
		public FCGrid GridDeds;
		public FCGrid GridTotals;
		public FCGrid GridMatch;
		public FCGrid GridDist;
		public FCGrid GridPay;
		public FCGrid GridVacSick;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblEmployee;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridCheck = new fecherFoundation.FCGrid();
			this.txtCheckNumber = new fecherFoundation.FCTextBox();
			this.GridDeds = new fecherFoundation.FCGrid();
			this.GridTotals = new fecherFoundation.FCGrid();
			this.GridMatch = new fecherFoundation.FCGrid();
			this.GridDist = new fecherFoundation.FCGrid();
			this.GridPay = new fecherFoundation.FCGrid();
			this.GridVacSick = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblEmployee = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDist)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVacSick)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 861);
			this.BottomPanel.Size = new System.Drawing.Size(758, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridCheck);
			this.ClientArea.Controls.Add(this.txtCheckNumber);
			this.ClientArea.Controls.Add(this.GridDeds);
			this.ClientArea.Controls.Add(this.GridTotals);
			this.ClientArea.Controls.Add(this.GridMatch);
			this.ClientArea.Controls.Add(this.GridDist);
			this.ClientArea.Controls.Add(this.GridPay);
			this.ClientArea.Controls.Add(this.GridVacSick);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblEmployee);
			this.ClientArea.Size = new System.Drawing.Size(778, 628);
			this.ClientArea.Controls.SetChildIndex(this.lblEmployee, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridVacSick, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridPay, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDist, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridMatch, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridTotals, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDeds, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCheckNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridCheck, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(778, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(188, 28);
			this.HeaderText.Text = "Edit Pay Records";
			// 
			// gridCheck
			// 
			this.gridCheck.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.gridCheck.Cols = 1;
			this.gridCheck.ColumnHeadersVisible = false;
			this.gridCheck.ExtendLastCol = true;
			this.gridCheck.FixedCols = 0;
			this.gridCheck.FixedRows = 0;
			this.gridCheck.Location = new System.Drawing.Point(558, 619);
			this.gridCheck.Name = "gridCheck";
			this.gridCheck.RowHeadersVisible = false;
			this.gridCheck.Rows = 1;
			this.gridCheck.Size = new System.Drawing.Size(195, 42);
			this.gridCheck.TabIndex = 9;
			this.gridCheck.Visible = false;
			this.gridCheck.ComboCloseUp += new System.EventHandler(this.gridCheck_ComboCloseUp);
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtCheckNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckNumber.Location = new System.Drawing.Point(558, 559);
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Size = new System.Drawing.Size(195, 40);
			this.txtCheckNumber.TabIndex = 7;
			this.txtCheckNumber.TextChanged += new System.EventHandler(this.txtCheckNumber_TextChanged);
			// 
			// GridDeds
			// 
			this.GridDeds.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridDeds.Cols = 7;
			this.GridDeds.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDeds.ExtendLastCol = true;
			this.GridDeds.FixedCols = 0;
			this.GridDeds.Location = new System.Drawing.Point(30, 233);
			this.GridDeds.Name = "GridDeds";
			this.GridDeds.ReadOnly = false;
			this.GridDeds.RowHeadersVisible = false;
			this.GridDeds.Rows = 1;
			this.GridDeds.Size = new System.Drawing.Size(723, 128);
			this.GridDeds.StandardTab = false;
			this.GridDeds.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDeds.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.GridDeds, "Use Insert key to add new deduction adjustments");
			this.GridDeds.ComboDropDown += new System.EventHandler(this.GridDeds_ComboDropDown);
			this.GridDeds.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridDeds_BeforeRowColChange);
			this.GridDeds.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridDeds_AfterEdit);
			this.GridDeds.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridDeds_ChangeEdit);
			this.GridDeds.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDeds_KeyDownEvent);
			// 
			// GridTotals
			// 
			this.GridTotals.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridTotals.Cols = 5;
			this.GridTotals.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridTotals.ExtendLastCol = true;
			this.GridTotals.Location = new System.Drawing.Point(30, 689);
			this.GridTotals.Name = "GridTotals";
			this.GridTotals.ReadOnly = false;
			this.GridTotals.Rows = 3;
			this.GridTotals.Size = new System.Drawing.Size(508, 172);
			this.GridTotals.StandardTab = false;
			this.GridTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridTotals.TabIndex = 1001;
			this.GridTotals.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridTotals_ChangeEdit);
			// 
			// GridMatch
			// 
			this.GridMatch.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridMatch.Cols = 8;
			this.GridMatch.ExtendLastCol = true;
			this.GridMatch.FixedCols = 0;
			this.GridMatch.Location = new System.Drawing.Point(30, 381);
			this.GridMatch.Name = "GridMatch";
			this.GridMatch.RowHeadersVisible = false;
			this.GridMatch.Rows = 1;
			this.GridMatch.Size = new System.Drawing.Size(723, 129);
			this.GridMatch.StandardTab = false;
			this.GridMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridMatch.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.GridMatch, "Use Insert key to add new match adjustment records");
			this.GridMatch.ComboDropDown += new System.EventHandler(this.GridMatch_ComboDropDown);
			this.GridMatch.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridMatch_BeforeRowColChange);
			this.GridMatch.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridMatch_AfterEdit);
			this.GridMatch.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridMatch_ChangeEdit);
			this.GridMatch.KeyDown += new Wisej.Web.KeyEventHandler(this.GridMatch_KeyDownEvent);
			// 
			// GridDist
			// 
			this.GridDist.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridDist.Cols = 8;
			this.GridDist.ExtendLastCol = true;
			this.GridDist.FixedCols = 0;
			this.GridDist.Location = new System.Drawing.Point(30, 65);
			this.GridDist.Name = "GridDist";
			this.GridDist.RowHeadersVisible = false;
			this.GridDist.Rows = 1;
			this.GridDist.Size = new System.Drawing.Size(723, 148);
			this.GridDist.StandardTab = false;
			this.GridDist.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDist.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.GridDist, "Use Insert key to add adjustment records");
			this.GridDist.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridDist_BeforeRowColChange);
			this.GridDist.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridDist_AfterEdit);
			this.GridDist.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridDist_ChangeEdit);
			this.GridDist.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDist_KeyDownEvent);
			// 
			// GridPay
			// 
			this.GridPay.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.GridPay.ColumnHeadersHeight = 90;
			this.GridPay.ExtendLastCol = true;
			this.GridPay.FixedCols = 0;
			this.GridPay.Location = new System.Drawing.Point(558, 689);
			this.GridPay.Name = "GridPay";
			this.GridPay.RowHeadersVisible = false;
			this.GridPay.Rows = 3;
			this.GridPay.Size = new System.Drawing.Size(195, 172);
			this.GridPay.TabIndex = 5;
			// 
			// GridVacSick
			// 
			this.GridVacSick.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridVacSick.Cols = 5;
			this.GridVacSick.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridVacSick.ExtendLastCol = true;
			this.GridVacSick.FixedCols = 0;
			this.GridVacSick.Location = new System.Drawing.Point(30, 530);
			this.GridVacSick.Name = "GridVacSick";
			this.GridVacSick.ReadOnly = false;
			this.GridVacSick.RowHeadersVisible = false;
			this.GridVacSick.Rows = 1;
			this.GridVacSick.Size = new System.Drawing.Size(508, 139);
			this.GridVacSick.StandardTab = false;
			this.GridVacSick.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridVacSick.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.GridVacSick, "Use Insert key to add new adjustment records");
			this.GridVacSick.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridVacSick_AfterEdit);
			this.GridVacSick.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridVacSick_ChangeEdit);
			this.GridVacSick.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridVacSick_MouseMoveEvent);
			this.GridVacSick.KeyDown += new Wisej.Web.KeyEventHandler(this.GridVacSick_KeyDownEvent);
			// 
			// Label1
			// 
			this.Label1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.Label1.Location = new System.Drawing.Point(558, 530);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(112, 15);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "CHECK NUMBER";
			// 
			// lblEmployee
			// 
			this.lblEmployee.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblEmployee.Location = new System.Drawing.Point(30, 30);
			this.lblEmployee.Name = "lblEmployee";
			this.lblEmployee.Size = new System.Drawing.Size(723, 15);
			this.lblEmployee.TabIndex = 2;
			this.lblEmployee.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 3;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(350, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmEditPayRecords
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(778, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditPayRecords";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Pay Records";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmEditPayRecords_Load);
			this.Resize += new System.EventHandler(this.frmEditPayRecords_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditPayRecords_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDist)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVacSick)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
    }
}