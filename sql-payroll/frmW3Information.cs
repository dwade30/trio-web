//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW3Information : BaseForm
	{
		public frmW3Information()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW3Information InstancePtr
		{
			get
			{
				return (frmW3Information)Sys.GetInstance(typeof(frmW3Information));
			}
		}

		protected frmW3Information _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       JUNE 23, 2005
		//
		// NOTES:
		//
		// MODIFIED BY:
		// **************************************************
		// private local variables
		private int intCounter;

		public bool SaveChanges()
		{
			bool SaveChanges = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// save all changes
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblW3Information", "TWPY0000.vb1");
				if (rsData.EndOfFile())
				{
					rsData.AddNew();
				}
				else
				{
					rsData.Edit();
				}
				rsData.Set_Fields("ContactPerson", txtContactPerson.Text);
				rsData.Set_Fields("EMail", txtEmail.Text);
				rsData.Set_Fields("PhoneNumber", txtPhoneNumber.Text);
				rsData.Set_Fields("FaxNumber", txtFaxNumber.Text);
				for (intCounter = 0; intCounter <= 7; intCounter++)
				{
					if (cmbKindOfPayer.SelectedIndex == intCounter)
					{
						if (intCounter != 6)
						{
							rsData.Set_Fields("KindOfPayer", intCounter);
							break;
						}
					}
				}
				string strTemp = "";
				if (cmbNoneApply.Text == "None Apply")
				{
					strTemp = "N";
				}
				else if (cmbNoneApply.Text == "501c non-govt")
				{
					strTemp = "T";
				}
				else if (cmbNoneApply.Text == "State/Local non 501c")
				{
					strTemp = "S";
				}
				else if (cmbNoneApply.Text == "Federal govt")
				{
					strTemp = "F";
				}
				else if (cmbNoneApply.Text == "State/local 501c")
				{
					strTemp = "Y";
				}
				else
				{
					strTemp = "S";
				}
				rsData.Set_Fields("KindOfEmployer", strTemp);
				if (chkThirdPartySickPay.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsData.Set_Fields("ThirdPartySickPay", true);
				}
				else
				{
					rsData.Set_Fields("ThirdPartySickPay", false);
				}
				rsData.Update();
				// MsgBox "Save was successful", vbInformation + vbOKOnly, "W3 Information"
				SaveChanges = true;
				return SaveChanges;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return SaveChanges;
			}
		}

		private void frmW3Information_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				// Call ForceFormToResize(Me)
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW3Information_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW3Information_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW3Information properties;
			//frmW3Information.ScaleWidth	= 6570;
			//frmW3Information.ScaleHeight	= 4830;
			//frmW3Information.LinkTopic	= "Form1";
			//frmW3Information.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				// Get the data from the database
				ShowData();
				modGlobalVariables.Statics.gboolCancelW3 = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblW3Information", "TWPY0000.vb1");
				if (!rsData.EndOfFile())
				{
					txtContactPerson.Text = FCConvert.ToString(rsData.Get_Fields_String("ContactPerson"));
					txtEmail.Text = FCConvert.ToString(rsData.Get_Fields("EMail"));
					txtPhoneNumber.Text = FCConvert.ToString(rsData.Get_Fields_String("PhoneNumber"));
					txtFaxNumber.Text = FCConvert.ToString(rsData.Get_Fields_String("FaxNumber"));
					if (cmbKindOfPayer.Items.Count > rsData.Get_Fields_Int16("KindOfPayer") && Conversion.Val(rsData.Get_Fields("kindofpayer")) != 6)
					{
						cmbKindOfPayer.SelectedIndex = FCConvert.ToInt32(rsData.Get_Fields_Int16("KindOfPayer"));
					}
					else
					{
						// chkThirdPartySickPay.Value = vbChecked
						cmbKindOfPayer.SelectedIndex = 0;
					}
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields("kindofemployer"))) == "N")
					{
						cmbNoneApply.Text = "None Apply";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields("kindofemployer"))) == "T")
					{
						cmbNoneApply.Text = "501c non-govt";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields("kindofemployer"))) == "S")
					{
						cmbNoneApply.Text = "State/Local non 501c";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields("kindofemployer"))) == "Y")
					{
						cmbNoneApply.Text = "State/local 501c";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields("kindofemployer"))) == "F")
					{
						cmbNoneApply.Text = "Federal govt";
					}
					else
					{
						cmbNoneApply.Text = "State/Local non 501c";
					}
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ThirdPartySickPay")))
					{
						chkThirdPartySickPay.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkThirdPartySickPay.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolCancelW3 = false;
			SaveChanges();
			Close();
		}
	}
}
