//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmSetupAuditArchiveReport.
	/// </summary>
	partial class frmSetupAuditArchiveReport
	{
		public fecherFoundation.FCComboBox cmbDateAll;
		public fecherFoundation.FCComboBox cmbScreenAll;
		public fecherFoundation.FCComboBox cmbEmployeeAll;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraPayDate;
		public fecherFoundation.FCComboBox cboPayDate;
		public fecherFoundation.FCComboBox cboPayRunID;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboScreen;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbDateAll = new fecherFoundation.FCComboBox();
            this.cmbScreenAll = new fecherFoundation.FCComboBox();
            this.cmbEmployeeAll = new fecherFoundation.FCComboBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.txtLowDate = new Global.T2KDateBox();
            this.txtHighDate = new Global.T2KDateBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.fraPayDate = new fecherFoundation.FCFrame();
            this.cboPayDate = new fecherFoundation.FCComboBox();
            this.cboPayRunID = new fecherFoundation.FCComboBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cboScreen = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboEmployee = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).BeginInit();
            this.fraPayDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(386, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(386, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(386, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(238, 30);
            this.HeaderText.Text = "Audit Archive Report";
            // 
            // cmbDateAll
            // 
            this.cmbDateAll.AutoSize = false;
            this.cmbDateAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDateAll.FormattingEnabled = true;
            this.cmbDateAll.Items.AddRange(new object[] {
            "All",
             "Date Range",
            "Pay Date / Run"});
            this.cmbDateAll.Location = new System.Drawing.Point(20, 30);
            this.cmbDateAll.Name = "cmbDateAll";
			this.cmbDateAll.SelectedIndex = 0;
            this.cmbDateAll.Size = new System.Drawing.Size(280, 40);
            this.cmbDateAll.TabIndex = 15;
			this.cmbDateAll.SelectedIndexChanged += cmbDateAll_SelectedIndexChanged; ;
            // 
            // cmbScreenAll
            // 
            this.cmbScreenAll.AutoSize = false;
            this.cmbScreenAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbScreenAll.FormattingEnabled = true;
            this.cmbScreenAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbScreenAll.Location = new System.Drawing.Point(20, 30);
            this.cmbScreenAll.Name = "cmbScreenAll";
			this.cmbScreenAll.SelectedIndex = 0;
            this.cmbScreenAll.Size = new System.Drawing.Size(280, 40);
            this.cmbScreenAll.TabIndex = 8;
			this.cmbScreenAll.SelectedIndexChanged += cmbScreenAll_SelectedIndexChanged; ;
            // 
            // cmbEmployeeAll
            // 
            this.cmbEmployeeAll.AutoSize = false;
            this.cmbEmployeeAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbEmployeeAll.FormattingEnabled = true;
            this.cmbEmployeeAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbEmployeeAll.Location = new System.Drawing.Point(20, 30);
            this.cmbEmployeeAll.Name = "cmbEmployeeAll";
			this.cmbEmployeeAll.SelectedIndex = 0;
            this.cmbEmployeeAll.Size = new System.Drawing.Size(280, 40);
            this.cmbEmployeeAll.TabIndex = 3;
			this.cmbEmployeeAll.SelectedIndexChanged += cmbEmployeeAll_SelectedIndexChanged; ;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.fraDateRange);
            this.Frame3.Controls.Add(this.cmbDateAll);
            this.Frame3.Controls.Add(this.fraPayDate);
            this.Frame3.Location = new System.Drawing.Point(30, 370);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(320, 210);
            this.Frame3.TabIndex = 10;
            this.Frame3.Text = "Date Range";
            // 
            // fraDateRange
            // 
            this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
            this.fraDateRange.Controls.Add(this.txtLowDate);
            this.fraDateRange.Controls.Add(this.txtHighDate);
            this.fraDateRange.Controls.Add(this.Label2);
            this.fraDateRange.Enabled = false;
            this.fraDateRange.Location = new System.Drawing.Point(1, 80);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(308, 50);
            this.fraDateRange.TabIndex = 14;
            // 
            // txtLowDate
            // 
            this.txtLowDate.Enabled = false;
            this.txtLowDate.Location = new System.Drawing.Point(19, 10);
            this.txtLowDate.Mask = "00/00/0000";
            this.txtLowDate.Name = "txtLowDate";
            this.txtLowDate.Size = new System.Drawing.Size(115, 40);
            this.txtLowDate.TabIndex = 16;
            this.txtLowDate.Text = "";
            // 
            // txtHighDate
            // 
            this.txtHighDate.Enabled = false;
            this.txtHighDate.Location = new System.Drawing.Point(185, 10);
            this.txtHighDate.Mask = "00/00/0000";
            this.txtHighDate.Name = "txtHighDate";
            this.txtHighDate.Size = new System.Drawing.Size(115, 40);
            this.txtHighDate.TabIndex = 17;
            this.txtHighDate.Text = "";
            // 
            // Label2
            // 
            this.Label2.Enabled = false;
            this.Label2.Location = new System.Drawing.Point(148, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 22);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fraPayDate
            // 
            this.fraPayDate.AppearanceKey = "groupBoxNoBorders";
            this.fraPayDate.Controls.Add(this.cboPayDate);
            this.fraPayDate.Controls.Add(this.cboPayRunID);
            this.fraPayDate.Enabled = false;
            this.fraPayDate.Location = new System.Drawing.Point(1, 150);
            this.fraPayDate.Name = "fraPayDate";
            this.fraPayDate.Size = new System.Drawing.Size(308, 40);
            this.fraPayDate.TabIndex = 15;
            // 
            // cboPayDate
            // 
            this.cboPayDate.AutoSize = false;
            this.cboPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayDate.Enabled = false;
            this.cboPayDate.FormattingEnabled = true;
            this.cboPayDate.Location = new System.Drawing.Point(19, 0);
            this.cboPayDate.Name = "cboPayDate";
            this.cboPayDate.Size = new System.Drawing.Size(145, 40);
            this.cboPayDate.TabIndex = 20;
            this.cboPayDate.SelectedIndexChanged += new System.EventHandler(this.cboPayDate_SelectedIndexChanged);
            // 
            // cboPayRunID
            // 
            this.cboPayRunID.AutoSize = false;
            this.cboPayRunID.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayRunID.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayRunID.Enabled = false;
            this.cboPayRunID.FormattingEnabled = true;
            this.cboPayRunID.Location = new System.Drawing.Point(185, 0);
            this.cboPayRunID.Name = "cboPayRunID";
            this.cboPayRunID.Size = new System.Drawing.Size(113, 40);
            this.cboPayRunID.TabIndex = 19;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cboScreen);
            this.Frame2.Controls.Add(this.cmbScreenAll);
            this.Frame2.Location = new System.Drawing.Point(30, 200);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(320, 150);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Screen";
            // 
            // cboScreen
            // 
            this.cboScreen.AutoSize = false;
            this.cboScreen.BackColor = System.Drawing.SystemColors.Window;
            this.cboScreen.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboScreen.Enabled = false;
            this.cboScreen.FormattingEnabled = true;
            this.cboScreen.Location = new System.Drawing.Point(20, 90);
            this.cboScreen.Name = "cboScreen";
            this.cboScreen.Size = new System.Drawing.Size(280, 40);
            this.cboScreen.TabIndex = 8;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cboEmployee);
            this.Frame1.Controls.Add(this.cmbEmployeeAll);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(320, 150);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Employee";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoSize = false;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmployee.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboEmployee.Enabled = false;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.Location = new System.Drawing.Point(20, 90);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(280, 40);
            this.cboEmployee.TabIndex = 4;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 1;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(313, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdPrint.Size = new System.Drawing.Size(45, 24);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(118, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(150, 48);
            this.cmdPrintPreview.TabIndex = 0;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // frmSetupAuditArchiveReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(386, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupAuditArchiveReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Audit Archive Report";
            this.Load += new System.EventHandler(this.frmSetupAuditArchiveReport_Load);
            this.Activated += new System.EventHandler(this.frmSetupAuditArchiveReport_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAuditArchiveReport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).EndInit();
            this.fraPayDate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private FCButton cmdPrintPreview;
		private FCButton cmdPrint;
	}
}