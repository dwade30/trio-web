//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWPY0000
{
	public class modSubMain
	{
		//=========================================================
	

		public static void Main()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Sub Main";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsBD = new clsDRWrapper();
				FCFileSystem ff = new FCFileSystem();
				clsDRWrapper rs = new clsDRWrapper();
				string strDataPath = "";
				
				if (!FCConvert.ToBoolean(modGlobalFunctions.LoadSQLConfig("TWPY0000.VB1")))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				rsBD.OpenRecordset("Select PY, PYDate from Modules", "SystemSettings");
				if (rsBD.EndOfFile())
				{
					MessageBox.Show("Permissions in GE are not set to use the PY application. Please contact TRIO.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
					return;
				}
				else if (rsBD.Get_Fields_Boolean("PY"))
				{
					if (!Information.IsDate(rsBD.Get_Fields("PYDate")) || DateTime.Today.ToOADate() > rsBD.Get_Fields_DateTime("PYDate").ToOADate())
					{
						MessageBox.Show("Permissions in GE are not set to use the PY application. Please contact TRIO.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						Application.Exit();
						return;
					}
				}
				else
				{
					MessageBox.Show("Permissions in GE are not set to use the PY application. Please contact TRIO.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
					return;
				}
				// THIS ROUTINE WILL VALIDATE THAT THE USER HAS ALL THE FIELDS
				// IN THE DATABASE. IF THEY DO NOT EXIST THEN IT WILL ADD THEM.
				CheckVersion();
				modCoreysSweeterCode.Statics.gboolESP = false;
				modCoreysSweeterCode.Statics.gboolTimeClockPlus = false;
				rsBD.OpenRecordset("select * from modules", "SystemSettings");
				if (!rsBD.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsBD.Get_Fields_Boolean("ESPService")))
					{
						if (fecherFoundation.DateAndTime.DateDiff("d", (DateTime)rsBD.Get_Fields("espdate"), DateTime.Today) < 1)
						{
							modCoreysSweeterCode.Statics.gboolESP = true;
						}
					}
					if (Conversion.Val(rsBD.Get_Fields_Int16("TimeClockLevel")) == 1)
					{
						modCoreysSweeterCode.Statics.gboolTimeClockPlus = true;
					}
				}
				rsBD.OpenRecordset("Select BD,BDDate from Modules", "SystemSettings");
				if (rsBD.EndOfFile())
				{
					modGlobalVariables.Statics.gboolBudgetary = false;
					modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms = false;
				}
				else
				{
					if (FCConvert.ToBoolean(rsBD.Get_Fields_Boolean("BD")))
					{
						if (fecherFoundation.DateAndTime.DateDiff("d", (DateTime)rsBD.Get_Fields_DateTime("BDDate"), DateTime.Today) > 1)
						{
							modGlobalVariables.Statics.gboolBudgetary = false;
						}
						else
						{
							modGlobalVariables.Statics.gboolBudgetary = true;
						}
					}
					else
					{
						modGlobalVariables.Statics.gboolBudgetary = false;
					}
				}
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					rsBD.OpenRecordset("select due from BUDGETARY", "twbd0000.vb1");
					if (!rsBD.EndOfFile())
					{
						if (FCConvert.CBool(fecherFoundation.Strings.UCase(FCConvert.ToString(FCConvert.ToString(rsBD.Get_Fields("due")) == "Y"))))
						{
							modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms = true;
						}
						else
						{
							modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms = false;
						}
					}
					else
					{
						modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms = false;
					}
				}
				modGlobalFunctions.UpdateUsedModules();
				modAccountTitle.SetAccountFormats();
				// assign a ROBO HELP file to this application and allow the F1 key to work
				if (Strings.Right(App.Path, 1) == "\\")
				{
					App.HelpFile = App.Path + "TWPY0000.HLP";
				}
				else
				{
					App.HelpFile = App.Path + "\\" + "TWPY0000.HLP";
				}
				// Added 1/14/04 Matthew
				modGlobalFunctions.GetGlobalRegistrySetting();
				modErrorHandler.Statics.gstrFormName = "modSubMain";
                // GET SOME GLOBAL INFORMATION
                //FC:FINAL:AM:#2770 - get the logged in user
                //UserName();
                modGlobalVariables.Statics.gstrUser = TWSharedLibrary.Variables.Statics.UserName;
                modReplaceWorkFiles.GetGlobalVariables();
				GetPrinters();
				modGlobalRoutines.LoadDefaultInformation();
				modGlobalFunctions.LoadTRIOColors();
				DoesDivisionExist();

				modNewAccountBox.GetFormats();
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					modValidateAccount.SetBDFormats();
				}
				modGlobalConstants.Statics.clsSecurityClass.Init("PY");
				// GET SOME GLOBAL INFORMATION
                modReplaceWorkFiles.Statics.gstrNetworkFlag = (FCConvert.ToString(modRegistry.GetRegistryKey("NetworkFlag")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y");
				modGlobalVariables.Statics.gstrPayrollName = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
				modReplaceWorkFiles.Statics.gintSecurityID = FCConvert.ToInt32((FCConvert.ToString(modRegistry.GetRegistryKey("SecurityID")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "0"));
				modGlobalVariables.Statics.gstrMuniName = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName));
				//MDIParent.InstancePtr.StatusBar1.Panels[0].Text = modGlobalConstants.Statics.MuniName;
				App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
				//FC:FINAL:DDU: comment this lines to access grid menu
				//modBlockEntry.WriteYY();
				if (modGlobalConstants.Statics.gstrEntryFlag == "XD")
					modGlobalVariables.Statics.gboolDosFlag = true;
				clsDRWrapper rsData = new clsDRWrapper();
				// LOAD GLOBAL VARIABLES WITH INFORMATION ON WHAT
				// THE PAY RUN AND PAY RUN ID ARE.
				rsData.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gintCurrentPayRun = 1;
					modGlobalVariables.Statics.gdatCurrentPayDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					modGlobalVariables.Statics.gdatCurrentWeekEndingDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					modGlobalVariables.Statics.gintWeekNumber = 1;
				}
				else
				{
					modGlobalVariables.Statics.gintCurrentPayRun = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("PayRunID"))));
					modGlobalVariables.Statics.gdatCurrentPayDate = FCConvert.ToDateTime(Strings.Format(rsData.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy"));
					if (Information.IsDate(rsData.Get_Fields("weekenddate")))
					{
						modGlobalVariables.Statics.gdatCurrentWeekEndingDate = FCConvert.ToDateTime(Strings.Format(rsData.Get_Fields("weekenddate"), "MM/dd/yyyy"));
					}
					else
					{
						modGlobalVariables.Statics.gdatCurrentWeekEndingDate = modGlobalVariables.Statics.gdatCurrentPayDate;
					}
					modGlobalVariables.Statics.gintWeekNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("WeekNumber"))));
				}
				MDIParent.InstancePtr.Init();
				// SET THE DYNAMIC MENU OPTIONS FOR THE HELP FILES FOR EACH MODULE.
				//modGlobalFunctions.SetHelpMenuOptions();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private static void DoesDivisionExist()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (!modGlobalVariables.Statics.gboolBudgetary)
			{
				rsData.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gboolHaveDivision = Conversion.Val(rsData.Get_Fields_Int32("DivLength") + " ") > 0;
				}
				else
				{
					modGlobalVariables.Statics.gboolHaveDivision = false;
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (!rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gboolHaveDivision = Conversion.Val(Strings.Mid(fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Expenditure") + " "), 3, 2)) > 0;
				}
				else
				{
					modGlobalVariables.Statics.gboolHaveDivision = false;
				}
			}
		}

		private static void CheckVersion()
		{
			cPayrollDB pDB = new cPayrollDB();
			if (!pDB.CheckVersion())
			{
				cVersionInfo tVer;
				tVer = pDB.GetVersion();
				MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
			}
			cSystemSettings cSS = new cSystemSettings();
			cSS.CheckVersion();
			cBudgetaryDB bDB = new cBudgetaryDB();
			bDB.CheckVersion();
		}

		public static void GetPrinters()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CallErrorRoutine = 1;
			const int curOnErrorGoToLabel_ErrorRoutine = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				vOnErrorGoToLabel = curOnErrorGoToLabel_CallErrorRoutine;
				/* On Error GoTo CallErrorRoutine */
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GetPrinters";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//FC:FINAL:DDU: commented out this code part because isn't needed
				//goto ResumeCode;
				//CallErrorRoutine:
				//;
				//modErrorHandler.SetErrorHandler(ex);
				//return;
				ResumeCode:
				;
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorRoutine;
				/* On Error GoTo ErrorRoutine */
				fecherFoundation.Information.Err().Clear();
				modGlobalVariables.Statics.gstrReceiptPrinter = FCConvert.ToString(modReplaceWorkFiles.StripColon(fecherFoundation.Strings.Trim((FCConvert.ToString(modRegistry.GetRegistryKey("RcptprinterPort")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : ""))));
				modGlobalVariables.Statics.gstrReceiptWidth = FCConvert.ToString(modReplaceWorkFiles.StripColon(fecherFoundation.Strings.Trim((FCConvert.ToString(modRegistry.GetRegistryKey("RcptWidth")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : ""))));
				modGlobalVariables.Statics.gstrMvr3Printer = FCConvert.ToString(modReplaceWorkFiles.StripColon(fecherFoundation.Strings.Trim((FCConvert.ToString(modRegistry.GetRegistryKey("MVR3PrinterPort")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : ""))));
			}
			catch (Exception ex)
			{
				ErrorRoutine:
				;
				MessageBox.Show(fecherFoundation.Information.Err(ex).Description);
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_CallErrorRoutine:
						//? goto CallErrorRoutine;
						break;
					case curOnErrorGoToLabel_ErrorRoutine:
						//? goto ErrorRoutine;
						break;
				}
			}
		}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsBud = new clsDRWrapper();
			public clsDRWrapper rsBud_AutoInitialized = null;
			public clsDRWrapper rsBud
			{
				get
				{
					if ( rsBud_AutoInitialized == null)
					{
						 rsBud_AutoInitialized = new clsDRWrapper();
					}
					return rsBud_AutoInitialized;
				}
				set
				{
					 rsBud_AutoInitialized = value;
				}
			}
			private bool boolMessageShown;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
