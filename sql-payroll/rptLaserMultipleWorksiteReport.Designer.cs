﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptLaserMultipleWorksiteReport.
	/// </summary>
	partial class rptLaserMultipleWorksiteReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLaserMultipleWorksiteReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateQuarterEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine7b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine7a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine16Return = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine16Refund = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckSemiWeekly = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckMonthly = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine17a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine17b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine17c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine17d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateQuarterEnded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16Return)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16Refund)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckSemiWeekly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckMonthly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Image1,
				this.txtName,
				this.txtAddress,
				this.txtCityStateZip,
				this.txtEmployerID,
				this.txtDateQuarterEnded,
				this.txtLine2,
				this.txtLine3,
				this.txtLine4,
				this.txtLine5,
				this.txtLine6b,
				this.txtLine6d,
				this.txtLine7b,
				this.txtLine8,
				this.txtLine9,
				this.txtLine11,
				this.txtLine12,
				this.txtLine14,
				this.txtLine13,
				this.txtLine15,
				this.txtLine10,
				this.txtLine6a,
				this.txtLine6c,
				this.txtLine7a,
				this.txtLine16,
				this.txtLine16Return,
				this.txtLine16Refund,
				this.txtLine1,
				this.Field1,
				this.txtDate,
				this.txtCheckSemiWeekly,
				this.txtCheckMonthly,
				this.txtLine17a,
				this.txtLine17b,
				this.txtLine17c,
				this.txtLine17d
			});
			this.Detail.Height = 10F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.ReportFooter.Height = 0.07291666F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Image1
			// 
			this.Image1.Height = 10F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = ((System.IO.Stream)(resources.GetObject("Image1.ImageData")));
			this.Image1.Left = 0F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.Image1.Top = 0F;
			this.Image1.Width = 7.5F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.5625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = null;
			this.txtName.Top = 1F;
			this.txtName.Width = 2.4375F;
			// 
			// txtAddress
			// 
			this.txtAddress.CanGrow = false;
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.5625F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.625F;
			this.txtAddress.Width = 2.4375F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.CanGrow = false;
			this.txtCityStateZip.Height = 0.1875F;
			this.txtCityStateZip.Left = 4.125F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Style = "font-family: \'Tahoma\'";
			this.txtCityStateZip.Text = null;
			this.txtCityStateZip.Top = 1.625F;
			this.txtCityStateZip.Width = 1.875F;
			// 
			// txtEmployerID
			// 
			this.txtEmployerID.CanGrow = false;
			this.txtEmployerID.Height = 0.1875F;
			this.txtEmployerID.Left = 4.125F;
			this.txtEmployerID.Name = "txtEmployerID";
			this.txtEmployerID.Style = "font-family: \'Tahoma\'";
			this.txtEmployerID.Text = null;
			this.txtEmployerID.Top = 1.3125F;
			this.txtEmployerID.Width = 1.875F;
			// 
			// txtDateQuarterEnded
			// 
			this.txtDateQuarterEnded.CanGrow = false;
			this.txtDateQuarterEnded.Height = 0.1875F;
			this.txtDateQuarterEnded.Left = 4.125F;
			this.txtDateQuarterEnded.Name = "txtDateQuarterEnded";
			this.txtDateQuarterEnded.Style = "font-family: \'Tahoma\'";
			this.txtDateQuarterEnded.Text = null;
			this.txtDateQuarterEnded.Top = 1F;
			this.txtDateQuarterEnded.Width = 1.4375F;
			// 
			// txtLine2
			// 
			this.txtLine2.CanGrow = false;
			this.txtLine2.Height = 0.1875F;
			this.txtLine2.Left = 5.9375F;
			this.txtLine2.Name = "txtLine2";
			this.txtLine2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine2.Text = null;
			this.txtLine2.Top = 3.65625F;
			this.txtLine2.Width = 1.25F;
			// 
			// txtLine3
			// 
			this.txtLine3.CanGrow = false;
			this.txtLine3.Height = 0.1875F;
			this.txtLine3.Left = 5.9375F;
			this.txtLine3.Name = "txtLine3";
			this.txtLine3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine3.Text = null;
			this.txtLine3.Top = 3.8125F;
			this.txtLine3.Width = 1.25F;
			// 
			// txtLine4
			// 
			this.txtLine4.CanGrow = false;
			this.txtLine4.Height = 0.1875F;
			this.txtLine4.Left = 5.9375F;
			this.txtLine4.Name = "txtLine4";
			this.txtLine4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine4.Text = null;
			this.txtLine4.Top = 3.96875F;
			this.txtLine4.Width = 1.25F;
			// 
			// txtLine5
			// 
			this.txtLine5.CanGrow = false;
			this.txtLine5.Height = 0.1875F;
			this.txtLine5.Left = 5.9375F;
			this.txtLine5.Name = "txtLine5";
			this.txtLine5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine5.Text = null;
			this.txtLine5.Top = 4.125F;
			this.txtLine5.Width = 1.25F;
			// 
			// txtLine6b
			// 
			this.txtLine6b.CanGrow = false;
			this.txtLine6b.Height = 0.1875F;
			this.txtLine6b.Left = 5.9375F;
			this.txtLine6b.Name = "txtLine6b";
			this.txtLine6b.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine6b.Text = null;
			this.txtLine6b.Top = 4.28125F;
			this.txtLine6b.Width = 1.25F;
			// 
			// txtLine6d
			// 
			this.txtLine6d.CanGrow = false;
			this.txtLine6d.Height = 0.1875F;
			this.txtLine6d.Left = 5.9375F;
			this.txtLine6d.Name = "txtLine6d";
			this.txtLine6d.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine6d.Text = null;
			this.txtLine6d.Top = 4.4375F;
			this.txtLine6d.Width = 1.25F;
			// 
			// txtLine7b
			// 
			this.txtLine7b.CanGrow = false;
			this.txtLine7b.Height = 0.1875F;
			this.txtLine7b.Left = 5.9375F;
			this.txtLine7b.Name = "txtLine7b";
			this.txtLine7b.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine7b.Text = null;
			this.txtLine7b.Top = 4.59375F;
			this.txtLine7b.Width = 1.25F;
			// 
			// txtLine8
			// 
			this.txtLine8.CanGrow = false;
			this.txtLine8.Height = 0.1875F;
			this.txtLine8.Left = 5.9375F;
			this.txtLine8.Name = "txtLine8";
			this.txtLine8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine8.Text = null;
			this.txtLine8.Top = 4.875F;
			this.txtLine8.Width = 1.25F;
			// 
			// txtLine9
			// 
			this.txtLine9.CanGrow = false;
			this.txtLine9.Height = 0.1875F;
			this.txtLine9.Left = 5.9375F;
			this.txtLine9.Name = "txtLine9";
			this.txtLine9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine9.Text = "0.00";
			this.txtLine9.Top = 5.1875F;
			this.txtLine9.Width = 1.25F;
			// 
			// txtLine11
			// 
			this.txtLine11.CanGrow = false;
			this.txtLine11.Height = 0.1875F;
			this.txtLine11.Left = 5.9375F;
			this.txtLine11.Name = "txtLine11";
			this.txtLine11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine11.Text = null;
			this.txtLine11.Top = 5.53125F;
			this.txtLine11.Width = 1.25F;
			// 
			// txtLine12
			// 
			this.txtLine12.CanGrow = false;
			this.txtLine12.Height = 0.1875F;
			this.txtLine12.Left = 5.9375F;
			this.txtLine12.Name = "txtLine12";
			this.txtLine12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine12.Text = null;
			this.txtLine12.Top = 5.6875F;
			this.txtLine12.Width = 1.25F;
			// 
			// txtLine14
			// 
			this.txtLine14.CanGrow = false;
			this.txtLine14.Height = 0.1875F;
			this.txtLine14.Left = 5.9375F;
			this.txtLine14.Name = "txtLine14";
			this.txtLine14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine14.Text = null;
			this.txtLine14.Top = 6.1875F;
			this.txtLine14.Width = 1.25F;
			// 
			// txtLine13
			// 
			this.txtLine13.CanGrow = false;
			this.txtLine13.Height = 0.1875F;
			this.txtLine13.Left = 5.9375F;
			this.txtLine13.Name = "txtLine13";
			this.txtLine13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine13.Text = null;
			this.txtLine13.Top = 6F;
			this.txtLine13.Width = 1.25F;
			// 
			// txtLine15
			// 
			this.txtLine15.CanGrow = false;
			this.txtLine15.Height = 0.1875F;
			this.txtLine15.Left = 5.9375F;
			this.txtLine15.Name = "txtLine15";
			this.txtLine15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine15.Text = null;
			this.txtLine15.Top = 6.5F;
			this.txtLine15.Width = 1.25F;
			// 
			// txtLine10
			// 
			this.txtLine10.CanGrow = false;
			this.txtLine10.Height = 0.1875F;
			this.txtLine10.Left = 5.9375F;
			this.txtLine10.Name = "txtLine10";
			this.txtLine10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine10.Text = null;
			this.txtLine10.Top = 5.375F;
			this.txtLine10.Width = 1.25F;
			// 
			// txtLine6a
			// 
			this.txtLine6a.CanGrow = false;
			this.txtLine6a.Height = 0.1875F;
			this.txtLine6a.Left = 3.3125F;
			this.txtLine6a.Name = "txtLine6a";
			this.txtLine6a.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine6a.Text = null;
			this.txtLine6a.Top = 4.28125F;
			this.txtLine6a.Width = 1.125F;
			// 
			// txtLine6c
			// 
			this.txtLine6c.CanGrow = false;
			this.txtLine6c.Height = 0.1875F;
			this.txtLine6c.Left = 3.3125F;
			this.txtLine6c.Name = "txtLine6c";
			this.txtLine6c.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine6c.Text = null;
			this.txtLine6c.Top = 4.4375F;
			this.txtLine6c.Width = 1.125F;
			// 
			// txtLine7a
			// 
			this.txtLine7a.CanGrow = false;
			this.txtLine7a.Height = 0.1875F;
			this.txtLine7a.Left = 3.3125F;
			this.txtLine7a.Name = "txtLine7a";
			this.txtLine7a.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine7a.Text = null;
			this.txtLine7a.Top = 4.59375F;
			this.txtLine7a.Width = 1.125F;
			// 
			// txtLine16
			// 
			this.txtLine16.CanGrow = false;
			this.txtLine16.Height = 0.1875F;
			this.txtLine16.Left = 4.375F;
			this.txtLine16.Name = "txtLine16";
			this.txtLine16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine16.Text = null;
			this.txtLine16.Top = 6.625F;
			this.txtLine16.Width = 1.25F;
			// 
			// txtLine16Return
			// 
			this.txtLine16Return.Height = 0.1875F;
			this.txtLine16Return.Left = 1.854167F;
			this.txtLine16Return.Name = "txtLine16Return";
			this.txtLine16Return.Style = "font-family: \'Tahoma\'";
			this.txtLine16Return.Text = null;
			this.txtLine16Return.Top = 6.847222F;
			this.txtLine16Return.Width = 0.1875F;
			// 
			// txtLine16Refund
			// 
			this.txtLine16Refund.Height = 0.1875F;
			this.txtLine16Refund.Left = 3.770833F;
			this.txtLine16Refund.Name = "txtLine16Refund";
			this.txtLine16Refund.Style = "font-family: \'Tahoma\'";
			this.txtLine16Refund.Text = null;
			this.txtLine16Refund.Top = 6.847222F;
			this.txtLine16Refund.Width = 0.1875F;
			// 
			// txtLine1
			// 
			this.txtLine1.CanGrow = false;
			this.txtLine1.Height = 0.1875F;
			this.txtLine1.Left = 4.5F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine1.Text = null;
			this.txtLine1.Top = 3.5F;
			this.txtLine1.Width = 1.125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 6.885417F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = "X";
			this.Field1.Top = 8.25F;
			this.Field1.Width = 0.1875F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.3125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txtDate.Text = null;
			this.txtDate.Top = 9.125F;
			this.txtDate.Width = 0.875F;
			// 
			// txtCheckSemiWeekly
			// 
			this.txtCheckSemiWeekly.Height = 0.1875F;
			this.txtCheckSemiWeekly.Left = 7F;
			this.txtCheckSemiWeekly.Name = "txtCheckSemiWeekly";
			this.txtCheckSemiWeekly.Style = "font-family: \'Tahoma\'";
			this.txtCheckSemiWeekly.Text = null;
			this.txtCheckSemiWeekly.Top = 7.267361F;
			this.txtCheckSemiWeekly.Width = 0.1875F;
			// 
			// txtCheckMonthly
			// 
			this.txtCheckMonthly.Height = 0.1875F;
			this.txtCheckMonthly.Left = 7F;
			this.txtCheckMonthly.Name = "txtCheckMonthly";
			this.txtCheckMonthly.Style = "font-family: \'Tahoma\'";
			this.txtCheckMonthly.Text = null;
			this.txtCheckMonthly.Top = 7.461805F;
			this.txtCheckMonthly.Width = 0.1875F;
			// 
			// txtLine17a
			// 
			this.txtLine17a.CanGrow = false;
			this.txtLine17a.Height = 0.1875F;
			this.txtLine17a.Left = 0.4375F;
			this.txtLine17a.Name = "txtLine17a";
			this.txtLine17a.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine17a.Text = null;
			this.txtLine17a.Top = 8.0625F;
			this.txtLine17a.Width = 1.4375F;
			// 
			// txtLine17b
			// 
			this.txtLine17b.CanGrow = false;
			this.txtLine17b.Height = 0.1875F;
			this.txtLine17b.Left = 2.1875F;
			this.txtLine17b.Name = "txtLine17b";
			this.txtLine17b.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine17b.Text = null;
			this.txtLine17b.Top = 8.0625F;
			this.txtLine17b.Width = 1.4375F;
			// 
			// txtLine17c
			// 
			this.txtLine17c.CanGrow = false;
			this.txtLine17c.Height = 0.1875F;
			this.txtLine17c.Left = 4F;
			this.txtLine17c.Name = "txtLine17c";
			this.txtLine17c.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine17c.Text = null;
			this.txtLine17c.Top = 8.0625F;
			this.txtLine17c.Width = 1.4375F;
			// 
			// txtLine17d
			// 
			this.txtLine17d.CanGrow = false;
			this.txtLine17d.Height = 0.1875F;
			this.txtLine17d.Left = 5.6875F;
			this.txtLine17d.Name = "txtLine17d";
			this.txtLine17d.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtLine17d.Text = null;
			this.txtLine17d.Top = 8.0625F;
			this.txtLine17d.Width = 1.4375F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.5F;
			// 
			// rptLaserMultipleWorksiteReport
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateQuarterEnded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16Return)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16Refund)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckSemiWeekly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckMonthly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateQuarterEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16Return;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16Refund;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckSemiWeekly;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckMonthly;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17d;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
	}
}
