﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmDistributionBaseRateUpdate : BaseForm
	{
		public frmDistributionBaseRateUpdate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDistributionBaseRateUpdate InstancePtr
		{
			get
			{
				return (frmDistributionBaseRateUpdate)Sys.GetInstance(typeof(frmDistributionBaseRateUpdate));
			}
		}

		protected frmDistributionBaseRateUpdate _InstancePtr = null;
		//=========================================================
		// ****************************************************************************
		// ***************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTTHEW LARRABEE
		// DATE:       APRIL 29, 2004
		//
		// NOTES:  THIS SCREEN WAS BUILT TO ALLOW THE USER THE
		// ABILITY TO UPDATE MULTIPLE DISTRIBUTION LINES THAT
		// HAVE THE SAME BASE PAY WITH EITHER A NEW FLAT RATE
		// OR ENTER A PERCENTAGE TO HAVE THE BASE RATE ADDED TO.
		//
		// ****************************************************************************
		// ****************************************************************************
		// PRIVATE LOCAL VARIABLES
		private int intCounter;
		private clsHistory clsHistoryClass = new clsHistory();

		private void frmDistributionBaseRateUpdate_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// DO NOT ALLOW MULTIPLE INSTANCES OF THIS FORM TO EXIST
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDistributionBaseRateUpdate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void SaveContinue()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveContinue";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				double dblRate = 0;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				// CHECK TO SEE IF THE CURRENT RATE HAS BEEN ENTERED
				if (txtCurrentRate.Text == string.Empty)
				{
					txtCurrentRate.Text = "0";
				}
				// CHECK THE VALIDITY OF THE CURRENT VALUE.
				if (Conversion.Val(txtCurrentRate.Text) < 0 || txtCurrentRate.Text == string.Empty)
				{
					MessageBox.Show("Invalid Current Rate of Distribution.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtCurrentRate.Focus();
					return;
				}
				// CHECK TO SEE IF THEY HAVE ENTERED A NEW RATE
				if (txtNewRate.Text == string.Empty)
				{
					txtNewRate.Text = FCConvert.ToString(0);
				}
				// CHECK THE VALIDITY OF THE NEW VALUE AND MAKE SURE THAT IT IS POSITIVE
				if (Conversion.Val(txtNewRate.Text) < 0 || txtNewRate.Text == string.Empty)
				{
					MessageBox.Show("Invalid New Rate of Distribution.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtNewRate.Focus();
					return;
				}
				if (MessageBox.Show("Are you sure you wish to update all records with the base rate of:  " + txtCurrentRate.Text + (cmbtion1.Text == "New Amount" ? "  to a rate of:  " : "  with a percentage of  ") + txtNewRate.Text + (cmbtion1.Text == "Percentage Adjustment" ? "% " : "") + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGlobalFunctions.AddCYAEntry("PY", "Multiple Base Rate Update", "From: " + txtCurrentRate.Text, "To: " + txtNewRate.Text, "Type: " + (cmbtion1.Text == "New Amount" ? "Flat Rate" : "Percentage"), "EmployeeNumber: " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
					// MATTHEW 4/30/04 WE DO NOT WANT TO UPDATE THE TABLE AS IF WE WERE TO RELOAD THE
					// DISTIRBUTION GRID TO SHOW THIS CHANGE WE WOULD LOOSE ANY OTHER CHANGE THAT
					// WAS DONE TO THIS SCREEN BEFORE THEY CHOOSE TO UPDATE  THE MULTIPLE BASE RATES.
					// Dim rsExecute As New clsDRWrapper
					// If Option1(0) Then
					// this is a flat rate adjustment
					// Call rsExecute.Execute("Update tblPayrollDistribution Set BaseRate = " & CDbl(txtNewRate.Text) & " where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' AND BaseRate = " & Format(txtCurrentRate.Text, "0.0000"), "TWPY0000.vb1")
					// dblRate = CDbl(txtNewRate.Text)
					// Else
					// this is a percentage adjustment
					// Call rsExecute.OpenRecordset("Select BaseRate from tblPayrollDistribution where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' AND BaseRate = " & Format(txtCurrentRate.Text, "0.0000"), "TWPY0000.vb1")
					// While Not rsExecute.EndOfFile
					// rsExecute.Edit
					// rsExecute.Fields("BaseRate") = rsExecute.Fields("BaseRate") + (rsExecute.Fields("BaseRate") * (CDbl(txtNewRate.Text) / 100))
					// rsExecute.Update
					// 
					// rsExecute.MoveNext
					// Wend
					// End If
					for (intCounter = 2; intCounter <= (frmPayrollDistribution.InstancePtr.Grid.Rows - 1); intCounter++)
					{
						if (cmbtion1.Text == "New Amount")
						{
							// this is a flat rate adjustment
							dblRate = FCConvert.ToDouble(txtNewRate.Text);
						}
						else
						{
							// this is a percentage
							dblRate = FCConvert.ToDouble(frmPayrollDistribution.InstancePtr.Grid.TextMatrix(intCounter, 11)) + (FCConvert.ToDouble(frmPayrollDistribution.InstancePtr.Grid.TextMatrix(intCounter, 11)) * (FCConvert.ToDouble(txtNewRate.Text) / 100));
						}
						// UPDATE THE GRID WITH THE NEW RATE. THEY WILL NEED TO STILL SAVE THE DISTRIBUTION SCREEN
						if (Strings.Format(frmPayrollDistribution.InstancePtr.Grid.TextMatrix(intCounter, 11), "0.0000") == Strings.Format(FCConvert.ToDouble(txtCurrentRate.Text), "0.0000"))
						{
							frmPayrollDistribution.InstancePtr.Grid.TextMatrix(intCounter, 11, FCConvert.ToString(dblRate));
						}
					}
					Close();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDistributionBaseRateUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDistributionBaseRateUpdate properties;
			//frmDistributionBaseRateUpdate.ScaleWidth	= 8145;
			//frmDistributionBaseRateUpdate.ScaleHeight	= 4800;
			//frmDistributionBaseRateUpdate.LinkTopic	= "Form1";
			//frmDistributionBaseRateUpdate.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// SET THE SIZE OF THE FORM
				modGlobalFunctions.SetFixedSize(this, 1);
				fraWarning.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				lblPercentage.Visible = false;
				lblEmployee.Text = frmPayrollDistribution.InstancePtr.lblEmployeeNumber.Text;
				this.txtCurrentRate.Text = FCConvert.ToString(FCConvert.ToDouble(Conversion.Val(frmPayrollDistribution.InstancePtr.Grid.TextMatrix(frmPayrollDistribution.InstancePtr.Grid.Row, 11))));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// UNLOAD THE FORM
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// SET FOCUS BACK TO THE MENU OPTIONS OF THE MDIPARENT
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveContinue();
		}

		private void Option1_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// SHOW THE PERCENTAGE DESCRIPTION IF THEY HAVE CHOOSE THIS AS THE ADJUSTMENT TYPE
			lblPercentage.Visible = Index == 1;
		}

		private void Option1_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtion1.SelectedIndex;
			Option1_CheckedChanged(index, sender, e);
		}

		private void txtCurrentRate_Enter(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtCurrentRate.Text).Length > 0)
			{
				txtCurrentRate.SelectionStart = 0;
				txtCurrentRate.SelectionLength = txtCurrentRate.Text.Length;
			}
		}

		private void txtNewRate_Enter(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtNewRate.Text).Length > 0)
			{
				txtNewRate.SelectionStart = 0;
				txtNewRate.SelectionLength = txtNewRate.Text.Length;
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(cmdSave, EventArgs.Empty);
        }
    }
}
