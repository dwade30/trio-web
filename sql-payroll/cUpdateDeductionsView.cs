﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cUpdateDeductionsView
	{
		//=========================================================
		private cEmployeeService empServ = new cEmployeeService();
		private cDeductionController dedServ = new cDeductionController();
		private int lngCurrentUserID;
		private Dictionary<string, cEmployee> collAllowedEmployees = new Dictionary<string, cEmployee>();
		private cGenericCollection collEmpsWithDed = new cGenericCollection();
		private cGenericCollection collFilteredEmps = new cGenericCollection();
		private cGenericCollection collDeductions = new cGenericCollection();
		private cGenericCollection collDepartments = new cGenericCollection();
		private cGenericCollection collSequences = new cGenericCollection();
		private cGenericCollection collCodeOnes = new cGenericCollection();
		private cGenericCollection collCodeTwos = new cGenericCollection();
		private cGenericCollection collGroups = new cGenericCollection();
		private cGenericCollection collFullTime = new cGenericCollection();
		private cGenericCollection collAmountTypes = new cGenericCollection();
		private cGenericCollection collFrequencies = new cGenericCollection();
		private cGenericCollection collPercentTypes = new cGenericCollection();
		private cGenericCollection collLimitTypes = new cGenericCollection();
		private int intUseDepartment;
		private int intUseSequenceNumber;
		private int intUseCode1;
		private int intUseCode2;
		private int intUseGroupID;
		private int intUseFullTime;
		private int lngCurrentDeduction;

		public delegate void DeductionsChangedEventHandler();

		public event DeductionsChangedEventHandler DeductionsChanged;

		public delegate void CurrentDeductionChangedEventHandler();

		public event CurrentDeductionChangedEventHandler CurrentDeductionChanged;

		public delegate void DepartmentsChangedEventHandler();

		public event DepartmentsChangedEventHandler DepartmentsChanged;

		public delegate void SequencesChangedEventHandler();

		public event SequencesChangedEventHandler SequencesChanged;

		public delegate void CodeOnesChangedEventHandler();

		public event CodeOnesChangedEventHandler CodeOnesChanged;

		public delegate void CodeTwosChangedEventHandler();

		public event CodeTwosChangedEventHandler CodeTwosChanged;

		public delegate void FullTimesChangedEventHandler();

		public event FullTimesChangedEventHandler FullTimesChanged;

		public delegate void GroupIDsChangedEventHandler();

		public event GroupIDsChangedEventHandler GroupIDsChanged;

		public delegate void FilteredEmployeesChangedEventHandler();

		public event FilteredEmployeesChangedEventHandler FilteredEmployeesChanged;

		public delegate void AmountTypesChangedEventHandler();

		public event AmountTypesChangedEventHandler AmountTypesChanged;

		public delegate void FrequenciesChangedEventHandler();

		public event FrequenciesChangedEventHandler FrequenciesChanged;

		public delegate void PercentTypesChangedEventHandler();

		public event PercentTypesChangedEventHandler PercentTypesChanged;

		public delegate void LimitTypesChangedEventHandler();

		public event LimitTypesChangedEventHandler LimitTypesChanged;

		public cGenericCollection LimitTypes
		{
			get
			{
				cGenericCollection LimitTypes = null;
				LimitTypes = collLimitTypes;
				return LimitTypes;
			}
		}

		public cGenericCollection PercentTypes
		{
			get
			{
				cGenericCollection PercentTypes = null;
				PercentTypes = collPercentTypes;
				return PercentTypes;
			}
		}

		public cGenericCollection Frequencies
		{
			get
			{
				cGenericCollection Frequencies = null;
				Frequencies = collFrequencies;
				return Frequencies;
			}
		}

		public cGenericCollection AmountTypes
		{
			get
			{
				cGenericCollection AmountTypes = null;
				AmountTypes = collAmountTypes;
				return AmountTypes;
			}
		}

		public cGenericCollection FullTimes
		{
			get
			{
				cGenericCollection FullTimes = null;
				FullTimes = collFullTime;
				return FullTimes;
			}
		}

		public cGenericCollection Groups
		{
			get
			{
				cGenericCollection Groups = null;
				Groups = collGroups;
				return Groups;
			}
		}

		public cGenericCollection CodeTwos
		{
			get
			{
				cGenericCollection CodeTwos = null;
				CodeTwos = collCodeTwos;
				return CodeTwos;
			}
		}

		public cGenericCollection CodeOnes
		{
			get
			{
				cGenericCollection CodeOnes = null;
				CodeOnes = collCodeOnes;
				return CodeOnes;
			}
		}

		public cGenericCollection Sequences
		{
			get
			{
				cGenericCollection Sequences = null;
				Sequences = collSequences;
				return Sequences;
			}
		}

		public cGenericCollection Departments
		{
			get
			{
				cGenericCollection Departments = null;
				Departments = collDepartments;
				return Departments;
			}
		}

		public cGenericCollection Deductions
		{
			get
			{
				cGenericCollection Deductions = null;
				Deductions = collDeductions;
				return Deductions;
			}
		}

		private void GetAllowedEmployees()
		{
			collAllowedEmployees = empServ.GetEmployeesPermissableAsDictionary(lngCurrentUserID, "EmployeeNumber", false);
		}

		private void GetEmployeesWithDeduction()
		{
			collEmpsWithDed = dedServ.GetEmployeesUsingDeduction(lngCurrentDeduction);
		}

		public int CurrentCodeTwoIndex
		{
			set
			{
				if (value > 0)
				{
					if (collCodeTwos.ItemCount() >= value)
					{
						if (value != intUseCode2)
						{
							intUseCode2 = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentCodeTwoIndex = 0;
				CurrentCodeTwoIndex = intUseCode2;
				return CurrentCodeTwoIndex;
			}
		}

		public int CurrentDepartmentIndex
		{
			set
			{
				if (value > 0)
				{
					if (collDepartments.ItemCount() >= value)
					{
						if (value != intUseDepartment)
						{
							intUseDepartment = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentDepartmentIndex = 0;
				CurrentDepartmentIndex = intUseDepartment;
				return CurrentDepartmentIndex;
			}
		}

		public int CurrentCodeOneIndex
		{
			set
			{
				if (value > 0)
				{
					if (collCodeOnes.ItemCount() >= value)
					{
						if (value != intUseCode1)
						{
							intUseCode1 = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentCodeOneIndex = 0;
				CurrentCodeOneIndex = intUseCode1;
				return CurrentCodeOneIndex;
			}
		}

		public int CurrentFullTimePartTime
		{
			set
			{
				if (value > 0 && value < 4)
				{
					if (intUseFullTime != value)
					{
						intUseFullTime = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentFullTimePartTime = 0;
				CurrentFullTimePartTime = intUseFullTime;
				return CurrentFullTimePartTime;
			}
		}

		public int CurrentGroupIDIndex
		{
			set
			{
				if (collGroups.ItemCount() >= value)
				{
					if (intUseGroupID != value)
					{
						intUseGroupID = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentGroupIDIndex = 0;
				CurrentGroupIDIndex = intUseGroupID;
				return CurrentGroupIDIndex;
			}
		}

		public int CurrentSequenceIndex
		{
			set
			{
				if (collSequences.ItemCount() >= value)
				{
					if (intUseSequenceNumber != value)
					{
						intUseSequenceNumber = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentSequenceIndex = 0;
				CurrentSequenceIndex = intUseSequenceNumber;
				return CurrentSequenceIndex;
			}
		}

		public cGenericCollection FilteredEmployees
		{
			get
			{
				cGenericCollection FilteredEmployees = null;
				FilteredEmployees = collFilteredEmps;
				return FilteredEmployees;
			}
		}

		public void SetCurrentDeduction(int lngID)
		{
			if (lngID == lngCurrentDeduction)
			{
				return;
			}
			collDeductions.MoveFirst();
			cDeductionSetup dedSet;
			while (collDeductions.IsCurrent())
			{
				//App.DoEvents();
				dedSet = (cDeductionSetup)collDeductions.GetCurrentItem();
				if (dedSet.ID == lngID)
				{
					lngCurrentDeduction = dedSet.ID;
					GetEmployeesWithDeduction();
					if (this.CurrentDeductionChanged != null)
						this.CurrentDeductionChanged();
					FilterEmployees();
				}
				collDeductions.MoveNext();
			}
		}

		public int CurrentDeduction
		{
			get
			{
				int CurrentDeduction = 0;
				CurrentDeduction = lngCurrentDeduction;
				return CurrentDeduction;
			}
		}

		public void InitializeValues(int lngUserID)
		{
			lngCurrentUserID = lngUserID;
			LoadDeductions();
			FillLists();
			GetAllowedEmployees();
			collDeductions.MoveFirst();
			if (collDeductions.IsCurrent())
			{
				cDeductionSetup dedSet;
				dedSet = (cDeductionSetup)collDeductions.GetCurrentItem();
				lngCurrentDeduction = dedSet.ID;
				if (this.CurrentDeductionChanged != null)
					this.CurrentDeductionChanged();
				GetEmployeesWithDeduction();
			}
			ClearValues();
			FilterEmployees();
		}

		private void LoadDeductions()
		{
			collDeductions = dedServ.GetUsedDeductions();
			if (this.DeductionsChanged != null)
				this.DeductionsChanged();
		}

		private void ClearValues()
		{
			intUseDepartment = 1;
			intUseSequenceNumber = 1;
			intUseCode1 = 1;
			intUseCode2 = 1;
			intUseGroupID = 1;
			intUseFullTime = 1;
		}

		private void FillLists()
		{
			collDepartments.ClearList();
			collSequences.ClearList();
			collCodeOnes.ClearList();
			collCodeTwos.ClearList();
			collGroups.ClearList();
			collFullTime.ClearList();
			collDepartments = empServ.GetDepartmentsUsed();
			collDepartments.MoveFirst();
			collDepartments.InsertItemBefore("");
			collSequences = empServ.GetSequenceNumbersUsed();
			collSequences.MoveFirst();
			collSequences.InsertItemBefore("");
			collCodeOnes = empServ.GetCodeOnesUsed();
			collCodeOnes.MoveFirst();
			collCodeOnes.InsertItemBefore("");
			collCodeTwos = empServ.GetCodeTwosUsed();
			collCodeTwos.MoveFirst();
			collCodeTwos.InsertItemBefore("");
			collFullTime.AddItem("");
			collFullTime.AddItem("Full Time");
			collFullTime.AddItem("Part Time");
			collGroups = empServ.GetGroupIDsUsed();
			collGroups.MoveFirst();
			collGroups.InsertItemBefore("");
			FillAmountTypes();
			FillPercentTypes();
			FillLimitTypes();
			// FillStatuses
			FillFrequencies();
			if (this.AmountTypesChanged != null)
				this.AmountTypesChanged();
			if (this.LimitTypesChanged != null)
				this.LimitTypesChanged();
			if (this.PercentTypesChanged != null)
				this.PercentTypesChanged();
			if (this.FrequenciesChanged != null)
				this.FrequenciesChanged();
			if (this.DepartmentsChanged != null)
				this.DepartmentsChanged();
			if (this.SequencesChanged != null)
				this.SequencesChanged();
			if (this.CodeOnesChanged != null)
				this.CodeOnesChanged();
			if (this.CodeTwosChanged != null)
				this.CodeTwosChanged();
			if (this.FullTimesChanged != null)
				this.FullTimesChanged();
			if (this.GroupIDsChanged != null)
				this.GroupIDsChanged();
		}

		private void FilterEmployees()
		{
			collFilteredEmps.ClearList();
			collEmpsWithDed.MoveFirst();
			cEmployee empl;
			string strTemp = "";
			bool boolUse = false;
			int lngTemp = 0;
			string strEmp = "";
			while (collEmpsWithDed.IsCurrent())
			{
				strEmp = FCConvert.ToString(collEmpsWithDed.GetCurrentItem());
				if (collAllowedEmployees.ContainsKey(strEmp))
				{
					empl = (cEmployee)collAllowedEmployees[strEmp];
					boolUse = true;
					if (intUseCode1 > 1)
					{
						strTemp = FCConvert.ToString(collCodeOnes.GetItemByIndex(intUseCode1));
						if (fecherFoundation.Strings.Trim(empl.Code1) != fecherFoundation.Strings.Trim(strTemp))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseCode2 > 1)
					{
						strTemp = FCConvert.ToString(collCodeTwos.GetItemByIndex(intUseCode2));
						if (fecherFoundation.Strings.Trim(empl.Code2) != fecherFoundation.Strings.Trim(strTemp))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseDepartment > 1)
					{
						strTemp = FCConvert.ToString(collDepartments.GetItemByIndex(intUseDepartment));
						if (fecherFoundation.Strings.Trim(strTemp) != fecherFoundation.Strings.Trim(empl.Department))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseFullTime > 1)
					{
						if (intUseFullTime == 2)
						{
							// fulltime
							if (empl.IsPartTime)
							{
								boolUse = false;
							}
						}
						else
						{
							// parttime
							if (!empl.IsPartTime)
							{
								boolUse = false;
							}
						}
					}
					if (boolUse && intUseGroupID > 1)
					{
						strTemp = FCConvert.ToString(collGroups.GetItemByIndex(intUseGroupID));
						if (fecherFoundation.Strings.Trim(strTemp) != fecherFoundation.Strings.Trim(empl.GroupID))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseSequenceNumber > 1)
					{
						lngTemp = FCConvert.ToInt32(collSequences.GetItemByIndex(intUseSequenceNumber));
						if (lngTemp != empl.SequenceNumber)
						{
							boolUse = false;
						}
					}
					if (boolUse)
					{
						collFilteredEmps.AddItem(empl);
					}
				}
				collEmpsWithDed.MoveNext();
			}
			if (this.FilteredEmployeesChanged != null)
				this.FilteredEmployeesChanged();
		}

		private void FillAmountTypes()
		{
			collAmountTypes.ClearList();
			cCodeDescription cd;
			cd = new cCodeDescription();
			cd.ID = 1;
			cd.Code = "Dollars";
			cd.Description = "Dollars";
			collAmountTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 2;
			cd.Code = "Percent";
			cd.Description = "Percent";
			collAmountTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 3;
			cd.Code = "Levy";
			cd.Description = "Levy";
			collAmountTypes.AddItem(cd);
		}

		private void FillPercentTypes()
		{
			collPercentTypes.ClearList();
			cCodeDescription cd;
			cd = new cCodeDescription();
			cd.ID = 1;
			cd.Code = "Gross";
			cd.Description = "Gross";
			collPercentTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 2;
			cd.Code = "Net";
			cd.Description = "Net";
			collPercentTypes.AddItem(cd);
		}

		private void FillFrequencies()
		{
			collFrequencies = dedServ.GetFrequencyCodes();
			collFrequencies.MoveFirst();
			cCodeDescription cd = new cCodeDescription();
			cd.ID = 0;
			cd.Code = "";
			cd.Description = "";
			collFrequencies.InsertItemBefore(cd);
		}

		private void FillLimitTypes()
		{
			collLimitTypes = dedServ.GetDeductionLimits();
		}

		public void UpdateDeductions(bool boolUseAmount, double dblAmount, string strAmountType, string strPercentType, bool boolUseLimit, double dblLimit, string strLimitType, bool boolUseFrequency, int lngFrequency)
		{
			if (boolUseAmount)
			{
				if (strAmountType == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid amount type", null, null);
				}
				if (strAmountType == "Percent")
				{
					if (dblAmount < 0 || dblAmount > 100)
					{
						fecherFoundation.Information.Err().Raise(9999, "", "Invalid percent amount", null, null);
					}
					if (strPercentType == "")
					{
						fecherFoundation.Information.Err().Raise(9999, "", "Invalid percent type", null, null);
					}
				}
			}
			if (boolUseLimit)
			{
				if (strLimitType == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid limit type", null, null);
				}
			}
			collFilteredEmps.MoveFirst();
			cEmployee emp;
			clsDRWrapper rsSave = new clsDRWrapper();
			while (collFilteredEmps.IsCurrent())
			{
				emp = (cEmployee)collFilteredEmps.GetCurrentItem();
				rsSave.OpenRecordset("select * from tblEmployeeDeductions where employeenumber = '" + emp.EmployeeNumber + "' and deductioncode = " + FCConvert.ToString(lngCurrentDeduction), "Payroll");
				while (!rsSave.EndOfFile())
				{
					rsSave.Edit();
					if (boolUseAmount)
					{
						rsSave.Set_Fields("amount", dblAmount);
						rsSave.Set_Fields("amounttype", strAmountType);
						rsSave.Set_Fields("percenttype", strPercentType);
					}
					if (boolUseLimit)
					{
						rsSave.Set_Fields("limit", dblLimit);
						rsSave.Set_Fields("Limittype", strLimitType);
					}
					if (boolUseFrequency)
					{
						rsSave.Set_Fields("FrequencyCode", lngFrequency);
					}
					rsSave.Update();
					rsSave.MoveNext();
				}
				collFilteredEmps.MoveNext();
			}
		}
	}
}
