//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSimpleEmployeeInformationReport.
	/// </summary>
	public partial class rptSimpleEmployeeInformationReport : BaseSectionReport
	{
		public rptSimpleEmployeeInformationReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee Information Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSimpleEmployeeInformationReport InstancePtr
		{
			get
			{
				return (rptSimpleEmployeeInformationReport)Sys.GetInstance(typeof(rptSimpleEmployeeInformationReport));
			}
		}

		protected rptSimpleEmployeeInformationReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsProcessing?.Dispose();
				rsTax?.Dispose();
                rsProcessing = null;
                rsTax = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSimpleEmployeeInformationReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private object intEmployeeReport;
		private int intCounter;
		private int intpage;
		private string[] strEmployeeNumbers = null;
		private bool boolRange;
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		private clsDRWrapper rsTax = new clsDRWrapper();
		private bool boolEmpty;
		private bool boolDeductions;
		private bool boolEmpMatch;
		private bool boolDirectDeposit;
		private bool boolPayTotals;
		private bool boolVacSick;
		private bool boolMSRS;
		private bool boolDistribtuion;
		private double dblTaxTotal;
		private cGenericCollection employeeList;
        private SSNViewType ssnViewPermission = SSNViewType.None;
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			double dblTemp;
			try
            {
                int w4Year = 2019;
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				Next1:
				;
				// are there any more employee to process
				if (boolEmpty)
				{
					eArgs.EOF = true;
					return;
				}
				if (intCounter > Information.UBound(strEmployeeNumbers, 1))
				{
					eArgs.EOF = true;
					return;
				}
				modGlobalVariables.Statics.gstrEmployeeToGet = strEmployeeNumbers[intCounter];
				intCounter += 1;
				eArgs.EOF = false;
				rsProcessing.OpenRecordset("Select * From tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gstrEmployeeToGet + "'", "TWPY0000.vb1");
				if (rsProcessing.EndOfFile())
					goto Next1;
				txtEmployeeNumber.Text = rsProcessing.Get_Fields("EmployeeNumber");
				txtEmployeeName.Text = rsProcessing.Get_Fields_String("FirstName") + " " + rsProcessing.Get_Fields_String("MiddleName") + " " + rsProcessing.Get_Fields_String("LastName");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsProcessing.Get_Fields_String("SSN"))).Length == 9)
				{					
                    switch (ssnViewPermission)
                    {
                        case SSNViewType.Full:
                            txtSSN.Text = Strings.Left(FCConvert.ToString(rsProcessing.Get_Fields_String("SSN")), 3) + "-" + Strings.Mid(FCConvert.ToString(rsProcessing.Get_Fields_String("SSN")), 4, 2) + "-" + Strings.Right(FCConvert.ToString(rsProcessing.Get_Fields_String("SSN")), 4);
                            break;
                        case SSNViewType.Masked:
                            txtSSN.Text = "***-**-"  + "-" + rsProcessing.Get_Fields_String("SSN").Right(4);
                            break;
                        default:
                            txtSSN.Text = "***-**-****";
                            break;
                    }
				}
				else
                {
                    txtSSN.Text = "***-**-****";
					//txtSSN.Text = rsProcessing.Get_Fields_String("SSN");
				}

                var tempDate = rsProcessing.Get_Fields_DateTime("W4Date");
                if (tempDate == DateTime.FromOADate(0) || tempDate == DateTime.MinValue)
                {
                    txtW4Date.Text = "";
                    w4Year = 2019;
                }
                else
                {
                    w4Year = tempDate.Year;
                    txtW4Date.Text = tempDate.FormatAndPadShortDate();
                }

                if (w4Year < 2020)
                {
                    txtOtherDeduction.Text = "";
                    txtOtherIncome.Text = "";
                    lblOtherIncome.Visible = false;
                    lblOtherDeduction.Visible = false;
                }
                else
                {
                    lblOtherIncome.Visible = true;
                    lblOtherDeduction.Visible = true;
                    txtOtherIncome.Text =  rsProcessing.Get_Fields_Double("FederalOtherIncome").ToString("C");
                    txtOtherDeduction.Text = rsProcessing.Get_Fields_Double("AddFederalDeduction").ToString("C");
                }

				rsTax.OpenRecordset("SELECT * FROM tblPayStatuses WHERE ID = " + rsProcessing.Get_Fields_Int32("FedFilingStatusID"), "TWPY0000.vb1");
				if (!rsTax.EndOfFile())
				{
                    if (w4Year < 2020)
                    {
                        txtFederalStatus.Text = rsTax.Get_Fields("Description") + " - # Dep: " + rsProcessing.Get_Fields_Int32("FedStatus");
                    }
                    else
                    {
                        txtFederalStatus.Text = rsTax.Get_Fields("Description");
                    }
				}
				else
				{
					txtFederalStatus.Text = string.Empty;
				}
				rsTax.OpenRecordset("SELECT * FROM tblPayStatuses WHERE ID = " + rsProcessing.Get_Fields_Int32("StateFilingStatusID"), "TWPY0000.vb1");
				if (!rsTax.EndOfFile())
				{
					txtStateStatus.Text = rsTax.Get_Fields("Description") + " - # Dep: " + rsProcessing.Get_Fields_Int32("StateStatus");
				}
				else
				{
					txtStateStatus.Text = string.Empty;
				}
				rsTax.OpenRecordset("Select sum(distgrosspay) as TotalPay From tblCheckDetail where checkvoid = 0 and EmployeeNumber = '" + modGlobalVariables.Statics.gstrEmployeeToGet + "' and (year(paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ")", "TWPY0000.vb1");
				if (rsTax.EndOfFile())
				{
					txtTotalPay.Text = "$0.00";
				}
				else
				{
					txtTotalPay.Text = Strings.Format(rsTax.Get_Fields("TotalPay"), "$0.00");
				}
				txtFederalTax.Text = "0";
				txtFICATax.Text = "0";
				txtMedicareTax.Text = "0";
				txtStateTax.Text = "0";
				dblTaxTotal = 0;
				dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, modGlobalVariables.Statics.gstrEmployeeToGet, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtFederalTax.Text = Strings.Format(dblTemp, "$0.00");
				dblTaxTotal += dblTemp;
				dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, modGlobalVariables.Statics.gstrEmployeeToGet, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtFICATax.Text = Strings.Format(dblTemp, "$0.00");
				dblTaxTotal += dblTemp;
				dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, modGlobalVariables.Statics.gstrEmployeeToGet, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtMedicareTax.Text = Strings.Format(dblTemp, "$0.00");
				dblTaxTotal += dblTemp;
				dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, modGlobalVariables.Statics.gstrEmployeeToGet, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtStateTax.Text = Strings.Format(dblTemp, "$0.00");
				dblTaxTotal += dblTemp;
				txtTotalTax.Text = Strings.Format(dblTaxTotal, "$0.00");
				srptEmployeeDeductions.Report = new srptSimpleEmployeeDeductions();
				srptEmployeeDeductions.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
				srptBenefits.Report = new srptBenefits();
				srptBenefits.Report.UserData = modGlobalVariables.Statics.gstrEmployeeToGet;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_ReportEnd";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolEmpty)
				{
				}
				else
				{
					if (boolDeductions)
					{
						srptEmployeeDeductions.Report.Unload();
					}
					srptEmployeeDeductions.Report = null;
				}
				//if (boolBenefits) {
				//	srptBenefits.Report.Unload();
				//}
				srptBenefits.Report = null;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_ReportStart";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter;
				//clsDRWrapper rsData = new clsDRWrapper();
				string strSQL = "";
				string strOrderBy = "";
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				rsProcessing.DefaultDB = "TWPY0000.vb1";
				
				if (employeeList.ItemCount() < 1)
				{
					boolEmpty = true;
				}
				else
				{
					boolEmpty = false;
					strEmployeeNumbers = new string[employeeList.ItemCount() - 1 + 1];
					cEmployee emp;
					for (intCounter = 0; intCounter <= employeeList.ItemCount() - 1; intCounter++)
					{
						emp = (cEmployee)employeeList.GetItemByIndex(intCounter + 1);
						strEmployeeNumbers[intCounter] = emp.EmployeeNumber;
					}
				}
				intCounter = 0;
				intpage = 0;
				lblHeader.Tag = "EMPLOYEE DATA";

                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        break;
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		public void Init(cGenericCollection empList)
		{
			employeeList = empList;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: false);
		}

		
	}
}
