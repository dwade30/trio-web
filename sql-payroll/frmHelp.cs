//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmHelp : BaseForm
	{
		public frmHelp()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmHelp InstancePtr
		{
			get
			{
				return (frmHelp)Sys.GetInstance(typeof(frmHelp));
			}
		}

		protected frmHelp _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 26,2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		// Private dbHelp    As DAO.Database
		private clsDRWrapper rsHelp = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdQuit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmHelp_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmHelp_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyUp";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (KeyCode == Keys.Escape)
				{
					KeyCode = (Keys)0;
					Close();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmHelp_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmHelp properties;
			//frmHelp.ScaleWidth	= 6585;
			//frmHelp.ScaleHeight	= 5325;
			//frmHelp.LinkTopic	= "Form2";
			//frmHelp.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, FCConvert.ToInt16(true));
				modGlobalFunctions.SetTRIOColors(this);
				// clear out the listbox
				vsData.Clear();
				rsHelp.DefaultDB = "TWPY0000.vb1";
				// open the forms global database connection
				// Set dbHelp = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsHelp.OpenRecordset("Select * from tblHelp where FieldName = '" + modGlobalVariables.Statics.gstrHelpFieldName + "'", "TWPY0000.vb1");
				if (!rsHelp.EndOfFile())
				{
					txtDescription.Text = fecherFoundation.Strings.Trim(rsHelp.Get_Fields("Description") + " ");
				}
				if (modGlobalVariables.Statics.gstrHelpFieldName == "DEDUCTIONCODE")
				{
					SetDeductionCodeGrid();
				}
				else if (modGlobalVariables.Statics.gstrHelpFieldName == "FREQUENCYCODE")
				{
					SetFrequencyCodeGrid();
				}
				else if (modGlobalVariables.Statics.gstrHelpFieldName == "TAXSTATUSCODE")
				{
					SetTaxStatusCodeGrid();
				}
				else if (modGlobalVariables.Statics.gstrHelpFieldName == "AMOUNTTYPE")
				{
					SetAmountTypeGrid();
				}
				else if (modGlobalVariables.Statics.gstrHelpFieldName == "LIMITCODE")
				{
					SetLimitCodeGrid();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetDeductionCodeGrid()
		{
			rsHelp.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
			if (!rsHelp.EndOfFile())
			{
				rsHelp.MoveLast();
				rsHelp.MoveFirst();
				lblTitle.Text = "Deduction Codes";
				// set grid properties
				vsData.WordWrap = true;
				vsData.Cols = 3;
				vsData.ExtendLastCol = true;
				// set column 0 properties
				vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(0, 400);
				// set column 1 properties
				vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(1, 600);
				vsData.TextMatrix(0, 1, "ID");
				// set column 2 properties
				vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(2, 2000);
				vsData.TextMatrix(0, 2, "Description");
				// fill the grid
				for (intCounter = 1; intCounter <= (rsHelp.RecordCount()); intCounter++)
				{
					vsData.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
					vsData.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsHelp.Get_Fields_Int32("DeductionNumber") + " "));
					vsData.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsHelp.Get_Fields("Description") + " "));
					rsHelp.MoveNext();
				}
			}
		}

		public void SetFrequencyCodeGrid()
		{
			rsHelp.OpenRecordset("Select * from tblFrequencyCodes order by ID", "TWPY0000.vb1");
			if (!rsHelp.EndOfFile())
			{
				rsHelp.MoveLast();
				rsHelp.MoveFirst();
				lblTitle.Text = "Frequency Codes";
				// set grid properties
				vsData.WordWrap = true;
				vsData.Cols = 3;
				vsData.ExtendLastCol = true;
				// set column 0 properties
				vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(0, 400);
				// set column 1 properties
				vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(1, 600);
				vsData.TextMatrix(0, 1, "Code");
				// set column 2 properties
				vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(2, 2000);
				vsData.TextMatrix(0, 2, "Description");
				// fill the grid
				for (intCounter = 1; intCounter <= (rsHelp.RecordCount()); intCounter++)
				{
					vsData.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
					vsData.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsHelp.Get_Fields("FrequencyCode") + " "));
					vsData.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsHelp.Get_Fields("Description") + " "));
					rsHelp.MoveNext();
				}
			}
		}

		public void SetTaxStatusCodeGrid()
		{
			rsHelp.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
			if (!rsHelp.EndOfFile())
			{
				rsHelp.MoveLast();
				rsHelp.MoveFirst();
				lblTitle.Text = "Tax Status Codes";
				// set grid properties
				vsData.WordWrap = true;
				vsData.Cols = 7;
				vsData.ExtendLastCol = true;
				// set column 0 properties
				vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(0, 400);
				// set column 1 properties
				vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(1, 600);
				vsData.TextMatrix(0, 1, "Code");
				// set column 2 properties
				vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(2, 2300);
				vsData.TextMatrix(0, 2, "Description");
				// set column 3 properties
				vsData.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(3, 600);
				vsData.TextMatrix(0, 3, "FED");
				vsData.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
				// set column 4 properties
				vsData.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(4, 600);
				vsData.TextMatrix(0, 4, "FICA");
				vsData.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
				// set column 5 properties
				vsData.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(5, 600);
				vsData.TextMatrix(0, 5, "MUNI");
				vsData.ColDataType(5, FCGrid.DataTypeSettings.flexDTBoolean);
				// set column 6 properties
				vsData.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.ColWidth(6, 600);
				vsData.TextMatrix(0, 6, "STATE");
				vsData.ColDataType(6, FCGrid.DataTypeSettings.flexDTBoolean);
				// set the number of rows in the grid
				vsData.Rows = rsHelp.RecordCount() + 1;
				// fill the grid
				for (intCounter = 1; intCounter <= (rsHelp.RecordCount()); intCounter++)
				{
					vsData.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
					vsData.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsHelp.Get_Fields("TaxStatusCode") + " "));
					vsData.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsHelp.Get_Fields("Description") + " "));
					vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsHelp.Get_Fields_Boolean("FedExempt")));
					vsData.TextMatrix(intCounter, 4, FCConvert.ToString(rsHelp.Get_Fields_Boolean("FicaExempt")));
					vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsHelp.Get_Fields_Boolean("MedicareExempt")));
					vsData.TextMatrix(intCounter, 6, FCConvert.ToString(rsHelp.Get_Fields_Boolean("StateExempt")));
					rsHelp.MoveNext();
				}
			}
		}

		public void SetAmountTypeGrid()
		{
			lblTitle.Text = "Amount Types";
			// set grid properties
			vsData.WordWrap = true;
			vsData.Cols = 2;
			vsData.ExtendLastCol = true;
			// set column 0 properties
			vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColWidth(0, 400);
			// set column 1 properties
			vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColWidth(1, 1500);
			vsData.TextMatrix(0, 1, "Description");
			// fill the grid
			vsData.TextMatrix(1, 0, FCConvert.ToString(1));
			vsData.TextMatrix(1, 1, "Dollar");
			vsData.TextMatrix(2, 0, FCConvert.ToString(2));
			vsData.TextMatrix(2, 1, "Percent");
		}

		public void SetLimitCodeGrid()
		{
			lblTitle.Text = "Limit Codes";
			// set grid properties
			vsData.WordWrap = true;
			vsData.Cols = 2;
			vsData.ExtendLastCol = true;
			// set column 0 properties
			vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColWidth(0, 400);
			// set column 1 properties
			vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColWidth(1, 1500);
			vsData.TextMatrix(0, 1, "Description");
			// fill the grid
			vsData.TextMatrix(1, 0, FCConvert.ToString(1));
			vsData.TextMatrix(1, 1, "Life");
			vsData.TextMatrix(2, 0, FCConvert.ToString(2));
			vsData.TextMatrix(2, 1, "Calendar");
			vsData.TextMatrix(3, 0, FCConvert.ToString(3));
			vsData.TextMatrix(3, 1, "Fiscal");
			vsData.TextMatrix(4, 0, FCConvert.ToString(4));
			vsData.TextMatrix(4, 1, "Pay-Period");
		}
	}
}
