﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class clsACHBatch
	{
		//=========================================================
		private clsACHBatchHeader tBatchHeader = new clsACHBatchHeader();
		private clsACHBatchControlRecord tBatchControlRecord = new clsACHBatchControlRecord();
		private FCCollection tDets = new FCCollection();
		private FCCollection tOffsets = new FCCollection();
		private int intBlocks;
		private int intBatchNumber;

		public void AddOffset(ref clsACHOffsetRecord tRec)
		{
			tOffsets.Add(tRec);
		}

		public void AddDetail(ref clsACHDetailRecord tRec)
		{
			tDets.Add(tRec);
		}

		public FCCollection Offsets()
		{
			return tOffsets;
		}

		public FCCollection Details()
		{
			return tDets;
		}

		public int BatchNumber
		{
			get
			{
				int BatchNumber = 0;
				BatchNumber = intBatchNumber;
				return BatchNumber;
			}
			set
			{
				intBatchNumber = value;
			}
		}

		public double Debits
		{
			get
			{
				double Debits = 0;
				Debits = tBatchControlRecord.TotalDebit;
				return Debits;
			}
		}

		public double Credits
		{
			get
			{
				double Credits = 0;
				Credits = tBatchControlRecord.TotalCredit;
				return Credits;
			}
		}

		public double Hash
		{
			get
			{
				double Hash = 0;
				Hash = tBatchControlRecord.Hash;
				return Hash;
			}
		}

		public int BlockRecords
		{
			get
			{
				int BlockRecords = 0;
				BlockRecords = intBlocks;
				return BlockRecords;
			}
			set
			{
				intBlocks = value;
			}
		}

		public clsACHBatchHeader BatchHeader()
		{
			clsACHBatchHeader BatchHeader = null;
			BatchHeader = tBatchHeader;
			return BatchHeader;
		}

		public clsACHBatchControlRecord BatchControl()
		{
			clsACHBatchControlRecord BatchControl = null;
			BatchControl = tBatchControlRecord;
			return BatchControl;
		}

		public double CalcTotalCredits()
		{
			double CalcTotalCredits = 0;
			double dblTotalCredits;
			dblTotalCredits = 0;
			foreach (clsACHDetailRecord tRec in tDets)
			{
				dblTotalCredits += tRec.TotalAmount;
			}
			// tRec
			CalcTotalCredits = dblTotalCredits;
			return CalcTotalCredits;
		}

		public void BuildBatchControlRecord()
		{
			int intRecCount = 0;
			double dblHashNumber = 0;
			double dblTotalDebits = 0;
			double dblTotalCredits = 0;
			intBlocks = 2;
			foreach (clsACHDetailRecord tRec in tDets)
			{
				intRecCount += 1;
				tRec.RecordNumber = intRecCount;
				dblHashNumber += tRec.HashNumber;
				dblTotalCredits += tRec.TotalAmount;
			}
			// tRec
			foreach (clsACHOffsetRecord tOff in tOffsets)
			{
				intRecCount += 1;
				tOff.RecordNumber = intRecCount;
				dblHashNumber += tOff.HashNumber;
				dblTotalDebits += tOff.TotalAmount;
			}
			// tOff
			intBlocks += intRecCount;
			tBatchControlRecord.Hash = dblHashNumber;
			tBatchControlRecord.EntryAddendaCount = intRecCount;
			tBatchControlRecord.TotalCredit = dblTotalCredits;
			tBatchControlRecord.TotalDebit = dblTotalDebits;
			tBatchControlRecord.BatchNumber = intBatchNumber;
		}
	}
}
