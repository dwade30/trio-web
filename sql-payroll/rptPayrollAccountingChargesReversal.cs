//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayrollAccountingChargesReversal.
	/// </summary>
	public partial class rptPayrollAccountingChargesReversal : BaseSectionReport
	{
		public rptPayrollAccountingChargesReversal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounting Charges Reversal";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayrollAccountingChargesReversal InstancePtr
		{
			get
			{
				return (rptPayrollAccountingChargesReversal)Sys.GetInstance(typeof(rptPayrollAccountingChargesReversal));
			}
		}

		protected rptPayrollAccountingChargesReversal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayrollAccountingChargesReversal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curEmployeeTotal As Decimal	OnWrite(int, Decimal)
		Decimal curEmployeeTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		bool blnSummaryPage;
		public DateTime datPayDate;
		// vbPorter upgrade warning: intPayRunID As int	OnWriteFCConvert.ToInt32(
		public int intPayRunID;
		public string strEmployeeNumber = "";
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		public bool blnShowBadAccountLabel;
		public int lngCheckNumber;
		public bool boolPreviousFiscal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("EmployeeBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsEmployeeInfo.MoveNext();
				eArgs.EOF = rsEmployeeInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["EmployeeBinder"].Value = rsEmployeeInfo.Get_Fields("EmployeeNumber");
			}
			else
			{
				blnSummaryPage = true;
			}
			if (eArgs.EOF)
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			clsDRWrapper rsMaster = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnShowBadAccountLabel = false;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy");
			lblCheckReturn.Text = "Check Return: " + FCConvert.ToString(frmCheckReturn.InstancePtr.lngCheckNumber);
			blnFirstRecord = true;
			curEmployeeTotal = 0;
			curFinalTotal = 0;
			blnSummaryPage = false;
			rsMaster.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
			if (rsMaster.EndOfFile())
			{
				rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, tblEmployeeMaster.MiddleName AS MiddleName, tblCheckDetail.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistAccountNumber as DistAccountNumber, SUM(tblCheckDetail.DistGrossPay) as TotalPay FROM (tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CheckVoid = 1 AND tblCheckDetail.EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName, tblCheckDetail.EmployeeNumber, DistAccountNumber HAVING SUM(tblCheckDetail.DistGrossPay) <> 0 ORDER BY tblCheckDetail.EmployeeNumber");
			}
			else
			{
				switch (rsMaster.Get_Fields_Int32("DataEntrySequence"))
				{
					case 0:
						{
							rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, tblEmployeeMaster.MiddleName AS MiddleName, tblCheckDetail.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistAccountNumber as DistAccountNumber, SUM(tblCheckDetail.DistGrossPay) as TotalPay FROM (tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CheckVoid = 1 AND tblCheckDetail.EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName, tblCheckDetail.EmployeeNumber, DistAccountNumber HAVING SUM(tblCheckDetail.DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							break;
						}
					case 1:
						{
							rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, tblEmployeeMaster.MiddleName AS MiddleName, tblCheckDetail.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistAccountNumber as DistAccountNumber, SUM(tblCheckDetail.DistGrossPay) as TotalPay FROM (tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CheckVoid = 1 AND tblCheckDetail.EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName, tblCheckDetail.EmployeeNumber, DistAccountNumber HAVING SUM(tblCheckDetail.DistGrossPay) <> 0 ORDER BY tblCheckDetail.EmployeeNumber");
							break;
						}
					case 2:
						{
							rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, tblEmployeeMaster.MiddleName AS MiddleName, tblCheckDetail.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistAccountNumber as DistAccountNumber, SUM(tblCheckDetail.DistGrossPay) as TotalPay FROM (tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CheckVoid = 1 AND tblCheckDetail.EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName, tblCheckDetail.EmployeeNumber, DistAccountNumber HAVING SUM(tblCheckDetail.DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							break;
						}
					case 3:
						{
							rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.DeptDiv as DeptDiv, tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, tblEmployeeMaster.MiddleName AS MiddleName, tblCheckDetail.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistAccountNumber as DistAccountNumber, SUM(tblCheckDetail.DistGrossPay) as TotalPay FROM (tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " AND CheckVoid = 1 AND tblCheckDetail.EmployeeNumber = '" + strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName, tblCheckDetail.EmployeeNumber, DistAccountNumber HAVING SUM(tblCheckDetail.DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							break;
						}
				}
				//end switch
			}
			rsMaster.Dispose();
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Accounting charges reversed for this check.", "No information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldAccount As object	OnWrite(string, object)
			fldAmount.Text = Strings.Format(Conversion.Val(rsEmployeeInfo.Get_Fields("TotalPay")) * -1, "#,##0.00");
			curEmployeeTotal -= FCConvert.ToDecimal(Conversion.Val(rsEmployeeInfo.Get_Fields("TotalPay")));
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			if (!modValidateAccount.AccountValidate(rsEmployeeInfo.Get_Fields_String("DistAccountNumber")))
			{
				blnShowBadAccountLabel = true;
				fldAccount.Text = "**" + rsEmployeeInfo.Get_Fields_String("DistAccountNumber");
			}
			else
			{
				fldAccount.Text = rsEmployeeInfo.Get_Fields_String("DistAccountNumber");
			}
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalTotal As object	OnWrite(string)
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
			curFinalTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployeeTotal As object	OnWrite(string)
			fldEmployeeTotal.Text = Strings.Format(curEmployeeTotal, "#,##0.00");
			curFinalTotal += curEmployeeTotal;
			curEmployeeTotal = 0;
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: SubReport1 As object	OnWrite(srptAccountingSummaryReversal)
			Subreport1.Report = new srptAccountingSummaryReversal();
		}

		private void GroupFooter4_Format(object sender, EventArgs e)
		{
			if (blnShowBadAccountLabel == true)
			{
				lblBadAccount.Visible = true;
			}
			else
			{
				lblBadAccount.Visible = false;
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployee As object	OnWrite(string)
			fldEmployee.Text = Strings.Format(rsEmployeeInfo.Get_Fields("EmployeeNumber"), "00") + "  " + rsEmployeeInfo.Get_Fields_String("LastName") + ", " + rsEmployeeInfo.Get_Fields_String("FirstName") + " " + rsEmployeeInfo.Get_Fields_String("MiddleName");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (blnSummaryPage)
			{
				Line2.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
			}
		}
		// vbPorter upgrade warning: intReversalPayRunID As int	OnRead
		public void Init(ref string strEmployee, ref DateTime datReversalPayDate, ref int intReversalPayRunID, int lngCheckNumberToVoid, bool boolFromPreviousFiscalYear)
		{
			lngCheckNumber = lngCheckNumberToVoid;
			modGlobalVariables.Statics.glngCheckNumberToVoid = lngCheckNumber;
			boolPreviousFiscal = boolFromPreviousFiscalYear;
			datPayDate = datReversalPayDate;
			intPayRunID = intReversalPayRunID;
			strEmployeeNumber = strEmployee;
            // Me.Show vbModal, MDIParent
            //FC:FINAL:SBE - #i2247 - show report viewer as modal dialog (opened from another modal dialog)
            //frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "AccountingChargesReversal");
            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "AccountingChargesReversal", showModal: true);
        }

		
	}
}
