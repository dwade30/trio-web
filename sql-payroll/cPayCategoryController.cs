//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cPayCategoryController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public Dictionary<object, object> GetPayCategoriesAsDictionary()
		{
			Dictionary<object, object> GetPayCategoriesAsDictionary = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				Dictionary<object, object> paycats = new Dictionary<object, object>();
				rsLoad.OpenRecordset("select * from tblPayCategories order by CategoryNumber", "Payroll");
				cPayCategory tempCat;
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					tempCat = new cPayCategory();
					tempCat.CategoryNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CategoryNumber"))));
					tempCat.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
					tempCat.ExemptCode1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("ExemptCode1"))));
					tempCat.ExemptCode2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("ExemptCode2"))));
					tempCat.ExemptCode3 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ExemptCode3"))));
					tempCat.ExemptCode4 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ExemptCode4"))));
					tempCat.ExemptCode5 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ExemptCode5"))));
					tempCat.ExemptType1 = FCConvert.ToString(rsLoad.Get_Fields_String("ExemptType1"));
					tempCat.ExemptType2 = FCConvert.ToString(rsLoad.Get_Fields_String("ExemptType2"));
					tempCat.ExemptType3 = FCConvert.ToString(rsLoad.Get_Fields_String("ExemptType3"));
					tempCat.ExemptType4 = FCConvert.ToString(rsLoad.Get_Fields_String("ExemptType4"));
					tempCat.ExemptType5 = FCConvert.ToString(rsLoad.Get_Fields_String("ExemptType5"));
					tempCat.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					tempCat.LastUpdate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("LastUpdate"));
					tempCat.LastUserID = FCConvert.ToString(rsLoad.Get_Fields("LastUserID"));
					tempCat.MSR = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("MSR"));
					tempCat.Multiplier = Conversion.Val(rsLoad.Get_Fields_Double("Multi"));
					tempCat.TaxStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("TaxStatus"))));
					tempCat.Unemployment = FCConvert.ToBoolean(rsLoad.Get_Fields("Unemployment"));
					tempCat.UnitType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					tempCat.WorkersComp = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("WorkersComp"));
					paycats.Add(tempCat.ID, tempCat);
					rsLoad.MoveNext();
				}
				GetPayCategoriesAsDictionary = paycats;
				return GetPayCategoriesAsDictionary;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetPayCategoriesAsDictionary;
		}
	}
}
