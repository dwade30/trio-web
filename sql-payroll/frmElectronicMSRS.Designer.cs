//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmElectronicMSRS.
	/// </summary>
	partial class frmElectronicMSRS
	{
		public fecherFoundation.FCComboBox cmbFilingType;
		public fecherFoundation.FCComboBox cmbPayrollCycle;
		public fecherFoundation.FCTextBox txtSubmissionNumber;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtEmployerCode;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtTransmitterCode;
		public Global.T2KPhoneNumberBox t2kPhone;
		public fecherFoundation.FCTextBox txtContactPerson;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtTransmitterName;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label9;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbFilingType = new fecherFoundation.FCComboBox();
            this.cmbPayrollCycle = new fecherFoundation.FCComboBox();
            this.txtSubmissionNumber = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtEmployerCode = new fecherFoundation.FCTextBox();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtTransmitterCode = new fecherFoundation.FCTextBox();
            this.t2kPhone = new Global.T2KPhoneNumberBox();
            this.txtContactPerson = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtTransmitterName = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(993, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbFilingType);
            this.ClientArea.Controls.Add(this.cmbPayrollCycle);
            this.ClientArea.Controls.Add(this.txtSubmissionNumber);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(993, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(993, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(262, 30);
            this.HeaderText.Text = "Electronic MainePERS";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbFilingType
            // 
            this.cmbFilingType.AutoSize = false;
            this.cmbFilingType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFilingType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFilingType.FormattingEnabled = true;
            this.cmbFilingType.Location = new System.Drawing.Point(776, 30);
            this.cmbFilingType.Name = "cmbFilingType";
            this.cmbFilingType.Size = new System.Drawing.Size(180, 40);
            this.cmbFilingType.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbFilingType, null);
            // 
            // cmbPayrollCycle
            // 
            this.cmbPayrollCycle.AutoSize = false;
            this.cmbPayrollCycle.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayrollCycle.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPayrollCycle.FormattingEnabled = true;
            this.cmbPayrollCycle.Location = new System.Drawing.Point(430, 30);
            this.cmbPayrollCycle.Name = "cmbPayrollCycle";
            this.cmbPayrollCycle.Size = new System.Drawing.Size(180, 40);
            this.cmbPayrollCycle.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbPayrollCycle, null);
            // 
            // txtSubmissionNumber
            // 
            this.txtSubmissionNumber.AutoSize = false;
            this.txtSubmissionNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSubmissionNumber.LinkItem = null;
            this.txtSubmissionNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSubmissionNumber.LinkTopic = null;
            this.txtSubmissionNumber.Location = new System.Drawing.Point(180, 30);
            this.txtSubmissionNumber.Name = "txtSubmissionNumber";
            this.txtSubmissionNumber.Size = new System.Drawing.Size(60, 40);
            this.txtSubmissionNumber.TabIndex = 0;
            this.txtSubmissionNumber.Text = "1";
            this.ToolTip1.SetToolTip(this.txtSubmissionNumber, "The # of submissions of this file for this month");
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtEmployerCode);
            this.Frame2.Controls.Add(this.txtEmployerName);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Location = new System.Drawing.Point(30, 440);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(499, 150);
            this.Frame2.TabIndex = 11;
            this.Frame2.Text = "Employer Information";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtEmployerCode
            // 
			this.txtEmployerCode.MaxLength = 6;
            this.txtEmployerCode.AutoSize = false;
            this.txtEmployerCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerCode.LinkItem = null;
            this.txtEmployerCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployerCode.LinkTopic = null;
            this.txtEmployerCode.Location = new System.Drawing.Point(195, 30);
            this.txtEmployerCode.Name = "txtEmployerCode";
            this.txtEmployerCode.Size = new System.Drawing.Size(155, 40);
            this.txtEmployerCode.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtEmployerCode, null);
            // 
            // txtEmployerName
            // 
			this.txtEmployerName.MaxLength = 20;
            this.txtEmployerName.AutoSize = false;
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.LinkItem = null;
            this.txtEmployerName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployerName.LinkTopic = null;
            this.txtEmployerName.Location = new System.Drawing.Point(195, 90);
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(284, 40);
            this.txtEmployerName.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtEmployerName, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(46, 17);
            this.Label3.TabIndex = 15;
            this.Label3.Text = "CODE";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(47, 17);
            this.Label2.TabIndex = 14;
            this.Label2.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtTransmitterCode);
            this.Frame1.Controls.Add(this.t2kPhone);
            this.Frame1.Controls.Add(this.txtContactPerson);
            this.Frame1.Controls.Add(this.txtAddress);
            this.Frame1.Controls.Add(this.txtTransmitterName);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Location = new System.Drawing.Point(30, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(499, 330);
            this.Frame1.TabIndex = 10;
            this.Frame1.Text = "Transmitter Information";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // txtTransmitterCode
            // 
			this.txtTransmitterCode.MaxLength = 5;
            this.txtTransmitterCode.AutoSize = false;
            this.txtTransmitterCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransmitterCode.LinkItem = null;
            this.txtTransmitterCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTransmitterCode.LinkTopic = null;
            this.txtTransmitterCode.Location = new System.Drawing.Point(195, 30);
            this.txtTransmitterCode.Name = "txtTransmitterCode";
            this.txtTransmitterCode.Size = new System.Drawing.Size(155, 40);
            this.txtTransmitterCode.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtTransmitterCode, null);
            // 
            // t2kPhone
            // 
            this.t2kPhone.Location = new System.Drawing.Point(195, 270);
            this.t2kPhone.Mask = "(999)000-0000";
            this.t2kPhone.Name = "t2kPhone";
            this.t2kPhone.Size = new System.Drawing.Size(155, 40);
            this.t2kPhone.TabIndex = 7;
            this.t2kPhone.Text = "(   )    -";
            this.ToolTip1.SetToolTip(this.t2kPhone, null);
            // 
            // txtContactPerson
            // 
			this.txtContactPerson.MaxLength = 20;
            this.txtContactPerson.AutoSize = false;
            this.txtContactPerson.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactPerson.LinkItem = null;
            this.txtContactPerson.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactPerson.LinkTopic = null;
            this.txtContactPerson.Location = new System.Drawing.Point(195, 210);
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(284, 40);
            this.txtContactPerson.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtContactPerson, null);
            // 
            // txtAddress
            // 
			this.txtAddress.MaxLength = 50;
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.LinkItem = null;
            this.txtAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress.LinkTopic = null;
            this.txtAddress.Location = new System.Drawing.Point(195, 150);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(284, 40);
            this.txtAddress.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtAddress, null);
            // 
            // txtTransmitterName
            // 
			this.txtTransmitterName.MaxLength = 20;
            this.txtTransmitterName.AutoSize = false;
            this.txtTransmitterName.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransmitterName.LinkItem = null;
            this.txtTransmitterName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTransmitterName.LinkTopic = null;
            this.txtTransmitterName.Location = new System.Drawing.Point(195, 90);
            this.txtTransmitterName.Name = "txtTransmitterName";
            this.txtTransmitterName.Size = new System.Drawing.Size(284, 40);
            this.txtTransmitterName.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtTransmitterName, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(46, 15);
            this.Label8.TabIndex = 20;
            this.Label8.Text = "CODE";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 284);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(90, 15);
            this.Label7.TabIndex = 19;
            this.Label7.Text = "TELEPHONE";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 224);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(133, 15);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "CONTACT PERSON";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 164);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(74, 15);
            this.Label5.TabIndex = 17;
            this.Label5.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 104);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(47, 15);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(640, 44);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(74, 15);
            this.Label10.TabIndex = 21;
            this.Label10.Text = "FILING TYPE";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(270, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 15);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "PAYROLL CYCLE";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(90, 15);
            this.Label9.TabIndex = 12;
            this.Label9.Text = "SUBMISSION #";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(442, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(82, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmElectronicMSRS
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(993, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmElectronicMSRS";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Electronic MainePERS";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmElectronicMSRS_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmElectronicMSRS_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
    }
}
