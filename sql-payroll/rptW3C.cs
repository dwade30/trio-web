//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3C.
	/// </summary>
	public partial class rptW3C : BaseSectionReport
	{
		public rptW3C()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W-3C";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW3C InstancePtr
		{
			get
			{
				return (rptW3C)Sys.GetInstance(typeof(rptW3C));
			}
		}

		protected rptW3C _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW3C	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsW2C clsW2CInfo;
		double dblLaserLineAdjustment;
		double dblHorizAdjust;
		bool boolPrintTest;
		// Dim intRecCount As Integer
		public void Init(ref clsW2C clsW2CSummary, bool modalDialog, bool boolTestPrint = false)
		{
			boolPrintTest = boolTestPrint;
			clsW2CInfo = clsW2CSummary;
			// dblLaserLineAdjustment = dblAdjustment
			// dblHorizAdjust = 120 * dblHorizAdjustment
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, false, showModal: modalDialog);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                string[] straddr = new string[5 + 1];
                string strSQL = "";
                int X = 0;
                // intRecCount = 0
                // If Not boolPrintTest Then
                dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
                dblHorizAdjust = FCConvert.ToDouble(modGlobalRoutines.SetW2HorizAdjustment(this));
                // End If
                foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
                {
                    ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
                    ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
                }

                if (!boolPrintTest)
                {
                    strSQL = "select * from tblw2master";
                    rsTemp.OpenRecordset(strSQL, "twpy0000.vb1");
                    FCUtils.EraseSafe(straddr);
                    if (!rsTemp.EndOfFile())
                    {
                        X = 1;
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("employersname"))) !=
                            string.Empty)
                        {
                            straddr[X] = FCConvert.ToString(rsTemp.Get_Fields("employersname"));
                            X += 1;
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("address1"))) !=
                            string.Empty)
                        {
                            straddr[X] = FCConvert.ToString(rsTemp.Get_Fields("address1"));
                            X += 1;
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("address2"))) !=
                            string.Empty)
                        {
                            straddr[X] = FCConvert.ToString(rsTemp.Get_Fields("address2"));
                            X += 1;
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("city"))) !=
                            string.Empty || FCConvert.ToString(rsTemp.Get_Fields("state")) != string.Empty)
                        {
                            straddr[X] = fecherFoundation.Strings.Trim(
                                fecherFoundation.Strings.Trim(
                                    rsTemp.Get_Fields("city") + "  " + rsTemp.Get_Fields("state")) + " " +
                                rsTemp.Get_Fields_String("zip"));
                            X += 1;
                        }

                        txtEmployerName.Text = straddr[1];
                        txtEmployerAddress1.Text = straddr[2];
                        txtEmployerAddress2.Text = straddr[3];
                        txtEmployerAddress3.Text = straddr[4];
                        // txtEmployersFedEIN.Text = rsTemp.Fields("employersfederalid")
                    }

                    txtYear.Text = FCConvert.ToString(clsW2CInfo.TaxYear);
                    if (clsW2CInfo.CorrectingW2C)
                    {
                        txtW2Type.Text = "2C";
                    }
                    else
                    {
                        txtW2Type.Text = "2";
                    }

                    chk941.Visible = false;
                    chkCT1.Visible = false;
                    chkMilitary.Visible = false;
                    chkHshld.Visible = false;
                    chk943.Visible = false;
                    chkMed.Visible = false;
                    chkThirdParty.Visible = false;
                    chk944.Visible = false;
                    switch (clsW2CInfo.TypeOfPayer)
                    {
                        case 0:
                        {
                            // 941
                            chk941.Visible = true;
                            break;
                        }
                        case 1:
                        {
                            // ct1
                            chkCT1.Visible = true;
                            break;
                        }
                        case 2:
                        {
                            // military
                            chkMilitary.Visible = true;
                            break;
                        }
                        case 3:
                        {
                            // household
                            chkHshld.Visible = true;
                            break;
                        }
                        case 4:
                        {
                            // 943
                            chk943.Visible = true;
                            break;
                        }
                        case 5:
                        {
                            // mqge
                            chkMed.Visible = true;
                            break;
                        }
                        case 6:
                        {
                            // 3rd party
                            chkThirdParty.Visible = true;
                            break;
                        }
                        case 7:
                        {
                            // 944
                            chk944.Visible = true;
                            break;
                        }
                    }

                    //end switch
                    // txtEmployerName.Text = .EmployerName
                    if (clsW2CInfo.PrevTotalFedWage > 0 || clsW2CInfo.TotalFederalWage > 0)
                    {
                        txtPrevWages.Text = Strings.Format(clsW2CInfo.PrevTotalFedWage, "0.00");
                        txtCurrWages.Text = Strings.Format(clsW2CInfo.TotalFederalWage, "0.00");
                    }

                    if (clsW2CInfo.PrevFicaWages > 0 || clsW2CInfo.CurrFicaWages > 0)
                    {
                        txtPrevSSWages.Text = Strings.Format(clsW2CInfo.PrevFicaWages, "0.00");
                        txtCurrSSWages.Text = Strings.Format(clsW2CInfo.CurrFicaWages, "0.00");
                    }

                    if (clsW2CInfo.PrevMedWages > 0 || clsW2CInfo.CurrMedWages > 0)
                    {
                        txtPrevMedicare.Text = Strings.Format(clsW2CInfo.PrevMedWages, "0.00");
                        txtCurrMedicare.Text = Strings.Format(clsW2CInfo.CurrMedWages, "0.00");
                    }

                    if (clsW2CInfo.PrevSSTips > 0 || clsW2CInfo.CurrSSTips > 0)
                    {
                        txtPrevSSTips.Text = Strings.Format(clsW2CInfo.PrevSSTips, "0.00");
                        txtCurrSSTips.Text = Strings.Format(clsW2CInfo.CurrSSTips, "0.00");
                    }

                    if (clsW2CInfo.PrevNonQualified > 0 || clsW2CInfo.CurrNonQualified > 0)
                    {
                        txtPrevNonQualifiedPlans.Text = Strings.Format(clsW2CInfo.PrevNonQualified, "0.00");
                        txtCurrNonQualifiedPlans.Text = Strings.Format(clsW2CInfo.CurrNonQualified, "0.00");
                    }

                    if (clsW2CInfo.PrevFedTax > 0 || clsW2CInfo.CurrFedTax > 0)
                    {
                        txtPrevFedTax.Text = Strings.Format(clsW2CInfo.PrevFedTax, "0.00");
                        txtCurrFedTax.Text = Strings.Format(clsW2CInfo.CurrFedTax, "0.00");
                    }

                    if (clsW2CInfo.PrevFicaTax > 0 || clsW2CInfo.CurrFicaTax > 0)
                    {
                        txtPrevSSTax.Text = Strings.Format(clsW2CInfo.PrevFicaTax, "0.00");
                        txtCurrSSTax.Text = Strings.Format(clsW2CInfo.CurrFicaTax, "0.00");
                    }

                    if (clsW2CInfo.PrevMedTax > 0 || clsW2CInfo.CurrMedTax > 0)
                    {
                        txtPrevMedTax.Text = Strings.Format(clsW2CInfo.PrevMedTax, "0.00");
                        txtCurrMedTax.Text = Strings.Format(clsW2CInfo.CurrMedTax, "0.00");
                    }

                    if (clsW2CInfo.PrevAllocatedTips > 0 || clsW2CInfo.CurrAllocatedTips > 0)
                    {
                        txtPrevAllocatedTips.Text = Strings.Format(clsW2CInfo.PrevAllocatedTips, "0.00");
                        txtCurrAllocatedTips.Text = Strings.Format(clsW2CInfo.CurrAllocatedTips, "0.00");
                    }

                    if (clsW2CInfo.PrevDependentCare > 0 || clsW2CInfo.CurrDependentCare > 0)
                    {
                        txtPrevDependentCare.Text = Strings.Format(clsW2CInfo.PrevDependentCare, "0.00");
                        txtCurrDependentCare.Text = Strings.Format(clsW2CInfo.CurrDependentCare, "0.00");
                    }

                    txtExplanation.Text = clsW2CInfo.Explanation;
                    if (clsW2CInfo.PrevBox12 > 0 || clsW2CInfo.CurrBox12 > 0)
                    {
                        txtPrevBox12.Text = Strings.Format(clsW2CInfo.PrevBox12, "0.00");
                        txtCurrBox12.Text = Strings.Format(clsW2CInfo.CurrBox12, "0.00");
                    }

                    if (clsW2CInfo.PrevBox14 > 0 || clsW2CInfo.CurrBox14 > 0)
                    {
                        txtPrevBox14.Text = Strings.Format(clsW2CInfo.PrevBox14, "0.00");
                        txtCurrentBox14.Text = Strings.Format(clsW2CInfo.CurrBox14, "0.00");
                    }

                    if (clsW2CInfo.PrevStateWage > 0 || clsW2CInfo.CurrStateWage > 0)
                    {
                        txtPrevStateWage.Text = Strings.Format(clsW2CInfo.PrevStateWage, "0.00");
                        txtCurrStateWages.Text = Strings.Format(clsW2CInfo.CurrStateWage, "0.00");
                    }

                    if (clsW2CInfo.PrevStateTax > 0 || clsW2CInfo.CurrStateTax > 0)
                    {
                        txtPrevStateTax.Text = Strings.Format(clsW2CInfo.PrevStateTax, "0.00");
                        txtCurrStateTax.Text = Strings.Format(clsW2CInfo.CurrStateTax, "0.00");
                    }

                    if (clsW2CInfo.EmploymentReturnAdjustment)
                    {
                        chkAdjustmentNo.Visible = false;
                        chkAdjustmentYes.Visible = true;
                        if (Information.IsDate(clsW2CInfo.EmpReturnDate))
                        {
                            txtDateFiled.Text = Strings.Format(clsW2CInfo.EmpReturnDate, "MM/dd/yyyy");
                        }
                        else
                        {
                            txtDateFiled.Text = "";
                        }
                    }
                    else
                    {
                        chkAdjustmentNo.Visible = true;
                        chkAdjustmentYes.Visible = false;
                        txtDateFiled.Text = "";
                    }

                    txtTitle.Text = clsW2CInfo.ContactTitle;
                    txtEmail.Text = clsW2CInfo.Email;
                    txtContactPerson.Text = clsW2CInfo.ContactPerson;
                    string strTemp = "";
                    strTemp = clsW2CInfo.PhoneNumber;
                    if (Strings.Left(strTemp, 3) != "000")
                    {
                        txtAreaCode.Text = Strings.Left(strTemp, 3);
                    }
                    else
                    {
                        txtAreaCode.Text = "";
                    }

                    if (Strings.Mid(strTemp, 4, 3) != "000")
                    {
                        txtPhone.Text = Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
                    }
                    else
                    {
                        txtPhone.Text = "";
                    }

                    strTemp = clsW2CInfo.FaxNumber;
                    if (Strings.Left(strTemp, 3) != "000")
                    {
                        txtFaxAreaCode.Text = Strings.Left(strTemp, 3);
                    }
                    else
                    {
                        txtFaxAreaCode.Text = "";
                    }

                    if (Strings.Mid(strTemp, 4, 3) != "000")
                    {
                        txtFax.Text = Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
                    }
                    else
                    {
                        txtFax.Text = "";
                    }
                }
                else
                {
                    txtEmployerName.Text = "Employer";
                    txtEmployerAddress1.Text = "Address 1";
                    txtEmployerAddress2.Text = "Address 2";
                    txtEmployerAddress3.Text = "Address 3";
                    txtYear.Text = "9999";
                    txtW2Type.Text = "2";
                    chk941.Visible = true;
                    chkCT1.Visible = true;
                    chkMilitary.Visible = true;
                    chkHshld.Visible = true;
                    chk943.Visible = true;
                    chkMed.Visible = true;
                    chkThirdParty.Visible = true;
                    chk944.Visible = true;
                    txtPrevWages.Text = "000.00";
                    txtCurrWages.Text = "000.00";
                    txtPrevSSWages.Text = "000.00";
                    txtCurrSSWages.Text = "000.00";
                    txtPrevMedicare.Text = "000.00";
                    txtCurrMedicare.Text = "000.00";
                    txtPrevSSTips.Text = "000.00";
                    txtCurrSSTips.Text = "000.00";
                    txtPrevNonQualifiedPlans.Text = "000.00";
                    txtCurrNonQualifiedPlans.Text = "000.00";
                    txtPrevFedTax.Text = "000.00";
                    txtCurrFedTax.Text = "000.00";
                    txtPrevSSTax.Text = "000.00";
                    txtCurrSSTax.Text = "000.00";
                    txtPrevMedTax.Text = "000.00";
                    txtCurrMedTax.Text = "000.00";
                    txtPrevAllocatedTips.Text = "000.00";
                    txtCurrAllocatedTips.Text = "000.00";
                    txtPrevDependentCare.Text = "000.00";
                    txtCurrDependentCare.Text = "000.00";
                    txtExplanation.Text = "Explanation";
                    txtPrevBox12.Text = "000.00";
                    txtCurrBox12.Text = "000.00";
                    txtPrevBox14.Text = "000.00";
                    txtCurrentBox14.Text = "000.00";
                    txtPrevStateWage.Text = "000.00";
                    txtCurrStateWages.Text = "000.00";
                    txtPrevStateTax.Text = "000.00";
                    txtCurrStateTax.Text = "000.00";
                    chkAdjustmentNo.Visible = true;
                    chkAdjustmentYes.Visible = true;
                    txtDateFiled.Text = "99/99/9999";
                    txtTitle.Text = "Title";
                    txtEmail.Text = "E-mail";
                    txtContactPerson.Text = "Contact";
                    txtAreaCode.Text = "000";
                    txtPhone.Text = "000-0000";
                    txtFaxAreaCode.Text = "000";
                    txtFax.Text = "000-0000";
                }
            }
        }
	}
}
