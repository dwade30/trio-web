//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptLaserCheckBody1.
	/// </summary>
	public partial class srptLaserCheckBody1 : FCSectionReport
	{
		public srptLaserCheckBody1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Sample Check";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptLaserCheckBody1 InstancePtr
		{
			get
			{
				return (srptLaserCheckBody1)Sys.GetInstance(typeof(srptLaserCheckBody1));
			}
		}

		protected srptLaserCheckBody1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLaserCheckBody1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolMuniInfoNonNegotiableOnly;
		private bool boolNonNegotiable;
		private int lngFormatToUse;
		const int CNSTCheckColBank = 55;
		const int CNSTCheckColBankName = 56;
		const int CNSTCheckColBankRouting = 57;
		const int CNSTCheckColBankAccount = 58;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
			double dblRatio = 0;
			int lngWidth = 0;
			int lngHeight = 0;
			FCFileSystem fso = new FCFileSystem();
			int lngID = 0;
			string strFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolNonNegotiable = false;
				lngFormatToUse = modCustomChecks.Statics.intCustomCheckFormatToUse;
				if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais")
				{
					this.Detail.Height = 5220 / 1440F;
					this.Detail.CanShrink = false;
				}
				if (FCConvert.ToString(this.UserData) != string.Empty && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "HALLOWELL")
				{
					clsPrt.SetReportFontsByTag(this, "Text", FCConvert.ToString(this.UserData));
					clsPrt.SetReportFontsByTag(this, "bold", FCConvert.ToString(this.UserData), true);
				}
				else
				{
					int x;
					// if it's a dotmatrix
					if (Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 30)) == 0)
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "TEXT" || fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "BOLD")
							{
								(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font((this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Name, 9);
							}
						}
						// x
					}
				}
				Image1.Visible = false;
				if (rptLaserCheck1.InstancePtr.boolPrintSig)
				{
					lngID = rptLaserCheck1.InstancePtr.lngSignatureID;
					if (lngID > 0)
					{
						strFile = modSignatureFile.GetSigFileFromID(ref lngID);
						if (strFile != string.Empty)
						{
							// If FCFileSystem.FileExists(CurDir & "\Sigfile\TWPYSIG.SIG") Then
							if (FCFileSystem.FileExists(strFile))
							{
								// Image1.Picture = LoadPicture(CurDir & "\Sigfile\TWPYSIG.SIG")
								Image1.Image = FCUtils.LoadPicture(strFile);
								Image1.Visible = true;
								lngWidth = Image1.Image.Width;
								lngHeight = Image1.Image.Height;
								dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
								if (dblRatio * Image1.Height > Image1.Width)
								{
									// keep width, change height
									Image1.Height = FCConvert.ToSingle(Image1.Width / dblRatio);
								}
								else
								{
									// keep height, change width
									Image1.Width = FCConvert.ToSingle(Image1.Height * dblRatio);
								}
								// Image1.Top = 3500 - Image1.Height  +  'keep bottom of signature on line. This is default laser check
								Image1.Top = 3885 / 1440F - Image1.Height;
								// keep bottom of signature on line. This is default laser check
							}
						}
					}
				}
				else
				{
					Image1.Image = null;
					Image1.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check Body ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// vbPorter upgrade warning: lngTop As int	OnWrite(double, int)
				float lngTop = 0;
				// vbPorter upgrade warning: lngLeft As int	OnWriteFCConvert.ToDouble(
				float lngLeft = 0;
				int lngSignatureLeft;
				int lngSignatureTop;
				double dblDirectDeposit = 0;
                using (clsDRWrapper clsLoad = new clsDRWrapper())
                {
                    FCFileSystem fso = new FCFileSystem();
                    string strLogoFile = "";
                    fecherFoundation.Information.Err().Clear();
                    boolNonNegotiable = rptLaserCheck1.InstancePtr.NonNegotiable;
                    if (boolNonNegotiable)
                    {
                        lngFormatToUse = rptLaserCheck1.InstancePtr.NonNegotiableFormat;
                    }

                    if (rptLaserCheck1.InstancePtr.boolCustomCheck)
                    {
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "TOWN" ||
                            fecherFoundation.Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "CITY")
                        {
                            lblMunicipalName.Text = modGlobalConstants.Statics.gstrCityTown + " of " +
                                                    modGlobalConstants.Statics.MuniName;
                        }
                        else
                        {
                            lblMunicipalName.Text = modGlobalConstants.Statics.MuniName;
                        }

                        clsLoad.OpenRecordset("select * from tblcheckformat", "twpy0000.vb1");
                        boolMuniInfoNonNegotiableOnly =
                            FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("LogoOnNonNegotiableOnly"));
                        if (!boolNonNegotiable)
                        {
                            clsLoad.OpenRecordset(
                                "select * from customchecks where ID = " +
                                FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), "twpy0000.vb1");
                        }
                        else
                        {
                            clsLoad.OpenRecordset(
                                "select * from customchecks where id = " + FCConvert.ToString(lngFormatToUse),
                                "twpy0000.vb1");
                        }

                        if (!clsLoad.EndOfFile())
                        {
                            lblVoidAfter90Days.Text = clsLoad.Get_Fields("voidaftermessage");
                        }

                        clsLoad.OpenRecordset(
                            "select * from customcheckfields where formatid = " + FCConvert.ToString(lngFormatToUse),
                            "twpy0000.vb1");
                        while (!clsLoad.EndOfFile())
                        {
                            fecherFoundation.Information.Err().Clear();
                            Detail.Controls[clsLoad.Get_Fields("controlname")].Visible = clsLoad.Get_Fields("include");
                            lngLeft = FCConvert.ToSingle(144 * Conversion.Val(clsLoad.Get_Fields("xposition"))) / 1440F;
                            lngTop = FCConvert.ToSingle(240 * Conversion.Val(clsLoad.Get_Fields("Yposition"))) / 1440F;
                            Detail.Controls[clsLoad.Get_Fields("controlname")].Left = lngLeft;
                            Detail.Controls[clsLoad.Get_Fields("controlname")].Top = lngTop;
                            CustomFieldError: ;
                            clsLoad.MoveNext();
                        }

                        fecherFoundation.Information.Err().Clear();
                        if (boolMuniInfoNonNegotiableOnly)
                        {
                            if (!boolNonNegotiable)
                            {
                                lblReturnAddress.Visible = false;
                                txtLogo.Visible = false;
                                imgLogo.Visible = false;
                                lblMunicipalName.Visible = false;
                            }
                        }

                        if (txtAddressName.Visible == false)
                        {
                            txtAddress2.Visible = false;
                            txtAddress1.Visible = false;
                            txtCityStateZip.Visible = false;
                        }

                        lngTop = txtAddressName.Top + (240 / 1440F);
                        lngLeft = txtAddressName.Left;
                        txtAddress1.Left = lngLeft;
                        txtAddress1.Top = lngTop;
                        lngTop += (240 / 1440F);
                        txtAddress2.Left = lngLeft;
                        txtAddress2.Top = lngTop;
                        lngTop += (240 / 1440F);
                        txtCityStateZip.Left = lngLeft;
                        txtCityStateZip.Top = lngTop;
                        lngLeft = lblStandardVoid2.Left;
                        lngTop = lblStandardVoid2.Top;
                        if (lngLeft > 0 || lngTop > 0)
                        {
                            Image1.Top = (lngTop) - Image1.Height + (240 / 1440F);
                            Image1.Left = lngLeft;
                        }

                        clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                        if (!clsLoad.EndOfFile())
                        {
                            string strAddress = "";
                            strAddress =
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress1")));
                            if (fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2"))) != "")
                            {
                                if (strAddress != "")
                                    strAddress += "\r\n";
                                strAddress +=
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2")));
                            }

                            if (fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3"))) != "")
                            {
                                if (strAddress != "")
                                    strAddress += "\r\n";
                                strAddress +=
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3")));
                            }

                            if (fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4"))) != "")
                            {
                                if (strAddress != "")
                                    strAddress += "\r\n";
                                strAddress +=
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4")));
                            }

                            lblReturnAddress.Text = strAddress;
                            if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 0 ||
                                (boolMuniInfoNonNegotiableOnly && !boolNonNegotiable))
                            {
                                txtLogo.Visible = false;
                                txtLogo.Image = null;
                            }
                            else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 1)
                            {
                                // town seal
                                txtLogo.Visible = true;
                                if (Strings.Left(Environment.CurrentDirectory, 1) != "\\")
                                {
                                    strLogoFile = Environment.CurrentDirectory + "\\TownSeal.pic";
                                }
                                else
                                {
                                    strLogoFile = Environment.CurrentDirectory + "TownSeal.pic";
                                }

                                if (strLogoFile != string.Empty)
                                {
                                    if (FCFileSystem.FileExists(strLogoFile))
                                    {
                                        txtLogo.Image = FCUtils.LoadPicture(strLogoFile);
                                    }
                                }
                            }
                            else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 2)
                            {
                                // custom image
                                txtLogo.Visible = true;
                                strLogoFile =
                                    fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("checklogo")));
                                if (strLogoFile != string.Empty)
                                {
                                    if (FCFileSystem.FileExists(strLogoFile))
                                    {
                                        txtLogo.Image = FCUtils.LoadPicture(strLogoFile);
                                    }
                                }
                            }
                        }
                    }

                    if (Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 30)) == 0 &&
                        !(rptLaserCheck1.InstancePtr.clsFormat.Get_Fields_Boolean("customsetup") == true))
                    {
                        txtDate.Top += .5F; //720 / 1440F;
                        // three lines
                        txtCheckAmount.Top += .5F; //720 / 1440F;
                        txtDate.Left -= (288 / 1440F);
                        // two characters
                    }

                    txtAmountString.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 10);
                    txtAddressName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
                    txtName.Text = txtAddressName.Text;
                    txtAddress1.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 19);
                    txtAddress2.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 20);
                    if (txtAddressName.Visible)
                    {
                        if (txtAddress2.Text == string.Empty)
                        {
                            txtCityStateZip.Top = txtAddress2.Top;
                            txtAddress2.Visible = false;
                        }
                        else
                        {
                            txtAddress2.Visible = true;
                        }
                    }

                    txtCityStateZip.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 21);
                    txtEmployeeNumCheck.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 22);
                    txtCheckAmount.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 11);
                    txtCheckNumber.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12);
                    txtDate.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 24);
                    lblLaserVoid.Visible = false;
                    lblLaserVoid2.Visible = false;
                    lblStandardVoid1.Visible = false;
                    lblStandardVoid2.Visible = false;
                    dblDirectDeposit = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 29)) +
                                       Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 34));
                    string strRoutingNumber = "";
                    string strAccount = "";
                    strRoutingNumber = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTCheckColBankRouting);
                    strAccount = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTCheckColBankAccount);
                    fldMICRLine.Text =
                        "C" + FCConvert.ToString(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12)
                            .Replace("CHECK ", "").Replace("Check ", ""))) + "C A" + strRoutingNumber + "A  " +
                        strAccount + "C";
                    // MATTHEW COREY FOUND FOR CARIBOU THAT THE NET VALUE HAD A E-13 DECIMAL
                    // VALUE SO THEY WERE NEVER NETTING OUT TO ZERO. 2/26/2004
                    // If .TextMatrix(0, 27) = "R" And (dblDirectDeposit >= Format(Val(.TextMatrix(0, 9)), "0.00")) Then
                    if (rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 27) == "R" &&
                        (Conversion.Val(Strings.Format(dblDirectDeposit, "0.00")) >= Conversion.Val(
                            Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)),
                                "0.00"))))
                    {
                        Image1.Visible = false;
                        if (Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 30)) == 1)
                        {
                            lblLaserVoid.Visible = true;
                            lblLaserVoid2.Visible = true;
                        }
                        else
                        {
                            lblStandardVoid1.Text = "*** VOID *** VOID ***";
                            lblStandardVoid2.Text = lblStandardVoid1.Text;
                            lblStandardVoid1.Visible = true;
                            lblStandardVoid2.Visible = true;
                            lblLaserVoid2.Visible = true;
                        }
                    }
                    else if (Strings.Left(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 10) + "    ", 6) ==
                             "** EFT")
                    {
                        Image1.Visible = false;
                        lblStandardVoid1.Text = "***  EFT *** EFT  ***";
                        lblStandardVoid2.Text = lblStandardVoid1.Text;
                        lblStandardVoid1.Visible = true;
                        lblStandardVoid2.Visible = true;
                        lblLaserVoid2.Visible = true;
                    }
                    else if (Strings.Left(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 10) + "    ", 6) ==
                             "** ACH")
                    {
                        Image1.Visible = false;
                        lblStandardVoid1.Text = "***  ACH *** ACH  ***";
                        lblStandardVoid2.Text = lblStandardVoid1.Text;
                        lblStandardVoid1.Visible = true;
                        lblStandardVoid2.Visible = true;
                        lblLaserVoid2.Visible = true;
                    }
                }

                return;
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check Body DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void srptLaserCheckBody1_Load(object sender, System.EventArgs e)
		{
		}
	}
}
