//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Enums;
using SharedApplication.Payroll.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDirectDeposit.
	/// </summary>
	public partial class rptDirectDeposit : BaseSectionReport
	{
		public rptDirectDeposit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Direct Deposit";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDirectDeposit InstancePtr
		{
			get
			{
				return (rptDirectDeposit)Sys.GetInstance(typeof(rptDirectDeposit));
			}
		}

		protected rptDirectDeposit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBankInfo?.Dispose();
                rsBankInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDirectDeposit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsBankInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curBankTotal As Decimal	OnWrite(int, Decimal)
		Decimal curBankTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		int intBankCount;
		int intFinalCount;
		public DateTime datPayDate;
		public int intPayRunID;
		bool boolFromTempTable;
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;
        private List<BankEmployeeDirectDepositReportInfo> reportData;
        private int dataCounter = 0;

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				dataCounter++;
				CheckRecord:
				;
				if (dataCounter < reportData.Count)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsBankInfo.MoveNext();
					if (rsBankInfo.EndOfFile() != true)
					{
						dataCounter = 0;
						reportData = StaticSettings.GlobalCommandDispatcher.Send(
							new GetBankEmployeeDirectDepositReportInfo
							{
								BankNumber = rsBankInfo.Get_Fields("ID"),
								PayDate = datPayDate,
								PayRunId = modGlobalConstants.Statics.gboolPrintALLPayRuns ? 0 : intPayRunID,
								UseTempTable = boolFromTempTable
							}).Result.ToList();

						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsBankInfo.Get_Fields_Int32("RecordNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
			}
			else
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			}
			blnFirstRecord = true;
			intBankCount = 0;
			intFinalCount = 0;
			curBankTotal = 0;
			curFinalTotal = 0;
			if (!boolFromTempTable)
			{
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
				{
					rsBankInfo.OpenRecordset("SELECT DISTINCT tblBanks.ID as ID, tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, tblBanks.Address1 as Address1, tblBanks.Address2 as Address2, tblBanks.Address3 as Address3 FROM (tblBanks INNER JOIN tblCheckDetail ON tblBanks.ID = convert(int, tblCheckDetail.DDBankNumber)) WHERE tblCheckDetail.BankRecord = 1 AND tblCheckDetail.PayDate = '" + FCConvert.ToString(datPayDate) + "' ORDER BY tblBanks.RecordNumber");
				}
				else
				{
					rsBankInfo.OpenRecordset("SELECT DISTINCT tblBanks.ID as ID, tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, tblBanks.Address1 as Address1, tblBanks.Address2 as Address2, tblBanks.Address3 as Address3 FROM (tblBanks INNER JOIN tblCheckDetail ON tblBanks.ID = convert(int, tblCheckDetail.DDBankNumber)) WHERE tblCheckDetail.BankRecord = 1 AND tblCheckDetail.PayDate = '" + FCConvert.ToString(datPayDate) + "' AND tblCheckDetail.PayRunID = " + FCConvert.ToString(intPayRunID) + " ORDER BY tblBanks.RecordNumber");
				}
			}
			else
			{
				rsBankInfo.OpenRecordset("SELECT DISTINCT tblBanks.ID as ID, tblBanks.RecordNumber as RecordNumber, tblBanks.Name as Name, tblBanks.Address1 as Address1, tblBanks.Address2 as Address2, tblBanks.Address3 as Address3 FROM (tblBanks INNER JOIN tblTempPayProcess ON tblBanks.ID = convert(int,tblTempPayProcess.DDBankNumber)) WHERE tblTempPayProcess.BankRecord = 1 AND tblTempPayProcess.PayDate = '" + FCConvert.ToString(datPayDate) + "' AND tblTempPayProcess.PayRunID = " + FCConvert.ToString(intPayRunID) + " ORDER BY tblBanks.RecordNumber");
			}
			if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
			{
				dataCounter = 0;
				reportData = StaticSettings.GlobalCommandDispatcher.Send(
					new GetBankEmployeeDirectDepositReportInfo
					{
						BankNumber = rsBankInfo.Get_Fields("ID"),
						PayDate = datPayDate,
						PayRunId = modGlobalConstants.Statics.gboolPrintALLPayRuns ? 0 : intPayRunID,
						UseTempTable = boolFromTempTable
					}).Result.ToList();
			}
			else
			{
				MessageBox.Show("No direct deposit information found for this pay run.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				if (!boolFromTempTable)
				{
					modGlobalRoutines.UpdatePayrollStepTable("BankList");
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
					{
						this.Close();
						return;
					}
					else
					{
						modDavesSweetCode.Statics.blnReportCompleted = true;
					}
				}
			}

            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewBankAccountNumbers.ToInteger()))
            {
                case "F":
                    bankAccountViewPermission = BankAccountViewType.Full;
                    break;
                case "P":
                    bankAccountViewPermission = BankAccountViewType.Masked;
                    break;
                default:
                    bankAccountViewPermission = BankAccountViewType.None;
                    break;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!boolFromTempTable)
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
				{
					modGlobalRoutines.UpdatePayrollStepTable("BankList");
				}
				else
				{
					modDavesSweetCode.Statics.blnReportCompleted = true;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldEmployee As object	OnWrite(string)
			intBankCount += 1;
			if (dataCounter >= reportData.Count)
			{
			}
			else
            {
				var bankAccount = "";
				switch (bankAccountViewPermission)
				{
					case BankAccountViewType.Full:
						bankAccount = reportData[dataCounter].DirectDepositAccountNumber;
						break;
					case BankAccountViewType.Masked:
						bankAccount = "******" + reportData[dataCounter].DirectDepositAccountNumber.Right(4);
						break;
					default:
						bankAccount = "**********";
						break;
				}

				fldAccount.Text = bankAccount;
				fldAmount.Text = Strings.Format(reportData[dataCounter].DirectDepositTotal, "#,##0.00");
				curBankTotal += reportData[dataCounter].DirectDepositTotal;
				fldEmployee.Text = Strings.Format(reportData[dataCounter].EmployeeNumber, "00") + "  " + reportData[dataCounter].EmployeeName;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldBankTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldBankCount As object	OnWrite
			fldBankTotal.Text = Strings.Format(curBankTotal, "#,##0.00");
			fldBankCount.Text = intBankCount.ToString();
			curFinalTotal += curBankTotal;
			intFinalCount += intBankCount;
			curBankTotal = 0;
			intBankCount = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			subTotals.Report = new srptDirectDepositTotals();
			subTotals.Report.UserData = boolFromTempTable;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldBank As object	OnWrite(string)
			fldBank.Text = rsBankInfo.Get_Fields_Int32("RecordNumber") + "  " + rsBankInfo.Get_Fields_String("Name");
			fldBankAddress1.Text = rsBankInfo.Get_Fields_String("Address1");
			fldBankAddress2.Text = rsBankInfo.Get_Fields_String("Address2");
			fldBankAddress3.Text = FCConvert.ToString(rsBankInfo.Get_Fields("Address3"));
		}

		public void Init(bool boolFromTempPayProcess = false, DateTime? tempdtPayDat = null, int intPRun = 1, bool boolDontGroup = false, List<string> batchReports = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtPayDat == null)
			{
				tempdtPayDat = DateTime.FromOADate(0);
			}
			DateTime dtPayDat = tempdtPayDat.Value;

			boolFromTempTable = boolFromTempPayProcess;
			if (boolDontGroup)
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			if ((modGlobalVariables.Statics.gstrMQYProcessing == "NONE" && !boolFromTempTable) && !modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				if (Information.IsDate(dtPayDat) && dtPayDat.ToOADate() != 0)
				{
					datPayDate = dtPayDat;
					intPayRunID = intPRun;
				}
				else
				{
					datPayDate = DateTime.Today;
					intPayRunID = 1;
					frmSelectDateInfo.InstancePtr.Init2("Please select the pay date you wish to have reported for.", -1, -1, -1, ref datPayDate, ref intPayRunID, false);
					if (datPayDate.ToOADate() != 0)
					{
						// do nothing
					}
					else
					{
						this.Close();
						return;
					}
				}
			}
			else
			{
				datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(rptDirectDeposit.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptDirectDeposit.PrintReport False
			}
			else
			{
				// frmReportViewer.Init Me
				modCoreysSweeterCode.CheckDefaultPrint(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "DirectDeposit");
			}
		}
	}
}
