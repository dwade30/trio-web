﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptACH.
	/// </summary>
	partial class rptACH
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptACH));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDFI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBank = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDFI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBank)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldAmount,
            this.fldEmployee,
            this.txtDFI,
            this.txtBank,
            this.Field1});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1666667F;
			this.fldAccount.Left = 2.5F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1666667F;
			this.fldAmount.Left = 4.65625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.78125F;
			// 
			// fldEmployee
			// 
			this.fldEmployee.Height = 0.1666667F;
			this.fldEmployee.Left = 5.46875F;
			this.fldEmployee.Name = "fldEmployee";
			this.fldEmployee.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldEmployee.Text = null;
			this.fldEmployee.Top = 0F;
			this.fldEmployee.Width = 1.78125F;
			// 
			// txtDFI
			// 
			this.txtDFI.Height = 0.1666667F;
			this.txtDFI.Left = 1.625F;
			this.txtDFI.Name = "txtDFI";
			this.txtDFI.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtDFI.Text = null;
			this.txtDFI.Top = 0F;
			this.txtDFI.Width = 0.75F;
			// 
			// txtBank
			// 
			this.txtBank.CanGrow = false;
			this.txtBank.Height = 0.1666667F;
			this.txtBank.Left = 0F;
			this.txtBank.Name = "txtBank";
			this.txtBank.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtBank.Text = null;
			this.txtBank.Top = 0F;
			this.txtBank.Width = 1.59375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 3.65625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.Field1.Text = null;
			this.Field1.Top = 0F;
			this.Field1.Width = 0.9375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label14,
            this.fldFinalTotal,
            this.Label15,
            this.fldFinalCount,
            this.Line2});
			this.ReportFooter.Height = 0.46875F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.3125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.Label14.Text = "Totals";
			this.Label14.Top = 0.28125F;
			this.Label14.Width = 1.0625F;
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.Height = 0.19F;
			this.fldFinalTotal.Left = 4.4375F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldFinalTotal.Text = "Field1";
			this.fldFinalTotal.Top = 0.28125F;
			this.fldFinalTotal.Width = 1F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.5625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.Label15.Text = "Entries";
			this.Label15.Top = 0.28125F;
			this.Label15.Width = 1.0625F;
			// 
			// fldFinalCount
			// 
			this.fldFinalCount.Height = 0.19F;
			this.fldFinalCount.Left = 3.6875F;
			this.fldFinalCount.Name = "fldFinalCount";
			this.fldFinalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldFinalCount.Text = "Field1";
			this.fldFinalCount.Top = 0.28125F;
			this.fldFinalCount.Width = 0.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.21875F;
			this.Line2.Width = 6.5F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 6.5625F;
			this.Line2.Y1 = 0.21875F;
			this.Line2.Y2 = 0.21875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.lblTime,
            this.lblMuniname,
            this.lblDate,
            this.lblPayDate,
            this.txtPage});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.229F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.Label1.Text = "Payroll ACH";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.688F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = "Label7";
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.5F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1875F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniname.Text = "Label2";
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 1.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.1875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = "Label3";
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.3125F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.229F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = null;
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 4.688F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.txtPage.Text = "Label18";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.3125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label19,
            this.txtAddress1,
            this.txtAddress2,
            this.txtAddress3});
			this.GroupHeader1.Height = 0.6875F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// Label19
			// 
			this.Label19.Height = 0.166F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.Label19.Text = "ACH Bank";
			this.Label19.Top = 0F;
			this.Label19.Width = 0.75F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.HyperLink = null;
			this.txtAddress1.Left = 0F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 2.875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.HyperLink = null;
			this.txtAddress2.Left = 0F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 2.875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.HyperLink = null;
			this.txtAddress3.Left = 0F;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.46875F;
			this.txtAddress3.Width = 2.875F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.Label10,
            this.Line1,
            this.Label11,
            this.Label17,
            this.Label18,
            this.Label20});
			this.GroupHeader2.Height = 0.2708333F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label9
			// 
			this.Label9.Height = 0.177F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.04166667F;
			this.Label9.Width = 1.094F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.177F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.65625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.Label10.Text = "Type";
			this.Label10.Top = 0.04166667F;
			this.Label10.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.21875F;
			this.Line1.Width = 7.25F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.25F;
			this.Line1.Y1 = 0.21875F;
			this.Line1.Y2 = 0.21875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.177F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.46875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.Label11.Text = "Employee";
			this.Label11.Top = 0.04166667F;
			this.Label11.Width = 1.531F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.Label17.Text = "DFI Number";
			this.Label17.Top = 0.04166667F;
			this.Label17.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.Label18.Text = "Bank";
			this.Label18.Top = 0.0625F;
			this.Label18.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.177F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.8125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label20.Text = "Amount";
			this.Label20.Top = 0.04166667F;
			this.Label20.Width = 0.594F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// rptACH
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDFI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBank)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDFI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBank;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
	}
}
