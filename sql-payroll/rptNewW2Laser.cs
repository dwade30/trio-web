//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Laser.
	/// </summary>
	public partial class rptW2Laser : BaseSectionReport
	{
		public rptW2Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "W2 Laser";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2Laser InstancePtr
		{
			get
			{
				return (rptW2Laser)Sys.GetInstance(typeof(rptW2Laser));
			}
		}

		protected rptW2Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsW2Master?.Dispose();
				rsW2Deductions?.Dispose();
				rsW2Matches?.Dispose();
				rsBox10?.Dispose();
				rsBox14?.Dispose();
				rsbox12?.Dispose();
                rsData = null;
                rsW2Master = null;
                rsW2Deductions = null;
                rsW2Matches = null;
                rsBox10 = null;
                rsBox14 = null;
                rsbox12 = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsW2Master = new clsDRWrapper();
		clsDRWrapper rsW2Deductions = new clsDRWrapper();
		clsDRWrapper rsW2Matches = new clsDRWrapper();
		clsDRWrapper rsbox12 = new clsDRWrapper();
		clsDRWrapper rsBox14 = new clsDRWrapper();
		clsDRWrapper rsBox10 = new clsDRWrapper();
		double dblLaserLineAdjustment;
		object ControlName;
		bool boolMove;
		bool boolThirdParty;
		// vbPorter upgrade warning: intArrayCounter As int	OnWriteFCConvert.ToInt32(
		int intArrayCounter;
		// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
		int intloop;
		bool boolPrintTest;
		int intIteration;
		double dblHorizAdjust;
		int lngYearToUse;

		private struct BoxEntries
		{
			public string Code;
			public double Amount;
			public bool ThirdParty;
		};

		private BoxEntries[] Box1214 = null;
		private BoxEntries[] aryBox10 = null;
		private BoxEntries[] aryBox14 = null;

		public void Init(bool boolTestPrint = false, double dblAdjustment = 0, double dblHorizAdjustment = 0)
		{
			boolPrintTest = boolTestPrint;
			intIteration = 1;
			dblLaserLineAdjustment = dblAdjustment;
			dblHorizAdjust = (120 * dblHorizAdjustment) / 1440F;
			frmReportViewer.InstancePtr.Init(this, "", 1, boolAllowEmail: false, showModal: true);
			if (!boolPrintTest)
			{
				rptW3Laser.InstancePtr.Init();
				if (lngYearToUse > 0)
				{
					frmW3ME.InstancePtr.Init(lngYearToUse, false);
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{			
			boolThirdParty = false;
			bool boolFound = false;
			double dblTemp = 0;
            var nonBox1214ThirdParty = false;

            if (!boolPrintTest)
			{
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					if (rsW2Master.EndOfFile())
					{
					}
					else
					{
						txtFederalEIN.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
						txtEmployersName.Text = rsW2Master.Get_Fields_String("EmployersName") + "\r";
						txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address1") + "\r";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("Address2"))) != string.Empty)
						{
							txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address2") + "\r";
						}
						txtEmployersName.Text = txtEmployersName.Text + fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("City"))) + ", " + rsW2Master.Get_Fields("State") + " " + rsW2Master.Get_Fields("Zip");
						txtLocalityName.Text = rsW2Master.Get_Fields_String("LocalityName");
						txtState.Text = rsW2Master.Get_Fields_String("StateCode");
						txtEmployersStateEIN.Text = rsW2Master.Get_Fields_String("EmployersStateID");
					}
					txtSSN.Text = rsData.Get_Fields_String("SSN");
					// corey 01/23/05
					// separating last from first and middle
					txtEmployeesName.Text = rsData.Get_Fields("Firstname") + " " + fecherFoundation.Strings.Trim(Strings.Left(rsData.Get_Fields("MIddlename") + " ", 1)) + "\r" + "\r" + "\r" + "\r";
					txtLastName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("lastname")));
					txtDesig.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("desig")));
					// txtEmployeesName = rsData.Fields("EmployeeName") & Chr(13) & Chr(13) & Chr(13) & Chr(13)
					txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("Address") + "\r";
					txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("CityStateZip");
					// SET THE WAGES AND TAXES
					txtTotalWages.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("FederalWage")));
					txtFederalTax.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("FederalTax")));
					txtFICAWages.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("FICAWage")));
					txtFICATax.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("FICATax")));
					txtMedWages.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("MedicareWage")));
					txtMedTaxes.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("MedicareTax")));
                    nonBox1214ThirdParty = rsData.Get_Fields_Boolean("W23rdPartySickPay");
                    // PER RON AND KPORT 12/08/2004
                    txtStateWages.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("StateWage")));
					txtStateTax.Text = modGlobalRoutines.CheckForBlank(FCConvert.ToString(rsData.Get_Fields("StateTax")));
					txtPen.Visible = rsData.Get_Fields_Boolean("W2Retirement");
					txtStatutoryEmployee.Visible = rsData.Get_Fields_Boolean("W2StatutoryEmployee");
					// ********************************************************************
					// SAVE W3 INFORMATION
					modGlobalVariables.Statics.W3Information.dblTotalWages += Conversion.Val(txtTotalWages.Text);
					modGlobalVariables.Statics.W3Information.dblFederalTax += Conversion.Val(txtFederalTax.Text);
					modGlobalVariables.Statics.W3Information.dblFicaWages += Conversion.Val(txtFICAWages.Text);
					modGlobalVariables.Statics.W3Information.dblFicaTax += Conversion.Val(txtFICATax.Text);
					modGlobalVariables.Statics.W3Information.dblMedWages += Conversion.Val(txtMedWages.Text);
					modGlobalVariables.Statics.W3Information.dblMedTaxes += Conversion.Val(txtMedTaxes.Text);
					// PER RON AND KPORT 12/08/2004
					modGlobalVariables.Statics.W3Information.dblStateWages += Conversion.Val(txtStateWages.Text);
					modGlobalVariables.Statics.W3Information.dblStateTax += Conversion.Val(txtStateTax.Text);
					// ********************************************************************
					// SET THE BOX 12 AND BOX 14 INFORMATION
					txtOther.Text = string.Empty;
					txt12a.Text = string.Empty;
					txt12aAmount.Text = string.Empty;
					txt12b.Text = string.Empty;
					txt12bAmount.Text = string.Empty;
					txt12c.Text = string.Empty;
					txt12cAmount.Text = string.Empty;
					txt12d.Text = string.Empty;
					txt12dAmount.Text = string.Empty;
					// If rsData.Fields("EmployeeNumber") = "016" Then
					// Else
					// GoTo Done14
					// End If
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box12 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
					for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = Conversion.Val(rsW2Deductions.Get_Fields("SumOfCYTDAmount"));
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					// Call rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE (((tblW2Matches.DeductionNumber) Not In (Select DeductionNumber from tblW2Deductions where Code <> '' AND EmployeeNumber ='" & rsData.Fields("EmployeeNumber") & "' AND tblW2Box12And14.Box12 = 1))) AND TBLW2MATCHES.employeenumber = '" & rsData.Fields("employeenumber") & "' and tblw2box12and14.Box12 = 1 GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code ", "TWPY0000.VB1")
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  TBLW2MATCHES.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' and tblw2box12and14.Box12 = 1 GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code ", "TWPY0000.VB1");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					// ReDim Preserve Box1214(UBound(Box1214) + rsW2Matches.RecordCount)
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("code")))
							{
								boolFound = true;
								Box1214[intloop].Amount += Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
								break;
							}
						}
						// intloop
						if (!boolFound)
						{
							Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + 1 + 1);
							Box1214[Information.UBound(Box1214)].Amount = Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
							Box1214[Information.UBound(Box1214)].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
							Box1214[Information.UBound(Box1214)].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("thirdpartysave"));
						}
						rsW2Matches.MoveNext();
					}
					// now show box 12 info
					intCounter = 1;
					boolThirdParty = false;
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
					{
						if (!boolThirdParty)
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = Conversion.Val(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txt12a.Text = Box1214[intArrayCounter].Code;
							txt12aAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 2)
						{
							txt12b.Text = Box1214[intArrayCounter].Code;
							txt12bAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 3)
						{
							txt12c.Text = Box1214[intArrayCounter].Code;
							txt12cAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 4)
						{
							txt12d.Text = Box1214[intArrayCounter].Code;
							txt12dAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipEntry:
						;
					}
					// now fill box 14
					Box1214 = new BoxEntries[0 + 1];
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
					for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = Conversion.Val(rsW2Deductions.Get_Fields("SumOfCYTDAmount"));
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box14 = 1 and tblw2matches.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code ");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
							{
								Box1214[intloop].Amount += Conversion.Val(rsW2Matches.Get_Fields("SumOfCYTDAmount"));
								boolFound = true;
								break;
							}
							else if (Box1214[intloop].Code == "")
							{
								Box1214[intloop].Amount = Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
								Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
								Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
								boolFound = true;
								break;
							}
						}
						// intloop
						rsW2Matches.MoveNext();
					}
					intCounter = 1;
					boolThirdParty = false;
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
					{
						if (!boolThirdParty)
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipMatchEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = Conversion.Val(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txtOther.Text = Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else
						{
							txtOther.Text = txtOther.Text + "\r" + Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipMatchEntry:
						;
					}
					Done14:
					;
					// NOW FILL BOX 10
					dblTemp = 0;
					rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp = Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					rsBox10.OpenRecordset("SELECT Sum(tblW2matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2matches ON tblW2Box12And14.DeductionNumber = tblW2matches.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2matches.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp += Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					txtDependentCare.Text = Strings.Format(dblTemp, "0.00");
					rsData.MoveNext();
					eArgs.EOF = false;
				}
                if (nonBox1214ThirdParty && !boolThirdParty)
                {
                    boolThirdParty = true;
                }
                txtThirdParty.Visible = boolThirdParty;
			}
			else
			{
				txt12a.Text = "X";
				txt12aAmount.Text = "999.99";
				txt12b.Text = "X";
				txt12bAmount.Text = "999.99";
				txt12c.Text = "X";
				txt12cAmount.Text = "999.99";
				txt12d.Text = "X";
				txt12dAmount.Text = "999.99";
				txtDependentCare.Text = "999.99";
				txtEmployeesName.Text = "First Middle" + "\r\n" + "\r\n" + "\r\n" + "\r\n" + "Address" + "\r\n" + "City, ST Zip";
				txtLastName.Text = "Last";
				txtEmployersName.Text = "Employer";
				txtEmployersStateEIN.Text = "XXXX";
				txtFederalEIN.Text = "XXXX";
				txtFederalTax.Text = "999.99";
				txtFICATax.Text = "999.99";
				txtFICAWages.Text = "999.99";
				txtLocalityName.Text = "XXX";
				txtMedTaxes.Text = "999.99";
				txtMedWages.Text = "999.99";
				txtSSN.Text = "999-99-9999";
				txtState.Text = "99";
				txtStateTax.Text = "999.99";
				txtStateWages.Text = "999.99";
				txtTotalWages.Text = "999.99";
                txtPen.Visible = true;
                txtStatutoryEmployee.Visible = true;
                txtThirdParty.Visible = true;
				intIteration += 1;
				if (intIteration > 3)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strReportOrder = "";
			if (!boolPrintTest)
			{
				dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
				dblHorizAdjust = FCConvert.ToDouble(modGlobalRoutines.SetW2HorizAdjustment(this));
			}
            else
            {
                foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
                {
                    ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
                    ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
                }
            }
            dblLaserLineAdjustment += 3;
			if (!boolPrintTest)
			{
				rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				switch (rsData.Get_Fields_Int32("ReportSequence"))
				{
					case 0:
						{
							// employee name
							strReportOrder = "LastName,FirstName,MiddleName";
							break;
						}
					case 1:
						{
							// employee number
							strReportOrder = "EmployeeNumber";
							break;
						}
					case 2:
						{
							// seq
							strReportOrder = "seqnumber,lastname,firstname";
							break;
						}
					case 3:
						{
							strReportOrder = "deptdiv,lastname,firstname";
							break;
						}
				}
				//end switch
				if (Conversion.Val(modGlobalVariables.Statics.gstrPrintAllW2s) == 0)
				{
					rsData.OpenRecordset("Select * from tblW2EditTable order by " + strReportOrder, "TWPY0000.vb1");
				}
				else
				{
					rsData.OpenRecordset("Select * from tblW2EditTable Where EmployeeNumber = '" + modGlobalVariables.Statics.gstrPrintAllW2s + "' order by " + strReportOrder, "TWPY0000.vb1");
				}
				if (!rsData.EndOfFile())
				{
					lngYearToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("year"))));
				}
				rsW2Master.OpenRecordset("Select * from tblW2Master", "TWPY0000.vb1");
				rsW2Deductions.OpenRecordset("Select * from  tblW2Deductions where Code <> '' Order by Code", "TWPY0000.vb1");
				// Call rsW2Matches.OpenRecordset("Select * from tblW2Matches where Code <> ''", "TWPY0000.vb1")
				// Call rsW2Matches.OpenRecordset("SELECT tblW2Matches.* From tblW2Matches, tblW2Deductions WHERE tblW2Matches.DeductionNumber<>[tblW2Deductions].[DeductionNumber] AND tblW2Matches.Code <> '' Order by tblW2Matches.Code", "TWPY0000.vb1")
				rsbox12.OpenRecordset("Select * from tblW2Box12AND14 Where Box12 = 1");
				rsBox14.OpenRecordset("Select * from tblW2Box12AND14 Where Box14 = 1");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
		}

	
	}
}
