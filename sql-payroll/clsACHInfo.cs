﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsACHInfo
	{
		//=========================================================
		private string strACHBankName = "";
		private string strACHBankRT = "";

		public string ACHBankName
		{
			get
			{
				string ACHBankName = "";
				ACHBankName = strACHBankName;
				return ACHBankName;
			}
			set
			{
				strACHBankName = value;
			}
		}

		public string ACHBankRT
		{
			get
			{
				string ACHBankRT = "";
				ACHBankRT = strACHBankRT;
				return ACHBankRT;
			}
			set
			{
				strACHBankRT = value;
			}
		}
	}
}
