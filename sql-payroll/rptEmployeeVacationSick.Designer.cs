﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeVacationSick.
	/// </summary>
	partial class rptEmployeeVacationSick
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmployeeVacationSick));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSecondTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCurrentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCurrentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBalanceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalanceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee,
				this.txtAccruedCurrent,
				this.txtUsedCurrent,
				this.txtBalance
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccruedCurrentTotal,
				this.txtUsedCurrentTotal,
				this.txtBalanceTotal,
				this.txtEmployeeTotal,
				this.Label22,
				this.Line1
			});
			this.ReportFooter.Height = 0.3541667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.txtTitle,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.Label13,
				this.Label15,
				this.lblBalance,
				this.txtSecondTitle
			});
			this.PageHeader.Height = 0.84375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label27,
				this.txtDeptDiv
			});
			this.GroupHeader1.Height = 0.3020833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.1875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label9.Text = "Employee";
			this.Label9.Top = 0.625F;
			this.Label9.Width = 1.375F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.HyperLink = null;
			this.txtTitle.Left = 1.5625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 4.9375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.125F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.8125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label13.Text = "Accrued";
			this.Label13.Top = 0.625F;
			this.Label13.Width = 0.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.8125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label15.Text = "Used";
			this.Label15.Top = 0.625F;
			this.Label15.Width = 0.5625F;
			// 
			// lblBalance
			// 
			this.lblBalance.Height = 0.1875F;
			this.lblBalance.HyperLink = null;
			this.lblBalance.Left = 4.625F;
			this.lblBalance.Name = "lblBalance";
			this.lblBalance.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblBalance.Text = "Balance";
			this.lblBalance.Top = 0.625F;
			this.lblBalance.Width = 0.6875F;
			// 
			// txtSecondTitle
			// 
			this.txtSecondTitle.Height = 0.1875F;
			this.txtSecondTitle.Left = 2.4375F;
			this.txtSecondTitle.Name = "txtSecondTitle";
			this.txtSecondTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtSecondTitle.Text = null;
			this.txtSecondTitle.Top = 0.25F;
			this.txtSecondTitle.Width = 3.125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.1875F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label27.Text = "Dept./Div";
			this.Label27.Top = 0F;
			this.Label27.Width = 0.875F;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.Height = 0.1875F;
			this.txtDeptDiv.Left = 1.125F;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtDeptDiv.Text = null;
			this.txtDeptDiv.Top = 0F;
			this.txtDeptDiv.Width = 2.3125F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.1875F;
			this.txtEmployee.Left = 0.1875F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEmployee.Text = null;
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 2.3125F;
			// 
			// txtAccruedCurrent
			// 
			this.txtAccruedCurrent.Height = 0.1875F;
			this.txtAccruedCurrent.Left = 2.75F;
			this.txtAccruedCurrent.Name = "txtAccruedCurrent";
			this.txtAccruedCurrent.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtAccruedCurrent.Text = null;
			this.txtAccruedCurrent.Top = 0F;
			this.txtAccruedCurrent.Width = 0.75F;
			// 
			// txtUsedCurrent
			// 
			this.txtUsedCurrent.Height = 0.1875F;
			this.txtUsedCurrent.Left = 3.625F;
			this.txtUsedCurrent.Name = "txtUsedCurrent";
			this.txtUsedCurrent.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtUsedCurrent.Text = null;
			this.txtUsedCurrent.Top = 0F;
			this.txtUsedCurrent.Width = 0.75F;
			// 
			// txtBalance
			// 
			this.txtBalance.Height = 0.1875F;
			this.txtBalance.Left = 4.5F;
			this.txtBalance.Name = "txtBalance";
			this.txtBalance.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtBalance.Text = null;
			this.txtBalance.Top = 0F;
			this.txtBalance.Width = 0.8125F;
			// 
			// txtAccruedCurrentTotal
			// 
			this.txtAccruedCurrentTotal.Height = 0.1875F;
			this.txtAccruedCurrentTotal.Left = 2.75F;
			this.txtAccruedCurrentTotal.Name = "txtAccruedCurrentTotal";
			this.txtAccruedCurrentTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtAccruedCurrentTotal.Text = null;
			this.txtAccruedCurrentTotal.Top = 0.125F;
			this.txtAccruedCurrentTotal.Width = 0.75F;
			// 
			// txtUsedCurrentTotal
			// 
			this.txtUsedCurrentTotal.Height = 0.1875F;
			this.txtUsedCurrentTotal.Left = 3.625F;
			this.txtUsedCurrentTotal.Name = "txtUsedCurrentTotal";
			this.txtUsedCurrentTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtUsedCurrentTotal.Text = null;
			this.txtUsedCurrentTotal.Top = 0.125F;
			this.txtUsedCurrentTotal.Width = 0.75F;
			// 
			// txtBalanceTotal
			// 
			this.txtBalanceTotal.Height = 0.1875F;
			this.txtBalanceTotal.Left = 4.5F;
			this.txtBalanceTotal.Name = "txtBalanceTotal";
			this.txtBalanceTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtBalanceTotal.Text = null;
			this.txtBalanceTotal.Top = 0.125F;
			this.txtBalanceTotal.Width = 0.8125F;
			// 
			// txtEmployeeTotal
			// 
			this.txtEmployeeTotal.Height = 0.1875F;
			this.txtEmployeeTotal.Left = 2.1875F;
			this.txtEmployeeTotal.Name = "txtEmployeeTotal";
			this.txtEmployeeTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtEmployeeTotal.Text = null;
			this.txtEmployeeTotal.Top = 0.125F;
			this.txtEmployeeTotal.Width = 0.5F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label22.Text = "Final Totals:  Number Listed";
			this.Label22.Top = 0.125F;
			this.Label22.Width = 2.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 7.8125F;
			this.Line1.X1 = 0.125F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// rptEmployeeVacationSick
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.427083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalanceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalance;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCurrentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCurrentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalanceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
