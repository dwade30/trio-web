//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCustomDeductionReports : BaseForm
	{
		public frmCustomDeductionReports()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomDeductionReports InstancePtr
		{
			get
			{
				return (frmCustomDeductionReports)Sys.GetInstance(typeof(frmCustomDeductionReports));
			}
		}

		protected frmCustomDeductionReports _InstancePtr = null;
		//=========================================================
		private void ChkAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstDeductions.Items.Count - 1; intCounter++)
			{
				lstDeductions.SetSelected(intCounter, 0 != (ChkAll.CheckState));
			}
			lstDeductions.SelectedIndex = 0;
		}

		private void frmCustomDeductionReports_Activated(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE #2628 order has changed, forced fire events
            optReportOn_CheckedChanged(0, cmbReportOn, EventArgs.Empty);
			// Call ForceFormToResize(Me)
		}

		private void frmCustomDeductionReports_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomDeductionReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomDeductionReports properties;
			//frmCustomDeductionReports.ScaleWidth	= 9045;
			//frmCustomDeductionReports.ScaleHeight	= 7575;
			//frmCustomDeductionReports.LinkTopic	= "Form1";
			//frmCustomDeductionReports.LockControls	= -1  'True;
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblDeductionSetup Order by DeductionNumber", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber"))).Length > 6)
				{
					lstDeductions.AddItem(rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(10 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber"))).Length, " ") + rsData.Get_Fields("Description"));
				}
				else
				{
					lstDeductions.AddItem(rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(7 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber"))).Length, " ") + rsData.Get_Fields("Description"));
				}
				lstDeductions.ItemData(lstDeductions.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}

            lstDeductions.Columns[0].Width = (int)(lstDeductions.Width * 0.97);
            lstDeductions.GridLineStyle = GridLineStyle.None;
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(GetSQL()))
			{
				modGlobalVariables.Statics.gboolSummaryDedCustomReport = cmbType.Text == "Summary";
				if (!(cmbReportOn.Text == "Date Range") && !(cmbReportOn.Text == "Emp / Date Range"))
				{
					modGlobalVariables.Statics.gstrShowCYTDDeductionReport = string.Empty;
				}
				else
				{
					modGlobalVariables.Statics.gstrShowCYTDDeductionReport = txtStartDate.Text + " - " + txtEndDate.Text;
				}
				if (cmbSort.Text == "Employee Number")
				{
					modGlobalVariables.Statics.gstrDeductionSummaryType = "ID";
				}
				else if (cmbSort.Text == "Employee Name")
				{
					modGlobalVariables.Statics.gstrDeductionSummaryType = "Name";
				}
				else if (cmbSort.Text == "Deduction Number")
				{
					modGlobalVariables.Statics.gstrDeductionSummaryType = "Deduction";
				}
				else if (cmbSort.Text == "Pay Date")
				{
					modGlobalVariables.Statics.gstrDeductionSummaryType = "PayDate";
				}
				if (cmbType.Text == "Deduction Setup Information")
				{
					frmReportViewer.InstancePtr.Init(rptCustomDeductionSetup.InstancePtr, boolAllowEmail: true, strAttachmentName: "DeductionSetup");
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptCustomDeductionReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "Deduction");
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object GetSQL()
		{
			object GetSQL = null;
			int intQuarter;
			if (cmbType.Text == "Deduction Setup Information")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT tblEmployeeMaster.*,tblEmployeeDeductions.* , tblemployeedeductions.id as tblemployeedeductionsID from tblEmployeeMaster INNER JOIN tblEmployeeDeductions on tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber where DeductionCode <> 0 ";
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT  tblCheckDetail.*, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE tblCheckDetail.EmployeeNumber <> '' ";
				if (!(cmbReportOn.Text == "Date Range") && !(cmbReportOn.Text == "Emp / Date Range"))
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " and tblcheckdetail.paydate between '" + "1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				}
			}
			GetSQL = false;
			if (cmbReportOn.Text == "Single Employee")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeNumber.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Deduction Setup Information")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblEmployeeMaster.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
					}
				}
			}
			else if (cmbReportOn.Text == "Date Range")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Deduction Setup Information")
					{
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					}
				}
			}
			else if (cmbReportOn.Text == "All Files")
			{
			}
			else if (cmbReportOn.Text == "Emp / Date Range")
			{
				if (txtEmployeeBoth.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeBoth.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Deduction Setup Information")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblEmployeeMaster.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
				}
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Deduction Setup Information")
					{
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					}
				}
			}
			else
			{
			}
			if (cmbType.Text == "Deduction Setup Information")
			{
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL += " AND deductionrecord = 1 AND CheckVoid = 0";
			}
			GetDeductionNumbersForFilter();
			if (!(cmbType.Text == "Deduction Setup Information"))
			{
				if (cmbSort.Text == "Employee Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblCheckDetail.EmployeeNumber,DedDeductionNumber,paydate";
				}
				else if (cmbSort.Text == "Employee Name")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,tblcheckdetail.employeenumber,DedDeductionNumber,paydate";
				}
				else if (cmbSort.Text == "Deduction Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DedDeductionNumber,LastName,FirstName,paydate";
				}
				else if (cmbSort.Text == "Pay Date")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by PayDate,DedDeductionNumber,LastName,FirstName";
				}
			}
			else
			{
				if (cmbSort.Text == "Employee Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblEmployeeMaster.EmployeeNumber,DeductionCode";
				}
				else if (cmbSort.Text == "Employee Name")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,DeductionCode";
				}
				else if (cmbSort.Text == "Deduction Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DeductionCode,LastName,FirstName";
				}
				else if (cmbSort.Text == "Pay Date")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DeductionCode,LastName,FirstName";
				}
			}
			GetSQL = true;
			return GetSQL;
		}

		private object GetDeductionNumbersForFilter()
		{
			object GetDeductionNumbersForFilter = null;
			int intCounter;
			bool boolDeductionFound = false;
			modGlobalVariables.Statics.gstrCheckListingSQL += " AND (";
			for (intCounter = 0; intCounter <= lstDeductions.Items.Count - 1; intCounter++)
			{
				if (lstDeductions.Selected(intCounter) == true)
				{
					if (boolDeductionFound)
					{
						// this is the Second deduction
						modGlobalVariables.Statics.gstrCheckListingSQL += " OR DedDeductionNumber = " + FCConvert.ToString(lstDeductions.ItemData(intCounter));
					}
					else
					{
						// this is the first Deduction
						modGlobalVariables.Statics.gstrCheckListingSQL += " DedDeductionNumber = " + FCConvert.ToString(lstDeductions.ItemData(intCounter));
					}
					boolDeductionFound = true;
				}
			}
			if (!boolDeductionFound)
			{
				// do not want any deductions if none are checked
				modGlobalVariables.Statics.gstrCheckListingSQL += "DedDeductionNumber = 9998198)";
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL += ")";
			}
			if (cmbType.Text == "Deduction Setup Information")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = modGlobalVariables.Statics.gstrCheckListingSQL.Replace("DedDeductionNumber", "DeductionCode");
			}
			return GetDeductionNumbersForFilter;
		}

		private void optReportOn_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			txtEmployeeNumber.Visible = cmbReportOn.Text == "Single Employee";
			txtStartDate.Visible = (cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Emp / Date Range");
			txtEndDate.Visible = (cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Emp / Date Range");
			lblCaption.Visible = cmbReportOn.Text != "All Files";
			// lblDash.Visible = Index = 4
			txtEmployeeNumber.Text = string.Empty;
			lblEmployeeBoth.Visible = cmbReportOn.Text == "Emp / Date Range";
			txtEmployeeBoth.Visible = cmbReportOn.Text == "Emp / Date Range";
			lblDateRange.Visible = false;
			lblCaption.Visible = true;
			switch (cmbReportOn.Text)
			{
				case "Single Employee":
					{
						lblCaption.Text = "Employee Number";
						break;
					}
				//FC:FINAL:DDU:#i2211 - deleted indexes from original
				//case 1:
				//	{
				//		lblCaption.Text = "Month of Pay Date";
				//		break;
				//	}
				//case 2:
				//	{
				//		lblCaption.Text = "Qtr. of Pay Date";
				//		break;
				//	}
				//case 3:
				//	{
				//		lblCaption.Text = "Year of Pay Date";
				//		break;
				//	}
				case "Date Range":
				case "Emp / Date Range":
					{
						lblCaption.Visible = false;
						lblDateRange.Visible = true;
						break;
					}
				case "All Files":
					{
						break;
					}
			}
			//end switch
		}

		private void optReportOn_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReportOn.SelectedIndex;
			optReportOn_CheckedChanged(index, sender, e);
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtEndDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtEndDate.Text.Length == 2 || txtEndDate.Text.Length == 5) && Strings.Right(txtEndDate.Text, 1) != "/")
        //	{
        //		txtEndDate.Text = txtEndDate.Text + "/";
        //		txtEndDate.SelectionStart = txtEndDate.Text.Length;
        //		txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //	}
        //}

        //private void txtEndDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtEndDate.SelectionStart = 0;
        //	txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //}

        //private void txtEndDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if (KeyAscii == Keys.Back)
        //	{
        //		if (txtEndDate.Text.Length > 2)
        //		{
        //			txtEndDate.Text = Strings.Left(txtEndDate.Text, txtEndDate.Text.Length - 2);
        //			txtEndDate.SelectionStart = txtEndDate.Text.Length;
        //			KeyAscii = (Keys)0;
        //		}
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        //private void txtStartDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtStartDate.Text.Length == 2 || txtStartDate.Text.Length == 5) && Strings.Right(txtStartDate.Text, 1) != "/")
        //	{
        //		txtStartDate.Text = txtStartDate.Text + "/";
        //		txtStartDate.SelectionStart = txtStartDate.Text.Length;
        //		txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //	}
        //}

        //private void txtStartDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtStartDate.SelectionStart = 0;
        //	txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //}

        //private void txtStartDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if (KeyAscii == Keys.Back)
        //	{
        //		if (txtStartDate.Text.Length > 2)
        //		{
        //			txtStartDate.Text = Strings.Left(txtStartDate.Text, txtStartDate.Text.Length - 2);
        //			txtStartDate.SelectionStart = txtStartDate.Text.Length;
        //			KeyAscii = (Keys)0;
        //		}
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
