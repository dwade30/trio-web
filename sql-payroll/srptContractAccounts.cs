﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptContractAccounts.
	/// </summary>
	public partial class srptContractAccounts : FCSectionReport
	{
		public srptContractAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptContractAccounts InstancePtr
		{
			get
			{
				return (srptContractAccounts)Sys.GetInstance(typeof(srptContractAccounts));
			}
		}

		protected srptContractAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                conCT = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptContractAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngContractID;
		clsContract conCT = new clsContract();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = conCT.EndOfAccounts();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dtAsOf As DateTime	OnWrite(int, string)
			DateTime dtAsOf;
			string[] strAry = null;
			dtAsOf = DateTime.FromOADate(0);
			strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
			lngContractID = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			if (Information.UBound(strAry, 1) > 0)
			{
				if (Information.IsDate(strAry[1]))
				{
					dtAsOf = FCConvert.ToDateTime(Strings.Format(strAry[1], "MM/dd/yyyy"));
				}
			}
			if (lngContractID > 0)
			{
				conCT.LoadContract(lngContractID, null, dtAsOf);
			}
			else
			{
				this.Stop();
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!conCT.EndOfAccounts())
			{
				txtAccount.Text = conCT.CurAccount;
				txtAmount.Text = Strings.Format(conCT.CurAccountOriginalAmount, "#,###,##0.00");
				txtPaid.Text = Strings.Format(conCT.CurAccountPaid, "#,###,##0.00");
				txtRemaining.Text = Strings.Format(conCT.CurAccountOriginalAmount - conCT.CurAccountPaid, "#,###,##0.00");
				conCT.MoveNextAccount();
			}
		}

		
	}
}
