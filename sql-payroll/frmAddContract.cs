//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmAddContract : BaseForm
	{
		public frmAddContract()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_3, 1);
			this.Label1.AddControlArrayElement(Label1_4, 2);
			cmbLoadBack.Text = "No";
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmAddContract InstancePtr
		{
			get
			{
				return (frmAddContract)Sys.GetInstance(typeof(frmAddContract));
			}
		}

		protected frmAddContract _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngContractID;
		private string strCurEmployee = string.Empty;
		const int CNSTGRIDACCOUNTSCOLID = 0;
		const int CNSTGRIDACCOUNTSCOLCONTRACTID = 1;
		const int CNSTGRIDACCOUNTSCOLACCOUNT = 2;
		const int CNSTGRIDACCOUNTSCOLORIGINALAMOUNT = 3;
		const int CNSTGRIDACCOUNTSCOLPAID = 4;
		const int CNSTGRIDACCOUNTSCOLLEFT = 5;
		const int CNSTGRIDPAYCOLPAYDATE = 1;
		const int CNSTGRIDPAYCOLACCOUNT = 0;
		const int CNSTGRIDPAYCOLGROSS = 2;
		const int CNSTGRIDPAYUSE = 3;
		private bool boolAllUse;
		private clsGridAccount clsGA = new clsGridAccount();

		public int Init(string strEmployee)
		{
			int Init = 0;
			Init = 0;
			lngContractID = 0;
			strCurEmployee = strEmployee;
			boolAllUse = false;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngContractID;
			return Init;
		}

		private void cmd1Cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmd1Next_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtDescription.Text) == string.Empty)
			{
				MessageBox.Show("You must enter a description for the contract", "No Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (!Information.IsDate(t2kStartDate.Text))
			{
				MessageBox.Show("You must enter a valid start date for the contract", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				t2kStartDate.Focus();
				return;
			}
			if (Information.IsDate(t2kEndDate.Text))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(t2kStartDate.Text), FCConvert.ToDateTime(t2kEndDate.Text)) < 0)
				{
					MessageBox.Show("The end date cannot be before the start date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					t2kEndDate.Focus();
					return;
				}
			}
			Frame2.Visible = true;
			cmd1Next.Visible = false;
			//cmd1Cancel.Visible = false;
			Frame1.Enabled = false;
		}

		private void cmd2Back_Click(object sender, System.EventArgs e)
		{
			Frame2.Visible = false;
			Frame1.Visible = true;
			cmd1Next.Visible = true;
			//cmd1Cancel.Visible = true;
			Frame1.Enabled = true;
		}

		private void SetupGridAccounts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridAccounts.Cols = 6;
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLID, true);
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLCONTRACTID, true);
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLACCOUNT, "Account");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "Amount");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, "Paid");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "Remaining");
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLPAID, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLLEFT, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridTotals.Cols = GridAccounts.Cols;
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLACCOUNT, "Total");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, "0.00");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
                GridTotals.ColAlignment(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridTotals.ColAlignment(CNSTGRIDACCOUNTSCOLPAID, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridTotals.ColAlignment(CNSTGRIDACCOUNTSCOLLEFT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				GridTotals.ColHidden(CNSTGRIDACCOUNTSCOLID, true);
				GridTotals.ColHidden(CNSTGRIDACCOUNTSCOLCONTRACTID, true);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridAccounts()
		{
			int GridWidth = 0;
			GridWidth = GridAccounts.WidthOriginal;
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLACCOUNT, FCConvert.ToInt32(0.4 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLACCOUNT, FCConvert.ToInt32(0.4 * GridWidth));
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.2 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.2 * GridWidth));
			GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToInt32(0.2 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToInt32(0.2 * GridWidth));
			//GridTotals.Height = GridTotals.RowHeight(0) + 60;
		}

		private void cmd2Cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmd2Next_Click(object sender, System.EventArgs e)
		{
			Frame2.Visible = false;
			if (cmbLoadBack.Text == "No")
			{
				// no
				Frame4.Visible = true;
				//mnuSaveExit.Enabled = true;
                cmdSaveExit.Enabled = true;
                GridPay.Rows = 1;
			}
			else
			{
				// yes
				Frame3.Visible = true;
				GridPay.Rows = 1;
				clsDRWrapper rsLoad = new clsDRWrapper();
				// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
				DateTime dtStart;
				// vbPorter upgrade warning: dtEnd As DateTime	OnWrite(string, DateTime)
				DateTime dtEnd;
				dtStart = FCConvert.ToDateTime(t2kStartDate.Text);
				if (Information.IsDate(t2kEndDate.Text))
				{
					dtEnd = FCConvert.ToDateTime(t2kEndDate.Text);
				}
				else
				{
					dtEnd = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				string strSQL = "";
				int lngRow;
				strSQL = "select distaccountnumber,sum(distgrosspay) as totpay,paydate from tblcheckdetail where employeenumber = '" + strCurEmployee + "' and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' and checkvoid = 0 and distributionrecord = 1 and convert(int, isnull(contractid, 0)) = 0 group by distaccountnumber,paydate order by distaccountnumber,paydate";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					GridPay.Rows += 1;
					lngRow = GridPay.Rows - 1;
					GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLACCOUNT, FCConvert.ToString(rsLoad.Get_Fields("distaccountnumber")));
					GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLGROSS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totpay"))));
					GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLPAYDATE, Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
					GridPay.TextMatrix(lngRow, CNSTGRIDPAYUSE, FCConvert.ToString(false));
					rsLoad.MoveNext();
				}
			}
		}

		private void cmd3Back_Click(object sender, System.EventArgs e)
		{
			Frame2.Visible = true;
			Frame3.Visible = false;
		}

		private void cmd3Cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmd3Next_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridAccounts.Rows = 1;
				int lngRow;
				int lngAcctRow;
				string strLastAccount;
				double dblAmount;
				double dblTotal;
				lngAcctRow = 0;
				strLastAccount = "";
				dblAmount = 0;
				dblTotal = 0;
				for (lngRow = 1; lngRow <= GridPay.Rows - 1; lngRow++)
				{
					if (FCConvert.CBool(GridPay.TextMatrix(lngRow, CNSTGRIDPAYUSE)))
					{
						if (strLastAccount != GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLACCOUNT))
						{
							GridAccounts.Rows += 1;
							lngAcctRow = GridAccounts.Rows - 1;
							GridAccounts.TextMatrix(lngAcctRow, CNSTGRIDACCOUNTSCOLACCOUNT, GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLACCOUNT));
							dblAmount = 0;
							GridAccounts.TextMatrix(lngAcctRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
							GridAccounts.TextMatrix(lngAcctRow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
							strLastAccount = GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLACCOUNT);
						}
						dblAmount += Conversion.Val(GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLGROSS));
						dblTotal += Conversion.Val(GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLGROSS));
						GridAccounts.TextMatrix(lngAcctRow, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblAmount));
						GridAccounts.RowData(lngAcctRow, true);
					}
				}
				// lngRow
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblTotal));
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
				GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
				Frame4.Visible = true;
				//mnuSaveExit.Enabled = true;
                cmdSaveExit.Enabled = true;
                Frame3.Visible = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmd3Next_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmd4Back_Click(object sender, System.EventArgs e)
		{
			Frame4.Visible = false;
			//mnuSaveExit.Enabled = false;
            cmdSaveExit.Enabled = false;
            if (cmbLoadBack.Text == "No")
			{
				Frame2.Visible = true;
			}
			else
			{
				Frame3.Visible = true;
			}
		}

		private void cmd4Cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmd4Next_Click(object sender, System.EventArgs e)
		{
			if (GridAccounts.Rows < 2)
			{
				MessageBox.Show("You must add at least one account", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			int lngRow;
			double dblOriginal;
			double dblPaid;
			string strLastAccount;
			dblOriginal = 0;
			dblPaid = 0;
			GridAccounts.Col = CNSTGRIDACCOUNTSCOLACCOUNT;
			GridAccounts.Sort = FCGrid.SortSettings.flexSortStringAscending;
			strLastAccount = "";
			for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
			{
				dblOriginal += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
				dblPaid += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID));
				if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)) <= 0)
				{
					MessageBox.Show("Contract amounts must be greater than 0", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.Trim(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT)) == string.Empty || fecherFoundation.Strings.Trim(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT)).Length < 4)
				{
					MessageBox.Show("You must enter valid accounts in all entries", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID)) > Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)))
				{
					MessageBox.Show("Paid amounts cannot be greater than original amounts", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (strLastAccount == GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT))
				{
					MessageBox.Show("Cannot have more than one instance of an account in the same contract", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					strLastAccount = GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT);
				}
			}
			// lngRow
			if (CreateContract())
			{
				Close();
			}
		}

		public void cmd4Next_Click()
		{
			cmd4Next_Click(cmd4Next, new System.EventArgs());
		}

		private void frmAddContract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmAddContract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAddContract properties;
			//frmAddContract.FillStyle	= 0;
			//frmAddContract.ScaleWidth	= 9300;
			//frmAddContract.ScaleHeight	= 7620;
			//frmAddContract.LinkTopic	= "Form2";
			//frmAddContract.LockControls	= -1  'True;
			//frmAddContract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridAccounts();
			SetupGridPay();
			clsGA.GRID7Light = GridAccounts;
			//if (!modValidateAccount.Statics.gboolSchoolAccounts)
			//{
				clsGA.DefaultAccountType = "E";
			//}
			//else
			//{
			//	clsGA.DefaultAccountType = "P";
			//}
			clsGA.OnlyAllowDefaultType = false;
			clsGA.AccountCol = CNSTGRIDACCOUNTSCOLACCOUNT;
			clsGA.Validation = true;
			clsGA.AllowSplits = modRegionalTown.IsRegionalTown();
		}

		private void frmAddContract_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPay();
			ResizeGridAccounts();
		}

		private void GridAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// VB6 Bad Scope Dim:
			int lngRow = 0;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						GridAccounts.Rows += 1;
						lngRow = GridAccounts.Rows - 1;
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "0.00");
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
						GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID, "0.00");
						GridAccounts.RowData(lngRow, false);
						GridAccounts.TopRow = lngRow;
						break;
					}
				case Keys.Delete:
					{
						if (GridAccounts.Row > 0)
						{
							if (!FCConvert.ToBoolean(GridAccounts.RowData(GridAccounts.Row)))
							{
								KeyCode = 0;
								GridAccounts.RemoveItem(GridAccounts.Row);
								ReAddAccounts();
							}
							else
							{
								KeyCode = 0;
								MessageBox.Show("Cannot delete automatically created rows", "Invalid Operation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						break;
					}
			}
			//end switch
		}

		private void ReAddAccounts()
		{
			int lngRow;
			double dblOriginal;
			double dblLeft;
			double dblPaid;
			//App.DoEvents();
			dblOriginal = 0;
			dblLeft = 0;
			dblPaid = 0;
			for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
			{
				dblOriginal += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
				dblPaid += Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID));
			}
			// lngRow
			dblLeft = dblOriginal - dblPaid;
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToString(dblOriginal));
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblPaid));
			GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, FCConvert.ToString(dblLeft));
		}

		private void GridAccounts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridAccounts[e.ColumnIndex, e.RowIndex];
            int lngRow;
			lngRow = GridAccounts.GetFlexRowIndex(e.RowIndex);
			// If lngRow <> CNSTGRIDACCOUNTSCOLACCOUNT Then
			//ToolTip1.SetToolTip(GridAccounts, "Use Insert and Delete to add and remove accounts");
			cell.ToolTipText = "Use Insert and Delete to add and remove accounts";
			// End If
		}

		private void GridAccounts_RowColChange(object sender, System.EventArgs e)
		{
			if (GridAccounts.Row > 0)
			{
				if (FCConvert.ToBoolean(GridAccounts.RowData(GridAccounts.Row)))
				{
					if (GridAccounts.Col == CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (GridAccounts.Col == CNSTGRIDACCOUNTSCOLORIGINALAMOUNT || GridAccounts.Col == CNSTGRIDACCOUNTSCOLACCOUNT)
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						GridAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
			}
		}

		private void GridAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridAccounts.Row < 1)
				return;
			double dblPaid = 0;
			double dblOriginal = 0;
			switch (GridAccounts.Col)
			{
				case CNSTGRIDACCOUNTSCOLORIGINALAMOUNT:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.EditText);
						GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToString(dblOriginal));
						break;
					}
				case CNSTGRIDACCOUNTSCOLPAID:
					{
						dblPaid = Conversion.Val(GridAccounts.EditText);
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToString(dblPaid));
						break;
					}
				case CNSTGRIDACCOUNTSCOLLEFT:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						GridAccounts.EditText = FCConvert.ToString(dblOriginal - dblPaid);
						break;
					}
				default:
					{
						dblPaid = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLPAID));
						dblOriginal = Conversion.Val(GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT));
						break;
					}
			}
			//end switch
			GridAccounts.TextMatrix(GridAccounts.Row, CNSTGRIDACCOUNTSCOLLEFT, FCConvert.ToString(dblOriginal - dblPaid));
			ReAddAccounts();
		}

		private void GridPay_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			intCol = GridPay.MouseCol;
			lngRow = GridPay.MouseRow;
			if (lngRow == 0)
			{
				if (intCol == CNSTGRIDPAYUSE)
				{
					for (lngRow = 1; lngRow <= GridPay.Rows - 1; lngRow++)
					{
						GridPay.TextMatrix(lngRow, CNSTGRIDPAYUSE, FCConvert.ToString(!boolAllUse));
					}
					// lngRow
				}
				boolAllUse = !boolAllUse;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmd4Next_Click();
		}

		private void t2kStartDate_Change(object sender, System.EventArgs e)
		{
			if (Frame1.Visible == true && cmd1Next.Visible == true)
			{
				if (Information.IsDate(t2kStartDate.Text))
				{
					if (fecherFoundation.Strings.Trim(txtDescription.Text) != string.Empty)
					{
						cmd1Next.Enabled = true;
					}
					else
					{
						cmd1Next.Enabled = false;
					}
				}
				else
				{
					cmd1Next.Enabled = false;
				}
			}
		}

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			if (Frame1.Visible == true && cmd1Next.Visible == true)
			{
				if (fecherFoundation.Strings.Trim(txtDescription.Text) != string.Empty)
				{
					if (Information.IsDate(t2kStartDate.Text))
					{
						cmd1Next.Enabled = true;
					}
				}
				else
				{
					cmd1Next.Enabled = false;
				}
			}
		}

		private void SetupGridPay()
		{
			GridPay.Rows = 1;
			GridPay.TextMatrix(0, CNSTGRIDPAYCOLPAYDATE, "PayDate");
			GridPay.TextMatrix(0, CNSTGRIDPAYCOLACCOUNT, "Account");
			GridPay.TextMatrix(0, CNSTGRIDPAYCOLGROSS, "Amount");
			GridPay.TextMatrix(0, CNSTGRIDPAYUSE, "");
            GridPay.ColDataType(CNSTGRIDPAYUSE, FCGrid.DataTypeSettings.flexDTBoolean);
        }

        private void ResizeGridPay()
		{
			int GridWidth = 0;
			GridWidth = GridPay.WidthOriginal;
			GridPay.ColWidth(CNSTGRIDPAYCOLACCOUNT, FCConvert.ToInt32(0.45 * GridWidth));
			GridPay.ColWidth(CNSTGRIDPAYCOLPAYDATE, FCConvert.ToInt32(0.2 * GridWidth));
			GridPay.ColWidth(CNSTGRIDPAYCOLGROSS, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private bool CreateContract()
		{
			bool CreateContract = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CreateContract = false;
				bool boolEncumber;
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsLoad = new clsDRWrapper();
				double dblEncumber = 0;
				int lngRow;
				boolEncumber = false;
				rsSave.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
				if (rsSave.Get_Fields_Boolean("EncumberContracts") && modGlobalConstants.Statics.gboolBD)
				{
					boolEncumber = true;
				}
				rsSave.OpenRecordset("select * from contracts where ID = " + FCConvert.ToString(lngContractID), "twpy0000.vb1");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("employeenumber", strCurEmployee);
				rsSave.Set_Fields("description", txtDescription.Text);
				rsSave.Set_Fields("startdate", t2kStartDate.Text);
				if (Information.IsDate(t2kEndDate.Text))
				{
					rsSave.Set_Fields("enddate", t2kEndDate.Text);
				}
				else
				{
					rsSave.Set_Fields("enddate", 0);
				}
				rsSave.Set_Fields("vendor", 0);
				rsSave.Set_Fields("originalamount", FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT))));
				rsSave.Set_Fields("paid", FCConvert.ToString(Conversion.Val(GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID))));
				rsSave.Set_Fields("encumbrancejournal", 0);
				rsSave.Update();
				lngContractID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
				// add account entries
				rsSave.Execute("delete from contractaccounts where contractid = " + FCConvert.ToString(lngContractID), "twpy0000.vb1");
				rsSave.OpenRecordset("select * from contractaccounts where id = 0", "twpy0000.vb1");
				for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
				{
					rsSave.AddNew();
					rsSave.Set_Fields("contractid", lngContractID);
					rsSave.Set_Fields("account", GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT));
					rsSave.Set_Fields("originalamount", FCConvert.ToString(Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT))));
					rsSave.Set_Fields("Paid", FCConvert.ToString(Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID))));
					rsSave.Update();
				}
				// lngRow
				// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
				DateTime dtStart;
				// vbPorter upgrade warning: dtEnd As DateTime	OnWrite(string, DateTime)
				DateTime dtEnd;
				dtStart = FCConvert.ToDateTime(t2kStartDate.Text);
				if (Information.IsDate(t2kEndDate.Text))
				{
					dtEnd = FCConvert.ToDateTime(t2kEndDate.Text);
				}
				else
				{
					dtEnd = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				if (cmbLoadBack.Text == "Yes")
				{
					for (lngRow = 1; lngRow <= GridPay.Rows - 1; lngRow++)
					{
						if (FCConvert.CBool(GridPay.TextMatrix(lngRow, CNSTGRIDPAYUSE)))
						{
							rsSave.Execute("update tblcheckdetail set contractid = " + FCConvert.ToString(lngContractID) + " where employeenumber = '" + strCurEmployee + "' and paydate = '" + GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLPAYDATE) + "' and checkvoid = 0 and distributionrecord = 1 and distaccountnumber = '" + GridPay.TextMatrix(lngRow, CNSTGRIDPAYCOLACCOUNT) + "'", "twpy0000.vb1");
						}
					}
					// lngRow
				}
				if (boolEncumber)
				{
					int lngJournal = 0;
					string[] strAry = null;
					int intTemp = 0;
					dblEncumber = Conversion.Val(GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)) - Conversion.Val(GridTotals.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID));
					if (dblEncumber > 0)
					{
						string strTemp = "";
						if (fecherFoundation.Strings.Trim(txtDescription.Text) != string.Empty)
						{
							strTemp = fecherFoundation.Strings.Trim(Strings.Left(txtDescription.Text + Strings.StrDup(25, " "), 25));
						}
						else
						{
							strTemp = fecherFoundation.Strings.Trim(Strings.Left("PY Contract Emp " + strCurEmployee + Strings.StrDup(25, " "), 25));
						}
						lngJournal = modCoreysSweeterCode.CreateAJournal(strTemp, 0, "EN");
						if (lngJournal > 0)
						{
							int lngEncumbrance = 0;
							rsLoad.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
							rsSave.OpenRecordset("select * from encumbrances where ID = 0", "twbd0000.vb1");
							rsSave.AddNew();
							rsSave.Set_Fields("JournalNumber", lngJournal);
							rsSave.Set_Fields("Date", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							rsSave.Set_Fields("PO", lngContractID);
							rsSave.Set_Fields("Status", "E");
							if (fecherFoundation.Strings.Trim(txtDescription.Text) != string.Empty)
							{
								rsSave.Set_Fields("description", fecherFoundation.Strings.Trim(Strings.Left(txtDescription.Text + Strings.StrDup(25, " "), 25)));
							}
							else
							{
								rsSave.Set_Fields("description", fecherFoundation.Strings.Trim(Strings.Left("PY Contract Emp " + strCurEmployee + Strings.StrDup(25, " "), 25)));
							}
							rsSave.Set_Fields("period", DateTime.Today.Month);
							rsSave.Set_Fields("amount", dblEncumber);
							rsSave.Set_Fields("tempvendorname", fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("middlename")) + " " + rsLoad.Get_Fields("lastname") + rsLoad.Get_Fields("desig")));
							rsSave.Set_Fields("tempvendoraddress1", rsLoad.Get_Fields("address1"));
							rsSave.Set_Fields("tempvendoraddress2", rsLoad.Get_Fields("address2"));
							rsSave.Set_Fields("tempvendorcity", rsLoad.Get_Fields("city"));
							strAry = Strings.Split(FCConvert.ToString(rsLoad.Get_Fields_String("zip")), "-", -1, CompareConstants.vbTextCompare);
							rsSave.Set_Fields("tempvendorzip", strAry[0]);
							if (Information.UBound(strAry, 1) > 0)
							{
								rsSave.Set_Fields("tempvendorzip4", strAry[1]);
							}
							intTemp = FCConvert.ToInt16(rsLoad.Get_Fields("state"));
							rsLoad.OpenRecordset("select * from states  where id = " + FCConvert.ToString(intTemp), "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								rsSave.Set_Fields("tempvendorstate", rsLoad.Get_Fields("state"));
							}
							rsSave.Update();
							lngEncumbrance = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							double dblLeft = 0;
							// individual entries
							for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
							{
								dblLeft = Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT)) - Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLPAID));
								if (dblLeft > 0)
								{
									rsSave.OpenRecordset("select * from encumbrancedetail where ID = 0", "twbd0000.vb1");
									rsSave.AddNew();
									rsSave.Set_Fields("EncumbranceID", lngEncumbrance);
									rsSave.Set_Fields("Account", GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLACCOUNT));
									rsSave.Set_Fields("amount", dblLeft);
									rsSave.Set_Fields("description", "PY Contract " + FCConvert.ToString(lngContractID));
									rsSave.Update();
								}
							}
							// lngRow
							rsSave.Execute("update contracts set encumbrancejournal = " + FCConvert.ToString(lngJournal) + ",encumbranceid = " + FCConvert.ToString(lngEncumbrance) + " where ID = " + FCConvert.ToString(lngContractID), "TWPY0000.VB1");
							rsSave.Execute("update journalmaster set totalamount = " + FCConvert.ToString(dblEncumber) + " where journalnumber = " + FCConvert.ToString(lngJournal), "twbd0000.vb1");
							MessageBox.Show("Created journal number " + FCConvert.ToString(lngJournal), "Created Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				CreateContract = true;
				return CreateContract;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateContract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateContract;
		}
	}
}
