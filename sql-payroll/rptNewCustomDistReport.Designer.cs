﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewCustomDistReport.
	/// </summary>
	partial class rptNewCustomDistReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewCustomDistReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGrossPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroup1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblGroup1Hours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGroup1Gross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroup2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup2Hours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup2GrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroup2Employees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup1Hours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup1GrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroup1Employees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotEmployees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroup1Hours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroup1Gross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2Hours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2GrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroup2Employees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1Hours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1GrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroup1Employees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotEmployees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmpNo,
				this.txtEmployeeName,
				this.txtHours,
				this.txtGrossPay
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotHours,
				this.txtTotGross,
				this.Line3,
				this.Label3,
				this.fldTotEmployees,
				this.Field8
			});
			this.ReportFooter.Height = 0.1979167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.lblHours,
				this.lblGrossPay
			});
			this.PageHeader.Height = 0.6666667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup1,
				this.lblGroup1Hours,
				this.lblGroup1Gross
			});
			this.GroupHeader1.Height = 0.21875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup1Hours,
				this.txtGroup1GrossPay,
				this.Line2,
				this.Field2,
				this.fldGroup1Employees,
				this.Field6
			});
			this.GroupFooter1.Height = 0.1979167F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			//
			// 
			this.GroupHeader2.DataField = "grpHeader2";
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup2,
				this.Label1,
				this.Label2
			});
			this.GroupHeader2.Height = 0.2083333F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter2
			//
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup2Hours,
				this.txtGroup2GrossPay,
				this.Line1,
				this.Field1,
				this.fldGroup2Employees,
				this.Field4
			});
			this.GroupFooter2.Height = 0.1875F;
			this.GroupFooter2.KeepTogether = true;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.270833F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Distribution Summary Report";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.395833F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.270833F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblHours
			// 
			this.lblHours.Height = 0.1666667F;
			this.lblHours.HyperLink = null;
			this.lblHours.Left = 4.916667F;
			this.lblHours.Name = "lblHours";
			this.lblHours.Style = "font-weight: bold; text-align: right";
			this.lblHours.Text = "Hours";
			this.lblHours.Top = 0.5F;
			this.lblHours.Width = 1F;
			// 
			// lblGrossPay
			// 
			this.lblGrossPay.Height = 0.1666667F;
			this.lblGrossPay.HyperLink = null;
			this.lblGrossPay.Left = 6.333333F;
			this.lblGrossPay.Name = "lblGrossPay";
			this.lblGrossPay.Style = "font-weight: bold; text-align: right";
			this.lblGrossPay.Text = "Gross Pay";
			this.lblGrossPay.Top = 0.5F;
			this.lblGrossPay.Width = 1F;
			// 
			// txtGroup1
			// 
			this.txtGroup1.DataField = "Group1DField";
			this.txtGroup1.Height = 0.1666667F;
			this.txtGroup1.Left = 0F;
			this.txtGroup1.Name = "txtGroup1";
			this.txtGroup1.Style = "font-weight: bold";
			this.txtGroup1.Text = "Field1";
			this.txtGroup1.Top = 0F;
			this.txtGroup1.Width = 4F;
			// 
			// lblGroup1Hours
			// 
			this.lblGroup1Hours.Height = 0.1666667F;
			this.lblGroup1Hours.HyperLink = null;
			this.lblGroup1Hours.Left = 4.916667F;
			this.lblGroup1Hours.Name = "lblGroup1Hours";
			this.lblGroup1Hours.Style = "font-weight: bold; text-align: right";
			this.lblGroup1Hours.Text = "Hours";
			this.lblGroup1Hours.Top = 0F;
			this.lblGroup1Hours.Visible = false;
			this.lblGroup1Hours.Width = 1F;
			// 
			// lblGroup1Gross
			// 
			this.lblGroup1Gross.Height = 0.1666667F;
			this.lblGroup1Gross.HyperLink = null;
			this.lblGroup1Gross.Left = 6.333333F;
			this.lblGroup1Gross.Name = "lblGroup1Gross";
			this.lblGroup1Gross.Style = "font-weight: bold; text-align: right";
			this.lblGroup1Gross.Text = "Gross Pay";
			this.lblGroup1Gross.Top = 0F;
			this.lblGroup1Gross.Visible = false;
			this.lblGroup1Gross.Width = 1F;
			// 
			// txtGroup2
			// 
			this.txtGroup2.DataField = "Group2DField";
			this.txtGroup2.Height = 0.1666667F;
			this.txtGroup2.Left = 0.25F;
			this.txtGroup2.Name = "txtGroup2";
			this.txtGroup2.Style = "font-weight: bold";
			this.txtGroup2.Text = "Field1";
			this.txtGroup2.Top = 0F;
			this.txtGroup2.Width = 4.583333F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 4.916667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Hours";
			this.Label1.Top = 0F;
			this.Label1.Visible = false;
			this.Label1.Width = 1F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 6.333333F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Text = "Gross Pay";
			this.Label2.Top = 0F;
			this.Label2.Visible = false;
			this.Label2.Width = 1F;
			// 
			// txtEmpNo
			// 
			this.txtEmpNo.Height = 0.1666667F;
			this.txtEmpNo.Left = 0.4166667F;
			this.txtEmpNo.Name = "txtEmpNo";
			this.txtEmpNo.Text = "Field1";
			this.txtEmpNo.Top = 0F;
			this.txtEmpNo.Width = 0.75F;
			// 
			// txtEmployeeName
			// 
			this.txtEmployeeName.Height = 0.1666667F;
			this.txtEmployeeName.Left = 1.25F;
			this.txtEmployeeName.Name = "txtEmployeeName";
			this.txtEmployeeName.Text = "Field1";
			this.txtEmployeeName.Top = 0F;
			this.txtEmployeeName.Width = 3.583333F;
			// 
			// txtHours
			// 
			this.txtHours.Height = 0.1666667F;
			this.txtHours.Left = 4.916667F;
			this.txtHours.Name = "txtHours";
			this.txtHours.Style = "text-align: right";
			this.txtHours.Text = "Field1";
			this.txtHours.Top = 0F;
			this.txtHours.Width = 1F;
			// 
			// txtGrossPay
			// 
			this.txtGrossPay.Height = 0.1666667F;
			this.txtGrossPay.Left = 6.25F;
			this.txtGrossPay.Name = "txtGrossPay";
			this.txtGrossPay.Style = "text-align: right";
			this.txtGrossPay.Text = "Field1";
			this.txtGrossPay.Top = 0F;
			this.txtGrossPay.Width = 1.083333F;
			// 
			// txtGroup2Hours
			// 
			this.txtGroup2Hours.Height = 0.1666667F;
			this.txtGroup2Hours.Left = 4.916667F;
			this.txtGroup2Hours.Name = "txtGroup2Hours";
			this.txtGroup2Hours.Style = "text-align: right";
			this.txtGroup2Hours.Text = "Field1";
			this.txtGroup2Hours.Top = 0F;
			this.txtGroup2Hours.Width = 1F;
			// 
			// txtGroup2GrossPay
			// 
			this.txtGroup2GrossPay.Height = 0.1666667F;
			this.txtGroup2GrossPay.Left = 6.25F;
			this.txtGroup2GrossPay.Name = "txtGroup2GrossPay";
			this.txtGroup2GrossPay.Style = "text-align: right";
			this.txtGroup2GrossPay.Text = "Field1";
			this.txtGroup2GrossPay.Top = 0F;
			this.txtGroup2GrossPay.Width = 1.083333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.833333F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 2.583333F;
			this.Line1.X1 = 4.833333F;
			this.Line1.X2 = 7.416667F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// Field1
			// 
			this.Field1.DataField = "Group2TField";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 2.59375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold; text-align: right";
			this.Field1.Text = "Total";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.15625F;
			// 
			// fldGroup2Employees
			// 
			this.fldGroup2Employees.Height = 0.19F;
			this.fldGroup2Employees.Left = 1.90625F;
			this.fldGroup2Employees.Name = "fldGroup2Employees";
			this.fldGroup2Employees.Style = "text-align: right";
			this.fldGroup2Employees.Text = "Field1";
			this.fldGroup2Employees.Top = 0F;
			this.fldGroup2Employees.Width = 0.53125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 0.53125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-weight: bold; text-align: right";
			this.Field4.Text = "# of Employees";
			this.Field4.Top = 0F;
			this.Field4.Width = 1.34375F;
			// 
			// txtGroup1Hours
			// 
			this.txtGroup1Hours.Height = 0.1666667F;
			this.txtGroup1Hours.Left = 4.916667F;
			this.txtGroup1Hours.Name = "txtGroup1Hours";
			this.txtGroup1Hours.Style = "text-align: right";
			this.txtGroup1Hours.Text = "Field1";
			this.txtGroup1Hours.Top = 0F;
			this.txtGroup1Hours.Width = 1F;
			// 
			// txtGroup1GrossPay
			// 
			this.txtGroup1GrossPay.Height = 0.1666667F;
			this.txtGroup1GrossPay.Left = 6.25F;
			this.txtGroup1GrossPay.Name = "txtGroup1GrossPay";
			this.txtGroup1GrossPay.Style = "text-align: right";
			this.txtGroup1GrossPay.Text = "Field1";
			this.txtGroup1GrossPay.Top = 0F;
			this.txtGroup1GrossPay.Width = 1.083333F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.833333F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 2.583333F;
			this.Line2.X1 = 4.833333F;
			this.Line2.X2 = 7.416667F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Field2
			// 
			this.Field2.DataField = "Group1TField";
			this.Field2.Height = 0.19F;
			this.Field2.Left = 2.59375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold; text-align: right";
			this.Field2.Text = "Total";
			this.Field2.Top = 0F;
			this.Field2.Width = 2.15625F;
			// 
			// fldGroup1Employees
			// 
			this.fldGroup1Employees.Height = 0.19F;
			this.fldGroup1Employees.Left = 1.90625F;
			this.fldGroup1Employees.Name = "fldGroup1Employees";
			this.fldGroup1Employees.Style = "text-align: right";
			this.fldGroup1Employees.Text = "Field1";
			this.fldGroup1Employees.Top = 0F;
			this.fldGroup1Employees.Width = 0.53125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 0.53125F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-weight: bold; text-align: right";
			this.Field6.Text = "# of Employees";
			this.Field6.Top = 0F;
			this.Field6.Width = 1.34375F;
			// 
			// txtTotHours
			// 
			this.txtTotHours.Height = 0.1666667F;
			this.txtTotHours.Left = 4.916667F;
			this.txtTotHours.Name = "txtTotHours";
			this.txtTotHours.Style = "text-align: right";
			this.txtTotHours.Text = "Field1";
			this.txtTotHours.Top = 0F;
			this.txtTotHours.Width = 1F;
			// 
			// txtTotGross
			// 
			this.txtTotGross.Height = 0.1666667F;
			this.txtTotGross.Left = 6.25F;
			this.txtTotGross.Name = "txtTotGross";
			this.txtTotGross.Style = "text-align: right";
			this.txtTotGross.Text = "Field1";
			this.txtTotGross.Top = 0F;
			this.txtTotGross.Width = 1.083333F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.833333F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.583333F;
			this.Line3.X1 = 4.833333F;
			this.Line3.X2 = 7.416667F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.083333F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Total";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.6666667F;
			// 
			// fldTotEmployees
			// 
			this.fldTotEmployees.Height = 0.19F;
			this.fldTotEmployees.Left = 1.90625F;
			this.fldTotEmployees.Name = "fldTotEmployees";
			this.fldTotEmployees.Style = "text-align: right";
			this.fldTotEmployees.Text = "Field1";
			this.fldTotEmployees.Top = 0F;
			this.fldTotEmployees.Width = 0.53125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 0.53125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-weight: bold; text-align: right";
			this.Field8.Text = "# of Employees";
			this.Field8.Top = 0F;
			this.Field8.Width = 1.34375F;
			// 
			// rptNewCustomDistReport
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.427083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroup1Hours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroup1Gross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2Hours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup2GrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroup2Employees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1Hours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1GrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroup1Employees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotEmployees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmpNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGross;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotEmployees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroup1Hours;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroup1Gross;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup1Hours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup1GrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroup1Employees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup2Hours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup2GrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroup2Employees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
	}
}
