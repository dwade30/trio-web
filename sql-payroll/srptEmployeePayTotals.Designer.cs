﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeePayTotals.
	/// </summary>
	partial class srptEmployeePayTotals
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptEmployeePayTotals));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalFYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrentPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalMTDPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalFYTDPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCalendarPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalQTDPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentFedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentFicaWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFicaWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFicaWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFicaWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFicaWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDMedWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDStateWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentFicaGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFicaGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFicaGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFicaGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFicaGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrentPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTDPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFYTDPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendarPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTDPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFicaWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFicaGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDescription,
            this.txtCurrent,
            this.txtMTD,
            this.txtFiscal,
            this.txtCalendar,
            this.txtQTD});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 0.1875F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 8pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.375F;
			// 
			// txtCurrent
			// 
			this.txtCurrent.Height = 0.1875F;
			this.txtCurrent.Left = 1.6875F;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.OutputFormat = resources.GetString("txtCurrent.OutputFormat");
			this.txtCurrent.Style = "font-size: 8pt; text-align: right";
			this.txtCurrent.Text = null;
			this.txtCurrent.Top = 0F;
			this.txtCurrent.Width = 0.6875F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.1875F;
			this.txtMTD.Left = 2.5F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.OutputFormat = resources.GetString("txtMTD.OutputFormat");
			this.txtMTD.Style = "font-size: 8pt; text-align: right";
			this.txtMTD.Text = null;
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.6875F;
			// 
			// txtFiscal
			// 
			this.txtFiscal.Height = 0.1875F;
			this.txtFiscal.Left = 4F;
			this.txtFiscal.Name = "txtFiscal";
			this.txtFiscal.OutputFormat = resources.GetString("txtFiscal.OutputFormat");
			this.txtFiscal.Style = "font-size: 8pt; text-align: right";
			this.txtFiscal.Text = null;
			this.txtFiscal.Top = 0F;
			this.txtFiscal.Width = 0.6875F;
			// 
			// txtCalendar
			// 
			this.txtCalendar.Height = 0.1875F;
			this.txtCalendar.Left = 4.75F;
			this.txtCalendar.Name = "txtCalendar";
			this.txtCalendar.Style = "font-size: 8pt; text-align: right";
			this.txtCalendar.Text = null;
			this.txtCalendar.Top = 0F;
			this.txtCalendar.Width = 0.6875F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.1875F;
			this.txtQTD.Left = 3.25F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "font-size: 8pt; text-align: right";
			this.txtQTD.Text = null;
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 0.6875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.txtTotalCurrent,
            this.txtTotalMTD,
            this.txtTotalFYTD,
            this.txtTotalCalendar,
            this.txtTotalQTD,
            this.Line1,
            this.Field3,
            this.txtTotalCurrentPaid,
            this.txtTotalMTDPaid,
            this.txtTotalFYTDPaid,
            this.txtTotalCalendarPaid,
            this.txtTotalQTDPaid,
            this.Line2,
            this.Field4,
            this.txtCurrentFedWH,
            this.txtMTDFedWH,
            this.txtFYTDFedWH,
            this.txtCYTDFedWH,
            this.txtQTDFedWH,
            this.Line3,
            this.Field10,
            this.txtCurrentFicaWH,
            this.txtMTDFicaWH,
            this.txtFYTDFicaWH,
            this.txtCYTDFicaWH,
            this.txtQTDFicaWH,
            this.Field16,
            this.txtCurrentMedWH,
            this.txtMTDMedWH,
            this.txtFYTDMedWH,
            this.txtCYTDMedWH,
            this.txtQTDMedWH,
            this.Field22,
            this.txtCurrentStateWH,
            this.txtMTDStateWH,
            this.txtFYTDStateWH,
            this.txtCYTDStateWH,
            this.txtQTDStateWH,
            this.Field34,
            this.txtCurrentFedGross,
            this.txtMTDFedGross,
            this.txtFYTDFedGross,
            this.txtCYTDFedGross,
            this.txtQTDFedGross,
            this.Field40,
            this.txtCurrentFicaGross,
            this.txtMTDFicaGross,
            this.txtFYTDFicaGross,
            this.txtCYTDFicaGross,
            this.txtQTDFicaGross,
            this.Field46,
            this.txtCurrentMedGross,
            this.txtMTDMedGross,
            this.txtFYTDMedGross,
            this.txtCYTDMedGross,
            this.txtQTDMedGross,
            this.Field52,
            this.txtCurrentStateGross,
            this.txtMTDStateGross,
            this.txtFYTDStateGross,
            this.txtCYTDStateGross,
            this.txtQTDStateGross,
            this.Line4});
			this.ReportFooter.Height = 2.208333F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.1875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 8pt";
			this.Field1.Text = "Total Gross Pay";
			this.Field1.Top = 1.666667F;
			this.Field1.Width = 1.375F;
			// 
			// txtTotalCurrent
			// 
			this.txtTotalCurrent.Height = 0.1875F;
			this.txtTotalCurrent.Left = 1.6875F;
			this.txtTotalCurrent.Name = "txtTotalCurrent";
			this.txtTotalCurrent.OutputFormat = resources.GetString("txtTotalCurrent.OutputFormat");
			this.txtTotalCurrent.Style = "font-size: 8pt; text-align: right";
			this.txtTotalCurrent.Text = null;
			this.txtTotalCurrent.Top = 1.666667F;
			this.txtTotalCurrent.Width = 0.6875F;
			// 
			// txtTotalMTD
			// 
			this.txtTotalMTD.Height = 0.1875F;
			this.txtTotalMTD.Left = 2.5F;
			this.txtTotalMTD.Name = "txtTotalMTD";
			this.txtTotalMTD.OutputFormat = resources.GetString("txtTotalMTD.OutputFormat");
			this.txtTotalMTD.Style = "font-size: 8pt; text-align: right";
			this.txtTotalMTD.Text = null;
			this.txtTotalMTD.Top = 1.666667F;
			this.txtTotalMTD.Width = 0.6875F;
			// 
			// txtTotalFYTD
			// 
			this.txtTotalFYTD.Height = 0.1875F;
			this.txtTotalFYTD.Left = 4F;
			this.txtTotalFYTD.Name = "txtTotalFYTD";
			this.txtTotalFYTD.OutputFormat = resources.GetString("txtTotalFYTD.OutputFormat");
			this.txtTotalFYTD.Style = "font-size: 8pt; text-align: right";
			this.txtTotalFYTD.Text = null;
			this.txtTotalFYTD.Top = 1.666667F;
			this.txtTotalFYTD.Width = 0.6875F;
			// 
			// txtTotalCalendar
			// 
			this.txtTotalCalendar.Height = 0.1875F;
			this.txtTotalCalendar.Left = 4.75F;
			this.txtTotalCalendar.Name = "txtTotalCalendar";
			this.txtTotalCalendar.Style = "font-size: 8pt; text-align: right";
			this.txtTotalCalendar.Text = null;
			this.txtTotalCalendar.Top = 1.666667F;
			this.txtTotalCalendar.Width = 0.6875F;
			// 
			// txtTotalQTD
			// 
			this.txtTotalQTD.Height = 0.1875F;
			this.txtTotalQTD.Left = 3.25F;
			this.txtTotalQTD.Name = "txtTotalQTD";
			this.txtTotalQTD.Style = "font-size: 8pt; text-align: right";
			this.txtTotalQTD.Text = null;
			this.txtTotalQTD.Top = 1.666667F;
			this.txtTotalQTD.Width = 0.6875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.1666667F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.666667F;
			this.Line1.Width = 5.333333F;
			this.Line1.X1 = 0.1666667F;
			this.Line1.X2 = 5.5F;
			this.Line1.Y1 = 1.666667F;
			this.Line1.Y2 = 1.666667F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.1875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-size: 8pt";
			this.Field3.Text = "Total Paid";
			this.Field3.Top = 2F;
			this.Field3.Width = 1.375F;
			// 
			// txtTotalCurrentPaid
			// 
			this.txtTotalCurrentPaid.Height = 0.1875F;
			this.txtTotalCurrentPaid.Left = 1.6875F;
			this.txtTotalCurrentPaid.Name = "txtTotalCurrentPaid";
			this.txtTotalCurrentPaid.OutputFormat = resources.GetString("txtTotalCurrentPaid.OutputFormat");
			this.txtTotalCurrentPaid.Style = "font-size: 8pt; text-align: right";
			this.txtTotalCurrentPaid.Text = null;
			this.txtTotalCurrentPaid.Top = 2F;
			this.txtTotalCurrentPaid.Width = 0.6875F;
			// 
			// txtTotalMTDPaid
			// 
			this.txtTotalMTDPaid.Height = 0.1875F;
			this.txtTotalMTDPaid.Left = 2.5F;
			this.txtTotalMTDPaid.Name = "txtTotalMTDPaid";
			this.txtTotalMTDPaid.OutputFormat = resources.GetString("txtTotalMTDPaid.OutputFormat");
			this.txtTotalMTDPaid.Style = "font-size: 8pt; text-align: right";
			this.txtTotalMTDPaid.Text = null;
			this.txtTotalMTDPaid.Top = 2F;
			this.txtTotalMTDPaid.Width = 0.6875F;
			// 
			// txtTotalFYTDPaid
			// 
			this.txtTotalFYTDPaid.Height = 0.1875F;
			this.txtTotalFYTDPaid.Left = 4F;
			this.txtTotalFYTDPaid.Name = "txtTotalFYTDPaid";
			this.txtTotalFYTDPaid.OutputFormat = resources.GetString("txtTotalFYTDPaid.OutputFormat");
			this.txtTotalFYTDPaid.Style = "font-size: 8pt; text-align: right";
			this.txtTotalFYTDPaid.Text = null;
			this.txtTotalFYTDPaid.Top = 2F;
			this.txtTotalFYTDPaid.Width = 0.6875F;
			// 
			// txtTotalCalendarPaid
			// 
			this.txtTotalCalendarPaid.Height = 0.1875F;
			this.txtTotalCalendarPaid.Left = 4.75F;
			this.txtTotalCalendarPaid.Name = "txtTotalCalendarPaid";
			this.txtTotalCalendarPaid.Style = "font-size: 8pt; text-align: right";
			this.txtTotalCalendarPaid.Text = null;
			this.txtTotalCalendarPaid.Top = 2F;
			this.txtTotalCalendarPaid.Width = 0.6875F;
			// 
			// txtTotalQTDPaid
			// 
			this.txtTotalQTDPaid.Height = 0.1875F;
			this.txtTotalQTDPaid.Left = 3.25F;
			this.txtTotalQTDPaid.Name = "txtTotalQTDPaid";
			this.txtTotalQTDPaid.Style = "font-size: 8pt; text-align: right";
			this.txtTotalQTDPaid.Text = null;
			this.txtTotalQTDPaid.Top = 2F;
			this.txtTotalQTDPaid.Width = 0.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.1666667F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2F;
			this.Line2.Width = 5.333333F;
			this.Line2.X1 = 0.1666667F;
			this.Line2.X2 = 5.5F;
			this.Line2.Y1 = 2F;
			this.Line2.Y2 = 2F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0.1875F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-size: 8pt";
			this.Field4.Text = "Federal Tax Withheld";
			this.Field4.Top = 0F;
			this.Field4.Width = 1.375F;
			// 
			// txtCurrentFedWH
			// 
			this.txtCurrentFedWH.Height = 0.1875F;
			this.txtCurrentFedWH.Left = 1.6875F;
			this.txtCurrentFedWH.Name = "txtCurrentFedWH";
			this.txtCurrentFedWH.OutputFormat = resources.GetString("txtCurrentFedWH.OutputFormat");
			this.txtCurrentFedWH.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentFedWH.Text = null;
			this.txtCurrentFedWH.Top = 0F;
			this.txtCurrentFedWH.Width = 0.6875F;
			// 
			// txtMTDFedWH
			// 
			this.txtMTDFedWH.Height = 0.1875F;
			this.txtMTDFedWH.Left = 2.5F;
			this.txtMTDFedWH.Name = "txtMTDFedWH";
			this.txtMTDFedWH.OutputFormat = resources.GetString("txtMTDFedWH.OutputFormat");
			this.txtMTDFedWH.Style = "font-size: 8pt; text-align: right";
			this.txtMTDFedWH.Text = null;
			this.txtMTDFedWH.Top = 0F;
			this.txtMTDFedWH.Width = 0.6875F;
			// 
			// txtFYTDFedWH
			// 
			this.txtFYTDFedWH.Height = 0.1875F;
			this.txtFYTDFedWH.Left = 4F;
			this.txtFYTDFedWH.Name = "txtFYTDFedWH";
			this.txtFYTDFedWH.OutputFormat = resources.GetString("txtFYTDFedWH.OutputFormat");
			this.txtFYTDFedWH.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDFedWH.Text = null;
			this.txtFYTDFedWH.Top = 0F;
			this.txtFYTDFedWH.Width = 0.6875F;
			// 
			// txtCYTDFedWH
			// 
			this.txtCYTDFedWH.Height = 0.1875F;
			this.txtCYTDFedWH.Left = 4.75F;
			this.txtCYTDFedWH.Name = "txtCYTDFedWH";
			this.txtCYTDFedWH.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDFedWH.Text = null;
			this.txtCYTDFedWH.Top = 0F;
			this.txtCYTDFedWH.Width = 0.6875F;
			// 
			// txtQTDFedWH
			// 
			this.txtQTDFedWH.Height = 0.1875F;
			this.txtQTDFedWH.Left = 3.25F;
			this.txtQTDFedWH.Name = "txtQTDFedWH";
			this.txtQTDFedWH.Style = "font-size: 8pt; text-align: right";
			this.txtQTDFedWH.Text = null;
			this.txtQTDFedWH.Top = 0F;
			this.txtQTDFedWH.Width = 0.6875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.1666667F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 5.333333F;
			this.Line3.X1 = 0.1666667F;
			this.Line3.X2 = 5.5F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 0.1875F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 8pt";
			this.Field10.Text = "Fica Tax Withheld";
			this.Field10.Top = 0.1666667F;
			this.Field10.Width = 1.375F;
			// 
			// txtCurrentFicaWH
			// 
			this.txtCurrentFicaWH.Height = 0.1875F;
			this.txtCurrentFicaWH.Left = 1.6875F;
			this.txtCurrentFicaWH.Name = "txtCurrentFicaWH";
			this.txtCurrentFicaWH.OutputFormat = resources.GetString("txtCurrentFicaWH.OutputFormat");
			this.txtCurrentFicaWH.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentFicaWH.Text = null;
			this.txtCurrentFicaWH.Top = 0.1666667F;
			this.txtCurrentFicaWH.Width = 0.6875F;
			// 
			// txtMTDFicaWH
			// 
			this.txtMTDFicaWH.Height = 0.1875F;
			this.txtMTDFicaWH.Left = 2.5F;
			this.txtMTDFicaWH.Name = "txtMTDFicaWH";
			this.txtMTDFicaWH.OutputFormat = resources.GetString("txtMTDFicaWH.OutputFormat");
			this.txtMTDFicaWH.Style = "font-size: 8pt; text-align: right";
			this.txtMTDFicaWH.Text = null;
			this.txtMTDFicaWH.Top = 0.1666667F;
			this.txtMTDFicaWH.Width = 0.6875F;
			// 
			// txtFYTDFicaWH
			// 
			this.txtFYTDFicaWH.Height = 0.1875F;
			this.txtFYTDFicaWH.Left = 4F;
			this.txtFYTDFicaWH.Name = "txtFYTDFicaWH";
			this.txtFYTDFicaWH.OutputFormat = resources.GetString("txtFYTDFicaWH.OutputFormat");
			this.txtFYTDFicaWH.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDFicaWH.Text = null;
			this.txtFYTDFicaWH.Top = 0.1666667F;
			this.txtFYTDFicaWH.Width = 0.6875F;
			// 
			// txtCYTDFicaWH
			// 
			this.txtCYTDFicaWH.Height = 0.1875F;
			this.txtCYTDFicaWH.Left = 4.75F;
			this.txtCYTDFicaWH.Name = "txtCYTDFicaWH";
			this.txtCYTDFicaWH.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDFicaWH.Text = null;
			this.txtCYTDFicaWH.Top = 0.1666667F;
			this.txtCYTDFicaWH.Width = 0.6875F;
			// 
			// txtQTDFicaWH
			// 
			this.txtQTDFicaWH.Height = 0.1875F;
			this.txtQTDFicaWH.Left = 3.25F;
			this.txtQTDFicaWH.Name = "txtQTDFicaWH";
			this.txtQTDFicaWH.Style = "font-size: 8pt; text-align: right";
			this.txtQTDFicaWH.Text = null;
			this.txtQTDFicaWH.Top = 0.1666667F;
			this.txtQTDFicaWH.Width = 0.6875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 0.1875F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-size: 8pt";
			this.Field16.Text = "Medicare Tax Withheld";
			this.Field16.Top = 0.3333333F;
			this.Field16.Width = 1.375F;
			// 
			// txtCurrentMedWH
			// 
			this.txtCurrentMedWH.Height = 0.1875F;
			this.txtCurrentMedWH.Left = 1.6875F;
			this.txtCurrentMedWH.Name = "txtCurrentMedWH";
			this.txtCurrentMedWH.OutputFormat = resources.GetString("txtCurrentMedWH.OutputFormat");
			this.txtCurrentMedWH.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentMedWH.Text = null;
			this.txtCurrentMedWH.Top = 0.3333333F;
			this.txtCurrentMedWH.Width = 0.6875F;
			// 
			// txtMTDMedWH
			// 
			this.txtMTDMedWH.Height = 0.1875F;
			this.txtMTDMedWH.Left = 2.5F;
			this.txtMTDMedWH.Name = "txtMTDMedWH";
			this.txtMTDMedWH.OutputFormat = resources.GetString("txtMTDMedWH.OutputFormat");
			this.txtMTDMedWH.Style = "font-size: 8pt; text-align: right";
			this.txtMTDMedWH.Text = null;
			this.txtMTDMedWH.Top = 0.3333333F;
			this.txtMTDMedWH.Width = 0.6875F;
			// 
			// txtFYTDMedWH
			// 
			this.txtFYTDMedWH.Height = 0.1875F;
			this.txtFYTDMedWH.Left = 4F;
			this.txtFYTDMedWH.Name = "txtFYTDMedWH";
			this.txtFYTDMedWH.OutputFormat = resources.GetString("txtFYTDMedWH.OutputFormat");
			this.txtFYTDMedWH.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDMedWH.Text = null;
			this.txtFYTDMedWH.Top = 0.3333333F;
			this.txtFYTDMedWH.Width = 0.6875F;
			// 
			// txtCYTDMedWH
			// 
			this.txtCYTDMedWH.Height = 0.1875F;
			this.txtCYTDMedWH.Left = 4.75F;
			this.txtCYTDMedWH.Name = "txtCYTDMedWH";
			this.txtCYTDMedWH.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDMedWH.Text = null;
			this.txtCYTDMedWH.Top = 0.3333333F;
			this.txtCYTDMedWH.Width = 0.6875F;
			// 
			// txtQTDMedWH
			// 
			this.txtQTDMedWH.Height = 0.1875F;
			this.txtQTDMedWH.Left = 3.25F;
			this.txtQTDMedWH.Name = "txtQTDMedWH";
			this.txtQTDMedWH.Style = "font-size: 8pt; text-align: right";
			this.txtQTDMedWH.Text = null;
			this.txtQTDMedWH.Top = 0.3333333F;
			this.txtQTDMedWH.Width = 0.6875F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 0.1875F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-size: 8pt";
			this.Field22.Text = "State Tax Withheld";
			this.Field22.Top = 0.5F;
			this.Field22.Width = 1.375F;
			// 
			// txtCurrentStateWH
			// 
			this.txtCurrentStateWH.Height = 0.1875F;
			this.txtCurrentStateWH.Left = 1.6875F;
			this.txtCurrentStateWH.Name = "txtCurrentStateWH";
			this.txtCurrentStateWH.OutputFormat = resources.GetString("txtCurrentStateWH.OutputFormat");
			this.txtCurrentStateWH.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentStateWH.Text = null;
			this.txtCurrentStateWH.Top = 0.5F;
			this.txtCurrentStateWH.Width = 0.6875F;
			// 
			// txtMTDStateWH
			// 
			this.txtMTDStateWH.Height = 0.1875F;
			this.txtMTDStateWH.Left = 2.5F;
			this.txtMTDStateWH.Name = "txtMTDStateWH";
			this.txtMTDStateWH.OutputFormat = resources.GetString("txtMTDStateWH.OutputFormat");
			this.txtMTDStateWH.Style = "font-size: 8pt; text-align: right";
			this.txtMTDStateWH.Text = null;
			this.txtMTDStateWH.Top = 0.5F;
			this.txtMTDStateWH.Width = 0.6875F;
			// 
			// txtFYTDStateWH
			// 
			this.txtFYTDStateWH.Height = 0.1875F;
			this.txtFYTDStateWH.Left = 4F;
			this.txtFYTDStateWH.Name = "txtFYTDStateWH";
			this.txtFYTDStateWH.OutputFormat = resources.GetString("txtFYTDStateWH.OutputFormat");
			this.txtFYTDStateWH.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDStateWH.Text = null;
			this.txtFYTDStateWH.Top = 0.5F;
			this.txtFYTDStateWH.Width = 0.6875F;
			// 
			// txtCYTDStateWH
			// 
			this.txtCYTDStateWH.Height = 0.1875F;
			this.txtCYTDStateWH.Left = 4.75F;
			this.txtCYTDStateWH.Name = "txtCYTDStateWH";
			this.txtCYTDStateWH.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDStateWH.Text = null;
			this.txtCYTDStateWH.Top = 0.5F;
			this.txtCYTDStateWH.Width = 0.6875F;
			// 
			// txtQTDStateWH
			// 
			this.txtQTDStateWH.Height = 0.1875F;
			this.txtQTDStateWH.Left = 3.25F;
			this.txtQTDStateWH.Name = "txtQTDStateWH";
			this.txtQTDStateWH.Style = "font-size: 8pt; text-align: right";
			this.txtQTDStateWH.Text = null;
			this.txtQTDStateWH.Top = 0.5F;
			this.txtQTDStateWH.Width = 0.6875F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 0.1875F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-size: 8pt";
			this.Field34.Text = "Federal Gross";
			this.Field34.Top = 0.8333333F;
			this.Field34.Width = 1.375F;
			// 
			// txtCurrentFedGross
			// 
			this.txtCurrentFedGross.Height = 0.1875F;
			this.txtCurrentFedGross.Left = 1.6875F;
			this.txtCurrentFedGross.Name = "txtCurrentFedGross";
			this.txtCurrentFedGross.OutputFormat = resources.GetString("txtCurrentFedGross.OutputFormat");
			this.txtCurrentFedGross.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentFedGross.Text = null;
			this.txtCurrentFedGross.Top = 0.8333333F;
			this.txtCurrentFedGross.Width = 0.6875F;
			// 
			// txtMTDFedGross
			// 
			this.txtMTDFedGross.Height = 0.1875F;
			this.txtMTDFedGross.Left = 2.5F;
			this.txtMTDFedGross.Name = "txtMTDFedGross";
			this.txtMTDFedGross.OutputFormat = resources.GetString("txtMTDFedGross.OutputFormat");
			this.txtMTDFedGross.Style = "font-size: 8pt; text-align: right";
			this.txtMTDFedGross.Text = null;
			this.txtMTDFedGross.Top = 0.8333333F;
			this.txtMTDFedGross.Width = 0.6875F;
			// 
			// txtFYTDFedGross
			// 
			this.txtFYTDFedGross.Height = 0.1875F;
			this.txtFYTDFedGross.Left = 4F;
			this.txtFYTDFedGross.Name = "txtFYTDFedGross";
			this.txtFYTDFedGross.OutputFormat = resources.GetString("txtFYTDFedGross.OutputFormat");
			this.txtFYTDFedGross.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDFedGross.Text = null;
			this.txtFYTDFedGross.Top = 0.8333333F;
			this.txtFYTDFedGross.Width = 0.6875F;
			// 
			// txtCYTDFedGross
			// 
			this.txtCYTDFedGross.Height = 0.1875F;
			this.txtCYTDFedGross.Left = 4.75F;
			this.txtCYTDFedGross.Name = "txtCYTDFedGross";
			this.txtCYTDFedGross.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDFedGross.Text = null;
			this.txtCYTDFedGross.Top = 0.8333333F;
			this.txtCYTDFedGross.Width = 0.6875F;
			// 
			// txtQTDFedGross
			// 
			this.txtQTDFedGross.Height = 0.1875F;
			this.txtQTDFedGross.Left = 3.25F;
			this.txtQTDFedGross.Name = "txtQTDFedGross";
			this.txtQTDFedGross.Style = "font-size: 8pt; text-align: right";
			this.txtQTDFedGross.Text = null;
			this.txtQTDFedGross.Top = 0.8333333F;
			this.txtQTDFedGross.Width = 0.6875F;
			// 
			// Field40
			// 
			this.Field40.Height = 0.1875F;
			this.Field40.Left = 0.1875F;
			this.Field40.Name = "Field40";
			this.Field40.Style = "font-size: 8pt";
			this.Field40.Text = "Fica Gross";
			this.Field40.Top = 1F;
			this.Field40.Width = 1.375F;
			// 
			// txtCurrentFicaGross
			// 
			this.txtCurrentFicaGross.Height = 0.1875F;
			this.txtCurrentFicaGross.Left = 1.6875F;
			this.txtCurrentFicaGross.Name = "txtCurrentFicaGross";
			this.txtCurrentFicaGross.OutputFormat = resources.GetString("txtCurrentFicaGross.OutputFormat");
			this.txtCurrentFicaGross.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentFicaGross.Text = null;
			this.txtCurrentFicaGross.Top = 1F;
			this.txtCurrentFicaGross.Width = 0.6875F;
			// 
			// txtMTDFicaGross
			// 
			this.txtMTDFicaGross.Height = 0.1875F;
			this.txtMTDFicaGross.Left = 2.5F;
			this.txtMTDFicaGross.Name = "txtMTDFicaGross";
			this.txtMTDFicaGross.OutputFormat = resources.GetString("txtMTDFicaGross.OutputFormat");
			this.txtMTDFicaGross.Style = "font-size: 8pt; text-align: right";
			this.txtMTDFicaGross.Text = null;
			this.txtMTDFicaGross.Top = 1F;
			this.txtMTDFicaGross.Width = 0.6875F;
			// 
			// txtFYTDFicaGross
			// 
			this.txtFYTDFicaGross.Height = 0.1875F;
			this.txtFYTDFicaGross.Left = 4F;
			this.txtFYTDFicaGross.Name = "txtFYTDFicaGross";
			this.txtFYTDFicaGross.OutputFormat = resources.GetString("txtFYTDFicaGross.OutputFormat");
			this.txtFYTDFicaGross.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDFicaGross.Text = null;
			this.txtFYTDFicaGross.Top = 1F;
			this.txtFYTDFicaGross.Width = 0.6875F;
			// 
			// txtCYTDFicaGross
			// 
			this.txtCYTDFicaGross.Height = 0.1875F;
			this.txtCYTDFicaGross.Left = 4.75F;
			this.txtCYTDFicaGross.Name = "txtCYTDFicaGross";
			this.txtCYTDFicaGross.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDFicaGross.Text = null;
			this.txtCYTDFicaGross.Top = 1F;
			this.txtCYTDFicaGross.Width = 0.6875F;
			// 
			// txtQTDFicaGross
			// 
			this.txtQTDFicaGross.Height = 0.1875F;
			this.txtQTDFicaGross.Left = 3.25F;
			this.txtQTDFicaGross.Name = "txtQTDFicaGross";
			this.txtQTDFicaGross.Style = "font-size: 8pt; text-align: right";
			this.txtQTDFicaGross.Text = null;
			this.txtQTDFicaGross.Top = 1F;
			this.txtQTDFicaGross.Width = 0.6875F;
			// 
			// Field46
			// 
			this.Field46.Height = 0.1875F;
			this.Field46.Left = 0.1875F;
			this.Field46.Name = "Field46";
			this.Field46.Style = "font-size: 8pt";
			this.Field46.Text = "Medicare Gross";
			this.Field46.Top = 1.166667F;
			this.Field46.Width = 1.375F;
			// 
			// txtCurrentMedGross
			// 
			this.txtCurrentMedGross.Height = 0.1875F;
			this.txtCurrentMedGross.Left = 1.6875F;
			this.txtCurrentMedGross.Name = "txtCurrentMedGross";
			this.txtCurrentMedGross.OutputFormat = resources.GetString("txtCurrentMedGross.OutputFormat");
			this.txtCurrentMedGross.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentMedGross.Text = null;
			this.txtCurrentMedGross.Top = 1.166667F;
			this.txtCurrentMedGross.Width = 0.6875F;
			// 
			// txtMTDMedGross
			// 
			this.txtMTDMedGross.Height = 0.1875F;
			this.txtMTDMedGross.Left = 2.5F;
			this.txtMTDMedGross.Name = "txtMTDMedGross";
			this.txtMTDMedGross.OutputFormat = resources.GetString("txtMTDMedGross.OutputFormat");
			this.txtMTDMedGross.Style = "font-size: 8pt; text-align: right";
			this.txtMTDMedGross.Text = null;
			this.txtMTDMedGross.Top = 1.166667F;
			this.txtMTDMedGross.Width = 0.6875F;
			// 
			// txtFYTDMedGross
			// 
			this.txtFYTDMedGross.Height = 0.1875F;
			this.txtFYTDMedGross.Left = 4F;
			this.txtFYTDMedGross.Name = "txtFYTDMedGross";
			this.txtFYTDMedGross.OutputFormat = resources.GetString("txtFYTDMedGross.OutputFormat");
			this.txtFYTDMedGross.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDMedGross.Text = null;
			this.txtFYTDMedGross.Top = 1.166667F;
			this.txtFYTDMedGross.Width = 0.6875F;
			// 
			// txtCYTDMedGross
			// 
			this.txtCYTDMedGross.Height = 0.1875F;
			this.txtCYTDMedGross.Left = 4.75F;
			this.txtCYTDMedGross.Name = "txtCYTDMedGross";
			this.txtCYTDMedGross.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDMedGross.Text = null;
			this.txtCYTDMedGross.Top = 1.166667F;
			this.txtCYTDMedGross.Width = 0.6875F;
			// 
			// txtQTDMedGross
			// 
			this.txtQTDMedGross.Height = 0.1875F;
			this.txtQTDMedGross.Left = 3.25F;
			this.txtQTDMedGross.Name = "txtQTDMedGross";
			this.txtQTDMedGross.Style = "font-size: 8pt; text-align: right";
			this.txtQTDMedGross.Text = null;
			this.txtQTDMedGross.Top = 1.166667F;
			this.txtQTDMedGross.Width = 0.6875F;
			// 
			// Field52
			// 
			this.Field52.Height = 0.1875F;
			this.Field52.Left = 0.1875F;
			this.Field52.Name = "Field52";
			this.Field52.Style = "font-size: 8pt";
			this.Field52.Text = "State Gross";
			this.Field52.Top = 1.333333F;
			this.Field52.Width = 1.375F;
			// 
			// txtCurrentStateGross
			// 
			this.txtCurrentStateGross.Height = 0.1875F;
			this.txtCurrentStateGross.Left = 1.6875F;
			this.txtCurrentStateGross.Name = "txtCurrentStateGross";
			this.txtCurrentStateGross.OutputFormat = resources.GetString("txtCurrentStateGross.OutputFormat");
			this.txtCurrentStateGross.Style = "font-size: 8pt; text-align: right";
			this.txtCurrentStateGross.Text = null;
			this.txtCurrentStateGross.Top = 1.333333F;
			this.txtCurrentStateGross.Width = 0.6875F;
			// 
			// txtMTDStateGross
			// 
			this.txtMTDStateGross.Height = 0.1875F;
			this.txtMTDStateGross.Left = 2.5F;
			this.txtMTDStateGross.Name = "txtMTDStateGross";
			this.txtMTDStateGross.OutputFormat = resources.GetString("txtMTDStateGross.OutputFormat");
			this.txtMTDStateGross.Style = "font-size: 8pt; text-align: right";
			this.txtMTDStateGross.Text = null;
			this.txtMTDStateGross.Top = 1.333333F;
			this.txtMTDStateGross.Width = 0.6875F;
			// 
			// txtFYTDStateGross
			// 
			this.txtFYTDStateGross.Height = 0.1875F;
			this.txtFYTDStateGross.Left = 4F;
			this.txtFYTDStateGross.Name = "txtFYTDStateGross";
			this.txtFYTDStateGross.OutputFormat = resources.GetString("txtFYTDStateGross.OutputFormat");
			this.txtFYTDStateGross.Style = "font-size: 8pt; text-align: right";
			this.txtFYTDStateGross.Text = null;
			this.txtFYTDStateGross.Top = 1.333333F;
			this.txtFYTDStateGross.Width = 0.6875F;
			// 
			// txtCYTDStateGross
			// 
			this.txtCYTDStateGross.Height = 0.1875F;
			this.txtCYTDStateGross.Left = 4.75F;
			this.txtCYTDStateGross.Name = "txtCYTDStateGross";
			this.txtCYTDStateGross.Style = "font-size: 8pt; text-align: right";
			this.txtCYTDStateGross.Text = null;
			this.txtCYTDStateGross.Top = 1.333333F;
			this.txtCYTDStateGross.Width = 0.6875F;
			// 
			// txtQTDStateGross
			// 
			this.txtQTDStateGross.Height = 0.1875F;
			this.txtQTDStateGross.Left = 3.25F;
			this.txtQTDStateGross.Name = "txtQTDStateGross";
			this.txtQTDStateGross.Style = "font-size: 8pt; text-align: right";
			this.txtQTDStateGross.Text = null;
			this.txtQTDStateGross.Top = 1.333333F;
			this.txtQTDStateGross.Width = 0.6875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.1666667F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.8333333F;
			this.Line4.Width = 5.333333F;
			this.Line4.X1 = 0.1666667F;
			this.Line4.X2 = 5.5F;
			this.Line4.Y1 = 0.8333333F;
			this.Line4.Y2 = 0.8333333F;
			// 
			// srptEmployeePayTotals
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.09375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrentPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTDPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFYTDPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendarPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTDPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFicaWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFicaGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalQTD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrentPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMTDPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFYTDPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCalendarPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalQTDPaid;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentFedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFedWH;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentFicaWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFicaWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFicaWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFicaWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFicaWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDMedWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDStateWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentFicaGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFicaGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFicaGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFicaGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFicaGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
	}
}
