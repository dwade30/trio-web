﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDeductionReport.
	/// </summary>
	partial class rptCustomDeductionReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomDeductionReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtField1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCaption2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtField1,
            this.txtField2,
            this.txtField3});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtField1
			// 
			this.txtField1.Height = 0.1666667F;
			this.txtField1.Left = 0.9F;
			this.txtField1.Name = "txtField1";
			this.txtField1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtField1.Text = "Field1";
			this.txtField1.Top = 0F;
			this.txtField1.Width = 3.066667F;
			// 
			// txtField2
			// 
			this.txtField2.Height = 0.1666667F;
			this.txtField2.Left = 4.1F;
			this.txtField2.Name = "txtField2";
			this.txtField2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField2.Text = "Field1";
			this.txtField2.Top = 0F;
			this.txtField2.Width = 0.9333333F;
			// 
			// txtField3
			// 
			this.txtField3.Height = 0.1666667F;
			this.txtField3.Left = 5.133333F;
			this.txtField3.Name = "txtField3";
			this.txtField3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtField3.Text = "Field1";
			this.txtField3.Top = 0F;
			this.txtField3.Width = 0.9333333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtTime,
            this.txtPage,
            this.lblCaption2});
			this.PageHeader.Height = 0.65625F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  CUSTOM  DEDUCTION  REPORT";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.9375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 5.65625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 5.65625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.3125F;
			// 
			// lblCaption2
			// 
			this.lblCaption2.Height = 0.2F;
			this.lblCaption2.HyperLink = null;
			this.lblCaption2.Left = 0F;
			this.lblCaption2.Name = "lblCaption2";
			this.lblCaption2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblCaption2.Text = null;
			this.lblCaption2.Top = 0.2F;
			this.lblCaption2.Width = 6.933333F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroup});
            this.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.GroupHeader1.Height = 0.2395833F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.1666667F;
			this.txtGroup.Left = 0.6333333F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtGroup.Text = "Field1";
			this.txtGroup.Top = 0.03333334F;
			this.txtGroup.Width = 2.933333F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.1354167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptCustomDeductionReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.rptCustomDeductionReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
