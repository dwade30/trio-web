//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmUnemploymentReview : BaseForm
	{
		public frmUnemploymentReview()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUnemploymentReview InstancePtr
		{
			get
			{
				return (frmUnemploymentReview)Sys.GetInstance(typeof(frmUnemploymentReview));
			}
		}

		protected frmUnemploymentReview _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 02/08/2006
		// ********************************************************
		// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
		DateTime dtStartDate;
		// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(string)
		DateTime dtEndDate;
		const int cnstgridcolID = 1;
		const int CNSTGridColName = 0;
		const int CNSTGRIDCOLEMPLOYEENUMBER = 2;
		const int CNSTGRIDCOLDISTGROSSPAY = 3;
		const int CNSTGRIDCOLNONSEASONAL = 4;
		const int CNSTGRIDCOLSEASONAL = 5;
		const int CNSTGRIDCOLOTHER = 6;
		const int CNSTGRIDCOLDISTU = 7;
		const int CNSTGRIDCOLNATURECODE = 8;
		const int CNSTGRIDCOLPARENTROW = 9;

		public void Init(ref int intQuarter, ref int lngYear)
		{
			switch (intQuarter)
			{
				case 1:
					{
						dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
						dtEndDate = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(lngYear));
						break;
					}
				case 2:
					{
						dtStartDate = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(lngYear));
						dtEndDate = FCConvert.ToDateTime("6/30/" + FCConvert.ToString(lngYear));
						break;
					}
				case 3:
					{
						dtStartDate = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(lngYear));
						dtEndDate = FCConvert.ToDateTime("9/30/" + FCConvert.ToString(lngYear));
						break;
					}
				case 4:
					{
						dtStartDate = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(lngYear));
						dtEndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
						break;
					}
			}
			//end switch
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmUnemploymentReview_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUnemploymentReview_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUnemploymentReview properties;
			//frmUnemploymentReview.FillStyle	= 0;
			//frmUnemploymentReview.ScaleWidth	= 9300;
			//frmUnemploymentReview.ScaleHeight	= 7635;
			//frmUnemploymentReview.LinkTopic	= "Form2";
			//frmUnemploymentReview.LockControls	= -1  'True;
			//frmUnemploymentReview.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void frmUnemploymentReview_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Grid.Row < 1)
				return;
			if (Grid.RowOutlineLevel(Grid.Row) == 1)
			{
				Grid.RowData(Grid.Row, true);
				// recalc totals for the employee
				RecalcEmployeeTotals(FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLPARENTROW))));
			}
		}
		// vbPorter upgrade warning: lngParentRow As int	OnWriteFCConvert.ToDouble(
		private void RecalcEmployeeTotals(int lngParentRow)
		{
			int lngRow;
			double dblSeasonal;
			double dblNonSeasonal;
			double dblOther;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngParentRow == Grid.Rows - 1 || lngParentRow < 1)
					return;
				lngRow = lngParentRow + 1;
				dblSeasonal = 0;
				dblNonSeasonal = 0;
				dblOther = 0;
				do
				{
					dblTemp = 0;
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPARENTROW)) == lngParentRow)
					{
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLSEASONAL)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLSEASONAL));
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLNONSEASONAL)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLNONSEASONAL));
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER)) > 0)
						{
							dblTemp += FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER));
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLSEASONAL, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNONSEASONAL, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, "");
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU)) == modCoreysSweeterCode.CNSTDISTUNONE)
						{
							dblOther += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, "");
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU)) == modCoreysSweeterCode.CNSTDISTUSEASONAL)
						{
							dblSeasonal += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSEASONAL, Strings.Format(dblTemp, "#,###,##0.00"));
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU)) == modCoreysSweeterCode.CNSTDISTUYES)
						{
							dblNonSeasonal += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNONSEASONAL, Strings.Format(dblTemp, "#,###,##0.00"));
						}
						else
						{
							dblOther += dblTemp;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUNONE));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, "");
						}
						lngRow += 1;
					}
					else
					{
						// terminate do loop
						lngRow = Grid.Rows;
					}
				}
				while (!(lngRow >= Grid.Rows));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLSEASONAL, Strings.Format(dblSeasonal, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLNONSEASONAL, Strings.Format(dblNonSeasonal, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLOTHER, Strings.Format(dblOther, "#,###,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RecalcEmployeeTotals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		private void Grid_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewRow == 0)
				return;
			if (Grid.RowOutlineLevel(e.NewRow) == 1)
			{
				switch (e.NewCol)
				{
					case CNSTGRIDCOLDISTU:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					case CNSTGRIDCOLNATURECODE:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					default:
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			clsDRWrapper clsDetails = new clsDRWrapper();
			int lngParentRow = 0;
			double dblTemp = 0;
			string strList = "";
			string strWhere = "";
			string strTemp = "";
			int lngRow;
			double dblNonSeasonal = 0;
			double dblSeasonal = 0;
			double dblOther = 0;
			clsDRWrapper clsCat = new clsDRWrapper();
			Grid.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
			strSQL = "Select * from tblPayCategories";
			clsCat.OpenRecordset(strSQL, "twpy0000.vb1");
			Grid.Rows = 1;
			strSQL = "select * from tblemployeemaster inner join (select employeenumber from tblcheckdetail where distributionrecord = 1 and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' group by employeenumber) as tbl1 on (tbl1.employeenumber = tblemployeemaster.employeenumber)  order by lastname,firstname";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngParentRow = Grid.Rows - 1;
				Grid.RowOutlineLevel(lngParentRow, 0);
				Grid.IsSubtotal(lngParentRow, true);
				Grid.TextMatrix(lngParentRow, CNSTGridColName, clsLoad.Get_Fields("Lastname") + ", " + clsLoad.Get_Fields("firstname"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToString(clsLoad.Get_Fields("tblemployeemaster.employeenumber")));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngParentRow, 0, lngParentRow, Grid.Cols - 1, Information.RGB(200, 255, 200));
				dblTemp = 0;
				strSQL = "select sum(distgrosspay) as totgross from tblcheckdetail where employeenumber = '" + clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "' and distributionrecord = 1 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 " + strWhere;
				clsDetails.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!clsDetails.EndOfFile())
				{
					dblTemp = Conversion.Val(clsDetails.Get_Fields("totgross"));
				}
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLDISTGROSSPAY, Strings.Format(dblTemp, "#,###,##0.00"));
				// now add details
				dblSeasonal = 0;
				dblNonSeasonal = 0;
				dblOther = 0;
				strSQL = "select * from tblcheckdetail where employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "' and checkvoid = 0 and (distributionrecord = 1) and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' order by paydate";
				clsDetails.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsDetails.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.RowOutlineLevel(lngRow, 1);
					Grid.IsSubtotal(lngRow, false);
					Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(clsDetails.Get_Fields("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPARENTROW, FCConvert.ToString(lngParentRow));
					if (!(clsCat.EndOfFile() && clsCat.BeginningOfFile()))
					{
						if (clsCat.FindFirstRecord("ID", Conversion.Val(clsDetails.Get_Fields("distpaycategory"))))
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTGROSSPAY, FCConvert.ToString(clsCat.Get_Fields_String("description")));
						}
					}
					Grid.TextMatrix(lngRow, CNSTGridColName, FCConvert.ToString(clsDetails.Get_Fields("paydate")));
					if (fecherFoundation.Strings.UCase(Strings.Left(clsDetails.Get_Fields("distu") + "N", 1)) == "N")
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblOther += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUNONE));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, "");
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(clsDetails.Get_Fields("distu") + "N", 1)) == "Y")
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblNonSeasonal += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNONSEASONAL, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUYES));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, fecherFoundation.Strings.Trim(Strings.Mid(clsDetails.Get_Fields("distu") + " ", 2)));
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(clsDetails.Get_Fields("distu") + "N", 1)) == "S")
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblSeasonal += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLSEASONAL, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUSEASONAL));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, fecherFoundation.Strings.Trim(Strings.Mid(clsDetails.Get_Fields("distu") + " ", 2)));
					}
					else
					{
						dblTemp = Conversion.Val(clsDetails.Get_Fields("distgrosspay"));
						dblOther += dblTemp;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOTHER, Strings.Format(dblTemp, "#,###,##0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU, FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUNONE));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE, "");
					}
					Grid.RowData(lngRow, false);
					clsDetails.MoveNext();
				}
				dblTemp = dblSeasonal + dblNonSeasonal + dblOther;
				// Grid.TextMatrix(lngParentRow, CNSTGRIDCOLEARNABLECOMP) = Format(dblTemp, "#,###,##0.00")
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLSEASONAL, Strings.Format(dblSeasonal, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLNONSEASONAL, Strings.Format(dblNonSeasonal, "#,###,##0.00"));
				Grid.TextMatrix(lngParentRow, CNSTGRIDCOLOTHER, Strings.Format(dblOther, "#,###,##0.00"));
				clsLoad.MoveNext();
			}
			Grid.Row = 0;
			Grid.Outline(0);
            modColorScheme.ColorGrid(Grid);
		}

		private void SetupGrid()
		{
			string strTemp;
			strTemp = "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUNONE) + ";No|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUYES) + ";Yes|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTDISTUSEASONAL) + ";Seasonal";
			Grid.TextMatrix(0, CNSTGridColName, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLEMPLOYEENUMBER, "Emp No");
			Grid.TextMatrix(0, CNSTGRIDCOLDISTGROSSPAY, "Amount");
			Grid.TextMatrix(0, CNSTGRIDCOLSEASONAL, "Seasonal");
			Grid.TextMatrix(0, CNSTGRIDCOLNONSEASONAL, "Regular");
			Grid.TextMatrix(0, CNSTGRIDCOLOTHER, "Other");
			Grid.TextMatrix(0, CNSTGRIDCOLDISTU, "Include");
			Grid.TextMatrix(0, CNSTGRIDCOLNATURECODE, "Nature Code");
			Grid.ColComboList(CNSTGRIDCOLDISTU, strTemp);
			Grid.ColHidden(cnstgridcolID, true);
			Grid.ColHidden(CNSTGRIDCOLPARENTROW, true);

            Grid.ColAlignment(CNSTGRIDCOLNATURECODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:BSE:#4259 fix grid alignment 
            Grid.ColAlignment(CNSTGRIDCOLDISTGROSSPAY, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid.ColAlignment(CNSTGRIDCOLNONSEASONAL, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid.ColAlignment(CNSTGRIDCOLSEASONAL, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid.ColAlignment(CNSTGRIDCOLOTHER, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGridColName, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDISTGROSSPAY, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSEASONAL, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLNONSEASONAL, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLOTHER, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDISTU, FCConvert.ToInt32(0.09 * GridWidth));
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				SaveInfo = false;
				Grid.Row = 0;
				//App.DoEvents();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (Grid.RowOutlineLevel(lngRow) == 1)
					{
						if (FCConvert.ToBoolean(Grid.RowData(lngRow)))
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID)) > 0)
							{
								clsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, cnstgridcolID))), "twpy0000.vb1");
								if (!clsSave.EndOfFile())
								{
									clsSave.Edit();
									if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU)) == modCoreysSweeterCode.CNSTDISTUSEASONAL)
									{
										clsSave.Set_Fields("distu", fecherFoundation.Strings.Trim("S" + Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE)));
									}
									else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDISTU)) == modCoreysSweeterCode.CNSTDISTUYES)
									{
										clsSave.Set_Fields("distu", fecherFoundation.Strings.Trim("Y" + Grid.TextMatrix(lngRow, CNSTGRIDCOLNATURECODE)));
									}
									else
									{
										clsSave.Set_Fields("distu", "N");
									}
									clsSave.Update();
								}
							}
							Grid.RowData(lngRow, false);
						}
					}
				}
				// lngRow
				SaveInfo = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Unemployment Records Updated", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Save Info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}
	}
}
