//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Enums;
using SharedApplication.Payroll.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmProcessDisplay : BaseForm
	{
		public frmProcessDisplay()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmProcessDisplay InstancePtr
		{
			get
			{
				return (frmProcessDisplay)Sys.GetInstance(typeof(frmProcessDisplay));
			}
		}

		protected frmProcessDisplay _InstancePtr = null;
		//=========================================================
		private bool boolEncumberContracts;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;
		private bool boolUnloadForm;
		private bool pboolNonPaid;
        private Dictionary<int,int> FederalPayStatusCodes = new Dictionary<int, int>();
		private List<CategoryTag> categoryTags = new List<CategoryTag>();
        private ITaxTableService taxTableService;
		private struct PayrollEmployee
		{
			public double TotalGross;
			// vbPorter upgrade warning: TotalNet As double	OnWrite(string, double, int)
			public double TotalNet;
			// vbPorter upgrade warning: FedTaxGross As double	OnWrite(double, int, string)
			public double FedTaxGross;
			// vbPorter upgrade warning: StateTaxGross As double	OnWrite(double, int, string)
			public double StateTaxGross;
			// vbPorter upgrade warning: FICATaxGross As double	OnWrite(double, int, string)
			public double FICATaxGross;
			// vbPorter upgrade warning: MedTaxGross As double	OnWrite(double, int, string)
			public double MedTaxGross;
		};

		private struct AnnualizedPayrollEmployee
		{
			public double AnnualizedFedTax;
			public double AnnualizedStateTax;
		};

		private struct PayrollEmployeeDeductions
		{
			public double TotAmount;
			public double Amount;
			public string AmountType;
			public string PercentType;
			public bool FedTaxExempt;
			public bool StateTaxExempt;
			public bool FICATaxExempt;
			public bool MedTaxExempt;
			public double DeductionGross;
			public bool DeductionAdded;
			public int DeductionNumber;
			public int Priority;
			public double AdjustmentAmount;
			public int TaxStatusCode;
			public bool HitLimit;
			public double Limit;
			public int DeductionCode;
			public int DeductionID;
			public string DeductionDescription;
			public string DeductionAccountNumber;
			public int RowNumber;
			public int EmpDeductionID;
			public bool UExempt;
			public double DeductionAmount;
			public bool Apply;
		};

		private struct PayrollEmployeeMatches
		{
			public double MaxAmount;
			public double Amount;
			public double OriginalAmount;
			public string AmountType;
			public int DollarsHours;
			public bool FedTaxExempt;
			public bool StateTaxExempt;
			public bool FICATaxExempt;
			public bool MedTaxExempt;
			public double MatchGross;
			public bool MatchAdded;
			public int MatchNumber;
			public int DeductionNumber;
			public int DeductionID;
			public int DedTaxCode;
			public string Account;
			public string DeductionDescription;
			public string DeductionGLAccount;
			public int RowNumber;
			public int EmpDeductionID;
			public string ObjectCode;
			public double Percentage;
			public int Check;
			public bool HitLimit;
			public double TotAmount;
		};

		private struct PAYROLLDISTRIBUTION
		{
			public double Gross;
			public string Type;
			public int NumberWeeks;
			public int WeeksWithheld;
			public bool FedTaxExempt;
			public bool StateTaxExempt;
			public bool FICATaxExempt;
			public bool MedTaxExempt;
			public string Account;
			public bool boolFromSchool;
			public int ContractID;
			public bool boolEncumbrance;
			// vbPorter upgrade warning: Check As int	OnWrite(string)
			public int Check;
		};

		private struct CONTRACTRECORD
		{
			public int ContractID;
			public double OriginalAmount;
			public double Paid;
			public int EncumbranceJournal;
			public string Account;
			public string Description;
		};

		private struct CALCULATEDDISTRIBUTION
		{
			public double Gross;
			public double Hours;
			public double Rate;
			public double Factor;
		};

		private struct DEDUCTIONSETUP
		{
			public double Amount;
			public string AmountType;
			public string PercentType;
			public int TaxCode;
			public string Description;
			public int Frequency;
			public double Limit;
			public string LimitType;
			public int Priority;
			public int DeductionCode;
			public string AccountNumber;
			public bool UExempt;
			public string Status;
		};

		private struct TAXSTATUSCODES
		{
			public int ID;
			public string TaxStatusCode;
			public string Description;
			public int PetainsTo;
			public bool FedTaxExempt;
			public bool StateTaxExempt;
			public bool FICATaxExempt;
			public bool MedTaxExempt;
		};

		private struct TaxTable
		{
			public int ID;
			public int RecordNumber;
			public double PayMin;
			public double PayMax;
			public double Tax;
			public double TaxPercent;
			public double AmountOver;
		};

		private struct PayCategorysType
		{
			public int CategoryNumber;
			public int ExemptCode1;
			public string ExemptType1;
			public int ExemptCode2;
			public string ExemptType2;
			public int ExemptCode3;
			public string ExemptType3;
			public int ExemptCode4;
			public string ExemptType4;
			public int ExemptCode5;
			public string ExemptType5;
		};

		private struct PayrollProcessSetupTable
		{
			public int ID;
			public int WeekNumber;
			public string FirstWeekOF;
			public int MonthOf;
			public DateTime WeekEndDate;
			public DateTime PayDate;
			public int NumberOfPayWeeks;
			public int TaxUpTop;
			public int DeductionUpTop;
			public bool PayFreqWeekly;
			public bool PayFreqBiWeekly;
			public DateTime DateHire;
			public string DateAnniversary;
			public DateTime DateBirth;
			public bool DATAENTRY;
			public string DeptDiv;
			public int SeqNumber;
			public string Check;
			public double HrsDay;
			public double HrsWk;
			public int DaysWeek;
			public int PeriodTaxes;
			public int PeriodDeds;
			public int PayFrequencyID;
			public string PayFrequencyCode;
			public string PayFrequencyDesc;
			public double PayFrequencyValue;
			public int FirstFiscalMonth;
		};

		private struct TaxSetupStuff
		{
			public double MaxSSN;
			public double PercentSSN;
			public double EmployerFicaRate;
			public double MaxMed;
			public double PercentMed;
			public double EmployerMedRate;
			public double ThresholdMedRate;
			public double MedThreshold;
		};

		private bool boolloaded;
		private int intTWeeks;
		private int intTWHWeeks;
		private int gintLevyGridRow;
		private double dblGrandTotalNet;
		private double dblGrandTotalGross;
		private bool boolSepCheck;
		private int intDirectDeposit;
		private int intBothDeposits;
		private int intRegularChecks;
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intFactor;
		private int intTaxTotalMulti;
		private int intTaxUpTop;
		private int intDeductionUpTop;
		private int intPayMonths;
		// vbPorter upgrade warning: intGridRow As int	OnWriteFCConvert.ToInt32(
		private int intGridRow;
		private int intEmployeeRowID;
		private int intEmployeeCheckCounter;
		private bool boolErrorInDistributions;
		private bool boolBiWeeklyTown;
		private bool boolUseMultiFund;
		private string strFundNumber = "";
		// vbPorter upgrade warning: lngDeduction As double	OnWrite(string)
		private double lngDeduction;
		// vbPorter upgrade warning: lngGross As double	OnWrite(string)
		private double lngGross;
		private double lngEmployeeMatch;
		private double dblBasePayRate;
		private string strTestEmployeeID = string.Empty;
		private string strTable = "";
		private string strPercent = "";
		private string strMultiCheckCode = "";
		// vbPorter upgrade warning: strCurrentCheckCode As string	OnRead
		private string strCurrentCheckCode = string.Empty;
		private string strExemptType = "";
		private TaxSetupStuff udtTaxSetupStuff = new TaxSetupStuff();
		private PayrollEmployee udtEmployee = new PayrollEmployee();
		private PayrollEmployee udtAdjustedEmployee = new PayrollEmployee();
		private AnnualizedPayrollEmployee udtAnnualizedEmployee = new AnnualizedPayrollEmployee();
		private PayrollEmployeeDeductions[] aryDeductions = null;
		private PayrollEmployeeDeductions[] aryTotDeductions = null;
		private PayrollEmployeeMatches[] aryEmployeeMatches = null;
		//private modCoreysSweeterCode.SchoolObjectCodesRec[] arySchoolObjects = null;
		private PAYROLLDISTRIBUTION[] aryDistribution = null;
		private CONTRACTRECORD[] aryContractAccounts = null;
		private CALCULATEDDISTRIBUTION[] aryCalcedDist = null;
		private PayCategorysType[] aryPayCategories = null;
		private DEDUCTIONSETUP[] DefaultDeductions = null;
		private DEDUCTIONSETUP CurrentDeductions = new DEDUCTIONSETUP();
		private TAXSTATUSCODES[] DefaultTaxStatusCodes = null;
		//private TaxTable[] DefaultTaxTableValues = null;
		private PayrollProcessSetupTable CheckProcessSetupInfo = new PayrollProcessSetupTable();
		private modGlobalVariables.VacationSickCodes CurrentVacationSickInfo = new modGlobalVariables.VacationSickCodes();
		private clsDRWrapper rsMaster;
		private clsDRWrapper rsDistribution;
		private clsDRWrapper rsDeductions;
		private clsDRWrapper rsDeductionOverrides;
		private clsDRWrapper rsMatchOverrides;
		private clsDRWrapper rsEmployeeMatches;
		private clsDRWrapper rsPayCategory;
		private clsDRWrapper rsContracts = new clsDRWrapper();
		private double[] PayCategoryItemTotal = null;
		// vbPorter upgrade warning: dblFedTaxTotal As double	OnWrite(double, int, string)
		private double dblFedTaxTotal;
		// vbPorter upgrade warning: dblStateTaxTotal As double	OnWrite(double, int, string)
		private double dblStateTaxTotal;
		private double dblFICATaxTotal;
		private double dblEmployerFicaTaxTotal;
		private double dblEmployerMedicareTaxTotal;
		private double dblMedTaxTotal;
		private double dblGrossTotal;
		// vbPorter upgrade warning: dblNetTotal As double	OnWrite(int, string, double)
		private double dblNetTotal;
		private clsDRWrapper rsEICStandardLimits = new clsDRWrapper();
		private bool boolFromSchool;
		private double dblStandardFedDeduction;
        private double dblFederalStdMarriedDeduction;
        private double dblFederalStdSingleDeduction;
        private double dblFederalQualDepCreditAmount;
        private double dblFederalOtherDepCreditAmount;
		//private Dictionary<string,decimal> DistributionsByTag = new Dictionary<string, decimal>();
		private Dictionary<string,Dictionary<string,decimal>> DistributionsByCheck = new Dictionary<string, Dictionary<string, decimal>>();

		private List<TblPayStatus> mainePayStatuses = new List<TblPayStatus>();
		private void Command1_Click()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: strTemp As object	OnWrite(string, object)
				string strTemp;
				string strPrintSequence = "";
				string strEmployeeNumber = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblDeductionTotal;
				int counter;
				string strGroupText = "";
                // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(

                mainePayStatuses = StaticSettings.GlobalCommandDispatcher.Send(new GetStatePayStatusCodes()).Result.ToList();

                categoryTags = StaticSettings.GlobalCommandDispatcher.Send(new GetCategoryTags(0)).Result.ToList();
                rsTemp.OpenRecordset("select * from tblPayStatuses where type = 'Federal'", "Payroll");
                while (!rsTemp.EndOfFile())
                {
                    FederalPayStatusCodes.Add(rsTemp.Get_Fields_Int32("ID"),rsTemp.Get_Fields_Int32("StatusCode"));
                    rsTemp.MoveNext();
                }
                int x = 0;
				rsTemp.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
				if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayFreqWeekly")))
				{
					boolBiWeeklyTown = false;
				}
				else if (rsTemp.Get_Fields_Boolean("PayFreqBiWeekly"))
				{
					boolBiWeeklyTown = true;
				}
				
				// get the information from the Payroll Process Setup screen
				rsTemp.OpenRecordset("Select * from tblPayrollProcessSetup", modGlobalVariables.DEFAULTDATABASE);
				if (!rsTemp.EndOfFile())
				{
					CheckProcessSetupInfo.ID = FCConvert.ToInt32(rsTemp.Get_Fields("ID"));
					CheckProcessSetupInfo.FirstWeekOF = FCConvert.ToString(rsTemp.Get_Fields("FirstWeekOF"));
					CheckProcessSetupInfo.NumberOfPayWeeks = FCConvert.ToInt16(rsTemp.Get_Fields("NumberOfPayWeeks"));
					CheckProcessSetupInfo.PayDate = (DateTime)rsTemp.Get_Fields_DateTime("PayDate");
					if (FCConvert.ToString(rsTemp.Get_Fields_DateTime("WeekEndDate")) == string.Empty)
					{
						MessageBox.Show("Week Ending Date was not specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						boolErrorInDistributions = true;
						return;
					}
					else
					{
						CheckProcessSetupInfo.WeekEndDate = (DateTime)rsTemp.Get_Fields_DateTime("WeekEndDate");
					}
					CheckProcessSetupInfo.WeekNumber = FCConvert.ToInt32(rsTemp.Get_Fields("WeekNumber"));
					CheckProcessSetupInfo.MonthOf = FCConvert.ToInt16(rsTemp.Get_Fields("MonthOf"));
                }
                var payYear = CheckProcessSetupInfo.PayDate.Year;
                var taxYear = payYear;
                var availableTaxYears = StaticSettings.GlobalCommandDispatcher.Send(new GetAvailableTaxYears()).Result;
                if (!availableTaxYears.Any())
                {
                    FCMessageBox.Show("Could not find tax tables", MsgBoxStyle.Critical, "No Tables Found");
                    return;
                }
                if (!availableTaxYears.Any(y => y == taxYear))
                {
                    if (availableTaxYears.Any(y => y == taxYear - 1))
                    {
                        taxYear = taxYear - 1;
                    }
                    else
                    {
                        taxYear = availableTaxYears.Max();
                    }

                    if (FCMessageBox.Show(
                        "Could not find tax information for " + payYear +
                        ". Would you like to calculate with data for " + taxYear + "?",
                        MsgBoxStyle.Question | MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton2, "No Tables Found") != DialogResult.Yes)
                    {
                        return;
                    }
                }

                taxTableService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxTableService(taxYear)).Result;
				
                if (!taxTableService.HasTables())
                {
                    FCMessageBox.Show("Unable to load tax tables", MsgBoxStyle.Critical, "Error Loading Tables");
                    return;
                }

				var federalLocality = new PayrollLocality("US");
				var maineLocality = new PayrollLocality("US","ME");
				var amountResult = taxTableService.GetSetupValue(federalLocality, "FicaMaximumWage");
                var fedAmountsSucceeded = true;
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
				var stateAmountsSucceeded = false;
                var stateTaxSetup = new Dictionary<string, decimal>();
                udtTaxSetupStuff.MaxSSN = amountResult.Value.ToDouble(); // Conversion.Val(rsTemp.Get_Fields_Double("WageSSN") + " ");
                amountResult = taxTableService.GetSetupValue(federalLocality, "FicaEmployeeRate");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.PercentSSN = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("PercentSSN") + " ");
                amountResult = taxTableService.GetSetupValue(federalLocality, "FicaEmployerRate");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.EmployerFicaRate = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("EmployerFicaRate") + "");
                amountResult = taxTableService.GetSetupValue(federalLocality, "MedicareEmployerRate");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.EmployerMedRate = amountResult.Value.ToDouble(); // Conversion.Val(rsTemp.Get_Fields_Double("EmployerMedicareRate"));
                amountResult = taxTableService.GetSetupValue(federalLocality, "MedicareThreshold");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.MedThreshold = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("EmployeeMedicareAdditionalThreshold"));
                amountResult = taxTableService.GetSetupValue(federalLocality, "MedicareThresholdRate");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.ThresholdMedRate = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("EmployeeMedicareAdditional"));
                amountResult = taxTableService.GetSetupValue(federalLocality, "MedicareMaximumWage");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.MaxMed = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("WageMedicare") + " ");
                amountResult = taxTableService.GetSetupValue(federalLocality, "MedicareEmployeeRate");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                udtTaxSetupStuff.PercentMed = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields_Double("PercentMedicare") + " ");
                amountResult = taxTableService.GetSetupValue(federalLocality, "StandardDeduction");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                dblStandardFedDeduction = amountResult.Value.ToDouble(); //Conversion.Val(rsTemp.Get_Fields("Federal") + " ");
                amountResult = taxTableService.GetSetupValue(federalLocality, "MarriedDeduction");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                dblFederalStdMarriedDeduction = amountResult.Value.ToDouble(); // rsTemp.Get_Fields_Double("FederalStdMarriedDeduction");
                amountResult = taxTableService.GetSetupValue(federalLocality, "SingleDeduction");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                dblFederalStdSingleDeduction = amountResult.Value.ToDouble(); // rsTemp.Get_Fields_Double("FederalStdSingleDeduction");
                amountResult = taxTableService.GetSetupValue(federalLocality, "QualifyingChildrenCredit");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                dblFederalQualDepCreditAmount = amountResult.Value.ToDouble(); // rsTemp.Get_Fields_Double("FederalQualifyingDependentCreditAmount");
                amountResult = taxTableService.GetSetupValue(federalLocality, "OtherDependentsCredit");
                fedAmountsSucceeded = fedAmountsSucceeded && amountResult.Success;
                dblFederalOtherDepCreditAmount = amountResult.Value.ToDouble(); // rsTemp.Get_Fields_Double("FederalOtherDependentCreditAmount");

                if (!fedAmountsSucceeded)
                {
                    FCMessageBox.Show("Could not find federal deduction values", MsgBoxStyle.Critical,
                        "No Values Found");
                    return;
                }

                amountResult = taxTableService.GetSetupValue(maineLocality, "WithholdingAllowance");
                stateAmountsSucceeded = amountResult.Success;
                stateTaxSetup.Add("WithholdingAllowance", amountResult.Value);
                amountResult = taxTableService.GetSetupValue(maineLocality, "MarriedDeduction");
				stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
				stateTaxSetup.Add("MarriedDeduction", amountResult.Value);
				amountResult = taxTableService.GetSetupValue(maineLocality, "SingleDeduction");
                stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
                stateTaxSetup.Add("SingleDeduction", amountResult.Value);
				amountResult = taxTableService.GetSetupValue(maineLocality, "SingleFullDeductionWageLimit");
                stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
				stateTaxSetup.Add("SingleFullDeductionWageLimit",amountResult.Value);
                amountResult = taxTableService.GetSetupValue(maineLocality, "MarriedFullDeductionWageLimit");
                stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
                stateTaxSetup.Add("MarriedFullDeductionWageLimit", amountResult.Value);
				amountResult = taxTableService.GetSetupValue(maineLocality, "SingleDeductionWageCutoff");
                stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
                stateTaxSetup.Add("SingleDeductionWageCutoff", amountResult.Value);
				amountResult = taxTableService.GetSetupValue(maineLocality, "MarriedDeductionWageCutoff");
                stateAmountsSucceeded = stateAmountsSucceeded && amountResult.Success;
                stateTaxSetup.Add("MarriedDeductionWageCutoff", amountResult.Value);

				if (!stateAmountsSucceeded)
                {
                    FCMessageBox.Show("Could not find state deduction values", MsgBoxStyle.Critical,
                        "No Values Found");
                    return;
                }
				
				
				

                rsTemp.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayFreqWeekly")) ? true : false);
					modGlobalRoutines.Statics.ggboolBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayFreqBiWeekly")) ? true : false);
				}
				rsTemp.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolPRBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("WeeklyPR")) ? true : false);
				}
				rsTemp.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (!rsTemp.EndOfFile())
				{
					modGlobalRoutines.Statics.ggintWeekNumber = FCConvert.ToInt16(rsTemp.Get_Fields("WeekNumber"));
					modGlobalRoutines.Statics.ggintNumberPayWeeks = FCConvert.ToInt16(rsTemp.Get_Fields("NumberOfPayWeeks"));
					modGlobalRoutines.Statics.ggboolSetupBiWeekly = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("PayBiWeeklys")) ? true : false);
					// pay run is to pay weeklys with bi's
					modGlobalRoutines.Statics.ggboolBiWeeklyDeds = (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("BiWeeklyDeds")) ? true : false);
				}
				intGridRow = 0;
				this.vsGrid.Clear();
				this.vsGrid.Rows = 1;
				this.vsGrid.Refresh();
				this.vsGrid.Redraw = false;
				boolErrorInDistributions = false;
				rsMaster = new clsDRWrapper();
				rsDistribution = new clsDRWrapper();
				rsDeductions = new clsDRWrapper();
				rsDeductionOverrides = new clsDRWrapper();
				rsMatchOverrides = new clsDRWrapper();
				rsEmployeeMatches = new clsDRWrapper();
				rsPayCategory = new clsDRWrapper();
				string strFind;
				strFind = "";
				int lngID;
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				rsTemp.OpenRecordset("select * from payrollpermissions where [Userid] = " + FCConvert.ToString(lngID), "twpy0000.vb1");
                bool executeNextEmployee = false;
                while (!rsTemp.EndOfFile())
				{
					switch (rsTemp.Get_Fields_Int32("type"))
					{
						case CNSTPYTYPEDEPTDIV:
							{
								strFind += "not deptdiv = '" + rsTemp.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEGROUP:
							{
								strFind += "not groupid = '" + rsTemp.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEIND:
							{
								strFind += "not employeenumber = '" + rsTemp.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPESEQ:
							{
								strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
								break;
							}
						case CNSTPYTYPEDEPARTMENT:
							{
								strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
								break;
							}
					}
					//end switch
					rsTemp.MoveNext();
				}
				if (strFind != string.Empty)
				{
					strFind = Strings.Mid(strFind, 1, strFind.Length - 4);
					rsMaster.Execute("delete from tbltemppayprocess where employeenumber in (select employeenumber from tblemployeemaster where " + strFind + ")", "twpy0000.vb1");
					rsMaster.Execute("delete from tbltemppayprocess where trustrecord = 1", "twpy0000.vb1");
				}
				else
				{
					rsMaster.Execute("Delete from tblTempPayProcess", modGlobalVariables.DEFAULTDATABASE);
				}
				rsDeductions.Execute("update tblemployeedeductions set currenttotal = 0", "twpy0000.vb1");
	
				rsMaster.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
				if (rsMaster.EndOfFile())
				{
					strPrintSequence = "tblEmployeeMaster.EmployeeNumber";
				}
				else
				{
					switch (rsMaster.Get_Fields_Int32("DataEntrySequence"))
					{
						case 0:
							{
								strPrintSequence = "tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,tblemployeemaster.employeenumber";
								break;
							}
						case 1:
							{
								strPrintSequence = "tblPayrollDistribution.EmployeeNumber";
								break;
							}
						case 2:
							{
								strPrintSequence = "tblEmployeeMaster.SeqNumber,tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,tblemployeemaster.employeenumber";
								break;
							}
						case 3:
							{
								strPrintSequence = "tblEmployeeMaster.DeptDiv,tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,tblemployeemaster.employeenumber";
								break;
							}
					}
					//end switch
					boolUseMultiFund = FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("UseMultiFund"));
				}

				// MAKE SURE THAT IF THE GROUP FLAG WAS SET THEN RESET THEM
				rsMaster.OpenRecordset("SELECT * from tblPayrollProcessSetup", modGlobalVariables.DEFAULTDATABASE);
				if (!rsMaster.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsMaster.Get_Fields("GroupID")))
					{
						strGroupText = FCConvert.ToString(rsMaster.Get_Fields_String("GroupText"));
						// PROCESS A THIS GROUP
						rsMaster.Execute("Update tblEmployeeMaster Set Status = 'Hold 1 Week' where Status = 'Active'", "TWPY0000.vb1");
						rsMaster.Execute("Update tblEmployeeMaster Set Status = 'Active' Where GroupID = '" + strGroupText + "' AND status = 'Hold 1 Week'", "TWPY0000.vb1");
					}
				}
				string strSQL = "";
				if (strFind == string.Empty)
				{
					strSQL = "SELECT tblTaxStatusCodes.FedExempt, tblTaxStatusCodes.FicaExempt, tblTaxStatusCodes.MedicareExempt, tblTaxStatusCodes.StateExempt, tblPayrollDistribution.*, tblPayCategories.[Type], tblPayCodes.Rate, tblPayCodes.PayCode, tblEmployeeMaster.[Status], tblEmployeeMaster.[Check], tblEmployeeMaster.FicaExempt AS MasterFICAExempt, tblEmployeeMaster.FreqCodeId, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,TBLemployeemaster.middlename, tblEmployeeMaster.Desig, tblEmployeeMaster.DeductionPercent,tblEmployeeMaster.DeductionDollars, tblEmployeeMaster.S1CheckAsP1Vacation, tblEmployeeMaster.MatchDollars, tblEmployeeMaster.MatchPercentGross, tblEmployeeMaster.MatchPercentDeduction, tblEmployeeMaster.DDPercent, tblEmployeeMaster.DDDollars, tblEmployeeMaster.MedicareExempt AS MasterMEDExempt, tblEmployeeMaster.SeqNumber,tblemployeemaster.minimumcheck" + " FROM (((tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID) INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID) INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblTaxStatusCodes ON tblPayCategories.TaxStatus = tblTaxStatusCodes.ID" + " WHERE (((tblEmployeeMaster.Status)='Active') AND ((tblPayrollDistribution.DefaultHours)<>0)) OR (((tblEmployeeMaster.Status)='Active') AND ((tblPayrollDistribution.HoursWeek)<>0)) and accountcode <> '1' ";
					rsTemp.Execute("Update tblVacationSick SET AccruedCurrent = 0,UsedCurrent = 0", "TWPY0000.vb1");
				}
				else
				{
					strSQL = "SELECT tblTaxStatusCodes.FedExempt, tblTaxStatusCodes.FicaExempt, tblTaxStatusCodes.MedicareExempt, tblTaxStatusCodes.StateExempt, tblPayrollDistribution.*, tblPayCategories.[Type], tblPayCodes.Rate, tblPayCodes.PayCode, tblEmployeeMaster.[Status], tblEmployeeMaster.[Check], tblEmployeeMaster.FicaExempt AS MasterFICAExempt, tblEmployeeMaster.FreqCodeId, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, tblEmployeeMaster.Desig, tblEmployeeMaster.DeductionPercent,tblEmployeeMaster.DeductionDollars, tblEmployeeMaster.S1CheckAsP1Vacation, tblEmployeeMaster.MatchDollars, tblEmployeeMaster.MatchPercentGross, tblEmployeeMaster.MatchPercentDeduction, tblEmployeeMaster.DDPercent, tblEmployeeMaster.DDDollars, tblEmployeeMaster.MedicareExempt AS MasterMEDExempt, tblEmployeeMaster.SeqNumber,tblemployeemaster.minimumcheck" + " FROM (((tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID) INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID) INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblTaxStatusCodes ON tblPayCategories.TaxStatus = tblTaxStatusCodes.ID" + " WHERE ((((tblEmployeeMaster.Status)='Active') AND ((tblPayrollDistribution.DefaultHours)<>0)) OR (((tblEmployeeMaster.Status)='Active') AND ((tblPayrollDistribution.HoursWeek)<>0)) and accountcode <> '1' ";
					strSQL += ") and " + strFind;
					rsTemp.Execute("Update tblVacationSick SET AccruedCurrent = 0,UsedCurrent = 0 where employeenumber not in (select employeenumber from tblemployeemaster where " + strFind + ")", "TWPY0000.vb1");
				}
				if (Strings.Left(strPrintSequence, 25) == "tblEmployeeMaster.DeptDiv")
				{
					strSQL += " ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
				}
				else
				{
					strSQL += " order by " + strPrintSequence;
					// strSQL = strSQL & " ORDER BY tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName"
				}
				rsMaster.OpenRecordset(strSQL);
				strTestEmployeeID = string.Empty;
				if (!rsMaster.EndOfFile())
					pbrStatus.Maximum = (rsMaster.RecordCount()) * 10;
				int intCounter;
				pbrStatus.Value = 0;
				lblStatus.Text = "Preparing for calculations";
				this.Refresh();

				rsEICStandardLimits.OpenRecordset("Select * from tblStandardLimits", modGlobalVariables.DEFAULTDATABASE);
				if (rsEICStandardLimits.EndOfFile())
				{
					MessageBox.Show("There are no Standard Limit records currently setup.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				SetupPayCatArray();
				
				rsPayCategory.OpenRecordset("SELECT Max(ID) as MaxID FROM tblPayCategories", "TWPY0000.vb1");
				if (!rsPayCategory.EndOfFile())
				{
					PayCategoryItemTotal = new double[FCConvert.ToInt32(rsPayCategory.Get_Fields("MaxID")) + 1];
				}

				dblGrandTotalGross = 0;
				dblGrandTotalNet = 0;
				strCurrentCheckCode = "P";
				clsDRWrapper rsDedsUpTop = new clsDRWrapper();
				rsDedsUpTop.OpenRecordset("Select * from tblEmployeeMaster");
				while (!rsMaster.EndOfFile() && !modGlobalVariables.Statics.pboolUnloadProcessGrid)
				{
					boolFromSchool = false;
					intTWeeks = -1;
					intTWHWeeks = -1;

					// If rsDedsUpTop.FindFirstRecord("EmployeeNumber", rsMaster.Fields("EmployeeNumber")) Then
					if (rsDedsUpTop.FindFirst("EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "'"))
					{
						modGlobalRoutines.Statics.ggintDedsUpTop = FCConvert.ToInt16(rsDedsUpTop.Get_Fields_Double("PeriodDeds"));
						modGlobalRoutines.Statics.ggintFreqCodeID = FCConvert.ToInt16(rsDedsUpTop.Get_Fields("FreqCodeID"));
					}
					else
					{
						modGlobalRoutines.Statics.ggintDedsUpTop = 0;
						modGlobalRoutines.Statics.ggintFreqCodeID = 0;
					}
					//App.DoEvents();
					if (strTestEmployeeID == FCConvert.ToString(rsMaster.Get_Fields("EmployeeNumber")))
					{
						if (pbrStatus.Value + 10 > pbrStatus.Maximum)
						{
							pbrStatus.Value = pbrStatus.Maximum;
						}
						else
						{
							UpdateProgressBar();
						}
                        executeNextEmployee = true;
                        goto NextEmployee;
						
					}
					else
					{
						strTestEmployeeID = FCConvert.ToString(rsMaster.Get_Fields("EmployeeNumber"));
					}
					intFactor = GetFrequencyFactor();
					if (intFactor == 0)
					{
                        // this person does not get paid for this period
                        executeNextEmployee = true;
                        goto NextEmployee;
                      
                    }
					else
					{
						// verify the pay frequency
						if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strTestEmployeeID))
						{
						}
						else
						{
                            executeNextEmployee = true;
                            goto NextEmployee;
    
						}
					}
					modGlobalVariables.Statics.gboolAdditionalStateTaxNetPercent = false;
					modGlobalVariables.Statics.gboolAdditionalFederalTaxNetPercent = false;
					gintLevyGridRow = 0;
					dblGrossTotal = 0;
					strMultiCheckCode = "P0";
                    DistributionsByCheck.Clear();
					if (pbrStatus.Value > 100)
						pbrStatus.Value = 99;

					lblStatus.Text = "Processing Employee " + strTestEmployeeID;
					UpdateProgressBar();
					this.Refresh();

					rsDistribution.OpenRecordset("SELECT tblTaxStatusCodes.FedExempt, tblTaxStatusCodes.FicaExempt, tblTaxStatusCodes.MedicareExempt, tblTaxStatusCodes.StateExempt, tblPayrollDistribution.*, tblPayCategories.CategoryNumber, tblPayCategories.Type, tblPayCategories.Description, tblPayCodes.Rate, tblPayCodes.PayCode " + " FROM ((tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID) INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID) INNER JOIN tblTaxStatusCodes ON tblPayCategories.TaxStatus = tblTaxStatusCodes.ID " + " WHERE (((tblPayrollDistribution.EmployeeNumber)='" + strTestEmployeeID + "')) and accountcode <> '1' ORDER BY tblPayrollDistribution.RecordNumber;");
					if (!rsDistribution.EndOfFile())
					{
						aryDistribution = new PAYROLLDISTRIBUTION[rsDistribution.RecordCount() + 1];
						aryCalcedDist = new CALCULATEDDISTRIBUTION[rsDistribution.RecordCount() + 1];
						rsContracts.OpenRecordset("select contracts.ID as ContractID,contractaccounts.originalamount as originalamount,account,description,startdate,enddate,encumbrancejournal,encumbranceid,vendor from contracts inner join contractaccounts on (contracts.ID = contractaccounts.contractid) where employeenumber = '" + strTestEmployeeID + "' order by contracts.ID,account", "twpy0000.vb1");
						aryContractAccounts = new CONTRACTRECORD[1 + 1];
						x = 0;
						while (!rsContracts.EndOfFile())
						{
							strTemp = "select sum(distgrosspay) as thesum from tblcheckdetail where checkvoid = 0 and contractid = " + rsContracts.Get_Fields("contractid") + " and distaccountnumber = '" + rsContracts.Get_Fields("account") + "'";
							rsTemp.OpenRecordset(strTemp, "twpy0000.vb1");
							if (!rsTemp.EndOfFile())
							{
								if (Conversion.Val(rsTemp.Get_Fields("thesum")) < Conversion.Val(rsContracts.Get_Fields("originalamount")))
								{
									if (x > 1)
									{
										Array.Resize(ref aryContractAccounts, x + 1);
									}
									aryContractAccounts[x].Account = FCConvert.ToString(rsContracts.Get_Fields("account"));
									aryContractAccounts[x].ContractID = FCConvert.ToInt32(rsContracts.Get_Fields("contractid"));
									aryContractAccounts[x].Description = FCConvert.ToString(rsContracts.Get_Fields_String("description"));
									aryContractAccounts[x].EncumbranceJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(rsContracts.Get_Fields("encumbrancejournal"))));
									aryContractAccounts[x].OriginalAmount = Conversion.Val(rsContracts.Get_Fields("originalamount"));
									aryContractAccounts[x].Paid = Conversion.Val(rsTemp.Get_Fields("thesum"));
									x += 1;
								}
							}
							else
							{
								if (x > 1)
								{
									Array.Resize(ref aryContractAccounts, x + 1);
								}
								aryContractAccounts[x].Account = FCConvert.ToString(rsContracts.Get_Fields("account"));
								aryContractAccounts[x].ContractID = FCConvert.ToInt32(rsContracts.Get_Fields("contractid"));
								aryContractAccounts[x].Description = FCConvert.ToString(rsContracts.Get_Fields_String("description"));
								aryContractAccounts[x].EncumbranceJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(rsContracts.Get_Fields("encumbrancejournal"))));
								aryContractAccounts[x].OriginalAmount = Conversion.Val(rsContracts.Get_Fields("originalamount"));
								aryContractAccounts[x].Paid = 0;
								x += 1;
							}
							rsContracts.MoveNext();
						}
						strTemp = rsDistribution.Get_Fields("accountnumber");

						if (!rsDedsUpTop.EndOfFile())
						{
							intTaxUpTop = FCConvert.ToInt16(rsDedsUpTop.Get_Fields_Double("PeriodTaxes"));
							intDeductionUpTop = FCConvert.ToInt16(rsDedsUpTop.Get_Fields_Double("PeriodDeds"));
						}
					}

					rsDeductions.OpenRecordset("SELECT tblEmployeeDeductions.* FROM tblEmployeeDeductions WHERE EmployeeNumber = '" + strTestEmployeeID + "' order by tblEmployeeDeductions.RecordNumber;", "TWPY0000.vb1");
					if (!rsDeductions.EndOfFile())
					{
						aryDeductions = new PayrollEmployeeDeductions[rsDeductions.RecordCount() + 1];
						rsDeductionOverrides.OpenRecordset("select * from deductionoverrides where employeenumber = '" + strTestEmployeeID + "' order by recordnumber", "twpy0000.vb1");
					}

					rsEmployeeMatches.OpenRecordset("SELECT tblEmployersMatch.*, tblEmployersMatch.EmployeeNumber, tblTaxStatusCodes.FedExempt, tblTaxStatusCodes.FicaExempt, tblTaxStatusCodes.MedicareExempt, tblTaxStatusCodes.StateExempt, tblTaxStatusCodes.Pertains FROM (tblEmployersMatch INNER JOIN tblDeductionSetup ON tblEmployersMatch.DeductionCode = tblDeductionSetup.ID) INNER JOIN tblTaxStatusCodes ON tblDeductionSetup.TaxStatusCode = tblTaxStatusCodes.ID " + " WHERE (((tblEmployersMatch.EmployeeNumber)='" + strTestEmployeeID + "')) ORDER BY TBLEMPLOYERSMATCH.RECORDNUMBER", "TWPY0000.vb1");
					if (!rsEmployeeMatches.EndOfFile())
					{
						aryEmployeeMatches = new PayrollEmployeeMatches[rsEmployeeMatches.RecordCount() + 1];
						rsMatchOverrides.OpenRecordset("select * from matchoverrides where employeenumber = '" + strTestEmployeeID + "' order by recordnumber", "twpy0000.vb1");
					}

					for (x = 0; x <= (Information.UBound(PayCategoryItemTotal, 1)); x++)
					{
						PayCategoryItemTotal[x] = 0;
					}
					// x
					vsGrid.TextMatrix(0, 2, "Total Gross");
					vsGrid.TextMatrix(0, 3, "Net Pay");
					intEmployeeCheckCounter = 0;
					clearARYDeductions();
					clearARYEmployeeMatches();
					if (!rsDeductions.EndOfFile())
					{
						aryDeductions = new PayrollEmployeeDeductions[rsDeductions.RecordCount() + 1];
						LoadDeductionArray();
					}
					else
					{
						aryDeductions = new PayrollEmployeeDeductions[0 + 1];
					}
					if (!rsEmployeeMatches.EndOfFile())
					{
						aryEmployeeMatches = new PayrollEmployeeMatches[rsEmployeeMatches.RecordCount() + 1];
						LoadMatchArray();
					}
					else
					{
						aryEmployeeMatches = new PayrollEmployeeMatches[0 + 1];
					}
					NextCheckForSameEmp:
					;

					intTWeeks = -1;
					intTWHWeeks = -1;
					// vbPorter upgrade warning: intArrayCounter As int	OnWriteFCConvert.ToInt32(
					int intArrayCounter;
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(aryDeductions, 1)); intArrayCounter++)
					{
						aryDeductions[intArrayCounter].DeductionGross = 0;
					}
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(aryEmployeeMatches, 1)); intArrayCounter++)
					{
						aryEmployeeMatches[intArrayCounter].MatchGross = 0;
						aryEmployeeMatches[intArrayCounter].Amount = 0;
					}

					intGridRow += 1;
					vsGrid.Rows += 1;
					if (modGlobalVariables.Statics.pboolUnloadProcessGrid)
						return;
					vsGrid.TextMatrix(intGridRow, 1, "Employee #" + strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 4, fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("FirstName") + " " + rsMaster.Get_Fields("middlename")) + " " + rsMaster.Get_Fields_String("LastName") + " " + rsMaster.Get_Fields_String("Desig"));
					vsGrid.TextMatrix(intGridRow, 12, strMultiCheckCode);
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 0);
					vsGrid.IsSubtotal(intGridRow, true);
					intEmployeeRowID = intGridRow;
					// Clear out annualized gross's
					udtAnnualizedEmployee.AnnualizedStateTax = 0;
					udtAnnualizedEmployee.AnnualizedFedTax = 0;

					GetAllDistributions();
					if (boolErrorInDistributions)
					{
						lblStatus.Text = "Error in Calculating Distributions";
						goto QuitCalculation;
					}

					if (dblGrossTotal == 0 && !pboolNonPaid)
					{
						intGridRow -= 2;
						vsGrid.Rows -= 2;
                        executeNextEmployee = true;
                        goto NextEmployee;
					}
					intEmployeeCheckCounter += 1;
					if (intEmployeeCheckCounter == 1 && strMultiCheckCode != "P0")
					{
						MessageBox.Show("Employee " + strTestEmployeeID + " is ONLY being paid from distributions assigned as separate check. Please verify that deductions and matches are being applied appropriately.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 1, "Gross after Distribution");
					vsGrid.TextMatrix(intGridRow, 7, "Total Tax Gross   ");
					vsGrid.TextMatrix(intGridRow, 3, "Fed Tax Gross     ");
					vsGrid.TextMatrix(intGridRow, 6, "State Tax Gross   ");
					vsGrid.TextMatrix(intGridRow, 4, "FICA Tax Gross    ");
					vsGrid.TextMatrix(intGridRow, 5, "Med Tax Gross     ");
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 7, Strings.Format(udtEmployee.TotalGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtEmployee.FedTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtEmployee.StateTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtEmployee.FICATaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtEmployee.MedTaxGross, "0.00"));
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 1, "Ann. Gross after Distribution");
					vsGrid.TextMatrix(intGridRow, 3, "Ann. Fed Gross    ");
					vsGrid.TextMatrix(intGridRow, 6, "Ann. State Gross  ");
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtAnnualizedEmployee.AnnualizedFedTax, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAnnualizedEmployee.AnnualizedStateTax, "0.00"));
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					// Get deduction setup information
					// lblStatus.Text = "Processing Employee " & strTestEmployeeID & " Deduction Information"
					// Call UpdateProgressBar
					// Me.Refresh
					LoadDeductionSetupInfo();
					// Load the default tax status codes
					LoadDefaultTaxStatusCodes();
					// Get all deductions
					if (!rsDeductions.EndOfFile())
					{
						rsDeductions.MoveFirst();
						GetAllDeductions();
					}
					// Call UpdatePayrollStepTable("Calculation", strTestEmployeeID, "Deductions Calculated")
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 1, "Gross after Deductions");
					vsGrid.TextMatrix(intGridRow, 7, "Total Gross   ");
					vsGrid.TextMatrix(intGridRow, 3, "Fed Tax Gross     ");
					vsGrid.TextMatrix(intGridRow, 6, "State Tax Gross   ");
					vsGrid.TextMatrix(intGridRow, 4, "FICA Tax Gross    ");
					vsGrid.TextMatrix(intGridRow, 5, "Med Tax Gross     ");
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(intGridRow, 7, Strings.Format(udtEmployee.TotalGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtEmployee.FedTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtEmployee.StateTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtEmployee.FICATaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtEmployee.MedTaxGross, "0.00"));
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, "Ann. after Deductions");
					vsGrid.TextMatrix(intGridRow, 3, "Ann. Fed Gross    ");
					vsGrid.TextMatrix(intGridRow, 6, "Ann. State Gross  ");
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					udtAnnualizedEmployee.AnnualizedFedTax = GetAnnualizedAmount(udtEmployee.FedTaxGross, intTWeeks);
					udtAnnualizedEmployee.AnnualizedStateTax = GetAnnualizedAmount(udtEmployee.StateTaxGross, intTWeeks);
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtAnnualizedEmployee.AnnualizedFedTax, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAnnualizedEmployee.AnnualizedStateTax, "0.00"));
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);

					GetEmployeeMatch();

					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, "Gross after Employer Match");
					vsGrid.TextMatrix(intGridRow, 7, "Total Gross   ");
					vsGrid.TextMatrix(intGridRow, 3, "Fed Tax Gross     ");
					vsGrid.TextMatrix(intGridRow, 6, "State Tax Gross   ");
					vsGrid.TextMatrix(intGridRow, 4, "FICA Tax Gross    ");
					vsGrid.TextMatrix(intGridRow, 5, "Med Tax Gross     ");
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 7, Strings.Format(udtEmployee.TotalGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtEmployee.FedTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtEmployee.StateTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtEmployee.FICATaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtEmployee.MedTaxGross, "0.00"));
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, "Ann. after Employers Match");
					vsGrid.TextMatrix(intGridRow, 3, "Ann. Fed Gross    ");
					vsGrid.TextMatrix(intGridRow, 6, "Ann. State Gross  ");
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					udtAnnualizedEmployee.AnnualizedFedTax = GetAnnualizedAmount(udtEmployee.FedTaxGross, intTWeeks);
					udtAnnualizedEmployee.AnnualizedStateTax = GetAnnualizedAmount(udtEmployee.StateTaxGross, intTWeeks);
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtAnnualizedEmployee.AnnualizedFedTax, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAnnualizedEmployee.AnnualizedStateTax, "0.00"));
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);

					udtEmployee.TotalNet = FCConvert.ToDouble(Strings.Format(udtEmployee.TotalGross, "0.00"));
					udtAdjustedEmployee.FedTaxGross = udtEmployee.FedTaxGross;
					udtAdjustedEmployee.FICATaxGross = udtEmployee.FICATaxGross;
					udtAdjustedEmployee.MedTaxGross = udtEmployee.MedTaxGross;
					udtAdjustedEmployee.StateTaxGross = udtEmployee.StateTaxGross;
					CalculateFICATaxes();
					CalculateMEDTaxes();
					CalculateFEDTaxes(ref rsDedsUpTop);
                    CalculateSTATETaxes(ref rsDedsUpTop,stateTaxSetup);

					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, "Tax Totals");
					vsGrid.TextMatrix(intGridRow, 3, "FedTaxTotal");
					vsGrid.TextMatrix(intGridRow, 6, "StateTaxTotal");
					vsGrid.TextMatrix(intGridRow, 4, "FICATaxTotal");
					vsGrid.TextMatrix(intGridRow, 5, "MEDTaxTotal");
					vsGrid.RowOutlineLevel(intGridRow, 1);
					vsGrid.IsSubtotal(intGridRow, true);
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(dblFedTaxTotal, "#0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(dblStateTaxTotal, "#0.00"));
					vsGrid.TextMatrix(intGridRow, 4, Strings.Format(dblFICATaxTotal, "#0.00"));
					vsGrid.TextMatrix(intGridRow, 5, Strings.Format(dblMedTaxTotal, "#0.00"));
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					
					CreateTaxTotalTempPayProcessRecord();
					// Call UpdatePayrollStepTable("Calculation", strTestEmployeeID, "Temp Total Record Created")
					// CALCULATE NET PAY
					clsDRWrapper rsNet = new clsDRWrapper();
					double dblDeductionAmount = 0;
					// SET THE AMOUNT VARIABLES TO ZERO
					dblNetTotal = 0;
					dblDeductionAmount = 0;
					for (intCounter = 0; intCounter <= Information.UBound(aryDeductions, 1) - 1; intCounter++)
					{
						dblDeductionAmount += aryDeductions[intCounter].Amount;
					}
					// intCounter
					// THI
					rsNet.OpenRecordset("Select * from tblTempPayProcess where TotalRecord = 1 and EmployeeNumber = '" + strTestEmployeeID + "' and MasterRecord = '" + strMultiCheckCode + "'", modGlobalVariables.DEFAULTDATABASE);
					dblNetTotal = FCConvert.ToDouble(Strings.Format(udtEmployee.TotalGross - dblDeductionAmount, "0.00"));
					dblNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal - rsNet.Get_Fields("FICATaxWH"), "0.00"));
					dblNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal - rsNet.Get_Fields("MedicareTaxWH"), "0.00"));
					dblNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal - rsNet.Get_Fields("FederalTaxWH"), "0.00"));
					dblNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal - rsNet.Get_Fields("StateTaxWH"), "0.00"));
					dblNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal, "0.00"));
					// MATTHEW 10/05/2004 NEED TO FIX A ROUNDING ISSUE (ISSUE WITH TOPSHAM DATA)
					udtEmployee.TotalNet = FCConvert.ToDouble(Strings.Format(dblNetTotal, "0.00"));
					if (dblNetTotal < 0)
					{
						if (!FixNetFromDeductions(dblNetTotal))
						{
							// aborted due to - net
							boolErrorInDistributions = true;
							lblStatus.Text = "Negative Net Error";
							MessageBox.Show("Calculation Cancelled", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							//App.DoEvents();
							return;
						}
						// SET THE ADJUSTEDEMPLOYEE UDT EQUAL TO 0 IF THE AMOUNTS
						// COME OUT OF THE CALCUATION AS A NEGATIVE
						if (udtAdjustedEmployee.FedTaxGross < 0)
							udtAdjustedEmployee.FedTaxGross = 0;
						if (udtAdjustedEmployee.FICATaxGross < 0)
							udtAdjustedEmployee.FICATaxGross = 0;
						if (udtAdjustedEmployee.MedTaxGross < 0)
							udtAdjustedEmployee.MedTaxGross = 0;
						if (udtAdjustedEmployee.StateTaxGross < 0)
							udtAdjustedEmployee.StateTaxGross = 0;
						// place adjusted tax gross amounts into original so that we can recalculate taxes
						udtEmployee.FedTaxGross = udtAdjustedEmployee.FedTaxGross;
						udtEmployee.FICATaxGross = udtAdjustedEmployee.FICATaxGross;
						udtEmployee.MedTaxGross = udtAdjustedEmployee.MedTaxGross;
						udtEmployee.StateTaxGross = udtAdjustedEmployee.StateTaxGross;
						// MATTHEW 10/05/2004 NEED TO FIX A ROUNDING ISSUE (ISSUE WITH TOPSHAM DATA)
						udtEmployee.TotalNet = FCConvert.ToDouble(Strings.Format(udtEmployee.TotalGross, "0.00"));
						CalculateFICATaxes();
						CalculateMEDTaxes();
						CalculateFEDTaxes(ref rsDedsUpTop);
						CalculateSTATETaxes(ref rsDedsUpTop,stateTaxSetup);
						
						dblDeductionAmount = 0;
						for (intCounter = 0; intCounter <= Information.UBound(aryDeductions, 1) - 1; intCounter++)
						{
							dblDeductionAmount += aryDeductions[intCounter].Amount;
						}
						rsNet.OpenRecordset("Select * from tblTempPayProcess where TotalRecord = 1 and EmployeeNumber = '" + strTestEmployeeID + "' and MasterRecord = '" + strMultiCheckCode + "'", modGlobalVariables.DEFAULTDATABASE);
						dblNetTotal = udtEmployee.TotalGross - dblDeductionAmount;
						dblNetTotal -= rsNet.Get_Fields("FICATaxWH");
						dblNetTotal -= rsNet.Get_Fields("MedicareTaxWH");
						dblNetTotal -= rsNet.Get_Fields("FederalTaxWH");
						dblNetTotal -= rsNet.Get_Fields("StateTaxWH");
						// MATTHEW 10/05/2004 NEED TO FIX A ROUNDING ISSUE (ISSUE WITH TOPSHAM DATA)
						udtEmployee.TotalNet = FCConvert.ToDouble(Strings.Format(dblNetTotal, "0.00"));
						// matthew 05/17/03
						// problem with fed and state being neg
						intGridRow += 1;
						vsGrid.Rows += 1;
						vsGrid.RowData(intGridRow, strTestEmployeeID);
						vsGrid.TextMatrix(intGridRow, 1, "Tax Totals");
						vsGrid.TextMatrix(intGridRow, 3, "FedTaxTotal");
						vsGrid.TextMatrix(intGridRow, 6, "StateTaxTotal");
						vsGrid.TextMatrix(intGridRow, 4, "FICATaxTotal");
						vsGrid.TextMatrix(intGridRow, 5, "MEDTaxTotal");
						vsGrid.RowOutlineLevel(intGridRow, 1);
						vsGrid.IsSubtotal(intGridRow, true);
						intGridRow += 1;
						vsGrid.Rows += 1;
						vsGrid.RowData(intGridRow, strTestEmployeeID);
						vsGrid.TextMatrix(intGridRow, 3, Strings.Format(dblFedTaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 6, Strings.Format(dblStateTaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 4, Strings.Format(dblFICATaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 5, Strings.Format(dblMedTaxTotal, "#0.00"));
						vsGrid.RowOutlineLevel(intGridRow, 2);
						vsGrid.IsSubtotal(intGridRow, false);
                        CreateTaxTotalTempPayProcessRecord (false);

                    }
					else
					{
						// add the net pay to the total record
						CreateNetPayTempPayProcessRecord(dblNetTotal);
					}
					// *************************************************************************************
					// ADJUSTING THE FED AND STATE TAX BY THE ADDITIONAL NET PERCENT IF THAT IS WHAT
					// IS SPECIFIED ON THEIR MASTER SCREEN  Matthew 07/01/03
					if ((modGlobalVariables.Statics.gboolAdditionalFederalTaxNetPercent || modGlobalVariables.Statics.gboolAdditionalStateTaxNetPercent) && dblNetTotal > 0)
					{
						// vbPorter upgrade warning: dblTempNetTotal As double	OnWrite(string, double)
						double dblTempNetTotal = 0;
						double dblTempNetAmount = 0;
						dblTempNetAmount = dblNetTotal;
						if (modGlobalVariables.Statics.gboolAdditionalFederalTaxNetPercent && dblNetTotal > 0)
						{
							dblTempNetTotal = FCConvert.ToDouble(Strings.Format(dblNetTotal * modGlobalVariables.Statics.gdblAdditionalFederalTaxNetPercent, "0.00"));
							dblNetTotal -= dblTempNetTotal;
							if (dblNetTotal < 0)
							{
								dblTempNetTotal += dblNetTotal;
								dblNetTotal = 0;
							}
							dblFedTaxTotal += FCConvert.ToDouble(Strings.Format(dblTempNetTotal, "0.00"));
						}
						if (modGlobalVariables.Statics.gboolAdditionalStateTaxNetPercent && dblNetTotal > 0)
						{
							dblTempNetTotal = FCConvert.ToDouble(Strings.Format(dblTempNetAmount * modGlobalVariables.Statics.gdblAdditionalStateTaxNetPercent, "0.00"));
							dblNetTotal -= dblTempNetTotal;
							if (dblNetTotal < 0)
							{
								dblTempNetTotal += dblNetTotal;
								dblNetTotal = 0;
							}
							dblStateTaxTotal += dblTempNetTotal;
						}
						intGridRow += 1;
						vsGrid.Rows += 1;
						vsGrid.RowData(intGridRow, strTestEmployeeID);
						vsGrid.TextMatrix(intGridRow, 1, "Tax Totals");
						vsGrid.TextMatrix(intGridRow, 3, "FedTaxTotal");
						vsGrid.TextMatrix(intGridRow, 6, "StateTaxTotal");
						vsGrid.TextMatrix(intGridRow, 4, "FICATaxTotal");
						vsGrid.TextMatrix(intGridRow, 5, "MEDTaxTotal");
						vsGrid.RowOutlineLevel(intGridRow, 1);
						vsGrid.IsSubtotal(intGridRow, true);
						intGridRow += 1;
						vsGrid.Rows += 1;
						vsGrid.RowData(intGridRow, strTestEmployeeID);
						vsGrid.TextMatrix(intGridRow, 3, Strings.Format(dblFedTaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 6, Strings.Format(dblStateTaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 4, Strings.Format(dblFICATaxTotal, "#0.00"));
						vsGrid.TextMatrix(intGridRow, 5, Strings.Format(dblMedTaxTotal, "#0.00"));
						vsGrid.RowOutlineLevel(intGridRow, 2);
						vsGrid.IsSubtotal(intGridRow, false);
						CreateTaxTotalTempPayProcessRecord(false);
						// add the net pay to the total record
						// MATTHEW 10/05/2004 NEED TO FIX A ROUNDING ISSUE (ISSUE WITH TOPSHAM DATA)
						udtEmployee.TotalNet = FCConvert.ToDouble(Strings.Format(dblNetTotal, "0.00"));
						CreateNetPayTempPayProcessRecord(dblNetTotal);
					}
					// *************************************************************************************
					CheckForPercentNetDeductions();
					// CHECK FOR LEVY DEDUCTIONS
					CheckForLevyDeductions();					
					CreateDeductionTempPayProcessRecord();					
					CreateMatchTempPayProcessRecord();
					CalculateVacSickTime(ref rsDedsUpTop);
					// UPDATE THE TABLE TBLTEMPPAYROLLPROCESS WITH DIRECT DEPOSIT RECORDS
					CreateDirectDepositTempPayProcessRecord(fecherFoundation.Strings.UCase(FCConvert.ToString(rsMaster.Get_Fields_String("Check"))) == "REGULAR");
					// Call UpdatePayrollStepTable("Calculation", strTestEmployeeID, "Direct Deposit Records Created")
					UpdateTotalRecordWithDirectDeposit();
					if (pbrStatus.Value <= pbrStatus.Maximum - 2)
					{
						pbrStatus.Value = pbrStatus.Value + 2;
					}
					else
					{
						pbrStatus.Value = 0;
						pbrStatus.Value = pbrStatus.Value + 2;
					}
					this.Refresh();
					if (chkNext.CheckState == CheckState.Checked)
					{
						// A
						if (MessageBox.Show("Continue to next employee? Selecting NO will end this check process.", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							rsMaster.MoveNext();
						}
						else
						{
							FinishGrid();
							this.vsGrid.Redraw = true;
							lblStatus.Text = "Process stopped by the user.";
							return;
						}
						// add the gross total and net total to this employee record
						vsGrid.TextMatrix(intEmployeeRowID, 2, Strings.Format(udtEmployee.TotalGross, "0.00"));
						vsGrid.TextMatrix(intEmployeeRowID, 3, Strings.Format(udtEmployee.TotalNet, "0.00"));
						dblGrandTotalNet += udtEmployee.TotalNet;
						dblGrandTotalGross += udtEmployee.TotalGross;
					}
					else
					{
						// add the gross total and net total to this employee record
						vsGrid.TextMatrix(intEmployeeRowID, 2, Strings.Format(udtEmployee.TotalGross, "0.00"));
						vsGrid.TextMatrix(intEmployeeRowID, 3, Strings.Format(udtEmployee.TotalNet, "0.00"));
						dblGrandTotalNet += udtEmployee.TotalNet;
						dblGrandTotalGross += udtEmployee.TotalGross;
                        executeNextEmployee = true;
                        goto NextEmployee;
					}
                // Call UpdatePayrollStepTable("Calculation", strTestEmployeeID, "Employee Record Completed")
                NextEmployee:;
                    if (executeNextEmployee)
                    {
                        if (boolSepCheck)
                        {
                            dblGrossTotal = 0;
                            boolSepCheck = false;
                            strMultiCheckCode = Strings.Left(strMultiCheckCode, 1) + FCConvert.ToString(Conversion.Val(Strings.Right(strMultiCheckCode, 1)) + 1);
                            rsDeductions.MoveFirst();
                            rsDistribution.MoveFirst();
                            rsEmployeeMatches.MoveFirst();
                            goto NextCheckForSameEmp;
                        }
                        else
                        {
                            rsMaster.MoveNext();
                        }
                        executeNextEmployee = false;
                    }
                }
				if (modGlobalVariables.Statics.pboolUnloadProcessGrid)
					return;
				// CALCULATE THE TRUST AND AGENCY CHECKS
				// lblStatus.Text = "Calculating Trust and Agency Information for Employee " & strTestEmployeeID
				lblStatus.Text = "Processing Trust and Agency Information";
				// for Employee " & strTestEmployeeID
				UpdateProgressBar();
				this.Refresh();
				CalculateTrustANDAgencyChecks();
                this.Refresh();
                // Call UpdatePayrollStepTable("Calculation", strTestEmployeeID, "Trust and Agency Records Created")
                modGlobalRoutines.UpdatePayrollStepTable("Calculation");
				FinishGrid();
                this.Refresh();
                // THIS FUNCTION WAS DONE MAINLY FOR THE NEW MULTI - TOWN FUNCTION THAT IS BEING ADDED
                // WE'LL NEED TO KNOW WHAT SEQ OR TOWN EACH EMPLOYEE BELONGED TO IN CASE THEY WERE TO
                // SWITCH FROM ONE TO ANOTHER.
                SetSequenceNumbersToHistoryRecords();
                this.Refresh();
                SetMultiFundRecords();
                this.Refresh();
                lblStatus.Visible = false;
				pbrStatus.Visible = false;
				//FC:FINAL:DDU:#2716 - after loading accounts show grid on full form
				vsGrid.Location = new System.Drawing.Point(30, 30);
				vsGrid.Size = new System.Drawing.Size(vsGrid.Size.Width, ClientArea.Height - 60);
				this.vsGrid.Redraw = true;
				lblStatus.Text = "Calculation completed successfully.";
                this.Refresh();
                modColorScheme.ColorGrid(vsGrid);
				QuitCalculation:
				;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + " ");
			}
		}

		private void SetSequenceNumbersToHistoryRecords()
		{
			clsDRWrapper rsExecute = new clsDRWrapper();
			clsDRWrapper rsEmployee = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsEmployee.OpenRecordset("Select Distinct EmployeeNumber,SeqNumber from tblEmployeeMaster");
				while (!rsEmployee.EndOfFile())
				{
					rsExecute.Execute("Update tblTempPayProcess Set SequenceNumber = " + rsEmployee.Get_Fields("SeqNumber") + " Where EmployeeNumber = '" + rsEmployee.Get_Fields("EmployeeNumber") + "'", "Payroll");
					rsEmployee.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetSequenceNumbersToHistoryRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetMultiFundRecords()
		{
			int intFundNumber = 0;
			clsDRWrapper rsExecute = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (modGlobalVariables.Statics.gintUseGroupMultiFund == 0)
				{
					// rsExecute.OpenRecordset "Select * from tblPayrollAccounts where Code = 'CC'"
					// If rsExecute.EndOfFile Or Not gboolBD Then
					intFundNumber = 0;
					// Else
					// intFundNumber = Mid(rsExecute.Fields("Account"), 3, Mid(Ledger, 1, 2))
					// End If
				}
				else
				{
					intFundNumber = modGlobalVariables.Statics.gintUseGroupMultiFund;
				}
				rsExecute.Execute("Update tblTempPayProcess Set MultiFundNumber = " + FCConvert.ToString(intFundNumber), "Payroll");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetMultiFundRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckForPercentNetDeductions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				double tNet;
				// vbPorter upgrade warning: dblTemp As double	OnWrite(string, double)
				double dblTemp = 0;
				bool boolUsedOne;
				boolUsedOne = false;
				tNet = udtEmployee.TotalNet;
				for (intCounter = 0; intCounter <= (Information.UBound(aryDeductions, 1)); intCounter++)
				{
					if (udtEmployee.TotalNet > 0)
					{
						if (fecherFoundation.Strings.UCase(aryDeductions[intCounter].AmountType) == "PERCENT")
						{
							if (fecherFoundation.Strings.UCase(aryDeductions[intCounter].PercentType) == "NET")
							{
								dblTemp = FCConvert.ToDouble(Strings.Format(tNet * aryDeductions[intCounter].DeductionAmount, "0.00"));
								if (dblTemp > udtEmployee.TotalNet)
									dblTemp = udtEmployee.TotalNet;
								aryDeductions[intCounter].Amount = dblTemp;
								vsGrid.TextMatrix(aryDeductions[intCounter].RowNumber, 2, FCConvert.ToString(aryDeductions[intCounter].Amount));
								udtEmployee.TotalNet -= dblTemp;
								boolUsedOne = true;
							}
						}
					}
				}
				// intCounter
				if (boolUsedOne)
				{
					dblNetTotal = udtEmployee.TotalNet;
					CreateNetPayTempPayProcessRecord(dblNetTotal);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckForPercentNetDeductions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckForLevyDeductions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 0; intCounter <= (Information.UBound(aryDeductions, 1)); intCounter++)
				{
					if (aryDeductions[intCounter].AmountType == "Levy" && aryDeductions[intCounter].Apply)
					{
						if (udtEmployee.TotalNet > aryDeductions[intCounter].Limit)
						{
							aryDeductions[intCounter].Amount = udtEmployee.TotalNet - aryDeductions[intCounter].Limit;
							vsGrid.TextMatrix(gintLevyGridRow, 2, FCConvert.ToString(aryDeductions[intCounter].Amount));
							udtEmployee.TotalNet = aryDeductions[intCounter].Limit;
							dblNetTotal = udtEmployee.TotalNet;
							CreateNetPayTempPayProcessRecord(dblNetTotal);
						}
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + " ");
			}
		}

		private void CalculateTrustANDAgencyChecks()
		{
			string strTemp = "";
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// vbPorter upgrade warning: intArrayIndex As int	OnWriteFCConvert.ToInt32(
			int intArrayIndex;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsFileSetup = new clsDRWrapper();
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsPayTotals = new clsDRWrapper();
			clsDRWrapper rsEmployersMatch = new clsDRWrapper();
			clsDRWrapper rsDeductions = new clsDRWrapper();
			clsDRWrapper rsDeductionAccounts = new clsDRWrapper();
			clsDRWrapper rspayrollaccounts = new clsDRWrapper();
			string strTAChecksPrinted = "";
			string strgroup = "";
			DateTime dtStart;
			DateTime dtEnd;
			DateTime dtTemp;
			if (boolUseMultiFund)
				return;
			rsFileSetup.OpenRecordset("Select * from tblPayrollProcessSetup", modGlobalVariables.DEFAULTDATABASE);
			if (rsFileSetup.EndOfFile())
			{
				MessageBox.Show("Payroll Process Setup has not been created.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				strTAChecksPrinted = FCConvert.ToString(rsFileSetup.Get_Fields_String("TAChecksPrinted"));
				strgroup = FCConvert.ToString(rsFileSetup.Get_Fields_String("GroupText"));
			}
			modGlobalVariables.Statics.TrustCheck = new modGlobalVariables.TrustAgencyChecks[0 + 1];
			modGlobalVariables.Statics.CHECKRECIPIENTS = new modGlobalVariables.TrustAgencyChecks[0 + 1];
			rsData.OpenRecordset("SELECT * FROM tblRecipients ORDER BY RecptNumber", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				// check the frequency to see if this is included in this pay period
				if (FCConvert.ToString(rsData.Get_Fields("Freq")) == "0" || FCConvert.ToString(rsData.Get_Fields("Freq")) == "Weekly")
				{
					// "Weekly"
					Array.Resize(ref modGlobalVariables.Statics.CHECKRECIPIENTS, Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) + 1 + 1);
					modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].RecipientNumber = rsData.Get_Fields("ID");
					modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].Frequency = rsData.Get_Fields("Freq");
				}
				else if (FCConvert.ToString(rsData.Get_Fields("Freq")) == "1" || FCConvert.ToString(rsData.Get_Fields("Freq")) == "Monthly")
				{
					// "Monthly"
					if (Conversion.Val(rsFileSetup.Get_Fields("WeekNumber")) == Conversion.Val(rsFileSetup.Get_Fields("NumberOfPayWeeks")))
					{
						Array.Resize(ref modGlobalVariables.Statics.CHECKRECIPIENTS, Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) + 1 + 1);
						modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].RecipientNumber = rsData.Get_Fields("ID");
						modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].Frequency = rsData.Get_Fields("Freq");
					}
				}
				else if (FCConvert.ToString(rsData.Get_Fields("Freq")) == "2" || FCConvert.ToString(rsData.Get_Fields("Freq")) == "Quarterly")
				{
					// "Quarterly"
					if (Conversion.Val(rsFileSetup.Get_Fields("WeekNumber")) == Conversion.Val(rsFileSetup.Get_Fields("NumberOfPayWeeks")))
					{
						switch (rsFileSetup.Get_Fields_Int32("MonthOf"))
						{
							case 3:
							case 6:
							case 9:
							case 12:
								{
									Array.Resize(ref modGlobalVariables.Statics.CHECKRECIPIENTS, Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) + 1 + 1);
									modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].RecipientNumber = rsData.Get_Fields("ID");
									modGlobalVariables.Statics.CHECKRECIPIENTS[Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS) - 1].Frequency = rsData.Get_Fields("Freq");
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
					}
				}
				rsData.MoveNext();
			}
			if (Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) == 0)
				return;
			if (Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) > 1)
			{
				Array.Resize(ref modGlobalVariables.Statics.CHECKRECIPIENTS, Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1) - 1 + 1);
			}
			rsData.OpenRecordset("SELECT tblCategories.* FROM tblCategories Order by ID", "TWPY0000.vb1");
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No Check Recipient Categories were set up.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			// For intCounter = 0 To rsData.RecordCount - 1
			while (!rsData.EndOfFile())
			{
				for (intArrayIndex = 0; intArrayIndex <= (Information.UBound(modGlobalVariables.Statics.CHECKRECIPIENTS, 1)); intArrayIndex++)
				{
					if (modGlobalVariables.Statics.CHECKRECIPIENTS[intArrayIndex].RecipientNumber == rsData.Get_Fields("Include"))
					{
						// this category has a recipient that is being included
						// in this pay run
						Array.Resize(ref modGlobalVariables.Statics.TrustCheck, Information.UBound(modGlobalVariables.Statics.TrustCheck, 1) + 1 + 1);
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].Description = rsData.Get_Fields("Description");
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].DeductionDescription = rsData.Get_Fields("Description");
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].DeductionNumber = rsData.Get_Fields("code");
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].RecipientNumber = rsData.Get_Fields("Include");
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].Frequency = modGlobalVariables.Statics.CHECKRECIPIENTS[intArrayIndex].Frequency;
						modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].CheckRecipCatID = rsData.Get_Fields("ID");
						string vbPorterVar = modGlobalVariables.Statics.CHECKRECIPIENTS[intArrayIndex].Frequency;
						if (vbPorterVar == "Weekly")
						{
							// Weekly
							modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].ColumnToAdd = "Current";
						}
						else if (vbPorterVar == "Monthly")
						{
							// Monthly
							modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].ColumnToAdd = "Month";
						}
						else if (vbPorterVar == "Quarterly")
						{
							// Quarterly
							modGlobalVariables.Statics.TrustCheck[Information.UBound(modGlobalVariables.Statics.TrustCheck) - 1].ColumnToAdd = "Quarter";
						}
						else
						{
						}
						break;
					}
				}
				rsData.MoveNext();
				// Next
			}
			// GET RID OF THE EXTRA ELEMENT IN THE ARRAY
			if (Information.UBound(modGlobalVariables.Statics.TrustCheck, 1) > 1)
			{
				Array.Resize(ref modGlobalVariables.Statics.TrustCheck, Information.UBound(modGlobalVariables.Statics.TrustCheck, 1) - 1 + 1);
			}
			// ID    Description
			// 1           Federal Tax
			// 2           FICA
			// 3           Medicare
			// 4           State Tax
			// 5           Local Tax
			// NOW GET ALL EMPLOYEES AND SEE IF THEY USE ANY OF THESE CATEGORIES
			bool boolWeeklyAmountOnly;
			bool boolWeeklyRecipients;
			boolWeeklyAmountOnly = false;
			boolWeeklyRecipients = false;
			if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "YES")
			{
				// YES MEANS THAT MONTHLY TA HAVE BEEN RUN
				// THIS IS A SINGLE EMPLOYEE SO RUN ALL TA CHECKS FOR THIS EMPLOYEE FOR THIS PAY RUN ONLY
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Where Status = 'Active' Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
				boolWeeklyAmountOnly = true;
				// boolWeeklyRecipients = True
			}
			else if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "NO")
			{
				// NO MEANS THAT MONTHLY TA'S HAVE NOT BEEN RUN
				// THIS IS A SINGLE EMPLOYEE BUT RUN ONLY RECIPIENTS THAT ARE ONLY WEEKLY
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Where Status = 'Active' Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
			}
			else if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "FULL")
			{
				// THIS IS FULL PAY RUN SO RUN ALL TA CHECKS
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
			}
			else if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "GROUP")
			{
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Where GroupID = '" + strgroup + "' Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
			}
			else if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "WEEKLY")
			{
				// THIS IS FOR THOSE WHO HAVE RECIPIENTS THAT ARE ONLY WEEKLY
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
				boolWeeklyAmountOnly = true;
				boolWeeklyRecipients = true;
			}
			else if (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "ALL")
			{
				// THIS IS FULL PAY RUN SO RUN ALL TA CHECKS
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
			}
			else
			{
				// NO APPARENT SETTING SO RUN ALL TA CHECKS
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
			}
			rsDeductionAccounts.OpenRecordset("Select * from tblDeductionSetup", modGlobalVariables.DEFAULTDATABASE);
			rspayrollaccounts.OpenRecordset("Select * from tblPayrollAccounts", modGlobalVariables.DEFAULTDATABASE);
			
			if ((fecherFoundation.Strings.UCase(strTAChecksPrinted) == "GROUP") || (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "NO") || (fecherFoundation.Strings.UCase(strTAChecksPrinted) == "YES"))
			{
				while (!rsMaster.EndOfFile())
				{
					rsEmployersMatch.OpenRecordset("Select * from tblEmployersMatch where EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "'", modGlobalVariables.DEFAULTDATABASE);
					rsDeductions.OpenRecordset("Select tblEmployeeDeductions.*, tblDeductionSetup.* from tblEmployeeDeductions INNER JOIN tblDeductionSetup ON tblEmployeeDeductions.DeductionCode = tblDeductionSetup.ID where EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "'", modGlobalVariables.DEFAULTDATABASE);
					UpdateProgressBar();
					for (intCounter = 0; intCounter <= (Information.UBound(modGlobalVariables.Statics.TrustCheck, 1)); intCounter++)
					{
						if (!(boolWeeklyRecipients && modGlobalVariables.Statics.TrustCheck[intCounter].Frequency != "Weekly"))
						{
							if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFEDTAX
								|| Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX
								|| Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX
								|| Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPESTATETAX)
							{
								if (boolWeeklyAmountOnly)
								{
									// dtStart = gdatCurrentPayDate
									// dtEnd = dtStart
									modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
									if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
									{
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
									}
								}
								else
								{
									string vbPorterVar1 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
									if (vbPorterVar1 == "Weekly")
									{
										// Weekly
										// dtStart = gdatCurrentPayDate
										// dtEnd = gdatCurrentPayDate
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
										{
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
									else if (vbPorterVar1 == "Monthly")
									{
										// Monthly
										// dtStart = Month(gdatCurrentPayDate) & "/1/" & Year(gdatCurrentPayDate)
										// dtEnd = gdatCurrentPayDate
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
										{
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
									else if (vbPorterVar1 == "Quarterly")
									{
										// Quarterly
										// dtTemp = gdatCurrentPayDate
										// Call GetDatesForAQuarter(dtStart, dtEnd, dtTemp)
										// dtEnd = gdatCurrentPayDate
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
										{
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
								}

								modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = "0";
								// rspayrollaccounts.FindFirstRecord "CategoryID", Trim(modGlobalVariables.Statics.TrustCheck(intCounter).CheckRecipCatID)
								rspayrollaccounts.FindFirst("CategoryID = " + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID)));
								if (rspayrollaccounts.NoMatch)
								{
								}
								else
								{
									modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = rspayrollaccounts.Get_Fields("Account");
									modGlobalVariables.Statics.TrustCheck[intCounter].DeductionDescription = rspayrollaccounts.Get_Fields("Description");
								}
							}
							else
							{
								// get the employers match totals
								// rsEmployersMatch.FindFirstRecord "DeductionCode", Val(modGlobalVariables.Statics.TrustCheck(intCounter).DeductionNumber)
								rsEmployersMatch.FindFirst("DeductionCode = " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)));
								if (rsEmployersMatch.NoMatch)
								{
								}
								else
								{
								NextMatchAmount:
									;
									if (boolWeeklyAmountOnly)
									{
										// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("CurrentTotal")
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
									}
									else
									{
										string vbPorterVar2 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
										if (vbPorterVar2 == "Weekly")
										{
											// Weekly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar2 == "Monthly")
										{
											// Monthly
											// 
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar2 == "Quarterly")
										{
											// Quarterly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("QTDTotal") + rsEmployersMatch.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
								}
								rsDeductions.FindFirst("DeductionCode = " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)));
								if (rsDeductions.NoMatch)
								{
								}
								else
								{
								NextDeductionRecord:
									;
									if (boolWeeklyAmountOnly)
									{
										// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("CurrentTotal")
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
									}
									else
									{
										string vbPorterVar3 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
										if (vbPorterVar3 == "Weekly")
										{
											// Weekly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar3 == "Monthly")
										{
											// Monthly
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar3 == "Quarterly")
										{
											// Quarterly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("QTDTotal") + rsDeductions.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), rsMaster.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
								}
								rsDeductionAccounts.FindFirst("ID = " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber.ToString()));
								if (rsDeductionAccounts.NoMatch)
								{
								}
								else
								{
									modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = rsDeductionAccounts.Get_Fields_String("AccountID");
									modGlobalVariables.Statics.TrustCheck[intCounter].DeductionDescription = rsDeductionAccounts.Get_Fields("Description");
								}
							}
						}
						NextRecipient:
						;
					}
					rsMaster.MoveNext();
				}
			}
			else
			{
				for (intCounter = 0; intCounter <= (Information.UBound(modGlobalVariables.Statics.TrustCheck, 1)); intCounter++)
				{
					UpdateProgressBar();
					if (!(boolWeeklyRecipients && modGlobalVariables.Statics.TrustCheck[intCounter].Frequency != "Weekly"))
					{
						switch (modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID)
						{
						// Case 1, 2, 3, 4, 5 'these are the hard coded taxes
							case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
							case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
							case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
							case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
								{
									if (boolWeeklyAmountOnly)
									{
										// dtStart = gdatCurrentPayDate
										// dtEnd = dtStart
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
										if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
										{
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
									else
									{
										string vbPorterVar4 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
										if (vbPorterVar4 == "Weekly")
										{
											// Weekly
											// dtStart = gdatCurrentPayDate
											// dtEnd = gdatCurrentPayDate
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
											{
												modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											}
										}
										else if (vbPorterVar4 == "Monthly")
										{
											// Monthly
											// dtStart = Month(gdatCurrentPayDate) & "/1/" & Year(gdatCurrentPayDate)
											// dtEnd = gdatCurrentPayDate
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
											{
												modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
												modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											}
										}
										else if (vbPorterVar4 == "Quarterly")
										{
											// Quarterly
											// dtTemp = gdatCurrentPayDate
											// Call GetDatesForAQuarter(dtStart, dtEnd, dtTemp)
											// dtEnd = gdatCurrentPayDate
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDTaxWHGross(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											if (Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID) == modPYConstants.CNSTPAYTOTALTYPEFICATAX)
											{
												modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
												modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX, "", modGlobalVariables.Statics.gdatCurrentPayDate);
											}
										}
									}
									// Call rsPayTotals.OpenRecordset("select sum(" & strTemp & ") as tot from tblcheckdetail where employeenumber = '" & rsMaster.Fields("employeenumber") & "' and checkvoid = 0 and paydate between '" & dtStart & "' and '" & dtEnd & "'", "twpy0000.vb1")
									// If Not rsPayTotals.EndOfFile Then
									// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + Val(rsPayTotals.Fields("tot"))
									// End If
									modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = "0";
									// rspayrollaccounts.FindFirstRecord "CategoryID", Trim(modGlobalVariables.Statics.TrustCheck(intCounter).CheckRecipCatID)
									if (rspayrollaccounts.FindFirst("isnull(CategoryID,0) = " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID.ToString())))
									{
										modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = rspayrollaccounts.Get_Fields("Account");
										modGlobalVariables.Statics.TrustCheck[intCounter].DeductionDescription = rspayrollaccounts.Get_Fields("Description");
									}
									else
									{
									}
									break;
								}
							default:
								{
									// get the employers match totals
									if (boolWeeklyAmountOnly)
									{
										// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("CurrentTotal")
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
									}
									else
									{
										string vbPorterVar5 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
										if (vbPorterVar5 == "Weekly")
										{
											// Weekly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar5 == "Monthly")
										{
											// Monthly
											// 
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar5 == "Quarterly")
										{
											// Quarterly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsEmployersMatch.Fields("QTDTotal") + rsEmployersMatch.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDMatchTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
									// get the Deduction totals
									if (boolWeeklyAmountOnly)
									{
										// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("CurrentTotal")
										modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
									}
									else
									{
										string vbPorterVar6 = modGlobalVariables.Statics.TrustCheck[intCounter].Frequency;
										if (vbPorterVar6 == "Weekly")
										{
											// Weekly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar6 == "Monthly")
										{
											// Monthly
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetMTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
										else if (vbPorterVar6 == "Quarterly")
										{
											// Quarterly
											// modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount = modGlobalVariables.Statics.TrustCheck(intCounter).TotalAmount + rsDeductions.Fields("QTDTotal") + rsDeductions.Fields("CurrentTotal")
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
											modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount += modCoreysSweeterCode.GetQTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber)), "", modGlobalVariables.Statics.gdatCurrentPayDate);
										}
									}
									// rsDeductionAccounts.FindFirstRecord "ID", Trim(modGlobalVariables.Statics.TrustCheck(intCounter).DeductionNumber)
									if (rsDeductionAccounts.FindFirst("ID = " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber.ToString())))
									{
										modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber = rsDeductionAccounts.Get_Fields_String("AccountID");
										modGlobalVariables.Statics.TrustCheck[intCounter].DeductionDescription = rsDeductionAccounts.Get_Fields("Description");
									}
									break;
								}
						}
						//end switch
					}
				}
			}
			// NOW WRITE THE RECIPTIENT AND AMOUNTS TO THE TABLE TBLTEMPPAYPROCESS
			rsMaster.OpenRecordset("Select * from tblTempPayProcess where isnull(TrustRecord,0) = 1", modGlobalVariables.DEFAULTDATABASE);
			for (intCounter = 0; intCounter <= (Information.UBound(modGlobalVariables.Statics.TrustCheck, 1)); intCounter++)
			{
				UpdateProgressBar();
				if (modGlobalVariables.Statics.TrustCheck[intCounter].RecipientNumber != 0)
				{
					// ZERO IS THE CODE FOR STOP CHECK
					rsMaster.AddNew();
					rsMaster.Set_Fields("TrustRecord", true);
					rsMaster.Set_Fields("TrustRecipientID", modGlobalVariables.Statics.TrustCheck[intCounter].RecipientNumber);
					rsMaster.Set_Fields("TrustCategoryID", modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID);
					// If modGlobalVariables.Statics.TrustCheck(intCounter).CheckRecipCatID = 2 Or modGlobalVariables.Statics.TrustCheck(intCounter).CheckRecipCatID = 3 Then
					if (modGlobalVariables.Statics.TrustCheck[intCounter].CheckRecipCatID == 3)
					{
						// *2 because of employee and employer contribution matthew/ron 6/4/2003
						// Medicare, Fica is now separate for employee and employer
						rsMaster.Set_Fields("TrustAmount", Strings.Format(modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount * 2, "0.00"));
					}
					else
					{
						rsMaster.Set_Fields("TrustAmount", Strings.Format(modGlobalVariables.Statics.TrustCheck[intCounter].TotalAmount, "0.00"));
					}
					rsMaster.Set_Fields("TrustDeductionDescription", modGlobalVariables.Statics.TrustCheck[intCounter].DeductionDescription);
					rsMaster.Set_Fields("TrustDeductionNumber", modGlobalVariables.Statics.TrustCheck[intCounter].DeductionNumber);
					if (modGlobalVariables.Statics.gintUseGroupMultiFund == 0)
					{
						rsMaster.Set_Fields("TrustDeductionAccountNumber", modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber);
					}
					else
					{
						strTemp = modGlobalVariables.Statics.TrustCheck[intCounter].DeductionAccountNumber;
						// corey 01/03/2006 Need to format the fund
						Strings.MidSet(ref strTemp, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
						rsMaster.Set_Fields("TrustDeductionAccountNumber", strTemp);
					}
					if (rsMaster.Get_Fields_Double("TrustAmount") == 0)
					{
						rsMaster.Delete();
						goto NextRec;
					}
					rsMaster.Update();
					NextRec:
					;
				}
			}
		}

		private void FinishGrid()
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// intGridRow = intGridRow + 1
			vsGrid.Rows += 1;
			intGridRow = (vsGrid.Rows - 1);
			vsGrid.TextMatrix(intGridRow, 1, "Totals");
			vsGrid.TextMatrix(intGridRow, 2, Strings.Format(dblGrandTotalGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 3, Strings.Format(dblGrandTotalNet, "0.00"));
			vsGrid.RowOutlineLevel(intGridRow, 0);
			vsGrid.IsSubtotal(intGridRow, true);
			// COLLAPSE ALL ROWS TO SHOW JUST THE HEADERS
			for (intRow = 1; intRow <= (vsGrid.Rows - 1); intRow++)
			{
				if (vsGrid.RowOutlineLevel(intRow) == 0 || vsGrid.RowOutlineLevel(intRow) == 1)
				{
					vsGrid.IsCollapsed(intRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, 1, intRow, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			vsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeOutline;
			modColorScheme.ColorGrid(vsGrid, 1, vsGrid.Rows - 1, 1, (vsGrid.Cols - 1), true);
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public object GetPayFreqDesc(int lngID)
		{
			object GetPayFreqDesc = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblFrequencyCodes where ID = " + FCConvert.ToString(lngID), modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				GetPayFreqDesc = string.Empty;
			}
			else
			{
				CheckProcessSetupInfo.PayFrequencyDesc = FCConvert.ToString(rsData.Get_Fields("Description"));
				CheckProcessSetupInfo.PayFrequencyCode = FCConvert.ToString(rsData.Get_Fields("FrequencyCode"));
				if (rsData.Get_Fields("PeriodsPerYear") > 0)
				{
					CheckProcessSetupInfo.PayFrequencyValue = (52 / rsData.Get_Fields("PeriodsPerYear")) * CheckProcessSetupInfo.DeductionUpTop;
				}
				else
				{
					CheckProcessSetupInfo.PayFrequencyValue = 0;
				}
			}
			return GetPayFreqDesc;
		}

		public void CalculateVacSickTime(ref clsDRWrapper rsEmp)
		{
			double dblTotal;
			double dblUsedTotal = 0;
			double dblValue = 0;
			double dblNonPayTotal = 0;
			int intCounter;
			clsDRWrapper rsVacSickTotals = new clsDRWrapper();
			clsDRWrapper rsVacSickCodes = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsCurrent = new clsDRWrapper();
			clsDRWrapper rsAss = new clsDRWrapper();
			clsDRWrapper rsPayCat = new clsDRWrapper();
			clsDRWrapper rsDistribution = new clsDRWrapper();
			// On Error GoTo ErrorHandler
			// get the information from the top of the distribution screen
			CheckProcessSetupInfo.TaxUpTop = intTaxUpTop;
			CheckProcessSetupInfo.DeductionUpTop = intDeductionUpTop;
			// get the information from the Payroll Process Setup screen
			rsVacSickTotals.OpenRecordset("Select * from tblPayrollProcessSetup", modGlobalVariables.DEFAULTDATABASE);
			if (!rsVacSickTotals.EndOfFile())
			{
				CheckProcessSetupInfo.ID = FCConvert.ToInt32(rsVacSickTotals.Get_Fields("ID"));
				CheckProcessSetupInfo.FirstWeekOF = FCConvert.ToString(rsVacSickTotals.Get_Fields("FirstWeekOF"));
				CheckProcessSetupInfo.NumberOfPayWeeks = FCConvert.ToInt16(rsVacSickTotals.Get_Fields("NumberOfPayWeeks"));
				CheckProcessSetupInfo.PayDate = (DateTime)rsVacSickTotals.Get_Fields_DateTime("PayDate");
				if (FCConvert.ToString(rsVacSickTotals.Get_Fields_DateTime("WeekEndDate")) == string.Empty)
				{
					MessageBox.Show("Week Ending Date was not specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					boolErrorInDistributions = true;
					return;
				}
				else
				{
					CheckProcessSetupInfo.WeekEndDate = (DateTime)rsVacSickTotals.Get_Fields_DateTime("WeekEndDate");
				}
				CheckProcessSetupInfo.WeekNumber = FCConvert.ToInt32(rsVacSickTotals.Get_Fields("WeekNumber"));
				CheckProcessSetupInfo.MonthOf = FCConvert.ToInt16(rsVacSickTotals.Get_Fields("MonthOf"));
			}
			// GET THE PAY FREQUENCY FROM THE TABLE FIELDS LENGTHS
			rsVacSickTotals.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
			if (!rsVacSickTotals.EndOfFile())
			{
				CheckProcessSetupInfo.PayFreqWeekly = FCConvert.ToBoolean(rsVacSickTotals.Get_Fields_Boolean("PayFreqWeekly"));
				CheckProcessSetupInfo.PayFreqBiWeekly = FCConvert.ToBoolean(rsVacSickTotals.Get_Fields_Boolean("PayFreqBiWeekly"));
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					// get this from GE
					rsVacSickTotals.OpenRecordset("Select FiscalStart from Budgetary", "TWBD0000.vb1");
					CheckProcessSetupInfo.FirstFiscalMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsVacSickTotals.Get_Fields_String("FiscalStart"))));
				}
				else
				{
					CheckProcessSetupInfo.FirstFiscalMonth = FCConvert.ToInt16(rsVacSickTotals.Get_Fields("FirstFiscalMonth"));
				}
			}
			// THIS WILL GET EMPLOYEE SPECIFIC INFORMATION
			// With rsVacSickTotals
			// .OpenRecordset "Select * from tblEmployeeMaster where EmployeeNumber = '" & strTestEmployeeID & "'", DEFAULTDATABASE
			// If Not rsVacSickTotals.EndOfFile Then
			if (!rsEmp.EndOfFile())
			{
				// fill in the data from the employee
				if (!rsEmp.IsFieldNull("DateHire"))
					CheckProcessSetupInfo.DateHire = rsEmp.Get_Fields_DateTime("DateHire");
				CheckProcessSetupInfo.DateAnniversary = rsEmp.Get_Fields_String("DateAnniversary");
				if (!rsEmp.IsFieldNull("DateBirth") && !rsEmp.Get_Fields_DateTime("DateBirth").IsEmptyDate())
					CheckProcessSetupInfo.DateBirth = rsEmp.Get_Fields_DateTime("DateBirth");
				CheckProcessSetupInfo.DATAENTRY = rsEmp.Get_Fields("DataEntry");
				CheckProcessSetupInfo.DeptDiv = rsEmp.Get_Fields("DeptDiv");
				CheckProcessSetupInfo.SeqNumber = rsEmp.Get_Fields("SeqNumber");
				CheckProcessSetupInfo.Check = rsEmp.Get_Fields_String("Check");
				CheckProcessSetupInfo.HrsDay = rsEmp.Get_Fields_Double("HrsDay");
				CheckProcessSetupInfo.HrsWk = rsEmp.Get_Fields_Double("HrsWk");
				CheckProcessSetupInfo.PeriodTaxes = FCConvert.ToInt32(rsEmp.Get_Fields_Double("PeriodTaxes"));
				CheckProcessSetupInfo.PeriodDeds = FCConvert.ToInt32(rsEmp.Get_Fields_Double("PeriodDeds"));
				CheckProcessSetupInfo.PayFrequencyID = rsEmp.Get_Fields("FreqCodeId");
				CheckProcessSetupInfo.PayFrequencyDesc = FCConvert.ToString(GetPayFreqDesc(CheckProcessSetupInfo.PayFrequencyID));
			}
			// NOW CALCULATE THE VAC/SICK TIME FOR THIS PERSON
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Vac/Sick/etc");
			vsGrid.TextMatrix(intGridRow, 2, "Accrued");
			vsGrid.TextMatrix(intGridRow, 3, "Used   ");
			vsGrid.TextMatrix(intGridRow, 4, "Type");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			SetupVacationSickCodesArray();
			rsVacSickTotals.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + strTestEmployeeID + "' AND selected = 1", modGlobalVariables.DEFAULTDATABASE);
			while (!rsVacSickTotals.EndOfFile())
			{
                if (!DistributionsByCheck.TryGetValue("P" + (intEmployeeCheckCounter - 1), out var distributionsByTag))
                {
					distributionsByTag = new Dictionary<string, decimal>();
                }
				if (intEmployeeCheckCounter == 1 || rsMaster.Get_Fields_Boolean("S1CheckAsP1Vacation"))
				{
					if (FCConvert.ToInt32(rsVacSickTotals.Get_Fields("CodeID")) != 0)
					{
						// this is a default code record and we need to get that info from the tblCodes table
						// use the array VacationSickCodeInfo(.Fields("CodeID")) to get default info
						dblValue = CalculateVacSickValue(ref rsVacSickTotals, true, rsVacSickTotals.Get_Fields_Double("UsedBalance"),distributionsByTag);
					}
					else
					{
						// this is a manually entered type so we have all of the data
						dblValue = CalculateVacSickValue(ref rsVacSickTotals, false, rsVacSickTotals.Get_Fields_Double("UsedBalance"),distributionsByTag);
					}
				}
				else
				{
					dblValue = 0;
					// MATTHEW FIXED FOR TOPSHAM 6/28/2005
					if (FCConvert.ToInt32(rsVacSickTotals.Get_Fields("CodeID")) != 0)
					{
						CurrentVacationSickInfo.Code = FCConvert.ToString(modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].ID);
						CurrentVacationSickInfo.CodeType = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].CodeType;
						CurrentVacationSickInfo.Description = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].Description;
						CurrentVacationSickInfo.DHW = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].DHW;
						CurrentVacationSickInfo.Max = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].Max;
						CurrentVacationSickInfo.MonthDay = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].MonthDay;
						CurrentVacationSickInfo.Rate = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].Rate;
						CurrentVacationSickInfo.Type = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].Type;
						CurrentVacationSickInfo.Cleared = modGlobalVariables.Statics.VacationSickCodeInfo[rsVacSickTotals.Get_Fields("CodeID")].Cleared;
					}
					else
					{
						// CodeID in this recordset is the ID of the Code Type in tblCodes
						CurrentVacationSickInfo.Code = FCConvert.ToString(rsVacSickTotals.Get_Fields("CodeID"));
						CurrentVacationSickInfo.CodeType = FCConvert.ToInt32(rsVacSickTotals.Get_Fields("TypeID"));
						CurrentVacationSickInfo.Description = FCConvert.ToString(rsVacSickTotals.Get_Fields_String("Des"));
						CurrentVacationSickInfo.DHW = FCConvert.ToString(rsVacSickTotals.Get_Fields_String("UM"));
						CurrentVacationSickInfo.Max = rsVacSickTotals.Get_Fields("Max");
						CurrentVacationSickInfo.MonthDay = FCConvert.ToString(rsVacSickTotals.Get_Fields_String("MD"));
						CurrentVacationSickInfo.Rate = FCConvert.ToDecimal(rsVacSickTotals.Get_Fields_Double("Rates"));
						CurrentVacationSickInfo.Type = FCConvert.ToString(rsVacSickTotals.Get_Fields("Type"));
						// .Cleared = RecordSet.Fields("Cleared")
					}
				}
				// UPDATE THE TEMPPAYROLLPROCESS TABLE WITH THIS VALUE AND THE USED (TYPE) VALUE
				rsData.OpenRecordset("Select * from tblTempPayProcess where EmployeeNumber = '" + strTestEmployeeID + "' AND VSTypeID = " + rsVacSickTotals.Get_Fields("TypeID") + " and VSRecord = 1 and MasterRecord = '" + strMultiCheckCode + "'", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					rsData.AddNew();
				}
				else
				{
					rsData.Edit();
				}
				rsData.Set_Fields("EmployeeNumber", strTestEmployeeID);
				rsData.Set_Fields("EmployeeName", rsMaster.Get_Fields_String("FirstName") + " " + rsMaster.Get_Fields_String("LastName") + " " + rsMaster.Get_Fields_String("Desig"));
				rsData.Set_Fields("MasterRecord", strMultiCheckCode);
				dblNonPayTotal = 0;
				dblUsedTotal = 0;
				rsAss.OpenRecordset("Select * from tblAssociateCodes where CodeType = " + rsVacSickTotals.Get_Fields("TypeID"), modGlobalVariables.DEFAULTDATABASE);
				if (rsAss.EndOfFile())
				{
				}
				else
				{
					while (!rsAss.EndOfFile())
					{
						rsCurrent.OpenRecordset("Select sum(DistHours) as AmountUsed from tblTempPayProcess where EmployeeNumber = '" + strTestEmployeeID + "' AND MasterRecord = '" + strMultiCheckCode + "' AND DistPayCategory = " + FCConvert.ToString(Conversion.Val(rsAss.Get_Fields("PayCatID"))), modGlobalVariables.DEFAULTDATABASE);
						if (!rsCurrent.EndOfFile())
						{
							// commented out 6/19/2003 Matthew
							// Call rsPayCat.OpenRecordset("Select Type from tblPayCategories where CategoryNumber = " & Val(rsAss.Fields("PayCatID")), DEFAULTDATABASE)
							rsPayCat.OpenRecordset("Select Type from tblPayCategories where ID = " + FCConvert.ToString(Conversion.Val(rsAss.Get_Fields("PayCatID"))), modGlobalVariables.DEFAULTDATABASE);
							if (rsPayCat.EndOfFile())
							{
								goto NextTypeID;
							}
							else
							{
								string strCheck = "";
								if (strMultiCheckCode == "P0")
								{
									strCheck = " AND (AccountCode = '2' or AccountCode = '3' or AccountCode = '13')";
								}
								else if (strMultiCheckCode == "P1")
								{
									strCheck = " AND AccountCode = '4'";
								}
								else if (strMultiCheckCode == "P2")
								{
									strCheck = " AND AccountCode = '5'";
								}
								else if (strMultiCheckCode == "P3")
								{
									strCheck = " AND AccountCode = '6'";
								}
								else if (strMultiCheckCode == "P4")
								{
									strCheck = " AND AccountCode = '7'";
								}
								else if (strMultiCheckCode == "P5")
								{
									strCheck = " AND AccountCode = '8'";
								}
								else if (strMultiCheckCode == "P6")
								{
									strCheck = " AND AccountCode = '9'";
								}
								else if (strMultiCheckCode == "P7")
								{
									strCheck = " AND AccountCode = '10'";
								}
								else if (strMultiCheckCode == "P8")
								{
									strCheck = " AND AccountCode = '11'";
								}
								else if (strMultiCheckCode == "P9")
								{
									strCheck = " AND AccountCode = '12'";
								}
								// rsDistribution.OpenRecordset "Select * from tblPayrollDistribution where EmployeeNumber = '" & strTestEmployeeID & "' AND Cat = " & Val(rsAss.Fields("PayCatID")), DEFAULTDATABASE
								rsDistribution.OpenRecordset("Select * from tblPayrollDistribution where EmployeeNumber = '" + strTestEmployeeID + "' AND Cat = " + FCConvert.ToString(Conversion.Val(rsAss.Get_Fields("PayCatID"))) + strCheck, modGlobalVariables.DEFAULTDATABASE);
								// *********************************************
								if (rsDistribution.EndOfFile())
								{
								}
								else
								{

									if (Conversion.Val(rsDistribution.Get_Fields_String("AccountCode")) > 3)
									{
										if (strMultiCheckCode != "P0")
										{
										}
										else
										{
											goto NextAss;
										}
									}
									else
									{
										if (strMultiCheckCode == "P0" || rsMaster.Get_Fields_Boolean("S1CheckAsP1Vacation"))
										{
										}
										else
										{
											goto NextAss;
										}
									}
									// **********************************************************************************************************************
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsPayCat.Get_Fields("Type"))) == "Non-Pay (Hours)")
									{
										if (Conversion.Val(rsCurrent.Get_Fields("amountused")) >= 0)
										{
											dblNonPayTotal += Conversion.Val(rsCurrent.Get_Fields("AmountUsed"));
										}
										else
										{
											dblUsedTotal -= Conversion.Val(rsCurrent.Get_Fields("amountused"));
										}
										// End If
									}
									else
									{
										dblUsedTotal += Conversion.Val(rsCurrent.Get_Fields("AmountUsed"));
									}
								}
							}
						}
						NextAss:
						;
						rsAss.MoveNext();
					}
				}
				dblNonPayTotal += dblValue;
				if (CurrentVacationSickInfo.Max < rsVacSickTotals.Get_Fields_Double("UsedBalance") + dblNonPayTotal - dblUsedTotal + Conversion.Val(rsVacSickTotals.Get_Fields_Double("AccruedCurrent")))
				{
					dblNonPayTotal = (CurrentVacationSickInfo.Max + dblUsedTotal) - rsVacSickTotals.Get_Fields_Double("UsedBalance") - Conversion.Val(rsVacSickTotals.Get_Fields("accruedcurrent"));
					if (dblNonPayTotal < 0)
						dblNonPayTotal = 0;
				}
				if (dblUsedTotal == 0 && dblNonPayTotal == 0)
					goto NextTypeID;
				if (dblNonPayTotal != 0)
				{
					rsData.Set_Fields("VSAccrued", Strings.Format(dblNonPayTotal, "0.0000"));
				}
				rsData.Set_Fields("VSUsed", Strings.Format(dblUsedTotal, "0.0000"));
				rsData.Set_Fields("VSTypeID", rsVacSickTotals.Get_Fields("TypeID"));
				rsData.Set_Fields("VSBalance", rsVacSickTotals.Get_Fields_Double("UsedBalance"));
				rsData.Set_Fields("VSRecord", true);
				rsData.Update();
				// REPLACE VALUE IN THE ACCRUED CURRENT COLUMN FOR THIS TYPE (IE. SICK) IN THE
				// EMPLOYEE VACATION SICK SCREEN.
				rsData.OpenRecordset("Select * from tblVacationSick where selected = 1 and EmployeeNumber = '" + strTestEmployeeID + "' AND TypeID = " + rsVacSickTotals.Get_Fields("TypeID"), modGlobalVariables.DEFAULTDATABASE);
				rsData.Edit();
				if (dblNonPayTotal != 0)
				{
					// matthew 03/15/2005 for call ID 65245
					// rsData.Fields("AccruedCurrent") = Format(dblNonPayTotal, "0.0000")
					rsData.Set_Fields("AccruedCurrent", Strings.Format(rsData.Get_Fields_Double("AccruedCurrent") + dblNonPayTotal, "0.0000"));
				}
				// matthew 03/15/2005 for call ID 65245
				// rsData.Fields("UsedCurrent") = Format(dblUsedTotal, "0.0000")
				rsData.Set_Fields("UsedCurrent", Strings.Format(rsData.Get_Fields_Double("UsedCurrent") + dblUsedTotal, "0.0000"));
				rsData.Update();
				intGridRow += 1;
				vsGrid.Rows += 1;
				vsGrid.RowData(intGridRow, strTestEmployeeID);
				if (dblNonPayTotal != 0)
				{
					vsGrid.TextMatrix(intGridRow, 2, Strings.Format(dblNonPayTotal, "0.0000"));
				}
				vsGrid.TextMatrix(intGridRow, 3, Strings.Format(dblUsedTotal, "0.0000"));
				vsGrid.TextMatrix(intGridRow, 4, FCConvert.ToString(GetPayFreqDescription(rsVacSickTotals.Get_Fields("TypeID"))));
				vsGrid.RowOutlineLevel(intGridRow, 2);
				vsGrid.IsSubtotal(intGridRow, false);
				NextTypeID:
				;
				rsVacSickTotals.MoveNext();
			}
			return;
			ErrorHandler:
			;
			// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In CalculateVacSickTime", vbCritical, "Error"
		}
		// vbPorter upgrade warning: 'Return' As double	OnWrite(int, Decimal)
		public double CalculateVacSickValue(ref clsDRWrapper Recordset, bool boolUseDefaultCodeInfo, double UsedBalance,Dictionary<string,decimal> distributionsByTag)
		{
			double CalculateVacSickValue = 0;
			string strCode = "";
			string strCodeType = "";
			if (boolUseDefaultCodeInfo)
			{
				CurrentVacationSickInfo.Code = FCConvert.ToString(modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].ID);
				CurrentVacationSickInfo.CodeType = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].CodeType;
				CurrentVacationSickInfo.Description = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].Description;
				CurrentVacationSickInfo.DHW = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].DHW;
				CurrentVacationSickInfo.Max = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].Max;
				CurrentVacationSickInfo.MonthDay = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].MonthDay;
				CurrentVacationSickInfo.Rate = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].Rate;
				CurrentVacationSickInfo.Type = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].Type;
				CurrentVacationSickInfo.Cleared = modGlobalVariables.Statics.VacationSickCodeInfo[Recordset.Get_Fields("CodeID")].Cleared;
			}
			else
			{
				// CodeID in this recordset is the ID of the Code Type in tblCodes
				CurrentVacationSickInfo.Code = FCConvert.ToString(Recordset.Get_Fields("CodeID"));
				CurrentVacationSickInfo.CodeType = Recordset.Get_Fields("TypeID");
				CurrentVacationSickInfo.Description = Recordset.Get_Fields_String("Des").Trim();
				CurrentVacationSickInfo.DHW = Recordset.Get_Fields_String("UM").Trim();
				CurrentVacationSickInfo.Max = Recordset.Get_Fields("Max");
				CurrentVacationSickInfo.MonthDay = Recordset.Get_Fields_String("MD");
				CurrentVacationSickInfo.Rate = FCConvert.ToDecimal(Recordset.Get_Fields_Double("Rates"));
				CurrentVacationSickInfo.Type = Recordset.Get_Fields_String("Type").Trim();
				// .Cleared = RecordSet.Fields("Cleared")
			}
			if (DoNotApplyThisPeriod())
			{
				CalculateVacSickValue = 0;
				return CalculateVacSickValue;
			}
			// #Period = CheckProcessSetupInfo.DeductionUpTop
			// HD = CheckProcessSetupInfo.HrsDay
			// HW = CheckProcessSetupInfo.HrsWk
			// Value = CurrentVacationSickInfo.Rate
			// Weeks = CheckProcessSetupInfo.PayFrequencyValue
			// These will set the values to a default if the user has not set
			// these fields in the
			if (CheckProcessSetupInfo.HrsDay == 0)
				CheckProcessSetupInfo.HrsDay = 8;
			if (CheckProcessSetupInfo.HrsWk == 0)
				CheckProcessSetupInfo.HrsWk = 40;
			string vbPorterVar = fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.DHW)) + "|" + fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type));
			if (vbPorterVar == "DAYS|PAY PERIOD")
			{
				// Value * HD * #Period
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay * CheckProcessSetupInfo.DeductionUpTop;
			}
			else if (vbPorterVar == "DAYS|MONTH")
			{
				// Value * HD * 12 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay * intPayMonths;
				// * 12 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "DAYS|QUARTER")
			{
				// Value * HD * 4 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay;
				// * 4 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "DAYS|YEAR")
			{
				// Value * HD / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay;
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "DAYS|ANNIVERSARY")
			{
				// Value * HD / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay;
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "DAYS|ANNIVERSARY DATE")
			{
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsDay;
			}
			else if (vbPorterVar == "HOURS|THIS PAY PERIOD ONLY")
			{
				// Value
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate);
			}
			else if (vbPorterVar == "HOURS|PAY PERIOD")
			{
				// Value * #Period
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate * CheckProcessSetupInfo.DeductionUpTop);
			}
			else if (vbPorterVar == "HOURS|MONTH")
			{
				// Value * 12 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate * intPayMonths);
				// * 12 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "HOURS|QUARTER")
			{
				// Value * 4 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate);
				// * 4 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "HOURS|YEAR")
			{
				// Value / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate);
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "HOURS|ANNIVERSARY")
			{
				// Value / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate);
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "HOURS|ANNIVERSARY DATE")
			{
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate);
			}
			else if (vbPorterVar == "WEEKS|PAY PERIOD")
			{
				// Value * HW * #Period
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk * CheckProcessSetupInfo.DeductionUpTop;
			}
			else if (vbPorterVar == "WEEKS|MONTH")
			{
				// Value * HW * 12 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk * intPayMonths;
				// * 12 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "WEEKS|QUARTER")
			{
				// Value * HW * 4 / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk;
				// * 4 / 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "WEEKS|YEAR")
			{
				// Value * HW / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk;
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "WEEKS|ANNIVERSARY")
			{
				// Value * HW / 52 * Weeks
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk;
				// 52 * CheckProcessSetupInfo.PayFrequencyValue
			}
			else if (vbPorterVar == "WEEKS|ANNIVERSARY DATE")
			{
				CalculateVacSickValue = FCConvert.ToDouble(CurrentVacationSickInfo.Rate) * CheckProcessSetupInfo.HrsWk;
			}
			else if (vbPorterVar == "HOURS|HOUR")
            {
				// only uses the hardcoded tag of WorkedHours at the moment
                if (distributionsByTag.ContainsKey("Worked Hours"))
                {
                    var workedHours = distributionsByTag["Worked Hours"];
                    if (workedHours > 0)
                    {
                        CalculateVacSickValue = (CurrentVacationSickInfo.Rate * workedHours).ToDouble();
					}
                }
				
            }

					return CalculateVacSickValue;
		}

		public bool DoNotApplyThisPeriod()
		{
			bool DoNotApplyThisPeriod = false;
			// check to see if this entry gets calculated for this pay period
			// vbPorter upgrade warning: strTemp As object	OnWrite(string, string())
			string strTemp;
			int intCounter;
			// vbPorter upgrade warning: intPayWeekNumber As int	OnWriteFCConvert.ToInt32(
			int intPayWeekNumber = 0;
			// vbPorter upgrade warning: intVacSicWeek As int	OnWrite(int, string)
			int intVacSicWeek = 0;
			int intMonthOf;
			intPayMonths = 0;
			intMonthOf = CheckProcessSetupInfo.MonthOf;
			for (intCounter = 1; intCounter <= intDeductionUpTop; intCounter++)
			{
				if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "THIS PAY PERIOD ONLY")
				{
					DoNotApplyThisPeriod = false;
					return DoNotApplyThisPeriod;
				}
				else if (CurrentVacationSickInfo.Type.Trim().ToLower() == EarnedTimeAccrual.Hour.ToName().ToLower())
                {
                    return false;
                }
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "PAY PERIOD")
				{
					DoNotApplyThisPeriod = false;
					return DoNotApplyThisPeriod;
				}
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "MONTH")
				{
					// IS THE PAY PERIOD WEEK IN PAYROLL PROCESS SETUP EQUAL TO
					// THE PAY WEEK DEFINED FOR THIS VAC/SIC TYPE IN THE EMPLOYEE
					// VAC/SIC SCREEN
					intVacSicWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(CurrentVacationSickInfo.MonthDay)));
					intPayWeekNumber = CheckProcessSetupInfo.WeekNumber;
					if (intVacSicWeek == intPayWeekNumber)
					{
						// then use this calculation
						DoNotApplyThisPeriod = false;
						intPayMonths += 1;
						return DoNotApplyThisPeriod;
					}
					else
					{
						intPayWeekNumber += 1;
						if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
						{
							intPayWeekNumber = 1;
						}
					}
				}
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "QUARTER")
				{
					// this is the month of the quarter
					if (CurrentVacationSickInfo.MonthDay.Length > 2)
					{
						strTemp = Strings.Mid(CurrentVacationSickInfo.MonthDay, 3);
						intVacSicWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(CurrentVacationSickInfo.MonthDay)));
						intPayWeekNumber = CheckProcessSetupInfo.WeekNumber;
						// If intPayWeekNumber <> 1 Then
						if (intPayWeekNumber != Conversion.Val(strTemp))
						{
							DoNotApplyThisPeriod = true;
							return DoNotApplyThisPeriod;
						}
						if (intVacSicWeek == 1)
						{
							if (intMonthOf == 1 || intMonthOf == 4 || intMonthOf == 7 || intMonthOf == 10)
							{
								// then use this calculation
								DoNotApplyThisPeriod = false;
								return DoNotApplyThisPeriod;
							}
							else
							{
								intPayWeekNumber += 1;
								if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
								{
									intPayWeekNumber = 1;
									intMonthOf += 1;
									if (intMonthOf > 12)
										intMonthOf = 1;
								}
							}
						}
						else if (intVacSicWeek == 2)
						{
							if (intMonthOf == 2 || intMonthOf == 5 || intMonthOf == 8 || intMonthOf == 11)
							{
								// then use this calculation
								DoNotApplyThisPeriod = false;
								return DoNotApplyThisPeriod;
							}
							else
							{
								intPayWeekNumber += 1;
								if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
								{
									intPayWeekNumber = 1;
									intMonthOf += 1;
									if (intMonthOf > 12)
										intMonthOf = 1;
								}
							}
						}
						else if (intVacSicWeek == 3)
						{
							if (intMonthOf == 3 || intMonthOf == 6 || intMonthOf == 9 || intMonthOf == 12)
							{
								// then use this calculation
								DoNotApplyThisPeriod = false;
								return DoNotApplyThisPeriod;
							}
							else
							{
								intPayWeekNumber += 1;
								if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
								{
									intPayWeekNumber = 1;
									intMonthOf += 1;
									if (intMonthOf > 12)
										intMonthOf = 1;
								}
							}
						}
					}
					else
					{
						DoNotApplyThisPeriod = true;
						return DoNotApplyThisPeriod;
					}
				}
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "YEAR")
				{
					string[] strTemp1 = Strings.Split(CurrentVacationSickInfo.MonthDay, "/", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strTemp1, 1) >= 1)
					{
						intVacSicWeek = FCConvert.ToInt32(fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp1[1])));
					}
					else
					{
						// MsgBox "Invalid Month/Week for this Vacation/Sick entry.", vbCritical + vbOKOnly, "TRIO Software"
						break;
					}
					intPayWeekNumber = CheckProcessSetupInfo.WeekNumber;
					// check to make sure that month(?) is actually the month
					if (FCConvert.ToDateTime(CurrentVacationSickInfo.MonthDay).Month == intMonthOf)
					{
						if (intVacSicWeek == intPayWeekNumber)
						{
							// then use this calculation
							DoNotApplyThisPeriod = false;
							return DoNotApplyThisPeriod;
						}
						else
						{
							intPayWeekNumber += 1;
							if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
							{
								intPayWeekNumber = 1;
								if (intMonthOf > 12)
									intMonthOf = 1;
							}
						}
					}
					else
					{
						intPayWeekNumber += 1;
						if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
						{
							intPayWeekNumber = 1;
							intMonthOf += 1;
							if (intMonthOf > 12)
								intMonthOf = 1;
						}
					}
				}
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "ANNIVERSARY DATE")
				{
					if (Information.IsDate(CheckProcessSetupInfo.DateAnniversary))
					{
						if (Information.IsDate(CheckProcessSetupInfo.DateAnniversary + "/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year)))
						{
							clsDRWrapper rsTemp = new clsDRWrapper();
							// vbPorter upgrade warning: dtAniv As DateTime	OnWrite(string)
							DateTime dtAniv;
							dtAniv = FCConvert.ToDateTime(CheckProcessSetupInfo.DateAnniversary + "/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
							if (dtAniv.Month == modGlobalVariables.Statics.gdatCurrentPayDate.Month)
							{
								if (modGlobalVariables.Statics.gdatCurrentPayDate.Day >= dtAniv.Day)
								{
									rsTemp.OpenRecordset("select top 1 * from tblcheckdetail where employeenumber = '" + strTestEmployeeID + "' and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtAniv) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and vsrecord = 1 and vstypeid = " + FCConvert.ToString(CurrentVacationSickInfo.CodeType), "twpy0000.vb1");
									if (rsTemp.EndOfFile())
									{
										DoNotApplyThisPeriod = false;
										return DoNotApplyThisPeriod;
									}
									else
									{
										DoNotApplyThisPeriod = true;
										return DoNotApplyThisPeriod;
									}
								}
							}
							else
							{
								if (fecherFoundation.DateAndTime.DateAdd("m", -1, modGlobalVariables.Statics.gdatCurrentPayDate).Month == FCConvert.ToDateTime(CheckProcessSetupInfo.DateAnniversary).Month)
								{
									dtAniv = FCConvert.ToDateTime(CheckProcessSetupInfo.DateAnniversary + "/" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("m", -1, modGlobalVariables.Statics.gdatCurrentPayDate).Year));
									rsTemp.OpenRecordset("select top 1 * from tblcheckdetail where employeenumber = '" + strTestEmployeeID + "' and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtAniv) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", "twpy0000.vb1");
									if (rsTemp.EndOfFile())
									{
										DoNotApplyThisPeriod = false;
										return DoNotApplyThisPeriod;
									}
									else
									{
										DoNotApplyThisPeriod = true;
										return DoNotApplyThisPeriod;
									}
								}
							}
						}
						else
						{
							MessageBox.Show("Invalid Anniversary Date on Vac/Sick Codes for employee: " + strTestEmployeeID, "Invalid Anniversary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DoNotApplyThisPeriod = true;
							return DoNotApplyThisPeriod;
						}
					}
					else
					{
						MessageBox.Show("Invalid Anniversary Date on Vac/Sick Codes for employee: " + strTestEmployeeID, "Invalid Anniversary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						DoNotApplyThisPeriod = true;
						return DoNotApplyThisPeriod;
					}
				}
				else if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(CurrentVacationSickInfo.Type)) == "ANNIVERSARY")
				{
					intVacSicWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(CurrentVacationSickInfo.MonthDay)));
					intPayWeekNumber = CheckProcessSetupInfo.WeekNumber;
					if (Information.IsDate(CheckProcessSetupInfo.DateAnniversary))
					{
						// check to make sure that month(?) is actually the month
						if (FCConvert.ToDateTime(CheckProcessSetupInfo.DateAnniversary).Month == CheckProcessSetupInfo.MonthOf)
						{
							if (intVacSicWeek == intPayWeekNumber)
							{
								// then use this calculation
								DoNotApplyThisPeriod = false;
								return DoNotApplyThisPeriod;
							}
							else
							{
								intPayWeekNumber += 1;
								if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
								{
									intPayWeekNumber = 1;
									intMonthOf += 1;
									if (intMonthOf > 12)
										intMonthOf = 1;
								}
							}
						}
						else
						{
							intPayWeekNumber += 1;
							if (intPayWeekNumber > CheckProcessSetupInfo.NumberOfPayWeeks)
							{
								intPayWeekNumber = 1;
								intMonthOf += 1;
								if (intMonthOf > 12)
									intMonthOf = 1;
							}
						}
					}
					else
					{
						MessageBox.Show("Invalid Date Anniversary on Vac/Sick Codes for employee: " + strTestEmployeeID, "TRIO Sofware", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						DoNotApplyThisPeriod = true;
						return DoNotApplyThisPeriod;
					}
				}
			}
			DoNotApplyThisPeriod = true;
			return DoNotApplyThisPeriod;
		}

		public bool FixNetFromDeductions(double dblNetTotal)
		{
			bool FixNetFromDeductions = false;
			int intPriority;
			int intDedCode;
			// vbPorter upgrade warning: intHighDedCode As int	OnWriteFCConvert.ToInt32(
			int intHighDedCode;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			double dblAdjustmentTotals = 0;
			double dblDeductionTotals = 0;
			double dblDifference = 0;
			FixNetFromDeductions = true;
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Net Amount After Ded #");
			vsGrid.TextMatrix(intGridRow, 2, "Amount");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			intHighDedCode = (Information.UBound(aryDeductions, 1) - 1);
			for (intPriority = 9; intPriority >= 1; intPriority--)
			{
				for (intDedCode = intHighDedCode; intDedCode >= 0; intDedCode--)
				{
					if (aryDeductions[intDedCode].Priority == intPriority)
					{
						if (aryDeductions[intDedCode].Amount > 0)
						{
							// there is some money taken out
							if (aryDeductions[intDedCode].Amount >= (dblNetTotal * -1))
							{
								// this will cover the shortage
								if (!((aryDeductions[intDedCode].FedTaxExempt && aryDeductions[intDedCode].FICATaxExempt && aryDeductions[intDedCode].MedTaxExempt && aryDeductions[intDedCode].StateTaxExempt) || (!aryDeductions[intDedCode].FedTaxExempt && !aryDeductions[intDedCode].FICATaxExempt && !aryDeductions[intDedCode].MedTaxExempt && !aryDeductions[intDedCode].StateTaxExempt)))
								{
									// this is mixed
									if (MessageBox.Show("Employee " + strTestEmployeeID + " has a negative net." + "\r\n" + "To fix this, adjustments have to be made to at least one deduction that is partially pre and partially post tax." + "\r\n" + "This will most likely result in an incorrect taxable gross." + "\r\n" + "Do you want to continue?", "Negative Net", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
									{
										FixNetFromDeductions = false;
										return FixNetFromDeductions;
									}
									else
									{
										modGlobalFunctions.AddCYAEntry_6("PY", "Continued with negative net calculation", strTestEmployeeID);
									}
								}
								aryDeductions[intDedCode].AdjustmentAmount = aryDeductions[intDedCode].Amount - (aryDeductions[intDedCode].Amount + dblNetTotal);
								aryDeductions[intDedCode].Amount += dblNetTotal;
								// THIS WAS MOVED HERE TO SHOW THAT THERE WAS A NEGATIVE
								// AMOUNT. THE NEXT FEW LINES OF CODE WILL MAKE THE AMOUNT ZERO
								// MATTHEW 6/4/03
								intGridRow += 1;
								vsGrid.Rows += 1;
								vsGrid.RowData(intGridRow, strTestEmployeeID);
								vsGrid.TextMatrix(intGridRow, 2, FCConvert.ToString(dblNetTotal));
								dblNetTotal += aryDeductions[intDedCode].AdjustmentAmount;
								// corey 4/21/2006
								// vsGrid.TextMatrix(intGridRow, 1) = aryDeductions(intDedCode).DeductionID & " - " & aryDeductions(intDedCode).DeductionDescription
								vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryDeductions[intDedCode].DeductionCode) + " - " + aryDeductions[intDedCode].DeductionDescription);
								vsGrid.RowOutlineLevel(intGridRow, 2);
								vsGrid.IsSubtotal(intGridRow, false);
								goto Completed;
							}
							else
							{
								aryDeductions[intDedCode].AdjustmentAmount = aryDeductions[intDedCode].Amount;
								// THIS WAS MOVED HERE TO SHOW THAT THERE WAS A NEGATIVE
								// AMOUNT. THE NEXT FEW LINES OF CODE WILL MAKE THE AMOUNT ZERO
								// MATTHEW 6/4/03
								intGridRow += 1;
								vsGrid.Rows += 1;
								vsGrid.RowData(intGridRow, strTestEmployeeID);
								vsGrid.TextMatrix(intGridRow, 2, FCConvert.ToString(dblNetTotal));
								dblNetTotal += aryDeductions[intDedCode].AdjustmentAmount;
								aryDeductions[intDedCode].Amount = 0;
								// corey 4/21/2006
								// vsGrid.TextMatrix(intGridRow, 1) = aryDeductions(intDedCode).DeductionID & " - " & aryDeductions(intDedCode).DeductionDescription
								vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryDeductions[intDedCode].DeductionCode) + " - " + aryDeductions[intDedCode].DeductionDescription);
								vsGrid.RowOutlineLevel(intGridRow, 2);
								vsGrid.IsSubtotal(intGridRow, false);
							}
						}
					}
				}
				// intDedCode
			}
			// intPriority
			Completed:
			;
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Adj Deduction #");
			vsGrid.TextMatrix(intGridRow, 2, "Amount");
			vsGrid.TextMatrix(intGridRow, 3, "Adj. Amount");
			vsGrid.TextMatrix(intGridRow, 4, "Fed");
			vsGrid.TextMatrix(intGridRow, 7, "State");
			vsGrid.TextMatrix(intGridRow, 5, "FICA");
			vsGrid.TextMatrix(intGridRow, 6, "Med");

			vsGrid.TextMatrix(intGridRow, 8, "Ann. Fed Gross");
			vsGrid.TextMatrix(intGridRow, 9, "Ann. State Gross");

			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			// apply deductions to Taxable Gross values
			for (intCounter = intHighDedCode; intCounter >= 0; intCounter--)
			{
				if (aryDeductions[intCounter].FedTaxExempt)
					udtAdjustedEmployee.FedTaxGross += aryDeductions[intCounter].AdjustmentAmount;
				if (aryDeductions[intCounter].StateTaxExempt)
					udtAdjustedEmployee.StateTaxGross += aryDeductions[intCounter].AdjustmentAmount;
				if (aryDeductions[intCounter].FICATaxExempt)
					udtAdjustedEmployee.FICATaxGross += aryDeductions[intCounter].AdjustmentAmount;
				if (aryDeductions[intCounter].MedTaxExempt)
					udtAdjustedEmployee.MedTaxGross += aryDeductions[intCounter].AdjustmentAmount;
				
				// Reduce annualized grosses by Annualized deductions
				if (aryDeductions[intCounter].FedTaxExempt)
				{
					if (intDeductionUpTop > 0)
					{
						udtAnnualizedEmployee.AnnualizedFedTax = (udtAnnualizedEmployee.AnnualizedFedTax) + ((aryDeductions[intCounter].AdjustmentAmount / intDeductionUpTop) * intFactor);
						// udtAnnualizedEmployee.AnnualizedFedTax = (udtAnnualizedEmployee.AnnualizedFedTax) + ((aryDeductions(intCounter).AdjustmentAmount / intDeductionUpTop) * intFactor)
					}
				}
				if (aryDeductions[intCounter].StateTaxExempt)
				{
					if (intDeductionUpTop > 0)
					{
						udtAnnualizedEmployee.AnnualizedStateTax += ((aryDeductions[intCounter].AdjustmentAmount / intDeductionUpTop) * intFactor);
					}
				}
				
				// Display Adjusted amounts to the user screen
				intGridRow += 1;
				vsGrid.Rows += 1;
				vsGrid.RowData(intGridRow, strTestEmployeeID);
				// corey 4/21/2006
				// vsGrid.TextMatrix(intGridRow, 1) = aryDeductions(intCounter).DeductionID & " - " & aryDeductions(intCounter).DeductionDescription
				vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryDeductions[intCounter].DeductionCode) + " - " + aryDeductions[intCounter].DeductionDescription);
				vsGrid.TextMatrix(intGridRow, 2, Strings.Format(aryDeductions[intCounter].Amount, "0.00"));
				vsGrid.TextMatrix(intGridRow, 3, Strings.Format(aryDeductions[intCounter].AdjustmentAmount, "0.00"));
				vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtAdjustedEmployee.FedTaxGross, "0.00"));
				vsGrid.TextMatrix(intGridRow, 7, Strings.Format(udtAdjustedEmployee.StateTaxGross, "0.00"));
				vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtAdjustedEmployee.FICATaxGross, "0.00"));
				vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAdjustedEmployee.MedTaxGross, "0.00"));
				vsGrid.TextMatrix(intGridRow, 8, Strings.Format(udtAnnualizedEmployee.AnnualizedFedTax, "0.00"));
				vsGrid.TextMatrix(intGridRow, 9, Strings.Format(udtAnnualizedEmployee.AnnualizedStateTax, "0.00"));
				vsGrid.RowOutlineLevel(intGridRow, 2);
				vsGrid.IsSubtotal(intGridRow, false);
			}
			// intCounter
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Gross after Adj Deductions");
			vsGrid.TextMatrix(intGridRow, 2, "Total Gross   ");
			vsGrid.TextMatrix(intGridRow, 3, "Fed Tax Gross     ");
			vsGrid.TextMatrix(intGridRow, 6, "State Tax Gross   ");
			vsGrid.TextMatrix(intGridRow, 4, "FICA Tax Gross    ");
			vsGrid.TextMatrix(intGridRow, 5, "Med Tax Gross     ");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 2, Strings.Format(udtEmployee.TotalGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtAdjustedEmployee.FedTaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAdjustedEmployee.StateTaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtAdjustedEmployee.FICATaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtAdjustedEmployee.MedTaxGross, "0.00"));
			vsGrid.RowOutlineLevel(intGridRow, 2);
			vsGrid.IsSubtotal(intGridRow, false);
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Adj Employer Match #");
			vsGrid.TextMatrix(intGridRow, 2, "Amount");
			vsGrid.TextMatrix(intGridRow, 3, "Fed");
			vsGrid.TextMatrix(intGridRow, 6, "State");
			vsGrid.TextMatrix(intGridRow, 4, "FICA");
			vsGrid.TextMatrix(intGridRow, 5, "Med");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			for (intCounter = 0; intCounter <= (Information.UBound(aryEmployeeMatches, 1) - 1); intCounter++)
			{
				if (aryEmployeeMatches[intCounter].AmountType == "%D")
				{
					for (counter = 0; counter <= (Information.UBound(aryDeductions, 1) - 1); counter++)
					{
						if (aryDeductions[counter].DeductionCode == aryEmployeeMatches[intCounter].DeductionNumber)
						{
							dblAdjustmentTotals += aryDeductions[counter].Amount;
							dblDeductionTotals += aryDeductions[counter].Amount;
						}
					}
					// now need to make adjustments
					dblDifference = aryEmployeeMatches[intCounter].Amount;
					aryEmployeeMatches[intCounter].Amount = (dblDeductionTotals * (aryEmployeeMatches[intCounter].OriginalAmount / 100));
					dblDifference -= aryEmployeeMatches[intCounter].Amount;
					if (!aryEmployeeMatches[intCounter].FedTaxExempt)
						udtAdjustedEmployee.FedTaxGross -= dblDifference;
					if (!aryEmployeeMatches[intCounter].StateTaxExempt)
						udtAdjustedEmployee.StateTaxGross -= dblDifference;
					if (!aryEmployeeMatches[intCounter].FICATaxExempt)
						udtAdjustedEmployee.FICATaxGross -= dblDifference;
					if (!aryEmployeeMatches[intCounter].MedTaxExempt)
						udtAdjustedEmployee.MedTaxGross -= dblDifference;
					// Display Adjusted amounts to the user screen
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryEmployeeMatches[intCounter].DeductionNumber));
					vsGrid.TextMatrix(intGridRow, 2, FCConvert.ToString(aryEmployeeMatches[intCounter].Amount));
					vsGrid.TextMatrix(intGridRow, 3, FCConvert.ToString(udtAdjustedEmployee.FedTaxGross));
					vsGrid.TextMatrix(intGridRow, 6, FCConvert.ToString(udtAdjustedEmployee.StateTaxGross));
					vsGrid.TextMatrix(intGridRow, 4, FCConvert.ToString(udtAdjustedEmployee.FICATaxGross));
					vsGrid.TextMatrix(intGridRow, 5, FCConvert.ToString(udtAdjustedEmployee.MedTaxGross));
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
				}
			}
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Gross after Adj Match");
			vsGrid.TextMatrix(intGridRow, 2, "Total Gross   ");
			vsGrid.TextMatrix(intGridRow, 3, "Fed Tax Gross     ");
			vsGrid.TextMatrix(intGridRow, 6, "State Tax Gross   ");
			vsGrid.TextMatrix(intGridRow, 4, "FICA Tax Gross    ");
			vsGrid.TextMatrix(intGridRow, 5, "Med Tax Gross     ");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 2, Strings.Format(udtEmployee.TotalGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtAdjustedEmployee.FedTaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtAdjustedEmployee.StateTaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtAdjustedEmployee.FICATaxGross, "0.00"));
			vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtAdjustedEmployee.MedTaxGross, "0.00"));
			vsGrid.RowOutlineLevel(intGridRow, 2);
			vsGrid.IsSubtotal(intGridRow, false);
			return FixNetFromDeductions;
		}

        public void CalculateFEDTaxes(ref clsDRWrapper rsTax)
        {
            int intMarriedStatus = 0;
            int fedStatusCode = 0;
            // Dim dblStandardFedDeduction As Double
            int intDependents = 0;
            double dblFedTax;
            double dblAdditionalTax = 0;
            string gstrFromTable = "";
            //clsDRWrapper rsTax = new clsDRWrapper();
            clsDRWrapper rsOVTax = new clsDRWrapper();
            string strFedAddType = "";
            double dblFedAddAmt = 0;

            double dblFedW4OtherIncome = 0;
            double dblFedW4OtherDeductions = 0;
            int intOtherDependents = 0;
            double dblTaxCredits = 0;
            DateTime dtW4Date = DateTime.MinValue;
            bool emplHas2020W4 = false;
            bool w4MultipleJobsChecked = false;

            if (udtEmployee.FedTaxGross < 0)
            {
                dblFedTaxTotal = 0;
                udtEmployee.FedTaxGross = 0;
                return;
            }

            if (!rsTax.EndOfFile())
            {
                
                intMarriedStatus = FCConvert.ToInt32(Math.Round(
                    Conversion.Val(fecherFoundation.Strings.Trim(rsTax.Get_Fields("FedFilingStatusID") + " "))));
                FederalPayStatusCodes.TryGetValue(intMarriedStatus, out fedStatusCode);
                if (rsTax.Get_Fields_DateTime("W4Date") != DateTime.FromOADate(0) &&
                    rsTax.Get_Fields_DateTime("W4Date") != DateTime.MinValue)
                {
                    dtW4Date = rsTax.Get_Fields_DateTime("W4Date");
                }

                emplHas2020W4 = false;
                w4MultipleJobsChecked = false;
                if (dtW4Date.IsLaterThan(new DateTime(2019, 12, 31)))
                {
                    emplHas2020W4 = true;
                    w4MultipleJobsChecked = rsTax.Get_Fields_Boolean("W4MultipleJobs");
                }

                // THIS PERSON HAS THE MASTER SCREEN SET TO NO TAXES
                if (intMarriedStatus == 0)
                {
                    dblFedTaxTotal = 0;
                    udtEmployee.FedTaxGross = 0;
                    return;
                }

                dblFedAddAmt = Conversion.Val(rsTax.Get_Fields_Double("AddFed"));
                strFedAddType =
                    fecherFoundation.Strings.Trim(FCConvert.ToString(rsTax.Get_Fields_String("FedDollarPercent")));

                dblFedW4OtherIncome = rsTax.Get_Fields_Double("FederalOtherIncome");
                dblFedW4OtherDeductions = rsTax.Get_Fields_Double("AddFederalDeduction");

                rsOVTax.OpenRecordset(
                    "select * from withholdingoverrides where employeenumber = '" + strTestEmployeeID + "'",
                    "twpy0000.vb1");
                if (!rsOVTax.EndOfFile())
                {
                    dblFedAddAmt = Conversion.Val(rsOVTax.Get_Fields("Addfed"));
                    strFedAddType =
                        fecherFoundation.Strings.Trim(FCConvert.ToString(rsOVTax.Get_Fields("feddollarpercent")));
                }

                // get the number of dependents
                intDependents =
                    FCConvert.ToInt32(
                        Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsTax.Get_Fields("FedStatus") + " "))));
                intOtherDependents = rsTax.Get_Fields_Int32("FederalOtherDependents");

                if (fecherFoundation.Strings.UCase(strFedAddType) == "DOLLAR")
                {
                    dblAdditionalTax = dblFedAddAmt;
                }
                else if (fecherFoundation.Strings.UCase(strFedAddType) == "PERCENT")
                {
                    dblAdditionalTax = udtEmployee.FedTaxGross * (dblFedAddAmt / 100);
                    // ****************************************************************************************
                }
                else if (fecherFoundation.Strings.UCase(strFedAddType) == "GROSS PERCENT")
                {
                    dblAdditionalTax = udtEmployee.FedTaxGross * (dblFedAddAmt / 100);
                }
                else if (fecherFoundation.Strings.UCase(strFedAddType) == "NET PERCENT")
                {
                    modGlobalVariables.Statics.gboolAdditionalFederalTaxNetPercent = true;
                    modGlobalVariables.Statics.gdblAdditionalFederalTaxNetPercent = (dblFedAddAmt / 100);
                    dblAdditionalTax = 0;
                }
                else if (intEmployeeCheckCounter > 1)
                {
                    dblAdditionalTax = 0;
                }

                // MATTHEW 6/3/04
                if (strMultiCheckCode != "P0")
                {
                    if (FCConvert.ToBoolean(rsTax.Get_Fields_Boolean("BoolFedFirstCheckOnly")))
                    {
                        dblAdditionalTax = 0;
                    }
                }
            }
            //SetupFederalTaxTableArray(fedStatusCode,w4MultipleJobsChecked);

            if (udtAnnualizedEmployee.AnnualizedFedTax < 0)
                udtAnnualizedEmployee.AnnualizedFedTax = 0;

            var statusService = new FederalPayStatusService();
            if (emplHas2020W4)
            {
                double dblStandardDeduction = 0;
                var deductionToUse = statusService.DeductionToUse(statusService.StatusFromCode(fedStatusCode), w4MultipleJobsChecked);
                switch (deductionToUse)
                {
                    case FederalDeductionType.Married:
                        dblStandardDeduction = dblFederalStdMarriedDeduction;
                        break;
                    case FederalDeductionType.Single:
                        dblStandardDeduction = dblFederalStdSingleDeduction;
                        break;
                    default:
                        dblStandardDeduction = 0;
                        break;
                }
                
                dblFedTax = udtAnnualizedEmployee.AnnualizedFedTax + dblFedW4OtherIncome -
                            (dblFedW4OtherDeductions + dblStandardDeduction);
            }
            else
            {
                dblFedTax = udtAnnualizedEmployee.AnnualizedFedTax - (dblStandardFedDeduction * intDependents);
            }


            if (dblFedTax < 0.01)
                dblFedTax = 0;
            var fedTaxable = dblFedTax.ToDecimal();

            var fedTax = taxTableService.GetTax(fedTaxable,
                new PayrollFilingInfo(
                    new PayrollFilingStatusInfo((int) statusService.StatusFromCode(fedStatusCode),
                        w4MultipleJobsChecked), new PayrollLocality("US", "", "")));
			dblFedTax = (fedTax.ToDouble() / intFactor) * intTaxTotalMulti;
			if (emplHas2020W4)
            {
                dblTaxCredits =
                        ((intDependents * dblFederalQualDepCreditAmount) +
                         (intOtherDependents * dblFederalOtherDepCreditAmount)) / intFactor;
                dblTaxCredits = dblTaxCredits.RoundToMoney();
                if (dblFedTax >= dblTaxCredits)
                {
                    dblFedTax = dblFedTax - dblTaxCredits;
                }
                else
                {
                    dblFedTax = 0;
                }
            }

		dblFedTaxTotal = (dblFedTax + (dblAdditionalTax * intTaxTotalMulti)).RoundToMoney();
			if (dblFedTaxTotal < 0.01)
				dblFedTaxTotal = 0;
            udtEmployee.TotalNet = (udtEmployee.TotalNet - dblFedTaxTotal).RoundToMoney();
			if (udtEmployee.TotalNet < 0)
			{
				dblFedTaxTotal = (dblFedTaxTotal + (udtEmployee.TotalNet));
				udtEmployee.TotalNet = 0;
			}
		}

		public void CalculateSTATETaxes(ref clsDRWrapper rsTax,Dictionary<string,decimal> stateSetup)
		{
			int intMarriedStatus = 0;
            int intDependents = 0;
			double dblStateTax = 0;
			double dblAdditionalTax = 0;
            clsDRWrapper rsOVTax = new clsDRWrapper();
			string strSTAddType = "";
			double dblSTAdd = 0;
			if (udtEmployee.StateTaxGross < 0)
			{
				dblStateTaxTotal = 0;
				udtEmployee.StateTaxGross = 0;
				return;
			}

			if (!rsTax.EndOfFile())
			{
                intMarriedStatus = rsTax.Get_Fields_Int32("StateFilingStatusID");
				// THIS PERSON HAS THE MASTER SCREEN SET TO NO TAXES
				if (intMarriedStatus == 0)
				{
					dblStateTaxTotal = 0;
					udtEmployee.StateTaxGross = 0;
					return;
				}
				strSTAddType = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTax.Get_Fields("statedollarpercent")));
				dblSTAdd = Conversion.Val(rsTax.Get_Fields("addstate"));
				rsOVTax.OpenRecordset("select * from withholdingoverrides where employeenumber = '" + strTestEmployeeID + "'", "twpy0000.vb1");
				if (!rsOVTax.EndOfFile())
				{
					strSTAddType = fecherFoundation.Strings.Trim(FCConvert.ToString(rsOVTax.Get_Fields("statedollarpercent")));
					dblSTAdd = Conversion.Val(rsOVTax.Get_Fields("addstate"));
				}
                intDependents = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsTax.Get_Fields("StateStatus") + " "))));
				if (fecherFoundation.Strings.UCase(strSTAddType) == "DOLLAR")
				{
					dblAdditionalTax = Conversion.Val(dblSTAdd);
                }
				else if (fecherFoundation.Strings.UCase(strSTAddType) == "PERCENT")
				{
					dblAdditionalTax = udtEmployee.StateTaxGross * (dblSTAdd / 100);
                }
				else if (fecherFoundation.Strings.UCase(strSTAddType) == "GROSS PERCENT")
				{
					dblAdditionalTax = udtEmployee.StateTaxGross * (dblSTAdd / 100);
				}
				else if (fecherFoundation.Strings.UCase(strSTAddType) == "NET PERCENT")
				{
					// WILL BE DONE AFTER WE CALCULATE NET
					modGlobalVariables.Statics.gboolAdditionalStateTaxNetPercent = true;
					modGlobalVariables.Statics.gdblAdditionalStateTaxNetPercent = (dblSTAdd / 100);
					dblAdditionalTax = 0;
				}
				else if (intEmployeeCheckCounter > 1)
				{
					dblAdditionalTax = 0;
				}
				// MATTHEW 6/3/04
				if (strMultiCheckCode != "P0")
				{
					if (FCConvert.ToBoolean(rsTax.Get_Fields_Boolean("BoolStateFirstCheckOnly")))
					{
						dblAdditionalTax = 0;
					}
				}
			}
            if (udtAnnualizedEmployee.AnnualizedStateTax < 0)
				udtAnnualizedEmployee.AnnualizedStateTax = 0;
            if (stateSetup.TryGetValue("WithholdingAllowance", out var standardStateDeduction))
            {
                dblStateTax = udtAnnualizedEmployee.AnnualizedStateTax - (standardStateDeduction * intDependents).ToDouble();
			}
			
			if (dblStateTax < 0.01)
				dblStateTax = 0;
			bool boolIsSingle;
			boolIsSingle = IsSingle(intMarriedStatus);
			// 01/03/2017 tropy-724
			dblStateTax -= GetStateStandardDeduction(udtAnnualizedEmployee.AnnualizedStateTax.ToDecimal(), boolIsSingle,stateSetup).ToDouble();
			if (dblStateTax < 0)
				dblStateTax = 0;
			//GetTax(ref dblStateTax);
            var stateTaxable = dblStateTax.ToDecimal();
            var stateStatusCode = GetStateStatusCodeFromID(intMarriedStatus);
            var stateTax = taxTableService.GetTax(stateTaxable,
                new PayrollFilingInfo(
                    new PayrollFilingStatusInfo(stateStatusCode,
                        false), new PayrollLocality("US", "ME", "")));
			stateTax =  (stateTax / intFactor) * intTaxTotalMulti;

            dblStateTaxTotal = (stateTax.ToDouble() + (dblAdditionalTax * intTaxTotalMulti)).RoundToMoney();
			if (dblStateTaxTotal < 0)
				dblStateTaxTotal = 0;

            udtEmployee.TotalNet = (udtEmployee.TotalNet - dblStateTaxTotal).RoundToMoney();
			if (udtEmployee.TotalNet < 0)
			{
                dblStateTaxTotal = (dblStateTaxTotal + (udtEmployee.TotalNet));
				udtEmployee.TotalNet = 0;
			}
		}

		private decimal GetStateStandardDeduction(decimal annualizedWage, bool boolSingle,Dictionary<string,decimal> stateSetup)
        {
            decimal wageLimit = 0;
            decimal cutoff = 0;
            decimal fullDeduction = 0;
            if (boolSingle)
			{
                if (!stateSetup.TryGetValue("SingleFullDeductionWageLimit", out wageLimit))
                {
                    return 0;
                }
                if (!stateSetup.TryGetValue("SingleDeductionWageCutoff", out  cutoff))
                {
                    return 0;
                }
                if (!stateSetup.TryGetValue("SingleDeduction", out  fullDeduction))
                {
                    return 0;
                }
            }
			else
			{
                if (!stateSetup.TryGetValue("MarriedFullDeductionWageLimit", out wageLimit))
                {
                    return 0;
                }
                if (!stateSetup.TryGetValue("MarriedDeductionWageCutoff", out cutoff))
                {
                    return 0;
                }
                if (!stateSetup.TryGetValue("MarriedDeduction", out fullDeduction))
                {
                    return 0;
                }
			}


            if (annualizedWage < wageLimit)
            {
                return fullDeduction;
            }
            if (annualizedWage >= wageLimit && annualizedWage <= cutoff)
            {
                return ProrateDeduction(wageLimit, cutoff,
                    annualizedWage, fullDeduction);
            }
			return 0;
		}


        private decimal ProrateDeduction(decimal limit, decimal cutoff,decimal wages,decimal fullDeduction)
        {
            var range = cutoff - limit;
            if (range > 0)
            {
                var ratio = ((wages - cutoff) / range).RoundPlaces(8) * -1;
                if (ratio > 0)
                {
                    return (ratio * fullDeduction).RoundToMoney();
                }
            }

            return fullDeduction;
        }

		public void CalculateFICATaxes()
		{
			double dblMaxWage;
			double dblPercentWage;
            double dblFicaTax;
            clsDRWrapper rsTax = new clsDRWrapper();
			double dblCalendarFICAWage = 0;
            double dblEmployerFicaTax;
			double dblEmployerRate;
			if (udtEmployee.FICATaxGross < 0)
			{
				dblFICATaxTotal = 0;
				dblEmployerFicaTaxTotal = 0;
				dblEmployerFicaTax = 0;
				udtEmployee.FICATaxGross = 0;
				return;
			}
			if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterFicaExempt")))
			{
				dblFICATaxTotal = 0;
				dblEmployerFicaTaxTotal = 0;
				dblEmployerFicaTax = 0;
				udtEmployee.FICATaxGross = 0;
				return;
			}

			dblMaxWage = udtTaxSetupStuff.MaxSSN;
			dblPercentWage = udtTaxSetupStuff.PercentSSN;
			dblEmployerRate = udtTaxSetupStuff.EmployerFicaRate;
			// Determining if any of wage is over max
			rsTax.OpenRecordset("select sum(ficataxgross) as CalendarYTDTotal from tblcheckdetail where checkvoid = 0 and totalrecord = 1 and employeenumber = '" + strTestEmployeeID + "' and paydate between '" + "1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", "twpy0000.vb1");
			if (!rsTax.EndOfFile())
			{
				dblCalendarFICAWage = Conversion.Val(rsTax.Get_Fields("CalendarYTDTotal"));
			}
			double dblTemp;
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate);
			dblCalendarFICAWage += dblTemp;
			if (udtEmployee.FICATaxGross < 0)
				udtEmployee.FICATaxGross = 0;
			if ((dblCalendarFICAWage + udtEmployee.FICATaxGross) > dblMaxWage)
			{
				// udtEmployee.FICATaxGross = (dblCalendarFICAWage + udtEmployee.FICATaxGross) - dblMaxWage
				// made this change because when we hit the standard limit things were not
				// showing up correctly. Matthew 06/11/2003
				udtEmployee.FICATaxGross = dblMaxWage - dblCalendarFICAWage;
			}
			// MADE THIS CHANGE FOR JAY BECAUSE EXEMPT PAY MINUS EXEMPT DEDUCTION
			// IS LESS THAN ZERO.
			if (udtEmployee.FICATaxGross < 0)
				udtEmployee.FICATaxGross = 0;
			// Get the fica tax
            dblFicaTax = (udtEmployee.FICATaxGross * dblPercentWage).RoundToMoney();
			if (dblFicaTax < 0.01)
				dblFicaTax = 0;
            dblEmployerFicaTax = (udtEmployee.FICATaxGross * dblEmployerRate).RoundToMoney();
			if (dblEmployerFicaTax < 0.01)
				dblEmployerFicaTax = 0;
			dblFICATaxTotal = dblFicaTax;
			dblEmployerFicaTaxTotal = dblEmployerFicaTax;

            udtEmployee.TotalNet = (udtEmployee.TotalNet - dblFicaTax).RoundToMoney();
			if (udtEmployee.TotalNet < 0)
			{
                dblFicaTax = dblFicaTax + udtEmployee.TotalNet;
				udtEmployee.TotalNet = 0;
			}
		}

		public void CalculateMEDTaxes()
		{
			double dblMaxWage;
			double dblPercentWage;
			double dblMedTax;
			string gstrFromTable = "";
			clsDRWrapper rsTax = new clsDRWrapper();
			double dblCalendarMEDWage = 0;
            double dblEmployerMedicareTax;
			double dblEmployerRate;
            double dblBelowThresholdWage = 0;
            double dblAboveThresholdWage = 0;
			double dblAboveThresholdRate;
			if (udtEmployee.MedTaxGross < 0)
			{
				dblMedTaxTotal = 0;
				udtEmployee.MedTaxGross = 0;
				dblEmployerMedicareTax = 0;
				dblEmployerMedicareTaxTotal = 0;
				return;
			}
			if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterMEDExempt")))
			{
				dblMedTaxTotal = 0;
				udtEmployee.MedTaxGross = 0;
				dblEmployerMedicareTax = 0;
				dblEmployerMedicareTaxTotal = 0;
				return;
			}

			dblMaxWage = udtTaxSetupStuff.MaxMed;
			dblPercentWage = udtTaxSetupStuff.PercentMed;
			dblEmployerRate = udtTaxSetupStuff.EmployerMedRate;
			dblAboveThresholdRate = udtTaxSetupStuff.ThresholdMedRate;
			rsTax.OpenRecordset("select sum(medicaretaxgross) as CalendarYTDTotal from tblcheckdetail where not checkvoid = 1 and employeenumber = '" + strTestEmployeeID + "' and totalrecord = 1 and paydate between '" + "1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", "twpy0000.vb1");
			if (!rsTax.EndOfFile())
			{
				dblCalendarMEDWage = Conversion.Val(rsTax.Get_Fields("CalendarYTDTotal"));
			}
			double dblTemp;
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate);
			dblCalendarMEDWage += dblTemp;
			if (udtEmployee.MedTaxGross < 0)
				udtEmployee.MedTaxGross = 0;
			if ((dblCalendarMEDWage + udtEmployee.MedTaxGross) > dblMaxWage)
			{
                udtEmployee.MedTaxGross = dblMaxWage - dblCalendarMEDWage;
				if (udtEmployee.MedTaxGross < 0)
					udtEmployee.MedTaxGross = 0;
			}
			if (dblCalendarMEDWage >= udtTaxSetupStuff.MedThreshold)
			{
				dblBelowThresholdWage = 0;
				dblAboveThresholdWage = udtEmployee.MedTaxGross;
			}
			else if (dblCalendarMEDWage + udtEmployee.MedTaxGross < udtTaxSetupStuff.MedThreshold)
			{
				dblBelowThresholdWage = udtEmployee.MedTaxGross;
				dblAboveThresholdWage = 0;
			}
			else
			{
				// little of both
                dblBelowThresholdWage = (udtTaxSetupStuff.MedThreshold - (dblCalendarMEDWage + udtEmployee.MedTaxGross))
                    .RoundToMoney();
                dblAboveThresholdWage =
                    (dblCalendarMEDWage + udtEmployee.MedTaxGross - dblBelowThresholdWage).RoundToMoney();
            }
            dblMedTax = (dblBelowThresholdWage * dblPercentWage).RoundToMoney() +
                        (dblAboveThresholdWage * dblAboveThresholdRate).RoundToMoney();
			if (dblMedTax < 0.01)
				dblMedTax = 0;
			dblMedTaxTotal = dblMedTax;
            dblEmployerMedicareTax = (udtEmployee.MedTaxGross * dblEmployerRate).RoundToMoney();
			if (dblEmployerMedicareTax < 0.01)
				dblEmployerMedicareTax = 0;
			dblEmployerMedicareTaxTotal = dblEmployerMedicareTax;
            udtEmployee.TotalNet = (udtEmployee.TotalNet - dblMedTax).RoundToMoney();
			if (udtEmployee.TotalNet < 0)
			{
                dblMedTax = (dblMedTax + (udtEmployee.TotalNet));
				udtEmployee.TotalNet = 0;
			}
		}


        public void SetupVacationSickCodesArray()
		{
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			clsDRWrapper rsCodes = new clsDRWrapper();
			modGlobalVariables.Statics.VacationSickCodeInfo = new modGlobalVariables.VacationSickCodes[0 + 1];
			rsCodes.OpenRecordset("Select Max(ID) as MaxID from tblCodes", "TWPY0000.vb1");
			modGlobalVariables.Statics.VacationSickCodeInfo = new modGlobalVariables.VacationSickCodes[FCConvert.ToInt32(Conversion.Val(rsCodes.Get_Fields("MaxID"))) + 1];
			rsCodes.OpenRecordset("Select * from tblCodes Order by CodeType,Code", "TWPY0000.vb1");
			for (intCount = 0; intCount <= (rsCodes.RecordCount() - 1); intCount++)
			{
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].ID = rsCodes.Get_Fields("ID");
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Code = rsCodes.Get_Fields_String("Code").Trim();
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].CodeType = rsCodes.Get_Fields("CodeType");
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Description = rsCodes.Get_Fields_String("Description").Trim();
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].DHW = rsCodes.Get_Fields_String("DHW").Trim();
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Max = rsCodes.Get_Fields("Max");
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].MonthDay = rsCodes.Get_Fields_String("MonthDay");
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Rate = FCConvert.ToDecimal(rsCodes.Get_Fields("Rate"));
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Type = rsCodes.Get_Fields_String("Type").Trim();
				modGlobalVariables.Statics.VacationSickCodeInfo[rsCodes.Get_Fields("ID")].Cleared = rsCodes.Get_Fields_String("Cleared");
				rsCodes.MoveNext();
			}
		}

	
		public void LoadDeductionArray()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsEmployeeDeductions = new clsDRWrapper();
			clsDRWrapper rsEtblDeductionSetup = new clsDRWrapper();
			rsEmployeeDeductions.OpenRecordset("Select * from tblEmployeeDeductions where EmployeeNumber = '" + strTestEmployeeID + "' Order by RecordNumber", "Twpy0000.vb1");
			if (!rsEmployeeDeductions.EndOfFile())
			{
				for (intCounter = 0; intCounter <= (rsEmployeeDeductions.RecordCount() - 1); intCounter++)
				{
					rsEtblDeductionSetup.OpenRecordset("Select * from tblDeductionSetup where ID = " + rsEmployeeDeductions.Get_Fields("DeductionCode"), "Twpy0000.vb1");
					aryDeductions[intCounter].DeductionNumber = FCConvert.ToInt32(rsEmployeeDeductions.Get_Fields("RecordNumber"));
					aryDeductions[intCounter].DeductionCode = FCConvert.ToInt32(rsEtblDeductionSetup.Get_Fields("DeductionNumber"));
					aryDeductions[intCounter].DeductionDescription = FCConvert.ToString(rsEtblDeductionSetup.Get_Fields("Description"));
					aryDeductions[intCounter].DeductionID = FCConvert.ToInt32(rsEtblDeductionSetup.Get_Fields("ID"));
					aryDeductions[intCounter].AmountType = FCConvert.ToString(rsEtblDeductionSetup.Get_Fields("amounttype"));
					if (FCConvert.ToString(rsEmployeeDeductions.Get_Fields("amounttype")) != "")
					{
						aryDeductions[intCounter].AmountType = FCConvert.ToString(rsEmployeeDeductions.Get_Fields("amounttype"));
						if (FCConvert.ToString(rsEmployeeDeductions.Get_Fields("percenttype")) != "")
						{
							aryDeductions[intCounter].PercentType = FCConvert.ToString(rsEmployeeDeductions.Get_Fields("percenttype"));
						}
					}
					aryDeductions[intCounter].EmpDeductionID = FCConvert.ToInt32(rsEmployeeDeductions.Get_Fields("ID"));
					rsEmployeeDeductions.MoveNext();
				}
			}
		}

		public void LoadMatchArray()
		{
			int intCounter;
			clsDRWrapper rsMatchArray = new clsDRWrapper();
			int x;
			string strAccounts = "";
			string[] strAry = null;
			double dblMaxHours;
			double dblTemp;
			clsDRWrapper rsEmployeeMatches = new clsDRWrapper();
			clsDRWrapper rsEtblDeductionSetup = new clsDRWrapper();
			rsEmployeeMatches.OpenRecordset("Select * from tblEmployersMatch where EmployeeNumber = '" + strTestEmployeeID + "' ORDER BY recordnumber", "Twpy0000.vb1");
			intCounter = 0;
			while (!rsEmployeeMatches.EndOfFile())
			{
				rsEtblDeductionSetup.OpenRecordset("Select * from tblDeductionSetup where ID = " + rsEmployeeMatches.Get_Fields("DeductionCode"), "Twpy0000.vb1");
				aryEmployeeMatches[intCounter].MatchNumber = FCConvert.ToInt32(rsEmployeeMatches.Get_Fields("RecordNumber"));
				aryEmployeeMatches[intCounter].DeductionNumber = FCConvert.ToInt32(rsEtblDeductionSetup.Get_Fields("DeductionNumber"));
				aryEmployeeMatches[intCounter].DeductionDescription = FCConvert.ToString(rsEtblDeductionSetup.Get_Fields("Description"));
				aryEmployeeMatches[intCounter].DeductionGLAccount = FCConvert.ToString(rsEtblDeductionSetup.Get_Fields_String("AccountID"));
				aryEmployeeMatches[intCounter].EmpDeductionID = FCConvert.ToInt32(rsEmployeeMatches.Get_Fields("ID"));
				aryEmployeeMatches[intCounter].ObjectCode = "";
				aryEmployeeMatches[intCounter].Percentage = 100;
				aryEmployeeMatches[intCounter].AmountType = FCConvert.ToString(rsEmployeeMatches.Get_Fields("amounttype"));
				aryEmployeeMatches[intCounter].DollarsHours = FCConvert.ToInt32(Math.Round(Conversion.Val(rsEmployeeMatches.Get_Fields("dollarshours"))));
				aryEmployeeMatches[intCounter].MaxAmount = rsEmployeeMatches.Get_Fields_Double("MaxAmount");
				aryEmployeeMatches[intCounter].DedTaxCode = FCConvert.ToInt32(rsEtblDeductionSetup.Get_Fields("TaxStatusCode"));
				aryEmployeeMatches[intCounter].DeductionID = FCConvert.ToInt32(rsEmployeeMatches.Get_Fields("DeductionCode"));
				aryEmployeeMatches[intCounter].Account = FCConvert.ToString(rsEmployeeMatches.Get_Fields("Account"));
				intCounter += 1;
				rsEmployeeMatches.MoveNext();
			}
		}

		private void CalculateAnnualizedGross(int intCount)
		{
			// Calculate Annualized Grosses
			if (!FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FedExempt")))
			{
				udtAnnualizedEmployee.AnnualizedFedTax += GetAnnualizedTax(intCount);
			}
			if (!FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("StateExempt")))
			{
				udtAnnualizedEmployee.AnnualizedStateTax += GetAnnualizedTax(intCount);
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(double, int)
		private double GetAnnualizedTax(int intCount)
		{
			if (aryDistribution[intCount].NumberWeeks > 0)
			{
				return (FCConvert.ToInt32(Strings.Format(aryDistribution[intCount].Gross, "0.00")) / aryDistribution[intCount].NumberWeeks) * intFactor;
			}
			else
			{
				return 0;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWriteFCConvert.ToDouble(
		private double GetAnnualizedAmount(double dblAmount, int intNumWeeks)
		{
			if (intNumWeeks > 0)
			{
				return dblAmount / intNumWeeks * intFactor;
			}
			else
			{
				return dblAmount;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, int)
		private int GetFrequencyFactor()
		{
			int GetFrequencyFactor = 0;
			clsDRWrapper rsFrequency = new clsDRWrapper();
			// Get the 'FACTOR' from the frequency on the Employee Master Screen
			rsFrequency.OpenRecordset("SELECT tblFrequencyCodes.FrequencyCode, tblFrequencyCodes.PeriodsPerYear FROM tblEmployeeMaster INNER JOIN tblFrequencyCodes ON tblEmployeeMaster.FreqCodeId = tblFrequencyCodes.ID where tblEmployeeMaster.EmployeeNumber = '" + strTestEmployeeID + "'", "Twpy0000.vb1");
			if (!rsFrequency.EndOfFile())
			{
				if (FCConvert.ToString(rsFrequency.Get_Fields("FrequencyCode")) == "D")
				{
					rsFrequency.OpenRecordset("SELECT PayFreqWeekly, PayFreqBiWeekly from tblFieldLengths", "Twpy0000.vb1");
					if (FCConvert.ToBoolean(rsFrequency.Get_Fields_Boolean("PayFreqWeekly")))
					{
						rsFrequency.OpenRecordset("SELECT tblFrequencyCodes.FrequencyCode, tblFrequencyCodes.PeriodsPerYear FROM tblFrequencyCodes where FrequencyCode = 'W'", "Twpy0000.vb1");
						if (!rsFrequency.EndOfFile())
						{
							GetFrequencyFactor = rsFrequency.Get_Fields("PeriodsPerYear");
						}
						else
						{
							GetFrequencyFactor = 0;
						}
					}
					else
					{
						rsFrequency.OpenRecordset("SELECT tblFrequencyCodes.FrequencyCode, tblFrequencyCodes.PeriodsPerYear FROM tblFrequencyCodes where FrequencyCode = 'B'", "Twpy0000.vb1");
						if (!rsFrequency.EndOfFile())
						{
							GetFrequencyFactor = rsFrequency.Get_Fields("PeriodsPerYear");
						}
						else
						{
							GetFrequencyFactor = 0;
						}
					}
				}
				else
				{
					GetFrequencyFactor = rsFrequency.Get_Fields("PeriodsPerYear");
				}
			}
			return GetFrequencyFactor;
		}

		public void AdjustEmployeeMatchArray(int lngExemptCode, double intAmount)
		{
			// vbPorter upgrade warning: intArray As int	OnWriteFCConvert.ToInt32(
			int intArray;
			// Need to cycle through the array to find a deduction # Exempt Code
			for (intArray = 0; intArray <= (Information.UBound(aryEmployeeMatches, 1) - 1); intArray++)
			{
				if (aryEmployeeMatches[intArray].DeductionID == lngExemptCode || lngExemptCode == 9999)
				{
					aryEmployeeMatches[intArray].MatchGross -= intAmount;
				}
			}
		}

		public void AdjustDeductionArray(int lngExemptCode, double intAmount)
		{
			// vbPorter upgrade warning: intArray As int	OnWriteFCConvert.ToInt32(
			int intArray;
			// Need to cycle through the array to find a deduction # Exempt Code
			for (intArray = 0; intArray <= Information.UBound(aryDeductions, 1) - 1; intArray++)
			{
				if (aryDeductions[intArray].DeductionID == lngExemptCode || lngExemptCode == 9999)
				{
					if (aryDeductions[intArray].AmountType != "Dollars")
					{
						aryDeductions[intArray].DeductionGross -= intAmount;
					}
				}
			}
		}

		public void AdjustEmployeeMatchArrayExemptBut(int lngExemptCode, double intAmount)
		{
			// vbPorter upgrade warning: intArray As int	OnWriteFCConvert.ToInt32(
			int intArray;
			// Need to cycle through the array to find a deduction # Exempt Code
			for (intArray = 0; intArray <= Information.UBound(aryEmployeeMatches, 1) - 1; intArray++)
			{
				if (aryEmployeeMatches[intArray].DeductionID != lngExemptCode)
				{
					aryEmployeeMatches[intArray].MatchGross -= intAmount;
				}
			}
		}

		public void AdjustDeductionArrayExemptBut(int lngExemptCode, double intAmount)
		{
			// vbPorter upgrade warning: intArray As int	OnWriteFCConvert.ToInt32(
			int intArray;
			// Need to cycle through the array to find a deduction # Exempt Code
			for (intArray = 0; intArray <= Information.UBound(aryDeductions, 1) - 1; intArray++)
			{
				if (aryDeductions[intArray].DeductionID != lngExemptCode)
				{
					aryDeductions[intArray].DeductionGross -= intAmount;
				}
			}
		}

		private bool GetAllDistributions()
		{
			bool GetAllDistributions = false;
			int intCount;
			// vbPorter upgrade warning: intCount2 As int	OnWriteFCConvert.ToInt32(
			int intCount2;
			clsDRWrapper rsPayCat = new clsDRWrapper();
			clsDRWrapper rsVacSick = new clsDRWrapper();
			string strSepCheckCode = "";
			GetAllDistributions = false;
			intCount = 0;
			intTaxTotalMulti = 0;
			dblBasePayRate = 0;
			pboolNonPaid = false;
			clearUDTEmployee();
			intGridRow += 1;
			vsGrid.Rows += 1;
			vsGrid.RowData(intGridRow, strTestEmployeeID);
			vsGrid.TextMatrix(intGridRow, 1, "Distribution #");
			vsGrid.TextMatrix(intGridRow, 7, "Dist Hours   ");
			vsGrid.TextMatrix(intGridRow, 3, "Fed Gross     ");
			vsGrid.TextMatrix(intGridRow, 6, "State Gross   ");
			vsGrid.TextMatrix(intGridRow, 4, "FICA Gross    ");
			vsGrid.TextMatrix(intGridRow, 5, "Med Gross     ");
			vsGrid.TextMatrix(intGridRow, 2, "Dist Gross");
			vsGrid.TextMatrix(intGridRow, 8, "Tax # Per");
			vsGrid.RowOutlineLevel(intGridRow, 1);
			vsGrid.IsSubtotal(intGridRow, true);
			boolSepCheck = false;
			NextCheck:
			;
			// If strTestEmployeeID = 301 Then MsgBox "A"
			while (!rsDistribution.EndOfFile() && !modGlobalVariables.Statics.pboolUnloadProcessGrid)
			{
				//App.DoEvents();
				strCurrentCheckCode = FCConvert.ToString(GetCheckCode(FCConvert.ToInt32(Conversion.Val(rsDistribution.Get_Fields_String("AccountCode")))));
				if (intCount == 0)
				{
					// THIS IS THE FIRST RECORD FOR THE DISTRIBUTIONS SO
					// THIS WILL BE OUR BASE PAY RATE.
					dblBasePayRate = Conversion.Val(rsDistribution.Get_Fields_Decimal("BaseRate"));
				}
				// ******************************************************************************************
				// this would be if we were to have a new distribution code type that was just weeks 1-4 and
				// not 5. matthew 10/26/2004
				if (Conversion.Val(rsDistribution.Get_Fields_String("AccountCode")) == 13 && ((CheckProcessSetupInfo.WeekNumber == 5 && !boolBiWeeklyTown) || (CheckProcessSetupInfo.WeekNumber == 3 && boolBiWeeklyTown)))
				{
					// do not pay
				}
				else if (Conversion.Val(rsDistribution.Get_Fields("accountcode")) >= 14 && Conversion.Val(rsDistribution.Get_Fields("accountcode")) <= 17 && (CheckProcessSetupInfo.WeekNumber != Conversion.Val(rsDistribution.Get_Fields("accountcode")) - 13))
				{
				}
				else
				{
					if (Strings.Right(strMultiCheckCode, 1) == strCurrentCheckCode)
					{
						lngGross = CalculateGross();
						lngGross = FCConvert.ToDouble(Strings.Format(lngGross, "0.00"));
						dblGrossTotal += lngGross;
						if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) != "Non-Pay (Hours)" && Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + "")) < 0)
						{
							// then there is a type that is NOT non pay and there is a negative hours so stop this whole process.
							MessageBox.Show("Employee " + strTestEmployeeID + " has a distribution with a negative hours value with a NON Non-Pay type selected.");
							boolErrorInDistributions = true;
							return GetAllDistributions;
						}

                        var hoursThisDistribution = rsDistribution.Get_Fields_Double("HoursWeek").ToDecimal();

						if (hoursThisDistribution > 0)
                        {
                            var tags = categoryTags.Where(t =>
                                t.PayCategory == rsDistribution.Get_Fields_Int32("CAT"));
                            if (!DistributionsByCheck.TryGetValue(strMultiCheckCode, out var distributionsByTag))
                            {
								distributionsByTag = new Dictionary<string, decimal>();
								DistributionsByCheck.Add(strMultiCheckCode,distributionsByTag);
                            }
                            foreach (var tag in tags)
                            {
								
                                if (!distributionsByTag.ContainsKey(tag.Tag))
                                {
									distributionsByTag.Add(tag.Tag,hoursThisDistribution);
                                }
                                else
                                {
                                    var previousHours = distributionsByTag[tag.Tag];
                                    distributionsByTag[tag.Tag] = previousHours + hoursThisDistribution;
                                }
                            }
                        }
						// CALL ID 73485 MATTHEW 12/09/2005
						if (!pboolNonPaid)
							pboolNonPaid = (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "Non-Pay (Hours)" && Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + "")) != 0);
						// MATTHEW 7/20/2004 'TOPSHAM' WE ONLY WANT TO IGNORE A ZERO IF THIS IS A DOLLARS OR HOURS DISTRIBUTION
						if (lngGross == 0 && (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) != "Non-Pay (Hours)" || Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + "")) == 0))
						{
							// If lngGross = 0 THEN  'MATTHEW 6/29/2004
							// THEN WE DON'T WANT TO ENTER A RECORD AT ALL
							aryCalcedDist[intCount].Gross = 0;
							aryCalcedDist[intCount].Hours = 0;
							aryCalcedDist[intCount].Rate = 0;
							aryCalcedDist[intCount].Factor = 0;
						}
						else if (lngGross < 0)
						{
							// If Trim(Trim(rsDistribution.Fields("Type"))) = "Non-Pay (Hours)" And Val(Trim(rsDistribution.Fields("HoursWeek") & "")) > 0 Then
							// WE ALLOW A NEGATIVE IF THIS IS OF A TYPE NON PAY
							if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "Non-Pay (Hours)")
							{
								CreateDistTempPayProcessRecord(intCount);
							}
							aryCalcedDist[intCount].Gross = 0;
							aryCalcedDist[intCount].Hours = 0;
							aryCalcedDist[intCount].Rate = 0;
							aryCalcedDist[intCount].Factor = 0;
						}
						else
						{
							// UPDATE THE TAX UDT FOR THIS DISTRIBUTION
							udtEmployee.TotalGross += FCConvert.ToDouble(Strings.Format(lngGross, "0.00"));
							udtEmployee.FedTaxGross += (FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FedExempt")) ? 0 : lngGross);
							udtEmployee.StateTaxGross += (FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("StateExempt")) ? 0 : lngGross);
							if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterFicaExempt")))
							{
							}
							else
							{
								udtEmployee.FICATaxGross += (FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FICAExempt")) ? 0 : lngGross);
							}
							if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterMEDExempt")))
							{
							}
							else
							{
								udtEmployee.MedTaxGross += (FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("MedicareExempt")) ? 0 : lngGross);
							}
							intGridRow += 1;
							vsGrid.Rows += 1;
							vsGrid.RowData(intGridRow, strTestEmployeeID);
							vsGrid.TextMatrix(intGridRow, 1, rsDistribution.Get_Fields("CategoryNumber") + Strings.StrDup(5 - FCConvert.ToString(rsDistribution.Get_Fields("CategoryNumber")).Length, " ") + " - " + rsDistribution.Get_Fields("Description"));
							vsGrid.TextMatrix(intGridRow, 3, Strings.Format((FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FedExempt")) ? 0 : lngGross), "0.00"));
							vsGrid.TextMatrix(intGridRow, 6, Strings.Format((FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("StateExempt")) ? 0 : lngGross), "0.00"));
							if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterFicaExempt")))
							{
								vsGrid.TextMatrix(intGridRow, 4, FCConvert.ToString(0));
							}
							else
							{
								vsGrid.TextMatrix(intGridRow, 4, Strings.Format((FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FICAExempt")) ? 0 : lngGross), "0.00"));
							}
							if (FCConvert.ToBoolean(rsMaster.Get_Fields("MasterMEDExempt")))
							{
								vsGrid.TextMatrix(intGridRow, 5, FCConvert.ToString(0));
							}
							else
							{
								vsGrid.TextMatrix(intGridRow, 5, Strings.Format((FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("MedicareExempt")) ? 0 : lngGross), "0.00"));
							}
							vsGrid.TextMatrix(intGridRow, 7, Strings.Format((FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("LocalExempt")) ? 0 : lngGross), "0.00"));
							vsGrid.RowOutlineLevel(intGridRow, 2);
							vsGrid.IsSubtotal(intGridRow, false);
							// Update distribution array
							aryDistribution[intCount].Gross = lngGross;
							aryDistribution[intCount].Type = FCConvert.ToString(rsDistribution.Get_Fields("Type"));
							aryDistribution[intCount].FedTaxExempt = FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FedExempt"));
							aryDistribution[intCount].StateTaxExempt = FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("StateExempt"));
							aryDistribution[intCount].FICATaxExempt = FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("FICAExempt"));
							aryDistribution[intCount].MedTaxExempt = FCConvert.ToBoolean(rsDistribution.Get_Fields_Boolean("MedicareExempt"));
							aryDistribution[intCount].Account = FCConvert.ToString(rsDistribution.Get_Fields("AccountNumber"));
							aryDistribution[intCount].ContractID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDistribution.Get_Fields("contractid"))));
							aryDistribution[intCount].Check = FCConvert.ToInt32(strCurrentCheckCode);
							if (aryDistribution[intCount].ContractID > 0)
							{
								aryDistribution[intCount].boolEncumbrance = boolEncumberContracts;
							}
							else
							{
								aryDistribution[intCount].boolEncumbrance = false;
							}
							aryCalcedDist[intCount].Gross = lngGross;
							aryCalcedDist[intCount].Hours = rsDistribution.Get_Fields("hoursweek");
							aryCalcedDist[intCount].Rate = Conversion.Val(rsDistribution.Get_Fields_Decimal("BaseRate"));
							aryCalcedDist[intCount].Factor = Conversion.Val(rsDistribution.Get_Fields("factor"));
							if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) != "HOURS")
							{
								aryCalcedDist[intCount].Hours = 0;
							}
                            aryDistribution[intCount].boolFromSchool = false;

							if (FCConvert.ToInt32(rsDistribution.Get_Fields("NumberWeeks")) == 0)
							{
								aryDistribution[intCount].NumberWeeks = intTaxUpTop;
							}
							else
							{
								aryDistribution[intCount].NumberWeeks = FCConvert.ToInt16(rsDistribution.Get_Fields("NumberWeeks"));
							}
							if (FCConvert.ToInt32(rsDistribution.Get_Fields("WeeksTaxWithheld")) == 0)
							{
								aryDistribution[intCount].WeeksWithheld = intTaxUpTop;
							}
							else
							{
								aryDistribution[intCount].WeeksWithheld = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDistribution.Get_Fields("weekstaxwithheld"))));
							}
							if (intCount > 0)
							{
								if (aryDistribution[intCount].NumberWeeks != intTWeeks && intTWeeks >= 0)
								{
									MessageBox.Show("Employee " + strTestEmployeeID + " has more than one Tax # Per set for this pay run" + "\r\n" + "Cannot continue", "Bad Tax # Per", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									boolErrorInDistributions = true;
									return GetAllDistributions;
								}
								if (aryDistribution[intCount].WeeksWithheld != intTWHWeeks && intTWHWeeks >= 0)
								{
									MessageBox.Show("Employee " + strTestEmployeeID + " has more than one Tax Period withheld set for this pay run" + "\r\n" + "Cannot continue", "Bad tax periods withheld", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									boolErrorInDistributions = true;
									return GetAllDistributions;
								}
							}
							if (aryDistribution[intCount].Gross != 0)
							{
								intTWeeks = aryDistribution[intCount].NumberWeeks;
								intTWHWeeks = aryDistribution[intCount].WeeksWithheld;
							}
							// Add up all Tax # periods to get a total
							// intTaxTotalMulti = intTaxUpTop
							intTaxTotalMulti = intTWHWeeks;
							// Calculate AnnualizedGross's
							CalculateAnnualizedGross(intCount);
							// Add this distribution to each item in the deduction array
							for (intCount2 = 0; intCount2 <= (Information.UBound(aryDeductions, 1) - 1); intCount2++)
							{
								aryDeductions[intCount2].DeductionGross += lngGross;
							}
							// Add this distribution to each item in the EmployeeMatch array
							for (intCount2 = 0; intCount2 <= (Information.UBound(aryEmployeeMatches, 1) - 1); intCount2++)
							{
								aryEmployeeMatches[intCount2].MatchGross += lngGross;
							}
							// This is the Category ID Number
							// Add this distribution to the total array for the Pay Totals Table
							PayCategoryItemTotal[rsDistribution.Get_Fields("CAT")] += lngGross;

							rsPayCat.OpenRecordset("Select * from tblPayCategories where ID = " + rsDistribution.Get_Fields("CAT"), "Twpy0000.vb1");
							for (intCount2 = 1; intCount2 <= 5; intCount2++)
							{
								if (FCConvert.ToInt32(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2))) != 0)
								{
									// If this pay category is exempt then remove this distribution from the appropriate array
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 1)) == "R")
									{
										AdjustEmployeeMatchArray(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 1)) == "E")
									{
										AdjustDeductionArray(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 1)) == "B")
									{
										AdjustDeductionArray(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
										AdjustEmployeeMatchArray(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 2)) == "XR")
									{
										AdjustEmployeeMatchArrayExemptBut(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 2)) == "XE")
									{
										AdjustDeductionArrayExemptBut(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
									if (fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(rsPayCat.Get_Fields("ExemptType" + FCConvert.ToString(intCount2))), 2)) == "XB")
									{
										AdjustDeductionArrayExemptBut(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
										AdjustEmployeeMatchArrayExemptBut(FCConvert.ToInt32(Conversion.Val(rsPayCat.Get_Fields_String("ExemptCode" + FCConvert.ToString(intCount2)))), lngGross);
									}
								}
							}
							// ****************************************************************************
							vsGrid.TextMatrix(intGridRow, 8, FCConvert.ToString(rsDistribution.Get_Fields_Double("HoursWeek")));
							vsGrid.TextMatrix(intGridRow, 2, Strings.Format(rsDistribution.Get_Fields_Decimal("Gross"), "0.00"));
							vsGrid.TextMatrix(intGridRow, 9, FCConvert.ToString(aryDistribution[intCount].NumberWeeks));
							CreateDistTempPayProcessRecord(intCount);
						}
					}
					else
					{
						// CHECK TO SEE IF THIS IS A SEPERATE CHECK OR IF THIS
						// IS PART OF THE PRIMARY.
						if (Conversion.Val(strCurrentCheckCode) > Conversion.Val(Strings.Right(strMultiCheckCode, 1)))
						{
							boolSepCheck = true;
						}
					}
				}
				intCount += 1;
				rsDistribution.MoveNext();
			}
			GetAllDistributions = true;
			return GetAllDistributions;
		}

		private void CreateDistTempPayProcessRecord(int intCount = 0)
		{
			string strSQL = "";
			double dblHours;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsMSRS = new clsDRWrapper();
			int intMultiTown = 0;
			string strAccountNumber;
			clsDRWrapper rsMulti = new clsDRWrapper();
			int intBreakdownCode = 0;
			// vbPorter upgrade warning: dblDistGrossPay As double	OnWrite(string, double, int)
			double dblDistGrossPay;
			double dblTotHours;
			double dblTotDGP;
			strAccountNumber = FCConvert.ToString(rsDistribution.Get_Fields("AccountNumber"));
			dblHours = rsDistribution.Get_Fields_Double("HoursWeek");
			dblDistGrossPay = FCConvert.ToDouble(Strings.Format(aryDistribution[intCount].Gross, "0.00"));
			if (modRegionalTown.IsRegionalTown())
			{
				if (fecherFoundation.Strings.Trim(strAccountNumber).Length >= 3 && Strings.Left(fecherFoundation.Strings.Trim(strAccountNumber), 1) == "E")
				{
					if (FCConvert.ToDouble(Strings.Mid(fecherFoundation.Strings.Trim(strAccountNumber), 3, 1)) == 0)
					{
						// this is a multitown number so check it three times
						intBreakdownCode = 0;
						rsMulti.OpenRecordset("Select * from DeptDivTitles where Department = '1" + Strings.Mid(strAccountNumber, 4, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) - 1)) + "'", "TWBD0000.vb1");
						if (!rsMulti.EndOfFile())
						{
							if (FCConvert.ToInt32(rsMulti.Get_Fields("BreakDownCode")) != 0)
							{
								intBreakdownCode = FCConvert.ToInt16(rsMulti.Get_Fields("BreakDownCode"));
							}
						}
						if (intBreakdownCode == 0)
						{
							intMultiTown = 1;
							strAccountNumber = "E 1" + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
						}
						else
						{
							rsMulti.OpenRecordset("Select * from RegionalTownBreakDown where Code = " + FCConvert.ToString(intBreakdownCode) + " Order by TownNumber", "CentralData");
							while (!rsMulti.EndOfFile())
							{
								intMultiTown += 1;
								rsMulti.MoveNext();
							}
							rsMulti.MoveFirst();
							strAccountNumber = "E " + rsMulti.Get_Fields("TownNumber") + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
							dblHours = rsDistribution.Get_Fields_Double("HoursWeek") * rsMulti.Get_Fields_Double("Percentage");
							dblDistGrossPay = FCConvert.ToDouble(Strings.Format(aryDistribution[intCount].Gross * rsMulti.Get_Fields_Double("Percentage"), "0.00"));
						}
					}
					else
					{
						intMultiTown = 1;
					}
				}
				else
				{
					intMultiTown = 1;
				}
			}
			else
			{
				intMultiTown = 1;
			}
			dblTotHours = rsDistribution.Get_Fields_Double("HoursWeek");
			dblTotDGP = aryDistribution[intCount].Gross;
			for (intCounter = 1; intCounter <= intMultiTown; intCounter++)
			{
				if (intCounter > 1)
				{
					rsMulti.MoveNext();
					strAccountNumber = "E " + rsMulti.Get_Fields("TownNumber") + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
					if (intCounter < intMultiTown)
					{
						dblHours = rsDistribution.Get_Fields_Double("HoursWeek") * rsMulti.Get_Fields_Double("Percentage");
						dblDistGrossPay = FCConvert.ToDouble(Strings.Format(aryDistribution[intCount].Gross * rsMulti.Get_Fields_Double("Percentage"), "0.00"));
					}
					else
					{
						if (dblTotHours >= 0)
						{
							dblHours = dblTotHours;
						}
						else
						{
							dblHours = 0;
						}
						if (dblTotDGP >= 0)
						{
							dblDistGrossPay = dblTotDGP;
						}
						else
						{
							dblDistGrossPay = 0;
						}
					}
					dblTotHours -= dblHours;
					dblTotDGP -= dblDistGrossPay;
				}
				else
				{
					dblTotHours -= dblHours;
					dblTotDGP -= dblDistGrossPay;
				}
				strSQL = "Insert INTO tblTempPayProcess (DistAutoID, [Status], EmployeeNumber, ";
				strSQL += "EmployeeName, CheckNumber, PayDate, NetPay, JournalNumber, ";
				strSQL += "WarrantNumber,AccountingPeriod,DistLineCode,DistAccountNumber, ";
				// strSQL = strSQL & "DistPayCategory, DistTaxCode, DistM, DistE, DistU, DistW, "
				strSQL += "DistPayCategory, DistTaxCode, DistM, StatusCode, GrantFunded, DistU, ";
				strSQL += "DistWCCode, DistPayCode, DistPayRate, DistMult, DistNumTaxPeriods, ";
				strSQL += "DistHours , DistGrossPay, DistributionRecord,DistHoursDefault,MasterRecord,FICATaxGross,MedicareTaxGross,BasePayRate,fromschool,PROJECT,contractid,boolencumbrance) VALUES (";
				// strSQL = strSQL & rsMaster.Fields("ID") & ", '"
				strSQL += rsDistribution.Get_Fields("ID") + ",'";
				strSQL += rsMaster.Get_Fields_String("Status") + "', '";
				strSQL += rsMaster.Get_Fields("EmployeeNumber") + "', '";
				strSQL += fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("FirstName"))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields("middlename")))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("LastName"))) + " " + rsMaster.Get_Fields_String("Desig") + "', ";
				strSQL += " " + "0, '";
				// CheckNumber
				strSQL += " " + "', ";
				// PayDate
				strSQL += " " + "0, ";
				// NetPay
				strSQL += " " + "0, ";
				// JournalNumber
				strSQL += " " + "0, '";
				// WarrantNumber
				strSQL += rsDistribution.Get_Fields_String("AccountCode") + "', '";
				strSQL += rsDistribution.Get_Fields("RecordNumber") + "', '";
				strSQL += strAccountNumber + "', ";
				// strSQL = strSQL & rsDistribution.Fields("AccountNumber") & "', "
				strSQL += rsDistribution.Get_Fields("CAT") + ", '";
				// matthew tax code 12/12/03
				// strSQL = strSQL & rsDistribution.Fields("TaxCode") & "', '"
				strSQL += FCConvert.ToString(modGlobalRoutines.GetNewTaxCode(FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Cat"))))))) + "', '";
				strSQL += FCConvert.ToString(GetDistMRecord(rsDistribution.Get_Fields("MSRS"), rsMaster.Get_Fields("EmployeeNumber"))) + "', '";
				strSQL += (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields_String("StatusCode"))) == string.Empty ? GetDistMRecord("", rsMaster.Get_Fields("EmployeeNumber"), true) : fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields_String("StatusCode")))) + "', '";
				strSQL += FCConvert.ToString(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("GrantFunded")) == string.Empty ? "0" : fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_String("GrantFunded"))) + "', '";
				strSQL += FCConvert.ToString(GetDistURecord(rsDistribution.Get_Fields_String("DistU"), rsMaster.Get_Fields("EmployeeNumber"))) + "', '";
				// we have gotten rid of the DistW and it is now saved in WorkComp 03/28/2003 Matthew
				// strSQL = strSQL & GetWCFromEmployeeScreen(rsDistribution.Fields("WorkComp"), rsMaster.Fields("EmployeeNumber")) & "', '"
				strSQL += rsDistribution.Get_Fields_String("WorkComp") + "', '";
				strSQL += rsDistribution.Get_Fields("CD") + "', '";
				// DistPayCode
				strSQL += rsDistribution.Get_Fields_Decimal("BaseRate") + "', '";
				// DistPayRate
				strSQL += rsDistribution.Get_Fields_Double("Factor") + "', '";
				// DistMult
				if (FCConvert.ToInt32(rsDistribution.Get_Fields("NumberWeeks")) == 0)
				{
					strSQL += FCConvert.ToString(intTaxUpTop) + "', '";
				}
				else
				{
					strSQL += rsDistribution.Get_Fields("NumberWeeks") + "', '";
				}
				strSQL += FCConvert.ToString(dblHours) + "', '";
				// strSQL = strSQL & rsDistribution.Fields("HoursWeek") & "', '"
				strSQL += FCConvert.ToString(dblDistGrossPay) + "',1,";
				// strSQL = strSQL & Format(rsDistribution.Fields("Gross"), "0.00") & "',TRUE,"
				strSQL += FCConvert.ToString((rsDistribution.Get_Fields_Double("DefaultHours") == rsDistribution.Get_Fields_Double("HoursWeek") ? 1 : 0)) + ",'";
				strSQL += strMultiCheckCode + "',";
				if (aryDistribution[intCount].FICATaxExempt)
				{
					strSQL += "0,";
				}
				else
				{
					// strSQL = strSQL & Format(aryDistribution(intCount).Gross, "0.00") & ","
					strSQL += FCConvert.ToString(dblDistGrossPay) + ",";
				}
				if (aryDistribution[intCount].MedTaxExempt)
				{
					strSQL += "0,";
				}
				else
				{
					// strSQL = strSQL & Format(aryDistribution(intCount).Gross, "0.00") & ","
					strSQL += FCConvert.ToString(dblDistGrossPay) + ",";
				}
				strSQL += FCConvert.ToString(dblBasePayRate) + ",";
				strSQL += FCConvert.ToString((aryDistribution[intCount].boolFromSchool ? 1 : 0));
				strSQL += "," + FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields("project")));
				strSQL += "," + FCConvert.ToString(Conversion.Val(rsDistribution.Get_Fields("contractid")));
				strSQL += "," + FCConvert.ToString((aryDistribution[intCount].boolEncumbrance ? 1 : 0));
				strSQL += ")";
				strSQL = fecherFoundation.Strings.Trim(strSQL);
				strSQL = Strings.Left(strSQL, strSQL.Length - 1);
				strSQL += ")";
				rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
				//App.DoEvents();
				if (FCConvert.ToInt32(rsDistribution.Get_Fields("MSRSID")) == -2)
				{
					rsMSRS.OpenRecordset("Select * from tblMSRSDistributionDetail where boolDefault = 1 and EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "'");
				}
				else
				{
					if (Conversion.Val(rsDistribution.Get_Fields("MSRSID")) == 0)
					{
						rsDistribution.Edit();
						rsDistribution.Set_Fields("MSRSID", modCoreysSweeterCode.CNSTDISTMNOMSRS);
						rsDistribution.Update();
					}
					rsMSRS.OpenRecordset("Select * from tblMSRSDistributionDetail where ID = " + rsDistribution.Get_Fields("MSRSID"));
				}
				if (!rsMSRS.EndOfFile())
				{
					rsData.OpenRecordset("Select Max(ID) as MaxID from tblTempPayProcess");
					if (rsData.EndOfFile())
					{
					}
					else
					{
						if (Conversion.Val(rsData.Get_Fields("MaxID")) != 0)
						{
							rsData.OpenRecordset("Select * from tblTempPayProcess where ID = " + rsData.Get_Fields("MaxID"));
							if (!rsData.EndOfFile())
							{
								rsData.Edit();
								rsData.Set_Fields("ReportType", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields("ReportType"))));
								rsData.Set_Fields("FedCompAmount", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields_Double("FedCompAmount"))));
								rsData.Set_Fields("FedCompDollarPercent", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields("FedCompDollarPercent"))));
								rsData.Set_Fields("WorkWeeksPerYear", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields("WorkWeeksPerYear"))));
								rsData.Set_Fields("HourlyDailyContract", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields("HourlyDailyContract"))));
								rsData.Set_Fields("FullTimeEquivalent", FCConvert.ToString(Conversion.Val(rsMSRS.Get_Fields("FullTimeEquivalent"))));
								rsData.Set_Fields("MSRSID", rsMSRS.Get_Fields("ID"));
								rsData.Update();
							}
						}
					}
				}
			}
		}

		//private string DetermineExemptType(int lngCategory, int lngDeductionID, bool boolMatch = false)
		//{
		//	string DetermineExemptType = "";
		//	// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		//	int intCounter;
		//	// THIS IS A DEFAULT SO IF NO EXEMPT WAS FOUND
		//	DetermineExemptType = "X";
		//	for (intCounter = 0; intCounter <= (Information.UBound(aryPayCategories, 1)); intCounter++)
		//	{
		//		if (aryPayCategories[intCounter].CategoryNumber == lngCategory)
		//		{
		//			if (boolMatch)
		//			{
		//				if (aryPayCategories[intCounter].ExemptCode1 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType1, 1) == "R" || Strings.Left(aryPayCategories[intCounter].ExemptType1, 1) == "B") || aryPayCategories[intCounter].ExemptCode2 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType2, 1) == "R" || Strings.Left(aryPayCategories[intCounter].ExemptType2, 1) == "B") || aryPayCategories[intCounter].ExemptCode3 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType3, 1) == "R" || Strings.Left(aryPayCategories[intCounter].ExemptType3, 1) == "B") || aryPayCategories[intCounter].ExemptCode4 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType4, 1) == "R" || Strings.Left(aryPayCategories[intCounter].ExemptType4, 1) == "B") || aryPayCategories[intCounter].ExemptCode5 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType5, 1) == "R" || Strings.Left(aryPayCategories[intCounter].ExemptType5, 1) == "B"))
		//				{
		//					DetermineExemptType = "B";
		//					return DetermineExemptType;
		//				}
		//				else
		//				{
		//				}
		//			}
		//			else
		//			{
		//				if (aryPayCategories[intCounter].ExemptCode1 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType1, 1) == "E" || Strings.Left(aryPayCategories[intCounter].ExemptType1, 1) == "B") || aryPayCategories[intCounter].ExemptCode2 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType2, 1) == "E" || Strings.Left(aryPayCategories[intCounter].ExemptType2, 1) == "B") || aryPayCategories[intCounter].ExemptCode3 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType3, 1) == "E" || Strings.Left(aryPayCategories[intCounter].ExemptType3, 1) == "B") || aryPayCategories[intCounter].ExemptCode4 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType4, 1) == "E" || Strings.Left(aryPayCategories[intCounter].ExemptType4, 1) == "B") || aryPayCategories[intCounter].ExemptCode5 == FCConvert.ToDouble("9999") && (Strings.Left(aryPayCategories[intCounter].ExemptType5, 1) == "E" || Strings.Left(aryPayCategories[intCounter].ExemptType5, 1) == "B"))
		//				{
		//					DetermineExemptType = "B";
		//					return DetermineExemptType;
		//				}
		//				else
		//				{
		//				}
		//			}
		//			if (aryPayCategories[intCounter].ExemptCode1 == lngDeductionID)
		//			{
		//				DetermineExemptType = aryPayCategories[intCounter].ExemptType1;
		//			}
		//			else if (aryPayCategories[intCounter].ExemptCode2 == lngDeductionID)
		//			{
		//				DetermineExemptType = aryPayCategories[intCounter].ExemptType2;
		//			}
		//			else if (aryPayCategories[intCounter].ExemptCode3 == lngDeductionID)
		//			{
		//				DetermineExemptType = aryPayCategories[intCounter].ExemptType3;
		//			}
		//			else if (aryPayCategories[intCounter].ExemptCode4 == lngDeductionID)
		//			{
		//				DetermineExemptType = aryPayCategories[intCounter].ExemptType4;
		//			}
		//			else if (aryPayCategories[intCounter].ExemptCode5 == lngDeductionID)
		//			{
		//				DetermineExemptType = aryPayCategories[intCounter].ExemptType5;
		//			}
		//			DetermineExemptType = fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(Strings.Left(DetermineExemptType, 1)));
		//			return DetermineExemptType;
		//		}
		//	}
		//	return DetermineExemptType;
		//}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetDistURecord(string strText, string strEmployeeNumber)
		{
			object GetDistURecord = null;
			// if the master screen has a "N" then this record needs to be an N
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" + strEmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				GetDistURecord = "N";
			}
			else
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Unemployment")))) == "N")
				{
					GetDistURecord = "N";
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Unemployment")))) == "Y")
				{
					// then there is an S or Y for Unemployement master screen
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "Y")
					{
						GetDistURecord = "Y" + rsData.Get_Fields_String("NatureCode");
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "S")
					{
						GetDistURecord = "S" + rsData.Get_Fields_String("NatureCode");
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "N")
					{
						GetDistURecord = "N";
					}
					else
					{
						GetDistURecord = strText;
					}
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Unemployment")))) == "S")
				{
					// then there is an S or Y for Unemployement master screen
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "Y")
					{
						GetDistURecord = "S" + rsData.Get_Fields_String("NatureCode");
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "S")
					{
						GetDistURecord = "S" + rsData.Get_Fields_String("NatureCode");
					}
					else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) == "N")
					{
						GetDistURecord = "N";
					}
					else
					{
						GetDistURecord = strText;
					}
				}
			}
			return GetDistURecord;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object GetDistMRecord(string strText, string strEmployeeNumber, bool boolStatusCode = false)
		{
			object GetDistMRecord = null;
			clsDRWrapper rsData = new clsDRWrapper();
			if (boolStatusCode)
			{
				// MATTHEW CALL ID 72106 7/6/2005
				// Call rsData.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" & strEmployeeNumber & "'", DEFAULTDATABASE)
				rsData.OpenRecordset("Select * from tblMSRSDistributionDetail where boolDefault = 1 AND EmployeeNumber = '" + strEmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					GetDistMRecord = string.Empty;
				}
				else
				{
					GetDistMRecord = rsData.Get_Fields_String("Status");
					// MATTHEW CALL ID 72106 7/6/2005
					// GetDistMRecord = rsData.Fields("StatusCode")
				}
			}
			else
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strText)) != "Y")
				{
					GetDistMRecord = strText;
				}
				else
				{
					// MATTHEW CALL ID 72106 7/6/2005
					// Call rsData.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" & strEmployeeNumber & "'", DEFAULTDATABASE)
					rsData.OpenRecordset("Select * from tblMSRSDistributionDetail where boolDefault = 1 AND EmployeeNumber = '" + strEmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
						GetDistMRecord = "N";
					}
					else
					{
						// MATTHEW CALL ID 72106 7/6/2005
						// If IsNull(rsData.Fields("PositionCode")) Then
						if (fecherFoundation.FCUtils.IsNull(rsData.Get_Fields_String("Position")))
						{
							GetDistMRecord = "";
						}
						else
						{
							// MATTHEW CALL ID 72106 7/6/2005
							// GetDistMRecord = rsData.Fields("PositionCode")
							GetDistMRecord = rsData.Get_Fields_String("Position");
						}
					}
				}
			}
			return GetDistMRecord;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, int, object)
		//private object GetWCFromEmployeeScreen(string WCText, string strEmployeeNumber)
		//{
		//	object GetWCFromEmployeeScreen = null;
		//	if (Conversion.Val(WCText) != 0)
		//	{
		//		GetWCFromEmployeeScreen = WCText;
		//	}
		//	else
		//	{
		//		clsDRWrapper rsData = new clsDRWrapper();
		//		rsData.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + strEmployeeNumber + "'", modGlobalVariables.DEFAULTDATABASE);
		//		if (rsData.EndOfFile())
		//		{
		//			GetWCFromEmployeeScreen = 0;
		//		}
		//		else
		//		{
		//			GetWCFromEmployeeScreen = rsData.Get_Fields_String("WorkCompCode");
		//		}
		//	}
		//	return GetWCFromEmployeeScreen;
		//}

		private void GetAllDeductions()
		{
			int intTaxStatusArrayIndex = 0;
			if (!rsDeductions.EndOfFile() && !modGlobalVariables.Statics.pboolUnloadProcessGrid)
			{
				for (intCounter = 0; intCounter <= (rsDeductions.RecordCount() - 1); intCounter++)
				{
					//App.DoEvents();
					// is not default so get Amount, AmountType etc from tblEmployeeDeductions
					if (!rsDeductions.EndOfFile())
					{
						lngDeduction = CalculateDeduction(intCounter);
						lngDeduction = FCConvert.ToDouble(Strings.Format(lngDeduction, "0.00"));
						// NOT SURE WHY WE ARE COMMENTING THIS LINE OUT.
						// A PAY RUN WITH JAY ON 3/15/2004 MADE A DEDUCTION GO NEGATIVE AND
						// THIS LINE OF CODE LET THAT NEGATIVE NUMBER GO THRU.
						if (lngDeduction != 0 && dblGrossTotal != 0)
						{
							if (intEmployeeCheckCounter == 1 || (CurrentDeductions.AmountType == "Percent" && rsMaster.Get_Fields_Boolean("DeductionPercent")) || (CurrentDeductions.AmountType == "Dollars" && rsMaster.Get_Fields_Boolean("DeductionDollars")))
							{
								// get the array index for the tax status code array
								intTaxStatusArrayIndex = FCConvert.ToInt16(GetTaxStatusArrayIndex(FCConvert.ToString(CurrentDeductions.TaxCode)));
								// CHECK TO SEE IF THE PERTAINS TO FIELD FOR THE TAX STATUS CODE
								// IS EITHER 1 FOR EMPLOYEE OR 3 FOR BOTH MATTHEW 10/22/2004
								if (DefaultTaxStatusCodes[intTaxStatusArrayIndex].PetainsTo == 2)
								{
								}
								else
								{
									aryDeductions[intCounter].Priority = CurrentDeductions.Priority;
									aryDeductions[intCounter].Amount = lngDeduction;
									aryDeductions[intCounter].FedTaxExempt = DefaultTaxStatusCodes[intTaxStatusArrayIndex].FedTaxExempt;
									aryDeductions[intCounter].StateTaxExempt = DefaultTaxStatusCodes[intTaxStatusArrayIndex].StateTaxExempt;
									aryDeductions[intCounter].FICATaxExempt = DefaultTaxStatusCodes[intTaxStatusArrayIndex].FICATaxExempt;
									aryDeductions[intCounter].MedTaxExempt = DefaultTaxStatusCodes[intTaxStatusArrayIndex].MedTaxExempt;
									aryDeductions[intCounter].UExempt = CurrentDeductions.UExempt;
									aryDeductions[intCounter].AmountType = CurrentDeductions.AmountType;
									aryDeductions[intCounter].PercentType = CurrentDeductions.PercentType;
									aryDeductions[intCounter].TaxStatusCode = CurrentDeductions.TaxCode;
									aryDeductions[intCounter].Limit = CurrentDeductions.Limit;
									aryDeductions[intCounter].DeductionDescription = CurrentDeductions.Description;
									aryDeductions[intCounter].DeductionAccountNumber = CurrentDeductions.AccountNumber;
									// update life to date
									rsDeductions.Edit();
									rsDeductions.Set_Fields("CurrentTotal", lngDeduction);
									rsDeductions.Update();
									// annualize deduction
									if (aryDeductions[intCounter].FedTaxExempt)
									{
										if (intDeductionUpTop > 0)
										{
											udtAnnualizedEmployee.AnnualizedFedTax = (udtAnnualizedEmployee.AnnualizedFedTax) - ((lngDeduction / intDeductionUpTop) * intFactor);
											if (udtAnnualizedEmployee.AnnualizedFedTax < 0)
												udtAnnualizedEmployee.AnnualizedFedTax = 0;
										}
									}
									if (aryDeductions[intCounter].StateTaxExempt)
									{
										if (intDeductionUpTop > 0)
										{
											udtAnnualizedEmployee.AnnualizedStateTax -= ((lngDeduction / intDeductionUpTop) * intFactor);
											if (udtAnnualizedEmployee.AnnualizedStateTax < 0)
												udtAnnualizedEmployee.AnnualizedStateTax = 0;
										}
									}
								}
							}
							else
							{
								aryDeductions[intCounter].Amount = 0;
							}
							// End If
						}
						else
						{
							aryDeductions[intCounter].Amount = 0;
						}
					}
					rsDeductions.MoveNext();
				}
				intGridRow += 1;
				vsGrid.Rows += 1;
				vsGrid.RowData(intGridRow, strTestEmployeeID);
				vsGrid.TextMatrix(intGridRow, 1, "Deduction #");
				vsGrid.TextMatrix(intGridRow, 2, "Amount");
				vsGrid.TextMatrix(intGridRow, 3, "Fed Gross");
				vsGrid.TextMatrix(intGridRow, 6, "State Gross");
				vsGrid.TextMatrix(intGridRow, 4, "FICA Gross");
				vsGrid.TextMatrix(intGridRow, 5, "Med Gross");
				vsGrid.RowOutlineLevel(intGridRow, 1);
				vsGrid.IsSubtotal(intGridRow, true);
				// apply deductions to Gross values
				for (intCounter = 0; intCounter <= (Information.UBound(aryDeductions, 1) - 1); intCounter++)
				{
					if (aryDeductions[intCounter].FedTaxExempt)
						udtEmployee.FedTaxGross -= aryDeductions[intCounter].Amount;
					if (aryDeductions[intCounter].StateTaxExempt)
						udtEmployee.StateTaxGross -= aryDeductions[intCounter].Amount;
					if (aryDeductions[intCounter].FICATaxExempt)
					{
						// 12/11/2007 Jay
						if (!FCConvert.ToBoolean(rsMaster.Get_Fields("masterficaexempt")))
						{
							udtEmployee.FICATaxGross -= aryDeductions[intCounter].Amount;
						}
					}
					if (aryDeductions[intCounter].MedTaxExempt)
					{
						if (!FCConvert.ToBoolean(rsMaster.Get_Fields("mastermedexempt")))
						{
							udtEmployee.MedTaxGross -= aryDeductions[intCounter].Amount;
						}
					}
					// Display
					intGridRow += 1;
					vsGrid.Rows += 1;
					vsGrid.RowData(intGridRow, strTestEmployeeID);
					vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryDeductions[intCounter].DeductionCode) + " - " + aryDeductions[intCounter].DeductionDescription);
					vsGrid.TextMatrix(intGridRow, 2, Strings.Format(aryDeductions[intCounter].Amount, "0.00"));
					vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtEmployee.FedTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtEmployee.StateTaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtEmployee.FICATaxGross, "0.00"));
					vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtEmployee.MedTaxGross, "0.00"));
					aryDeductions[intCounter].RowNumber = intGridRow;
					vsGrid.RowOutlineLevel(intGridRow, 2);
					vsGrid.IsSubtotal(intGridRow, false);
					if (aryDeductions[intCounter].AmountType == "Levy" && aryDeductions[intCounter].Apply)
					{
						gintLevyGridRow = intGridRow;
					}
				}
			}
		}

		private void CreateDeductionTempPayProcessRecord()
		{
			string strSQL = "";
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strAccount = "";
			clsDRWrapper rsData = new clsDRWrapper();
			// If strTestEmployeeID = 365 Then MsgBox "A"
			for (intCounter = 0; intCounter <= (Information.UBound(aryDeductions, 1) - 1); intCounter++)
			{
				if (Conversion.Val(aryDeductions[intCounter].Amount) == 0)
				{
				}
				else
				{
					strSQL = "Insert INTO tblTempPayProcess (EmpDeductionID,DedDeductionNumber, EmployeeNumber, EmployeeName, ";
					strSQL += "DedTaxCode, DedAmount, DedHitLimit, ";
					strSQL += "DedReduced, DeductionRecord, MasterRecord, DeductionTitle, DeductionAccountNumber,FICATaxGross,MedicareTaxGross,DedGrossAmount,uexempt) VALUES (";
					strSQL += FCConvert.ToString(aryDeductions[intCounter].EmpDeductionID) + ",";
					strSQL += FCConvert.ToString(aryDeductions[intCounter].DeductionID) + ", '";
					strSQL += strTestEmployeeID + "', '";
					strSQL += fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("FirstName"))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields("middlename")))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("LastName"))) + " " + rsMaster.Get_Fields_String("Desig") + "', '";
					strSQL += FCConvert.ToString(aryDeductions[intCounter].TaxStatusCode) + "', '";
					strSQL += Strings.Format(aryDeductions[intCounter].Amount, "0.00") + "', ";
					// strSQL = strSQL & IIf(aryDeductions(intCounter).Limit = 0, False, IIf(aryDeductions(intCounter).Limit > aryDeductions(intCounter).HitLimit, False, True))
					strSQL += FCConvert.ToString((aryDeductions[intCounter].HitLimit ? 1 : 0));
					if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
					{
						strAccount = aryDeductions[intCounter].DeductionAccountNumber;
						// 01/03/2006 Need to format fund
						Strings.MidSet(ref strAccount, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
						strSQL += ", 0, 1,'" + strMultiCheckCode + "','" + FCConvert.ToString(modGlobalRoutines.FixQuote(modDavesSweetCode.GetAccountDescription(strAccount))) + "','" + strAccount + "',";
					}
					else
					{
						strSQL += ", 0, 1,'" + strMultiCheckCode + "','" + FCConvert.ToString(modGlobalRoutines.FixQuote(aryDeductions[intCounter].DeductionDescription)) + "','" + aryDeductions[intCounter].DeductionAccountNumber + "',";
					}
					if (aryDeductions[intCounter].FICATaxExempt)
					{
						if (!FCConvert.ToBoolean(rsMaster.Get_Fields("masterficaexempt")))
						{
							strSQL += FCConvert.ToString(FCConvert.ToDouble(Strings.Format(aryDeductions[intCounter].Amount, "0.00")) * -1) + ",";
						}
						else
						{
							strSQL += "0,";
						}
					}
					else
					{
						strSQL += "0,";
					}
					if (aryDeductions[intCounter].MedTaxExempt)
					{
						if (!FCConvert.ToBoolean(rsMaster.Get_Fields("mastermedexempt")))
						{
							strSQL += FCConvert.ToString(FCConvert.ToDouble(Strings.Format(aryDeductions[intCounter].Amount, "0.00")) * -1);
						}
						else
						{
							strSQL += "0";
						}
					}
					else
					{
						strSQL += "0";
					}
					strSQL += "," + FCConvert.ToString(aryDeductions[intCounter].DeductionGross);
					strSQL += "," + FCConvert.ToString((aryDeductions[intCounter].UExempt ? 1 : 0));
					strSQL += ")";
					strSQL = fecherFoundation.Strings.Trim(strSQL);
					strSQL = Strings.Left(strSQL, strSQL.Length - 1);
					strSQL += ")";
					rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
				}
			}
		}

		private void GetEmployeeMatch()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (!rsEmployeeMatches.EndOfFile())
			{
				lngEmployeeMatch = 0;
				string strAccounts = "";
				string[] strAry = null;
				PayrollEmployeeMatches tmpMatch = new PayrollEmployeeMatches();
				int x;
				int intindex;
				double dblTemp;
				double dblAmountLeft;
				double dblMatchAmountLeft;
				double dblOriginalAmountLeft;
				intCounter = 0;
				// For intCounter = 0 To rsEmployeeMatches.RecordCount - 1
				while (intCounter < rsEmployeeMatches.RecordCount())
				{
					lngEmployeeMatch = CalculateEmployeeMatch(intCounter);
					if (lngEmployeeMatch != 0 && dblGrossTotal != 0)
					{
						if (intEmployeeCheckCounter == 1 || (FCConvert.ToString(rsEmployeeMatches.Get_Fields_String("AmountType")) == "D" && rsMaster.Get_Fields_Boolean("MatchDollars")) || ((FCConvert.ToString(rsEmployeeMatches.Get_Fields_String("AmountType")) == "%G" || FCConvert.ToString(rsEmployeeMatches.Get_Fields_String("AmountType")) == "%D") && rsMaster.Get_Fields_Boolean("MatchPercentGross")))
						{
							// Or (rsEmployeeMatches.Fields("AmountType") = "%D" And rsMaster.Fields("MatchPercentDeduction")) Then
							// The <> D will allow any percentage to go thru because we do want them even
							// if this is a second or third check
							// CHECK TO SEE IF THE PERTAINS TO FIELD FOR THE TAX STATUS CODE
							// IS EITHER 2 FOR EMPLOYER OR 3 FOR BOTH MATTHEW 10/22/2004
							if (FCConvert.ToInt32(rsEmployeeMatches.Get_Fields("Pertains")) == 1)
							{
							}
							else
							{
								aryEmployeeMatches[intCounter].RowNumber = FCConvert.ToInt16(rsEmployeeMatches.Get_Fields("RecordNumber"));
								aryEmployeeMatches[intCounter].Amount = lngEmployeeMatch;
								// aryEmployeeMatches(intCounter).OriginalAmount = rsEmployeeMatches.Fields("Amount")
								aryEmployeeMatches[intCounter].OriginalAmount = CurrentDeductions.Amount;
								aryEmployeeMatches[intCounter].FedTaxExempt = FCConvert.CBool((FCConvert.ToString(rsEmployeeMatches.Get_Fields_Boolean("FedExempt")) == string.Empty ? false : rsEmployeeMatches.Get_Fields_Boolean("FedExempt")));
								aryEmployeeMatches[intCounter].StateTaxExempt = FCConvert.CBool((FCConvert.ToString(rsEmployeeMatches.Get_Fields_Boolean("StateExempt")) == string.Empty ? false : rsEmployeeMatches.Get_Fields_Boolean("StateExempt")));
								aryEmployeeMatches[intCounter].FICATaxExempt = FCConvert.CBool((FCConvert.ToString(rsEmployeeMatches.Get_Fields_Boolean("FICAExempt")) == string.Empty ? false : rsEmployeeMatches.Get_Fields_Boolean("FICAExempt")));
								aryEmployeeMatches[intCounter].MedTaxExempt = FCConvert.CBool((FCConvert.ToString(rsEmployeeMatches.Get_Fields_Boolean("MedicareExempt")) == string.Empty ? false : rsEmployeeMatches.Get_Fields_Boolean("MedicareExempt")));
								// aryEmployeeMatches(intCounter).AmountType = Trim(rsEmployeeMatches.Fields("AmountType") & " ")
								aryEmployeeMatches[intCounter].AmountType = CurrentDeductions.AmountType;
								rsTemp.OpenRecordset("SELECT * FROM tblEmployersMatch WHERE ID = " + rsEmployeeMatches.Get_Fields("ID"));
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
									rsTemp.Edit();
									rsTemp.Set_Fields("CurrentTotal", lngEmployeeMatch);
									rsTemp.Update();
								}
								// 12/11/2007 Jay
								if (!aryEmployeeMatches[intCounter].FICATaxExempt)
								{
									aryEmployeeMatches[intCounter].FICATaxExempt = FCConvert.ToBoolean(rsMaster.Get_Fields("masterficaexempt"));
								}
								if (!aryEmployeeMatches[intCounter].MedTaxExempt)
								{
									aryEmployeeMatches[intCounter].MedTaxExempt = FCConvert.ToBoolean(rsMaster.Get_Fields("mastermedexempt"));
								}
								// annualize Employer Match
								if (!aryEmployeeMatches[intCounter].FedTaxExempt)
								{
									if (intDeductionUpTop > 0)
									{
										udtAnnualizedEmployee.AnnualizedFedTax += ((lngEmployeeMatch / intDeductionUpTop) * intFactor);
									}
								}
								if (!aryEmployeeMatches[intCounter].StateTaxExempt)
								{
									if (intDeductionUpTop > 0)
									{
										udtAnnualizedEmployee.AnnualizedStateTax += ((lngEmployeeMatch / intDeductionUpTop) * intFactor);
									}
								}
							}
							switch (fecherFoundation.Strings.UCase(Strings.Left(aryEmployeeMatches[intCounter].Account + " ", 1)))
							{
							
								default:
									{
										break;
									}
							}
							//end switch
						}
						else
						{
							aryEmployeeMatches[intCounter].Amount = 0;
						}
					}
					else
					{
						aryEmployeeMatches[intCounter].Amount = 0;
					}
					rsEmployeeMatches.MoveNext();
					// Next
					intCounter += 1;
				}
				// Display
				intGridRow += 1;
				vsGrid.Rows += 1;
				vsGrid.RowData(intGridRow, strTestEmployeeID);
				vsGrid.TextMatrix(intGridRow, 1, "Employer Match #");
				vsGrid.TextMatrix(intGridRow, 2, "Amount");
				vsGrid.TextMatrix(intGridRow, 3, "Fed Gross");
				vsGrid.TextMatrix(intGridRow, 6, "State Gross");
				vsGrid.TextMatrix(intGridRow, 4, "FICA Gross");
				vsGrid.TextMatrix(intGridRow, 5, "Med Gross");
				vsGrid.RowOutlineLevel(intGridRow, 1);
				vsGrid.IsSubtotal(intGridRow, true);
				// apply Employer Matches to Gross values
				for (intCounter = 0; intCounter <= (Information.UBound(aryEmployeeMatches, 1)); intCounter++)
				{
					if (aryEmployeeMatches[intCounter].DeductionID != 0)
					{
						if (!aryEmployeeMatches[intCounter].FedTaxExempt)
							udtEmployee.FedTaxGross = FCConvert.ToDouble(Strings.Format(Conversion.Val(Strings.Format(udtEmployee.FedTaxGross, "0.00")) + Conversion.Val(Strings.Format(aryEmployeeMatches[intCounter].Amount, "0.00")), "0.00"));
						if (!aryEmployeeMatches[intCounter].StateTaxExempt)
							udtEmployee.StateTaxGross = FCConvert.ToDouble(Strings.Format(Conversion.Val(Strings.Format(udtEmployee.StateTaxGross, "0.00")) + Conversion.Val(Strings.Format(aryEmployeeMatches[intCounter].Amount, "0.00")), "0.00"));
						if (!aryEmployeeMatches[intCounter].FICATaxExempt)
							udtEmployee.FICATaxGross = FCConvert.ToDouble(Strings.Format(Conversion.Val(Strings.Format(udtEmployee.FICATaxGross, "0.00")) + Conversion.Val(Strings.Format(aryEmployeeMatches[intCounter].Amount, "0.00")), "0.00"));
						if (!aryEmployeeMatches[intCounter].MedTaxExempt)
							udtEmployee.MedTaxGross = FCConvert.ToDouble(Strings.Format(Conversion.Val(Strings.Format(udtEmployee.MedTaxGross, "0.00")) + Conversion.Val(Strings.Format(aryEmployeeMatches[intCounter].Amount, "0.00")), "0.00"));
						// Display
						intGridRow += 1;
						vsGrid.Rows += 1;
						vsGrid.RowData(intGridRow, strTestEmployeeID);
						// matthew 12/29/03
						// vsGrid.TextMatrix(intGridRow, 1) = aryEmployeeMatches(intCounter).matchnumber & " - " & aryEmployeeMatches(intCounter).DeductionDescription
						vsGrid.TextMatrix(intGridRow, 1, FCConvert.ToString(aryEmployeeMatches[intCounter].DeductionNumber) + " - " + aryEmployeeMatches[intCounter].DeductionDescription);
						vsGrid.TextMatrix(intGridRow, 2, Strings.Format(aryEmployeeMatches[intCounter].Amount, "0.00"));
						vsGrid.TextMatrix(intGridRow, 3, Strings.Format(udtEmployee.FedTaxGross, "0.00"));
						vsGrid.TextMatrix(intGridRow, 6, Strings.Format(udtEmployee.StateTaxGross, "0.00"));
						vsGrid.TextMatrix(intGridRow, 4, Strings.Format(udtEmployee.FICATaxGross, "0.00"));
						vsGrid.TextMatrix(intGridRow, 5, Strings.Format(udtEmployee.MedTaxGross, "0.00"));
						vsGrid.RowOutlineLevel(intGridRow, 2);
						vsGrid.IsSubtotal(intGridRow, false);
					}
				}
			}
			// ********************************************************************************************************
			// CREATED THESE ENTRIES BECAUSE THE EXEMPT PAY MINUS EXEMPT DEDUCTIONS CAN IN SOME CASES GO NEGATIVE.
			// WAIT TILL THIS POINT TO MAKE SURE THAT MATCH DOESN'T OFFSET THE NEGATIVE
			// MATTHEW 3/9/2004
			if (udtEmployee.FedTaxGross < 0.01)
				udtEmployee.FedTaxGross = 0;
			if (udtEmployee.StateTaxGross < 0.01)
				udtEmployee.StateTaxGross = 0;
			if (udtEmployee.FICATaxGross < 0.01)
				udtEmployee.FICATaxGross = 0;
			if (udtEmployee.MedTaxGross < 0.01)
				udtEmployee.MedTaxGross = 0;
			// ********************************************************************************************************
		}

		
		
		private object GetCheckCode(int intCodeID)
		{
			object GetCheckCode = null;
			// #1;N      -Not Used
			// #2;W      -Weekly
			// #3;1      -One Week Only
			// ******************************************************************************************
			// this would be if we were to have a new distribution code type that was just weeks 1-4 and
			// not 5. matthew 10/26/2004
			// #13;5      -Week 1-4 Not 5
			// #4;S1     -Separate Check(W)
			// #5;S2     -Separate Check(W)
			// #6;S3     -Separate Check(W)
			// #7;S4     -Separate Check(W)
			// #8;S5     -Separate Check(W)
			// #9;S6     -Separate Check(W)
			// #10;S7     -Separate Check(W)
			// #11;S8     -Separate Check(W)
			// #12;S9     -Separate Check(W)
			switch (intCodeID)
			{
				case 1:
					{
						GetCheckCode = "0";
						break;
					}
				case 2:
					{
						GetCheckCode = "0";
						break;
					}
				case 3:
					{
						GetCheckCode = "0";
						break;
					}
				case 4:
					{
						GetCheckCode = "1";
						break;
					}
				case 5:
					{
						GetCheckCode = "2";
						break;
					}
				case 6:
					{
						GetCheckCode = "3";
						break;
					}
				case 7:
					{
						GetCheckCode = "4";
						break;
					}
				case 8:
					{
						GetCheckCode = "5";
						break;
					}
				case 9:
					{
						GetCheckCode = "6";
						break;
					}
				case 10:
					{
						GetCheckCode = "7";
						break;
					}
				case 11:
					{
						GetCheckCode = "8";
						break;
					}
				case 12:
					{
						GetCheckCode = "9";
						break;
					}
				case 13:
					{
						GetCheckCode = "0";
						break;
					}
				default:
					{
						GetCheckCode = "0";
						break;
					}
			}
			//end switch
			return GetCheckCode;
		}

		private void CreateDirectDepositTempPayProcessRecord(bool boolRegularChecks)
		{
			string strSQL = "";
			int intCounter;
			double dblNet = 0;
            double dblAmount = 0;
			bool boolDepositMade = false;
			clsDRWrapper rsData = new clsDRWrapper();
			var rsSave = new clsDRWrapper();
			double dblDepositAmount;
            double dblDDAmount = 0;
			double dblMinCheck = 0;
			double dblNetToUse = 0;
            int intTotPerc;
			// CLEA
			modGlobalVariables.Statics.gdblBothRegularTotal = 0;
			modGlobalVariables.Statics.gdblBothDirectDepositTotal = 0;
			modGlobalVariables.Statics.gdblRegularTotal = 0;
			modGlobalVariables.Statics.gdblDirectDepositTotal = 0;
			intDirectDeposit = 0;
			intRegularChecks = 0;
			intBothDeposits = 0;
			intTotPerc = 0;
			rsData.OpenRecordset("SELECT tblBanks.BankDepositID, tblDirectDeposit.* FROM tblBanks INNER JOIN tblDirectDeposit ON tblBanks.ID = tblDirectDeposit.Bank where EmployeeNumber = '" + strTestEmployeeID + "' and prenote = 0 Order by RowNumber", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				intRegularChecks += 1;
				modGlobalVariables.Statics.gdblRegularTotal += udtEmployee.TotalNet;
			}
			else if (boolRegularChecks)
			{
				intRegularChecks += 1;
				modGlobalVariables.Statics.gdblRegularTotal += udtEmployee.TotalNet;
			}
			else
			{
				dblNet = udtEmployee.TotalNet;
				dblNetToUse = udtEmployee.TotalNet;
				dblMinCheck = 0;
				if (Conversion.Val(rsMaster.Get_Fields("minimumcheck")) > 0)
				{
					dblMinCheck = Conversion.Val(rsMaster.Get_Fields("minimumcheck")) * intDeductionUpTop;
				}
				if (dblMinCheck >= dblNetToUse)
				{
					intRegularChecks += 1;
					modGlobalVariables.Statics.gdblRegularTotal += udtEmployee.TotalNet;
				}
				else
				{
					dblNet -= dblMinCheck;
					dblNetToUse -= dblMinCheck;
					while (!rsData.EndOfFile() && dblNet > 0)
					{
						if (Conversion.Val(rsData.Get_Fields("Amount")) > 0)
						{
							if (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars")
							{
								dblDDAmount = (Strings.Format(rsData.Get_Fields_Double("Amount"), "0.00")).ToDoubleValue() * intDeductionUpTop;
							}
							else
							{
								// WE ARE NOW USING NET AND NOT GROSS PER ASSUMPTION
								// THAT THE USER WOULD WANT % OF TAKE HOME AND NOT % OF GROSS
								// MATTHEW 06/19/03
								// dblDDAmount = Format(udtEmployee.TotalGross * (rsdata.Fields("Amount") / 100), "0.00")
								if (Conversion.Val(rsData.Get_Fields("amount")) + intTotPerc < 100)
								{
									intTotPerc += Conversion.Val(rsData.Get_Fields("amount"));
									dblDDAmount = FCConvert.ToDouble(Strings.Format(dblNetToUse * (FCConvert.ToDouble(rsData.Get_Fields("Amount")) / 100), "0.00"));
								}
								else
								{
									// this brings it to 100% so take the rest.
									intTotPerc = 100;
									dblDDAmount = dblNet;
								}
								// dblDDAmount = Format(dblNet * (rsData.Fields("Amount") / 100), "0.00")
							}
							dblAmount = FCConvert.ToDouble(Strings.Format(Conversion.Val(dblNet) - Conversion.Val(dblDDAmount), "0.00"));
                           
							if (intEmployeeCheckCounter == 1 || (FCConvert.ToString(rsData.Get_Fields("Type")) == "Percent" && rsMaster.Get_Fields_Boolean("DDPercent")) || (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars" && rsMaster.Get_Fields_Boolean("DDDollars")))
							{
                                rsSave.OpenRecordset("select * from tbltemppayprocess where id = -1", "Payroll");
                                rsSave.AddNew();
								
								rsSave.Set_Fields("EmployeeNumber", strTestEmployeeID);
                                    rsSave.Set_Fields("EmployeeName",
                                        ((rsMaster.Get_Fields_String("FirstName") + " " +
                                         rsMaster.Get_Fields_String("middlename")
                                         ).Trim() + " " +
                                         rsMaster.Get_Fields_String("LastName") + " " +
                                         rsMaster.Get_Fields_String("Desig")).Trim());
									rsSave.Set_Fields("DDBankNumber",rsData.Get_Fields_String("Bank"));
									rsSave.Set_Fields("DDAccountNumber",rsData.Get_Fields_String("Account"));
                                    if (dblAmount > 0)
                                    {
										rsSave.Set_Fields("DDAmount",dblDDAmount.FormatAsMoney().ToDecimalValue());
                                    }
                                    else
                                    {
										// THERE IS NOT ENOUGH GROSS TO COVER THE DEPOSIT SO ADD
										// ALL OF THE GROSS
									rsSave.Set_Fields("DDAmount", dblNet.FormatAsMoney().ToDecimalValue());
										dblNet = 0;
										dblAmount = 0;
									}

                                    rsSave.Set_Fields("DDAccountType",rsData.Get_Fields_String("AccountType"));
									rsSave.Set_Fields("BankRecord",true);
									rsSave.Set_Fields("MasterRecord", strMultiCheckCode);
                                    rsSave.Set_Fields("DDBankDFI", rsData.Get_Fields_String("BankDepositID"));
                                    rsSave.Update();
									
									boolDepositMade = true;

							}
							else
							{
								// call id 78573 matthew 10/4/2005
								// if the first one is dollars and it is an s1 check then nothing was created for this entry so we need the dblAmount to be
								// back to what it was before we did the calculation for this deduction.
								dblAmount = dblNet;
							}
						}
						else
						{
							dblAmount = dblNet;
						}
						rsData.MoveNext();
						dblNet = dblAmount;
					}
					dblNet += dblMinCheck;
					if (boolDepositMade)
					{
						if (dblNet > 0)
						{
							modGlobalVariables.Statics.gdblBothRegularTotal += dblNet;
							modGlobalVariables.Statics.gdblBothDirectDepositTotal += (udtEmployee.TotalNet - dblNet);
							intBothDeposits += 1;
						}
						else
						{
							modGlobalVariables.Statics.gdblDirectDepositTotal += (udtEmployee.TotalNet - dblNet);
							intDirectDeposit += 1;
						}
					}
					else
					{
						intRegularChecks += 1;
						modGlobalVariables.Statics.gdblRegularTotal += udtEmployee.TotalNet;
					}
				}
			}
			EndOfRecord:
			;
			// update the total record for this person with the total amounts
			rsData.OpenRecordset("Select * from tblTempPayProcess where EmployeeNumber = '" + strTestEmployeeID + "' AND MasterRecord = '" + strMultiCheckCode + "' AND TotalRecord = 1", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				rsData.AddNew();
				rsData.Set_Fields("EmployeeNumber", strTestEmployeeID);
				rsData.Set_Fields("PayDate", modGlobalVariables.Statics.gdatCurrentPayDate);
				rsData.Set_Fields("PayRunID", modGlobalVariables.Statics.gintCurrentPayRun);
				rsData.Set_Fields("TotalRecord", true);
			}
			else
			{
				rsData.Edit();
			}
			rsData.Set_Fields("RegularCheckAmount", Strings.Format(modGlobalVariables.Statics.gdblRegularTotal, "0.00"));
			rsData.Set_Fields("DirectDepositAmount", Strings.Format(modGlobalVariables.Statics.gdblDirectDepositTotal, "0.00"));
			rsData.Set_Fields("BothRegularCheckAmount", Strings.Format(modGlobalVariables.Statics.gdblBothRegularTotal, "0.00"));
			rsData.Set_Fields("BothDirectDepositAmount", Strings.Format(modGlobalVariables.Statics.gdblBothDirectDepositTotal, "0.00"));
			rsData.Update();
		}

		private void CreateMatchTempPayProcessRecord()
		{
			string strSQL = "";
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			int intMultiTown = 0;
			string strAccountNumber = "";
			clsDRWrapper rsMulti = new clsDRWrapper();
			int intBreakdownCode = 0;
			double dblDistGrossPay;
			double dblHours = 0;
			int intCount;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				for (intCounter = 0; intCounter <= (Information.UBound(aryEmployeeMatches, 1)); intCounter++)
				{
					if (aryEmployeeMatches[intCounter].Amount != 0)
					{
						strAccountNumber = aryEmployeeMatches[intCounter].Account;
						dblHours = aryEmployeeMatches[intCounter].Amount;
						if (modRegionalTown.IsRegionalTown())
						{
							if (fecherFoundation.Strings.Trim(strAccountNumber).Length >= 3 && Strings.Left(fecherFoundation.Strings.Trim(strAccountNumber), 1) == "E")
							{
								if (FCConvert.ToDouble(Strings.Mid(fecherFoundation.Strings.Trim(strAccountNumber), 3, 1)) == 0)
								{
									// this is a multitown number so check it three times
									intBreakdownCode = 0;
									rsMulti.OpenRecordset("Select * from DeptDivTitles where Department = '1" + Strings.Mid(strAccountNumber, 4, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) - 1)) + "'", "TWBD0000.vb1");
									if (!rsMulti.EndOfFile())
									{
										if (FCConvert.ToInt32(rsMulti.Get_Fields("BreakDownCode")) != 0)
										{
											intBreakdownCode = FCConvert.ToInt16(rsMulti.Get_Fields("BreakDownCode"));
										}
									}
									if (intBreakdownCode == 0)
									{
										intMultiTown = 1;
										strAccountNumber = "E 1" + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
									}
									else
									{
										rsMulti.OpenRecordset("Select * from RegionalTownBreakDown where Code = " + FCConvert.ToString(intBreakdownCode) + " Order by TownNumber", "CentralData");
										while (!rsMulti.EndOfFile())
										{
											intMultiTown += 1;
											rsMulti.MoveNext();
										}
										rsMulti.MoveFirst();
										strAccountNumber = "E " + rsMulti.Get_Fields("TownNumber") + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
										dblHours = aryEmployeeMatches[intCounter].Amount * rsMulti.Get_Fields_Double("Percentage");
									}
								}
								else
								{
									intMultiTown = 1;
								}
							}
							else
							{
								intMultiTown = 1;
							}
						}
						else
						{
							intMultiTown = 1;
						}
						for (intCount = 1; intCount <= intMultiTown; intCount++)
						{
							if (intCount > 1)
							{
								rsMulti.MoveNext();
								strAccountNumber = "E " + rsMulti.Get_Fields("TownNumber") + Strings.Right(strAccountNumber, strAccountNumber.Length - 3);
								dblHours = aryEmployeeMatches[intCounter].Amount * rsMulti.Get_Fields_Double("Percentage");
							}
							strSQL = "Insert INTO tblTempPayProcess (EmpDeductionID,MatchDeductionNumber, EmployeeNumber, EmployeeName,";
							strSQL += "MatchTaxCode, MatchAmount, MatchHitMax, ";
							strSQL += "MatchAccountNumber, MatchRecord, MasterRecord,FICATaxGross,MedicareTaxGross,MatchDeductionTitle,MatchGLAccountNumber) VALUES (";
							strSQL += FCConvert.ToString(aryEmployeeMatches[intCounter].EmpDeductionID) + ",";
							strSQL += FCConvert.ToString(aryEmployeeMatches[intCounter].DeductionID) + ", '";
							strSQL += strTestEmployeeID + "', '";
							strSQL += fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("FirstName"))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields("middlename")))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("LastName"))) + " " + rsMaster.Get_Fields_String("Desig") + "', '";
							strSQL += FCConvert.ToString(aryEmployeeMatches[intCounter].DedTaxCode) + "', '";
							strSQL += Strings.Format(dblHours, "0.00") + "', ";
							strSQL += FCConvert.ToString((aryEmployeeMatches[intCounter].MaxAmount == 0 ? 0 : (aryEmployeeMatches[intCounter].MaxAmount > lngEmployeeMatch ? 1 : 0)));
							strSQL += ", '" + strAccountNumber + "', 1,'" + strMultiCheckCode + "',";
							if (aryEmployeeMatches[intCounter].FICATaxExempt)
							{
								strSQL += "0,";
							}
							else
							{
								strSQL += Strings.Format(dblHours, "0.00") + ",";
							}
							if (aryEmployeeMatches[intCounter].MedTaxExempt)
							{
								strSQL += "0,'";
							}
							else
							{
								strSQL += Strings.Format(dblHours, "0.00") + ",'";
							}
							if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
							{
								modNewAccountBox.Statics.strAccount = aryEmployeeMatches[intCounter].DeductionGLAccount;
								// corey 01/03/2006 Need to format fund
								Strings.MidSet(ref modNewAccountBox.Statics.strAccount, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
								strSQL += modDavesSweetCode.GetAccountDescription(modNewAccountBox.Statics.strAccount) + "','";
								strSQL += modNewAccountBox.Statics.strAccount + "')";
							}
							else
							{
								strSQL += aryEmployeeMatches[intCounter].DeductionDescription + "','";
								strSQL += aryEmployeeMatches[intCounter].DeductionGLAccount + "')";
							}
							strSQL = fecherFoundation.Strings.Trim(strSQL);
							strSQL = Strings.Left(strSQL, strSQL.Length - 1);
							strSQL += ")";
							rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateMatchTempPayProcessRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdateTotalRecordWithDirectDeposit(bool boolInsert = true)
		{
			string strSQL = "";
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblTempPayProcess where EmployeeNumber = '" + strTestEmployeeID + "' and TotalRecord = 1 and MasterRecord = '" + strMultiCheckCode + "'", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				strSQL = "Insert INTO tblTempPayProcess (EmployeeNumber, ";
				strSQL += "NumberChecks, NumberDirectDeposits, NumberBoth, ";
				strSQL += "TotalRecord,FederalTaxGross,StateTaxGross,FICATaxGross,MedicareTaxGross,MasterRecord) VALUES ('";
				strSQL += strTestEmployeeID + "', ";
				strSQL += FCConvert.ToString(intRegularChecks) + ", ";
				strSQL += FCConvert.ToString(intDirectDeposit) + ", ";
				strSQL += FCConvert.ToString(intBothDeposits) + ", '";
				strSQL += Strings.Format(udtEmployee.FedTaxGross, "0.00") + "', '";
				strSQL += Strings.Format(udtEmployee.StateTaxGross, "0.00") + "', '";
				strSQL += Strings.Format(udtEmployee.FICATaxGross, "0.00") + "', '";
				strSQL += Strings.Format(udtEmployee.MedTaxGross, "0.00") + "', 1,'" + strMultiCheckCode + "')";
				strSQL = fecherFoundation.Strings.Trim(strSQL);
				rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
			}
			else
			{
				// update the record that already exists
				rsData.Edit();
				rsData.Set_Fields("NumberChecks", intRegularChecks);
				rsData.Set_Fields("NumberDirectDeposits", intDirectDeposit);
				rsData.Set_Fields("NumberBoth", intBothDeposits);
				rsData.Set_Fields("FederalTaxGross", Strings.Format(udtEmployee.FedTaxGross, "0.00"));
				rsData.Set_Fields("StateTaxGross", Strings.Format(udtEmployee.StateTaxGross, "0.00"));
				rsData.Set_Fields("FICATaxGross", Strings.Format(udtEmployee.FICATaxGross, "0.00"));
				rsData.Set_Fields("MedicareTaxGross", Strings.Format(udtEmployee.MedTaxGross, "0.00"));
				rsData.Update();
			}
		}

		private void CreateTaxTotalTempPayProcessRecord(bool boolInsert = true)
		{
			string strSQL = "";
			clsDRWrapper rsData = new clsDRWrapper();
			if (boolInsert)
			{
				strSQL = "Insert INTO tblTempPayProcess (EmployeeNumber, EmployeeName,";
				strSQL += "FederalTaxWH, StateTaxWH, FICATaxWH,EmployerFicaTax, ";
				strSQL += "MedicareTaxWH,EmployerMedicareTax, GrossPay, TotalRecord, MasterRecord,BasePayRate,MultiFundNumber,fromschool) VALUES ('";
				strSQL += rsMaster.Get_Fields("EmployeeNumber") + "', '";
				strSQL += fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("FirstName"))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields("middlename")))) + " " + FCConvert.ToString(modGlobalRoutines.FixQuote(rsMaster.Get_Fields_String("LastName"))) + " " + rsMaster.Get_Fields_String("Desig") + "', ";
				strSQL += Strings.Format(dblFedTaxTotal, "0.00") + ", ";
				strSQL += Strings.Format(dblStateTaxTotal, "0.00") + ", ";
				strSQL += Strings.Format(dblFICATaxTotal, "0.00") + ", ";
				strSQL += Strings.Format(dblEmployerFicaTaxTotal, "0.00") + ",";
				strSQL += Strings.Format(dblMedTaxTotal, "0.00") + ",";
				strSQL += Strings.Format(dblEmployerMedicareTaxTotal, "0.00") + ",";
				strSQL += Strings.Format(udtEmployee.TotalGross, "0.00") + ",1,'" + strMultiCheckCode + "'," + FCConvert.ToString(dblBasePayRate) + "," + FCConvert.ToString(modGlobalVariables.Statics.gintUseGroupMultiFund) + "," + FCConvert.ToString((boolFromSchool ? 1 : 0)) + ")";
				strSQL = fecherFoundation.Strings.Trim(strSQL);
				strSQL = Strings.Left(strSQL, strSQL.Length - 1);
				strSQL += ")";
				rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
			}
			else
			{
				// update the record that already exists
				strSQL = "Update tblTempPayProcess Set FederalTaxWH = '" + Strings.Format(dblFedTaxTotal, "0.00") + "',";
				strSQL += "StateTaxWH = " + Strings.Format(dblStateTaxTotal, "0.00") + ",";
				strSQL += "FICATaxWH = " + Strings.Format(dblFICATaxTotal, "0.00") + ",";
				strSQL += "EmployerFicaTax = " + Strings.Format(dblEmployerFicaTaxTotal, "0.00") + ",";
				strSQL += "MedicareTaxWH = " + Strings.Format(dblMedTaxTotal, "0.00") + ",";
				strSQL += "EmployerMedicareTax = " + Strings.Format(dblEmployerMedicareTaxTotal, "0.00") + ",";
				strSQL += "GrossPay = " + Strings.Format(udtEmployee.TotalGross, "0.00") + ", ";
				strSQL += "MasterRecord = '" + strMultiCheckCode + "',";
				strSQL += "MultiFundNumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintUseGroupMultiFund) + "";
				strSQL += ",fromschool = " + FCConvert.ToString((boolFromSchool ? 1 : 0));
				strSQL += " Where EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND TotalRecord = 1 AND MasterRecord = '" + strMultiCheckCode + "'";
				// THIS DID NOT HAVE THE TOTAL RECORD BEFORE 7/30/03 MATTHEW
				// strSQL = strSQL & " Where EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "' AND MasterRecord = '" & strMultiCheckCode & "'"
				rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
			}
		}

		private void CreateNetPayTempPayProcessRecord(double dblNetPay)
		{
			string strSQL;
			clsDRWrapper rsData = new clsDRWrapper();
			// update the record that already exists
			strSQL = "Update tblTempPayProcess Set NetPay = " + Strings.Format(dblNetPay, "0.00");
			strSQL += ",PayDate = '" + FCConvert.ToString(CheckProcessSetupInfo.PayDate) + "'";
			strSQL += " Where EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND TotalRecord = 1 AND MasterRecord = '" + strMultiCheckCode + "'";
			rsData.Execute(strSQL, modGlobalVariables.DEFAULTDATABASE);
		}

		public void clearUDTEmployee()
		{
			// clear User Defined Type
			udtEmployee.TotalGross = 0;
			udtEmployee.FedTaxGross = 0;
			udtEmployee.StateTaxGross = 0;
			udtEmployee.FICATaxGross = 0;
			udtEmployee.MedTaxGross = 0;
			udtEmployee.TotalNet = 0;
		}

		public void clearARYDeductions()
		{
			// clear deductions array
			aryDeductions = new PayrollEmployeeDeductions[0 + 1];
			aryDeductions[0].Amount = 0;
			aryDeductions[0].FedTaxExempt = true;
			aryDeductions[0].FICATaxExempt = true;
			aryDeductions[0].MedTaxExempt = true;
			aryDeductions[0].StateTaxExempt = true;
			aryDeductions[0].AmountType = string.Empty;
			aryDeductions[0].PercentType = string.Empty;
			aryDeductions[0].DeductionNumber = 0;
			aryDeductions[0].HitLimit = false;
			aryDeductions[0].TaxStatusCode = 0;
			aryDeductions[0].AdjustmentAmount = 0;
			aryDeductions[0].RowNumber = 0;
			aryDeductions[0].DeductionAmount = 0;
		}

		public void clearARYEmployeeMatches()
		{
			// clear Employer Match array
			aryEmployeeMatches = new PayrollEmployeeMatches[0 + 1];
			aryEmployeeMatches[0].Amount = 0;
			aryEmployeeMatches[0].FedTaxExempt = true;
			aryEmployeeMatches[0].FICATaxExempt = true;
			aryEmployeeMatches[0].MedTaxExempt = true;
			aryEmployeeMatches[0].StateTaxExempt = true;
			aryEmployeeMatches[0].AmountType = string.Empty;
			aryEmployeeMatches[0].MatchNumber = 0;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(double, int)
		public double CalculateGross(string strType = "T")
		{
			double CalculateGross = 0;
			if (Conversion.Val(fecherFoundation.Strings.Trim(rsDistribution.Get_Fields_Double("HoursWeek") + " ")) != 0)
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistribution.Get_Fields("Type")))) == "HOURS")
				{
					// if paycode is not user defined then use value from tblPayCodes table
					if (FCConvert.ToString(rsDistribution.Get_Fields_String("PayCode")) != "0" && FCConvert.ToString(rsDistribution.Get_Fields_String("PayCode")) != "998" && FCConvert.ToString(rsDistribution.Get_Fields_String("PayCode")) != "999")
					{
                        CalculateGross =
                            ((rsDistribution.Get_Fields_Decimal("Rate").ToDouble() * rsDistribution.Get_Fields_Double("Factor")) *
                             rsDistribution.Get_Fields_Double("HoursWeek"));
                    }
					else
					{
                        CalculateGross = ((rsDistribution.Get_Fields_Decimal("BaseRate").ToDouble() * rsDistribution.Get_Fields_Double("Factor")) * rsDistribution.Get_Fields_Double("HoursWeek"));
					}
				}
				else if (rsDistribution.Get_Fields_String("Type").Trim() == "Non-Pay (Hours)")
				{
					CalculateGross = 0;
				}
				else
				{
					// if paycode is not user defined then use value from tblPayCodes table
					if (rsDistribution.Get_Fields_String("PayCode") != "0")
					{
                        CalculateGross = ((rsDistribution.Get_Fields_Decimal("Rate").ToDouble() * rsDistribution.Get_Fields_Double("Factor")) * rsDistribution.Get_Fields_Double("HoursWeek"));
					}
					else
					{
                        CalculateGross = ((rsDistribution.Get_Fields_Decimal("BaseRate").ToDouble() * rsDistribution.Get_Fields_Double("Factor")) * rsDistribution.Get_Fields_Double("HoursWeek"));
					}
				}
				if (Conversion.Val(rsDistribution.Get_Fields("contractid")) > 0)
				{
					if (!(rsContracts.EndOfFile() && rsContracts.BeginningOfFile()))
					{
						// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
						int x;
						for (x = 0; x <= (Information.UBound(aryContractAccounts, 1)); x++)
						{
							if (aryContractAccounts[x].ContractID == FCConvert.ToInt32(rsDistribution.Get_Fields("contractid")) && aryContractAccounts[x].Account == FCConvert.ToString(rsDistribution.Get_Fields("accountnumber")))
							{
								if (aryContractAccounts[x].Paid < aryContractAccounts[x].OriginalAmount)
								{
									if ((aryContractAccounts[x].OriginalAmount - aryContractAccounts[x].Paid) >= CalculateGross)
									{
										aryContractAccounts[x].Paid += CalculateGross;
									}
									else
									{
										CalculateGross = aryContractAccounts[x].OriginalAmount - aryContractAccounts[x].Paid;
										aryContractAccounts[x].Paid = aryContractAccounts[x].OriginalAmount;
									}
								}
								else
								{
									CalculateGross = 0;
								}
								break;
							}
						}
						// x
					}
				}
			}
			else
			{
				CalculateGross = 0;
			}
			return CalculateGross;
		}

		public void GetCurrentDeduction(ref clsDRWrapper rsCurrent, int intArrayIndex, bool boolMatchRec = false)
		{
			// if the deduction uses some of the default values from the Deduction
			// Setup screen then we need to get those values otherwise we need
			// to use the values that we saved in the tblEmployeeDeduction table
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(rsCurrent.Get_Fields_String("AmountType") + " ") == string.Empty)
				{
					CurrentDeductions.AmountType = DefaultDeductions[intArrayIndex].AmountType;
				}
				else
				{
					CurrentDeductions.AmountType = rsCurrent.Get_Fields_String("AmountType");
				}
				CurrentDeductions.AccountNumber = DefaultDeductions[intArrayIndex].AccountNumber;
				if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("Amount") + " ")) == 0)
				{
					CurrentDeductions.Amount = DefaultDeductions[intArrayIndex].Amount;
				}
				else
				{
					CurrentDeductions.Amount = Conversion.Val(rsCurrent.Get_Fields("Amount"));
				}
				if (fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("Description") + " ") == string.Empty)
				{
					CurrentDeductions.Description = DefaultDeductions[intArrayIndex].Description;
				}
				else
				{
					CurrentDeductions.Description = rsCurrent.Get_Fields("Description");
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("FrequencyCode") + " ")) == 0)
				{
					CurrentDeductions.Frequency = DefaultDeductions[intArrayIndex].Frequency;
				}
				else
				{
					if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("FrequencyCode") + "")) == 12)
					{
						// 12 = Default This is the ID in the frequency code master TABLE
						CurrentDeductions.Frequency = DefaultDeductions[intArrayIndex].Frequency;
					}
					else
					{
						CurrentDeductions.Frequency = rsCurrent.Get_Fields("FrequencyCode");
					}
				}
				CurrentDeductions.Status = DefaultDeductions[intArrayIndex].Status;
				if (boolMatchRec)
				{
					// the rest doesn't apply
					if (rsCurrent.Get_Fields("status") == "Inactive")
						CurrentDeductions.Status = "Inactive";
					return;
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields_Double("Limit") + " ")) == 0 && fecherFoundation.Strings.UCase(rsCurrent.Get_Fields_String("AmountType") + "") != "LEVY")
				{
					CurrentDeductions.Limit = DefaultDeductions[intArrayIndex].Limit;
				}
				else
				{
					CurrentDeductions.Limit = rsCurrent.Get_Fields_Double("Limit");
				}
				if (fecherFoundation.Strings.Trim(rsCurrent.Get_Fields_String("LimitType") + " ") == string.Empty)
				{
					CurrentDeductions.LimitType = DefaultDeductions[intArrayIndex].LimitType;
				}
				else
				{
					CurrentDeductions.LimitType = rsCurrent.Get_Fields_String("LimitType");
				}
				if (fecherFoundation.Strings.Trim(rsCurrent.Get_Fields_String("PercentType") + " ") == string.Empty)
				{
					CurrentDeductions.PercentType = DefaultDeductions[intArrayIndex].PercentType;
				}
				else
				{
					CurrentDeductions.PercentType = rsCurrent.Get_Fields_String("PercentType");
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("Priority") + " ")) == 0)
				{
					CurrentDeductions.Priority = DefaultDeductions[intArrayIndex].Priority;
				}
				else
				{
					CurrentDeductions.Priority = rsCurrent.Get_Fields("Priority");
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(rsCurrent.Get_Fields("TaxStatusCode") + " ")) == 0)
				{
					CurrentDeductions.TaxCode = DefaultDeductions[intArrayIndex].TaxCode;
				}
				else
				{
					CurrentDeductions.TaxCode = rsCurrent.Get_Fields("TaxStatusCode");
				}
				CurrentDeductions.UExempt = DefaultDeductions[intArrayIndex].UExempt;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in GetCurrentDeduction", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(int, double, string)
		public double CalculateDeduction(int intCount)
		{
			double CalculateDeduction;
			string strAmountType = "";
			double dblAmount;
			int intArrayIndex;
			double dblTempAmount = 0;
			double dblLimitTotal;
			double dblInfiniteLimit;
			dblInfiniteLimit = 999999.99;
			// then use the information from the deduction setup screen
			intArrayIndex = FCConvert.ToInt16(GetArrayIndex(rsDeductions.Get_Fields("DeductionCode")));
			if (intArrayIndex >= 0)
			{
				if (rsDeductionOverrides.RecordCount() <= 0)
				{
					GetCurrentDeduction(ref rsDeductions, intArrayIndex);
				}
				else
				{
					// If rsDeductionOverrides.FindFirstRecord("OrigID", Val(rsDeductions.Fields("ID"))) Then
					if (rsDeductionOverrides.FindFirst("OrigID = " + FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("ID")))))
					{
						GetCurrentDeduction(ref rsDeductionOverrides, intArrayIndex);
					}
					else
					{
						GetCurrentDeduction(ref rsDeductions, intArrayIndex);
					}
				}
				if (fecherFoundation.Strings.UCase(DefaultDeductions[intArrayIndex].Status) != "ACTIVE")
				{
					aryDeductions[intCount].Apply = false;
					CalculateDeduction = 0;
					return CalculateDeduction;
				}
			}
			if (modGlobalRoutines.ApplyThisEmployeeDeduction(CurrentDeductions.Frequency, strTestEmployeeID, "DED"))
			{
				aryDeductions[intCount].Apply = true;
			}
			else
			{
				aryDeductions[intCount].Apply = false;
				CalculateDeduction = 0;
				return CalculateDeduction;
			}
			if (fecherFoundation.Strings.UCase(CurrentDeductions.AmountType) != "LEVY")
			{
				// matthew call id 70298  6/29/2005
				if (fecherFoundation.Strings.UCase(CurrentDeductions.AmountType) != "DOLLARS")
				{
					// CalculateDeduction = Format((aryDeductions(intCount).DeductionGross * (CurrentDeductions.AMOUNT / 100)), "#0.00")
					if (intDeductionUpTop > 0)
					{
						if (fecherFoundation.Strings.UCase(CurrentDeductions.PercentType) != "NET")
						{
							CalculateDeduction = (aryDeductions[intCount].DeductionGross * (CurrentDeductions.Amount / 100));
						}
						else
						{
							CalculateDeduction = 0;
							aryDeductions[intCount].DeductionAmount = (CurrentDeductions.Amount / 100);
							aryDeductions[intCount].PercentType = "Net";
						}
					}
					else
					{
						CalculateDeduction = 0;
					}
				}
				else
				{
					// the amount type is dollars
					// matthew call id 73416
					// CalculateDeduction = CurrentDeductions.Amount * intDeductionUpTop
					CalculateDeduction = CurrentDeductions.Amount * modGlobalRoutines.Statics.gintDeductionWeeks;
				}
				if (CalculateDeduction > aryDeductions[intCount].DeductionGross)
				{
					CalculateDeduction = aryDeductions[intCount].DeductionGross;
				}
			}
			else
			{
				CalculateDeduction = 0;
			}
			// get the total amount for the limit type
			if (CurrentDeductions.Limit < dblInfiniteLimit && CurrentDeductions.Limit != 0)
			{
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "CALENDAR")
				{
					dblTempAmount = modCoreysSweeterCode.GetCYTDDeductionTotal(aryDeductions[intCount].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryDeductions[intCount].EmpDeductionID);
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "MONTH")
				{
					// 
					dblTempAmount = modCoreysSweeterCode.GetMTDDeductionTotal(aryDeductions[intCount].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryDeductions[intCount].EmpDeductionID);
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "QUARTER")
				{
					dblTempAmount = modCoreysSweeterCode.GetQTDDeductionTotal(aryDeductions[intCount].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryDeductions[intCount].EmpDeductionID);
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "FISCAL")
				{
					dblTempAmount = modCoreysSweeterCode.GetFYTDDeductionTotal(aryDeductions[intCount].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryDeductions[intCount].EmpDeductionID);
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "LIFE")
				{
					dblTempAmount = rsDeductions.Get_Fields_Double("LTDTotal");
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))) == "% GROSS")
				{
					double dblTemp = 0;
					dblTemp = aryDeductions[intCount].DeductionGross * (Conversion.Val(rsDeductions.Get_Fields_Double("Limit")) / 100);
					if (dblTemp < CalculateDeduction)
						CalculateDeduction = dblTemp;
					// CalculateDeduction = CalculateDeduction - aryDeductions(intCount).DeductionGross * (Val(rsDeductions.Fields("Limit")) / 100)
					// If CalculateDeduction < 0 Then CalculateDeduction = 0
				}
				else
				{
					// to handle pay advances we must multiply by the deduction up top
					dblTempAmount = 0;
				}
			}
			else
			{
				dblTempAmount = 0;
			}
			// dblTempAmount = dblTempAmount + aryDeductions(intCount).Amount
			dblTempAmount += aryDeductions[intCount].TotAmount;
			dblLimitTotal = dblTempAmount;
			// MATTHEW IF THE FIRST CHECK GOES TO THE LIMIT THEN THE SECOND CHECK SHOULDN'T
			// TAKE ANY AMOUNTS
			// aryDeductions(intCount).HitLimit = False
			// If dblTempAmount >= CurrentDeductions.Limit Then
			if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsDeductions.Get_Fields("limittype"))) != "% GROSS")
			{
				if (dblTempAmount >= CurrentDeductions.Limit || aryDeductions[intCount].HitLimit)
				{
					aryDeductions[intCount].HitLimit = true;
					CalculateDeduction = 0;
				}
				else
				{
					// add the limit total for this limit type to this deduction amount
					dblTempAmount += CalculateDeduction;
					if (dblTempAmount > CurrentDeductions.Limit && CurrentDeductions.Limit != 999999.99)
					{
						// the amount for the deduction plus the total amount is the bottom grid is
						// greater then the limit for that deduction so do not take anything from this ded.
						CalculateDeduction = CurrentDeductions.Limit - dblLimitTotal;
						aryDeductions[intCount].HitLimit = true;
					}
				}
			}
			CalculateDeduction = FCConvert.ToDouble(Strings.Format(CalculateDeduction, "0.00"));
			aryDeductions[intCount].TotAmount += CalculateDeduction;
			aryDeductions[intCount].Amount += CalculateDeduction;
			aryDeductions[intCount].AmountType = CurrentDeductions.AmountType;
			aryDeductions[intCount].PercentType = CurrentDeductions.PercentType;
			aryDeductions[intCount].Limit = CurrentDeductions.Limit;
			aryDeductions[intCount].DeductionAccountNumber = CurrentDeductions.AccountNumber;
			return CalculateDeduction;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite
		public object GetArrayIndex(int intDeductionCode)
		{
			object GetArrayIndex = null;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// get the current array index in the Default Deduction array
			// for the deduction code that we are working with
			for (intCounter = 0; intCounter <= (Information.UBound(DefaultDeductions, 1) - 1); intCounter++)
			{
				if (DefaultDeductions[intCounter].DeductionCode == intDeductionCode)
				{
					GetArrayIndex = intCounter;
					return GetArrayIndex;
				}
			}
			GetArrayIndex = -1;
			return GetArrayIndex;
		}
		// vbPorter upgrade warning: strTaxStatusCode As string	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As object	OnWrite
		public object GetTaxStatusArrayIndex(string strTaxStatusCode)
		{
			object GetTaxStatusArrayIndex = null;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// get the current array index for the Default Tax Status array
			// for the Tax Status Code that we are working with
			for (intCounter = 0; intCounter <= (Information.UBound(DefaultTaxStatusCodes, 1) - 1); intCounter++)
			{
				if (DefaultTaxStatusCodes[intCounter].ID == FCConvert.ToDouble(strTaxStatusCode))
				{
					GetTaxStatusArrayIndex = intCounter;
					return GetTaxStatusArrayIndex;
				}
			}
			GetTaxStatusArrayIndex = -1;
			return GetTaxStatusArrayIndex;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(int, double)
		public double CalculateEmployeeMatch(int intCount)
		{
			double CalculateEmployeeMatch = 0;
			double dblDeductionTotal = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intArrayIndex;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			double dblMaxHours = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
			double dblTemp = 0;
			double dblMaxAmount = 0;
			double dblTempAmount = 0;
			double dblLimitTotal = 0;
			intArrayIndex = FCConvert.ToInt16(GetArrayIndex(rsEmployeeMatches.Get_Fields("DeductionCode")));
			if (intArrayIndex >= 0)
			{
				if (rsMatchOverrides.RecordCount() <= 0)
				{
					GetCurrentDeduction(ref rsEmployeeMatches, intArrayIndex, true);
				}
				else
				{
					// If rsMatchOverrides.FindFirstRecord("OrigID", Val(rsEmployeeMatches.Fields("ID"))) Then
					if (rsMatchOverrides.FindFirst("OrigID = " + FCConvert.ToString(Conversion.Val(rsEmployeeMatches.Get_Fields("ID")))))
					{
						GetCurrentDeduction(ref rsMatchOverrides, intArrayIndex, true);
					}
					else
					{
						GetCurrentDeduction(ref rsEmployeeMatches, intArrayIndex, true);
					}
				}
				// Call GetCurrentDeduction(rsEmployeeMatches, intArrayIndex, True)
				if (fecherFoundation.Strings.UCase(DefaultDeductions[intArrayIndex].Status) != "ACTIVE" || fecherFoundation.Strings.UCase(CurrentDeductions.Status) == "INACTIVE")
				{
					CalculateEmployeeMatch = 0;
					return CalculateEmployeeMatch;
				}
			}
			// matthew call id 73416 10/13/2005
			// If ApplyThisEmployee(rsEmployeeMatches.Fields("FREQUENCYCODE"), strTestEmployeeID, "MATCH") Then
			// If ApplyThisEmployeeDeduction(rsEmployeeMatches.Fields("FREQUENCYCODE"), strTestEmployeeID, "MATCH") Then
			if (modGlobalRoutines.ApplyThisEmployeeDeduction(CurrentDeductions.Frequency, strTestEmployeeID, "MATCH"))
			{
			}
			else
			{
				CalculateEmployeeMatch = 0;
				return CalculateEmployeeMatch;
			}
			// may need to recalculate max amount
			if (aryEmployeeMatches[intCount].AmountType == "%G" && aryEmployeeMatches[intCounter].DollarsHours == 2)
			{
				// hours
				dblMaxHours = Conversion.Val(aryEmployeeMatches[intCounter].MaxAmount);
				dblMaxAmount = 0;
				for (x = 0; x <= (Information.UBound(aryCalcedDist, 1)); x++)
				{
					if (aryCalcedDist[x].Gross > 0 && aryCalcedDist[x].Hours > 0)
					{
						if (dblMaxHours > aryCalcedDist[x].Hours)
						{
							dblTemp = aryCalcedDist[x].Hours * aryCalcedDist[x].Factor * aryCalcedDist[x].Rate;
							dblMaxHours -= aryCalcedDist[x].Hours;
							dblMaxAmount += dblTemp;
						}
						else
						{
							dblTemp = dblMaxHours * aryCalcedDist[x].Factor * aryCalcedDist[x].Rate;
							dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp, "0.00"));
							dblMaxHours = 0;
							dblMaxAmount += dblTemp;
							break;
						}
					}
				}
				// x
				if (aryEmployeeMatches[intCounter].MatchGross > dblMaxAmount)
				{
					aryEmployeeMatches[intCounter].MatchGross = dblMaxAmount;
				}
			}
			else
			{
				dblMaxAmount = aryEmployeeMatches[intCounter].MaxAmount;
			}
			// WE ALWAYS WANT THE VALUE AS THE CALLING FUNCTION WILL CHECK FOR S(X) CHECKS
			// If UCase(rsEmployeeMatches.Fields("AmountType")) = "D" And intEmployeeCheckCounter = 1 Then 'Dollars
			// If UCase(rsEmployeeMatches.Fields("AmountType")) = "D" Then 'Dollars
			if (fecherFoundation.Strings.UCase(CurrentDeductions.AmountType) == "D")
			{
				// CalculateEmployeeMatch = rsEmployeeMatches.Fields("Amount") * intDeductionUpTop
				// matthew call id 73416
				// CalculateEmployeeMatch = rsEmployeeMatches.Fields("Amount") * gintDeductionWeeks
				CalculateEmployeeMatch = CurrentDeductions.Amount * modGlobalRoutines.Statics.gintDeductionWeeks;
			}
			else
			{
				if (intDeductionUpTop > 0)
				{
					// If UCase(rsEmployeeMatches.Fields("AmountType")) = "%G" Then
					if (fecherFoundation.Strings.UCase(CurrentDeductions.AmountType) == "%G")
					{
						// CalculateEmployeeMatch = (aryEmployeeMatches(intCount).MatchGross * (rsEmployeeMatches.Fields("Amount") / 100))
						CalculateEmployeeMatch = (aryEmployeeMatches[intCount].MatchGross * (CurrentDeductions.Amount / 100));
						// ElseIf UCase(rsEmployeeMatches.Fields("AmountType")) = "%D" Then ' Percent of Deduction
					}
					else if (fecherFoundation.Strings.UCase(CurrentDeductions.AmountType) == "%D")
					{
						// Percent of Deduction
						for (counter = 0; counter <= (Information.UBound(aryDeductions, 1) - 1); counter++)
						{
							if (aryDeductions[counter].DeductionCode == aryEmployeeMatches[intCount].DeductionNumber)
							{
								dblDeductionTotal += aryDeductions[counter].Amount;
							}
						}
						// CalculateEmployeeMatch = (dblDeductionTotal * (rsEmployeeMatches.Fields("Amount") / 100))
						CalculateEmployeeMatch = (dblDeductionTotal * (CurrentDeductions.Amount / 100));
					}
					else
					{
						CalculateEmployeeMatch = 0;
					}
				}
				else
				{
					CalculateEmployeeMatch = 0;
				}
			}
			if (aryEmployeeMatches[intCounter].DollarsHours != FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.Dollars) && aryEmployeeMatches[intCounter].DollarsHours != FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.Hours))
			{
				if (aryEmployeeMatches[intCounter].DollarsHours == FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.MonthToDate))
				{
					dblTempAmount = modCoreysSweeterCode.GetMTDMatchTotal(aryEmployeeMatches[intCounter].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryEmployeeMatches[intCounter].EmpDeductionID);
				}
				else if (aryEmployeeMatches[intCounter].DollarsHours == FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.QuarterToDate))
				{
					dblTempAmount = modCoreysSweeterCode.GetQTDMatchTotal(aryEmployeeMatches[intCounter].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryEmployeeMatches[intCounter].EmpDeductionID);
				}
				else if (aryEmployeeMatches[intCounter].DollarsHours == FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.CalendarToDate))
				{
					dblTempAmount = modCoreysSweeterCode.GetCYTDMatchTotal(aryEmployeeMatches[intCounter].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryEmployeeMatches[intCounter].EmpDeductionID);
				}
				else if (aryEmployeeMatches[intCounter].DollarsHours == FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.FiscalToDate))
				{
					dblTempAmount = modCoreysSweeterCode.GetFYTDMatchTotal(aryEmployeeMatches[intCounter].DeductionID, strTestEmployeeID, modGlobalVariables.Statics.gdatCurrentPayDate, aryEmployeeMatches[intCounter].EmpDeductionID);
				}
				else if (aryEmployeeMatches[intCounter].DollarsHours == FCConvert.ToInt32(modCoreysSweeterCode.MatchLimitType.Life))
				{
					dblTempAmount = Conversion.Val(rsEmployeeMatches.Get_Fields("ltd"));
				}
				else
				{
					dblTempAmount = 0;
				}
				dblTempAmount += aryEmployeeMatches[intCounter].TotAmount;
				dblLimitTotal = dblTempAmount;
				if (dblTempAmount >= dblMaxAmount || aryEmployeeMatches[intCounter].HitLimit)
				{
					aryEmployeeMatches[intCounter].HitLimit = true;
					CalculateEmployeeMatch = 0;
				}
				else
				{
					// add the limit total for this limit type to this deduction amount
					dblTempAmount += CalculateEmployeeMatch;
					if (dblTempAmount > dblMaxAmount && dblMaxAmount != 999999.99)
					{
						// the amount for the deduction plus the total amount is the bottom grid is
						// greater then the limit for that deduction so do not take anything from this ded.
						CalculateEmployeeMatch = dblMaxAmount - dblLimitTotal;
						aryEmployeeMatches[intCounter].HitLimit = true;
					}
				}
			}
			else
			{
				// NOW CHECK THE MAX AMOUNT
				if ((CalculateEmployeeMatch + aryEmployeeMatches[intCount].Amount) > dblMaxAmount)
				{
					// CalculateEmployeeMatch = aryEmployeeMatches(intCount).MaxAmount - CalculateEmployeeMatch
					// MADE A CHANGE HERE FOR THE CASE WHERE THE MAX IS ZERO AND WE WERE GOING TO ZERO
					// MATTHEW 9/14/2004 This will also make it work for multiple checks
					CalculateEmployeeMatch = dblMaxAmount - aryEmployeeMatches[intCount].Amount;
				}
				// aryEmployeeMatches(intCount).Amount = aryEmployeeMatches(intCount).Amount + CalculateEmployeeMatch
			}
			aryEmployeeMatches[intCounter].Amount = aryEmployeeMatches[intCount].Amount + CalculateEmployeeMatch;
			aryEmployeeMatches[intCounter].TotAmount += CalculateEmployeeMatch;
			// aryDeductions(intCount).Limit = CurrentDeductions.Limit
			return CalculateEmployeeMatch;
		}

		private void LoadDeductionSetupInfo()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsDEDSetup = new clsDRWrapper();
			rsDEDSetup.OpenRecordset("Select * from tblDeductionSetup", "Twpy0000.vb1");
			if (!rsDEDSetup.EndOfFile())
			{
				DefaultDeductions = new DEDUCTIONSETUP[rsDEDSetup.RecordCount() + 1];
				for (intCounter = 0; intCounter <= (rsDEDSetup.RecordCount() - 1); intCounter++)
				{
					DefaultDeductions[intCounter].Amount = Conversion.Val(rsDEDSetup.Get_Fields("Amount"));
					DefaultDeductions[intCounter].AmountType = FCConvert.ToString(rsDEDSetup.Get_Fields_String("AmountType"));
					DefaultDeductions[intCounter].Description = FCConvert.ToString(rsDEDSetup.Get_Fields("Description"));
					DefaultDeductions[intCounter].AccountNumber = FCConvert.ToString(rsDEDSetup.Get_Fields_String("AccountID"));
					DefaultDeductions[intCounter].Frequency = FCConvert.ToInt16(rsDEDSetup.Get_Fields("FrequencyCode"));
					DefaultDeductions[intCounter].Limit = rsDEDSetup.Get_Fields_Double("Limit");
					DefaultDeductions[intCounter].LimitType = FCConvert.ToString(rsDEDSetup.Get_Fields_String("LimitType"));
					DefaultDeductions[intCounter].PercentType = FCConvert.ToString(rsDEDSetup.Get_Fields_String("Percent"));
					DefaultDeductions[intCounter].Priority = FCConvert.ToInt16(rsDEDSetup.Get_Fields("Priority"));
					DefaultDeductions[intCounter].Status = FCConvert.ToString(rsDEDSetup.Get_Fields("status"));
					// CHANGED THIS BECASUE WE ARE NO LONGER SAVING THE TAX CODE IN THE MATCH TABLE MATTHEW 12/18/03
					DefaultDeductions[intCounter].TaxCode = FCConvert.ToInt32(rsDEDSetup.Get_Fields("TaxStatusCode"));
					DefaultDeductions[intCounter].DeductionCode = FCConvert.ToInt16(rsDEDSetup.Get_Fields("ID"));
					DefaultDeductions[intCounter].UExempt = FCConvert.ToBoolean(rsDEDSetup.Get_Fields_Boolean("uexempt"));
					rsDEDSetup.MoveNext();
				}
			}
		}

		private void LoadDefaultTaxStatusCodes()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsDEDSetup = new clsDRWrapper();
			rsDEDSetup.OpenRecordset("Select * from tblTaxStatusCodes", "Twpy0000.vb1");
			if (!rsDEDSetup.EndOfFile())
			{
				DefaultTaxStatusCodes = new TAXSTATUSCODES[rsDEDSetup.RecordCount() + 1];
				for (intCounter = 0; intCounter <= (rsDEDSetup.RecordCount() - 1); intCounter++)
				{
					DefaultTaxStatusCodes[intCounter].ID = FCConvert.ToInt32(rsDEDSetup.Get_Fields("ID"));
					DefaultTaxStatusCodes[intCounter].TaxStatusCode = FCConvert.ToString(rsDEDSetup.Get_Fields("TaxStatusCode"));
					DefaultTaxStatusCodes[intCounter].Description = FCConvert.ToString(rsDEDSetup.Get_Fields("Description"));
					DefaultTaxStatusCodes[intCounter].FedTaxExempt = FCConvert.ToBoolean(rsDEDSetup.Get_Fields_Boolean("FedExempt"));
					DefaultTaxStatusCodes[intCounter].FICATaxExempt = FCConvert.ToBoolean(rsDEDSetup.Get_Fields_Boolean("FICAExempt"));
					DefaultTaxStatusCodes[intCounter].MedTaxExempt = FCConvert.ToBoolean(rsDEDSetup.Get_Fields_Boolean("MedicareExempt"));
					DefaultTaxStatusCodes[intCounter].StateTaxExempt = FCConvert.ToBoolean(rsDEDSetup.Get_Fields_Boolean("StateExempt"));
					rsDEDSetup.MoveNext();
				}
			}
		}

		private void cmdConfirmation_Click(object sender, System.EventArgs e)
		{
			frmConfirmation.InstancePtr.Show();
		}

		private void frmProcessDisplay_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			//App.DoEvents();
			if (!boolloaded)
			{
				boolloaded = true;
				//FC:FINAL:DDU:#i2505 - add expand button on grid
				vsGrid.AddExpandButton();
                //FC:FINAl:SBE - #i2493 - runt task asynchronous
                FCUtils.StartTask(this, () =>
                {
                    RunCalc();
                    if (boolErrorInDistributions)
                    {
                        modGlobalVariables.Statics.pboolUnloadProcessGrid = true;
                    }
                    if (modGlobalVariables.Statics.pboolUnloadProcessGrid)
                    {
                        Close();
                        // pboolUnloadProcessGrid = False
                    }
                    FCUtils.UnlockUserInterface();
                });
            }
		}

		private void frmProcessDisplay_Load(object sender, System.EventArgs e)
		{
			
			if (boolloaded)
				return;
			boolEncumberContracts = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("select encumbercontracts from tbldefaultinformation", "twpy0000.vb1");
			if (!rsTemp.EndOfFile())
			{
				boolEncumberContracts = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("encumbercontracts"));
			}
			modGlobalVariables.Statics.pboolUnloadProcessGrid = false;
			SetGridProperties();
			modGlobalFunctions.SetFixedSize(this, 0);
			
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			modGlobalVariables.Statics.pboolUnloadProcessGrid = true;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobalVariables.Statics.pboolUnloadProcessGrid = true;
		}

		private void mnuDetailReport_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrPrintProcessDetailReport = "TRUE";
			modGlobalVariables.Statics.gboolCancelSelected = true;
			frmPrintPayrollProcess.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (!modGlobalVariables.Statics.gboolCancelSelected)
			{
				if (fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gstrPrintProcessDetailReport) == string.Empty)
				{
				}
				else
				{
					rptPrintProcessDetail.InstancePtr.Init(this.Modal);
					// frmReportViewer.Init rptPrintProcessDetail, , 1
					modGlobalVariables.Statics.gstrPrintProcessDetailReport = string.Empty;
				}
			}
		}

		private void mnuDistributionListing_Click(object sender, System.EventArgs e)
		{
			string strOrderBy = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strFind;
			strFind = "";
			int lngID;
			lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			rsTemp.OpenRecordset("select * from payrollpermissions where UserID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
			while (!rsTemp.EndOfFile())
			{
				switch (rsTemp.Get_Fields_Int32("type"))
				{
					case CNSTPYTYPEDEPTDIV:
						{
							strFind += "not deptdiv = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEGROUP:
						{
							strFind += "not groupid = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEIND:
						{
							strFind += "not employeenumber = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPESEQ:
						{
							strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
							break;
						}
					case CNSTPYTYPEDEPARTMENT:
						{
							strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
							break;
						}
				}
				//end switch
				rsTemp.MoveNext();
			}
			rsTemp.OpenRecordset("select reportsequence from tblDefaultInformation", "twpy0000.vb1");
			switch (rsTemp.Get_Fields_Int32("reportsequence"))
			{
				case 0:
					{
						// name
						strOrderBy = "ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblTempPayProcess.DistPayCategory";
						modGlobalVariables.Statics.gintDistributionLisitingField = 0;
						break;
					}
				case 1:
					{
						// employee number
						strOrderBy = "ORDER BY tblTempPayProcess.employeenumber, tblTempPayProcess.DistPayCategory ";
						modGlobalVariables.Statics.gintDistributionLisitingField = 1;
						break;
					}
				case 2:
					{
						// sequence
						strOrderBy = "ORDER BY tblEmployeeMaster.SeqNumber, tblTempPayProcess.DistPayCategory ";
						modGlobalVariables.Statics.gintDistributionLisitingField = 2;
						break;
					}
				case 3:
					{
						// dept div
						strOrderBy = "ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename,tblemployeemaster.desig, tblTempPayProcess.DistPayCategory ";
						modGlobalVariables.Statics.gintDistributionLisitingField = 3;
						break;
					}
			}
			//end switch
			// gstrCheckListingSQL = "SELECT DISTINCT tblTempPayProcess.*, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where (((tblTempPayProcess.DistributionRecord) = 1)) ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblTempPayProcess.PayDate, tblTempPayProcess.DistPayCategory"
			modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT DISTINCT tblTempPayProcess.*, tblEmployeeMaster.SeqNumber, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName,tblemployeemaster.middlename,tblemployeemaster.desig FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where " + strFind + " (((tblTempPayProcess.DistributionRecord) = 1)) " + strOrderBy;
			modGlobalVariables.Statics.gstrCheckListingSort = "- - Employee Name - -";
			// rptTempCheckListingDistribution.Show, MDIParent
			rptTempCheckListingDistribution.InstancePtr.Init(this.Modal);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrPrintProcessDetailReport = string.Empty;
			// frmPrintPayrollProcess.Show vbModal, MDIParent
			frmPrintPayrollProcess.InstancePtr.Init(false);
			// Unload frmPrintPayrollProcess
		}

		private void mnuPrintDistributionbyPayCategory_Click(object sender, System.EventArgs e)
		{
			string strOrderBy;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strFind;
			strFind = "";
			int lngID;
			lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			rsTemp.OpenRecordset("select * from payrollpermissions where UserID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
			while (!rsTemp.EndOfFile())
			{
				switch (rsTemp.Get_Fields_Int32("type"))
				{
					case CNSTPYTYPEDEPTDIV:
						{
							strFind += "not deptdiv = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEGROUP:
						{
							strFind += "not groupid = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPEIND:
						{
							strFind += "not employeenumber = '" + rsTemp.Get_Fields("val") + "' and ";
							break;
						}
					case CNSTPYTYPESEQ:
						{
							strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
							break;
						}
					case CNSTPYTYPEDEPARTMENT:
						{
							strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("val"))) + " and ";
							break;
						}
				}
				//end switch
				rsTemp.MoveNext();
			}
			strOrderBy = "ORDER BY tblTempPayProcess.DistPayCategory, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
			modGlobalVariables.Statics.gintDistributionLisitingField = 4;
			modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT DISTINCT tblTempPayProcess.*, tblEmployeeMaster.SeqNumber, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblTempPayProcess INNER JOIN tblEmployeeMaster ON tblTempPayProcess.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Where " + strFind + " (((tblTempPayProcess.DistributionRecord) = 1)) " + strOrderBy;
			modGlobalVariables.Statics.gstrCheckListingSort = "- - Employee Name - -";
			rptTempCheckListingDistribution.InstancePtr.Init(this.Modal);
		}

		public void Init()
		{
			boolErrorInDistributions = false;
			modGlobalVariables.Statics.pboolUnloadProcessGrid = false;
			boolloaded = false;
			lblStatus.Text = lblStatus.Text;
			//App.DoEvents();
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void RunCalc()
		{
			lblStatus.Text = lblStatus.Text;
			//App.DoEvents();
			Command1_Click();
		}

		private void mnuPrintZeroNet_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrPrintProcessDetailReport = string.Empty;
			// frmPrintPayrollProcess.Show vbModal, MDIParent
			frmPrintPayrollProcess.InstancePtr.Init(true);
		}

		public void mnuProcess_Click(object sender, System.EventArgs e)
		{
			RunCalc();
			if (boolErrorInDistributions)
				Close();
		}

		private void frmProcessDisplay_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void SetGridProperties()
		{
			vsGrid.Cols = 13;
			vsGrid.Rows = 1;
			vsGrid.ColHidden(12, true);
            // SET THE HEADINGS FOR THE GRID
            //FC:FINAL:AM:#2716 - adjust column widths
            vsGrid.ColWidth(0, 200);
            vsGrid.ColWidth(1, 3500);
            vsGrid.ColWidth(2, 1400);
			vsGrid.ColWidth(3, 1900);
			vsGrid.ColWidth(4, 3000);
			vsGrid.ColWidth(5, 1900);
			vsGrid.ColWidth(6, 1900);
			vsGrid.ColWidth(7, 1900);
			vsGrid.ColWidth(8, 1900);
			vsGrid.ColWidth(9, 1600);
			vsGrid.ColWidth(10, 400);
			vsGrid.ColWidth(11, 400);
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetPayFreqDescription(int lngID)
		{
			object GetPayFreqDescription = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblCodeTypes where ID = " + FCConvert.ToString(lngID), modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				GetPayFreqDescription = string.Empty;
			}
			else
			{
				GetPayFreqDescription = GetPayFreqDescription + rsData.Get_Fields("Description");
			}
			return GetPayFreqDescription;
		}

		private void UpdateProgressBar()
		{
			if (this.pbrStatus.Value + 1 > this.pbrStatus.Maximum)
			{
				this.pbrStatus.Value = 0;
			}
			else
			{
				this.pbrStatus.Value = this.pbrStatus.Value + 1;
			}
            this.Refresh();
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite
		//private string GetMultiFundNumber(string strDeptDiv)
		//{
		//	string GetMultiFundNumber = "";
		//	clsDRWrapper rsData = new clsDRWrapper();
		//	rsData.OpenRecordset("Select * from DeptDivTitles where Department = '" + Strings.Left(strDeptDiv, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "'", "TWBD0000.vb1");
		//	if (rsData.EndOfFile())
		//	{
		//		GetMultiFundNumber = FCConvert.ToString(0);
		//	}
		//	else
		//	{
		//		GetMultiFundNumber = FCConvert.ToString(rsData.Get_Fields("Fund"));
		//	}
		//	return GetMultiFundNumber;
		//}

		private void SetupPayCatArray()
		{
			// get all pay categories
			int intCount = 0;
			clsDRWrapper rsPayCat = new clsDRWrapper();
			rsPayCat.OpenRecordset("SELECT * FROM tblPayCategories", "TWPY0000.vb1");
			if (!rsPayCat.EndOfFile())
			{
				rsPayCat.MoveLast();
				rsPayCat.MoveFirst();
				aryPayCategories = new PayCategorysType[rsPayCat.RecordCount() - 1 + 1];
				while (!rsPayCat.EndOfFile())
				{
					// changed for jay on 12/16/03 the code that uses this needs the autonumber
					// aryPayCategories(intCount).CategoryNumber = rsPayCategory.Fields("CategoryNumber")
					aryPayCategories[intCount].CategoryNumber = FCConvert.ToInt32(rsPayCat.Get_Fields("ID"));
					aryPayCategories[intCount].ExemptCode1 = FCConvert.ToInt32(rsPayCat.Get_Fields("ExemptCode1"));
					aryPayCategories[intCount].ExemptType1 = FCConvert.ToString(rsPayCat.Get_Fields_String("ExemptType1"));
					aryPayCategories[intCount].ExemptCode2 = FCConvert.ToInt32(rsPayCat.Get_Fields("ExemptCode2"));
					aryPayCategories[intCount].ExemptType2 = FCConvert.ToString(rsPayCat.Get_Fields_String("ExemptType2"));
					aryPayCategories[intCount].ExemptCode3 = FCConvert.ToInt32(rsPayCat.Get_Fields("ExemptCode3"));
					aryPayCategories[intCount].ExemptType3 = FCConvert.ToString(rsPayCat.Get_Fields_String("ExemptType3"));
					aryPayCategories[intCount].ExemptCode4 = FCConvert.ToInt32(rsPayCat.Get_Fields("ExemptCode4"));
					aryPayCategories[intCount].ExemptType4 = FCConvert.ToString(rsPayCat.Get_Fields_String("ExemptType4"));
					aryPayCategories[intCount].ExemptCode5 = FCConvert.ToInt32(rsPayCat.Get_Fields("ExemptCode5"));
					aryPayCategories[intCount].ExemptType5 = FCConvert.ToString(rsPayCat.Get_Fields_String("ExemptType5"));
					intCount += 1;
					rsPayCat.MoveNext();
					//App.DoEvents();
				}
			}
		}

		private bool IsSingle(int intMarriedStatus)
		{
            var statusCode = mainePayStatuses.FirstOrDefault(s => s.Id == intMarriedStatus);
            if (statusCode.UseTable.ToLower() == "married")
            {
                return false;
            }

            if (statusCode.UseTable.ToLower() == "married 2-income")
            {
                return false;
            }

            return true;
        }

        private int GetStateStatusCodeFromID(int statusId)
        {
            return mainePayStatuses.Where(s => s.Id == statusId).Select(s => s.StatusCode).FirstOrDefault().ToIntegerValue();
        }
	}
}
