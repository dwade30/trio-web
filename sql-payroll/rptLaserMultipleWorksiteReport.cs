//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptLaserMultipleWorksiteReport.
	/// </summary>
	public partial class rptLaserMultipleWorksiteReport : BaseSectionReport
	{
		public rptLaserMultipleWorksiteReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLaserMultipleWorksiteReport InstancePtr
		{
			get
			{
				return (rptLaserMultipleWorksiteReport)Sys.GetInstance(typeof(rptLaserMultipleWorksiteReport));
			}
		}

		protected rptLaserMultipleWorksiteReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLaserMultipleWorksiteReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intQuarterUsed;
		int lngYearUsed;
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolSemiWeekly;
		double dblDepositsMade;
		string strSequence = "";

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strTemp = "";
                // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
                DateTime dtStart = DateTime.FromOADate(0);
                DateTime dtEnd = DateTime.FromOADate(0);
                string strSQL = "";
                int lngLine1 = 0;
                double dblLine2 = 0;
                double dblLine3 = 0;
                double dblLine4 = 0;
                double dblLine5 = 0;
                double dblLine6a = 0;
                double dblLine6b = 0;
                double dblLine6c = 0;
                double dblLine6d = 0;
                double dblLine7a = 0;
                double dblLine7b = 0;
                double dblLine8 = 0;
                // vbPorter upgrade warning: dblLine9 As double	OnWrite(string)
                double dblLine9 = 0;
                double dblLine10 = 0;
                double dblLine11 = 0;
                double dblLine12 = 0;
                double dblLine13 = 0;
                double dblLine14 = 0;
                double dblLine15 = 0;
                double dblLine17a = 0;
                double dblLine17b = 0;
                double dblLine17c = 0;
                int intMonth = 0;
                string[] strAry = null;
                string strWhere;
                string strSQL2 = "";
                // vbPorter upgrade warning: dtMarchStart As DateTime	OnWrite(string)
                DateTime dtMarchStart = DateTime.FromOADate(0);
                // vbPorter upgrade warning: dtMarchEnd As DateTime	OnWrite(string)
                DateTime dtMarchEnd = DateTime.FromOADate(0);
                double dblSumTaxWH = 0;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                txtName.Text = EWRRecord.EmployerName;
                txtAddress.Text = EWRRecord.EmployerAddress;
                strTemp = fecherFoundation.Strings.Trim(EWRRecord.EmployerCity + " " + EWRRecord.EmployerState + " " +
                                                        EWRRecord.EmployerZip + " " + EWRRecord.EmployerZip4);
                txtCityStateZip.Text = strTemp;
                txtEmployerID.Text = EWRRecord.FederalEmployerID;
                modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStart, ref dtEnd, intQuarterUsed, lngYearUsed);
                txtDateQuarterEnded.Text = Strings.Format(dtEnd, "MM/dd/yyyy");
                txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                if (intQuarterUsed == 1)
                {
                    dtMarchStart =
                        FCConvert.ToDateTime(Strings.Format(frmPrint941.InstancePtr.txtStartDate[0].Text,
                            "MM/dd/yyyy"));
                    dtMarchEnd =
                        FCConvert.ToDateTime(Strings.Format(frmPrint941.InstancePtr.txtEndDate[0].Text, "MM/dd/yyyy"));
                }

                strWhere = "";
                // now look at data to get the rest
                if (strSequence == string.Empty || strSequence == "-1")
                {
                    strSQL =
                        "SELECT Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail  Where ( checkvoid = 0) and  PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1";
                    if (intQuarterUsed == 1)
                    {
                        strSQL2 =
                            "select count(employeenumber) as numemployees from (select employeenumber from tblcheckdetail where (checkvoid = 0) and  PayDate >= '" +
                            FCConvert.ToString(dtMarchStart) + "' And PayDate <= '" + FCConvert.ToString(dtMarchEnd) +
                            "' And TotalRecord = 1 group by employeenumber)";
                    }
                }
                else
                {
                    strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
                    if (Information.UBound(strAry, 1) > 0)
                    {
                        strWhere = " seqnumber between " + strAry[0] + " and " + strAry[1];
                    }
                    else
                    {
                        strWhere = " seqnumber = " + strAry[0];
                    }

                    strSQL =
                        "SELECT Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where (checkvoid = 0) and PayDate >= '" +
                        FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) +
                        "' And TotalRecord = 1 and " + strWhere;
                    if (intQuarterUsed == 1)
                    {
                        strSQL2 =
                            "select count(tblcheckdetail.employeenumber) as numemployees from (select tblcheckdetail.employeenumber from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where (checkvoid = 0) PayDate >= '" +
                            FCConvert.ToString(dtMarchStart) + "' And PayDate <= '" + FCConvert.ToString(dtMarchEnd) +
                            "' And TotalRecord = 1 and " + strWhere + " group by tblcheckdetail.employeenumber)";
                    }
                }

                if (intQuarterUsed == 1)
                {
                    clsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        lngLine1 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("numemployees"))));
                    }
                }

                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    dblLine2 = Conversion.Val(clsLoad.Get_Fields("sumofgrosspay"));
                    dblLine3 = Conversion.Val(clsLoad.Get_Fields("sumoffederaltaxwh"));
                    dblLine4 = 0;
                    dblLine5 = dblLine3 + dblLine4;
                    dblLine6a = Conversion.Val(clsLoad.Get_Fields("SumOfFicaTaxGross"));
                    dblLine6b = Conversion.Val(Strings.Format(dblLine6a * 0.124, "0.00"));
                    dblLine6c = 0;
                    // tips
                    dblLine6d = Conversion.Val(Strings.Format(dblLine6c * 0.124, "0.00"));
                    dblLine7a = Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxgross"));
                    dblLine7b = Conversion.Val(Strings.Format(dblLine7a * 0.029, "0.00"));
                    dblLine8 = Conversion.Val(Strings.Format(dblLine7b + dblLine6b + dblLine6d, "0.00"));
                    dblSumTaxWH = Conversion.Val(clsLoad.Get_Fields("sumofficataxwh")) +
                                  Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxwh"));
                    dblLine9 = FCConvert.ToDouble(Strings.Format((dblSumTaxWH * 2) - dblLine8, "0.00"));
                    // adjustments, multiply by 2 to include employermatch
                    dblLine10 = dblLine8 + dblLine9;
                    dblLine11 = Conversion.Val(Strings.Format(dblLine10 + dblLine5, "0.00"));
                    dblLine12 = 0;
                    // advanced EIC payments
                    dblLine13 = Conversion.Val(Strings.Format(dblLine11 - dblLine12, "0.00"));
                    dblLine14 = dblDepositsMade;
                    if (dblLine14 > dblLine13)
                    {
                        dblLine15 = Conversion.Val(Strings.Format(dblLine14 - dblLine13, "0.00"));
                    }
                    else
                    {
                        dblLine15 = Conversion.Val(Strings.Format(dblLine13 - dblLine14, "0.00"));
                    }

                    if (intQuarterUsed == 1)
                    {
                        txtLine1.Text = Strings.Format(lngLine1, "#,##0");
                    }
                    else
                    {
                        txtLine1.Text = "";
                    }

                    txtLine2.Text = Strings.Format(dblLine2, "#,###,###,##0.00");
                    txtLine3.Text = Strings.Format(dblLine3, "#,###,###,##0.00");
                    txtLine4.Text = Strings.Format(dblLine4, "#,###,###,##0.00");
                    txtLine5.Text = Strings.Format(dblLine5, "#,###,###,##0.00");
                    txtLine6a.Text = Strings.Format(dblLine6a, "#,###,##0.00");
                    txtLine6b.Text = Strings.Format(dblLine6b, "#,###,##0.00");
                    txtLine6c.Text = Strings.Format(dblLine6c, "#,###,##0.00");
                    txtLine6d.Text = Strings.Format(dblLine6d, "#,###,##0.00");
                    txtLine7a.Text = Strings.Format(dblLine7a, "#,###,##0.00");
                    txtLine7b.Text = Strings.Format(dblLine7b, "#,###,##0.00");
                    txtLine8.Text = Strings.Format(dblLine8, "#,###,##0.00");
                    txtLine9.Text = Strings.Format(dblLine9, "#,###,##0.00");
                    txtLine10.Text = Strings.Format(dblLine10, "#,###,##0.00");
                    txtLine11.Text = Strings.Format(dblLine11, "#,###,##0.00");
                    txtLine12.Text = Strings.Format(dblLine12, "#,###,##0.00");
                    txtLine13.Text = Strings.Format(dblLine13, "#,###,##0.00");
                    txtLine14.Text = Strings.Format(dblLine14, "#,###,##0.00");
                    if (dblLine14 > dblLine13)
                    {
                        txtLine15.Text = "";
                        txtLine16.Text = Strings.Format(dblLine15, "#,###,##0.00");
                        if (MessageBox.Show(
                            "Do you want the overpayment to be refunded?" + "\r\n" +
                            "Responding no will apply it to the next return.", "Overpayment",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            txtLine16Refund.Text = "X";
                            txtLine16Return.Text = "";
                        }
                        else
                        {
                            txtLine16Refund.Text = "";
                            txtLine16Return.Text = "X";
                        }
                    }
                    else if (dblLine13 > dblLine14)
                    {
                        txtLine15.Text = Strings.Format(dblLine15, "#,###,##0.00");
                        txtLine16.Text = "";
                    }
                    else
                    {
                        txtLine16.Text = "";
                        txtLine15.Text = "0.00";
                    }

                    if (dblLine13 >= 2500)
                    {
                        if (!boolSemiWeekly)
                        {
                            txtCheckMonthly.Text = "X";
                            txtCheckSemiWeekly.Text = "";
                            intMonth = (intQuarterUsed * 3) - 2;
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17a = modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere);
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth + 1) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17b = modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere);
                            dtStart = FCConvert.ToDateTime(FCConvert.ToString(intMonth + 2) + "/1/" +
                                                           FCConvert.ToString(lngYearUsed));
                            dtEnd = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStart));
                            dblLine17c = modCoreysSweeterCode.GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere);
                            txtLine17a.Text = Strings.Format(dblLine17a, "#,###,##0.00");
                            txtLine17b.Text = Strings.Format(dblLine17b, "#,###,##0.00");
                            txtLine17c.Text = Strings.Format(dblLine17c, "#,###,##0.00");
                            txtLine17d.Text = Strings.Format(dblLine17a + dblLine17b + dblLine17c, "#,###,###,##0.00");
                        }
                        else
                        {
                            txtCheckMonthly.Text = "";
                            txtCheckSemiWeekly.Text = "X";
                            // will have to fill out schedule B
                            SubReport1.Report = new srpt941ScheduleB();
                            if (strWhere != string.Empty)
                            {
                                SubReport1.Report.UserData =
                                    FCConvert.ToString(intQuarterUsed) + "," + FCConvert.ToString(lngYearUsed) + "," +
                                    strWhere;
                            }
                            else
                            {
                                SubReport1.Report.UserData =
                                    FCConvert.ToString(intQuarterUsed) + "," + FCConvert.ToString(lngYearUsed);
                            }
                        }
                    }
                    else
                    {
                        // dont fill out anything if less than 2500
                        txtCheckMonthly.Text = "";
                        txtCheckSemiWeekly.Text = "";
                    }
                }
            }
        }

		public void Init(ref int intQuarter, ref int lngYear, ref bool boolUseSemiWeekly, ref double dblDeposits, ref string strSeq)
		{
			strSequence = strSeq;
			intQuarterUsed = intQuarter;
			lngYearUsed = lngYear;
			boolSemiWeekly = boolUseSemiWeekly;
			dblDepositsMade = dblDeposits;
			LoadInfo();
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName);
				// Me.PrintReport False
			}
			else
			{
				// Me.Show vbModal, MDIParent
				frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "MultipleWorsites");
			}
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

		
	}
}
