﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1EmployeeRecord.
	/// </summary>
	partial class srptC1EmployeeRecord
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1EmployeeRecord));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label4,
				this.txtSSN,
				this.txtUGross,
				this.txtExcess,
				this.Label5,
				this.txtLast,
				this.Label6,
				this.txtFirst,
				this.txtTaxable,
				this.txtStateGross,
				this.txtStateTax,
				this.Label10,
				this.txtSex,
				this.Label11,
				this.txtSeasonal
			});
			this.Detail.Height = 0.6979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "SSN";
			this.Label4.Top = 0.34375F;
			this.Label4.Width = 0.53125F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1666667F;
			this.txtSSN.Left = 0.6979167F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.34375F;
			this.txtSSN.Width = 1.489583F;
			// 
			// txtUGross
			// 
			this.txtUGross.Height = 0.1666667F;
			this.txtUGross.Left = 3.135417F;
			this.txtUGross.Name = "txtUGross";
			this.txtUGross.Style = "text-align: right";
			this.txtUGross.Text = null;
			this.txtUGross.Top = 0.01041667F;
			this.txtUGross.Width = 0.8020833F;
			// 
			// txtExcess
			// 
			this.txtExcess.Height = 0.1666667F;
			this.txtExcess.Left = 4.010417F;
			this.txtExcess.Name = "txtExcess";
			this.txtExcess.Style = "text-align: right";
			this.txtExcess.Text = null;
			this.txtExcess.Top = 0.01041667F;
			this.txtExcess.Width = 0.8020833F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Last";
			this.Label5.Top = 0.01041667F;
			this.Label5.Width = 0.53125F;
			// 
			// txtLast
			// 
			this.txtLast.Height = 0.1666667F;
			this.txtLast.Left = 0.6979167F;
			this.txtLast.Name = "txtLast";
			this.txtLast.Text = null;
			this.txtLast.Top = 0.01041667F;
			this.txtLast.Width = 2.364583F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "First";
			this.Label6.Top = 0.1770833F;
			this.Label6.Width = 0.46875F;
			// 
			// txtFirst
			// 
			this.txtFirst.Height = 0.1666667F;
			this.txtFirst.Left = 0.6979167F;
			this.txtFirst.Name = "txtFirst";
			this.txtFirst.Text = null;
			this.txtFirst.Top = 0.1770833F;
			this.txtFirst.Width = 2.364583F;
			// 
			// txtTaxable
			// 
			this.txtTaxable.Height = 0.1666667F;
			this.txtTaxable.Left = 4.885417F;
			this.txtTaxable.Name = "txtTaxable";
			this.txtTaxable.Style = "text-align: right";
			this.txtTaxable.Text = null;
			this.txtTaxable.Top = 0.01041667F;
			this.txtTaxable.Width = 0.8020833F;
			// 
			// txtStateGross
			// 
			this.txtStateGross.Height = 0.1666667F;
			this.txtStateGross.Left = 5.760417F;
			this.txtStateGross.Name = "txtStateGross";
			this.txtStateGross.Style = "text-align: right";
			this.txtStateGross.Text = null;
			this.txtStateGross.Top = 0.01041667F;
			this.txtStateGross.Width = 0.8020833F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1666667F;
			this.txtStateTax.Left = 6.635417F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 0.01041667F;
			this.txtStateTax.Width = 0.8020833F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Sex";
			this.Label10.Top = 0.5F;
			this.Label10.Width = 0.59375F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1666667F;
			this.txtSex.Left = 0.6979167F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Style = "text-align: left";
			this.txtSex.Text = null;
			this.txtSex.Top = 0.5F;
			this.txtSex.Width = 0.3645833F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.1875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "Seasonal";
			this.Label11.Top = 0.5F;
			this.Label11.Width = 0.71875F;
			// 
			// txtSeasonal
			// 
			this.txtSeasonal.Height = 0.1666667F;
			this.txtSeasonal.Left = 1.947917F;
			this.txtSeasonal.Name = "txtSeasonal";
			this.txtSeasonal.Style = "text-align: left";
			this.txtSeasonal.Text = null;
			this.txtSeasonal.Top = 0.5F;
			this.txtSeasonal.Width = 0.3645833F;
			// 
			// srptC1EmployeeRecord
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcess;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeasonal;
	}
}
