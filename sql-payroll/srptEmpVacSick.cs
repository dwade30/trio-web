//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmpVacSick.
	/// </summary>
	public partial class srptEmpVacSick : FCSectionReport
	{
		public srptEmpVacSick()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptEmpVacSick InstancePtr
		{
			get
			{
				return (srptEmpVacSick)Sys.GetInstance(typeof(srptEmpVacSick));
			}
		}

		protected srptEmpVacSick _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptEmpVacSick	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsData = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// MATTHEW 1/20/2005
			// Call rsData.OpenRecordset("select * from tblcodes inner join (tblvacationsick inner join tblcodetypes on (tblcodetypes.ID = tblvacationsick.typeid)) on (tblvacationsick.codeid = tblcodes.ID) where tblvacationsick.employeenumber = '" & Me.Tag & "' order by tblvacationsick.typeid", "twpy0000.vb1")
			rsData.OpenRecordset("SELECT tblcodetypes.Description, tblcodes.Description, tblvacationsick.*, * FROM (tblvacationsick INNER JOIN tblcodetypes ON tblvacationsick.TypeID = tblcodetypes.ID) LEFT JOIN tblcodes ON tblvacationsick.CodeID = tblcodes.ID WHERE (((tblvacationsick.EmployeeNumber)='" + this.UserData + "') and tblvacationsick.selected = 1)");
			if (rsData.EndOfFile())
			{
				//this.Visible = false;
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtCategory.Text = FCConvert.ToString(rsData.Get_Fields("tblcodetypes.description"));
			if (Conversion.Val(rsData.Get_Fields_Int32("CodeID")) == 0)
			{
				// THIS MUST BE A MANUAL ENTRY
				txtRate.Text = Strings.Format(rsData.Get_Fields_Double("Rates"), "0.0000");
				txtUM.Text = rsData.Get_Fields_String("UM");
				txtPER.Text = FCConvert.ToString(rsData.Get_Fields("tblVacationSick.Type"));
			}
			else
			{
				// THERE IS AN ACTUAL PAY CODE SET UP
				txtRate.Text = Strings.Format(rsData.Get_Fields("Rate"), "0.0000");
				txtUM.Text = rsData.Get_Fields_String("DHW");
				txtPER.Text = FCConvert.ToString(rsData.Get_Fields("tblcodes.Type"));
			}
			txtAccruedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("accruedcurrent")), "0.00");
			txtAccruedFiscal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("AccruedFiscal")), "0.00");
			txtAccruedCalendar.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("AccruedCalendar")), "0.00");
			txtUsedCurrent.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedCurrent")), "0.00");
			txtUsedFiscal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedFiscal")), "0.00");
			txtUsedCalendar.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedCalendar")), "0.00");
			txtBalance.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("UsedBalance")), "0.00");
			rsData.MoveNext();
		}

		
	}
}
