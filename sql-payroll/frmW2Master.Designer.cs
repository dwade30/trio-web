//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2Master.
	/// </summary>
	partial class frmW2Master
	{
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame fraDeductions;
		public fecherFoundation.FCGrid vsDeductions;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame fraMatch;
		public fecherFoundation.FCGrid vsMatch;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsBox10;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddBox10;
		public fecherFoundation.FCToolStripMenuItem mnuAddDeduction;
		public fecherFoundation.FCToolStripMenuItem mnuAddEmpMatchCode;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteBox10;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteBox12;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteBox14;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.vsData = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.fraDeductions = new fecherFoundation.FCFrame();
            this.vsDeductions = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.fraMatch = new fecherFoundation.FCFrame();
            this.vsMatch = new fecherFoundation.FCGrid();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsBox10 = new fecherFoundation.FCGrid();
            //this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuDeleteBox10 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteBox12 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteBox14 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddBox10 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddDeduction = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddEmpMatchCode = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddBox10 = new fecherFoundation.FCButton();
            this.cmdAddBox12 = new fecherFoundation.FCButton();
            this.cmdAddBox14Code = new fecherFoundation.FCButton();
            this.cmdDeleteBox10 = new fecherFoundation.FCButton();
            this.cmdDeleteBox12 = new fecherFoundation.FCButton();
            this.cmdDeleteBox14 = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeductions)).BeginInit();
            this.fraDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMatch)).BeginInit();
            this.fraMatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox14Code)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddBox10);
            this.TopPanel.Controls.Add(this.cmdAddBox12);
            this.TopPanel.Controls.Add(this.cmdAddBox14Code);
            this.TopPanel.Controls.Add(this.cmdDeleteBox10);
            this.TopPanel.Controls.Add(this.cmdDeleteBox12);
            this.TopPanel.Controls.Add(this.cmdDeleteBox14);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteBox14, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteBox12, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteBox10, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddBox14Code, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddBox12, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddBox10, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "W2 Information";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Location = new System.Drawing.Point(30, 30);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.ShowFocusRect = false;
            this.SSTab1.Size = new System.Drawing.Size(1020, 800);
            this.SSTab1.TabIndex = 0;
            this.SSTab1.TabsPerRow = 0;
            this.SSTab1.Text = "Town Information";
            this.SSTab1.WordWrap = false;
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.vsData);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "Town Information";
            // 
            // vsData
            // 
            this.vsData.AllowSelection = false;
            this.vsData.AllowUserToResizeColumns = false;
            this.vsData.AllowUserToResizeRows = false;
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsData.BackColorBkg = System.Drawing.Color.Empty;
            this.vsData.BackColorFixed = System.Drawing.Color.Empty;
            this.vsData.BackColorSel = System.Drawing.Color.Empty;
            this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsData.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsData.ColumnHeadersHeight = 30;
            this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsData.DragIcon = null;
            this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsData.FixedCols = 0;
            this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsData.FrozenCols = 0;
            this.vsData.GridColor = System.Drawing.Color.Empty;
            this.vsData.GridColorFixed = System.Drawing.Color.Empty;
            this.vsData.Location = new System.Drawing.Point(20, 20);
            this.vsData.Name = "vsData";
            this.vsData.OutlineCol = 0;
            this.vsData.ReadOnly = true;
            this.vsData.RowHeadersVisible = false;
            this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsData.RowHeightMin = 0;
            this.vsData.Rows = 50;
            this.vsData.ScrollTipText = null;
            this.vsData.ShowColumnVisibilityMenu = false;
            this.vsData.Size = new System.Drawing.Size(978, 713);
            this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsData.TabIndex = 1;
            this.vsData.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsData_KeyPressEdit);
            this.vsData.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsData_ChangeEdit);
            this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
            this.vsData.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsData_ValidateEdit);
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            this.vsData.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsData_MouseMoveEvent);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.fraDeductions);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Text = "Box 12 Deductions";
            // 
            // fraDeductions
            // 
            this.fraDeductions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraDeductions.AppearanceKey = "groupBoxNoBorders";
            this.fraDeductions.Controls.Add(this.vsDeductions);
            this.fraDeductions.Location = new System.Drawing.Point(20, 20);
            this.fraDeductions.Name = "fraDeductions";
            this.fraDeductions.Size = new System.Drawing.Size(978, 711);
            this.fraDeductions.TabIndex = 2;
            this.fraDeductions.Text = "Box 12";
            // 
            // vsDeductions
            // 
            this.vsDeductions.AllowSelection = false;
            this.vsDeductions.AllowUserToResizeColumns = false;
            this.vsDeductions.AllowUserToResizeRows = false;
            this.vsDeductions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDeductions.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsDeductions.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorBkg = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorSel = System.Drawing.Color.Empty;
            this.vsDeductions.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsDeductions.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsDeductions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsDeductions.ColumnHeadersHeight = 30;
            this.vsDeductions.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsDeductions.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsDeductions.DragIcon = null;
            this.vsDeductions.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsDeductions.FixedCols = 0;
            this.vsDeductions.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.FrozenCols = 0;
            this.vsDeductions.GridColor = System.Drawing.Color.Empty;
            this.vsDeductions.GridColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.Location = new System.Drawing.Point(0, 30);
            this.vsDeductions.Name = "vsDeductions";
            this.vsDeductions.OutlineCol = 0;
            this.vsDeductions.ReadOnly = true;
            this.vsDeductions.RowHeadersVisible = false;
            this.vsDeductions.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsDeductions.RowHeightMin = 0;
            this.vsDeductions.Rows = 50;
            this.vsDeductions.ScrollTipText = null;
            this.vsDeductions.ShowColumnVisibilityMenu = false;
            this.vsDeductions.Size = new System.Drawing.Size(978, 681);
            this.vsDeductions.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsDeductions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDeductions.TabIndex = 3;
            this.vsDeductions.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_AfterEdit);
            this.vsDeductions.CurrentCellChanged += new System.EventHandler(this.vsDeductions_RowColChange);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.fraMatch);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Text = "Box 14 Deductions";
            // 
            // fraMatch
            // 
            this.fraMatch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMatch.AppearanceKey = "groupBoxNoBorders";
            this.fraMatch.Controls.Add(this.vsMatch);
            this.fraMatch.Location = new System.Drawing.Point(20, 20);
            this.fraMatch.Name = "fraMatch";
            this.fraMatch.Size = new System.Drawing.Size(978, 711);
            this.fraMatch.TabIndex = 4;
            this.fraMatch.Text = "Box 14";
            // 
            // vsMatch
            // 
            this.vsMatch.AllowSelection = false;
            this.vsMatch.AllowUserToResizeColumns = false;
            this.vsMatch.AllowUserToResizeRows = false;
            this.vsMatch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMatch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsMatch.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsMatch.BackColorBkg = System.Drawing.Color.Empty;
            this.vsMatch.BackColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.BackColorSel = System.Drawing.Color.Empty;
            this.vsMatch.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsMatch.Cols = 10;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsMatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.vsMatch.ColumnHeadersHeight = 30;
            this.vsMatch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsMatch.DefaultCellStyle = dataGridViewCellStyle6;
            this.vsMatch.DragIcon = null;
            this.vsMatch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsMatch.FixedCols = 0;
            this.vsMatch.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.FrozenCols = 0;
            this.vsMatch.GridColor = System.Drawing.Color.Empty;
            this.vsMatch.GridColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.Location = new System.Drawing.Point(0, 30);
            this.vsMatch.Name = "vsMatch";
            this.vsMatch.OutlineCol = 0;
            this.vsMatch.ReadOnly = true;
            this.vsMatch.RowHeadersVisible = false;
            this.vsMatch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsMatch.RowHeightMin = 0;
            this.vsMatch.Rows = 50;
            this.vsMatch.ScrollTipText = null;
            this.vsMatch.ShowColumnVisibilityMenu = false;
            this.vsMatch.Size = new System.Drawing.Size(978, 681);
            this.vsMatch.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsMatch.TabIndex = 5;
            this.vsMatch.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsMatch_AfterEdit);
            this.vsMatch.CurrentCellChanged += new System.EventHandler(this.vsMatch_RowColChange);
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.Frame1);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Text = "Box 10 Dependent Care";
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsBox10);
            this.Frame1.Location = new System.Drawing.Point(20, 20);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(978, 711);
            this.Frame1.TabIndex = 6;
            this.Frame1.Text = "Box 10";
            // 
            // vsBox10
            // 
            this.vsBox10.AllowSelection = false;
            this.vsBox10.AllowUserToResizeColumns = false;
            this.vsBox10.AllowUserToResizeRows = false;
            this.vsBox10.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBox10.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBox10.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBox10.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBox10.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBox10.BackColorSel = System.Drawing.Color.Empty;
            this.vsBox10.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsBox10.Cols = 10;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsBox10.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.vsBox10.ColumnHeadersHeight = 30;
            this.vsBox10.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBox10.DefaultCellStyle = dataGridViewCellStyle8;
            this.vsBox10.DragIcon = null;
            this.vsBox10.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBox10.ExtendLastCol = true;
            this.vsBox10.FixedCols = 0;
            this.vsBox10.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsBox10.FrozenCols = 0;
            this.vsBox10.GridColor = System.Drawing.Color.Empty;
            this.vsBox10.GridColorFixed = System.Drawing.Color.Empty;
            this.vsBox10.Location = new System.Drawing.Point(0, 30);
            this.vsBox10.Name = "vsBox10";
            this.vsBox10.OutlineCol = 0;
            this.vsBox10.ReadOnly = true;
            this.vsBox10.RowHeadersVisible = false;
            this.vsBox10.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBox10.RowHeightMin = 0;
            this.vsBox10.Rows = 50;
            this.vsBox10.ScrollTipText = null;
            this.vsBox10.ShowColumnVisibilityMenu = false;
            this.vsBox10.Size = new System.Drawing.Size(978, 681);
            this.vsBox10.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsBox10.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsBox10.TabIndex = 7;
            // 
            // MainMenu1
            // 
            //this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            //this.mnuDeleteBox10,
            //this.mnuDeleteBox12,
            //this.mnuDeleteBox14});
            //this.MainMenu1.Name = null;
            // 
            // mnuDeleteBox10
            // 
            this.mnuDeleteBox10.Index = 0;
            this.mnuDeleteBox10.Name = "mnuDeleteBox10";
            this.mnuDeleteBox10.Text = "Delete Box 10 Code";
            this.mnuDeleteBox10.Click += new System.EventHandler(this.mnuDeleteBox10_Click);
            // 
            // mnuDeleteBox12
            // 
            this.mnuDeleteBox12.Index = 1;
            this.mnuDeleteBox12.Name = "mnuDeleteBox12";
            this.mnuDeleteBox12.Text = "Delete Box 12 Code";
            this.mnuDeleteBox12.Click += new System.EventHandler(this.mnuDeleteBox12_Click);
            // 
            // mnuDeleteBox14
            // 
            this.mnuDeleteBox14.Index = 2;
            this.mnuDeleteBox14.Name = "mnuDeleteBox14";
            this.mnuDeleteBox14.Text = "Delete Box 14 Code";
            this.mnuDeleteBox14.Click += new System.EventHandler(this.mnuDeleteBox14_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuAddBox10
            // 
            this.mnuAddBox10.Index = -1;
            this.mnuAddBox10.Name = "mnuAddBox10";
            this.mnuAddBox10.Text = "Add Box 10 Code";
            this.mnuAddBox10.Click += new System.EventHandler(this.mnuAddBox10_Click);
            // 
            // mnuAddDeduction
            // 
            this.mnuAddDeduction.Index = -1;
            this.mnuAddDeduction.Name = "mnuAddDeduction";
            this.mnuAddDeduction.Text = "Add Box 12 Code";
            this.mnuAddDeduction.Click += new System.EventHandler(this.mnuAddDeduction_Click);
            // 
            // mnuAddEmpMatchCode
            // 
            this.mnuAddEmpMatchCode.Index = -1;
            this.mnuAddEmpMatchCode.Name = "mnuAddEmpMatchCode";
            this.mnuAddEmpMatchCode.Text = "Add Box 14 Code";
            this.mnuAddEmpMatchCode.Click += new System.EventHandler(this.mnuAddEmpMatchCode_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Save & Exit                    ";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(493, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddBox10
            // 
            this.cmdAddBox10.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddBox10.AppearanceKey = "toolbarButton";
            this.cmdAddBox10.Location = new System.Drawing.Point(216, 29);
            this.cmdAddBox10.Name = "cmdAddBox10";
            this.cmdAddBox10.Size = new System.Drawing.Size(126, 24);
            this.cmdAddBox10.TabIndex = 1;
            this.cmdAddBox10.Text = "Add Box 10 Code";
            this.cmdAddBox10.Click += new System.EventHandler(this.mnuAddBox10_Click);
            // 
            // cmdAddBox12
            // 
            this.cmdAddBox12.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddBox12.AppearanceKey = "toolbarButton";
            this.cmdAddBox12.Location = new System.Drawing.Point(348, 29);
            this.cmdAddBox12.Name = "cmdAddBox12";
            this.cmdAddBox12.Size = new System.Drawing.Size(126, 24);
            this.cmdAddBox12.TabIndex = 2;
            this.cmdAddBox12.Text = "Add Box 12 Code";
            this.cmdAddBox12.Click += new System.EventHandler(this.mnuAddDeduction_Click);
            // 
            // cmdAddBox14Code
            // 
            this.cmdAddBox14Code.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddBox14Code.AppearanceKey = "toolbarButton";
            this.cmdAddBox14Code.Location = new System.Drawing.Point(480, 29);
            this.cmdAddBox14Code.Name = "cmdAddBox14Code";
            this.cmdAddBox14Code.Size = new System.Drawing.Size(126, 24);
            this.cmdAddBox14Code.TabIndex = 3;
            this.cmdAddBox14Code.Text = "Add Box 14 Code";
            this.cmdAddBox14Code.Click += new System.EventHandler(this.mnuAddEmpMatchCode_Click);
            // 
            // cmdDeleteBox10
            // 
            this.cmdDeleteBox10.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteBox10.AppearanceKey = "toolbarButton";
            this.cmdDeleteBox10.Location = new System.Drawing.Point(612, 29);
            this.cmdDeleteBox10.Name = "cmdDeleteBox10";
            this.cmdDeleteBox10.Size = new System.Drawing.Size(144, 24);
            this.cmdDeleteBox10.TabIndex = 4;
            this.cmdDeleteBox10.Text = "Delete Box 10 Code";
            this.cmdDeleteBox10.Click += new System.EventHandler(this.mnuDeleteBox10_Click);
            // 
            // cmdDeleteBox12
            // 
            this.cmdDeleteBox12.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteBox12.AppearanceKey = "toolbarButton";
            this.cmdDeleteBox12.Location = new System.Drawing.Point(762, 29);
            this.cmdDeleteBox12.Name = "cmdDeleteBox12";
            this.cmdDeleteBox12.Size = new System.Drawing.Size(142, 24);
            this.cmdDeleteBox12.TabIndex = 5;
            this.cmdDeleteBox12.Text = "Delete Box 12 Code";
            this.cmdDeleteBox12.Click += new System.EventHandler(this.mnuDeleteBox12_Click);
            // 
            // cmdDeleteBox14
            // 
            this.cmdDeleteBox14.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteBox14.AppearanceKey = "toolbarButton";
            this.cmdDeleteBox14.Location = new System.Drawing.Point(910, 29);
            this.cmdDeleteBox14.Name = "cmdDeleteBox14";
            this.cmdDeleteBox14.Size = new System.Drawing.Size(140, 24);
            this.cmdDeleteBox14.TabIndex = 6;
            this.cmdDeleteBox14.Text = "Delete Box 14 Code";
            this.cmdDeleteBox14.Click += new System.EventHandler(this.mnuDeleteBox14_Click);
            // 
            // frmW2Master
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            //this.Menu = this.MainMenu1;
            this.Name = "frmW2Master";
            this.Text = "W2 Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmW2Master_Load);
            this.Activated += new System.EventHandler(this.frmW2Master_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2Master_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW2Master_KeyPress);
            this.Resize += new System.EventHandler(this.frmW2Master_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDeductions)).EndInit();
            this.fraDeductions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMatch)).EndInit();
            this.fraMatch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddBox14Code)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteBox14)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddBox12;
		private FCButton cmdAddBox10;
		private FCButton cmdAddBox14Code;
        private FCButton cmdDeleteBox14;
        private FCButton cmdDeleteBox12;
        private FCButton cmdDeleteBox10;
    }
}