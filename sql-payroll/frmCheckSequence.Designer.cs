//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckSequence.
	/// </summary>
	partial class frmCheckSequence
	{
		public fecherFoundation.FCComboBox cmbReprint;
		public fecherFoundation.FCLabel lblReprint;
		public fecherFoundation.FCTextBox txtCheckMessage;
		public fecherFoundation.FCFrame fraPayDate;
		public fecherFoundation.FCTextBox txtPayRunID;
		public fecherFoundation.FCTextBox txtPayDate;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblWarrant;
		public fecherFoundation.FCLabel lblRunNumber;
		public fecherFoundation.FCLabel lblPayDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraReprint;
		public fecherFoundation.FCTextBox txtStartingNonNegotiable;
		public fecherFoundation.FCTextBox txtEndReprint;
		public fecherFoundation.FCTextBox txtStartReprint;
		public fecherFoundation.FCTextBox txtStartingCheck;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblStartReprint;
		public fecherFoundation.FCLabel lblEndReprint;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label8;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbReprint = new fecherFoundation.FCComboBox();
			this.lblReprint = new fecherFoundation.FCLabel();
			this.txtCheckMessage = new fecherFoundation.FCTextBox();
			this.fraPayDate = new fecherFoundation.FCFrame();
			this.txtPayRunID = new fecherFoundation.FCTextBox();
			this.txtPayDate = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblWarrant = new fecherFoundation.FCLabel();
			this.lblRunNumber = new fecherFoundation.FCLabel();
			this.lblPayDate = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraReprint = new fecherFoundation.FCFrame();
			this.txtStartingNonNegotiable = new fecherFoundation.FCTextBox();
			this.txtEndReprint = new fecherFoundation.FCTextBox();
			this.txtStartReprint = new fecherFoundation.FCTextBox();
			this.txtStartingCheck = new fecherFoundation.FCTextBox();
			this.Label9 = new fecherFoundation.FCLabel();
			this.lblStartReprint = new fecherFoundation.FCLabel();
			this.lblEndReprint = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).BeginInit();
			this.fraPayDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReprint)).BeginInit();
			this.fraReprint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(864, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraReprint);
			this.ClientArea.Controls.Add(this.fraPayDate);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.txtCheckMessage);
			this.ClientArea.Controls.Add(this.cmbReprint);
			this.ClientArea.Controls.Add(this.lblReprint);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Size = new System.Drawing.Size(864, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(864, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(94, 30);
			this.HeaderText.Text = "Checks";
			// 
			// cmbReprint
			// 
			this.cmbReprint.Items.AddRange(new object[] {
            "Reprint",
            "Initial Run"});
			this.cmbReprint.Location = new System.Drawing.Point(165, 30);
			this.cmbReprint.Name = "cmbReprint";
			this.cmbReprint.Size = new System.Drawing.Size(190, 40);
			this.cmbReprint.TabIndex = 26;
			this.cmbReprint.Text = "Initial Run";
			this.cmbReprint.SelectedIndexChanged += new System.EventHandler(this.optReprint_CheckedChanged);
			// 
			// lblReprint
			// 
			this.lblReprint.AutoSize = true;
			this.lblReprint.Location = new System.Drawing.Point(30, 44);
			this.lblReprint.Name = "lblReprint";
			this.lblReprint.Size = new System.Drawing.Size(80, 15);
			this.lblReprint.TabIndex = 27;
			this.lblReprint.Text = "PRINT TYPE";
			// 
			// txtCheckMessage
			// 
			this.txtCheckMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckMessage.Location = new System.Drawing.Point(195, 320);
			this.txtCheckMessage.MaxLength = 40;
			this.txtCheckMessage.Name = "txtCheckMessage";
			this.txtCheckMessage.Size = new System.Drawing.Size(633, 40);
			this.txtCheckMessage.TabIndex = 25;
			// 
			// fraPayDate
			// 
			this.fraPayDate.Controls.Add(this.txtPayRunID);
			this.fraPayDate.Controls.Add(this.txtPayDate);
			this.fraPayDate.Controls.Add(this.Label7);
			this.fraPayDate.Controls.Add(this.Label6);
			this.fraPayDate.Location = new System.Drawing.Point(30, 380);
			this.fraPayDate.Name = "fraPayDate";
			this.fraPayDate.Size = new System.Drawing.Size(471, 150);
			this.fraPayDate.TabIndex = 20;
			this.fraPayDate.Text = "Check Information";
			this.fraPayDate.Visible = false;
			// 
			// txtPayRunID
			// 
			this.txtPayRunID.BackColor = System.Drawing.SystemColors.Window;
			this.txtPayRunID.Enabled = false;
			this.txtPayRunID.Location = new System.Drawing.Point(277, 90);
			this.txtPayRunID.Name = "txtPayRunID";
			this.txtPayRunID.Size = new System.Drawing.Size(174, 40);
			this.txtPayRunID.TabIndex = 22;
			this.txtPayRunID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtPayDate
			// 
			this.txtPayDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtPayDate.Enabled = false;
			this.txtPayDate.Location = new System.Drawing.Point(277, 30);
			this.txtPayDate.Name = "txtPayDate";
			this.txtPayDate.Size = new System.Drawing.Size(174, 40);
			this.txtPayDate.TabIndex = 21;
			this.txtPayDate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// Label7
			// 
			this.Label7.Enabled = false;
			this.Label7.Location = new System.Drawing.Point(20, 104);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(78, 22);
			this.Label7.TabIndex = 24;
			this.Label7.Text = "PAY RUN ID ";
			// 
			// Label6
			// 
			this.Label6.Enabled = false;
			this.Label6.Location = new System.Drawing.Point(20, 44);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(193, 22);
			this.Label6.TabIndex = 23;
			this.Label6.Text = "PAY DATE OF CHECK TO REPRINT";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.lblJournal);
			this.Frame2.Controls.Add(this.Label4);
			this.Frame2.Controls.Add(this.lblWarrant);
			this.Frame2.Controls.Add(this.lblRunNumber);
			this.Frame2.Controls.Add(this.lblPayDate);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Location = new System.Drawing.Point(30, 90);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(325, 210);
			this.Frame2.TabIndex = 9;
			this.Frame2.Text = "Check Information";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(168, 135);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(137, 15);
			this.lblJournal.TabIndex = 19;
			this.lblJournal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 135);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(70, 15);
			this.Label4.TabIndex = 18;
			this.Label4.Text = "JOURNAL";
			// 
			// lblWarrant
			// 
			this.lblWarrant.Location = new System.Drawing.Point(168, 100);
			this.lblWarrant.Name = "lblWarrant";
			this.lblWarrant.Size = new System.Drawing.Size(137, 15);
			this.lblWarrant.TabIndex = 17;
			this.lblWarrant.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblRunNumber
			// 
			this.lblRunNumber.Location = new System.Drawing.Point(168, 65);
			this.lblRunNumber.Name = "lblRunNumber";
			this.lblRunNumber.Size = new System.Drawing.Size(137, 15);
			this.lblRunNumber.TabIndex = 16;
			this.lblRunNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Location = new System.Drawing.Point(168, 30);
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Size = new System.Drawing.Size(137, 15);
			this.lblPayDate.TabIndex = 15;
			this.lblPayDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 100);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(70, 15);
			this.Label3.TabIndex = 14;
			this.Label3.Text = "WARRANT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 65);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(83, 15);
			this.Label2.TabIndex = 13;
			this.Label2.Text = "RUN NUMBER";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(64, 15);
			this.Label1.TabIndex = 12;
			this.Label1.Text = "PAY DATE";
			// 
			// fraReprint
			// 
			this.fraReprint.Controls.Add(this.txtStartingNonNegotiable);
			this.fraReprint.Controls.Add(this.txtEndReprint);
			this.fraReprint.Controls.Add(this.txtStartReprint);
			this.fraReprint.Controls.Add(this.txtStartingCheck);
			this.fraReprint.Controls.Add(this.Label9);
			this.fraReprint.Controls.Add(this.lblStartReprint);
			this.fraReprint.Controls.Add(this.lblEndReprint);
			this.fraReprint.Controls.Add(this.Label5);
			this.fraReprint.Location = new System.Drawing.Point(385, 30);
			this.fraReprint.Name = "fraReprint";
			this.fraReprint.Size = new System.Drawing.Size(443, 270);
			this.fraReprint.TabIndex = 3;
			this.fraReprint.Text = "Print/Reprint Checks";
			// 
			// txtStartingNonNegotiable
			// 
			this.txtStartingNonNegotiable.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartingNonNegotiable.Location = new System.Drawing.Point(287, 210);
			this.txtStartingNonNegotiable.Name = "txtStartingNonNegotiable";
			this.txtStartingNonNegotiable.Size = new System.Drawing.Size(136, 40);
			this.txtStartingNonNegotiable.TabIndex = 11;
			this.txtStartingNonNegotiable.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtStartingNonNegotiable.Visible = false;
			// 
			// txtEndReprint
			// 
			this.txtEndReprint.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndReprint.Enabled = false;
			this.txtEndReprint.Location = new System.Drawing.Point(287, 90);
			this.txtEndReprint.Name = "txtEndReprint";
			this.txtEndReprint.Size = new System.Drawing.Size(136, 40);
			this.txtEndReprint.TabIndex = 8;
			this.txtEndReprint.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtStartReprint
			// 
			this.txtStartReprint.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartReprint.Enabled = false;
			this.txtStartReprint.Location = new System.Drawing.Point(287, 30);
			this.txtStartReprint.Name = "txtStartReprint";
			this.txtStartReprint.Size = new System.Drawing.Size(136, 40);
			this.txtStartReprint.TabIndex = 7;
			this.txtStartReprint.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtStartingCheck
			// 
			this.txtStartingCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartingCheck.Location = new System.Drawing.Point(287, 150);
			this.txtStartingCheck.Name = "txtStartingCheck";
			this.txtStartingCheck.Size = new System.Drawing.Size(136, 40);
			this.txtStartingCheck.TabIndex = 10;
			this.txtStartingCheck.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 224);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(197, 22);
			this.Label9.TabIndex = 27;
			this.Label9.Text = "FIRST NON-NEGOTIABLE NUMBER";
			this.Label9.Visible = false;
			// 
			// lblStartReprint
			// 
			this.lblStartReprint.Enabled = false;
			this.lblStartReprint.Location = new System.Drawing.Point(20, 44);
			this.lblStartReprint.Name = "lblStartReprint";
			this.lblStartReprint.Size = new System.Drawing.Size(197, 21);
			this.lblStartReprint.TabIndex = 6;
			this.lblStartReprint.Text = "FIRST CHECK NUMBER TO REPRINT";
			// 
			// lblEndReprint
			// 
			this.lblEndReprint.Enabled = false;
			this.lblEndReprint.Location = new System.Drawing.Point(20, 104);
			this.lblEndReprint.Name = "lblEndReprint";
			this.lblEndReprint.Size = new System.Drawing.Size(214, 21);
			this.lblEndReprint.TabIndex = 5;
			this.lblEndReprint.Text = "LAST CHECK NUMBER TO REPRINT";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 164);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(139, 21);
			this.Label5.TabIndex = 4;
			this.Label5.Text = "FIRST CHECK NUMBER";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 334);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(110, 22);
			this.Label8.TabIndex = 26;
			this.Label8.Text = "CHECK MESSAGE";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(315, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmCheckSequence
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(864, 688);
			this.FillColor = 0;
			this.Name = "frmCheckSequence";
			this.ShowInTaskbar = false;
			this.Text = "Checks";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmCheckSequence_Load);
			this.Activated += new System.EventHandler(this.frmCheckSequence_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCheckSequence_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).EndInit();
			this.fraPayDate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraReprint)).EndInit();
			this.fraReprint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
