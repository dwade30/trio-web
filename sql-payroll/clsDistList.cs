//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public class clsDistList
	{
		//=========================================================
		private List<clsDistributionLine> lstDistList = new List<clsDistributionLine>();
		private string strEmployeeNumber = string.Empty;
		private int intCurrentIndex;

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				return strEmployeeNumber;
			}
		}

		public clsDistList() : base()
		{
			strEmployeeNumber = "";
			intCurrentIndex = -1;
		}

		public clsDistributionLine AddNew(int intRecNumber)
		{
			clsDistributionLine tDist = new clsDistributionLine();
			tDist.EmployeeNumber = strEmployeeNumber;
			tDist.ID = 0;
			tDist.RecordNumber = intRecNumber;
			lstDistList.Add(tDist);
			return tDist;
		}

		public void LoadEmployee(string strEmpNum)
		{
			strEmployeeNumber = strEmpNum;
			ClearList();
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			clsDistributionLine tDist;
			strSQL = "Select * from tblPayrolldistribution where employeenumber = '" + strEmployeeNumber + "' order by recordnumber";
			rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				tDist = new clsDistributionLine();
				tDist.ID = rsLoad.Get_Fields_Int32("ID");
				tDist.AccountCode = rsLoad.Get_Fields_String("AccountCode").ToIntegerValue();
				tDist.AccountNumber = rsLoad.Get_Fields_String("AccountNumber");
				tDist.BaseRate = rsLoad.Get_Fields_Decimal("baserate").ToDouble();
				tDist.CD = rsLoad.Get_Fields_Int32("CD");
				tDist.ContractID = rsLoad.Get_Fields_Int32("ContractID");
				tDist.DefaultHours = rsLoad.Get_Fields_Double("DefaultHours");
				tDist.DistU = rsLoad.Get_Fields_String("DistU");
				tDist.EmployeeNumber = strEmpNum;
				tDist.Factor = rsLoad.Get_Fields_Double("Factor");
				tDist.GrantFunded = rsLoad.Get_Fields_String("grantfunded");
				tDist.Gross = rsLoad.Get_Fields_Decimal("gross").ToDouble();
				tDist.HoursWeek = rsLoad.Get_Fields_Double("hoursweek");
				tDist.MSRS = rsLoad.Get_Fields_String("msrs");
                tDist.MSRSID = rsLoad.Get_Fields_Int32("msrsid");
				tDist.NumberWeeks = rsLoad.Get_Fields_Int32("NumberWeeks");
				tDist.PayCategory = rsLoad.Get_Fields_Int32("cat");
				tDist.Project = rsLoad.Get_Fields_Int32("Project");
				tDist.RecordNumber = rsLoad.Get_Fields_Int32("RecordNumber");
				tDist.StatusCode = rsLoad.Get_Fields_String("StatusCode");
				tDist.TaxCode = rsLoad.Get_Fields_Int32("TaxCode");
				tDist.WC = rsLoad.Get_Fields_String("WC");
				tDist.WeeksTaxWithheld = rsLoad.Get_Fields_Int16("WeeksTaxWithheld");
				tDist.WorkComp = rsLoad.Get_Fields_String("WorkComp");
				lstDistList.Add(tDist);
				rsLoad.MoveNext();
			}
			if (!(lstDistList.Count < 1))
			{
				intCurrentIndex = 0;
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		private void ClearList()
		{
			if (!(lstDistList == null))
			{
				lstDistList.Clear();
			}
		}

		public bool FindFirstCat(int lngCat)
		{
			if (MoveFirst())
			{
				while (intCurrentIndex >= 0)
				{
					if (lstDistList[intCurrentIndex].PayCategory == lngCat)
                    {
                        return true;
                    }
					MoveNext();
				}
			}

            return false;
        }

		public bool FindNextCat(int lngCat)
		{
			MoveNext();
			if (IsCurrent())
			{
				while (intCurrentIndex >= 0)
				{
					if (lstDistList[intCurrentIndex].PayCategory == lngCat)
                    {
                        return true;
                    }
					MoveNext();
				}
			}

            return false;
        }

		public bool MoveFirst()
		{
			if (!(lstDistList == null))
			{
				if (!FCUtils.IsEmpty(lstDistList))
				{
					if (lstDistList.Count > 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}

			if (intCurrentIndex >= 0)
            {
                return true;
            }

            return false;
        }

		public void MoveLast()
		{
			if (!(lstDistList == null))
			{
				if (!FCUtils.IsEmpty(lstDistList))
				{
					if (lstDistList.Count > 0)
					{
						intCurrentIndex = lstDistList.Count - 1;
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{

			if (!FCUtils.IsEmpty(lstDistList))
			{
				if (intCurrentIndex > lstDistList.Count)
                {
                    intCurrentIndex = -1;
                }
				else
				{
					while (intCurrentIndex < lstDistList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex >= lstDistList.Count)
						{
                            intCurrentIndex = -1;
							break;
						}
						else if (lstDistList[intCurrentIndex] == null)
						{
						}
						else
						{
							break;
						}
					}
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
			return intCurrentIndex;
		}

		public bool IsCurrent()
		{
			if (intCurrentIndex >= 0)
            {
                return true;
            }

            return false;
        }

		public bool FindID(int lngID)
		{
            if (lngID > 0)
            {
                if (lstDistList.Count > 0)
                {
                    int X;
                    for (X = 0; X <= lstDistList.Count - 1; X++)
                    {
                        if (!(lstDistList[X] == null))
                        {
                            if (lstDistList[X].ID == lngID)
                            {
                                intCurrentIndex = X;
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
		}

		public clsDistributionLine GetCurrentDist()
		{
			clsDistributionLine tDist;
			tDist = null;
			if (!FCUtils.IsEmpty(lstDistList))
			{
				if (intCurrentIndex >= 0)
				{
					if (!(lstDistList[intCurrentIndex] == null))
					{
						tDist = lstDistList[intCurrentIndex];
					}
				}
			}
			
			return tDist;
		}
    }
}
