﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1EmployeeRecord.
	/// </summary>
	public partial class srptC1EmployeeRecord : FCSectionReport
	{
		public srptC1EmployeeRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1EmployeeRecord InstancePtr
		{
			get
			{
				return (srptC1EmployeeRecord)Sys.GetInstance(typeof(srptC1EmployeeRecord));
			}
		}

		protected srptC1EmployeeRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1EmployeeRecord	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strRec;
			string strTemp;
			double dblTemp;
			strRec = FCConvert.ToString(this.UserData);
			strTemp = Strings.Mid(strRec, 2, 9);
			strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
			txtSSN.Text = strTemp;
			strTemp = Strings.Mid(strRec, 11, 20);
			txtLast.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 31, 12);
			txtFirst.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 64, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtUGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 78, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtExcess.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 92, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtTaxable.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 177, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtStateGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 191, 14);
			dblTemp = Conversion.Val(strTemp);
			dblTemp /= 100;
			txtStateTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 205, 1);
			txtSeasonal.Text = strTemp;
			strTemp = Strings.Mid(strRec, 226, 1);
			if (Conversion.Val(strTemp) == 1)
			{
				txtSex.Text = "F";
			}
			else
			{
				txtSex.Text = "M";
			}
		}

		
	}
}
