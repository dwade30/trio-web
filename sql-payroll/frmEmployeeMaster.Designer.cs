//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeMaster.
	/// </summary>
	partial class frmEmployeeMaster
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label37;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkFirstCheckOnly;
		public fecherFoundation.FCComboBox cboStatus;
		public fecherFoundation.FCFrame Frame5;
		public FCGrid GridSchedule;
		public fecherFoundation.FCCheckBox chkFTOrPT;
		public fecherFoundation.FCComboBox cboCheck;
		public fecherFoundation.FCComboBox cboPayFrequency;
		public fecherFoundation.FCTextBox txtSeqNumber;
		public fecherFoundation.FCTextBox txtCode1;
		public fecherFoundation.FCTextBox txtCode2;
		public fecherFoundation.FCTextBox txtDeptDiv;
		public fecherFoundation.FCTextBox txtStdWk;
		public fecherFoundation.FCTextBox txtStdDay;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCTextBox txtGroupID;
		public fecherFoundation.FCTextBox txtBLSWorksiteID;
		public fecherFoundation.FCTextBox txtWorkCompCode;
		public fecherFoundation.FCLabel Label37_0;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblCode1;
		public fecherFoundation.FCLabel lblCode2;
		public fecherFoundation.FCLabel lblDeptDiv;
		public fecherFoundation.FCLabel Label37_1;
		public fecherFoundation.FCLabel Label37_2;
		public fecherFoundation.FCLabel Label37_4;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label37_5;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkMQGE;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCCheckBox chkW2StatutoryEmployee;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCComboBox cboState;
		public fecherFoundation.FCMaskedTextBox txtSSN;
		public T2KDateBox txtDateHire;
		public fecherFoundation.FCMaskedTextBox txtDateAnniversary;
		public T2KDateBox txtDateBirth;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCComboBox cboSex;
		public fecherFoundation.FCCheckBox chkW2DefIncome;
		public fecherFoundation.FCCheckBox chkW2Pen;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtDesignation;
		public fecherFoundation.FCTextBox txtMiddleName;
		public T2KDateBox txtTerminationDate;
		public T2KDateBox txtRetirementDate;
		public fecherFoundation.FCTextBox txtPhone;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel lblFullName;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtFederalStatus;
		public fecherFoundation.FCTextBox txtStateStatus;
		//public fecherFoundation.FCComboBox cboFedPayStatus;
        public Wisej.Web.ComboBox cboFedPayStatus;
		public fecherFoundation.FCComboBox cboStatePayStatus;
		public fecherFoundation.FCComboBox cboFedDollarPercent;
		public fecherFoundation.FCTextBox txtFederalTaxes;
		public fecherFoundation.FCComboBox cboStateDollarPercent;
		public fecherFoundation.FCTextBox txtStateTaxes;
		public fecherFoundation.FCCheckBox chkMedicare;
		public fecherFoundation.FCCheckBox chkUnemploymentExempt;
		public fecherFoundation.FCCheckBox chkFicaExempt;
		public fecherFoundation.FCCheckBox chkWorkersCompExempt;
		public fecherFoundation.FCCheckBox chkFirstCheckOnly_0;
		public fecherFoundation.FCCheckBox chkFirstCheckOnly_1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel lblDependents;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCFrame fraComments;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCTextBox txtComments;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel lblComments;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnuPayFrequency;
		public fecherFoundation.FCToolStripMenuItem mnuPayStatus;
		public fecherFoundation.FCToolStripMenuItem mnuComments;
		public fecherFoundation.FCToolStripMenuItem mnuTemporaryOverrides;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmployeeMaster));
            this.cboStatus = new fecherFoundation.FCComboBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.GridSchedule = new fecherFoundation.FCGrid();
            this.chkFTOrPT = new fecherFoundation.FCCheckBox();
            this.cboCheck = new fecherFoundation.FCComboBox();
            this.cboPayFrequency = new fecherFoundation.FCComboBox();
            this.txtSeqNumber = new fecherFoundation.FCTextBox();
            this.txtCode1 = new fecherFoundation.FCTextBox();
            this.txtCode2 = new fecherFoundation.FCTextBox();
            this.txtDeptDiv = new fecherFoundation.FCTextBox();
            this.txtStdWk = new fecherFoundation.FCTextBox();
            this.txtStdDay = new fecherFoundation.FCTextBox();
            this.chkPrint = new fecherFoundation.FCCheckBox();
            this.txtGroupID = new fecherFoundation.FCTextBox();
            this.txtBLSWorksiteID = new fecherFoundation.FCTextBox();
            this.txtWorkCompCode = new fecherFoundation.FCTextBox();
            this.Label37_0 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblCode1 = new fecherFoundation.FCLabel();
            this.lblCode2 = new fecherFoundation.FCLabel();
            this.lblDeptDiv = new fecherFoundation.FCLabel();
            this.Label37_1 = new fecherFoundation.FCLabel();
            this.Label37_2 = new fecherFoundation.FCLabel();
            this.Label37_4 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label37_5 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkMQGE = new fecherFoundation.FCCheckBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.chkW2StatutoryEmployee = new fecherFoundation.FCCheckBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.cboState = new fecherFoundation.FCComboBox();
            this.txtSSN = new fecherFoundation.FCMaskedTextBox();
            this.txtDateHire = new Global.T2KDateBox();
            this.txtDateAnniversary = new fecherFoundation.FCMaskedTextBox();
            this.txtDateBirth = new Global.T2KDateBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.cboSex = new fecherFoundation.FCComboBox();
            this.chkW2DefIncome = new fecherFoundation.FCCheckBox();
            this.chkW2Pen = new fecherFoundation.FCCheckBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.txtDesignation = new fecherFoundation.FCTextBox();
            this.txtMiddleName = new fecherFoundation.FCTextBox();
            this.txtTerminationDate = new Global.T2KDateBox();
            this.txtRetirementDate = new Global.T2KDateBox();
            this.txtPhone = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.lblFullName = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label25 = new fecherFoundation.FCLabel();
            this.Label26 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.lblOtherDependents = new fecherFoundation.FCLabel();
            this.txtFederalOtherDependents = new fecherFoundation.FCTextBox();
            this.lblOtherIncome = new fecherFoundation.FCLabel();
            this.lblOtherDeductions = new fecherFoundation.FCLabel();
            this.txtFederalOtherIncome = new fecherFoundation.FCTextBox();
            this.txtFederalAdditionalDeduction = new fecherFoundation.FCTextBox();
            this.chkW4MultJobs = new fecherFoundation.FCCheckBox();
            this.txtW4Date = new Global.T2KDateBox();
            this.lblW4Date = new fecherFoundation.FCLabel();
            this.txtFederalStatus = new fecherFoundation.FCTextBox();
            this.txtStateStatus = new fecherFoundation.FCTextBox();
            this.cboFedPayStatus = new Wisej.Web.ComboBox();
            this.cboStatePayStatus = new fecherFoundation.FCComboBox();
            this.cboFedDollarPercent = new fecherFoundation.FCComboBox();
            this.txtFederalTaxes = new fecherFoundation.FCTextBox();
            this.cboStateDollarPercent = new fecherFoundation.FCComboBox();
            this.txtStateTaxes = new fecherFoundation.FCTextBox();
            this.chkMedicare = new fecherFoundation.FCCheckBox();
            this.chkUnemploymentExempt = new fecherFoundation.FCCheckBox();
            this.chkFicaExempt = new fecherFoundation.FCCheckBox();
            this.chkWorkersCompExempt = new fecherFoundation.FCCheckBox();
            this.chkFirstCheckOnly_0 = new fecherFoundation.FCCheckBox();
            this.chkFirstCheckOnly_1 = new fecherFoundation.FCCheckBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.lblDependents = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.fraComments = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.txtComments = new fecherFoundation.FCTextBox();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.lblComments = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayFrequency = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayStatus = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTemporaryOverrides = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdPay = new fecherFoundation.FCButton();
            this.cmdFiling = new fecherFoundation.FCButton();
            this.cmdComments = new fecherFoundation.FCButton();
            this.cmdTemporaryOverrides = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFTOrPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMQGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2StatutoryEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2DefIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2Pen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerminationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRetirementDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkW4MultJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMedicare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnemploymentExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFicaExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWorkersCompExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstCheckOnly_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstCheckOnly_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraComments)).BeginInit();
            this.fraComments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFiling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 975);
            this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.cboStatus);
            this.ClientArea.Controls.Add(this.ImgComment);
            this.ClientArea.Controls.Add(this.lblComments);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Controls.Add(this.fraComments);
            this.ClientArea.Size = new System.Drawing.Size(1088, 640);
            this.ClientArea.Controls.SetChildIndex(this.fraComments, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblEmployeeNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblComments, 0);
            this.ClientArea.Controls.SetChildIndex(this.ImgComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboStatus, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdTemporaryOverrides);
            this.TopPanel.Controls.Add(this.cmdComments);
            this.TopPanel.Controls.Add(this.cmdFiling);
            this.TopPanel.Controls.Add(this.cmdPay);
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPay, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFiling, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdTemporaryOverrides, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(257, 28);
            this.HeaderText.Text = "Employee Add / Update";
            // 
            // cboStatus
            // 
            this.cboStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cboStatus.Items.AddRange(new object[] {
            "Active",
            "Terminated",
            "Suspended",
            "Hold 1 Week",
            "Retired",
            "Resigned"});
            this.cboStatus.Location = new System.Drawing.Point(870, 30);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(180, 40);
            this.cboStatus.TabIndex = 1;
            this.cboStatus.Tag = "STAY";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // Frame5
            // 
            this.Frame5.AppearanceKey = "groupBoxNoBorders";
            this.Frame5.BackColor = System.Drawing.Color.White;
            this.Frame5.Controls.Add(this.GridSchedule);
            this.Frame5.Controls.Add(this.chkFTOrPT);
            this.Frame5.Controls.Add(this.cboCheck);
            this.Frame5.Controls.Add(this.cboPayFrequency);
            this.Frame5.Controls.Add(this.txtSeqNumber);
            this.Frame5.Controls.Add(this.txtCode1);
            this.Frame5.Controls.Add(this.txtCode2);
            this.Frame5.Controls.Add(this.txtDeptDiv);
            this.Frame5.Controls.Add(this.txtStdWk);
            this.Frame5.Controls.Add(this.txtStdDay);
            this.Frame5.Controls.Add(this.chkPrint);
            this.Frame5.Controls.Add(this.txtGroupID);
            this.Frame5.Controls.Add(this.txtBLSWorksiteID);
            this.Frame5.Controls.Add(this.txtWorkCompCode);
            this.Frame5.Controls.Add(this.Label37_0);
            this.Frame5.Controls.Add(this.Label21);
            this.Frame5.Controls.Add(this.Label1);
            this.Frame5.Controls.Add(this.lblCode1);
            this.Frame5.Controls.Add(this.lblCode2);
            this.Frame5.Controls.Add(this.lblDeptDiv);
            this.Frame5.Controls.Add(this.Label37_1);
            this.Frame5.Controls.Add(this.Label37_2);
            this.Frame5.Controls.Add(this.Label37_4);
            this.Frame5.Controls.Add(this.Label19);
            this.Frame5.Controls.Add(this.Label37_5);
            this.Frame5.Location = new System.Drawing.Point(0, 651);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(1050, 324);
            this.Frame5.TabIndex = 5;
            // 
            // GridSchedule
            // 
            this.GridSchedule.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridSchedule.ExtendLastCol = true;
            this.GridSchedule.FixedCols = 0;
            this.GridSchedule.Location = new System.Drawing.Point(30, 210);
            this.GridSchedule.Name = "GridSchedule";
            this.GridSchedule.ReadOnly = false;
            this.GridSchedule.RowHeadersVisible = false;
            this.GridSchedule.Size = new System.Drawing.Size(744, 114);
            this.GridSchedule.TabIndex = 26;
            this.GridSchedule.ComboCloseUp += new System.EventHandler(this.GridSchedule_ComboCloseUp);
            this.GridSchedule.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridSchedule_MouseMoveEvent);
            // 
            // chkFTOrPT
            // 
            this.chkFTOrPT.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFTOrPT.Location = new System.Drawing.Point(30, 110);
            this.chkFTOrPT.Name = "chkFTOrPT";
            this.chkFTOrPT.Size = new System.Drawing.Size(119, 22);
            this.chkFTOrPT.TabIndex = 4;
            this.chkFTOrPT.Text = "Part Time Emp";
            this.chkFTOrPT.CheckedChanged += new System.EventHandler(this.chkFTOrPT_CheckedChanged);
            // 
            // cboCheck
            // 
            this.cboCheck.BackColor = System.Drawing.SystemColors.Window;
            this.cboCheck.Items.AddRange(new object[] {
            "Regular",
            "Direct Deposit"});
            this.cboCheck.Location = new System.Drawing.Point(122, 10);
            this.cboCheck.Name = "cboCheck";
            this.cboCheck.Size = new System.Drawing.Size(180, 40);
            this.cboCheck.TabIndex = 1;
            this.cboCheck.Tag = "STAY";
            this.cboCheck.SelectedIndexChanged += new System.EventHandler(this.cboCheck_SelectedIndexChanged);
            // 
            // cboPayFrequency
            // 
            this.cboPayFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayFrequency.Location = new System.Drawing.Point(122, 60);
            this.cboPayFrequency.Name = "cboPayFrequency";
            this.cboPayFrequency.Size = new System.Drawing.Size(180, 40);
            this.cboPayFrequency.TabIndex = 3;
            this.cboPayFrequency.Tag = "STAY";
            this.cboPayFrequency.SelectedIndexChanged += new System.EventHandler(this.cboPayFrequency_SelectedIndexChanged);
            this.cboPayFrequency.DropDown += new System.EventHandler(this.cboPayFrequency_DropDown);
            // 
            // txtSeqNumber
            // 
            this.txtSeqNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeqNumber.Location = new System.Drawing.Point(920, 10);
            this.txtSeqNumber.Name = "txtSeqNumber";
            this.txtSeqNumber.Size = new System.Drawing.Size(130, 40);
            this.txtSeqNumber.TabIndex = 17;
            this.txtSeqNumber.Enter += new System.EventHandler(this.txtSeqNumber_Enter);
            this.txtSeqNumber.TextChanged += new System.EventHandler(this.txtSeqNumber_TextChanged);
            this.txtSeqNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeqNumber_Validating);
            this.txtSeqNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSeqNumber_KeyPress);
            // 
            // txtCode1
            // 
            this.txtCode1.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode1.Location = new System.Drawing.Point(920, 160);
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.Size = new System.Drawing.Size(130, 40);
            this.txtCode1.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.txtCode1, "Double-click to the left to set code description");
            this.txtCode1.Enter += new System.EventHandler(this.txtCode1_Enter);
            this.txtCode1.TextChanged += new System.EventHandler(this.txtCode1_TextChanged);
            // 
            // txtCode2
            // 
            this.txtCode2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode2.Location = new System.Drawing.Point(920, 210);
            this.txtCode2.Name = "txtCode2";
            this.txtCode2.Size = new System.Drawing.Size(130, 40);
            this.txtCode2.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.txtCode2, "Double-click to the left to set code description");
            this.txtCode2.Enter += new System.EventHandler(this.txtCode2_Enter);
            this.txtCode2.TextChanged += new System.EventHandler(this.txtCode2_TextChanged);
            // 
            // txtDeptDiv
            // 
            this.txtDeptDiv.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptDiv.Location = new System.Drawing.Point(920, 60);
            this.txtDeptDiv.Name = "txtDeptDiv";
            this.txtDeptDiv.Size = new System.Drawing.Size(130, 40);
            this.txtDeptDiv.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.txtDeptDiv, "Double click to see a list of valid department/division codes");
            this.txtDeptDiv.Enter += new System.EventHandler(this.txtDeptDiv_Enter);
            this.txtDeptDiv.DoubleClick += new System.EventHandler(this.txtDeptDiv_DoubleClick);
            this.txtDeptDiv.TextChanged += new System.EventHandler(this.txtDeptDiv_TextChanged);
            this.txtDeptDiv.Validating += new System.ComponentModel.CancelEventHandler(this.txtDeptDiv_Validating);
            this.txtDeptDiv.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDeptDiv_KeyDown);
            this.txtDeptDiv.KeyUp += new Wisej.Web.KeyEventHandler(this.txtDeptDiv_KeyUp);
            // 
            // txtStdWk
            // 
            this.txtStdWk.BackColor = System.Drawing.SystemColors.Window;
            this.txtStdWk.Location = new System.Drawing.Point(684, 110);
            this.txtStdWk.Name = "txtStdWk";
            this.txtStdWk.Size = new System.Drawing.Size(90, 40);
            this.txtStdWk.TabIndex = 15;
            this.txtStdWk.Enter += new System.EventHandler(this.txtStdWk_Enter);
            this.txtStdWk.TextChanged += new System.EventHandler(this.txtStdWk_TextChanged);
            this.txtStdWk.Validating += new System.ComponentModel.CancelEventHandler(this.txtStdWk_Validating);
            this.txtStdWk.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStdWk_KeyPress);
            // 
            // txtStdDay
            // 
            this.txtStdDay.BackColor = System.Drawing.SystemColors.Window;
            this.txtStdDay.Location = new System.Drawing.Point(507, 110);
            this.txtStdDay.Name = "txtStdDay";
            this.txtStdDay.Size = new System.Drawing.Size(89, 40);
            this.txtStdDay.TabIndex = 13;
            this.txtStdDay.Enter += new System.EventHandler(this.txtStdDay_Enter);
            this.txtStdDay.TextChanged += new System.EventHandler(this.txtStdDay_TextChanged);
            this.txtStdDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtStdDay_Validating);
            this.txtStdDay.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStdDay_KeyPress);
            // 
            // chkPrint
            // 
            this.chkPrint.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPrint.Location = new System.Drawing.Point(30, 147);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(116, 22);
            this.chkPrint.TabIndex = 5;
            this.chkPrint.Text = "Print Pay Rate";
            this.ToolTip1.SetToolTip(this.chkPrint, "Pay rate will always come from the first line of distribution");
            this.chkPrint.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
            // 
            // txtGroupID
            // 
            this.txtGroupID.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroupID.Location = new System.Drawing.Point(920, 110);
            this.txtGroupID.Name = "txtGroupID";
            this.txtGroupID.Size = new System.Drawing.Size(130, 40);
            this.txtGroupID.TabIndex = 21;
            this.txtGroupID.Enter += new System.EventHandler(this.txtGroupID_Enter);
            this.txtGroupID.TextChanged += new System.EventHandler(this.txtGroupID_TextChanged);
            this.txtGroupID.MouseMove += new Wisej.Web.MouseEventHandler(this.txtGroupID_MouseMove);
            // 
            // txtBLSWorksiteID
            // 
            this.txtBLSWorksiteID.BackColor = System.Drawing.SystemColors.Window;
            this.txtBLSWorksiteID.Location = new System.Drawing.Point(507, 60);
            this.txtBLSWorksiteID.Name = "txtBLSWorksiteID";
            this.txtBLSWorksiteID.Size = new System.Drawing.Size(267, 40);
            this.txtBLSWorksiteID.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtBLSWorksiteID, "Enter: Report Number \" - \" Worksite Number");
            this.txtBLSWorksiteID.Enter += new System.EventHandler(this.txtBLSWorksiteID_Enter);
            this.txtBLSWorksiteID.TextChanged += new System.EventHandler(this.txtBLSWorksiteID_TextChanged);
            this.txtBLSWorksiteID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBLSWorksiteID_KeyPress);
            // 
            // txtWorkCompCode
            // 
            this.txtWorkCompCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtWorkCompCode.Location = new System.Drawing.Point(507, 10);
            this.txtWorkCompCode.Name = "txtWorkCompCode";
            this.txtWorkCompCode.Size = new System.Drawing.Size(267, 40);
            this.txtWorkCompCode.TabIndex = 7;
            this.txtWorkCompCode.Enter += new System.EventHandler(this.txtWorkCompCode_Enter);
            this.txtWorkCompCode.TextChanged += new System.EventHandler(this.txtWorkCompCode_TextChanged);
            this.txtWorkCompCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtWorkCompCode_Validating);
            this.txtWorkCompCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtWorkCompCode_KeyPress);
            // 
            // Label37_0
            // 
            this.Label37_0.Location = new System.Drawing.Point(30, 24);
            this.Label37_0.Name = "Label37_0";
            this.Label37_0.Size = new System.Drawing.Size(60, 16);
            this.Label37_0.TabIndex = 27;
            this.Label37_0.Text = "CHECK";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(30, 74);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(60, 16);
            this.Label21.TabIndex = 2;
            this.Label21.Text = "PAY FREQ";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(794, 24);
            this.Label1.Name = "Label1";
            this.Label1.TabIndex = 16;
            this.Label1.Text = "SEQ NUMBER";
            // 
            // lblCode1
            // 
            this.lblCode1.Location = new System.Drawing.Point(794, 174);
            this.lblCode1.Name = "lblCode1";
            this.lblCode1.TabIndex = 22;
            this.lblCode1.Text = "CODE 1";
            // 
            // lblCode2
            // 
            this.lblCode2.Location = new System.Drawing.Point(794, 224);
            this.lblCode2.Name = "lblCode2";
            this.lblCode2.TabIndex = 24;
            this.lblCode2.Text = "CODE 2";
            // 
            // lblDeptDiv
            // 
            this.lblDeptDiv.Location = new System.Drawing.Point(794, 74);
            this.lblDeptDiv.Name = "lblDeptDiv";
            this.lblDeptDiv.TabIndex = 18;
            this.lblDeptDiv.Text = "HOME DEPT/DIV";
            // 
            // Label37_1
            // 
            this.Label37_1.Location = new System.Drawing.Point(322, 24);
            this.Label37_1.Name = "Label37_1";
            this.Label37_1.Size = new System.Drawing.Size(160, 16);
            this.Label37_1.TabIndex = 6;
            this.Label37_1.Text = "WORK COMP CODE";
            // 
            // Label37_2
            // 
            this.Label37_2.Location = new System.Drawing.Point(322, 124);
            this.Label37_2.Name = "Label37_2";
            this.Label37_2.Size = new System.Drawing.Size(160, 16);
            this.Label37_2.TabIndex = 12;
            this.Label37_2.Text = "HOURS IN STANDARD:  DAY";
            // 
            // Label37_4
            // 
            this.Label37_4.Location = new System.Drawing.Point(616, 124);
            this.Label37_4.Name = "Label37_4";
            this.Label37_4.Size = new System.Drawing.Size(33, 19);
            this.Label37_4.TabIndex = 14;
            this.Label37_4.Text = "WEEK";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(794, 124);
            this.Label19.Name = "Label19";
            this.Label19.TabIndex = 20;
            this.Label19.Text = "GROUP ID";
            // 
            // Label37_5
            // 
            this.Label37_5.Location = new System.Drawing.Point(322, 74);
            this.Label37_5.Name = "Label37_5";
            this.Label37_5.Size = new System.Drawing.Size(160, 16);
            this.Label37_5.TabIndex = 10;
            this.Label37_5.Text = "BLS WORKSITE ID";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.chkMQGE);
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.chkW2StatutoryEmployee);
            this.Frame1.Controls.Add(this.txtLastName);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.cboState);
            this.Frame1.Controls.Add(this.txtSSN);
            this.Frame1.Controls.Add(this.txtDateHire);
            this.Frame1.Controls.Add(this.txtDateAnniversary);
            this.Frame1.Controls.Add(this.txtDateBirth);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.cboSex);
            this.Frame1.Controls.Add(this.chkW2DefIncome);
            this.Frame1.Controls.Add(this.chkW2Pen);
            this.Frame1.Controls.Add(this.txtFirstName);
            this.Frame1.Controls.Add(this.txtDesignation);
            this.Frame1.Controls.Add(this.txtMiddleName);
            this.Frame1.Controls.Add(this.txtTerminationDate);
            this.Frame1.Controls.Add(this.txtRetirementDate);
            this.Frame1.Controls.Add(this.txtPhone);
            this.Frame1.Controls.Add(this.txtEmail);
            this.Frame1.Controls.Add(this.Label27);
            this.Frame1.Controls.Add(this.Label16);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.lblFullName);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label20);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label36);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.Label22);
            this.Frame1.Controls.Add(this.Label24);
            this.Frame1.Controls.Add(this.Label25);
            this.Frame1.Controls.Add(this.Label26);
            this.Frame1.Location = new System.Drawing.Point(20, 71);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1030, 353);
            this.Frame1.TabIndex = 3;
            // 
            // chkMQGE
            // 
            this.chkMQGE.Location = new System.Drawing.Point(10, 326);
            this.chkMQGE.Name = "chkMQGE";
            this.chkMQGE.Size = new System.Drawing.Size(274, 22);
            this.chkMQGE.TabIndex = 40;
            this.chkMQGE.Text = "Medicare Qualifed Government Employee";
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(536, 189);
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(90, 40);
            this.txtZip4.TabIndex = 18;
            // 
            // chkW2StatutoryEmployee
            // 
            this.chkW2StatutoryEmployee.Location = new System.Drawing.Point(214, 289);
            this.chkW2StatutoryEmployee.Name = "chkW2StatutoryEmployee";
            this.chkW2StatutoryEmployee.Size = new System.Drawing.Size(173, 22);
            this.chkW2StatutoryEmployee.TabIndex = 38;
            this.chkW2StatutoryEmployee.Text = "W-2 Statutory Employee";
            this.chkW2StatutoryEmployee.CheckedChanged += new System.EventHandler(this.chkW2StatutoryEmployee_CheckedChanged);
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Location = new System.Drawing.Point(131, 39);
            this.txtLastName.MaxLength = 25;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(120, 40);
            this.txtLastName.TabIndex = 5;
            this.txtLastName.TextChanged += new System.EventHandler(this.txtLastName_TextChanged);
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Location = new System.Drawing.Point(131, 89);
            this.txtAddress1.MaxLength = 100;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(495, 40);
            this.txtAddress1.TabIndex = 10;
            this.txtAddress1.TextChanged += new System.EventHandler(this.txtAddress1_TextChanged);
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Location = new System.Drawing.Point(131, 139);
            this.txtAddress2.MaxLength = 100;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(495, 40);
            this.txtAddress2.TabIndex = 12;
            this.txtAddress2.TextChanged += new System.EventHandler(this.txtAddress2_TextChanged);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(131, 189);
            this.txtCity.MaxLength = 25;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(125, 40);
            this.txtCity.TabIndex = 14;
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            // 
            // cboState
            // 
            this.cboState.BackColor = System.Drawing.SystemColors.Window;
            this.cboState.Location = new System.Drawing.Point(266, 189);
            this.cboState.Name = "cboState";
            this.cboState.Size = new System.Drawing.Size(130, 40);
            this.cboState.Sorted = true;
            this.cboState.TabIndex = 15;
            this.cboState.Tag = "STAY";
            this.cboState.SelectedIndexChanged += new System.EventHandler(this.cboState_SelectedIndexChanged);
            // 
            // txtSSN
            // 
            this.txtSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtSSN.Location = new System.Drawing.Point(850, 10);
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Size = new System.Drawing.Size(180, 22);
            this.txtSSN.TabIndex = 24;
            // 
            // txtDateHire
            // 
            this.txtDateHire.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateHire.Location = new System.Drawing.Point(850, 60);
            this.txtDateHire.Name = "txtDateHire";
            this.txtDateHire.Size = new System.Drawing.Size(180, 22);
            this.txtDateHire.TabIndex = 26;
            this.txtDateHire.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateHire_Validating);
            // 
            // txtDateAnniversary
            // 
            this.txtDateAnniversary.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateAnniversary.Location = new System.Drawing.Point(850, 110);
            this.txtDateAnniversary.Mask = "##/##";
            this.txtDateAnniversary.Name = "txtDateAnniversary";
            this.txtDateAnniversary.Size = new System.Drawing.Size(180, 22);
            this.txtDateAnniversary.TabIndex = 28;
            this.txtDateAnniversary.Enter += new System.EventHandler(this.txtDateAnniversary_Enter);
            this.txtDateAnniversary.TextChanged += new System.EventHandler(this.txtDateAnniversary_TextChanged);
            this.txtDateAnniversary.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateAnniversary_Validating);
            this.txtDateAnniversary.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDateAnniversary_KeyPress);
            // 
            // txtDateBirth
            // 
            this.txtDateBirth.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateBirth.Location = new System.Drawing.Point(850, 160);
            this.txtDateBirth.Name = "txtDateBirth";
            this.txtDateBirth.Size = new System.Drawing.Size(180, 22);
            this.txtDateBirth.TabIndex = 30;
            this.txtDateBirth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateBirth_Validating);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(406, 189);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(105, 40);
            this.txtZip.TabIndex = 16;
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            this.txtZip.TextChanged += new System.EventHandler(this.txtZip_TextChanged);
            this.txtZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip_Validating);
            this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
            // 
            // cboSex
            // 
            this.cboSex.BackColor = System.Drawing.SystemColors.Window;
            this.cboSex.Items.AddRange(new object[] {
            "N/A",
            "Male",
            "Female"});
            this.cboSex.Location = new System.Drawing.Point(850, 310);
            this.cboSex.Name = "cboSex";
            this.cboSex.Size = new System.Drawing.Size(180, 40);
            this.cboSex.TabIndex = 36;
            this.cboSex.Tag = "STAY";
            this.cboSex.SelectedIndexChanged += new System.EventHandler(this.cboSex_SelectedIndexChanged);
            // 
            // chkW2DefIncome
            // 
            this.chkW2DefIncome.Location = new System.Drawing.Point(10, 289);
            this.chkW2DefIncome.Name = "chkW2DefIncome";
            this.chkW2DefIncome.Size = new System.Drawing.Size(156, 22);
            this.chkW2DefIncome.TabIndex = 37;
            this.chkW2DefIncome.Text = "W-2 Deferred Income";
            this.chkW2DefIncome.CheckedChanged += new System.EventHandler(this.chkW2DefIncome_CheckedChanged);
            // 
            // chkW2Pen
            // 
            this.chkW2Pen.Location = new System.Drawing.Point(439, 289);
            this.chkW2Pen.Name = "chkW2Pen";
            this.chkW2Pen.Size = new System.Drawing.Size(123, 22);
            this.chkW2Pen.TabIndex = 39;
            this.chkW2Pen.Text = "W-2 Retirement";
            this.chkW2Pen.CheckedChanged += new System.EventHandler(this.chkW2Pen_CheckedChanged);
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Location = new System.Drawing.Point(261, 39);
            this.txtFirstName.MaxLength = 25;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(120, 40);
            this.txtFirstName.TabIndex = 6;
            this.txtFirstName.TextChanged += new System.EventHandler(this.txtFirstName_TextChanged);
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtDesignation.Location = new System.Drawing.Point(516, 39);
            this.txtDesignation.MaxLength = 25;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(110, 40);
            this.txtDesignation.TabIndex = 8;
            this.txtDesignation.TextChanged += new System.EventHandler(this.txtDesignation_TextChanged);
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.BackColor = System.Drawing.SystemColors.Window;
            this.txtMiddleName.Location = new System.Drawing.Point(391, 39);
            this.txtMiddleName.MaxLength = 25;
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(115, 40);
            this.txtMiddleName.TabIndex = 7;
            // 
            // txtTerminationDate
            // 
            this.txtTerminationDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTerminationDate.Location = new System.Drawing.Point(850, 210);
            this.txtTerminationDate.Name = "txtTerminationDate";
            this.txtTerminationDate.Size = new System.Drawing.Size(180, 22);
            this.txtTerminationDate.TabIndex = 32;
            this.txtTerminationDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTerminationDate_Validating);
            // 
            // txtRetirementDate
            // 
            this.txtRetirementDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtRetirementDate.Location = new System.Drawing.Point(850, 260);
            this.txtRetirementDate.Name = "txtRetirementDate";
            this.txtRetirementDate.Size = new System.Drawing.Size(180, 22);
            this.txtRetirementDate.TabIndex = 34;
            this.txtRetirementDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtRetirementDate_Validating);
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhone.Location = new System.Drawing.Point(131, 239);
            this.txtPhone.MaxLength = 25;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(185, 40);
            this.txtPhone.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.txtPhone, "Press Insert Key To Add City, State and Zip");
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(401, 239);
            this.txtEmail.MaxLength = 255;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(225, 40);
            this.txtEmail.TabIndex = 22;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // Label27
            // 
            this.Label27.Location = new System.Drawing.Point(521, 203);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(13, 19);
            this.Label27.TabIndex = 17;
            this.Label27.Text = "-";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(391, 10);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(80, 15);
            this.Label16.TabIndex = 2;
            this.Label16.Text = "MIDDLE NAME";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(261, 10);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(98, 15);
            this.Label13.TabIndex = 1;
            this.Label13.Text = "FIRST NAME ";
            // 
            // lblFullName
            // 
            this.lblFullName.Location = new System.Drawing.Point(10, 53);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.TabIndex = 4;
            this.lblFullName.Text = "FULL NAME";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(10, 103);
            this.Label9.Name = "Label9";
            this.Label9.TabIndex = 9;
            this.Label9.Text = "ADDRESS 1";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(10, 153);
            this.Label10.Name = "Label10";
            this.Label10.TabIndex = 11;
            this.Label10.Text = "ADDRESS 2";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(10, 203);
            this.Label11.Name = "Label11";
            this.Label11.TabIndex = 13;
            this.Label11.Text = "CITY/STATE/ZIP";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(701, 24);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(120, 16);
            this.Label6.TabIndex = 23;
            this.Label6.Text = "SOCIAL SECURITY #";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(701, 174);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(120, 16);
            this.Label20.TabIndex = 29;
            this.Label20.Text = "BIRTH DATE";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(701, 124);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(120, 16);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "ANNIVERSARY DATE";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(701, 74);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(120, 16);
            this.Label5.TabIndex = 25;
            this.Label5.Text = "DATE HIRED";
            // 
            // Label36
            // 
            this.Label36.Location = new System.Drawing.Point(701, 324);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(120, 16);
            this.Label36.TabIndex = 35;
            this.Label36.Text = "SEX";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(131, 10);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(98, 15);
            this.Label15.TabIndex = 41;
            this.Label15.Text = "LAST NAME ";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(516, 10);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(35, 15);
            this.Label14.TabIndex = 3;
            this.Label14.Text = "DESIG";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(701, 224);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(120, 16);
            this.Label22.TabIndex = 31;
            this.Label22.Text = "TERMINATION DATE";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(701, 274);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(120, 16);
            this.Label24.TabIndex = 33;
            this.Label24.Text = " RETIREMENT DATE";
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(10, 253);
            this.Label25.Name = "Label25";
            this.Label25.TabIndex = 19;
            this.Label25.Text = "PHONE";
            // 
            // Label26
            // 
            this.Label26.Location = new System.Drawing.Point(338, 253);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(48, 19);
            this.Label26.TabIndex = 21;
            this.Label26.Text = "E-MAIL";
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.lblOtherDependents);
            this.Frame2.Controls.Add(this.txtFederalOtherDependents);
            this.Frame2.Controls.Add(this.lblOtherIncome);
            this.Frame2.Controls.Add(this.lblOtherDeductions);
            this.Frame2.Controls.Add(this.txtFederalOtherIncome);
            this.Frame2.Controls.Add(this.txtFederalAdditionalDeduction);
            this.Frame2.Controls.Add(this.chkW4MultJobs);
            this.Frame2.Controls.Add(this.txtW4Date);
            this.Frame2.Controls.Add(this.lblW4Date);
            this.Frame2.Controls.Add(this.txtFederalStatus);
            this.Frame2.Controls.Add(this.txtStateStatus);
            this.Frame2.Controls.Add(this.cboFedPayStatus);
            this.Frame2.Controls.Add(this.cboStatePayStatus);
            this.Frame2.Controls.Add(this.cboFedDollarPercent);
            this.Frame2.Controls.Add(this.txtFederalTaxes);
            this.Frame2.Controls.Add(this.cboStateDollarPercent);
            this.Frame2.Controls.Add(this.txtStateTaxes);
            this.Frame2.Controls.Add(this.chkMedicare);
            this.Frame2.Controls.Add(this.chkUnemploymentExempt);
            this.Frame2.Controls.Add(this.chkFicaExempt);
            this.Frame2.Controls.Add(this.chkWorkersCompExempt);
            this.Frame2.Controls.Add(this.chkFirstCheckOnly_0);
            this.Frame2.Controls.Add(this.chkFirstCheckOnly_1);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label7);
            this.Frame2.Controls.Add(this.Label12);
            this.Frame2.Controls.Add(this.lblDependents);
            this.Frame2.Controls.Add(this.Label18);
            this.Frame2.Controls.Add(this.Label23);
            this.Frame2.Location = new System.Drawing.Point(30, 434);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(1020, 211);
            this.Frame2.TabIndex = 4;
            this.Frame2.Text = "Tax Table";
            // 
            // lblOtherDependents
            // 
            this.lblOtherDependents.Location = new System.Drawing.Point(386, 83);
            this.lblOtherDependents.Name = "lblOtherDependents";
            this.lblOtherDependents.Size = new System.Drawing.Size(67, 19);
            this.lblOtherDependents.TabIndex = 42;
            this.lblOtherDependents.Text = "OTHER";
            this.lblOtherDependents.ToolTipText = "Number of other dependents";
            // 
            // txtFederalOtherDependents
            // 
            this.txtFederalOtherDependents.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalOtherDependents.Location = new System.Drawing.Point(386, 110);
            this.txtFederalOtherDependents.MaxLength = 25;
            this.txtFederalOtherDependents.Name = "txtFederalOtherDependents";
            this.txtFederalOtherDependents.Size = new System.Drawing.Size(71, 40);
            this.txtFederalOtherDependents.TabIndex = 7;
            // 
            // lblOtherIncome
            // 
            this.lblOtherIncome.Location = new System.Drawing.Point(776, 35);
            this.lblOtherIncome.Name = "lblOtherIncome";
            this.lblOtherIncome.Size = new System.Drawing.Size(120, 16);
            this.lblOtherIncome.TabIndex = 41;
            this.lblOtherIncome.Text = "OTHER INCOME";
            // 
            // lblOtherDeductions
            // 
            this.lblOtherDeductions.Location = new System.Drawing.Point(487, 35);
            this.lblOtherDeductions.Name = "lblOtherDeductions";
            this.lblOtherDeductions.Size = new System.Drawing.Size(148, 16);
            this.lblOtherDeductions.TabIndex = 40;
            this.lblOtherDeductions.Text = "OTHER DEDUCTIONS";
            // 
            // txtFederalOtherIncome
            // 
            this.txtFederalOtherIncome.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalOtherIncome.Location = new System.Drawing.Point(902, 19);
            this.txtFederalOtherIncome.MaxLength = 25;
            this.txtFederalOtherIncome.Name = "txtFederalOtherIncome";
            this.txtFederalOtherIncome.Size = new System.Drawing.Size(110, 40);
            this.txtFederalOtherIncome.TabIndex = 4;
            // 
            // txtFederalAdditionalDeduction
            // 
            this.txtFederalAdditionalDeduction.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalAdditionalDeduction.Location = new System.Drawing.Point(641, 19);
            this.txtFederalAdditionalDeduction.MaxLength = 25;
            this.txtFederalAdditionalDeduction.Name = "txtFederalAdditionalDeduction";
            this.txtFederalAdditionalDeduction.Size = new System.Drawing.Size(110, 40);
            this.txtFederalAdditionalDeduction.TabIndex = 3;
            // 
            // chkW4MultJobs
            // 
            this.chkW4MultJobs.AutoSize = false;
            this.chkW4MultJobs.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkW4MultJobs.Location = new System.Drawing.Point(280, 19);
            this.chkW4MultJobs.Name = "chkW4MultJobs";
            this.chkW4MultJobs.Size = new System.Drawing.Size(192, 40);
            this.chkW4MultJobs.TabIndex = 2;
            this.chkW4MultJobs.Text = "Step 2C Multiple Jobs";
            this.chkW4MultJobs.CheckedChanged += new System.EventHandler(this.fcCheckBox1_CheckedChanged);
            // 
            // txtW4Date
            // 
            this.txtW4Date.AutoSize = false;
            this.txtW4Date.BackColor = System.Drawing.SystemColors.Window;
            this.txtW4Date.Location = new System.Drawing.Point(102, 20);
            this.txtW4Date.Name = "txtW4Date";
            this.txtW4Date.Size = new System.Drawing.Size(154, 39);
            this.txtW4Date.TabIndex = 1;
            // 
            // lblW4Date
            // 
            this.lblW4Date.Location = new System.Drawing.Point(20, 35);
            this.lblW4Date.Name = "lblW4Date";
            this.lblW4Date.Size = new System.Drawing.Size(120, 16);
            this.lblW4Date.TabIndex = 35;
            this.lblW4Date.Text = "W-4 DATE";
            // 
            // txtFederalStatus
            // 
            this.txtFederalStatus.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalStatus.Location = new System.Drawing.Point(305, 110);
            this.txtFederalStatus.MaxLength = 25;
            this.txtFederalStatus.Name = "txtFederalStatus";
            this.txtFederalStatus.Size = new System.Drawing.Size(71, 40);
            this.txtFederalStatus.TabIndex = 6;
            this.txtFederalStatus.Enter += new System.EventHandler(this.txtFederalStatus_Enter);
            this.txtFederalStatus.TextChanged += new System.EventHandler(this.txtFederalStatus_TextChanged);
            this.txtFederalStatus.Validating += new System.ComponentModel.CancelEventHandler(this.txtFederalStatus_Validating);
            this.txtFederalStatus.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalStatus_KeyPress);
            // 
            // txtStateStatus
            // 
            this.txtStateStatus.BackColor = System.Drawing.SystemColors.Window;
            this.txtStateStatus.Location = new System.Drawing.Point(305, 160);
            this.txtStateStatus.MaxLength = 25;
            this.txtStateStatus.Name = "txtStateStatus";
            this.txtStateStatus.Size = new System.Drawing.Size(71, 40);
            this.txtStateStatus.TabIndex = 11;
            this.txtStateStatus.Enter += new System.EventHandler(this.txtStateStatus_Enter);
            this.txtStateStatus.TextChanged += new System.EventHandler(this.txtStateStatus_TextChanged);
            this.txtStateStatus.Validating += new System.ComponentModel.CancelEventHandler(this.txtStateStatus_Validating);
            this.txtStateStatus.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStateStatus_KeyPress);
            // 
            // cboFedPayStatus
            // 
            this.cboFedPayStatus.AutoSize = false;
            this.cboFedPayStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cboFedPayStatus.DisplayMember = "Description";
            this.cboFedPayStatus.Location = new System.Drawing.Point(105, 110);
            this.cboFedPayStatus.Name = "cboFedPayStatus";
            this.cboFedPayStatus.Size = new System.Drawing.Size(190, 40);
            this.cboFedPayStatus.TabIndex = 5;
            this.cboFedPayStatus.Tag = "STAY";
            this.cboFedPayStatus.ValueMember = "ID";
            this.cboFedPayStatus.SelectedIndexChanged += new System.EventHandler(this.cboFedPayStatus_SelectedIndexChanged);
            // 
            // cboStatePayStatus
            // 
            this.cboStatePayStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cboStatePayStatus.Location = new System.Drawing.Point(105, 160);
            this.cboStatePayStatus.Name = "cboStatePayStatus";
            this.cboStatePayStatus.Size = new System.Drawing.Size(190, 40);
            this.cboStatePayStatus.TabIndex = 10;
            this.cboStatePayStatus.Tag = "STAY";
            this.cboStatePayStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatePayStatus_SelectedIndexChanged);
            // 
            // cboFedDollarPercent
            // 
            this.cboFedDollarPercent.BackColor = System.Drawing.SystemColors.Window;
            this.cboFedDollarPercent.Items.AddRange(new object[] {
            "Dollar",
            "Gross Percent",
            "Net Percent"});
            this.cboFedDollarPercent.Location = new System.Drawing.Point(588, 110);
            this.cboFedDollarPercent.Name = "cboFedDollarPercent";
            this.cboFedDollarPercent.Size = new System.Drawing.Size(156, 40);
            this.cboFedDollarPercent.TabIndex = 9;
            this.cboFedDollarPercent.Tag = "STAY";
            this.cboFedDollarPercent.SelectedIndexChanged += new System.EventHandler(this.cboFedDollarPercent_SelectedIndexChanged);
            this.cboFedDollarPercent.Leave += new System.EventHandler(this.cboFedDollarPercent_Leave);
            // 
            // txtFederalTaxes
            // 
            this.txtFederalTaxes.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalTaxes.Location = new System.Drawing.Point(468, 110);
            this.txtFederalTaxes.MaxLength = 25;
            this.txtFederalTaxes.Name = "txtFederalTaxes";
            this.txtFederalTaxes.Size = new System.Drawing.Size(110, 40);
            this.txtFederalTaxes.TabIndex = 8;
            this.txtFederalTaxes.Enter += new System.EventHandler(this.txtFederalTaxes_Enter);
            this.txtFederalTaxes.TextChanged += new System.EventHandler(this.txtFederalTaxes_TextChanged);
            this.txtFederalTaxes.Validating += new System.ComponentModel.CancelEventHandler(this.txtFederalTaxes_Validating);
            this.txtFederalTaxes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalTaxes_KeyPress);
            // 
            // cboStateDollarPercent
            // 
            this.cboStateDollarPercent.BackColor = System.Drawing.SystemColors.Window;
            this.cboStateDollarPercent.Items.AddRange(new object[] {
            "Dollar",
            "Gross Percent",
            "Net Percent"});
            this.cboStateDollarPercent.Location = new System.Drawing.Point(588, 160);
            this.cboStateDollarPercent.Name = "cboStateDollarPercent";
            this.cboStateDollarPercent.Size = new System.Drawing.Size(157, 40);
            this.cboStateDollarPercent.TabIndex = 13;
            this.cboStateDollarPercent.Tag = "STAY";
            this.cboStateDollarPercent.SelectedIndexChanged += new System.EventHandler(this.cboStateDollarPercent_SelectedIndexChanged);
            this.cboStateDollarPercent.Leave += new System.EventHandler(this.cboStateDollarPercent_Leave);
            // 
            // txtStateTaxes
            // 
            this.txtStateTaxes.BackColor = System.Drawing.SystemColors.Window;
            this.txtStateTaxes.Location = new System.Drawing.Point(468, 160);
            this.txtStateTaxes.MaxLength = 25;
            this.txtStateTaxes.Name = "txtStateTaxes";
            this.txtStateTaxes.Size = new System.Drawing.Size(110, 40);
            this.txtStateTaxes.TabIndex = 12;
            this.txtStateTaxes.Enter += new System.EventHandler(this.txtStateTaxes_Enter);
            this.txtStateTaxes.TextChanged += new System.EventHandler(this.txtStateTaxes_TextChanged);
            this.txtStateTaxes.Validating += new System.ComponentModel.CancelEventHandler(this.txtStateTaxes_Validating);
            this.txtStateTaxes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStateTaxes_KeyPress);
            // 
            // chkMedicare
            // 
            this.chkMedicare.Location = new System.Drawing.Point(811, 112);
            this.chkMedicare.Name = "chkMedicare";
            this.chkMedicare.Size = new System.Drawing.Size(133, 22);
            this.chkMedicare.TabIndex = 23;
            this.chkMedicare.Text = "Medicare Exempt";
            this.chkMedicare.CheckedChanged += new System.EventHandler(this.chkMedicare_CheckedChanged);
            // 
            // chkUnemploymentExempt
            // 
            this.chkUnemploymentExempt.Location = new System.Drawing.Point(811, 145);
            this.chkUnemploymentExempt.Name = "chkUnemploymentExempt";
            this.chkUnemploymentExempt.Size = new System.Drawing.Size(114, 22);
            this.chkUnemploymentExempt.TabIndex = 24;
            this.chkUnemploymentExempt.Text = "FUTA Exempt";
            this.chkUnemploymentExempt.CheckedChanged += new System.EventHandler(this.chkUnemploymentExempt_CheckedChanged);
            // 
            // chkFicaExempt
            // 
            this.chkFicaExempt.Location = new System.Drawing.Point(811, 78);
            this.chkFicaExempt.Name = "chkFicaExempt";
            this.chkFicaExempt.Size = new System.Drawing.Size(104, 22);
            this.chkFicaExempt.TabIndex = 22;
            this.chkFicaExempt.Text = "Fica Exempt";
            this.chkFicaExempt.CheckedChanged += new System.EventHandler(this.chkFicaExempt_CheckedChanged);
            // 
            // chkWorkersCompExempt
            // 
            this.chkWorkersCompExempt.Location = new System.Drawing.Point(811, 180);
            this.chkWorkersCompExempt.Name = "chkWorkersCompExempt";
            this.chkWorkersCompExempt.Size = new System.Drawing.Size(166, 22);
            this.chkWorkersCompExempt.TabIndex = 25;
            this.chkWorkersCompExempt.Text = "Workers Comp Exempt";
            this.chkWorkersCompExempt.CheckedChanged += new System.EventHandler(this.chkWorkersCompExempt_CheckedChanged);
            // 
            // chkFirstCheckOnly_0
            // 
            this.chkFirstCheckOnly_0.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFirstCheckOnly_0.Checked = true;
            this.chkFirstCheckOnly_0.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFirstCheckOnly_0.Location = new System.Drawing.Point(761, 116);
            this.chkFirstCheckOnly_0.Name = "chkFirstCheckOnly_0";
            this.chkFirstCheckOnly_0.Size = new System.Drawing.Size(32, 22);
            this.chkFirstCheckOnly_0.TabIndex = 19;
            // 
            // chkFirstCheckOnly_1
            // 
            this.chkFirstCheckOnly_1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFirstCheckOnly_1.Checked = true;
            this.chkFirstCheckOnly_1.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFirstCheckOnly_1.Location = new System.Drawing.Point(761, 166);
            this.chkFirstCheckOnly_1.Name = "chkFirstCheckOnly_1";
            this.chkFirstCheckOnly_1.Size = new System.Drawing.Size(32, 22);
            this.chkFirstCheckOnly_1.TabIndex = 20;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 124);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(78, 16);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "FEDERAL";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 174);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(60, 16);
            this.Label7.TabIndex = 8;
            this.Label7.Text = "STATE";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(105, 83);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(108, 19);
            this.Label12.TabIndex = 26;
            this.Label12.Text = "FILING STATUS";
            // 
            // lblDependents
            // 
            this.lblDependents.Location = new System.Drawing.Point(305, 83);
            this.lblDependents.Name = "lblDependents";
            this.lblDependents.Size = new System.Drawing.Size(152, 19);
            this.lblDependents.TabIndex = 1;
            this.lblDependents.Text = "DEPS / QC";
            this.lblDependents.ToolTipText = "Number of dependents or qualifying children";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(468, 83);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(130, 19);
            this.Label18.TabIndex = 2;
            this.Label18.Text = "ADDITIONAL       TAXES";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(754, 83);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(37, 19);
            this.Label23.TabIndex = 18;
            this.Label23.Text = "AFCO";
            this.ToolTip1.SetToolTip(this.Label23, "Additional Tax on First Check Only");
            // 
            // fraComments
            // 
            this.fraComments.BackColor = System.Drawing.Color.White;
            this.fraComments.Controls.Add(this.cmdCancel);
            this.fraComments.Controls.Add(this.cmdSave);
            this.fraComments.Controls.Add(this.txtComments);
            this.fraComments.Location = new System.Drawing.Point(30, 80);
            this.fraComments.Name = "fraComments";
            this.fraComments.Size = new System.Drawing.Size(1020, 780);
            this.fraComments.TabIndex = 4;
            this.fraComments.Text = "Comments";
            this.fraComments.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(118, 725);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(98, 40);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(20, 725);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(80, 40);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtComments
            // 
            this.txtComments.AcceptsReturn = true;
            this.txtComments.BackColor = System.Drawing.SystemColors.Window;
            this.txtComments.Location = new System.Drawing.Point(20, 30);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(980, 680);
            this.txtComments.TabIndex = 56;
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(30, 30);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(40, 40);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ImgComment.Visible = false;
            this.ImgComment.Click += new System.EventHandler(this.ImgComment_Click);
            // 
            // lblComments
            // 
            this.lblComments.Location = new System.Drawing.Point(870, 34);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(70, 16);
            this.lblComments.TabIndex = 2;
            this.lblComments.Tag = "Employee #";
            this.lblComments.Text = "COMMENTS";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(721, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(79, 20);
            this.Label3.TabIndex = 87;
            this.Label3.Text = "STATUS";
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(90, 44);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(383, 20);
            this.lblEmployeeNumber.TabIndex = 98;
            this.lblEmployeeNumber.Tag = "Employee #";
            this.lblEmployeeNumber.Text = "EMPLOYEE #";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuSP1,
            this.mnuSelectEmployee,
            this.mnuPayFrequency,
            this.mnuPayStatus,
            this.mnuComments,
            this.mnuTemporaryOverrides,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = 2;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnuPayFrequency
            // 
            this.mnuPayFrequency.Index = 3;
            this.mnuPayFrequency.Name = "mnuPayFrequency";
            this.mnuPayFrequency.Text = "Pay Frequency";
            this.mnuPayFrequency.Click += new System.EventHandler(this.mnuPayFrequency_Click);
            // 
            // mnuPayStatus
            // 
            this.mnuPayStatus.Index = 4;
            this.mnuPayStatus.Name = "mnuPayStatus";
            this.mnuPayStatus.Text = "Filing Status";
            this.mnuPayStatus.Click += new System.EventHandler(this.mnuPayStatus_Click);
            // 
            // mnuComments
            // 
            this.mnuComments.Index = 5;
            this.mnuComments.Name = "mnuComments";
            this.mnuComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComments.Text = "Comments";
            this.mnuComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // mnuTemporaryOverrides
            // 
            this.mnuTemporaryOverrides.Index = 6;
            this.mnuTemporaryOverrides.Name = "mnuTemporaryOverrides";
            this.mnuTemporaryOverrides.Text = "Temporary Overrides";
            this.mnuTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 7;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 8;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                  ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 9;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 10;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 11;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(406, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.Location = new System.Drawing.Point(462, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(122, 24);
            this.cmdSelect.TabIndex = 2;
            this.cmdSelect.Text = "Select Employee";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdPay
            // 
            this.cmdPay.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPay.Location = new System.Drawing.Point(590, 29);
            this.cmdPay.Name = "cmdPay";
            this.cmdPay.Size = new System.Drawing.Size(112, 24);
            this.cmdPay.TabIndex = 3;
            this.cmdPay.Text = "Pay Frequency";
            this.cmdPay.Click += new System.EventHandler(this.mnuPayFrequency_Click);
            // 
            // cmdFiling
            // 
            this.cmdFiling.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFiling.Location = new System.Drawing.Point(708, 29);
            this.cmdFiling.Name = "cmdFiling";
            this.cmdFiling.Size = new System.Drawing.Size(92, 24);
            this.cmdFiling.TabIndex = 4;
            this.cmdFiling.Text = "Filing Status";
            this.cmdFiling.Click += new System.EventHandler(this.mnuPayStatus_Click);
            // 
            // cmdComments
            // 
            this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComments.Location = new System.Drawing.Point(806, 29);
            this.cmdComments.Name = "cmdComments";
            this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComments.Size = new System.Drawing.Size(86, 24);
            this.cmdComments.TabIndex = 5;
            this.cmdComments.Text = "Comments";
            this.cmdComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // cmdTemporaryOverrides
            // 
            this.cmdTemporaryOverrides.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdTemporaryOverrides.Location = new System.Drawing.Point(898, 29);
            this.cmdTemporaryOverrides.Name = "cmdTemporaryOverrides";
            this.cmdTemporaryOverrides.Size = new System.Drawing.Size(152, 24);
            this.cmdTemporaryOverrides.TabIndex = 6;
            this.cmdTemporaryOverrides.Text = "Temporary Overrides";
            this.cmdTemporaryOverrides.Click += new System.EventHandler(this.mnuTemporaryOverrides_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(462, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(80, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Save";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmEmployeeMaster
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeMaster";
            this.Text = "Employee Add / Update";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeeMaster_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeMaster_Activated);
            this.Resize += new System.EventHandler(this.frmEmployeeMaster_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmployeeMaster_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeMaster_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFTOrPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMQGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2StatutoryEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2DefIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkW2Pen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTerminationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRetirementDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkW4MultJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMedicare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnemploymentExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFicaExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWorkersCompExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstCheckOnly_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstCheckOnly_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraComments)).EndInit();
            this.fraComments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFiling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdTemporaryOverrides)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdNew;
		private FCButton cmdSelect;
		private FCButton cmdComments;
		private FCButton cmdFiling;
		private FCButton cmdPay;
		private FCButton cmdTemporaryOverrides;
		private FCButton cmdProcess;
        public FCLabel lblOtherIncome;
        public FCLabel lblOtherDeductions;
        public FCTextBox txtFederalOtherIncome;
        public FCTextBox txtFederalAdditionalDeduction;
        public FCCheckBox chkW4MultJobs;
        public T2KDateBox txtW4Date;
        public FCLabel lblW4Date;
        public FCTextBox txtFederalOtherDependents;
        public FCLabel lblOtherDependents;
    }
}
