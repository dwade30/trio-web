﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptElectronicC1.
	/// </summary>
	public partial class rptElectronicC1 : BaseSectionReport
	{
		public rptElectronicC1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptElectronicC1 InstancePtr
		{
			get
			{
				return (rptElectronicC1)Sys.GetInstance(typeof(rptElectronicC1));
			}
		}

		protected rptElectronicC1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
        // nObj = 1
        //   0	rptElectronicC1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        //=========================================================
        StreamReader ts;
		string strRec = "";
		bool boolUCOV;
		double dblUCOVRate;

		public void Init(string strFileName, bool modalDialog, bool boolOverride = false, double dblOVRate = 0)
		{
			FCFileSystem fso = new FCFileSystem();
			boolUCOV = boolOverride;
			dblUCOVRate = dblOVRate;
			dblUCOVRate = dblOVRate / 100;
			if (!FCFileSystem.FileExists(strFileName))
			{
				MessageBox.Show(strFileName + " not found", "Bad Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			if (fecherFoundation.Strings.UCase(strFileName) == "RTNWAGESCH.TXT")
			{
				txtsubtitle.Text = "School Employees";
			}
			else
			{
				txtsubtitle.Text = "";
			}
			ts = FCFileSystem.OpenText(strFileName);
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "ElectronicC1", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = ts.EndOfStream;
			if (!eArgs.EOF)
			{
				strRec = ts.ReadLine();
				if (strRec.Length > 0)
				{
					if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "S")
					{
						this.Fields["grpHeader"].Value = "S";
					}
					else
					{
						this.Fields["grpHeader"].Value = "";
					}
				}
				else
				{
					this.Fields["grpHeader"].Value = "";
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			ts.Close();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!ts.EndOfStream)
			{
				if (strRec.Length > 0)
				{
					if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "A")
					{
						// transmitter record
						SubReport1.Report = new srptC1TransmitterRecord();
						SubReport1.Report.UserData = strRec;
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "B")
					{
						// authorization record
						SubReport1.Report = new srptC1AuthorizationRecord();
						SubReport1.Report.UserData = strRec;
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "E")
					{
						// Employer Record
						SubReport1.Report = new srptC1EmployerRecord();
						SubReport1.Report.UserData = strRec;
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "S")
					{
						// Employee Record
						SubReport1.Report = new srptC1EmployeeRecord();
						SubReport1.Report.UserData = strRec;
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "T")
					{
						// Total Record
						SubReport1.Report = new srptC1TotalRecord();
						if (!boolUCOV)
						{
							SubReport1.Report.UserData = strRec;
						}
						else
						{
							SubReport1.Report.UserData = strRec + Strings.Format(dblUCOVRate, "000.000");
						}
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "R")
					{
						// Reconciliation Record
						SubReport1.Report = new srptC1ReconciliationlRecord();
						SubReport1.Report.UserData = strRec;
					}
					else if (fecherFoundation.Strings.UCase(Strings.Left(strRec, 1)) == "F")
					{
						// Final Record
						SubReport1.Report = null;
						Detail.Visible = false;
					}
					else
					{
						SubReport1.Report = null;
						Detail.Visible = false;
					}
				}
				else
				{
					SubReport1.Report = null;
					Detail.Visible = false;
				}
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Fields["grpHeader"].Value)) == "S")
			{
				txtExcess.Visible = true;
				txtStateGross.Visible = true;
				txtStateTax.Visible = true;
				txtTaxable.Visible = true;
				txtUGross.Visible = true;
			}
			else
			{
				txtExcess.Visible = false;
				txtStateGross.Visible = false;
				txtStateTax.Visible = false;
				txtTaxable.Visible = false;
				txtUGross.Visible = false;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}



		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
