//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmGetScheduleTime.
	/// </summary>
	partial class frmGetScheduleTime
	{
		public fecherFoundation.FCComboBox cmbScheduled;
		public fecherFoundation.FCFrame framScheduled;
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCTextBox txtLunchTime;
		public fecherFoundation.FCComboBox cmbToAMPM;
		public fecherFoundation.FCComboBox cmbToMinute;
		public fecherFoundation.FCComboBox cmbToHour;
		public fecherFoundation.FCComboBox cmbFromAMPM;
		public fecherFoundation.FCComboBox cmbFromMinute;
		public fecherFoundation.FCComboBox cmbFromHour;
		public FCGrid gridAccount;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbScheduled = new fecherFoundation.FCComboBox();
            this.framScheduled = new fecherFoundation.FCFrame();
            this.cmbAccount = new fecherFoundation.FCComboBox();
            this.txtLunchTime = new fecherFoundation.FCTextBox();
            this.cmbToAMPM = new fecherFoundation.FCComboBox();
            this.cmbToMinute = new fecherFoundation.FCComboBox();
            this.cmbToHour = new fecherFoundation.FCComboBox();
            this.cmbFromAMPM = new fecherFoundation.FCComboBox();
            this.cmbFromMinute = new fecherFoundation.FCComboBox();
            this.cmbFromHour = new fecherFoundation.FCComboBox();
            this.gridAccount = new fecherFoundation.FCGrid();
            this.lblDefaultAccount = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framScheduled)).BeginInit();
            this.framScheduled.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 383);
            this.BottomPanel.Size = new System.Drawing.Size(511, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbScheduled);
            this.ClientArea.Controls.Add(this.framScheduled);
            this.ClientArea.Size = new System.Drawing.Size(511, 323);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(511, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(184, 30);
            this.HeaderText.Text = "Edit Shift Times";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbScheduled
            // 
            this.cmbScheduled.AutoSize = false;
            this.cmbScheduled.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbScheduled.FormattingEnabled = true;
            this.cmbScheduled.Items.AddRange(new object[] {
            "Scheduled/Worked",
            "Not Scheduled/Worked"});
            this.cmbScheduled.Location = new System.Drawing.Point(30, 30);
            this.cmbScheduled.Name = "cmbScheduled";
            this.cmbScheduled.Size = new System.Drawing.Size(284, 40);
            this.cmbScheduled.TabIndex = 0;
            this.cmbScheduled.Text = "Scheduled/Worked";
            this.ToolTip1.SetToolTip(this.cmbScheduled, null);
            this.cmbScheduled.SelectedIndexChanged += new System.EventHandler(this.optScheduled_CheckedChanged);
            // 
            // framScheduled
            // 
            this.framScheduled.AppearanceKey = "groupBoxNoBorders";
            this.framScheduled.Controls.Add(this.cmbAccount);
            this.framScheduled.Controls.Add(this.txtLunchTime);
            this.framScheduled.Controls.Add(this.cmbToAMPM);
            this.framScheduled.Controls.Add(this.cmbToMinute);
            this.framScheduled.Controls.Add(this.cmbToHour);
            this.framScheduled.Controls.Add(this.cmbFromAMPM);
            this.framScheduled.Controls.Add(this.cmbFromMinute);
            this.framScheduled.Controls.Add(this.cmbFromHour);
            this.framScheduled.Controls.Add(this.gridAccount);
            this.framScheduled.Controls.Add(this.lblDefaultAccount);
            this.framScheduled.Controls.Add(this.Label5);
            this.framScheduled.Controls.Add(this.Label4);
            this.framScheduled.Controls.Add(this.Label3);
            this.framScheduled.Controls.Add(this.Label2);
            this.framScheduled.Controls.Add(this.Label1);
            this.framScheduled.Location = new System.Drawing.Point(30, 90);
            this.framScheduled.Name = "framScheduled";
            this.framScheduled.Size = new System.Drawing.Size(453, 222);
            this.framScheduled.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.framScheduled, null);
            // 
            // cmbAccount
            // 
            this.cmbAccount.AutoSize = false;
            this.cmbAccount.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAccount.FormattingEnabled = true;
            this.cmbAccount.Location = new System.Drawing.Point(204, 180);
            this.cmbAccount.Name = "cmbAccount";
            this.cmbAccount.Size = new System.Drawing.Size(80, 40);
            this.cmbAccount.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.cmbAccount, null);
            this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.cmbAccount_SelectedIndexChanged);
            // 
            // txtLunchTime
            // 
            this.txtLunchTime.AutoSize = false;
            this.txtLunchTime.BackColor = System.Drawing.SystemColors.Window;
            this.txtLunchTime.LinkItem = null;
            this.txtLunchTime.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLunchTime.LinkTopic = null;
            this.txtLunchTime.Location = new System.Drawing.Point(204, 120);
            this.txtLunchTime.Name = "txtLunchTime";
            this.txtLunchTime.Size = new System.Drawing.Size(80, 40);
            this.txtLunchTime.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtLunchTime, "Amount of time to subtract for a lunch break");
            // 
            // cmbToAMPM
            // 
            this.cmbToAMPM.AutoSize = false;
            this.cmbToAMPM.BackColor = System.Drawing.SystemColors.Window;
            this.cmbToAMPM.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbToAMPM.FormattingEnabled = true;
            this.cmbToAMPM.Location = new System.Drawing.Point(294, 60);
            this.cmbToAMPM.Name = "cmbToAMPM";
            this.cmbToAMPM.Size = new System.Drawing.Size(80, 40);
            this.cmbToAMPM.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.cmbToAMPM, null);
            // 
            // cmbToMinute
            // 
            this.cmbToMinute.AutoSize = false;
            this.cmbToMinute.BackColor = System.Drawing.SystemColors.Window;
            this.cmbToMinute.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbToMinute.FormattingEnabled = true;
            this.cmbToMinute.Location = new System.Drawing.Point(204, 60);
            this.cmbToMinute.Name = "cmbToMinute";
            this.cmbToMinute.Size = new System.Drawing.Size(80, 40);
            this.cmbToMinute.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cmbToMinute, null);
            // 
            // cmbToHour
            // 
            this.cmbToHour.AutoSize = false;
            this.cmbToHour.BackColor = System.Drawing.SystemColors.Window;
            this.cmbToHour.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbToHour.FormattingEnabled = true;
            this.cmbToHour.Location = new System.Drawing.Point(101, 60);
            this.cmbToHour.Name = "cmbToHour";
            this.cmbToHour.Size = new System.Drawing.Size(80, 40);
            this.cmbToHour.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.cmbToHour, null);
            // 
            // cmbFromAMPM
            // 
            this.cmbFromAMPM.AutoSize = false;
            this.cmbFromAMPM.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFromAMPM.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFromAMPM.FormattingEnabled = true;
            this.cmbFromAMPM.Location = new System.Drawing.Point(294, 0);
            this.cmbFromAMPM.Name = "cmbFromAMPM";
            this.cmbFromAMPM.Size = new System.Drawing.Size(80, 40);
            this.cmbFromAMPM.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbFromAMPM, null);
            // 
            // cmbFromMinute
            // 
            this.cmbFromMinute.AutoSize = false;
            this.cmbFromMinute.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFromMinute.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFromMinute.FormattingEnabled = true;
            this.cmbFromMinute.Location = new System.Drawing.Point(204, 0);
            this.cmbFromMinute.Name = "cmbFromMinute";
            this.cmbFromMinute.Size = new System.Drawing.Size(80, 40);
            this.cmbFromMinute.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbFromMinute, null);
            // 
            // cmbFromHour
            // 
            this.cmbFromHour.AutoSize = false;
            this.cmbFromHour.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFromHour.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFromHour.FormattingEnabled = true;
            this.cmbFromHour.Location = new System.Drawing.Point(101, 0);
            this.cmbFromHour.Name = "cmbFromHour";
            this.cmbFromHour.Size = new System.Drawing.Size(80, 40);
            this.cmbFromHour.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbFromHour, null);
            // 
            // gridAccount
            // 
            this.gridAccount.AllowSelection = false;
            this.gridAccount.AllowUserToResizeColumns = false;
            this.gridAccount.AllowUserToResizeRows = false;
            this.gridAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.gridAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.BackColorSel = System.Drawing.Color.Empty;
            this.gridAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridAccount.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridAccount.ColumnHeadersHeight = 30;
            this.gridAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridAccount.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridAccount.DragIcon = null;
            this.gridAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridAccount.ExtendLastCol = true;
            this.gridAccount.FixedCols = 0;
            this.gridAccount.FixedRows = 0;
            this.gridAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.FrozenCols = 0;
            this.gridAccount.GridColor = System.Drawing.Color.Empty;
            this.gridAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.gridAccount.Location = new System.Drawing.Point(294, 180);
            this.gridAccount.Name = "gridAccount";
            this.gridAccount.OutlineCol = 0;
            this.gridAccount.RowHeadersVisible = false;
            this.gridAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridAccount.RowHeightMin = 0;
            this.gridAccount.Rows = 1;
            this.gridAccount.ScrollTipText = null;
            this.gridAccount.ShowColumnVisibilityMenu = false;
            this.gridAccount.Size = new System.Drawing.Size(159, 42);
            this.gridAccount.StandardTab = true;
            this.gridAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridAccount.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.gridAccount, null);
            this.gridAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.gridAccount_KeyPressEdit);
            // 
            // lblDefaultAccount
            // 
            this.lblDefaultAccount.Location = new System.Drawing.Point(0, 194);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(115, 18);
            this.lblDefaultAccount.TabIndex = 17;
            this.lblDefaultAccount.Text = "DEFAULT ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblDefaultAccount, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(101, 134);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(42, 15);
            this.Label5.TabIndex = 14;
            this.Label5.Text = "LUNCH";
            this.ToolTip1.SetToolTip(this.Label5, "Amount of time to subtract for a lunch break");
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(0, 74);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(42, 15);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "TO";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(191, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(3, 16);
            this.Label3.TabIndex = 9;
            this.Label3.Text = ":";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(0, 14);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(42, 15);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "FROM";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(191, 14);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(3, 16);
            this.Label1.TabIndex = 4;
            this.Label1.Text = ":";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(163, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmGetScheduleTime
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(511, 491);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGetScheduleTime";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Shift Times";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmGetScheduleTime_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetScheduleTime_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framScheduled)).EndInit();
            this.framScheduled.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}