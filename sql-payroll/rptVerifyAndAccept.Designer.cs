﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptVerifyAndAccept.
	/// </summary>
	partial class rptVerifyAndAccept
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVerifyAndAccept));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtData3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtData4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtData1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtData2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.LineTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtData3,
				this.txtData4,
				this.txtData1,
				this.txtData2,
				this.LineTotals,
				this.Line1
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime
			});
			this.PageHeader.Height = 1.125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "VERIFY AND ACCEPT";
			this.Label1.Top = 0.04166667F;
			this.Label1.Width = 9.78125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.08333334F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.40625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.40625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtData3
			// 
			this.txtData3.Height = 0.2083333F;
			this.txtData3.Left = 4.84375F;
			this.txtData3.Name = "txtData3";
			this.txtData3.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtData3.Text = null;
			this.txtData3.Top = 0F;
			this.txtData3.Width = 2.125F;
			// 
			// txtData4
			// 
			this.txtData4.Height = 0.2083333F;
			this.txtData4.Left = 7.09375F;
			this.txtData4.Name = "txtData4";
			this.txtData4.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtData4.Text = null;
			this.txtData4.Top = 0F;
			this.txtData4.Width = 2.8125F;
			// 
			// txtData1
			// 
			this.txtData1.Height = 0.2083333F;
			this.txtData1.Left = 0.0625F;
			this.txtData1.Name = "txtData1";
			this.txtData1.Style = "font-family: \'Tahoma\'";
			this.txtData1.Text = null;
			this.txtData1.Top = 0F;
			this.txtData1.Width = 2.59375F;
			// 
			// txtData2
			// 
			this.txtData2.Height = 0.2083333F;
			this.txtData2.Left = 2.71875F;
			this.txtData2.Name = "txtData2";
			this.txtData2.Style = "font-family: \'Tahoma\'";
			this.txtData2.Text = null;
			this.txtData2.Top = 0F;
			this.txtData2.Width = 2.0625F;
			// 
			// LineTotals
			// 
			this.LineTotals.Height = 0F;
			this.LineTotals.Left = 8.3125F;
			this.LineTotals.LineWeight = 1F;
			this.LineTotals.Name = "LineTotals";
			this.LineTotals.Top = 0F;
			this.LineTotals.Visible = false;
			this.LineTotals.Width = 1.5625F;
			this.LineTotals.X1 = 8.3125F;
			this.LineTotals.X2 = 9.875F;
			this.LineTotals.Y1 = 0F;
			this.LineTotals.Y2 = 0F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.2083333F;
			this.Line1.Width = 9.875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 9.875F;
			this.Line1.Y1 = 0.2083333F;
			this.Line1.Y2 = 0.2083333F;
			// 
			// rptVerifyAndAccept
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.916667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtData2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtData3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtData4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtData1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtData2;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
