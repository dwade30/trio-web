//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frm941TaxLiability.
	/// </summary>
	partial class frm941TaxLiability
	{
		public fecherFoundation.FCGrid vsBanks;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdSave1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsBanks = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdExit = new fecherFoundation.FCButton();
            this.cmdSave1 = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave1);
            this.BottomPanel.Location = new System.Drawing.Point(0, 495);
            this.BottomPanel.Size = new System.Drawing.Size(732, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsBanks);
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Controls.Add(this.cmdNew);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Size = new System.Drawing.Size(732, 435);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(732, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(212, 30);
            this.HeaderText.Text = "Verify Tax Liability";
            // 
            // vsBanks
            // 
            this.vsBanks.AllowSelection = false;
            this.vsBanks.AllowUserToResizeColumns = false;
            this.vsBanks.AllowUserToResizeRows = false;
            this.vsBanks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBanks.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBanks.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBanks.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBanks.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.BackColorSel = System.Drawing.Color.Empty;
            this.vsBanks.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsBanks.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsBanks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsBanks.ColumnHeadersHeight = 30;
            this.vsBanks.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBanks.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsBanks.DragIcon = null;
            this.vsBanks.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsBanks.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBanks.ExtendLastCol = true;
            this.vsBanks.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.FrozenCols = 0;
            this.vsBanks.GridColor = System.Drawing.Color.Empty;
            this.vsBanks.GridColorFixed = System.Drawing.Color.Empty;
            this.vsBanks.Location = new System.Drawing.Point(30, 30);
            this.vsBanks.Name = "vsBanks";
            this.vsBanks.OutlineCol = 0;
            this.vsBanks.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBanks.RowHeightMin = 0;
            this.vsBanks.Rows = 50;
            this.vsBanks.ScrollTipText = null;
            this.vsBanks.ShowColumnVisibilityMenu = false;
            this.vsBanks.ShowFocusCell = false;
            this.vsBanks.Size = new System.Drawing.Size(673, 395);
            this.vsBanks.StandardTab = true;
            this.vsBanks.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsBanks.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsBanks.TabIndex = 0;
            this.vsBanks.CurrentCellChanged += new System.EventHandler(this.vsBanks_RowColChange);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(320, 165);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(114, 25);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "toolbarButton";
            this.cmdSave.Location = new System.Drawing.Point(320, 86);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(114, 25);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(320, 113);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(114, 25);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            // 
            // cmdNew
            // 
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(320, 115);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(114, 25);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.AppearanceKey = "toolbarButton";
            this.cmdRefresh.Location = new System.Drawing.Point(320, 139);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(114, 25);
            this.cmdRefresh.TabIndex = 4;
            this.cmdRefresh.Text = "Refresh";
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "toolbarButton";
            this.cmdExit.Location = new System.Drawing.Point(320, 192);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(114, 25);
            this.cmdExit.TabIndex = 6;
            this.cmdExit.Text = "Exit";
            // 
            // cmdSave1
            // 
            this.cmdSave1.AppearanceKey = "acceptButton";
            this.cmdSave1.Location = new System.Drawing.Point(290, 30);
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave1.Size = new System.Drawing.Size(112, 48);
            this.cmdSave1.TabIndex = 0;
            this.cmdSave1.Text = "Continue";
            this.cmdSave1.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frm941TaxLiability
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(732, 603);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frm941TaxLiability";
            this.Text = "Verify Tax Liability";
            this.Activated += new System.EventHandler(this.frm941TaxLiability_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frm941TaxLiability_KeyPress);
            this.Resize += new System.EventHandler(this.frm941TaxLiability_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}