//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMMAUnemploymentReport.
	/// </summary>
	public partial class rptMMAUnemploymentReport : BaseSectionReport
	{
		public rptMMAUnemploymentReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MMA Unemployment Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMMAUnemploymentReport InstancePtr
		{
			get
			{
				return (rptMMAUnemploymentReport)Sys.GetInstance(typeof(rptMMAUnemploymentReport));
			}
		}

		protected rptMMAUnemploymentReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
				rsFirstInfo?.Dispose();
                rsFirstInfo = null;
				rsSecondInfo?.Dispose();
                rsSecondInfo = null;
				rsThirdInfo?.Dispose();
                rsThirdInfo = null;
				rsTotalPay?.Dispose();
                rsTotalPay = null;
				rsDed?.Dispose();
                rsDed = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMMAUnemploymentReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		//clsDRWrapper rsNatureCodes = new clsDRWrapper();
		int[] intGrandTotalFemales = new int[3 + 1];
		int[] intGrandTotalEmployees = new int[3 + 1];
		int[] intPageTotalEmployees = new int[3 + 1];
		int[] intPageTotalFemales = new int[3 + 1];
		// vbPorter upgrade warning: curPageTotalSeasonal As Decimal	OnWrite(int, Decimal)
		Decimal curPageTotalSeasonal;
		// vbPorter upgrade warning: curGrandTotalSeasonal As Decimal	OnWrite(int, Decimal)
		Decimal curGrandTotalSeasonal;
		// vbPorter upgrade warning: curPageTotalNonSeasonal As Decimal	OnWrite(int, Decimal)
		Decimal curPageTotalNonSeasonal;
		// vbPorter upgrade warning: curGrandTotalNonSeasonal As Decimal	OnWrite(int, Decimal)
		Decimal curGrandTotalNonSeasonal;
		bool blnSummaryPage;
		bool blnShading;
		bool boolRunAgain;
		string strPerson = "";
		int intRecordsPerPage;
		bool[] blnPeriod = new bool[3 + 1];
		double dblTotDed;
		clsDRWrapper rsFirstInfo = new clsDRWrapper();
		clsDRWrapper rsSecondInfo = new clsDRWrapper();
		clsDRWrapper rsThirdInfo = new clsDRWrapper();
		clsDRWrapper rsTotalPay = new clsDRWrapper();
		clsDRWrapper rsDed = new clsDRWrapper();
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// TH
			this.Fields.Add("MaxPageCount");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				if (boolRunAgain)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsEmployeeInfo.MoveNext();
					eArgs.EOF = rsEmployeeInfo.EndOfFile();
					if (eArgs.EOF == true)
					{
						blnSummaryPage = true;
					}
				}
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			intRecordsPerPage = 0;
			if (blnSummaryPage)
			{
				Line1.Visible = false;
				Line4.Visible = false;
				Line5.Visible = false;
				Line6.Visible = false;
				Line7.Visible = false;
				Line8.Visible = false;
				Line9.Visible = false;
				Line10.Visible = false;
				Line11.Visible = false;
				Line12.Visible = false;
				Line13.Visible = false;
				Line14.Visible = false;
				Line26.Visible = false;
				Line27.Visible = false;
				Label46.Visible = false;
				Label48.Visible = false;
				lblSeasonal.Visible = false;
				lblFirst.Visible = false;
				lblSecond.Visible = false;
				lblThird.Visible = false;
				Shape3.Visible = false;
				Shape4.Visible = false;
				Shape5.Visible = false;
				Line23.Visible = false;
				Line24.Visible = false;
				Line25.Visible = false;
				lblPageSummary1.Visible = false;
				lblPageSummary2.Visible = false;
				lblPageSummary3.Visible = false;
				lblPageSummary4.Visible = false;
				fldPageTotalAllOne.Visible = false;
				fldPageTotalAllThree.Visible = false;
				fldPageTotalAllTwo.Visible = false;
				fldPageTotalFemOne.Visible = false;
				fldPageTotalFemThree.Visible = false;
				fldPageTotalFemTwo.Visible = false;
				fldPageTotalNonSeasonal.Visible = false;
				fldPageTotalSeasonal.Visible = false;
				Line1.X1 = lblFirst.Left;
				Line1.X2 = lblSeasonal.Left + lblSeasonal.Width;
				Label9.Visible = false;
				Label39.Visible = false;
				Label10.Visible = false;
				Label38.Visible = false;
				Label11.Visible = false;
				Label13.Visible = false;
				Label37.Visible = false;
				Label40.Visible = false;
				Label41.Visible = false;
				Label45.Visible = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{

			int counter;
			modDavesSweetCode.Statics.blnReportStarted = true;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label4.Text = Strings.Format(DateTime.Today, "m/d/yy") + "    " + Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			intRecordsPerPage = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				intPageTotalEmployees[counter] = 0;
				intPageTotalFemales[counter] = 0;
				intGrandTotalEmployees[counter] = 0;
				intGrandTotalFemales[counter] = 0;
			}
			curPageTotalSeasonal = 0;
			curPageTotalNonSeasonal = 0;
			curGrandTotalSeasonal = 0;
			curGrandTotalNonSeasonal = 0;
			blnSummaryPage = false;
			blnShading = true;
			if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 0)
			{
				// matthew call id 81152
				// rsEmployeeInfo.OpenRecordset "SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistU as DistU, tblEmployeeMaster.SSN as SSN, left(tblEmployeeMaster.Sex, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM ((tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.UnemploymentExempt = 0 AND ((tblMiscUpdate.Unemployment)<>'N' And (tblMiscUpdate.Unemployment)<>'' And (tblMiscUpdate.Unemployment)<>'_') AND (trim(tblCheckDetail.DistU) <> '' AND trim(tblCheckDetail.DistU) <> 'N') " &
				rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistU as DistU, tblEmployeeMaster.SSN as SSN, substring(tblEmployeeMaster.Sex,1, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM ((tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE ((tblMiscUpdate.Unemployment) <> 'N' And (tblMiscUpdate.Unemployment) <> '' And (tblMiscUpdate.Unemployment) <> '_') AND (tblCheckDetail.DistU <> '' AND tblCheckDetail.DistU <> 'N') " + "GROUP BY tblEmployeeMaster.EmployeeNumber, tblCheckDetail.DistU, tblEmployeeMaster.SSN, substring(tblEmployeeMaster.Sex,1, 1), tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, tblEmployeeMaster.MiddleName, tblMiscUpdate.Unemployment, tblMiscUpdate.NatureCode Order by tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,distu desc");
			}
			else if (frmUnemploymentReport.InstancePtr.cboSequenceRange.SelectedIndex == 1)
                {
                    rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistU as DistU, tblEmployeeMaster.SSN as SSN, substring(tblEmployeeMaster.Sex,1, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM ((tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.SeqNumber = " + frmUnemploymentReport.InstancePtr.cboSingleSequence.Text + " AND tblEmployeeMaster.UnemploymentExempt = 0 AND ((tblMiscUpdate.Unemployment) <> 'N' And (tblMiscUpdate.Unemployment) <> '' And (tblMiscUpdate.Unemployment) <> '_') AND (rtrim(tblCheckDetail.DistU) <> '' AND rtrim(tblCheckDetail.DistU) <> 'N') " + "GROUP BY tblEmployeeMaster.EmployeeNumber, tblCheckDetail.DistU, tblEmployeeMaster.SSN, substring(tblEmployeeMaster.Sex,1, 1), tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, tblEmployeeMaster.MiddleName, tblMiscUpdate.Unemployment, tblMiscUpdate.NatureCode Order by tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,distu desc");
                }
                else
                {
                    rsEmployeeInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber as EmployeeNumber, tblCheckDetail.DistU as DistU, tblEmployeeMaster.SSN as SSN, substring(tblEmployeeMaster.Sex,1, 1) as Sex, tblEmployeeMaster.FirstName as FirstName, tblEmployeeMaster.LastName as LastName, tblEmployeeMaster.MiddleName as MiddleName, tblMiscUpdate.Unemployment as Unemployment, tblMiscUpdate.NatureCode as NatureCode FROM ((tblEmployeeMaster INNER JOIN tblMiscUpdate ON tblEmployeeMaster.EmployeeNumber = tblMiscUpdate.EmployeeNumber) INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.SeqNumber >= " + frmUnemploymentReport.InstancePtr.cboStartSequence.Text + " AND tblEmployeeMaster.SeqNumber <= " + frmUnemploymentReport.InstancePtr.cboEndSequence.Text + " AND tblEmployeeMaster.UnemploymentExempt = 0  AND ((tblMiscUpdate.Unemployment) <> 'N' And (tblMiscUpdate.Unemployment)<>'' And (tblMiscUpdate.Unemployment) <> '_')" + "AND (rtrim(tblCheckDetail.DistU) <> '' AND rtrim(tblCheckDetail.DistU) <> 'N') GROUP BY tblEmployeeMaster.EmployeeNumber, tblCheckDetail.DistU, tblEmployeeMaster.SSN, substring(tblEmployeeMaster.Sex,1, 1), tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, tblEmployeeMaster.MiddleName, tblMiscUpdate.Unemployment, tblMiscUpdate.NatureCode Order by tblEmployeeMaster.LastName,tblEmployeeMaster.FirstName,distu desc");
                }
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Cancel();
				this.Close();
				return;
			}
			lblFirst.Text = Strings.Format(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text, "MMM");
			lblSecond.Text = Strings.Format(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text, "MMM");
			lblThird.Text = Strings.Format(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text, "MMM");
			fldReportNumber.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtReportNumber.Text);
			fldEmployer.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtEmployer.Text);
			fldQuarter.Text = frmUnemploymentReport.InstancePtr.cboQuarter.Text + "/" + frmUnemploymentReport.InstancePtr.cboYear.Text;
			fldPreparer.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtPreparer.Text);
			fldTelephoneNumber.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtTelephone.Text);
			fldFederalID.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtFederalID.Text);
			fldStateID.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtStateID.Text);
			fldMemberCode.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtMemberCode.Text);
			fldSeasonalCode.Text = fecherFoundation.Strings.Trim(frmUnemploymentReport.InstancePtr.txtSeasonalCode.Text);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("MMA");
				frmUnemploymentReport.InstancePtr.Unload();
			}
			// frmUnemploymentReport.Show , MDIParent
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			
			Decimal curPayTotal;
			DateTime datLowDate;
			DateTime datHighDate;
			bool boolSamePerson = false;
			double dblTemp;
			double dblTempDed = 0;
			double dblFirstDed;
			double dblSecondDed;
			double dblThirdDed;
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			if (Line29.Visible == false)
			{
				fldNature.Visible = true;
				fldSocialSecurity.Visible = true;
				fldFirst.Visible = true;
				fldLast.Visible = true;
				fldMI.Visible = true;
				fldOne.Visible = true;
				fldTwo.Visible = true;
				fldThree.Visible = true;
				fldSeasonal.Visible = true;
				fldNonseasonal.Visible = true;
				fldFem.Visible = true;
				Line15.Visible = true;
				Line16.Visible = true;
				Line17.Visible = true;
				Line18.Visible = true;
				Line19.Visible = true;
				Line20.Visible = true;
				Line21.Visible = true;
				Line22.Visible = true;
				Line28.Visible = true;
				Line29.Visible = true;
			}
			else
			{
				if (!blnSummaryPage)
				{
					if (blnShading)
					{
						blnShading = false;
						this.Detail.BackColor = ColorTranslator.FromOle(0xF3F3F2);
						// &HEDEDEC
					}
					else
					{
						blnShading = true;
						this.Detail.BackColor = Color.White;
					}
				}
			}
			curPayTotal = 0;
			// If rsEmployeeInfo.Fields("EmployeeNumber") = "320" Then MsgBox "A"
			datLowDate = modDavesSweetCode.FindFirstDay(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text);
			datHighDate = modDavesSweetCode.FindLastDay(FCConvert.ToString(modDavesSweetCode.FindFirstDay(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)));
			rsFirstInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE rtrim(DistU) = '" + rsEmployeeInfo.Get_Fields_String("DistU") + "' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[0].Text)) + "' AND CheckVoid = 0");
			rsDed.OpenRecordset("SELECT SUM(Dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[0].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[0].Text)) + "' AND checkvoid = 0 ", "twpy0000.vb1");
			dblFirstDed = 0;
			if (!rsDed.EndOfFile())
			{
				dblFirstDed = Conversion.Val(rsDed.Get_Fields("totded"));
			}
			rsSecondInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE rtrim(DistU) = '" + rsEmployeeInfo.Get_Fields_String("DistU") + "' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[1].Text)) + "' AND CheckVoid = 0");
			rsDed.OpenRecordset("SELECT SUM(Dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[1].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[1].Text)) + "' AND checkvoid = 0 ", "twpy0000.vb1");
			dblSecondDed = 0;
			if (!rsDed.EndOfFile())
			{
				dblSecondDed = Conversion.Val(rsDed.Get_Fields("totded"));
			}
			rsThirdInfo.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE rtrim(DistU) = '" + rsEmployeeInfo.Get_Fields_String("DistU") + "' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[2].Text)) + "' AND CheckVoid = 0");
			rsDed.OpenRecordset("SELECT SUM(Dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbStartDate[2].Text)) + "' AND PayDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmUnemploymentReport.InstancePtr.cmbEndDate[2].Text)) + "' AND checkvoid = 0 ", "twpy0000.vb1");
			dblThirdDed = 0;
			if (!rsDed.EndOfFile())
			{
				dblThirdDed = Conversion.Val(rsDed.Get_Fields("totded"));
			}
			// MATTHEW 4/9/04
			// rsTotalPay.OpenRecordset "SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE rtrim(DistU) = '" & rsEmployeeInfo.Fields("DistU") & "' AND EmployeeNumber = '" & rsEmployeeInfo.Fields("EmployeeNumber") & "' AND PayDate >= '" & datLowDate & "' AND PayDate <= '" & datHighDate & "'"
			rsTotalPay.OpenRecordset("SELECT SUM(DistGrossPay) as GrossPayTotal FROM tblCheckDetail WHERE rtrim(DistU) = '" + rsEmployeeInfo.Get_Fields_String("DistU") + "' AND EmployeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND not MSRSadjustrecord = 1 and checkvoid = 0 and distributionrecord = 1");
			if (FCConvert.ToString(rsEmployeeInfo.Get_Fields("Unemployment")) == "Y" || FCConvert.ToString(rsEmployeeInfo.Get_Fields("Unemployment")) == "S")
			{
				rsDed.OpenRecordset("SELECT SUM(dedamount) as totded FROM tblCheckDetail WHERE uexempt = 1 and deductionrecord = 1 and employeeNumber = '" + rsEmployeeInfo.Get_Fields("EmployeeNumber") + "' AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'  AND checkvoid = 0 ", "twpy0000.vb1");
				// dblTotDed = 0
				dblTempDed = 0;
				if (!rsDed.EndOfFile())
				{
					// dblTotDed = Val(rsDed.Fields("totded"))
					dblTempDed = Conversion.Val(rsDed.Get_Fields("totded"));
				}
				if (Conversion.Val(rsTotalPay.Get_Fields("GrossPayTotal")) - dblTotDed == 0)
				{
					fldNature.Visible = false;
					fldSocialSecurity.Visible = false;
					fldFirst.Visible = false;
					fldLast.Visible = false;
					fldMI.Visible = false;
					fldOne.Visible = false;
					fldTwo.Visible = false;
					fldThree.Visible = false;
					fldSeasonal.Visible = false;
					fldNonseasonal.Visible = false;
					fldFem.Visible = false;
					Line15.Visible = false;
					Line16.Visible = false;
					Line17.Visible = false;
					Line18.Visible = false;
					Line19.Visible = false;
					Line20.Visible = false;
					Line21.Visible = false;
					Line22.Visible = false;
					Line28.Visible = false;
					Line29.Visible = false;
					return;
				}
			}

            var strTempSSN = Strings.Format(rsEmployeeInfo.Get_Fields_String("SSN"), "000-00-0000"); ;			
            switch (ssnViewPermission)
            {
                case SSNViewType.Full:
                    fldSocialSecurity.Text = Strings.Left(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("SSN")), 3) + "-" + Strings.Mid(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("SSN")), 4, 2) + "-" + Strings.Right(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("SSN")), 4);
                    break;
                case SSNViewType.Masked:
                    fldSocialSecurity.Text = "***-**-" + "-" + rsEmployeeInfo.Get_Fields_String("SSN").Right(4);
                    break;
                default:
                    fldSocialSecurity.Text = "***-**-****";
                    break;
            }
            boolSamePerson = strTempSSN == strPerson;
			strPerson = strTempSSN;
			if (!boolSamePerson)
				dblTotDed = dblTempDed;
			intRecordsPerPage += 1;
			if (intRecordsPerPage == 25)
			{
				this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("DistU"))).Length > 1)
			{
				fldNature.Text = Strings.Right(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("DistU"))), fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("DistU"))).Length - 1);
			}
			else
			{
				fldNature.Text = rsEmployeeInfo.Get_Fields_String("NatureCode");
			}
			fldFirst.Text = fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName")), VbStrConv.ProperCase);
			fldLast.Text = fecherFoundation.Strings.StrConv(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName")), VbStrConv.ProperCase);
			fldMI.Text = fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.UCase(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) + " ", 1));
			if (!boolSamePerson)
			{
				blnPeriod[1] = false;
				blnPeriod[2] = false;
				blnPeriod[3] = false;
			}
			if (rsFirstInfo.EndOfFile() != true && rsFirstInfo.BeginningOfFile() != true)
			{
				if (Conversion.Val(rsFirstInfo.Get_Fields("GrossPayTotal")) > 0)
				{
					fldOne.Text = "X";
					if (!boolSamePerson || blnPeriod[1] == false)
					{
						intPageTotalEmployees[1] += 1;
						intGrandTotalEmployees[1] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intPageTotalFemales[1] += 1;
							intGrandTotalFemales[1] += 1;
						}
						blnPeriod[1] = true;
					}
				}
				else
				{
					fldOne.Text = "";
				}
			}
			else
			{
				fldOne.Text = "";
			}
			if (rsSecondInfo.EndOfFile() != true && rsSecondInfo.BeginningOfFile() != true)
			{
				if (Conversion.Val(rsSecondInfo.Get_Fields("GrossPayTotal")) > 0)
				{
					fldTwo.Text = "X";
					if (!boolSamePerson || blnPeriod[2] == false)
					{
						intPageTotalEmployees[2] += 1;
						intGrandTotalEmployees[2] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intPageTotalFemales[2] += 1;
							intGrandTotalFemales[2] += 1;
						}
						blnPeriod[2] = true;
					}
				}
				else
				{
					fldTwo.Text = "";
				}
			}
			else
			{
				fldTwo.Text = "";
			}
			if (rsThirdInfo.EndOfFile() != true && rsThirdInfo.BeginningOfFile() != true)
			{
				if (Conversion.Val(rsThirdInfo.Get_Fields("GrossPayTotal")) > 0)
				{
					fldThree.Text = "X";
					if (!boolSamePerson || blnPeriod[3] == false)
					{
						intPageTotalEmployees[3] += 1;
						intGrandTotalEmployees[3] += 1;
						if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
						{
							intPageTotalFemales[3] += 1;
							intGrandTotalFemales[3] += 1;
						}
						blnPeriod[3] = true;
					}
				}
				else
				{
					fldThree.Text = "";
				}
			}
			else
			{
				fldThree.Text = "";
			}
			// If .Fields("Unemployment") = "Y" Then
			if (Strings.Left(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("DistU")), 1) == "Y")
			{
				// default is yes
				if (rsTotalPay.EndOfFile() != true && rsTotalPay.BeginningOfFile() != true)
				{
					curPayTotal = FCConvert.ToDecimal(Conversion.Val(rsTotalPay.Get_Fields("GrossPayTotal")) - dblTotDed);
					if (Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal")) < dblTotDed)
					{
						dblTotDed -= Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal"));
					}
					else
					{
						dblTotDed = 0;
					}
				}
				else
				{
					curPayTotal = 0;
				}
				fldSeasonal.Text = "";
				fldNonseasonal.Text = Strings.Format(curPayTotal, "#,##0.00");
				curPageTotalNonSeasonal += curPayTotal;
				curGrandTotalNonSeasonal += curPayTotal;
				// ElseIf .Fields("Unemployment") = "S" Then
			}
			else if (Strings.Left(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("DistU")), 1) == "S")
			{
				if (rsTotalPay.EndOfFile() != true && rsTotalPay.BeginningOfFile() != true)
				{
					curPayTotal = FCConvert.ToDecimal(Conversion.Val(rsTotalPay.Get_Fields("GrossPayTotal")) - dblTotDed);
					if (Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal")) < dblTotDed)
					{
						dblTotDed -= Conversion.Val(rsTotalPay.Get_Fields("grosspaytotal"));
					}
					else
					{
						dblTotDed = 0;
					}
				}
				else
				{
					curPayTotal = 0;
				}
				fldSeasonal.Text = Strings.Format(curPayTotal, "#,##0.00");
				fldNonseasonal.Text = "";
				curPageTotalSeasonal += curPayTotal;
				curGrandTotalSeasonal += curPayTotal;
			}
			else
			{
				fldSeasonal.Text = "";
				fldNonseasonal.Text = Strings.Format(curPayTotal, "#,##0.00");
				curPageTotalNonSeasonal += curPayTotal;
				curGrandTotalNonSeasonal += curPayTotal;
			}
			if (FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Sex")) == "F")
			{
				fldFem.Text = "Y";
			}
			else
			{
				fldFem.Text = "N";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			lblSummary.Text = "SUMMARY for " + fldEmployer.Text;
			lblSummaryFirst.Text = lblFirst.Text;
			lblSummarySecond.Text = lblSecond.Text;
			lblSummaryThird.Text = lblThird.Text;
			fldGrandTotalAllOne.Text = intGrandTotalEmployees[1].ToString();
			fldGrandTotalAllTwo.Text = intGrandTotalEmployees[2].ToString();
			fldGrandTotalAllThree.Text = intGrandTotalEmployees[3].ToString();
			fldGrandTotalFemOne.Text = intGrandTotalFemales[1].ToString();
			fldGrandTotalFemTwo.Text = intGrandTotalFemales[2].ToString();
			fldGrandTotalFemThree.Text = intGrandTotalFemales[3].ToString();
			fldGrandTotalSeasonal.Text = Strings.Format(curGrandTotalSeasonal, "#,##0.00");
			fldGrandTotalNonSeasonal.Text = Strings.Format(curGrandTotalNonSeasonal, "#,##0.00");
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPageTotalAllOne As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalAllTwo As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalAllThree As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalFemOne As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalFemTwo As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalFemThree As object	OnWrite
			// vbPorter upgrade warning: fldPageTotalSeasonal As object	OnWrite(string)
			// vbPorter upgrade warning: fldPageTotalNonSeasonal As object	OnWrite(string)
			int counter;
			fldPageTotalAllOne.Text = intPageTotalEmployees[1].ToString();
			fldPageTotalAllTwo.Text = intPageTotalEmployees[2].ToString();
			fldPageTotalAllThree.Text = intPageTotalEmployees[3].ToString();
			fldPageTotalFemOne.Text = intPageTotalFemales[1].ToString();
			fldPageTotalFemTwo.Text = intPageTotalFemales[2].ToString();
			fldPageTotalFemThree.Text = intPageTotalFemales[3].ToString();
			fldPageTotalSeasonal.Text = Strings.Format(curPageTotalSeasonal, "#,##0.00");
			fldPageTotalNonSeasonal.Text = Strings.Format(curPageTotalNonSeasonal, "#,##0.00");
			for (counter = 0; counter <= 3; counter++)
			{
				intPageTotalEmployees[counter] = 0;
				intPageTotalFemales[counter] = 0;
			}
			curPageTotalSeasonal = 0;
			curPageTotalNonSeasonal = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			PageCounter += 1;
			this.Fields["MaxPageCount"].Value = PageCounter;
			Label3.Text = "Page " + FCConvert.ToString(PageCounter) + " of ";
		}

		
	}
}
