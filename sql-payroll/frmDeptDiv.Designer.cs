//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

#if TWBD0000
using TWBD0000;
#endif
namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmDeptDiv.
	/// </summary>
	partial class frmDeptDiv
	{
		public fecherFoundation.FCGrid vsData;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsData = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 426);
            this.BottomPanel.Size = new System.Drawing.Size(754, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Size = new System.Drawing.Size(754, 366);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(754, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(378, 30);
            this.HeaderText.Text = "Budgetary Departments/Divisions";
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 10;
            this.vsData.ExtendLastCol = true;
            this.vsData.FixedCols = 0;
            this.vsData.Location = new System.Drawing.Point(30, 30);
            this.vsData.Name = "vsData";
            this.vsData.RowHeadersVisible = false;
            this.vsData.Rows = 50;
            this.vsData.Size = new System.Drawing.Size(687, 328);
            this.vsData.DoubleClick += new System.EventHandler(this.vsData_DblClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmDeptDiv
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(754, 534);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmDeptDiv";
            this.Text = "Budgetary Departments/Divisions";
            this.Load += new System.EventHandler(this.frmDeptDiv_Load);
            this.Activated += new System.EventHandler(this.frmDeptDiv_Activated);
            this.Resize += new System.EventHandler(this.frmDeptDiv_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeptDiv_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeptDiv_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}