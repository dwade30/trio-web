//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modCoreysSweeterCode
	{
		//=========================================================
		public const int CNSTPAYCATREGULAR = 1;
		public const int CNSTPAYCATOVERTIME = 2;
		public const int CNSTPAYCATSICK = 3;
		public const int CNSTPAYCATVACATION = 4;
		public const int CNSTPAYCATHOLIDAY = 5;
		public const int CNSTSCHOOLORTOWNBOTH = 0;
		public const int CNSTSCHOOLORTOWNTOWN = 1;
		public const int CNSTSCHOOLORTOWNSCHOOL = 2;
		public const int CNSTACHACCOUNTTYPESAVINGS = 3;
		public const int CNSTACHACCOUNTTYPECHECKING = 2;
		public const int CNSTACHACCOUNTTYPELOAN = 5;
		public const int CNSTACHACCOUNTRECORDREGULAR = 2;
		public const int CNSTACHACCOUNTRECORDPRENOTE = 3;
		public const int CNSTICMAPLANTYPE401 = 1;
		public const int CNSTICMAPLANTYPE457 = 3;
		public const int CNSTICMAPLANTYPEIRA = 7;
		public const int CNSTICMAPLANTYPERHS = 8;
		public const int CNSTMSRSREPORTTYPENONE = 0;
		public const int CNSTMSRSREPORTTYPEPLD = 1;
		public const int CNSTMSRSREPORTTYPESCHOOLPLD = 2;
		public const int CNSTMSRSREPORTTYPETEACHER = 3;
		public const int CNSTMSRSHOURLY = 0;
		public const int CNSTMSRSDAILY = 1;
		public const int CNSTMSRSCONTRACT = 2;
		public const int CNSTDISTMNOMSRS = -3;
		public const int CNSTDISTMDEFAULT = -2;
		public const int CNSTDISTUNONE = 0;
		public const int CNSTDISTUYES = 1;
		public const int CNSTDISTUSEASONAL = 2;
		public const int CNSTMSRSPLANCATEGORYNONE = 0;
		public const int CNSTMSRSPLANCATEGORYFIREFIGHTER = 1;
		public const int CNSTMSRSPLANCATEGORYGENERALGOVERNMENT = 2;
		public const int CNSTMSRSPLANCATEGORYLAWENFORCEMENT = 3;
		public const int CNSTMSRSPLANCATEGORYTEACHER = 4;
		public const int CNSTMSRSPLANCATEGORYTEACHERBEFORE93 = 5;
		public const int CNSTMSRSPLANCATEGORYFIREFIGHTERLAWGOVERNMENT = 6;
		public const int CNSTMSRSFEDCOMPDOLLAR = 0;
		public const int CNSTMSRSFEDCOMPPERCENT = 1;

		public enum MatchLimitType
		{
			Dollars = 1,
			Hours = 2,
			Period = 3,
			MonthToDate = 4,
			QuarterToDate = 5,
			CalendarToDate = 6,
			FiscalToDate = 7,
			Life = 8,
		}

		public struct SchoolObjectCodesRec
		{
			public string[] aryCodes/*?  = new string[12 + 1] */;
			public SchoolObjectCodesRec(int unusedParam)
			{
				this.aryCodes = new string[0];
			}
		};

		public const int CNSTSCHOOLOBJECTCODEDESCRIPTION = 0;
		public const int CNSTSCHOOLOBJECTCODESALARY = 1;
		public const int CNSTSCHOOLOBJECTCODERETIREMENT = 2;
		public const int CNSTSCHOOLOBJECTCODETUITION = 3;
		public const int CNSTSCHOOLOBJECTCODEOTHERBENEFITS = 4;
		public const int CNSTSCHOOLOBJECTCODEGROUPINS = 5;
		public const int CNSTSCHOOLOBJECTCODEOTHERGROUPINS = 6;
		public const int CNSTSCHOOLOBJECTCODESSMED = 7;
		public const int CNSTSCHOOLOBJECTCODEONBEHALF = 8;
		public const int CNSTSCHOOLOBJECTCODEUNEMP = 9;
		public const int CNSTSCHOOLOBJECTCODEWC = 10;
		public const int CNSTSCHOOLOBJECTCODEHEALTHBENEFITS = 11;
		// some constants for the ACH file
		public const int ACHNameLen = 23;
		public const int ACHRTLen = 9;
		public const int ACHEmployerNameLen = 23;
		public const int ACHEmployerIDLen = 9;
		public const int ACHEmployerRTLen = 9;
		public const int ACHEmployerAccountLen = 17;

		public struct ACHInfo
		{
			public string ACHBankName;
			public string ACHBankRT;
			public string ImmediateOriginName;
			public string ImmediateOriginRT;
			public string EmployerName;
			public string EmployerRT;
			public string EmployerAccount;
			public string EmployerID;
			// vbPorter upgrade warning: EffectiveDate As DateTime	OnWrite(string)
			public DateTime EffectiveDate;
			public string ODFI;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ACHInfo(int unusedParam)
			{
				this.ACHBankName =String.Empty;
				this.ACHBankRT = String.Empty;
				this.ImmediateOriginName = String.Empty;
				this.ImmediateOriginRT = String.Empty;
				this.EmployerName = String.Empty;
				this.EmployerRT = String.Empty;
				this.EmployerAccount = String.Empty;
				this.EmployerID = String.Empty;
				this.EffectiveDate = DateTime.FromOADate(0);
				this.ODFI = String.Empty;
			}
		};
		// electronic wage report info
		public const int EWRNameLen = 50;
		public const int EWRAddressLen = 40;
		public const int EWRCityLen = 25;
		public const int EWRReturnNameLen = 44;
		public const int EWRReturnAddressLen = 35;
		public const int EWRReturnCityLen = 20;
		public const int EWRFederalIDLen = 9;
		public const int EWRStateUCLen = 10;
		public const int EWRMRSWithholdingLen = 11;
		public const int EWRTransmitterTitleLen = 30;
		public const int EWRTransmitterExtLen = 4;
		public const int EWRProcessorLicenseCodeLen = 7;

		public struct EWRInfo
		{
			public string EmployerName;
			public string EmployerAddress;
			public string EmployerCity;
			public string EmployerState;
			public int EmployerStateCode;
			public string EmployerZip;
			public string EmployerZip4;
			public string FederalEmployerID;
			public string StateUCAccount;
			public string SchoolStateUCAccount;
			public string MRSWithholdingID;
			public string SchoolMRSWithholdingID;
			public string TransmitterTitle;
			public string TransmitterPhone;
			public string TransmitterExt;
			public string TransmitterName;
			public string ProcessorLicenseCode;
			public string TransmitterEmail;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public EWRInfo(int unusedParam)
			{
				this.EmployerName = String.Empty;
				this.EmployerAddress = String.Empty;
				this.EmployerCity = String.Empty;
				this.EmployerState = String.Empty;
				this.EmployerStateCode = 0;
				this.EmployerZip = String.Empty;
				this.EmployerZip4 = String.Empty;
				this.FederalEmployerID = String.Empty;
				this.StateUCAccount = String.Empty;
				this.SchoolStateUCAccount = String.Empty;
				this.MRSWithholdingID = String.Empty;
				this.SchoolMRSWithholdingID = String.Empty;
				this.TransmitterTitle = String.Empty;
				this.TransmitterPhone = String.Empty;
				this.TransmitterExt = String.Empty;
				this.TransmitterName = String.Empty;
				this.ProcessorLicenseCode = String.Empty;
				this.TransmitterEmail = String.Empty;
			}
		};

		public struct EWRData
		{
			public int lngFirstMonthWorkers;
			public int lngSecondMonthWorkers;
			public int lngThirdMonthWorkers;
			public int lngFirstMonthFemales;
			public int lngSecondMonthFemales;
			public int lngThirdMonthFemales;
			public double dblTotalGrossReportableWages;
			public double dblExcessWages;
			public double dblTotalStateWithheld;
			public double dblTotalPayments;
			public double dblRate;
			public double dblCSSFRate;
            public double dblUPAFRate;
			public double dblLimit;
			public int intQuarter;
			// vbPorter upgrade warning: lngYear As int	OnRead(string)
			public int lngYear;
			// vbPorter upgrade warning: M1Start As DateTime	OnWrite(string)
			public DateTime M1Start;
			// vbPorter upgrade warning: M1End As DateTime	OnWrite(string)
			public DateTime M1End;
			// vbPorter upgrade warning: M2Start As DateTime	OnWrite(string)
			public DateTime M2Start;
			// vbPorter upgrade warning: M2End As DateTime	OnWrite(string)
			public DateTime M2End;
			// vbPorter upgrade warning: M3Start As DateTime	OnWrite(string)
			public DateTime M3Start;
			// vbPorter upgrade warning: M3End As DateTime	OnWrite(string)
			public DateTime M3End;
			public bool PrintTest;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public EWRData(int unusedParam)
			{
				this.lngFirstMonthWorkers = 0;
				this.lngSecondMonthWorkers = 0;
				this.lngThirdMonthWorkers = 0;
				this.lngFirstMonthFemales = 0;
				this.lngSecondMonthFemales = 0;
				this.lngThirdMonthFemales = 0;
				this.dblTotalGrossReportableWages = 0;
				this.dblExcessWages = 0;
				this.dblTotalStateWithheld = 0;
				this.dblTotalPayments = 0;
				this.dblRate = 0;
				this.dblCSSFRate = 0;
                this.dblUPAFRate = 0;
				this.dblLimit = 0;
				this.intQuarter = 0;
				this.lngYear = 0;
				this.M1Start = DateTime.FromOADate(0);
				this.M1End = DateTime.FromOADate(0);
				this.M2Start = DateTime.FromOADate(0);
				this.M2End = DateTime.FromOADate(0);
				this.M3Start = DateTime.FromOADate(0);
				this.M3End = DateTime.FromOADate(0);
				this.PrintTest = false;
			}
		};

		public struct MSRSData
		{
			public string TransmitterCode;
			public string TransmitterName;
			public string TransmitterAddress;
			public string ContactPerson;
			public string Telephone;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public MSRSData(int unusedParam)
			{
				this.TransmitterCode = String.Empty;
				this.TransmitterName = String.Empty;
				this.TransmitterAddress = String.Empty;
				this.ContactPerson = String.Empty;
				this.Telephone = String.Empty;
			}
		};

		public struct MSRSDetailRecord
		{
			public string SSN;
			public string Name;
			public double FullTimeEquivalent;
			public string DateOfBirth;
			public string StatusCode;
			public string PositionCode;
			public string ExcessPaybackStatus;
			public string LifeInsuranceSchedule;
			public string LifeInsuranceCode;
			public string RetirementPlan;
			public double LifeInsuranceLevel;
			public double LifeInsurancePremium;
			public double EarnableCompensation;
			public double EmployeeContributions;
			public double ExcessPaybackContributions;
			public double MonthlyHoursDays;
			public string PayRateCode;
			public double FullTimeWorkWeekDaysHours;
			public int WorkWeeksPerYear;
			public double PayRate;
			public string BenefitPlanCode;
			public int RateSchedule;
			// vbPorter upgrade warning: LastPayDate As DateTime	OnWrite(string)
			public DateTime LastPayDate;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public MSRSDetailRecord(int unusedParam)
			{
				this.SSN = String.Empty;
				this.Name = String.Empty;
				this.FullTimeEquivalent = 0;
				this.DateOfBirth = String.Empty;
				this.StatusCode = String.Empty;
				this.PositionCode = String.Empty;
				this.ExcessPaybackStatus = String.Empty;
				this.LifeInsuranceSchedule = String.Empty;
				this.LifeInsuranceCode = String.Empty;
				this.RetirementPlan = String.Empty;
				this.LifeInsuranceLevel = 0;
				this.LifeInsurancePremium = 0;
				this.EarnableCompensation = 0;
				this.EmployeeContributions = 0;
				this.ExcessPaybackContributions = 0;
				this.MonthlyHoursDays = 0;
				this.PayRateCode = String.Empty;
				this.FullTimeWorkWeekDaysHours = 0;
				this.WorkWeeksPerYear = 0;
				this.PayRate = 0;
				this.BenefitPlanCode = String.Empty;
				this.RateSchedule = 0;
				this.LastPayDate = DateTime.FromOADate(0);
			}
		};

		public struct ICMAPlanRecord1
		{
			// vbPorter upgrade warning: RCPlanNumber As int	OnWrite(string)
			public int RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string IRSNumber;
			public string PlanName;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMAPlanRecord1(int unusedParam)
			{
				this.RCPlanNumber = 0;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.IRSNumber = String.Empty;
				this.PlanName = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct ICMAPlanRecord2
		{
			public string RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string IRSNumber;
			public string TotalRemittance;
			public string FormatCode;
			public string PayrollDate;
			public string TaxYearIndicator;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMAPlanRecord2(int unusedParam)
			{
				this.RCPlanNumber = String.Empty;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.IRSNumber = String.Empty;
				this.TotalRemittance = String.Empty;
				this.FormatCode = String.Empty;
				this.PayrollDate = String.Empty;
				this.TaxYearIndicator = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct ICMAContributionRecord1
		{
			public string RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string ParticipantSSN;
			public string ParticipantName;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMAContributionRecord1(int unusedParam)
			{
				this.RCPlanNumber = String.Empty;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.ParticipantSSN = String.Empty;
				this.ParticipantName = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct ICMAContributionRecord2
		{
			public string RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string FundID;
			public string SourceCode;
			public string ParticipantSSN;
			public string ContributionAmount;
			public string TaxYearIndicator;
			public string LocationID;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMAContributionRecord2(int unusedParam)
			{
				this.RCPlanNumber = String.Empty;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.FundID = String.Empty;
				this.SourceCode = String.Empty;
				this.ParticipantSSN = String.Empty;
				this.ContributionAmount = String.Empty;
				this.TaxYearIndicator = String.Empty;
				this.LocationID = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct ICMALoanRepaymentRecord1
		{
			public string RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string ParticipantSSN;
			public string ParticipantName;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMALoanRepaymentRecord1(int unusedParam)
			{
				this.RCPlanNumber = String.Empty;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.ParticipantSSN = String.Empty;
				this.ParticipantName = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct ICMALoanRepaymentRecord2
		{
			public string RCPlanNumber;
			public string RecordType;
			public string RecordSequence;
			public string LoanNumber;
			public string ParticipantSSN;
			public string LoanRepaymentAmount;
			public string LoanPayoffIndicator;
			public string FormatID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public ICMALoanRepaymentRecord2(int unusedParam)
			{
				this.RCPlanNumber = String.Empty;
				this.RecordType = String.Empty;
				this.RecordSequence = String.Empty;
				this.LoanNumber = String.Empty;
				this.ParticipantSSN = String.Empty;
				this.LoanRepaymentAmount = String.Empty;
				this.LoanPayoffIndicator = String.Empty;
				this.FormatID = String.Empty;
			}
		};

		public struct MSRSSummaryRecord
		{
			public string EmployerCode;
			public string EmployerName;
			// vbPorter upgrade warning: PayDate1 As string	OnWrite	OnRead(DateTime)
			public string PayDate1;
			// vbPorter upgrade warning: PayDate2 As string	OnWrite	OnRead(DateTime)
			public string PayDate2;
			// vbPorter upgrade warning: PayDate3 As string	OnWrite	OnRead(DateTime)
			public string PayDate3;
			// vbPorter upgrade warning: PayDate4 As string	OnWrite	OnRead(DateTime)
			public string PayDate4;
			// vbPorter upgrade warning: PayDate5 As string	OnWrite	OnRead(DateTime)
			public string PayDate5;
			public string PayrollCycle;
			public double BasicInsuranceRate;
			public string MSRSReportingcode;
			public double TotalEarnableCompensation;
			public double EmployerPaidTotalEarnableCompensation;
			public double TotalRetirementContributions;
			public double TotalExcessPayback;
			public double TotalLifeInsurancePremiums;
			public int NumberOfDetailRecords;
			public double TotalActive;
			public double TotalRetiree;
			public double TotalSupplemental;
			public double TotalDependent;
			public double TotalGrantFunded;
			public double TotalGrantFundedEmployerContributions;
			public double TotalAdjustmentsGrantFunded;
			public double TotalAdjustmentsGrantFundedEmployer;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public MSRSSummaryRecord(int unusedParam)
			{
				this.EmployerCode = String.Empty;
				this.EmployerName = String.Empty;
				this.PayDate1 = String.Empty;
				this.PayDate2 = String.Empty;
				this.PayDate3 = String.Empty;
				this.PayDate4 = String.Empty;
				this.PayDate5 = String.Empty;
				this.PayrollCycle = String.Empty;
				this.BasicInsuranceRate = 0;
				this.MSRSReportingcode = String.Empty;
				this.TotalEarnableCompensation = 0;
				this.EmployerPaidTotalEarnableCompensation = 0;
				this.TotalRetirementContributions = 0;
				this.TotalExcessPayback = 0;
				this.TotalExcessPayback = 0;
				this.TotalLifeInsurancePremiums = 0;
				this.NumberOfDetailRecords = 0;
				this.TotalActive = 0;
				this.TotalRetiree = 0;
				this.TotalSupplemental = 0;
				this.TotalDependent = 0;
				this.TotalGrantFunded = 0;
				this.TotalGrantFundedEmployerContributions = 0;
				this.TotalAdjustmentsGrantFunded = 0;
				this.TotalAdjustmentsGrantFundedEmployer = 0;
			}
		};

		public static object ShowACHReport(bool modalDialog, List<string> batchReports = null)
		{
			object ShowACHReport = null;
			string[] arString = null;
			DateTime dtDate = DateTime.FromOADate(0);
			int intPayRun = 0;
			string strTemp;
			bool boolCreateFile = false;
			DialogResult intResponse = 0;
			bool boolFirstRun;
			boolFirstRun = false;
			boolFirstRun = !modGlobalRoutines.CheckIfPayProcessCompleted("BankList", "BankList", true);
			if (!boolFirstRun)
			{
				// MUST ASK IF THEY WANT TO CREATE THE FILE
				intResponse = MessageBox.Show("Do you wish to create the ACH file?", "Create File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Yes)
				{
					boolCreateFile = true;
				}
				else
				{
					boolCreateFile = false;
				}
			}
			else
			{
				boolCreateFile = true;
			}
			// MATTHEW 7/29/2004
			// YOU CAN BE WEEKLY BUT NO THE FIRST PAY RUN.
			if (modGlobalVariables.Statics.gstrMQYProcessing == "WEEKLY" && modGlobalVariables.Statics.gintCurrentPayRun == 1)
			{
				// If gstrMQYProcessing = "WEEKLY" Then
				boolFirstRun = true;
				// FORCE IT TO BE THE CURRENT PAYRDATE
			}
			else
			{
				boolFirstRun = false;
			}
			modGlobalVariables.Statics.gboolCancelACHForm = false;
			// strtemp will be the paydate and payrun chosen
			strTemp = frmACHBankInformation.InstancePtr.Init(boolCreateFile, boolFirstRun);
			arString = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
			if (arString[0] != "0")
			{
				dtDate = FCConvert.ToDateTime(arString[0]);
				intPayRun = FCConvert.ToInt32(Math.Round(Conversion.Val(arString[1])));
			}
			if (modGlobalVariables.Statics.gboolCancelACHForm)
			{
			}
			else
			{
                //modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = true;
                if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
                {
                    rptACH.InstancePtr.Init(modalDialog, dtDate, intPayRun, batchReports: batchReports);
                    // Unload frmReportViewer
                    rptDirectDeposit.InstancePtr.Init(false, dtDate, intPayRun, batchReports: batchReports);
                }
                else
                {
                    rptACH.InstancePtr.Init(true, dtDate, intPayRun);
                    // Unload frmReportViewer
                    rptDirectDeposit.InstancePtr.Init(false, dtDate, intPayRun);
                }
            }
			return ShowACHReport;
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, string)
		public static DateTime GetHighestPayDate(DateTime? tempdtStart = null, DateTime? tempdtEnd = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtStart == null)
			{
				tempdtStart = DateTime.FromOADate(0);
			}
			DateTime dtStart = tempdtStart.Value;
			if (tempdtEnd == null)
			{
				tempdtEnd = DateTime.FromOADate(0);
			}
			DateTime dtEnd = tempdtEnd.Value;

			DateTime GetHighestPayDate = System.DateTime.Now;
			// takes a start and end date and gives the highest paydate between them
			// or provide only one date to have just an upper or lower limit
			string strSQL = "";
			DateTime dtDate;
			clsDRWrapper clsLoad = new clsDRWrapper();
			GetHighestPayDate = DateTime.FromOADate(0);
			if (dtStart.ToOADate() == 0)
			{
				if (dtEnd.ToOADate() == 0)
				{
					strSQL = "select top 1 paydate from tblcheckdetail order by paydate desc";
				}
				else
				{
					// CALL ID 66240
					// MATTHEW 4/7/2005
					// strSQL = "select top 1 paydate from tblcheckdetail where paydate < '" & dtEnd & "' order by paydate desc"
					strSQL = "select top 1 paydate from tblcheckdetail where paydate <= '" + FCConvert.ToString(dtEnd) + "' order by paydate desc";
				}
			}
			else if (dtEnd.ToOADate() == 0)
			{
				// CALL ID 66240
				// MATTHEW 4/7/2005
				// strSQL = "select top 1 paydate from tblcheckdetail where paydate > '" & dtStart & "' order by paydate desc"
				strSQL = "select top 1 paydate from tblcheckdetail where paydate >= '" + FCConvert.ToString(dtStart) + "' order by paydate desc";
			}
			else
			{
				strSQL = "select top 1 paydate from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' order by paydate desc";
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (Information.IsDate(clsLoad.Get_Fields("paydate")))
				{
					GetHighestPayDate = FCConvert.ToDateTime(Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
				}
			}
			return GetHighestPayDate;
		}

		private static void FillSchoolAccountsTable()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL = "";
			string strValues = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strSQL = "Insert into PayrollSchoolObjectCodes (description,salary,retirement,tuition,otherbenefits,groupins,otherins,ssmed,onbehalf,unemployment,wc,healthbenefits) values (";
				strValues = "'Stipends','1500','2300','2500','2000','2100','2101','2200','2400','2600','2700','2800')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Professionals (Teachers)','1010','2310','2510','2010','2110','2111','2210','2410','2610','2710','2810')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Instructional Aides or Assistants','1020','2320','2520','2020','2120','2121','2220','2420','2620','2720','2820')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Instructional Aides or Assistants - Ed Tech I','1021','2120','2121','2220','2420','2620','2720','2820','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Instructional Aides or Assistants - Ed Tech II','1022','2120','2121','2220','2420','2620','2720','2820','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Instructional Aides or Assistants - Ed Tech III','1023','2120','2121','2220','2420','2620','2720','2820','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Administrators','1040','2340','2540','2040','2140','2141','2240','2440','2640','2740','2840')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Assistant Administrators','1050','2150','2151','2250','2450','2650','2750','2850','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Other Proff Salaries','1060','2160','2161','2260','2460','2660','2760','2860','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Others','1190','2190','2191','2290','2490','2690','2790','2890','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Regular Employees - Mgrs','1170','2170','2171','2270','2470','2670','2770','2870','','','')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Regular Employees','1180','2380','2580','2080','2180','2181','2280','2480','2680','2780','2880')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Temporary Employees','1200','2330','2530','2030','2130','2131','2230','2430','2630','2730','2830')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Temp Employees Tutors','1210','2330','2530','2030','2130','2131','2230','2430','2630','2730','2830')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				strValues = "'Temp Employees Subs','1230','2330','2530','2030','2130','2131','2230','2430','2630','2730','2830')";
				rsSave.Execute(strSQL + strValues, "twpy0000.vb1");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillSchoolAccountsTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckReportSequence()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSave.Execute("update tbldefaultinformation set reportsequence = 0 where reportsequence = 4", "twpy0000.vb1");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckReportSequence", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckEmpDedID()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strSQL = "select * from tblemployeedeductions order by employeenumber,recordnumber desc";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					strSQL = "update tblcheckdetail set empdeductionid = " + rsLoad.Get_Fields("ID") + " where deductionrecord = 1 and employeenumber = '" + rsLoad.Get_Fields("employeenumber") + "' and deddeductionnumber = " + rsLoad.Get_Fields("deductioncode") + " and convert(int, isnull(empdeductionid, 0)) = 0 ";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					rsLoad.MoveNext();
				}
				strSQL = "select * from tblemployersmatch order by employeenumber,recordnumber desc";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					strSQL = "update tblcheckdetail set empdeductionid = " + rsLoad.Get_Fields("ID") + " where matchrecord = 1 and employeenumber = '" + rsLoad.Get_Fields("employeenumber") + "' and matchdeductionnumber = " + rsLoad.Get_Fields("deductioncode") + " and (convert(int, isnull(empdeductionid, 0)) = 0 or paydate between #02/01/2006# and #03/01/2006#) ";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckEmpDedID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void FillMSRSDistributionDetailTable()
		{
			clsDRWrapper clsDist = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsMSRSDist = new clsDRWrapper();
			string strTemp = "";
			string strMSRS = "";
			int lngID = 0;
			string strSQL = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDist.OpenRecordset("select * from globalvariables", "SystemSettings");
				modGlobalConstants.Statics.MuniName = FCConvert.ToString(clsDist.Get_Fields("muniname"));
				clsDist.OpenRecordset("select * from tblpayrolldistribution inner join tblmiscupdate on (tblmiscupdate.employeenumber = tblpayrolldistribution.employeenumber) order by tblpayrolldistribution.employeenumber,RECORDNUMBER", "twpy0000.vb1");
				clsMSRSDist.OpenRecordset("select * from tblMSRSDistributionDetail", "twpy0000.vb1");
				while (!clsDist.EndOfFile())
				{
					// create a record in the msrsdistributiondetail table for each record here except for N or Y
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("MSRS"))) == "")
					{
						clsDist.Edit();
						clsDist.Set_Fields("msrsid", CNSTDISTMNOMSRS);
						clsDist.Update();
					}
					else if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("MSRS"))) == "N")
					{
						// no msrs
						clsDist.Edit();
						clsDist.Set_Fields("MSRSID", CNSTDISTMNOMSRS);
						clsDist.Update();
					}
					else if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("MSRS"))) == "Y")
					{
						// default
						clsDist.Edit();
						clsDist.Set_Fields("MSRSID", CNSTDISTMDEFAULT);
						clsDist.Update();
						// have to add a row in case they only have Y setup and no specific ones
						strMSRS = FCConvert.ToString(clsDist.Get_Fields("positioncode"));
						if (!clsMSRSDist.FindFirstRecord2("employeenumber,booldefault", clsDist.Get_Fields("tblmiscupdate.employeenumber") + ",1", ","))
						{
							clsMSRSDist.AddNew();
							// lngID = clsMSRSDist.Fields("ID")
							lngID = CNSTDISTMDEFAULT;
							clsMSRSDist.Set_Fields("employeenumber", clsDist.Get_Fields("tblmiscupdate.employeenumber"));
							clsMSRSDist.Set_Fields("position", strMSRS);
							clsMSRSDist.Set_Fields("status", clsDist.Get_Fields("tblmiscupdate.statuscode"));
							clsMSRSDist.Set_Fields("boolDEFAULT", true);
							// the rest is the same as for the specific code
							clsMSRSDist.Set_Fields("FedCompAmount", FCConvert.ToString(Conversion.Val(clsDist.Get_Fields("fedcompamt"))));
							strTemp = Strings.Left(clsDist.Get_Fields("federalcomp") + "D", 1);
							if (fecherFoundation.Strings.UCase(strTemp) == "P")
							{
								clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPPERCENT);
							}
							else
							{
								clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPDOLLAR);
							}
							if ((Conversion.Val(clsMSRSDist.Get_Fields("STATUS")) >= 71 && Conversion.Val(clsMSRSDist.Get_Fields("status")) <= 75) || Conversion.Val(clsMSRSDist.Get_Fields("status")) == 43)
							{
								// fed funded
								clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPPERCENT);
								clsMSRSDist.Set_Fields("fedcompamount", 100);
							}
							clsMSRSDist.Set_Fields("workweeksperyear", FCConvert.ToString(Conversion.Val(clsDist.Get_Fields("workweekperyear"))));
							clsMSRSDist.Set_Fields("fulltimeEquivalent", 0);
							if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "H")
							{
								clsMSRSDist.Set_Fields("HourlyDailyContract", CNSTMSRSHOURLY);
							}
							else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "D")
							{
								clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSDAILY);
							}
							else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "C")
							{
								clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSCONTRACT);
							}
							else
							{
							}
							if (fecherFoundation.Strings.UCase(Strings.Left(strMSRS + "   ", 3)) == "YEX")
							{
								clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSCONTRACT);
							}
							if (fecherFoundation.Strings.UCase(Strings.Left(strMSRS, 1)) == "Y")
							{
								// teacher
								clsMSRSDist.Set_Fields("Reporttype", CNSTMSRSREPORTTYPETEACHER);
								clsDist.Edit();
								clsDist.Set_Fields("MSRSID", lngID);
								clsDist.Set_Fields("msrsjobcategory", CNSTMSRSPLANCATEGORYTEACHER);
								clsDist.Update();
							}
							else
							{
								clsDist.Edit();
								clsDist.Set_Fields("msrsid", lngID);
								if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName)) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
								{
									// Trim(UCase(MuniName)) <> "JAY" And
									clsMSRSDist.Set_Fields("reporttype", CNSTMSRSREPORTTYPEPLD);
									// clsDist.Fields("msrsjobcategory") = CNSTMSRSPLANCATEGORYNONE
								}
								else
								{
									// If Trim(UCase(MuniName)) <> "JAY" Then
									clsMSRSDist.Set_Fields("reporttype", CNSTMSRSREPORTTYPESCHOOLPLD);
								}
								clsDist.Update();
							}
							clsMSRSDist.Update();
							lngID = FCConvert.ToInt32(clsMSRSDist.Get_Fields("ID"));
						}
					}
					else
					{
						// specific position code, need to create a new record
						strMSRS = FCConvert.ToString(clsDist.Get_Fields("MSRS"));
						strSQL = "employeenumber = '" + clsDist.Get_Fields("tblmiscupdate.employeenumber") + "'";
						strSQL += " and position = '" + strMSRS + "'";
						strTemp = FCConvert.ToString(clsDist.Get_Fields("tblpayrolldistribution.statuscode"));
						if (strTemp == string.Empty)
							strTemp = FCConvert.ToString(clsDist.Get_Fields("tblmiscupdate.statuscode"));
						strSQL += " and status = '" + strTemp + "'";
						if (clsMSRSDist.FindFirstRecord2("employeenumber,position,status", clsDist.Get_Fields("tblmiscupdate.employeenumber") + "," + strMSRS + "," + strTemp, ","))
						{
							goto skip;
						}
						clsMSRSDist.AddNew();
						clsMSRSDist.Set_Fields("employeenumber", clsDist.Get_Fields("tblmiscupdate.employeenumber"));
						clsMSRSDist.Set_Fields("Position", strMSRS);
						clsMSRSDist.Set_Fields("status", clsDist.Get_Fields("tblpayrolldistribution.statuscode"));
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("tblpayrolldistribution.statuscode"))) == string.Empty)
						{
							clsMSRSDist.Set_Fields("status", clsDist.Get_Fields("tblmiscupdate.statuscode"));
						}
						Continue:
						clsMSRSDist.Set_Fields("FedCompAmount", FCConvert.ToString(Conversion.Val(clsDist.Get_Fields("fedcompamt"))));
						strTemp = Strings.Left(clsDist.Get_Fields("federalcomp") + "D", 1);
						if (fecherFoundation.Strings.UCase(strTemp) == "P")
						{
							clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPPERCENT);
						}
						else
						{
							clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPDOLLAR);
						}
						if ((Conversion.Val(clsMSRSDist.Get_Fields("STATUS")) >= 71 && Conversion.Val(clsMSRSDist.Get_Fields("status")) <= 75) || Conversion.Val(clsMSRSDist.Get_Fields("status")) == 43)
						{
							// fed funded
							clsMSRSDist.Set_Fields("fedcompdollarpercent", CNSTMSRSFEDCOMPPERCENT);
							clsMSRSDist.Set_Fields("fedcompamount", 100);
						}
						clsMSRSDist.Set_Fields("workweeksperyear", FCConvert.ToString(Conversion.Val(clsDist.Get_Fields("workweekperyear"))));
						clsMSRSDist.Set_Fields("fulltimeEquivalent", 0);
						if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "H")
						{
							clsMSRSDist.Set_Fields("HourlyDailyContract", CNSTMSRSHOURLY);
						}
						else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "D")
						{
							clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSDAILY);
						}
						else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsDist.Get_Fields("payratecode")))) == "C")
						{
							clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSCONTRACT);
						}
						else
						{
						}
						if (fecherFoundation.Strings.UCase(Strings.Left(strMSRS + "   ", 3)) == "YEX")
						{
							clsMSRSDist.Set_Fields("hourlydailycontract", CNSTMSRSCONTRACT);
						}
						if (fecherFoundation.Strings.UCase(Strings.Left(strMSRS, 1)) == "Y")
						{
							// teacher
							clsMSRSDist.Set_Fields("Reporttype", CNSTMSRSREPORTTYPETEACHER);
							clsDist.Edit();
							clsDist.Set_Fields("MSRSID", lngID);
							clsDist.Set_Fields("msrsjobcategory", CNSTMSRSPLANCATEGORYTEACHER);
							clsDist.Update();
						}
						else
						{
							clsDist.Edit();
							clsDist.Set_Fields("msrsid", lngID);
							if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName)) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
							{
								clsMSRSDist.Set_Fields("reporttype", CNSTMSRSREPORTTYPEPLD);
							}
							else
							{
								// If Trim(UCase(MuniName)) <> "JAY" Then
								clsMSRSDist.Set_Fields("reporttype", CNSTMSRSREPORTTYPESCHOOLPLD);
							}
							clsDist.Update();
						}
						clsMSRSDist.Update();
						lngID = FCConvert.ToInt32(clsMSRSDist.Get_Fields("ID"));
					}
					skip:
					;
					clsDist.MoveNext();
				}
				// take care of federally funded teachers for past reports
				strSQL = "update tblcheckdetail set fedcompdollarpercent = " + FCConvert.ToString(CNSTMSRSFEDCOMPPERCENT) + ",fedcompamount = 100 where (convert(int, isnull(statuscode, 0)) = 43 or (convert(int, isnull(statuscode, 0)) between 71 and 75))";
				clsTemp.Execute(strSQL, "twpy0000.vb1");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillMSRSDistributionDetailTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intTypeID As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intLayoutID As int	OnWriteFCConvert.ToInt32(
		public static void PrintVacSickReports(int intTypeID, int intLayoutID, bool boolSeparatePages = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intResponse = 0;
			DateTime dtReturndate;
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
			DateTime dtStartDate = DateTime.FromOADate(0);
			// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(DateTime, string)
			DateTime dtEndDate = DateTime.FromOADate(0);
			int lngYear = 0;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth = 0;
			int intPayRun = 0;
			bool boolPrintedOne = false;
			if (intLayoutID == 0)
			{
				// just current stats so no need to gather more info. Just print
				if (intTypeID == -1000)
				{
					clsLoad.OpenRecordset("select * from tblcodetypes order by ID", "twpy0000.vb1");
					boolPrintedOne = false;
					while (!clsLoad.EndOfFile())
					{
						if (rptCurrEmployeeVacSick.InstancePtr.Init(clsLoad.Get_Fields("ID"), false, boolSeparatePages))
						{
							boolPrintedOne = true;
							rptCurrEmployeeVacSick.InstancePtr.Unload();
						}
						clsLoad.MoveNext();
					}
					if (!boolPrintedOne)
					{
						MessageBox.Show("No data found", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					rptCurrEmployeeVacSick.InstancePtr.Init(intTypeID, true, boolSeparatePages);
					rptCurrEmployeeVacSick.InstancePtr.Unload();
				}
				return;
			}
			if (modGlobalVariables.Statics.gstrMQYProcessing == "WEEKLY")
			{
				intResponse = 0;
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
			{
				intResponse = 1;
			}
			else if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
			{
				intResponse = 4;
			}
			else
			{
				intResponse = 4;
			}
			switch (intResponse)
			{
				case 0:
					{
						// weekly
						dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
						intPayRun = modGlobalVariables.Statics.gintCurrentPayRun;
						break;
					}
				case 1:
					{
						// monthly
						dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
						break;
					}
				case 2:
					{
						// quarterly
						modGlobalRoutines.GetDatesForAQuarter(ref dtStartDate, ref dtEndDate, modGlobalVariables.Statics.gdatCurrentPayDate);
						break;
					}
				case 3:
					{
						// yearly
						dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
						dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
						break;
					}
				case 4:
					{
						// individual
						// must select weekly or monthly
						dtReturndate = modGlobalVariables.Statics.gdatCurrentPayDate;
						lngYear = dtReturndate.Year;
						intMonth = dtReturndate.Month;
						intPayRun = 1;
						intResponse = frmMQYFlag.InstancePtr.Init("", "Monthly", "Quarterly", "Yearly", "Weekly", true, ref dtReturndate, ref lngYear, ref intMonth, ref intPayRun);
						switch (intResponse)
						{
							case -1:
								{
									// cancelled
									return;
								}
							case 0:
								{
									dtEndDate = dtReturndate;
									break;
								}
							case 1:
								{
									if (intMonth == modGlobalVariables.Statics.gdatCurrentPayDate.Month && lngYear == modGlobalVariables.Statics.gdatCurrentPayDate.Year)
									{
										dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
									}
									else
									{
										dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
										dtEndDate = modDavesSweetCode.FindLastDay(FCConvert.ToString(dtStartDate));
									}
									break;
								}
							case 2:
								{
									// quarterly
									modGlobalRoutines.GetDatesForAQuarter(ref dtStartDate, ref dtEndDate, FCConvert.ToDateTime(FCConvert.ToString((intMonth * 3) - 2) + "/1/" + FCConvert.ToString(lngYear)));
									break;
								}
							case 3:
								{
									// yearly
									// dtStartDate = "1/1/" & Year(dtReturndate)
									dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
									// dtEndDate = dtReturndate
									if (lngYear == modGlobalVariables.Statics.gdatCurrentPayDate.Year)
									{
										dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
									}
									else
									{
										dtEndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
									}
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
			if (intTypeID == -1000)
			{
				// all types
				clsLoad.OpenRecordset("select * from tblcodetypes order by ID", "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					rptEmployeeVacationSick.InstancePtr.Init(intResponse, clsLoad.Get_Fields("ID"), dtEndDate, 1, boolSeparatePages);
					clsLoad.MoveNext();
				}
			}
			else
			{
				rptEmployeeVacationSick.InstancePtr.Init(intResponse, intTypeID, dtEndDate, 1, boolSeparatePages);
			}
			return;
		}

		private static void CheckMPersStatus()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Today, FCConvert.ToDateTime("02/01/2009")) > 0)
				{
					strSQL = "Update tblMSRSDistributionDetail set status = '11' where status = '21'";
					rsSave.Execute(strSQL, "Twpy0000.vb1");
					strSQL = "update tblMSRSDistributionDetail set status = '12' where status = '22'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSDistributionDetail set status = '14' where status = '24'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSDistributionDetail set status = '15' where status = '25'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSDistributionDetail set status = '17' where status = '27' or status = '28' or status = '18' or status = '1A' or status = '1B' or status = '2A' or status = '2B' or status = '23' or status = '26' or status = '29' or status = '2C'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSDistributionDetail set status = '65' where status = '67'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "Update tblPayrollDistribution set statusCODE = '11' where statuscode = '21'";
					rsSave.Execute(strSQL, "Twpy0000.vb1");
					strSQL = "update tblPayrollDistribution set statuscode = '12' where statuscode = '22'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblPayrollDistribution set statuscode = '14' where statuscode = '24'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblPayrollDistribution set statuscode = '15' where statuscode = '25'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblPayrollDistribution set statuscode = '17' where statuscode = '27' or statuscode = '28' or statuscode = '18' or statuscode = '1A' or statuscode = '1B' or statuscode = '2A' or statuscode = '2B' or statuscode = '23' or statuscode = '26' or statuscode = '29' or statuscode = '2C'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblPayrollDistribution set statuscode = '65' where statuscode = '67'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "Update tblMSRSnonpaid set statuscode = '11' where statuscode = '21'";
					rsSave.Execute(strSQL, "Twpy0000.vb1");
					strSQL = "update tblMSRSnonpaid set statuscode = '12' where statuscode = '22'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSnonpaid set statuscode = '14' where statuscode = '24'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSnonpaid set statuscode = '15' where statuscode = '25'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSnonpaid set statuscode = '17' where statuscode = '27' or statuscode = '28' or statuscode = '18' or statuscode = '1A' or statuscode = '1B' or statuscode = '2A' or statuscode = '2B' or statuscode = '23' or statuscode = '26' or statuscode = '29' or statuscode = '2C'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
					strSQL = "update tblMSRSnonpaid set statuscode = '65' where statuscode = '67'";
					rsSave.Execute(strSQL, "twpy0000.vb1");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckMPersStatus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void PrintPayandTaxSummaryReports(int intWhichReports, bool boolChooseDates = false, DateTime? tempdtPassedInDate = null, int lngPassedInYear = 0, int intMonthQuarter = 0, int intPassedInPayRun = 0, List<string> batchReports = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtPassedInDate == null)
			{
				tempdtPassedInDate = DateTime.FromOADate(0);
			}
			DateTime dtPassedInDate = tempdtPassedInDate.Value;
			// 0 = weekly
			// 1 = monthly
			// 2 = quarterly
			// 3 = yearly
			// 4 = fiscal
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(DateTime, string)
			DateTime dtDate = DateTime.FromOADate(0);
			int lngYear = 0;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth = 0;
			int intQuarter = 0;
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			int intPayRun = 0;
			modCustomReport.Statics.strCustomTitle = "Pay Summary Report";
			switch (intWhichReports)
			{
				case 0:
					{
						// paydate
						if (boolChooseDates)
						{
							dtDate = dtPassedInDate;
							intPayRun = intPassedInPayRun;
						}
						else
						{
							dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
							intPayRun = modGlobalVariables.Statics.gintCurrentPayRun;
						}
						break;
					}
				case 1:
					{
						// month to date
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
						intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
						if (boolChooseDates)
						{
							intMonth = intMonthQuarter;
							lngYear = lngPassedInYear;
							// Call frmSelectDateInfo.Init("Select the Month and Year for the report", lngYear, , intMonth)
						}
						switch (intMonth)
						{
							case 1:
							case 3:
							case 5:
							case 7:
							case 8:
							case 10:
							case 12:
								{
									dtDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/31/" + FCConvert.ToString(lngYear));
									break;
								}
							case 2:
								{
									// dtDate = intMonth & "/28/" & lngYear
									dtDate = fecherFoundation.DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime("3/1/" + FCConvert.ToString(lngYear)));
									break;
								}
							default:
								{
									dtDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/30/" + FCConvert.ToString(lngYear));
									break;
								}
						}
						//end switch
						break;
					}
				case 2:
					{
						// Quarter to Date
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
						intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
						if (boolChooseDates)
						{
							intQuarter = intMonthQuarter;
							lngYear = lngPassedInYear;
							// intQuarter = 1
							// Call frmSelectDateInfo.Init("Select the Quarter and Year for the report", lngYear, intQuarter)
						}
						else
						{
							if (intMonth >= 1 && intMonth <= 3)
							{
								intQuarter = 1;
							}
							else if (intMonth >= 4 && intMonth <= 6)
							{
								intQuarter = 2;
							}
							else if (intMonth >= 7 && intMonth <= 9)
							{
								intQuarter = 3;
							}
							else if (intMonth >= 10 && intMonth <= 12)
							{
								intQuarter = 4;
							}
						}
						GetDateRangeForYearQuarter(ref dtStartDate, ref dtDate, intQuarter, lngYear);
						break;
					}
				case 3:
					{
						// calendar year to date
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
						if (boolChooseDates)
						{
							lngYear = lngPassedInYear;
							// Call frmSelectDateInfo.Init("Select the Year for the report", lngYear)
						}
						dtDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
						break;
					}
				case 4:
					{
						// fiscal year to date
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
						if (boolChooseDates)
						{
							lngYear = lngPassedInYear;
							// Call frmSelectDateInfo.Init("Select the Year for the report", lngYear)
							dtDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
							dtDate = GetLastFiscalDay(dtDate);
						}
						else
						{
							dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
						}
						break;
					}
			}
			//end switch
			rptPaySummaryReport.InstancePtr.Init(intWhichReports, dtDate, intPayRun, batchReports: batchReports);
			// rptPaySummaryReport.Show vbModal, MDIParent
			// 
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
			}
            //frmFullSetofReports.WaitForReportPrint();
			modCustomReport.Statics.strCustomTitle = "Tax Summary Report";
			rptTaxSumaryReport.InstancePtr.Init(intWhichReports, dtDate, intPayRun, batchReports: batchReports);
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				//FCGlobal.Printer.DeviceName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
			}
            //frmFullSetofReports.WaitForReportPrint();
            modCustomReport.Statics.strCustomTitle = "Taxable Wage Summary Report";
			rptTaxableWageSummaryReport.InstancePtr.Init(intWhichReports, dtDate, intPayRun, batchReports: batchReports);
            //frmFullSetofReports.WaitForReportPrint();
        }

		public static void PrintPaySummaryReport(int intWhatReportsCode, List<string> batchReports = null)
		{
			// 0 through 4
			// 0 = weekly
			// 1 = monthly
			// 2 = quarterly
			// 3 = yearly
			// 4 = ask
			int intReturn = 0;
			DateTime dtDate;
			int lngYear = 0;
			// vbPorter upgrade warning: intMonthQuarter As int	OnWriteFCConvert.ToInt32(
			int intMonthQuarter = 0;
			int intPRun = 0;
			if (intWhatReportsCode == 4)
			{
				dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				lngYear = dtDate.Year;
				intMonthQuarter = dtDate.Month;
				intReturn = frmMQYFlag.InstancePtr.Init("Fis Year to date", "Month to date", "Quarter to date", "Cal Year to date", "Pay Date ", true, ref dtDate, ref lngYear, ref intMonthQuarter, ref intPRun);
				if (intReturn < 0)
					return;
				PrintPayandTaxSummaryReports(intReturn, true, dtDate, lngYear, intMonthQuarter, intPRun, batchReports: batchReports);
			}
			else
			{
				// do weekly
				// If gtypeFullSetReports.boolFullSet Then
				PrintPayandTaxSummaryReports(intWhatReportsCode, batchReports: batchReports);
			}
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, string)
		public static DateTime GetPrecedingFirstFiscalDay(DateTime? tempdtCurDate = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtCurDate == null)
			{
				tempdtCurDate = DateTime.FromOADate(0);
			}
			DateTime dtCurDate = tempdtCurDate.Value;

			DateTime GetPrecedingFirstFiscalDay = System.DateTime.Now;
			// returns the first fiscal month
			int intMonth = 0;
			// vbPorter upgrade warning: intCurMonth As int	OnWriteFCConvert.ToInt32(
			int intCurMonth;
			clsDRWrapper rsData = new clsDRWrapper();
			GetPrecedingFirstFiscalDay = DateTime.FromOADate(0);
			if (dtCurDate.ToOADate() == 0)
				dtCurDate = DateTime.Today;
			intCurMonth = dtCurDate.Month;
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_String("FiscalStart"))));
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int16("FirstFiscalMonth"))));
				}
			}
			if (intMonth == 0)
				intMonth = intCurMonth;
			if (intCurMonth >= intMonth)
			{
				GetPrecedingFirstFiscalDay = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(dtCurDate.Year));
			}
			else
			{
				GetPrecedingFirstFiscalDay = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(dtCurDate.Year - 1));
			}
			return GetPrecedingFirstFiscalDay;
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, string)
		public static DateTime GetNewPrecedingFirstFiscalDay(DateTime? tempdtCurDate = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtCurDate == null)
			{
				tempdtCurDate = DateTime.FromOADate(0);
			}
			DateTime dtCurDate = tempdtCurDate.Value;
			DateTime GetNewPrecedingFirstFiscalDay = System.DateTime.Now;
			// returns the first fiscal month
			int intMonth = 0;
			// vbPorter upgrade warning: intCurMonth As int	OnWriteFCConvert.ToInt32(
			int intCurMonth;
			clsDRWrapper rsData = new clsDRWrapper();
			GetNewPrecedingFirstFiscalDay = DateTime.FromOADate(0);
			if (dtCurDate.ToOADate() == 0)
				dtCurDate = DateTime.Today;
			intCurMonth = dtCurDate.Month;
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_String("FiscalStart"))));
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int16("FirstFiscalMonth"))));
				}
			}
			if (intCurMonth >= intMonth)
			{
				GetNewPrecedingFirstFiscalDay = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(dtCurDate.Year));
			}
			else
			{
				GetNewPrecedingFirstFiscalDay = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(dtCurDate.Year - 1));
			}
			return GetNewPrecedingFirstFiscalDay;
		}
		// vbPorter upgrade warning: dtCurDate As DateTime	OnWrite(string, DateTime)
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, DateTime)
		public static DateTime GetLastFiscalDay(DateTime? tempdtCurDate = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtCurDate == null)
			{
				tempdtCurDate = DateTime.FromOADate(0);
			}
			DateTime dtCurDate = tempdtCurDate.Value;

			DateTime GetLastFiscalDay = System.DateTime.Now;
			int intMonth = 0;
			// vbPorter upgrade warning: intCurMonth As int	OnWriteFCConvert.ToInt32(
			int intCurMonth;
			int lngYear;
			int intEndMonth;
			clsDRWrapper rsData = new clsDRWrapper();
			GetLastFiscalDay = DateTime.FromOADate(0);
			if (dtCurDate.ToOADate() == 0)
				dtCurDate = DateTime.Today;
			intCurMonth = dtCurDate.Month;
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_String("FiscalStart"))));
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					intMonth = 1;
				}
				else
				{
					intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int16("FirstFiscalMonth"))));
				}
			}
			modGlobalVariables.Statics.gintFirstFiscalMonth = intMonth;
			intEndMonth = (intMonth + 11) % 12;
			if (intEndMonth == 0)
				intEndMonth = 12;
			// above doesn't work when intMonth is 1
			// now figure out last month from the first month
			lngYear = dtCurDate.Year;
			// if the cur month is <= fiscal start month then the year is correct
			// otherwise it is the wrong year
			if (intCurMonth > intEndMonth)
			{
				lngYear += 1;
			}
			GetLastFiscalDay = modDavesSweetCode.FindLastDay(FCConvert.ToString(intEndMonth) + "/1/" + FCConvert.ToString(lngYear));
			return GetLastFiscalDay;
		}

		public static double GetCalculatedFedTaxLiability(ref DateTime dtStart, ref DateTime dtEnd, ref string strWhere)
		{
			double GetCalculatedFedTaxLiability = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				string strSQL2 = "";
				string strSQL3 = "";
				string strSQLShouldve = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				double dblShouldveFica;
				double dblShouldveMed;
				double dblFedTax;
				GetCalculatedFedTaxLiability = 0;
				strSQL = "SELECT Sum(StateTaxGross) AS SumOfStateTaxGross, Sum(FederalTaxGross) AS SumOfFederalTaxGross, Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH,sum(EmployerFicaTax) as SumOfEmployerFicaTaxWH ,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH, Sum(MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 and " + strWhere;
				if (fecherFoundation.Strings.Trim(strWhere) != "")
				{
					strSQLShouldve = "SELECT FICATaxGross, MedicareTaxGross, FederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where w2adjustment = 0 and (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 and " + strWhere;
				}
				else
				{
					strSQLShouldve = "SELECT FICATaxGross, MedicareTaxGross, FederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) Where w2adjustment = 0 and (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1";
				}
				// If intQuarterUsed = 1 Then
				dblFedTax = 0;
				dblShouldveFica = 0;
				dblShouldveMed = 0;
				clsLoad.OpenRecordset(strSQLShouldve, "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					dblShouldveFica += FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ficataxgross")), "0.00")) * 0.062, "0.00"));
					dblShouldveFica += FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("ficataxgross")), "0.00")) * 0.062, "0.00"));
					dblShouldveMed += FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("medicaretaxgross")), "0.00")) * 0.0145, "0.00"));
					dblFedTax += FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("FederalTaxWH")), "0.00"));
					clsLoad.MoveNext();
				}
				// dblShouldveFica = dblShouldveFica * 2
				dblShouldveMed *= 2;
				// vbPorter upgrade warning: dblReturn As double	OnWrite(double, string)
				double dblReturn;
				dblReturn = dblShouldveMed + dblShouldveFica + dblFedTax;
				dblReturn = FCConvert.ToDouble(Strings.Format(dblReturn, "0.00"));
				GetCalculatedFedTaxLiability = dblReturn;
				return GetCalculatedFedTaxLiability;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCalculatedFedTaxLiability", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCalculatedFedTaxLiability;
		}

		public static double GetFedTaxLiability_1(DateTime dtStart, DateTime dtEnd, ref string strWhere, bool boolDontUseWH = false)
		{
			return GetFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere, boolDontUseWH);
		}
		public static double GetFedTaxLiability(ref DateTime dtStart, ref DateTime dtEnd, ref string strWhere, bool boolDontUseWH = false)
		{
			double GetFedTaxLiability = 0;
			// Lines follow 941 form
			string strSQL = "";
			double dblLine2;
			double dblLine3 = 0;
			double dblLine4 = 0;
			double dblLine5 = 0;
			double dblLine6a = 0;
			double dblLine6b = 0;
			double dblLine6c = 0;
			double dblLine6d = 0;
			double dblLine7a = 0;
			double dblLine7b = 0;
			double dblLine8 = 0;
			double dblLine9 = 0;
			double dblLine10 = 0;
			double dblLine11 = 0;
			double dblLine12 = 0;
			double dblLine13 = 0;
			double dblLine14;
			double dblSumWH = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetFedTaxLiability = 0;
				if (boolDontUseWH)
				{
					GetFedTaxLiability = GetCalculatedFedTaxLiability(ref dtStart, ref dtEnd, ref strWhere);
					return GetFedTaxLiability;
				}
				
				if (!boolDontUseWH)
				{
					if (strWhere == string.Empty)
					{
						strSQL = "SELECT   Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, sum(EmployerFicaTax) as SumOfEmployerFicaTaxWH,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH,sum(medicaretaxwh) as sumofmedicaretaxwh, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail  Where (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 " + strWhere;
					}
					else
					{
						strSQL = "SELECT   Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(FICATaxWH) AS SumOfFICATaxWH, sum(EmployerFicaTax) as SumOfEmployerFicaTaxWH ,sum(EmployerMedicareTax) as SumOfEmployerMedicareTaxWH,Sum(MedicareTaxWH) AS SumOfMedicareTaxWH,  Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) Where (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 and " + strWhere;
					}
				}
				else
				{
					if (strWhere == string.Empty)
					{
						strSQL = "SELECT   Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(format(convert(double, isnull(FICATaxGross, 0)) * .062,'0.00')) AS SumOfFICATaxWH, sum(format(convert(double, isnull(FICATaxGross, 0)) * .062,'0.00')) as SumOfEmployerFicaTaxWH ,sum(medicaretaxwh) as sumofmedicaretaxwh, Sum(StateTaxWH) AS SumOfStateTaxWH, Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail  Where (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 " + strWhere;
					}
					else
					{
						strSQL = "SELECT   Sum(FICATaxGross) AS SumOfFICATaxGross, Sum(MedicareTaxGross) AS SumOfMedicareTaxGross, Sum(GrossPay) AS SumOfGrossPay, Sum(format(convert(double, isnull(FICATaxGross, 0)) * .062,'0.00')) AS SumOfFICATaxWH, sum(format(convert(double, isnull(FICATaxGross, 0)) * .062,'0.00')) as SumOfEmployerFicaTaxWH ,Sum(MedicareTaxWH) AS SumOfMedicareTaxWH,  Sum(FederalTaxWH) AS SumOfFederalTaxWH FROM tblCheckDetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) Where (checkvoid = 0) and PayDate >= '" + FCConvert.ToString(dtStart) + "' And PayDate <= '" + FCConvert.ToString(dtEnd) + "' And TotalRecord = 1 and " + strWhere;
					}
				}
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblLine2 = Conversion.Val(clsLoad.Get_Fields("sumofgrosspay"));
					dblLine3 = Conversion.Val(clsLoad.Get_Fields("sumoffederaltaxwh"));
					dblLine4 = 0;
					dblLine5 = dblLine3 + dblLine4;
					dblLine6a = Conversion.Val(clsLoad.Get_Fields("SumOfFicaTaxGross"));
					dblLine6b = Conversion.Val(Strings.Format(dblLine6a * 0.124, "0.00"));
					dblLine6c = 0;
					// tips
					dblLine6d = Conversion.Val(Strings.Format(dblLine6c * 0.124, "0.00"));
					dblLine7a = Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxgross"));
					dblLine7b = Conversion.Val(Strings.Format(dblLine7a * 0.029, "0.00"));
					dblLine8 = Conversion.Val(Strings.Format(dblLine7b + dblLine6b + dblLine6d, "0.00"));
					dblSumWH = ((Conversion.Val(clsLoad.Get_Fields("sumofmedicaretaxwh")) + Conversion.Val(clsLoad.Get_Fields("SumOfEmployerMedicareTaxWH"))) + Conversion.Val(clsLoad.Get_Fields("sumofficataxwh"))) + Conversion.Val(clsLoad.Get_Fields("SumOFemployerficataxwh"));
					dblLine9 = (dblSumWH) - dblLine8;
					// adjustments
					dblLine10 = dblLine8 + dblLine9;
					dblLine11 = Conversion.Val(Strings.Format(dblLine10 + dblLine5, "0.00"));
					dblLine12 = 0;
					// advanced EIC payments
					dblLine13 = Conversion.Val(Strings.Format(dblLine11 - dblLine12, "0.00"));
					GetFedTaxLiability = dblLine13;
				}
				return GetFedTaxLiability;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetFedTaxLiability", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetFedTaxLiability;
		}
		// vbPorter upgrade warning: StartDate As DateTime	OnWrite(DateTime, string)
		// vbPorter upgrade warning: EndDate As DateTime	OnWrite(DateTime, string)
		public static void GetDateRangeForYearQuarter(ref DateTime StartDate, ref DateTime EndDate, int intQuarter, int lngYear = -1)
		{
			// takes a quarter and optional year.  If not specified, the current year is assumed
			// will return the beginning and ending dates of that quarter
			if (lngYear == -1)
				lngYear = DateTime.Today.Year;
			switch (intQuarter)
			{
				case 1:
					{
						// jan - march
						StartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
						EndDate = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(lngYear));
						break;
					}
				case 2:
					{
						// april - june
						StartDate = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(lngYear));
						EndDate = FCConvert.ToDateTime("6/30/" + FCConvert.ToString(lngYear));
						break;
					}
				case 3:
					{
						// july - sept
						StartDate = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(lngYear));
						EndDate = FCConvert.ToDateTime("9/30/" + FCConvert.ToString(lngYear));
						break;
					}
				case 4:
					{
						// oct - dec
						StartDate = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(lngYear));
						EndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear));
						break;
					}
			}
			//end switch
		}

		public static bool PrintPayrollChecks(bool boolReprint, int lngStartingCheckNum, int lngWarrant, int lngJournal, bool boolPrintSig, int lngFirstCheck = 0, int lngLastCheck = 0, int lngSigID = 0, int lngBankNumber = 0, int lngNonNegStart = 0)
		{
			bool PrintPayrollChecks = false;
			// vbPorter upgrade warning: lngNumChecks As int	OnWrite(int, double)
			int lngNumChecks = 0;
			bool boolACH = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngStart = 0;
			int lngEnd = 0;
			string strSQL = "";
			int lngBank;
			lngBank = lngBankNumber;
			PrintPayrollChecks = FCConvert.ToBoolean(1);
            rptLaserCheck1.InstancePtr.Unload();
			if (!boolReprint)
			{
				// initial run
				lngNumChecks = 0;				

                try
                {
                    clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("achclearinghouse")))
                    {
                        boolACH = true;
                    }
                    
                    clsLoad.OpenRecordset($"select count(employeenumber) as checkcount from tblcheckdetail where convert(int, isnull(checknumber, 0)) = 0 and paydate = '{FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate)}' and payrunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and TotalRecord = 1 group by employeenumber,MASTERRECORD", "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        lngNumChecks += Conversion.Val(clsLoad.Get_Fields("checkcount"));
                    }

                    // if ach then only 1 check
                    clsLoad.OpenRecordset($"select count(ddbanknumber) as checkcount from tblcheckdetail where convert(int, isnull(checknumber, 0)) = 0 and paydate = '{FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate)}' and payrunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and BankRecord = 1 group by ddbanknumber", "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        // if ach just add one
                        if (boolACH)
                        {
                            lngNumChecks += 1;
                        }
                        else
                        {
                            lngNumChecks += Conversion.Val(clsLoad.Get_Fields("checkcount"));
                        }
                    }
                    
                    clsLoad.OpenRecordset($"select count(trustrecipientid) as checkcount from tblcheckdetail where convert(int, isnull(checknumber, 0)) = 0 and paydate = '{FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate)}' and payrunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and TrustRecord = 1 group by trustrecipientid", "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        lngNumChecks += Conversion.Val(clsLoad.Get_Fields("checkcount"));
                    }
                    
                    lngStart = lngStartingCheckNum;
                    lngEnd = lngNumChecks + lngStart - 1;
                    
                    clsLoad.OpenRecordset($"select ID from tblcheckdetail where paydate = '{FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate)}' and payrunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and checknumber between {FCConvert.ToString(lngStart)} and {FCConvert.ToString(lngEnd)}", "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        // already a check number out there
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        MessageBox.Show($"There is already at least one check for this pay date and pay run that has a check number between {FCConvert.ToString(lngStart)} and {FCConvert.ToString(lngEnd)}. Cannot continue.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return PrintPayrollChecks;
                    }
                }
                finally
                {
                    clsLoad.DisposeOf();
                }
				
                rptLaserCheck1.InstancePtr.Init(false, lngStartingCheckNum, lngWarrant, boolPrintSig, 0, 0, "", lngBank, lngSigID, lngNonNegStart);
			}
			else
			{
				rptLaserCheck1.InstancePtr.Init(true, lngStartingCheckNum, lngWarrant, boolPrintSig, lngFirstCheck, lngLastCheck, "", lngBank, lngSigID, lngNonNegStart);
				PrintPayrollChecks = FCConvert.ToBoolean(1);
			}
			return PrintPayrollChecks;
		}
		// vbPorter upgrade warning: intNum As int	OnWrite(string)
		public static string GetStateAbbr(int intNum)
		{
			string GetStateAbbr = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from states where id = " + FCConvert.ToString(intNum), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				GetStateAbbr = FCConvert.ToString(clsLoad.Get_Fields("state"));
			}
			else
			{
				GetStateAbbr = "";
			}
			return GetStateAbbr;
		}

		public static int GetStateCode(string strState)
		{
			int GetStateCode = 0;
			// returns the FIPS numeric code for a state
			if ((fecherFoundation.Strings.UCase(strState) == "AL") || (fecherFoundation.Strings.UCase(strState) == "ALABAMA"))
			{
				GetStateCode = 1;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "AK") || (fecherFoundation.Strings.UCase(strState) == "ALASKA"))
			{
				GetStateCode = 2;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "AZ") || (fecherFoundation.Strings.UCase(strState) == "ARIZONA"))
			{
				GetStateCode = 4;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "AR") || (fecherFoundation.Strings.UCase(strState) == "ARKANSAS"))
			{
				GetStateCode = 5;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "CA") || (fecherFoundation.Strings.UCase(strState) == "CALIFORNIA"))
			{
				GetStateCode = 6;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "CO") || (fecherFoundation.Strings.UCase(strState) == "COLORADO"))
			{
				GetStateCode = 8;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "CT") || (fecherFoundation.Strings.UCase(strState) == "CONNECTICUT"))
			{
				GetStateCode = 9;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "DE") || (fecherFoundation.Strings.UCase(strState) == "DELAWARE"))
			{
				GetStateCode = 10;
			}
			else if (fecherFoundation.Strings.UCase(strState) == "DC")
			{
				GetStateCode = 11;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "FL") || (fecherFoundation.Strings.UCase(strState) == "FLORIDA"))
			{
				GetStateCode = 12;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "GA") || (fecherFoundation.Strings.UCase(strState) == "GEORGIA"))
			{
				GetStateCode = 13;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "HI") || (fecherFoundation.Strings.UCase(strState) == "HAWAII"))
			{
				GetStateCode = 15;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "ID") || (fecherFoundation.Strings.UCase(strState) == "IDAHO"))
			{
				GetStateCode = 16;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "IL") || (fecherFoundation.Strings.UCase(strState) == "ILLINOIS"))
			{
				GetStateCode = 17;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "IN") || (fecherFoundation.Strings.UCase(strState) == "INDIANA"))
			{
				GetStateCode = 18;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "IA") || (fecherFoundation.Strings.UCase(strState) == "IOWA"))
			{
				GetStateCode = 19;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "KS") || (fecherFoundation.Strings.UCase(strState) == "KANSAS"))
			{
				GetStateCode = 20;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "KY") || (fecherFoundation.Strings.UCase(strState) == "KENTUCKY"))
			{
				GetStateCode = 21;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "LA") || (fecherFoundation.Strings.UCase(strState) == "LOUSIANA"))
			{
				GetStateCode = 22;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "ME") || (fecherFoundation.Strings.UCase(strState) == "MAINE"))
			{
				GetStateCode = 23;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MD") || (fecherFoundation.Strings.UCase(strState) == "MARYLAND"))
			{
				GetStateCode = 24;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MA") || (fecherFoundation.Strings.UCase(strState) == "MASSACHUSETTS"))
			{
				GetStateCode = 25;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MI") || (fecherFoundation.Strings.UCase(strState) == "MICHIGAN"))
			{
				GetStateCode = 26;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MN") || (fecherFoundation.Strings.UCase(strState) == "MINNESOTA"))
			{
				GetStateCode = 27;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MS") || (fecherFoundation.Strings.UCase(strState) == "MISSISSIPPI"))
			{
				GetStateCode = 28;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MO") || (fecherFoundation.Strings.UCase(strState) == "MISSOURI"))
			{
				GetStateCode = 29;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "MT") || (fecherFoundation.Strings.UCase(strState) == "MONTANA"))
			{
				GetStateCode = 30;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NE") || (fecherFoundation.Strings.UCase(strState) == "NEBRASKA"))
			{
				GetStateCode = 31;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NV") || (fecherFoundation.Strings.UCase(strState) == "NEVADA"))
			{
				GetStateCode = 32;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NH") || (fecherFoundation.Strings.UCase(strState) == "NEW HAMPSHIRE"))
			{
				GetStateCode = 33;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NJ") || (fecherFoundation.Strings.UCase(strState) == "NEW JERSEY"))
			{
				GetStateCode = 34;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NM") || (fecherFoundation.Strings.UCase(strState) == "NEW MEXICO"))
			{
				GetStateCode = 35;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NY") || (fecherFoundation.Strings.UCase(strState) == "NEW YORK"))
			{
				GetStateCode = 36;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "NC") || (fecherFoundation.Strings.UCase(strState) == "NORTH CAROLINA"))
			{
				GetStateCode = 37;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "ND") || (fecherFoundation.Strings.UCase(strState) == "NORTH DAKOTA"))
			{
				GetStateCode = 38;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "OH") || (fecherFoundation.Strings.UCase(strState) == "OHIO"))
			{
				GetStateCode = 40;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "OK") || (fecherFoundation.Strings.UCase(strState) == "OKLAHOMA"))
			{
				GetStateCode = 40;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "OR") || (fecherFoundation.Strings.UCase(strState) == "OREGON"))
			{
				GetStateCode = 41;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "PA") || (fecherFoundation.Strings.UCase(strState) == "PENNSYLVANIA"))
			{
				GetStateCode = 42;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "RI") || (fecherFoundation.Strings.UCase(strState) == "RHODE ISLAND"))
			{
				GetStateCode = 44;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "SC") || (fecherFoundation.Strings.UCase(strState) == "SOUTH CAROLINA"))
			{
				GetStateCode = 45;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "SD") || (fecherFoundation.Strings.UCase(strState) == "SOUTH DAKOTA"))
			{
				GetStateCode = 46;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "TN") || (fecherFoundation.Strings.UCase(strState) == "TENNESSEE"))
			{
				GetStateCode = 47;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "TX") || (fecherFoundation.Strings.UCase(strState) == "TEXAS"))
			{
				GetStateCode = 48;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "UT") || (fecherFoundation.Strings.UCase(strState) == "UTAH"))
			{
				GetStateCode = 49;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "VT") || (fecherFoundation.Strings.UCase(strState) == "VERMONT"))
			{
				GetStateCode = 50;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "VA") || (fecherFoundation.Strings.UCase(strState) == "VIRGINIA"))
			{
				GetStateCode = 51;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "WA") || (fecherFoundation.Strings.UCase(strState) == "WASHINGTON"))
			{
				GetStateCode = 53;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "WV") || (fecherFoundation.Strings.UCase(strState) == "WEST VIRGINIA"))
			{
				GetStateCode = 54;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "WI") || (fecherFoundation.Strings.UCase(strState) == "WISCONSIN"))
			{
				GetStateCode = 55;
			}
			else if ((fecherFoundation.Strings.UCase(strState) == "WY") || (fecherFoundation.Strings.UCase(strState) == "WYOMING"))
			{
				GetStateCode = 56;
			}
			else
			{
				// default to maine
				GetStateCode = 23;
			}
			return GetStateCode;
		}

		public static void CreateElectronicW2File()
		{
			// create the file then copy to a floppy
			frmW2Report.InstancePtr.Init();
		}

		public static string escapequote(ref string a)
		{
			string escapequote = "";
			// searches for a singlequote and puts another next to it
			// this fixes problems with sql statements that have 's in the text
			string tstring = "";
			string t2string = "";
			int stringindex;
			// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
			int intpos = 0;
			string strReturn;
			strReturn = a;
			escapequote = a;
			if (strReturn == string.Empty)
				return escapequote;
			stringindex = 1;
			while (stringindex <= strReturn.Length)
			{
				intpos = Strings.InStr(stringindex, strReturn, "'", CompareConstants.vbBinaryCompare);
				if (intpos == 0)
					break;
				tstring = Strings.Mid(strReturn, 1, intpos);
				t2string = Strings.Mid(strReturn, intpos + 1);
				if (intpos == strReturn.Length)
				{
					// strreturn = tstring & "' "
					strReturn = Strings.Mid(strReturn, 1, intpos - 1);
					break;
				}
				else
				{
					strReturn = tstring + "'" + t2string;
				}
				stringindex = intpos + 2;
			}
			escapequote = strReturn;
			return escapequote;
		}

		private static void UpdateMSRSAdjustments()
		{
			// fixes msrs adjustments that have no report type selected
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strSQL = "";
				if (clsSave.DoesFieldExist("MSRSUpdate", "tblfieldlengths", "twpy0000.vb1"))
				{
					clsSave.OpenRecordset("select * from tblfieldlengths", "twpy0000.vb1");
					if (clsSave.EndOfFile())
						return;
					if (!Information.IsDate(clsSave.Get_Fields("msrsupdate")))
					{
						clsSave.Edit();
						clsSave.Set_Fields("msrsupdate", DateTime.Today);
						clsSave.Update();
						strSQL = "select * from TBlcheckdetail where msrsadjustrecord = 1 and convert(int, isnull(reporttype, 0)) = " + FCConvert.ToString(CNSTMSRSREPORTTYPENONE);
						clsSave.OpenRecordset(strSQL, "twpy0000.vb1");
						while (!clsSave.EndOfFile())
						{
							clsLoad.OpenRecordset("select * from tblmsrsdistributiondetail where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and status = '" + clsSave.Get_Fields("statuscode") + "' and position = '" + clsSave.Get_Fields("distm") + "'", "twpy0000.vb1");
							if (!clsLoad.EndOfFile())
							{
								clsSave.Edit();
								clsSave.Set_Fields("reporttype", clsLoad.Get_Fields("reporttype"));
								clsSave.Update();
							}
							clsSave.MoveNext();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateAdjustments", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void UnemploymentHistoryFix()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL = "";
				// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
				object dtStart;
				object dtEnd;
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngCode = 0;
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsLoad2 = new clsDRWrapper();
				string strCode = "";
				string strNature = "";
				string strWhere = "";
				clsDRWrapper clsExecute = new clsDRWrapper();
				if (MessageBox.Show("Would you like to update unemployment records for all paydates?" + "\r\n" + "Choose no to specify a date range", "Update All?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// strSQL = "select employeenumber,distlinecode from tblcheckdetail where distributionrecord group by employeenumber,distlinecode"
					strSQL = "select EmployeeNumber,DistAutoID from tblcheckdetail where distributionrecord group by EmployeeNumber,DistAutoID";
					strWhere = "";
				}
				else
				{
					dtStart = FCConvert.ToDateTime(FCConvert.ToString(DateTime.Today.Month) + "/01/" + FCConvert.ToString(DateTime.Today.Year));
					if (!frmInput.InstancePtr.Init(ref dtStart, "Beginning Date", "Enter Start Date", 2880, false, modGlobalConstants.InputDTypes.idtDate))
					{
						return;
					}
					dtEnd = DateTime.Today;
					if (!frmInput.InstancePtr.Init(ref dtEnd, "End Date", "Enter End Date", 2880, false, modGlobalConstants.InputDTypes.idtDate, Strings.Format(DateTime.Today, "MM/dd/yyyy")))
					{
						return;
					}
					// strSQL = "Select employeenumber,distlinecode from tblCheckDetail where distributionrecord = 1 and paydate between '" & dtStart & "' and '" & dtEnd & "' group by employeenumber,distlinecode"
					strSQL = "Select EmployeeNumber,DistAutoID from tblCheckDetail where distributionrecord = 1 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' group by EmployeeNumber,DistAutoID";
					strWhere = " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' ";
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//App.DoEvents();
				frmWait.InstancePtr.Init("Checking Unemployment Records");
				// ************************************************************************************************
				// WE NEED TO SAVE THE ID FOR THE DISTRIBUTION LINE AND NOT LOOK AT THE LINE CODE AS IF
				// THE USER WAS TO DELETE A DISTRIBUTION LINE THEN IT RENUMBERS ALL OF THE RECORDNUMBER SO THE
				// THIS HISTORY FIX WOULD NOT WORK RIGHT. THIS BIT OF CODE WILL TAKE ALL HISTORY RECORDS AND TRY
				// AND FILL THEM WITH THE CURRENT ID'S. THIS WAS DONE FOR LINDA IN BUCKSPORT FOR THE PAY RUN.
				// MATTHEW 7/27/2005
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsNew = new clsDRWrapper();
				clsDRWrapper rsEmployee = new clsDRWrapper();
				rsEmployee.OpenRecordset("Select distinct EmployeeNumber from tblEmployeeMaster");
				while (!rsEmployee.EndOfFile())
				{
					//App.DoEvents();
					rsNew.OpenRecordset("Select * from tblPayrollDistribution where EmployeeNumber = '" + rsEmployee.Get_Fields("EmployeeNumber") + "'");
					rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" + rsEmployee.Get_Fields("EmployeeNumber") + "' AND distributionrecord = 1");
					while (!rsData.EndOfFile())
					{
						if (rsNew.FindFirstRecord("RecordNumber", Conversion.Val(rsData.Get_Fields_String("DistLineCode"))))
						{
							rsData.Edit();
							rsData.Set_Fields("DistAutoID", rsNew.Get_Fields("ID"));
							rsData.Update();
						}
						rsData.MoveNext();
					}
					rsEmployee.MoveNext();
				}
				// ************************************************************************************************
				clsSave.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsSave.EndOfFile())
				{
					//App.DoEvents();
					lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_Int32("DistAutoID"))));
					clsLoad2.OpenRecordset("select * from tblmiscupdate where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
					if (!clsLoad2.EndOfFile())
					{
						strCode = FCConvert.ToString(clsLoad2.Get_Fields("unemployment"));
						strNature = FCConvert.ToString(clsLoad2.Get_Fields("naturecode"));
						if (lngCode == 0)
						{
							clsLoad.OpenRecordset("select * from tblpayrolldistribution where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and recordnumber = " + FCConvert.ToString(lngCode), "twpy0000.vb1");
						}
						else
						{
							clsLoad.OpenRecordset("select * from tblpayrolldistribution where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and ID = " + FCConvert.ToString(lngCode), "twpy0000.vb1");
						}
						if (!clsLoad.EndOfFile())
						{
							if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("distu"))) == string.Empty)
							{
								// strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("distu"))) == "N")
							{
								// strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("distu"))) == 0)
							{
								// MATTHEW 6/29/2005 CALL ID 71694
								// strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = 'N' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("distu"))) == "Y")
							{
								// strSQL = "update tblcheckdetail set distu = '" & strCode & strNature & "' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = '" + strCode + strNature + "' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("distu"))) == "S")
							{
								// strSQL = "update tblcheckdetail set distu = 'S" & strNature & "' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = 'S" + strNature + "' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							else
							{
								// strSQL = "update tblcheckdetail set distu = '" & clsLoad.Fields("distu") & "' where employeenumber = '" & clsSave.Fields("employeenumber") & "' and distributionrecord = 1 and distlinecode = '" & lngCode & "' " & strWhere
								strSQL = "update tblcheckdetail set distu = '" + clsLoad.Get_Fields("distu") + "' where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and DistAutoID = " + FCConvert.ToString(lngCode) + " " + strWhere;
							}
							clsExecute.Execute(strSQL, "twpy0000.vb1");
						}
					}
					clsSave.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UnemploymentHistoryFix", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void UpdateMSRS(bool boolForceRun = false)
		{
			// applies a one-time fix to msrs fields in tblcheckdetail
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				clsDRWrapper rsMSRS = new clsDRWrapper();
				clsDRWrapper rsMSRSDetail = new clsDRWrapper();
				string strSQL = "";
				int intCode;
				bool boolEditing;
				DateTime dtStart;
				DateTime dtEnd;
				rsMSRS.OpenRecordset("Select * from tblMiscUpdate");
				rsMSRSDetail.OpenRecordset("Select * from tblMSRSDistributionDetail where boolDefault = 1");
				clsSave.Execute("UPDATE tblcheckdetail set reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPENONE) + " where distm = 'N'", "twpy0000.vb1");
				clsSave.Execute("update tblpayrolldistribution set msrs = 'Y' where msrsid = " + FCConvert.ToString(CNSTDISTMDEFAULT), "twpy0000.vb1");
				clsSave.Execute("update  tblpayrolldistribution inner join tblmsrsdistributiondetail on (tblpayrolldistribution.msrsid = tblmsrsdistributiondetail.ID) set msrs = position", "twpy0000.vb1");
				if (!boolForceRun)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					//App.DoEvents();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (boolForceRun)
				{
					frmWait.InstancePtr.Unload();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateMSRS", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckStatus()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSave.Execute("update tblemployeemaster set status = 'Suspended' where status = 'Inactive'", "twpy0000.vb1");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckStatus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckMSRSDistribution()
		{
			// makes sure that any distribution records that have something for distm (not an N) have msrsid set
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			string strMSRS = "";
			int lngID = 0;
			clsSave.Execute("update tblpayrolldistribution set msrsid = " + FCConvert.ToString(CNSTDISTMNOMSRS) + " where msrs = 'N' and convert(int, isnull(msrsid, 0)) = 0", "twpy0000.vb1");
			clsSave.OpenRecordset("select * from TBLpayrolldistribution where MSRS <> 'N'  and convert(int, isnull(msrsid, 0)) = 0", "twpy0000.vb1");
			while (!clsSave.EndOfFile())
			{
				strMSRS = "";
				if (FCConvert.ToString(clsSave.Get_Fields("msrs")) != "Y")
				{
					strMSRS = FCConvert.ToString(clsSave.Get_Fields("msrs"));
					clsLoad.OpenRecordset("select * from tblmsrsdistributiondetail where employeenumber = '" + clsSave.Get_Fields("employeenumber") + "' and position = '" + strMSRS + "' and status = '" + clsSave.Get_Fields("statuscode") + "' order by booldefault", "twpy0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						lngID = FCConvert.ToInt32(clsLoad.Get_Fields("ID"));
						clsLoad.Execute("update tblpayrolldistribution set msrsid = " + FCConvert.ToString(lngID) + " where ID = " + clsSave.Get_Fields("ID"), "twpy0000.vb1");
					}
					else
					{
						clsLoad.Execute("update tblpayrolldistribution set msrsid = " + FCConvert.ToString(CNSTDISTMDEFAULT) + " where ID = " + clsSave.Get_Fields("ID"), "twpy0000.vb1");
					}
				}
				else
				{
					clsLoad.Execute("update tblpayrolldistribution set msrsid = " + FCConvert.ToString(CNSTDISTMDEFAULT) + " where ID = " + clsSave.Get_Fields("ID"), "twpy0000.vb1");
				}
				clsSave.MoveNext();
			}
		}

		private static void CheckMPersIncludeStatus()
		{
			// Dim rsSave As New clsDRWrapper
			// Dim strSQL As String
			// strSQL = "update (select * from tblmsrsdistributiondetail inner join tblmiscupdate on (tblmiscupdate.employeenumber = tblmsrsdistributiondetail.employeenumber)) set include = false where status = '65'"
			// Call rsSave.Execute(strSQL, "twpy0000.vb1")
		}

		private static void FixDDAccountTypes()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			clsSave.Execute("update tblcheckdetail set ddaccounttype = '22' where ddaccounttype = '2'", "twpy0000.vb1");
			clsSave.Execute("update tblDirectDeposit set Accounttype = '22' where rtrim(isnull(accounttype, '')) = '' or rtrim(isnull(accounttype, '')) = '2'", "twpy0000.vb1");
		}

		public static int GetReportSequence()
		{
			int GetReportSequence = 0;
			int intReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetReportSequence = 0;
				intReturn = 0;
				clsLoad.OpenRecordset("select * from tblDefaultInformation", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					intReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("ReportSequence"))));
				}
				GetReportSequence = intReturn;
				return GetReportSequence;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetReportSequence", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetReportSequence;
		}

		public static void MakeIMCAFiles(ref DateTime dtPayDate)
		{
			clsICMA clsICMAFile = new clsICMA();
			if (clsICMAFile.CreateFiles(ref dtPayDate))
			{
				rptICMA.InstancePtr.Init(clsICMAFile.GetFileList());
			}
		}
		// vbPorter upgrade warning: rptObj As object	OnWrite(rptDataEntryForms, rptTaxSumaryReport, rptNewAuditHistory, rptFUTA940Report, rptMSRSPayrollSummary, rptEmployeeDeductionReport, rptEmployeeVacationSick, rptCurrEmployeeVacSick, rptDirectDeposit, rpt941Wage, rpt940, rptC1Laser, rptTrustAndAgencyChecks, rptMSRSNewWayPayrollDetail, rptMSRSNewPayrollSummaryTeacher, rpt941, rptStateSchoolUnemploymentC1, rptICMA, rptStateUnemploymentC1)
		// vbPorter upgrade warning: intModal As int	OnWrite(int, FCForm.FormShowEnum)
		public static void CheckDefaultPrint(FCSectionReport rptObj = null, string strPrinter = "", int intModal = (int)(FCForm.FormShowEnum.Modeless), bool boolDuplex = false, bool boolDuplexPairsMandatory = false, string strDuplexTitle = "Pages", bool boolLandscape = false, string strFileName = "", string strFileTitle = "TRIO Software", bool boolOverRideDuplex = false, bool boolAllowEmail = false, string strAttachmentName = "", bool boolDontAllowCloseTilReportDone = false)
		{
			string strPrn;
			strPrn = strPrinter;

			frmReportViewer.InstancePtr.Init(rptObj, strPrn, intModal, boolDuplex, boolDuplexPairsMandatory, strDuplexTitle, boolLandscape, strFileName, strFileTitle, boolOverRideDuplex, boolAllowEmail, strAttachmentName, false, boolDontAllowCloseTilReportDone, showModal: FCConvert.CBool(intModal));
		}
		// vbPorter upgrade warning: intTaxType As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(DateTime, string)
		public static double GetCYTDTaxWHGross(int intTaxType, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int intSchoolTown = CNSTSCHOOLORTOWNBOTH)
		{
			double GetCYTDTaxWHGross = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate;
			string strField = "";
			bool boolGross;
			string strWhereEmp = "";
			bool boolUnemployment;
			bool boolMSRS = false;
			boolUnemployment = false;
			boolGross = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				string strWhereSchool = "";
				switch (intSchoolTown)
				{
					case CNSTSCHOOLORTOWNTOWN:
						{
							strWhereSchool = " and not fromschool ";
							break;
						}
					case CNSTSCHOOLORTOWNSCHOOL:
						{
							strWhereSchool = " and fromschool ";
							break;
						}
					default:
						{
							strWhereSchool = "";
							break;
						}
				}
				//end switch
				GetCYTDTaxWHGross = 0;
				switch (intTaxType)
				{
					case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
						{
							strField = "FederalTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
						{
							strField = "FicaTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX:
						{
							strField = "EmployerFicaTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
						{
							strField = "MedicareTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX:
						{
							strField = "EmployerMedicareTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
						{
							strField = "StateTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFEDGROSS:
						{
							strField = "FederalTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICAGROSS:
						{
							strField = "FicaTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS:
						{
							strField = "MedicareTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATEGROSS:
						{
							strField = "StateTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPETOTALGROSS:
						{
							strField = "GrossPay";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT:
						{
							strField = "DistGrossPay";
							boolUnemployment = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS:
						{
							strField = "distgrosspay";
							boolMSRS = true;
							break;
						}
				}
				//end switch
				if (!boolGross && !boolUnemployment && !boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where isnull(checkvoid,0) = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolUnemployment)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and isnull(checkvoid,0) = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and isnull(checkvoid,0) = 0 and (isnull(reporttype,0) = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or isnull(reporttype,0) = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or isnull(reporttype,0) = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereEmp + strWhereSchool;
				}
				else
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and isnull(checkvoid,0) = 0 " + strWhereEmp + strWhereSchool;
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!boolUnemployment)
					{
						GetCYTDTaxWHGross = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						double dblTemp = 0;
						double dblUExempt = 0;
						dblTemp = Conversion.Val(rsLoad.Get_Fields("tot"));
						strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and checkvoid = 0 and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' " + strWhereEmp + strWhereSchool;
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblUExempt = Conversion.Val(rsLoad.Get_Fields("totded"));
						}
						else
						{
							dblUExempt = 0;
						}
						if (dblUExempt > dblTemp)
						{
							dblTemp = 0;
						}
						else
						{
							dblTemp -= dblUExempt;
						}
						GetCYTDTaxWHGross = dblTemp;
					}
				}
				return GetCYTDTaxWHGross;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCYTDFederalTaxWHGross", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCYTDTaxWHGross;
		}
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(DateTime, string)
		public static double GetCYTDPayTotal(int lngPayCat, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetCYTDPayTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetCYTDPayTotal = 0;
				strSQL = "Select sum(distgrosspay) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 " + strWhereEmp + " and distributionrecord = 1";
				if (lngPayCat >= 0)
					strSQL += " and distpaycatEGORY = " + FCConvert.ToString(lngPayCat) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCYTDPayTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCYTDPayTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCYTDPayTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCYTDPayTotal;
		}
		// vbPorter upgrade warning: intTaxType As object	OnWrite(int, double)
		public static double GetMTDTaxWHGross(int intTaxType, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int intSchoolTown = CNSTSCHOOLORTOWNBOTH)
		{
			double GetMTDTaxWHGross = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate;
			string strField = "";
			bool boolGross;
			string strWhereEmp = "";
			string strWhereSchool = "";
			bool boolUnemployment;
			bool boolMSRS;
			boolMSRS = false;
			boolUnemployment = false;
			boolGross = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				switch (intSchoolTown)
				{
					case CNSTSCHOOLORTOWNTOWN:
						{
							strWhereSchool = " and not fromschool ";
							break;
						}
					case CNSTSCHOOLORTOWNSCHOOL:
						{
							strWhereSchool = " and fromschool ";
							break;
						}
					default:
						{
							strWhereSchool = "";
							break;
						}
				}
				//end switch
				GetMTDTaxWHGross = 0;
				switch (intTaxType)
				{
					case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
						{
							strField = "FederalTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
						{
							strField = "FicaTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX:
						{
							strField = "EmployerFicaTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX:
						{
							strField = "EmployerMedicareTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
						{
							strField = "MedicareTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
						{
							strField = "StateTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFEDGROSS:
						{
							strField = "FederalTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICAGROSS:
						{
							strField = "FicaTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS:
						{
							strField = "MedicareTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATEGROSS:
						{
							strField = "StateTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPETOTALGROSS:
						{
							strField = "GrossPay";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT:
						{
							strField = "DistGrossPay";
							boolUnemployment = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS:
						{
							strField = "distgrosspay";
							boolMSRS = true;
							break;
						}
				}
				//end switch
				if (!boolGross && !boolUnemployment && !boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolUnemployment)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereEmp + strWhereSchool;
				}
				else
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '" + FCConvert.ToString(dtDate.Month) + "/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!boolUnemployment)
					{
						GetMTDTaxWHGross = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						double dblTemp = 0;
						double dblUExempt = 0;
						dblTemp = Conversion.Val(rsLoad.Get_Fields("tot"));
						strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtDate.Month) + "/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' " + strWhereEmp + strWhereSchool;
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblUExempt = Conversion.Val(rsLoad.Get_Fields("totded"));
						}
						else
						{
							dblUExempt = 0;
						}
						if (dblUExempt > dblTemp)
						{
							dblTemp = 0;
						}
						else
						{
							dblTemp -= dblUExempt;
						}
						GetMTDTaxWHGross = dblTemp;
					}
				}
				return GetMTDTaxWHGross;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetmTDFederalTaxWHGross", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMTDTaxWHGross;
		}

		public static string GetBox1Matches()
		{
			string GetBox1Matches = "";
			string strMatchBox1List;
			clsDRWrapper rsData = new clsDRWrapper();
			GetBox1Matches = "";
			strMatchBox1List = "";
			rsData.OpenRecordset("select * from tbldefaultdeductions where type = 'M'", "twpy0000.vb1");
			while (!rsData.EndOfFile())
			{
				strMatchBox1List += FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Int32("DeductionID"))) + ",";
				rsData.MoveNext();
			}
			if (strMatchBox1List != "")
			{
				strMatchBox1List = Strings.Mid(strMatchBox1List, 1, strMatchBox1List.Length - 1);
			}
			GetBox1Matches = strMatchBox1List;
			return GetBox1Matches;
		}

		public static double GetQTDBox1Matches(string strDedIDList, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, DateTime dtBeginDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetQTDBox1Matches = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			double dblReturn;
			string strList = "";
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string, DateTime)
			DateTime dtStartDate;
			int intQuarter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strDedIDList) == "")
				{
					GetQTDBox1Matches = 0;
					return GetQTDBox1Matches;
				}
				dblReturn = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				strSQL = "select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1 and matchdeductionnumber in (" + strDedIDList + ") ";
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				if (dtBeginDate.ToOADate() == 0)
				{
					switch (dtDate.Month)
					{
						case 1:
						case 2:
						case 3:
							{
								dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtDate.Year));
								break;
							}
						case 4:
						case 5:
						case 6:
							{
								dtStartDate = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(dtDate.Year));
								break;
							}
						case 7:
						case 8:
						case 9:
							{
								dtStartDate = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(dtDate.Year));
								break;
							}
						default:
							{
								dtStartDate = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(dtDate.Year));
								break;
							}
					}
					//end switch
				}
				else
				{
					dtStartDate = dtBeginDate;
				}
				strSQL += " and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					dblReturn = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				GetQTDBox1Matches = dblReturn;
				return GetQTDBox1Matches;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetQTDBox1Matches", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetQTDBox1Matches;
		}
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(string)
		public static double GetCYTDBox1Matches(ref string strDedIDList, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetCYTDBox1Matches = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			double dblReturn;
			string strList = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strDedIDList) == "")
				{
					GetCYTDBox1Matches = 0;
					return GetCYTDBox1Matches;
				}
				dblReturn = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				strSQL = "select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1 and matchdeductionnumber in (" + strDedIDList + ") ";
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					dblReturn = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				GetCYTDBox1Matches = dblReturn;
				return GetCYTDBox1Matches;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCYTDBox1Matches", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCYTDBox1Matches;
		}
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(DateTime, string)
		// vbPorter upgrade warning: lngMatchID As int	OnWrite(int, double)
		public static double GetCYTDMatchTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngMatchID = 0)
		{
			double GetCYTDMatchTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetCYTDMatchTotal = 0;
				strSQL = "Select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				if (lngMatchID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngMatchID) + " ";
				}
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCYTDMatchTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCYTDMatchTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCYTDMatchTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCYTDMatchTotal;
		}
		// vbPorter upgrade warning: dtCurrentDate As DateTime	OnWrite(DateTime, string)
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetCYTDDeductionTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetCYTDDeductionTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetCYTDDeductionTotal = 0;
				strSQL = "Select sum(dedamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and deductionrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID) + " ";
				}
				strSQL += " and paydate between '1/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCYTDDeductionTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCYTDDeductionTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCYTDDeductionTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCYTDDeductionTotal;
		}

		public static double GetMTDPayTotal(int lngPayCat, ref string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetMTDPayTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetMTDPayTotal = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				strSQL = "Select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and distributionrecord = 1";
				if (lngPayCat >= 0)
					strSQL += " and distpaycatEGORY = " + FCConvert.ToString(lngPayCat) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '" + FCConvert.ToString(dtDate.Month) + "/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetMTDPayTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetMTDPayTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetMTDPayTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMTDPayTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWrite(int, double)
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetMTDMatchTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetMTDMatchTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetMTDMatchTotal = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				strSQL = "Select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '" + FCConvert.ToString(dtDate.Month) + "/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetMTDMatchTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetMTDMatchTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetMTDMatchTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMTDMatchTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWrite(double, int)
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetMTDDeductionTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetMTDDeductionTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetMTDDeductionTotal = 0;
				strSQL = "Select sum(dedamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and deductionrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				strSQL += " and paydate between '" + FCConvert.ToString(dtDate.Month) + "/1/" + FCConvert.ToString(dtDate.Year) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetMTDDeductionTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetMTDDeductionTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetMTDDeductionTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMTDDeductionTotal;
		}
		// vbPorter upgrade warning: intTaxType As object	OnWrite(int, double)
		public static double GetQTDTaxWHGross(int intTaxType, string strEmpNum, DateTime? tempdtCurrentDate = null, int intSchoolTown = CNSTSCHOOLORTOWNBOTH)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtCurrentDate == null)
			{
				tempdtCurrentDate = DateTime.FromOADate(0);
			}
			DateTime dtCurrentDate = tempdtCurrentDate.Value;

			double GetQTDTaxWHGross = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate = DateTime.FromOADate(0);
			string strField = "";
			bool boolGross;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			string strWhereEmp = "";
			bool boolUnemployment;
			bool boolMSRS = false;
			boolGross = false;
			boolUnemployment = false;
            if(dtCurrentDate == default(DateTime))
            {
                dtCurrentDate = DateTime.FromOADate(0);
            }
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				string strWhereSchool = "";
				switch (intSchoolTown)
				{
					case CNSTSCHOOLORTOWNTOWN:
						{
							strWhereSchool = " and not fromschool ";
							break;
						}
					case CNSTSCHOOLORTOWNSCHOOL:
						{
							strWhereSchool = " and fromschool ";
							break;
						}
					default:
						{
							strWhereSchool = "";
							break;
						}
				}
				//end switch
				GetQTDTaxWHGross = 0;
				switch (intTaxType)
				{
					case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
						{
							strField = "FederalTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
						{
							strField = "FicaTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX:
						{
							strField = "EmployerFicaTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX:
						{
							strField = "EmployerMedicareTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
						{
							strField = "MedicareTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
						{
							strField = "StateTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFEDGROSS:
						{
							strField = "FederalTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICAGROSS:
						{
							strField = "FicaTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS:
						{
							strField = "MedicareTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATEGROSS:
						{
							strField = "StateTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPETOTALGROSS:
						{
							strField = "GrossPay";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT:
						{
							strField = "DistGrossPay";
							boolUnemployment = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS:
						{
							strField = "DistGrossPay";
							boolMSRS = true;
							break;
						}
				}
				//end switch
				if (!boolGross && !boolUnemployment && !boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolUnemployment)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereEmp + strWhereSchool;
				}
				else
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!boolUnemployment)
					{
						GetQTDTaxWHGross = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						double dblTemp = 0;
						double dblUExempt = 0;
						dblTemp = Conversion.Val(rsLoad.Get_Fields("tot"));
						strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' " + strWhereEmp + strWhereSchool;
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblUExempt = Conversion.Val(rsLoad.Get_Fields("totded"));
						}
						else
						{
							dblUExempt = 0;
						}
						if (dblUExempt > dblTemp)
						{
							dblTemp = 0;
						}
						else
						{
							dblTemp -= dblUExempt;
						}
						GetQTDTaxWHGross = dblTemp;
					}
				}
				return GetQTDTaxWHGross;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetQTDFederalTaxWHGross", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetQTDTaxWHGross;
		}

		public static double GetQTDPayTotal(int lngPayCat, ref string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetQTDPayTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetQTDPayTotal = 0;
				strSQL = "Select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and distributionrecord = 1";
				if (lngPayCat >= 0)
					strSQL += " and distpaycatEGORY = " + FCConvert.ToString(lngPayCat) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetQTDPayTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetQTDPayTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetQTDPayTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetQTDPayTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWrite(double, int)
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetQTDMatchTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetQTDMatchTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetQTDMatchTotal = 0;
				strSQL = "Select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetQTDMatchTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetQTDMatchTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetQTDMatchTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetQTDMatchTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWrite(double, int)
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetQTDDeductionTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetQTDDeductionTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetQTDDeductionTotal = 0;
				strSQL = "Select sum(dedamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and deductionrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetQTDDeductionTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetQTDDeductionTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetQTDDeductionTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetQTDDeductionTotal;
		}
		// vbPorter upgrade warning: intTaxType As object	OnWrite
		public static double GetFYTDTaxWHGross(int intTaxType, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int intSchoolTown = CNSTSCHOOLORTOWNBOTH)
		{
			double GetFYTDTaxWHGross = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate;
			string strField = "";
			bool boolGross;
			DateTime dtStart;
			string strWhereEmp = "";
			string strWhereSchool = "";
			bool boolUnemployment;
			bool boolMSRS = false;
			boolUnemployment = false;
			boolGross = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				switch (intSchoolTown)
				{
					case CNSTSCHOOLORTOWNTOWN:
						{
							strWhereSchool = " and not fromschool ";
							break;
						}
					case CNSTSCHOOLORTOWNSCHOOL:
						{
							strWhereSchool = " and fromschool ";
							break;
						}
					default:
						{
							strWhereSchool = "";
							break;
						}
				}
				//end switch
				GetFYTDTaxWHGross = 0;
				switch (intTaxType)
				{
					case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
						{
							strField = "FederalTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
						{
							strField = "FicaTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX:
						{
							strField = "EmployerFicaTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
						{
							strField = "MedicareTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX:
						{
							strField = "EmployerMedicareTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
						{
							strField = "StateTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFEDGROSS:
						{
							strField = "FederalTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICAGROSS:
						{
							strField = "FicaTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS:
						{
							strField = "MedicareTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATEGROSS:
						{
							strField = "StateTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPETOTALGROSS:
						{
							strField = "GrossPay";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT:
						{
							strField = "DistGrossPay";
							boolUnemployment = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS:
						{
							boolMSRS = true;
							strField = "DistGrossPay";
							break;
						}
				}
				//end switch
				if (!boolGross && !boolUnemployment && !boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolUnemployment)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				else if (boolMSRS)
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereEmp + strWhereSchool;
				}
				else
				{
					strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and checkvoid = 0 " + strWhereEmp + strWhereSchool;
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				dtStart = GetPrecedingFirstFiscalDay(dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!boolUnemployment)
					{
						GetFYTDTaxWHGross = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						double dblTemp = 0;
						double dblUExempt = 0;
						dblTemp = Conversion.Val(rsLoad.Get_Fields("tot"));
						strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' " + strWhereEmp + strWhereSchool;
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblUExempt = Conversion.Val(rsLoad.Get_Fields("totded"));
						}
						else
						{
							dblUExempt = 0;
						}
						if (dblUExempt > dblTemp)
						{
							dblTemp = 0;
						}
						else
						{
							dblTemp -= dblUExempt;
						}
						GetFYTDTaxWHGross = dblTemp;
					}
				}
				return GetFYTDTaxWHGross;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetFYTDFederalTaxWHGross", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetFYTDTaxWHGross;
		}

		public static double GetFYTDPayTotal(int lngPayCat, ref string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetFYTDPayTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart;
			DateTime dtEnd;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetFYTDPayTotal = 0;
				strSQL = "Select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and distributionrecord = 1";
				if (lngPayCat >= 0)
					strSQL += " and distpaycatEGORY = " + FCConvert.ToString(lngPayCat) + " ";
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				dtStart = GetPrecedingFirstFiscalDay(dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetFYTDPayTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetFYTDPayTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetFYTDPayTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetFYTDPayTotal;
		}
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetFYTDMatchTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetFYTDMatchTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				GetFYTDMatchTotal = 0;
				strSQL = "Select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and matchrecord = 1";
				if (lngDedID >= 0)
					strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				dtStart = GetPrecedingFirstFiscalDay(dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetFYTDMatchTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetFYTDMatchTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetFYTDMatchTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetFYTDMatchTotal;
		}
		// vbPorter upgrade warning: lngRecID As int	OnWrite(int, double)
		public static double GetFYTDDeductionTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetFYTDDeductionTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			DateTime dtDate;
			DateTime dtStart;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetFYTDDeductionTotal = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				strSQL = "Select sum(dedamount) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and deductionrecord = 1";
				// - id means all
				if (lngDedID >= 0)
					strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				dtStart = GetPrecedingFirstFiscalDay(dtDate);
				strSQL += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetFYTDDeductionTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetFYTDDeductionTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetFYTDDeductionTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetFYTDDeductionTotal;
		}
		// vbPorter upgrade warning: intTaxType As object	OnWrite(int, double)
		public static double GetCurrentTaxWHGross(int intTaxType, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int intSchoolTown = CNSTSCHOOLORTOWNBOTH)
		{
			double GetCurrentTaxWHGross = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate;
			string strField = "";
			bool boolGross;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			bool boolUseTemp = false;
			bool boolUnemployment;
			bool boolMSRS = false;
			boolUnemployment = false;
			boolGross = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetCurrentTaxWHGross = 0;
				string strWhereSchool = "";
				switch (intSchoolTown)
				{
					case CNSTSCHOOLORTOWNTOWN:
						{
							strWhereSchool = " and not fromschool ";
							break;
						}
					case CNSTSCHOOLORTOWNSCHOOL:
						{
							strWhereSchool = " and fromschool ";
							break;
						}
					default:
						{
							strWhereSchool = "";
							break;
						}
				}
				//end switch
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(dtDate) + "' order by payrunid desc", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("VerifyAccept")))
					{
						boolUseTemp = true;
					}
				}
				switch (intTaxType)
				{
					case modPYConstants.CNSTPAYTOTALTYPEFEDTAX:
						{
							strField = "FederalTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICATAX:
						{
							strField = "FicaTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERFICATAX:
						{
							strField = "EmployerFicaTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX:
						{
							strField = "EmployerMedicareTax";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX:
						{
							strField = "MedicareTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATETAX:
						{
							strField = "StateTaxWH";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFEDGROSS:
						{
							strField = "FederalTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEFICAGROSS:
						{
							strField = "FicaTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS:
						{
							strField = "MedicareTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPESTATEGROSS:
						{
							strField = "StateTaxGross";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPETOTALGROSS:
						{
							strField = "GrossPay";
							boolGross = true;
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT:
						{
							boolUnemployment = true;
							strField = "DistGrossPay";
							break;
						}
					case modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS:
						{
							boolMSRS = true;
							strField = "DistGrossPay";
							break;
						}
				}
				//end switch
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					if (!boolGross && !boolUnemployment && !boolMSRS)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where isnull(checkvoid,0) = 0 and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where  employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
					}
					else if (boolUnemployment)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and isnull(checkvoid,0) = 0 and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> ''  and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
					}
					else if (boolMSRS)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and isnull(checkvoid,0) = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ") and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where distm <> 'N' and len(isnull(distm, '')) > 0 and isnull(checkvoid,0) = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ") and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
					}
					else
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and isnull(checkvoid,0) = 0 and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where TotalRecord = 1 and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
						}
					}
					if (!boolUseTemp)
					{
						strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
					}
				}
				else
				{
					if (!boolGross && !boolUnemployment && !boolMSRS)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where isnull(checkvoid,0) = 0 " + strWhereSchool;
							if (!boolUseTemp)
							{
								strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
							}
						}
						else
						{
							if (strWhereSchool == string.Empty)
							{
								strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess  ";
							}
							else
							{
								strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where " + strWhereSchool;
							}
						}
					}
					else if (boolUnemployment)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> '' and isnull(checkvoid,0) = 0 " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where distributionrecord = 1 and isnull(distu, '') <> 'N' and isnull(distu, '') <> ''  " + strWhereSchool;
						}
						if (!boolUseTemp)
						{
							strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
						}
					}
					else if (boolMSRS)
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where distm <> 'N' and len(isnull(distm, '')) > 0 and isnull(checkvoid,0) = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where distm <> 'N' and len(isnull(distm, '')) > 0 and isnull(checkvoid,0) = 0 and (reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPEPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPESCHOOLPLD) + " or reporttype = " + FCConvert.ToString(CNSTMSRSREPORTTYPETEACHER) + ")" + strWhereSchool;
						}
						if (!boolUseTemp)
						{
							strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
						}
					}
					else
					{
						if (!boolUseTemp)
						{
							strSQL = "select sum(" + strField + ") as tot from tblcheckdetail where totalrecord = 1 and isnull(checkvoid,0) = 0 " + strWhereSchool;
						}
						else
						{
							strSQL = "select sum(" + strField + ") as tot from tbltemppayprocess where totalrecord  " + strWhereSchool;
						}
						if (!boolUseTemp)
						{
							strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
						}
					}
				}
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!boolUnemployment)
					{
						GetCurrentTaxWHGross = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						double dblTemp = 0;
						double dblUExempt = 0;
						dblTemp = Conversion.Val(rsLoad.Get_Fields("tot"));
						if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
						{
							if (!boolUseTemp)
							{
								strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and isnull(checkvoid,0) = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
							}
							else
							{
								strSQL = "select sum(dedamount) as totded from tbltemppayprocess where uexempt = 1 and deductionrecord = 1 and isnull(checkvoid,0) = 0  and employeenumber = '" + strEmpNum + "' " + strWhereSchool;
							}
						}
						else
						{
							if (!boolUseTemp)
							{
								strSQL = "select sum(dedamount) as totded from tblcheckdetail where uexempt = 1 and deductionrecord = 1 and isnull(checkvoid,0) = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtDate) + "' " + strWhereSchool;
							}
							else
							{
								strSQL = "select sum(dedamount) as totded from tbltemppayprocess where uexempt = 1 and deductionrecord = 1 and isnull(checkvoid,0) = 0  " + strWhereSchool;
							}
						}
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblUExempt = Conversion.Val(rsLoad.Get_Fields("totded"));
						}
						else
						{
							dblUExempt = 0;
						}
						if (dblUExempt > dblTemp)
						{
							dblTemp = 0;
						}
						else
						{
							dblTemp -= dblUExempt;
						}
						GetCurrentTaxWHGross = dblTemp;
					}
				}
				return GetCurrentTaxWHGross;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCurrentTaxWHGross", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCurrentTaxWHGross;
		}

		public static double GetCurrentPayTotal(int lngPayCat, ref string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/)
		{
			double GetCurrentPayTotal = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			DateTime dtDate;
			DateTime dtStart;
			bool boolUseTemp;
			string strWhereEmp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolUseTemp = false;
				GetCurrentPayTotal = 0;
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " employeenumber = '" + strEmpNum + "'";
				}
				else
				{
					strWhereEmp = "";
				}
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				else
				{
					dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				}
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(dtDate) + "' order by payrunid desc", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("VerifyAccept")))
					{
						boolUseTemp = true;
					}
				}
				if (!boolUseTemp)
				{
					if (strWhereEmp != string.Empty)
					{
						strWhereEmp = " and " + strWhereEmp;
					}
					strSQL = "Select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 " + strWhereEmp + " and distributionrecord = 1";
					strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
				}
				else
				{
					if (strWhereEmp != "")
					{
						strWhereEmp += " and ";
					}
					strSQL = "Select sum(distgrosspay) as tot from tbltemppayprocess where " + strWhereEmp + "  distributionrecord = 1";
				}
				if (lngPayCat >= 0)
					strSQL += " and distpaycatEGORY = " + FCConvert.ToString(lngPayCat) + " ";
				rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCurrentPayTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCurrentPayTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCurrentPayTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCurrentPayTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWrite(int, double)
		public static double GetCurrentMatchTotal(int lngDedID, string strEmpNum, DateTime dtCurrentDate/*= FCConvert.ToDateTime("12:00:00 AM")*/, int lngRecID = 0)
		{
			double GetCurrentMatchTotal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				DateTime dtDate;
				bool boolUseTemp;
				string strWhereEmp = "";
				GetCurrentMatchTotal = 0;
				dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				boolUseTemp = false;
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(dtDate) + "' order by payrunid desc", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("VerifyAccept")))
					{
						boolUseTemp = true;
					}
				}
				if (!boolUseTemp)
				{
					strSQL = "Select sum(matchamount) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 " + strWhereEmp + " and matchrecord = 1";
					if (lngDedID >= 0)
						strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID);
					strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
				}
				else
				{
					strSQL = "Select sum(matchamount) as tot from tbltemppayprocess where isnull(checkvoid,0) = 0 " + strWhereEmp + " and matchrecord = 1";
					if (lngDedID >= 0)
						strSQL += " and matchdeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				}
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCurrentMatchTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCurrentMatchTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCurrentMatchTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCurrentMatchTotal;
		}
		// vbPorter upgrade warning: lngDedID As int	OnWriteFCConvert.ToDouble(
		public static double GetCurrentDeductionTotal(int lngDedID, string strEmpNum, DateTime? tempdtCurrentDate = null, int lngRecID = 0)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtCurrentDate == null)
			{
				tempdtCurrentDate = DateTime.FromOADate(0);
			}
			DateTime dtCurrentDate = tempdtCurrentDate.Value;

			double GetCurrentDeductionTotal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				DateTime dtDate;
				bool boolUseTemp;
				string strWhereEmp = "";
				GetCurrentDeductionTotal = 0;
				dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				boolUseTemp = false;
				//if (dtCurrentDate.ToOADate() != FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				if (dtCurrentDate.ToOADate() != 0)
				{
					dtDate = dtCurrentDate;
				}
				if (fecherFoundation.Strings.Trim(strEmpNum) != string.Empty)
				{
					strWhereEmp = " and employeenumber = '" + strEmpNum + "' ";
				}
				else
				{
					strWhereEmp = "";
				}
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(dtDate) + "' order by payrunid desc", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("VerifyAccept")))
					{
						boolUseTemp = true;
					}
				}
				if (!boolUseTemp)
				{
					strSQL = "Select sum(dedamount) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 " + strWhereEmp + " and deductionrecord = 1";
					if (lngDedID >= 0)
						strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID);
					strSQL += " and paydate = '" + FCConvert.ToString(dtDate) + "' ";
				}
				else
				{
					strSQL = "Select sum(dedamount) as tot from tbltemppayprocess where isnull(checkvoid,0) = 0 " + strWhereEmp + "  and deductionrecord = 1";
					if (lngDedID >= 0)
						strSQL += " and deddeductionnumber = " + FCConvert.ToString(lngDedID) + " ";
				}
				if (lngRecID > 0)
				{
					strSQL += " and empdeductionid = " + FCConvert.ToString(lngRecID);
				}
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					GetCurrentDeductionTotal = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				return GetCurrentDeductionTotal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCurrentDeductionTotal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCurrentDeductionTotal;
		}		

		public static void CheckStateTaxStatus()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from tblpaystatuses where  type = 'STATE' and description = 'Married - 2 Incomes'", "twpy0000.vb1");
				if (!rsSave.EndOfFile())
				{
					// Call rsSave.Execute("update tblemployeemaster set statefilingstatusid = 10 where statefilingstatusid = 6", "twpy0000.vb1")
					rsSave.Execute("update tblemployeemaster set statefilingstatusid = 6 where statefilingstatusid = 8", "twpy0000.vb1");
					rsSave.Execute("delete from tblpaystatuses where type = 'STATE' and description = 'Married - 2 Incomes'", "twpy0000.vb1");
					rsSave.Execute("update tblpaystatuses set description = 'Married', USETABLE = 'MARRIED' where type = 'STATE' and ID = 6", "twpy0000.vb1");
				}
				rsSave.Execute("UPDATE TBLpaystatuses set usetable = 'MARRIED' where ID = 6", "twpy0000.vb1");
				rsSave.OpenRecordset("select * from tblpaystatuses where type = 'STATE' and description = 'Married Filing Single'", "twpy0000.vb1");
				if (rsSave.EndOfFile())
				{
					rsSave.Execute("insert into tblpaystatuses (ID,statuscode,description,usetable,type) values (10,3,'Married Filing Single','SINGLE','STATE')", "twpy0000.vb1");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckStateTaxStatus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void AddFrequencyCode()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from tblfrequencycodes where ID = 23", "tWPY0000.VB1");
				if (rsSave.EndOfFile())
				{
					string strSQL = "";
					strSQL = "Insert into tblfrequencycodes (ID,FrequencyCode,Description,LastUpdate,PeriodsPerYear) values (23,'F','Semi-Annually (When Coded)',#" + FCConvert.ToString(DateTime.Now) + "#,2)";
					rsSave.Execute(strSQL, "twpy0000.vb1");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		public static int CreateAJournal(string strJournalDescription, int lngPeriod = 0, string strType = "GJ")
		{
			int CreateAJournal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngJournal;
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				int TempJournal = 0;
				lngJournal = 0;
				CreateAJournal = 0;
				TryLockAgain:
				;
				// attempt to lock the journal table to get a number
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					// if unable to lock show a message and leave the function
					frmWait.InstancePtr.Unload();
					switch (MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently locking the journal table.  You must wait until they are finished before you save.  Would you like to retry?", "Unable to Save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								goto TryLockAgain;
							}
						default:
							{
								// give the user an option of saving the records in a BD table or resetting the locks
								switch (MessageBox.Show("If you are sure that no one is currently using the TRIO Budgetary Accounting System then you can reset the Budgetary lock controls.  Would you like to unlock the controls now?" + "\r\n" + "Select No to retry.", "Unlock Controls", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
								{
									case DialogResult.Yes:
										{
											// CYA Entry
											modGlobalFunctions.AddCYAEntry_6("PY", "Unlocked BD Controls for Journal Entry.");
											// unlock controls
											modBudgetaryAccounting.UnlockJournal();
											goto TryLockAgain;
										}
									case DialogResult.No:
										{
											switch (MessageBox.Show("If you are sure that no one is currently using the TRIO Budgetary Accounting System then you can save this information in a temporary table.  Would you like to save the Budgetary entries now?" + "\r\n" + "Select No to retry.", "Unlock Controls", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
											{
												case DialogResult.Yes:
													{
														// save the entries into a fake journal
														modGlobalFunctions.AddCYAEntry_6("PY", "Saved BD entries.");
														// TempJournalMaster is the temporary table in BD
														rsTemp.OpenRecordset("SELECT * FROM TempJournalEntries ORDER BY JournalNumber Desc", "TWBD0000.vb1");
														if (rsTemp.EndOfFile() != true)
														{
															TempJournal = rsTemp.Get_Fields("JournalNumber") + 1;
														}
														else
														{
															TempJournal = 1;
														}
														break;
													}
												case DialogResult.No:
													{
														goto TryLockAgain;
													}
												default:
													{
														goto TryLockAgain;
													}
											}
											//end switch
											break;
										}
									default:
										{
											goto TryLockAgain;
										}
								}
								//end switch
								break;
							}
					}
					//end switch
				}
				else
				{
					// everything is fine
					// get the next available journal
					Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC", "TWBD0000.vb1");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						TempJournal = Master.Get_Fields("JournalNumber") + 1;
					}
					else
					{
						TempJournal = 1;
					}
					lngJournal = TempJournal;
					// add a new journal to the JournalMaster table
					Master.AddNew();
					Master.Set_Fields("JournalNumber", TempJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", fecherFoundation.DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					Master.Set_Fields("Description", Strings.Left(strJournalDescription, 25));
					Master.Set_Fields("Type", strType);
					if (lngPeriod > 0 && lngPeriod <= 12)
					{
						Master.Set_Fields("Period", lngPeriod);
					}
					else
					{
						Master.Set_Fields("Period", DateTime.Today.Month);
					}
					Master.Update();
					Master.Reset();
					// unlock the journal table so other people can get journal numbers
					modBudgetaryAccounting.UnlockJournal();
				}
				CreateAJournal = lngJournal;
				return CreateAJournal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateAJournal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateAJournal;
		}

		public static DateTime GetEaster(ref int Y)
		{
			DateTime GetEaster = System.DateTime.Now;
			// The algorithm is due to J.-M. Oudin (1940) and is reprinted in the Explanatory Supplement to the Astronomical Almanac, ed. P. K. Seidelmann (1992)
			// vbPorter upgrade warning: FirstDig As object	OnWrite
			// vbPorter upgrade warning: Remain19 As object	OnWrite
			// vbPorter upgrade warning: tEmp As object	OnWrite
			double FirstDig, Remain19, tEmp;
			// intermediate results
			// vbPorter upgrade warning: tA As object	OnWrite
			// vbPorter upgrade warning: tB As object	OnWrite
			// vbPorter upgrade warning: tC As object	OnWrite
			// vbPorter upgrade warning: tD As object	OnWrite
			// vbPorter upgrade warning: tE As object	OnWrite
			double tA, tB, tC, tD, tE;
			// table A to E results
			// vbPorter upgrade warning: dtReturn As DateTime	OnWrite(string)
			DateTime dtReturn;
			int m = 0;
			// vbPorter upgrade warning: d As int	OnWrite(double, int)
			int d;
			FirstDig = Y / 100;
			// first 2 digits of year
			Remain19 = Y % 19;
			// remainder of year / 19
			// calculate PFM date
			tEmp = (FirstDig - 15) / 2 + 202 - 11 * Remain19;
			if ((FirstDig == 21) || (FirstDig == 24) || (FirstDig == 25) || (FirstDig >= 27 && FirstDig <= 32) || (FirstDig == 34) || (FirstDig == 35) || (FirstDig == 38))
			{
				tEmp -= 1;
			}
			else if ((FirstDig == 33) || (FirstDig == 36) || (FirstDig == 37) || (FirstDig == 39) || (FirstDig == 40))
			{
				tEmp -= 2;
			}
			tEmp = FCUtils.iMod(tEmp, 30);
			tA = tEmp + 21;
			if (FCConvert.ToInt32(tEmp) == 29)
				tA -= 1;
			if (FCConvert.ToInt32(tEmp) == 28 && Remain19 > 10)
				tA -= 1;
			// find the next Sunday
			tB = (tA - 19) % 7;
			tC = (40 - FirstDig) % 4;
			if (FCConvert.ToInt32(tC) == 3)
				tC += 1;
			if (tC > 1)
				tC += 1;
			tEmp = Y % 100;
			tD = (tEmp + FCUtils.iDiv(tEmp, 4)) % 7;
			tE = ((20 - tB - tC - tD) % 7) + 1;
			d = FCConvert.ToInt16(tA + tE);
			// return the date
			if (d > 31)
			{
				d -= 31;
				m = 4;
			}
			else
			{
				m = 3;
			}
			dtReturn = FCConvert.ToDateTime(FCConvert.ToString(m) + "/" + FCConvert.ToString(d) + "/" + FCConvert.ToString(Y));
			GetEaster = dtReturn;
			return GetEaster;
		}

		public static void UndoCurrentPayRun()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				// vbPorter upgrade warning: dtTargetDate As DateTime	OnWrite
				DateTime dtTargetDate;
				bool boolSameFiscal;
				bool boolSameCalendar;
				bool boolEncumberContracts = false;
				int lngJournalNum = 0;
				int lngReverseJournal;
				int lngBank = 0;
				bool boolOK;
				boolOK = true;
				lngReverseJournal = 0;
				rsTemp.OpenRecordset("select * from tblDefaultInformation", "twpy0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					boolEncumberContracts = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("encumbercontracts"));
				}
				dtTargetDate = DateTime.FromOADate(0);
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					lngJournalNum = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("JournalNumber"))));
					lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("banknumber"))));
				}
				else
				{
					MessageBox.Show("Error loading current pay run information" + "\r\n" + "Unable to continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				if (modGlobalVariables.Statics.gintCurrentPayRun > 1)
				{
					rsLoad.OpenRecordset("select * from tblPayrollSteps where convert(int, isnull(numberofpayweeks, 0)) > 0 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun - 1), "twpy0000.vb1");
				}
				else
				{
					rsLoad.OpenRecordset("select top 1 * from tblPayrollSteps where convert(int, isnull(numberofpayweeks, 0)) > 0 and not (paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ) order by paydate desc,payrunid desc", "twpy0000.vb1");
				}
				boolSameFiscal = true;
				boolSameCalendar = true;
				if (!rsLoad.EndOfFile())
				{
					dtTargetDate = (DateTime)rsLoad.Get_Fields("paydate");
					if (dtTargetDate.Year == modGlobalVariables.Statics.gdatCurrentPayDate.Year)
					{
						boolSameCalendar = true;
					}
					else
					{
						boolSameCalendar = false;
					}
					if (fecherFoundation.DateAndTime.DateDiff("d", GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate), dtTargetDate) >= 0)
					{
						boolSameFiscal = true;
					}
					else
					{
						boolSameFiscal = false;
					}
				}
				// if end of file then we can void this payrun but can't go back to the last one.
				if (MessageBox.Show("This will reverse the current pay run" + "\r\n" + "Do you want to continue?", "Reverse Pay Run?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					object lngReturn = 0;
					if (frmInput.InstancePtr.Init(ref lngReturn, "Access Code", "Call TRIO for today's access code", 2340, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
					{
						if (!modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngReturn), "PY"))
						{
							MessageBox.Show("Invalid Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
					}
					else
					{
						return;
					}
					// Find out what kind of reversal.  Checks might not have been created yet
					modGlobalFunctions.AddCYAEntry_6("PY", "Reversed Payrun", "Paydate " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate), "Payrun " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun));
					rsTemp.OpenRecordset("select * from tblPayrollSteps where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
					if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("verifyaccept")))
					{
						// deduction life totals
						rsTemp.OpenRecordset("select sum(dedamount) as totDed,employeenumber,empdeductionid from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and  checkvoid = 0  and deductionrecord = 1 group by employeenumber,empdeductionid", "twpy0000.vb1");
						while (!rsTemp.EndOfFile())
						{
							rsSave.Execute("update tblEmployeeDeductions set ltdtotal = 0 where ltdtotal <= " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("totded"))) + " and ID = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("empdeductionid"))), "twpy0000.vb1");
							rsSave.Execute("update tblEmployeeDeductions set ltdtotal = ltdtotal - " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("totded"))) + " where ltdtotal > " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("totded"))) + " and ID = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("empdeductionid"))), "twpy0000.vb1");
							rsTemp.MoveNext();
						}
						// vacation sick
						rsTemp.OpenRecordset("select sum(vsaccrued) as totAccrued,sum(vsused) as totUsed,vstypeid,employeenumber,vsbalance from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and  checkvoid = 0 and vsrecord = 1 group by employeenumber,vstypeid,vsbalance", "twpy0000.vb1");
						while (!rsTemp.EndOfFile())
						{
							if (boolSameCalendar)
							{
								rsSave.Execute("update tblvacationsick  set accruedcalendar = accruedcalendar - " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("TOTAccrued"))) + ",usedcalendar = usedcalendar + " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("TOTused"))) + "   where typeid = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("vstypeid"))) + " and employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
							}
							if (boolSameFiscal)
							{
								rsSave.Execute("update tblvacationsick  set accruedfiscal = accruedfiscal - " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("TOTAccrued"))) + ",usedfiscal = usedfiscal + " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("TOTused"))) + "   where typeid = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("vstypeid"))) + " and employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
							}
							rsSave.Execute("update tblvacationsick  set usedbalance = " + rsTemp.Get_Fields("vsbalance") + ",usedcurrent = 0,accruedcurrent = 0  where typeid = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("vstypeid"))) + " and employeenumber = '" + rsTemp.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
							rsTemp.MoveNext();
						}
						// contracts
						if (boolEncumberContracts)
						{
							rsTemp.OpenRecordset("SELECT DistAccountNumber,SUM(DistGrossPay) as TotalPay, contractid FROM tblcheckdetail WHERE checkvoid = 0 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and  distributionrecord = 1 and contractid > 0 GROUP BY DistAccountNumber,contractid HAVING SUM(DistGrossPay) <> 0 ORDER BY contractid,distaccountnumber", "twpy0000.vb1");
							int lngEncumbranceID = 0;
							int lngEncumbranceJournal;
							clsContract conCT = new clsContract();
							double dblTotal = 0;
							while (!rsTemp.EndOfFile())
							{
								if (conCT.ContractID != FCConvert.ToInt32(rsTemp.Get_Fields("contractid")))
								{
									conCT.LoadContract(FCConvert.ToInt32(rsTemp.Get_Fields("contractid")));
									lngEncumbranceID = conCT.EncumbranceID;
									lngEncumbranceJournal = conCT.EncumbranceJournal;
								}
								dblTotal = Conversion.Val(rsTemp.Get_Fields("totalpay"));
								rsSave.OpenRecordset("SELECT * FROM ENCUMBRANCEDETAIL where ACCOUNT = '" + rsTemp.Get_Fields("DISTACCOUNTNUMBER") + "' AND RECORDNUMBER = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
								if (!rsSave.EndOfFile())
								{
									rsSave.Edit();
									rsSave.Set_Fields("liquidated", rsSave.Get_Fields("liquidated") - dblTotal);
									rsSave.Update();
									rsSave.Execute("update encumbrances set liquidated = liquidated - " + FCConvert.ToString(dblTotal) + " where ID = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
								}
								rsTemp.MoveNext();
							}
						}
						// restore ta adjustments
						rsTemp.OpenRecordset("select * from tareductionadjust where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrun = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
						rsSave.OpenRecordset("select * from tblTAReduction order by recipientid,deductionid", "twpy0000.vb1");
						while (!rsTemp.EndOfFile())
						{
							if (rsSave.FindFirstRecord2("recipientid,categoryid,deductionaccountnumber,deductionid,reporttype", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("recipientid"))) + "," + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("categoryid"))) + "," + rsTemp.Get_Fields("deductionaccount") + "," + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("deductionid"))) + "," + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("reporttype"))), ","))
							{
								rsSave.Edit();
								rsSave.Set_Fields("Amount", rsSave.Get_Fields("Amount") - Conversion.Val(rsTemp.Get_Fields("amount")));
								rsSave.Update();
								if (FCConvert.ToInt32(rsSave.Get_Fields("amount")) == 0)
								{
									rsSave.Delete();
								}
							}
							else
							{
								// must make a new one
								rsSave.AddNew();
								rsSave.Set_Fields("Date", DateTime.Today);
								rsSave.Set_Fields("Time", DateTime.Now);
								rsSave.Set_Fields("Recipientid", rsTemp.Get_Fields("recipientid"));
								rsSave.Set_Fields("CategoryID", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("categoryid"))));
								rsSave.Set_Fields("DeductionID", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("DeductionID"))));
								rsSave.Set_Fields("Amount", -Conversion.Val(rsTemp.Get_Fields("amount")));
								rsSave.Set_Fields("CheckNumber", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("checknumber"))));
								rsSave.Set_Fields("DeductionAccountNumber", rsTemp.Get_Fields_String("DeductionAccount"));
								rsSave.Set_Fields("reporttype", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("reporttype"))));
								rsSave.Update();
							}
							rsTemp.MoveNext();
						}
						rsTemp.Execute("delete from tareductionadjust where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrun = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
						if (modGlobalConstants.Statics.gboolBD && boolSameFiscal)
						{
							// look for journal. If unposted just delete it
							// if posted then reverse it
							if (lngJournalNum > 0)
							{
								rsTemp.OpenRecordset("select * from journalmaster where journalnumber = " + FCConvert.ToString(lngJournalNum), "twbd0000.vb1");
								if (!rsTemp.EndOfFile())
								{
									if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("status"))) == "P")
									{
										// posted
										modGlobalFunctions.AddCYAEntry_6("BD", "Payroll Reversed Payrun", "PayDate " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate), "Payrun " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "Created reversing entries");
										lngReverseJournal = ReverseBDJournal(lngJournalNum, lngBank);
										boolOK = lngReverseJournal > 0;
									}
									else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("status"))) == "E")
									{
										// unposted
										// just delete it
										modGlobalFunctions.AddCYAEntry_6("BD", "Payroll Reversed Payrun", "PayDate " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate), "Payrun " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "Deleted unposted journal " + FCConvert.ToString(lngJournalNum));
										rsTemp.Execute("delete from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNum), "twbd0000.vb1");
										rsTemp.Execute("delete from journalmaster where journalnumber = " + FCConvert.ToString(lngJournalNum), "twbd0000.vb1");
									}
									else
									{
										// d for deleted but anything else we don't mess with anyway
									}
								}
							}
							rsTemp.Execute("delete from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
						}
					}
					else
					{
					}
					if (dtTargetDate.ToOADate() != 0 && modGlobalVariables.Statics.gintCurrentPayRun == 1)
					{
						clsScheduleList tSchedList = new clsScheduleList();
						if (tSchedList.LoadSchedules())
						{
							tSchedList.MoveFirst();
							if (tSchedList.MoveNext() >= 0)
							{
								if (MessageBox.Show("Do you want to reverse all employees weekly rotations by one week?", "Reverse Rotations?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									tSchedList.ReverseWeekRotations();
								}
							}
						}
					}
					if (!rsLoad.EndOfFile())
					{
						// get rid of the current step and set current info to the last step
						rsSave.OpenRecordset("select * from tblpayrollprocesssetup", "twpy0000.vb1");
						rsSave.Edit();
						rsSave.Set_Fields("weeknumber", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("weeknumber"))));
						rsSave.Set_Fields("FirstWeekOf", rsLoad.Get_Fields("firstweekof"));
						rsSave.Set_Fields("WeekEndDate", rsLoad.Get_Fields("weekenddate"));
						rsSave.Set_Fields("PeriodStartDate", rsLoad.Get_Fields_DateTime("PeriodStartDate"));
						rsSave.Set_Fields("Paydate", rsLoad.Get_Fields("paydate"));
						rsSave.Set_Fields("NumberofPayweeks", rsLoad.Get_Fields("NumberofPayWeeks"));
						rsSave.Set_Fields("LastUpdated", rsLoad.Get_Fields("lastupdated"));
						rsSave.Set_Fields("UpdatedBy", rsLoad.Get_Fields("updatedby"));
						rsSave.Set_Fields("MonthOf", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int16("MonthOf"))));
						rsSave.Set_Fields("PayBiWeeklys", rsLoad.Get_Fields_Boolean("PayBiWeeklys"));
						rsSave.Set_Fields("PayRunID", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("PayRunID"))));
						rsSave.Set_Fields("ManualCheckEmpID", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ManualCheckEmpID"))));
						rsSave.Set_Fields("GroupID", rsLoad.Get_Fields("groupid"));
						rsSave.Set_Fields("GroupText", rsLoad.Get_Fields_String("GroupText"));
						rsSave.Set_Fields("TAChecksPrinted", rsLoad.Get_Fields_String("TAChecksPrinted"));
						rsSave.Set_Fields("BIWeeklyDeds", rsLoad.Get_Fields_Boolean("BiWeeklyDeds"));
						rsSave.Set_Fields("UseGroupMultiFund", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int16("UseGroupMultiFund"))));
						rsSave.Update();
						rsSave.Execute("DELETE FROM tblPayrollSteps where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
						modGlobalVariables.Statics.gdatCurrentPayDate = (DateTime)rsLoad.Get_Fields_DateTime("PayDate");
						modGlobalVariables.Statics.gintCurrentPayRun = FCConvert.ToInt16(rsLoad.Get_Fields("payrunid"));
						modGlobalVariables.Statics.gdatCurrentWeekEndingDate = (DateTime)rsLoad.Get_Fields("weekenddate");
						if (boolOK)
						{
							if (lngReverseJournal > 0)
							{
								MessageBox.Show("Pay run was reversed and journal " + FCConvert.ToString(lngReverseJournal) + " was created.", "Pay Run Reversed", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								MessageBox.Show("Pay run was reversed.", "Pay Run Reversed", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					else
					{
						if (boolOK)
						{
							// MsgBox "The current pay information was reversed but there was not enough information to make the previous pay run the current one."
							if (lngReverseJournal > 0)
							{
								MessageBox.Show("Pay run was reversed and journal " + FCConvert.ToString(lngReverseJournal) + " was created. File setup dates were not reset.", "Pay Run Reversed", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								MessageBox.Show("Pay run was reversed. File setup dates were not reset.", "Pay Run Reversed", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					// If boolOK Then
					// MsgBox "Current paydate is now " & gdatCurrentPayDate & " and current pay run is now " & gintCurrentPayRun, vbInformation, "Pay Run Reversed"
					// End If
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UndoCurrentPayrun", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static int ReverseBDJournal(int lngJournalNum, int lngBank)
		{
			int ReverseBDJournal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ReverseBDJournal = 0;
				modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				modGlobalVariables.Statics.SortedAccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				int intArrayID;
				int lngJournalNumber;
				string strAcctOR = "";
				double dblValue;
				string strCashAcct;
				string strSchoolCashAcct;
				intArrayID = 0;
				modAccountTitle.SetAccountFormats();
				clsDRWrapper rsLoad = new clsDRWrapper();
				// Dim rsSchoolAccounts As New clsDRWrapper
				rsLoad.OpenRecordset("select * from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNum) + " and rcb <> 'L' order by number", "twbd0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
					modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsLoad.Get_Fields("Account"));
					modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
					modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = -Conversion.Val(rsLoad.Get_Fields("amount"));
					modGlobalVariables.Statics.AccountStrut[intArrayID].Description = rsLoad.Get_Fields_String("description");
					modGlobalVariables.Statics.AccountStrut[intArrayID].Project = FCConvert.ToString(rsLoad.Get_Fields("project"));
					modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = rsLoad.Get_Fields_String("RCB");
					intArrayID += 1;
					rsLoad.MoveNext();
				}
				if (Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) > 0)
				{
					Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) - 1 + 1);
				}
				// NOW GET THE CHECKING SUM RECORD
				dblValue = 0;
				rsLoad.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
				// rsPayrollAccounts.FindFirstRecord "Description", "'Cash / Checking'"
				strCashAcct = "";
				strSchoolCashAcct = "";
				if (!rsLoad.EndOfFile())
				{
					strCashAcct = FCConvert.ToString(rsLoad.Get_Fields("account"));
				}

				modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);

				int lngTownCashIndex = 0;
				int lngSchoolCashIndex;
				int lngCurrCashIndex;
				int lngTempIndex;
				string strCurrAcct = "";
				string strTempAcct = "";
				int lngSwapIndex;
                strAcctOR = "";
                strAcctOR = modBudgetaryAccounting.GetBankOverride(ref lngBank, true);
                if (strAcctOR != string.Empty)
                {
                    // had default cash account that may need to be split
                    modBudgetaryAccounting.CalcCashFundsFromOverride(ref strAcctOR, ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " PY", "PY", false);
                }
                else
                {
                    if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, lngTownCashIndex, true))
                    {
                    }
                }
				for (lngTempIndex = 0; lngTempIndex <= Information.UBound(modGlobalVariables.Statics.AccountStrut, 1); lngTempIndex++)
				{
					if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.AccountStrut[lngTempIndex].RCB) == "E")
					{
						modGlobalVariables.Statics.AccountStrut[lngTempIndex].Amount *= -1;
					}
				}
				// lngTempIndex
				lngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modGlobalVariables.Statics.AccountStrut, "PY", 0, DateTime.Today, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " PY Reversal");
				ReverseBDJournal = lngJournalNumber;
				return ReverseBDJournal;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReverseBDJournal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ReverseBDJournal;
		}

		public class StaticVariables
		{
			public MSRSSummaryRecord MSRSSummaryRec = new MSRSSummaryRecord();
			public bool gboolUsesDueToDueFroms;
			private bool boolAddedMSRSTables;
			public bool gboolESP;
			public MSRSDetailRecord MSRSDetRec = new MSRSDetailRecord();
			public bool gboolTimeClockPlus;
			public EWRData EWRWageTotals = new EWRData();
			public string gstrEditList = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
