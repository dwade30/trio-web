//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmStartupWait.
	/// </summary>
	partial class frmStartupWait
	{
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 429);
			this.BottomPanel.Size = new System.Drawing.Size(515, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Location = new System.Drawing.Point(0, 0);
			this.ClientArea.Size = new System.Drawing.Size(515, 429);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(515, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 61);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(290, 126);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "CHANGES ARE BEING MADE TO YOUR DATABASE. MAKE SURE NO ONE ELSE RUNS THIS PROGRAM " +
    "UNTIL THIS MESSAGE DISAPPEARS";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(290, 31);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE WAIT";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// frmStartupWait
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(349, 220);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.Name = "frmStartupWait";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Activated += new System.EventHandler(this.frmStartupWait_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion
	}
}