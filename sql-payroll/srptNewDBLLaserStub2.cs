﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewDBLLaserStub2.
	/// </summary>
	public partial class srptNewDBLLaserStub2 : FCSectionReport
	{
		public srptNewDBLLaserStub2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptNewDBLLaserStub2 InstancePtr
		{
			get
			{
				return (srptNewDBLLaserStub2)Sys.GetInstance(typeof(srptNewDBLLaserStub2));
			}
		}

		protected srptNewDBLLaserStub2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptNewDBLLaserStub2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTMAXSTUBLINES = 23;
		const int CNSTMAXBenLines = 12;
		const int CNSTMaxDedLines = 20;
		const int CNSTMaxPayLines = 12;
		const int CNSTMaxBankLines = 7;
		private int intMaxPayLine;
		private int intMaxDedLine;
		private bool boolTestPrint;
		const int CNSTFICACOL = 46;
		const int CNSTMEDCOL = 47;
		const int CNSTYTDFICACOL = 48;
		const int CNSTYTDMEDCOL = 49;
		const int CNSTDEPTDIVDESCCOL = 50;
		const int CNSTYTDFedGrossCol = 51;
		const int CNSTYTDFicaGrossCol = 52;
		const int CNSTYTDMedGrossCol = 53;
		const int CNSTYTDStateGrossCol = 54;
		const int CNSTMatchDescCol = 8;
		const int CNSTMatchAmountCol = 9;
		const int CNSTMatchYTDAmountCol = 10;
		const int CNSTBankAmountCol = 6;
		const int CNSTBankDescCol = 7;
		private bool boolShowIndividualPayRates;
        private bool boolShowPayPeriod;

		public void Init(bool boolPrintTest)
		{
			boolTestPrint = boolPrintTest;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			boolShowIndividualPayRates = (this.ParentReport as rptLaserCheck1).ShowIndividualPayRates;
			if (boolShowIndividualPayRates)
			{
				lblIndRate.Visible = true;
			}
			else
			{
				lblIndRate.Visible = false;
			}
			int x;
			for (x = 1; x <= CNSTMaxPayLines; x++)
			{
				if (boolShowIndividualPayRates)
				{
					Detail.Controls["txtRate" + x].Visible = true;
				}
				else
				{
					Detail.Controls["txtRate" + x].Visible = false;
				}
			}
			// x
			if (!boolTestPrint)
			{
				// CreateDynamicControls
				ClearFields();
			}
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= CNSTMaxDedLines; x++)
			{
				(Detail.Controls["txtDedDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtDedYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMAXBenLines; x++)
			{
				(Detail.Controls["lblBenefit" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblBenAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblBenYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMaxPayLines; x++)
			{
				(Detail.Controls["txtPayDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtHours" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtPayAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtRate" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= CNSTMaxBankLines; x++)
			{
				(Detail.Controls["txtBank" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["txtBankAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			for (x = 1; x <= 6; x++)
			{
				(Detail.Controls["lblCode" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Accrued"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Taken"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblCode" + x + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
			}
			// x
			if (boolShowIndividualPayRates)
			{
				lblIndRate.Text = "Rate";
			}
			else
			{
				lblIndRate.Text = "";
			}
			txtTotalHours.Text = "0.00";
			txtTotalAmount.Text = "0.00";
			txtEMatchCurrent.Text = "0.00";
			txtEmatchYTD.Text = "0.00";
			txtCurrentDeductions.Text = "0.00";
			txtTotalYTDDeductions.Text = "0.00";
			txtCheckNo.Text = "";
			txtDirectDeposit.Text = "";
			txtDirectDepSav.Text = "";
			txtChkAmount.Text = "";
			txtEmployeeNo.Text = "";
			txtPayRate.Text = "0.00";
			txtDepartment.Text = "";
			txtDate.Text = "";
			txtName.Text = "";
		}

		private void CreateDynamicControls()
		{
			GrapeCity.ActiveReports.SectionReportModel.Label ctl;
			int CRow = 0;
			int x;
			float lngStartTop;
			string strTemp = "";
			lngStartTop = txtPayDesc1.Top;
			intMaxPayLine = 1;
			intMaxDedLine = 1;
			// get the max number of pay lines and the max deduction lines
			// there won't be more since the parent report has already taken into
			// account what cnstmaxstublines is.  This can be changed to put the burden
			// on the stub but would have to be done to both stub sub reports.
			for (x = 1; x <= CNSTMAXSTUBLINES; x++)
			{
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 2)) != string.Empty)
				{
					intMaxPayLine = x + 1;
				}
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 4)) != string.Empty)
				{
					intMaxDedLine = x + 1;
				}
			}

			for (x = 2; x <= intMaxPayLine; x++)
			{
				CRow = x;
                strTemp = "txtPayDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 180) / 1440F;
				ctl.Width = txtPayDesc1.Width;
				ctl.Left = txtPayDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtPayDesc1.Height;
				Fields.Add(ctl.Name);
				ctl.Font = new Font(txtPayDesc1.Font.Name, ctl.Font.Size);

                strTemp = "txtHours" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 180) / 1440F;
				ctl.Width = txtHours1.Width;
				ctl.Left = txtHours1.Left;
				ctl.Height = txtHours1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				Fields.Add(ctl.Name);
				ctl.Font = new Font(txtHours1.Font.Name, ctl.Font.Size);

                strTemp = "txtPayAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 180) / 1440F;
				ctl.Width = txtPayAmount1.Width;
				ctl.Left = txtPayAmount1.Left;
				ctl.Height = txtPayAmount1.Height;
				ctl.Alignment = txtPayAmount1.Alignment;
				Fields.Add(ctl.Name);
				ctl.Font = new Font(txtPayAmount1.Font.Name, ctl.Font.Size);
			}

			lngStartTop = txtDedDesc1.Top;
			for (x = 2; x <= intMaxDedLine; x++)
			{
				CRow = x;
                strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedDesc1.Width;
				ctl.Left = txtDedDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtDedDesc1.Height;
				//strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedDesc1.Font.Name, ctl.Font.Size);
                //Detail.Controls.Add(ctl);

                strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedAmount1.Width;
				ctl.Left = txtDedAmount1.Left;
				ctl.Height = txtDedAmount1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedAmount1.Font.Name, ctl.Font.Size);
                //Detail.Controls.Add(ctl);

                strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedYTD1.Width;
				ctl.Left = txtDedYTD1.Left;
				ctl.Height = txtDedYTD1.Height;
				ctl.Alignment = txtDedYTD1.Alignment;
				//strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedYTD1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            boolShowPayPeriod = (this.ParentReport as rptLaserCheck1).PrintPayPeriod;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// fill textboxes from grid
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			int x;
			if (!boolTestPrint)
			{
                cPayRun pRun;
                pRun = (this.ParentReport as rptLaserCheck1).PayRun;
                if (!boolShowPayPeriod)
                {
                    lblCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
                }
                else
                {
                    if (!(pRun == null))
                    {
                        if (Information.IsDate(pRun.PeriodStart) && Information.IsDate(pRun.PeriodEnd))
                        {
                            lblCheckMessage.Text = "Pay Period: " + Strings.Format(pRun.PeriodStart, "MM/dd/yyyy") + " to " + Strings.Format(pRun.PeriodEnd, "MM/dd/yyyy");
                        }
                    }
                }
				// 
				for (x = 0; x <= CNSTMaxPayLines - 1; x++)
				{
					if (rptLaserCheck1.InstancePtr.Grid.Rows > x)
					{
						(Detail.Controls["txtPayDesc" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 0);
						(Detail.Controls["txtHours" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 1);
						(Detail.Controls["txtPayAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 2);
						if (boolShowIndividualPayRates)
						{
							(Detail.Controls["txtRate" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 11);
						}
					}
				}
				// x
				for (x = 0; x <= CNSTMaxDedLines - 1; x++)
				{
					if (rptLaserCheck1.InstancePtr.Grid.Rows > x)
					{
						(Detail.Controls["txtDedDesc" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 3);
						(Detail.Controls["txtDedAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 4);
						(Detail.Controls["txtDedYTD" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, 5);
					}
				}
				// x
				for (x = 0; x <= CNSTMAXBenLines - 1; x++)
				{
					if (rptLaserCheck1.InstancePtr.Grid.Rows > x)
					{
						(Detail.Controls["lblBenefit" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTMatchDescCol);
						(Detail.Controls["lblBenAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTMatchAmountCol);
						(Detail.Controls["lblBenYTD" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTMatchYTDAmountCol);
					}
				}
				// x
				for (x = 0; x <= CNSTMaxBankLines - 1; x++)
				{
					if (rptLaserCheck1.InstancePtr.Grid.Rows > x)
					{
						(Detail.Controls["txtBank" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTBankDescCol);
						if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTBankDescCol)) != "")
						{
							(Detail.Controls["txtBankAmount" + (x + 1)] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(x, CNSTBankAmountCol);
						}
					}
				}

				dblDirectDepChk = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 29));
				dblDirectDepSav = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 34));
				dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
				txtDate.Text = Strings.Format(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 24), "MM/dd/yyyy");
				txtCurrentGross.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 5);
				txtCurrentFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 6);
				txtCurrentFica.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTFICACOL);
				txtCurrentMedicare.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTMEDCOL);
				txtCurrentState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 8);
				txtCurrentDeductions.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 23);
				txtCurrentNet.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9);
				txtVacationBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 0);
				txtSickBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 1);
				
				txtYTDGross.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 13);
				txtYTDFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 14);
				txtYTDFICA.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDFICACOL);
				txtYTDMedicare.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDMEDCOL);
				txtYTDFedTaxable.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDFedGrossCol);
				txtYTDFicaTaxable.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDFicaGrossCol);
				txtYTDMedicareTaxable.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDMedGrossCol);
				txtYTDStateTaxable.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTYTDStateGrossCol);
				txtYTDState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 16);
				txtYTDNet.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 17);
				txtTotalHours.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 3);
				txtTotalAmount.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 4);
				txtEMatchCurrent.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 25);
				txtEmatchYTD.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 26);
				txtEmployeeNo.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 2).Replace("EMPLOYEE", ""));
				txtName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
				txtCheckNo.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12).Replace("CHECK", ""));
				txtTotalYTDDeductions.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 42);
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTDEPTDIVDESCCOL)) != "")
				{
					txtDepartment.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, CNSTDEPTDIVDESCCOL);
					lblDepartmentLabel.Visible = true;
				}
				else
				{
					txtDepartment.Text = "";
					lblDepartmentLabel.Visible = false;
				}
				if (dblDirectDeposit > 0)
				{
					Line9.Visible = true;
					lineDirectDepositBottom.Visible = true;
					lineDirectDepositLeft.Visible = true;
					lineDirectDepositRight.Visible = true;
					txtDirectDeposit.Visible = true;
					txtDirectDepositLabel.Visible = true;
					txtChkAmount.Visible = true;
					lblCheckAmount.Visible = true;
					txtDirectDepChkLabel.Visible = true;
					txtDirectDepositSavLabel.Visible = true;
					txtDirectDepSav.Visible = true;
					txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
					txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
					txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)) - dblDirectDeposit, "0.00");
				}
				else
				{
					txtDirectDepChkLabel.Visible = false;
					Line9.Visible = false;
					lineDirectDepositBottom.Visible = false;
					lineDirectDepositLeft.Visible = false;
					lineDirectDepositRight.Visible = false;
					txtDirectDepositSavLabel.Visible = false;
					txtDirectDeposit.Visible = false;
					txtDirectDepositLabel.Visible = false;
					txtChkAmount.Visible = false;
					lblCheckAmount.Visible = false;
					txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)), "0.00");
				}
				if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31)) == string.Empty)
				{
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
				}
				else
				{
					lblPayRate.Visible = true;
					txtPayRate.Visible = true;
					txtPayRate.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31));
				}
			
				cVacSickHistoryItem vsHistoryItem;
				int vsCount = 1;
                double totAccrued = 0;
                double totTaken = 0;
                double totBalance = 0;

				for (x = 1; x <= rptLaserCheck1.InstancePtr.VacSickHistoryColl.Count; x++)
				{
					vsHistoryItem = rptLaserCheck1.InstancePtr.VacSickHistoryColl[x];
					if (fecherFoundation.Strings.LCase(vsHistoryItem.Description) == "vacation")
					{
						txtVacationAccrued.Text = Strings.Format(vsHistoryItem.Accrued, "0.00");
						txtVacationUsed.Text = Strings.Format(vsHistoryItem.Used, "0.00");
					}
					else if (fecherFoundation.Strings.LCase(vsHistoryItem.Description) == "sick")
					{
						txtSickAccrued.Text = Strings.Format(vsHistoryItem.Accrued, "0.00");
						txtSickTaken.Text = Strings.Format(vsHistoryItem.Used, "0.00");
					}
					else
					{
                        totAccrued = totAccrued + Math.Round(vsHistoryItem.Accrued, 2, MidpointRounding.AwayFromZero);
                        totTaken = totTaken + Math.Round(vsHistoryItem.Used, 2, MidpointRounding.AwayFromZero);
                        totBalance = totBalance + Math.Round(vsHistoryItem.Balance, 2, MidpointRounding.AwayFromZero);
						if (vsCount < 7)
						{
							(Detail.Controls["lblCode" + vsCount] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = vsHistoryItem.Description;
							(Detail.Controls["lblCode" + vsCount + "Accrued"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Accrued, "0.00");
							(Detail.Controls["lblCode" + vsCount + "Taken"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Used, "0.00");
							(Detail.Controls["lblCode" + vsCount + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Balance, "0.00");
						}
                        else
                        {
                            lblCode6.Text = "Other";
                            (Detail.Controls["lblCode" + vsCount + "Accrued"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Accrued, "0.00");
                            (Detail.Controls["lblCode" + vsCount + "Taken"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Used, "0.00");
                            (Detail.Controls["lblCode" + vsCount + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(vsHistoryItem.Balance, "0.00");
                        }
						vsCount += 1;
					}
				}
				// x
			}
		}
	}
}
