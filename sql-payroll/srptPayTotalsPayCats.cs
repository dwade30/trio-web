//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptPayTotalsPayCats.
	/// </summary>
	public partial class srptPayTotalsPayCats : FCSectionReport
	{
		public srptPayTotalsPayCats()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptPayTotalsPayCats InstancePtr
		{
			get
			{
				return (srptPayTotalsPayCats)Sys.GetInstance(typeof(srptPayTotalsPayCats));
			}
		}

		protected srptPayTotalsPayCats _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRep?.Dispose();
                rsRep = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptPayTotalsPayCats	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsRep = new clsDRWrapper();
		string strEmp;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsRep.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strEmp = FCConvert.ToString(this.UserData);
			rsRep.OpenRecordset("select ID,CATEGORYNUMBER,DESCRIPTION from tblpaycategories ORDER BY ID", "TWPY0000.vb1");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsRep.EndOfFile())
			{
				
				txtDesc.Text = rsRep.Get_Fields_String("description");
				//txtCurrent.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetCurrentPayTotal(rsRep.Get_Fields("ID"), ref strEmp, FCConvert.ToDateTime("12:00:00 AM"))), "#,###,##0.00");
				//txtMTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetMTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, FCConvert.ToDateTime("12:00:00 AM"))), "#,###,##0.00");
				//txtQTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetQTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, FCConvert.ToDateTime("12:00:00 AM"))), "#,###,##0.00");
				//txtFYTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetFYTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, FCConvert.ToDateTime("12:00:00 AM"))), "#,###,##0.00");
				//txtCYTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetCYTDPayTotal(rsRep.Get_Fields("ID"), strEmp, FCConvert.ToDateTime("12:00:00 AM"))), "#,###,##0.00");
				txtCurrent.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetCurrentPayTotal(rsRep.Get_Fields("ID"), ref strEmp, DateTime.FromOADate(0))), "#,###,##0.00");
				txtMTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetMTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, DateTime.FromOADate(0))), "#,###,##0.00");
				txtQTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetQTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, DateTime.FromOADate(0))), "#,###,##0.00");
				txtFYTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetFYTDPayTotal(rsRep.Get_Fields("ID"), ref strEmp, DateTime.FromOADate(0))), "#,###,##0.00");
				txtCYTD.Text = Strings.Format(Conversion.Val(modCoreysSweeterCode.GetCYTDPayTotal(rsRep.Get_Fields("ID"), strEmp, DateTime.FromOADate(0))), "#,###,##0.00");
				rsRep.MoveNext();
			}
		}

		
	}
}
