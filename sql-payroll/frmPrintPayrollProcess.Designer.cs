//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPrintPayrollProcess.
	/// </summary>
	partial class frmPrintPayrollProcess
	{
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintDetail;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuAll;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsData = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintDetail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 503);
            this.BottomPanel.Size = new System.Drawing.Size(864, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Size = new System.Drawing.Size(864, 443);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSelectAll);
            this.TopPanel.Size = new System.Drawing.Size(864, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(241, 30);
            this.HeaderText.Text = "Print Payroll Process";
            // 
            // vsData
            // 
            this.vsData.AllowSelection = false;
            this.vsData.AllowUserToResizeColumns = false;
            this.vsData.AllowUserToResizeRows = false;
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsData.BackColorBkg = System.Drawing.Color.Empty;
            this.vsData.BackColorFixed = System.Drawing.Color.Empty;
            this.vsData.BackColorSel = System.Drawing.Color.Empty;
            this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsData.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsData.ColumnHeadersHeight = 30;
            this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsData.DragIcon = null;
            this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsData.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsData.FixedCols = 0;
            this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsData.FrozenCols = 0;
            this.vsData.GridColor = System.Drawing.Color.Empty;
            this.vsData.GridColorFixed = System.Drawing.Color.Empty;
            this.vsData.Location = new System.Drawing.Point(30, 30);
            this.vsData.Name = "vsData";
            this.vsData.OutlineCol = 0;
            this.vsData.ReadOnly = true;
            this.vsData.RowHeadersVisible = false;
            this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsData.RowHeightMin = 0;
            this.vsData.Rows = 50;
            this.vsData.ScrollTipText = null;
            this.vsData.ShowColumnVisibilityMenu = false;
            this.vsData.Size = new System.Drawing.Size(804, 402);
            this.vsData.StandardTab = true;
            this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsData.TabIndex = 0;
            this.vsData.Click += new System.EventHandler(this.vsData_ClickEvent);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintDetail,
            this.mnuSP2,
            this.mnuAll,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrintDetail
            // 
            this.mnuPrintDetail.Index = 0;
            this.mnuPrintDetail.Name = "mnuPrintDetail";
            this.mnuPrintDetail.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintDetail.Text = "Print Detail Report";
            this.mnuPrintDetail.Click += new System.EventHandler(this.mnuPrintDetail_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuAll
            // 
            this.mnuAll.Index = 2;
            this.mnuAll.Name = "mnuAll";
            this.mnuAll.Text = "Select/Deselect All";
            this.mnuAll.Click += new System.EventHandler(this.mnuAll_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(305, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(190, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print Detail Report";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrintDetail_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelectAll.AppearanceKey = "toolbarButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(696, 29);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(140, 24);
            this.cmdSelectAll.TabIndex = 1;
            this.cmdSelectAll.Text = "Select / Deselect All";
            this.cmdSelectAll.Click += new System.EventHandler(this.mnuAll_Click);
            // 
            // frmPrintPayrollProcess
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(864, 611);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrintPayrollProcess";
            this.Text = "Print Payroll Process";
            this.Load += new System.EventHandler(this.frmPrintPayrollProcess_Load);
            this.Activated += new System.EventHandler(this.frmPrintPayrollProcess_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintPayrollProcess_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintPayrollProcess_KeyPress);
            this.Resize += new System.EventHandler(this.frmPrintPayrollProcess_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrint;
		private FCButton cmdSelectAll;
	}
}