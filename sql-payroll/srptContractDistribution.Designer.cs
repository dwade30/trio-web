﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptContractDistribution.
	/// </summary>
	partial class srptContractDistribution
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptContractDistribution));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemaining = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaydate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemaining)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaydate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtAmount,
				this.txtPaid,
				this.txtRemaining,
				this.txtPaydate
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1666667F;
			this.txtAccount.Left = 0.8020833F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 9pt; text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.01041667F;
			this.txtAccount.Width = 1.552083F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1666667F;
			this.txtAmount.Left = 2.395833F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-size: 9pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.01041667F;
			this.txtAmount.Width = 0.7395833F;
			// 
			// txtPaid
			// 
			this.txtPaid.Height = 0.1666667F;
			this.txtPaid.Left = 3.166667F;
			this.txtPaid.Name = "txtPaid";
			this.txtPaid.Style = "font-size: 9pt; text-align: right";
			this.txtPaid.Text = null;
			this.txtPaid.Top = 0.01041667F;
			this.txtPaid.Width = 0.7395833F;
			// 
			// txtRemaining
			// 
			this.txtRemaining.Height = 0.1666667F;
			this.txtRemaining.Left = 3.9375F;
			this.txtRemaining.Name = "txtRemaining";
			this.txtRemaining.Style = "font-size: 9pt; text-align: right";
			this.txtRemaining.Text = null;
			this.txtRemaining.Top = 0.01041667F;
			this.txtRemaining.Width = 0.7395833F;
			// 
			// txtPaydate
			// 
			this.txtPaydate.Height = 0.1666667F;
			this.txtPaydate.Left = 0F;
			this.txtPaydate.Name = "txtPaydate";
			this.txtPaydate.Style = "font-size: 9pt; text-align: right";
			this.txtPaydate.Text = null;
			this.txtPaydate.Top = 0.01041667F;
			this.txtPaydate.Width = 0.7395833F;
			// 
			// srptContractDistribution
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 4.6875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemaining)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaydate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemaining;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaydate;
	}
}
