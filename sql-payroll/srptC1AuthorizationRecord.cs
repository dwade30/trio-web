﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1AuthorizationRecord.
	/// </summary>
	public partial class srptC1AuthorizationRecord : FCSectionReport
	{
		public srptC1AuthorizationRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1AuthorizationRecord InstancePtr
		{
			get
			{
				return (srptC1AuthorizationRecord)Sys.GetInstance(typeof(srptC1AuthorizationRecord));
			}
		}

		protected srptC1AuthorizationRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1AuthorizationRecord	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strRec;
			string strTemp;
			strRec = FCConvert.ToString(this.UserData);
			strTemp = Strings.Mid(strRec, 2, 4);
			txtYearCovered.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 6, 9);
			txtFedID.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 147, 44);
			txtCompany.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.Mid(strRec, 191, 35);
			txtAddress.Text = fecherFoundation.Strings.Trim(strTemp);
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 226, 20)) + ", " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 246, 2)) + "  " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 253, 10));
			txtCityStateZip.Text = fecherFoundation.Strings.Trim(strTemp);
		}

		
	}
}
