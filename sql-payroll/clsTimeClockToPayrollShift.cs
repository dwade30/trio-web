//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsTimeClockToPayrollShift
	{
		//=========================================================
		private clsDRWrapper rsLoad = new clsDRWrapper();

		public clsTimeClockToPayrollShift() : base()
		{
			string strSQL;
			strSQL = "Select * from TIMECLOCKSHIFTS order by timecardshift";
			rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
		}

		public int GetPayrollShift(string strTimeShift)
		{
			int GetPayrollShift = 0;
			if (!(rsLoad.EndOfFile() && rsLoad.BeginningOfFile()))
			{
				if (rsLoad.FindFirstRecord("timecardshift", strTimeShift))
				{
					GetPayrollShift = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("payrollshift"))));
				}
				else
				{
					GetPayrollShift = 0;
				}
			}
			else
			{
				GetPayrollShift = 0;
			}
			return GetPayrollShift;
		}
	}
}
