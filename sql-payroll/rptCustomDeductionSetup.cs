//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCustomDeductionSetup.
	/// </summary>
	public partial class rptCustomDeductionSetup : BaseSectionReport
	{
		public rptCustomDeductionSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Deduction Setup";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomDeductionSetup InstancePtr
		{
			get
			{
				return (rptCustomDeductionSetup)Sys.GetInstance(typeof(rptCustomDeductionSetup));
			}
		}

		protected rptCustomDeductionSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsDeductions?.Dispose();
				rsTax?.Dispose();
				rsFreq?.Dispose();
                rsData = null;
                rsDeductions = null;
                rsTax = null;
                rsFreq = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomDeductionSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsDeductions = new clsDRWrapper();
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsTax = new clsDRWrapper();
		clsDRWrapper rsFreq = new clsDRWrapper();
		int intpage;
		bool boolShade;
		double dblTotCurrent;
		double dblTotCYTD;
		double dblTotFYTD;
		double dblTotMTD;
		double dblTotQTD;
		double dblTemp;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void RptCustomDeductionSetup_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("fldHeader");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			if (this.Fields["fldHeader"].Value != rsData.Get_Fields("EmployeeNumber"))
			{
				this.Fields["fldHeader"].Value = rsData.Get_Fields("EmployeeNumber");
				txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + "   " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
				boolShade = !boolShade;
			}
			txtDeductionDescription.Text = FCConvert.ToString(GetDeductionDescription(rsData.Get_Fields_Int32("DeductionCode")));
			txtTaxStatus.Text = FCConvert.ToString(GetTaxStatusDescription(rsData.Get_Fields_Int32("DeductionCode")));
			rsDeductions.FindFirstRecord("ID", rsData.Get_Fields_Int32("DeductionCode"));
			// If Trim(rsData.Fields("AmountType")) = vbNullString Then
			// THIS RECORD IS THE DEFAULT INFORMATION FROM THE DEDUCTION SETUP SCREEN.
			// If rsDeductions.NoMatch Then
			// txtAmount = Format(rsDeductions.Fields("Amount"), "0.00")
			// txtAmountType = rsDeductions.Fields("AmountType")
			// txtFrequency = GetFrequencyCodeDescription(rsDeductions.Fields("FrequencyCode"))
			// txtLimit = Format(rsDeductions.Fields("Limit"), "0.00")
			// txtLimitType = rsDeductions.Fields("LimitType")
			// txtPriority = rsDeductions.Fields("Priority")
			// End If
			// Else
			txtAmount.Text = Strings.Format(rsData.Get_Fields("Amount"), "0.00");
			txtAmountType.Text = rsData.Get_Fields_String("AmountType");
			txtFrequency.Text = GetFrequencyCodeDescription(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("FrequencyCode"))));
			txtLimit.Text = Strings.Format(rsData.Get_Fields_Double("Limit"), "0.00");
			txtLimitType.Text = FCConvert.ToString(rsData.Get_Fields_String("LimitType"));
			txtPriority.Text = FCConvert.ToString(rsData.Get_Fields("Priority"));
			// End If
			// SOME OF THESE FIELDS CAN BE THE DEFAULT FROM DEDUCTION SETUP
			if (!rsDeductions.NoMatch)
			{
				if (Conversion.Val(fecherFoundation.Strings.Trim(txtAmount.Text)) == 0)
					txtAmount.Text = Strings.Format(rsDeductions.Get_Fields("Amount"), "0.00");
				if (fecherFoundation.Strings.Trim(txtAmountType.Text) == string.Empty)
					txtAmountType.Text = rsDeductions.Get_Fields_String("AmountType");
				if (fecherFoundation.Strings.Trim(txtFrequency.Text) == string.Empty)
					txtFrequency.Text = GetFrequencyCodeDescription(rsDeductions.Get_Fields("FrequencyCode"));
				if (Conversion.Val(fecherFoundation.Strings.Trim(txtLimit.Text)) == 0)
					txtLimit.Text = Strings.Format(rsDeductions.Get_Fields_Double("Limit"), "0.00");
				if (fecherFoundation.Strings.Trim(txtLimitType.Text) == string.Empty)
					txtLimitType.Text = FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"));
				if (fecherFoundation.Strings.Trim(txtPriority.Text) == string.Empty)
					txtPriority.Text = FCConvert.ToString(rsDeductions.Get_Fields("Priority"));
			}
			dblTemp = modCoreysSweeterCode.GetCurrentDeductionTotal(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("DEDUCTIONCODE"))), FCConvert.ToString(rsData.Get_Fields("employeenumber")), modGlobalVariables.Statics.gdatCurrentPayDate, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("tblemployeedeductionsID"))));
			txtCurrent.Text = Strings.Format(dblTemp, "0.00");
			dblTotCurrent += dblTemp;
			dblTemp = modCoreysSweeterCode.GetMTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("deductioncode"))), FCConvert.ToString(rsData.Get_Fields("employeenumber")), modGlobalVariables.Statics.gdatCurrentPayDate, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("tblemployeedeductionsID"))));
			txtMTD.Text = Strings.Format(dblTemp, "0.00");
			dblTotMTD += dblTemp;
			dblTemp = modCoreysSweeterCode.GetQTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("deductioncode"))), FCConvert.ToString(rsData.Get_Fields("employeenumber")), modGlobalVariables.Statics.gdatCurrentPayDate, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("tblemployeedeductionsID"))));
			txtQTD.Text = Strings.Format(dblTemp, "0.00");
			dblTotQTD += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("deductioncode"))), FCConvert.ToString(rsData.Get_Fields("employeenumber")), modGlobalVariables.Statics.gdatCurrentPayDate, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("tblemployeedeductionsID"))));
			txtCYTD.Text = Strings.Format(dblTemp, "0.00");
			dblTotCYTD += dblTemp;
			dblTemp = modCoreysSweeterCode.GetFYTDDeductionTotal(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("deductioncode"))), FCConvert.ToString(rsData.Get_Fields("employeenumber")), modGlobalVariables.Statics.gdatCurrentPayDate, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("tblemployeedeductionsID"))));
			txtFYTD.Text = Strings.Format(dblTemp, "0.00");
			dblTotFYTD += dblTemp;
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetTaxStatusDescription(int lngDeductionNumber)
		{
			object GetTaxStatusDescription = null;
			rsDeductions.FindFirstRecord("ID", lngDeductionNumber);
			if (rsDeductions.NoMatch)
			{
				GetTaxStatusDescription = string.Empty;
			}
			else
			{
				rsTax.FindFirstRecord("ID", rsDeductions.Get_Fields("TaxStatusCode"));
				if (rsTax.NoMatch)
				{
					GetTaxStatusDescription = string.Empty;
				}
				else
				{
					GetTaxStatusDescription = rsTax.Get_Fields("TaxStatusCode") + "  " + rsTax.Get_Fields("Description");
				}
			}
			return GetTaxStatusDescription;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetDeductionDescription(int lngDeductionNumber)
		{
			object GetDeductionDescription = null;
			rsDeductions.FindFirstRecord("ID", lngDeductionNumber);
			if (rsDeductions.NoMatch)
			{
				GetDeductionDescription = string.Empty;
			}
			else
			{
				GetDeductionDescription = rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + rsDeductions.Get_Fields("Description");
			}
			return GetDeductionDescription;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetFrequencyCodeDescription(int lngFrequencyID)
		{
			object GetFrequencyCodeDescription = null;
			rsFreq.FindFirstRecord("ID", lngFrequencyID);
			if (rsFreq.NoMatch)
			{
				GetFrequencyCodeDescription = string.Empty;
			}
			else
			{
				GetFrequencyCodeDescription = rsFreq.Get_Fields("FrequencyCode") + "  " + rsFreq.Get_Fields("Description");
			}
			return GetFrequencyCodeDescription;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			dblTotCurrent = 0;
			dblTotCYTD = 0;
			dblTotFYTD = 0;
			dblTotMTD = 0;
			dblTotQTD = 0;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsData.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, "TWPY0000.vb1");
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
			rsTax.OpenRecordset("Select * from tblTaxStatusCodes", "TWPY0000.vb1");
			rsFreq.OpenRecordset("Select * from tblFrequencyCodes", "TWPY0000.vb1");
			if (!rsData.EndOfFile())
			{
				this.Fields["fldHeader"].Value = rsData.Get_Fields("EmployeeNumber");
				txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + "   " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolShade)
			{
				Detail.BackColor = ColorTranslator.FromOle(0xF3F3F2);
				this.GroupHeader1.BackColor = Color.White;
			}
			else
			{
				Detail.BackColor = Color.White;
				this.GroupHeader1.BackColor = ColorTranslator.FromOle(0xF3F3F2);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// TH
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotCurrent.Text = Strings.Format(dblTotCurrent, "#,###,##0.00");
			txtTotCYTD.Text = Strings.Format(dblTotCYTD, "#,###,##0.00");
			txtTotFYTD.Text = Strings.Format(dblTotFYTD, "#,###,##0.00");
			txtTotMTD.Text = Strings.Format(dblTotMTD, "#,###,##0.00");
			txtTotQTD.Text = Strings.Format(dblTotQTD, "#,###,##0.00");
		}

		
	}
}
