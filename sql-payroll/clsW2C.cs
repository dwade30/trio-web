//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsW2C
	{
		//=========================================================
		private int lngTaxYear;
		private clsDRWrapper rsW2C = new clsDRWrapper();
		private string strEstablishmentNumber = string.Empty;
		private string strEmployersEIN = "";
		private string strEmployersName = string.Empty;
		private string strEmployeePIN = "";
		private string[] strEmployersAddress = new string[2 + 1];
		private string strEmployerState = "";
		private string strEmployerCity = "";
		private string strEmployerZip = "";
		private bool boolThirdPartySick;
		private bool boolW2C;
		private int lngNumW2C;
		private int intTypeofPayer;
		private string strKindOfEmployer = string.Empty;
		private string strIncorrectEIN = string.Empty;
		private string strIncorrectEstablishment = string.Empty;
		private string strStateID = "";
		private string strIncorrectStateID = string.Empty;
		private int intContactPreference;
		// vbPorter upgrade warning: lngCurrTotalFedWage As int	OnWrite(int, double)
		private int lngCurrTotalFedWage;
		// vbPorter upgrade warning: lngPrevTotalFedWage As int	OnWrite(int, double)
		private int lngPrevTotalFedWage;
		// vbPorter upgrade warning: lngPrevFicaWages As int	OnWrite(int, double)
		private int lngPrevFicaWages;
		// vbPorter upgrade warning: lngCurrFicaWages As int	OnWrite(int, double)
		private int lngCurrFicaWages;
		// vbPorter upgrade warning: lngPrevMedWages As int	OnWrite(int, double)
		private int lngPrevMedWages;
		// vbPorter upgrade warning: lngCurrMedWages As int	OnWrite(int, double)
		private int lngCurrMedWages;
		// vbPorter upgrade warning: lngPrevSSTips As int	OnWrite(int, double)
		private int lngPrevSSTips;
		// vbPorter upgrade warning: lngCurrSSTips As int	OnWrite(int, double)
		private int lngCurrSSTips;
		// vbPorter upgrade warning: lngPrevNonQualified As int	OnWrite(int, double)
		private int lngPrevNonQualified;
		// vbPorter upgrade warning: lngCurrNonQualified As int	OnWrite(int, double)
		private int lngCurrNonQualified;
		// vbPorter upgrade warning: lngPrevFedTax As int	OnWrite(int, double)
		private int lngPrevFedTax;
		// vbPorter upgrade warning: lngCurrFedtax As int	OnWrite(int, double)
		private int lngCurrFedtax;
		// vbPorter upgrade warning: lngPrevFicaTax As int	OnWrite(int, double)
		private int lngPrevFicaTax;
		// vbPorter upgrade warning: lngCurrFicaTax As int	OnWrite(int, double)
		private int lngCurrFicaTax;
		// vbPorter upgrade warning: lngPrevMedTax As int	OnWrite(int, double)
		private int lngPrevMedTax;
		// vbPorter upgrade warning: lngCurrMedTax As int	OnWrite(int, double)
		private int lngCurrMedTax;
		// vbPorter upgrade warning: lngPrevAllocatedTips As int	OnWrite(int, double)
		private int lngPrevAllocatedTips;
		// vbPorter upgrade warning: lngCurrAllocatedTips As int	OnWrite(int, double)
		private int lngCurrAllocatedTips;
		// vbPorter upgrade warning: lngPrevDependentCare As int	OnWrite(int, double)
		private int lngPrevDependentCare;
		// vbPorter upgrade warning: lngCurrDependentCare As int	OnWrite(int, double)
		private int lngCurrDependentCare;
		// vbPorter upgrade warning: lngPrevBox12 As int	OnWrite(int, double)
		private int lngPrevBox12;
		// vbPorter upgrade warning: lngCurrBox12 As int	OnWrite(int, double)
		private int lngCurrBox12;
		// vbPorter upgrade warning: lngPrevBox14 As int	OnWrite(int, double)
		private int lngPrevBox14;
		// vbPorter upgrade warning: lngCurrBox14 As int	OnWrite(int, double)
		private int lngCurrBox14;
		// vbPorter upgrade warning: lngPrevStateWage As int	OnWrite(int, double)
		private int lngPrevStateWage;
		// vbPorter upgrade warning: lngCurrStateWage As int	OnWrite(int, double)
		private int lngCurrStateWage;
		// vbPorter upgrade warning: lngPrevStateTax As int	OnWrite(int, double)
		private int lngPrevStateTax;
		// vbPorter upgrade warning: lngCurrStateTax As int	OnWrite(int, double)
		private int lngCurrStateTax;
		private string strDecreases = "";
		private bool boolEmploymentReturnAdjustment;
		private string strEmpReturnDate = string.Empty;
		private string strContactPerson = string.Empty;
		private string strEmailAddress = string.Empty;
		private string strPhone = string.Empty;
		private string strPhoneExt = "";
		private string strFaxNumber = "";
		private string strContactPreference = "";
		private string strTitle = string.Empty;
		private string strExplanation = string.Empty;
		// vbPorter upgrade warning: lngTot401k As int	OnWrite(int, double)
		private int lngTot401k;
		// vbPorter upgrade warning: lngTotPrev401k As int	OnWrite(int, double)
		private int lngTotPrev401k;
		// vbPorter upgrade warning: lngTot403b As int	OnWrite(int, double)
		private int lngTot403b;
		// vbPorter upgrade warning: lngTotPrev403b As int	OnWrite(int, double)
		private int lngTotPrev403b;
		// vbPorter upgrade warning: lngTot408k As int	OnWrite(int, double)
		private int lngTot408k;
		// vbPorter upgrade warning: lngTotPrev408k As int	OnWrite(int, double)
		private int lngTotPrev408k;
		// vbPorter upgrade warning: lngTot457b As int	OnWrite(int, double)
		private int lngTot457b;
		// vbPorter upgrade warning: lngTotPrev457b As int	OnWrite(int, double)
		private int lngTotPrev457b;
		// vbPorter upgrade warning: lngTot501c As int	OnWrite(int, double)
		private int lngTot501c;
		// vbPorter upgrade warning: lngTotPrev501c As int	OnWrite(int, double)
		private int lngTotPrev501c;
		// vbPorter upgrade warning: lngTotLife As int	OnWrite(int, double)
		private int lngTotLife;
		// vbPorter upgrade warning: lngTotPrevLife As int	OnWrite(int, double)
		private int lngTotPrevLife;
		// vbPorter upgrade warning: lngTotHealthSavings As int	OnWrite(int, double)
		private int lngTotHealthSavings;
		// vbPorter upgrade warning: lngTotPrevHealthSavings As int	OnWrite(int, double)
		private int lngTotPrevHealthSavings;
		// vbPorter upgrade warning: lngTotDeferredCompensation As int	OnWrite(int, double)
		private int lngTotDeferredCompensation;
		// vbPorter upgrade warning: lngTotPrevDeferredCompensation As int	OnWrite(int, double)
		private int lngTotPrevDeferredCompensation;
		// vbPorter upgrade warning: lngTotRoth401k As int	OnWrite(int, double)
		private int lngTotRoth401k;
		// vbPorter upgrade warning: lngTotRoth403b As int	OnWrite(int, double)
		private int lngTotRoth403b;
		// vbPorter upgrade warning: lngTotPrevRoth401k As int	OnWrite(int, double)
		private int lngTotPrevRoth401k;
		// vbPorter upgrade warning: lngTotPrevRoth403b As int	OnWrite(int, double)
		private int lngTotPrevRoth403b;

		public string KindOfEmployer
		{
			set
			{
				strKindOfEmployer = value;
			}
			get
			{
				string KindOfEmployer = "";
				KindOfEmployer = strKindOfEmployer;
				return KindOfEmployer;
			}
		}

		public bool ThirdPartySick
		{
			set
			{
				boolThirdPartySick = value;
			}
			get
			{
				bool ThirdPartySick = false;
				ThirdPartySick = boolThirdPartySick;
				return ThirdPartySick;
			}
		}

		public string ContactTitle
		{
			set
			{
				strTitle = value;
			}
			get
			{
				string ContactTitle = "";
				ContactTitle = strTitle;
				return ContactTitle;
			}
		}

		public bool CorrectingW2C
		{
			set
			{
				boolW2C = value;
			}
			get
			{
				bool CorrectingW2C = false;
				CorrectingW2C = boolW2C;
				return CorrectingW2C;
			}
		}

		public int TaxYear
		{
			set
			{
				lngTaxYear = value;
			}
			get
			{
				int TaxYear = 0;
				TaxYear = lngTaxYear;
				return TaxYear;
			}
		}

		public int TypeOfPayer
		{
			set
			{
				intTypeofPayer = value;
			}
			get
			{
				int TypeOfPayer = 0;
				TypeOfPayer = intTypeofPayer;
				return TypeOfPayer;
			}
		}

		public string EmployerName
		{
			set
			{
				strEmployersName = value;
			}
			get
			{
				string EmployerName = "";
				EmployerName = strEmployersName;
				return EmployerName;
			}
		}

		public string ContactPerson
		{
			set
			{
				strContactPerson = Strings.Replace(value, ",", " ", 1, -1, CompareConstants.vbTextCompare);
			}
			get
			{
				string ContactPerson = "";
				ContactPerson = strContactPerson;
				return ContactPerson;
			}
		}

		public string PhoneNumber
		{
			set
			{
				strPhone = value;
				strPhone = Strings.Replace(strPhone, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strPhone = Strings.Replace(strPhone, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strPhone = Strings.Replace(strPhone, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strPhone = Strings.Right("0000000000" + strPhone, 10);
			}
			get
			{
				string PhoneNumber = "";
				PhoneNumber = strPhone;
				return PhoneNumber;
			}
		}

		public string FormattedPhone()
		{
			string FormattedPhone = "";
			string strTemp;
			strTemp = strPhone;
			if (Strings.Left(strTemp, 3) == "000")
			{
				strTemp = Strings.Mid(strTemp, 4);
			}
			if (strTemp.Length == 10)
			{
				FormattedPhone = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
			}
			else
			{
				if (Strings.Left(strTemp, 3) == "000")
				{
					FormattedPhone = "";
				}
				else
				{
					FormattedPhone = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4);
				}
			}
			return FormattedPhone;
		}

		public string FormattedFax()
		{
			string FormattedFax = "";
			string strTemp;
			strTemp = strFaxNumber;
			if (Strings.Left(strTemp, 3) == "000")
			{
				strTemp = Strings.Mid(strTemp, 4);
			}
			if (strTemp.Length == 10)
			{
				FormattedFax = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
			}
			else
			{
				if (Strings.Left(strTemp, 3) == "000")
				{
					FormattedFax = "";
				}
				else
				{
					FormattedFax = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4);
				}
			}
			return FormattedFax;
		}

		public string FaxNumber
		{
			set
			{
				strFaxNumber = value;
				strFaxNumber = Strings.Replace(strFaxNumber, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strFaxNumber = Strings.Replace(strFaxNumber, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strFaxNumber = Strings.Replace(strFaxNumber, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strFaxNumber = Strings.Right("0000000000" + strFaxNumber, 10);
			}
			get
			{
				string FaxNumber = "";
				FaxNumber = strFaxNumber;
				return FaxNumber;
			}
		}

		public string Email
		{
			set
			{
				strEmailAddress = value;
			}
			get
			{
				string Email = "";
				Email = strEmailAddress;
				return Email;
			}
		}

		public bool EmploymentReturnAdjustment
		{
			set
			{
				boolEmploymentReturnAdjustment = value;
			}
			get
			{
				bool EmploymentReturnAdjustment = false;
				EmploymentReturnAdjustment = boolEmploymentReturnAdjustment;
				return EmploymentReturnAdjustment;
			}
		}

		public string EmpReturnDate
		{
			set
			{
				strEmpReturnDate = value;
			}
			get
			{
				string EmpReturnDate = "";
				EmpReturnDate = strEmpReturnDate;
				return EmpReturnDate;
			}
		}

		public string Explanation
		{
			set
			{
				strExplanation = value;
			}
			get
			{
				string Explanation = "";
				Explanation = strExplanation;
				return Explanation;
			}
		}

		public string IncorrectEIN
		{
			set
			{
				strIncorrectEIN = value;
			}
			get
			{
				string IncorrectEIN = "";
				IncorrectEIN = strIncorrectEIN;
				return IncorrectEIN;
			}
		}

		public string IncorrectEstablishment
		{
			set
			{
				strIncorrectEstablishment = value;
			}
			get
			{
				string IncorrectEstablishment = "";
				IncorrectEstablishment = strIncorrectEstablishment;
				return IncorrectEstablishment;
			}
		}

		public string Establishment
		{
			set
			{
				strEstablishmentNumber = value;
			}
			get
			{
				string Establishment = "";
				Establishment = strEstablishmentNumber;
				return Establishment;
			}
		}

		public string IncorrectStateID
		{
			set
			{
				strIncorrectStateID = value;
			}
			get
			{
				string IncorrectStateID = "";
				IncorrectStateID = strIncorrectStateID;
				return IncorrectStateID;
			}
		}

		public void MoveNext()
		{
			rsW2C.MoveNext();
		}

		public void MoveFirst()
		{
			rsW2C.MoveFirst();
		}

		public void MoveLast()
		{
			rsW2C.MoveLast();
		}

		public bool EndOfFile()
		{
			bool EndOfFile = false;
			EndOfFile = rsW2C.EndOfFile();
			return EndOfFile;
		}

		public bool GetW3Info()
		{
			bool GetW3Info = false;
			GetW3Info = frmW3C.InstancePtr.Init(this);
			return GetW3Info;
		}

		public bool LoadW2Info()
		{
			bool LoadW2Info = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadW2Info = false;
				rsW2C.OpenRecordset("select * from w2c where taxyear = " + FCConvert.ToString(lngTaxYear) + " and [current] = 1 order by orderno", "twpy0000.vb1");
				if (!rsW2C.EndOfFile())
				{
					LoadW2Info = true;
				}
				return LoadW2Info;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadW2Info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadW2Info;
		}

		private void GetEmployerInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				int X;
				strSQL = "select * from tblw2master";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					strEmployersName = FCConvert.ToString(rsLoad.Get_Fields("employersname"));
					strEmployersAddress[1] = FCConvert.ToString(rsLoad.Get_Fields("address1"));
					strEmployersAddress[2] = FCConvert.ToString(rsLoad.Get_Fields("address2"));
					strEmployerCity = FCConvert.ToString(rsLoad.Get_Fields("city"));
					strEmployerState = FCConvert.ToString(rsLoad.Get_Fields("state"));
					strEmployerZip = FCConvert.ToString(rsLoad.Get_Fields_String("zip"));
					strEmployersEIN = FCConvert.ToString(rsLoad.Get_Fields("employersfederalid"));
					strEmployersEIN = Strings.Replace(strEmployersEIN, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strStateID = FCConvert.ToString(rsLoad.Get_Fields("employersstateid"));
					strEmailAddress = FCConvert.ToString(rsLoad.Get_Fields("contactEmilAddress"));
					strContactPerson = FCConvert.ToString(rsLoad.Get_Fields("contactperson"));
					intContactPreference = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("contactpreference1"))));
					this.PhoneNumber = rsLoad.Get_Fields("contacttelephonenumber");
					this.FaxNumber = rsLoad.Get_Fields("contactfaxnumber");
					strEmployeePIN = FCConvert.ToString(rsLoad.Get_Fields("employeepin"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetEmployerInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsW2C() : base()
		{
			strPhone = "0000000000";
			strFaxNumber = "0000000000";
			ClearValues();
		}

		private void ClearValues()
		{
			lngCurrAllocatedTips = 0;
			lngCurrBox12 = 0;
			lngCurrBox14 = 0;
			lngCurrDependentCare = 0;
			lngCurrFedtax = 0;
			lngCurrFicaTax = 0;
			lngCurrFicaWages = 0;
			lngCurrMedTax = 0;
			lngCurrMedWages = 0;
			lngCurrNonQualified = 0;
			lngCurrSSTips = 0;
			lngCurrStateTax = 0;
			lngCurrStateWage = 0;
			lngCurrTotalFedWage = 0;
			lngPrevAllocatedTips = 0;
			lngPrevBox12 = 0;
			lngPrevBox14 = 0;
			lngPrevDependentCare = 0;
			lngPrevFedTax = 0;
			lngPrevFicaTax = 0;
			lngPrevFicaWages = 0;
			lngPrevMedTax = 0;
			lngPrevMedWages = 0;
			lngPrevNonQualified = 0;
			lngPrevSSTips = 0;
			lngPrevStateTax = 0;
			lngPrevStateWage = 0;
			lngPrevTotalFedWage = 0;
			lngNumW2C = 0;
		}

		public bool CreateElectronicW2C()
		{
			bool CreateElectronicW2C = false;
			FCFileSystem fso = new FCFileSystem();
			StreamWriter ts = null;
			string strFile;
			bool boolFileOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strRecord;
				int lngNumRecords;
				boolFileOpen = false;
				CreateElectronicW2C = false;
				ClearValues();
				GetEmployerInfo();
				if (!LoadW2Info())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateElectronicW2C;
				}
				if (!frmW3C.InstancePtr.Init(this))
				{
					return CreateElectronicW2C;
				}
				strFile = "W2CReport";
                strFile = FCUtils.GetTempFilename("");
				if (File.Exists(strFile))
                {
					File.Delete(strFile);
                }
				
				ts = File.CreateText(strFile);
				strRecord = CreateRCARecord();
				if (strRecord != string.Empty)
				{
					ts.WriteLine(strRecord);
				}
				else
				{
					ts.Close();
					MessageBox.Show("Blank RCA record" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateElectronicW2C;
				}
				strRecord = CreateRCERecord();
				if (strRecord != string.Empty)
				{
					ts.WriteLine(strRecord);
				}
				else
				{
					ts.Close();
					MessageBox.Show("Blank RCE record" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateElectronicW2C;
				}
				lngNumRecords = 0;
				while (!rsW2C.EndOfFile())
				{
					strRecord = CreateRCWRecord();
					if (strRecord != string.Empty)
					{
						ts.WriteLine(strRecord);
						lngNumRecords += 1;
					}
					else
					{
						ts.Close();
						MessageBox.Show("Blank RCW record" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					rsW2C.MoveNext();
				}
				strRecord = CreateRCTRecord(ref lngNumRecords);
				if (strRecord != string.Empty)
				{
					ts.WriteLine(strRecord);
				}
				else
				{
					ts.Close();
					MessageBox.Show("Blank RCT record" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateElectronicW2C;
				}
				strRecord = CreateRCFRecord(ref lngNumRecords);
				if (strRecord != string.Empty)
				{
					ts.WriteLine(strRecord);
				}
				else
				{
					ts.Close();
					MessageBox.Show("Blank RCF record" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateElectronicW2C;
				}
				ts.Close();
				CreateElectronicW2C = true;
                FCUtils.Download(strFile, "W2CReport");
				//MessageBox.Show(Environment.CurrentDirectory + "\\" + strFile + " created", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return CreateElectronicW2C;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateElectronicW2C", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateElectronicW2C;
		}

		private string CreateRCARecord()
		{
			string CreateRCARecord = "";
			string strLine;
			string strTemp;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// submitter record
				strLine = "";
				CreateRCARecord = "";
				if (intContactPreference == 1)
				{
					if (fecherFoundation.Strings.Trim(strEmailAddress) == string.Empty)
					{
						MessageBox.Show("You have chosen E-mail as your chosen contact preference but there is no valid E-mail address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CreateRCARecord;
					}
				}
				else if (intContactPreference == 2)
				{
					if (fecherFoundation.Strings.Trim(strEmployersAddress[1]) == string.Empty || fecherFoundation.Strings.Trim(strEmployerCity) == string.Empty || fecherFoundation.Strings.Trim(strEmployerState) == string.Empty || fecherFoundation.Strings.Trim(strEmployerZip) == string.Empty)
					{
						MessageBox.Show("You have chosen postal service as your contact preference but there is no valie address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CreateRCARecord;
					}
				}
				else
				{
					MessageBox.Show("Invalid contact preference", "Invalid Preference", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateRCARecord;
				}
				strLine = "RCA";
				strTemp = Strings.Replace(strEmployersEIN, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Right("000000000" + strTemp, 9);
				strLine += strTemp;
				strLine += Strings.Right("00000000" + strEmployeePIN, 8);
				strLine += Strings.StrDup(9, " ");
				strLine += "99";
				// indicates not an in-house program
				strLine += Strings.Left(strEmployersName + Strings.StrDup(57, " "), 57);
				// submitter name
				strLine += Strings.Left(strEmployersAddress[1] + Strings.StrDup(22, " "), 22);
				// location address
				strLine += Strings.Left(strEmployersAddress[2] + Strings.StrDup(22, " "), 22);
				// delivery address
				strLine += Strings.Left(strEmployerCity + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(strEmployerState + "  ", 2);
				strAry = Strings.Split(strEmployerZip, "-", -1, CompareConstants.vbTextCompare);
				strLine += Strings.Left(strAry[0] + Strings.StrDup(5, " "), 5);
				if (Information.UBound(strAry, 1) > 0)
				{
					strLine += Strings.Left(strAry[1] + Strings.StrDup(4, " "), 4);
				}
				else
				{
					strLine += Strings.StrDup(4, " ");
				}
				strLine += Strings.StrDup(6, " ");
				strLine += Strings.StrDup(23, " ");
				// foreign state/province
				strLine += Strings.StrDup(15, " ");
				// foreign postal code
				strLine += Strings.StrDup(2, " ");
				// blank because we're US
				strLine += Strings.Left(strContactPerson + Strings.StrDup(27, " "), 27);
				// contact person
				strLine += Strings.Left(strPhone + Strings.StrDup(15, " "), 15);
				// contact phone
				strLine += Strings.Left(strPhoneExt + Strings.StrDup(5, " "), 5);
				// contact extension
				strLine += Strings.StrDup(3, " ");
				strLine += Strings.Left(strEmailAddress + Strings.StrDup(40, " "), 40);
				// contact email
				strLine += Strings.StrDup(3, " ");
				strLine += Strings.Left(strFaxNumber + Strings.StrDup(10, " "), 10);
				// fax number
				strLine += FCConvert.ToString(intContactPreference);
				// contact preference
				strLine += "L";
				// self prepared
				strLine += "0";
				// not resubmitted
				strLine += Strings.StrDup(6, " ");
				// WFID if being resubmitted
				strLine += Strings.StrDup(701, " ");
				// blank
				CreateRCARecord = strLine;
				return CreateRCARecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRCARecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRCARecord;
		}

		private string CreateRCWRecord()
		{
			string CreateRCWRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				string[] strAry = null;
				int lng401k;
				// 401k
				int lng403b;
				// 403b
				int lng408k;
				// 408k
				int lng457b;
				// 457b
				int lng501c;
				// 501c
				int lngRoth401k;
				int lngRoth403b;
				int lngPrevRoth401k;
				int lngPrevRoth403b;
				int lngLife;
				int lngPrev401k;
				int lngPrev403b;
				int lngPrev408k;
				int lngPrev457b;
				int lngPrev501c;
				int lngPrevLife;
				clsDRWrapper rsTemp = new clsDRWrapper();
				// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
				int lngTemp = 0;
				// vbPorter upgrade warning: lngPrevTemp As int	OnWriteFCConvert.ToDouble(
				int lngPrevTemp = 0;
				int lngHealthSavings;
				int lngPrevHealthSavings;
				int lngPrevReportedCostHealthCoverage;
				int lngReportedCostHealthCoverage;
				lngHealthSavings = 0;
				lngPrevHealthSavings = 0;
				lngRoth401k = 0;
				lngPrevRoth401k = 0;
				lngRoth403b = 0;
				lngPrevRoth403b = 0;
				lng401k = 0;
				lng403b = 0;
				lng408k = 0;
				lng457b = 0;
				lng501c = 0;
				lngLife = 0;
				lngPrev401k = 0;
				lngPrev403b = 0;
				lngPrev408k = 0;
				lngPrev457b = 0;
				lngPrev501c = 0;
				lngPrevLife = 0;
				lngPrevReportedCostHealthCoverage = 0;
				lngReportedCostHealthCoverage = 0;
				strLine = "";
				CreateRCWRecord = "";
				strLine = "RCW";
				strLine += Strings.Left(Strings.Replace(FCConvert.ToString(rsW2C.Get_Fields("prevssn")), "-", "", 1, -1, CompareConstants.vbTextCompare) + Strings.StrDup(9, " "), 9);
				strLine += Strings.Left(Strings.Replace(FCConvert.ToString(rsW2C.Get_Fields("ssn")), "-", "", 1, -1, CompareConstants.vbTextCompare) + Strings.StrDup(9, " "), 9);
				strLine += Strings.Left(rsW2C.Get_Fields("prevfirstname") + Strings.StrDup(15, " "), 15);
				strLine += Strings.Left(rsW2C.Get_Fields("prevmiddlename") + Strings.StrDup(15, " "), 15);
				strLine += Strings.Left(rsW2C.Get_Fields("prevlastname") + Strings.StrDup(20, " "), 20);
				strLine += Strings.Left(rsW2C.Get_Fields("firstname") + Strings.StrDup(15, " "), 15);
				strLine += Strings.Left(rsW2C.Get_Fields("middlename") + Strings.StrDup(15, " "), 15);
				strLine += Strings.Left(rsW2C.Get_Fields("lastname") + Strings.StrDup(20, " "), 20);
				strLine += Strings.Left(rsW2C.Get_Fields("address1") + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(rsW2C.Get_Fields("address2") + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(rsW2C.Get_Fields("city") + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(rsW2C.Get_Fields("state") + Strings.StrDup(2, " "), 2);
				strAry = Strings.Split(FCConvert.ToString(rsW2C.Get_Fields_String("zip")), " ", -1, CompareConstants.vbBinaryCompare);
				strLine += Strings.Left(strAry[0] + Strings.StrDup(5, " "), 5);
				if (Information.UBound(strAry, 1) > 0)
				{
					strLine += Strings.Left(strAry[1] + Strings.StrDup(4, " "), 4);
				}
				else
				{
					strLine += Strings.StrDup(4, " ");
				}
				strLine += Strings.StrDup(5, " ");
				strLine += Strings.StrDup(23, " ");
				// foreign state/province
				strLine += Strings.StrDup(15, " ");
				// foreign postal code
				strLine += Strings.StrDup(2, " ");
				// country code
				if (Conversion.Val(rsW2C.Get_Fields("federalwage")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevfederalwage")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevfederalwage")) * 100);
					lngPrevTotalFedWage += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("federalwage")) * 100);
					lngCurrTotalFedWage += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("federaltax")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevfederaltax")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevfederaltax")) * 100);
					lngPrevFedTax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("federaltax")) * 100);
					lngCurrFedtax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("ficawage")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevficawage")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevficawage")) * 100);
					lngPrevFicaWages += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("ficawage")) * 100);
					lngCurrFicaWages += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("ficatax")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevficatax")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevficatax")) * 100);
					lngPrevFicaTax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("ficatax")) * 100);
					lngCurrFicaTax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("medicarewage")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevmedicarewage")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevmedicarewage")) * 100);
					lngPrevMedWages += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("medicarewage")) * 100);
					lngCurrMedWages += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("medicaretax")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevmedicaretax")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevmedicaretax")) * 100);
					lngPrevMedTax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("medicaretax")) * 100);
					lngCurrMedTax += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (Conversion.Val(rsW2C.Get_Fields("sstips")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevsstips")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevsstips")) * 100);
					lngPrevSSTips += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("sstips")) * 100);
					lngCurrSSTips += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				
				//EIC is removed
				strLine += Strings.StrDup(11, " ");
				strLine += Strings.StrDup(11, " ");

				if (Conversion.Val(rsW2C.Get_Fields("dependentcare")) > 0 || Conversion.Val(rsW2C.Get_Fields("prevdependentcare")) > 0)
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("prevdependentcare")) * 100);
					lngPrevDependentCare += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsW2C.Get_Fields("dependentcare")) * 100);
					lngCurrDependentCare += lngTemp;
					strLine += Strings.Format(lngTemp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				rsTemp.OpenRecordset("select * from w2cbox1214 where recordid = " + rsW2C.Get_Fields("ID") + " and box12 = 1", "twpy0000.vb1");
				while (!rsTemp.EndOfFile())
				{
					lngTemp = FCConvert.ToInt32(Conversion.Val(rsTemp.Get_Fields("Amount")) * 100);
					lngPrevTemp = FCConvert.ToInt32(Conversion.Val(rsTemp.Get_Fields("prevamount")) * 100);
					if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "C")
					{
						// life term
						lngLife += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "D")
					{
						// 401k
						lng401k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "E")
					{
						// 403b
						lng403b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "F")
					{
						// 408
						lng408k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "G")
					{
						// 457
						lng457b += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "H")
					{
						// 501
						lng501c += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "W")
					{
						// health savings
						lngHealthSavings += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "AA")
					{
						lngRoth401k += lngTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "BB")
					{
						lngRoth403b += lngTemp;
					}
					if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "C")
					{
						// life term
						lngPrevLife += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "D")
					{
						// 401k
						lngPrev401k += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "E")
					{
						// 403b
						lngPrev403b += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "F")
					{
						// 408
						lngPrev408k += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "G")
					{
						// 457
						lngPrev457b += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "H")
					{
						// 501
						lngPrev501c += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "W")
					{
						// health savings
						lngPrevHealthSavings += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "AA")
					{
						lngPrevRoth401k += lngPrevTemp;
					}
					else if (fecherFoundation.Strings.Trim(Strings.Mid(fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("code"))) + "  ", 1, 2)) == "BB")
					{
						lngPrevRoth403b += lngPrevTemp;
					}
					rsTemp.MoveNext();
				}
				lngTot401k += lng401k;
				lngTot403b += lng403b;
				lngTot408k += lng408k;
				lngTot457b += lng457b;
				lngTot501c += lng501c;
				lngTotLife += lngLife;
				lngTotRoth401k += lngRoth401k;
				lngTotPrevRoth401k += lngPrevRoth401k;
				lngTotRoth403b += lngRoth403b;
				lngTotPrevRoth403b += lngPrevRoth403b;
				lngTotHealthSavings += lngHealthSavings;
				lngTotPrevHealthSavings += lngPrevHealthSavings;
				lngTotPrev401k += lngPrev401k;
				lngTotPrev403b += lngPrev403b;
				lngTotPrev408k += lngPrev408k;
				lngTotPrev457b += lngPrev457b;
				lngTotPrev501c += lngPrev501c;
				lngTotPrevLife += lngPrevLife;
				if (lngPrev401k > 0 || lng401k > 0)
				{
					strLine += Strings.Format(lngPrev401k, "00000000000");
					strLine += Strings.Format(lng401k, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrev403b > 0 || lng403b > 0)
				{
					strLine += Strings.Format(lngPrev403b, "00000000000");
					strLine += Strings.Format(lng403b, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrev408k > 0 || lng408k > 0)
				{
					strLine += Strings.Format(lngPrev408k, "00000000000");
					strLine += Strings.Format(lng408k, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrev457b > 0 || lng457b > 0)
				{
					strLine += Strings.Format(lngPrev457b, "00000000000");
					strLine += Strings.Format(lng457b, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrev501c > 0 || lng501c > 0)
				{
					strLine += Strings.Format(lngPrev501c, "00000000000");
					strLine += Strings.Format(lng501c, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				int lngDeferredComp;
				int lngPrevDeferredComp;
				lngDeferredComp = lng401k + lng403b + lng408k + lng457b + lng501c;
				lngPrevDeferredComp = lngPrev401k + lngPrev403b + lngPrev408k + lngPrev457b + lngPrev501c;
				if (lngDeferredComp > 0 || lngPrevDeferredComp > 0)
				{
					strLine += Strings.Format(lngPrevDeferredComp, "00000000000");
					strLine += Strings.Format(lngDeferredComp, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				strLine += Strings.StrDup(11, " ");
				// prev military combat pay
				strLine += Strings.StrDup(11, " ");
				// correct military combat pay
				strLine += Strings.StrDup(11, " ");
				// non qualified 457
				strLine += Strings.StrDup(11, " ");
				// non qualified 457
				if (lngPrevHealthSavings > 0 || lngHealthSavings > 0)
				{
					strLine += Strings.Format(lngPrevHealthSavings, "00000000000");
					strLine += Strings.Format(lngHealthSavings, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				strLine += Strings.StrDup(11, " ");
				// not qualified non 457
				strLine += Strings.StrDup(11, " ");
				// not qualified non 457
				strLine += Strings.StrDup(11, " ");
				// nontaxable combat pay
				strLine += Strings.StrDup(11, " ");
				// nontaxable combat pay
				strLine += Strings.StrDup(22, " ");
				if (lngLife > 0 || lngPrevLife > 0)
				{
					strLine += Strings.Format(lngPrevLife, "00000000000");
					strLine += Strings.Format(lngLife, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				strLine += Strings.StrDup(11, " ");
				// nonstatutory stock options
				strLine += Strings.StrDup(11, " ");
				// nonstatutory stock options
				strLine += Strings.StrDup(11, " ");
				// non qualified 409a
				strLine += Strings.StrDup(11, " ");
				// non qualified 409a
				if (lngPrevRoth401k > 0 || lngRoth401k > 0)
				{
					strLine += Strings.Format(lngPrevRoth401k, "00000000000");
					strLine += Strings.Format(lngRoth401k, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrevRoth403b > 0 || lngRoth403b > 0)
				{
					strLine += Strings.Format(lngPrevRoth403b, "00000000000");
					strLine += Strings.Format(lngRoth403b, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				if (lngPrevReportedCostHealthCoverage > 0 || lngReportedCostHealthCoverage > 0)
				{
					strLine += Strings.Format(lngPrevReportedCostHealthCoverage, "00000000000");
					strLine += Strings.Format(lngReportedCostHealthCoverage, "00000000000");
				}
				else
				{
					strLine += Strings.StrDup(11, " ");
					strLine += Strings.StrDup(11, " ");
				}
				strLine += Strings.StrDup(165, " ");
				if (rsW2C.Get_Fields_Boolean("w2statutoryemployee") != rsW2C.Get_Fields_Boolean("prevw2statutoryemployee"))
				{
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("prevw2statutoryemployee")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("w2statutoryemployee")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
				}
				else
				{
					strLine += Strings.StrDup(2, " ");
				}
				if (rsW2C.Get_Fields_Boolean("w2retirement") != rsW2C.Get_Fields_Boolean("prevw2retirement"))
				{
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("prevw2retirement")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("w2retirement")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
				}
				else
				{
					strLine += Strings.StrDup(2, " ");
				}
				if (rsW2C.Get_Fields_Boolean("prevw2deferred") != rsW2C.Get_Fields_Boolean("w2deferred"))
				{
					// third party sick pay
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("prevw2deferred")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
					if (FCConvert.ToBoolean(rsW2C.Get_Fields_Boolean("w2deferred")))
					{
						strLine += "1";
					}
					else
					{
						strLine += "0";
					}
				}
				else
				{
					strLine += Strings.StrDup(2, " ");
				}
				strLine += Strings.StrDup(16, " ");
				CreateRCWRecord = strLine;
				return CreateRCWRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRCWRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRCWRecord;
		}

		private string CreateRCERecord()
		{
			string CreateRCERecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				string[] strAry = null;
				strLine = "";
				CreateRCERecord = "";
				strLine = "RCE";
				strLine += FCConvert.ToString(lngTaxYear);
				strLine += Strings.Left(strIncorrectEIN + Strings.StrDup(9, " "), 9);
				strLine += Strings.Left(strEmployersEIN + Strings.StrDup(9, " "), 9);
				strLine += " ";
				// agent indicator code
				strLine += Strings.StrDup(9, " ");
				// agent for ein
				strLine += Strings.Left(strIncorrectEstablishment + Strings.StrDup(4, " "), 4);
				strLine += Strings.Left(strEstablishmentNumber + Strings.StrDup(4, " "), 4);
				strLine += Strings.Left(strEmployersName + Strings.StrDup(57, " "), 57);
				strLine += Strings.Left(strEmployersAddress[1] + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(strEmployersAddress[2] + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(strEmployerCity + Strings.StrDup(22, " "), 22);
				strLine += Strings.Left(strEmployerState + "  ", 2);
				strAry = Strings.Split(strEmployerZip, "-", -1, CompareConstants.vbTextCompare);
				strLine += Strings.Left(strAry[0] + Strings.StrDup(5, " "), 5);
				if (Information.UBound(strAry, 1) > 0)
				{
					strLine += Strings.Left(strAry[1] + Strings.StrDup(4, " "), 4);
				}
				else
				{
					strLine += Strings.StrDup(4, " ");
				}
				strLine += Strings.StrDup(4, " ");
				strLine += Strings.StrDup(23, " ");
				// foreign state/province
				strLine += Strings.StrDup(15, " ");
				// foreign postal code
				strLine += Strings.StrDup(2, " ");
				// country code
				strLine += Strings.StrDup(1, " ");
				// incorrect employment code
				switch (intTypeofPayer)
				{
					case 0:
						{
							// 941
							strLine += "R";
							break;
						}
					case 1:
						{
							// CT-1
							strLine += "X";
							break;
						}
					case 2:
						{
							// military
							strLine += "M";
							break;
						}
					case 3:
						{
							// household
							strLine += "H";
							break;
						}
					case 4:
						{
							// 943
							strLine += "A";
							break;
						}
					case 5:
						{
							// medicare qualified government employment
							strLine += "Q";
							break;
						}
					case 6:
						{
							// third party sickpay
							strLine += "R";
							// not sure
							break;
						}
					case 7:
						{
							// 944
							strLine += "F";
							break;
						}
				}
				//end switch
				strLine += " ";
				// original third party sick indicator
				strLine += " ";
				// correct third party sick indicator
				strLine += " ";
				// original kind of employer
				strLine += strKindOfEmployer;
				// correct kind of employer
				strLine += Strings.StrDup(797, " ");
				CreateRCERecord = strLine;
				return CreateRCERecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRCERecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRCERecord;
		}

		private string CreateRCTRecord(ref int lngNumRecords)
		{
			string CreateRCTRecord = "";
			string strLine;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLine = "";
				CreateRCTRecord = "";
				strLine = "RCT";
				strLine += Strings.Format(lngNumRecords, "0000000");
				if (lngPrevTotalFedWage > 0 || lngCurrTotalFedWage > 0)
				{
					strLine += Strings.Format(lngPrevTotalFedWage, "000000000000000");
					strLine += Strings.Format(lngCurrTotalFedWage, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevFedTax > 0 || lngCurrFedtax > 0)
				{
					strLine += Strings.Format(lngPrevFedTax, "000000000000000");
					strLine += Strings.Format(lngCurrFedtax, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevFicaWages > 0 || lngCurrFicaWages > 0)
				{
					strLine += Strings.Format(lngPrevFicaWages, "000000000000000");
					strLine += Strings.Format(lngCurrFicaWages, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevFicaTax > 0 || lngCurrFicaTax > 0)
				{
					strLine += Strings.Format(lngPrevFicaTax, "000000000000000");
					strLine += Strings.Format(lngCurrFicaTax, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevMedWages > 0 || lngCurrMedWages > 0)
				{
					strLine += Strings.Format(lngPrevMedWages, "000000000000000");
					strLine += Strings.Format(lngCurrMedWages, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevMedTax > 0 || lngCurrMedTax > 0)
				{
					strLine += Strings.Format(lngPrevMedTax, "000000000000000");
					strLine += Strings.Format(lngCurrMedTax, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngPrevSSTips > 0 || lngCurrSSTips > 0)
				{
					strLine += Strings.Format(lngPrevSSTips, "000000000000000");
					strLine += Strings.Format(lngCurrSSTips, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				
				//EIC is removed
				strLine += Strings.StrDup(15, " ");
				strLine += Strings.StrDup(15, " ");

				if (lngPrevDependentCare > 0 || lngCurrDependentCare > 0)
				{
					strLine += Strings.Format(lngPrevDependentCare, "000000000000000");
					strLine += Strings.Format(lngCurrDependentCare, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTot401k > 0 || lngTotPrev401k > 0)
				{
					strLine += Strings.Format(lngTotPrev401k, "000000000000000");
					strLine += Strings.Format(lngTot401k, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTotPrev403b > 0 || lngTot403b > 0)
				{
					strLine += Strings.Format(lngTotPrev403b, "000000000000000");
					strLine += Strings.Format(lngTot403b, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTotPrev408k > 0 || lngTot408k > 0)
				{
					strLine += Strings.Format(lngTotPrev408k, "000000000000000");
					strLine += Strings.Format(lngTot408k, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTotPrev457b > 0 || lngTot457b > 0)
				{
					strLine += Strings.Format(lngTotPrev457b, "000000000000000");
					strLine += Strings.Format(lngTot457b, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTotPrev501c > 0 || lngTot501c > 0)
				{
					strLine += Strings.Format(lngTotPrev501c, "000000000000000");
					strLine += Strings.Format(lngTot501c, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				lngTotDeferredCompensation = lngTot501c + lngTot457b + lngTot408k + lngTot403b + lngTot401k;
				lngTotPrevDeferredCompensation = lngTotPrev501c + lngTotPrev457b + lngTotPrev408k + lngTotPrev403b + lngTotPrev401k;
				if (lngTotPrevDeferredCompensation > 0 || lngTotDeferredCompensation > 0)
				{
					strLine += Strings.Format(lngTotPrevDeferredCompensation, "000000000000000");
					strLine += Strings.Format(lngTotDeferredCompensation, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				strLine += Strings.StrDup(15, " ");
				// military
				strLine += Strings.StrDup(15, " ");
				strLine += Strings.StrDup(15, " ");
				// nonqualified 457
				strLine += Strings.StrDup(15, " ");
				if (lngTotHealthSavings > 0 || lngTotPrevHealthSavings > 0)
				{
					strLine += Strings.Format(lngTotPrevHealthSavings, "000000000000000");
					strLine += Strings.Format(lngTotHealthSavings, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				strLine += Strings.StrDup(15, " ");
				// nonqualified not 457
				strLine += Strings.StrDup(15, " ");
				strLine += Strings.StrDup(15, " ");
				// nontaxable combat pay
				strLine += Strings.StrDup(15, " ");
				strLine += Strings.StrDup(30, " ");
				// blank
				if (lngTotLife > 0 || lngTotPrevLife > 0)
				{
					strLine += Strings.Format(lngTotPrevLife, "000000000000000");
					strLine += Strings.Format(lngTotLife, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				strLine += Strings.StrDup(15, " ");
				// stock options
				strLine += Strings.StrDup(15, " ");
				strLine += Strings.StrDup(15, " ");
				// nonqualified 409a
				strLine += Strings.StrDup(15, " ");
				if (lngTotPrevRoth401k > 0 || lngTotRoth401k > 0)
				{
					strLine += Strings.Format(lngTotPrevRoth401k, "000000000000000");
					strLine += Strings.Format(lngTotRoth401k, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				if (lngTotPrevRoth403b > 0 || lngTotRoth403b > 0)
				{
					strLine += Strings.Format(lngTotPrevRoth403b, "000000000000000");
					strLine += Strings.Format(lngTotRoth403b, "000000000000000");
				}
				else
				{
					strLine += Strings.StrDup(15, " ");
					strLine += Strings.StrDup(15, " ");
				}
				strLine += Strings.StrDup(234, " ");
				// blank
				CreateRCTRecord = strLine;
				return CreateRCTRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRCTRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRCTRecord;
		}

		private string CreateRCFRecord(ref int lngNumRecords)
		{
			string CreateRCFRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLine = "";
				CreateRCFRecord = "";
				strLine = "RCF";
				strLine += Strings.Format(lngNumRecords, "000000000");
				strLine += Strings.StrDup(1012, " ");
				CreateRCFRecord = strLine;
				return CreateRCFRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateRCFRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateRCFRecord;
		}

		public double TotalFederalWage
		{
			set
			{
				lngCurrTotalFedWage = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotalFederalWage = 0;
				TotalFederalWage = lngCurrTotalFedWage / 100.0;
				return TotalFederalWage;
			}
		}

		public double PrevTotalFedWage
		{
			set
			{
				lngPrevTotalFedWage = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevTotalFedWage = 0;
				PrevTotalFedWage = lngPrevTotalFedWage / 100.0;
				return PrevTotalFedWage;
			}
		}

		public double PrevFicaWages
		{
			set
			{
				lngPrevFicaWages = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevFicaWages = 0;
				PrevFicaWages = lngPrevFicaWages / 100.0;
				return PrevFicaWages;
			}
		}

		public double CurrFicaWages
		{
			set
			{
				lngCurrFicaWages = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrFicaWages = 0;
				CurrFicaWages = lngCurrFicaWages / 100.0;
				return CurrFicaWages;
			}
		}

		public double PrevMedWages
		{
			set
			{
				lngPrevMedWages = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevMedWages = 0;
				PrevMedWages = lngPrevMedWages / 100.0;
				return PrevMedWages;
			}
		}

		public double CurrMedWages
		{
			set
			{
				lngCurrMedWages = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrMedWages = 0;
				CurrMedWages = lngCurrMedWages / 100.0;
				return CurrMedWages;
			}
		}

		public double PrevSSTips
		{
			set
			{
				lngPrevSSTips = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevSSTips = 0;
				PrevSSTips = lngPrevSSTips / 100.0;
				return PrevSSTips;
			}
		}

		public double CurrSSTips
		{
			set
			{
				lngCurrSSTips = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrSSTips = 0;
				CurrSSTips = lngCurrSSTips / 100.0;
				return CurrSSTips;
			}
		}

		public double PrevNonQualified
		{
			set
			{
				lngPrevNonQualified = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevNonQualified = 0;
				PrevNonQualified = lngPrevNonQualified / 100.0;
				return PrevNonQualified;
			}
		}

		public double CurrNonQualified
		{
			set
			{
				lngCurrNonQualified = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrNonQualified = 0;
				CurrNonQualified = lngCurrNonQualified / 100.0;
				return CurrNonQualified;
			}
		}

		public double PrevFedTax
		{
			set
			{
				lngPrevFedTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevFedTax = 0;
				PrevFedTax = lngPrevFedTax / 100.0;
				return PrevFedTax;
			}
		}

		public double CurrFedTax
		{
			set
			{
				lngCurrFedtax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrFedTax = 0;
				CurrFedTax = lngCurrFedtax / 100.0;
				return CurrFedTax;
			}
		}

		public double PrevFicaTax
		{
			set
			{
				lngPrevFicaTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevFicaTax = 0;
				PrevFicaTax = lngPrevFicaTax / 100.0;
				return PrevFicaTax;
			}
		}

		public double CurrFicaTax
		{
			set
			{
				lngCurrFicaTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrFicaTax = 0;
				CurrFicaTax = lngCurrFicaTax / 100.0;
				return CurrFicaTax;
			}
		}

		public double PrevMedTax
		{
			set
			{
				lngPrevMedTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevMedTax = 0;
				PrevMedTax = lngPrevMedTax / 100.0;
				return PrevMedTax;
			}
		}

		public double CurrMedTax
		{
			set
			{
				lngCurrMedTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrMedTax = 0;
				CurrMedTax = lngCurrMedTax / 100.0;
				return CurrMedTax;
			}
		}

		public double PrevAllocatedTips
		{
			set
			{
				lngPrevAllocatedTips = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevAllocatedTips = 0;
				PrevAllocatedTips = lngPrevAllocatedTips / 100.0;
				return PrevAllocatedTips;
			}
		}

		public double CurrAllocatedTips
		{
			set
			{
				lngCurrAllocatedTips = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrAllocatedTips = 0;
				CurrAllocatedTips = lngCurrAllocatedTips / 100.0;
				return CurrAllocatedTips;
			}
		}

		public double PrevDependentCare
		{
			set
			{
				lngPrevDependentCare = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevDependentCare = 0;
				PrevDependentCare = lngPrevDependentCare * 100;
				return PrevDependentCare;
			}
		}

		public double CurrDependentCare
		{
			set
			{
				lngCurrDependentCare = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrDependentCare = 0;
				CurrDependentCare = lngCurrDependentCare / 100.0;
				return CurrDependentCare;
			}
		}

		public double PrevBox12
		{
			set
			{
				lngPrevBox12 = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevBox12 = 0;
				PrevBox12 = lngPrevBox12 / 100.0;
				return PrevBox12;
			}
		}

		public double CurrBox12
		{
			set
			{
				lngCurrBox12 = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrBox12 = 0;
				CurrBox12 = lngCurrBox12 / 100.0;
				return CurrBox12;
			}
		}

		public double PrevBox14
		{
			set
			{
				lngPrevBox14 = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevBox14 = 0;
				PrevBox14 = lngPrevBox14 / 100.0;
				return PrevBox14;
			}
		}

		public double CurrBox14
		{
			set
			{
				lngCurrBox14 = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrBox14 = 0;
				CurrBox14 = lngCurrBox14 / 100.0;
				return CurrBox14;
			}
		}

		public double PrevStateWage
		{
			set
			{
				lngPrevStateWage = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevStateWage = 0;
				PrevStateWage = lngPrevStateWage / 100.0;
				return PrevStateWage;
			}
		}

		public double CurrStateWage
		{
			set
			{
				lngCurrStateWage = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrStateWage = 0;
				CurrStateWage = lngCurrStateWage / 100.0;
				return CurrStateWage;
			}
		}

		public double PrevStateTax
		{
			set
			{
				lngPrevStateTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double PrevStateTax = 0;
				PrevStateTax = lngPrevStateTax / 100.0;
				return PrevStateTax;
			}
		}

		public double CurrStateTax
		{
			set
			{
				lngCurrStateTax = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double CurrStateTax = 0;
				CurrStateTax = lngCurrStateTax / 100.0;
				return CurrStateTax;
			}
		}

		public double Tot401k
		{
			set
			{
				lngTot401k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double Tot401k = 0;
				Tot401k = lngTot401k / 100.0;
				return Tot401k;
			}
		}

		public double TotPrev401k
		{
			set
			{
				lngTotPrev401k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrev401k = 0;
				TotPrev401k = lngTotPrev401k / 100.0;
				return TotPrev401k;
			}
		}

		public double Tot403b
		{
			set
			{
				lngTot403b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double Tot403b = 0;
				Tot403b = lngTot403b / 100.0;
				return Tot403b;
			}
		}

		public double TotPrev403b
		{
			set
			{
				lngTotPrev403b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrev403b = 0;
				TotPrev403b = lngTotPrev403b / 100.0;
				return TotPrev403b;
			}
		}

		public double Tot408k
		{
			set
			{
				lngTot408k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double Tot408k = 0;
				Tot408k = lngTot408k / 100.0;
				return Tot408k;
			}
		}

		public double TotPrev408k
		{
			set
			{
				lngTotPrev408k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrev408k = 0;
				TotPrev408k = lngTotPrev408k / 100.0;
				return TotPrev408k;
			}
		}

		public double Tot457b
		{
			set
			{
				lngTot457b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double Tot457b = 0;
				Tot457b = lngTot457b / 100.0;
				return Tot457b;
			}
		}

		public double TotPrev457b
		{
			set
			{
				lngTotPrev457b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrev457b = 0;
				TotPrev457b = lngTotPrev457b / 100.0;
				return TotPrev457b;
			}
		}

		public double Tot501c
		{
			set
			{
				lngTot501c = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double Tot501c = 0;
				Tot501c = lngTot501c / 100.0;
				return Tot501c;
			}
		}

		public double TotPrev501c
		{
			set
			{
				lngTotPrev501c = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrev501c = 0;
				TotPrev501c = lngTotPrev501c / 100.0;
				return TotPrev501c;
			}
		}

		public double TotLife
		{
			set
			{
				lngTotLife = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotLife = 0;
				TotLife = lngTotLife / 100.0;
				return TotLife;
			}
		}

		public double TotPrevLife
		{
			set
			{
				lngTotPrevLife = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrevLife = 0;
				TotPrevLife = lngTotPrevLife / 100.0;
				return TotPrevLife;
			}
		}

		public double TotHealthSavings
		{
			set
			{
				lngTotHealthSavings = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotHealthSavings = 0;
				TotHealthSavings = lngTotHealthSavings / 100.0;
				return TotHealthSavings;
			}
		}

		public double TotPrevHealthSavings
		{
			set
			{
				lngTotPrevHealthSavings = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrevHealthSavings = 0;
				TotPrevHealthSavings = lngTotPrevHealthSavings / 100.0;
				return TotPrevHealthSavings;
			}
		}

		public double TotDeferredCompensation
		{
			set
			{
				lngTotDeferredCompensation = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotDeferredCompensation = 0;
				TotDeferredCompensation = lngTotDeferredCompensation / 100.0;
				return TotDeferredCompensation;
			}
		}

		public double TotPrevDeferredCompensation
		{
			set
			{
				lngTotPrevDeferredCompensation = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrevDeferredCompensation = 0;
				TotPrevDeferredCompensation = lngTotPrevDeferredCompensation / 100.0;
				return TotPrevDeferredCompensation;
			}
		}

		public double TotRoth401k
		{
			set
			{
				lngTotRoth401k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotRoth401k = 0;
				TotRoth401k = lngTotRoth401k / 100.0;
				return TotRoth401k;
			}
		}

		public double TotRoth403b
		{
			set
			{
				lngTotRoth403b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotRoth403b = 0;
				TotRoth403b = lngTotRoth403b / 100.0;
				return TotRoth403b;
			}
		}

		public double TotPrevRoth401k
		{
			set
			{
				lngTotPrevRoth401k = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrevRoth401k = 0;
				TotPrevRoth401k = lngTotPrevRoth401k / 100.0;
				return TotPrevRoth401k;
			}
		}

		public double TotPrevRoth403b
		{
			set
			{
				lngTotPrevRoth403b = FCConvert.ToInt32(value * 100);
			}
			get
			{
				double TotPrevRoth403b = 0;
				TotPrevRoth403b = lngTotPrevRoth403b / 100.0;
				return TotPrevRoth403b;
			}
		}
	}
}
