//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cFed941Controller
	{
		//=========================================================
		public object GetFed941(int intYear, int intQuarter)
		{
			object GetFed941 = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			cFed941 f = new cFed941();
			rsLoad.OpenRecordset("select * from tblFed941 where ReportYear = " + FCConvert.ToString(intYear) + " and [Quarter] = " + FCConvert.ToString(intQuarter), "Payroll");
			if (!rsLoad.EndOfFile())
			{
				f.Quarter = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("quarter"))));
				f.ReportYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("reportyear"))));
				f.CobraIndividuals = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CobraIndividuals"))));
				f.CobraPayments = Conversion.Val(rsLoad.Get_Fields_Double("CobraPayments"));
				f.CurrentYearIncomeTaxWHAdjust = Conversion.Val(rsLoad.Get_Fields_Double("CurrentYearIncomeTaxWithholdingAdjustment"));
				f.Deposits = Conversion.Val(rsLoad.Get_Fields("deposits"));
				f.PriorYearSSAndMedAdjustments = Conversion.Val(rsLoad.Get_Fields_Double("PriorQuarterSSAndMedicareAdjustment"));
				f.SickPayAdjustments = Conversion.Val(rsLoad.Get_Fields_Double("SickPayAdjustment"));
				f.TipsAndLifeAdjustments = Conversion.Val(rsLoad.Get_Fields_Double("TipsAndLifeInsuranceAdjustment"));
			}
			GetFed941 = f;
			return GetFed941;
		}

		public void SaveFed941(ref cFed941 f)
		{
			if (!(f == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from tblFed941 where reportyear = " + f.ReportYear + " and [quarter] = " + f.Quarter, "payroll");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("quarter", f.Quarter);
				rsSave.Set_Fields("ReportYear", f.ReportYear);
				rsSave.Set_Fields("CobraIndividuals", f.CobraIndividuals);
				rsSave.Set_Fields("CobraPayments", f.CobraPayments);
				rsSave.Set_Fields("CurrentYearIncomeTaxWithholdingAdjustment", f.CurrentYearIncomeTaxWHAdjust);
				rsSave.Set_Fields("Deposits", f.Deposits);
				rsSave.Set_Fields("PriorQuarterSSAndMedicareAdjustment", f.PriorYearSSAndMedAdjustments);
				rsSave.Set_Fields("SickPayAdjustment", f.SickPayAdjustments);
				rsSave.Set_Fields("TipsAndLifeInsuranceAdjustment", f.TipsAndLifeAdjustments);
				rsSave.Update();
			}
		}
	}
}
