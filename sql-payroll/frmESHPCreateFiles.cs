//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	public partial class frmESHPCreateFiles : BaseForm
	{
		public frmESHPCreateFiles()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmESHPCreateFiles InstancePtr
		{
			get
			{
				return (frmESHPCreateFiles)Sys.GetInstance(typeof(frmESHPCreateFiles));
			}
		}

		protected frmESHPCreateFiles _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsESHPChecks tChecks;
		private clsESHPEmployeeList tEmps;
		private clsESHPEmployeeTaxList tTaxes;
		private clsESHPFileCreator tCheckFile;
		private string strPayDate = string.Empty;
		private bool boolloaded;

		private void frmESHPCreateFiles_Activated(object sender, System.EventArgs e)
		{
			if (!boolloaded)
			{
				boolloaded = true;
				CreateFile();
			}
		}

		private void tCheckFile_CheckRecordAdded(ref int intPercent)
		{
			if (intPercent >= 0)
			{
				if (intPercent <= 100)
				{
					pbPercentDone.Value = intPercent;
				}
				else
				{
					pbPercentDone.Value = 100;
				}
			}
			else
			{
				pbPercentDone.Value = 0;
			}
			pbPercentDone.Refresh();
			// Me.SetFocus
			//App.DoEvents();
		}

		private void tCheckFile_EmployeeTaxRecordAdded(ref int intPercent)
		{
			if (intPercent >= 0)
			{
				if (intPercent <= 100)
				{
					pbPercentDone.Value = intPercent;
				}
				else
				{
					pbPercentDone.Value = 100;
				}
			}
			else
			{
				pbPercentDone.Value = 0;
			}
			pbPercentDone.Refresh();
			// Me.SetFocus
			//App.DoEvents();
		}

		private void tCheckFile_EmployeeRecordAdded(ref int intPercent)
		{
			if (intPercent >= 0)
			{
				if (intPercent <= 100)
				{
					pbPercentDone.Value = intPercent;
				}
				else
				{
					pbPercentDone.Value = 100;
				}
			}
			else
			{
				pbPercentDone.Value = 0;
			}
			pbPercentDone.Refresh();
			// Me.SetFocus
			//App.DoEvents();
		}

		private void frmESHPCreateFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmESHPCreateFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmESHPCreateFiles properties;
			//frmESHPCreateFiles.FillStyle	= 0;
			//frmESHPCreateFiles.ScaleWidth	= 5880;
			//frmESHPCreateFiles.ScaleHeight	= 4245;
			//frmESHPCreateFiles.LinkTopic	= "Form2";
			//frmESHPCreateFiles.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			boolloaded = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			//App.DoEvents();
		}

		public void Init(ref clsESHPChecks clsChecks)
		{
			tChecks = clsChecks;
			tCheckFile = new clsESHPFileCreator();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				tCheckFile.OrganizationNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("esporganization"))));
			}
			strPayDate = Strings.Format(tChecks.PayDate, "MM/dd/yyyy");
			//App.DoEvents();
			this.Show(App.MainForm);
			//App.DoEvents();
		}

		private void CreateFile()
		{
			bool boolOK;
			boolOK = true;
			lblUpdate.Text = "Creating check records";
			if (tCheckFile.CreateCheckFile(ref tChecks))
			{
				lblUpdate.Text = "Creating employee records";
				if (CreateEmployeeRecords())
				{
					if (tCheckFile.CreateEmployeeFile(ref tEmps))
					{
						if (tCheckFile.CreateEmployeeTaxFile(ref tTaxes))
						{
							//Upload();
						}
						else
						{
							Close();
						}
					}
					else
					{
						Close();
					}
				}
				else
				{
					Close();
				}
			}
			else
			{
				Close();
			}
		}

		private bool CreateEmployeeRecords()
		{
			bool CreateEmployeeRecords = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				tEmps = new clsESHPEmployeeList();
				tTaxes = new clsESHPEmployeeTaxList();
				tTaxes.ClearList();
				tEmps.ClearList();
				tEmps.PayDate = tChecks.PayDate;
				tEmps.Payrun = tChecks.Payrun;
				tTaxes.PayDate = tChecks.PayDate;
				tTaxes.Payrun = tChecks.Payrun;
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsState = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("select distinct ssn from tblemployeemaster where not (status = 'Terminated' or status = 'Retired' or status = 'Resigned')", "twpy0000.vb1");
				rsState.OpenRecordset("select * from states order by id", "twpy0000.vb1");
				rsLoad.OpenRecordset("select * from tblemployeemaster where not (status = 'Terminated' or status = 'Retired' or status = 'Resigned') order by employeenumber", "twpy0000.vb1");
				clsESHPEmployeeDetail tEmp;
				clsESHPEmployeeTax tTax;
				string strTemp = "";
				while (!rsTemp.EndOfFile())
				{
					if (rsLoad.FindFirstRecord("ssn", rsTemp.Get_Fields("ssn")))
					{
						if (FCConvert.ToString(rsLoad.Get_Fields("ssn")).Length != 9)
						{
							MessageBox.Show("Invalid social security number for " + rsLoad.Get_Fields("EmployeeNumber") + "  " + rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("lastname"));
							return CreateEmployeeRecords;
						}
						tEmp = new clsESHPEmployeeDetail();
						tEmp.SocialSecurityNumber = FCConvert.ToString(rsLoad.Get_Fields("ssn"));
						tEmp.Address1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("Address1")));
						tEmp.Address2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("Address2")));
						tEmp.City = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("city")));
						tEmp.ZipCode = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("zip")));
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("zip4"))) != "")
						{
							tEmp.ZipCode = tEmp.ZipCode + "-" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("zip4")));
						}
						tEmp.Email = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("email")));
						tEmp.FaxNumber = "";
						tEmp.FirstName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("firstname")));
						strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("Phone")));
						strTemp = strTemp.Replace("(", "");
						strTemp = strTemp.Replace(")", "");
						tEmp.HomePhone = strTemp;
						tEmp.LastName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("lastname")));
						if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ftorpt")))
						{
							tEmp.LeaveClassLink = "FullTime";
						}
						else
						{
							tEmp.LeaveClassLink = "PartTime";
						}
						tEmp.LegalFirstName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("firstname")));
						tEmp.LegalLastName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("lastname")));
						tEmp.LegalMiddleName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("middlename")));
						tEmp.MainPosition = "";
						if (FCConvert.ToInt32(rsLoad.Get_Fields("fedfilingstatusid")) == 4)
						{
							tEmp.MaritalStatus = "S";
						}
						else
						{
							tEmp.MaritalStatus = "M";
						}
						// fed
						tTax = new clsESHPEmployeeTax();
						tTax.Description = "Federal Income Tax";
						tTax.SortSequence = "1";
						tTax.SocialSecurityNumber = tEmp.SocialSecurityNumber;
						tTax.Exemptions = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fedstatus"))));
						tTax.StartDate = "1/1/" + FCConvert.ToString(DateTime.Today.Year);
						if (Information.IsDate(rsLoad.Get_Fields("datehire")))
						{
							if (Convert.ToDateTime(rsLoad.Get_Fields("datehire")).ToOADate() != 0)
							{
								tTax.StartDate = Strings.Format(rsLoad.Get_Fields("datehire"), "MMddyyyy");
							}
						}
						if (Conversion.Val(rsLoad.Get_Fields("fedfilingstatusID")) != 5)
						{
							tTax.FilingStatus = "S";
						}
						else
						{
							tTax.FilingStatus = "M";
						}
						tTax.AdditionalExemptions = 0;
						if (FCConvert.ToString(rsLoad.Get_Fields_String("FedDollarPercent")) == "Dollar")
						{
							tTax.AdditionalAmount = Conversion.Val(rsLoad.Get_Fields("addfed"));
							tTax.AdditionalPercent = 0;
						}
						else
						{
							tTax.AdditionalAmount = 0;
							tTax.AdditionalPercent = Conversion.Val(rsLoad.Get_Fields("addfed"));
						}
						tTaxes.AddItem(ref tTax);
						// state
						tTax = new clsESHPEmployeeTax();
						tTax.Description = "State Income Tax";
						tTax.SortSequence = "1";
						tTax.SocialSecurityNumber = tEmp.SocialSecurityNumber;
						tTax.Exemptions = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("statestatus"))));
						tTax.StartDate = "1/1/" + FCConvert.ToString(DateTime.Today.Year);
						if (Information.IsDate(rsLoad.Get_Fields("datehire")))
						{
							if (Convert.ToDateTime(rsLoad.Get_Fields("datehire")).ToOADate() != 0)
							{
								tTax.StartDate = Strings.Format(rsLoad.Get_Fields("datehire"), "MMddyyyy");
							}
						}
						if (Conversion.Val(rsLoad.Get_Fields("statefilingstatusID")) != 6)
						{
							tTax.FilingStatus = "S";
						}
						else
						{
							tTax.FilingStatus = "M";
						}
						tTax.AdditionalExemptions = 0;
						if (FCConvert.ToString(rsLoad.Get_Fields("statedollarpercent")) == "Dollar")
						{
							tTax.AdditionalAmount = Conversion.Val(rsLoad.Get_Fields("addstate"));
							tTax.AdditionalPercent = 0;
						}
						else
						{
							tTax.AdditionalAmount = 0;
							tTax.AdditionalPercent = Conversion.Val(rsLoad.Get_Fields("addstate"));
						}
						tTaxes.AddItem(ref tTax);
						tEmp.MiddleInitial = Strings.Left(rsLoad.Get_Fields("middlename") + " ", 1);
						tEmp.SocialSecurityNumber = FCConvert.ToString(rsLoad.Get_Fields("ssn"));
						if (rsState.FindFirstRecord("ID", rsLoad.Get_Fields("State")))
						{
							tEmp.State = FCConvert.ToString(rsState.Get_Fields("State"));
						}
						else
						{
							tEmp.State = "";
						}
						tEmps.AddItem(ref tEmp);
					}
					rsTemp.MoveNext();
				}
				CreateEmployeeRecords = true;
				return CreateEmployeeRecords;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateEmployeeRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateEmployeeRecords;
		}
		//FC:TODO
		//private void InitializeUploader(ref ESPUploader tUploader)
		//{
		//	clsDRWrapper rsLoad = new clsDRWrapper();
		//	rsLoad.OpenRecordset("select * from tblDefaultInformation", "twpy0000.vb1");
		//	if (!rsLoad.EndOfFile()) {
		//		tUploader.Organization = Conversion.Val(rsLoad.Get_Fields("esporganization")));
		//		tUploader.HostCheckPath = rsLoad.Get_Fields("espservercheckdir");
		//		tUploader.HostDatapath = rsLoad.Get_Fields("espserverdatadir");
		//		tUploader.HostExportPath = rsLoad.Get_Fields("espserverexportdir");
		//		tUploader.HostW2Path = rsLoad.Get_Fields("espserverw2dir");
		//		tUploader.HostName = rsLoad.Get_Fields("esphost");
		//		tUploader.Port = Conversion.Val(rsLoad.Get_Fields("espport")));
		//		tUploader.User = rsLoad.Get_Fields("espuser");
		//		tUploader.KeyPassword = rsLoad.Get_Fields("esppassword");
		//	}
		//	// Test password I3vViU4vIbJQa3rz
		//	// test user harrissftp
		//}
		//private void Upload()
		//{
		//	ESPUploader tUploader = new ESPUploader();
		//	string strReturn;
		//	try
		//	{	// On Error GoTo ErrorHandler
		//		fecherFoundation.Information.Err().Clear();
		//		InitializeUploader(ref tUploader);
		//		strReturn = tUploader.Connect;
		//		if (strReturn!="") {
		//			MessageBox.Show(strReturn, "Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			tUploader.Dispose();
		//			return;
		//		}
		//		strReturn = tUploader.AuthenticatePW;
		//		if (strReturn!="") {
		//			MessageBox.Show(strReturn, "Authentication Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			tUploader.Dispose();
		//			return;
		//		}
		//		strReturn = tUploader.InitializeSFTP;
		//		if (strReturn!="") {
		//			MessageBox.Show(strReturn, "SFTP Initialization Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			tUploader.Disconnect();
		//			tUploader.Dispose();
		//			return;
		//		}
		//		// upload data files
		//		if (UploadDataFiles(ref tUploader)) {
		//			// upload check files
		//			pbPercentDone.Value = 0;
		//			lblUpdate.Text = "";
		//			if (UploadCheckFiles(ref tUploader)) {
		//				pbPercentDone.Value = 0;
		//				lblUpdate.Text = "";
		//			} else {
		//				return;
		//			}
		//		} else {
		//			return;
		//		}
		//		MessageBox.Show("Transfer Complete", "Transferred", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		tUploader.Disconnect();
		//		tUploader.Dispose();
		//		Close();
		//		return;
		//	}
		//	catch (Exception ex)
		//	{	// ErrorHandler:
		//		MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+" "+fecherFoundation.Information.Err(ex).Description+"\r\n"+"In Upload", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}
		//private bool UploadDataFiles(ref ESPUploader tUploader)
		//{
		//	bool UploadDataFiles = false;
		//	try
		//	{	// On Error GoTo ErrorHandler
		//		fecherFoundation.Information.Err().Clear();
		//		string strDatDir;
		//		FCCollection strFList = new FCCollection();
		//		string strTemp;
		//		int x;
		//		FCFileSystem fs = new FCFileSystem();
		//		strDatDir = tCheckFile.DataPath+tCheckFile.PayRunPath;
		//		strTemp = FCFileSystem.Dir(strDatDir+"*.dat", 0);
		//		if (!(strTemp=="")) {
		//			while (strTemp!="") {
		//				strFList.Add(strTemp);
		//				strTemp = FCFileSystem.Dir();
		//			}
		//		}
		//		pbPercentDone.Value = 0;
		//		double dblIncrement;
		//		dblIncrement = 0;
		//		if (strFList.Count>0) {
		//			dblIncrement = 100/strFList.Count;
		//		}
		//		string strReturn = "";
		//		for(x=1; x<=strFList.Count; x++) {
		//			lblUpdate.Text = "Uploading "+strFList[x];
		//			strReturn = tUploader.UploadDataFile(strDatDir+strFList[x], strFList[x]);
		//			if (strReturn!="") {
		//				MessageBox.Show(strReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				UploadDataFiles = false;
		//				return UploadDataFiles;
		//			}
		//			IncrementProgress(dblIncrement);
		//			fs.DeleteFile(strDatDir+strFList[x], true);
		//		} // x
		//		UploadDataFiles = true;
		//		return UploadDataFiles;
		//	}
		//	catch (Exception ex)
		//	{	// ErrorHandler:
		//		MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+" "+fecherFoundation.Information.Err(ex).Description+"\r\n"+"In UploadDataFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return UploadDataFiles;
		//}
		//private bool UploadCheckFiles(ref ESPUploader tUploader)
		//{
		//	bool UploadCheckFiles = false;
		//	try
		//	{	// On Error GoTo ErrorHandler
		//		fecherFoundation.Information.Err().Clear();
		//		string strCheckDir;
		//		FCCollection strFList = new FCCollection();
		//		string strTemp;
		//		int x;
		//		FCFileSystem fs = new FCFileSystem();
		//		strCheckDir = tCheckFile.CheckPath+tCheckFile.PayRunPath;
		//		strTemp = FCFileSystem.Dir(strCheckDir+"*.pdf", 0);
		//		if (!(strTemp=="")) {
		//			while (strTemp!="") {
		//				strFList.Add(strTemp);
		//				strTemp = FCFileSystem.Dir();
		//			}
		//		}
		//		pbPercentDone.Value = 0;
		//		double dblIncrement;
		//		dblIncrement = 0;
		//		if (strFList.Count>0) {
		//			dblIncrement = 100/strFList.Count;
		//		}
		//		string strReturn = "";
		//		for(x=1; x<=strFList.Count; x++) {
		//			lblUpdate.Text = "Uploading "+strFList[x];
		//			strReturn = tUploader.UploadCheckFile(strCheckDir+strFList[x], strFList[x], strPayDate);
		//			if (strReturn!="") {
		//				MessageBox.Show(strReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				UploadCheckFiles = false;
		//				return UploadCheckFiles;
		//			}
		//			IncrementProgress(dblIncrement);
		//			fs.DeleteFile(strCheckDir+strFList[x], true);
		//		} // x
		//		UploadCheckFiles = true;
		//		return UploadCheckFiles;
		//	}
		//	catch (Exception ex)
		//	{	// ErrorHandler:
		//		MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+" "+fecherFoundation.Information.Err(ex).Description+"\r\n"+"In UploadCheckFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return UploadCheckFiles;
		//}
		private void IncrementProgress(double dblIncrement)
		{
			if (dblIncrement + pbPercentDone.Value > pbPercentDone.Maximum)
			{
				pbPercentDone.Value = pbPercentDone.Maximum;
			}
			else
			{
				pbPercentDone.Value = FCConvert.ToInt32(pbPercentDone.Value + dblIncrement);
			}
			pbPercentDone.Refresh();
			//App.DoEvents();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
