//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmReportSelection : BaseForm
	{
		public frmReportSelection()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReportSelection InstancePtr
		{
			get
			{
				return (frmReportSelection)Sys.GetInstance(typeof(frmReportSelection));
			}
		}

		protected frmReportSelection _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 08,2001
		//
		// NOTES:
		//
		// MODIFIED BY: DAN C. SOLTESZ
		//
		// **************************************************
		// private local variables
		// Private dbReportSelection     As DAO.Database
		private clsDRWrapper rsReportSelection = new clsDRWrapper();
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(DialogResult, int)
		private DialogResult intDataChanged;
		private Control ControlName = new Control();
		private int intReportOption;
		private bool boolDataChanged;
		private bool boolFormLoading;
		const int WEEKLY = 0;
		const int MONTHLY = 1;
		const int QUARTERLY = 2;
		const int FISCAL = 3;
		const int CALENDAR = 4;

		private void SetReportOptions(int intOption)
		{
			chkAccounting.Enabled = false;
			chkBankList.Enabled = false;
			chkDataEntryForms.Enabled = false;
			chkDeductions.Enabled = false;
			chkOther.Enabled = false;
			chkPayrollWarrant.Enabled = false;
			chkPayTaxSummary.Enabled = false;
			chkSick.Enabled = false;
			chkTrust.Enabled = false;
			chkVacation.Enabled = false;
			chkCheckRegister.Enabled = false;
			chkAudit.Enabled = false;
			chkTempList.Enabled = false;
			chkBackup.Enabled = false;
			Label2.Enabled = false;
			Label3.Enabled = false;
			Label4.Enabled = false;
			Label5.Enabled = false;
			switch (intOption)
			{
				case WEEKLY:
					{
						chkAccounting.Enabled = true;
						chkBankList.Enabled = true;
						chkDataEntryForms.Enabled = true;
						chkDeductions.Enabled = true;
						chkOther.Enabled = true;
						chkPayrollWarrant.Enabled = true;
						chkPayTaxSummary.Enabled = true;
						chkSick.Enabled = true;
						chkTrust.Enabled = true;
						chkVacation.Enabled = true;
						chkCheckRegister.Enabled = true;
						chkAudit.Enabled = true;
						chkTempList.Enabled = true;
						chkBackup.Enabled = true;
						chk941C1.Enabled = false;
						Label2.Enabled = true;
						Label3.Enabled = true;
						Label4.Enabled = true;
						Label5.Enabled = true;
						break;
					}
				case QUARTERLY:
					{
						chk941C1.Enabled = true;
						chkDeductions.Enabled = true;
						chkOther.Enabled = true;
						chkPayTaxSummary.Enabled = true;
						chkSick.Enabled = true;
						chkVacation.Enabled = true;
						break;
					}
				default:
					{
						chk941C1.Enabled = false;
						chkDeductions.Enabled = true;
						chkOther.Enabled = true;
						chkPayTaxSummary.Enabled = true;
						chkSick.Enabled = true;
						chkVacation.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void chkAccounting_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkAudit_CheckedChanged(object sender, System.EventArgs e)
		{
			// If intReportOption = WEEKLY Then chkAudit.Value = 1
			if (!boolFormLoading)
				boolDataChanged = true;
		}
		// Private Sub chkBackup_Click()
		// If intReportOption = WEEKLY Then chkBackup.Value = 1
		// If Not boolFormLoading Then boolDataChanged = True
		// End Sub
		private void chkBankList_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}
		// Private Sub chkCheckRegister_Click()
		// If intReportOption = WEEKLY Then chkCheckRegister.Value = 1
		// If Not boolFormLoading Then boolDataChanged = True
		// End Sub
		private void chkDataEntryForms_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkDeductions_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkOther_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkPayrollWarrant_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkPayTaxSummary_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkSick_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}
		// Private Sub chkTempList_Click()
		// If intReportOption = WEEKLY Then chkTempList.Value = 1
		// If Not boolFormLoading Then boolDataChanged = True
		// End Sub
		private void chkTrust_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void chkVacation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolFormLoading)
				boolDataChanged = true;
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Delete the field length information from the database
				if (MessageBox.Show("This action will delete Report Selection infomation. Continue?", "Payroll Standard Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsReportSelection.Execute("Delete from tblReportSelection", "Payroll");
					cmdNew_Click();
					MessageBox.Show("Delete of record was successful", "Payroll Report Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (boolDataChanged)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
						// Call SetVerifyAdjustNeeded(True)
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// clear out the controls butDo nothing to the database
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string, int)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCTextBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCComboBox)
					{
					}
					else if (ControlName is FCCheckBox)
					{
						(ControlName as FCCheckBox).Value = 0;
					}
					else
					{
						/*? On Error Resume Next  */
						//FC:FINAL:DSE:#i2264 Clear only TextBox, CheckBox and ComboBox
						//ControlName.Text = string.Empty;
						fecherFoundation.Information.Err().Clear();
					}
					ControlName = null;
				}
				// form is now dirty
				intDataChanged = (DialogResult)1;
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsReportSelection.OpenRecordset("Select * from tblReportSelection where ReportOption = " + FCConvert.ToString(intReportOption), "TWPY0000.vb1");
				if (rsReportSelection.EndOfFile())
				{
					rsReportSelection.AddNew();
				}
				else
				{
					rsReportSelection.Edit();
				}
				rsReportSelection.SetData("Trust", FCConvert.ToInt32(chkTrust.CheckState));
				// rsReportSelection.SetData "CheckRegister", chkCheckRegister
				if (intReportOption == WEEKLY)
				{
					rsReportSelection.Set_Fields("CheckRegister", true);
					rsReportSelection.Set_Fields("TempList", true);
					rsReportSelection.Set_Fields("Backup", true);
				}
				else
				{
					rsReportSelection.Set_Fields("CheckRegister", false);
					rsReportSelection.Set_Fields("TempList", false);
					rsReportSelection.Set_Fields("Backup", false);
				}
				rsReportSelection.SetData("PayTaxSummary", FCConvert.ToInt32(chkPayTaxSummary.CheckState));
				rsReportSelection.SetData("Deductions", FCConvert.ToInt32(chkDeductions.CheckState));
				rsReportSelection.SetData("BankLists", FCConvert.ToInt32(chkBankList.CheckState));
				rsReportSelection.SetData("Accounting", FCConvert.ToInt32(chkAccounting.CheckState));
				rsReportSelection.SetData("DataEntryForms", FCConvert.ToInt32(chkDataEntryForms.CheckState));
				rsReportSelection.SetData("Vacation", FCConvert.ToInt32(chkVacation.CheckState));
				rsReportSelection.SetData("Sick", FCConvert.ToInt32(chkSick.CheckState));
				rsReportSelection.SetData("Other", FCConvert.ToInt32(chkOther.CheckState));
				rsReportSelection.SetData("Audit", FCConvert.ToInt32(chkAudit.CheckState));
				// rsReportSelection.SetData "TempList", chkTempList
				// rsReportSelection.SetData "Backup", chkBackup
				rsReportSelection.SetData("PayrollWarrant", FCConvert.ToInt32(chkPayrollWarrant.CheckState));
				rsReportSelection.SetData("ReportOption", intReportOption);
				rsReportSelection.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
				if (chk941C1.CheckState == Wisej.Web.CheckState.Checked && intReportOption == QUARTERLY)
				{
					rsReportSelection.Set_Fields("C1", true);
				}
				else
				{
					rsReportSelection.Set_Fields("c1", false);
				}
				rsReportSelection.Update();
				// all changes have been updated
				intDataChanged = 0;
				boolDataChanged = false;
				MessageBox.Show("Save was successful", "Payroll Report Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmReportSelection_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				// Call ForceFormToResize(Me)
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmReportSelection_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				if (KeyAscii == Keys.Return)
				{
					if (this.ActiveControl is FCButton)
					{
					}
					else
					{
						Support.SendKeys("{TAB}", false);
					}
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmReportSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportSelection properties;
			//frmReportSelection.ScaleWidth	= 9045;
			//frmReportSelection.ScaleHeight	= 7260;
			//frmReportSelection.LinkTopic	= "Form1";
			//frmReportSelection.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				rsReportSelection.DefaultDB = "TWPY0000.vb1";
				// Get the data from the database
				ShowData();
				intDataChanged = 0;
                cmbOption.SelectedIndex = 0;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolFormLoading = true;
				chkAccounting.CheckState = Wisej.Web.CheckState.Unchecked;
				chkBankList.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDataEntryForms.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDeductions.CheckState = Wisej.Web.CheckState.Unchecked;
				chkOther.CheckState = Wisej.Web.CheckState.Unchecked;
				chkPayrollWarrant.CheckState = Wisej.Web.CheckState.Unchecked;
				chkPayTaxSummary.CheckState = Wisej.Web.CheckState.Unchecked;
				chkSick.CheckState = Wisej.Web.CheckState.Unchecked;
				chkTrust.CheckState = Wisej.Web.CheckState.Unchecked;
				chkVacation.CheckState = Wisej.Web.CheckState.Unchecked;
				if (intReportOption == WEEKLY)
				{
					// chkCheckRegister.Value = vbChecked
					chkAudit.CheckState = Wisej.Web.CheckState.Checked;
					// chkTempList.Value = vbChecked
					// chkBackup.Value = vbChecked
				}
				else
				{
					// chkCheckRegister.Value = vbUnchecked
					chkAudit.CheckState = Wisej.Web.CheckState.Unchecked;
					// chkTempList.Value = vbUnchecked
					// chkBackup.Value = vbUnchecked
				}
				// get all of the records from the database
				rsReportSelection.OpenRecordset("Select * from tblReportSelection where ReportOption = " + FCConvert.ToString(intReportOption), "TWPY0000.vb1");
				if (!rsReportSelection.EndOfFile())
				{
					if (rsReportSelection.Get_Fields_Boolean("Trust") == true)
					{
						chkTrust.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTrust.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("C1") == true)
					{
						chk941C1.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chk941C1.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// If rsReportSelection.Fields("CheckRegister") = True Then
					// chkCheckRegister.Value = vbChecked
					// Else
					// chkCheckRegister.Value = vbUnchecked
					// End If
					if (rsReportSelection.Get_Fields_Boolean("PayTaxSummary") == true)
					{
						chkPayTaxSummary.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPayTaxSummary.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("Deductions") == true)
					{
						chkDeductions.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDeductions.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("BankLists") == true)
					{
						chkBankList.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBankList.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("Accounting") == true)
					{
						chkAccounting.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkAccounting.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("DataEntryForms") == true)
					{
						chkDataEntryForms.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDataEntryForms.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("Vacation") == true)
					{
						chkVacation.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkVacation.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("Sick") == true)
					{
						chkSick.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSick.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields_Boolean("Audit") == true)
					{
						chkAudit.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkAudit.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (rsReportSelection.Get_Fields("Other") == true)
					{
						chkOther.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkOther.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// If rsReportSelection.Fields("TempList") = True Then
					// chkTempList.Value = vbChecked
					// Else
					// chkTempList.Value = vbUnchecked
					// End If
					// If rsReportSelection.Fields("Backup") = True Then
					// chkBackup.Value = vbChecked
					// Else
					// chkBackup.Value = vbUnchecked
					// End If
					if (rsReportSelection.Get_Fields_Boolean("PayrollWarrant") == true)
					{
						chkPayrollWarrant.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPayrollWarrant.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else if (intReportOption == QUARTERLY)
				{
					chk941C1.CheckState = Wisej.Web.CheckState.Checked;
				}
				boolFormLoading = false;
				// Set rsReportSelection = Nothing
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (intDataChanged == (DialogResult)2)
				{
					e.Cancel = true;
					return;
				}
				boolDataChanged = false;
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuReset_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void optOption_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has not been saved. Save Now?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					cmdSave_Click();
				}
			}
			boolDataChanged = false;
			intReportOption = Index;
			SetReportOptions(Index);
			ShowData();
		}

		private void optOption_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOption.SelectedIndex;
			optOption_CheckedChanged(index, sender, e);
		}
	}
}
