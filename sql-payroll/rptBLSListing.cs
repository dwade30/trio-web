//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBLSListing.
	/// </summary>
	public partial class rptBLSListing : BaseSectionReport
	{
		public rptBLSListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "BLS Worksite ID Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBLSListing InstancePtr
		{
			get
			{
				return (rptBLSListing)Sys.GetInstance(typeof(rptBLSListing));
			}
		}

		protected rptBLSListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBLSListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *********************************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       March 1st, 2005
		//
		//
		// NOTES: THIS WAS BUILT PER REQUEST FROM CALL ID 63779
		// THIS WI
		// **********************************************************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int intpage;
		int intTotal;
		string strCurrentValue = "";
		int lngGrandTotal;
		bool gboolEnd;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			while (!rsData.EndOfFile())
			{
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				gboolEnd = true;
				eArgs.EOF = true;
				return;
			}
			if (strCurrentValue == FCConvert.ToString(rsData.Get_Fields("BLSWorkSiteID")))
			{
			}
			else
			{
                //FC:FINAL:AM:#i2232 - have to use a data field for grouping
                //GroupHeader1.DataField = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BLSWorksiteID")));
                strCurrentValue = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("BLSWorkSiteID")));
                this.Fields["Binder"].Value = strCurrentValue;
            }
			txtEmployee.Text = rsData.Get_Fields_String("EmployeeNumber") + " - " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields_String("Desig");
			intTotal += 1;
			lngGrandTotal += 1;
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSort = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsData.OpenRecordset("Select * from tblDefaultInformation");
			// 0=EmployeeNumbe,r 1=Employee Name, 2=Sequence #, 3=DeptDiv
			if (!rsData.EndOfFile())
			{
				switch (rsData.Get_Fields_Int32("ReportSequence"))
				{
					case 0:
						{
							strSort = ",LastName,FirstName";
							break;
						}
					case 1:
						{
							strSort = ",EmployeeNumber";
							break;
						}
					case 2:
						{
							strSort = ",SeqNumber,LastName,FirstName";
							break;
						}
					case 3:
						{
							strSort = ",DeptDiv,LastName,FirstName";
							break;
						}
					default:
						{
							strSort = ",EmployeeNumber";
							break;
						}
				}
				//end switch
			}
			else
			{
				strSort = ",EmployeeNumber";
			}
			rsData.OpenRecordset("Select * from tblEmployeeMaster where Status <> 'Terminated' and status <> 'Resigned' Order by rtrim(isnull(blsworksiteid, ''))" + strSort);
			if (!rsData.EndOfFile())
			{
				strCurrentValue = FCConvert.ToString(rsData.Get_Fields("BLSWorkSiteID"));
                //GroupHeader1.DataField = rsData.Get_Fields_String("BLSWorksiteID");
                //FC:FINAL:AM:#i2232 - have to use a data field for grouping
                this.Fields["Binder"].Value = strCurrentValue;

            }
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (gboolEnd)
			{
				txtTotal.Text = FCConvert.ToString(intTotal);
			}
			else
			{
				txtTotal.Text = FCConvert.ToString(intTotal - 1);
			}
			if (FCConvert.ToInt32(txtTotal.Text) < 0)
				txtTotal.Text = "0";
			intTotal = 1;
		}

        private void GroupHeader1_Format(object sender, EventArgs e)
        {
            txtGroup.Text = strCurrentValue;
            // If Not rsData.EndOfFile Then strCurrentValue = rsData.Fields("BLSWorkSiteID")
        }

        private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtGrandTotal.Text = FCConvert.ToString(lngGrandTotal);
		}

		

        private void rptBLSListing_DataInitialize(object sender, EventArgs e)
        {
            this.Fields.Add("Binder");
        }
    }
}
