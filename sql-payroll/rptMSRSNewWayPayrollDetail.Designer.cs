﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSNewWayPayrollDetail.
	/// </summary>
	partial class rptMSRSNewWayPayrollDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMSRSNewWayPayrollDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtSocialSecurityNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateOfHire = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFirstLastMiddle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeCodes = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPlanCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRemarkCodes = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsuranceCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsuranceLevel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsurancePremium = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateOfBirth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableCompensation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPlanStatusRateSched = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeContributions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPaybackOfExcessContributions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.MonthlyHoursorDays = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.FullTimeWorkWeekHoursDays = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPayRateCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWorkWeeksPerYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtRateOfPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFullTimeEquivalent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEffectiveBasicInsuranceRatePer1000 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPaidDates = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlan = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGrant = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCompensation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtActiveX = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRetiredX = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLifeInsPageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLifeInsGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEarnedCompGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeContributionsGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtSocialSecurityNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfHire)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLastMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemarkCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsuranceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsuranceLevel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsurancePremium)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfBirth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableCompensation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanStatusRateSched)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeContributions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaybackOfExcessContributions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MonthlyHoursorDays)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FullTimeWorkWeekHoursDays)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRateCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkWeeksPerYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateOfPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullTimeEquivalent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveBasicInsuranceRatePer1000)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCompensation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActiveX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetiredX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsPageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnedCompGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeContributionsGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape2,
            this.txtSocialSecurityNo,
            this.txtDateOfHire,
            this.txtFirstLastMiddle,
            this.txtEmployeeCodes,
            this.txtPlanCode,
            this.txtRemarkCodes,
            this.txtLifeInsuranceCode,
            this.txtLifeInsuranceLevel,
            this.txtLifeInsurancePremium,
            this.txtDateOfBirth,
            this.txtEarnableCompensation,
            this.txtPlanStatusRateSched,
            this.txtEmployeeContributions,
            this.txtPaybackOfExcessContributions,
            this.MonthlyHoursorDays,
            this.FullTimeWorkWeekHoursDays,
            this.Line19,
            this.Line18,
            this.Line17,
            this.Line16,
            this.Line15,
            this.Line14,
            this.txtPayRateCode,
            this.txtWorkWeeksPerYear,
            this.Line20,
            this.txtRateOfPay,
            this.txtFullTimeEquivalent,
            this.Line21});
			this.Detail.Height = 0.3333333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Shape2
			// 
			this.Shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Shape2.Height = 0.1666667F;
			this.Shape2.Left = 0F;
			this.Shape2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.1666667F;
			this.Shape2.Width = 7.9375F;
			// 
			// txtSocialSecurityNo
			// 
			this.txtSocialSecurityNo.Height = 0.1666667F;
			this.txtSocialSecurityNo.HyperLink = null;
			this.txtSocialSecurityNo.Left = 0F;
			this.txtSocialSecurityNo.Name = "txtSocialSecurityNo";
			this.txtSocialSecurityNo.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtSocialSecurityNo.Text = null;
			this.txtSocialSecurityNo.Top = 0F;
			this.txtSocialSecurityNo.Width = 1.0625F;
			// 
			// txtDateOfHire
			// 
			this.txtDateOfHire.Height = 0.1666667F;
			this.txtDateOfHire.HyperLink = null;
			this.txtDateOfHire.Left = 0F;
			this.txtDateOfHire.Name = "txtDateOfHire";
			this.txtDateOfHire.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtDateOfHire.Text = null;
			this.txtDateOfHire.Top = 0.15625F;
			this.txtDateOfHire.Visible = false;
			this.txtDateOfHire.Width = 1.0625F;
			// 
			// txtFirstLastMiddle
			// 
			this.txtFirstLastMiddle.Height = 0.1666667F;
			this.txtFirstLastMiddle.HyperLink = null;
			this.txtFirstLastMiddle.Left = 1.0625F;
			this.txtFirstLastMiddle.Name = "txtFirstLastMiddle";
			this.txtFirstLastMiddle.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtFirstLastMiddle.Text = null;
			this.txtFirstLastMiddle.Top = 0F;
			this.txtFirstLastMiddle.Width = 2.125F;
			// 
			// txtEmployeeCodes
			// 
			this.txtEmployeeCodes.Height = 0.1666667F;
			this.txtEmployeeCodes.HyperLink = null;
			this.txtEmployeeCodes.Left = 1.0625F;
			this.txtEmployeeCodes.Name = "txtEmployeeCodes";
			this.txtEmployeeCodes.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtEmployeeCodes.Text = null;
			this.txtEmployeeCodes.Top = 0.1666667F;
			this.txtEmployeeCodes.Width = 1F;
			// 
			// txtPlanCode
			// 
			this.txtPlanCode.Height = 0.1666667F;
			this.txtPlanCode.HyperLink = null;
			this.txtPlanCode.Left = 2.0625F;
			this.txtPlanCode.Name = "txtPlanCode";
			this.txtPlanCode.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtPlanCode.Text = null;
			this.txtPlanCode.Top = 0.1666667F;
			this.txtPlanCode.Width = 0.5625F;
			// 
			// txtRemarkCodes
			// 
			this.txtRemarkCodes.Height = 0.1666667F;
			this.txtRemarkCodes.HyperLink = null;
			this.txtRemarkCodes.Left = 2.625F;
			this.txtRemarkCodes.Name = "txtRemarkCodes";
			this.txtRemarkCodes.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtRemarkCodes.Text = null;
			this.txtRemarkCodes.Top = 0.1666667F;
			this.txtRemarkCodes.Width = 0.5625F;
			// 
			// txtLifeInsuranceCode
			// 
			this.txtLifeInsuranceCode.Height = 0.1666667F;
			this.txtLifeInsuranceCode.HyperLink = null;
			this.txtLifeInsuranceCode.Left = 3.1875F;
			this.txtLifeInsuranceCode.Name = "txtLifeInsuranceCode";
			this.txtLifeInsuranceCode.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtLifeInsuranceCode.Text = null;
			this.txtLifeInsuranceCode.Top = 0F;
			this.txtLifeInsuranceCode.Width = 0.5F;
			// 
			// txtLifeInsuranceLevel
			// 
			this.txtLifeInsuranceLevel.Height = 0.1666667F;
			this.txtLifeInsuranceLevel.HyperLink = null;
			this.txtLifeInsuranceLevel.Left = 3.1875F;
			this.txtLifeInsuranceLevel.Name = "txtLifeInsuranceLevel";
			this.txtLifeInsuranceLevel.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtLifeInsuranceLevel.Text = null;
			this.txtLifeInsuranceLevel.Top = 0.1666667F;
			this.txtLifeInsuranceLevel.Width = 0.5F;
			// 
			// txtLifeInsurancePremium
			// 
			this.txtLifeInsurancePremium.Height = 0.1666667F;
			this.txtLifeInsurancePremium.HyperLink = null;
			this.txtLifeInsurancePremium.Left = 3.6875F;
			this.txtLifeInsurancePremium.Name = "txtLifeInsurancePremium";
			this.txtLifeInsurancePremium.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtLifeInsurancePremium.Text = null;
			this.txtLifeInsurancePremium.Top = 0F;
			this.txtLifeInsurancePremium.Width = 0.625F;
			// 
			// txtDateOfBirth
			// 
			this.txtDateOfBirth.Height = 0.167F;
			this.txtDateOfBirth.HyperLink = null;
			this.txtDateOfBirth.Left = 3.658F;
			this.txtDateOfBirth.Name = "txtDateOfBirth";
			this.txtDateOfBirth.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtDateOfBirth.Text = null;
			this.txtDateOfBirth.Top = 0.167F;
			this.txtDateOfBirth.Width = 0.665F;
			// 
			// txtEarnableCompensation
			// 
			this.txtEarnableCompensation.Height = 0.1666667F;
			this.txtEarnableCompensation.HyperLink = null;
			this.txtEarnableCompensation.Left = 4.3125F;
			this.txtEarnableCompensation.Name = "txtEarnableCompensation";
			this.txtEarnableCompensation.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtEarnableCompensation.Text = null;
			this.txtEarnableCompensation.Top = 0F;
			this.txtEarnableCompensation.Width = 0.875F;
			// 
			// txtPlanStatusRateSched
			// 
			this.txtPlanStatusRateSched.Height = 0.1666667F;
			this.txtPlanStatusRateSched.HyperLink = null;
			this.txtPlanStatusRateSched.Left = 4.3125F;
			this.txtPlanStatusRateSched.Name = "txtPlanStatusRateSched";
			this.txtPlanStatusRateSched.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtPlanStatusRateSched.Text = null;
			this.txtPlanStatusRateSched.Top = 0.1666667F;
			this.txtPlanStatusRateSched.Width = 0.875F;
			// 
			// txtEmployeeContributions
			// 
			this.txtEmployeeContributions.Height = 0.1666667F;
			this.txtEmployeeContributions.HyperLink = null;
			this.txtEmployeeContributions.Left = 5.1875F;
			this.txtEmployeeContributions.Name = "txtEmployeeContributions";
			this.txtEmployeeContributions.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtEmployeeContributions.Text = null;
			this.txtEmployeeContributions.Top = 0F;
			this.txtEmployeeContributions.Width = 0.8125F;
			// 
			// txtPaybackOfExcessContributions
			// 
			this.txtPaybackOfExcessContributions.Height = 0.1666667F;
			this.txtPaybackOfExcessContributions.HyperLink = null;
			this.txtPaybackOfExcessContributions.Left = 5.1875F;
			this.txtPaybackOfExcessContributions.Name = "txtPaybackOfExcessContributions";
			this.txtPaybackOfExcessContributions.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.txtPaybackOfExcessContributions.Text = null;
			this.txtPaybackOfExcessContributions.Top = 0.1666667F;
			this.txtPaybackOfExcessContributions.Width = 0.8125F;
			// 
			// MonthlyHoursorDays
			// 
			this.MonthlyHoursorDays.Height = 0.1666667F;
			this.MonthlyHoursorDays.HyperLink = null;
			this.MonthlyHoursorDays.Left = 6F;
			this.MonthlyHoursorDays.Name = "MonthlyHoursorDays";
			this.MonthlyHoursorDays.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.MonthlyHoursorDays.Text = null;
			this.MonthlyHoursorDays.Top = 0F;
			this.MonthlyHoursorDays.Width = 0.8125F;
			// 
			// FullTimeWorkWeekHoursDays
			// 
			this.FullTimeWorkWeekHoursDays.Height = 0.1666667F;
			this.FullTimeWorkWeekHoursDays.HyperLink = null;
			this.FullTimeWorkWeekHoursDays.Left = 6F;
			this.FullTimeWorkWeekHoursDays.Name = "FullTimeWorkWeekHoursDays";
			this.FullTimeWorkWeekHoursDays.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.FullTimeWorkWeekHoursDays.Text = null;
			this.FullTimeWorkWeekHoursDays.Top = 0.1666667F;
			this.FullTimeWorkWeekHoursDays.Width = 0.8125F;
			// 
			// Line19
			// 
			this.Line19.Height = 0.3333333F;
			this.Line19.Left = 6F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 0F;
			this.Line19.Width = 0F;
			this.Line19.X1 = 6F;
			this.Line19.X2 = 6F;
			this.Line19.Y1 = 0F;
			this.Line19.Y2 = 0.3333333F;
			// 
			// Line18
			// 
			this.Line18.Height = 0.3333333F;
			this.Line18.Left = 5.1875F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 0F;
			this.Line18.Width = 0F;
			this.Line18.X1 = 5.1875F;
			this.Line18.X2 = 5.1875F;
			this.Line18.Y1 = 0F;
			this.Line18.Y2 = 0.3333333F;
			// 
			// Line17
			// 
			this.Line17.Height = 0.3333333F;
			this.Line17.Left = 4.3125F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 0F;
			this.Line17.Width = 0F;
			this.Line17.X1 = 4.3125F;
			this.Line17.X2 = 4.3125F;
			this.Line17.Y1 = 0F;
			this.Line17.Y2 = 0.3333333F;
			// 
			// Line16
			// 
			this.Line16.Height = 0.3333333F;
			this.Line16.Left = 3.6875F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 0F;
			this.Line16.Width = 0F;
			this.Line16.X1 = 3.6875F;
			this.Line16.X2 = 3.6875F;
			this.Line16.Y1 = 0F;
			this.Line16.Y2 = 0.3333333F;
			// 
			// Line15
			// 
			this.Line15.Height = 0.3333333F;
			this.Line15.Left = 3.1875F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0F;
			this.Line15.Width = 0F;
			this.Line15.X1 = 3.1875F;
			this.Line15.X2 = 3.1875F;
			this.Line15.Y1 = 0F;
			this.Line15.Y2 = 0.3333333F;
			// 
			// Line14
			// 
			this.Line14.Height = 0.3333333F;
			this.Line14.Left = 1.0625F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 1.0625F;
			this.Line14.X2 = 1.0625F;
			this.Line14.Y1 = 0F;
			this.Line14.Y2 = 0.3333333F;
			// 
			// txtPayRateCode
			// 
			this.txtPayRateCode.Height = 0.1666667F;
			this.txtPayRateCode.HyperLink = null;
			this.txtPayRateCode.Left = 6.8125F;
			this.txtPayRateCode.Name = "txtPayRateCode";
			this.txtPayRateCode.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtPayRateCode.Text = null;
			this.txtPayRateCode.Top = 0F;
			this.txtPayRateCode.Width = 0.3125F;
			// 
			// txtWorkWeeksPerYear
			// 
			this.txtWorkWeeksPerYear.Height = 0.1666667F;
			this.txtWorkWeeksPerYear.HyperLink = null;
			this.txtWorkWeeksPerYear.Left = 6.8125F;
			this.txtWorkWeeksPerYear.Name = "txtWorkWeeksPerYear";
			this.txtWorkWeeksPerYear.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtWorkWeeksPerYear.Text = null;
			this.txtWorkWeeksPerYear.Top = 0.1666667F;
			this.txtWorkWeeksPerYear.Width = 0.3125F;
			// 
			// Line20
			// 
			this.Line20.Height = 0.3333333F;
			this.Line20.Left = 6.8125F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 0F;
			this.Line20.Width = 0F;
			this.Line20.X1 = 6.8125F;
			this.Line20.X2 = 6.8125F;
			this.Line20.Y1 = 0F;
			this.Line20.Y2 = 0.3333333F;
			// 
			// txtRateOfPay
			// 
			this.txtRateOfPay.Height = 0.1666667F;
			this.txtRateOfPay.HyperLink = null;
			this.txtRateOfPay.Left = 7.125F;
			this.txtRateOfPay.Name = "txtRateOfPay";
			this.txtRateOfPay.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtRateOfPay.Text = null;
			this.txtRateOfPay.Top = 0F;
			this.txtRateOfPay.Width = 0.8125F;
			// 
			// txtFullTimeEquivalent
			// 
			this.txtFullTimeEquivalent.Height = 0.1666667F;
			this.txtFullTimeEquivalent.HyperLink = null;
			this.txtFullTimeEquivalent.Left = 7.125F;
			this.txtFullTimeEquivalent.Name = "txtFullTimeEquivalent";
			this.txtFullTimeEquivalent.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtFullTimeEquivalent.Text = null;
			this.txtFullTimeEquivalent.Top = 0.1666667F;
			this.txtFullTimeEquivalent.Width = 0.8125F;
			// 
			// Line21
			// 
			this.Line21.Height = 0.3333333F;
			this.Line21.Left = 7.125F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 0F;
			this.Line21.Width = 0F;
			this.Line21.X1 = 7.125F;
			this.Line21.X2 = 7.125F;
			this.Line21.Y1 = 0F;
			this.Line21.Y2 = 0.3333333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.txtTitle1,
            this.Label2,
            this.txtDate,
            this.Label4,
            this.txtPage,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Line1,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.txtEffectiveBasicInsuranceRatePer1000,
            this.Label15,
            this.Label16,
            this.txtPaidDates,
            this.txtEmployerName,
            this.txtEmployerCode,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Label35,
            this.Label36,
            this.Label37,
            this.lblPlan,
            this.Label42,
            this.Label43,
            this.Label44,
            this.lblGrant,
            this.lblCompensation,
            this.Label47,
            this.Label48,
            this.Label49,
            this.Label50,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line9,
            this.Line10,
            this.Line11,
            this.Line12,
            this.Line13,
            this.txtActiveX,
            this.txtRetiredX,
            this.Label64});
			this.PageHeader.Height = 2F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Shape1.Height = 0.40625F;
			this.Shape1.Left = 0F;
			this.Shape1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 1.59375F;
			this.Shape1.Width = 7.9375F;
			// 
			// txtTitle1
			// 
			this.txtTitle1.Height = 0.1875F;
			this.txtTitle1.HyperLink = null;
			this.txtTitle1.Left = 2F;
			this.txtTitle1.Name = "txtTitle1";
			this.txtTitle1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle1.Text = "PARTICIPATING LOCAL DISTRICT";
			this.txtTitle1.Top = 0F;
			this.txtTitle1.Width = 4F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label2.Text = "MONTHLY PAYROLL DETAIL REPORT";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 4F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.18F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "";
			this.txtDate.Text = "RUN DATE ";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.563F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; text-align: center";
			this.Label4.Text = "ATTACH TO MONTHLY PAYROLL SUMMARY REPORT";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 4F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.18F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "";
			this.txtPage.Text = "PAGE NO.";
			this.txtPage.Top = 0.187F;
			this.txtPage.Width = 1.563F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 9pt";
			this.Label6.Text = "Maine Public Employee Retirement System";
			this.Label6.Top = 0F;
			this.Label6.Width = 2.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 9pt";
			this.Label7.Text = "46 STATE HOUSE STATION";
			this.Label7.Top = 0.15625F;
			this.Label7.Width = 2.25F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 9pt";
			this.Label8.Text = "AUGUSTA, MAINE 04333-0046";
			this.Label8.Top = 0.3125F;
			this.Label8.Width = 2.25F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 9pt";
			this.Label9.Text = "(207)512-3100 (OR) 1-800-451-9800";
			this.Label9.Top = 0.46875F;
			this.Label9.Width = 2.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.65625F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 0.65625F;
			this.Line1.Y2 = 0.65625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.17F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "EMPLOYER CODE";
			this.Label10.Top = 0.78125F;
			this.Label10.Width = 1.438F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.17F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "";
			this.Label11.Text = "EMPLOYER NAME";
			this.Label11.Top = 0.9375F;
			this.Label11.Width = 1.438F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.17F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "";
			this.Label12.Text = "PAID DATES ENDING";
			this.Label12.Top = 1.09375F;
			this.Label12.Width = 1.438F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.802083F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt";
			this.Label13.Text = "EFFECTIVE BASIC INSURANCE RATE PER $1,000 MONTHLY";
			this.Label13.Top = 0.78125F;
			this.Label13.Width = 3.197917F;
			// 
			// txtEffectiveBasicInsuranceRatePer1000
			// 
			this.txtEffectiveBasicInsuranceRatePer1000.Height = 0.19F;
			this.txtEffectiveBasicInsuranceRatePer1000.HyperLink = null;
			this.txtEffectiveBasicInsuranceRatePer1000.Left = 7.0625F;
			this.txtEffectiveBasicInsuranceRatePer1000.Name = "txtEffectiveBasicInsuranceRatePer1000";
			this.txtEffectiveBasicInsuranceRatePer1000.Style = "font-size: 8.5pt";
			this.txtEffectiveBasicInsuranceRatePer1000.Text = "$";
			this.txtEffectiveBasicInsuranceRatePer1000.Top = 0.78125F;
			this.txtEffectiveBasicInsuranceRatePer1000.Width = 0.7916667F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.4375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt";
			this.Label15.Text = "ACTIVE";
			this.Label15.Top = 1.09375F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 6.4375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt";
			this.Label16.Text = "RETIRED";
			this.Label16.Top = 1.09375F;
			this.Label16.Width = 0.5625F;
			// 
			// txtPaidDates
			// 
			this.txtPaidDates.Height = 0.17F;
			this.txtPaidDates.HyperLink = null;
			this.txtPaidDates.Left = 1.4375F;
			this.txtPaidDates.Name = "txtPaidDates";
			this.txtPaidDates.Style = "";
			this.txtPaidDates.Text = null;
			this.txtPaidDates.Top = 1.09375F;
			this.txtPaidDates.Width = 3.938F;
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.Height = 0.17F;
			this.txtEmployerName.HyperLink = null;
			this.txtEmployerName.Left = 1.4375F;
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Style = "";
			this.txtEmployerName.Text = null;
			this.txtEmployerName.Top = 0.9375F;
			this.txtEmployerName.Width = 3.438F;
			// 
			// txtEmployerCode
			// 
			this.txtEmployerCode.Height = 0.17F;
			this.txtEmployerCode.HyperLink = null;
			this.txtEmployerCode.Left = 1.4375F;
			this.txtEmployerCode.Name = "txtEmployerCode";
			this.txtEmployerCode.Style = "";
			this.txtEmployerCode.Text = null;
			this.txtEmployerCode.Top = 0.78125F;
			this.txtEmployerCode.Width = 1.563F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.125F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label20.Text = "SOCIAL SECURITY NO.";
			this.Label20.Top = 1.375F;
			this.Label20.Width = 1.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.125F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.0625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label21.Text = "ALPHABETICAL ORDER";
			this.Label21.Top = 1.3125F;
			this.Label21.Width = 2.125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.125F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 1.0625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label22.Text = "(NAME) LAST             FIRST               MIDDLE";
			this.Label22.Top = 1.4375F;
			this.Label22.Width = 2.125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.125F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label23.Text = "LIFE";
			this.Label23.Top = 1.3125F;
			this.Label23.Width = 0.5F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.125F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.1875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label24.Text = "INS. CODE";
			this.Label24.Top = 1.4375F;
			this.Label24.Width = 0.5F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.125F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3.6875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label25.Text = "LIFE";
			this.Label25.Top = 1.3125F;
			this.Label25.Width = 0.625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.125F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.6875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label26.Text = "INS. PREM.";
			this.Label26.Top = 1.4375F;
			this.Label26.Width = 0.625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.125F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 4.3125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label27.Text = "EARNABLE";
			this.Label27.Top = 1.3125F;
			this.Label27.Width = 0.875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.125F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 4.3125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label28.Text = "COMPENSATION";
			this.Label28.Top = 1.4375F;
			this.Label28.Width = 0.875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.125F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.1875F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label29.Text = "EMPLOYEE";
			this.Label29.Top = 1.3125F;
			this.Label29.Width = 0.8125F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.125F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.1875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label30.Text = "CONTRIBUTIONS";
			this.Label30.Top = 1.4375F;
			this.Label30.Width = 0.8125F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.125F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 6F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label31.Text = "MONTHLY";
			this.Label31.Top = 1.3125F;
			this.Label31.Width = 0.8125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.125F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 6F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label32.Text = "HOURS OR DAYS";
			this.Label32.Top = 1.4375F;
			this.Label32.Width = 0.8125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.3125F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 6.8125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 6pt; text-align: center; ddo-char-set: 0";
			this.Label33.Text = "PAY RATE CODE";
			this.Label33.Top = 1.28125F;
			this.Label33.Width = 0.3125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.125F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 7.125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label34.Text = "RATE OF PAY";
			this.Label34.Top = 1.375F;
			this.Label34.Width = 0.8125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.125F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 1.0625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label35.Text = "EMPLOYEE CODES";
			this.Label35.Top = 1.65625F;
			this.Label35.Width = 1.0625F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.125F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label36.Text = "DATE OF HIRE";
			this.Label36.Top = 1.71875F;
			this.Label36.Visible = false;
			this.Label36.Width = 1.0625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.125F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 1.0625F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label37.Text = "STATUS - POSITION";
			this.Label37.Top = 1.78125F;
			this.Label37.Width = 1.0625F;
			// 
			// lblPlan
			// 
			this.lblPlan.Height = 0.375F;
			this.lblPlan.HyperLink = null;
			this.lblPlan.Left = 2.25F;
			this.lblPlan.Name = "lblPlan";
			this.lblPlan.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.lblPlan.Text = "PLAN CODE /  PAYBACK CODE /  GLI SCH CODE";
			this.lblPlan.Top = 1.597222F;
			this.lblPlan.Width = 0.75F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.375F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.25F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label42.Text = "LIFE INS LEVEL";
			this.Label42.Top = 1.59375F;
			this.Label42.Width = 0.375F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.125F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 3.6875F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label43.Text = "DATE OF";
			this.Label43.Top = 1.65625F;
			this.Label43.Width = 0.625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.125F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 3.6875F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label44.Text = "BIRTH";
			this.Label44.Top = 1.78125F;
			this.Label44.Width = 0.625F;
			// 
			// lblGrant
			// 
			this.lblGrant.Height = 0.125F;
			this.lblGrant.HyperLink = null;
			this.lblGrant.Left = 4.3125F;
			this.lblGrant.Name = "lblGrant";
			this.lblGrant.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.lblGrant.Text = "PLAN STATUS /";
			this.lblGrant.Top = 1.65625F;
			this.lblGrant.Width = 0.875F;
			// 
			// lblCompensation
			// 
			this.lblCompensation.Height = 0.125F;
			this.lblCompensation.HyperLink = null;
			this.lblCompensation.Left = 4.3125F;
			this.lblCompensation.Name = "lblCompensation";
			this.lblCompensation.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.lblCompensation.Text = "RATE SCHEDULE";
			this.lblCompensation.Top = 1.78125F;
			this.lblCompensation.Width = 0.875F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.354F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 5.1875F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label47.Text = "PAYBACK OR EXCESS CONTRIBUTIONS";
			this.Label47.Top = 1.59375F;
			this.Label47.Width = 0.813F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.354F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 6F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label48.Text = "SERVICE FTE   WORK WEEK DAYS/HOURS";
			this.Label48.Top = 1.59375F;
			this.Label48.Width = 0.813F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.353F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 6.822917F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Tahoma\'; font-size: 4pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 0";
			this.Label49.Text = "SERVICE FTE WORK WEEKS PER YEAR";
			this.Label49.Top = 1.635417F;
			this.Label49.Width = 0.323F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.353F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 7.166667F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label50.Text = "FTE CONTRACT / STIPEND";
			this.Label50.Top = 1.611111F;
			this.Label50.Width = 0.667F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2F;
			this.Line3.Width = 7.9375F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.9375F;
			this.Line3.Y1 = 2F;
			this.Line3.Y2 = 2F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.28125F;
			this.Line4.Width = 7.9375F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.9375F;
			this.Line4.Y1 = 1.28125F;
			this.Line4.Y2 = 1.28125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.59375F;
			this.Line5.Width = 7.9375F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.9375F;
			this.Line5.Y1 = 1.59375F;
			this.Line5.Y2 = 1.59375F;
			// 
			// Line6
			// 
			this.Line6.Height = 0.71875F;
			this.Line6.Left = 1.0625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.28125F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 1.0625F;
			this.Line6.X2 = 1.0625F;
			this.Line6.Y1 = 1.28125F;
			this.Line6.Y2 = 2F;
			// 
			// Line7
			// 
			this.Line7.Height = 0.71875F;
			this.Line7.Left = 3.1875F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.28125F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 3.1875F;
			this.Line7.X2 = 3.1875F;
			this.Line7.Y1 = 1.28125F;
			this.Line7.Y2 = 2F;
			// 
			// Line8
			// 
			this.Line8.Height = 0.71875F;
			this.Line8.Left = 3.6875F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.28125F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 3.6875F;
			this.Line8.X2 = 3.6875F;
			this.Line8.Y1 = 1.28125F;
			this.Line8.Y2 = 2F;
			// 
			// Line9
			// 
			this.Line9.Height = 0.71875F;
			this.Line9.Left = 4.3125F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1.28125F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 4.3125F;
			this.Line9.X2 = 4.3125F;
			this.Line9.Y1 = 1.28125F;
			this.Line9.Y2 = 2F;
			// 
			// Line10
			// 
			this.Line10.Height = 0.71875F;
			this.Line10.Left = 5.1875F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 1.28125F;
			this.Line10.Width = 0F;
			this.Line10.X1 = 5.1875F;
			this.Line10.X2 = 5.1875F;
			this.Line10.Y1 = 1.28125F;
			this.Line10.Y2 = 2F;
			// 
			// Line11
			// 
			this.Line11.Height = 0.71875F;
			this.Line11.Left = 6F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 1.28125F;
			this.Line11.Width = 0F;
			this.Line11.X1 = 6F;
			this.Line11.X2 = 6F;
			this.Line11.Y1 = 1.28125F;
			this.Line11.Y2 = 2F;
			// 
			// Line12
			// 
			this.Line12.Height = 0.71875F;
			this.Line12.Left = 6.8125F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 1.28125F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 6.8125F;
			this.Line12.X2 = 6.8125F;
			this.Line12.Y1 = 1.28125F;
			this.Line12.Y2 = 2F;
			// 
			// Line13
			// 
			this.Line13.Height = 0.71875F;
			this.Line13.Left = 7.125F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 1.28125F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 7.125F;
			this.Line13.X2 = 7.125F;
			this.Line13.Y1 = 1.28125F;
			this.Line13.Y2 = 2F;
			// 
			// txtActiveX
			// 
			this.txtActiveX.Height = 0.19F;
			this.txtActiveX.HyperLink = null;
			this.txtActiveX.Left = 6F;
			this.txtActiveX.Name = "txtActiveX";
			this.txtActiveX.Style = "font-size: 8.5pt";
			this.txtActiveX.Text = "XX";
			this.txtActiveX.Top = 1.09375F;
			this.txtActiveX.Width = 0.3125F;
			// 
			// txtRetiredX
			// 
			this.txtRetiredX.Height = 0.19F;
			this.txtRetiredX.HyperLink = null;
			this.txtRetiredX.Left = 7F;
			this.txtRetiredX.Name = "txtRetiredX";
			this.txtRetiredX.Style = "font-size: 8.5pt";
			this.txtRetiredX.Text = "XX";
			this.txtRetiredX.Top = 1.09375F;
			this.txtRetiredX.Visible = false;
			this.txtRetiredX.Width = 0.3125F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.19F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 4.927083F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-size: 8.5pt";
			this.Label64.Text = "ARE THESE INSUREDS";
			this.Label64.Top = 0.9375F;
			this.Label64.Width = 1.260417F;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line22,
            this.Label51,
            this.Line26,
            this.Line23,
            this.Label52,
            this.Label53,
            this.Line24,
            this.Line25,
            this.Line27,
            this.Label54,
            this.Label55,
            this.Line28,
            this.Label56,
            this.Line29,
            this.Label57,
            this.Line30,
            this.Label58,
            this.Label59,
            this.txtLifeInsPageTotal,
            this.Field1,
            this.Field2,
            this.txtLifeInsGrandTotal,
            this.txtEarnedCompGrandTotal,
            this.txtEmployeeContributionsGrandTotal,
            this.Label62,
            this.Label63});
			this.PageFooter.Height = 0.7708333F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 0F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 0F;
			this.Line22.Width = 7.9375F;
			this.Line22.X1 = 0F;
			this.Line22.X2 = 7.9375F;
			this.Line22.Y1 = 0F;
			this.Line22.Y2 = 0F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 2.8125F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 9pt; text-align: center";
			this.Label51.Text = "GRAND TOTALS";
			this.Label51.Top = 0.46875F;
			this.Label51.Width = 1.125F;
			// 
			// Line26
			// 
			this.Line26.Height = 0.75F;
			this.Line26.Left = 4F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 0F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 4F;
			this.Line26.X2 = 4F;
			this.Line26.Y1 = 0F;
			this.Line26.Y2 = 0.75F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 0F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 0.46875F;
			this.Line23.Width = 7.9375F;
			this.Line23.X1 = 0F;
			this.Line23.X2 = 7.9375F;
			this.Line23.Y1 = 0.46875F;
			this.Line23.Y2 = 0.46875F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.125F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 2.9375F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label52.Text = "LAST PAGE";
			this.Label52.Top = 0.625F;
			this.Label52.Width = 0.8125F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 2.75F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 9pt; text-align: center";
			this.Label53.Text = "TOTALS THIS PAGE";
			this.Label53.Top = 0.21875F;
			this.Label53.Width = 1.25F;
			// 
			// Line24
			// 
			this.Line24.Height = 0.75F;
			this.Line24.Left = 2.75F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 0F;
			this.Line24.Width = 0F;
			this.Line24.X1 = 2.75F;
			this.Line24.X2 = 2.75F;
			this.Line24.Y1 = 0F;
			this.Line24.Y2 = 0.75F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 2.75F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 0.75F;
			this.Line25.Width = 5.1875F;
			this.Line25.X1 = 2.75F;
			this.Line25.X2 = 7.9375F;
			this.Line25.Y1 = 0.75F;
			this.Line25.Y2 = 0.75F;
			// 
			// Line27
			// 
			this.Line27.Height = 0F;
			this.Line27.Left = 2.75F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 0.15625F;
			this.Line27.Width = 5.1875F;
			this.Line27.X1 = 2.75F;
			this.Line27.X2 = 7.9375F;
			this.Line27.Y1 = 0.15625F;
			this.Line27.Y2 = 0.15625F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.125F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 4.125F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label54.Text = "LIFE INS. PREMIUM";
			this.Label54.Top = 0.03125F;
			this.Label54.Width = 1F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.125F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 5.375F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label55.Text = "EARN. COMPENSATION";
			this.Label55.Top = 0.03125F;
			this.Label55.Width = 1.1875F;
			// 
			// Line28
			// 
			this.Line28.Height = 0.75F;
			this.Line28.Left = 5.3125F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 0F;
			this.Line28.Width = 0F;
			this.Line28.X1 = 5.3125F;
			this.Line28.X2 = 5.3125F;
			this.Line28.Y1 = 0F;
			this.Line28.Y2 = 0.75F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.125F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 6.625F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label56.Text = "EMPLOYEE CONTRIBUTIONS";
			this.Label56.Top = 0.03125F;
			this.Label56.Width = 1.3125F;
			// 
			// Line29
			// 
			this.Line29.Height = 0.75F;
			this.Line29.Left = 6.625F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 0F;
			this.Line29.Width = 0F;
			this.Line29.X1 = 6.625F;
			this.Line29.X2 = 6.625F;
			this.Line29.Y1 = 0F;
			this.Line29.Y2 = 0.75F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.125F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0.875F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center; ddo-char-set: 0";
			this.Label57.Text = "PAY RATE CODES";
			this.Label57.Top = 0F;
			this.Label57.Width = 1F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 0F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 0.125F;
			this.Line30.Width = 2.75F;
			this.Line30.X1 = 0F;
			this.Line30.X2 = 2.75F;
			this.Line30.Y1 = 0.125F;
			this.Line30.Y2 = 0.125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.125F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 1F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label58.Text = "D - DAILY";
			this.Label58.Top = 0.1875F;
			this.Label58.Width = 1F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.125F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 1F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label59.Text = "H - HOURLY";
			this.Label59.Top = 0.3125F;
			this.Label59.Width = 1F;
			// 
			// txtLifeInsPageTotal
			// 
			this.txtLifeInsPageTotal.DataField = "fldlifeinspremium";
			this.txtLifeInsPageTotal.Height = 0.1875F;
			this.txtLifeInsPageTotal.Left = 4.0625F;
			this.txtLifeInsPageTotal.Name = "txtLifeInsPageTotal";
			this.txtLifeInsPageTotal.OutputFormat = resources.GetString("txtLifeInsPageTotal.OutputFormat");
			this.txtLifeInsPageTotal.Style = "font-size: 10pt; text-align: right";
			this.txtLifeInsPageTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtLifeInsPageTotal.Text = null;
			this.txtLifeInsPageTotal.Top = 0.21875F;
			this.txtLifeInsPageTotal.Width = 1.25F;
			// 
			// Field1
			// 
			this.Field1.DataField = "fldEarnedCompensation";
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 5.375F;
			this.Field1.Name = "Field1";
			this.Field1.OutputFormat = resources.GetString("Field1.OutputFormat");
			this.Field1.Style = "font-size: 10pt; text-align: right";
			this.Field1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.Field1.Text = null;
			this.Field1.Top = 0.21875F;
			this.Field1.Width = 1.25F;
			// 
			// Field2
			// 
			this.Field2.DataField = "fldEmployeeContributions";
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 6.6875F;
			this.Field2.Name = "Field2";
			this.Field2.OutputFormat = resources.GetString("Field2.OutputFormat");
			this.Field2.Style = "font-size: 10pt; text-align: right";
			this.Field2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.Field2.Text = null;
			this.Field2.Top = 0.21875F;
			this.Field2.Width = 1.25F;
			// 
			// txtLifeInsGrandTotal
			// 
			this.txtLifeInsGrandTotal.Height = 0.1875F;
			this.txtLifeInsGrandTotal.Left = 4.0625F;
			this.txtLifeInsGrandTotal.Name = "txtLifeInsGrandTotal";
			this.txtLifeInsGrandTotal.OutputFormat = resources.GetString("txtLifeInsGrandTotal.OutputFormat");
			this.txtLifeInsGrandTotal.Style = "font-size: 10pt; text-align: right";
			this.txtLifeInsGrandTotal.Text = null;
			this.txtLifeInsGrandTotal.Top = 0.5F;
			this.txtLifeInsGrandTotal.Width = 1.25F;
			// 
			// txtEarnedCompGrandTotal
			// 
			this.txtEarnedCompGrandTotal.Height = 0.1875F;
			this.txtEarnedCompGrandTotal.Left = 5.375F;
			this.txtEarnedCompGrandTotal.Name = "txtEarnedCompGrandTotal";
			this.txtEarnedCompGrandTotal.OutputFormat = resources.GetString("txtEarnedCompGrandTotal.OutputFormat");
			this.txtEarnedCompGrandTotal.Style = "font-size: 10pt; text-align: right";
			this.txtEarnedCompGrandTotal.Text = null;
			this.txtEarnedCompGrandTotal.Top = 0.5F;
			this.txtEarnedCompGrandTotal.Width = 1.25F;
			// 
			// txtEmployeeContributionsGrandTotal
			// 
			this.txtEmployeeContributionsGrandTotal.Height = 0.1875F;
			this.txtEmployeeContributionsGrandTotal.Left = 6.6875F;
			this.txtEmployeeContributionsGrandTotal.Name = "txtEmployeeContributionsGrandTotal";
			this.txtEmployeeContributionsGrandTotal.OutputFormat = resources.GetString("txtEmployeeContributionsGrandTotal.OutputFormat");
			this.txtEmployeeContributionsGrandTotal.Style = "font-size: 10pt; text-align: right";
			this.txtEmployeeContributionsGrandTotal.Text = null;
			this.txtEmployeeContributionsGrandTotal.Top = 0.5F;
			this.txtEmployeeContributionsGrandTotal.Width = 1.25F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.125F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 0.0625F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label62.Text = "MSRS AC1";
			this.Label62.Top = 0.5F;
			this.Label62.Width = 0.5625F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.125F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 0.0625F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: left; ddo-char-set: 0";
			this.Label63.Text = "REV. 12/98";
			this.Label63.Top = 0.625F;
			this.Label63.Width = 0.5625F;
			// 
			// rptMSRSNewWayPayrollDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.9375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtSocialSecurityNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfHire)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLastMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemarkCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsuranceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsuranceLevel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsurancePremium)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfBirth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableCompensation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanStatusRateSched)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeContributions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaybackOfExcessContributions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MonthlyHoursorDays)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FullTimeWorkWeekHoursDays)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRateCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkWeeksPerYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRateOfPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullTimeEquivalent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveBasicInsuranceRatePer1000)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCompensation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActiveX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetiredX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsPageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLifeInsGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnedCompGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeContributionsGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSocialSecurityNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDateOfHire;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFirstLastMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeCodes;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlanCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRemarkCodes;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtLifeInsuranceCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtLifeInsuranceLevel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtLifeInsurancePremium;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDateOfBirth;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableCompensation;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlanStatusRateSched;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeContributions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPaybackOfExcessContributions;
		private GrapeCity.ActiveReports.SectionReportModel.Label MonthlyHoursorDays;
		private GrapeCity.ActiveReports.SectionReportModel.Label FullTimeWorkWeekHoursDays;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRateCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtWorkWeeksPerYear;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRateOfPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFullTimeEquivalent;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEffectiveBasicInsuranceRatePer1000;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPaidDates;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlan;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGrant;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCompensation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtActiveX;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRetiredX;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLifeInsPageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLifeInsGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEarnedCompGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeContributionsGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
	}
}
