//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	public partial class frmEmployeeListing : BaseForm
	{
		public frmEmployeeListing()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeeListing InstancePtr
		{
			get
			{
				return (frmEmployeeListing)Sys.GetInstance(typeof(frmEmployeeListing));
			}
		}

		protected frmEmployeeListing _InstancePtr = null;
		//=========================================================
		// vbPorter upgrade warning: intGridColumn As int	OnWriteFCConvert.ToInt32(
		private int intGridColumn;
		private bool boolASC;
		private bool boolGridSort;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;
		private clsDRWrapper rsEmpPerm = new clsDRWrapper();
		private bool boolHasPerms;
		public bool FromDataEntry = false;

		private void frmEmployeeListing_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmEmployeeListing_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployeeListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmployeeListing properties;
			//frmEmployeeListing.ScaleWidth	= 9105;
			//frmEmployeeListing.ScaleHeight	= 7395;
			//frmEmployeeListing.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int lngID;
				if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county")
				{
                    //mnuImportTimeClock.Visible = true;
                    cmdImportTimeClock.Visible = true;
                }
				else
				{
					//mnuImportTimeClock.Visible = false;
                    cmdImportTimeClock.Visible = false;
                }
				if (modCoreysSweeterCode.Statics.gboolTimeClockPlus)
				{
					//mnuImportFromTimeClockPlus.Visible = true;
                    cmdImportFromTimeClockPlus.Visible = true;
                }
				else
				{
					//mnuImportFromTimeClockPlus.Visible = false;
                    cmdImportFromTimeClockPlus.Visible = false;
                }
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				// vsElasticLight1.Enabled = True
				rsEmpPerm.OpenRecordset("select * from payrollpermissions where [Userid] = " + FCConvert.ToString(lngID), "twpy0000.vb1");
				if (rsEmpPerm.EndOfFile())
				{
					boolHasPerms = true;
				}
				else
				{
					boolHasPerms = false;
				}
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				vsGrid.FixedCols = 0;
				//FC:FINAL:SBE - in original application header style is not applied
                //vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, vsGrid.Rows - 1, vsGrid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 6, Color.White);
				//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 6, modGlobalConstants.Statics.TRIOCOLORBLUE);
				//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
				vsGrid.ColHidden(0, true);
				lblPayRun.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				boolGridSort = false;
				LoadProcessGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ReloadAll()
		{
			int x;
			int lngRow;
			clsDRWrapper rsData = new clsDRWrapper();
			bool boolDone;
			string[] strEntryDone = null;
			double dblTotal;
			clsDRWrapper rsSum = new clsDRWrapper();
			int intArrayCounter;
			double dblHours = 0;
			string strEmp = "";
			dblTotal = 0;
			rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
			}
			else
			{
				boolDone = FCConvert.ToBoolean(rsData.Get_Fields("DataEntry"));
				strEntryDone = Strings.Split(FCConvert.ToString(rsData.Get_Fields("Description")), ";", -1, CompareConstants.vbBinaryCompare);
			}
			for (lngRow = 1; lngRow <= vsGrid.Rows - 1; lngRow++)
			{
				strEmp = vsGrid.TextMatrix(lngRow, 2);
				// same employee
				rsData.OpenRecordset("Select tblFrequencyCodes.*, tblEmployeeMaster.* from tblEmployeeMaster INNER JOIN tblFrequencyCodes ON tblEmployeeMaster.FreqCodeID = tblFrequencyCodes.ID Where tblEmployeeMaster.employeenumber = '" + strEmp + "'", "twpy0000.vb1");
				if (!rsData.EndOfFile())
				{
					// If boolDone Then
					// .TextMatrix(lngRow, 1) = True
					// Else
					// .TextMatrix(lngRow, 1) = False
					// For intArrayCounter = 0 To UBound(strEntryDone)
					// If Trim(strEntryDone(intArrayCounter)) = Trim(rsData.Fields("EmployeeNumber")) Then
					// .TextMatrix(lngRow, 1) = True
					// Exit For
					// Else
					// .TextMatrix(lngRow, 1) = False
					// End If
					// Next
					// End If
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("dataentrydone")))
					{
						vsGrid.TextMatrix(lngRow, 1, FCConvert.ToString(true));
					}
					else
					{
						vsGrid.TextMatrix(lngRow, 1, FCConvert.ToString(false));
					}
					if (modGlobalRoutines.ApplyThisEmployee(rsData.Get_Fields("FreqCodeID"), rsData.Get_Fields("EMPLOYEENUMBER"), "MAIN", true))
					{
						dblHours = 0;
						rsSum.OpenRecordset("SELECT * FROM tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID WHERE tblpayrolldistribution.employeenumber = '" + rsData.Get_Fields("EmployeeNumber") + "' and (((tblPayrollDistribution.AccountCode)<>'1') AND ((tblPayCategories.Type)='Hours')) ");
						while (!rsSum.EndOfFile())
						{
							// still have to check the distributions frequency
							if (Conversion.Val(rsSum.Get_Fields("accountcode")) != 13)
							{
								dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
							}
							else
							{
								if (modGlobalRoutines.Statics.ggboolBiWeekly)
								{
									if (modGlobalRoutines.Statics.ggintWeekNumber < 3)
									{
										dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
									}
								}
								else
								{
									if (modGlobalRoutines.Statics.ggintWeekNumber < 5)
									{
										dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
									}
								}
							}
							rsSum.MoveNext();
						}
						vsGrid.TextMatrix(lngRow, 6, FCConvert.ToString(dblHours));
					}
				}
				dblTotal += Conversion.Val(vsGrid.TextMatrix(lngRow, 6));
			}
			// lngRow
			txtTotal.Text = FCConvert.ToString(dblTotal);
		}

		private void Reload(string strList)
		{
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngRow;
			clsDRWrapper rsData = new clsDRWrapper();
			bool boolDone;
			string[] strEntryDone = null;
			double dblTotal;
			clsDRWrapper rsSum = new clsDRWrapper();
			int intArrayCounter;
			double dblHours = 0;
			dblTotal = 0;
			if (strList != string.Empty)
			{
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
				}
				else
				{
					boolDone = FCConvert.ToBoolean(rsData.Get_Fields("DataEntry"));
					strEntryDone = Strings.Split(FCConvert.ToString(rsData.Get_Fields("Description")), ";", -1, CompareConstants.vbBinaryCompare);
				}
				strAry = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
				for (lngRow = 1; lngRow <= vsGrid.Rows - 1; lngRow++)
				{
					for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
					{
						if (fecherFoundation.Strings.Trim(strAry[x]) != string.Empty)
						{
							if (strAry[x] == vsGrid.TextMatrix(lngRow, 2))
							{
								// same employee
								rsData.OpenRecordset("Select tblFrequencyCodes.*, tblEmployeeMaster.* from tblEmployeeMaster INNER JOIN tblFrequencyCodes ON tblEmployeeMaster.FreqCodeID = tblFrequencyCodes.ID Where tblEmployeeMaster.employeenumber = '" + strAry[x] + "'", "twpy0000.vb1");
								if (!rsData.EndOfFile())
								{
									// If boolDone Then
									// .TextMatrix(lngRow, 1) = True
									// Else
									// .TextMatrix(lngRow, 1) = False
									// For intArrayCounter = 0 To UBound(strEntryDone)
									// If Trim(strEntryDone(intArrayCounter)) = Trim(rsData.Fields("EmployeeNumber")) Then
									// .TextMatrix(lngRow, 1) = True
									// Exit For
									// Else
									// .TextMatrix(lngRow, 1) = False
									// End If
									// Next
									// End If
									if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("dataentrydone")))
									{
										vsGrid.TextMatrix(lngRow, 1, FCConvert.ToString(true));
									}
									else
									{
										vsGrid.TextMatrix(lngRow, 1, FCConvert.ToString(false));
									}
									if (modGlobalRoutines.ApplyThisEmployee(rsData.Get_Fields("FreqCodeID"), rsData.Get_Fields("EMPLOYEENUMBER"), "MAIN", true))
									{
										dblHours = 0;
										rsSum.OpenRecordset("SELECT * FROM tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID WHERE tblpayrolldistribution.employeenumber = '" + rsData.Get_Fields("EmployeeNumber") + "' and (((tblPayrollDistribution.AccountCode)<>'1') AND ((tblPayCategories.Type)='Hours')) ");
										while (!rsSum.EndOfFile())
										{
											// still have to check the distributions frequency
											if (Conversion.Val(rsSum.Get_Fields("accountcode")) != 13)
											{
												dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
											}
											else
											{
												if (modGlobalRoutines.Statics.ggboolBiWeekly)
												{
													if (modGlobalRoutines.Statics.ggintWeekNumber < 3)
													{
														dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
													}
												}
												else
												{
													if (modGlobalRoutines.Statics.ggintWeekNumber < 5)
													{
														dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
													}
												}
											}
											rsSum.MoveNext();
										}
										vsGrid.TextMatrix(lngRow, 6, FCConvert.ToString(dblHours));
									}
								}
							}
						}
					}
					// x
					dblTotal += Conversion.Val(vsGrid.TextMatrix(lngRow, 6));
				}
				// lngRow
				txtTotal.Text = FCConvert.ToString(dblTotal);
			}
		}

		public void LoadProcessGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadProcessGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//DAO.Field FieldName = new DAO.Field();
				int intArrayCounter;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter = 0;
				bool pboolPrinted;
				clsDRWrapper rsSum = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				string strDeptDiv = "";
				string strDeptDivPassword = "";
				// vbPorter upgrade warning: strEntryDone As object	OnWrite(string())
				object strEntryDone;
				bool boolDone;
				bool boolSequence = false;
				bool boolDeptDiv = false;
				bool boolSelectedDeptDiv;
				string strNewValue = "";
				string strOldValue = "";
				// vbPorter upgrade warning: strColor As string	OnWrite(int, string)
				string strColor = "";
				string strPrintSequence = "";
				double dblTotal;
				string strSortOrder = "";
				string strWhere = "";
				double dblHours = 0;
                vsGrid.Redraw = false;
                vsGrid.Clear();
				vsGrid.Rows = 1;
				dblTotal = 0;
				// For intCounter = 0 To UBound(strEntryDone) - 1
				// strEntryDone(intCounter) = 0
				// Next
				vsGrid.TextMatrix(0, 1, "DE Done");
				vsGrid.TextMatrix(0, 2, "Emp Number");
				vsGrid.TextMatrix(0, 3, "Employee Name");
				vsGrid.TextMatrix(0, 4, "Dept/Div");
				vsGrid.TextMatrix(0, 5, "Frequency");
				vsGrid.TextMatrix(0, 6, "Hours This Week");
				vsGrid.ColHidden(0, true);
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
				}
				else
				{
					boolDone = FCConvert.ToBoolean(rsData.Get_Fields("DataEntry"));
					strEntryDone = Strings.Split(FCConvert.ToString(rsData.Get_Fields("Description")), ";", -1, CompareConstants.vbBinaryCompare);
				}
				if (boolGridSort)
				{
					if (boolASC)
					{
						strSortOrder = " ASC";
					}
					else
					{
						strSortOrder = " DESC";
					}
					switch (intGridColumn)
					{
						case 0:
							{
								strPrintSequence = " Order by tblEmployeeMaster.DataEntry" + strSortOrder + ",  tblEmployeeMaster.LastName" + strSortOrder + ",tblEmployeeMaster.FirstName" + strSortOrder;
								break;
							}
						case 1:
							{
								strPrintSequence = " Order by tblEmployeeMaster.DataEntry" + strSortOrder + ",  tblEmployeeMaster.LastName" + strSortOrder + ",tblEmployeeMaster.FirstName" + strSortOrder;
								break;
							}
						case 2:
							{
								strPrintSequence = " Order by tblEmployeeMaster.DataEntry" + strSortOrder + ", tblEmployeeMaster.EmployeeNumber" + strSortOrder;
								break;
							}
						case 3:
							{
								strPrintSequence = " Order by tblEmployeeMaster.LastName" + strSortOrder + ",tblEmployeeMaster.FirstName" + strSortOrder;
								break;
							}
						case 4:
							{
								strPrintSequence = " Order by tblEmployeeMaster.DeptDiv" + strSortOrder + ",tblEmployeeMaster.LastName" + strSortOrder + ",tblEmployeeMaster.FirstName" + strSortOrder;
								break;
							}
						case 5:
							{
								strPrintSequence = " Order by Description" + strSortOrder;
								break;
							}
						default:
							{
								strPrintSequence = " Order by tblEmployeeMaster.DataEntry" + strSortOrder + ",  tblEmployeeMaster.EmployeeNumber" + strSortOrder;
								break;
							}
					}
					//end switch
					boolGridSort = false;
				}
				else
				{
					clsDRWrapper rsDefault = new clsDRWrapper();
					rsDefault.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
					if (rsDefault.EndOfFile())
					{
						strPrintSequence = " Order by tblEmployeeMaster.DataEntry desc, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
					}
					else
					{
						// Employee Name = 0
						// Employee Number = 1
						// Sequence Number = 2
						// Department / Div = 3
						// Selected Dept / Div = 4
						switch (rsDefault.Get_Fields_Int32("DataEntrySequence"))
						{
							case 0:
								{
									strPrintSequence = " Order by tblEmployeeMaster.DataEntry desc,  tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
									break;
								}
							case 1:
								{
									strPrintSequence = " Order by tblEmployeeMaster.DataEntry desc,  tblEmployeeMaster.EmployeeNumber";
									break;
								}
							case 2:
								{
									strPrintSequence = " Order by tblEmployeeMaster.DataEntry desc,  tblEmployeeMaster.SeqNumber,tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
									boolSequence = true;
									break;
								}
							case 3:
								{
									strPrintSequence = " Order by tblEmployeeMaster.DataEntry desc,  tblEmployeeMaster.DeptDiv,tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
									boolDeptDiv = true;
									break;
								}
							case 4:
								{
									if (modGlobalConstants.Statics.gboolASKPermission)
									{
										strDeptDiv = fecherFoundation.Strings.Trim(Interaction.InputBox("Enter Dept / Div to report on", "Department / Division", null));
										if (fecherFoundation.Strings.Trim(strDeptDiv) == string.Empty)
										{
											MessageBox.Show("Invalid Department / Division", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
											return;
										}
										else
										{
											strDeptDivPassword = fecherFoundation.Strings.Trim(Interaction.InputBox("Enter the password for this Dept/Div that was setup in General Entry.", "Department / Division", null));
											if (fecherFoundation.Strings.Trim(strDeptDivPassword) == string.Empty)
											{
												MessageBox.Show("Invalid Password for the Department / Division: " + strDeptDiv, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
												return;
											}
											else
											{
												clsDRWrapper clsSave = new clsDRWrapper();
												if (!clsSave.UpdateDatabaseTable("tblPayrollDeptDiv", "SystemSettings"))
												{
													// THEY DO NOT HAVE A NEW GE SO THEY TABLE DOES NOT YET EXIST
													MessageBox.Show("A new General Entry must be installed for this new function to work. Plase call TRIO.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
													return;
													// Call AddPayrollDeptDivTable
												}
												rsData.OpenRecordset("Select * from tblPayrollDeptDiv where DeptDiv = '" + fecherFoundation.Strings.Trim(strDeptDiv) + "'", "SystemSettings");
												if (rsData.EndOfFile())
												{
													MessageBox.Show("Password for Department / Division: " + strDeptDiv + " has not been setup in General Entry", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
													return;
												}
												else
												{
													if (fecherFoundation.Strings.Trim(strDeptDivPassword) != fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Password"))))
													{
														MessageBox.Show("Password for Department / Division: " + strDeptDiv + " is incorrect.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
														return;
													}
													else
													{
														modGlobalConstants.Statics.gstrDeptDiv = fecherFoundation.Strings.Trim(strDeptDiv);
														modGlobalConstants.Statics.gboolASKPermission = false;
													}
												}
											}
										}
									}
									else
									{
										strDeptDiv = modGlobalConstants.Statics.gstrDeptDiv;
									}
									strPrintSequence = " AND DeptDiv = '" + strDeptDiv + "' Order by tblEmployeeMaster.DataEntry desc,  tblEmployeeMaster.DeptDiv,tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
									boolSelectedDeptDiv = true;
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
					}
				}
				rsData.OpenRecordset("Select tblFrequencyCodes.*, tblEmployeeMaster.* from tblEmployeeMaster INNER JOIN tblFrequencyCodes ON tblEmployeeMaster.FreqCodeID = tblFrequencyCodes.ID Where tblEmployeeMaster.Status = 'Active'" + strPrintSequence);
				if (!rsData.EndOfFile())
				{
					lblPayRun.Text = "Pay Date: " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "       Pay Run ID: " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
					vsGrid.Rows += 1;
					// need to check the type of frequency.  Hold 1 week or week 1-4 only etc.
					// Call rsSum.OpenRecordset("SELECT Sum(tblPayrollDistribution.HoursWeek) AS SumOfHoursWeek, tblPayrollDistribution.EmployeeNumber FROM tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID WHERE (((tblPayrollDistribution.AccountCode)<>'1') AND ((tblPayCategories.Type)='Hours')) GROUP BY tblPayrollDistribution.EmployeeNumber")
					// For intCounter = 1 To rsData.RecordCount
					intCounter = 1;
					while (!rsData.EndOfFile())
					{
						if (FCConvert.ToBoolean(AllowedEditEmployee(rsData.Get_Fields("employeenumber"))))
						{
							vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("tblEmployeeMaster.ID")));
							if (boolSequence)
							{
								strNewValue = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
							}
							else if (boolDeptDiv)
							{
								strNewValue = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
							}
							if (FCConvert.ToBoolean(rsData.Get_Fields("DataEntry")))
							{
								strColor = FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								// strOldValue = strNewValue
							}
							else
							{
								if (!boolSequence && !boolDeptDiv)
								{
									strColor = FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								}
								else
								{
									if (strNewValue == strOldValue)
									{
										// the color is the same
										// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, .Cols - 1) = TRIOCOLORGRAYBACKGROUND
									}
									else
									{
										// the color is different
										if (strColor == string.Empty)
										{
											strColor = FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
										}
										else if (FCConvert.ToDouble(strColor) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
										{
											//FC:FINAL:DDU:#i2490 - fixed color to its decimal format
											//strColor = "&HE6F5FD";
											strColor = "16641766";
										}
										else
										{
											strColor = FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
										}
										strOldValue = strNewValue;
									}
								}
							}
							vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, vsGrid.Cols - 1, strColor);
							// If boolDone Then
							// .TextMatrix(intCounter, 1) = True
							// Else
							// For intArrayCounter = 0 To UBound(strEntryDone)
							// If Trim(strEntryDone(intArrayCounter)) = Trim(rsData.Fields("EmployeeNumber")) Then
							// .TextMatrix(intCounter, 1) = True
							// Exit For
							// Else
							// .TextMatrix(intCounter, 1) = False
							// End If
							// Next
							// End If
							if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DataEntryDone")))
							{
								vsGrid.TextMatrix(intCounter, 1, FCConvert.ToString(true));
							}
							else
							{
								vsGrid.TextMatrix(intCounter, 1, FCConvert.ToString(false));
							}
							vsGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
							vsGrid.TextMatrix(intCounter, 3, rsData.Get_Fields_String("LastName") + ", " + fecherFoundation.Strings.Trim(rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields("middlename")) + " " + rsData.Get_Fields_String("Desig"));
							vsGrid.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("DeptDiv"))));
							vsGrid.TextMatrix(intCounter, 5, FCConvert.ToString(rsData.Get_Fields("Description")));
							vsGrid.TextMatrix(intCounter, 6, FCConvert.ToString(0));
							if (modGlobalRoutines.ApplyThisEmployee(FCConvert.ToInt32(rsData.Get_Fields("FreqCodeID")), FCConvert.ToString(rsData.Get_Fields("EMPLOYEENUMBER")), "MAIN", true))
							{
								dblHours = 0;
								rsSum.OpenRecordset("SELECT * FROM tblPayrollDistribution INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID WHERE tblpayrolldistribution.employeenumber = '" + rsData.Get_Fields("EmployeeNumber") + "' and (((tblPayrollDistribution.AccountCode)<>'1') AND ((tblPayCategories.Type)='Hours')) ");
								while (!rsSum.EndOfFile())
								{
									// still have to check the distributions frequency
									if (Conversion.Val(rsSum.Get_Fields("accountcode")) != 13)
									{
										dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
									}
									else
									{
										if (modGlobalRoutines.Statics.ggboolBiWeekly)
										{
											if (modGlobalRoutines.Statics.ggintWeekNumber < 3)
											{
												dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
											}
										}
										else
										{
											if (modGlobalRoutines.Statics.ggintWeekNumber < 5)
											{
												dblHours += Conversion.Val(rsSum.Get_Fields_Double("HoursWeek"));
											}
										}
									}
									rsSum.MoveNext();
								}
								vsGrid.TextMatrix(intCounter, 6, FCConvert.ToString(dblHours));
								dblTotal += dblHours;
							}
							vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 0, intCounter, 6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsGrid.Rows += 1;
							intCounter += 1;
						}
						rsData.MoveNext();
					}
				}
				txtTotal.Text = FCConvert.ToString(dblTotal);
				if (vsGrid.Rows > 1)
					vsGrid.Rows -= 1;
				for (intCounter = 1; intCounter <= (vsGrid.Rows - 1); intCounter++)
				{
					if (vsGrid.TextMatrix(intCounter, 2) == modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber)
					{
						vsGrid.TopRow = intCounter;
						break;
					}
				}
                vsGrid.Redraw = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
                vsGrid.Redraw = true;
				return;
			}
		}

		private void frmEmployeeListing_Resize(object sender, System.EventArgs e)
		{
			// Adjust the widths of the columns to be a
			// percentage of the grid with itself
			vsGrid.ColWidth(0, vsGrid.WidthOriginal * 0);
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.09));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.32));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.13));
			vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuCompleted_Click(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper rsData = new clsDRWrapper();
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			string strDescription;
			clsDRWrapper rsSave = new clsDRWrapper();
			if (this.vsGrid.Rows > 2)
			{
				// THIS MEANS THAT THERE ARE MORE THEN ONE EMPLOYEE SO THIS MAY NOT BE A SINGLE
				// EMPLOYEE PAY RUN. WE NEED TO CHECK THE TA FLAG TO MAKE SURE ALL TA'S GET RUN
				// SEE CALL FOR CARIBOU #51578   'BAD CALL ID NUMBER MATTHEW 9/22/2004
				// Call rsData.OpenRecordset("Select * from tblPayrollProcessSetup")
				// If rsData.Fields("TAChecksPrinted") = "NO" Or rsData.Fields("TAChecksPrinted") = "YES" Then
				// rsData.Edit
				// rsData.Fields("TAChecksPrinted") = "ALL"
				// rsData.Update
				// 
				// AddCYAEntry "PY", "TAChecksPrinted", "GROUP", gstrUser
				// End If
			}
			modGlobalRoutines.UpdatePayrollStepTable("DataEntry");
			strDescription = string.Empty;
			for (intCount = 1; intCount <= (this.vsGrid.Rows - 1); intCount++)
			{
				if (Conversion.Val(this.vsGrid.TextMatrix(intCount, 2)) == 0)
				{
				}
				else
				{
					strDescription += ";" + this.vsGrid.TextMatrix(intCount, 2);
				}
				rsSave.Execute("update tblemployeemaster set DataEntryDone = 1 where employeenumber = '" + vsGrid.TextMatrix(intCount, 2) + "'", "twpy0000.vb1");
			}
			// intCount
			rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun));
			if (!rsData.EndOfFile())
			{
				rsData.Edit();
				// THIS I
				rsData.Set_Fields("Description", strDescription + " ");
				// rsData.Fields("Description") = strDescription
				rsData.Update();
			}
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuImportFromTimeClockPlus_Click(object sender, System.EventArgs e)
		{
			frmLoadFromTimeClockPlus.InstancePtr.Show();
			// ReloadAll
		}

		private void mnuImportTimeClock_Click(object sender, System.EventArgs e)
		{
			frmImportFromTimeClock.InstancePtr.Show();
		}

		private void vsGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsGrid.MouseRow == 0)
			{
				intGridColumn = vsGrid.Col;
				boolASC = !boolASC;
				boolGridSort = true;
				// If intGridColumn <> 6 Then
				// Call LoadProcessGrid
				// Else
				// vsGrid.Col = 6
				if (intGridColumn != 6)
				{
					if (intGridColumn != 1)
					{
						if (boolASC)
						{
							vsGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else
						{
							vsGrid.Sort = FCGrid.SortSettings.flexSortStringDescending;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					if (boolASC)
					{
						vsGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					}
					else
					{
						vsGrid.Sort = FCGrid.SortSettings.flexSortNumericDescending;
					}
				}
				// End If
			}
			else
			{
				boolGridSort = false;
			}
		}

		private void vsGrid_DblClick(object sender, System.EventArgs e)
		{
			string strData = "";
			clsDRWrapper rsSearch = new clsDRWrapper();
			if (vsGrid.MouseRow < 1)
				return;
			if (vsGrid.MouseCol == 1)
			{
				if (FCConvert.CBool(vsGrid.TextMatrix(vsGrid.MouseRow, 1)) != false)
				{
					if (MessageBox.Show("Are you sure you wish to mark this employee as incomplete?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						rsSearch.OpenRecordset("Select Description from tblPayrollSteps Where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "TWPY0000.vb1");
						if (!rsSearch.EndOfFile())
						{
							strData = FCConvert.ToString(rsSearch.Get_Fields("Description"));
							strData = strData.Replace(";" + vsGrid.TextMatrix(vsGrid.MouseRow, 2), "");
							rsSearch.Edit();
							rsSearch.Set_Fields("Description", strData);
							rsSearch.Update();
							rsSearch.OpenRecordset("Select DataEntry from tblPayrollSteps Where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "TWPY0000.vb1");
							rsSearch.Edit();
							rsSearch.Set_Fields("DataEntry", false);
							rsSearch.Update();
						}
						rsSearch.Execute("update tblemployeemaster set dataentrydone = 0 where employeenumber = '" + vsGrid.TextMatrix(vsGrid.MouseRow, 2) + "'", "twpy0000.vb1");
						LoadProcessGrid();
					}
				}
			}
			else
			{
				if (vsGrid.MouseRow >= 0)
				{
					// check if they are allowed to edit this person
					if (FCConvert.ToBoolean(AllowedEditEmployee(vsGrid.TextMatrix(vsGrid.MouseRow, 2))))
					{
						// gtypeCurrentEmployee.EMPLOYEENUMBER = VSGrid.TextMatrix(VSGrid.Row, 0)
						modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = vsGrid.TextMatrix(vsGrid.MouseRow, 2);
						modGlobalRoutines.SetCurrentEmployeeType(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, boolChild: false);
						// update the global settings table with the current employee number
						// Call rsSearch.Execute("Update tblGlobalSettings Set LastEmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "',LastUserID = '" & gstrUser & "'")
						modGlobalRoutines.ShowEmployeeInformation(true);
						modGlobalVariables.Statics.gboolDataEntry = true;
						modCoreysSweeterCode.Statics.gstrEditList = "";
						//FC:FINAL:DDU:#2711 - only from this form add save & exit button on frmPayrollDistribution
						FromDataEntry = true;
						frmPayrollDistribution.InstancePtr.Show(FCForm.FormShowEnum.Modal);
						// Unload Me
						// Me.Hide
						// frmPayrollDistribution.SetFocus
						// boolGridSort = True
						if (fecherFoundation.Strings.Trim(modCoreysSweeterCode.Statics.gstrEditList) != string.Empty)
						{
							modCoreysSweeterCode.Statics.gstrEditList = Strings.Mid(modCoreysSweeterCode.Statics.gstrEditList, 1, modCoreysSweeterCode.Statics.gstrEditList.Length - 1);
						}
						Reload(modCoreysSweeterCode.Statics.gstrEditList);
						// Call LoadProcessGrid
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object AllowedEditEmployee(string strEmployeeNumber)
		{
			object AllowedEditEmployee = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strDeptDiv = "";
			string strgroup = "";
			string strDepartment = "";
			int lngSeq = 0;
			bool boolAllowed;
			int lngID;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				boolAllowed = true;
				if (boolHasPerms)
				{
					AllowedEditEmployee = true;
					return AllowedEditEmployee;
				}
				rsLoad.OpenRecordset("select groupid,deptdiv,seqnumber,department from tblemployeemaster where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					strgroup = FCConvert.ToString(rsLoad.Get_Fields("groupid"));
					strDeptDiv = FCConvert.ToString(rsLoad.Get_Fields("deptdiv"));
					lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("seqnumber"))));
					strDepartment = FCConvert.ToString(rsLoad.Get_Fields("department"));
				}
				else
				{
					return AllowedEditEmployee;
				}
				rsEmpPerm.MoveFirst();
				while (!rsEmpPerm.EndOfFile())
				{
					switch (rsEmpPerm.Get_Fields_Int32("type"))
					{
						case CNSTPYTYPEDEPTDIV:
							{
								if (strDeptDiv == FCConvert.ToString(rsEmpPerm.Get_Fields("val")))
								{
									boolAllowed = false;
									break;
								}
								break;
							}
						case CNSTPYTYPEGROUP:
							{
								if (strgroup == FCConvert.ToString(rsEmpPerm.Get_Fields("val")))
								{
									boolAllowed = false;
									break;
								}
								break;
							}
						case CNSTPYTYPEIND:
							{
								if (strEmployeeNumber == FCConvert.ToString(rsEmpPerm.Get_Fields("val")))
								{
									boolAllowed = false;
									break;
								}
								break;
							}
						case CNSTPYTYPESEQ:
							{
								if (lngSeq == Conversion.Val(rsEmpPerm.Get_Fields("val")))
								{
									boolAllowed = true;
									break;
								}
								break;
							}
						case CNSTPYTYPEDEPARTMENT:
							{
								if (Conversion.Val(strDepartment) == Conversion.Val(rsEmpPerm.Get_Fields("val")))
								{
									boolAllowed = false;
								}
								break;
							}
					}
					//end switch
					rsEmpPerm.MoveNext();
				}
				AllowedEditEmployee = boolAllowed;
				return AllowedEditEmployee;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AllowedEditEmployee", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AllowedEditEmployee;
		}

		private void vsGrid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsGrid[e.ColumnIndex, e.RowIndex];
            if (vsGrid.GetFlexColIndex(e.ColumnIndex) == 1)
			{
				//ToolTip1.SetToolTip(vsGrid, "Double click to indicate employee is incomplete");
				cell.ToolTipText =  "Double click to indicate employee is incomplete";
			}
			else
			{
				//ToolTip1.SetToolTip(vsGrid, "Yellow lines indicate that this employee was selected in the data entry screen");
				cell.ToolTipText = "Yellow lines indicate that this employee was selected in the data entry screen";
			}
		}
	}
}
