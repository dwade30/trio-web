//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayCodes.
	/// </summary>
	public partial class rptPayCodes : BaseSectionReport
	{
		public rptPayCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Pay Codes";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayCodes InstancePtr
		{
			get
			{
				return (rptPayCodes)Sys.GetInstance(typeof(rptPayCodes));
			}
		}

		protected rptPayCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 30,2001
		//
		// NOTES: One visible problem with this code is that it references
		// the form frmPayCodes directly. If this report is called from
		// any other form then this one then the data will not display correctly.
		//
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter == frmPayCodes.InstancePtr.vsPayCodes.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				txtPayCode.Text = Strings.Left(FCConvert.ToString(frmPayCodes.InstancePtr.vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1);
				txtDescription.Text = frmPayCodes.InstancePtr.vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
				txtRate.Text = FCConvert.ToString(Conversion.Val(frmPayCodes.InstancePtr.vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)));
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Pay Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (this.Document.Printer != null)
				{
					// Me.Printer.PrintQuality = ddPQMedium
				}
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Payroll Pay Codes";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		
	}
}