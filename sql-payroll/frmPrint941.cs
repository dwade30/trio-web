//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPrint941 : BaseForm
	{
		public frmPrint941()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.cmbSchedule.Text = "Semiweekly schedule depositor";
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_9, 9);
			this.txtStartDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtStartDate.AddControlArrayElement(txtStartDate_0, 0);
			this.txtEndDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtEndDate.AddControlArrayElement(txtEndDate_0, 0);
			this.lblFrom = new System.Collections.Generic.List<FCLabel>();
			this.lblFrom.AddControlArrayElement(lblFrom_0, 0);
			this.lblTo = new System.Collections.Generic.List<FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrint941 InstancePtr
		{
			get
			{
				return (frmPrint941)Sys.GetInstance(typeof(frmPrint941));
			}
		}

		protected frmPrint941 _InstancePtr = null;
		//=========================================================
		int intQuarter;
		int lngYear;
		cFed941ViewContext viewContext;
        internal List<string> batchReports;

        private void frmPrint941_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmPrint941_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData)) / 0x10000;
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPrint941_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupNumericEntryTextBoxes();
			ToggleAdditionalInfoVisibility(false);
			LoadInfo();
		}

		private void SetupNumericEntryTextBoxes()
		{
			txtQualExpensesFamilyLeaveWages.AllowOnlyNumericInput(true);
			txtQualExpensesSickLeaveWages.AllowOnlyNumericInput(true);
			txtQualExpensesWagesLine21.AllowOnlyNumericInput(true);
			txtQualFamilyLeaveWages.AllowOnlyNumericInput(true);
			txtQualSickLeaveWages.AllowOnlyNumericInput(true);
			txt11bNonRefCredit.AllowOnlyNumericInput(true);
			txt11cNonRefCredit.AllowOnlyNumericInput(true);
			txt13cRefundableCreditForQualSFLeaveWages.AllowOnlyNumericInput(true);
			txt13dRefundableEmplRetentionCredit.AllowOnlyNumericInput(true);
			txtTotalAdvancesFromForm7200.AllowOnlyNumericInput(true);
			txtQualWagesEmplRetentionCredit.AllowOnlyNumericInput(true);
			txtCreditFromForm5884C.AllowOnlyNumericInput(true);
		}

		private void ToggleAdditionalInfoVisibility(bool isVisible)
		{
			fra941AdditionalInfo.Visible = isVisible;
			fraDateInfo.Visible = !isVisible;
			Frame1.Visible = !isVisible;
			Frame2.Visible = !isVisible;
			Frame3.Visible = !isVisible;
			Frame4.Visible = !isVisible;
			Frame5.Visible = !isVisible;
			Frame6.Visible = !isVisible;
		}

		private void LoadInfo()
		{
			string strSQL;
			clsDRWrapper clsLoad = new clsDRWrapper();
			modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
			string strTemp = "";

			switch (intQuarter)
			{
				case 1:
					{
						strTemp = "March";
						break;
					}
				case 2:
					{
						strTemp = "June";
						break;
					}
				case 3:
					{
						strTemp = "September";
						break;
					}
				case 4:
					{
						strTemp = "December";
						break;
					}
			}
			//end switch
			lblInstructions.Text = "Enter date range to cover 12th of the month for the month of " + strTemp;
			lblMonth.Text = strTemp + ":";
			txtStartDate[0].Text = Strings.Format(intQuarter * 3, "00") + "/12/" + FCConvert.ToString(lngYear);
			strSQL = "select paydate from tblcheckdetail where paydate between '" + txtStartDate[0].Text + "'  and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("d", -1, fecherFoundation.DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(FCConvert.ToString(intQuarter * 3) + "/01/" + FCConvert.ToString(lngYear))))) + "' order by paydate";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				txtEndDate[0].Text = Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy");
			}
			else
			{
				txtEndDate[0].Text = txtStartDate[0].Text;
			}
			// End If
			strSQL = "select * from tblemployerinfo";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
				EWRRecord.FederalEmployerID = Strings.Replace(FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID")), "-", "", 1, -1, CompareConstants.vbTextCompare);
				EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
				EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
				EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
				EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
				EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				EWRRecord.TransmitterName = FCConvert.ToString(clsLoad.Get_Fields("transmittername"));
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolSemiWeekly")))
				{
					cmbSchedule.Text = "Semiweekly schedule depositor";
				}
				else
				{
					cmbSchedule.Text = "Monthly schedule depositor";
				}
				if (clsLoad.Get_Fields_Boolean("ForceScheduleB") == true)
				{
					chkForceScheduleB.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkForceScheduleB.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				EWRRecord.TransmitterName = "";
				EWRRecord.EmployerAddress = "";
				EWRRecord.EmployerCity = "";
				EWRRecord.EmployerName = "";
				EWRRecord.EmployerState = "ME";
				EWRRecord.EmployerStateCode = 23;
				EWRRecord.EmployerZip = "";
				EWRRecord.EmployerZip4 = "";
				EWRRecord.FederalEmployerID = "";
				EWRRecord.MRSWithholdingID = "";
				EWRRecord.StateUCAccount = "";
				EWRRecord.TransmitterExt = "";
				EWRRecord.TransmitterPhone = "0000000000";
				EWRRecord.TransmitterTitle = "";
			}
			txtEmployerName.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerName);
			txtCity.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerCity);
			txtStreetAddress.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerAddress);
			txtState.Text = EWRRecord.EmployerState;
			txtZip.Text = EWRRecord.EmployerZip;
			txtZip4.Text = EWRRecord.EmployerZip4;
			txtTransmitterName.Text = EWRRecord.TransmitterName;
			txttitle.Text = EWRRecord.TransmitterTitle;
			txtPhone.Text = "(" + Strings.Mid(EWRRecord.TransmitterPhone, 1, 3) + ")" + Strings.Mid(EWRRecord.TransmitterPhone, 4, 3) + "-" + Strings.Mid(EWRRecord.TransmitterPhone, 7);
			txtExtension.Text = EWRRecord.TransmitterExt;
			txtFederalID.Text = EWRRecord.FederalEmployerID;
			clsLoad.OpenRecordset("select * from tblFed941 where quarter = " + FCConvert.ToString(intQuarter) + " and reportyear = " + FCConvert.ToString(lngYear), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				txtIncomeTax.Text = FCConvert.ToString(clsLoad.Get_Fields("CurrentYearIncomeTaxWithholdingAdjustment"));
				txtSSAndMedicare.Text = FCConvert.ToString(clsLoad.Get_Fields_Double("PriorQuarterSSAndMedicareAdjustment"));
				txtSickPay.Text = FCConvert.ToString(clsLoad.Get_Fields("SickPayAdjustment"));
				txtTipsAndInsurance.Text = FCConvert.ToString(clsLoad.Get_Fields("TipsAndLifeInsuranceAdjustment"));
				txtCobraPayments.Text = FCConvert.ToString(clsLoad.Get_Fields("CobraPayments"));
				txtCobraIndividuals.Text = FCConvert.ToString(clsLoad.Get_Fields("CobraIndividuals"));
				txtDeposits.Text = FCConvert.ToString(viewContext.TotalLiability());

				txtQualSickLeaveWages.Text = clsLoad.Get_Fields("QualifiedSickLeaveWages").ToString();
				txtQualFamilyLeaveWages.Text = clsLoad.Get_Fields("QualifiedFamilyLeaveWages").ToString();
				txt11bNonRefCredit.Text = clsLoad.Get_Fields("NonRefundableCreditforLeaveWages").ToString();
				txt11cNonRefCredit.Text = clsLoad.Get_Fields("NonRefundableEmplRetentionCredit").ToString();
				txt13cRefundableCreditForQualSFLeaveWages.Text = clsLoad.Get_Fields("RefundableCreditforLeaveWages").ToString();
				txt13dRefundableEmplRetentionCredit.Text = clsLoad.Get_Fields("RefundableEmplRetentionCredit").ToString();
				txtTotalAdvancesFromForm7200.Text = clsLoad.Get_Fields("TotalAdvancesFromForm7200").ToString();
				txtQualExpensesSickLeaveWages.Text = clsLoad.Get_Fields("QualifiedExpensesAllocableToSickLeaveWages").ToString();
				txtQualExpensesFamilyLeaveWages.Text =
					clsLoad.Get_Fields("QualifiedExpensesAllocableToFamilyLeaveWages").ToString();
				txtQualWagesEmplRetentionCredit.Text = clsLoad.Get_Fields("QualifiedWagesForEmplRetentionCredit").ToString();
				txtQualExpensesWagesLine21.Text = clsLoad.Get_Fields("QualifiedExpensesAllocableToLine21Wages").ToString();
				txtCreditFromForm5884C.Text = clsLoad.Get_Fields("CreditFromForm5884C").ToString();
			}
			else
			{
				txtDeposits.Text = FCConvert.ToString(viewContext.TotalLiability());
			}
		}

		public void Init(cFed941ViewContext theView)
		{
			viewContext = theView;
			intQuarter = viewContext.ReportQuarter;
			lngYear = viewContext.ReportYear;
			if (viewContext.ReportQuarter == 0 || viewContext.ReportYear == 0)
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
					{
						intQuarter = 1;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
					{
						intQuarter = 2;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
					{
						intQuarter = 3;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
					{
						intQuarter = 4;
					}
				}
				else
				{
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init3("Select year and quarter", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
				}
				viewContext.ReportQuarter = intQuarter;
				viewContext.ReportYear = lngYear;
			}
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL;
			string strSequence;
			string strTemp;
			bool boolUseSemiWeekly = false;
			double dblDeposits;

			if (!fra941AdditionalInfo.Visible)
			{
				ToggleAdditionalInfoVisibility(true);
				txtQualSickLeaveWages.Focus();
			}
			else
			{
				srpt941ScheduleB.InstancePtr.Unload();
				rptLaser941.InstancePtr.Unload();
				strSQL = "Select * from tblemployerinfo";
				clsSave.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("address1", Strings.Mid(txtStreetAddress.Text + Strings.StrDup(modCoreysSweeterCode.EWRReturnAddressLen, " "), 1, modCoreysSweeterCode.EWRReturnAddressLen));
				clsSave.Set_Fields("city", Strings.Mid(txtCity.Text + Strings.StrDup(modCoreysSweeterCode.EWRReturnCityLen, " "), 1, modCoreysSweeterCode.EWRReturnCityLen));
				clsSave.Set_Fields("employername", Strings.Mid(txtEmployerName.Text + Strings.StrDup(modCoreysSweeterCode.EWRReturnNameLen, " "), 1, modCoreysSweeterCode.EWRReturnNameLen));
				clsSave.Set_Fields("state", Strings.Mid(txtState.Text + "  ", 1, 2));
				clsSave.Set_Fields("zip", txtZip.Text);
				clsSave.Set_Fields("zip4", txtZip4.Text);
				clsSave.Set_Fields("transmittertitle", txttitle.Text);
				clsSave.Set_Fields("transmitterextension", txtExtension.Text);
				clsSave.Set_Fields("transmittername", txtTransmitterName.Text);
				strTemp = txtPhone.Text;
				strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Format(strTemp, "0000000000");
				clsSave.Set_Fields("transmitterphone", strTemp);
				clsSave.Set_Fields("FederalEmployerID", txtFederalID.Text);
				if (cmbSchedule.Text == "Semiweekly schedule depositor")
				{
					boolUseSemiWeekly = true;
				}
				else
				{
					boolUseSemiWeekly = false;
				}
				if (chkForceScheduleB.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ForceScheduleB", true);
				}
				else
				{
					clsSave.Set_Fields("ForceScheduleB", false);
				}
				clsSave.Set_Fields("boolSemiWeekly", boolUseSemiWeekly);
				clsSave.Update();
				strSQL = "select * from tblFed941 where quarter = " + FCConvert.ToString(intQuarter) + " and reportyear = " + FCConvert.ToString(lngYear);
				clsSave.OpenRecordset(strSQL, "Twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				dblDeposits = Conversion.Val(txtDeposits.Text);
				clsSave.Set_Fields("CurrentYearIncomeTaxWithholdingAdjustment", FCConvert.ToString(Conversion.Val(txtIncomeTax.Text)));
				clsSave.Set_Fields("PriorQuarterSSAndMedicareAdjustment", FCConvert.ToString(Conversion.Val(txtSSAndMedicare.Text)));
				clsSave.Set_Fields("SickPayAdjustment", FCConvert.ToString(Conversion.Val(txtSickPay.Text)));
				clsSave.Set_Fields("TipsAndLifeInsuranceAdjustment", FCConvert.ToString(Conversion.Val(txtTipsAndInsurance.Text)));
				clsSave.Set_Fields("Deposits", dblDeposits);
				clsSave.Set_Fields("quarter", intQuarter);
				clsSave.Set_Fields("reportyear", lngYear);
				clsSave.Set_Fields("CobraPayments", FCConvert.ToString(Conversion.Val(txtCobraPayments.Text)));
				clsSave.Set_Fields("CobraIndividuals", FCConvert.ToString(Conversion.Val(txtCobraIndividuals.Text)));
				clsSave.Set_Fields("QualifiedSickLeaveWages", Conversion.Val(txtQualSickLeaveWages.Text));
				clsSave.Set_Fields("QualifiedFamilyLeaveWages", Conversion.Val(txtQualFamilyLeaveWages.Text));
				clsSave.Set_Fields("NonRefundableCreditforLeaveWages", Conversion.Val(txt11bNonRefCredit.Text));
				clsSave.Set_Fields("NonRefundableEmplRetentionCredit", Conversion.Val(txt11cNonRefCredit.Text));
				clsSave.Set_Fields("RefundableCreditforLeaveWages", Conversion.Val(txt13cRefundableCreditForQualSFLeaveWages.Text));
				clsSave.Set_Fields("RefundableEmplRetentionCredit", Conversion.Val(txt13dRefundableEmplRetentionCredit.Text));
				clsSave.Set_Fields("TotalAdvancesFromForm7200", Conversion.Val(txtTotalAdvancesFromForm7200.Text));
				clsSave.Set_Fields("QualifiedExpensesAllocableToSickLeaveWages", Conversion.Val(txtQualExpensesSickLeaveWages.Text));
				clsSave.Set_Fields("QualifiedExpensesAllocableToFamilyLeaveWages", Conversion.Val(txtQualExpensesFamilyLeaveWages.Text));
				clsSave.Set_Fields("QualifiedWagesForEmplRetentionCredit", Conversion.Val(txtQualWagesEmplRetentionCredit.Text));
				clsSave.Set_Fields("QualifiedExpensesAllocableToLine21Wages", Conversion.Val(txtQualExpensesWagesLine21.Text));
				clsSave.Set_Fields("CreditFromForm5884C", Conversion.Val(txtCreditFromForm5884C.Text));
				clsSave.Update();
				strSequence = viewContext.Sequence;
				rptLaser941.InstancePtr.Init(ref viewContext, ref boolUseSemiWeekly, ref dblDeposits, this.Modal, batchReports: batchReports);
				Unload();
			}
		}

		private bool KeyIsNumber(int KeyAscii)
		{
			bool KeyIsNumber = false;
			KeyIsNumber = false;
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 27 || KeyAscii == 127 || KeyAscii == 8))
			{
				KeyIsNumber = true;
			}
			return KeyIsNumber;
		}

		private void txtFederalID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
