﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEUC1EmployeeRecord
	{
		//=========================================================
		private string strSSN = string.Empty;
		private string strLastName = string.Empty;
		private string strFirstName = string.Empty;
		private string strMiddleInitial = string.Empty;
		private int intQuarter;
		private int intYear;
		private double dblUCWages;
		private double dblExcessWages;
		private double dblTaxableWages;
		private string strUCAccountNumber = string.Empty;
		private bool boolSeasonal;
		private int intWagePlanCode;
		private bool[] boolMonthEmployment = new bool[3 + 1];
		private bool boolFemale;

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string LastName
		{
			set
			{
				strLastName = value;
			}
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
		}

		public string FirstName
		{
			set
			{
				strFirstName = value;
			}
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
		}

		public string MiddleName
		{
			set
			{
				strMiddleInitial = value;
			}
			get
			{
				string MiddleName = "";
				MiddleName = strMiddleInitial;
				return MiddleName;
			}
		}

		public int Quarter
		{
			set
			{
				intQuarter = value;
			}
			get
			{
				int Quarter = 0;
				Quarter = intQuarter;
				return Quarter;
			}
		}

		public int TaxYear
		{
			set
			{
				intYear = value;
			}
			get
			{
				int TaxYear = 0;
				TaxYear = intYear;
				return TaxYear;
			}
		}

		public double UCWages
		{
			set
			{
				dblUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblUCWages;
				return UCWages;
			}
		}

		public double ExcessWages
		{
			set
			{
				dblExcessWages = value;
			}
			get
			{
				double ExcessWages = 0;
				ExcessWages = dblExcessWages;
				return ExcessWages;
			}
		}

		public double TaxableWages
		{
			set
			{
				dblTaxableWages = value;
			}
			get
			{
				double TaxableWages = 0;
				TaxableWages = dblTaxableWages;
				return TaxableWages;
			}
		}

		public string UCAccountNumber
		{
			set
			{
				strUCAccountNumber = value;
			}
			get
			{
				string UCAccountNumber = "";
				UCAccountNumber = strUCAccountNumber;
				return UCAccountNumber;
			}
		}

		public bool IsSeasonal
		{
			set
			{
				boolSeasonal = value;
			}
			get
			{
				bool IsSeasonal = false;
				IsSeasonal = boolSeasonal;
				return IsSeasonal;
			}
		}

		public int WagePlanCode
		{
			set
			{
				intWagePlanCode = value;
			}
			get
			{
				int WagePlanCode = 0;
				WagePlanCode = intWagePlanCode;
				return WagePlanCode;
			}
		}

		public void Set_EmployedDuringMonth(int intindex, bool boolEmployed)
		{
			if (intindex > 0 && intindex < 4)
			{
				boolMonthEmployment[intindex - 1] = boolEmployed;
			}
		}

		public bool Get_EmployedDuringMonth(int intindex)
		{
			bool EmployedDuringMonth = false;
			if (intindex > 0 && intindex < 4)
			{
				EmployedDuringMonth = boolMonthEmployment[intindex - 1];
			}
			return EmployedDuringMonth;
		}

		public bool Female
		{
			set
			{
				boolFemale = value;
			}
			get
			{
				bool Female = false;
				Female = boolFemale;
				return Female;
			}
		}

		public string ToString()
		{
			string ToString = "";
			string strReturn;
			strReturn = "S";
			strReturn += Strings.Format(strSSN, "000000000");
			strReturn += Strings.Left(strLastName + Strings.StrDup(20, " "), 20);
			strReturn += Strings.Left(strFirstName + Strings.StrDup(12, " "), 12);
			strReturn += Strings.Left(strMiddleInitial + " ", 1);
			strReturn += "23";
			switch (intQuarter)
			{
				case 1:
					{
						strReturn += "03" + FCConvert.ToString(intYear);
						break;
					}
				case 2:
					{
						strReturn += "06" + FCConvert.ToString(intYear);
						break;
					}
				case 3:
					{
						strReturn += "09" + FCConvert.ToString(intYear);
						break;
					}
				case 4:
					{
						strReturn += "12" + FCConvert.ToString(intYear);
						break;
					}
			}
			//end switch
			strReturn += Strings.StrDup(12, " ");
			// not used by maine
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblUCWages, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblExcessWages, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblTaxableWages, "0.00")) * 100, Strings.StrDup(14, "0"));
			strReturn += Strings.StrDup(37, " ");
			strReturn += "WAGE";
			strReturn += strUCAccountNumber;
			strReturn += Strings.StrDup(48, " ");
			// not used by maine
			if (boolSeasonal)
			{
				strReturn += "S";
			}
			else
			{
				strReturn += "N";
			}
			strReturn += Strings.StrDup(5, " ");
			// not used by state
			strReturn += FCConvert.ToString(intWagePlanCode);
			int x;
			for (x = 1; x <= 3; x++)
			{
				if (boolMonthEmployment[x - 1])
				{
					strReturn += "1";
				}
				else
				{
					strReturn += "0";
				}
			}
			// x
			strReturn += Strings.StrDup(11, " ");
			// not used by maine
			if (boolFemale)
			{
				strReturn += "1";
			}
			else
			{
				strReturn += "0";
			}
			strReturn += Strings.StrDup(49, " ");
			ToString = strReturn;
			return ToString;
		}
	}
}
