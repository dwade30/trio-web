﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCreateCustomCheckFormat.
	/// </summary>
	partial class frmCreateCustomCheckFormat
	{
		public fecherFoundation.FCComboBox cmbMailer;
		public fecherFoundation.FCLabel lblMailer;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblField;
		public fecherFoundation.FCCheckBox chkShowPayRate;
		public fecherFoundation.FCCheckBox chkNonNegotiable;
		public fecherFoundation.FCComboBox cboCheckPosition;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid vsCheckFields;
		public fecherFoundation.FCCheckBox chkDoubleStub;
		public fecherFoundation.FCComboBox cmbCustomType;
		public fecherFoundation.FCCheckBox chkTwoStubs;
		public fecherFoundation.FCTextBox txtVoidAfter;
		public Wisej.Web.ImageList ImageList1;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCPictureBox imgCheck;
		public fecherFoundation.FCLabel Label2;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileResetDefaults;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileLoadImage;
		public fecherFoundation.FCToolStripMenuItem mnuFileUnloadImage;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel lblField_0;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateCustomCheckFormat));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbMailer = new fecherFoundation.FCComboBox();
            this.lblMailer = new fecherFoundation.FCLabel();
            this.chkShowPayRate = new fecherFoundation.FCCheckBox();
            this.chkNonNegotiable = new fecherFoundation.FCCheckBox();
            this.cboCheckPosition = new fecherFoundation.FCComboBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.vsCheckFields = new fecherFoundation.FCGrid();
            this.chkDoubleStub = new fecherFoundation.FCCheckBox();
            this.cmbCustomType = new fecherFoundation.FCComboBox();
            this.chkTwoStubs = new fecherFoundation.FCCheckBox();
            this.txtVoidAfter = new fecherFoundation.FCTextBox();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.imgCheck = new fecherFoundation.FCPictureBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFileResetDefaults = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileLoadImage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileUnloadImage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.lblField_0 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdFileUnloadImage = new fecherFoundation.FCButton();
            this.cmdFileLoadImage = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNonNegotiable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsCheckFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDoubleStub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTwoStubs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileUnloadImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileLoadImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkShowPayRate);
            this.ClientArea.Controls.Add(this.chkNonNegotiable);
            this.ClientArea.Controls.Add(this.cmbMailer);
            this.ClientArea.Controls.Add(this.lblMailer);
            this.ClientArea.Controls.Add(this.cboCheckPosition);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.chkDoubleStub);
            this.ClientArea.Controls.Add(this.cmbCustomType);
            this.ClientArea.Controls.Add(this.chkTwoStubs);
            this.ClientArea.Controls.Add(this.txtVoidAfter);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.imgCheck);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblField_0);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdFileLoadImage);
            this.TopPanel.Controls.Add(this.cmdFileUnloadImage);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileUnloadImage, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileLoadImage, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(244, 30);
            this.HeaderText.Text = "Custom Check Setup";
            // 
            // cmbMailer
            // 
            this.cmbMailer.AutoSize = false;
            this.cmbMailer.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMailer.FormattingEnabled = true;
            this.cmbMailer.Items.AddRange(new object[] {
            "Mailer",
            "Laser"});
            this.cmbMailer.Location = new System.Drawing.Point(195, 90);
            this.cmbMailer.Name = "cmbMailer";
            this.cmbMailer.Size = new System.Drawing.Size(203, 40);
            this.cmbMailer.TabIndex = 18;
            this.cmbMailer.SelectedIndexChanged += new System.EventHandler(this.cmbMailer_SelectedIndexChanged);
            // 
            // lblMailer
            // 
            this.lblMailer.AutoSize = true;
            this.lblMailer.Location = new System.Drawing.Point(30, 104);
            this.lblMailer.Name = "lblMailer";
            this.lblMailer.Size = new System.Drawing.Size(86, 15);
            this.lblMailer.TabIndex = 19;
            this.lblMailer.Text = "CHECK TYPE";
            // 
            // chkShowPayRate
            // 
            this.chkShowPayRate.Location = new System.Drawing.Point(30, 257);
            this.chkShowPayRate.Name = "chkShowPayRate";
            this.chkShowPayRate.Size = new System.Drawing.Size(213, 27);
            this.chkShowPayRate.TabIndex = 17;
            this.chkShowPayRate.Text = "Show individual pay rates";
            // 
            // chkNonNegotiable
            // 
            this.chkNonNegotiable.Location = new System.Drawing.Point(30, 364);
            this.chkNonNegotiable.Name = "chkNonNegotiable";
            this.chkNonNegotiable.Size = new System.Drawing.Size(486, 27);
            this.chkNonNegotiable.TabIndex = 10;
            this.chkNonNegotiable.Text = "Show logo, municipal name & address only on non-negotiables";
            // 
            // cboCheckPosition
            // 
            this.cboCheckPosition.AutoSize = false;
            this.cboCheckPosition.BackColor = System.Drawing.SystemColors.Window;
            this.cboCheckPosition.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboCheckPosition.FormattingEnabled = true;
            this.cboCheckPosition.Location = new System.Drawing.Point(195, 150);
            this.cboCheckPosition.Name = "cboCheckPosition";
            this.cboCheckPosition.Size = new System.Drawing.Size(203, 40);
            this.cboCheckPosition.TabIndex = 7;
            this.cboCheckPosition.SelectedIndexChanged += new System.EventHandler(this.cboCheckPosition_SelectedIndexChanged);
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.vsCheckFields);
            this.Frame2.Location = new System.Drawing.Point(452, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(597, 283);
            this.Frame2.TabIndex = 15;
            this.Frame2.Text = "Setup Custom Check Fields";
            // 
            // vsCheckFields
            // 
            this.vsCheckFields.AllowSelection = false;
            this.vsCheckFields.AllowUserToResizeColumns = false;
            this.vsCheckFields.AllowUserToResizeRows = false;
            this.vsCheckFields.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsCheckFields.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsCheckFields.BackColorBkg = System.Drawing.Color.Empty;
            this.vsCheckFields.BackColorFixed = System.Drawing.Color.Empty;
            this.vsCheckFields.BackColorSel = System.Drawing.Color.Empty;
            this.vsCheckFields.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsCheckFields.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsCheckFields.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsCheckFields.ColumnHeadersHeight = 30;
            this.vsCheckFields.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsCheckFields.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsCheckFields.DragIcon = null;
            this.vsCheckFields.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsCheckFields.ExtendLastCol = true;
            this.vsCheckFields.FixedCols = 2;
            this.vsCheckFields.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsCheckFields.FrozenCols = 0;
            this.vsCheckFields.GridColor = System.Drawing.Color.Empty;
            this.vsCheckFields.GridColorFixed = System.Drawing.Color.Empty;
            this.vsCheckFields.Location = new System.Drawing.Point(0, 30);
            this.vsCheckFields.Name = "vsCheckFields";
            this.vsCheckFields.OutlineCol = 0;
            this.vsCheckFields.ReadOnly = true;
            this.vsCheckFields.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsCheckFields.RowHeightMin = 0;
            this.vsCheckFields.Rows = 1;
            this.vsCheckFields.ScrollTipText = null;
            this.vsCheckFields.ShowColumnVisibilityMenu = false;
            this.vsCheckFields.Size = new System.Drawing.Size(591, 233);
            this.vsCheckFields.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsCheckFields.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsCheckFields.TabIndex = 11;
            this.vsCheckFields.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsCheckFields_KeyDownEdit);
            this.vsCheckFields.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCheckFields_CellChanged);
            this.vsCheckFields.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsCheckFields_BeforeEdit);
            this.vsCheckFields.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCheckFields_ValidateEdit);
            this.vsCheckFields.CurrentCellChanged += new System.EventHandler(this.vsCheckFields_RowColChange);
            this.vsCheckFields.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsCheckFields_KeyPressEvent);
            this.vsCheckFields.Click += new System.EventHandler(this.vsCheckFields_ClickEvent);
            // 
            // chkDoubleStub
            // 
            this.chkDoubleStub.Location = new System.Drawing.Point(30, 210);
            this.chkDoubleStub.Name = "chkDoubleStub";
            this.chkDoubleStub.Size = new System.Drawing.Size(196, 27);
            this.chkDoubleStub.TabIndex = 8;
            this.chkDoubleStub.Text = "Use double-height stub";
            this.chkDoubleStub.CheckedChanged += new System.EventHandler(this.chkDoubleStub_CheckedChanged);
            // 
            // cmbCustomType
            // 
            this.cmbCustomType.AutoSize = false;
            this.cmbCustomType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCustomType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCustomType.FormattingEnabled = true;
            this.cmbCustomType.Location = new System.Drawing.Point(195, 30);
            this.cmbCustomType.Name = "cmbCustomType";
            this.cmbCustomType.Size = new System.Drawing.Size(203, 40);
            this.cmbCustomType.TabIndex = 3;
            this.cmbCustomType.SelectedIndexChanged += new System.EventHandler(this.cmbCustomType_SelectedIndexChanged);
            // 
            // chkTwoStubs
            // 
            this.chkTwoStubs.Location = new System.Drawing.Point(30, 210);
            this.chkTwoStubs.Name = "chkTwoStubs";
            this.chkTwoStubs.Size = new System.Drawing.Size(130, 27);
            this.chkTwoStubs.TabIndex = 2;
            this.chkTwoStubs.Text = "Use two stubs";
            this.chkTwoStubs.CheckedChanged += new System.EventHandler(this.chkTwoStubs_CheckedChanged);
            // 
            // txtVoidAfter
            // 
            this.txtVoidAfter.AutoSize = false;
            this.txtVoidAfter.BackColor = System.Drawing.SystemColors.Window;
            this.txtVoidAfter.LinkItem = null;
            this.txtVoidAfter.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtVoidAfter.LinkTopic = null;
            this.txtVoidAfter.Location = new System.Drawing.Point(225, 304);
            this.txtVoidAfter.Name = "txtVoidAfter";
            this.txtVoidAfter.Size = new System.Drawing.Size(173, 40);
            this.txtVoidAfter.TabIndex = 9;
            this.txtVoidAfter.Text = "Void after 180 days";
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 256);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // dlg1
            // 
            this.dlg1.Color = System.Drawing.Color.Black;
            this.dlg1.DefaultExt = null;
            this.dlg1.FilterIndex = ((short)(0));
            this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dlg1.FontName = "Microsoft Sans Serif";
            this.dlg1.FontSize = 8.25F;
            this.dlg1.ForeColor = System.Drawing.Color.Black;
            this.dlg1.FromPage = 0;
            this.dlg1.Location = new System.Drawing.Point(0, 0);
            this.dlg1.Name = "dlg1";
            this.dlg1.PrinterSettings = null;
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            this.dlg1.TabIndex = 0;
            this.dlg1.ToPage = 0;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 164);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(103, 18);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "CHECK POSITION";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(54, 18);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "FORMAT";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 318);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(130, 18);
            this.Label4.TabIndex = 13;
            this.Label4.Text = "VOID AFTER MESSAGE";
            // 
            // imgCheck
            // 
            this.imgCheck.AllowDrop = true;
            this.imgCheck.DrawStyle = ((short)(0));
            this.imgCheck.DrawWidth = ((short)(1));
            this.imgCheck.FillStyle = ((short)(1));
            this.imgCheck.FontTransparent = true;
            this.imgCheck.Image = ((System.Drawing.Image)(resources.GetObject("imgCheck.Image")));
            this.imgCheck.Location = new System.Drawing.Point(30, 440);
            this.imgCheck.Name = "imgCheck";
            this.imgCheck.Picture = ((System.Drawing.Image)(resources.GetObject("imgCheck.Picture")));
            this.imgCheck.Size = new System.Drawing.Size(550, 180);
            this.imgCheck.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCheck.TabIndex = 20;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 411);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(118, 15);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "PREVIEW";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileResetDefaults});
            this.MainMenu1.Name = null;
            // 
            // mnuFileResetDefaults
            // 
            this.mnuFileResetDefaults.Index = 0;
            this.mnuFileResetDefaults.Name = "mnuFileResetDefaults";
            this.mnuFileResetDefaults.Text = "Set Fields to Default Position";
            this.mnuFileResetDefaults.Click += new System.EventHandler(this.mnuFileResetDefaults_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuFileSeperator2,
            this.mnuFileLoadImage,
            this.mnuFileUnloadImage,
            this.mnuNew,
            this.mnuDelete,
            this.mnuFileSeperator,
            this.mnuFileSave,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Sample Check";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFileLoadImage
            // 
            this.mnuFileLoadImage.Index = 2;
            this.mnuFileLoadImage.Name = "mnuFileLoadImage";
            this.mnuFileLoadImage.Text = "Load Scanned Check Image";
            this.mnuFileLoadImage.Click += new System.EventHandler(this.mnuFileLoadImage_Click);
            // 
            // mnuFileUnloadImage
            // 
            this.mnuFileUnloadImage.Index = 3;
            this.mnuFileUnloadImage.Name = "mnuFileUnloadImage";
            this.mnuFileUnloadImage.Text = "Unload Scanned Check Image";
            this.mnuFileUnloadImage.Click += new System.EventHandler(this.mnuFileUnloadImage_Click);
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 4;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 5;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Format";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 6;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 7;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 8;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 9;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 10;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // lblField_0
            // 
            this.lblField_0.BackColor = System.Drawing.Color.Transparent;
            this.lblField_0.Location = new System.Drawing.Point(30, 44);
            this.lblField_0.Name = "lblField_0";
            this.lblField_0.Size = new System.Drawing.Size(86, 13);
            this.lblField_0.TabIndex = 1;
            this.lblField_0.Text = "LABEL3";
            this.lblField_0.Visible = false;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(397, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(81, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(884, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(940, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(110, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete Format";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdFileUnloadImage
            // 
            this.cmdFileUnloadImage.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileUnloadImage.AppearanceKey = "toolbarButton";
            this.cmdFileUnloadImage.Location = new System.Drawing.Point(670, 29);
            this.cmdFileUnloadImage.Name = "cmdFileUnloadImage";
            this.cmdFileUnloadImage.Size = new System.Drawing.Size(208, 24);
            this.cmdFileUnloadImage.TabIndex = 3;
            this.cmdFileUnloadImage.Text = "Unload Scanned Check Image";
            this.cmdFileUnloadImage.Click += new System.EventHandler(this.mnuFileUnloadImage_Click);
            // 
            // cmdFileLoadImage
            // 
            this.cmdFileLoadImage.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileLoadImage.AppearanceKey = "toolbarButton";
            this.cmdFileLoadImage.Location = new System.Drawing.Point(470, 29);
            this.cmdFileLoadImage.Name = "cmdFileLoadImage";
            this.cmdFileLoadImage.Size = new System.Drawing.Size(194, 24);
            this.cmdFileLoadImage.TabIndex = 4;
            this.cmdFileLoadImage.Text = "Load Scanned Check Image";
            this.cmdFileLoadImage.Click += new System.EventHandler(this.mnuFileLoadImage_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(324, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(140, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print Sample Check";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmCreateCustomCheckFormat
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmCreateCustomCheckFormat";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Custom Check Setup";
            this.Load += new System.EventHandler(this.frmCreateCustomCheckFormat_Load);
            this.Activated += new System.EventHandler(this.frmCreateCustomCheckFormat_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreateCustomCheckFormat_KeyPress);
            this.Resize += new System.EventHandler(this.frmCreateCustomCheckFormat_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNonNegotiable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsCheckFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDoubleStub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTwoStubs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileUnloadImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileLoadImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
        private FCButton cmdDelete;
        private FCButton cmdNew;
        private FCButton cmdPrint;
        private FCButton cmdFileLoadImage;
        private FCButton cmdFileUnloadImage;
    }
}
