//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmDeductionSetup.
	/// </summary>
	partial class frmDeductionSetup
	{
		public FCGrid vsDeductions;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdSave;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuTaxStatusCodes;
		public fecherFoundation.FCToolStripMenuItem mnuFrequencyCodes;
		public fecherFoundation.FCToolStripMenuItem mnuGlobalDeductionUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuGlobalMatchUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.vsDeductions = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuTaxStatusCodes = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFrequencyCodes = new fecherFoundation.FCToolStripMenuItem();
            this.mnuGlobalDeductionUpdate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuGlobalMatchUpdate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdExit = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 464);
            this.BottomPanel.Size = new System.Drawing.Size(797, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsDeductions);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Size = new System.Drawing.Size(797, 404);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Size = new System.Drawing.Size(797, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(194, 30);
            this.HeaderText.Text = "Deduction Setup";
            // 
            // vsDeductions
            // 
            this.vsDeductions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDeductions.Cols = 10;
            this.vsDeductions.Location = new System.Drawing.Point(30, 30);
            this.vsDeductions.Name = "vsDeductions";
            this.vsDeductions.Rows = 50;
            this.vsDeductions.Size = new System.Drawing.Size(739, 365);
            this.vsDeductions.StandardTab = false;
            this.vsDeductions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDeductions.TabIndex = 6;
            this.vsDeductions.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsDeductions_KeyPressEdit);
            this.vsDeductions.ComboDropDown += new System.EventHandler(this.vsDeductions_ComboDropDown);
            this.vsDeductions.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_AfterEdit);
            this.vsDeductions.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_ChangeEdit);
            this.vsDeductions.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VsDeductions_CellFormatting);
            this.vsDeductions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsDeductions_ValidateEdit);
            this.vsDeductions.CurrentCellChanged += new System.EventHandler(this.vsDeductions_RowColChange);
            this.vsDeductions.KeyDown += new Wisej.Web.KeyEventHandler(this.vsDeductions_KeyDownEvent);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(270, 284);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(82, 40);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.TabStop = false;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.AppearanceKey = "actionButton";
            this.cmdRefresh.Location = new System.Drawing.Point(146, 284);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(104, 40);
            this.cmdRefresh.TabIndex = 3;
            this.cmdRefresh.TabStop = false;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(610, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 2;
            this.cmdNew.TabStop = false;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "actionButton";
            this.cmdDelete.Enabled = false;
            this.cmdDelete.Location = new System.Drawing.Point(30, 284);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(96, 40);
            this.cmdDelete.TabIndex = 1;
            this.cmdDelete.TabStop = false;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(354, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(82, 48);
            this.cmdSave.TabStop = false;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTaxStatusCodes,
            this.mnuFrequencyCodes,
            this.mnuGlobalDeductionUpdate,
            this.mnuGlobalMatchUpdate});
            this.MainMenu1.Name = null;
            // 
            // mnuTaxStatusCodes
            // 
            this.mnuTaxStatusCodes.Index = 0;
            this.mnuTaxStatusCodes.Name = "mnuTaxStatusCodes";
            this.mnuTaxStatusCodes.Text = "Edit Tax Status Codes";
            this.mnuTaxStatusCodes.Click += new System.EventHandler(this.mnuTaxStatusCodes_Click);
            // 
            // mnuFrequencyCodes
            // 
            this.mnuFrequencyCodes.Index = 1;
            this.mnuFrequencyCodes.Name = "mnuFrequencyCodes";
            this.mnuFrequencyCodes.Text = "Edit Frequency Codes";
            this.mnuFrequencyCodes.Click += new System.EventHandler(this.mnuFrequencyCodes_Click);
            // 
            // mnuGlobalDeductionUpdate
            // 
            this.mnuGlobalDeductionUpdate.Index = 2;
            this.mnuGlobalDeductionUpdate.Name = "mnuGlobalDeductionUpdate";
            this.mnuGlobalDeductionUpdate.Text = "Globally update employee deductions";
            this.mnuGlobalDeductionUpdate.Click += new System.EventHandler(this.mnuGlobalDeductionUpdate_Click);
            // 
            // mnuGlobalMatchUpdate
            // 
            this.mnuGlobalMatchUpdate.Index = 3;
            this.mnuGlobalMatchUpdate.Name = "mnuGlobalMatchUpdate";
            this.mnuGlobalMatchUpdate.Text = "Globally update employer matches";
            this.mnuGlobalMatchUpdate.Click += new System.EventHandler(this.mnuGlobalMatchUpdate_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP1,
            this.mnuRefresh,
            this.mnuSP2,
            this.mnuPrint,
            this.mnuSP3,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP4,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New ";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete ";
            this.mnuDelete.Visible = false;
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = 3;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 4;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 5;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 6;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 7;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 8;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                      ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP4
            // 
            this.mnuSP4.Index = 9;
            this.mnuSP4.Name = "mnuSP4";
            this.mnuSP4.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 10;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "actionButton";
            this.cmdExit.Location = new System.Drawing.Point(372, 284);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(72, 40);
            this.cmdExit.TabIndex = 5;
            this.cmdExit.TabStop = false;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.Location = new System.Drawing.Point(665, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(100, 24);
            this.cmdPrintPreview.TabIndex = 3;
            this.cmdPrintPreview.Text = "Print/Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmDeductionSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(797, 572);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmDeductionSetup";
            this.Text = "Deduction Setup";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDeductionSetup_Load);
            this.Activated += new System.EventHandler(this.frmDeductionSetup_Activated);
            this.Resize += new System.EventHandler(this.frmDeductionSetup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeductionSetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeductionSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        public FCButton cmdExit;
        private FCButton cmdPrintPreview;
    }
}