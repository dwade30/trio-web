//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingRegular.
	/// </summary>
	public partial class rptCheckListingRegular : BaseSectionReport
	{
		public rptCheckListingRegular()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Check Listing Regular";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCheckListingRegular InstancePtr
		{
			get
			{
				return (rptCheckListingRegular)Sys.GetInstance(typeof(rptCheckListingRegular));
			}
		}

		protected rptCheckListingRegular _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsMaster?.Dispose();
				rsMain?.Dispose();
				rsWages?.Dispose();
				rsDeductions?.Dispose();
				rsVacSick?.Dispose();
				rsCodeType?.Dispose();
				rsType?.Dispose();
                rsMaster = null;
                rsMain = null;
                rsWages = null;
                rsDeductions = null;
                rsVacSick = null;
                rsCodeType = null;
                rsType = null;
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckListingRegular	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsMaster = new clsDRWrapper();
		private clsDRWrapper rsWages = new clsDRWrapper();
		private clsDRWrapper rsDeductions = new clsDRWrapper();
		private clsDRWrapper rsVacSick = new clsDRWrapper();
		private clsDRWrapper rsCodeType = new clsDRWrapper();
		private double dblTotal;
		private double dblHoursTotal;
		private clsDRWrapper rsType = new clsDRWrapper();
		private double dblTotalAmount;
		private double dblTotalRegular;
		private double dblTotalOther;
		private double dblTotalTaxes;
		private double dblTotalDeductions;
		private double dblTotalVacUsed;
		private double dblTotalSickUsed;
		private double dblTotalHours;
		private int intPageNumber;
		private double dblGroupAmount;
		private double dblGroupRegular;
		private double dblGroupOther;
		private double dblGroupTaxes;
		private double dblGroupDeductions;
		private double dblGroupVacUsed;
		private double dblGroupSickUsed;
		private double dblGroupHours;
		private double dblGroupGross;
		private double dblTotalGross;
		private bool boolClearGroupTotals;
		private bool boolWagesAsGross;

		public void Init(ref bool boolShowWagesAsGross)
		{
			boolWagesAsGross = boolShowWagesAsGross;
			if (boolWagesAsGross)
			{
				txtRegular.Visible = false;
				txtOther.Visible = false;
				txtGross.Visible = true;
				lblRegular.Visible = false;
				lblOther.Visible = false;
				lblGross.Visible = true;
				txtTotalRegular.Visible = false;
				txtTotalOther.Visible = false;
				txtTotGross.Visible = true;
				txtGroupGross.Visible = true;
				txtGroupOther.Visible = false;
				txtGroupRegular.Visible = false;
			}
			else
			{
				txtRegular.Visible = true;
				txtOther.Visible = true;
				txtGross.Visible = false;
				lblRegular.Visible = true;
				lblOther.Visible = true;
				lblGross.Visible = false;
				txtTotalRegular.Visible = true;
				txtTotalOther.Visible = true;
				txtTotGross.Visible = false;
				txtGroupGross.Visible = false;
				txtGroupOther.Visible = true;
				txtGroupRegular.Visible = true;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "CheckListing");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				NextRecord:
				;
				double dblGross;
				eArgs.EOF = false;
                var employeeNumber = rsMaster.Get_Fields("EmployeeNumber");

                while (!rsMaster.EndOfFile())
				{
					//Application.DoEvents();
					if (!employeeDict.ContainsKey(employeeNumber))
					{
						rsMaster.MoveNext();
					}
					else
					{
						break;
					}
				}
				if (rsMaster.EndOfFile())
				{
					this.Fields["grpHeader"].Value = "0";
					frmWait.InstancePtr.Unload();
					eArgs.EOF = true;
					return;
				}

                var payDate = rsMaster.Get_Fields_DateTime("PayDate");
                var checkNumber = rsMaster.Get_Fields("CheckNumber");
                var payRunId = rsMaster.Get_Fields("PayRunID");
                rsMain.OpenRecordset($"SELECT tblCheckDetail.PayDate, tblCheckDetail.CheckVoid, tblCheckDetail.CheckNumber,  Sum(tblCheckDetail.FederalTaxWH+tblCheckDetail.StateTaxWH+tblCheckDetail.FICATaxWH+tblCheckDetail.MedicareTaxWH) AS TotalTaxes, tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName From tblCheckDetail GROUP BY tblCheckDetail.PayDate, tblCheckDetail.CheckVoid, tblCheckDetail.CheckNumber,  tblCheckDetail.TotalRecord,tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.PayRunID HAVING (((tblCheckDetail.TotalRecord)=1) AND EmployeeNumber = '{employeeNumber}' AND PayDate = '{payDate}' AND PayRunID = {payRunId} AND tblCheckDetail.CheckNumber = {checkNumber}) order by tblcheckdetail.paydate", modGlobalVariables.DEFAULTDATABASE);
				rsWages.OpenRecordset($"SELECT tblCheckDetail.DistributionRecord, Sum(tblCheckDetail.DistHours) as SumOfDistHours, Sum(tblCheckDetail.DistGrossPay) as SumOfDistGrossPay, tblCheckDetail.CheckNumber, tblPayCategories.Description, tblPayCategories.ID, tblPayCategories.Type FROM tblCheckDetail INNER JOIN tblPayCategories ON tblCheckDetail.DistPayCategory = tblPayCategories.ID GROUP BY tblCheckDetail.DistributionRecord, tblCheckDetail.DistHours, tblCheckDetail.DistGrossPay, tblCheckDetail.CheckNumber, tblPayCategories.Description, tblPayCategories.ID, tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate, tblPayCategories.Type Having ((tblCheckDetail.distributionrecord = 1) AND EmployeeNumber = '{employeeNumber}' AND PayDate = '{payDate}' AND PayRunID = {payRunId} AND tblCheckDetail.CheckNumber = {checkNumber}) ORDER BY tblPayCategories.ID", modGlobalVariables.DEFAULTDATABASE);
				rsDeductions.OpenRecordset($"SELECT Sum(tblCheckDetail.DedAmount) AS SumOfDedAmount, tblCheckDetail.CheckNumber, tblCheckDetail.DeductionRecord From tblCheckDetail GROUP BY tblCheckDetail.CheckNumber, tblCheckDetail.DeductionRecord,tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate Having (((tblCheckDetail.DeductionRecord) = 1) AND EmployeeNumber = '{employeeNumber}' AND PayDate = '{payDate}' AND PayRunID = {payRunId} AND CheckNumber = {checkNumber})", modGlobalVariables.DEFAULTDATABASE);
				rsVacSick.OpenRecordset("SELECT tblCheckDetail.CheckNumber, tblCheckDetail.VSRecord, Sum(tblCheckDetail.VSUsed) AS SumOfVSUsed, tblCheckDetail.VSTypeID From tblCheckDetail GROUP BY tblCheckDetail.CheckNumber, tblCheckDetail.VSRecord, tblCheckDetail.VSTypeID,tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate Having (((tblCheckDetail.VSRecord) = 1) AND EmployeeNumber = '" + employeeNumber + "' AND PayDate = '" + payDate + "' AND PayRunID = " + payRunId + " AND CheckNumber = " + checkNumber + ")", modGlobalVariables.DEFAULTDATABASE);
				if (!rsMain.EndOfFile())
				{
					this.Fields["grpHeader"].Value = rsMain.Get_Fields(modGlobalVariables.Statics.gstrCheckListingReportGroup);

				}
				else
				{
					if (!rsMaster.EndOfFile())
						rsMaster.MoveNext();
					goto NextRecord;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// If gtypeFullSetReports.boolFullSet = False Then
			// Call UpdatePayrollStepTable("DEForms")
			// Else
			// blnReportCompleted = True
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intPageNumber = 0;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsMaster.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, modGlobalVariables.DEFAULTDATABASE);
			rsCodeType.OpenRecordset("Select * from tblCodeTypes");
			lblSort.Text = modGlobalVariables.Statics.gstrCheckListingSort;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolClearGroupTotals = false;
			if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
            rsMaster.DisposeOf();
            rsCodeType.DisposeOf();
            rsDeductions.DisposeOf();
            rsMain.DisposeOf();
            rsType.DisposeOf();
            rsVacSick.DisposeOf();
            rsWages.DisposeOf();

			frmReportViewer.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            var employeeNumber = rsMain.Get_Fields_String("EmployeeNumber");

            if (employeeNumber.Length > 7)
			{
				txtEmployee.Text = employeeNumber + " " + rsMain.Get_Fields_String("EmployeeName");
			}
			else
			{
				txtEmployee.Text = employeeNumber + Strings.StrDup(7 - employeeNumber.Length, " ") + rsMain.Get_Fields_String("EmployeeName");
			}
			txtDateField.Text = FCConvert.ToString(rsMain.Get_Fields("PayDate"));
			rsType.OpenRecordset("SELECT tblCheckDetail.* From tblCheckDetail where TotalRecord = 1 AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND CheckNumber = " + rsMaster.Get_Fields("CheckNumber") + " order by ID", modGlobalVariables.DEFAULTDATABASE);
			if (FCConvert.ToString(rsType.Get_Fields_Int32("NumberDirectDeposits")) == string.Empty)
			{
				txtType.Text = string.Empty;
			}
			else if (rsType.Get_Fields_Int32("NumberDirectDeposits") != 0)
			{
				txtType.Text = "   D";
			}
			else if (rsType.Get_Fields_Int32("NumberChecks") != 0)
			{
				txtType.Text = "   R";
			}
			else if (rsType.Get_Fields_Int32("NumberBoth") != 0)
			{
				txtType.Text = "   B";
			}
			else
			{
				txtType.Text = string.Empty;
			}
			txtVoid.Text = (FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("CheckVoid")) ? "  Y" : "  N");
			txtCheckNumber.Text = FCConvert.ToString(rsMain.Get_Fields("CheckNumber"));
			double dblNet;
			// vbPorter upgrade warning: dblTax As double	OnWrite(int, string)
			double dblTax;
			// vbPorter upgrade warning: dblDeds As double	OnWrite(int, string)
			double dblDeds;
			double dblGross;
			dblNet = 0;
			dblTax = 0;
			dblDeds = 0;
			// txtAmount.Text = Format(rsMain.Fields("NetPay"), "0.00")
			// dblTotalAmount = dblTotalAmount + Format(rsMain.Fields("NetPay"), "0.00")
			dblTotal = 0;
			dblHoursTotal = 0;
			txtRegular.Text = Strings.Format(0, "0.00");
			dblGross = 0;
			while (!rsWages.EndOfFile())
			{
				if (FCConvert.ToInt32(rsWages.Get_Fields("ID")) == 1)
				{
					txtRegular.Text = Strings.Format(FCConvert.ToDouble(txtRegular.Text) + FCConvert.ToDouble(Strings.Format(rsWages.Get_Fields("SumOfDistGrossPay"), "0.00")), "0.00");
					dblTotalRegular += FCConvert.ToDouble(Strings.Format(rsWages.Get_Fields("SumOfDistGrossPay"), "0.00"));
					dblGross += Conversion.Val(rsWages.Get_Fields("SumOfDistGrossPay"));
				}
				else
				{
					dblTotal += rsWages.Get_Fields("SumOfDistGrossPay");
					dblGross += Conversion.Val(rsWages.Get_Fields("sumofdistgrosspay"));
				}
				if (FCConvert.ToString(rsWages.Get_Fields("Type")) == "Dollars")
				{
				}
				else
				{
					dblHoursTotal += rsWages.Get_Fields("SumOfDistHours");
				}
				rsWages.MoveNext();
			}
			txtOther.Text = Strings.Format(dblTotal, "0.00");
			dblTotalOther += FCConvert.ToDouble(Strings.Format(dblTotal, "0.00"));
			txtGross.Text = Strings.Format(dblGross, "0.00");
			dblTotalGross += FCConvert.ToDouble(Strings.Format(dblGross, "0.00"));
			if (!rsMain.EndOfFile())
			{
				txtTaxes.Text = Strings.Format(rsMain.Get_Fields("TotalTaxes"), "0.00");
				dblTax = FCConvert.ToDouble(Strings.Format(rsMain.Get_Fields("TotalTaxes"), "0.00"));
				dblTotalTaxes += FCConvert.ToDouble(Strings.Format(rsMain.Get_Fields("TotalTaxes"), "0.00"));
			}
			else
			{
				txtTaxes.Text = "0.00";
			}
			if (!rsDeductions.EndOfFile())
			{
				txtDeductions.Text = Strings.Format(rsDeductions.Get_Fields("SumOfDedAmount"), "0.00");
				dblDeds = FCConvert.ToDouble(Strings.Format(rsDeductions.Get_Fields("SumOfDedAmount"), "0.00"));
				dblTotalDeductions += FCConvert.ToDouble(Strings.Format(rsDeductions.Get_Fields("SumOfDedAmount"), "0.00"));
			}
			else
			{
				txtDeductions.Text = "0.00";
			}
			dblNet = dblGross - dblTax - dblDeds;
			if (dblNet < 0)
				dblNet = 0;
			txtAmount.Text = Strings.Format(dblNet, "0.00");
			dblTotalAmount += dblNet;
			txtVacUsed.Text = "0.00";
			txtSick.Text = "0.00";
			while (!rsVacSick.EndOfFile())
			{
				if (rsCodeType.FindFirstRecord("Description", "Sick"))
				{
					if (rsVacSick.Get_Fields_Int32("VSTypeID") == rsCodeType.Get_Fields("ID"))
					{
						txtSick.Text = Strings.Format(rsVacSick.Get_Fields("SumOfVSUsed"), "0.00");
						dblTotalSickUsed += FCConvert.ToDouble(Strings.Format(rsVacSick.Get_Fields("SumOfVSUsed"), "0.00"));
					}
				}
				else
				{
					txtSick.Text = "0";
				}
				if (rsCodeType.FindFirstRecord("Description", "Vacation"))
				{
					if (rsVacSick.Get_Fields_Int32("VSTypeID") == rsCodeType.Get_Fields("ID"))
					{
						txtVacUsed.Text = Strings.Format(rsVacSick.Get_Fields("SumOfVSUsed"), "0.00");
						dblTotalVacUsed += FCConvert.ToDouble(Strings.Format(rsVacSick.Get_Fields("SumOfVSUsed"), "0.00"));
					}
				}
				else
				{
					txtVacUsed.Text = "0";
				}
				rsVacSick.MoveNext();
			}
			txtHours.Text = Strings.Format(dblHoursTotal, "0.00");
			dblTotalHours += FCConvert.ToDouble(Strings.Format(dblHoursTotal, "0.00"));
			dblGroupAmount += FCConvert.ToDouble(txtAmount.Text);
			dblGroupRegular += FCConvert.ToDouble(txtRegular.Text);
			dblGroupOther += FCConvert.ToDouble(txtOther.Text);
			dblGroupGross = dblGroupRegular + dblGroupOther;
			dblGroupTaxes += FCConvert.ToDouble(txtTaxes.Text);
			dblGroupDeductions += FCConvert.ToDouble(txtDeductions.Text);
			dblGroupVacUsed += FCConvert.ToDouble(txtVacUsed.Text);
			// dblGroupSickUsed = dblGroupSickUsed + txtSickUsed
			dblGroupHours += FCConvert.ToDouble(txtHours.Text);
			if (!rsMaster.EndOfFile())
				rsMaster.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// If rsMaster.EndOfFile Then
			txtGroupAmount.Text = Strings.Format(dblGroupAmount, "0.00");
			txtGroupRegular.Text = Strings.Format(dblGroupRegular, "0.00");
			txtGroupOther.Text = Strings.Format(dblGroupOther, "0.00");
			txtGroupTaxes.Text = Strings.Format(dblGroupTaxes, "0.00");
			txtGroupDeductions.Text = Strings.Format(dblGroupDeductions, "0.00");
			txtGroupVacUsed.Text = Strings.Format(dblGroupVacUsed, "0.00");
			txtGroupSickUsed.Text = Strings.Format(dblGroupSickUsed, "0.00");
			txtGroupHours.Text = Strings.Format(dblGroupHours, "0.00");
			txtGroupGross.Text = Strings.Format(dblGroupGross, "0.00");
			// Else
			// txtGroupAmount = Format(dblGroupAmount - txtAmount, "0.00")
			// txtGroupRegular = Format(dblGroupRegular - txtRegular, "0.00")
			// txtGroupOther = Format(dblGroupOther - txtOther, "0.00")
			// txtGroupGross = Format(dblGroupGross - txtGross, "0.00")
			// txtGroupTaxes = Format(dblGroupTaxes - txtTaxes, "0.00")
			// txtGroupDeductions = Format(dblGroupDeductions - txtDeductions, "0.00")
			// txtGroupVacUsed = Format(dblGroupVacUsed - txtVacUsed, "0.00")
			// txtGroupSickUsed = Format(dblGroupSickUsed - txtSickUsed, "0.00")
			// txtGroupHours = Format(dblGroupHours - txtHours, "0.00")
			// End If
			dblGroupAmount = 0;
			dblGroupRegular = 0;
			dblGroupOther = 0;
			dblGroupTaxes = 0;
			dblGroupDeductions = 0;
			dblGroupVacUsed = 0;
			dblGroupSickUsed = 0;
			dblGroupHours = 0;
			dblGroupGross = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// If boolClearGroupTotals Then
			// dblGroupAmount = txtAmount
			// dblGroupRegular = txtRegular
			// dblGroupOther = txtOther
			// dblGroupTaxes = txtTaxes
			// dblGroupDeductions = txtDeductions
			// dblGroupVacUsed = txtVacUsed
			// dblGroupSickUsed = txtSickUsed
			// dblGroupHours = txtHours
			// dblGroupGross = txtGross
			// Else
			// boolClearGroupTotals = True
			// End If
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAmount.Text = Strings.Format(dblTotalAmount, "0.00");
			txtTotalRegular.Text = Strings.Format(dblTotalRegular, "0.00");
			txtTotalOther.Text = Strings.Format(dblTotalOther, "0.00");
			txtTotalTaxes.Text = Strings.Format(dblTotalTaxes, "0.00");
			txtTotalDeduction.Text = Strings.Format(dblTotalDeductions, "0.00");
			txtTotalVacUsed.Text = Strings.Format(dblTotalVacUsed, "0.00");
			txtTotalSickUsed.Text = Strings.Format(dblTotalSickUsed, "0.00");
			txtTotalHours.Text = Strings.Format(dblTotalHours, "0.00");
			txtTotGross.Text = Strings.Format(dblTotalGross, "0.00");
		}

		

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
