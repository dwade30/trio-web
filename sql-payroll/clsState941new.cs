//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsState941New
	{
		//=========================================================
		private bool boolC1;
		// if false the unemployment info is blank
		private int intQuarterCovered;
		private int lngYearCovered;
		private int intLastMonthInQuarter;
		private string strSequence = string.Empty;
		private DateTime dtM1Start;
		private DateTime dtM1End;
		private DateTime dtM2Start;
		private DateTime dtM2End;
		private DateTime dtM3Start;
		private DateTime dtM3End;
		private string strFileToSave = string.Empty;
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		clsDRWrapper clsEmployees = new clsDRWrapper();
		clsDRWrapper clsPayments = new clsDRWrapper();
		double dblCredit;
		double dblTotalExcess;
		double dblTotalWages;
		double dblTotalWH;
		double dblTotalGrossWages;
		double dblLimit;
		double dblRate;
		double dblCSSFRate;
		clsDRWrapper clsMonth1 = new clsDRWrapper();
		clsDRWrapper clsMonth2 = new clsDRWrapper();
		clsDRWrapper clsMonth3 = new clsDRWrapper();
		int lngFemsM1;
		int lngFemsM2;
		int lngFemsM3;
		int lngM1;
		int lngM2;
		int lngM3;
		double dblRemitted;
		int lngTotWithholdableEmployees;
		string strLastEmployee = "";

		public string FileName
		{
			set
			{
				strFileToSave = FileName;
			}
			get
			{
				string FileName = "";
				FileName = strFileToSave;
				return FileName;
			}
		}

		public bool IncludeC1
		{
			set
			{
				boolC1 = value;
			}
			get
			{
				bool IncludeC1 = false;
				IncludeC1 = boolC1;
				return IncludeC1;
			}
		}

		public DateTime Month1Start
		{
			set
			{
				dtM1Start = value;
			}
			get
			{
				DateTime Month1Start = System.DateTime.Now;
				Month1Start = dtM1Start;
				return Month1Start;
			}
		}

		public DateTime Month1End
		{
			set
			{
				dtM1End = value;
			}
			get
			{
				DateTime Month1End = System.DateTime.Now;
				Month1End = dtM1End;
				return Month1End;
			}
		}

		public DateTime Month2Start
		{
			set
			{
				dtM2Start = value;
			}
			get
			{
				DateTime Month2Start = System.DateTime.Now;
				Month2Start = dtM2Start;
				return Month2Start;
			}
		}

		public DateTime Month2End
		{
			set
			{
				dtM2End = value;
			}
			get
			{
				DateTime Month2End = System.DateTime.Now;
				Month2End = dtM2End;
				return Month2End;
			}
		}

		public DateTime Month3Start
		{
			set
			{
				dtM3Start = value;
			}
			get
			{
				DateTime Month3Start = System.DateTime.Now;
				Month3Start = dtM3Start;
				return Month3Start;
			}
		}

		public DateTime Month3End
		{
			set
			{
				dtM3End = value;
			}
			get
			{
				DateTime Month3End = System.DateTime.Now;
				Month3End = dtM3End;
				return Month3End;
			}
		}

		public int YearCovered
		{
			set
			{
				lngYearCovered = value;
			}
			get
			{
				int YearCovered = 0;
				YearCovered = lngYearCovered;
				return YearCovered;
			}
		}

		public int QuarterCovered
		{
			set
			{
				intQuarterCovered = value;
				switch (intQuarterCovered)
				{
					case 1:
						{
							intLastMonthInQuarter = 3;
							break;
						}
					case 2:
						{
							intLastMonthInQuarter = 6;
							break;
						}
					case 3:
						{
							intLastMonthInQuarter = 9;
							break;
						}
					case 4:
						{
							intLastMonthInQuarter = 12;
							break;
						}
				}
				//end switch
			}
			get
			{
				int QuarterCovered = 0;
				QuarterCovered = intQuarterCovered;
				return QuarterCovered;
			}
		}

		public string Sequence
		{
			set
			{
				strSequence = value;
			}
			get
			{
				string Sequence = "";
				Sequence = strSequence;
				return Sequence;
			}
		}

		public bool CreateFile()
		{
			bool CreateFile = false;
			CreateFile = MakeFile();
			return CreateFile;
		}

		private bool MakeFile()
		{
			bool MakeFile = false;
			FCFileSystem fso = new FCFileSystem();
			StreamWriter ts = null;
			bool boolFileOpened = false;
			string strRecord;
			int lngNumEmployeeRecords;
			int intCurrentQuarter;
			int lngCurrentYear;
			string strSQL = "";
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			string strQuery1;
			string strQuery2;
			string strQuery3;
			string strQuery4;
			string[] strAry = null;
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			string strRange = "";
			string strStateEmploymentQuery2;
			string strStateEmploymentQuery3;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeFile = false;
				boolFileOpened = false;
				LoadInfo();
				dblTotalWages = 0;
				dblTotalExcess = 0;
				lngFemsM1 = 0;
				lngFemsM2 = 0;
				lngFemsM3 = 0;
				dblTotalWH = 0;
				dblTotalGrossWages = 0;
				lngM1 = 0;
				lngM2 = 0;
				lngM3 = 0;
				lngCurrentYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
				if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
				{
					intCurrentQuarter = 1;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
				{
					intCurrentQuarter = 2;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
				{
					intCurrentQuarter = 3;
				}
				else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
				{
					intCurrentQuarter = 4;
				}
				strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) < 1)
				{
					// all or single
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				}
				else
				{
					// range
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
				}
				if (lngSeqStart >= 0)
				{
					strRange = " and seqnumber between " + FCConvert.ToString(lngSeqStart) + " and " + FCConvert.ToString(lngSeqEnd);
				}
				else
				{
					strRange = "";
				}
				boolFileOpened = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (boolC1)
				{
					strSQL = "select employeenumber from tblcheckdetail where checkvoid = 0 and (distu <> 'N' and distu <> '') and  paydate between '" + FCConvert.ToString(dtM1Start) + "' and '" + FCConvert.ToString(dtM1End) + "' group by employeenumber";
					clsMonth1.OpenRecordset(strSQL, "twpy0000.vb1");
					strSQL = "select employeenumber from tblcheckdetail where checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + FCConvert.ToString(dtM2Start) + "' and '" + FCConvert.ToString(dtM2End) + "' group by employeenumber";
					clsMonth2.OpenRecordset(strSQL, "twpy0000.vb1");
					strSQL = "select employeenumber from tblcheckdetail where checkvoid = 0 and (distu <> 'N' and distu <> '') and paydate between '" + FCConvert.ToString(dtM3Start) + "' and '" + FCConvert.ToString(dtM3End) + "' group by employeenumber";
					clsMonth3.OpenRecordset(strSQL, "twpy0000.vb1");
				}
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarterCovered, lngYearCovered);
				strSQL = "select count(employeenumber) as thecount from (SELECT EMPLOYEENUMBER FROM tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber having  sum(statetaxwh) > 0)";
				clsEmployees.OpenRecordset(strSQL, "twpy0000.vb1");
				lngTotWithholdableEmployees = 0;
				if (!clsEmployees.EndOfFile())
				{
					lngTotWithholdableEmployees = FCConvert.ToInt32(Math.Round(Conversion.Val(clsEmployees.Get_Fields("thecount"))));
				}
				strSQL = "select employeenumber as enum,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
				strStateEmploymentQuery2 = " (" + strSQL + ") as StateUnemploymentQuery2 ";
				strSQL = "select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where paydate between '" + "1/1/" + FCConvert.ToString(lngYearCovered) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber ";
				strStateEmploymentQuery3 = " (" + strSQL + ") as StateUnemploymentQuery3 ";
				strQuery1 = "(Select * from " + strStateEmploymentQuery3 + " inner join " + strStateEmploymentQuery2 + " on (stateunemploymentquery2.enum = stateunemploymentquery3.employeenum)) as C1Query1";
				strQuery2 = "(select employeenumber,sum(statetaxgross) as qtdTotalGross,sum(stateTaxWH) as qtdStateWH from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber having (sum(grosspay) > 0 or sum(statetaxwh) > 0) ) as C1Query2 ";
				strQuery3 = "(select * from " + strQuery2 + " left join " + strQuery1 + " on (c1query1.eNUM = c1query2.employeenumBER)) as C1Query3";
				strQuery4 = "(select * from " + strQuery3 + " left join (select employeenumber as enum,unemployment as seasonal from tblMiscUpdate) as C1Query4 on (c1query4.enum = c1query3.employeenumber)) as C1Query5 ";
				strSQL = "select *,TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery4 + " on (tblemployeemaster.employeenumber = c1query5.employeenumber) where not unemploymentexempt " + strRange + " order by tblemployeemaster.employeenumber";
				clsEmployees.OpenRecordset(strSQL, "twpy0000.vb1");
				// get rid of any existing file
				if (FCFileSystem.FileExists("rtnwage.txt"))
				{
					FCFileSystem.DeleteFile("rtnwage.txt");
				}
				// create the file
				ts = FCFileSystem.CreateTextFile("rtnwage.txt");
				// write the transmitter record (first record)
				strRecord = MakeTransmitterRecord();
				if (strRecord == "")
				{
					ts.Close();
					return MakeFile;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write the authorization record
				strRecord = MakeAuthorizationRecord();
				if (strRecord == "")
				{
					ts.Close();
					return MakeFile;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write employer record
				strRecord = MakeEmployerRecord();
				if (strRecord == "")
				{
					ts.Close();
					return MakeFile;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write employee records
				lngNumEmployeeRecords = 0;
				while (!clsEmployees.EndOfFile())
				{
					//App.DoEvents();
					strRecord = MakeEmployeeRecord();
					if (strRecord == "")
					{
						ts.Close();
						return MakeFile;
					}
					ts.WriteLine(strRecord);
					lngNumEmployeeRecords += 1;
					clsEmployees.MoveNext();
					// frmWait.IncrementProgress
					//App.DoEvents();
				}
				// write total record
				strRecord = MakeTotalRecord(ref lngNumEmployeeRecords);
				if (strRecord == "")
				{
					ts.Close();
					return MakeFile;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write reconciliation record(s)
				// strSQL = "select * from tblStatePayments where paid > 0 order by datepaid"
				strSQL = "select * from tblStatePayments where paid > 0 or withheld > 0 order by datepaid";
				clsPayments.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsPayments.EndOfFile())
				{
					//App.DoEvents();
					strRecord = MakeReconciliationRecord();
					if (strRecord == "")
					{
						ts.Close();
						return MakeFile;
					}
					ts.WriteLine(strRecord);
					clsPayments.MoveNext();
					// frmWait.IncrementProgress
					//App.DoEvents();
				}
				// write final record
				strRecord = MakeFinalRecord(ref lngNumEmployeeRecords);
				if (strRecord == "")
				{
					ts.Close();
					return MakeFile;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// close the file
				ts.Close();
				boolFileOpened = false;
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				ts = null;
				MakeFile = true;
				return MakeFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpened)
				{
					ts.Close();
				}
				if (fecherFoundation.Information.Err(ex).Number == 52)
				{
					MessageBox.Show("Bad file name" + "\r\n" + "Make sure there are no illegal characters in the name such as / or *", "Bad File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return MakeFile;
		}

		private string MakeAuthorizationRecord()
		{
			string MakeAuthorizationRecord = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strOut;
				MakeAuthorizationRecord = "";
				strOut = "B";
				// this is an authorization record
				strOut += Strings.Format(lngYearCovered, "0000");
				// current year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += Strings.StrDup(8, " ");
				// used on tapes and magnetic reels only
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += " ";
				// space
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += "ASC";
				// in ascii format not ebcdic
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += Strings.StrDup(2, " ");
				// used on tapes and magnetic reels only
				strOut += "WITH";
				strOut += Strings.StrDup(108, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRReturnNameLen, " "), 1, modCoreysSweeterCode.EWRReturnNameLen);
				// company to return to
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRReturnAddressLen, " "), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRReturnCityLen, " "), 1, modCoreysSweeterCode.EWRReturnCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + "  ", 1, 2);
				strOut += Strings.StrDup(5, " ");
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					strOut += "-" + Strings.Format(EWRRecord.EmployerZip4, "0000");
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.StrDup(13, " ");
				MakeAuthorizationRecord = strOut;
				return MakeAuthorizationRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeAuthorizationRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeAuthorizationRecord;
		}

		private string MakeEmployeeRecord()
		{
			string MakeEmployeeRecord = "";
			string strOut = "";
			double dblTotal = 0;
			// vbPorter upgrade warning: dblExcess As double	OnWrite(Decimal, int)
			double dblExcess = 0;
			double dblTemp = 0;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeEmployeeRecord = "";
				strOut = "S";
				// employeerecord
				strOut += Strings.Format(FCConvert.ToString(clsEmployees.Get_Fields("ssn")).Replace("-", ""), "000000000");
				// employees social security number
				strOut += Strings.Mid(clsEmployees.Get_Fields("lastname") + Strings.StrDup(20, " "), 1, 20);
				strOut += Strings.Mid(clsEmployees.Get_Fields("firstname") + Strings.StrDup(12, " "), 1, 12);
				strOut += " ";
				// middle initial
				strOut += Strings.Format(EWRRecord.EmployerStateCode, "00");
				// state code
				// MATTHEW 10/08/2003
				// strOut = strOut & Format(intLastMonthInQuarter, "00") & Format(lngYearcovered, "yyyy")  'reporting year and quarter
				strOut += Strings.Format(intLastMonthInQuarter, "00") + FCConvert.ToString(lngYearCovered);
				// reporting year and quarter
				strOut += Strings.StrDup(12, " ");
				if (boolC1)
				{
					dblTotal = Conversion.Val(clsEmployees.Get_Fields("qtdgrosspay"));
					dblExcess = FCConvert.ToDouble(modDavesSweetCode.CalculateExcess(Conversion.Val(clsEmployees.Get_Fields("ytdgrosspay")), FCConvert.ToDecimal(dblTotal), FCConvert.ToDecimal(dblLimit)));
					dblTotalExcess += dblExcess;
					dblTotalWages += dblTotal;
				}
				else
				{
					dblTotal = 0;
					dblExcess = 0;
					dblTotalExcess = 0;
					dblTotalWages = 0;
				}
				strOut += Strings.Format(dblTotal * 100, "00000000000000");
				strOut += Strings.Format(dblExcess * 100, "00000000000000");
				strOut += Strings.Format((dblTotal - dblExcess) * 100, "00000000000000");
				strOut += Strings.StrDup(37, " ");
				// spaces
				strOut += "WITH";
				if (boolC1)
				{
					strOut += EWRRecord.StateUCAccount;
				}
				else
				{
					strOut += Strings.StrDup(10, " ");
				}
				strOut += Strings.StrDup(20, " ");
				// spaces
				dblTemp = Conversion.Val(clsEmployees.Get_Fields("qtdtotalgross"));
				dblTotalGrossWages += dblTemp;
				strOut += Strings.Format(dblTemp * 100, "00000000000000");
				dblTemp = Conversion.Val(clsEmployees.Get_Fields("qtdstatewh"));
				dblTotalWH += dblTemp;
				strOut += Strings.Format(dblTemp * 100, "00000000000000");
				if (FCConvert.ToString(clsEmployees.Get_Fields_Boolean("seasonal")) == "S")
				{
					strOut += "S";
				}
				else
				{
					strOut += "N";
				}
				strOut += Strings.StrDup(5, " ");
				strOut += "0";
				// not mandatory
				strTemp = fecherFoundation.Strings.UCase(Strings.Left(clsEmployees.Get_Fields("sex") + " ", 1));
				if (boolC1)
				{
					if (!clsMonth1.EndOfFile())
					{
						if (clsMonth1.FindFirstRecord("employeenumber", clsEmployees.Get_Fields("employeenumber")))
						{
							strOut += "1";
							if (fecherFoundation.Strings.Trim(strLastEmployee) != fecherFoundation.Strings.Trim(FCConvert.ToString(clsEmployees.Get_Fields("employeenumber"))))
							{
								lngM1 += 1;
								if (strTemp == "F")
									lngFemsM1 += 1;
							}
						}
						else
						{
							clsMonth1.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsMonth2.EndOfFile())
					{
						if (clsMonth2.FindFirstRecord("employeenumber", clsEmployees.Get_Fields("employeenumber")))
						{
							strOut += "1";
							if (strLastEmployee != FCConvert.ToString(clsEmployees.Get_Fields("employeenumber")))
							{
								lngM2 += 1;
								if (strTemp == "F")
									lngFemsM2 += 1;
							}
						}
						else
						{
							clsMonth2.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
					if (!clsMonth3.EndOfFile())
					{
						if (clsMonth3.FindFirstRecord("employeenumber", clsEmployees.Get_Fields("employeenumber")))
						{
							strOut += "1";
							if (strLastEmployee != FCConvert.ToString(clsEmployees.Get_Fields("employeenumber")))
							{
								lngM3 += 1;
								if (strTemp == "F")
									lngFemsM3 += 1;
							}
						}
						else
						{
							clsMonth3.MoveFirst();
							strOut += "0";
						}
					}
					else
					{
						strOut += "0";
					}
				}
				else
				{
					strOut += "0";
					strOut += "0";
					strOut += "0";
				}
				strLastEmployee = FCConvert.ToString(clsEmployees.Get_Fields("employeenumber"));
				strOut += EWRRecord.MRSWithholdingID;
				if (boolC1)
				{
					if (strTemp == "F")
					{
						strOut += "1";
					}
					else
					{
						strOut += "0";
					}
				}
				else
				{
					strOut += " ";
				}
				strOut += Strings.StrDup(8, "0");
				strOut += Strings.StrDup(8, "0");
				strOut += Strings.StrDup(33, " ");
				MakeEmployeeRecord = strOut;
				return MakeEmployeeRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeEmployeeRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeEmployeeRecord;
		}

		private string MakeEmployerRecord()
		{
			string MakeEmployerRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeEmployerRecord = "";
				strOut = "E";
				// employer record
				strOut += Strings.Format(lngYearCovered, "0000");
				// current year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += Strings.StrDup(9, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRAddressLen, " "), 1, modCoreysSweeterCode.EWRAddressLen);
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRCityLen, " "), 1, modCoreysSweeterCode.EWRCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + Strings.StrDup(2, " "), 1, 2);
				strOut += Strings.StrDup(8, " ");
				// spaces
				// zip and zip4 are backwards in this record.  Is this a typo in the specs?
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					strOut += "-" + Strings.Format(EWRRecord.EmployerZip4, "0000");
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				strOut += Strings.StrDup(8, " ");
				// spaces
				strOut += "WITH";
				strOut += Strings.Format(EWRRecord.EmployerStateCode, "00");
				if (boolC1)
				{
					strOut += EWRRecord.StateUCAccount;
					// already forced to 10
				}
				else
				{
					strOut += Strings.StrDup(10, " ");
				}
				strOut += Strings.StrDup(5, " ");
				// spaces
				strOut += Strings.Format(intLastMonthInQuarter, "00");
				// last month in period covered
				if (!clsEmployees.EndOfFile())
				{
					strOut += "1";
					// 0 is not to be followed by employee records
				}
				else
				{
					strOut += "0";
				}
				// corey 11/16/05 additions to specs was just 67 blanks
				// strOut = strOut & String(67, " ")   'spaces
				strOut += Strings.StrDup(18, " ");
				// spaces
				strOut += Strings.StrDup(9, "0");
				// preparer EIN, only if using a paid preparer
				strOut += Strings.Left(EWRRecord.ProcessorLicenseCode + Strings.StrDup(7, " "), 7);
				// processor license code
				strOut += Strings.Format(lngTotWithholdableEmployees, "0000");
				// total # of employees subject to withholding
				strOut += Strings.StrDup(29, " ");
				// spaces
				strOut += EWRRecord.MRSWithholdingID;
				// already forced to 11
				strOut += Strings.StrDup(7, " ");
				// spaces
				MakeEmployerRecord = strOut;
				return MakeEmployerRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeEmployerRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeEmployerRecord;
		}

		private string MakeFinalRecord(ref int lngNumEmployeeRecords)
		{
			string MakeFinalRecord = "";
			string strOut;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeFinalRecord = "";
				strOut = "F";
				// final record
				strOut += Strings.Format(lngNumEmployeeRecords, "0000000000");
				strOut += Strings.Format(1, "0000000000");
				// total number of E records
				strOut += "WITH";
				strOut += Strings.StrDup(15, " ");
				if (boolC1)
				{
					dblTemp = dblTotalWages * 100;
				}
				else
				{
					dblTemp = 0;
				}
				strOut += Strings.Format(dblTemp, "000000000000000");
				strOut += Strings.StrDup(220, " ");
				MakeFinalRecord = strOut;
				return MakeFinalRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeFinalRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeFinalRecord;
		}

		private string MakeReconciliationRecord()
		{
			string MakeReconciliationRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeReconciliationRecord = "";
				strOut = "R";
				// reconciliation record
				strOut += Strings.Format(clsPayments.Get_Fields_DateTime("DatePaid"), "MMddyyyy");
				strOut += Strings.Format((clsPayments.Get_Fields("withheld") * 100), "000000000");
				strOut += Strings.Format(Conversion.Val(clsPayments.Get_Fields("paid")) * 100, "000000000");
				strOut += EWRRecord.StateUCAccount;
				strOut += Strings.StrDup(238, " ");
				MakeReconciliationRecord = strOut;
				return MakeReconciliationRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeReconciliationRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeReconciliationRecord;
		}

		private string MakeTotalRecord(ref int lngNumEmployeeRecords)
		{
			string MakeTotalRecord = "";
			string strOut;
			clsDRWrapper clsLoad = new clsDRWrapper();
			double dblIncomeDue;
			// vbPorter upgrade warning: dblUCContributionsDue As double	OnWrite(string, int)
			double dblUCContributionsDue = 0;
			double dblVoucherPayments = 0;
			// vbPorter upgrade warning: dblCSSFAssessment As double	OnWrite(string, int)
			double dblCSSFAssessment = 0;
			// vbPorter upgrade warning: dblTotalContributionsDue As double	OnWrite(string, int)
			double dblTotalContributionsDue = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeTotalRecord = "";
				strOut = "T";
				// total record
				strOut += Strings.Format(lngNumEmployeeRecords, "0000000");
				strOut += "WITH";
				if (boolC1)
				{
					strOut += EWRRecord.StateUCAccount;
					strOut += Strings.StrDup(4, " ");
					// spaces
					strOut += Strings.Format(dblTotalWages * 100, "00000000000000");
					strOut += Strings.Format(dblTotalExcess * 100, "00000000000000");
					strOut += Strings.Format((dblTotalWages - dblTotalExcess) * 100, "00000000000000");
					strOut += Strings.StrDup(19, " ");
					strOut += Strings.Format(FCConvert.ToInt32(Strings.Format((dblTotalWages - dblTotalExcess) * dblRate, "0.00")) * 100, "0000000000000");
					// total due
					dblUCContributionsDue = FCConvert.ToDouble(Strings.Format((dblTotalWages - dblTotalExcess) * dblRate, "0.00"));
					dblCSSFAssessment = FCConvert.ToDouble(Strings.Format((dblTotalWages - dblTotalExcess) * dblCSSFRate, "0.00"));
					dblTotalContributionsDue = FCConvert.ToDouble(Strings.Format(dblUCContributionsDue + dblCSSFAssessment, "0.00"));
					strOut += Strings.Format(dblUCContributionsDue * 100, "0000000000000");
					// total due
				}
				else
				{
					strOut += Strings.StrDup(10, " ");
					strOut += Strings.StrDup(4, " ");
					strOut += "00000000000000";
					strOut += "00000000000000";
					strOut += "00000000000000";
					strOut += Strings.StrDup(19, " ");
					strOut += "0000000000000";
					dblUCContributionsDue = 0;
					dblCSSFAssessment = 0;
					dblTotalContributionsDue = 0;
				}
				// strOut = strOut & String(11, " ")
				strOut += Strings.Format(dblCSSFAssessment * 100, "00000000000");
				// payments made
				clsLoad.OpenRecordset("select sum(paid) as totalpaid from tblStatePayments", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblVoucherPayments = Conversion.Val(clsLoad.Get_Fields("totalpaid"));
				}
				else
				{
					dblVoucherPayments = 0;
				}
				strOut += Strings.Format(dblVoucherPayments * 100, "00000000000");
				dblIncomeDue = dblTotalWH - (dblCredit + dblVoucherPayments);
				if (dblIncomeDue < 0)
				{
					// strOut = strOut & "00000000000"
					strOut += Strings.Format(dblIncomeDue * 100, "0000000000");
				}
				else
				{
					strOut += Strings.Format(dblIncomeDue * 100, "00000000000");
				}
				strOut += Strings.StrDup(11, " ");
				strOut += Strings.Format(10000 * dblRate, "0000");
				// strOut = strOut & String(4, "0")
				// strOut = strOut & String(4, "0")
				strOut += Strings.Format(10000 * dblCSSFRate, "0000");
				strOut += Strings.StrDup(22, " ");
				// amount due
				if (dblIncomeDue + dblTotalContributionsDue < 0)
				{
					// strOut = strOut & "00000000000"
					strOut += Strings.Format((dblIncomeDue + dblUCContributionsDue) * 100, "0000000000");
				}
				else
				{
					strOut += Strings.Format((dblIncomeDue + dblUCContributionsDue) * 100, "00000000000");
				}
				// corey 02082005
				// state doesn't want remitted anymore, just zeroes
				strOut += "0000000000000";
				// strOut = strOut & Format(dblRemitted * 100, "0000000000000")
				strOut += Strings.Format(dblTotalGrossWages * 100, "00000000000000");
				strOut += Strings.Format(dblTotalWH * 100, "00000000000000");
				if (boolC1)
				{
					strOut += Strings.Format(lngM1, "0000000");
					strOut += Strings.Format(lngM2, "0000000");
					strOut += Strings.Format(lngM3, "0000000");
					strOut += Strings.Format(lngFemsM1, "0000000");
					strOut += Strings.Format(lngFemsM2, "0000000");
					strOut += Strings.Format(lngFemsM3, "0000000");
				}
				else
				{
					strOut += "0000000";
					strOut += "0000000";
					strOut += "0000000";
					strOut += "0000000";
					strOut += "0000000";
					strOut += "0000000";
				}
				strOut += Strings.StrDup(7, " ");
				MakeTotalRecord = strOut;
				return MakeTotalRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeTotalRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeTotalRecord;
		}

		private string MakeTransmitterRecord()
		{
			string MakeTransmitterRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeTransmitterRecord = "";
				strOut = "A";
				strOut += Strings.Format(lngYearCovered, "0000");
				// four digit year
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += "WITH";
				strOut += Strings.StrDup(5, " ");
				// spaces
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				// transmitting company name
				strOut += Strings.Mid(EWRRecord.EmployerAddress + Strings.StrDup(modCoreysSweeterCode.EWRAddressLen, " "), 1, modCoreysSweeterCode.EWRAddressLen);
				// transmitting company street address
				strOut += Strings.Mid(EWRRecord.EmployerCity + Strings.StrDup(modCoreysSweeterCode.EWRCityLen, " "), 1, modCoreysSweeterCode.EWRCityLen);
				strOut += Strings.Mid(EWRRecord.EmployerState + "  ", 1, 2);
				strOut += Strings.StrDup(13, " ");
				// spaces
				strOut += Strings.Format(EWRRecord.EmployerZip, "00000");
				int intValue;
				if (fecherFoundation.Strings.Trim(EWRRecord.EmployerZip4) != string.Empty)
				{
					intValue = FCConvert.ToInt32(EWRRecord.EmployerZip4);
					strOut += "-" + FCConvert.ToString(modGlobalRoutines.PadToString(intValue, 4));
					EWRRecord.EmployerZip4 = FCConvert.ToString(intValue);
				}
				else
				{
					strOut += Strings.StrDup(5, " ");
				}
				strOut += Strings.Mid(EWRRecord.TransmitterTitle + Strings.StrDup(modCoreysSweeterCode.EWRTransmitterTitleLen, " "), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				// person responsible for accuracy of this report
				strOut += Strings.Format(EWRRecord.TransmitterPhone, "0000000000");
				intValue = FCConvert.ToInt32(EWRRecord.TransmitterExt);
				strOut += FCConvert.ToString(modGlobalRoutines.PadToString(intValue, 4));
				EWRRecord.TransmitterExt = FCConvert.ToString(intValue);
				strOut += Strings.StrDup(68, " ");
				MakeTransmitterRecord = strOut;
				return MakeTransmitterRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeTransmitterRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeTransmitterRecord;
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			if (boolC1)
			{
				clsLoad.OpenRecordset("select unemploymentrate,unemploymentbase from tblStandardLimits", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblRate = Conversion.Val(clsLoad.Get_Fields("unemploymentrate")) / 100;
					dblLimit = Conversion.Val(clsLoad.Get_Fields("unemploymentbase"));
				}
				else
				{
					dblRate = 0;
					dblLimit = 7000;
				}
			}
			else
			{
				dblRate = 0;
				dblLimit = 0;
			}
			strSQL = "select * from tblemployerinfo";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
				EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
				EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
				EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
				EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
				EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
				EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
				EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
				EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
				EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
				EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
				EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
				EWRRecord.ProcessorLicenseCode = FCConvert.ToString(clsLoad.Get_Fields_String("ProcessorLicenseCode"));
			}
			else
			{
				EWRRecord.EmployerAddress = "";
				EWRRecord.EmployerCity = "";
				EWRRecord.EmployerName = "";
				EWRRecord.EmployerState = "ME";
				EWRRecord.EmployerStateCode = 23;
				EWRRecord.EmployerZip = "";
				EWRRecord.EmployerZip4 = "";
				EWRRecord.FederalEmployerID = "";
				EWRRecord.MRSWithholdingID = "";
				EWRRecord.StateUCAccount = "";
				EWRRecord.TransmitterExt = "";
				EWRRecord.TransmitterPhone = "0000000000";
				EWRRecord.TransmitterTitle = "";
			}
		}
	}
}
