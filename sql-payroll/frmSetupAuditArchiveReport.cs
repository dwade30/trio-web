//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmSetupAuditArchiveReport : BaseForm
	{
		public frmSetupAuditArchiveReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSetupAuditArchiveReport InstancePtr
		{
			get
			{
				return (frmSetupAuditArchiveReport)Sys.GetInstance(typeof(frmSetupAuditArchiveReport));
			}
		}

		protected frmSetupAuditArchiveReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cboPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			if (cboPayDate.SelectedIndex >= 0)
			{
				// rsPayDateInfo.OpenRecordset "SELECT DISTINCT UserField3 FROM AuditChangesArchive WHERE isdate(isnull(userfield2, '')) and CDate(isnull(userfield2, '')) = '" & CDate(cboPayDate.Text) & "' ORDER BY UserField3"
				rsPayDateInfo.OpenRecordset("SELECT DISTINCT UserField3 FROM AuditChangesArchive WHERE isdate(isnull(userfield2, '')) = 1 and (isnull(userfield2, '')) = '" + Strings.Format(fecherFoundation.DateAndTime.DateValue(cboPayDate.Text), "m/d/yyyy") + "' ORDER BY UserField3", "Payroll");
				cboPayRunID.Clear();
				if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
				{
					do
					{
						cboPayRunID.AddItem(rsPayDateInfo.Get_Fields_String("UserField3"));
						rsPayDateInfo.MoveNext();
					}
					while (rsPayDateInfo.EndOfFile() != true);
					cboPayRunID.SelectedIndex = 0;
				}
			}
		}

		private void frmSetupAuditArchiveReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAuditArchiveReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAuditArchiveReport properties;
			//frmSetupAuditArchiveReport.FillStyle	= 0;
			//frmSetupAuditArchiveReport.ScaleWidth	= 9045;
			//frmSetupAuditArchiveReport.ScaleHeight	= 7335;
			//frmSetupAuditArchiveReport.LinkTopic	= "Form2";
			//frmSetupAuditArchiveReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadEmployeeCombo();
			LoadScreenCombo();
			LoadPayDateCombo();
		}

		private void frmSetupAuditArchiveReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.Text == "Date Range")
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtLowDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptAuditArchiveReport.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.Text == "Date Range")
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtLowDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			rptAuditArchiveReport.InstancePtr.PrintReport(true);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadEmployeeCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
			cboEmployee.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField1 FROM AuditChangesArchive ORDER BY UserField1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsEmployeeInfo.OpenRecordset("SELECT * FROM tblEmployeeMaster");
				do
				{
					if (rsEmployeeInfo.FindFirstRecord("EmployeeNumber", FCConvert.ToString(rsInfo.Get_Fields_String("UserField1")).Replace("'", "''")))
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) != "")
						{
							cboEmployee.AddItem(fecherFoundation.Strings.Trim(rsInfo.Get_Fields_String("UserField1") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig")))));
						}
						else
						{
							cboEmployee.AddItem(fecherFoundation.Strings.Trim(rsInfo.Get_Fields_String("UserField1") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig")))));
						}
					}
					else
					{
						cboEmployee.AddItem(rsInfo.Get_Fields_String("UserField1") + " - UNKNOWN");
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadScreenCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboScreen.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT Location FROM AuditChangesArchive ORDER BY Location");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboScreen.AddItem(FCConvert.ToString(rsInfo.Get_Fields("Location")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadPayDateCombo()
		{
			clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			rsPayDateInfo.OpenRecordset("SELECT DISTINCT convert(datetime, UserField2) as PayDate FROM AuditChangesArchive where isdate(userfield2) = 1 ORDER BY convert(datetime, UserField2) DESC");
			cboPayDate.Clear();
			cboPayRunID.Clear();
			if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPayDate.AddItem(Strings.Format(fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(rsPayDateInfo.Get_Fields_DateTime("PayDate"))), "MM/dd/yyyy"));
					rsPayDateInfo.MoveNext();
				}
				while (rsPayDateInfo.EndOfFile() != true);
				cboPayDate.SelectedIndex = 0;
			}
		}
		private void cmbDateAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (cmbDateAll.SelectedIndex)
			{
				case 0:
					optDateAll_CheckedChanged(sender, EventArgs.Empty);
					break;
				case 1:
                    optDateRange_CheckedChanged(sender, EventArgs.Empty);
                    break;
				case 2:
					 optPayDate_CheckedChanged(sender, EventArgs.Empty);
                    break;
			}
		}

		private void optDateAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			Label2.Enabled = false;
			txtLowDate.Enabled = false;
			txtHighDate.Enabled = false;
			txtLowDate.Text = "";
			txtHighDate.Text = "";
			fraPayDate.Enabled = false;
			cboPayDate.Enabled = false;
			cboPayRunID.Enabled = false;
			cboPayDate.SelectedIndex = -1;
			cboPayRunID.SelectedIndex = -1;
		}

		private void optDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			Label2.Enabled = true;
			txtLowDate.Enabled = true;
			txtHighDate.Enabled = true;
			fraPayDate.Enabled = false;
			cboPayDate.Enabled = false;
			cboPayRunID.Enabled = false;
			cboPayDate.SelectedIndex = -1;
			cboPayRunID.SelectedIndex = -1;
		}

		private void cmbEmployeeAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch(cmbEmployeeAll.SelectedIndex)
			{
				case 0:
					optEmployeeAll_CheckedChanged(sender, EventArgs.Empty);
					break;
				case 1:
					optEmployeeSelected_CheckedChanged(sender, EventArgs.Empty);
					break;
			}
		}

		private void optEmployeeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cboEmployee.Enabled = false;
			cboEmployee.SelectedIndex = -1;
		}

		private void optEmployeeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			cboEmployee.Enabled = true;
			if (cboEmployee.Items.Count > 0)
			{
				cboEmployee.SelectedIndex = 0;
			}
		}

		private void optPayDate_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			Label2.Enabled = false;
			txtLowDate.Enabled = false;
			txtHighDate.Enabled = false;
			txtLowDate.Text = "";
			txtHighDate.Text = "";
			fraPayDate.Enabled = true;
			cboPayDate.Enabled = true;
			cboPayRunID.Enabled = true;
			if (cboPayDate.Items.Count > 0)
			{
				cboPayDate.SelectedIndex = 0;
			}
		}

		private void cmbScreenAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (cmbScreenAll.SelectedIndex)
			{
				case 0:
					optScreenAll_CheckedChanged(sender, EventArgs.Empty);
					break;
				case 1:
					optScreenSelected_CheckedChanged(sender, EventArgs.Empty);
					break;
			}
		}

		private void optScreenAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cboScreen.Enabled = false;
			cboScreen.SelectedIndex = -1;
		}

		private void optScreenSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			cboScreen.Enabled = true;
			if (cboScreen.Items.Count > 0)
			{
				cboScreen.SelectedIndex = 0;
			}
		}
	}
}
