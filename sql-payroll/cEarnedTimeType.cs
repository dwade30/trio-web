﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cEarnedTimeType
	{
		//=========================================================
		private int lngIDNumber;
		private string strDescription = string.Empty;

		public int ID
		{
			set
			{
				lngIDNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngIDNumber;
				return ID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}
	}
}
