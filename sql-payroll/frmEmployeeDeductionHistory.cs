//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmployeeDeductionHistory : BaseForm
	{
		public frmEmployeeDeductionHistory()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeeDeductionHistory InstancePtr
		{
			get
			{
				return (frmEmployeeDeductionHistory)Sys.GetInstance(typeof(frmEmployeeDeductionHistory));
			}
		}

		protected frmEmployeeDeductionHistory _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 04,2001
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbHistory       As DAO.Database
		private clsDRWrapper rsHistory = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private string strSortField = string.Empty;

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the frequency code report
				// rptHistory.Show
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductionHistory_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductionHistory_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployeeDeductionHistory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmployeeDeductionHistory properties;
			//frmEmployeeDeductionHistory.ScaleWidth	= 9045;
			//frmEmployeeDeductionHistory.ScaleHeight	= 7170;
			//frmEmployeeDeductionHistory.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsHistory.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				strSortField = "LastUpdate";
				// Load the grid with data
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsHistory.OpenRecordset("Select * from tblEmployeeDeductionHistory where EmployeeNumber = " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + " Order by " + strSortField + " Desc", "TWPY0000.vb1");
				if (!rsHistory.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsHistory.MoveLast();
					rsHistory.MoveFirst();
					// set the number of rows in the grid
					vsHistory.Rows = rsHistory.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsHistory.RecordCount()); intCounter++)
					{
						vsHistory.TextMatrix(intCounter, 0, fecherFoundation.Strings.Trim(rsHistory.Get_Fields_Int32("RecordNumber") + " "));
						vsHistory.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsHistory.Get_Fields_String("FieldName") + " "));
						vsHistory.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsHistory.Get_Fields_String("OldValue") + " "));
						vsHistory.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsHistory.Get_Fields_String("NewValue") + " "));
						vsHistory.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsHistory.Get_Fields("LastUserID") + " "));
						vsHistory.TextMatrix(intCounter, 5, fecherFoundation.Strings.Trim(FCConvert.ToString(rsHistory.Get_Fields_DateTime("LastUpdate")) + " "));
						vsHistory.TextMatrix(intCounter, 6, fecherFoundation.Strings.Trim(rsHistory.Get_Fields("ID") + " "));
						rsHistory.MoveNext();
					}
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsHistory.WordWrap = true;
				vsHistory.Cols = 7;
				vsHistory.ColHidden(6, true);
				modGlobalRoutines.CenterGridCaptions(ref vsHistory);
				//vsHistory.RowHeight(0) *= 2;
				// set column 0 properties
				vsHistory.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(0, 800);
				vsHistory.TextMatrix(0, 0, "Emp Ded Number");
				// set column 1 properties
				vsHistory.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(1, 1500);
				vsHistory.TextMatrix(0, 1, "Field Name");
				// set column 2 properties
				vsHistory.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(2, 1600);
				vsHistory.TextMatrix(0, 2, "Old Value");
				// set column 3 properties
				vsHistory.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(3, 1600);
				vsHistory.TextMatrix(0, 3, "New Value");
				// set column 4 properties
				vsHistory.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(4, 1500);
				vsHistory.TextMatrix(0, 4, "Changed By");
				// set column 5 properties
				vsHistory.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsHistory.ColWidth(5, 1500);
				vsHistory.TextMatrix(0, 5, "Change Date");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDeductionHistory_Resize(object sender, System.EventArgs e)
		{
			vsHistory.RowHeight(0, 600);
			vsHistory.ColWidth(0, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.11));
			vsHistory.ColWidth(1, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.17));
			vsHistory.ColWidth(2, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.17));
			vsHistory.ColWidth(3, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.17));
			vsHistory.ColWidth(4, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.17));
			vsHistory.ColWidth(5, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.17));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set focus back to the menu options of the MDIParent
				// CallByName MDIParent, "Grid_GotFocus", VbMethod
				frmEmployeeDeductions.InstancePtr.Show();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				for (intCounter = 0; intCounter <= (vsHistory.SelectedRows.Count - 1); intCounter++)
				{
					rsHistory.Execute("Delete from tblEmployeeDeductionHistory where ID = " + vsHistory.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 6), "Payroll");
				}
				// increment the counter as to how many records are dirty
				intDataChanged -= 1;
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void vsHistory_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsHistory.MouseRow == 0)
			{
				switch (vsHistory.MouseCol)
				{
					case 0:
						{
							strSortField = "RecordNumber";
							break;
						}
					case 1:
						{
							strSortField = "FieldName";
							break;
						}
					case 2:
						{
							strSortField = "OldValue";
							break;
						}
					case 3:
						{
							strSortField = "NewValue";
							break;
						}
					case 4:
						{
							strSortField = "LastUserID";
							break;
						}
					case 5:
						{
							strSortField = "LastUpdate";
							break;
						}
				}
				//end switch
				LoadGrid();
			}
		}

		private void vsHistory_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsHistory_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// select all of the data in the new cell
				vsHistory.EditCell();
				vsHistory.EditSelStart = 0;
				vsHistory.EditSelLength = FCConvert.ToString(vsHistory.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsHistory.Row, vsHistory.Col)).Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
	}
}
