//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	public partial class frmEmployersMatch : BaseForm
	{
		public frmEmployersMatch()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployersMatch InstancePtr
		{
			get
			{
				return (frmEmployersMatch)Sys.GetInstance(typeof(frmEmployersMatch));
			}
		}

		protected frmEmployersMatch _InstancePtr = null;
		//=========================================================
		// ************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 09,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		//
		// NOTES: The variable boolFromAccount is used in conjunction with
		// the Account OCX control. We needed the TAB key to function as it
		// it does as if the user was just in a cell in the grid and tab to
		// the next column. Since the TAB key is not a trapable key we needed
		// another way to complete this functionality. This variable is set to
		// TRUE whenever the OCX control gains focus. Whenever the user looses
		// focus from the control we force focus back to the calling grid and
		// check this variable. If control came from the OCX we move to the next
		// column. If not, we process the key stroke. ***If there is a better
		// way to handle this I am all ears*** Matthew 6/6/01
		//
		// **********************************************************************
		const int CNSTGRIDTOTALCOLLTD = 9;
		// private local variables
		// Private dbMatch             As DAO.Database
		private clsDRWrapper rsMatch = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		private FCRecordset rsTotals = null;
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private bool boolSkip;
		// vbPorter upgrade warning: intRowNumber As int	OnWriteFCConvert.ToInt32(
		private int intRowNumber;
		public bool boolFromMatch;
		private double dblAmtPercentTotal;
		// vbPorter upgrade warning: intChildID As int	OnRead(string)
		private int intChildID;
		private clsGridAccount clsGA = new clsGridAccount();
		// see form comments
		private bool boolFromAccount;
		private bool boolCantEdit;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void frmEmployersMatch_Resize(object sender, System.EventArgs e)
		{
			vsMatch.Cols = 13;
			vsMatch.ColWidth(0, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(1, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(2, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.07));
			vsMatch.ColWidth(3, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.21));
			vsMatch.ColWidth(4, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.08));
			vsMatch.ColWidth(5, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.06));
			vsMatch.ColWidth(6, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.19));
			vsMatch.ColWidth(7, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.13));
			vsMatch.ColWidth(8, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.07));
			vsMatch.ColWidth(9, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.05));
			vsMatch.ColWidth(10, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.06));
			vsMatch.ColWidth(11, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.06));
			vsTotals.ColWidth(0, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.05));
			vsTotals.ColWidth(1, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.07));
			vsTotals.ColWidth(2, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.21));
			vsTotals.ColWidth(3, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(4, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(5, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(6, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
			vsTotals.ColWidth(8, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.12));
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				LoadGrid();
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				int counter;
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
			}
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsMatch.Row < 0)
					return;
				if (vsMatch.Col < 0)
					return;
				clsDRWrapper rsData = new clsDRWrapper();
				// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string, DateTime)
				DateTime dtDate;
				DateTime dtTemp;
				dtDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
				dtTemp = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp, dtDate) > 0)
				{
					dtDate = dtTemp;
				}
				// Call rsData.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' AND MatchDeductionNumber = " & Trim(Left(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 2), 6)))
				// If rsData.EndOfFile Then
				// Else
				// MsgBox "There is a history record for the Pay Date of " & rsData.Fields("PayDate") & vbNewLine & "This deduction cannot be deleted.", vbCritical + vbOKOnly, "Cannot Delete"
				// Exit Sub
				// End If
				if (Conversion.Val(vsMatch.TextMatrix(vsMatch.Row, 12)) > 0)
				{
					rsData.OpenRecordset("select * from tblcheckdetail where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and matchrecord = 1 and empdeductionid = " + vsMatch.TextMatrix(vsMatch.Row, 12) + " and checkvoid = 0 and paydate >= '" + FCConvert.ToString(dtDate) + "' order by paydate desc", "twpy0000.vb1");
					if (!rsData.EndOfFile())
					{
						MessageBox.Show("There is a history record for the Pay Date of " + rsData.Get_Fields_DateTime("PayDate") + "\r\n" + "This match cannot be deleted.", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
				if (MessageBox.Show("This action will delete record #" + vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 1) + ". Continue?", "Payroll Employee Match", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// We don't want to remove this record from the table as there is still
					// a deduction number ??? be we want to clear it out.
					// Call dbMatch.Execute("Update tblEmployersMatch set Description ='"
					// & "" & "', DeductionCode = 0,FrequencyCode = 0,Amount = 0,AmountType = ' ',Limit = 0, LimitType = ' ',TaxStatusCode = "
					// & "0,DeductionNumber = 0,PercentType = '',Priority = 0 Where ID = " & vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 12))
					// For intCounter = 3 To 7
					// If intCounter = 7 Then intCounter = 8
					// 
					// If (vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, intCounter)) > 0 Then
					// MsgBox "Can't Delete Record, Check Totals", vbExclamation, "Error"
					// Exit Sub
					// End If
					// Next
					modGlobalFunctions.AddCYAEntry_6("PY", "Employers Match Deleted", "Number " + fecherFoundation.Strings.Trim(Strings.Left(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 2)), 6)), "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
					rsMatch.Execute("Delete from tblEmployersMatch Where ID = " + vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 12), "Payroll");
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsMatch);
					// clear the edit symbol from the row number
					vsMatch.TextMatrix(vsMatch.Row, 0, Strings.Mid(vsMatch.TextMatrix(vsMatch.Row, 0), 1, (vsMatch.TextMatrix(vsMatch.Row, 0).Length < 2 ? 2 : vsMatch.TextMatrix(vsMatch.Row, 0).Length - 2)));
					// Dave 12/14/2006---------------------------------------------------
					// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
					modAuditReporting.RemoveGridRow_8("vsMatch", intTotalNumberOfControls - 1, vsMatch.Row, clsControlInfo);
					// We then add a change record saying the row was deleted
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsMatch.Row) + " from Match");
					// -------------------------------------------------------------------
					// load the grid with the new data to show the 'clean out' of the current row
					LoadGrid();
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Payroll Employee Match", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CallErrorRoutine = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				int x;
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				bool boolNew = false;
				vsMatch.Select(0, 0);
				vsTotals.Select(0, 0);
				if (NotValidData() == true)
					return;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Match", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					rsMatch.OpenRecordset("Select * from tblEmployersMatch where ID = " + FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 12))), "TWPY0000.vb1");
					if (vsMatch.TextMatrix(intCounter, 2) == string.Empty && FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 4)) == 0)
					{
						goto NextRecord;
					}
					else
					{
						if (!rsMatch.EndOfFile())
						{
							boolNew = false;
							rsMatch.Edit();
						}
						else
						{
							boolNew = true;
							rsMatch.AddNew();
						}
						// If Right(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 2) = ".." Or Right(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 2) = ".." Then
						// boolNew = False
						// rsMatch.Edit
						// ElseIf Right(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 1) = "." Or Right(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 1) = "." Then
						// If Right(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 1) = "." Or Right(vsTotals.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 1) = "." Then
						// boolNew = True
						// rsMatch.AddNew
						// Else
						// GoTo NextRecord
						// boolNew = False
						// rsMatch.Edit
						// End If
					}
					rsMatch.SetData("RecordNumber", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1));
					rsMatch.SetData("DeductionCode", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2) + " "));
					rsMatch.SetData("Description", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3));
					rsMatch.SetData("Amount", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4)));
					rsMatch.SetData("AmountType", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5));
					rsMatch.SetData("Account", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6));
					rsMatch.SetData("MaxAmount", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 7)));
					rsMatch.SetData("DollarsHours", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 8)));
					// DO NOT WANT TO SAVE THE TAX CODE AS WE WANT TO LOOK AT THE SETUP SCREEN
					// rsMatch.SetData "TaxStatusCode", Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 9))
					rsMatch.SetData("FrequencyCode", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 10)));
					// SAVE THE VALUE OF THE FREQUENCY SO THAT IF IT IS EVERY CHANGED TO
					// AND N OR P THEN THE RESET TEMP FLAG REPORT WILL KNOW WHAT TO REVERT IT TO
					if (Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 10)) != 9 && Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 10)) != 10)
					{
						rsMatch.SetData("OldFrequency", Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 10)));
					}
					rsMatch.SetData("Status", fecherFoundation.Strings.Trim(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 11))));
					rsMatch.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
					rsMatch.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
					rsMatch.Set_Fields("LTD", FCConvert.ToString(Conversion.Val(vsTotals.TextMatrix(intCounter, CNSTGRIDTOTALCOLLTD))));
					rsMatch.Update();
					/*? On Error Resume Next  */
					if (boolNew)
					{
						// clear the new symbol from the row number
						vsMatch.TextMatrix(intCounter, 0, Strings.Mid(vsMatch.TextMatrix(intCounter, 0), 1, vsMatch.TextMatrix(intCounter, 0).Length - 1));
						vsTotals.TextMatrix(intCounter, 0, Strings.Mid(vsTotals.TextMatrix(intCounter, 0), 1, vsTotals.TextMatrix(intCounter, 0).Length - 1));
						// get the new ID from the table for the record that was just added and place it into column 3
						rsMatch.OpenRecordset("Select Max(ID) as NewID from tblEmployersMatch", "TWPY0000.vb1");
						vsMatch.TextMatrix(intCounter, 12, FCConvert.ToString(rsMatch.Get_Fields("NewID")));
						vsTotals.TextMatrix(intCounter, 8, FCConvert.ToString(rsMatch.Get_Fields("NewID")));
					}
					else
					{
						// clear the edit symbol from the row number
						if (vsMatch.TextMatrix(intCounter, 0).Length > 2)
						{
							vsMatch.TextMatrix(intCounter, 0, Strings.Mid(vsMatch.TextMatrix(intCounter, 0), 1, vsMatch.TextMatrix(intCounter, 0).Length - 2));
						}
						if (vsTotals.TextMatrix(intCounter, 0).Length > 2)
						{
							vsTotals.TextMatrix(intCounter, 0, Strings.Mid(vsTotals.TextMatrix(intCounter, 0), 1, vsTotals.TextMatrix(intCounter, 0).Length - 2));
						}
					}
					fecherFoundation.Information.Err().Clear();
					NextRecord:
					;
				}
				clsHistoryClass.Compare();
				LoadGrid();
				// CHECK TO SEE IF THE CALCULATION HAS ALREADY BEEN RUN
				if (modGlobalRoutines.CheckIfPayProcessCompleted("Calculation", "Calculation", true))
				{
					// CHECK TO SEE IF THE VERIFY AND ACCEPT HAS BEEN RUN
					if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "VerifyAccept", true))
					{
						// ACCEPT HAS BEEN DONE SO DO NOT CLEAR OUT THE CALCULATION FLAG
					}
					else
					{
						// CLEAR OUT THE CALCULATION FIELD
						modGlobalRoutines.UpdatePayrollStepTable("EditTotals");
					}
				}
				MessageBox.Show("Record(s) Saved successfully.", "Employer's Match", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				return;
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_CallErrorRoutine:
						//? goto CallErrorRoutine;
						break;
				}
				modErrorHandler.SetErrorHandler(ex);
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Frequency Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
			{
				if (vsMatch.TextMatrix(intCounter, 2) == string.Empty && FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 4)) != 0)
				{
					MessageBox.Show("Select Item From List", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsMatch.Select(intCounter, 2);
					NotValidData = true;
					return NotValidData;
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 9))) == string.Empty)
				{
					MessageBox.Show("Tax Code must be selected", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					vsMatch.Select(intCounter, 9);
					NotValidData = true;
					return NotValidData;
				}
				// If vsMatch.TextMatrix(intCounter, 10) = "1" Then
				// If Val(vsMatch.TextMatrix(intCounter, 4)) < 1 Or Val(vsMatch.TextMatrix(intCounter, 7)) < 1 Then
				// MsgBox "Must Have Value For Amount And/Or Max Amount", , "Status Is Active"
				// vsMatch.Select intCounter, 10
				// NotValidData = True
				// Exit Function
				// End If
				// End If
			}
			NotValidData = false;
			return NotValidData;
		}

		public void frmEmployersMatch_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
				vsMatch.Focus();
				if (vsMatch.Rows > 1)
				{
					if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsMatch.Col = 4;
					}
					else
					{
						vsMatch.Col = 2;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployersMatch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if key is the ins key then add a new record
				if (KeyCode == Keys.Insert)
				{
					cmdNew_Click();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployersMatch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
			{
				// MsgBox "Form_KeyPress"
				Close();
				// MsgBox "After unload"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployersMatch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmployersMatch properties;
			//frmEmployersMatch.ScaleWidth	= 9045;
			//frmEmployersMatch.ScaleHeight	= 7155;
			//frmEmployersMatch.LinkTopic	= "Form1";
			//frmEmployersMatch.LockControls	= -1  'True;
			//End Unmaped Properties
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolCantEdit = false;
				modGlobalRoutines.Statics.gboolShowVerifyTotalsMessage = false;
				clsGA.GRID7Light = vsMatch;
				modGlobalRoutines.CheckCurrentEmployee();
				// Call GetFormats
				boolFromMatch = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				rsMatch.DefaultDB = "TWPY0000.vb1";
				// open the forms global database connection
				rsMatch.OpenRecordset("Select * from tblEmployersMatch Order by ID", "TWPY0000.vb1");
				// set the properties on the deduction grid
				SetGridProperties();
				clsGA.AccountCol = 6;
				clsGA.Validation = true;
				clsGA.DefaultAccountType = "G";
				clsGA.OnlyAllowDefaultType = false;
				clsGA.AllowSplits = modRegionalTown.IsRegionalTown();
				// set the properties on the Total grid
				SetTotalGridProperties();
				modMultiPay.Statics.gstrEmployeeCaption = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modMultiPay.SetMultiPayCaptions(this);
				// load the employee deduction grid
				LoadGrid();
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					lblEmployeeNumber.Text = "";
				}
				else
				{
					lblEmployeeNumber.Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				}
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					cmdNew.Enabled = false;
					cmdDelete.Enabled = false;
					cmdSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					mnuDedSetup.Enabled = false;
					mnuTemporaryOverrides.Enabled = false;
					mnuTaxStatusCodes.Enabled = false;
					vsMatch.Enabled = false;
					vsTotals.Enabled = false;
				}
				clsHistoryClass.SetGridIDColumn("PY", "vsMatch", 12);
				clsHistoryClass.SetGridIDColumn("PY", "vsTotals", 1);
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// append a new row to the end of the grid
				vsMatch.AddItem(FCConvert.ToString(vsMatch.Rows));
				vsTotals.AddItem(FCConvert.ToString(vsTotals.Rows));
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// assign a row number to this new record
				vsMatch.TextMatrix(vsMatch.Rows - 1, 0, vsMatch.TextMatrix(vsMatch.Rows - 1, 0) + ".");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 0, vsTotals.TextMatrix(vsTotals.Rows - 1, 0) + ".");
				if (modGlobalVariables.Statics.gboolHaveDivision)
				{
					vsMatch.TextMatrix(vsMatch.Rows - 1, 6, "E __-__-__-__");
				}
				else
				{
					vsMatch.TextMatrix(vsMatch.Rows - 1, 6, "E __-__-__");
				}
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Rows - 1, 9, 2);
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Rows - 1, 10, 12);
				vsMatch.TextMatrix(vsMatch.Rows - 1, 11, "Active");
				vsMatch.TextMatrix(vsMatch.Rows - 1, 5, "D");
				vsMatch.TextMatrix(vsMatch.Rows - 1, 4, "0");
				vsMatch.TextMatrix(vsMatch.Rows - 1, 1, FCConvert.ToString(vsMatch.Rows - 1));
				vsMatch.TextMatrix(vsMatch.Rows - 1, 7, "999999");
				vsMatch.TextMatrix(vsMatch.Rows - 1, 8, FCConvert.ToString(1));
				vsTotals.TextMatrix(vsTotals.Rows - 1, 3, "0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 4, "0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 5, "0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 8, "0");
				vsTotals.TextMatrix(vsTotals.Rows - 1, 6, "0");
				vsMatch.Select(vsMatch.Rows - 1, 2);
				vsTotals.Select(vsTotals.Rows - 1, 2);
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsMatch.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Rows - 1, 9, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				//FC:FINAL:BSE:#4103 add color to grid columns
                vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsMatch.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsMatch.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				//vsMatch.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, 10, vsMatch.Rows - 1, 10, 9);
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Match");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				FCRecordset rsTemp = null;
				// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
				int intCount;
				string[,] aryDescription = null;
				vsMatch.Rows = 1;
				vsTotals.Rows = 1;
				vsMatch.Select(0, 0);
				rsMatch.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					aryDescription = new string[rsMatch.RecordCount() + 1, 2 + 1];
					for (intCounter = 0; intCounter <= (rsMatch.RecordCount() - 1); intCounter++)
					{
						aryDescription[intCounter, 0] = FCConvert.ToString(rsMatch.Get_Fields("ID"));
						aryDescription[intCounter, 1] = FCConvert.ToString(rsMatch.Get_Fields("Description"));
						rsMatch.MoveNext();
					}
				}
				// get all of the records from the database
				rsMatch.OpenRecordset("Select * from tblEmployersMatch Where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RecordNumber", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// set the number of rows in the grid
					vsMatch.Rows = rsMatch.RecordCount() + 1;
					vsTotals.Rows = vsMatch.Rows;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						if (intCounter != FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("RecordNumber") + " ")))
						{
							rsMatch.Execute("Update tblEmployersMatch Set RecordNumber = " + FCConvert.ToString(intCounter) + " where ID = " + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("ID") + " "), "Payroll");
							vsMatch.TextMatrix(intCounter, 1, FCConvert.ToString(intCounter));
						}
						else
						{
							vsMatch.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("RecordNumber") + " "));
						}
						vsMatch.TextMatrix(intCounter, 2, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " "))));
						for (intCount = 0; intCount <= (Information.UBound(aryDescription, 1) - 1); intCount++)
						{
							if (FCConvert.ToDouble(aryDescription[intCount, 0]) == Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " ")))
							{
								break;
							}
						}
						if (intCount == Information.UBound(aryDescription, 1))
						{
							vsMatch.TextMatrix(intCounter, 3, string.Empty);
						}
						else
						{
							vsMatch.TextMatrix(intCounter, 3, aryDescription[intCount, 1]);
						}
						vsMatch.TextMatrix(intCounter, 4, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Amount") + " "))));
						vsMatch.TextMatrix(intCounter, 5, fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("AmountType") + " "));
						vsMatch.TextMatrix(intCounter, 6, fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Account") + " "));
						vsMatch.TextMatrix(intCounter, 7, FCConvert.ToString((FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Double("MaxAmount") + " ")))) == 0 ? "0" : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Double("MaxAmount") + " "))))));
						vsMatch.TextMatrix(intCounter, 8, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("DollarsHours") + " "))));
						// vsMatch.TextMatrix(intCounter, 9) = IIf(Trim(rsMatch.Fields("TaxStatusCode") & " ") = vbNullString, Trim(rsMatch.Fields("TaxStatusCode") & " "), Trim(rsMatch.Fields("TaxStatusCode") & " "))
						vsMatch.TextMatrix(intCounter, 9, modGlobalRoutines.GetDeductionTaxStatusCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " "))))));
						// vsMatch.TextMatrix(intCounter, 10) = IIf(Trim(rsMatch.Fields("FrequencyCode") & " ") = vbNullString, Trim(rsMatch.Fields("FrequencyCode") & " "), Trim(rsMatch.Fields("FrequencyCode") & " "))
						// Select Case Val(rsMatch.Fields("frequencycode"))
						// Case 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
						vsMatch.TextMatrix(intCounter, 10, FCConvert.ToString(rsMatch.Get_Fields("frequencycode")));
						// Case Else
						// vsMatch.TextMatrix(intCounter, 10) = 9  'hold
						// End Select
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 9, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.TextMatrix(intCounter, 11, fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("Status") + " "));
						vsMatch.TextMatrix(intCounter, 12, fecherFoundation.Strings.Trim(rsMatch.Get_Fields("ID") + " "));
						// load the total grid
						vsTotals.TextMatrix(intCounter, 0, vsMatch.TextMatrix(intCounter, 1));
						vsTotals.TextMatrix(intCounter, 1, FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)));
						vsTotals.TextMatrix(intCounter, 2, vsMatch.TextMatrix(intCounter, 3));
						vsTotals.TextMatrix(intCounter, 3, FCConvert.ToString(modCoreysSweeterCode.GetCurrentMatchTotal(rsMatch.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsMatch.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 4, FCConvert.ToString(modCoreysSweeterCode.GetMTDMatchTotal(rsMatch.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsMatch.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 5, FCConvert.ToString(modCoreysSweeterCode.GetQTDMatchTotal(rsMatch.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsMatch.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 8, FCConvert.ToString(modCoreysSweeterCode.GetCYTDMatchTotal(rsMatch.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsMatch.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, 7, vsMatch.TextMatrix(intCounter, 11));
						vsTotals.TextMatrix(intCounter, 6, FCConvert.ToString(modCoreysSweeterCode.GetFYTDMatchTotal(rsMatch.Get_Fields("deductioncode"), modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate, rsMatch.Get_Fields("ID"))));
						vsTotals.TextMatrix(intCounter, CNSTGRIDTOTALCOLLTD, FCConvert.ToString(Conversion.Val(rsMatch.Get_Fields("ltd"))));
						if (Conversion.Val(vsTotals.TextMatrix(intCounter, 3)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 4)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 8)) != 0 || Conversion.Val(vsTotals.TextMatrix(intCounter, 6)) != 0)
						{
							// need code here to lock this one row
							vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
						//vsMatch.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, 10, vsMatch.Rows - 1, 10, 9);
						rsMatch.MoveNext();
					}
					vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsMatch.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    //FC:FINAL:BSE:#4103 add color to grid columns
					vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsTotals.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsTotals.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                    // initialize the counter as to have many rows are dirty
                    intDataChanged = 0;
				}
				clsHistoryClass.Init = this;
				if (vsMatch.Rows > 2)
					vsMatch.Select(1, 3);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Validate that there is not changes that need to be saved
				// Call SaveChanges
				// Unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// Save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// MsgBox "Before Save Changes"
				SaveChanges();
				// MsgBox "MDIParent.Show"
				//MDIParent.InstancePtr.Show();
				// MsgBox "Before CallByName"
				// Set Focus Back To The Menu Options Of The MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
				// MsgBox "After CallByName"
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strList = "";
				// set grid properties
				//vsMatch.WordWrap = true;
				vsMatch.Cols = 13;
				//FC:FINAL:DDU:#i2394 - changed fixedCols to 1 from 2 to delete extra space at the end of grid
				vsMatch.FixedCols = 1;
				vsMatch.ColHidden(0, true);
				vsMatch.ColHidden(1, true);
				vsMatch.ColHidden(12, true);
				//vsMatch.RowHeight(0) *= 2;
				modGlobalRoutines.CenterGridCaptions(ref vsMatch);
				// set column 0 properties
				vsMatch.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(0, 400);
				// set column 1 properties
				vsMatch.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(1, 400);
				// set column 2 properties
				vsMatch.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(2, 500);
				vsMatch.TextMatrix(0, 2, "Ded");
				rsMatch.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// This forces this column to work as a combo box
					// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
					int intCounter2;
					for (intCounter2 = 1; intCounter2 <= (rsMatch.RecordCount()); intCounter2++)
					{
						if (intCounter2 == 1)
						{
							vsMatch.ColComboList(2, vsMatch.ColComboList(2) + "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("Description"))) != "" ? "\t" + rsMatch.Get_Fields("Description") : ""));
						}
						else
						{
							vsMatch.ColComboList(2, vsMatch.ColComboList(2) + "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("Description"))) != "" ? "\t" + rsMatch.Get_Fields("Description") : ""));
						}
						rsMatch.MoveNext();
					}
				}
				// set column 3 properties
				vsMatch.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(3, 1500);
				vsMatch.TextMatrix(0, 3, "Description");
				// set column 4 properties
				vsMatch.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMatch.ColWidth(4, 1100);
				vsMatch.TextMatrix(0, 4, "Amount");
				vsMatch.ColFormat(4, "#,##0.00;(#,###.00)");
				vsMatch.ColAllowedKeys(4, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				// set column 5 properties
				vsMatch.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(5, 550);
				vsMatch.TextMatrix(0, 5, "Type");
				vsMatch.ColComboList(5, "#1;D" + "\t" + "Dollars|#2;%G" + "\t" + "% of Gross|#3;%D" + "\t" + "% of Deduction");
				// set column 6 properties
				vsMatch.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(6, 1000);
				vsMatch.TextMatrix(0, 6, "Charge Account");
				// set column 7 properties
				vsMatch.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMatch.ColWidth(7, 1300);
				vsMatch.TextMatrix(0, 7, "Max Amount");
				vsMatch.ColFormat(7, "#,##0.00;(#,###.00)");
				vsMatch.ColAllowedKeys(7, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				// set column 8 properties
				vsMatch.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(8, 700);
				vsMatch.TextMatrix(0, 8, "Type");
				vsMatch.ColComboList(8, "#1;Dollars|#2;Hours|#3;Period|#4;Month|#5;Quarter|#6;Calendar|#7;Fiscal|#8;Life");
				// set column 9 properties
				vsMatch.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(9, 450);
				vsMatch.TextMatrix(0, 9, "Tax");
				rsMatch.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						if (intCounter == 3)
						{
							vsMatch.ColComboList(9, vsMatch.ColComboList(9) + "|#0; " + "\t" + "");
							vsMatch.ColComboList(9, vsMatch.ColComboList(9) + "|#00; " + "\t" + "   EXEMPT FROM");
							// vsMatch.ColComboList(9, vsMatch.ColComboList(9) & "|#000; " & vbTab & ""
						}
						if (intCounter == 1)
						{
							vsMatch.ColComboList(9, vsMatch.ColComboList(9) + "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " "));
						}
						else
						{
							vsMatch.ColComboList(9, vsMatch.ColComboList(9) + "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " "));
						}
						rsMatch.MoveNext();
					}
				}
				// set column 10 properties
				vsMatch.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(10, 500);
				vsMatch.TextMatrix(0, 10, "Freq");
				rsMatch.OpenRecordset("Select * from tblFrequencyCodes order by ID", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					strList = "";
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						// If intCounter = 1 Then
						// strList = strList & "#" & rsMatch.Fields("ID") & ";" & rsMatch.Fields("FrequencyCode") & vbTab & Trim(rsMatch.Fields("Description") & " ")
						// Else
						// strList = strList & "|#" & rsMatch.Fields("ID") & ";" & rsMatch.Fields("FrequencyCode") & vbTab & Trim(rsMatch.Fields("Description") & " ")
						// End If
						// Select Case rsMatch.Fields("ID")
						// Case 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
						if (intCounter == 1)
						{
							strList += "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("FrequencyCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " ");
						}
						else
						{
							strList += "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("FrequencyCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " ");
						}
						// Case Else
						// End Select
						rsMatch.MoveNext();
					}
				}
				vsMatch.ColComboList(10, strList);
				// vsMatch.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, 10, vsMatch.Rows - 1, 10) = 6
				// set column 11 properties
				vsMatch.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(11, 700);
				vsMatch.TextMatrix(0, 11, "Status");
				vsMatch.ColComboList(11, "#1;Active|#2;Inactive");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetTotalGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetTotalGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsTotals.Cols = 10;
				vsTotals.FixedCols = 3;
				vsTotals.ColHidden(7, true);
				vsTotals.ColHidden(0, true);
				modGlobalRoutines.CenterGridCaptions(ref vsTotals);
				//vsTotals.RowHeight(0) *= 2;
				// set column 0 properties
				vsTotals.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(0, 400);
				// set column 1 properties
				vsTotals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(1, 500);
				vsTotals.TextMatrix(0, 1, "Ded");
				vsTotals.ColComboList(1, vsTotals.ColComboList(2));
				// set column 2 properties
				vsTotals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsTotals.ColWidth(2, 1800);
				vsTotals.TextMatrix(0, 2, "Description");
				// set column 3 properties
				vsTotals.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(3, 1200);
				vsTotals.TextMatrix(0, 3, "Current");
				vsTotals.ColFormat(3, "$#,##0.00;($#,###.00)");
				// set column 4 properties
				vsTotals.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(4, 1200);
				vsTotals.TextMatrix(0, 4, "MTD");
				vsTotals.ColFormat(4, "$#,##0.00;($#,###.00)");
				// set column 5 properties
				vsTotals.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(5, 1200);
				vsTotals.TextMatrix(0, 5, "QTD");
				vsTotals.ColFormat(5, "$#,##0.00;($#,###.00)");
				// set column 6 properities
				vsTotals.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(8, 1200);
				vsTotals.TextMatrix(0, 8, "CYTD");
				vsTotals.ColFormat(8, "$#,##0.00;($#,###.00)");
				// set column 7 properties
				vsTotals.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.ColWidth(6, 1200);
				vsTotals.TextMatrix(0, 6, "FYTD");
				vsTotals.ColFormat(6, "$#,##0.00;($#,###.00)");
				vsTotals.ColAlignment(CNSTGRIDTOTALCOLLTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsTotals.TextMatrix(0, CNSTGRIDTOTALCOLLTD, "Life to Date");
				vsTotals.ColFormat(CNSTGRIDTOTALCOLLTD, "$#,##0.00;($#,###.00)");
				vsTotals.ColAllowedKeys(CNSTGRIDTOTALCOLLTD, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuCaption1_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(1));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = FCConvert.ToBoolean(0);
			LoadGrid();
			lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
		}

		private void mnuCaption2_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(2));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption3_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(3));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption4_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(4));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption5_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(5));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption6_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(6));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption7_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(7));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption8_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(8));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption9_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(9));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuDedSetup_Click(object sender, System.EventArgs e)
		{
			boolFromMatch = true;
			frmDeductionSetup.InstancePtr.Show();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuHistory_Click()
		{
			frmEmployeeDeductionHistory.InstancePtr.Show();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
			// Call SetVerifyAdjustNeeded(True)
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// Call cmdPrint_Click
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void mnuTaxStatusCodes_Click(object sender, System.EventArgs e)
		{
			boolFromMatch = true;
			frmTaxStatusCodes.InstancePtr.Show();
		}

		private void mnuViewValidAccounts_Click()
		{
			// vbPorter upgrade warning: intGridRow As int	OnWriteFCConvert.ToInt32(
			int intGridRow;
			intGridRow = vsMatch.Row;
			// frmValidAccounts.Show 1, MDIParent
			modGlobalVariables.Statics.gstrSelectedAccount = frmLoadValidAccounts.InstancePtr.Init(vsMatch.TextMatrix(intGridRow, 6));
			if (modGlobalVariables.Statics.gstrSelectedAccount != string.Empty)
			{
				if (intGridRow > 0)
				{
					vsMatch.TextMatrix(intGridRow, 6, modGlobalVariables.Statics.gstrSelectedAccount);
					vsMatch.Select(intGridRow, 6);
				}
				else
				{
					MessageBox.Show("Cursor must be in row that you wish account number to be intered in.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void mnuTemporaryOverrides_Click(object sender, System.EventArgs e)
		{
			frmTempEmployeeSetup.InstancePtr.Show(App.MainForm);
		}

		private void vsMatch_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsMatch.Col == 5)
			{
				if (vsMatch.EditText == "%D")
				{
					clsDRWrapper rsCheck = new clsDRWrapper();
					rsCheck.OpenRecordset("Select * from tblEmployeeDeductions where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and DeductionCode = " + vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, 2), "Twpy0000.vb1");
					if (rsCheck.EndOfFile())
					{
						MessageBox.Show("Deduction Number has not been set up for this employee.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, 5, "D");
					}
				}
			}
		}

		private void vsMatch_Enter(object sender, System.EventArgs e)
		{
			// see form comments
			// If boolFromAccount Then vsMatch.Col = 7: vsMatch.Col = 2
		}

		private void vsMatch_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			//App.DoEvents();
			// if the key is the TAB then make it work like the right arrow
			if (KeyCode == (Keys)9)
				return;
			if (KeyCode == (Keys)9)
				KeyCode = (Keys)39;
			if (KeyCode == (Keys)13)
				KeyCode = (Keys)39;
			boolSkip = true;
			if (vsMatch.Col == 2)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					boolSkip = false;
					vsMatch.Col = 3;
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)37)
				{
					// left key
					if (vsMatch.Row > 1)
					{
						vsMatch.Col = 10;
						vsMatch.Row -= 1;
					}
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)38)
				{
					// up
					intRowNumber = vsMatch.Row;
					NextRow:
					;
					if (vsMatch.Row > 1)
					{
						vsMatch.Row -= 1;
						if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							goto NextRow;
						}
					}
					else
					{
						vsMatch.Row = intRowNumber;
					}
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)40)
				{
					// down
					intRowNumber = vsMatch.Row;
					NextRow2:
					;
					if (vsMatch.Row < vsMatch.Rows - 1)
					{
						vsMatch.Row += 1;
						if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							goto NextRow2;
						}
					}
					else
					{
						vsMatch.Row = intRowNumber;
					}
					KeyCode = 0;
				}
			}
			if (vsMatch.Col == 4)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					// boolSkip = False
					vsMatch.Col = 2;
					if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						//vsMatch_KeyDown(37, 0);
					}
					KeyCode = 0;
				}
			}
			if (vsMatch.Col == 10)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					if (vsMatch.Row < vsMatch.Rows - 1)
					{
						// boolSkip = False
						vsMatch.Col = 1;
						vsMatch.Row += 1;
					}
					else
					{
						cmdNew_Click();
						KeyCode = 0;
					}
				}
			}
			if (vsMatch.Col == 6)
			{
				if (KeyCode == (Keys)37)
				{
					// left
					vsMatch.Col = 5;
				}
				else if (KeyCode == (Keys)38)
				{
					// up
					if (vsMatch.Row > 1)
					{
						vsMatch.Row -= 1;
					}
				}
				else if (KeyCode == (Keys)39)
				{
					// right
					vsMatch.Col = 7;
				}
				else if (KeyCode == (Keys)40)
				{
					// down
					if (vsMatch.Row < vsMatch.Rows - 1)
					{
						vsMatch.Row += 1;
					}
				}
				KeyCode = 0;
			}
			boolSkip = false;
		}

		private void vsMatch_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			// If Col = 6 Then Call CheckAccountKeyPress(vsMatch, Row, Col, KeyAscii)
		}

		private void vsMatch_KeyUpEvent(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_KeyUp";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this variable is used in the form frmHelp to determine what data to show
				modGlobalVariables.Statics.gstrHelpFieldName = string.Empty;
				if (e.KeyCode == Keys.F9)
				{
					switch (vsMatch.Col)
					{
						case 2:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "DEDUCTIONCODE";
								break;
							}
						case 5:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "AMOUNTTYPE";
								break;
							}
						case 9:
							{
								modGlobalVariables.Statics.gstrHelpFieldName = "TAXSTATUSCODE";
								// Case 8
								// gstrHelpFieldName = "FREQUENCYCODE"
								// 
								// Case 10
								// gstrHelpFieldName = "LIMITCODE"
								break;
							}
						default:
							{
								break;
							}
					}
					//end switch
					frmHelp.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsMatch_KeyUpEdit(object sender, KeyEventArgs e)
		{
			// If Col = 6 Then
			// Call CheckAccountKeyCode(vsMatch, Row, Col, KeyCode, 0)
			// KeyCode = 0
			// End If
		}

		private void vsMatch_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            /*? On Error Resume Next  */
            //FC:FINAL:BSE:#4107 - set tooltips
            if(e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsMatch[e.ColumnIndex, e.RowIndex];
            if (vsMatch.GetFlexColIndex(e.ColumnIndex) == 6)
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					//ToolTip1.SetToolTip(vsMatch, modDavesSweetCode.GetAccountDescription(vsMatch.TextMatrix(vsMatch.MouseRow, 6)));
					cell.ToolTipText = modDavesSweetCode.GetAccountDescription(vsMatch.TextMatrix(vsMatch.GetFlexRowIndex(e.RowIndex), 6));
				}
				else
				{
                    //ToolTip1.SetToolTip(vsMatch, string.Empty);
                    cell.ToolTipText = "";
				}
			}
			else
			{
                //ToolTip1.SetToolTip(vsMatch, string.Empty);
                cell.ToolTipText = "";
			}
		}

		private void vsMatch_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsMatch_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolSkip)
					return;
				// If vsMatch.Col = 6 Then Call GetMaxLength(vsMatch.TextMatrix(vsMatch.Row, vsMatch.Col))
				// if there are values in the total grid do not allow the user
				// to alter the deduction codes for that record
				vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				if (Conversion.Val(vsTotals.TextMatrix(vsMatch.Row, 3)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsMatch.Row, 4)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsMatch.Row, 6)) != 0 || Conversion.Val(vsTotals.TextMatrix(vsMatch.Row, 8)) != 0)
				{
					// need code here to lock this one row
					if (vsMatch.Col == 2)
					{
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (vsMatch.Col == 2 && vsMatch.Row > 0)
					{
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2, Color.White);
					}
				}
				if (vsMatch.Col == 2)
				{
					if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, 2)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				if (vsMatch.Col == 3)
				{
					// vsMatch.Col = 4
					vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
					return;
				}
				// if col = tax then move to the next column
				if (vsMatch.Col == 9)
				{
					// vsMatch.Col = 10
					vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
					return;
				}
				// set the account box
				if (vsMatch.Col == 6)
				{
					// if the current row is already marked as needing a save then do not mark it again
					if (Strings.Right(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 0)), 1) != ".")
					{
						// increment the counter as to how many rows are dirty
						intDataChanged += 1;
						// Change the row number in the first column to indicate that this record has been edited
						vsMatch.TextMatrix(vsMatch.Row, 0, vsMatch.TextMatrix(vsMatch.Row, 0) + "..");
					}
				}
				if (FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 5)) == "Percent")
				{
					if (Conversion.Val(vsMatch.TextMatrix(vsMatch.Row, 4)) > 100)
					{
						MessageBox.Show("Value Can't Be Greater Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsMatch.Select(vsMatch.Row, 4);
					}
				}
				// If vsMatch.Col = 6 Then
				// vsMatch.EditCell
				// vsMatch.EditSelStart = 2
				// vsMatch.EditSelLength = 1
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsMatch_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsMatch_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// dblAmtPercentTotal = 0
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsMatch.TextMatrix(vsMatch.Row, 0, vsMatch.TextMatrix(vsMatch.Row, 0) + "..");
				}
				if (vsMatch.Col == 2)
				{
					if (Strings.Right(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 0)), 1) == ".")
					{
						rsMatch.OpenRecordset("Select * from tblDeductionSetup where ID = " + FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, 2))), "TWPY0000.vb1");
						if (!rsMatch.EndOfFile())
						{
							vsTotals.TextMatrix(vsMatch.Row, 1, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
							vsMatch.TextMatrix(vsMatch.Row, 3, FCConvert.ToString(rsMatch.Get_Fields("Description")));
							vsTotals.TextMatrix(vsMatch.Row, 2, FCConvert.ToString(rsMatch.Get_Fields("Description")));
							vsMatch.TextMatrix(vsMatch.Row, 4, FCConvert.ToString(rsMatch.Get_Fields("Amount")));
							if (fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("AmountType") + " ") == "Dollars")
							{
								vsMatch.TextMatrix(vsMatch.Row, 5, "D");
							}
							else
							{
								vsMatch.TextMatrix(vsMatch.Row, 5, "%G");
							}
							vsMatch.TextMatrix(vsMatch.Row, 9, FCConvert.ToString(rsMatch.Get_Fields("TaxStatusCode")));
						}
					}
				}
				// For intCounter = 1 To vsMatch.Rows - 1
				// If vsMatch.TextMatrix(intCounter, 5) = "Percent" Or vsMatch.TextMatrix(intCounter, 5) = "2" Then
				// dblAmtPercentTotal = dblAmtPercentTotal + vsMatch.TextMatrix(intCounter, 4)
				// End If
				// Next
				// If dblAmtPercentTotal > 100 Then
				// MsgBox "Amount Totals For Percent Cannot Be Greater Than 100", vbOKOnly, "Error"
				// vsMatch.Select Row, Col
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsMatch_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsMatch_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsMatch.Col == 11)
					{
						if (vsMatch.Row < vsMatch.Rows - 1)
						{
							vsMatch.Row += 1;
							vsMatch.Col = 2;
						}
						else
						{
							// if there is no other row then create a new one
							cmdNew_Click();
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsMatch_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If Col = 6 Then Cancel = CheckAccountValidate(vsMatch, Row, Col, Cancel)
			dblAmtPercentTotal = 0;
			if (vsMatch.Col == 4)
			{
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (vsMatch.TextMatrix(intCounter, 5) == "Percent" || vsMatch.TextMatrix(intCounter, 5) == "2")
					{
						if (intCounter == vsMatch.Row)
						{
							dblAmtPercentTotal += Conversion.Val(vsMatch.EditText);
						}
						else
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 4));
						}
					}
				}
			}
			if (vsMatch.Col == 5)
			{
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (intCounter == vsMatch.Row)
					{
						if (vsMatch.EditText == "Percent" || vsMatch.EditText == "2")
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 4));
						}
					}
					else
					{
						if (vsMatch.TextMatrix(intCounter, 5) == "Percent" || vsMatch.TextMatrix(intCounter, 5) == "2")
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 4));
						}
					}
				}
			}
			if (dblAmtPercentTotal > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsMatch.Rows - 1); intRows++)
			{
				for (intCols = 2; intCols <= 11; intCols++)
				{
					if (intCols != 3 && intCols != 9)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsMatch";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsMatch.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}

		private void vsTotals_BeforeRowColChange(object sender, System.EventArgs e)
		{
			switch (vsTotals.Col)
			{
				case CNSTGRIDTOTALCOLLTD:
					{
						vsTotals.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsTotals.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void vsTotals_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// allow the entry of a decimal point
			if (KeyAscii == 46)
				return;
			// allow the entry of a backspace
			if (KeyAscii == 8)
				return;
			if (vsTotals.Col == CNSTGRIDTOTALCOLLTD)
			{
				if (KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9))
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
	}
}
