//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeListing.
	/// </summary>
	partial class frmEmployeeListing
	{
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPayRun;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuImportTimeClock;
		public fecherFoundation.FCToolStripMenuItem mnuImportFromTimeClockPlus;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuCompleted;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.txtTotal = new fecherFoundation.FCTextBox();
            this.vsGrid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblPayRun = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportTimeClock = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportFromTimeClockPlus = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCompleted = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdData = new fecherFoundation.FCButton();
            this.cmdImportTimeClock = new fecherFoundation.FCButton();
            this.cmdImportFromTimeClockPlus = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImportTimeClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImportFromTimeClockPlus)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdData);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(748, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtTotal);
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblPayRun);
            this.ClientArea.Size = new System.Drawing.Size(748, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdImportFromTimeClockPlus);
            this.TopPanel.Controls.Add(this.cmdImportTimeClock);
            this.TopPanel.Size = new System.Drawing.Size(748, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdImportTimeClock, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdImportFromTimeClockPlus, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Data Entry";
            // 
            // txtTotal
            // 
            this.txtTotal.AutoSize = false;
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.LinkItem = null;
            this.txtTotal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTotal.LinkTopic = null;
            this.txtTotal.Location = new System.Drawing.Point(179, 611);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(201, 40);
            this.txtTotal.TabIndex = 2;
            // 
            // vsGrid
            // 
            this.vsGrid.AllowSelection = false;
            this.vsGrid.AllowUserToResizeColumns = false;
            this.vsGrid.AllowUserToResizeRows = false;
            this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
            this.vsGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsGrid.Cols = 7;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsGrid.ColumnHeadersHeight = 30;
            this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsGrid.DragIcon = null;
            this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.FrozenCols = 0;
            this.vsGrid.GridColor = System.Drawing.Color.Empty;
            this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.vsGrid.Location = new System.Drawing.Point(30, 77);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.OutlineCol = 0;
            this.vsGrid.ReadOnly = true;
            this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsGrid.RowHeightMin = 0;
            this.vsGrid.Rows = 1;
            this.vsGrid.ScrollTipText = null;
            this.vsGrid.ShowColumnVisibilityMenu = false;
            this.vsGrid.Size = new System.Drawing.Size(688, 514);
            this.vsGrid.StandardTab = true;
            this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsGrid.TabIndex = 0;
            this.vsGrid.TabStop = false;
            this.vsGrid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsGrid_MouseMoveEvent);
            this.vsGrid.Click += new System.EventHandler(this.vsGrid_ClickEvent);
            this.vsGrid.DoubleClick += new System.EventHandler(this.vsGrid_DblClick);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 625);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 16);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "TOTAL HOURS";
            // 
            // lblPayRun
            // 
            this.lblPayRun.Location = new System.Drawing.Point(30, 30);
            this.lblPayRun.Name = "lblPayRun";
            this.lblPayRun.Size = new System.Drawing.Size(688, 26);
            this.lblPayRun.TabIndex = 1;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuImportTimeClock,
            this.mnuImportFromTimeClockPlus,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuImportTimeClock
            // 
            this.mnuImportTimeClock.Index = 0;
            this.mnuImportTimeClock.Name = "mnuImportTimeClock";
            this.mnuImportTimeClock.Text = "Import from Time Clock";
            this.mnuImportTimeClock.Visible = false;
            this.mnuImportTimeClock.Click += new System.EventHandler(this.mnuImportTimeClock_Click);
            // 
            // mnuImportFromTimeClockPlus
            // 
            this.mnuImportFromTimeClockPlus.Index = 1;
            this.mnuImportFromTimeClockPlus.Name = "mnuImportFromTimeClockPlus";
            this.mnuImportFromTimeClockPlus.Text = "Import from Time Clock Plus";
            this.mnuImportFromTimeClockPlus.Visible = false;
            this.mnuImportFromTimeClockPlus.Click += new System.EventHandler(this.mnuImportFromTimeClockPlus_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                  ";
            this.mnuSave.Visible = false;
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit                 ";
            this.mnuSaveExit.Visible = false;
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuCompleted
            // 
            this.mnuCompleted.Index = -1;
            this.mnuCompleted.Name = "mnuCompleted";
            this.mnuCompleted.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuCompleted.Text = "Data Entry Completed";
            this.mnuCompleted.Click += new System.EventHandler(this.mnuCompleted_Click);
            // 
            // cmdData
            // 
            this.cmdData.AppearanceKey = "acceptButton";
            this.cmdData.Location = new System.Drawing.Point(251, 30);
            this.cmdData.Name = "cmdData";
            this.cmdData.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdData.Size = new System.Drawing.Size(218, 48);
            this.cmdData.TabIndex = 0;
            this.cmdData.Text = "Data Entry Completed";
            this.cmdData.Click += new System.EventHandler(this.mnuCompleted_Click);
            // 
            // cmdImportTimeClock
            // 
            this.cmdImportTimeClock.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdImportTimeClock.AppearanceKey = "toolbarButton";
            this.cmdImportTimeClock.Location = new System.Drawing.Point(368, 29);
            this.cmdImportTimeClock.Name = "cmdImportTimeClock";
            this.cmdImportTimeClock.Size = new System.Drawing.Size(162, 24);
            this.cmdImportTimeClock.TabIndex = 1;
            this.cmdImportTimeClock.Text = "Import from Time Clock";
            this.cmdImportTimeClock.Visible = false;
            this.cmdImportTimeClock.Click += new System.EventHandler(this.mnuImportTimeClock_Click);
            // 
            // cmdImportFromTimeClockPlus
            // 
            this.cmdImportFromTimeClockPlus.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdImportFromTimeClockPlus.AppearanceKey = "toolbarButton";
            this.cmdImportFromTimeClockPlus.Location = new System.Drawing.Point(536, 29);
            this.cmdImportFromTimeClockPlus.Name = "cmdImportFromTimeClockPlus";
            this.cmdImportFromTimeClockPlus.Size = new System.Drawing.Size(183, 24);
            this.cmdImportFromTimeClockPlus.TabIndex = 2;
            this.cmdImportFromTimeClockPlus.Text = "Import from Time Clock Plus";
            this.cmdImportFromTimeClockPlus.Visible = false;
            this.cmdImportFromTimeClockPlus.Click += new System.EventHandler(this.mnuImportFromTimeClockPlus_Click);
            // 
            // frmEmployeeListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(748, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeListing";
            this.Text = "Data Entry";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeeListing_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeListing_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeListing_KeyPress);
            this.Resize += new System.EventHandler(this.frmEmployeeListing_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImportTimeClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImportFromTimeClockPlus)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdData;
		private FCButton cmdImportTimeClock;
		private FCButton cmdImportFromTimeClockPlus;
	}
}