﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditCalculatedRecords.
	/// </summary>
	partial class frmEditCalculatedRecords
	{
		public FCGrid gridCheck;
		public FCGrid GridDeds;
		public FCGrid GridTotals;
		public FCGrid GridMatch;
		public FCGrid GridDist;
		public FCGrid GridPay;
		public FCGrid GridVacSick;
		public fecherFoundation.FCLabel lblEmployee;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridCheck = new fecherFoundation.FCGrid();
			this.GridDeds = new fecherFoundation.FCGrid();
			this.GridTotals = new fecherFoundation.FCGrid();
			this.GridMatch = new fecherFoundation.FCGrid();
			this.GridDist = new fecherFoundation.FCGrid();
			this.GridPay = new fecherFoundation.FCGrid();
			this.GridVacSick = new fecherFoundation.FCGrid();
			this.lblEmployee = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDist)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVacSick)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 596);
			this.BottomPanel.Size = new System.Drawing.Size(658, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridCheck);
			this.ClientArea.Controls.Add(this.GridDeds);
			this.ClientArea.Controls.Add(this.GridTotals);
			this.ClientArea.Controls.Add(this.GridMatch);
			this.ClientArea.Controls.Add(this.GridDist);
			this.ClientArea.Controls.Add(this.GridPay);
			this.ClientArea.Controls.Add(this.GridVacSick);
			this.ClientArea.Controls.Add(this.lblEmployee);
			this.ClientArea.Size = new System.Drawing.Size(678, 628);
			this.ClientArea.Controls.SetChildIndex(this.lblEmployee, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridVacSick, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridPay, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDist, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridMatch, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridTotals, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDeds, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridCheck, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(678, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// gridCheck
			// 
			this.gridCheck.ColumnHeadersVisible = false;
			this.gridCheck.ExtendLastCol = true;
			this.gridCheck.FixedCols = 0;
			this.gridCheck.FixedRows = 0;
			this.gridCheck.Location = new System.Drawing.Point(443, 434);
			this.gridCheck.Name = "gridCheck";
			this.gridCheck.RowHeadersVisible = false;
			this.gridCheck.Rows = 1;
			this.gridCheck.Size = new System.Drawing.Size(189, 40);
			this.gridCheck.TabIndex = 1001;
			this.gridCheck.Visible = false;
			this.gridCheck.ComboCloseUp += new System.EventHandler(this.gridCheck_ComboCloseUp);
			// 
			// GridDeds
			// 
			this.GridDeds.Cols = 7;
			this.GridDeds.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridDeds.ExtendLastCol = true;
			this.GridDeds.FixedCols = 0;
			this.GridDeds.Location = new System.Drawing.Point(30, 207);
			this.GridDeds.Name = "GridDeds";
			this.GridDeds.ReadOnly = false;
			this.GridDeds.RowHeadersVisible = false;
			this.GridDeds.Rows = 1;
			this.GridDeds.Size = new System.Drawing.Size(602, 98);
			this.GridDeds.StandardTab = false;
			this.GridDeds.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDeds.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.GridDeds, "Use Insert key to add new deduction adjustments");
			this.GridDeds.ComboDropDown += new System.EventHandler(this.GridDeds_ComboDropDown);
			this.GridDeds.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridDeds_BeforeRowColChange);
			this.GridDeds.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridDeds_AfterEdit);
			this.GridDeds.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridDeds_ChangeEdit);
			this.GridDeds.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDeds_KeyDownEvent);
			// 
			// GridTotals
			// 
			this.GridTotals.Cols = 5;
			this.GridTotals.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridTotals.ExtendLastCol = true;
			this.GridTotals.Location = new System.Drawing.Point(30, 543);
			this.GridTotals.Name = "GridTotals";
			this.GridTotals.ReadOnly = false;
			this.GridTotals.Rows = 3;
			this.GridTotals.Size = new System.Drawing.Size(393, 53);
			this.GridTotals.StandardTab = false;
			this.GridTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridTotals.TabIndex = 2;
			this.GridTotals.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridTotals_ChangeEdit);
			// 
			// GridMatch
			// 
			this.GridMatch.Cols = 8;
			this.GridMatch.ExtendLastCol = true;
			this.GridMatch.FixedCols = 0;
			this.GridMatch.Location = new System.Drawing.Point(30, 325);
			this.GridMatch.Name = "GridMatch";
			this.GridMatch.RowHeadersVisible = false;
			this.GridMatch.Rows = 1;
			this.GridMatch.Size = new System.Drawing.Size(602, 89);
			this.GridMatch.StandardTab = false;
			this.GridMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridMatch.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.GridMatch, "Use Insert key to add new match adjustment records");
			this.GridMatch.ComboDropDown += new System.EventHandler(this.GridMatch_ComboDropDown);
			this.GridMatch.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridMatch_BeforeRowColChange);
			this.GridMatch.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridMatch_AfterEdit);
			this.GridMatch.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridMatch_ChangeEdit);
			this.GridMatch.KeyDown += new Wisej.Web.KeyEventHandler(this.GridMatch_KeyDownEvent);
			// 
			// GridDist
			// 
			this.GridDist.Cols = 8;
			this.GridDist.ExtendLastCol = true;
			this.GridDist.FixedCols = 0;
			this.GridDist.Location = new System.Drawing.Point(30, 59);
			this.GridDist.Name = "GridDist";
			this.GridDist.RowHeadersVisible = false;
			this.GridDist.Rows = 1;
			this.GridDist.Size = new System.Drawing.Size(602, 128);
			this.GridDist.StandardTab = false;
			this.GridDist.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridDist.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.GridDist, "Use Insert key to add adjustment records");
			this.GridDist.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridDist_BeforeRowColChange);
			this.GridDist.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridDist_AfterEdit);
			this.GridDist.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridDist_ChangeEdit);
			this.GridDist.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDist_KeyDownEvent);
			// 
			// GridPay
			// 
			this.GridPay.ColumnHeadersHeight = 90;
			this.GridPay.ExtendLastCol = true;
			this.GridPay.FixedCols = 0;
			this.GridPay.Location = new System.Drawing.Point(443, 543);
			this.GridPay.Name = "GridPay";
			this.GridPay.RowHeadersVisible = false;
			this.GridPay.Rows = 3;
			this.GridPay.Size = new System.Drawing.Size(189, 53);
			this.GridPay.TabIndex = 5;
			// 
			// GridVacSick
			// 
			this.GridVacSick.Cols = 5;
			this.GridVacSick.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridVacSick.ExtendLastCol = true;
			this.GridVacSick.FixedCols = 0;
			this.GridVacSick.Location = new System.Drawing.Point(30, 434);
			this.GridVacSick.Name = "GridVacSick";
			this.GridVacSick.ReadOnly = false;
			this.GridVacSick.RowHeadersVisible = false;
			this.GridVacSick.Rows = 1;
			this.GridVacSick.Size = new System.Drawing.Size(393, 89);
			this.GridVacSick.StandardTab = false;
			this.GridVacSick.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridVacSick.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.GridVacSick, "Use Insert key to add new adjustment records");
			this.GridVacSick.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridVacSick_AfterEdit);
			this.GridVacSick.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridVacSick_ChangeEdit);
			this.GridVacSick.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridVacSick_MouseMoveEvent);
			this.GridVacSick.KeyDown += new Wisej.Web.KeyEventHandler(this.GridVacSick_KeyDownEvent);
			// 
			// lblEmployee
			// 
			this.lblEmployee.Location = new System.Drawing.Point(30, 30);
			this.lblEmployee.Name = "lblEmployee";
			this.lblEmployee.Size = new System.Drawing.Size(619, 15);
			this.lblEmployee.TabIndex = 7;
			this.lblEmployee.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 0;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(288, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(79, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmEditCalculatedRecords
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(678, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditCalculatedRecords";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmEditCalculatedRecords_Load);
			this.Resize += new System.EventHandler(this.frmEditCalculatedRecords_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditCalculatedRecords_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDist)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVacSick)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
    }
}
