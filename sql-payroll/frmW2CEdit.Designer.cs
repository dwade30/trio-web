//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2CEdit.
	/// </summary>
	partial class frmW2CEdit
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label3;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label4;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> label10;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label7;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label5;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label9;
		public FCGrid Grid;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtSSN;
		public fecherFoundation.FCCheckBox chkCorrectingSSNorName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtMI;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtDesig;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCLabel Label3_4;
		public fecherFoundation.FCLabel Label3_3;
		public fecherFoundation.FCLabel Label3_2;
		public fecherFoundation.FCLabel Label3_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label4_0;
		public fecherFoundation.FCLabel Label3_0;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtIncorrectMI;
		public fecherFoundation.FCTextBox txtIncorrectLast;
		public fecherFoundation.FCTextBox txtIncorrectDesig;
		public fecherFoundation.FCTextBox txtIncorrectFirst;
		public fecherFoundation.FCTextBox txtIncorrectSSN;
		public fecherFoundation.FCLabel lblIncorrectFirst;
		public fecherFoundation.FCLabel lblIncorrectLast;
		public fecherFoundation.FCLabel lblIncorrectMI;
		public fecherFoundation.FCLabel lblIncorrectDesig;
		public fecherFoundation.FCLabel lblIncorrectSSN;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtCurrDependentCare;
		public fecherFoundation.FCTextBox txtCurrTips;
		public fecherFoundation.FCTextBox txtCurrMedTax;
		public fecherFoundation.FCTextBox txtCurrSSTax;
		public fecherFoundation.FCTextBox txtCurrFedTax;
		public FCGrid GridCurr12;
		public fecherFoundation.FCLabel label10_17;
		public fecherFoundation.FCLabel label10_16;
		public fecherFoundation.FCLabel label10_15;
		public fecherFoundation.FCLabel label10_11;
		public fecherFoundation.FCLabel Label7_1;
		public fecherFoundation.FCLabel Label5_2;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtPrevFedTax;
		public fecherFoundation.FCTextBox txtPrevSSTax;
		public fecherFoundation.FCTextBox txtPrevMedTax;
		public fecherFoundation.FCTextBox txtPrevTips;
		public fecherFoundation.FCTextBox txtPrevDependentCare;
		public FCGrid GridPrev12;
		public fecherFoundation.FCLabel Label5_1;
		public fecherFoundation.FCLabel Label7_0;
		public fecherFoundation.FCLabel label10_14;
		public fecherFoundation.FCLabel label10_13;
		public fecherFoundation.FCLabel label10_12;
		public fecherFoundation.FCLabel label10_10;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtCurrWages;
		public fecherFoundation.FCTextBox txtCurrSSWages;
		public fecherFoundation.FCTextBox txtCurrMedicare;
		public fecherFoundation.FCTextBox txtCurrSSTips;
		public fecherFoundation.FCTextBox txtCurrNonqualifiedPlans;
		public fecherFoundation.FCCheckBox chkCurrStatEmployee;
		public fecherFoundation.FCCheckBox chkCurrRetPlan;
		public fecherFoundation.FCCheckBox chkCurrThirdParty;
		public FCGrid GridCurr14;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel label10_9;
		public fecherFoundation.FCLabel label10_8;
		public fecherFoundation.FCLabel label10_6;
		public fecherFoundation.FCLabel label10_5;
		public fecherFoundation.FCFrame Frame4;
		public FCGrid GridPrev14;
		public fecherFoundation.FCCheckBox chkPrevThirdParty;
		public fecherFoundation.FCCheckBox chkPrevRetPlan;
		public fecherFoundation.FCCheckBox chkPrevStatEmployee;
		public fecherFoundation.FCTextBox txtPrevNonQualifiedPlans;
		public fecherFoundation.FCTextBox txtPrevSSTips;
		public fecherFoundation.FCTextBox txtPrevMedicare;
		public fecherFoundation.FCTextBox txtPrevSSWages;
		public fecherFoundation.FCTextBox txtPrevWages;
		public fecherFoundation.FCLabel label10_4;
		public fecherFoundation.FCLabel label10_3;
		public fecherFoundation.FCLabel label10_1;
		public fecherFoundation.FCLabel label10_0;
		public fecherFoundation.FCLabel Label9_0;
		public fecherFoundation.FCLabel Label5_0;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame Frame12;
		public fecherFoundation.FCTextBox txtPrevStateIncome2;
		public fecherFoundation.FCTextBox txtPrevStateWages2;
		public fecherFoundation.FCTextBox txtPrevStateID2;
		public fecherFoundation.FCTextBox txtPrevState2;
		public fecherFoundation.FCLabel label10_33;
		public fecherFoundation.FCLabel label10_32;
		public fecherFoundation.FCLabel label10_31;
		public fecherFoundation.FCLabel Label5_8;
		public fecherFoundation.FCFrame Frame11;
		public fecherFoundation.FCTextBox txtCurrState2;
		public fecherFoundation.FCTextBox txtCurrStateID2;
		public fecherFoundation.FCTextBox txtCurrStateWages2;
		public fecherFoundation.FCTextBox txtCurrStateIncome2;
		public fecherFoundation.FCLabel Label5_7;
		public fecherFoundation.FCLabel label10_30;
		public fecherFoundation.FCLabel label10_29;
		public fecherFoundation.FCLabel label10_18;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtCurrStateIncome;
		public fecherFoundation.FCTextBox txtCurrStateWages;
		public fecherFoundation.FCTextBox txtCurrStateID;
		public fecherFoundation.FCTextBox txtCurrState;
		public fecherFoundation.FCLabel label10_25;
		public fecherFoundation.FCLabel label10_24;
		public fecherFoundation.FCLabel label10_26;
		public fecherFoundation.FCLabel Label5_4;
		public fecherFoundation.FCFrame Frame8;
		public fecherFoundation.FCTextBox txtPrevState;
		public fecherFoundation.FCTextBox txtPrevStateID;
		public fecherFoundation.FCTextBox txtPrevStateWages;
		public fecherFoundation.FCTextBox txtPrevStateIncome;
		public fecherFoundation.FCLabel Label5_3;
		public fecherFoundation.FCLabel label10_27;
		public fecherFoundation.FCLabel label10_22;
		public fecherFoundation.FCLabel label10_21;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Grid = new fecherFoundation.FCGrid();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtSSN = new fecherFoundation.FCTextBox();
            this.chkCorrectingSSNorName = new fecherFoundation.FCCheckBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.txtMI = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.txtDesig = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.Label3_4 = new fecherFoundation.FCLabel();
            this.Label3_3 = new fecherFoundation.FCLabel();
            this.Label3_2 = new fecherFoundation.FCLabel();
            this.Label3_1 = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label4_0 = new fecherFoundation.FCLabel();
            this.Label3_0 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtIncorrectMI = new fecherFoundation.FCTextBox();
            this.txtIncorrectLast = new fecherFoundation.FCTextBox();
            this.txtIncorrectDesig = new fecherFoundation.FCTextBox();
            this.txtIncorrectFirst = new fecherFoundation.FCTextBox();
            this.txtIncorrectSSN = new fecherFoundation.FCTextBox();
            this.lblIncorrectFirst = new fecherFoundation.FCLabel();
            this.lblIncorrectLast = new fecherFoundation.FCLabel();
            this.lblIncorrectMI = new fecherFoundation.FCLabel();
            this.lblIncorrectDesig = new fecherFoundation.FCLabel();
            this.lblIncorrectSSN = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtCurrDependentCare = new fecherFoundation.FCTextBox();
            this.txtCurrTips = new fecherFoundation.FCTextBox();
            this.txtCurrMedTax = new fecherFoundation.FCTextBox();
            this.txtCurrSSTax = new fecherFoundation.FCTextBox();
            this.txtCurrFedTax = new fecherFoundation.FCTextBox();
            this.GridCurr12 = new fecherFoundation.FCGrid();
            this.label10_17 = new fecherFoundation.FCLabel();
            this.label10_16 = new fecherFoundation.FCLabel();
            this.label10_15 = new fecherFoundation.FCLabel();
            this.label10_11 = new fecherFoundation.FCLabel();
            this.Label7_1 = new fecherFoundation.FCLabel();
            this.Label5_2 = new fecherFoundation.FCLabel();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtPrevFedTax = new fecherFoundation.FCTextBox();
            this.txtPrevSSTax = new fecherFoundation.FCTextBox();
            this.txtPrevMedTax = new fecherFoundation.FCTextBox();
            this.txtPrevTips = new fecherFoundation.FCTextBox();
            this.txtPrevDependentCare = new fecherFoundation.FCTextBox();
            this.GridPrev12 = new fecherFoundation.FCGrid();
            this.Label5_1 = new fecherFoundation.FCLabel();
            this.Label7_0 = new fecherFoundation.FCLabel();
            this.label10_14 = new fecherFoundation.FCLabel();
            this.label10_13 = new fecherFoundation.FCLabel();
            this.label10_12 = new fecherFoundation.FCLabel();
            this.label10_10 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtCurrWages = new fecherFoundation.FCTextBox();
            this.txtCurrSSWages = new fecherFoundation.FCTextBox();
            this.txtCurrMedicare = new fecherFoundation.FCTextBox();
            this.txtCurrSSTips = new fecherFoundation.FCTextBox();
            this.txtCurrNonqualifiedPlans = new fecherFoundation.FCTextBox();
            this.chkCurrStatEmployee = new fecherFoundation.FCCheckBox();
            this.chkCurrRetPlan = new fecherFoundation.FCCheckBox();
            this.chkCurrThirdParty = new fecherFoundation.FCCheckBox();
            this.GridCurr14 = new fecherFoundation.FCGrid();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.label10_9 = new fecherFoundation.FCLabel();
            this.label10_8 = new fecherFoundation.FCLabel();
            this.label10_6 = new fecherFoundation.FCLabel();
            this.label10_5 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.GridPrev14 = new fecherFoundation.FCGrid();
            this.chkPrevThirdParty = new fecherFoundation.FCCheckBox();
            this.chkPrevRetPlan = new fecherFoundation.FCCheckBox();
            this.chkPrevStatEmployee = new fecherFoundation.FCCheckBox();
            this.txtPrevNonQualifiedPlans = new fecherFoundation.FCTextBox();
            this.txtPrevSSTips = new fecherFoundation.FCTextBox();
            this.txtPrevMedicare = new fecherFoundation.FCTextBox();
            this.txtPrevSSWages = new fecherFoundation.FCTextBox();
            this.txtPrevWages = new fecherFoundation.FCTextBox();
            this.label10_4 = new fecherFoundation.FCLabel();
            this.label10_3 = new fecherFoundation.FCLabel();
            this.label10_1 = new fecherFoundation.FCLabel();
            this.label10_0 = new fecherFoundation.FCLabel();
            this.Label9_0 = new fecherFoundation.FCLabel();
            this.Label5_0 = new fecherFoundation.FCLabel();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.Frame12 = new fecherFoundation.FCFrame();
            this.txtPrevStateIncome2 = new fecherFoundation.FCTextBox();
            this.txtPrevStateWages2 = new fecherFoundation.FCTextBox();
            this.txtPrevStateID2 = new fecherFoundation.FCTextBox();
            this.txtPrevState2 = new fecherFoundation.FCTextBox();
            this.label10_33 = new fecherFoundation.FCLabel();
            this.label10_32 = new fecherFoundation.FCLabel();
            this.label10_31 = new fecherFoundation.FCLabel();
            this.Label5_8 = new fecherFoundation.FCLabel();
            this.Frame11 = new fecherFoundation.FCFrame();
            this.txtCurrState2 = new fecherFoundation.FCTextBox();
            this.txtCurrStateID2 = new fecherFoundation.FCTextBox();
            this.txtCurrStateWages2 = new fecherFoundation.FCTextBox();
            this.txtCurrStateIncome2 = new fecherFoundation.FCTextBox();
            this.Label5_7 = new fecherFoundation.FCLabel();
            this.label10_30 = new fecherFoundation.FCLabel();
            this.label10_29 = new fecherFoundation.FCLabel();
            this.label10_18 = new fecherFoundation.FCLabel();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.txtCurrStateIncome = new fecherFoundation.FCTextBox();
            this.txtCurrStateWages = new fecherFoundation.FCTextBox();
            this.txtCurrStateID = new fecherFoundation.FCTextBox();
            this.txtCurrState = new fecherFoundation.FCTextBox();
            this.label10_25 = new fecherFoundation.FCLabel();
            this.label10_24 = new fecherFoundation.FCLabel();
            this.label10_26 = new fecherFoundation.FCLabel();
            this.Label5_4 = new fecherFoundation.FCLabel();
            this.Frame8 = new fecherFoundation.FCFrame();
            this.txtPrevState = new fecherFoundation.FCTextBox();
            this.txtPrevStateID = new fecherFoundation.FCTextBox();
            this.txtPrevStateWages = new fecherFoundation.FCTextBox();
            this.txtPrevStateIncome = new fecherFoundation.FCTextBox();
            this.Label5_3 = new fecherFoundation.FCLabel();
            this.label10_27 = new fecherFoundation.FCLabel();
            this.label10_22 = new fecherFoundation.FCLabel();
            this.label10_21 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCorrectingSSNorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCurr12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrev12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrStatEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrRetPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrThirdParty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCurr14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrev14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevThirdParty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevRetPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevStatEmployee)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).BeginInit();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).BeginInit();
            this.Frame11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
            this.Frame8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 777);
            this.BottomPanel.Size = new System.Drawing.Size(988, 86);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Size = new System.Drawing.Size(1008, 628);
            this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1008, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(65, 28);
            this.HeaderText.Text = "W-2C";
            // 
            // Grid
            // 
            this.Grid.Cols = 1;
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(491, 35);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 0;
            this.Grid.Size = new System.Drawing.Size(14, 2);
            this.Grid.TabIndex = 141;
            this.Grid.Visible = false;
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Location = new System.Drawing.Point(30, 30);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(942, 747);
            this.SSTab1.TabIndex = 76;
            this.SSTab1.Text = "Employee";
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.Frame1);
            this.SSTab1_Page1.Controls.Add(this.Frame2);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(940, 711);
            this.SSTab1_Page1.Text = "Employee";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtSSN);
            this.Frame1.Controls.Add(this.chkCorrectingSSNorName);
            this.Frame1.Controls.Add(this.txtFirstName);
            this.Frame1.Controls.Add(this.txtMI);
            this.Frame1.Controls.Add(this.txtLastName);
            this.Frame1.Controls.Add(this.txtDesig);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.Label3_4);
            this.Frame1.Controls.Add(this.Label3_3);
            this.Frame1.Controls.Add(this.Label3_2);
            this.Frame1.Controls.Add(this.Label3_1);
            this.Frame1.Controls.Add(this.Label2_0);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label4_0);
            this.Frame1.Controls.Add(this.Label3_0);
            this.Frame1.Location = new System.Drawing.Point(20, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(544, 370);
            this.Frame1.TabIndex = 77;
            this.Frame1.Text = "Correct Information";
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Location = new System.Drawing.Point(20, 240);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(253, 40);
            this.txtAddress2.TabIndex = 7;
            this.txtAddress2.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress2_Validating);
            // 
            // txtSSN
            // 
            this.txtSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtSSN.Enabled = false;
            this.txtSSN.Location = new System.Drawing.Point(20, 49);
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Size = new System.Drawing.Size(193, 40);
            this.txtSSN.TabIndex = 8;
            this.txtSSN.Validating += new System.ComponentModel.CancelEventHandler(this.txtSSN_Validating);
            // 
            // chkCorrectingSSNorName
            // 
            this.chkCorrectingSSNorName.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCorrectingSSNorName.Location = new System.Drawing.Point(228, 56);
            this.chkCorrectingSSNorName.Name = "chkCorrectingSSNorName";
            this.chkCorrectingSSNorName.Size = new System.Drawing.Size(196, 22);
            this.chkCorrectingSSNorName.TabIndex = 1;
            this.chkCorrectingSSNorName.Text = "Corrected SSN and/or name";
            this.chkCorrectingSSNorName.CheckedChanged += new System.EventHandler(this.chkCorrectingSSNorName_CheckedChanged);
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Location = new System.Drawing.Point(20, 119);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(193, 40);
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstName_Validating);
            // 
            // txtMI
            // 
            this.txtMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtMI.Location = new System.Drawing.Point(223, 119);
            this.txtMI.Name = "txtMI";
            this.txtMI.Size = new System.Drawing.Size(50, 40);
            this.txtMI.TabIndex = 3;
            this.txtMI.Validating += new System.ComponentModel.CancelEventHandler(this.txtMI_Validating);
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Location = new System.Drawing.Point(283, 119);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(179, 40);
            this.txtLastName.TabIndex = 4;
            this.txtLastName.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastName_Validating);
            // 
            // txtDesig
            // 
            this.txtDesig.BackColor = System.Drawing.SystemColors.Window;
            this.txtDesig.Location = new System.Drawing.Point(472, 119);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.Size = new System.Drawing.Size(52, 40);
            this.txtDesig.TabIndex = 5;
            this.txtDesig.Validating += new System.ComponentModel.CancelEventHandler(this.txtDesig_Validating);
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Location = new System.Drawing.Point(20, 190);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(253, 40);
            this.txtAddress1.TabIndex = 6;
            this.txtAddress1.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress1_Validating);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(20, 310);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(193, 40);
            this.txtCity.TabIndex = 8;
            this.txtCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtCity_Validating);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(223, 310);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(50, 40);
            this.txtState.TabIndex = 9;
            this.txtState.Validating += new System.ComponentModel.CancelEventHandler(this.txtState_Validating);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(283, 310);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(179, 40);
            this.txtZip.TabIndex = 10;
            this.txtZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip_Validating);
            // 
            // Label3_4
            // 
            this.Label3_4.Location = new System.Drawing.Point(283, 289);
            this.Label3_4.Name = "Label3_4";
            this.Label3_4.Size = new System.Drawing.Size(41, 15);
            this.Label3_4.TabIndex = 87;
            this.Label3_4.Text = "ZIP";
            // 
            // Label3_3
            // 
            this.Label3_3.Location = new System.Drawing.Point(223, 290);
            this.Label3_3.Name = "Label3_3";
            this.Label3_3.Size = new System.Drawing.Size(22, 13);
            this.Label3_3.TabIndex = 86;
            this.Label3_3.Text = "ST";
            // 
            // Label3_2
            // 
            this.Label3_2.Location = new System.Drawing.Point(20, 290);
            this.Label3_2.Name = "Label3_2";
            this.Label3_2.Size = new System.Drawing.Size(46, 13);
            this.Label3_2.TabIndex = 85;
            this.Label3_2.Text = "CITY";
            // 
            // Label3_1
            // 
            this.Label3_1.Location = new System.Drawing.Point(20, 169);
            this.Label3_1.Name = "Label3_1";
            this.Label3_1.Size = new System.Drawing.Size(186, 13);
            this.Label3_1.TabIndex = 84;
            this.Label3_1.Text = "ADDRESS";
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(20, 30);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(117, 13);
            this.Label2_0.TabIndex = 83;
            this.Label2_0.Text = "SSN";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(472, 99);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(52, 14);
            this.Label1_2.TabIndex = 81;
            this.Label1_2.Text = "DESIG";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(223, 99);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(39, 13);
            this.Label1_1.TabIndex = 80;
            this.Label1_1.Text = "MI";
            // 
            // Label4_0
            // 
            this.Label4_0.Location = new System.Drawing.Point(283, 99);
            this.Label4_0.Name = "Label4_0";
            this.Label4_0.Size = new System.Drawing.Size(117, 13);
            this.Label4_0.TabIndex = 79;
            this.Label4_0.Text = "LAST";
            // 
            // Label3_0
            // 
            this.Label3_0.Location = new System.Drawing.Point(20, 99);
            this.Label3_0.Name = "Label3_0";
            this.Label3_0.Size = new System.Drawing.Size(117, 13);
            this.Label3_0.TabIndex = 78;
            this.Label3_0.Text = "FIRST";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtIncorrectMI);
            this.Frame2.Controls.Add(this.txtIncorrectLast);
            this.Frame2.Controls.Add(this.txtIncorrectDesig);
            this.Frame2.Controls.Add(this.txtIncorrectFirst);
            this.Frame2.Controls.Add(this.txtIncorrectSSN);
            this.Frame2.Controls.Add(this.lblIncorrectFirst);
            this.Frame2.Controls.Add(this.lblIncorrectLast);
            this.Frame2.Controls.Add(this.lblIncorrectMI);
            this.Frame2.Controls.Add(this.lblIncorrectDesig);
            this.Frame2.Controls.Add(this.lblIncorrectSSN);
            this.Frame2.Enabled = false;
            this.Frame2.Location = new System.Drawing.Point(20, 410);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(660, 111);
            this.Frame2.TabIndex = 82;
            this.Frame2.Text = "Incorrect Information";
            // 
            // txtIncorrectMI
            // 
            this.txtIncorrectMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectMI.Enabled = false;
            this.txtIncorrectMI.Location = new System.Drawing.Point(335, 50);
            this.txtIncorrectMI.Name = "txtIncorrectMI";
            this.txtIncorrectMI.Size = new System.Drawing.Size(50, 40);
            this.txtIncorrectMI.TabIndex = 13;
            this.txtIncorrectMI.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncorrectMI_Validating);
            // 
            // txtIncorrectLast
            // 
            this.txtIncorrectLast.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectLast.Enabled = false;
            this.txtIncorrectLast.Location = new System.Drawing.Point(395, 50);
            this.txtIncorrectLast.Name = "txtIncorrectLast";
            this.txtIncorrectLast.Size = new System.Drawing.Size(173, 40);
            this.txtIncorrectLast.TabIndex = 14;
            this.txtIncorrectLast.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncorrectLast_Validating);
            // 
            // txtIncorrectDesig
            // 
            this.txtIncorrectDesig.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectDesig.Enabled = false;
            this.txtIncorrectDesig.Location = new System.Drawing.Point(578, 50);
            this.txtIncorrectDesig.Name = "txtIncorrectDesig";
            this.txtIncorrectDesig.Size = new System.Drawing.Size(60, 40);
            this.txtIncorrectDesig.TabIndex = 15;
            this.txtIncorrectDesig.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncorrectDesig_Validating);
            // 
            // txtIncorrectFirst
            // 
            this.txtIncorrectFirst.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectFirst.Enabled = false;
            this.txtIncorrectFirst.Location = new System.Drawing.Point(144, 50);
            this.txtIncorrectFirst.Name = "txtIncorrectFirst";
            this.txtIncorrectFirst.Size = new System.Drawing.Size(181, 40);
            this.txtIncorrectFirst.TabIndex = 12;
            this.txtIncorrectFirst.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncorrectFirst_Validating);
            // 
            // txtIncorrectSSN
            // 
            this.txtIncorrectSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtIncorrectSSN.Enabled = false;
            this.txtIncorrectSSN.Location = new System.Drawing.Point(20, 50);
            this.txtIncorrectSSN.Name = "txtIncorrectSSN";
            this.txtIncorrectSSN.Size = new System.Drawing.Size(114, 40);
            this.txtIncorrectSSN.TabIndex = 11;
            this.txtIncorrectSSN.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncorrectSSN_Validating);
            // 
            // lblIncorrectFirst
            // 
            this.lblIncorrectFirst.Enabled = false;
            this.lblIncorrectFirst.Location = new System.Drawing.Point(144, 30);
            this.lblIncorrectFirst.Name = "lblIncorrectFirst";
            this.lblIncorrectFirst.Size = new System.Drawing.Size(117, 13);
            this.lblIncorrectFirst.TabIndex = 132;
            this.lblIncorrectFirst.Text = "FIRST";
            // 
            // lblIncorrectLast
            // 
            this.lblIncorrectLast.Enabled = false;
            this.lblIncorrectLast.Location = new System.Drawing.Point(395, 30);
            this.lblIncorrectLast.Name = "lblIncorrectLast";
            this.lblIncorrectLast.Size = new System.Drawing.Size(117, 13);
            this.lblIncorrectLast.TabIndex = 131;
            this.lblIncorrectLast.Text = "LAST";
            // 
            // lblIncorrectMI
            // 
            this.lblIncorrectMI.Enabled = false;
            this.lblIncorrectMI.Location = new System.Drawing.Point(335, 30);
            this.lblIncorrectMI.Name = "lblIncorrectMI";
            this.lblIncorrectMI.Size = new System.Drawing.Size(39, 13);
            this.lblIncorrectMI.TabIndex = 130;
            this.lblIncorrectMI.Text = "MI";
            // 
            // lblIncorrectDesig
            // 
            this.lblIncorrectDesig.Enabled = false;
            this.lblIncorrectDesig.Location = new System.Drawing.Point(578, 30);
            this.lblIncorrectDesig.Name = "lblIncorrectDesig";
            this.lblIncorrectDesig.Size = new System.Drawing.Size(52, 14);
            this.lblIncorrectDesig.TabIndex = 129;
            this.lblIncorrectDesig.Text = "DESIG";
            // 
            // lblIncorrectSSN
            // 
            this.lblIncorrectSSN.Enabled = false;
            this.lblIncorrectSSN.Location = new System.Drawing.Point(20, 30);
            this.lblIncorrectSSN.Name = "lblIncorrectSSN";
            this.lblIncorrectSSN.Size = new System.Drawing.Size(117, 13);
            this.lblIncorrectSSN.TabIndex = 128;
            this.lblIncorrectSSN.Text = "SSN";
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.Frame7);
            this.SSTab1_Page2.Controls.Add(this.Frame6);
            this.SSTab1_Page2.Controls.Add(this.Frame5);
            this.SSTab1_Page2.Controls.Add(this.Frame4);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(940, 711);
            this.SSTab1_Page2.Text = "Federal";
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtCurrDependentCare);
            this.Frame7.Controls.Add(this.txtCurrTips);
            this.Frame7.Controls.Add(this.txtCurrMedTax);
            this.Frame7.Controls.Add(this.txtCurrSSTax);
            this.Frame7.Controls.Add(this.txtCurrFedTax);
            this.Frame7.Controls.Add(this.GridCurr12);
            this.Frame7.Controls.Add(this.label10_17);
            this.Frame7.Controls.Add(this.label10_16);
            this.Frame7.Controls.Add(this.label10_15);
            this.Frame7.Controls.Add(this.label10_11);
            this.Frame7.Controls.Add(this.Label7_1);
            this.Frame7.Controls.Add(this.Label5_2);
            this.Frame7.Location = new System.Drawing.Point(710, 20);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(210, 572);
            this.Frame7.TabIndex = 111;
            this.Frame7.Text = "Correct Information";
            // 
            // txtCurrDependentCare
            // 
            this.txtCurrDependentCare.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrDependentCare.Location = new System.Drawing.Point(20, 365);
            this.txtCurrDependentCare.Name = "txtCurrDependentCare";
            this.txtCurrDependentCare.Size = new System.Drawing.Size(170, 40);
            this.txtCurrDependentCare.TabIndex = 45;
            this.txtCurrDependentCare.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrDependentCare.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrDependentCare_Validating);
            // 
            // txtCurrTips
            // 
            this.txtCurrTips.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrTips.Location = new System.Drawing.Point(20, 288);
            this.txtCurrTips.Name = "txtCurrTips";
            this.txtCurrTips.Size = new System.Drawing.Size(170, 40);
            this.txtCurrTips.TabIndex = 43;
            this.txtCurrTips.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrTips.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrTips_Validating);
            // 
            // txtCurrMedTax
            // 
            this.txtCurrMedTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrMedTax.Location = new System.Drawing.Point(20, 211);
            this.txtCurrMedTax.Name = "txtCurrMedTax";
            this.txtCurrMedTax.Size = new System.Drawing.Size(170, 40);
            this.txtCurrMedTax.TabIndex = 41;
            this.txtCurrMedTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrMedTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrMedTax_Validating);
            // 
            // txtCurrSSTax
            // 
            this.txtCurrSSTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrSSTax.Location = new System.Drawing.Point(20, 134);
            this.txtCurrSSTax.Name = "txtCurrSSTax";
            this.txtCurrSSTax.Size = new System.Drawing.Size(170, 40);
            this.txtCurrSSTax.TabIndex = 39;
            this.txtCurrSSTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrSSTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrSSTax_Validating);
            // 
            // txtCurrFedTax
            // 
            this.txtCurrFedTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrFedTax.Location = new System.Drawing.Point(20, 57);
            this.txtCurrFedTax.Name = "txtCurrFedTax";
            this.txtCurrFedTax.Size = new System.Drawing.Size(170, 40);
            this.txtCurrFedTax.TabIndex = 37;
            this.txtCurrFedTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrFedTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrFedTax_Validating);
            // 
            // GridCurr12
            // 
            this.GridCurr12.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridCurr12.ExtendLastCol = true;
            this.GridCurr12.FixedCols = 0;
            this.GridCurr12.Location = new System.Drawing.Point(20, 442);
            this.GridCurr12.Name = "GridCurr12";
            this.GridCurr12.ReadOnly = false;
            this.GridCurr12.RowHeadersVisible = false;
            this.GridCurr12.Rows = 5;
            this.GridCurr12.Size = new System.Drawing.Size(170, 110);
            this.GridCurr12.TabIndex = 47;
            this.GridCurr12.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridCurr12_ValidateEdit);
            // 
            // label10_17
            // 
            this.label10_17.Location = new System.Drawing.Point(20, 415);
            this.label10_17.Name = "label10_17";
            this.label10_17.Size = new System.Drawing.Size(170, 16);
            this.label10_17.TabIndex = 117;
            this.label10_17.Text = "12";
            // 
            // label10_16
            // 
            this.label10_16.Location = new System.Drawing.Point(20, 338);
            this.label10_16.Name = "label10_16";
            this.label10_16.Size = new System.Drawing.Size(170, 16);
            this.label10_16.TabIndex = 116;
            this.label10_16.Text = "10. DEPENDENT CARE";
            // 
            // label10_15
            // 
            this.label10_15.Location = new System.Drawing.Point(20, 261);
            this.label10_15.Name = "label10_15";
            this.label10_15.Size = new System.Drawing.Size(170, 16);
            this.label10_15.TabIndex = 115;
            this.label10_15.Text = "8. ALLOCATED TIPS";
            // 
            // label10_11
            // 
            this.label10_11.Location = new System.Drawing.Point(20, 184);
            this.label10_11.Name = "label10_11";
            this.label10_11.Size = new System.Drawing.Size(170, 16);
            this.label10_11.TabIndex = 114;
            this.label10_11.Text = "6. MEDICARE TAX";
            // 
            // Label7_1
            // 
            this.Label7_1.Location = new System.Drawing.Point(20, 107);
            this.Label7_1.Name = "Label7_1";
            this.Label7_1.Size = new System.Drawing.Size(170, 16);
            this.Label7_1.TabIndex = 113;
            this.Label7_1.Text = "4. SOCIAL SECURITY TAX";
            // 
            // Label5_2
            // 
            this.Label5_2.Location = new System.Drawing.Point(20, 30);
            this.Label5_2.Name = "Label5_2";
            this.Label5_2.Size = new System.Drawing.Size(170, 16);
            this.Label5_2.TabIndex = 112;
            this.Label5_2.Text = "2. FEDERAL INCOME TAX";
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.txtPrevFedTax);
            this.Frame6.Controls.Add(this.txtPrevSSTax);
            this.Frame6.Controls.Add(this.txtPrevMedTax);
            this.Frame6.Controls.Add(this.txtPrevTips);
            this.Frame6.Controls.Add(this.txtPrevDependentCare);
            this.Frame6.Controls.Add(this.GridPrev12);
            this.Frame6.Controls.Add(this.Label5_1);
            this.Frame6.Controls.Add(this.Label7_0);
            this.Frame6.Controls.Add(this.label10_14);
            this.Frame6.Controls.Add(this.label10_13);
            this.Frame6.Controls.Add(this.label10_12);
            this.Frame6.Controls.Add(this.label10_10);
            this.Frame6.Location = new System.Drawing.Point(480, 20);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(210, 572);
            this.Frame6.TabIndex = 104;
            this.Frame6.Text = "Previously Reported";
            // 
            // txtPrevFedTax
            // 
            this.txtPrevFedTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevFedTax.Location = new System.Drawing.Point(20, 57);
            this.txtPrevFedTax.Name = "txtPrevFedTax";
            this.txtPrevFedTax.Size = new System.Drawing.Size(170, 40);
            this.txtPrevFedTax.TabIndex = 36;
            this.txtPrevFedTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevFedTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevFedTax_Validating);
            // 
            // txtPrevSSTax
            // 
            this.txtPrevSSTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevSSTax.Location = new System.Drawing.Point(20, 134);
            this.txtPrevSSTax.Name = "txtPrevSSTax";
            this.txtPrevSSTax.Size = new System.Drawing.Size(170, 40);
            this.txtPrevSSTax.TabIndex = 38;
            this.txtPrevSSTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevSSTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevSSTax_Validating);
            // 
            // txtPrevMedTax
            // 
            this.txtPrevMedTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevMedTax.Location = new System.Drawing.Point(20, 211);
            this.txtPrevMedTax.Name = "txtPrevMedTax";
            this.txtPrevMedTax.Size = new System.Drawing.Size(170, 40);
            this.txtPrevMedTax.TabIndex = 40;
            this.txtPrevMedTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevMedTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevMedTax_Validating);
            // 
            // txtPrevTips
            // 
            this.txtPrevTips.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevTips.Location = new System.Drawing.Point(20, 288);
            this.txtPrevTips.Name = "txtPrevTips";
            this.txtPrevTips.Size = new System.Drawing.Size(170, 40);
            this.txtPrevTips.TabIndex = 42;
            this.txtPrevTips.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevTips.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevTips_Validating);
            // 
            // txtPrevDependentCare
            // 
            this.txtPrevDependentCare.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevDependentCare.Location = new System.Drawing.Point(20, 365);
            this.txtPrevDependentCare.Name = "txtPrevDependentCare";
            this.txtPrevDependentCare.Size = new System.Drawing.Size(170, 40);
            this.txtPrevDependentCare.TabIndex = 44;
            this.txtPrevDependentCare.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevDependentCare.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevDependentCare_Validating);
            // 
            // GridPrev12
            // 
            this.GridPrev12.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPrev12.ExtendLastCol = true;
            this.GridPrev12.FixedCols = 0;
            this.GridPrev12.Location = new System.Drawing.Point(20, 442);
            this.GridPrev12.Name = "GridPrev12";
            this.GridPrev12.ReadOnly = false;
            this.GridPrev12.RowHeadersVisible = false;
            this.GridPrev12.Rows = 5;
            this.GridPrev12.Size = new System.Drawing.Size(170, 110);
            this.GridPrev12.TabIndex = 46;
            this.GridPrev12.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridPrev12_ValidateEdit);
            // 
            // Label5_1
            // 
            this.Label5_1.Location = new System.Drawing.Point(20, 30);
            this.Label5_1.Name = "Label5_1";
            this.Label5_1.Size = new System.Drawing.Size(170, 16);
            this.Label5_1.TabIndex = 110;
            this.Label5_1.Text = "2. FEDERAL INCOME TAX";
            // 
            // Label7_0
            // 
            this.Label7_0.Location = new System.Drawing.Point(20, 107);
            this.Label7_0.Name = "Label7_0";
            this.Label7_0.Size = new System.Drawing.Size(170, 16);
            this.Label7_0.TabIndex = 109;
            this.Label7_0.Text = "4. SOCIAL SECURITY TAX";
            // 
            // label10_14
            // 
            this.label10_14.Location = new System.Drawing.Point(20, 184);
            this.label10_14.Name = "label10_14";
            this.label10_14.Size = new System.Drawing.Size(170, 16);
            this.label10_14.TabIndex = 108;
            this.label10_14.Text = "6. MEDICARE TAX";
            // 
            // label10_13
            // 
            this.label10_13.Location = new System.Drawing.Point(20, 261);
            this.label10_13.Name = "label10_13";
            this.label10_13.Size = new System.Drawing.Size(170, 16);
            this.label10_13.TabIndex = 107;
            this.label10_13.Text = "8. ALLOCATED TIPS";
            // 
            // label10_12
            // 
            this.label10_12.Location = new System.Drawing.Point(20, 338);
            this.label10_12.Name = "label10_12";
            this.label10_12.Size = new System.Drawing.Size(170, 16);
            this.label10_12.TabIndex = 106;
            this.label10_12.Text = "10. DEPENDENT CARE";
            // 
            // label10_10
            // 
            this.label10_10.Location = new System.Drawing.Point(20, 415);
            this.label10_10.Name = "label10_10";
            this.label10_10.Size = new System.Drawing.Size(170, 16);
            this.label10_10.TabIndex = 105;
            this.label10_10.Text = "12";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtCurrWages);
            this.Frame5.Controls.Add(this.txtCurrSSWages);
            this.Frame5.Controls.Add(this.txtCurrMedicare);
            this.Frame5.Controls.Add(this.txtCurrSSTips);
            this.Frame5.Controls.Add(this.txtCurrNonqualifiedPlans);
            this.Frame5.Controls.Add(this.chkCurrStatEmployee);
            this.Frame5.Controls.Add(this.chkCurrRetPlan);
            this.Frame5.Controls.Add(this.chkCurrThirdParty);
            this.Frame5.Controls.Add(this.GridCurr14);
            this.Frame5.Controls.Add(this.Label11);
            this.Frame5.Controls.Add(this.Label6);
            this.Frame5.Controls.Add(this.label10_9);
            this.Frame5.Controls.Add(this.label10_8);
            this.Frame5.Controls.Add(this.label10_6);
            this.Frame5.Controls.Add(this.label10_5);
            this.Frame5.Location = new System.Drawing.Point(250, 20);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(210, 684);
            this.Frame5.TabIndex = 96;
            this.Frame5.Text = "Correct Information";
            // 
            // txtCurrWages
            // 
            this.txtCurrWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrWages.Location = new System.Drawing.Point(20, 57);
            this.txtCurrWages.Name = "txtCurrWages";
            this.txtCurrWages.Size = new System.Drawing.Size(170, 40);
            this.txtCurrWages.TabIndex = 17;
            this.txtCurrWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrWages_Validating);
            // 
            // txtCurrSSWages
            // 
            this.txtCurrSSWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrSSWages.Location = new System.Drawing.Point(20, 134);
            this.txtCurrSSWages.Name = "txtCurrSSWages";
            this.txtCurrSSWages.Size = new System.Drawing.Size(170, 40);
            this.txtCurrSSWages.TabIndex = 19;
            this.txtCurrSSWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrSSWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrSSWages_Validating);
            // 
            // txtCurrMedicare
            // 
            this.txtCurrMedicare.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrMedicare.Location = new System.Drawing.Point(20, 211);
            this.txtCurrMedicare.Name = "txtCurrMedicare";
            this.txtCurrMedicare.Size = new System.Drawing.Size(170, 40);
            this.txtCurrMedicare.TabIndex = 21;
            this.txtCurrMedicare.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrMedicare.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrMedicare_Validating);
            // 
            // txtCurrSSTips
            // 
            this.txtCurrSSTips.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrSSTips.Location = new System.Drawing.Point(20, 288);
            this.txtCurrSSTips.Name = "txtCurrSSTips";
            this.txtCurrSSTips.Size = new System.Drawing.Size(170, 40);
            this.txtCurrSSTips.TabIndex = 23;
            this.txtCurrSSTips.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrSSTips.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrSSTips_Validating);
            // 
            // txtCurrNonqualifiedPlans
            // 
            this.txtCurrNonqualifiedPlans.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrNonqualifiedPlans.Enabled = false;
            this.txtCurrNonqualifiedPlans.Location = new System.Drawing.Point(20, 366);
            this.txtCurrNonqualifiedPlans.Name = "txtCurrNonqualifiedPlans";
            this.txtCurrNonqualifiedPlans.Size = new System.Drawing.Size(170, 40);
            this.txtCurrNonqualifiedPlans.TabIndex = 27;
            this.txtCurrNonqualifiedPlans.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrNonqualifiedPlans.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrNonqualifiedPlans_Validating);
            // 
            // chkCurrStatEmployee
            // 
            this.chkCurrStatEmployee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCurrStatEmployee.Location = new System.Drawing.Point(20, 416);
            this.chkCurrStatEmployee.Name = "chkCurrStatEmployee";
            this.chkCurrStatEmployee.Size = new System.Drawing.Size(144, 22);
            this.chkCurrStatEmployee.TabIndex = 29;
            this.chkCurrStatEmployee.Text = "Statutory employee";
            // 
            // chkCurrRetPlan
            // 
            this.chkCurrRetPlan.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCurrRetPlan.Location = new System.Drawing.Point(20, 453);
            this.chkCurrRetPlan.Name = "chkCurrRetPlan";
            this.chkCurrRetPlan.Size = new System.Drawing.Size(124, 22);
            this.chkCurrRetPlan.TabIndex = 31;
            this.chkCurrRetPlan.Text = "Retirement plan";
            // 
            // chkCurrThirdParty
            // 
            this.chkCurrThirdParty.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCurrThirdParty.Location = new System.Drawing.Point(20, 490);
            this.chkCurrThirdParty.Name = "chkCurrThirdParty";
            this.chkCurrThirdParty.Size = new System.Drawing.Size(145, 22);
            this.chkCurrThirdParty.TabIndex = 33;
            this.chkCurrThirdParty.Text = "Third party sick pay";
            // 
            // GridCurr14
            // 
            this.GridCurr14.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridCurr14.ExtendLastCol = true;
            this.GridCurr14.FixedCols = 0;
            this.GridCurr14.Location = new System.Drawing.Point(20, 554);
            this.GridCurr14.Name = "GridCurr14";
            this.GridCurr14.ReadOnly = false;
            this.GridCurr14.RowHeadersVisible = false;
            this.GridCurr14.Rows = 5;
            this.GridCurr14.Size = new System.Drawing.Size(170, 110);
            this.GridCurr14.TabIndex = 35;
            this.GridCurr14.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridCurr14_ValidateEdit);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 30);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(170, 16);
            this.Label11.TabIndex = 103;
            this.Label11.Text = "1. WAGES, TIPS, OTHER";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 107);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(170, 16);
            this.Label6.TabIndex = 102;
            this.Label6.Text = "3. SOCIAL SECURITY WAGES";
            // 
            // label10_9
            // 
            this.label10_9.Location = new System.Drawing.Point(20, 184);
            this.label10_9.Name = "label10_9";
            this.label10_9.Size = new System.Drawing.Size(170, 16);
            this.label10_9.TabIndex = 101;
            this.label10_9.Text = "5. MEDICARE";
            // 
            // label10_8
            // 
            this.label10_8.Location = new System.Drawing.Point(20, 261);
            this.label10_8.Name = "label10_8";
            this.label10_8.Size = new System.Drawing.Size(170, 16);
            this.label10_8.TabIndex = 100;
            this.label10_8.Text = "7. SOCIAL SECURITY TIPS";
            // 
            // label10_6
            // 
            this.label10_6.Enabled = false;
            this.label10_6.Location = new System.Drawing.Point(20, 339);
            this.label10_6.Name = "label10_6";
            this.label10_6.Size = new System.Drawing.Size(170, 16);
            this.label10_6.TabIndex = 98;
            this.label10_6.Text = "11. NONQUALIFIED PLANS";
            // 
            // label10_5
            // 
            this.label10_5.Location = new System.Drawing.Point(20, 527);
            this.label10_5.Name = "label10_5";
            this.label10_5.Size = new System.Drawing.Size(122, 13);
            this.label10_5.TabIndex = 97;
            this.label10_5.Text = "14. OTHER";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.GridPrev14);
            this.Frame4.Controls.Add(this.chkPrevThirdParty);
            this.Frame4.Controls.Add(this.chkPrevRetPlan);
            this.Frame4.Controls.Add(this.chkPrevStatEmployee);
            this.Frame4.Controls.Add(this.txtPrevNonQualifiedPlans);
            this.Frame4.Controls.Add(this.txtPrevSSTips);
            this.Frame4.Controls.Add(this.txtPrevMedicare);
            this.Frame4.Controls.Add(this.txtPrevSSWages);
            this.Frame4.Controls.Add(this.txtPrevWages);
            this.Frame4.Controls.Add(this.label10_4);
            this.Frame4.Controls.Add(this.label10_3);
            this.Frame4.Controls.Add(this.label10_1);
            this.Frame4.Controls.Add(this.label10_0);
            this.Frame4.Controls.Add(this.Label9_0);
            this.Frame4.Controls.Add(this.Label5_0);
            this.Frame4.Location = new System.Drawing.Point(20, 20);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(210, 684);
            this.Frame4.TabIndex = 88;
            this.Frame4.Text = "Previously Reported";
            // 
            // GridPrev14
            // 
            this.GridPrev14.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPrev14.ExtendLastCol = true;
            this.GridPrev14.FixedCols = 0;
            this.GridPrev14.Location = new System.Drawing.Point(20, 554);
            this.GridPrev14.Name = "GridPrev14";
            this.GridPrev14.ReadOnly = false;
            this.GridPrev14.RowHeadersVisible = false;
            this.GridPrev14.Rows = 5;
            this.GridPrev14.Size = new System.Drawing.Size(170, 110);
            this.GridPrev14.TabIndex = 34;
            this.GridPrev14.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridPrev14_ValidateEdit);
            // 
            // chkPrevThirdParty
            // 
            this.chkPrevThirdParty.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPrevThirdParty.Location = new System.Drawing.Point(20, 490);
            this.chkPrevThirdParty.Name = "chkPrevThirdParty";
            this.chkPrevThirdParty.Size = new System.Drawing.Size(145, 22);
            this.chkPrevThirdParty.TabIndex = 32;
            this.chkPrevThirdParty.Text = "Third party sick pay";
            // 
            // chkPrevRetPlan
            // 
            this.chkPrevRetPlan.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPrevRetPlan.Location = new System.Drawing.Point(20, 453);
            this.chkPrevRetPlan.Name = "chkPrevRetPlan";
            this.chkPrevRetPlan.Size = new System.Drawing.Size(124, 22);
            this.chkPrevRetPlan.TabIndex = 30;
            this.chkPrevRetPlan.Text = "Retirement plan";
            // 
            // chkPrevStatEmployee
            // 
            this.chkPrevStatEmployee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPrevStatEmployee.Location = new System.Drawing.Point(20, 416);
            this.chkPrevStatEmployee.Name = "chkPrevStatEmployee";
            this.chkPrevStatEmployee.Size = new System.Drawing.Size(144, 22);
            this.chkPrevStatEmployee.TabIndex = 28;
            this.chkPrevStatEmployee.Text = "Statutory employee";
            // 
            // txtPrevNonQualifiedPlans
            // 
            this.txtPrevNonQualifiedPlans.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevNonQualifiedPlans.Enabled = false;
            this.txtPrevNonQualifiedPlans.Location = new System.Drawing.Point(20, 366);
            this.txtPrevNonQualifiedPlans.Name = "txtPrevNonQualifiedPlans";
            this.txtPrevNonQualifiedPlans.Size = new System.Drawing.Size(170, 40);
            this.txtPrevNonQualifiedPlans.TabIndex = 26;
            this.txtPrevNonQualifiedPlans.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevNonQualifiedPlans.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevNonQualifiedPlans_Validating);
            // 
            // txtPrevSSTips
            // 
            this.txtPrevSSTips.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevSSTips.Location = new System.Drawing.Point(20, 288);
            this.txtPrevSSTips.Name = "txtPrevSSTips";
            this.txtPrevSSTips.Size = new System.Drawing.Size(170, 40);
            this.txtPrevSSTips.TabIndex = 22;
            this.txtPrevSSTips.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevSSTips.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevSSTips_Validating);
            // 
            // txtPrevMedicare
            // 
            this.txtPrevMedicare.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevMedicare.Location = new System.Drawing.Point(20, 211);
            this.txtPrevMedicare.Name = "txtPrevMedicare";
            this.txtPrevMedicare.Size = new System.Drawing.Size(170, 40);
            this.txtPrevMedicare.TabIndex = 20;
            this.txtPrevMedicare.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevMedicare.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevMedicare_Validating);
            // 
            // txtPrevSSWages
            // 
            this.txtPrevSSWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevSSWages.Location = new System.Drawing.Point(20, 134);
            this.txtPrevSSWages.Name = "txtPrevSSWages";
            this.txtPrevSSWages.Size = new System.Drawing.Size(170, 40);
            this.txtPrevSSWages.TabIndex = 18;
            this.txtPrevSSWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevSSWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevSSWages_Validating);
            // 
            // txtPrevWages
            // 
            this.txtPrevWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevWages.Location = new System.Drawing.Point(20, 57);
            this.txtPrevWages.Name = "txtPrevWages";
            this.txtPrevWages.Size = new System.Drawing.Size(170, 40);
            this.txtPrevWages.TabIndex = 16;
            this.txtPrevWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevWages_Validating);
            // 
            // label10_4
            // 
            this.label10_4.Location = new System.Drawing.Point(20, 527);
            this.label10_4.Name = "label10_4";
            this.label10_4.Size = new System.Drawing.Size(122, 13);
            this.label10_4.TabIndex = 95;
            this.label10_4.Text = "14. OTHER";
            // 
            // label10_3
            // 
            this.label10_3.Enabled = false;
            this.label10_3.Location = new System.Drawing.Point(20, 339);
            this.label10_3.Name = "label10_3";
            this.label10_3.Size = new System.Drawing.Size(170, 16);
            this.label10_3.TabIndex = 94;
            this.label10_3.Text = "11. NONQUALIFIED PLANS";
            // 
            // label10_1
            // 
            this.label10_1.Location = new System.Drawing.Point(20, 261);
            this.label10_1.Name = "label10_1";
            this.label10_1.Size = new System.Drawing.Size(170, 16);
            this.label10_1.TabIndex = 92;
            this.label10_1.Text = "7. SOCIAL SECURITY TIPS";
            // 
            // label10_0
            // 
            this.label10_0.Location = new System.Drawing.Point(20, 184);
            this.label10_0.Name = "label10_0";
            this.label10_0.Size = new System.Drawing.Size(170, 16);
            this.label10_0.TabIndex = 91;
            this.label10_0.Text = "5. MEDICARE";
            // 
            // Label9_0
            // 
            this.Label9_0.Location = new System.Drawing.Point(20, 107);
            this.Label9_0.Name = "Label9_0";
            this.Label9_0.Size = new System.Drawing.Size(170, 16);
            this.Label9_0.TabIndex = 90;
            this.Label9_0.Text = "3. SOCIAL SECURITY WAGES";
            // 
            // Label5_0
            // 
            this.Label5_0.Location = new System.Drawing.Point(20, 30);
            this.Label5_0.Name = "Label5_0";
            this.Label5_0.Size = new System.Drawing.Size(150, 17);
            this.Label5_0.TabIndex = 89;
            this.Label5_0.Text = "1. WAGES, TIPS, OTHER";
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.Frame12);
            this.SSTab1_Page3.Controls.Add(this.Frame11);
            this.SSTab1_Page3.Controls.Add(this.Frame9);
            this.SSTab1_Page3.Controls.Add(this.Frame8);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(940, 711);
            this.SSTab1_Page3.Text = "State";
            // 
            // Frame12
            // 
            this.Frame12.Controls.Add(this.txtPrevStateIncome2);
            this.Frame12.Controls.Add(this.txtPrevStateWages2);
            this.Frame12.Controls.Add(this.txtPrevStateID2);
            this.Frame12.Controls.Add(this.txtPrevState2);
            this.Frame12.Controls.Add(this.label10_33);
            this.Frame12.Controls.Add(this.label10_32);
            this.Frame12.Controls.Add(this.label10_31);
            this.Frame12.Controls.Add(this.Label5_8);
            this.Frame12.Location = new System.Drawing.Point(480, 20);
            this.Frame12.Name = "Frame12";
            this.Frame12.Size = new System.Drawing.Size(210, 348);
            this.Frame12.TabIndex = 142;
            this.Frame12.Text = "Previously Reported";
            // 
            // txtPrevStateIncome2
            // 
            this.txtPrevStateIncome2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateIncome2.Location = new System.Drawing.Point(20, 288);
            this.txtPrevStateIncome2.Name = "txtPrevStateIncome2";
            this.txtPrevStateIncome2.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateIncome2.TabIndex = 62;
            this.txtPrevStateIncome2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevStateIncome2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateIncome2_Validating);
            // 
            // txtPrevStateWages2
            // 
            this.txtPrevStateWages2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateWages2.Location = new System.Drawing.Point(20, 211);
            this.txtPrevStateWages2.Name = "txtPrevStateWages2";
            this.txtPrevStateWages2.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateWages2.TabIndex = 60;
            this.txtPrevStateWages2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevStateWages2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateWages2_Validating);
            // 
            // txtPrevStateID2
            // 
            this.txtPrevStateID2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateID2.Location = new System.Drawing.Point(20, 134);
            this.txtPrevStateID2.Name = "txtPrevStateID2";
            this.txtPrevStateID2.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateID2.TabIndex = 58;
            this.txtPrevStateID2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateID2_Validating);
            // 
            // txtPrevState2
            // 
            this.txtPrevState2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevState2.Location = new System.Drawing.Point(20, 57);
            this.txtPrevState2.Name = "txtPrevState2";
            this.txtPrevState2.Size = new System.Drawing.Size(170, 40);
            this.txtPrevState2.TabIndex = 56;
            this.txtPrevState2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevState2_Validating);
            // 
            // label10_33
            // 
            this.label10_33.Location = new System.Drawing.Point(20, 261);
            this.label10_33.Name = "label10_33";
            this.label10_33.Size = new System.Drawing.Size(170, 16);
            this.label10_33.TabIndex = 151;
            this.label10_33.Text = "17. STATE INCOME TAX";
            // 
            // label10_32
            // 
            this.label10_32.Location = new System.Drawing.Point(20, 184);
            this.label10_32.Name = "label10_32";
            this.label10_32.Size = new System.Drawing.Size(170, 16);
            this.label10_32.TabIndex = 150;
            this.label10_32.Text = "16. STATE WAGES, TIPS";
            // 
            // label10_31
            // 
            this.label10_31.Location = new System.Drawing.Point(20, 107);
            this.label10_31.Name = "label10_31";
            this.label10_31.Size = new System.Drawing.Size(170, 16);
            this.label10_31.TabIndex = 149;
            this.label10_31.Text = "STATE ID";
            // 
            // Label5_8
            // 
            this.Label5_8.Location = new System.Drawing.Point(20, 30);
            this.Label5_8.Name = "Label5_8";
            this.Label5_8.Size = new System.Drawing.Size(170, 16);
            this.Label5_8.TabIndex = 148;
            this.Label5_8.Text = "15. STATE";
            // 
            // Frame11
            // 
            this.Frame11.Controls.Add(this.txtCurrState2);
            this.Frame11.Controls.Add(this.txtCurrStateID2);
            this.Frame11.Controls.Add(this.txtCurrStateWages2);
            this.Frame11.Controls.Add(this.txtCurrStateIncome2);
            this.Frame11.Controls.Add(this.Label5_7);
            this.Frame11.Controls.Add(this.label10_30);
            this.Frame11.Controls.Add(this.label10_29);
            this.Frame11.Controls.Add(this.label10_18);
            this.Frame11.Location = new System.Drawing.Point(710, 20);
            this.Frame11.Name = "Frame11";
            this.Frame11.Size = new System.Drawing.Size(210, 348);
            this.Frame11.TabIndex = 147;
            this.Frame11.Text = "Correct Information";
            this.Frame11.Enter += new System.EventHandler(this.Frame11_Enter);
            // 
            // txtCurrState2
            // 
            this.txtCurrState2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrState2.Location = new System.Drawing.Point(20, 57);
            this.txtCurrState2.Name = "txtCurrState2";
            this.txtCurrState2.Size = new System.Drawing.Size(170, 40);
            this.txtCurrState2.TabIndex = 57;
            this.txtCurrState2.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrState2_Validating);
            // 
            // txtCurrStateID2
            // 
            this.txtCurrStateID2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateID2.Location = new System.Drawing.Point(20, 134);
            this.txtCurrStateID2.Name = "txtCurrStateID2";
            this.txtCurrStateID2.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateID2.TabIndex = 59;
            this.txtCurrStateID2.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateID2_Validating);
            // 
            // txtCurrStateWages2
            // 
            this.txtCurrStateWages2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateWages2.Location = new System.Drawing.Point(20, 211);
            this.txtCurrStateWages2.Name = "txtCurrStateWages2";
            this.txtCurrStateWages2.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateWages2.TabIndex = 61;
            this.txtCurrStateWages2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrStateWages2.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateWages2_Validating);
            // 
            // txtCurrStateIncome2
            // 
            this.txtCurrStateIncome2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateIncome2.Location = new System.Drawing.Point(20, 288);
            this.txtCurrStateIncome2.Name = "txtCurrStateIncome2";
            this.txtCurrStateIncome2.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateIncome2.TabIndex = 63;
            this.txtCurrStateIncome2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrStateIncome2.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateIncome2_Validating);
            // 
            // Label5_7
            // 
            this.Label5_7.Location = new System.Drawing.Point(20, 30);
            this.Label5_7.Name = "Label5_7";
            this.Label5_7.Size = new System.Drawing.Size(170, 16);
            this.Label5_7.TabIndex = 146;
            this.Label5_7.Text = "15. STATE";
            // 
            // label10_30
            // 
            this.label10_30.Location = new System.Drawing.Point(20, 107);
            this.label10_30.Name = "label10_30";
            this.label10_30.Size = new System.Drawing.Size(170, 16);
            this.label10_30.TabIndex = 145;
            this.label10_30.Text = "STATE ID";
            // 
            // label10_29
            // 
            this.label10_29.Location = new System.Drawing.Point(20, 184);
            this.label10_29.Name = "label10_29";
            this.label10_29.Size = new System.Drawing.Size(170, 16);
            this.label10_29.TabIndex = 144;
            this.label10_29.Text = "16. STATE WAGES, TIPS";
            // 
            // label10_18
            // 
            this.label10_18.Location = new System.Drawing.Point(20, 261);
            this.label10_18.Name = "label10_18";
            this.label10_18.Size = new System.Drawing.Size(170, 16);
            this.label10_18.TabIndex = 143;
            this.label10_18.Text = "17. STATE INCOME TAX";
            // 
            // Frame9
            // 
            this.Frame9.Controls.Add(this.txtCurrStateIncome);
            this.Frame9.Controls.Add(this.txtCurrStateWages);
            this.Frame9.Controls.Add(this.txtCurrStateID);
            this.Frame9.Controls.Add(this.txtCurrState);
            this.Frame9.Controls.Add(this.label10_25);
            this.Frame9.Controls.Add(this.label10_24);
            this.Frame9.Controls.Add(this.label10_26);
            this.Frame9.Controls.Add(this.Label5_4);
            this.Frame9.Location = new System.Drawing.Point(250, 20);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(210, 348);
            this.Frame9.TabIndex = 123;
            this.Frame9.Text = "Correct Information";
            this.Frame9.Enter += new System.EventHandler(this.Frame9_Enter);
            // 
            // txtCurrStateIncome
            // 
            this.txtCurrStateIncome.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateIncome.Location = new System.Drawing.Point(20, 288);
            this.txtCurrStateIncome.Name = "txtCurrStateIncome";
            this.txtCurrStateIncome.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateIncome.TabIndex = 55;
            this.txtCurrStateIncome.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrStateIncome.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateIncome_Validating);
            // 
            // txtCurrStateWages
            // 
            this.txtCurrStateWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateWages.Location = new System.Drawing.Point(20, 211);
            this.txtCurrStateWages.Name = "txtCurrStateWages";
            this.txtCurrStateWages.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateWages.TabIndex = 53;
            this.txtCurrStateWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCurrStateWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateWages_Validating);
            // 
            // txtCurrStateID
            // 
            this.txtCurrStateID.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrStateID.Location = new System.Drawing.Point(20, 134);
            this.txtCurrStateID.Name = "txtCurrStateID";
            this.txtCurrStateID.Size = new System.Drawing.Size(170, 40);
            this.txtCurrStateID.TabIndex = 51;
            this.txtCurrStateID.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrStateID_Validating);
            // 
            // txtCurrState
            // 
            this.txtCurrState.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrState.Location = new System.Drawing.Point(20, 57);
            this.txtCurrState.Name = "txtCurrState";
            this.txtCurrState.Size = new System.Drawing.Size(170, 40);
            this.txtCurrState.TabIndex = 49;
            this.txtCurrState.TextChanged += new System.EventHandler(this.txtCurrState_TextChanged);
            this.txtCurrState.Validating += new System.ComponentModel.CancelEventHandler(this.txtCurrState_Validating);
            // 
            // label10_25
            // 
            this.label10_25.Location = new System.Drawing.Point(20, 261);
            this.label10_25.Name = "label10_25";
            this.label10_25.Size = new System.Drawing.Size(170, 16);
            this.label10_25.TabIndex = 127;
            this.label10_25.Text = "17. STATE INCOME TAX";
            // 
            // label10_24
            // 
            this.label10_24.Location = new System.Drawing.Point(20, 184);
            this.label10_24.Name = "label10_24";
            this.label10_24.Size = new System.Drawing.Size(170, 16);
            this.label10_24.TabIndex = 126;
            this.label10_24.Text = "16. STATE WAGES, TIPS";
            // 
            // label10_26
            // 
            this.label10_26.Location = new System.Drawing.Point(20, 107);
            this.label10_26.Name = "label10_26";
            this.label10_26.Size = new System.Drawing.Size(170, 16);
            this.label10_26.TabIndex = 125;
            this.label10_26.Text = "STATE ID";
            // 
            // Label5_4
            // 
            this.Label5_4.Location = new System.Drawing.Point(20, 30);
            this.Label5_4.Name = "Label5_4";
            this.Label5_4.Size = new System.Drawing.Size(170, 16);
            this.Label5_4.TabIndex = 124;
            this.Label5_4.Text = "15. STATE";
            // 
            // Frame8
            // 
            this.Frame8.Controls.Add(this.txtPrevState);
            this.Frame8.Controls.Add(this.txtPrevStateID);
            this.Frame8.Controls.Add(this.txtPrevStateWages);
            this.Frame8.Controls.Add(this.txtPrevStateIncome);
            this.Frame8.Controls.Add(this.Label5_3);
            this.Frame8.Controls.Add(this.label10_27);
            this.Frame8.Controls.Add(this.label10_22);
            this.Frame8.Controls.Add(this.label10_21);
            this.Frame8.Location = new System.Drawing.Point(20, 20);
            this.Frame8.Name = "Frame8";
            this.Frame8.Size = new System.Drawing.Size(210, 348);
            this.Frame8.TabIndex = 118;
            this.Frame8.Text = "Previously Reported";
            // 
            // txtPrevState
            // 
            this.txtPrevState.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevState.Location = new System.Drawing.Point(20, 57);
            this.txtPrevState.Name = "txtPrevState";
            this.txtPrevState.Size = new System.Drawing.Size(170, 40);
            this.txtPrevState.TabIndex = 48;
            this.txtPrevState.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevState_Validating);
            // 
            // txtPrevStateID
            // 
            this.txtPrevStateID.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateID.Location = new System.Drawing.Point(20, 134);
            this.txtPrevStateID.Name = "txtPrevStateID";
            this.txtPrevStateID.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateID.TabIndex = 50;
            this.txtPrevStateID.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateID_Validating);
            // 
            // txtPrevStateWages
            // 
            this.txtPrevStateWages.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateWages.Location = new System.Drawing.Point(20, 211);
            this.txtPrevStateWages.Name = "txtPrevStateWages";
            this.txtPrevStateWages.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateWages.TabIndex = 52;
            this.txtPrevStateWages.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevStateWages.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateWages_Validating);
            // 
            // txtPrevStateIncome
            // 
            this.txtPrevStateIncome.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevStateIncome.Location = new System.Drawing.Point(20, 288);
            this.txtPrevStateIncome.Name = "txtPrevStateIncome";
            this.txtPrevStateIncome.Size = new System.Drawing.Size(170, 40);
            this.txtPrevStateIncome.TabIndex = 54;
            this.txtPrevStateIncome.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrevStateIncome.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrevStateIncome_Validating);
            // 
            // Label5_3
            // 
            this.Label5_3.Location = new System.Drawing.Point(20, 30);
            this.Label5_3.Name = "Label5_3";
            this.Label5_3.Size = new System.Drawing.Size(170, 16);
            this.Label5_3.TabIndex = 122;
            this.Label5_3.Text = "15. STATE";
            // 
            // label10_27
            // 
            this.label10_27.Location = new System.Drawing.Point(20, 107);
            this.label10_27.Name = "label10_27";
            this.label10_27.Size = new System.Drawing.Size(170, 16);
            this.label10_27.TabIndex = 121;
            this.label10_27.Text = "STATE ID";
            // 
            // label10_22
            // 
            this.label10_22.Location = new System.Drawing.Point(20, 184);
            this.label10_22.Name = "label10_22";
            this.label10_22.Size = new System.Drawing.Size(170, 16);
            this.label10_22.TabIndex = 120;
            this.label10_22.Text = "16. STATE WAGES, TIPS";
            // 
            // label10_21
            // 
            this.label10_21.Location = new System.Drawing.Point(20, 261);
            this.label10_21.Name = "label10_21";
            this.label10_21.Size = new System.Drawing.Size(170, 16);
            this.label10_21.TabIndex = 119;
            this.label10_21.Text = "17. STATE INCOME TAX";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(456, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmW2CEdit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1008, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmW2CEdit";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "W-2C";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmW2CEdit_Load);
            this.Resize += new System.EventHandler(this.frmW2CEdit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2CEdit_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCorrectingSSNorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCurr12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPrev12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrStatEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrRetPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrThirdParty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCurr14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPrev14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevThirdParty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevRetPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevStatEmployee)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).EndInit();
            this.Frame12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).EndInit();
            this.Frame11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
            this.Frame8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}