//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsPayCats
	{
		//=========================================================
		private FCCollection ListPayCats = new FCCollection();
		private int intCurrentIndex;

		public clsPayCats() : base()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsOtherType = new clsDRWrapper();
			string strSQL;
			clsPayCat tCat;
			intCurrentIndex = -1;
			strSQL = "select * from tblassociatecodes order by codetype, paycatid";
			rsOtherType.OpenRecordset(strSQL, "twpy0000.vb1");
			strSQL = "Select * from tblPayCategories order by ID";
			rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				tCat = new clsPayCat();
				tCat.CatNum = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("CategoryNumber"));
				tCat.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
				tCat.ID = FCConvert.ToInt16(rsLoad.Get_Fields("ID"));
				tCat.Multiplier = rsLoad.Get_Fields_Double("Multi");
				tCat.PayType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
				tCat.TaxStatus = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("TaxStatus"));
				tCat.MSR = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("MSR"));
				tCat.UnEmployment = FCConvert.ToBoolean(rsLoad.Get_Fields("Unemployment"));
				tCat.WorkersComp = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("WorkersComp"));
				if (rsOtherType.FindFirstRecord("paycatid", tCat.ID))
				{
					if (FCConvert.ToInt32(rsOtherType.Get_Fields("codetype")) == 1)
					{
						tCat.IsSick = true;
						tCat.IsVacation = false;
						tCat.IsHoliday = false;
						tCat.IsOther = false;
					}
					else if (FCConvert.ToInt32(rsOtherType.Get_Fields("Codetype")) == 2)
					{
						tCat.IsSick = false;
						tCat.IsVacation = true;
						tCat.IsHoliday = false;
						tCat.IsOther = false;
					}
					else
					{
						tCat.IsSick = false;
						tCat.IsVacation = false;
						tCat.IsHoliday = false;
						tCat.IsOther = true;
					}
				}
				else
				{
					tCat.IsOther = false;
					tCat.IsSick = false;
					tCat.IsVacation = false;
					tCat.IsHoliday = false;
				}
				if (tCat.Multiplier > 1 || tCat.ID == 2)
				{
					tCat.IsOT = true;
				}
				else
				{
					tCat.IsOT = false;
				}
				if (tCat.ID == 5)
				{
					tCat.IsHoliday = true;
				}
				ListPayCats.Add(tCat);
				rsLoad.MoveNext();
			}
		}

		private void ClearList()
		{
			if (!(ListPayCats == null))
			{
				foreach (clsPayCat tCat in ListPayCats)
				{
					ListPayCats.Remove(1);
				}
				// tCat
			}
		}

		public void MoveFirst()
		{
			if (!(ListPayCats == null))
			{
				if (!FCUtils.IsEmpty(ListPayCats))
				{
					if (ListPayCats.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(ListPayCats))
			{
				if (intCurrentIndex > ListPayCats.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= ListPayCats.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > ListPayCats.Count)
						{
							intReturn = -1;
							break;
						}
						else if (ListPayCats[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public bool FindID(int lngID)
		{
			bool FindID = false;
			if (lngID > 0)
			{
				if (ListPayCats.Count > 0)
				{
					int X;
					for (X = 1; X <= ListPayCats.Count - 1; X++)
					{
						if (!(ListPayCats[X] == null))
						{
							if (ListPayCats[X].ID == lngID)
							{
								FindID = true;
								intCurrentIndex = X;
								return FindID;
							}
						}
					}
					// X
					FindID = false;
				}
				else
				{
					FindID = false;
				}
			}
			else
			{
				FindID = false;
			}
			return FindID;
		}

		public clsPayCat GetCurrentCat()
		{
			clsPayCat GetCurrentCat = null;
			clsPayCat tCat;
			tCat = null;
			if (!FCUtils.IsEmpty(ListPayCats))
			{
				if (intCurrentIndex > 0)
				{
					if (!(ListPayCats[intCurrentIndex] == null))
					{
						tCat = ListPayCats[intCurrentIndex];
					}
				}
			}
			GetCurrentCat = tCat;
			return GetCurrentCat;
		}

		public clsPayCat GetCatByIndex(ref int intindex)
		{
			clsPayCat GetCatByIndex = null;
			clsPayCat tCat;
			tCat = null;
			if (!FCUtils.IsEmpty(ListPayCats) && intindex > 0)
			{
				if (!(ListPayCats[intindex] == null))
				{
					intCurrentIndex = intindex;
					tCat = ListPayCats[intindex];
				}
			}
			GetCatByIndex = tCat;
			return GetCatByIndex;
		}
	}
}
