﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeMaster.
	/// </summary>
	partial class rptEmployeeMaster
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmployeeMaster));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.lblFullName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFullName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateHired = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAnnDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBirthDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDependents = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFedStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedDep = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateDep = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSEQ = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnempExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRetFlag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSCHHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFICAExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayFrequency = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateTaxesDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTaxesDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeferredIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWorkersCompExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMedicareExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWCompCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNatureCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPartTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtW4Date = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMultipleJobs = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOtherDeduction = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherDeduction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOtherIncome = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMultipleJobs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateHired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAnnDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBirthDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDependents)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedDep)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateDep)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSEQ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnempExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetFlag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSCHHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxesDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTaxesDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeferredIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkersCompExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicareExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWCompCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNatureCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPartTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMultipleJobs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherDeduction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherDeduction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMultipleJobs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFullName,
            this.txtFullName,
            this.lblAddress,
            this.txtAddress,
            this.txtSSN,
            this.txtDateHired,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.txtAnnDate,
            this.txtBirthDate,
            this.Label7,
            this.txtStatus,
            this.Shape1,
            this.Label8,
            this.Label9,
            this.Label11,
            this.lblDependents,
            this.Label13,
            this.txtFedStatus,
            this.txtStateStatus,
            this.txtFedDep,
            this.txtStateDep,
            this.txtFedTaxes,
            this.txtStateTaxes,
            this.txtSEQ,
            this.Label14,
            this.txtSex,
            this.Label15,
            this.txtUnempExempt,
            this.Label16,
            this.Label17,
            this.txtCode1,
            this.Label18,
            this.txtCode2,
            this.txtRetFlag,
            this.Label19,
            this.txtSCHHours,
            this.Label20,
            this.txtPayRate,
            this.Label21,
            this.txtFICAExempt,
            this.Label23,
            this.txtPayFrequency,
            this.Label24,
            this.txtStateTaxesDesc,
            this.txtFedTaxesDesc,
            this.Label25,
            this.txtGroup,
            this.Label26,
            this.txtDeferredIncome,
            this.Label27,
            this.txtDeptDiv,
            this.txtWorkersCompExempt,
            this.Label28,
            this.txtMedicareExempt,
            this.Label29,
            this.Label30,
            this.txtWCompCode,
            this.txtStateUnemployment,
            this.Label32,
            this.txtNatureCode,
            this.Label33,
            this.Label34,
            this.txtPhone,
            this.Label35,
            this.txtEmail,
            this.lblPartTime,
            this.label1,
            this.txtW4Date,
            this.lblMultipleJobs,
            this.lblOtherDeduction,
            this.txtOtherDeduction,
            this.lblOtherIncome,
            this.txtOtherIncome,
            this.txtMultipleJobs});
			this.Detail.Height = 2.250167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// lblFullName
			// 
			this.lblFullName.Height = 0.1875F;
			this.lblFullName.HyperLink = null;
			this.lblFullName.Left = 0F;
			this.lblFullName.Name = "lblFullName";
			this.lblFullName.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblFullName.Text = "Full Name";
			this.lblFullName.Top = 0.0625F;
			this.lblFullName.Width = 0.6875F;
			// 
			// txtFullName
			// 
			this.txtFullName.Height = 0.1875F;
			this.txtFullName.Left = 0.875F;
			this.txtFullName.Name = "txtFullName";
			this.txtFullName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFullName.Text = null;
			this.txtFullName.Top = 0.0625F;
			this.txtFullName.Width = 1.375F;
			// 
			// lblAddress
			// 
			this.lblAddress.Height = 0.1875F;
			this.lblAddress.HyperLink = null;
			this.lblAddress.Left = 0F;
			this.lblAddress.MultiLine = false;
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblAddress.Text = "Address";
			this.lblAddress.Top = 1.5625F;
			this.lblAddress.Width = 0.6875F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.26F;
			this.txtAddress.Left = 0.875F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.5625F;
			this.txtAddress.Width = 1.375F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1875F;
			this.txtSSN.Left = 0.875F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.25F;
			this.txtSSN.Width = 1.375F;
			// 
			// txtDateHired
			// 
			this.txtDateHired.Height = 0.1875F;
			this.txtDateHired.Left = 4.625F;
			this.txtDateHired.Name = "txtDateHired";
			this.txtDateHired.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtDateHired.Text = null;
			this.txtDateHired.Top = 0.0625F;
			this.txtDateHired.Width = 0.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label3.Text = "SSN";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.9375F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label4.Text = "Date Hired";
			this.Label4.Top = 0.0625F;
			this.Label4.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label5.Text = "Anniv. Date";
			this.Label5.Top = 0.4375F;
			this.Label5.Width = 0.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.3125F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label6.Text = "Birth Date";
			this.Label6.Top = 0.4375F;
			this.Label6.Width = 0.6875F;
			// 
			// txtAnnDate
			// 
			this.txtAnnDate.Height = 0.1875F;
			this.txtAnnDate.Left = 0.875F;
			this.txtAnnDate.Name = "txtAnnDate";
			this.txtAnnDate.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtAnnDate.Text = null;
			this.txtAnnDate.Top = 0.4375F;
			this.txtAnnDate.Width = 0.625F;
			// 
			// txtBirthDate
			// 
			this.txtBirthDate.Height = 0.1875F;
			this.txtBirthDate.Left = 3.1875F;
			this.txtBirthDate.Name = "txtBirthDate";
			this.txtBirthDate.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtBirthDate.Text = null;
			this.txtBirthDate.Top = 0.4375F;
			this.txtBirthDate.Width = 0.625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.5625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label7.Text = "Status";
			this.Label7.Top = 0.0625F;
			this.Label7.Width = 0.4375F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 6F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0.0625F;
			this.txtStatus.Width = 0.625F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 1.844F;
			this.Shape1.Left = 3.875F;
			this.Shape1.LineColor = System.Drawing.Color.FromArgb(64, 64, 64);
			this.Shape1.LineWeight = 4F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.375F;
			this.Shape1.Width = 3.562F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.9375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label8.Text = "Federal";
			this.Label8.Top = 0.625F;
			this.Label8.Width = 0.5625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.9375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label9.Text = "State";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 0.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.5F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "Tax Status";
			this.Label11.Top = 0.4375F;
			this.Label11.Width = 0.8125F;
			// 
			// lblDependents
			// 
			this.lblDependents.Height = 0.21875F;
			this.lblDependents.HyperLink = null;
			this.lblDependents.Left = 5.6875F;
			this.lblDependents.Name = "lblDependents";
			this.lblDependents.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblDependents.Text = "# of Dep";
			this.lblDependents.Top = 0.4375F;
			this.lblDependents.Width = 0.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.21875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.25F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label13.Text = "Additional Taxes";
			this.Label13.Top = 0.4375F;
			this.Label13.Width = 1F;
			// 
			// txtFedStatus
			// 
			this.txtFedStatus.Height = 0.19F;
			this.txtFedStatus.Left = 4.4375F;
			this.txtFedStatus.Name = "txtFedStatus";
			this.txtFedStatus.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFedStatus.Text = null;
			this.txtFedStatus.Top = 0.65625F;
			this.txtFedStatus.Width = 1.1875F;
			// 
			// txtStateStatus
			// 
			this.txtStateStatus.Height = 0.19F;
			this.txtStateStatus.Left = 4.4375F;
			this.txtStateStatus.Name = "txtStateStatus";
			this.txtStateStatus.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtStateStatus.Text = null;
			this.txtStateStatus.Top = 0.8125F;
			this.txtStateStatus.Width = 1.1875F;
			// 
			// txtFedDep
			// 
			this.txtFedDep.Height = 0.19F;
			this.txtFedDep.Left = 5.6875F;
			this.txtFedDep.Name = "txtFedDep";
			this.txtFedDep.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFedDep.Text = null;
			this.txtFedDep.Top = 0.65625F;
			this.txtFedDep.Width = 0.4375F;
			// 
			// txtStateDep
			// 
			this.txtStateDep.Height = 0.19F;
			this.txtStateDep.Left = 5.6875F;
			this.txtStateDep.Name = "txtStateDep";
			this.txtStateDep.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtStateDep.Text = null;
			this.txtStateDep.Top = 0.8125F;
			this.txtStateDep.Width = 0.4375F;
			// 
			// txtFedTaxes
			// 
			this.txtFedTaxes.Height = 0.19F;
			this.txtFedTaxes.Left = 6.1875F;
			this.txtFedTaxes.Name = "txtFedTaxes";
			this.txtFedTaxes.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtFedTaxes.Text = null;
			this.txtFedTaxes.Top = 0.65625F;
			this.txtFedTaxes.Width = 0.5F;
			// 
			// txtStateTaxes
			// 
			this.txtStateTaxes.Height = 0.19F;
			this.txtStateTaxes.Left = 6.1875F;
			this.txtStateTaxes.Name = "txtStateTaxes";
			this.txtStateTaxes.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtStateTaxes.Text = null;
			this.txtStateTaxes.Top = 0.8125F;
			this.txtStateTaxes.Width = 0.5F;
			// 
			// txtSEQ
			// 
			this.txtSEQ.Height = 0.1875F;
			this.txtSEQ.Left = 3.1875F;
			this.txtSEQ.Name = "txtSEQ";
			this.txtSEQ.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtSEQ.Text = null;
			this.txtSEQ.Top = 0.25F;
			this.txtSEQ.Width = 0.625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.3125F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label14.Text = "SEQ";
			this.Label14.Top = 0.25F;
			this.Label14.Width = 0.6875F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1875F;
			this.txtSex.Left = 3.1875F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtSex.Text = null;
			this.txtSex.Top = 0.0625F;
			this.txtSex.Width = 0.625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.3125F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label15.Text = "Sex";
			this.Label15.Top = 0.0625F;
			this.Label15.Width = 0.6875F;
			// 
			// txtUnempExempt
			// 
			this.txtUnempExempt.Height = 0.1875F;
			this.txtUnempExempt.Left = 5.375F;
			this.txtUnempExempt.Name = "txtUnempExempt";
			this.txtUnempExempt.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtUnempExempt.Text = null;
			this.txtUnempExempt.Top = 1.563F;
			this.txtUnempExempt.Width = 0.375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.9375F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "FUTA Exempt";
			this.Label16.Top = 1.563F;
			this.Label16.Width = 1.4375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.3125F;
			this.Label17.MultiLine = false;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label17.Text = "Code 1";
			this.Label17.Top = 0.625F;
			this.Label17.Width = 0.6875F;
			// 
			// txtCode1
			// 
			this.txtCode1.Height = 0.1875F;
			this.txtCode1.Left = 3.1875F;
			this.txtCode1.Name = "txtCode1";
			this.txtCode1.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtCode1.Text = null;
			this.txtCode1.Top = 0.625F;
			this.txtCode1.Width = 0.625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.3125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label18.Text = "Code 2";
			this.Label18.Top = 0.8125F;
			this.Label18.Width = 0.6875F;
			// 
			// txtCode2
			// 
			this.txtCode2.Height = 0.1875F;
			this.txtCode2.Left = 3.1875F;
			this.txtCode2.Name = "txtCode2";
			this.txtCode2.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtCode2.Text = null;
			this.txtCode2.Top = 0.8125F;
			this.txtCode2.Width = 0.625F;
			// 
			// txtRetFlag
			// 
			this.txtRetFlag.Height = 0.1875F;
			this.txtRetFlag.Left = 3.1875F;
			this.txtRetFlag.Name = "txtRetFlag";
			this.txtRetFlag.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtRetFlag.Text = null;
			this.txtRetFlag.Top = 1F;
			this.txtRetFlag.Width = 0.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.MultiLine = false;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label19.Text = "SCH Hours";
			this.Label19.Top = 0.625F;
			this.Label19.Width = 0.6875F;
			// 
			// txtSCHHours
			// 
			this.txtSCHHours.Height = 0.1875F;
			this.txtSCHHours.Left = 0.875F;
			this.txtSCHHours.Name = "txtSCHHours";
			this.txtSCHHours.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtSCHHours.Text = null;
			this.txtSCHHours.Top = 0.625F;
			this.txtSCHHours.Width = 1F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.3125F;
			this.Label20.MultiLine = false;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label20.Text = "Retirement";
			this.Label20.Top = 1F;
			this.Label20.Width = 0.6875F;
			// 
			// txtPayRate
			// 
			this.txtPayRate.Height = 0.1875F;
			this.txtPayRate.Left = 0.875F;
			this.txtPayRate.Name = "txtPayRate";
			this.txtPayRate.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPayRate.Text = null;
			this.txtPayRate.Top = 0.8125F;
			this.txtPayRate.Width = 1F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.MultiLine = false;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label21.Text = "Pay Rate";
			this.Label21.Top = 0.8125F;
			this.Label21.Width = 0.6875F;
			// 
			// txtFICAExempt
			// 
			this.txtFICAExempt.Height = 0.1875F;
			this.txtFICAExempt.Left = 6.812F;
			this.txtFICAExempt.Name = "txtFICAExempt";
			this.txtFICAExempt.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtFICAExempt.Text = null;
			this.txtFICAExempt.Top = 1.563F;
			this.txtFICAExempt.Width = 0.375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 5.75F;
			this.Label23.MultiLine = false;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label23.Text = "FICA Exempt";
			this.Label23.Top = 1.563F;
			this.Label23.Width = 0.875F;
			// 
			// txtPayFrequency
			// 
			this.txtPayFrequency.Height = 0.1875F;
			this.txtPayFrequency.Left = 0.875F;
			this.txtPayFrequency.Name = "txtPayFrequency";
			this.txtPayFrequency.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPayFrequency.Text = null;
			this.txtPayFrequency.Top = 1F;
			this.txtPayFrequency.Width = 0.625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0F;
			this.Label24.MultiLine = false;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label24.Text = "Frequency";
			this.Label24.Top = 1F;
			this.Label24.Width = 0.6875F;
			// 
			// txtStateTaxesDesc
			// 
			this.txtStateTaxesDesc.Height = 0.19F;
			this.txtStateTaxesDesc.Left = 6.75F;
			this.txtStateTaxesDesc.Name = "txtStateTaxesDesc";
			this.txtStateTaxesDesc.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtStateTaxesDesc.Text = null;
			this.txtStateTaxesDesc.Top = 0.8125F;
			this.txtStateTaxesDesc.Width = 0.4375F;
			// 
			// txtFedTaxesDesc
			// 
			this.txtFedTaxesDesc.Height = 0.19F;
			this.txtFedTaxesDesc.Left = 6.75F;
			this.txtFedTaxesDesc.Name = "txtFedTaxesDesc";
			this.txtFedTaxesDesc.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtFedTaxesDesc.Text = null;
			this.txtFedTaxesDesc.Top = 0.65625F;
			this.txtFedTaxesDesc.Width = 0.4375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.3125F;
			this.Label25.MultiLine = false;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label25.Text = "Group";
			this.Label25.Top = 1.375F;
			this.Label25.Width = 0.6875F;
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.1875F;
			this.txtGroup.Left = 3.1875F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtGroup.Text = null;
			this.txtGroup.Top = 1.375F;
			this.txtGroup.Width = 0.625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0F;
			this.Label26.MultiLine = false;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label26.Text = "Deferred Inc.";
			this.Label26.Top = 1.1875F;
			this.Label26.Width = 0.8125F;
			// 
			// txtDeferredIncome
			// 
			this.txtDeferredIncome.Height = 0.1875F;
			this.txtDeferredIncome.Left = 0.875F;
			this.txtDeferredIncome.Name = "txtDeferredIncome";
			this.txtDeferredIncome.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtDeferredIncome.Text = null;
			this.txtDeferredIncome.Top = 1.1875F;
			this.txtDeferredIncome.Width = 0.625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 2.3125F;
			this.Label27.MultiLine = false;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label27.Text = "Home Dept/Div";
			this.Label27.Top = 1.1875F;
			this.Label27.Width = 0.875F;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.Height = 0.1875F;
			this.txtDeptDiv.Left = 3.1875F;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtDeptDiv.Text = null;
			this.txtDeptDiv.Top = 1.1875F;
			this.txtDeptDiv.Width = 0.625F;
			// 
			// txtWorkersCompExempt
			// 
			this.txtWorkersCompExempt.Height = 0.1875F;
			this.txtWorkersCompExempt.Left = 5.375F;
			this.txtWorkersCompExempt.Name = "txtWorkersCompExempt";
			this.txtWorkersCompExempt.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtWorkersCompExempt.Text = null;
			this.txtWorkersCompExempt.Top = 1.751F;
			this.txtWorkersCompExempt.Width = 0.375F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.75F;
			this.Label28.MultiLine = false;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label28.Text = "Medicare Exempt";
			this.Label28.Top = 1.751F;
			this.Label28.Width = 1.0625F;
			// 
			// txtMedicareExempt
			// 
			this.txtMedicareExempt.Height = 0.1875F;
			this.txtMedicareExempt.Left = 6.812F;
			this.txtMedicareExempt.Name = "txtMedicareExempt";
			this.txtMedicareExempt.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMedicareExempt.Text = null;
			this.txtMedicareExempt.Top = 1.751F;
			this.txtMedicareExempt.Width = 0.375F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.9375F;
			this.Label29.MultiLine = false;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label29.Text = "Workers Comp Exempt";
			this.Label29.Top = 1.751F;
			this.Label29.Width = 1.4375F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0F;
			this.Label30.MultiLine = false;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label30.Text = "W Comp Code";
			this.Label30.Top = 1.375F;
			this.Label30.Width = 0.875F;
			// 
			// txtWCompCode
			// 
			this.txtWCompCode.Height = 0.1875F;
			this.txtWCompCode.Left = 0.875F;
			this.txtWCompCode.Name = "txtWCompCode";
			this.txtWCompCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtWCompCode.Text = null;
			this.txtWCompCode.Top = 1.375F;
			this.txtWCompCode.Width = 1.375F;
			// 
			// txtStateUnemployment
			// 
			this.txtStateUnemployment.Height = 0.2083333F;
			this.txtStateUnemployment.Left = 5.375F;
			this.txtStateUnemployment.Name = "txtStateUnemployment";
			this.txtStateUnemployment.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtStateUnemployment.Text = null;
			this.txtStateUnemployment.Top = 1.939F;
			this.txtStateUnemployment.Width = 0.375F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.2083333F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 5.75F;
			this.Label32.MultiLine = false;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label32.Text = "Nature Code";
			this.Label32.Top = 1.939F;
			this.Label32.Width = 1.0625F;
			// 
			// txtNatureCode
			// 
			this.txtNatureCode.Height = 0.2083333F;
			this.txtNatureCode.Left = 6.812F;
			this.txtNatureCode.Name = "txtNatureCode";
			this.txtNatureCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtNatureCode.Text = null;
			this.txtNatureCode.Top = 1.939F;
			this.txtNatureCode.Width = 0.375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.2083333F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.9375F;
			this.Label33.MultiLine = false;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label33.Text = "State Unemployment";
			this.Label33.Top = 1.939F;
			this.Label33.Width = 1.4375F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1666667F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0F;
			this.Label34.MultiLine = false;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label34.Text = "Phone";
			this.Label34.Top = 1.821F;
			this.Label34.Width = 0.7F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1666667F;
			this.txtPhone.Left = 0.875F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 1.821F;
			this.txtPhone.Width = 1.4F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1666667F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.02F;
			this.Label35.MultiLine = false;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label35.Text = "Email";
			this.Label35.Top = 2.009F;
			this.Label35.Width = 0.7F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.19F;
			this.txtEmail.Left = 0.906F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 2.009F;
			this.txtEmail.Width = 2.625F;
			// 
			// lblPartTime
			// 
			this.lblPartTime.Height = 0.19F;
			this.lblPartTime.HyperLink = null;
			this.lblPartTime.Left = 2.312F;
			this.lblPartTime.Name = "lblPartTime";
			this.lblPartTime.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblPartTime.Text = "Part Time Employee";
			this.lblPartTime.Top = 1.751F;
			this.lblPartTime.Width = 1.21875F;
			// 
			// label1
			// 
			this.label1.Height = 0.1875F;
			this.label1.HyperLink = null;
			this.label1.Left = 3.938F;
			this.label1.Name = "label1";
			this.label1.Style = "font-size: 8.5pt; font-weight: bold";
			this.label1.Text = "W-4 Date";
			this.label1.Top = 1.192F;
			this.label1.Width = 0.5625F;
			// 
			// txtW4Date
			// 
			this.txtW4Date.Height = 0.1875F;
			this.txtW4Date.Left = 4.508F;
			this.txtW4Date.Name = "txtW4Date";
			this.txtW4Date.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtW4Date.Text = null;
			this.txtW4Date.Top = 1.192F;
			this.txtW4Date.Width = 0.8670005F;
			// 
			// lblMultipleJobs
			// 
			this.lblMultipleJobs.Height = 0.1875F;
			this.lblMultipleJobs.HyperLink = null;
			this.lblMultipleJobs.Left = 3.938F;
			this.lblMultipleJobs.Name = "lblMultipleJobs";
			this.lblMultipleJobs.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblMultipleJobs.Text = "Multiple Jobs";
			this.lblMultipleJobs.Top = 1.375F;
			this.lblMultipleJobs.Width = 0.9580002F;
			// 
			// lblOtherDeduction
			// 
			this.lblOtherDeduction.Height = 0.1875F;
			this.lblOtherDeduction.HyperLink = null;
			this.lblOtherDeduction.Left = 5.563F;
			this.lblOtherDeduction.MultiLine = false;
			this.lblOtherDeduction.Name = "lblOtherDeduction";
			this.lblOtherDeduction.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblOtherDeduction.Text = "Other Deduction";
			this.lblOtherDeduction.Top = 1.187F;
			this.lblOtherDeduction.Width = 1.0625F;
			// 
			// txtOtherDeduction
			// 
			this.txtOtherDeduction.Height = 0.1875F;
			this.txtOtherDeduction.Left = 6.631999F;
			this.txtOtherDeduction.Name = "txtOtherDeduction";
			this.txtOtherDeduction.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtOtherDeduction.Text = null;
			this.txtOtherDeduction.Top = 1.187F;
			this.txtOtherDeduction.Width = 0.7500015F;
			// 
			// lblOtherIncome
			// 
			this.lblOtherIncome.Height = 0.2083333F;
			this.lblOtherIncome.HyperLink = null;
			this.lblOtherIncome.Left = 5.563F;
			this.lblOtherIncome.MultiLine = false;
			this.lblOtherIncome.Name = "lblOtherIncome";
			this.lblOtherIncome.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblOtherIncome.Text = "Other Income";
			this.lblOtherIncome.Top = 1.375F;
			this.lblOtherIncome.Width = 1.0625F;
			// 
			// txtOtherIncome
			// 
			this.txtOtherIncome.Height = 0.188F;
			this.txtOtherIncome.Left = 6.631999F;
			this.txtOtherIncome.Name = "txtOtherIncome";
			this.txtOtherIncome.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtOtherIncome.Text = null;
			this.txtOtherIncome.Top = 1.375F;
			this.txtOtherIncome.Width = 0.7500016F;
			// 
			// txtMultipleJobs
			// 
			this.txtMultipleJobs.Height = 0.1875F;
			this.txtMultipleJobs.Left = 4.952F;
			this.txtMultipleJobs.Name = "txtMultipleJobs";
			this.txtMultipleJobs.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMultipleJobs.Text = null;
			this.txtMultipleJobs.Top = 1.375F;
			this.txtMultipleJobs.Width = 0.375F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCaption,
            this.txtDate,
            this.lblPage,
            this.txtTime,
            this.txtMuniName});
			this.PageHeader.Height = 0.4375F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Visible = false;
			this.txtCaption.Width = 7.1875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Visible = false;
			this.txtDate.Width = 1.0625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.Left = 6.1875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.25F;
			this.lblPage.Visible = false;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Visible = false;
			this.txtTime.Width = 1.375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Visible = false;
			this.txtMuniName.Width = 1.375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// rptEmployeeMaster
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.49F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateHired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAnnDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBirthDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDependents)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedDep)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateDep)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSEQ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnempExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetFlag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSCHHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTaxesDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTaxesDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeferredIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorkersCompExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicareExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWCompCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNatureCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPartTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW4Date)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMultipleJobs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherDeduction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherDeduction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMultipleJobs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFullName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateHired;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAnnDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBirthDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDependents;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedDep;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateDep;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSEQ;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnempExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRetFlag;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSCHHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayFrequency;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTaxesDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTaxesDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeferredIncome;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWorkersCompExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicareExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWCompCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateUnemployment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNatureCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPartTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtW4Date;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMultipleJobs;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherDeduction;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherDeduction;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherIncome;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherIncome;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMultipleJobs;
    }
}
