﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptLaser941.
	/// </summary>
	partial class rptLaser941
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLaser941));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.image3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSandMedicareAdjustment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5bC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5cC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFractionsOfCents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotTaxAfterAdjustment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalDeposits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBalanceDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5bC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5cC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOverpayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOverPaymentReturn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOverPaymentRefund = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCheckSemiWeekly = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCheckMonthly = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine17a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine17b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine17c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine17d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEINBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQuarter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQuarter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQuarter3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQuarter4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStreetNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransmitterPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSickPayAdjustment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTipsAdjustment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIncomeTaxAdjustment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalAdjustments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransmitterName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransmitterTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCheckLess2500 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCobraPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCobraIndividuals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCobraPlusDeposits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt6b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt6c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt6d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSmallBusCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalAfterCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployerID2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aiC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aiC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aiiC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine5aiiC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNonRefCreditSFLeaveWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNonrefEmplRetentionCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRefCreditForLeaveWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotAdvancesForm7200 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotDepDefRefCreditsLessAdv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotNonRefundableCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRefEmplRententionCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotDepDefRefCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualifiedWagesForEmployeeRetentionCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualfiedExpAllocToSickLeaveWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualifiedExpAllocToFamilyLeaveWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQualifiedExpAllocToLine21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCreditFromForm5884C = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.image3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSandMedicareAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5bC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5cC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFractionsOfCents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotTaxAfterAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDeposits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalanceDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5bC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5cC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverpayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverPaymentReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverPaymentRefund)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckSemiWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickPayAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipsAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncomeTaxAdjustment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAdjustments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckLess2500)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraPlusDeposits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12e)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSmallBusCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAfterCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiiC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiiC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonRefCreditSFLeaveWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonrefEmplRetentionCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefCreditForLeaveWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotAdvancesForm7200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotDepDefRefCreditsLessAdv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotNonRefundableCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefEmplRententionCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotDepDefRefCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedWagesForEmployeeRetentionCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualfiedExpAllocToSickLeaveWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedExpAllocToFamilyLeaveWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedExpAllocToLine21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditFromForm5884C)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.image3,
            this.Image1,
            this.Image2,
            this.txtName,
            this.txtAddress,
            this.txtCity,
            this.txtLine2,
            this.txtLine3,
            this.txtSSandMedicareAdjustment,
            this.txtLine6,
            this.txtLine5aC2,
            this.txtLine5bC2,
            this.txtLine5cC2,
            this.txtLine5d,
            this.txtFractionsOfCents,
            this.txtTotTaxAfterAdjustment,
            this.txtTotalDeposits,
            this.txtBalanceDue,
            this.txtLine5aC1,
            this.txtLine5bC1,
            this.txtLine5cC1,
            this.txtOverpayment,
            this.txtOverPaymentReturn,
            this.txtOverPaymentRefund,
            this.txtLine1,
            this.txtDate,
            this.txtCheckSemiWeekly,
            this.txtCheckMonthly,
            this.txtLine17a,
            this.txtLine17b,
            this.txtLine17c,
            this.txtLine17d,
            this.txtState,
            this.txtZip,
            this.txtEINBox1,
            this.txtEINBox2,
            this.txtEINBox3,
            this.txtEINBox4,
            this.txtEINBox5,
            this.txtEINBox6,
            this.txtEINBox7,
            this.txtEINBox8,
            this.txtEINBox9,
            this.txtQuarter1,
            this.txtQuarter2,
            this.txtQuarter3,
            this.txtQuarter4,
            this.txtStreetNum,
            this.PageBreak1,
            this.Field1,
            this.Field2,
            this.txtTransmitterPhone,
            this.txtSickPayAdjustment,
            this.Field4,
            this.Field5,
            this.txtTipsAdjustment,
            this.txtIncomeTaxAdjustment,
            this.txtTotalAdjustments,
            this.txtTransmitterName,
            this.txtTransmitterTitle,
            this.txtCheckLess2500,
            this.txtCobraPayments,
            this.txtCobraIndividuals,
            this.txtCobraPlusDeposits,
            this.txt12c,
            this.txt12d,
            this.txt12e,
            this.Field6,
            this.txt6b,
            this.txt6c,
            this.txt6d,
            this.Field7,
            this.txtSmallBusCredit,
            this.txtTotalAfterCredits,
            this.txtName2,
            this.txtEmployerID,
            this.txtName3,
            this.txtEmployerID2,
            this.txtLine5aiC2,
            this.txtLine5aiC1,
            this.txtLine5aiiC2,
            this.txtLine5aiiC1,
            this.txtNonRefCreditSFLeaveWages,
            this.txtNonrefEmplRetentionCredit,
            this.txtRefCreditForLeaveWages,
            this.txtTotAdvancesForm7200,
            this.txtTotDepDefRefCreditsLessAdv,
            this.txtTotNonRefundableCredits,
            this.txtRefEmplRententionCredit,
            this.txtTotDepDefRefCredits,
            this.txtQualifiedWagesForEmployeeRetentionCredit,
            this.txtQualfiedExpAllocToSickLeaveWages,
            this.txtQualifiedExpAllocToFamilyLeaveWages,
            this.txtQualifiedExpAllocToLine21,
            this.txtCreditFromForm5884C});
            this.Detail.Height = 31.51384F;
            this.Detail.Name = "Detail";
            // 
            // image3
            // 
            this.image3.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.image3.DataField = "";
            this.image3.Height = 10.5F;
            this.image3.HyperLink = null;
            this.image3.ImageData = ((System.IO.Stream)(resources.GetObject("image3.ImageData")));
            this.image3.Left = 0F;
            this.image3.LineWeight = 1F;
            this.image3.Name = "image3";
            this.image3.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
            this.image3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.image3.Top = 21F;
            this.image3.Width = 8F;
            // 
            // Image1
            // 
            this.Image1.BackColor = System.Drawing.Color.White;
            this.Image1.Height = 10.5F;
            this.Image1.HyperLink = null;
            this.Image1.ImageData = ((System.IO.Stream)(resources.GetObject("Image1.ImageData")));
            this.Image1.Left = 0F;
            this.Image1.LineWeight = 1F;
            this.Image1.Name = "Image1";
            this.Image1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
            this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image1.Top = 0F;
            this.Image1.Width = 8F;
            // 
            // Image2
            // 
            this.Image2.BackColor = System.Drawing.Color.White;
            this.Image2.Height = 10.5F;
            this.Image2.HyperLink = null;
            this.Image2.ImageData = ((System.IO.Stream)(resources.GetObject("Image2.ImageData")));
            this.Image2.Left = 0F;
            this.Image2.LineWeight = 1F;
            this.Image2.Name = "Image2";
            this.Image2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
            this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image2.Top = 10.5F;
            this.Image2.Width = 8F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.25F;
            this.txtName.Left = 1.771F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; white-space: nowrap";
            this.txtName.Text = null;
            this.txtName.Top = 0.987F;
            this.txtName.Width = 3.302001F;
            // 
            // txtAddress
            // 
            this.txtAddress.CanGrow = false;
            this.txtAddress.Height = 0.25F;
            this.txtAddress.Left = 1.052F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 1.66375F;
            this.txtAddress.Width = 4.114667F;
            // 
            // txtCity
            // 
            this.txtCity.CanGrow = false;
            this.txtCity.Height = 0.25F;
            this.txtCity.Left = 1.052F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtCity.Text = null;
            this.txtCity.Top = 2.09075F;
            this.txtCity.Width = 2.395F;
            // 
            // txtLine2
            // 
            this.txtLine2.CanGrow = false;
            this.txtLine2.Height = 0.2083333F;
            this.txtLine2.Left = 6.012F;
            this.txtLine2.Name = "txtLine2";
            this.txtLine2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine2.Text = null;
            this.txtLine2.Top = 3.813361F;
            this.txtLine2.Width = 1.666667F;
            // 
            // txtLine3
            // 
            this.txtLine3.CanGrow = false;
            this.txtLine3.Height = 0.1979167F;
            this.txtLine3.Left = 6.012F;
            this.txtLine3.Name = "txtLine3";
            this.txtLine3.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine3.Text = null;
            this.txtLine3.Top = 4.139751F;
            this.txtLine3.Width = 1.666667F;
            // 
            // txtSSandMedicareAdjustment
            // 
            this.txtSSandMedicareAdjustment.CanGrow = false;
            this.txtSSandMedicareAdjustment.Height = 0.25F;
            this.txtSSandMedicareAdjustment.Left = 0.3020833F;
            this.txtSSandMedicareAdjustment.Name = "txtSSandMedicareAdjustment";
            this.txtSSandMedicareAdjustment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtSSandMedicareAdjustment.Text = "0.00";
            this.txtSSandMedicareAdjustment.Top = 6.299501F;
            this.txtSSandMedicareAdjustment.Visible = false;
            this.txtSSandMedicareAdjustment.Width = 1.25F;
            // 
            // txtLine6
            // 
            this.txtLine6.CanGrow = false;
            this.txtLine6.Height = 0.2083333F;
            this.txtLine6.Left = 6.002F;
            this.txtLine6.Name = "txtLine6";
            this.txtLine6.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine6.Text = null;
            this.txtLine6.Top = 7.233751F;
            this.txtLine6.Width = 1.6875F;
            // 
            // txtLine5aC2
            // 
            this.txtLine5aC2.CanGrow = false;
            this.txtLine5aC2.Height = 0.2083333F;
            this.txtLine5aC2.Left = 4.623F;
            this.txtLine5aC2.Name = "txtLine5aC2";
            this.txtLine5aC2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aC2.Text = null;
            this.txtLine5aC2.Top = 4.892752F;
            this.txtLine5aC2.Width = 1.305F;
            // 
            // txtLine5bC2
            // 
            this.txtLine5bC2.CanGrow = false;
            this.txtLine5bC2.Height = 0.21875F;
            this.txtLine5bC2.Left = 4.623F;
            this.txtLine5bC2.Name = "txtLine5bC2";
            this.txtLine5bC2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5bC2.Text = null;
            this.txtLine5bC2.Top = 5.643334F;
            this.txtLine5bC2.Width = 1.305F;
            // 
            // txtLine5cC2
            // 
            this.txtLine5cC2.CanGrow = false;
            this.txtLine5cC2.Height = 0.2395833F;
            this.txtLine5cC2.Left = 4.623F;
            this.txtLine5cC2.Name = "txtLine5cC2";
            this.txtLine5cC2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5cC2.Text = null;
            this.txtLine5cC2.Top = 5.886391F;
            this.txtLine5cC2.Width = 1.305F;
            // 
            // txtLine5d
            // 
            this.txtLine5d.CanGrow = false;
            this.txtLine5d.Height = 0.2291667F;
            this.txtLine5d.Left = 6.002F;
            this.txtLine5d.Name = "txtLine5d";
            this.txtLine5d.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5d.Text = null;
            this.txtLine5d.Top = 6.549751F;
            this.txtLine5d.Width = 1.6875F;
            // 
            // txtFractionsOfCents
            // 
            this.txtFractionsOfCents.CanGrow = false;
            this.txtFractionsOfCents.Height = 0.21875F;
            this.txtFractionsOfCents.Left = 6.002F;
            this.txtFractionsOfCents.Name = "txtFractionsOfCents";
            this.txtFractionsOfCents.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtFractionsOfCents.Text = null;
            this.txtFractionsOfCents.Top = 7.584752F;
            this.txtFractionsOfCents.Width = 1.6875F;
            // 
            // txtTotTaxAfterAdjustment
            // 
            this.txtTotTaxAfterAdjustment.CanGrow = false;
            this.txtTotTaxAfterAdjustment.Height = 0.2083333F;
            this.txtTotTaxAfterAdjustment.Left = 6.002F;
            this.txtTotTaxAfterAdjustment.Name = "txtTotTaxAfterAdjustment";
            this.txtTotTaxAfterAdjustment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotTaxAfterAdjustment.Text = null;
            this.txtTotTaxAfterAdjustment.Top = 8.572752F;
            this.txtTotTaxAfterAdjustment.Width = 1.6875F;
            // 
            // txtTotalDeposits
            // 
            this.txtTotalDeposits.CanGrow = false;
            this.txtTotalDeposits.Height = 0.1979167F;
            this.txtTotalDeposits.Left = 6.002F;
            this.txtTotalDeposits.Name = "txtTotalDeposits";
            this.txtTotalDeposits.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotalDeposits.Text = null;
            this.txtTotalDeposits.Top = 12.237F;
            this.txtTotalDeposits.Width = 1.6875F;
            // 
            // txtBalanceDue
            // 
            this.txtBalanceDue.CanGrow = false;
            this.txtBalanceDue.Height = 0.2083333F;
            this.txtBalanceDue.Left = 6.002F;
            this.txtBalanceDue.Name = "txtBalanceDue";
            this.txtBalanceDue.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtBalanceDue.Text = null;
            this.txtBalanceDue.Top = 14.55967F;
            this.txtBalanceDue.Width = 1.6875F;
            // 
            // txtLine5aC1
            // 
            this.txtLine5aC1.CanGrow = false;
            this.txtLine5aC1.Height = 0.2083333F;
            this.txtLine5aC1.Left = 2.698F;
            this.txtLine5aC1.Name = "txtLine5aC1";
            this.txtLine5aC1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aC1.Text = null;
            this.txtLine5aC1.Top = 4.893168F;
            this.txtLine5aC1.Width = 1.333333F;
            // 
            // txtLine5bC1
            // 
            this.txtLine5bC1.CanGrow = false;
            this.txtLine5bC1.Height = 0.21875F;
            this.txtLine5bC1.Left = 2.698F;
            this.txtLine5bC1.Name = "txtLine5bC1";
            this.txtLine5bC1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5bC1.Text = null;
            this.txtLine5bC1.Top = 5.643752F;
            this.txtLine5bC1.Width = 1.333333F;
            // 
            // txtLine5cC1
            // 
            this.txtLine5cC1.CanGrow = false;
            this.txtLine5cC1.Height = 0.2395833F;
            this.txtLine5cC1.Left = 2.698F;
            this.txtLine5cC1.Name = "txtLine5cC1";
            this.txtLine5cC1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5cC1.Text = null;
            this.txtLine5cC1.Top = 5.882751F;
            this.txtLine5cC1.Width = 1.333333F;
            // 
            // txtOverpayment
            // 
            this.txtOverpayment.CanGrow = false;
            this.txtOverpayment.Height = 0.1979167F;
            this.txtOverpayment.Left = 3.960001F;
            this.txtOverpayment.Name = "txtOverpayment";
            this.txtOverpayment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtOverpayment.Text = null;
            this.txtOverpayment.Top = 14.896F;
            this.txtOverpayment.Width = 1.25F;
            // 
            // txtOverPaymentReturn
            // 
            this.txtOverPaymentReturn.Height = 0.1666667F;
            this.txtOverPaymentReturn.Left = 5.941F;
            this.txtOverPaymentReturn.Name = "txtOverPaymentReturn";
            this.txtOverPaymentReturn.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center; vertical-" +
    "align: top";
            this.txtOverPaymentReturn.Text = null;
            this.txtOverPaymentReturn.Top = 14.90394F;
            this.txtOverPaymentReturn.Width = 0.1666667F;
            // 
            // txtOverPaymentRefund
            // 
            this.txtOverPaymentRefund.Height = 0.1666667F;
            this.txtOverPaymentRefund.Left = 6.927001F;
            this.txtOverPaymentRefund.Name = "txtOverPaymentRefund";
            this.txtOverPaymentRefund.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center";
            this.txtOverPaymentRefund.Text = null;
            this.txtOverPaymentRefund.Top = 14.90394F;
            this.txtOverPaymentRefund.Width = 0.1666667F;
            // 
            // txtLine1
            // 
            this.txtLine1.CanGrow = false;
            this.txtLine1.Height = 0.2395833F;
            this.txtLine1.Left = 6.002F;
            this.txtLine1.Name = "txtLine1";
            this.txtLine1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine1.Text = null;
            this.txtLine1.Top = 3.479361F;
            this.txtLine1.Width = 1.666667F;
            // 
            // txtDate
            // 
            this.txtDate.CanGrow = false;
            this.txtDate.Height = 0.2291667F;
            this.txtDate.Left = 1.866251F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: left";
            this.txtDate.Text = null;
            this.txtDate.Top = 28.05266F;
            this.txtDate.Width = 1.03125F;
            // 
            // txtCheckSemiWeekly
            // 
            this.txtCheckSemiWeekly.Height = 0.1666667F;
            this.txtCheckSemiWeekly.Left = 1.25F;
            this.txtCheckSemiWeekly.Name = "txtCheckSemiWeekly";
            this.txtCheckSemiWeekly.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtCheckSemiWeekly.Text = null;
            this.txtCheckSemiWeekly.Top = 17.952F;
            this.txtCheckSemiWeekly.Width = 0.1666667F;
            // 
            // txtCheckMonthly
            // 
            this.txtCheckMonthly.Height = 0.1666667F;
            this.txtCheckMonthly.Left = 1.25F;
            this.txtCheckMonthly.Name = "txtCheckMonthly";
            this.txtCheckMonthly.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtCheckMonthly.Text = null;
            this.txtCheckMonthly.Top = 16.372F;
            this.txtCheckMonthly.Width = 0.1666667F;
            // 
            // txtLine17a
            // 
            this.txtLine17a.CanGrow = false;
            this.txtLine17a.Height = 0.2291667F;
            this.txtLine17a.Left = 3F;
            this.txtLine17a.Name = "txtLine17a";
            this.txtLine17a.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine17a.Text = null;
            this.txtLine17a.Top = 16.80931F;
            this.txtLine17a.Width = 1.666667F;
            // 
            // txtLine17b
            // 
            this.txtLine17b.CanGrow = false;
            this.txtLine17b.Height = 0.2291667F;
            this.txtLine17b.Left = 3F;
            this.txtLine17b.Name = "txtLine17b";
            this.txtLine17b.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine17b.Text = null;
            this.txtLine17b.Top = 17.11139F;
            this.txtLine17b.Width = 1.666667F;
            // 
            // txtLine17c
            // 
            this.txtLine17c.CanGrow = false;
            this.txtLine17c.Height = 0.2291667F;
            this.txtLine17c.Left = 3F;
            this.txtLine17c.Name = "txtLine17c";
            this.txtLine17c.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine17c.Text = null;
            this.txtLine17c.Top = 17.41F;
            this.txtLine17c.Width = 1.666667F;
            // 
            // txtLine17d
            // 
            this.txtLine17d.CanGrow = false;
            this.txtLine17d.Height = 0.2291667F;
            this.txtLine17d.Left = 3F;
            this.txtLine17d.Name = "txtLine17d";
            this.txtLine17d.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine17d.Text = null;
            this.txtLine17d.Top = 17.71556F;
            this.txtLine17d.Width = 1.666667F;
            // 
            // txtState
            // 
            this.txtState.CanGrow = false;
            this.txtState.Height = 0.25F;
            this.txtState.Left = 3.603F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtState.Text = null;
            this.txtState.Top = 2.091167F;
            this.txtState.Width = 0.4594998F;
            // 
            // txtZip
            // 
            this.txtZip.CanGrow = false;
            this.txtZip.Height = 0.25F;
            this.txtZip.Left = 4.197917F;
            this.txtZip.Name = "txtZip";
            this.txtZip.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtZip.Text = null;
            this.txtZip.Top = 2.091167F;
            this.txtZip.Width = 0.9166667F;
            // 
            // txtEINBox1
            // 
            this.txtEINBox1.CanGrow = false;
            this.txtEINBox1.Height = 0.1666667F;
            this.txtEINBox1.Left = 1.957F;
            this.txtEINBox1.Name = "txtEINBox1";
            this.txtEINBox1.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox1.Text = null;
            this.txtEINBox1.Top = 0.6957501F;
            this.txtEINBox1.Width = 0.1666667F;
            // 
            // txtEINBox2
            // 
            this.txtEINBox2.CanGrow = false;
            this.txtEINBox2.Height = 0.1666667F;
            this.txtEINBox2.Left = 2.282F;
            this.txtEINBox2.Name = "txtEINBox2";
            this.txtEINBox2.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox2.Text = null;
            this.txtEINBox2.Top = 0.6957501F;
            this.txtEINBox2.Width = 0.1666667F;
            // 
            // txtEINBox3
            // 
            this.txtEINBox3.CanGrow = false;
            this.txtEINBox3.Height = 0.1666667F;
            this.txtEINBox3.Left = 2.833F;
            this.txtEINBox3.Name = "txtEINBox3";
            this.txtEINBox3.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox3.Text = null;
            this.txtEINBox3.Top = 0.6957501F;
            this.txtEINBox3.Width = 0.1666667F;
            // 
            // txtEINBox4
            // 
            this.txtEINBox4.CanGrow = false;
            this.txtEINBox4.Height = 0.1666667F;
            this.txtEINBox4.Left = 3.177F;
            this.txtEINBox4.Name = "txtEINBox4";
            this.txtEINBox4.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox4.Text = null;
            this.txtEINBox4.Top = 0.6957501F;
            this.txtEINBox4.Width = 0.1666667F;
            // 
            // txtEINBox5
            // 
            this.txtEINBox5.CanGrow = false;
            this.txtEINBox5.Height = 0.1666667F;
            this.txtEINBox5.Left = 3.531F;
            this.txtEINBox5.Name = "txtEINBox5";
            this.txtEINBox5.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox5.Text = null;
            this.txtEINBox5.Top = 0.6957501F;
            this.txtEINBox5.Width = 0.1666667F;
            // 
            // txtEINBox6
            // 
            this.txtEINBox6.CanGrow = false;
            this.txtEINBox6.Height = 0.1666667F;
            this.txtEINBox6.Left = 3.875F;
            this.txtEINBox6.Name = "txtEINBox6";
            this.txtEINBox6.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox6.Text = null;
            this.txtEINBox6.Top = 0.6957501F;
            this.txtEINBox6.Width = 0.1666667F;
            // 
            // txtEINBox7
            // 
            this.txtEINBox7.CanGrow = false;
            this.txtEINBox7.Height = 0.1666667F;
            this.txtEINBox7.Left = 4.209F;
            this.txtEINBox7.Name = "txtEINBox7";
            this.txtEINBox7.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox7.Text = null;
            this.txtEINBox7.Top = 0.6957501F;
            this.txtEINBox7.Width = 0.1666667F;
            // 
            // txtEINBox8
            // 
            this.txtEINBox8.CanGrow = false;
            this.txtEINBox8.Height = 0.1666667F;
            this.txtEINBox8.Left = 4.563F;
            this.txtEINBox8.Name = "txtEINBox8";
            this.txtEINBox8.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox8.Text = null;
            this.txtEINBox8.Top = 0.6957501F;
            this.txtEINBox8.Width = 0.1666667F;
            // 
            // txtEINBox9
            // 
            this.txtEINBox9.CanGrow = false;
            this.txtEINBox9.Height = 0.1666667F;
            this.txtEINBox9.Left = 4.917F;
            this.txtEINBox9.Name = "txtEINBox9";
            this.txtEINBox9.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.txtEINBox9.Text = null;
            this.txtEINBox9.Top = 0.6957501F;
            this.txtEINBox9.Width = 0.1666667F;
            // 
            // txtQuarter1
            // 
            this.txtQuarter1.Height = 0.1666667F;
            this.txtQuarter1.Left = 5.642F;
            this.txtQuarter1.Name = "txtQuarter1";
            this.txtQuarter1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center";
            this.txtQuarter1.Text = null;
            this.txtQuarter1.Top = 1.01175F;
            this.txtQuarter1.Width = 0.1666667F;
            // 
            // txtQuarter2
            // 
            this.txtQuarter2.Height = 0.1666667F;
            this.txtQuarter2.Left = 5.642F;
            this.txtQuarter2.Name = "txtQuarter2";
            this.txtQuarter2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center";
            this.txtQuarter2.Text = null;
            this.txtQuarter2.Top = 1.247861F;
            this.txtQuarter2.Width = 0.1666667F;
            // 
            // txtQuarter3
            // 
            this.txtQuarter3.Height = 0.1666667F;
            this.txtQuarter3.Left = 5.642F;
            this.txtQuarter3.Name = "txtQuarter3";
            this.txtQuarter3.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center";
            this.txtQuarter3.Text = null;
            this.txtQuarter3.Top = 1.4805F;
            this.txtQuarter3.Width = 0.1666667F;
            // 
            // txtQuarter4
            // 
            this.txtQuarter4.Height = 0.1666667F;
            this.txtQuarter4.Left = 5.642F;
            this.txtQuarter4.Name = "txtQuarter4";
            this.txtQuarter4.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: center";
            this.txtQuarter4.Text = null;
            this.txtQuarter4.Top = 1.709667F;
            this.txtQuarter4.Width = 0.1666667F;
            // 
            // txtStreetNum
            // 
            this.txtStreetNum.CanGrow = false;
            this.txtStreetNum.Height = 0.25F;
            this.txtStreetNum.Left = 1.052F;
            this.txtStreetNum.Name = "txtStreetNum";
            this.txtStreetNum.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtStreetNum.Text = null;
            this.txtStreetNum.Top = 1.66375F;
            this.txtStreetNum.Width = 0.974F;
            // 
            // PageBreak1
            // 
            this.PageBreak1.Height = 0.05555556F;
            this.PageBreak1.Left = 0F;
            this.PageBreak1.Name = "PageBreak1";
            this.PageBreak1.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
            this.PageBreak1.Top = 10.5F;
            this.PageBreak1.Width = 6.5F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.2395833F;
            this.Field1.Left = 0.6145841F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.Field1.Text = "M";
            this.Field1.Top = 15.95514F;
            this.Field1.Visible = false;
            this.Field1.Width = 0.2395833F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.2395833F;
            this.Field2.Left = 0.9270838F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.Field2.Text = "E";
            this.Field2.Top = 15.95514F;
            this.Field2.Visible = false;
            this.Field2.Width = 0.2395833F;
            // 
            // txtTransmitterPhone
            // 
            this.txtTransmitterPhone.CanGrow = false;
            this.txtTransmitterPhone.Height = 0.2291667F;
            this.txtTransmitterPhone.Left = 6.166251F;
            this.txtTransmitterPhone.Name = "txtTransmitterPhone";
            this.txtTransmitterPhone.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: left";
            this.txtTransmitterPhone.Text = null;
            this.txtTransmitterPhone.Top = 28.05219F;
            this.txtTransmitterPhone.Width = 1.482167F;
            // 
            // txtSickPayAdjustment
            // 
            this.txtSickPayAdjustment.CanGrow = false;
            this.txtSickPayAdjustment.Height = 0.1979167F;
            this.txtSickPayAdjustment.Left = 6.002F;
            this.txtSickPayAdjustment.Name = "txtSickPayAdjustment";
            this.txtSickPayAdjustment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtSickPayAdjustment.Text = "0.00";
            this.txtSickPayAdjustment.Top = 7.903751F;
            this.txtSickPayAdjustment.Width = 1.6875F;
            // 
            // Field4
            // 
            this.Field4.CanGrow = false;
            this.Field4.Height = 0.25F;
            this.Field4.Left = 0.3020833F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.Field4.Text = "0.00";
            this.Field4.Top = 6.612001F;
            this.Field4.Visible = false;
            this.Field4.Width = 1.25F;
            // 
            // Field5
            // 
            this.Field5.CanGrow = false;
            this.Field5.Height = 0.25F;
            this.Field5.Left = 0.3020833F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.Field5.Text = "0.00";
            this.Field5.Top = 6.914084F;
            this.Field5.Visible = false;
            this.Field5.Width = 1.25F;
            // 
            // txtTipsAdjustment
            // 
            this.txtTipsAdjustment.CanGrow = false;
            this.txtTipsAdjustment.Height = 0.1875F;
            this.txtTipsAdjustment.Left = 6.002F;
            this.txtTipsAdjustment.Name = "txtTipsAdjustment";
            this.txtTipsAdjustment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTipsAdjustment.Text = "0.00";
            this.txtTipsAdjustment.Top = 8.261752F;
            this.txtTipsAdjustment.Width = 1.6875F;
            // 
            // txtIncomeTaxAdjustment
            // 
            this.txtIncomeTaxAdjustment.CanGrow = false;
            this.txtIncomeTaxAdjustment.Height = 0.25F;
            this.txtIncomeTaxAdjustment.Left = 0.3020833F;
            this.txtIncomeTaxAdjustment.Name = "txtIncomeTaxAdjustment";
            this.txtIncomeTaxAdjustment.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtIncomeTaxAdjustment.Text = "0.00";
            this.txtIncomeTaxAdjustment.Top = 5.997418F;
            this.txtIncomeTaxAdjustment.Visible = false;
            this.txtIncomeTaxAdjustment.Width = 1.25F;
            // 
            // txtTotalAdjustments
            // 
            this.txtTotalAdjustments.CanGrow = false;
            this.txtTotalAdjustments.Height = 0.2083333F;
            this.txtTotalAdjustments.Left = 0.06944445F;
            this.txtTotalAdjustments.Name = "txtTotalAdjustments";
            this.txtTotalAdjustments.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotalAdjustments.Text = null;
            this.txtTotalAdjustments.Top = 6.716168F;
            this.txtTotalAdjustments.Visible = false;
            this.txtTotalAdjustments.Width = 1.75F;
            // 
            // txtTransmitterName
            // 
            this.txtTransmitterName.CanGrow = false;
            this.txtTransmitterName.Height = 0.2291667F;
            this.txtTransmitterName.Left = 5.760001F;
            this.txtTransmitterName.Name = "txtTransmitterName";
            this.txtTransmitterName.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: left";
            this.txtTransmitterName.Text = null;
            this.txtTransmitterName.Top = 27.30566F;
            this.txtTransmitterName.Width = 1.878F;
            // 
            // txtTransmitterTitle
            // 
            this.txtTransmitterTitle.CanGrow = false;
            this.txtTransmitterTitle.Height = 0.2291667F;
            this.txtTransmitterTitle.Left = 5.760001F;
            this.txtTransmitterTitle.Name = "txtTransmitterTitle";
            this.txtTransmitterTitle.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: left";
            this.txtTransmitterTitle.Text = null;
            this.txtTransmitterTitle.Top = 27.639F;
            this.txtTransmitterTitle.Width = 1.878F;
            // 
            // txtCheckLess2500
            // 
            this.txtCheckLess2500.Height = 0.1666667F;
            this.txtCheckLess2500.Left = 1.25F;
            this.txtCheckLess2500.Name = "txtCheckLess2500";
            this.txtCheckLess2500.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtCheckLess2500.Text = null;
            this.txtCheckLess2500.Top = 15.706F;
            this.txtCheckLess2500.Width = 0.1666667F;
            // 
            // txtCobraPayments
            // 
            this.txtCobraPayments.CanGrow = false;
            this.txtCobraPayments.Height = 0.1979167F;
            this.txtCobraPayments.Left = 4.03125F;
            this.txtCobraPayments.Name = "txtCobraPayments";
            this.txtCobraPayments.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtCobraPayments.Text = "0.00";
            this.txtCobraPayments.Top = 8.552974F;
            this.txtCobraPayments.Visible = false;
            this.txtCobraPayments.Width = 1.625F;
            // 
            // txtCobraIndividuals
            // 
            this.txtCobraIndividuals.CanGrow = false;
            this.txtCobraIndividuals.Height = 0.1979167F;
            this.txtCobraIndividuals.Left = 4.645833F;
            this.txtCobraIndividuals.Name = "txtCobraIndividuals";
            this.txtCobraIndividuals.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtCobraIndividuals.Text = "0";
            this.txtCobraIndividuals.Top = 8.893251F;
            this.txtCobraIndividuals.Visible = false;
            this.txtCobraIndividuals.Width = 1.0625F;
            // 
            // txtCobraPlusDeposits
            // 
            this.txtCobraPlusDeposits.CanGrow = false;
            this.txtCobraPlusDeposits.Height = 0.2083333F;
            this.txtCobraPlusDeposits.Left = 4.072501F;
            this.txtCobraPlusDeposits.Name = "txtCobraPlusDeposits";
            this.txtCobraPlusDeposits.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtCobraPlusDeposits.Text = null;
            this.txtCobraPlusDeposits.Top = 12.31297F;
            this.txtCobraPlusDeposits.Visible = false;
            this.txtCobraPlusDeposits.Width = 1.614583F;
            // 
            // txt12c
            // 
            this.txt12c.CanGrow = false;
            this.txt12c.Height = 0.1979167F;
            this.txt12c.Left = 1.770833F;
            this.txt12c.Name = "txt12c";
            this.txt12c.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt12c.Tag = "0";
            this.txt12c.Text = "0";
            this.txt12c.Top = 8.643251F;
            this.txt12c.Visible = false;
            this.txt12c.Width = 1.0625F;
            // 
            // txt12d
            // 
            this.txt12d.CanGrow = false;
            this.txt12d.Height = 0.1979167F;
            this.txt12d.Left = 1.010417F;
            this.txt12d.Name = "txt12d";
            this.txt12d.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt12d.Text = "0";
            this.txt12d.Top = 8.914084F;
            this.txt12d.Visible = false;
            this.txt12d.Width = 1.197917F;
            // 
            // txt12e
            // 
            this.txt12e.CanGrow = false;
            this.txt12e.Height = 0.1979167F;
            this.txt12e.Left = 2.65625F;
            this.txt12e.Name = "txt12e";
            this.txt12e.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt12e.Text = "0";
            this.txt12e.Top = 8.730057F;
            this.txt12e.Visible = false;
            this.txt12e.Width = 1.6875F;
            // 
            // Field6
            // 
            this.Field6.CanGrow = false;
            this.Field6.Height = 0.1979167F;
            this.Field6.Left = 4.697917F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.Field6.Text = "0";
            this.Field6.Top = 6.143251F;
            this.Field6.Visible = false;
            this.Field6.Width = 1.0625F;
            // 
            // txt6b
            // 
            this.txt6b.CanGrow = false;
            this.txt6b.Height = 0.1979167F;
            this.txt6b.Left = 4.697917F;
            this.txt6b.Name = "txt6b";
            this.txt6b.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt6b.Text = "0";
            this.txt6b.Top = 6.417557F;
            this.txt6b.Visible = false;
            this.txt6b.Width = 1.0625F;
            // 
            // txt6c
            // 
            this.txt6c.CanGrow = false;
            this.txt6c.Height = 0.1979167F;
            this.txt6c.Left = 4.010417F;
            this.txt6c.Name = "txt6c";
            this.txt6c.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt6c.Text = "0";
            this.txt6c.Top = 6.243947F;
            this.txt6c.Visible = false;
            this.txt6c.Width = 1.0625F;
            // 
            // txt6d
            // 
            this.txt6d.CanGrow = false;
            this.txt6d.Height = 0.2083333F;
            this.txt6d.Left = 0F;
            this.txt6d.Name = "txt6d";
            this.txt6d.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txt6d.Text = "0.00";
            this.txt6d.Top = 5.684918F;
            this.txt6d.Visible = false;
            this.txt6d.Width = 1.666667F;
            // 
            // Field7
            // 
            this.Field7.CanGrow = false;
            this.Field7.Height = 0.2291667F;
            this.Field7.Left = 6.002F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.Field7.Text = "0.00";
            this.Field7.Top = 6.913752F;
            this.Field7.Width = 1.6875F;
            // 
            // txtSmallBusCredit
            // 
            this.txtSmallBusCredit.CanGrow = false;
            this.txtSmallBusCredit.Height = 0.1979167F;
            this.txtSmallBusCredit.Left = 6.002F;
            this.txtSmallBusCredit.Name = "txtSmallBusCredit";
            this.txtSmallBusCredit.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtSmallBusCredit.Text = "0.00";
            this.txtSmallBusCredit.Top = 8.892752F;
            this.txtSmallBusCredit.Width = 1.6875F;
            // 
            // txtTotalAfterCredits
            // 
            this.txtTotalAfterCredits.CanGrow = false;
            this.txtTotalAfterCredits.Height = 0.1875F;
            this.txtTotalAfterCredits.Left = 6.002F;
            this.txtTotalAfterCredits.Name = "txtTotalAfterCredits";
            this.txtTotalAfterCredits.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotalAfterCredits.Text = null;
            this.txtTotalAfterCredits.Top = 11.833F;
            this.txtTotalAfterCredits.Width = 1.6875F;
            // 
            // txtName2
            // 
            this.txtName2.CanGrow = false;
            this.txtName2.Height = 0.2F;
            this.txtName2.Left = 0.3230005F;
            this.txtName2.Name = "txtName2";
            this.txtName2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtName2.Text = null;
            this.txtName2.Top = 10.997F;
            this.txtName2.Width = 4.59375F;
            // 
            // txtEmployerID
            // 
            this.txtEmployerID.CanGrow = false;
            this.txtEmployerID.Height = 0.2F;
            this.txtEmployerID.Left = 5.323F;
            this.txtEmployerID.Name = "txtEmployerID";
            this.txtEmployerID.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtEmployerID.Text = null;
            this.txtEmployerID.Top = 10.997F;
            this.txtEmployerID.Width = 2.083333F;
            // 
            // txtName3
            // 
            this.txtName3.CanGrow = false;
            this.txtName3.DataField = "";
            this.txtName3.DistinctField = "";
            this.txtName3.Height = 0.25F;
            this.txtName3.Left = 0.3230005F;
            this.txtName3.Name = "txtName3";
            this.txtName3.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; white-space: nowrap";
            this.txtName3.SummaryGroup = "";
            this.txtName3.Text = null;
            this.txtName3.Top = 21.499F;
            this.txtName3.Width = 4.594F;
            // 
            // txtEmployerID2
            // 
            this.txtEmployerID2.CanGrow = false;
            this.txtEmployerID2.DataField = "";
            this.txtEmployerID2.DistinctField = "";
            this.txtEmployerID2.Height = 0.2F;
            this.txtEmployerID2.Left = 5.323F;
            this.txtEmployerID2.Name = "txtEmployerID2";
            this.txtEmployerID2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt";
            this.txtEmployerID2.SummaryGroup = "";
            this.txtEmployerID2.Text = null;
            this.txtEmployerID2.Top = 21.499F;
            this.txtEmployerID2.Width = 2.083333F;
            // 
            // txtLine5aiC2
            // 
            this.txtLine5aiC2.CanGrow = false;
            this.txtLine5aiC2.DataField = "";
            this.txtLine5aiC2.DistinctField = "";
            this.txtLine5aiC2.Height = 0.2083333F;
            this.txtLine5aiC2.Left = 4.623F;
            this.txtLine5aiC2.Name = "txtLine5aiC2";
            this.txtLine5aiC2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aiC2.SummaryGroup = "";
            this.txtLine5aiC2.Text = null;
            this.txtLine5aiC2.Top = 5.135752F;
            this.txtLine5aiC2.Width = 1.305F;
            // 
            // txtLine5aiC1
            // 
            this.txtLine5aiC1.CanGrow = false;
            this.txtLine5aiC1.DataField = "";
            this.txtLine5aiC1.DistinctField = "";
            this.txtLine5aiC1.Height = 0.2083333F;
            this.txtLine5aiC1.Left = 2.698F;
            this.txtLine5aiC1.Name = "txtLine5aiC1";
            this.txtLine5aiC1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aiC1.SummaryGroup = "";
            this.txtLine5aiC1.Text = null;
            this.txtLine5aiC1.Top = 5.135752F;
            this.txtLine5aiC1.Width = 1.333333F;
            // 
            // txtLine5aiiC2
            // 
            this.txtLine5aiiC2.CanGrow = false;
            this.txtLine5aiiC2.DataField = "";
            this.txtLine5aiiC2.DistinctField = "";
            this.txtLine5aiiC2.Height = 0.2083333F;
            this.txtLine5aiiC2.Left = 4.623F;
            this.txtLine5aiiC2.Name = "txtLine5aiiC2";
            this.txtLine5aiiC2.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aiiC2.SummaryGroup = "";
            this.txtLine5aiiC2.Text = null;
            this.txtLine5aiiC2.Top = 5.380752F;
            this.txtLine5aiiC2.Width = 1.305F;
            // 
            // txtLine5aiiC1
            // 
            this.txtLine5aiiC1.CanGrow = false;
            this.txtLine5aiiC1.DataField = "";
            this.txtLine5aiiC1.DistinctField = "";
            this.txtLine5aiiC1.Height = 0.2083333F;
            this.txtLine5aiiC1.Left = 2.698F;
            this.txtLine5aiiC1.Name = "txtLine5aiiC1";
            this.txtLine5aiiC1.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtLine5aiiC1.SummaryGroup = "";
            this.txtLine5aiiC1.Text = null;
            this.txtLine5aiiC1.Top = 5.380752F;
            this.txtLine5aiiC1.Width = 1.333333F;
            // 
            // txtNonRefCreditSFLeaveWages
            // 
            this.txtNonRefCreditSFLeaveWages.CanGrow = false;
            this.txtNonRefCreditSFLeaveWages.DataField = "";
            this.txtNonRefCreditSFLeaveWages.DistinctField = "";
            this.txtNonRefCreditSFLeaveWages.Height = 0.2083333F;
            this.txtNonRefCreditSFLeaveWages.Left = 6.002F;
            this.txtNonRefCreditSFLeaveWages.Name = "txtNonRefCreditSFLeaveWages";
            this.txtNonRefCreditSFLeaveWages.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtNonRefCreditSFLeaveWages.SummaryGroup = "";
            this.txtNonRefCreditSFLeaveWages.Text = null;
            this.txtNonRefCreditSFLeaveWages.Top = 9.224751F;
            this.txtNonRefCreditSFLeaveWages.Width = 1.6875F;
            // 
            // txtNonrefEmplRetentionCredit
            // 
            this.txtNonrefEmplRetentionCredit.CanGrow = false;
            this.txtNonrefEmplRetentionCredit.DataField = "";
            this.txtNonrefEmplRetentionCredit.DistinctField = "";
            this.txtNonrefEmplRetentionCredit.Height = 0.21875F;
            this.txtNonrefEmplRetentionCredit.Left = 6.002F;
            this.txtNonrefEmplRetentionCredit.Name = "txtNonrefEmplRetentionCredit";
            this.txtNonrefEmplRetentionCredit.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtNonrefEmplRetentionCredit.SummaryGroup = "";
            this.txtNonrefEmplRetentionCredit.Text = null;
            this.txtNonrefEmplRetentionCredit.Top = 9.566751F;
            this.txtNonrefEmplRetentionCredit.Width = 1.6875F;
            // 
            // txtRefCreditForLeaveWages
            // 
            this.txtRefCreditForLeaveWages.CanGrow = false;
            this.txtRefCreditForLeaveWages.DataField = "";
            this.txtRefCreditForLeaveWages.DistinctField = "";
            this.txtRefCreditForLeaveWages.Height = 0.2083333F;
            this.txtRefCreditForLeaveWages.Left = 6.002F;
            this.txtRefCreditForLeaveWages.Name = "txtRefCreditForLeaveWages";
            this.txtRefCreditForLeaveWages.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtRefCreditForLeaveWages.SummaryGroup = "";
            this.txtRefCreditForLeaveWages.Text = null;
            this.txtRefCreditForLeaveWages.Top = 12.893F;
            this.txtRefCreditForLeaveWages.Width = 1.6875F;
            // 
            // txtTotAdvancesForm7200
            // 
            this.txtTotAdvancesForm7200.CanGrow = false;
            this.txtTotAdvancesForm7200.DataField = "";
            this.txtTotAdvancesForm7200.DistinctField = "";
            this.txtTotAdvancesForm7200.Height = 0.1979167F;
            this.txtTotAdvancesForm7200.Left = 6.002F;
            this.txtTotAdvancesForm7200.Name = "txtTotAdvancesForm7200";
            this.txtTotAdvancesForm7200.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotAdvancesForm7200.SummaryGroup = "";
            this.txtTotAdvancesForm7200.Text = null;
            this.txtTotAdvancesForm7200.Top = 13.893F;
            this.txtTotAdvancesForm7200.Width = 1.6875F;
            // 
            // txtTotDepDefRefCreditsLessAdv
            // 
            this.txtTotDepDefRefCreditsLessAdv.CanGrow = false;
            this.txtTotDepDefRefCreditsLessAdv.DataField = "";
            this.txtTotDepDefRefCreditsLessAdv.DistinctField = "";
            this.txtTotDepDefRefCreditsLessAdv.Height = 0.2083333F;
            this.txtTotDepDefRefCreditsLessAdv.Left = 6.002F;
            this.txtTotDepDefRefCreditsLessAdv.Name = "txtTotDepDefRefCreditsLessAdv";
            this.txtTotDepDefRefCreditsLessAdv.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotDepDefRefCreditsLessAdv.SummaryGroup = "";
            this.txtTotDepDefRefCreditsLessAdv.Text = null;
            this.txtTotDepDefRefCreditsLessAdv.Top = 14.237F;
            this.txtTotDepDefRefCreditsLessAdv.Width = 1.6875F;
            // 
            // txtTotNonRefundableCredits
            // 
            this.txtTotNonRefundableCredits.CanGrow = false;
            this.txtTotNonRefundableCredits.DataField = "";
            this.txtTotNonRefundableCredits.DistinctField = "";
            this.txtTotNonRefundableCredits.Height = 0.1979167F;
            this.txtTotNonRefundableCredits.Left = 6.002F;
            this.txtTotNonRefundableCredits.Name = "txtTotNonRefundableCredits";
            this.txtTotNonRefundableCredits.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotNonRefundableCredits.SummaryGroup = "";
            this.txtTotNonRefundableCredits.Text = "0.00";
            this.txtTotNonRefundableCredits.Top = 11.479F;
            this.txtTotNonRefundableCredits.Width = 1.6875F;
            // 
            // txtRefEmplRententionCredit
            // 
            this.txtRefEmplRententionCredit.CanGrow = false;
            this.txtRefEmplRententionCredit.DataField = "";
            this.txtRefEmplRententionCredit.DistinctField = "";
            this.txtRefEmplRententionCredit.Height = 0.1979167F;
            this.txtRefEmplRententionCredit.Left = 6.002F;
            this.txtRefEmplRententionCredit.Name = "txtRefEmplRententionCredit";
            this.txtRefEmplRententionCredit.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtRefEmplRententionCredit.SummaryGroup = "";
            this.txtRefEmplRententionCredit.Text = "0.00";
            this.txtRefEmplRententionCredit.Top = 13.24F;
            this.txtRefEmplRententionCredit.Width = 1.6875F;
            // 
            // txtTotDepDefRefCredits
            // 
            this.txtTotDepDefRefCredits.CanGrow = false;
            this.txtTotDepDefRefCredits.DataField = "";
            this.txtTotDepDefRefCredits.DistinctField = "";
            this.txtTotDepDefRefCredits.Height = 0.1875F;
            this.txtTotDepDefRefCredits.Left = 6.002F;
            this.txtTotDepDefRefCredits.Name = "txtTotDepDefRefCredits";
            this.txtTotDepDefRefCredits.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtTotDepDefRefCredits.SummaryGroup = "";
            this.txtTotDepDefRefCredits.Text = null;
            this.txtTotDepDefRefCredits.Top = 13.583F;
            this.txtTotDepDefRefCredits.Width = 1.6875F;
            // 
            // txtQualifiedWagesForEmployeeRetentionCredit
            // 
            this.txtQualifiedWagesForEmployeeRetentionCredit.CanGrow = false;
            this.txtQualifiedWagesForEmployeeRetentionCredit.DataField = "";
            this.txtQualifiedWagesForEmployeeRetentionCredit.DistinctField = "";
            this.txtQualifiedWagesForEmployeeRetentionCredit.Height = 0.2083333F;
            this.txtQualifiedWagesForEmployeeRetentionCredit.Left = 6.002F;
            this.txtQualifiedWagesForEmployeeRetentionCredit.Name = "txtQualifiedWagesForEmployeeRetentionCredit";
            this.txtQualifiedWagesForEmployeeRetentionCredit.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtQualifiedWagesForEmployeeRetentionCredit.SummaryGroup = "";
            this.txtQualifiedWagesForEmployeeRetentionCredit.Text = null;
            this.txtQualifiedWagesForEmployeeRetentionCredit.Top = 23.472F;
            this.txtQualifiedWagesForEmployeeRetentionCredit.Width = 1.6875F;
            // 
            // txtQualfiedExpAllocToSickLeaveWages
            // 
            this.txtQualfiedExpAllocToSickLeaveWages.CanGrow = false;
            this.txtQualfiedExpAllocToSickLeaveWages.DataField = "";
            this.txtQualfiedExpAllocToSickLeaveWages.DistinctField = "";
            this.txtQualfiedExpAllocToSickLeaveWages.Height = 0.1979167F;
            this.txtQualfiedExpAllocToSickLeaveWages.Left = 6.002F;
            this.txtQualfiedExpAllocToSickLeaveWages.Name = "txtQualfiedExpAllocToSickLeaveWages";
            this.txtQualfiedExpAllocToSickLeaveWages.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtQualfiedExpAllocToSickLeaveWages.SummaryGroup = "";
            this.txtQualfiedExpAllocToSickLeaveWages.Text = "0.00";
            this.txtQualfiedExpAllocToSickLeaveWages.Top = 22.815F;
            this.txtQualfiedExpAllocToSickLeaveWages.Width = 1.6875F;
            // 
            // txtQualifiedExpAllocToFamilyLeaveWages
            // 
            this.txtQualifiedExpAllocToFamilyLeaveWages.CanGrow = false;
            this.txtQualifiedExpAllocToFamilyLeaveWages.DataField = "";
            this.txtQualifiedExpAllocToFamilyLeaveWages.DistinctField = "";
            this.txtQualifiedExpAllocToFamilyLeaveWages.Height = 0.1875F;
            this.txtQualifiedExpAllocToFamilyLeaveWages.Left = 6.002F;
            this.txtQualifiedExpAllocToFamilyLeaveWages.Name = "txtQualifiedExpAllocToFamilyLeaveWages";
            this.txtQualifiedExpAllocToFamilyLeaveWages.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtQualifiedExpAllocToFamilyLeaveWages.SummaryGroup = "";
            this.txtQualifiedExpAllocToFamilyLeaveWages.Text = "0.00";
            this.txtQualifiedExpAllocToFamilyLeaveWages.Top = 23.152F;
            this.txtQualifiedExpAllocToFamilyLeaveWages.Width = 1.6875F;
            // 
            // txtQualifiedExpAllocToLine21
            // 
            this.txtQualifiedExpAllocToLine21.CanGrow = false;
            this.txtQualifiedExpAllocToLine21.DataField = "";
            this.txtQualifiedExpAllocToLine21.DistinctField = "";
            this.txtQualifiedExpAllocToLine21.Height = 0.1979167F;
            this.txtQualifiedExpAllocToLine21.Left = 6.002F;
            this.txtQualifiedExpAllocToLine21.Name = "txtQualifiedExpAllocToLine21";
            this.txtQualifiedExpAllocToLine21.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtQualifiedExpAllocToLine21.SummaryGroup = "";
            this.txtQualifiedExpAllocToLine21.Text = "0.00";
            this.txtQualifiedExpAllocToLine21.Top = 23.829F;
            this.txtQualifiedExpAllocToLine21.Width = 1.6875F;
            // 
            // txtCreditFromForm5884C
            // 
            this.txtCreditFromForm5884C.CanGrow = false;
            this.txtCreditFromForm5884C.DataField = "";
            this.txtCreditFromForm5884C.DistinctField = "";
            this.txtCreditFromForm5884C.Height = 0.1875F;
            this.txtCreditFromForm5884C.Left = 6.002F;
            this.txtCreditFromForm5884C.Name = "txtCreditFromForm5884C";
            this.txtCreditFromForm5884C.Style = "font-family: \\000027Tahoma\\000027; font-size: 12pt; text-align: right";
            this.txtCreditFromForm5884C.SummaryGroup = "";
            this.txtCreditFromForm5884C.Text = null;
            this.txtCreditFromForm5884C.Top = 24.152F;
            this.txtCreditFromForm5884C.Width = 1.6875F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport1});
            this.ReportFooter.Height = 0.07291666F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.0625F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0F;
            this.SubReport1.Width = 8F;
            // 
            // rptLaser941
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.image3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSandMedicareAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5bC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5cC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFractionsOfCents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotTaxAfterAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDeposits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalanceDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5bC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5cC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverpayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverPaymentReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverPaymentRefund)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckSemiWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine17d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEINBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickPayAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipsAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncomeTaxAdjustment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAdjustments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransmitterTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckLess2500)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCobraPlusDeposits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12e)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSmallBusCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAfterCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiiC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine5aiiC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonRefCreditSFLeaveWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonrefEmplRetentionCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefCreditForLeaveWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotAdvancesForm7200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotDepDefRefCreditsLessAdv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotNonRefundableCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefEmplRententionCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotDepDefRefCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedWagesForEmployeeRetentionCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualfiedExpAllocToSickLeaveWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedExpAllocToFamilyLeaveWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQualifiedExpAllocToLine21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditFromForm5884C)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSandMedicareAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5bC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5cC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFractionsOfCents;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTaxAfterAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDeposits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalanceDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5bC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5cC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverpayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverPaymentReturn;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverPaymentRefund;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckSemiWeekly;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckMonthly;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEINBox9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarter4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetNum;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransmitterPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSickPayAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTipsAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncomeTaxAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAdjustments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransmitterName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransmitterTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckLess2500;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCobraPayments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCobraIndividuals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCobraPlusDeposits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12e;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt6b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt6c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt6d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSmallBusCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAfterCredits;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
        private GrapeCity.ActiveReports.SectionReportModel.Picture image3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerID2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aiC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aiC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aiiC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5aiiC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonRefCreditSFLeaveWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonrefEmplRetentionCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRefCreditForLeaveWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAdvancesForm7200;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotDepDefRefCreditsLessAdv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNonRefundableCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRefEmplRententionCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotDepDefRefCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualifiedWagesForEmployeeRetentionCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualfiedExpAllocToSickLeaveWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualifiedExpAllocToFamilyLeaveWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQualifiedExpAllocToLine21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCreditFromForm5884C;
	}
}
