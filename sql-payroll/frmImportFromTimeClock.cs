﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.DataBaseLayer;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWPY0000
{
	public partial class frmImportFromTimeClock : BaseForm
	{
		public frmImportFromTimeClock()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmImportFromTimeClock InstancePtr
		{
			get
			{
				return (frmImportFromTimeClock)Sys.GetInstance(typeof(frmImportFromTimeClock));
			}
		}

		protected frmImportFromTimeClock _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strDefaultDir;
				strDefaultDir = Environment.CurrentDirectory;
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.InitDir = strDefaultDir;
				string strReturnFile;
				strReturnFile = "";
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strReturnFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				txtImport.Text = strReturnFile;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (!(fecherFoundation.Information.Err(ex).Number == 32755))
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmdBrowse", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					fecherFoundation.Information.Err(ex).Clear();
				}
			}
		}

		private void TryThis(ref string strFile)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper tDB = new clsDRWrapper();
				string strPath;
				string strTemp;
				FCFileSystem fs = new FCFileSystem();
				strPath = Path.GetDirectoryName(strFile);
				strTemp = Path.GetFileName(strFile);
				//tDB = OpenDatabase(strPath, false, false, "Text;Database="+strPath+";table = "+strTemp);
				tDB.Execute("select * from " + strTemp, "Payroll");
				int x = 0;
				x = x;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		private void frmImportFromTimeClock_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportFromTimeClock_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportFromTimeClock properties;
			//frmImportFromTimeClock.FillStyle	= 0;
			//frmImportFromTimeClock.ScaleWidth	= 9300;
			//frmImportFromTimeClock.ScaleHeight	= 7500;
			//frmImportFromTimeClock.LinkTopic	= "Form2";
			//frmImportFromTimeClock.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			string strTemp;
			string strDefault = "";
			if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "penobscot county")
			{
				strDefault = "Time Card";
			}
			else
			{
				strDefault = "Leave";
			}
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("TimeClockImportFile", "PY", strDefault));
			if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strTemp)) == "leave")
			{
				cmbTimeCard.Text = "Detailed Shift";
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("TimeClockLeaveFile", "PY", ""));
				txtImport.Text = strTemp;
			}
			else
			{
				cmbTimeCard.Text = "Time Card";
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("TimeClockFile", "PY", ""));
				txtImport.Text = strTemp;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsOpenLeaveFile tFil = new clsOpenLeaveFile();
			FCFileSystem fso = new FCFileSystem();
			if (MessageBox.Show("This will overwrite this weeks hours for all employees found in the file" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
			{
				return;
			}
			if (FCFileSystem.FileExists(txtImport.Text))
			{
				if (cmbTimeCard.Text == "Detailed Shift")
				{
					modRegistry.SaveRegistryKey("TimeClockImportFile", "Leave", "PY");
					modRegistry.SaveRegistryKey("TimeClockLeaveFile", txtImport.Text, "PY");
					if (tFil.OpenFile(txtImport.Text))
					{
						if (tFil.ProcessLeaveFile())
						{
							MessageBox.Show("File processed successfully", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Unable to successfully process file", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				else
				{
					modRegistry.SaveRegistryKey("TimeClockImportFile", "Time Card", "PY");
					modRegistry.SaveRegistryKey("TimeClockFile", txtImport.Text, "PY");
					if (tFil.OpenSimpleFile(txtImport.Text))
					{
						if (tFil.ProcessSimpleFile())
						{
							MessageBox.Show("File processed successfully", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Unable to successfully process file", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			else
			{
				MessageBox.Show("File " + txtImport.Text + " does not exist", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}
	}
}
