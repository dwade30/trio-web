//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDirectDeposit.
	/// </summary>
	public partial class srptDirectDeposit : FCSectionReport
	{
		public srptDirectDeposit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptDirectDeposit InstancePtr
		{
			get
			{
				return (srptDirectDeposit)Sys.GetInstance(typeof(srptDirectDeposit));
			}
		}

		protected srptDirectDeposit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBanks?.Dispose();
				rsInfo?.Dispose();
                rsBanks = null;
                rsInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptDirectDeposit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsBanks = new clsDRWrapper();
		bool blnFirstRecord;
		bool blnDontShow;
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;
        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsInfo.EndOfFile();
				blnDontShow = eArgs.EOF;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strEmployee;
			strEmployee = FCConvert.ToString(this.UserData);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewBankAccountNumbers.ToInteger()))
            {
                case "F":
                    bankAccountViewPermission = BankAccountViewType.Full;
                    break;
                case "P":
                    bankAccountViewPermission = BankAccountViewType.Masked;
                    break;
                default:
                    bankAccountViewPermission = BankAccountViewType.None;
                    break;
            }
            rsInfo.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + strEmployee + "'", "twpy0000.vb1");
			if (FCConvert.ToString(rsInfo.Get_Fields("CHECK")) != "Direct Deposit")
			{
				this.Close();
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM tblDirectDeposit WHERE EmployeeNumber = '" + strEmployee + "'");
			if (rsInfo.EndOfFile())
			{
				//this.Visible = false;
				this.Close();
				return;
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldBank As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldAccount As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldType As object	OnWrite(string)
			if (!blnDontShow)
			{
				rsBanks.OpenRecordset("SELECT * FROM tblBanks WHERE ID = " + rsInfo.Get_Fields_Int32("Bank"));
				if (rsBanks.EndOfFile() != true && rsBanks.BeginningOfFile() != true)
				{
					fldBank.Text = rsBanks.Get_Fields_String("Name");
				}
				else
				{
					fldBank.Text = "UNKNOWN";
				}

                var bankAccount = "";
                switch (bankAccountViewPermission)
                {
                    case BankAccountViewType.Full:
                        bankAccount = rsInfo.Get_Fields_String("Account");                        
                        break;
                    case BankAccountViewType.Masked:
                        bankAccount = "******" + rsInfo.Get_Fields_String("Account").Right(4);
                        break;
                    default:
                        bankAccount = bankAccount = "**********";
                        break;
                }

                fldAccount.Text = bankAccount;
				fldAmount.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "Dollars")
				{
					fldType.Text = "Dollars";
				}
				else
				{
					fldType.Text = "Percent";
				}
			}
			else
			{
				fldBank.Text = "";
				fldAccount.Text = "";
				fldAmount.Text = "";
				fldType.Text = "";
			}
		}

		
	}
}
