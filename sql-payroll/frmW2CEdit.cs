//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2CEdit : BaseForm
	{
		public frmW2CEdit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label3 = new System.Collections.Generic.List<FCLabel>();
			this.Label3.AddControlArrayElement(Label3_0, 0);
			this.Label3.AddControlArrayElement(Label3_1, 1);
			this.Label3.AddControlArrayElement(Label3_2, 2);
			this.Label3.AddControlArrayElement(Label3_3, 3);
			this.Label3.AddControlArrayElement(Label3_4, 4);
			this.Label2 = new System.Collections.Generic.List<FCLabel>();
			this.Label2.AddControlArrayElement(Label2_0, 0);
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_1, 0);
			this.Label1.AddControlArrayElement(Label1_2, 1);
			this.Label4 = new System.Collections.Generic.List<FCLabel>();
			this.Label4.AddControlArrayElement(Label4_0, 0);
			this.label10 = new System.Collections.Generic.List<FCLabel>();
			this.label10.AddControlArrayElement(label10_0, 0);
			this.label10.AddControlArrayElement(label10_1, 1);
			this.label10.AddControlArrayElement(label10_3, 3);
			this.label10.AddControlArrayElement(label10_4, 4);
			this.label10.AddControlArrayElement(label10_5, 5);
			this.label10.AddControlArrayElement(label10_6, 6);
			this.label10.AddControlArrayElement(label10_8, 8);
			this.label10.AddControlArrayElement(label10_9, 9);
			this.label10.AddControlArrayElement(label10_10, 10);
			this.label10.AddControlArrayElement(label10_11, 11);
			this.label10.AddControlArrayElement(label10_12, 12);
			this.label10.AddControlArrayElement(label10_13, 13);
			this.label10.AddControlArrayElement(label10_14, 14);
			this.label10.AddControlArrayElement(label10_15, 15);
			this.label10.AddControlArrayElement(label10_16, 16);
			this.label10.AddControlArrayElement(label10_17, 17);
			this.label10.AddControlArrayElement(label10_18, 18);
			this.label10.AddControlArrayElement(label10_21, 21);
			this.label10.AddControlArrayElement(label10_22, 22);
			this.label10.AddControlArrayElement(label10_24, 24);
			this.label10.AddControlArrayElement(label10_25, 25);
			this.label10.AddControlArrayElement(label10_26, 26);
			this.label10.AddControlArrayElement(label10_27, 27);
			this.label10.AddControlArrayElement(label10_29, 29);
			this.label10.AddControlArrayElement(label10_30, 30);
			this.label10.AddControlArrayElement(label10_31, 31);
			this.label10.AddControlArrayElement(label10_32, 32);
			this.label10.AddControlArrayElement(label10_33, 33);
			this.Label7 = new System.Collections.Generic.List<FCLabel>();
			this.Label7.AddControlArrayElement(Label7_0, 0);
			this.Label7.AddControlArrayElement(Label7_1, 1);
			this.Label5 = new System.Collections.Generic.List<FCLabel>();
			this.Label5.AddControlArrayElement(Label5_0, 0);
			this.Label5.AddControlArrayElement(Label5_1, 1);
			this.Label5.AddControlArrayElement(Label5_2, 2);
			this.Label5.AddControlArrayElement(Label5_3, 3);
			this.Label5.AddControlArrayElement(Label5_4, 4);
			this.Label5.AddControlArrayElement(Label5_7, 7);
			this.Label5.AddControlArrayElement(Label5_8, 8);
			this.Label9 = new System.Collections.Generic.List<FCLabel>();
			this.Label9.AddControlArrayElement(Label9_0, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2CEdit InstancePtr
		{
			get
			{
				return (frmW2CEdit)Sys.GetInstance(typeof(frmW2CEdit));
			}
		}

		protected frmW2CEdit _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngW2Year;
		private int lngCurrentRecord;
		private bool boolDataChanged;
		private string strCurrentSSN = string.Empty;
        private string currentEmployeeNumber = "";
        private string currentDeptDiv = "";
		public void Init(ref int lngYear, string strSSN, int currentRecord = 0)
		{
			lngW2Year = lngYear;
			strCurrentSSN = strSSN;
			lngCurrentRecord = currentRecord;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void chkCorrectingSSNorName_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCorrectingSSNorName.CheckState == Wisej.Web.CheckState.Checked)
			{
				Frame2.Enabled = true;
				lblIncorrectSSN.Enabled = true;
				lblIncorrectFirst.Enabled = true;
				lblIncorrectMI.Enabled = true;
				lblIncorrectLast.Enabled = true;
				lblIncorrectDesig.Enabled = true;
				txtIncorrectSSN.Enabled = true;
				txtIncorrectFirst.Enabled = true;
				txtIncorrectMI.Enabled = true;
				txtIncorrectLast.Enabled = true;
				txtIncorrectDesig.Enabled = true;
			}
			else
			{
				Frame2.Enabled = false;
				lblIncorrectSSN.Enabled = false;
				lblIncorrectFirst.Enabled = false;
				lblIncorrectMI.Enabled = false;
				lblIncorrectLast.Enabled = false;
				lblIncorrectDesig.Enabled = false;
				txtIncorrectSSN.Enabled = false;
				txtIncorrectFirst.Enabled = false;
				txtIncorrectMI.Enabled = false;
				txtIncorrectLast.Enabled = false;
				txtIncorrectDesig.Enabled = false;
			}
		}

		private void frmW2CEdit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmW2CEdit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2CEdit properties;
			//frmW2CEdit.FillStyle	= 0;
			//frmW2CEdit.ScaleWidth	= 9300;
			//frmW2CEdit.ScaleHeight	= 7500;
			//frmW2CEdit.LinkTopic	= "Form2";
			//frmW2CEdit.LockControls	= -1  'True;
			//frmW2CEdit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid14();
			SetupGrid12();
			LoadRecord();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has changed" + "\r\n" + "Save data now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveRecord())
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmW2CEdit_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid14();
			ResizeGrid12();
		}

		private void GridCurr12_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridCurr14_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridPrev12_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridPrev14_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolDataChanged = true;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid14()
		{
			GridPrev14.TextMatrix(0, 0, "Code");
			GridPrev14.TextMatrix(0, 1, "Amount");
            GridPrev14.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridCurr14.TextMatrix(0, 0, "Code");
			GridCurr14.TextMatrix(0, 1, "Amount");
            GridCurr14.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void SetupGrid12()
		{
			GridPrev12.TextMatrix(0, 0, "Code");
			GridPrev12.TextMatrix(0, 1, "Amount");
            GridPrev12.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridCurr12.TextMatrix(0, 0, "Code");
			GridCurr12.TextMatrix(0, 1, "Amount");
            GridCurr12.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGrid14()
		{
			int GridWidth = 0;
			int GridHeight = 0;
			GridWidth = GridPrev14.WidthOriginal;
			GridHeight = GridPrev14.RowHeight(0) * 5 + 50;
			GridPrev14.ColWidth(0, FCConvert.ToInt32(0.33 * GridWidth));
			//GridPrev14.Height = GridHeight;
			GridCurr14.ColWidth(0, FCConvert.ToInt32(0.33 * GridWidth));
			//GridCurr14.Height = GridHeight;
		}

		private void ResizeGrid12()
		{
			int GridWidth = 0;
			int GridHeight = 0;
			GridWidth = GridPrev12.WidthOriginal;
			GridHeight = GridPrev12.RowHeight(0) * 5 + 50;
			GridPrev12.ColWidth(0, FCConvert.ToInt32(0.33 * GridWidth));
			//GridPrev12.Height = GridHeight;
			GridCurr12.ColWidth(0, FCConvert.ToInt32(0.33 * GridWidth));
			//GridCurr12.Height = GridHeight;
		}

		private void LoadRecord()
		{
            var rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                rsLoad.OpenRecordset("select * from w2c where Id = " + lngCurrentRecord, "twpy0000.vb1");

                if (rsLoad.EndOfFile())
                {
                    lngCurrentRecord = 0;

                    return;
                }

                lngCurrentRecord = rsLoad.Get_Fields_Int32("ID");
                txtFirstName.Text = rsLoad.Get_Fields_String("firstname");
                txtLastName.Text = rsLoad.Get_Fields_String("lastname");
                txtMI.Text = FCConvert.ToString(rsLoad.Get_Fields("middlename"));
                txtDesig.Text = FCConvert.ToString(rsLoad.Get_Fields("desig"));
                txtAddress1.Text = FCConvert.ToString(rsLoad.Get_Fields("address1"));
                txtAddress2.Text = FCConvert.ToString(rsLoad.Get_Fields("address2"));
                txtCity.Text = FCConvert.ToString(rsLoad.Get_Fields("city"));
                txtState.Text = FCConvert.ToString(rsLoad.Get_Fields("state"));
                txtZip.Text = FCConvert.ToString(rsLoad.Get_Fields_String("zip"));
                txtSSN.Text = FCConvert.ToString(rsLoad.Get_Fields("ssn"));
                currentEmployeeNumber = rsLoad.Get_Fields_String("employeenumber");
                currentDeptDiv = rsLoad.Get_Fields_String("DeptDiv");
                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("correctingssnorname")))
                {
                    chkCorrectingSSNorName.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkCorrectingSSNorName.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                txtIncorrectSSN.Text = FCConvert.ToString(rsLoad.Get_Fields("prevssn"));
                txtIncorrectDesig.Text = FCConvert.ToString(rsLoad.Get_Fields("prevdesig"));
                txtIncorrectFirst.Text = FCConvert.ToString(rsLoad.Get_Fields("prevfirstname"));
                txtIncorrectLast.Text = FCConvert.ToString(rsLoad.Get_Fields("prevlastname"));
                txtIncorrectMI.Text = FCConvert.ToString(rsLoad.Get_Fields("prevmiddlename"));

                if (Conversion.Val(rsLoad.Get_Fields("federalwage")) != Conversion.Val(rsLoad.Get_Fields("prevfederalwage")) || Conversion.Val(rsLoad.Get_Fields("federalwage")) > 0)
                {
                    txtCurrWages.Text = Strings.Format(rsLoad.Get_Fields("federalwage"), "0.00");
                    txtPrevWages.Text = Strings.Format(rsLoad.Get_Fields("prevfederalwage"), "0.00");
                }
                else
                {
                    txtCurrWages.Text = "";
                    txtPrevWages.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields_Double("FicaWage")) != Conversion.Val(rsLoad.Get_Fields("prevficawage")) || Conversion.Val(rsLoad.Get_Fields("ficawage")) > 0)
                {
                    txtCurrSSWages.Text = Strings.Format(rsLoad.Get_Fields("ficawage"), "0.00");
                    txtPrevSSWages.Text = Strings.Format(rsLoad.Get_Fields("prevficawage"), "0.00");
                }
                else
                {
                    txtCurrSSWages.Text = "";
                    txtPrevSSWages.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("medicarewage")) != Conversion.Val(rsLoad.Get_Fields("prevmedicarewage")) || Conversion.Val(rsLoad.Get_Fields("medicarewage")) > 0)
                {
                    txtCurrMedicare.Text = Strings.Format(rsLoad.Get_Fields("medicarewage"), "0.00");
                    txtPrevMedicare.Text = Strings.Format(rsLoad.Get_Fields("prevmedicarewage"), "0.00");
                }
                else
                {
                    txtCurrMedicare.Text = "";
                    txtPrevMedicare.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("federaltax")) != Conversion.Val(rsLoad.Get_Fields("prevfederaltax")) || Conversion.Val(rsLoad.Get_Fields("federaltax")) > 0)
                {
                    txtCurrFedTax.Text = Strings.Format(rsLoad.Get_Fields("federaltax"), "0.00");
                    txtPrevFedTax.Text = Strings.Format(rsLoad.Get_Fields("prevfederaltax"), "0.00");
                }
                else
                {
                    txtCurrFedTax.Text = "";
                    txtPrevFedTax.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("ficatax")) != Conversion.Val(rsLoad.Get_Fields("prevficatax")) || Conversion.Val(rsLoad.Get_Fields("ficatax")) > 0)
                {
                    txtCurrSSTax.Text = Strings.Format(rsLoad.Get_Fields("ficatax"), "0.00");
                    txtPrevSSTax.Text = Strings.Format(rsLoad.Get_Fields("prevficatax"), "0.00");
                }
                else
                {
                    txtCurrSSTax.Text = "";
                    txtPrevSSTax.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("medicaretax")) != Conversion.Val(rsLoad.Get_Fields("prevmedicaretax")) || Conversion.Val(rsLoad.Get_Fields("medicaretax")) > 0)
                {
                    txtCurrMedTax.Text = Strings.Format(rsLoad.Get_Fields("medicaretax"), "0.00");
                    txtPrevMedTax.Text = Strings.Format(rsLoad.Get_Fields("prevmedicaretax"), "0.00");
                }
                else
                {
                    txtCurrMedTax.Text = "";
                    txtPrevMedTax.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("sstips")) != Conversion.Val(rsLoad.Get_Fields("prevsstips")) || Conversion.Val(rsLoad.Get_Fields("sstips")) > 0)
                {
                    txtCurrSSTips.Text = Strings.Format(rsLoad.Get_Fields("sstips"), "0.00");
                    txtPrevSSTips.Text = Strings.Format(rsLoad.Get_Fields("prevsstips"), "0.00");
                }
                else
                {
                    txtCurrSSTips.Text = "";
                    txtPrevSSTips.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("allocatedtips")) != Conversion.Val(rsLoad.Get_Fields("prevallocatedtips")) || Conversion.Val(rsLoad.Get_Fields("allocatedtips")) > 0)
                {
                    txtCurrTips.Text = Strings.Format(rsLoad.Get_Fields("allocatedtips"), "0.00");
                    txtPrevTips.Text = Strings.Format(rsLoad.Get_Fields("prevallocatedtips"), "0.00");
                }
                else
                {
                    txtCurrTips.Text = "";
                    txtPrevTips.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("nonqualifiedamounts")) != Conversion.Val(rsLoad.Get_Fields("prevnonqualifiedamounts")) || Conversion.Val(rsLoad.Get_Fields("nonqualifiedamounts")) > 0)
                {
                    txtCurrNonqualifiedPlans.Text = Strings.Format(rsLoad.Get_Fields("nonqualifiedamounts"), "0.00");
                    txtPrevNonQualifiedPlans.Text = Strings.Format(rsLoad.Get_Fields("prevnonqualifiedamounts"), "0.00");
                }
                else
                {
                    txtCurrNonqualifiedPlans.Text = "";
                    txtPrevNonQualifiedPlans.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("dependentcare")) != Conversion.Val(rsLoad.Get_Fields("prevdependentcare")) || Conversion.Val(rsLoad.Get_Fields("dependentcare")) > 0)
                {
                    txtCurrDependentCare.Text = Strings.Format(rsLoad.Get_Fields("dependentcare"), "0.00");
                    txtPrevDependentCare.Text = Strings.Format(rsLoad.Get_Fields("prevdependentcare"), "0.00");
                }
                else
                {
                    txtCurrDependentCare.Text = "";
                    txtPrevDependentCare.Text = "";
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("w2retirement")))
                {
                    chkCurrRetPlan.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkCurrRetPlan.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("w2deferred")))
                {
                    chkCurrThirdParty.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkCurrThirdParty.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("w2statutoryemployee")))
                {
                    chkCurrStatEmployee.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkCurrStatEmployee.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("prevw2retirement")))
                {
                    chkPrevRetPlan.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkPrevRetPlan.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("prevw2deferred")))
                {
                    chkPrevThirdParty.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkPrevThirdParty.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("prevw2statutoryemployee")))
                {
                    chkPrevStatEmployee.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkPrevStatEmployee.CheckState = Wisej.Web.CheckState.Unchecked;
                }

                // state info
                txtCurrState.Text = FCConvert.ToString(rsLoad.Get_Fields("statename"));
                txtPrevState.Text = FCConvert.ToString(rsLoad.Get_Fields("prevstatename"));
                txtCurrState2.Text = FCConvert.ToString(rsLoad.Get_Fields("statename2"));
                txtPrevState2.Text = FCConvert.ToString(rsLoad.Get_Fields("prevstatename2"));

                if (Conversion.Val(rsLoad.Get_Fields("statewage")) != Conversion.Val(rsLoad.Get_Fields("prevstatewage")) || Conversion.Val(rsLoad.Get_Fields("statewage")) > 0)
                {
                    txtCurrStateWages.Text = Strings.Format(rsLoad.Get_Fields("statewage"), "0.00");
                    txtPrevStateWages.Text = Strings.Format(rsLoad.Get_Fields("prevstatewage"), "0.00");
                }
                else
                {
                    txtCurrStateWages.Text = "";
                    txtPrevStateWages.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("statewage2")) != Conversion.Val(rsLoad.Get_Fields("prevstatewage2")) || Conversion.Val(rsLoad.Get_Fields("statewage2")) > 0)
                {
                    txtCurrStateWages2.Text = Strings.Format(rsLoad.Get_Fields("statewage2"), "0.00");
                    txtPrevStateWages2.Text = Strings.Format(rsLoad.Get_Fields("prevstatewage2"), "0.00");
                }
                else
                {
                    txtCurrStateWages2.Text = "";
                    txtPrevStateWages2.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("statetax")) != Conversion.Val(rsLoad.Get_Fields("prevstatetax")) || Conversion.Val(rsLoad.Get_Fields("statetax")) > 0)
                {
                    txtCurrStateIncome.Text = Strings.Format(rsLoad.Get_Fields("statetax"), "0.00");
                    txtPrevStateIncome.Text = Strings.Format(rsLoad.Get_Fields("prevstatetax"), "0.00");
                }
                else
                {
                    txtCurrStateIncome.Text = "";
                    txtPrevStateIncome.Text = "";
                }

                if (Conversion.Val(rsLoad.Get_Fields("statetax2")) != Conversion.Val(rsLoad.Get_Fields("prevstatetax2")) || Conversion.Val(rsLoad.Get_Fields("statetax2")) > 0)
                {
                    txtCurrStateIncome2.Text = Strings.Format(rsLoad.Get_Fields("statetax2"), "0.00");
                    txtPrevStateIncome2.Text = Strings.Format(rsLoad.Get_Fields("prevstatetax2"), "0.00");
                }
                else
                {
                    txtCurrStateIncome2.Text = "";
                    txtPrevStateIncome2.Text = "";
                }

                txtCurrStateID.Text = FCConvert.ToString(rsLoad.Get_Fields("stateid"));
                txtPrevStateID.Text = FCConvert.ToString(rsLoad.Get_Fields("prevstateid"));
                txtCurrStateID2.Text = FCConvert.ToString(rsLoad.Get_Fields("stateid2"));
                txtPrevStateID2.Text = FCConvert.ToString(rsLoad.Get_Fields("prevstateid2"));

                int x;
                rsLoad.OpenRecordset("select * from w2cbox1214 where recordid = " + FCConvert.ToString(lngCurrentRecord) + " order by line", "twpy0000.vb1");

                for (x = 1; x <= 4; x++)
                {
                    GridCurr12.TextMatrix(x, 0, "");
                    GridCurr12.TextMatrix(x, 1, "");
                    GridCurr14.TextMatrix(x, 0, "");
                    GridCurr14.TextMatrix(x, 1, "");
                    GridPrev12.TextMatrix(x, 0, "");
                    GridPrev12.TextMatrix(x, 1, "");
                    GridPrev14.TextMatrix(x, 0, "");
                    GridPrev14.TextMatrix(x, 1, "");
                }

                // x
                while (!rsLoad.EndOfFile())
                {
                    if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("box12")))
                    {
                        GridCurr12.TextMatrix(rsLoad.Get_Fields("line"), 0, FCConvert.ToString(rsLoad.Get_Fields("code")));
                        GridCurr12.TextMatrix(rsLoad.Get_Fields("line"), 1, Strings.Format(rsLoad.Get_Fields("amount"), "0.00"));
                        GridPrev12.TextMatrix(rsLoad.Get_Fields("line"), 0, FCConvert.ToString(rsLoad.Get_Fields("prevcode")));
                        GridPrev12.TextMatrix(rsLoad.Get_Fields("line"), 1, Strings.Format(rsLoad.Get_Fields("prevamount"), "0.00"));
                    }
                    else
                    {
                        GridCurr14.TextMatrix(rsLoad.Get_Fields("line"), 0, FCConvert.ToString(rsLoad.Get_Fields("code")));
                        GridCurr14.TextMatrix(rsLoad.Get_Fields("line"), 1, Strings.Format(rsLoad.Get_Fields("amount"), "0.00"));
                        GridPrev14.TextMatrix(rsLoad.Get_Fields("line"), 0, FCConvert.ToString(rsLoad.Get_Fields("prevcode")));
                        GridPrev14.TextMatrix(rsLoad.Get_Fields("line"), 1, Strings.Format(rsLoad.Get_Fields("prevamount"), "0.00"));
                    }

                    rsLoad.MoveNext();
                }

                boolDataChanged = false;

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
		}

		private bool SaveRecord()
		{
			var W2CInfo = new W2c
			{
				Id = lngCurrentRecord,
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				MiddleName = txtMI.Text,
				Desig = txtDesig.Text,
				Address1 = txtAddress1.Text,
				Address2 = txtAddress2.Text,
				City = txtCity.Text,
				State = txtState.Text,
				Zip = txtZip.Text,
				Ssn = txtSSN.Text,
				CorrectingSsnorName = chkCorrectingSSNorName.CheckState == Wisej.Web.CheckState.Checked,
				PrevSsn = txtIncorrectSSN.Text,
				PrevDesig = txtIncorrectDesig.Text,
				PrevFirstName = txtIncorrectFirst.Text,
				PrevLastName = txtIncorrectLast.Text,
				PrevMiddleName = txtIncorrectMI.Text,
				FederalWage = Conversion.Val(txtCurrWages.Text),
				PrevFederalWage = Conversion.Val(txtPrevWages.Text),
				FederalTax = Conversion.Val(txtCurrFedTax.Text),
				PrevFederalTax = Conversion.Val(txtPrevFedTax.Text),
				FicaWage = Conversion.Val(txtCurrSSWages.Text),
				PrevFicaWage = Conversion.Val(txtPrevSSWages.Text),
				FicaTax = Conversion.Val(txtCurrSSTax.Text),
				PrevFicaTax = Conversion.Val(txtPrevSSTax.Text),
				MedicareWage = Conversion.Val(txtCurrMedicare.Text),
				PrevMedicareWage = Conversion.Val(txtPrevMedicare.Text),
				MedicareTax = Conversion.Val(txtCurrMedTax.Text),
				PrevMedicareTax = Conversion.Val(txtPrevMedTax.Text),
				Sstips = Conversion.Val(txtCurrSSTips.Text),
				PrevSstips = Conversion.Val(txtPrevSSTips.Text),
				AllocatedTips = Conversion.Val(txtCurrTips.Text),
				PrevAllocatedTips = Conversion.Val(txtPrevTips.Text),
				NonQualifiedAmounts = Conversion.Val(txtCurrNonqualifiedPlans.Text),
				PrevNonQualifiedAmounts = Conversion.Val(txtPrevNonQualifiedPlans.Text),
				DependentCare = Conversion.Val(txtCurrDependentCare.Text),
				PrevDependentCare = Conversion.Val(txtPrevDependentCare.Text),
				W2retirement = chkCurrRetPlan.CheckState == Wisej.Web.CheckState.Checked,
				PrevW2retirement = chkPrevRetPlan.CheckState == Wisej.Web.CheckState.Checked,
				W2statutoryEmployee = chkCurrStatEmployee.CheckState == Wisej.Web.CheckState.Checked,
				PrevW2statutoryEmployee = chkPrevStatEmployee.CheckState == Wisej.Web.CheckState.Checked,
				W2deferred = chkCurrThirdParty.CheckState == Wisej.Web.CheckState.Checked,
				PrevW2deferred = chkPrevThirdParty.CheckState == Wisej.Web.CheckState.Checked,
				StateName = txtCurrState.Text,
				PrevStateName = txtPrevState.Text,
				StateWage = Conversion.Val(txtCurrStateWages.Text),
				PrevStateWage = Conversion.Val(txtPrevStateWages.Text),
				StateTax = Conversion.Val(txtCurrStateIncome.Text),
				PrevStateTax = Conversion.Val(txtPrevStateIncome.Text),
				StateId = txtCurrStateID.Text,
				PrevStateId = txtPrevStateID.Text,
				StateName2 = txtCurrState2.Text,
				PrevStateName2 = txtPrevState2.Text,
				StateWage2 = Conversion.Val(txtCurrStateWages2.Text),
				PrevStateWage2 = Conversion.Val(txtPrevStateWages2.Text),
				StateTax2 = Conversion.Val(txtCurrStateIncome2.Text),
				PrevStateTax2 = Conversion.Val(txtPrevStateIncome2.Text),
				StateId2 = txtCurrStateID2.Text,
				PrevStateId2 = txtPrevStateID2.Text,
				TaxYear = lngW2Year,
				EmployeeNumber =  currentEmployeeNumber,
				DeptDiv =  currentDeptDiv
			};

			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngID;
				SaveRecord = false;
				StaticSettings.GlobalCommandDispatcher.Send(new SaveW2CInfo
				{
					W2CInfo = W2CInfo
				});
				
				// box 12 and 14 stuff
				rsSave.OpenRecordset("select * from w2cbox1214 where recordid = " + FCConvert.ToString(lngCurrentRecord) + " order by line", "twpy0000.vb1");
				int x;
				for (x = 1; x <= 4; x++)
				{
					// box 12
					if (rsSave.FindFirstRecord2("Box12,line", "1," + FCConvert.ToString(x), ","))
					{
						if (fecherFoundation.Strings.Trim(GridCurr12.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridCurr12.TextMatrix(x, 1)) > 0 || fecherFoundation.Strings.Trim(GridPrev12.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridPrev12.TextMatrix(x, 1)) > 0)
						{
							rsSave.Edit();
							rsSave.Set_Fields("Code", GridCurr12.TextMatrix(x, 0));
							rsSave.Set_Fields("prevcode", GridPrev12.TextMatrix(x, 0));
							rsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(GridCurr12.TextMatrix(x, 1))));
							rsSave.Set_Fields("prevamount", FCConvert.ToString(Conversion.Val(GridPrev12.TextMatrix(x, 1))));
							rsSave.Update();
						}
						else
						{
							rsSave.Delete();
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(GridCurr12.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridCurr12.TextMatrix(x, 1)) > 0 || fecherFoundation.Strings.Trim(GridPrev12.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridPrev12.TextMatrix(x, 1)) > 0)
						{
							rsSave.AddNew();
							rsSave.Set_Fields("recordid", lngCurrentRecord);
							rsSave.Set_Fields("taxyear", lngW2Year);
							rsSave.Set_Fields("box12", true);
							rsSave.Set_Fields("Code", GridCurr12.TextMatrix(x, 0));
							rsSave.Set_Fields("prevcode", GridPrev12.TextMatrix(x, 0));
							rsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(GridCurr12.TextMatrix(x, 1))));
							rsSave.Set_Fields("prevamount", FCConvert.ToString(Conversion.Val(GridPrev12.TextMatrix(x, 1))));
							rsSave.Set_Fields("line", x);
							rsSave.Update();
						}
					}
					if (rsSave.FindFirstRecord2("box14,line", "1," + FCConvert.ToString(x), ","))
					{
						if (fecherFoundation.Strings.Trim(GridCurr14.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridCurr14.TextMatrix(x, 1)) > 0 || fecherFoundation.Strings.Trim(GridPrev14.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridPrev14.TextMatrix(x, 1)) > 0)
						{
							rsSave.Edit();
							rsSave.Set_Fields("Code", GridCurr14.TextMatrix(x, 0));
							rsSave.Set_Fields("prevcode", GridPrev14.TextMatrix(x, 0));
							rsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(GridCurr14.TextMatrix(x, 1))));
							rsSave.Set_Fields("prevamount", FCConvert.ToString(Conversion.Val(GridPrev14.TextMatrix(x, 1))));
							rsSave.Update();
						}
						else
						{
							rsSave.Delete();
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(GridCurr14.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridCurr14.TextMatrix(x, 1)) > 0 || fecherFoundation.Strings.Trim(GridPrev14.TextMatrix(x, 0)) != string.Empty || Conversion.Val(GridPrev14.TextMatrix(x, 1)) > 0)
						{
							rsSave.AddNew();
							rsSave.Set_Fields("recordid", lngCurrentRecord);
							rsSave.Set_Fields("taxyear", lngW2Year);
							rsSave.Set_Fields("box14", true);
							rsSave.Set_Fields("line", x);
							rsSave.Set_Fields("Code", GridCurr14.TextMatrix(x, 0));
							rsSave.Set_Fields("prevcode", GridPrev14.TextMatrix(x, 0));
							rsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(GridCurr14.TextMatrix(x, 1))));
							rsSave.Set_Fields("prevamount", FCConvert.ToString(Conversion.Val(GridPrev14.TextMatrix(x, 1))));
							rsSave.Update();
						}
					}
				}
				// x
				boolDataChanged = false;
				SaveRecord = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveRecord;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridCurr12.Row = 0;
			GridPrev12.Row = 0;
			GridCurr14.Row = 0;
			GridPrev14.Row = 0;
			//App.DoEvents();
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridCurr12.Row = 0;
			GridPrev12.Row = 0;
			GridCurr14.Row = 0;
			GridPrev14.Row = 0;
			//App.DoEvents();
			if (SaveRecord())
			{
				mnuExit_Click();
			}
		}

		private void txtAddress1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtAddress2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCity_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrDependentCare_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}


		private void txtCurrFedTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrMedicare_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrMedTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrNonqualifiedPlans_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrSSTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrSSTips_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrSSWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrState2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateID2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateIncome_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateIncome2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrStateWages2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrTips_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCurrWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtDesig_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtFirstName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtIncorrectDesig_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtIncorrectFirst_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtIncorrectLast_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtIncorrectMI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtIncorrectSSN_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLastName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtMI_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevDependentCare_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevFedTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevMedicare_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevMedTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevNonQualifiedPlans_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevSSTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevSSTips_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevSSWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevState2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateID2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateIncome_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateIncome2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevStateWages2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevTips_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPrevWages_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtSSN_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtZip_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

        private void txtCurrState_TextChanged(object sender, EventArgs e)
        {

        }

        private void Frame9_Enter(object sender, EventArgs e)
        {

        }

        private void Frame11_Enter(object sender, EventArgs e)
        {

        }
    }
}
