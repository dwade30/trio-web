﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsESHPEmployeeList
	{
		//=========================================================
		private FCCollection lstEmployees = new FCCollection();
		private int intCurrentIndex;
		private DateTime dtPayDate;
		private int intPayrun;

		public clsESHPEmployeeList() : base()
		{
			intCurrentIndex = -1;
		}

		public DateTime PayDate
		{
			set
			{
				dtPayDate = value;
			}
			get
			{
				DateTime PayDate = System.DateTime.Now;
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public int Payrun
		{
			set
			{
				intPayrun = value;
			}
			get
			{
				int Payrun = 0;
				Payrun = intPayrun;
				return Payrun;
			}
		}

		public void ClearList()
		{
			if (!(lstEmployees == null))
			{
				foreach (clsESHPEmployeeDetail tRec in lstEmployees)
				{
					lstEmployees.Remove(1);
				}
				// tRec
			}
		}

		public void AddItem(ref clsESHPEmployeeDetail tItem)
		{
			if (!(tItem == null))
			{
				lstEmployees.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			if (!(lstEmployees == null))
			{
				if (!FCUtils.IsEmpty(lstEmployees))
				{
					if (lstEmployees.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstEmployees))
			{
				if (intCurrentIndex > lstEmployees.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstEmployees.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstEmployees.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstEmployees[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public clsESHPEmployeeDetail GetCurrentEmployee()
		{
			clsESHPEmployeeDetail GetCurrentEmployee = null;
			clsESHPEmployeeDetail tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstEmployees))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstEmployees[intCurrentIndex] == null))
					{
						tRec = lstEmployees[intCurrentIndex];
					}
				}
			}
			GetCurrentEmployee = tRec;
			return GetCurrentEmployee;
		}

		public clsESHPEmployeeDetail GetEmployeeByIndex(ref int intindex)
		{
			clsESHPEmployeeDetail GetEmployeeByIndex = null;
			clsESHPEmployeeDetail tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstEmployees) && intindex > 0)
			{
				if (!(lstEmployees[intindex] == null))
				{
					intCurrentIndex = intindex;
					tRec = lstEmployees[intindex];
				}
			}
			GetEmployeeByIndex = tRec;
			return GetEmployeeByIndex;
		}

		public int ItemCount()
		{
			int ItemCount = 0;
			if (!FCUtils.IsEmpty(lstEmployees))
			{
				ItemCount = lstEmployees.Count;
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}
	}
}
