﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEUC1FinalRecord
	{
		//=========================================================
		private int intSRecords;
		private int intERecords;
		private double dblUCWages;

		public int NumberOfEmployerRecords
		{
			set
			{
				intERecords = value;
			}
			get
			{
				int NumberOfEmployerRecords = 0;
				NumberOfEmployerRecords = intERecords;
				return NumberOfEmployerRecords;
			}
		}

		public double UCWages
		{
			set
			{
				dblUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblUCWages;
				return UCWages;
			}
		}

		public int NumberOfEmployeeRecords
		{
			set
			{
				intSRecords = value;
			}
			get
			{
				int NumberOfEmployeeRecords = 0;
				NumberOfEmployeeRecords = intSRecords;
				return NumberOfEmployeeRecords;
			}
		}

		public string ToString()
		{
			string ToString = "";
			string strReturn;
			strReturn = "F";
			strReturn += Strings.Format(intSRecords, Strings.StrDup(10, "0"));
			strReturn += Strings.Format(intERecords, Strings.StrDup(10, "0"));
			strReturn += "WAGE";
			strReturn += Strings.StrDup(15, " ");
			// not used by maine
			strReturn += Strings.Format(FCConvert.ToInt32(Strings.Format(dblUCWages, "0.00")) * 100, Strings.StrDup(15, "0"));
			strReturn += Strings.StrDup(220, " ");
			// not used by maine
			ToString = strReturn;
			return ToString;
		}
	}
}
