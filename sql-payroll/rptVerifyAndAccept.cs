﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptVerifyAndAccept.
	/// </summary>
	public partial class rptVerifyAndAccept : BaseSectionReport
	{
		public rptVerifyAndAccept()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Verify and Accept";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptVerifyAndAccept InstancePtr
		{
			get
			{
				return (rptVerifyAndAccept)Sys.GetInstance(typeof(rptVerifyAndAccept));
			}
		}

		protected rptVerifyAndAccept _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVerifyAndAccept	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private bool boolFinished;
		private int intItemCounter;
		private bool boolBold;
		int intPageNumber = 0;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", modalDialog ? (int)FCForm.FormShowEnum.Modal : (int) FCForm.FormShowEnum.Modeless , false, false, "Pages", false, "", "TRIO Software", false, true, "VerifyAndAccept", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			boolBold = false;
			Line1.Visible = false;
			if (intItemCounter == 1)
			{
				// this is the first time thru.
				// print the pay cat grid
				txtData1.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 0);
				txtData2.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 1);
				txtData3.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 2);
				intCounter = 1;
				intItemCounter = 2;
				boolBold = true;
				Line1.Visible = true;
				eArgs.EOF = false;
				return;
			}
			else if (intItemCounter == 2)
			{
				// save the pay category information
				if (intCounter < frmProcessVerify.InstancePtr.vsPayCatGrid.Rows)
				{
					txtData1.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 0);
					txtData2.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 1);
					txtData3.Text = frmProcessVerify.InstancePtr.vsPayCatGrid.TextMatrix(intCounter, 2);
					intCounter += 1;
					eArgs.EOF = false;
					return;
				}
				else
				{
					intItemCounter = 3;
					intCounter = 0;
					txtData1.Text = string.Empty;
					txtData2.Text = string.Empty;
					txtData3.Text = string.Empty;
					eArgs.EOF = false;
					return;
				}
			}
			else if (intItemCounter == 3)
			{
				// Save the Totals information
				if (intCounter == 0)
				{
					// print the pay cat grid Captions
					txtData1.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 0);
					txtData2.Text = "Amount";
					txtData3.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 3);
					txtData4.Text = "Amount";
					intCounter = 1;
					Line1.Visible = true;
					boolBold = true;
					eArgs.EOF = false;
					return;
				}
				else
				{
					// save the total information
					if (intCounter < 6)
					{
						txtData1.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 0);
						txtData2.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 1);
						// this will pick up the employers match as it is on the next line
						if (intCounter != 6)
						{
							txtData3.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 3);
							txtData4.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter, 4);
						}
						else
						{
							txtData3.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter + 1, 3);
							txtData4.Text = frmProcessVerify.InstancePtr.vsTotals.TextMatrix(intCounter + 1, 4);
						}
						intCounter += 1;
						eArgs.EOF = false;
						return;
					}
					else
					{
						// intCounter = intCounter + 1
						intItemCounter = 4;
                        intCounter = 0;
						txtData1.Text = string.Empty;
						txtData2.Text = string.Empty;
						txtData3.Text = string.Empty;
						txtData4.Text = string.Empty;
						eArgs.EOF = false;
						return;
					}
				}
			}
			else if (intItemCounter == 4)
			{
				// Save the bottom section of the Totals information
				if (intCounter == 0)
				{
					txtData1.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter, 0);
					txtData2.Text = "Amount";
					txtData3.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter , 3);
					txtData4.Text = "Amount";
					intCounter += 1;
					Line1.Visible = true;
					boolBold = true;
					eArgs.EOF = false;
					return;
				}
				else
				{
					// save the total information
					if (intCounter < frmProcessVerify.InstancePtr.GridCheckSummary.Rows)
					{
						txtData1.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter, 0);
						txtData2.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter, 1);
                        txtData3.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter, 3);
                        txtData4.Text = frmProcessVerify.InstancePtr.GridCheckSummary.TextMatrix(intCounter , 4);
                       
						intCounter += 1;
						eArgs.EOF = false;
						return;
					}
					else
					{
						intItemCounter = 5;
						intCounter = 0;
						txtData1.Text = string.Empty;
						txtData2.Text = string.Empty;
						txtData3.Text = string.Empty;
						txtData4.Text = string.Empty;
						eArgs.EOF = false;
						return;
					}
				}
			}
            else if (intItemCounter == 5)
			{
				// Save the Deduction information
				if (intCounter == 0)
				{
					// Print the Deduction Captions
					txtData1.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 0);
					txtData2.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 1);
					txtData3.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 2);
					txtData4.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 3);
					intCounter = 1;
					Line1.Visible = true;
					boolBold = true;
					eArgs.EOF = false;
					return;
				}
				else
				{
					// save the pay category information
					if (intCounter < frmProcessVerify.InstancePtr.vsDeductionGrid.Rows)
					{
						txtData1.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 0);
						txtData2.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 1);
						txtData3.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 2);
						txtData4.Text = frmProcessVerify.InstancePtr.vsDeductionGrid.TextMatrix(intCounter, 3);
						intCounter += 1;
						eArgs.EOF = false;
						return;
					}
					else
					{
						intItemCounter = 6;
						intCounter = 0;
						txtData1.Text = string.Empty;
						txtData2.Text = string.Empty;
						txtData3.Text = string.Empty;
						txtData4.Text = string.Empty;
						eArgs.EOF = false;
						return;
					}
				}
			}
			else if (intItemCounter == 6)
			{
				// Save the Deduction information
				if (intCounter == 0)
				{
					// Print the Deduction Captions
					txtData1.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 0);
					txtData2.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 1);
					txtData3.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 2);
					txtData4.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 3);
					intCounter = 1;
					Line1.Visible = true;
					boolBold = true;
					eArgs.EOF = false;
					return;
				}
				else
				{
					// save the pay category information
					if (intCounter < frmProcessVerify.InstancePtr.vsMatchGrid.Rows)
					{
						txtData1.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 0);
						txtData2.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 1);
						txtData3.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 2);
						txtData4.Text = frmProcessVerify.InstancePtr.vsMatchGrid.TextMatrix(intCounter, 3);
						intCounter += 1;
						eArgs.EOF = false;
						return;
					}
					else
					{
						intItemCounter = 7;
						intCounter = 0;
						txtData1.Text = string.Empty;
						txtData2.Text = string.Empty;
						txtData3.Text = string.Empty;
						txtData4.Text = string.Empty;
						eArgs.EOF = false;
						return;
					}
				}
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			intItemCounter = 1;
			intCounter = 0;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			//modPrintToFile.SetPrintProperties(this);
		}

		private void Detail_BeforePrint(object sender, EventArgs e)
		{
			// txtData1.Style = "font-family: Tahoma: bold;"
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolBold)
			{
				txtData1.Style = "font-family: Tahoma; font-weight: bold;";
				txtData2.Style = txtData1.Style;
				txtData3.Style = txtData1.Style;
				txtData4.Style = txtData1.Style;
			}
			else
			{
				txtData1.Style = "font-family: Tahoma;";
				txtData2.Style = txtData1.Style;
				txtData3.Style = txtData1.Style;
				txtData4.Style = txtData1.Style;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniname As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + intPageNumber;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
