﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmDateRange : BaseForm
	{
		public frmDateRange()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDateRange InstancePtr
		{
			get
			{
				return (frmDateRange)Sys.GetInstance(typeof(frmDateRange));
			}
		}

		protected frmDateRange _InstancePtr = null;
		//=========================================================
		private void frmDateRange_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmDateRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
				return;
			}
		}

		private void frmDateRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDateRange properties;
			//frmDateRange.ScaleWidth	= 3885;
			//frmDateRange.ScaleHeight	= 2175;
			//frmDateRange.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vsElasticLight1.Enabled = True
				modGlobalVariables.Statics.gboolCancelSelected = true;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
				modGlobalFunctions.SetTRIOColors(this);
				SetGridProperties();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDateRange_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, FCConvert.ToInt32((vsData.WidthOriginal / 2.0) - 50));
			// vsData.ColWidth(1) = vsData.ColWidth(0)
			// vsData.RowHeight(1) = (vsData.Height - (vsData.RowHeight(0) + 100))
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			vsData.Select(0, 0);
			if (Information.IsDate(vsData.TextMatrix(1, 0)))
			{
			}
			else
			{
				MessageBox.Show("Invalid Start Date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				vsData.Select(1, 0);
				modGlobalVariables.Statics.gboolCancelSelected = true;
				return;
			}
			if (Information.IsDate(vsData.TextMatrix(1, 1)))
			{
			}
			else
			{
				MessageBox.Show("Invalid End Date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				vsData.Select(1, 1);
				modGlobalVariables.Statics.gboolCancelSelected = true;
				return;
			}
			modGlobalVariables.Statics.gdatStart = FCConvert.ToDateTime(vsData.TextMatrix(1, 0));
			modGlobalVariables.Statics.gdatEnd = FCConvert.ToDateTime(vsData.TextMatrix(1, 1));
			modGlobalVariables.Statics.gboolCancelSelected = false;
			Close();
		}

		private void SetGridProperties()
		{
			vsData.TextMatrix(0, 0, "From");
			vsData.TextMatrix(0, 1, "To");
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// vsData.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1) = True
			vsData.ColEditMask(0, "##/##/####");
			vsData.ColEditMask(1, "##/##/####");
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

        private void cmdProcess_Click(object sender, EventArgs e)
        {
            mnuProcess_Click(cmdProcess, EventArgs.Empty);
        }
    }
}
