//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmployeeDataEntry : BaseForm
	{
		public frmEmployeeDataEntry()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeeDataEntry InstancePtr
		{
			get
			{
				return (frmEmployeeDataEntry)Sys.GetInstance(typeof(frmEmployeeDataEntry));
			}
		}

		protected frmEmployeeDataEntry _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 09,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		// Private dbDataEntry         As DAO.Database
		private clsDRWrapper rsDataEntry = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(DialogResult, int)
		private DialogResult intDataChanged;
		private bool boolNew;

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the frequency code report
				// rptDataEntry.Show
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				for (intCounter = 1; intCounter <= (vsDataEntry.Rows - 1); intCounter++)
				{
					rsDataEntry.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 6) + "'", "TWPY0000.vb1");
					if (!rsDataEntry.EndOfFile())
					{
						rsDataEntry.Edit();
						// If Right(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 2) = ".." Then
						// boolNew = False
						// rsDataEntry.Edit
						// ElseIf Right(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0), 1) = "." Then
						// boolNew = True
						// rsDataEntry.AddNew
						// Else
						// GoTo NextRecord
						// End If
						if (FCConvert.CBool(vsDataEntry.TextMatrix(intCounter, 5)))
						{
							rsDataEntry.Set_Fields("DataEntry", true);
						}
						else
						{
							rsDataEntry.Set_Fields("DataEntry", false);
						}
						// rsDataEntry.SetData "DataEntry", vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 5)
						rsDataEntry.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
						rsDataEntry.Update();
					}
					// clear the edit symbol from the row number
					if (Strings.Right(vsDataEntry.TextMatrix(intCounter, 0), 2) == "..")
					{
						vsDataEntry.TextMatrix(intCounter, 0, Strings.Mid(vsDataEntry.TextMatrix(intCounter, 0), 1, vsDataEntry.TextMatrix(intCounter, 0).Length - 2));
					}
					else if (Strings.Left(vsDataEntry.TextMatrix(intCounter, 0), 1) == ".")
					{
						vsDataEntry.TextMatrix(intCounter, 0, Strings.Mid(vsDataEntry.TextMatrix(intCounter, 0), 1, vsDataEntry.TextMatrix(intCounter, 0).Length - 1));
					}
					NextRecord:
					;
				}
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				string strEmp;
				string strEmpFirstName;
				string strEmpLastName;
				strEmp = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				strEmpFirstName = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName;
				strEmpLastName = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = string.Empty;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = string.Empty;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = string.Empty;
				// gtypeCurrentEmployee.EmployeeMiddleName = ""
				// gtypeCurrentEmployee.EmployeeDesig = ""
				clsHistoryClass.Compare();
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = strEmp;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = strEmpFirstName;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = strEmpLastName;
				return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Data Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmEmployeeDataEntry_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployeeDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmployeeDataEntry properties;
			//frmEmployeeDataEntry.ScaleWidth	= 9045;
			//frmEmployeeDataEntry.ScaleHeight	= 7245;
			//frmEmployeeDataEntry.LinkTopic	= "Form1";
			//frmEmployeeDataEntry.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// open the forms global database connection
				// Set dbDataEntry = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsDataEntry.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// Load the grid with data
				LoadGrid();
				vsDataEntry.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortAndMove;
				clsHistoryClass.SetGridIDColumn("PY", "vsDataEntry", 0);
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				vsDataEntry.Rows = 1;
				rsDataEntry.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", "TWPY0000.vb1");
				if (!rsDataEntry.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsDataEntry.MoveLast();
					rsDataEntry.MoveFirst();
					// set the number of rows in the grid
					vsDataEntry.Rows = rsDataEntry.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsDataEntry.RecordCount()); intCounter++)
					{
						vsDataEntry.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						// vsDataEntry.TextMatrix(intCounter, 1) = Format(Trim(rsDataEntry.Fields("EmployeeNumber") & " "), "00000")
						vsDataEntry.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(rsDataEntry.Get_Fields("EmployeeNumber") + " "));
						vsDataEntry.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsDataEntry.Get_Fields_String("LastName") + " ") + ", " + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsDataEntry.Get_Fields_String("FirstName") + " ") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDataEntry.Get_Fields("MIDDLENAME")))) + " " + fecherFoundation.Strings.Trim(rsDataEntry.Get_Fields_String("Desig") + " "));
						// Add when tblEmployeeMaster is completed
						vsDataEntry.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDataEntry.Get_Fields("DeptDiv"))));
						vsDataEntry.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDataEntry.Get_Fields("SeqNumber"))));
						vsDataEntry.TextMatrix(intCounter, 5, FCConvert.ToString(rsDataEntry.Get_Fields("DataEntry")));
						vsDataEntry.TextMatrix(intCounter, 6, FCConvert.ToString(rsDataEntry.Get_Fields("EmployeeNumber")));
						vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, intCounter, 4, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						rsDataEntry.MoveNext();
					}
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsDataEntry.Cols = 7;
				vsDataEntry.ColHidden(6, true);
				vsDataEntry.ColHidden(0, true);
				// vsDataEntry.FixedCols = 5
				modGlobalRoutines.CenterGridCaptions(ref vsDataEntry);
				// set column 0 properties
				vsDataEntry.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(0, 350);
				// set column 1 properties
				vsDataEntry.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(1, 1400);
				vsDataEntry.TextMatrix(0, 1, "Employee #");
				// set column 2 properties
				vsDataEntry.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(2, 3800);
				vsDataEntry.TextMatrix(0, 2, "Full Name");
				// set column 3 properties
				vsDataEntry.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(3, 1400);
				vsDataEntry.TextMatrix(0, 3, "Dept/Div");
				// set column 4 properties
				vsDataEntry.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(4, 1400);
				vsDataEntry.TextMatrix(0, 4, "Sequence #");
				// set column 5 properties
				vsDataEntry.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDataEntry.ColWidth(5, 800);
				vsDataEntry.TextMatrix(0, 5, "Include");
				vsDataEntry.ColDataType(5, FCGrid.DataTypeSettings.flexDTBoolean);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeDataEntry_Resize(object sender, System.EventArgs e)
		{
			vsDataEntry.ColWidth(0, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.04));
			vsDataEntry.ColWidth(1, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.14));
			vsDataEntry.ColWidth(2, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.35));
			vsDataEntry.ColWidth(3, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.14));
			vsDataEntry.ColWidth(4, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.14));
			vsDataEntry.ColWidth(5, FCConvert.ToInt32(vsDataEntry.WidthOriginal * 0.08));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (intDataChanged == (DialogResult)2)
				{
					e.Cancel = true;
					return;
				}
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsDataEntry_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDataEntry_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDataEntry.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsDataEntry.TextMatrix(vsDataEntry.Row, 0, vsDataEntry.TextMatrix(vsDataEntry.Row, 0) + "..");
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDataEntry_ClickEvent(object sender, System.EventArgs e)
		{
			// If vsDataEntry.Col = 2 Then
			// vsDataEntry.Sort = flexSortStringAscending
			// End If
		}

		private void SortData(int Col)
		{
		}

		private void vsDataEntry_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDataEntry_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsDataEntry.Col == 5)
					{
						if (vsDataEntry.Row < vsDataEntry.Rows - 1)
						{
							vsDataEntry.Row += 1;
							vsDataEntry.Col = 5;
						}
						else
						{
							// if there is no other row then create a new one
							vsDataEntry.Select(1, 5);
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsDataEntry_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDataEntry[e.ColumnIndex, e.RowIndex];
            if (vsDataEntry.GetFlexRowIndex(e.RowIndex) == 0)
			{
				//ToolTip1.SetToolTip(vsDataEntry, "Click On Heading To Sort");
				cell.ToolTipText = "Click On Heading To Sort";
			}
		}

		private void vsDataEntry_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDataEntry_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// If vsDataEntry.Col = 5 Then
				// vsDataEntry.Select vsDataEntry.Row, vsDataEntry.Col
				// vsDataEntry.Editable = True
				// Else
				// vsDataEntry.Editable = False
				// End If
				vsDataEntry.Editable = FCConvert.ToInt32(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDataEntry.Row, vsDataEntry.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND ? FCGrid.EditableSettings.flexEDKbdMouse : FCGrid.EditableSettings.flexEDNone;
				// select all of the data in the new cell
				// vsDataEntry.EditCell
				// vsDataEntry.EditSelStart = 0
				// vsDataEntry.EditSelLength = Len(vsDataEntry.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDataEntry.Row, vsDataEntry.Col))
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
	}
}
