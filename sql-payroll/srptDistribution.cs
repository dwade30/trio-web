//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDistribution.
	/// </summary>
	public partial class srptDistribution : FCSectionReport
	{
		public srptDistribution()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptDistribution InstancePtr
		{
			get
			{
				return (srptDistribution)Sys.GetInstance(typeof(srptDistribution));
			}
		}

		protected srptDistribution _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
				clsTaxCode?.Dispose();
                clsLoad = null;
                clsTaxCode = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptDistribution	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		clsDRWrapper clsTaxCode = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strEmp;
			string strSQL;
			strEmp = FCConvert.ToString(this.UserData);
			strSQL = "Select * from tblpayrolldistribution where employeenumber = '" + strEmp + "' order by RECORDNUMBER";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (clsLoad.EndOfFile())
			{
				//this.Visible = false;
				this.Close();
				return;
			}
			strSQL = "Select * from tbltaxstatuscodes order by ID";
			clsTaxCode.OpenRecordset(strSQL, "twpy0000.vb1");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				if (clsLoad.Get_Fields_Int32("accountcode") == 1)
				{
					txtCode.Text = "N";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 2)
				{
					txtCode.Text = "W";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 3)
				{
					txtCode.Text = "1";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 4)
				{
					txtCode.Text = "S1";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 5)
				{
					txtCode.Text = "S2";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 6)
				{
					txtCode.Text = "S3";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 7)
				{
					txtCode.Text = "S4";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 8)
				{
					txtCode.Text = "S5";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 9)
				{
					txtCode.Text = "S6";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 10)
				{
					txtCode.Text = "S7";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 11)
				{
					txtCode.Text = "S8";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 12)
				{
					txtCode.Text = "S9";
				}
				else if (clsLoad.Get_Fields_Int32("accountcode") == 13)
				{
					txtCode.Text = "5";
				}
				else
				{
					txtCode.Text = "";
				}
				txtAccount.Text = clsLoad.Get_Fields("accountnumber");
				// matthew tax code 12/12/03
				// If clsTaxCode.FindFirstRecord("ID", Val(.Fields("taxcode"))) Then
				if (clsTaxCode.FindFirstRecord("ID", modGlobalRoutines.GetNewTaxCode(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("Cat"))))))
				{
					txtTax.Text = clsTaxCode.Get_Fields("taxstatuscode");
				}
				else
				{
					txtTax.Text = "";
				}
				txtM.Text = FCConvert.ToString(clsLoad.Get_Fields("MSRS"));
				txtCat.Text = FCConvert.ToString(clsLoad.Get_Fields("cat"));
				txtWC.Text = clsLoad.Get_Fields("workcomp");
				txtU.Text = clsLoad.Get_Fields("distu");
				txtCD.Text = clsLoad.Get_Fields("cd");
				txtBaseRate.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("baserate")), "#0.0000");
				txtMult.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("factor")), "#0.00");
				txtDefHours.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("defaulthours")), "#0.00");
				clsLoad.MoveNext();
			}
		}

	
	}
}
