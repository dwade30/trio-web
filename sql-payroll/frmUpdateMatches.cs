﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmUpdateMatches : BaseForm
	{
		public frmUpdateMatches()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUpdateMatches InstancePtr
		{
			get
			{
				return (frmUpdateMatches)Sys.GetInstance(typeof(frmUpdateMatches));
			}
		}

		protected frmUpdateMatches _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cUpdateMatchesView theView;
		private bool boolRefreshing;
		private cBDAccountController acctCont = new cBDAccountController();
		const int CNSTColEmployeeNumber = 0;
		const int CNSTColEmployeeName = 1;

		private void FillAccountCombo()
		{
			boolRefreshing = true;
			cmbAccountType.Clear();
			cmbAccountType.AddItem("");
			cmbAccountType.AddItem("E");
			cmbAccountType.ItemData(1, 1);
			cmbAccountType.AddItem("R");
			cmbAccountType.ItemData(2, 2);
			cmbAccountType.AddItem("G");
			cmbAccountType.ItemData(3, 3);
			boolRefreshing = false;
		}

		private void cmbAccountType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				theView.AccountTypeToShow = cmbAccountType.ItemData(cmbAccountType.SelectedIndex);
				boolRefreshing = false;
			}
		}

		private void cmbCodeOne_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbCodeOne.SelectedIndex;
				theView.CurrentCodeOneIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmbCodeTwo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbCodeTwo.SelectedIndex;
				theView.CurrentCodeTwoIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmbDepartment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbDepartment.SelectedIndex;
				theView.CurrentDepartmentIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmbFullTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				theView.CurrentFullTimePartTime = (cmbFullTime.SelectedIndex + 1);
				boolRefreshing = false;
			}
		}

		private void cmbGroupIDs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				theView.CurrentGroupIDIndex = (cmbGroupIDs.SelectedIndex + 1);
				boolRefreshing = false;
			}
		}

		private void cmbMatchType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				int lngID = 0;
				intindex = cmbMatchType.SelectedIndex;
				if (intindex >= 0)
				{
					lngID = cmbMatchType.ItemData(intindex);
					theView.SetCurrentMatch(lngID);
				}
				boolRefreshing = false;
			}
		}

		private void cmbSequence_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				int lngID;
				intindex = cmbSequence.SelectedIndex;
				theView.CurrentSequenceIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmdUpdate_Click(object sender, System.EventArgs e)
		{
			UpdateMatches();
		}

		private void frmUpdateMatches_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUpdateMatches_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdateMatches properties;
			//frmUpdateMatches.FillStyle	= 0;
			//frmUpdateMatches.ScaleWidth	= 9300;
			//frmUpdateMatches.ScaleHeight	= 7815;
			//frmUpdateMatches.LinkTopic	= "Form2";
			//frmUpdateMatches.LockControls	= -1  'True;
			//frmUpdateMatches.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			theView = new cUpdateMatchesView();
			//FC:FINAL:DSE:#i2261 Add object events
			theView.AccountTypeChanged += theView_AccountTypeChanged;
			theView.AmountTypesChanged += theView_AmountTypesChanged;
			theView.CodeOnesChanged += theView_CodeOnesChanged;
			theView.CodeTwosChanged += theView_CodeTwosChanged;
			theView.CurrentMatchChanged += theView_CurrentMatchChanged;
			theView.DepartmentsChanged += theView_DepartmentsChanged;
			theView.FilteredEmployeesChanged += theView_FilteredEmployeesChanged;
			theView.FrequenciesChanged += theView_FrequenciesChanged;
			theView.FullTimesChanged += theView_FullTimesChanged;
			theView.GroupIDsChanged += theView_GroupIDsChanged;
			theView.MatchesChanged += theView_MatchesChanged;
			theView.MaxTypesChanged += theView_MaxTypesChanged;
			theView.SequencesChanged += theView_SequencesChanged;
			theView.StatusesChanged += theView_StatusesChanged;
			GridEmployees.TextMatrix(0, CNSTColEmployeeName, "Employee Name");
			FillAccountCombo();
			theView.InitializeValues(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
		}

		private void ShowAmountTypes()
		{
			cGenericCollection listAmountTypes;
			string strTemp = "";
			listAmountTypes = theView.AmountTypes;
			cmbAmountType.Clear();
			if (!(listAmountTypes == null))
			{
				listAmountTypes.MoveFirst();
				cCodeDescription cd;
				while (listAmountTypes.IsCurrent())
				{
					cd = (cCodeDescription)listAmountTypes.GetCurrentItem();
					cmbAmountType.AddItem(cd.Description);
					cmbAmountType.ItemData(cmbAmountType.NewIndex, cd.ID);
					listAmountTypes.MoveNext();
				}
			}
		}

		private void ShowMaxTypes()
		{
			cGenericCollection listMaxTypes;
			string strTemp = "";
			listMaxTypes = theView.MaxTypes;
			cmbMaxType.Clear();
			if (!(listMaxTypes == null))
			{
				listMaxTypes.MoveFirst();
				cCodeDescription cd;
				while (listMaxTypes.IsCurrent())
				{
					cd = (cCodeDescription)listMaxTypes.GetCurrentItem();
					cmbMaxType.AddItem(cd.Description);
					cmbMaxType.ItemData(cmbMaxType.NewIndex, cd.ID);
					listMaxTypes.MoveNext();
				}
			}
		}

		private void ShowFrequencies()
		{
			cGenericCollection listFrequencies;
			cCodeDescription cd;
			listFrequencies = theView.Frequencies;
			cmbFrequency.Clear();
			if (!(listFrequencies == null))
			{
				listFrequencies.MoveFirst();
				while (listFrequencies.IsCurrent())
				{
					cd = (cCodeDescription)listFrequencies.GetCurrentItem();
					cmbFrequency.AddItem(cd.Description);
					cmbFrequency.ItemData(cmbFrequency.NewIndex, cd.ID);
					listFrequencies.MoveNext();
				}
			}
		}

		private void ShowStatuses()
		{
			cGenericCollection listStatuses;
			string strTemp = "";
			listStatuses = theView.Statuses;
			cmbStatus.Clear();
			if (!(listStatuses == null))
			{
				listStatuses.MoveFirst();
				while (listStatuses.IsCurrent())
				{
					strTemp = FCConvert.ToString(listStatuses.GetCurrentItem());
					cmbStatus.AddItem(strTemp);
					listStatuses.MoveNext();
				}
			}
		}

		private void ShowMatches()
		{
			cGenericCollection listMatches;
			// vbPorter upgrade warning: strTemp As string	OnWrite(int, string)
			string strTemp = "";
			boolRefreshing = true;
			listMatches = theView.Matches;
			cmbMatchType.Clear();
			if (!(listMatches == null))
			{
				listMatches.MoveFirst();
				cDeductionSetup empMatch;
				while (listMatches.IsCurrent())
				{
					empMatch = (cDeductionSetup)listMatches.GetCurrentItem();
					strTemp = FCConvert.ToString(empMatch.DeductionNumber);
					if (strTemp.Length < 5)
					{
						strTemp = Strings.Left(strTemp + "    ", 4);
					}
					cmbMatchType.AddItem(strTemp + "  " + empMatch.Description);
					cmbMatchType.ItemData(cmbMatchType.NewIndex, empMatch.ID);
					listMatches.MoveNext();
				}
			}
			boolRefreshing = false;
		}

		private void ShowDepartments()
		{
			cGenericCollection listDepts;
			boolRefreshing = true;
			listDepts = theView.Departments;
			cmbDepartment.Clear();
			if (!(listDepts == null))
			{
				listDepts.MoveFirst();
				string strTemp = "";
				while (listDepts.IsCurrent())
				{
					strTemp = FCConvert.ToString(listDepts.GetCurrentItem());
					cmbDepartment.AddItem(strTemp);
					listDepts.MoveNext();
				}
			}
			boolRefreshing = false;
		}

		private void ShowCodeOnes()
		{
			cGenericCollection listCodeOnes;
			boolRefreshing = true;
			listCodeOnes = theView.CodeOnes;
			listCodeOnes.MoveFirst();
			string strTemp = "";
			while (listCodeOnes.IsCurrent())
			{
				strTemp = FCConvert.ToString(listCodeOnes.GetCurrentItem());
				cmbCodeOne.AddItem(strTemp);
				listCodeOnes.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowCodeTwos()
		{
			cGenericCollection listCodeTwos;
			boolRefreshing = true;
			listCodeTwos = theView.CodeTwos;
			listCodeTwos.MoveFirst();
			string strTemp = "";
			while (listCodeTwos.IsCurrent())
			{
				strTemp = FCConvert.ToString(listCodeTwos.GetCurrentItem());
				cmbCodeTwo.AddItem(strTemp);
				listCodeTwos.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowSequences()
		{
			cGenericCollection listSequences;
			boolRefreshing = true;
			listSequences = theView.Sequences;
			listSequences.MoveFirst();
			string strTemp = "";
			while (listSequences.IsCurrent())
			{
				strTemp = FCConvert.ToString(listSequences.GetCurrentItem());
				cmbSequence.AddItem(strTemp);
				listSequences.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowGroups()
		{
			cGenericCollection listGroups;
			boolRefreshing = true;
			listGroups = theView.Groups;
			listGroups.MoveFirst();
			string strTemp = "";
			while (listGroups.IsCurrent())
			{
				strTemp = FCConvert.ToString(listGroups.GetCurrentItem());
				cmbGroupIDs.AddItem(strTemp);
				listGroups.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowPartFullTimes()
		{
			cGenericCollection listpftimes;
			boolRefreshing = true;
			listpftimes = theView.FullTimes;
			listpftimes.MoveFirst();
			string strTemp = "";
			while (listpftimes.IsCurrent())
			{
				strTemp = FCConvert.ToString(listpftimes.GetCurrentItem());
				cmbFullTime.AddItem(strTemp);
				listpftimes.MoveNext();
			}
			boolRefreshing = false;
		}

		private void frmUpdateMatches_Resize(object sender, System.EventArgs e)
		{
			ResizeGridEmployees();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (UpdateMatches())
			{
				Close();
			}
		}

		private void theView_AccountTypeChanged()
		{
			boolRefreshing = true;
			if (cmbAccountType.SelectedIndex != theView.AccountTypeToShow)
			{
				cmbAccountType.SelectedIndex = theView.AccountTypeToShow;
			}
			int x;
			txtSegment1.Text = "";
			txtSegment2.Text = "";
			txtSegment3.Text = "";
			txtSegment4.Text = "";
			int intLength;
			intLength = theView.GetSegmentLength(1);
			if (intLength > 0)
			{
				txtSegment1.MaxLength = intLength;
				txtSegment1.Visible = true;
			}
			else
			{
				txtSegment1.Visible = false;
			}
			intLength = theView.GetSegmentLength(2);
			if (intLength > 0)
			{
				txtSegment2.MaxLength = intLength;
				txtSegment2.Visible = true;
			}
			else
			{
				txtSegment2.Visible = false;
			}
			intLength = theView.GetSegmentLength(3);
			if (intLength > 0)
			{
				txtSegment3.MaxLength = intLength;
				txtSegment3.Visible = true;
			}
			else
			{
				txtSegment3.Visible = false;
			}
			intLength = theView.GetSegmentLength(4);
			if (intLength > 0)
			{
				txtSegment4.MaxLength = intLength;
				txtSegment4.Visible = true;
			}
			else
			{
				txtSegment4.Visible = false;
			}
			boolRefreshing = false;
		}

		private void theView_AmountTypesChanged()
		{
			ShowAmountTypes();
		}

		private void theView_CodeOnesChanged()
		{
			ShowCodeOnes();
		}

		private void theView_CodeTwosChanged()
		{
			ShowCodeTwos();
		}

		private void theView_CurrentMatchChanged()
		{
			int lngID;
			int lngOldID = 0;
			lngID = theView.CurrentMatch;
			if (cmbMatchType.SelectedIndex >= 0)
			{
				lngOldID = cmbMatchType.ItemData(cmbMatchType.SelectedIndex);
			}
			else
			{
				lngOldID = 0;
			}
			if (lngOldID != lngID)
			{
				boolRefreshing = true;
				if (lngID > 0)
				{
					int x;
					for (x = 0; x <= cmbMatchType.Items.Count - 1; x++)
					{
						if (cmbMatchType.ItemData(x) == lngID)
						{
							cmbMatchType.SelectedIndex = x;
							break;
						}
					}
					// x
				}
				else
				{
					cmbMatchType.SelectedIndex = -1;
				}
				boolRefreshing = false;
			}
		}

		private void theView_DepartmentsChanged()
		{
			ShowDepartments();
		}

		private void theView_FilteredEmployeesChanged()
		{
			ShowEmployees();
		}

		private void theView_FrequenciesChanged()
		{
			ShowFrequencies();
		}

		private void theView_FullTimesChanged()
		{
			ShowPartFullTimes();
		}

		private void theView_GroupIDsChanged()
		{
			ShowGroups();
		}

		private void theView_MatchesChanged()
		{
			ShowMatches();
		}

		private void theView_MaxTypesChanged()
		{
			ShowMaxTypes();
		}

		private void theView_SequencesChanged()
		{
			ShowSequences();
		}

		private void theView_StatusesChanged()
		{
			ShowStatuses();
		}

		private bool UpdateMatches()
		{
			bool UpdateMatches = false;
			bool boolUseAmount = false;
			bool boolUseMaxAmount = false;
			double dblAmount = 0;
			string strAmountType = "";
			double dblMaxAmount = 0;
			int lngMaxType = 0;
			bool boolUseStatus = false;
			string strStatus = "";
			bool boolUseFrequency = false;
			int lngFrequency = 0;
			string strAccount = "";
			string strTemp = "";
			bool boolUseAccount = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(txtAmount.Text) != "")
				{
					if (!Information.IsNumeric(txtAmount.Text))
					{
						MessageBox.Show("Amount must be blank or a valid number", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateMatches;
					}
					if (cmbAmountType.SelectedIndex >= 0)
					{
						// percentage
						if (Conversion.Val(txtAmount.Text) > 100)
						{
							MessageBox.Show("The amount type is set to a percentage but the amount specified is greater than 100", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return UpdateMatches;
						}
					}
					else
					{
						MessageBox.Show("You must select the type the amount is specifying", "Invalid Amount Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateMatches;
					}
					boolUseAmount = true;
					dblAmount = FCConvert.ToDouble(txtAmount.Text);
					switch (cmbAmountType.SelectedIndex)
					{
						case 0:
							{
								strAmountType = "D";
								break;
							}
						case 1:
							{
								strAmountType = "%G";
								break;
							}
						case 2:
							{
								strAmountType = "%D";
								break;
							}
					}
					//end switch
				}
				if (fecherFoundation.Strings.Trim(txtMaxAmount.Text) != "")
				{
					if (!Information.IsNumeric(txtMaxAmount.Text))
					{
						MessageBox.Show("Max amount must be blank or a valid number", "Invalid Maximum", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateMatches;
					}
					if (cmbMaxType.SelectedIndex < 0)
					{
						MessageBox.Show("You must select the type the max amount is specifying", "Invalid Max Amount Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateMatches;
					}
					boolUseMaxAmount = true;
					dblMaxAmount = FCConvert.ToDouble(txtMaxAmount.Text);
					lngMaxType = cmbMaxType.ItemData(cmbMaxType.SelectedIndex);
				}
				if (cmbStatus.SelectedIndex > 0)
				{
					boolUseStatus = true;
					strStatus = cmbStatus.Text;
				}
				if (cmbFrequency.SelectedIndex > 0)
				{
					boolUseFrequency = true;
					lngFrequency = cmbFrequency.ItemData(cmbFrequency.SelectedIndex);
				}
				bool boolInvalidAccount;
				boolInvalidAccount = false;
				if (cmbAccountType.SelectedIndex > 0)
				{
					switch (cmbAccountType.ItemData(cmbAccountType.SelectedIndex))
					{
						case 1:
							{
								// Expense
								strAccount = "E ";
								break;
							}
						case 2:
							{
								// Revenue
								strAccount = "R ";
								break;
							}
						case 3:
							{
								// Ledger
								strAccount = "G ";
								break;
							}
					}
					//end switch
					if (txtSegment1.Visible)
					{
						strTemp = fecherFoundation.Strings.Trim(txtSegment1.Text);
						if (strTemp != "")
						{
							strAccount += strTemp;
						}
						else
						{
							boolInvalidAccount = true;
						}
					}
					if (txtSegment2.Visible)
					{
						strTemp = fecherFoundation.Strings.Trim(txtSegment2.Text);
						if (strTemp != "")
						{
							strAccount += "-" + strTemp;
						}
						else
						{
							boolInvalidAccount = true;
						}
					}
					if (txtSegment3.Visible)
					{
						strTemp = fecherFoundation.Strings.Trim(txtSegment3.Text);
						if (strTemp != "")
						{
							strAccount += "-" + strTemp;
						}
						else
						{
							boolInvalidAccount = true;
						}
					}
					if (txtSegment4.Visible)
					{
						strTemp = fecherFoundation.Strings.Trim(txtSegment4.Text);
						if (strTemp != "")
						{
							strAccount += "-" + strTemp;
						}
						else
						{
							boolInvalidAccount = true;
						}
					}
					cBDAccount Acct;
					if (!boolInvalidAccount)
					{
						boolInvalidAccount = !modValidateAccount.AccountValidate(strAccount);
						// Set acct = acctCont.GetAccountByAccount(strAccount)
						// If Not acct Is Nothing Then
						// If Not acct.IsValidAccount Then
						// boolInvalidAccount = True
						// End If
						// Else
						// boolInvalidAccount = True
						// End If
					}
					if (boolInvalidAccount)
					{
						MessageBox.Show("Account is invalid", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateMatches;
					}
					boolUseAccount = true;
				}
				if (!boolUseAmount && !boolUseMaxAmount && !boolUseStatus && !boolUseFrequency && !boolUseAccount)
				{
					MessageBox.Show("You didn't specify anything to change", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return UpdateMatches;
				}
				theView.UpdateMatches(boolUseAmount, dblAmount, strAmountType, boolUseMaxAmount, dblMaxAmount, lngMaxType, boolUseStatus, strStatus, boolUseFrequency, lngFrequency, boolUseAccount, strAccount);
				UpdateMatches = true;
				MessageBox.Show("Update Successful", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return UpdateMatches;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateMatches;
		}

		private void txtAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			//FC:FINAL:DDU:#i2470 - fix dot conversion problem
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Delete))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMaxAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			//FC:FINAL:DDU:#i2470 - fix dot conversion problem
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Delete))
			{
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void ShowEmployees()
		{
			GridEmployees.Rows = 1;
			if (theView.FilteredEmployees.ItemCount() > 0)
			{
				theView.FilteredEmployees.MoveFirst();
				cEmployee emp;
				int lngRow;
				while (theView.FilteredEmployees.IsCurrent())
				{
					emp = (cEmployee)theView.FilteredEmployees.GetCurrentItem();
					GridEmployees.Rows += 1;
					lngRow = GridEmployees.Rows - 1;
					GridEmployees.TextMatrix(lngRow, CNSTColEmployeeNumber, emp.EmployeeNumber);
					GridEmployees.TextMatrix(lngRow, CNSTColEmployeeName, emp.FullName);
					theView.FilteredEmployees.MoveNext();
				}
			}
		}

		private void ResizeGridEmployees()
		{
			int lngGridWidth;
			lngGridWidth = GridEmployees.WidthOriginal;
			GridEmployees.ColWidth(CNSTColEmployeeNumber, FCConvert.ToInt32(0.2 * lngGridWidth));
		}

		private void RefreshAccountBoxes()
		{
			boolRefreshing = true;
			string strTemp = "";
			int intLen;
			txtSegment1.Text = "";
			txtSegment2.Text = "";
			txtSegment3.Text = "";
			txtSegment4.Text = "";
			intLen = theView.GetSegmentLength(1);
			if (intLen == 0)
			{
				txtSegment1.Visible = false;
			}
			else
			{
				txtSegment1.Visible = true;
				txtSegment1.MaxLength = intLen;
				// txtSegment1.Text = theView.GetSegmentFilter(1)
			}
			intLen = theView.GetSegmentLength(2);
			if (intLen == 0)
			{
				txtSegment2.Visible = false;
			}
			else
			{
				txtSegment2.Visible = true;
				txtSegment2.MaxLength = intLen;
				// txtSegment2.Text = theView.GetSegmentFilter(2)
			}
			intLen = theView.GetSegmentLength(3);
			if (intLen == 0)
			{
				txtSegment3.Visible = false;
			}
			else
			{
				txtSegment3.Visible = true;
				txtSegment3.MaxLength = intLen;
				// txtSegment3.Text = theView.GetSegmentFilter(3)
			}
			intLen = theView.GetSegmentLength(4);
			if (intLen == 0)
			{
				txtSegment4.Visible = false;
			}
			else
			{
				txtSegment4.Visible = true;
				txtSegment4.MaxLength = intLen;
				// txtSegment4.Text = theView.GetSegmentFilter(4)
			}
			boolRefreshing = false;
		}

		private void txtSegment1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			else if (KeyAscii >= (Keys)Convert.ToByte("0") && KeyAscii <= (Keys)Convert.ToByte("9"))
			{
				if (txtSegment1.Text.Length + 1 >= txtSegment1.MaxLength)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			else if (KeyAscii >= (Keys)Convert.ToByte("0") && KeyAscii <= (Keys)Convert.ToByte("9"))
			{
				if (txtSegment2.Text.Length + 1 >= txtSegment2.MaxLength)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSegment3_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			else if (KeyAscii >= (Keys)Convert.ToByte("0") && KeyAscii <= (Keys)Convert.ToByte("9"))
			{
				if (txtSegment3.Text.Length + 1 >= txtSegment3.MaxLength)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
