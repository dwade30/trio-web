//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2Report.
	/// </summary>
	partial class frmW2Report
	{
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCLabel lblPercDone;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuCopy;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ProgressBar1 = new fecherFoundation.FCProgressBar();
            this.lblPercDone = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopy = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 227);
            this.BottomPanel.Size = new System.Drawing.Size(309, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.ProgressBar1);
            this.ClientArea.Controls.Add(this.lblPercDone);
            this.ClientArea.Size = new System.Drawing.Size(309, 167);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(309, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(30, 74);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(251, 24);
            this.ProgressBar1.TabIndex = 0;
            // 
            // lblPercDone
            // 
            this.lblPercDone.Location = new System.Drawing.Point(30, 30);
            this.lblPercDone.Name = "lblPercDone";
            this.lblPercDone.Size = new System.Drawing.Size(251, 24);
            this.lblPercDone.TabIndex = 1;
            this.lblPercDone.Text = "0% DONE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuCopy,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuCopy
            // 
            this.mnuCopy.Enabled = false;
            this.mnuCopy.Index = 0;
            this.mnuCopy.Name = "mnuCopy";
            this.mnuCopy.Text = "Copy to Disk";
            this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmW2Report
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(309, 335);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmW2Report";
            this.Text = "W2 Report";
            this.Load += new System.EventHandler(this.frmW2Report_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2Report_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
        
	}
}