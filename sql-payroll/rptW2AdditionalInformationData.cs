//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2AdditionalInfoData.
	/// </summary>
	public partial class rptW2AdditionalInfoData : BaseSectionReport
	{
		public rptW2AdditionalInfoData()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Additional W2 Information";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2AdditionalInfoData InstancePtr
		{
			get
			{
				return (rptW2AdditionalInfoData)Sys.GetInstance(typeof(rptW2AdditionalInfoData));
			}
		}

		protected rptW2AdditionalInfoData _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployee?.Dispose();
				rsData?.Dispose();
				rsCode?.Dispose();
                rsEmployee = null;
                rsData = null;
                rsCode = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2AdditionalInfoData	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       November 18, 2004
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCode = new clsDRWrapper();
		clsDRWrapper rsEmployee = new clsDRWrapper();
		string strSortField = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				NextCode:
				;
				if (rsCode.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					rsCode.MoveNext();
					if (rsCode.EndOfFile())
					{
						eArgs.EOF = true;
					}
					else
					{
                        //FC:FINAL:AM:#i2200 - have to use a field
                        //this.GroupHeader1.DataField = rsCode.Get_Fields("ID");
                        this.Fields["Binder"].Value = rsCode.Get_Fields("ID");
                        rsData.OpenRecordset("Select * from tblW2AdditionalInfoData Where AdditionalDeductionID = " + FCConvert.ToString(rsCode.Get_Fields("ID") + 9000), "TWPY0000.vb1");
						if (rsData.EndOfFile())
						{
							goto NextCode;
						}
						else
						{
							txtNumber.Text = Strings.Format(rsData.Get_Fields("EmployeeNumber"), "0000");
							txtAmount.Text = Strings.Format(rsData.Get_Fields("Amount"), "0.00");
							if (rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
							{
								txtName.Text = fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("LastName") + " " + rsEmployee.Get_Fields("desig")) + ", " + rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("MiddleName");
							}
							else
							{
								txtName.Text = string.Empty;
							}
							if (!rsData.EndOfFile())
								rsData.MoveNext();
							intCounter += 1;
							eArgs.EOF = false;
						}
					}
				}
			}
			else
			{
				txtNumber.Text = Strings.Format(rsData.Get_Fields("EmployeeNumber"), "0000");
				txtAmount.Text = Strings.Format(rsData.Get_Fields("Amount"), "0.00");
				if (rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
				{
					txtName.Text = fecherFoundation.Strings.Trim(rsEmployee.Get_Fields_String("LastName") + " " + rsEmployee.Get_Fields("desig")) + ", " + rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("MiddleName");
				}
				else
				{
					txtName.Text = string.Empty;
				}
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
        {
			try
			{
                this.Fields.Add("Binder");
                // On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsCode.OpenRecordset("Select * from tblW2AdditionalInfo", "TWPY0000.vb1");
				rsData.OpenRecordset("Select * from tblW2AdditionalInfoData", "TWPY0000.vb1");
				rsEmployee.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
				if (!rsCode.EndOfFile())
				{
					rsData.OpenRecordset("Select * from tblW2AdditionalInfoData Where AdditionalDeductionID = " + FCConvert.ToString(rsCode.Get_Fields("ID") + 9000), "TWPY0000.vb1");
                    //FC:FINAL:AM:#i2200 - have to use a field
                    //this.GroupHeader1.DataField = rsCode.Get_Fields("ID");
                    this.Fields["Binder"].Value = rsCode.Get_Fields("ID");
                }
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (rsCode.EndOfFile())
			{
			}
			else
			{
				txtCode.Text = rsCode.Get_Fields_String("Code");
				txtDescription.Text = rsCode.Get_Fields_String("Description");
				chkBox12.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("Box12")) ? 1 : 0);
				chkBox14.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("Box14")) ? 1 : 0);
				chkFederal.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields("Federal")) ? 1 : 0);
				chkFICA.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields("FICA")) ? 1 : 0);
				chkState.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields("State")) ? 1 : 0);
				chkMedicare.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("Medicare")) ? 1 : 0);
				chkSickPay.Checked = FCConvert.ToBoolean(FCConvert.ToBoolean(rsCode.Get_Fields_Boolean("ThirdParty")) ? 1 : 0);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Additional W-2 Information";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
            txtTime.Text = DateTime.Now.ToShortTimeString();
		}

		
	}
}
