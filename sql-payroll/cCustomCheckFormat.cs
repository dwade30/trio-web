﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class cCustomCheckFormat
	{
		//=========================================================
		private int lngRecordID;
		private string strCheckType = string.Empty;
		private string strCheckPosition = string.Empty;
		private string strScannedCheckFile = string.Empty;
		private bool boolUseDoubleStub;
		private string strDescription = string.Empty;
		private bool boolUseTwoStubs;
		private string strVoidAfterMessage = string.Empty;
		private cGenericCollection colFields = new cGenericCollection();
		private bool boolShowDistributionRate;

		public bool ShowDistributionRate
		{
			set
			{
				boolShowDistributionRate = value;
			}
			get
			{
				bool ShowDistributionRate = false;
				ShowDistributionRate = boolShowDistributionRate;
				return ShowDistributionRate;
			}
		}

		public cGenericCollection CheckFields
		{
			get
			{
				cGenericCollection CheckFields = null;
				CheckFields = colFields;
				return CheckFields;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string CheckType
		{
			set
			{
				strCheckType = value;
			}
			get
			{
				string CheckType = "";
				CheckType = strCheckType;
				return CheckType;
			}
		}

		public string CheckPosition
		{
			set
			{
				strCheckPosition = value;
			}
			get
			{
				string CheckPosition = "";
				CheckPosition = strCheckPosition;
				return CheckPosition;
			}
		}

		public string ScannedCheckFile
		{
			set
			{
				strScannedCheckFile = value;
			}
			get
			{
				string ScannedCheckFile = "";
				ScannedCheckFile = strScannedCheckFile;
				return ScannedCheckFile;
			}
		}

		public bool UseDoubleStub
		{
			set
			{
				boolUseDoubleStub = value;
			}
			get
			{
				bool UseDoubleStub = false;
				UseDoubleStub = boolUseDoubleStub;
				return UseDoubleStub;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public bool UseTwoStubs
		{
			set
			{
				boolUseTwoStubs = value;
			}
			get
			{
				bool UseTwoStubs = false;
				UseTwoStubs = boolUseTwoStubs;
				return UseTwoStubs;
			}
		}

		public string VoidAfterMessage
		{
			set
			{
				strVoidAfterMessage = value;
			}
			get
			{
				string VoidAfterMessage = "";
				VoidAfterMessage = strVoidAfterMessage;
				return VoidAfterMessage;
			}
		}
	}
}
