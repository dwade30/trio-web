//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmC1Payments.
	/// </summary>
	partial class frmC1Payments
	{
		public fecherFoundation.FCCheckBox lblLastCheck;
		public FCGrid Grid;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuReload;
		public fecherFoundation.FCToolStripMenuItem mnuPayments;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.lblLastCheck = new fecherFoundation.FCCheckBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuReload = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cmdPayments = new fecherFoundation.FCButton();
            this.cmdReload = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 453);
            this.BottomPanel.Size = new System.Drawing.Size(748, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblLastCheck);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(748, 393);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdReload);
            this.TopPanel.Controls.Add(this.cmdPayments);
            this.TopPanel.Size = new System.Drawing.Size(748, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPayments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdReload, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(220, 30);
            this.HeaderText.Text = "Voucher Payments";
            // 
            // lblLastCheck
            // 
            this.lblLastCheck.Location = new System.Drawing.Point(30, 30);
            this.lblLastCheck.Name = "lblLastCheck";
            this.lblLastCheck.Size = new System.Drawing.Size(22, 26);
            this.lblLastCheck.TabIndex = 1;
            this.lblLastCheck.CheckedChanged += new System.EventHandler(this.lblLastCheck_CheckedChanged);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 60;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 2;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 76);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 17;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.Size = new System.Drawing.Size(690, 304);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 0;
            // 
            // mnuReload
            // 
            this.mnuReload.Index = -1;
            this.mnuReload.Name = "mnuReload";
            this.mnuReload.Text = "Reload and Recalculate Totals";
            this.mnuReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // mnuPayments
            // 
            this.mnuPayments.Index = -1;
            this.mnuPayments.Name = "mnuPayments";
            this.mnuPayments.Text = "Make Payments Same as Withholding";
            this.mnuPayments.Click += new System.EventHandler(this.mnuPayments_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = -1;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Save & Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(269, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(176, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Save & Continue";
            this.cmdProcess.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // cmdPayments
            // 
            this.cmdPayments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPayments.AppearanceKey = "toolbarButton";
            this.cmdPayments.Location = new System.Drawing.Point(470, 29);
            this.cmdPayments.Name = "cmdPayments";
            this.cmdPayments.Size = new System.Drawing.Size(250, 24);
            this.cmdPayments.TabIndex = 1;
            this.cmdPayments.Text = "Make Payments Same as Withholding";
            this.cmdPayments.Click += new System.EventHandler(this.mnuPayments_Click);
            // 
            // cmdReload
            // 
            this.cmdReload.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdReload.AppearanceKey = "toolbarButton";
            this.cmdReload.Location = new System.Drawing.Point(260, 29);
            this.cmdReload.Name = "cmdReload";
            this.cmdReload.Size = new System.Drawing.Size(204, 24);
            this.cmdReload.TabIndex = 2;
            this.cmdReload.Text = "Reload and Recalculate Totals";
            this.cmdReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // frmC1Payments
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(748, 561);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmC1Payments";
            this.Text = "Voucher Payments";
            this.Load += new System.EventHandler(this.frmC1Payments_Load);
            this.Resize += new System.EventHandler(this.frmC1Payments_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
		private FCButton cmdPayments;
		private FCButton cmdReload;
	}
}