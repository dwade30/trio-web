//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using Wisej.Web.Ext.CustomProperties;
using Wisej.Core;

namespace TWPY0000
{
	public partial class frmCustomDistributionReports : BaseForm
	{
		public frmCustomDistributionReports()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomDistributionReports InstancePtr
		{
			get
			{
				return (frmCustomDistributionReports)Sys.GetInstance(typeof(frmCustomDistributionReports));
			}
		}

		protected frmCustomDistributionReports _InstancePtr = null;
		//=========================================================
		private cPayrollReportService repServ = new cPayrollReportService();

		private void ChkAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstDistributions.Items.Count - 1; intCounter++)
			{
				lstDistributions.SetSelected(intCounter, 0 != (ChkAll.CheckState));
			}
			lstDistributions.SelectedIndex = 0;
		}

		private void chkShowDetails_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((cmbSort.Text == "Home Dept / Div" || cmbSort.Text == "Paid Dept / Div") && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by Pay Category";
			}
			else if (cmbSort.Text == "Pay Category" && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by dept/div";
			}
			else
			{
				chkSubgroup.Visible = false;
			}
		}

		private void frmCustomDistributionReports_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			//FC:FINAL:DSE:#i2220 Add handler for the key press 
			this.VSGrid.EditingControlShowing -= VSGrid_EditingControlShowing;
			this.VSGrid.EditingControlShowing += VSGrid_EditingControlShowing;
		}

		private void VSGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - Issue #745: in VB vs1_KeyDownEdit event is executed when key pressed in edit area
				e.Control.KeyDown -= VSGrid_KeyUpEdit;
				//e.Control.KeyPress -= vs1_KeyPressEdit;
				//e.Control.KeyUp -= vs1_KeyUpEdit;
				e.Control.KeyDown += VSGrid_KeyUpEdit;
				//e.Control.KeyPress += vs1_KeyPressEdit;
				//e.Control.KeyUp += vs1_KeyUpEdit;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (col == 0 || col == 1)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = col;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void frmCustomDistributionReports_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomDistributionReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomDistributionReports properties;
			//frmCustomDistributionReports.ScaleWidth	= 9300;
			//frmCustomDistributionReports.ScaleHeight	= 7830;
			//frmCustomDistributionReports.LinkTopic	= "Form1";
			//frmCustomDistributionReports.LockControls	= -1  'True;
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblPayCategories", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("CategoryNumber"))).Length > 6)
				{
					lstDistributions.AddItem(rsData.Get_Fields_Int32("CategoryNumber") + Strings.StrDup(10 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("CategoryNumber"))).Length, " ") + rsData.Get_Fields("Description"));
				}
				else
				{
					lstDistributions.AddItem(rsData.Get_Fields_Int32("CategoryNumber") + Strings.StrDup(7 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("CategoryNumber"))).Length, " ") + rsData.Get_Fields("Description"));
				}
				lstDistributions.ItemData(lstDistributions.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}
            // Call GetFormats

            lstDistributions.Columns[0].Width = (int)(lstDistributions.Width * 0.97);
            lstDistributions.GridLineStyle = GridLineStyle.None;
        }

		private void frmCustomDistributionReports_Resize(object sender, System.EventArgs e)
		{
			VSGrid.ColWidth(0, FCConvert.ToInt32(VSGrid.WidthOriginal *0.47));
			VSGrid.ColWidth(1, FCConvert.ToInt32(VSGrid.WidthOriginal *0.47));
			//VSGrid.RowHeight(0, (VSGrid.HeightOriginal) - 100);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbType.Text == "Distribution Setup Information")
			{
				cPYDistributionSetupReport distSetReport = new cPYDistributionSetupReport();
				if (ValidateSetupReport())
				{
					SetupSetupReport(ref distSetReport);
					if (cmbSort.Text == "Employee Name")
					{
						distSetReport.OrderByItem = 1;
					}
					else
					{
						distSetReport.OrderByItem = 0;
					}
					repServ.FillCustomDistributionSetupReport(ref distSetReport);
					rptCustomDistributionSetup.InstancePtr.Init(ref distSetReport);
				}
				return;
			}
			if (cmbType.Text == "Summary" && (cmbSort.Text == "Pay Category" || cmbSort.Text == "Home Dept / Div" || cmbSort.Text == "Paid Dept / Div"))
			{
				string strDist = "";
				string strEmpNo = "";
				// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(int, string)
				DateTime dtStartDate;
				// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(int, string)
				DateTime dtEndDate;
				string strAcctStart = "";
				string strAcctEnd = "";
				string strgroup = "";
				bool boolSummary = false;
				bool boolSubGroup = false;
				bool boolDetail = false;
				// strDist = GetDistributionNumbersForFilter
				// rptNewCustomDistReport.Init (optSort(2).Value)
				strDist = "";
				strEmpNo = "";
				dtStartDate = DateTime.FromOADate(0);
				dtEndDate = DateTime.FromOADate(0);
				strAcctStart = "";
				strAcctEnd = "";
				strgroup = "";
				if (GetSQL())
				{
					if (cmbReportOn.Text == "Single Employee")
					{
						// employee
						strEmpNo = txtEmployeeBoth.Text;
					}
					else if (cmbReportOn.Text == "Emp / Account Num")
					{
						// emp,date range, accounts
						strEmpNo = txtEmployeeBoth.Text;
						if (Information.IsDate(txtStartDate.Text))
						{
							dtStartDate = FCConvert.ToDateTime(txtStartDate.Text);
							if (Information.IsDate(txtEndDate.Text))
							{
								dtEndDate = FCConvert.ToDateTime(txtEndDate.Text);
							}
						}
						strAcctStart = VSGrid.TextMatrix(0, 0);
						strAcctEnd = VSGrid.TextMatrix(0, 1);
					}
					else if (cmbReportOn.Text == "All Files")
					{
						// nothing
					}
					else if (cmbReportOn.Text == "Single Group")
					{
						// group
						strgroup = txtGroup.Text;
					}
					else if (cmbReportOn.Text == "Date Range")
					{
						// date range
						if (Information.IsDate(txtStartDate.Text))
						{
							dtStartDate = FCConvert.ToDateTime(txtStartDate.Text);
							if (Information.IsDate(txtEndDate.Text))
							{
								dtEndDate = FCConvert.ToDateTime(txtEndDate.Text);
							}
						}
					}
					else if (cmbReportOn.Text == "Account Numbers")
					{
						// accounts
						strAcctStart = VSGrid.TextMatrix(0, 0);
						strAcctEnd = VSGrid.TextMatrix(0, 1);
						if (Information.IsDate(txtStartDate.Text))
						{
							dtStartDate = FCConvert.ToDateTime(txtStartDate.Text);
							if (Information.IsDate(txtEndDate.Text))
							{
								dtEndDate = FCConvert.ToDateTime(txtEndDate.Text);
							}
						}
					}
					else if (cmbReportOn.Text == "Emp / Date Range")
					{
						// emp,date range
						strEmpNo = txtEmployeeBoth.Text;
						if (Information.IsDate(txtStartDate.Text))
						{
							dtStartDate = FCConvert.ToDateTime(txtStartDate.Text);
							if (Information.IsDate(txtEndDate.Text))
							{
								dtEndDate = FCConvert.ToDateTime(txtEndDate.Text);
							}
						}
					}
					strDist = GetDistributionNumbersForFilter();
					boolSubGroup = (chkSubgroup.CheckState == Wisej.Web.CheckState.Checked);
					if (cmbType.Text == "Summary")
					{
						boolSummary = true;
					}
					else
					{
						boolSummary = false;
					}
					if (chkShowDetails.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolDetail = true;
					}
					else
					{
						boolDetail = false;
					}
					rptNewCustomDistReport.InstancePtr.Init(cmbSort.Text == "Pay Category", strEmpNo, dtStartDate, dtEndDate, strAcctStart, strAcctEnd, strgroup, strDist, boolSubGroup, boolDetail, boolSummary, cmbSort.Text == "Paid Dept / Div");
				}
			}
			else
			{
				if (GetSQL())
				{
					modGlobalVariables.Statics.gboolSummaryDistCustomReport = cmbType.Text == "Summary";
					if (cmbSort.Text == "Employee Number")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "ID";
					}
					else if (cmbSort.Text == "Employee Name")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "Name";
					}
					else if (cmbSort.Text == "Pay Category")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "Distribution";
					}
					else if (cmbSort.Text == "Pay Date")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "PayDate";
					}
					else if (cmbSort.Text == "Account Number")
					{
						if (chkSummaryOfAccounts.CheckState == CheckState.Checked)
						{
							modGlobalVariables.Statics.gstrDistributionSummaryType = "AccountSummary";
						}
						else
						{
							modGlobalVariables.Statics.gstrDistributionSummaryType = "Account";
						}
					}
					else if (cmbSort.Text == "Home Dept / Div")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "DeptDiv";
					}
					else if (cmbSort.Text == "Paid Dept / Div")
					{
						modGlobalVariables.Statics.gstrDistributionSummaryType = "DeptDiv";
					}
					if (cmbType.Text == "Distribution Setup Information")
					{
						// Dim distSetReport As New cPYDistributionSetupReport
						// If ValidateSetupReport Then
						// Call FillDistributionList(distSetReport.PayCodes)
						// 
						// Call repServ.FillCustomDistributionSetupReport(distSetReport)
						// Call rptCustomDistributionSetup.Init(distSetReport)
						// End If
						// frmReportViewer.Init rptCustomDistributionSetup, , , , , , , , , , True, "CustomDistribution"
					}
					else
					{
						// frmReportViewer.Init rptCustomAccountDistributionReport
						// frmReportViewer.Init rptCustomDistributionReport
						if (chkSubgroup.Visible && chkSubgroup.CheckState == Wisej.Web.CheckState.Checked)
						{
							if (chkShowDetails.CheckState == Wisej.Web.CheckState.Checked)
							{
								if (cmbSort.Text == "Pay Category")
								{
									rptNewCustomDistReport.InstancePtr.Init(true, modGlobalVariables.Statics.gstrCheckListingSQL);
								}
								else if (cmbSort.Text == "Home Dept / Div")
								{
									rptNewCustomDistReport.InstancePtr.Init(false, modGlobalVariables.Statics.gstrCheckListingSQL);
								}
								else if (cmbSort.Text == "Paid Dept / Div")
								{
									rptNewCustomDistReport.InstancePtr.Init(false, modGlobalVariables.Statics.gstrCheckListingSQL, boolPayDept: true);
								}
								else
								{
									rptCustomDistributionReport.InstancePtr.Init(true);
									// shouldn't get here
								}
							}
							else
							{
								rptCustomDistributionReport.InstancePtr.Init(true);
							}
						}
						else
						{
							if ((!(cmbReportOn.Text == "Single Employee") && !(cmbReportOn.Text == "Emp / Account Num") && !(cmbReportOn.Text == "Emp / Date Range") && !(cmbSort.Text == "Employee Number") && !(cmbSort.Text == "Employee Name")) || cmbType.Text == "Summary")
							{
								rptCustomDistributionReport.InstancePtr.Init(false);
							}
							else
							{
								rptCustomDistributionReport.InstancePtr.Init(false, true);
							}
						}
					}
				}
			}
		}

		private bool GetSQL()
		{
			bool GetSQL = false;
			int intQuarter;
			int intDeptDivStart;
			// vbPorter upgrade warning: intDeptDivLength As int	OnWriteFCConvert.ToDouble(
			int intDeptDivLength;
			intDeptDivStart = 3;
			intDeptDivLength = FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1);
			GetSQL = false;
			if (cmbType.Text == "Distribution Setup Information")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT tblEmployeeMaster.*,tblPayrollDistribution.* from tblEmployeeMaster INNER JOIN tblPayrollDistribution on tblEmployeeMaster.EmployeeNumber = tblPayrollDistribution.EmployeeNumber where CAT <> 0 ";
			}
			else
			{
				// gstrCheckListingSQL = "SELECT DISTINCT tblCheckDetail.*, tblEmployeeMaster.GroupID, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE not tblcheckdetail.checkvoid = 1 and tblCheckDetail.CheckNumber <> 0 AND tblCheckDetail.EmployeeNumber <> '' "
				if (cmbSort.Text != "Paid Dept / Div")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT DISTINCT tblCheckDetail.*, tblEmployeeMaster.GroupID, tblEmployeeMaster.DeptDiv, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE not tblcheckdetail.checkvoid = 1  AND tblCheckDetail.EmployeeNumber <> '' ";
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT DISTINCT tblCheckDetail.*, tblEmployeeMaster.GroupID, mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + ") as deptdiv, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE not tblcheckdetail.checkvoid = 1  AND tblCheckDetail.EmployeeNumber <> '' and (left(distaccountnumber & ' ',1) = 'E' or left(distaccountnumber & ' ',1) = 'G') ";
				}
			}
			GetSQL = false;
			if (cmbReportOn.Text == "Single Employee")
			{
				if (txtEmployeeBoth.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeBoth.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Distribution Setup Information")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblPayrollDistribution.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
				}
			}
			else if (cmbReportOn.Text == "Emp / Account Num")
			{
				// account and employee number
				if (txtEmployeeBoth.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeBoth.Focus();
					return GetSQL;
				}
				else if (VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) == string.Empty)
				{
					MessageBox.Show("At least one Account Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VSGrid.Focus();
					VSGrid.Select(0, 0);
					return GetSQL;
				}
				else
				{
					if (FCConvert.ToString(VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1)) == string.Empty)
					{
						// this is a single account number
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND DistAccountNumber = '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "'";
					}
					else
					{
						// this is a range
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND DistAccountNumber between '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "' AND '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1) + "'";
					}
					if (cmbType.Text == "Distribution Setup Information")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblPayrollDistribution.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
					if (Information.IsDate(txtStartDate.Text) && Information.IsDate(txtEndDate.Text))
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND PAYDATE >= '" + this.txtStartDate.Text + "' and PAYDATE <= '" + this.txtEndDate.Text + "'";
					}
					else if (Information.IsDate(txtStartDate.Text))
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND PAYDATE = '" + this.txtStartDate.Text + "'";
					}
				}
			}
			else if (cmbReportOn.Text == "Date Range")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Distribution Setup Information")
					{
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					}
				}
			}
			else if (cmbReportOn.Text == "Single Group")
			{
				if (txtGroup.Text != string.Empty)
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND GroupID = '" + txtGroup.Text + "'";
				}
			}
			else if (cmbReportOn.Text == "Account Numbers")
			{
				// this is the plain account number
				if (FCConvert.ToString(VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0)) == string.Empty)
				{
					MessageBox.Show("At least one Account Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VSGrid.Focus();
					VSGrid.Select(0, 0);
					return GetSQL;
				}
				else
				{
					if (!(cmbType.Text == "Distribution Setup Information"))
					{
						if (FCConvert.ToString(VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1)) == string.Empty)
						{
							// this is a single account number
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND DistAccountNumber = '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "'";
						}
						else
						{
							// this is a range
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND DistAccountNumber between '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "' AND '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1) + "'";
						}
					}
					else
					{
						if (FCConvert.ToString(VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1)) == string.Empty)
						{
							// this is a single account number
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND AccountNumber = '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "'";
						}
						else
						{
							// this is a range
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND AccountNumber between '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) + "' AND '" + VSGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 1) + "'";
						}
					}
					if (!(cmbType.Text == "Distribution Setup Information"))
					{
						if (Information.IsDate(txtStartDate.Text) && Information.IsDate(txtEndDate.Text))
						{
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND PAYDATE >= '" + this.txtStartDate.Text + "' and PAYDATE <= '" + this.txtEndDate.Text + "'";
						}
						else if (Information.IsDate(txtStartDate.Text))
						{
							modGlobalVariables.Statics.gstrCheckListingSQL += " AND PAYDATE = '" + this.txtStartDate.Text + "'";
						}
					}
				}
			}
			else if (cmbReportOn.Text == "Emp / Date Range")
			{
				if (txtEmployeeBoth.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeBoth.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Distribution Setup Information")
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblPayrollDistribution.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					}
				}
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return GetSQL;
				}
				else
				{
					if (cmbType.Text == "Distribution Setup Information")
					{
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					}
				}
			}
			else
			{
			}
			if (cmbType.Text == "Distribution Setup Information")
			{
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL += " AND distributionrecord = 1 ";
			}
			GetDistributionNumbersForFilter();
			if (cmbType.Text == "Distribution Setup Information")
			{
				if (cmbSort.Text == "Employee Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblEmployeeMaster.EmployeeNumber,RecordNumber";
				}
				else if (cmbSort.Text == "Employee Name")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,tblemployeemaster.employeenumber,RecordNumber";
				}
				else if (cmbSort.Text == "Pay Category")
				{
					if (chkSubgroup.CheckState == Wisej.Web.CheckState.Checked && chkShowDetails.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " order by distpaycategory,deptdiv,tblcheckdetail.employeenumber";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,RecordNumber";
					}
				}
				else if (cmbSort.Text == "Pay Date")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,RecordNumber";
				}
				else if (cmbSort.Text == "Account Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblEmployeeMaster.EmployeeNumber,RecordNumber";
				}
				else if (cmbSort.Text == "Home Dept / Div" || cmbSort.Text == "Paid Dept / Div")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblEmployeeMaster.DeptDiv, tblEmployeeMaster.EmployeeNumber,RecordNumber";
					// ElseIf optSort(6) Then
					// gstrCheckListingSQL = gstrCheckListingSQL & " order by mid(distaccountnumber," & intDeptDivStart & "," & intDeptDivLength & "),tblemployeemaster.employeenumber,recordnumber"
				}
			}
			else
			{
				if (cmbSort.Text == "Employee Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by tblCheckDetail.EmployeeNumber,DistPayCategory,PayDate";
				}
				else if (cmbSort.Text == "Employee Name")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by LastName,FirstName,tblcheckdetail.employeenumber,DistPayCategory,PayDate";
				}
				else if (cmbSort.Text == "Pay Category")
				{
					if (cmbType.Text == "Summary" && chkShowDetails.CheckState == Wisej.Web.CheckState.Checked && chkSubgroup.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DistPayCategory,deptdiv,LastName,FirstName,PayDate";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DistPayCategory,LastName,FirstName,PayDate";
					}
				}
				else if (cmbSort.Text == "Pay Date")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by PayDate,DistPayCategory,LastName,FirstName";
				}
				else if (cmbSort.Text == "Account Number")
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DistAccountNumber, DistPayCategory,PayDate";
				}
				else if (cmbSort.Text == "Home Dept / Div")
				{
					if (cmbType.Text == "Summary" && chkSubgroup.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DeptDiv, DistPayCategory,tblCheckDetail.EmployeeNumber,PayDate";
						// 
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " Order by DeptDiv, tblCheckDetail.EmployeeNumber,DistPayCategory,PayDate";
					}
					// gstrCheckListingSQL = gstrCheckListingSQL & " Order by tblCheckDetail.EmployeeNumber,DistAccountNumber, DistPayCategory,PayDate"
				}
				else if (cmbSort.Text == "Paid Dept / Div")
				{
					if (cmbType.Text == "Summary" && chkSubgroup.CheckState == Wisej.Web.CheckState.Checked)
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " order by mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),distpaycategory,tblcheckdetail.employeenumber,paydate";
					}
					else
					{
						modGlobalVariables.Statics.gstrCheckListingSQL += " order by mid(distaccountnumber," + FCConvert.ToString(intDeptDivStart) + "," + FCConvert.ToString(intDeptDivLength) + "),tblcheckdetail.employeenumber,distpaycategory,paydate";
					}
				}
			}
			GetSQL = true;
			return GetSQL;
		}

		private void FillDistributionList(Dictionary<object, object> dictDistributions)
		{
			if (!(dictDistributions == null))
			{
				int intCounter;
				dictDistributions.Clear();
				for (intCounter = 0; intCounter <= lstDistributions.Items.Count - 1; intCounter++)
				{
					//App.DoEvents();
					if (lstDistributions.Selected(intCounter) == true)
					{
						dictDistributions.Add(lstDistributions.ItemData(intCounter), lstDistributions.ItemData(intCounter));
					}
				}
			}
		}

		private string GetDistributionNumbersForFilter()
		{
			string GetDistributionNumbersForFilter = "";
			int intCounter;
			bool boolDeductionFound = false;
			string strReturn;
			strReturn = "";
			GetDistributionNumbersForFilter = "";
			modGlobalVariables.Statics.gstrCheckListingSQL += " AND (";
			strReturn = " and (";
			for (intCounter = 0; intCounter <= lstDistributions.Items.Count - 1; intCounter++)
			{
				if (lstDistributions.Selected(intCounter) == true)
				{
					if (boolDeductionFound)
					{
						// this is the Second deduction
						modGlobalVariables.Statics.gstrCheckListingSQL += " OR DistPayCategory = " + FCConvert.ToString(lstDistributions.ItemData(intCounter));
						strReturn += " or distpaycategory = " + FCConvert.ToString(lstDistributions.ItemData(intCounter));
					}
					else
					{
						// this is the first Deduction
						modGlobalVariables.Statics.gstrCheckListingSQL += " DistPayCategory = " + FCConvert.ToString(lstDistributions.ItemData(intCounter));
						strReturn += " DistPayCategory = " + FCConvert.ToString(lstDistributions.ItemData(intCounter));
					}
					boolDeductionFound = true;
				}
			}
			if (!boolDeductionFound)
			{
				// do not want any deductions if none are checked
				modGlobalVariables.Statics.gstrCheckListingSQL += "DistPayCategory = 9998198)";
				strReturn += "Distpaycategory = 9998198)";
			}
			else
			{
				modGlobalVariables.Statics.gstrCheckListingSQL += ")";
				strReturn += ") ";
			}
			if (cmbType.Text == "Distribution Setup Information")
			{
				modGlobalVariables.Statics.gstrCheckListingSQL = modGlobalVariables.Statics.gstrCheckListingSQL.Replace("DistPayCategory", "CAT");
				strReturn = strReturn.Replace("DistPayCategory", "CAT");
			}
			GetDistributionNumbersForFilter = strReturn;
			return GetDistributionNumbersForFilter;
		}

		private void optReportOn_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// txtEmployeeNumber.Visible = Index = 0 Or Index = 3
			txtStartDate.Visible = (cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Account Numbers" || cmbReportOn.Text == "Emp / Date Range");
			txtEndDate.Visible = (cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Account Numbers" || cmbReportOn.Text == "Emp / Date Range");
			// lblCaption.Visible = Index = 0 Or Index = 3
			// lblEmployeeBoth.Visible = Index = 6
			// txtEmployeeBoth.Visible = Index = 6
			txtEmployeeNumber.Visible = false;
			lblCaption.Visible = false;
			lblDate.Visible = cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Date Range" || cmbReportOn.Text == "Account Numbers" || cmbReportOn.Text == "Emp / Date Range";
			// lblDash.Visible = lblDate.Visible
			lblAccount.Visible = cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Account Numbers";
			VSGrid.Visible = cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Account Numbers";
			txtEmployeeBoth.Visible = cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Single Employee" || cmbReportOn.Text == "Emp / Date Range";
			lblEmployeeBoth.Visible = txtEmployeeBoth.Visible;
			lblGroup.Visible = cmbReportOn.Text == "Single Group";
			txtGroup.Visible = cmbReportOn.Text == "Single Group";
			// lblAccountEmp.Visible = Index = 1
			// Select Case Index
			// Case 0
			// lblCaption.Text = "Employee"
			// 
			// Case 1
			// lblCaption.Text = "Month of Pay Date"
			// 
			// Case 2
			// lblCaption.Text = "Qtr. of Pay Date"
			// 
			// Case 3
			// lblCaption.Text = "Year of Pay Date"
			// 
			// Case 4, 6
			// lblCaption.Text = "Pay Date Range"
			// 
			// Case 5
			// 
			// End Select
		}

		private void optReportOn_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReportOn.SelectedIndex;
			optReportOn_CheckedChanged(index, sender, e);
		}

		private void optSort_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			chkSummaryOfAccounts.Visible = cmbSort.Text == "Account Number";
			if (!chkSummaryOfAccounts.Visible)
				chkSummaryOfAccounts.CheckState = Wisej.Web.CheckState.Unchecked;
			if ((cmbSort.Text == "Home Dept / Div" || cmbSort.Text == "Paid Dept / Div") && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by Pay Category";
			}
			else if (cmbSort.Text == "Pay Category" && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by dept/div";
			}
			else
			{
				chkSubgroup.Visible = false;
			}
		}

		private void optSort_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSort.SelectedIndex;
			optSort_CheckedChanged(index, sender, e);
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			chkShowDetails.Visible = cmbType.Text == "Summary";
			if (cmbSort.Text == "Home Dept / Div" && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by Pay Category";
			}
			else if (cmbSort.Text == "Pay Category" && cmbType.Text == "Summary")
			{
				chkSubgroup.Visible = true;
				chkSubgroup.Text = "Sub-group by dept/div";
			}
			else
			{
				chkSubgroup.Visible = false;
			}
			if (cmbType.Text == "Distribution Setup Information")
			{
				if (cmbSort.Text == "Employee Number" || cmbSort.Text == "Employee Name")
				{
					// do nothing
				}
				else
				{
					cmbSort.Text = "Employee Number";
				}
				if (cmbSort.Items.Contains("Pay Category"))
				{
					cmbSort.Items.Remove("Pay Category");
				}
				if (cmbSort.Items.Contains("Pay Date"))
				{
					cmbSort.Items.Remove("Pay Date");
				}
				if (cmbSort.Items.Contains("Account Number"))
				{
					cmbSort.Items.Remove("Account Number");
				}
				if (cmbSort.Items.Contains("Home Dept / Div"))
				{
					cmbSort.Items.Remove("Home Dept / Div");
				}
				if (cmbSort.Items.Contains("Paid Dept / Div"))
				{
					cmbSort.Items.Remove("Paid Dept / Div");
				}
			}
			else
			{
				if (!cmbSort.Items.Contains("Pay Category"))
				{
					cmbSort.Items.Insert(2, "Pay Category");
				}
				if (!cmbSort.Items.Contains("Pay Date"))
				{
					cmbSort.Items.Insert(3, "Pay Date");
				}
				if (!cmbSort.Items.Contains("Account Number"))
				{
					cmbSort.Items.Insert(4, "Account Number");
				}
				if (!cmbSort.Items.Contains("Home Dept / Div"))
				{
					cmbSort.Items.Insert(5, "Home Dept / Div");
				}
				if (!cmbSort.Items.Contains("Paid Dept / Div"))
				{
					cmbSort.Items.Insert(6, "Paid Dept / Div");
				}
			}
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtEndDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtEndDate.Text.Length == 2 || txtEndDate.Text.Length == 5) && Strings.Right(txtEndDate.Text, 1) != "/")
        //	{
        //		txtEndDate.Text = txtEndDate.Text + "/";
        //		txtEndDate.SelectionStart = txtEndDate.Text.Length;
        //		txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //	}
        //}

        //private void txtEndDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtEndDate.SelectionStart = 0;
        //	txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //}

        //private void txtStartDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtStartDate.Text.Length == 2 || txtStartDate.Text.Length == 5) && Strings.Right(txtStartDate.Text, 1) != "/")
        //	{
        //		txtStartDate.Text = txtStartDate.Text + "/";
        //		txtStartDate.SelectionStart = txtStartDate.Text.Length;
        //		txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //	}
        //}

        //private void txtStartDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtStartDate.SelectionStart = 0;
        //	txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //}

        private void VSGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (VSGrid.Col == 0 || VSGrid.Col == 1)
				modNewAccountBox.CheckAccountKeyPress(VSGrid, VSGrid.Row, VSGrid.Col, KeyAscii, VSGrid.EditSelStart, VSGrid.EditText);
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void VSGrid_KeyUpEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (VSGrid.Col == 0 || VSGrid.Col == 1)
			{
				modNewAccountBox.CheckAccountKeyCode(VSGrid, VSGrid.Row, VSGrid.Col, FCConvert.ToInt32(KeyCode), 0, VSGrid.EditSelStart, VSGrid.EditText, VSGrid.EditSelLength);
				KeyCode = 0;
			}
		}

		private void VSGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (VSGrid.Col == 0 || VSGrid.Col == 1)
				modNewAccountBox.GetMaxLength(VSGrid.TextMatrix(VSGrid.Row, VSGrid.Col), ref VSGrid, VSGrid.Row, VSGrid.Col);
		}

		private void VSGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If Col = 0 Or Col = 1 Then
			// Cancel = CheckAccountValidate(VSGrid, Row, Col, Cancel)
			// Exit Sub
			// End If
		}

		private bool ValidateSetupReport()
		{
			bool ValidateSetupReport = false;
			if (cmbReportOn.Text == "Single Employee" || cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Emp / Date Range")
			{
				if (fecherFoundation.Strings.Trim(txtEmployeeBoth.Text) == "")
				{
					MessageBox.Show("You must enter an employee number", "Invalid Employee Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateSetupReport;
				}
			}
			if (cmbReportOn.Text == "Emp / Account Num" || cmbReportOn.Text == "Account Numbers")
			{
				if (VSGrid.TextMatrix(0, 0) == "")
				{
					MessageBox.Show("You must enter an account number", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateSetupReport;
				}
			}
			if (cmbReportOn.Text == "Single Group")
			{
				if (fecherFoundation.Strings.Trim(txtGroup.Text) == "")
				{
					MessageBox.Show("You must enter a group", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateSetupReport;
				}
			}
			ValidateSetupReport = true;
			return ValidateSetupReport;
		}

		private void SetupSetupReport(ref cPYDistributionSetupReport theReport)
		{
			string strEmpNo = "";
			string strgroup = "";
			string strAcctStart = "";
			string strAcctEnd = "";
			if (!(theReport == null))
			{
				if (cmbReportOn.Text == "Single Employee")
				{
					strEmpNo = fecherFoundation.Strings.Trim(txtEmployeeBoth.Text);
				}
				else if (cmbReportOn.Text == "Emp / Account Num")
				{
					// emp/accountnumber
					strEmpNo = fecherFoundation.Strings.Trim(txtEmployeeBoth.Text);
					strAcctStart = VSGrid.TextMatrix(0, 0);
					strAcctEnd = VSGrid.TextMatrix(0, 1);
				}
				else if (cmbReportOn.Text == "All Files")
				{
					// all
				}
				else if (cmbReportOn.Text == "Single Group")
				{
					// group
					strgroup = txtGroup.Text;
				}
				else if (cmbReportOn.Text == "Date Range")
				{
					// date range
				}
				else if (cmbReportOn.Text == "Account Numbers")
				{
					// account numbers
					strAcctStart = VSGrid.TextMatrix(0, 0);
					strAcctEnd = VSGrid.TextMatrix(0, 1);
				}
				else if (cmbReportOn.Text == "Emp / Date Range")
				{
					// emp / date
					strEmpNo = fecherFoundation.Strings.Trim(txtEmployeeBoth.Text);
				}
				if (strEmpNo != "")
				{
					theReport.EmployeesToReport.Add(strEmpNo, strEmpNo);
				}
				if (strgroup != "")
				{
					theReport.GroupID = strgroup;
				}
				if (strAcctStart != "" && strAcctEnd != "")
				{
					theReport.StartAccount = strAcctStart;
					theReport.EndAccount = strAcctEnd;
				}
				else if (strAcctStart != "")
				{
					theReport.Accounts.Add(strAcctStart, strAcctStart);
				}
				FillDistributionList(theReport.PayCodes);
			}
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
