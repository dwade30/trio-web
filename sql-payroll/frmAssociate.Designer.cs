//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmAssociate.
	/// </summary>
	partial class frmAssociate
	{
		public fecherFoundation.FCListBox lstAss;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lstAss = new fecherFoundation.FCListBox();
            this.cmdOK = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 298);
            this.BottomPanel.Size = new System.Drawing.Size(458, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstAss);
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Size = new System.Drawing.Size(458, 238);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(458, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(249, 30);
            this.HeaderText.Text = "Associate Information";
            // 
            // lstAss
            // 
            this.lstAss.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lstAss.Appearance = 0;
            this.lstAss.BackColor = System.Drawing.SystemColors.Window;
            this.lstAss.CheckBoxes = true;
            this.lstAss.Location = new System.Drawing.Point(30, 30);
            this.lstAss.MultiSelect = 0;
            this.lstAss.Name = "lstAss";
            this.lstAss.Size = new System.Drawing.Size(400, 200);
            this.lstAss.Sorted = false;
            this.lstAss.TabIndex = 0;
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(30, 190);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(67, 40);
            this.cmdOK.TabIndex = 1;
            this.cmdOK.Text = "Ok";
            this.cmdOK.Visible = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(127, 190);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(98, 40);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Visible = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                    ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(189, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmAssociate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(458, 406);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmAssociate";
            this.Text = "Associate Information";
            this.Load += new System.EventHandler(this.frmAssociate_Load);
            this.Activated += new System.EventHandler(this.frmAssociate_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAssociate_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAssociate_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}