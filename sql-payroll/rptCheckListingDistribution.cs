//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingDistribution.
	/// </summary>
	public partial class rptCheckListingDistribution : BaseSectionReport
	{
		public rptCheckListingDistribution()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee  Distribution Breakdown";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCheckListingDistribution InstancePtr
		{
			get
			{
				return (rptCheckListingDistribution)Sys.GetInstance(typeof(rptCheckListingDistribution));
			}
		}

		protected rptCheckListingDistribution _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMain?.Dispose();
				rsType?.Dispose();
				rsDistribution?.Dispose();
				rsPayCat?.Dispose();
				rsTax?.Dispose();
                rsMain = null;
                rsType = null;
                rsDistribution = null;
                rsPayCat = null;
                rsTax = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckListingDistribution	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsTax = new clsDRWrapper();
		private clsDRWrapper rsPayCat = new clsDRWrapper();
		private clsDRWrapper rsDistribution = new clsDRWrapper();
		private clsDRWrapper rsType = new clsDRWrapper();
		private int intPageNumber;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: txtVoid As object	OnWrite(string)
			// vbPorter upgrade warning: txtDistribution As object	OnWrite(string)
			// vbPorter upgrade warning: txtTax As object	OnWrite(string, object)
			// vbPorter upgrade warning: txtPayRate As object	OnWrite(string)
			// vbPorter upgrade warning: txtDistGrossPay As object	OnWrite(string)
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				NextRecord:
				;
				while (!rsMain.EndOfFile())
				{
					//Application.DoEvents();
					if (!employeeDict.ContainsKey(rsMain.Get_Fields("EmployeeNumber")))
					{
						rsMain.MoveNext();
					}
					else
					{
						break;
					}
				}
				if (rsMain.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					eArgs.EOF = true;
					return;
				}
				if (frmWait.InstancePtr.prgProgress.Value == 99)
				{
					frmWait.InstancePtr.prgProgress.Value = 0;
				}
				frmWait.InstancePtr.IncrementProgress();
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"))).Length > 5)
				{
					txtEmployee.Text = rsMain.Get_Fields_String("EmployeeNumber") + " " + rsMain.Get_Fields_String("EmployeeName");
				}
				else
				{
					txtEmployee.Text = rsMain.Get_Fields_String("EmployeeNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"))).Length, " ") + rsMain.Get_Fields_String("EmployeeName");
				}
				txtDateField.Text = FCConvert.ToString(rsMain.Get_Fields("PayDate"));
				rsType.OpenRecordset("SELECT tblCheckDetail.* From tblCheckDetail where TotalRecord = 1 AND EmployeeNumber = '" + rsMain.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMain.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMain.Get_Fields("PayRunID") + " AND CheckNumber = " + rsMain.Get_Fields("CheckNumber"), modGlobalVariables.DEFAULTDATABASE);
				if (!rsType.EndOfFile())
				{
					if (FCConvert.ToString(rsType.Get_Fields_Int32("NumberDirectDeposits")) == string.Empty)
					{
						txtType.Text = string.Empty;
					}
					else if (rsType.Get_Fields_Int32("NumberDirectDeposits") != 0)
					{
						txtType.Text = "D";
					}
					else if (rsType.Get_Fields_Int32("NumberChecks") != 0 )
					{
						txtType.Text = "R";
					}
					else if (rsType.Get_Fields_Int32("NumberBoth") != 0)
					{
						txtType.Text = "B";
					}
					else
					{
						txtType.Text = string.Empty;
					}
				}
				txtCheckNumber.Text = rsMain.Get_Fields_String("CheckNumber");
				txtJournal.Text = rsMain.Get_Fields_String("JournalNumber");
				txtWarrant.Text = FCConvert.ToString(rsMain.Get_Fields_Int32("WarrantNumber"));
				if (FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("CheckVoid")))
				{
					txtVoid.Text = "T";
				}
				else
				{
					txtVoid.Text = "F";
				}
				rsPayCat.FindFirstRecord("ID", rsMain.Get_Fields_Int32("DistPayCategory"));
				if (rsPayCat.NoMatch)
				{
					txtDistribution.Text = string.Empty;
					txtTax.Text = string.Empty;
				}
				else
				{
					txtDistribution.Text = FCConvert.ToString(rsPayCat.Get_Fields_Int32("CategoryNumber")) + " - " + FCConvert.ToString(rsPayCat.Get_Fields("Description"));
					rsTax.FindFirstRecord("ID", rsPayCat.Get_Fields_Int32("TaxStatus"));
					if (rsTax.NoMatch)
					{
						txtTax.Text = string.Empty;
					}
					else
					{
						txtTax.Text = rsTax.Get_Fields_String("TaxStatusCode");
					}
				}
				txtPayRate.Text = Strings.Format(rsMain.Get_Fields("DistPayRate"), "0.00");
				if (FCConvert.ToString(txtPayRate) == string.Empty)
				{
					rsDistribution.FindFirstRecord2("EmployeeNumber, Cat, AccountNumber", rsMain.Get_Fields("EmployeeNumber") + "," + rsPayCat.Get_Fields_Int32("CategoryNumber") + "," + rsMain.Get_Fields_String("DistAccountNumber"), ",");
					if (rsDistribution.NoMatch)
					{
					}
					else
					{
						txtPayRate.Text = Strings.Format(rsDistribution.Get_Fields_Decimal("BaseRate"), "0.00");
					}
				}
				txtAccount.Text = rsMain.Get_Fields_String("DistAccountNumber");
				txtHours.Text = rsMain.Get_Fields_String("DistHours");
				txtDistGrossPay.Text = Strings.Format(rsMain.Get_Fields("DistGrossPay"), "0.00");
				if (!rsMain.EndOfFile())
					rsMain.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intPageNumber = 0;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsMain.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, modGlobalVariables.DEFAULTDATABASE);
			rsPayCat.OpenRecordset("Select * from tblPayCategories", "TWPY0000.vb1");
			rsTax.OpenRecordset("Select * from tblTaxStatusCodes", "TWPY0000.vb1");
			rsDistribution.OpenRecordset("Select * from tblPayrollDistribution", "TWPY0000.vb1");
			lblSort.Text = modGlobalVariables.Statics.gstrCheckListingSort;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (rsMain.EndOfFile() != true && rsMain.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}


		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniName As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
