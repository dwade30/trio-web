﻿//Fecher vbPorter - Version 1.0.0.59
using Global;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cPYDistributionSetupReport
	{
		//=========================================================
		private cGenericCollection listEmployees = new cGenericCollection();
		private int intOrderBy;
		private string strAccountStart = "";
		private string strAccountEnd = "";
		private Dictionary<object, object> dictAccounts = new Dictionary<object, object>();
		private string strGroup = "";
		private Dictionary<object, object> dictPayCodes = new Dictionary<object, object>();
		private Dictionary<object, object> dictEmployees = new Dictionary<object, object>();

		public Dictionary<object, object> EmployeesToReport
		{
			get
			{
				Dictionary<object, object> EmployeesToReport = null;
				EmployeesToReport = dictEmployees;
				return EmployeesToReport;
			}
		}

		public Dictionary<object, object> PayCodes
		{
			get
			{
				Dictionary<object, object> PayCodes = null;
				PayCodes = dictPayCodes;
				return PayCodes;
			}
		}

		public int OrderByItem
		{
			set
			{
				intOrderBy = value;
			}
			get
			{
				int OrderByItem = 0;
				OrderByItem = intOrderBy;
				return OrderByItem;
			}
		}

		public string StartAccount
		{
			set
			{
				strAccountStart = value;
			}
			get
			{
				string StartAccount = "";
				StartAccount = strAccountStart;
				return StartAccount;
			}
		}

		public string EndAccount
		{
			set
			{
				strAccountEnd = value;
			}
			get
			{
				string EndAccount = "";
				EndAccount = strAccountEnd;
				return EndAccount;
			}
		}

		public Dictionary<object, object> Accounts
		{
			get
			{
				Dictionary<object, object> Accounts = null;
				Accounts = dictAccounts;
				return Accounts;
			}
		}

		public string GroupID
		{
			set
			{
				strGroup = value;
			}
			get
			{
				string GroupID = "";
				GroupID = strGroup;
				return GroupID;
			}
		}

		public cGenericCollection Employees
		{
			get
			{
				cGenericCollection Employees = null;
				Employees = listEmployees;
				return Employees;
			}
		}

		public void Clear()
		{
			listEmployees.ClearList();
			dictAccounts.Clear();
			dictEmployees.Clear();
			intOrderBy = 0;
			strAccountStart = "";
			strAccountEnd = "";
			strGroup = "";
			dictPayCodes.Clear();
		}
	}
}
