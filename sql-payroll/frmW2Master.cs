//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmW2Master : BaseForm
	{
		public frmW2Master()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2Master InstancePtr
		{
			get
			{
				return (frmW2Master)Sys.GetInstance(typeof(frmW2Master));
			}
		}

		protected frmW2Master _InstancePtr = null;
		//=========================================================
		private bool boolLoad;
		const int CNSTEmployersFederalIDRow = 0;
		const int CNSTEmployersStateID = 1;
		const int CNSTStateCode = 2;
		const int CNSTLocalityName = 3;
		const int CNSTEmployersName = 4;
		const int CNSTAddress1 = 5;
		const int CNSTAddress2 = 6;
		const int CNSTLocationAddress = 7;
		const int CNSTDeliveryAddress = 8;
		const int CNSTCityRow = 9;
		const int CNSTStateRow = 10;
		const int CNSTZipRow = 11;
		const int CNSTComputerMake = 12;
		const int CNSTContactPerson = 13;
		const int CNSTPhoneNumber = 14;
		const int CNSTPhoneExt = 15;
		const int CNSTFaxNumber = 16;
		const int CNSTEmail = 17;
		const int CNSTPreference = 18;
		const int CNSTEmployeePin = 19;

		private void frmW2Master_Activated(object sender, System.EventArgs e)
		{
			if (boolLoad)
			{
				LoadGrid();
				LoadDeductionGrid();
				boolLoad = false;
			}
			// Call ForceFormToResize(Me)
		}

		private void frmW2Master_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2Master properties;
			//frmW2Master.ScaleWidth	= 8985;
			//frmW2Master.ScaleHeight	= 7260;
			//frmW2Master.LinkTopic	= "Form1";
			//frmW2Master.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				boolLoad = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2Master_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
			else if (KeyCode == Keys.Insert)
			{
				if (this.ActiveControl.GetName() == "vsDeductions")
				{
					vsDeductions.Rows += 1;
					vsDeductions.Select(vsDeductions.Rows - 1, 1);
				}
				else if (this.ActiveControl.GetName() == "vsMatch")
				{
					vsMatch.Rows += 1;
					vsMatch.Select(vsMatch.Rows - 1, 1);
				}
			}
		}

		private void frmW2Master_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			vsData.Cols = 3;
			vsData.Rows = 20;
			vsData.FixedCols = 2;
			vsData.FixedRows = 0;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			rsData.OpenRecordset("Select * from tblW2Master");
			vsData.TextMatrix(CNSTEmployersFederalIDRow, 1, "Employers Federal ID");
			vsData.TextMatrix(CNSTEmployersStateID, 1, "Employers State ID");
			vsData.TextMatrix(CNSTStateCode, 1, "State Code  (ie..ME, NH, VT)");
			vsData.TextMatrix(CNSTLocalityName, 1, "Locality Name");
			vsData.TextMatrix(CNSTEmployersName, 1, "Employers Name");
			vsData.TextMatrix(CNSTAddress1, 1, "Address 1");
			vsData.TextMatrix(CNSTAddress2, 1, "Address 2");
			vsData.TextMatrix(CNSTLocationAddress, 1, "Location Address");
			vsData.TextMatrix(CNSTDeliveryAddress, 1, "Delivery Address");
			vsData.TextMatrix(CNSTCityRow, 1, "City");
			vsData.TextMatrix(CNSTStateRow, 1, "State");
			vsData.TextMatrix(CNSTZipRow, 1, "Zip");
			vsData.TextMatrix(CNSTComputerMake, 1, "Computer Make");
			vsData.TextMatrix(CNSTContactPerson, 1, "Contact Person");
			vsData.TextMatrix(CNSTPhoneNumber, 1, "Contact Telephone Number");
			vsData.TextMatrix(CNSTPhoneExt, 1, "Contact Telephone Ext");
			vsData.TextMatrix(CNSTFaxNumber, 1, "Contact Fax Number");
			vsData.TextMatrix(CNSTEmail, 1, "Contact Email Address");
			vsData.TextMatrix(CNSTPreference, 1, "Contact Preference");
			vsData.TextMatrix(CNSTEmployeePin, 1, "Employee PIN");
			if (!rsData.EndOfFile())
			{
				vsData.TextMatrix(CNSTEmployersFederalIDRow, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployersFederalID")));
				vsData.TextMatrix(CNSTEmployersStateID, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployersStateID")));
				vsData.TextMatrix(CNSTStateCode, 2, FCConvert.ToString(rsData.Get_Fields_String("StateCode")));
				vsData.TextMatrix(CNSTLocalityName, 2, FCConvert.ToString(rsData.Get_Fields_String("LocalityName")));
				vsData.TextMatrix(CNSTEmployersName, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployersName")));
				vsData.TextMatrix(CNSTAddress1, 2, FCConvert.ToString(rsData.Get_Fields_String("Address1")));
				vsData.TextMatrix(CNSTAddress2, 2, FCConvert.ToString(rsData.Get_Fields_String("Address2")));
				vsData.TextMatrix(CNSTLocationAddress, 2, fecherFoundation.Strings.Trim(Strings.Left(rsData.Get_Fields_String("LocationAddress") + Strings.StrDup(20, " "), 20)));
				vsData.TextMatrix(CNSTDeliveryAddress, 2, fecherFoundation.Strings.Trim(Strings.Left(rsData.Get_Fields_String("DeliveryAddress") + Strings.StrDup(20, " "), 20)));
				vsData.TextMatrix(CNSTCityRow, 2, FCConvert.ToString(rsData.Get_Fields_String("City")));
				vsData.TextMatrix(CNSTStateRow, 2, FCConvert.ToString(rsData.Get_Fields("State")));
				vsData.TextMatrix(CNSTZipRow, 2, FCConvert.ToString(rsData.Get_Fields("Zip")));
				vsData.TextMatrix(CNSTComputerMake, 2, FCConvert.ToString(rsData.Get_Fields_String("ComputerMake")));
				vsData.TextMatrix(CNSTContactPerson, 2, FCConvert.ToString(rsData.Get_Fields_String("ContactPerson")));
				vsData.TextMatrix(CNSTPhoneNumber, 2, FCConvert.ToString(rsData.Get_Fields_String("ContactTelephoneNumber")));
				vsData.TextMatrix(CNSTPhoneExt, 2, FCConvert.ToString(rsData.Get_Fields_String("ContactTelephoneExt")));
				vsData.TextMatrix(CNSTFaxNumber, 2, FCConvert.ToString(rsData.Get_Fields_String("ContactFaxNumber")));
				vsData.TextMatrix(CNSTEmail, 2, FCConvert.ToString(rsData.Get_Fields_String("ContactEmilAddress")));
				if (Conversion.Val(rsData.Get_Fields_String("ContactPreference1")) == 2)
				{
					vsData.TextMatrix(CNSTPreference, 2, "Postal Service");
				}
				else
				{
					vsData.TextMatrix(CNSTPreference, 2, "E-Mail");
				}
				// .TextMatrix(16, 2) = rsData.Fields("ContactPreference1")
				vsData.TextMatrix(CNSTEmployeePin, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployeePIN")));
			}
			vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsData.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void LoadDeductionGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDeductions = new clsDRWrapper();
			clsDRWrapper rsMatch = new clsDRWrapper();
			clsDRWrapper rsBox10 = new clsDRWrapper();
			clsDRWrapper rsAdditionalDeduction = new clsDRWrapper();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			vsData.Cols = 3;
			vsData.Rows = 20;
			vsData.FixedCols = 2;
			vsData.FixedRows = 0;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsDeductions.Cols = 4;
			vsDeductions.Rows = 1;
			vsDeductions.FixedCols = 1;
			vsDeductions.FixedRows = 1;
			vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			// ADDED ON 8/10/2004 BY MATTHEW
			// RON MENTIONED THAT THE BOX 12 WILL NEVER HAVE MORE THEN ONE CHARACTER
			vsDeductions.ColEditMask(1, "&&");
			vsMatch.Cols = 4;
			vsMatch.Rows = 1;
			vsMatch.FixedCols = 1;
			vsMatch.FixedRows = 1;
			vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsMatch.ColEditMask(1, "&&&&");
			vsBox10.Cols = 4;
			vsBox10.Rows = 1;
			vsBox10.FixedCols = 1;
			vsBox10.FixedRows = 1;
			vsBox10.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsBox10.ColHidden(1, true);
			// Call rsData.OpenRecordset("Select * from tblW2Box12And14 where Box12 = 1 AND DeductionNumber <9000")
			// Call rsMatch.OpenRecordset("Select * from tblW2Box12And14 where Box14 = 1 AND DeductionNumber <9000")
			rsData.OpenRecordset("Select * from tblW2Box12And14 where Box12 = 1");
			rsMatch.OpenRecordset("Select * from tblW2Box12And14 where Box14 = 1");
			rsBox10.OpenRecordset("Select * from tblW2Box12And14 where Box10 = 1");
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup Order by DeductionNumber");
			rsAdditionalDeduction.OpenRecordset("Select * from tblW2AdditionalInfo");
			// FILL THE DEDUCTION COLUMN
			if (!rsDeductions.EndOfFile())
			{
				rsDeductions.MoveLast();
				rsDeductions.MoveFirst();
				// makes this column work like a combo box.
				// this fills the box with the values from the tblPayCategories table
				// This forces this column to work as a combo box
				for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
				{
					if (intCounter == 1)
					{
						vsDeductions.ColComboList(2, vsDeductions.ColComboList(2) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
						vsMatch.ColComboList(2, vsMatch.ColComboList(2) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
						vsBox10.ColComboList(2, vsBox10.ColComboList(2) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
					}
					else
					{
						vsDeductions.ColComboList(2, vsDeductions.ColComboList(2) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
						vsMatch.ColComboList(2, vsMatch.ColComboList(2) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
						vsBox10.ColComboList(2, vsBox10.ColComboList(2) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description"));
					}
					rsDeductions.MoveNext();
				}
			}
			vsDeductions.TextMatrix(0, 1, "Code");
			vsDeductions.TextMatrix(0, 2, "Deduction");
			vsDeductions.TextMatrix(0, 3, "Ded");
			//vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsDeductions.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				vsDeductions.Rows += 1;
				if (rsData.Get_Fields_Int32("DeductionNumber") >= 9000)
				{
					if (rsAdditionalDeduction.FindFirstRecord("ID", rsData.Get_Fields_Int32("DeductionNumber") - 9000))
					{
						vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
						vsDeductions.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("Code")));
						vsDeductions.TextMatrix(intCounter, 2, rsData.Get_Fields_Int32("DeductionNumber") + " - " + rsAdditionalDeduction.Get_Fields("Description"));
						vsDeductions.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, vsDeductions.Cols - 1, Color.Lime);
					}
					else
					{
						vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
						vsDeductions.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("Code")));
						vsDeductions.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
						vsDeductions.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
					}
				}
				else
				{
					vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsDeductions.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("Code")));
					vsDeductions.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
					vsDeductions.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
				}
				rsData.MoveNext();
			}
			if (vsDeductions.Rows > 1)
			{
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsDeductions.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			vsMatch.TextMatrix(0, 1, "Code");
			vsMatch.TextMatrix(0, 2, "Deduction");
			vsMatch.TextMatrix(0, 3, "Ded");
			//vsMatch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsMatch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
			{
				vsMatch.Rows += 1;
				if (rsMatch.Get_Fields_Int32("DeductionNumber") >= 9000)
				{
					if (rsAdditionalDeduction.FindFirstRecord("ID", rsMatch.Get_Fields_Int32("DeductionNumber") - 9000))
					{
						vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(rsMatch.Get_Fields("ID")));
						vsMatch.TextMatrix(intCounter, 1, FCConvert.ToString(rsMatch.Get_Fields("Code")));
						vsMatch.TextMatrix(intCounter, 2, rsMatch.Get_Fields_Int32("DeductionNumber") + " - " + rsAdditionalDeduction.Get_Fields("Description"));
						vsMatch.TextMatrix(intCounter, 3, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, vsMatch.Cols - 1, Color.Lime);
					}
					else
					{
						vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(rsMatch.Get_Fields("ID")));
						vsMatch.TextMatrix(intCounter, 1, FCConvert.ToString(rsMatch.Get_Fields("Code")));
						vsMatch.TextMatrix(intCounter, 2, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
						vsMatch.TextMatrix(intCounter, 3, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
					}
				}
				else
				{
					vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(rsMatch.Get_Fields("ID")));
					vsMatch.TextMatrix(intCounter, 1, FCConvert.ToString(rsMatch.Get_Fields("Code")));
					vsMatch.TextMatrix(intCounter, 2, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
					vsMatch.TextMatrix(intCounter, 3, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
				}
				rsMatch.MoveNext();
			}
			if (vsMatch.Rows > 1)
			{
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsMatch.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			vsBox10.TextMatrix(0, 1, "Code");
			vsBox10.TextMatrix(0, 2, "Deduction");
			vsBox10.TextMatrix(0, 3, "Ded");
			//vsBox10.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBox10.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (intCounter = 1; intCounter <= (rsBox10.RecordCount()); intCounter++)
			{
				vsBox10.Rows += 1;
				vsBox10.TextMatrix(intCounter, 0, FCConvert.ToString(rsBox10.Get_Fields("ID")));
				vsBox10.TextMatrix(intCounter, 1, FCConvert.ToString(rsBox10.Get_Fields("Code")));
				vsBox10.TextMatrix(intCounter, 2, FCConvert.ToString(rsBox10.Get_Fields_Int32("DeductionNumber")));
				vsBox10.TextMatrix(intCounter, 3, FCConvert.ToString(rsBox10.Get_Fields_Int32("DeductionNumber")));
				rsBox10.MoveNext();
			}
			if (vsBox10.Rows > 1)
			{
				vsBox10.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsBox10.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            //FC:FINAL:BSE #2698 fixed columns lost after load deduction grid
            frmW2Master_Resize(this, EventArgs.Empty);
		}

		private void frmW2Master_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, this.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.39));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.59));
			vsDeductions.ColWidth(0, vsDeductions.WidthOriginal * 0);
			vsDeductions.ColWidth(1, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.22));
			vsDeductions.ColWidth(2, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.76));
			vsDeductions.ColWidth(3, vsDeductions.WidthOriginal * 0);
			vsMatch.ColWidth(0, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(1, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.22));
			vsMatch.ColWidth(2, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.76));
			vsMatch.ColWidth(3, vsMatch.WidthOriginal * 0);
			vsBox10.ColWidth(0, vsBox10.WidthOriginal * 0);
            // .ColWidth(1) = vsBox10.Width * 0.18
            // .ColWidth(2) = vsBox10.Width * 0.72
            vsBox10.ColWidth(2, vsBox10.WidthOriginal);
            vsBox10.ColWidth(3, vsBox10.WidthOriginal * 0);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuAddBox10_Click(object sender, System.EventArgs e)
		{
			vsBox10.Focus();
			vsBox10.Rows += 1;
			vsBox10.TextMatrix(vsBox10.Rows - 1, 0, FCConvert.ToString(0));
			vsBox10.Select(vsBox10.Rows - 1, 1);
			vsBox10.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsBox10.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		}

		private void mnuAddDeduction_Click(object sender, System.EventArgs e)
		{
			vsDeductions.Focus();
			vsDeductions.Rows += 1;
			vsDeductions.TextMatrix(vsDeductions.Rows - 1, 0, FCConvert.ToString(0));
			vsDeductions.Select(vsDeductions.Rows - 1, 1);
			vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		}

		private void mnuAddEmpMatchCode_Click(object sender, System.EventArgs e)
		{
			vsMatch.Focus();
			vsMatch.Rows += 1;
			vsMatch.TextMatrix(vsMatch.Rows - 1, 0, FCConvert.ToString(0));
			vsMatch.Select(vsMatch.Rows - 1, 1);
			vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Rows - 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		}

		private void mnuDeleteBox10_Click(object sender, System.EventArgs e)
		{
			if (vsBox10.Row > 0)
			{
				if (MessageBox.Show("This will remove code: " + vsBox10.TextMatrix(vsBox10.Row, 1) + " Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblW2Box12And14 where ID = " + FCConvert.ToString(Conversion.Val(vsBox10.TextMatrix(vsBox10.Row, 0))), "TWPY0000.vb1");
					vsBox10.RemoveItem(vsBox10.Row);
				}
			}
			else
			{
				MessageBox.Show("Box 10 Code must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuDeleteBox12_Click(object sender, System.EventArgs e)
		{
			if (vsDeductions.Row > 0)
			{
				if (MessageBox.Show("This will remove code: " + vsDeductions.TextMatrix(vsDeductions.Row, 1) + " Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblW2Box12And14 where ID = " + FCConvert.ToString(Conversion.Val(vsDeductions.TextMatrix(vsDeductions.Row, 0))), "TWPY0000.vb1");
					vsDeductions.RemoveItem(vsDeductions.Row);
				}
			}
			else
			{
				MessageBox.Show("Box 12 Code must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuDeleteBox14_Click(object sender, System.EventArgs e)
		{
			if (vsMatch.Row > 0)
			{
				if (MessageBox.Show("This will remove code: " + vsMatch.TextMatrix(vsMatch.Row, 1) + " Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblW2Box12And14 where ID = " + FCConvert.ToString(Conversion.Val(vsMatch.TextMatrix(vsMatch.Row, 0))), "TWPY0000.vb1");
					vsMatch.RemoveItem(vsMatch.Row);
				}
			}
			else
			{
				MessageBox.Show("Box 14 Code must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTPreference, 2)) == "POSTAL SERVICE")
				{
					if (fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTAddress1, 2)) == string.Empty || fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTCityRow, 2)) == string.Empty || fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTStateRow, 2)) == string.Empty || fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTZipRow, 2)) == string.Empty)
					{
						MessageBox.Show("Your preferred contact method is by postal service but you have not specified a valid address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInformation;
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTEmail, 2)) == string.Empty)
					{
						MessageBox.Show("Your preferred contact method is by E-mail but you have not specified a valid address", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInformation;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				vsData.Select(0, 0);
				vsDeductions.Select(0, 0);
				vsMatch.Select(0, 0);
				vsBox10.Select(0, 0);
				// ************************************************************
				rsUpdate.Execute("Update tblW2Deductions Set Code = ''", "Payroll");
				rsUpdate.Execute("Update tblW2Matches Set Code = ''", "Payroll");
				clsDRWrapper rsReportHeader = new clsDRWrapper();
				rsReportHeader.OpenRecordset("Select * from tblW2Master");
				if (rsReportHeader.EndOfFile())
				{
					rsReportHeader.AddNew();
				}
				else
				{
					rsReportHeader.Edit();
				}
				rsReportHeader.Set_Fields("EmployersFederalID", vsData.TextMatrix(CNSTEmployersFederalIDRow, 2));
				rsReportHeader.Set_Fields("EmployersStateID", vsData.TextMatrix(CNSTEmployersStateID, 2));
				rsReportHeader.Set_Fields("StateCode", vsData.TextMatrix(CNSTStateCode, 2));
				rsReportHeader.Set_Fields("LocalityName", vsData.TextMatrix(CNSTLocalityName, 2));
				rsReportHeader.Set_Fields("EmployersName", vsData.TextMatrix(CNSTEmployersName, 2));
				rsReportHeader.Set_Fields("Address1", vsData.TextMatrix(CNSTAddress1, 2));
				rsReportHeader.Set_Fields("Address2", vsData.TextMatrix(CNSTAddress2, 2));
				rsReportHeader.Set_Fields("LocationAddress", vsData.TextMatrix(CNSTLocationAddress, 2));
				rsReportHeader.Set_Fields("DeliveryAddress", vsData.TextMatrix(CNSTDeliveryAddress, 2));
				rsReportHeader.Set_Fields("City", vsData.TextMatrix(CNSTCityRow, 2));
				rsReportHeader.Set_Fields("State", vsData.TextMatrix(CNSTStateRow, 2));
				rsReportHeader.Set_Fields("Zip", vsData.TextMatrix(CNSTZipRow, 2));
				rsReportHeader.Set_Fields("ComputerMake", vsData.TextMatrix(CNSTComputerMake, 2));
				rsReportHeader.Set_Fields("ContactPerson", vsData.TextMatrix(CNSTContactPerson, 2));
				rsReportHeader.Set_Fields("ContactTelephoneNumber", vsData.TextMatrix(CNSTPhoneNumber, 2));
				rsReportHeader.Set_Fields("ContactTelephoneExt", vsData.TextMatrix(CNSTPhoneExt, 2));
				rsReportHeader.Set_Fields("ContactFaxNumber", vsData.TextMatrix(CNSTFaxNumber, 2));
				rsReportHeader.Set_Fields("ContactEmilAddress", vsData.TextMatrix(CNSTEmail, 2));
				if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTPreference, 2)) == "POSTAL SERVICE")
				{
					rsReportHeader.Set_Fields("ContactPreference1", 2);
				}
				else
				{
					rsReportHeader.Set_Fields("ContactPreference1", 1);
				}
				// .Fields("ContactPreference1") = vsData.TextMatrix(16, 2)
				rsReportHeader.Set_Fields("EmployeePIN", vsData.TextMatrix(CNSTEmployeePin, 2));
				rsReportHeader.Update();
				// ************************************************************
				rsData.Execute("Delete from tblW2Box12And14", "Payroll");
				rsData.OpenRecordset("Select * from tblW2Box12And14");
				for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsDeductions.TextMatrix(intCounter, 0)) == 0)
					{
						// this is a new record
						rsData.AddNew();
					}
					else
					{
						rsData.FindFirstRecord("ID", vsDeductions.TextMatrix(intCounter, 0));
						if (rsData.NoMatch)
						{
							rsData.AddNew();
						}
						else
						{
							rsData.Edit();
						}
					}
					rsData.Set_Fields("Code", vsDeductions.TextMatrix(intCounter, 1));
					rsData.Set_Fields("DeductionNumber", FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))));
					rsData.Set_Fields("Box12", true);
					rsData.Update();
					rsUpdate.Execute("Update tblW2Deductions Set Box12 = 1,Code = '" + vsDeductions.TextMatrix(intCounter, 1) + "' Where DeductionNumber = " + FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))), "Payroll");
				}
				// ***************************************************************
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, 0)) == 0)
					{
						// this is a new record
						rsData.AddNew();
					}
					else
					{
						rsData.FindFirstRecord("ID", vsMatch.TextMatrix(intCounter, 0));
						if (rsData.NoMatch)
						{
							rsData.AddNew();
						}
						else
						{
							rsData.Edit();
						}
					}
					rsData.Set_Fields("Code", vsMatch.TextMatrix(intCounter, 1) + " ");
					rsData.Set_Fields("DeductionNumber", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))));
					rsData.Set_Fields("Box14", true);
					rsData.Update();
					rsUpdate.Execute("Update tblW2Matches Set Box14 = 1,Code = '" + vsMatch.TextMatrix(intCounter, 1) + "' Where DeductionNumber = " + FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))), "Payroll");
				}
				// ************************************************************
				for (intCounter = 1; intCounter <= (vsBox10.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsBox10.TextMatrix(intCounter, 0)) == 0)
					{
						// this is a new record
						rsData.AddNew();
					}
					else
					{
						rsData.FindFirstRecord("ID", vsBox10.TextMatrix(intCounter, 0));
						if (rsData.NoMatch)
						{
							rsData.AddNew();
						}
						else
						{
							rsData.Edit();
						}
					}
					rsData.Set_Fields("Code", vsBox10.TextMatrix(intCounter, 1));
					rsData.Set_Fields("DeductionNumber", FCConvert.ToString(Conversion.Val(vsBox10.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))));
					rsData.Set_Fields("Box10", true);
					rsData.Update();
					rsUpdate.Execute("Update tblW2Deductions Set Code = '" + vsBox10.TextMatrix(intCounter, 1) + "' Where DeductionNumber = " + FCConvert.ToString(Conversion.Val(vsBox10.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2))), "Payroll");
				}
				// ***************************************************************
				rsUpdate.Execute("Update tblDefaultInformation SET W2MasterSaveDone = 1", "TWPY0000.vb1");
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				modGlobalRoutines.UpdateW2StatusData("Contact");
				MessageBox.Show("Save completed successfully", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInformation = true;
				return SaveInformation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveInformation;
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			if (SaveInformation())
				Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsData.Row == CNSTPreference)
			{
				vsData.EditMask = "0";
			}
			else
			{
				vsData.EditMask = string.Empty;
			}
		}

		private void vsData_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsData.Row == CNSTDeliveryAddress || vsData.Row == CNSTLocationAddress)
			{
				if (vsData.EditText.Length > 22)
				{
					// MsgBox "Delivery and location addresses may not exceed 22 characters in length", vbExclamation, "Invalid Address"
					vsData.EditText = Strings.Left(vsData.EditText, 22);
				}
			}
		}

		private void vsData_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsData.Row == CNSTPreference)
			{
				if (KeyAscii == 49 || KeyAscii == 50 || KeyAscii == 51 || KeyAscii == 8)
				{
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsData_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsData[e.ColumnIndex, e.RowIndex];
            if (vsData.GetFlexRowIndex(e.RowIndex) == CNSTPreference)
			{
				//ToolTip1.SetToolTip(vsData, "Enter 1 for E Mail" + "      2 for Postal Service");
				cell.ToolTipText =  "Enter 1 for E Mail" + "      2 for Postal Service";
			}
			else
			{
                //ToolTip1.SetToolTip(vsData, string.Empty);
                cell.ToolTipText = "";
			}
		}

		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Row == CNSTPreference)
			{
				vsData.ComboList = "E-Mail|Postal Service";
			}
			else
			{
				vsData.ComboList = "";
			}
		}

		private void vsData_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsData.Row == CNSTDeliveryAddress || vsData.Row == CNSTLocationAddress)
			{
				if (vsData.EditText.Length > 22)
				{
					MessageBox.Show("Delivery and location addresses may not exceed 22 characters in length", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
				}
			}
		}

		private void vsDeductions_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsDeductions.TextMatrix(vsDeductions.Row, 3, FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, 2)));
			if (vsDeductions.Col == 1)
			{
				vsDeductions.TextMatrix(vsDeductions.Row, vsDeductions.Col, fecherFoundation.Strings.UCase(FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, vsDeductions.Col))));
			}
		}

		private void vsMatch_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsMatch.TextMatrix(vsMatch.Row, 3, FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, 2)));
			if (vsMatch.Col == 1)
			{
				vsMatch.TextMatrix(vsMatch.Row, vsMatch.Col, fecherFoundation.Strings.UCase(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, vsMatch.Col))));
			}
		}

        private void vsDeductions_RowColChange(object sender, System.EventArgs e)
        {
            //FC:FINAL:AM:#4161 - don't set the green cells to editable; it will trigger OnCellBeginEdit
            //if (vsDeductions.Col == 1 || vsDeductions.Col == 2)
            //{
            //	vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            //}
            //else
            //{
            //	vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
            //}
            //if (vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col) == ColorTranslator.ToOle(Color.Lime))
            //{
            //	vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
            //}
            //else
            //{
            //	vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            //}
            if (vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col) == ColorTranslator.ToOle(Color.Lime))
            {
                vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
            }
            else if (vsDeductions.Col == 1 || vsDeductions.Col == 2)
            {
                vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            }
            else
            {
                vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private void vsMatch_RowColChange(object sender, System.EventArgs e)
		{
			if (vsMatch.Col == 1 || vsMatch.Col == 2)
			{
				vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, vsMatch.Col) == ColorTranslator.ToOle(Color.Lime))
			{
				vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
		}
	}
}
