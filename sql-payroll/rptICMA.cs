﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptICMA.
	/// </summary>
	public partial class rptICMA : BaseSectionReport
	{
		public rptICMA()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ICMA";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptICMA InstancePtr
		{
			get
			{
				return (rptICMA)Sys.GetInstance(typeof(rptICMA));
			}
		}

		protected rptICMA _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptICMA	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private FCFileSystem fso = new FCFileSystem();
        StreamReader ts;
		string[] strFileList = null;
		private int intFileIndex;
		int lngPage;
		string strPayDate;

		public void Init(string strListOfFiles)
		{
			// get a list of the files
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strFile = "";
				int X;
				strFileList = Strings.Split(strListOfFiles, ",", -1, CompareConstants.vbTextCompare);
				// strFile = Dir("ICMA/*.prn")
				// x = 0
				// Do While Not strFile = vbNullString
				// x = x + 1
				// ReDim Preserve strFileList(x)
				// strFileList(x) = strFile
				// strFile = Dir
				// Loop
				if (strListOfFiles == string.Empty)
				{
					MessageBox.Show("No ICMA files found");
					this.Close();
					return;
				}
				intFileIndex = 0;
				// Call frmReportViewer.Init(Me)
				modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: true, strAttachmentName: "ICMA");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!ts.EndOfStream)
			{
				eArgs.EOF = false;
			}
			else
			{
				intFileIndex += 1;
				if (intFileIndex > Information.UBound(strFileList, 1))
				{
					ts.Close();
					eArgs.EOF = true;
					return;
				}
				else
				{
					eArgs.EOF = false;
					ts.Close();
					ts = FCFileSystem.OpenText("ICMA/" + strFileList[intFileIndex]);
					this.Fields["grpHeader"].Value = strFileList[intFileIndex];
					strPayDate = GetPlanDate();
					ts.Close();
					ts = FCFileSystem.OpenText("ICMA/" + strFileList[intFileIndex]);
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngPage = 1;
			ts = FCFileSystem.OpenText("ICMA/" + strFileList[intFileIndex]);
			this.Fields["grpHeader"].Value = strFileList[intFileIndex];
			strPayDate = GetPlanDate();
			ts.Close();
			ts = FCFileSystem.OpenText("ICMA/" + strFileList[intFileIndex]);
		}

		private string GetPlanDate()
		{
			string GetPlanDate = "";
			string strRec;
			string strType;
			string strSeq;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetPlanDate = "";
				strRec = ts.ReadLine();
				strType = Strings.Mid(strRec, 7, 2);
				strSeq = Strings.Mid(strRec, 9, 4);
				while (Conversion.Val(strType) != 1 || Conversion.Val(strSeq) != 2)
				{
					strRec = ts.ReadLine();
					strType = Strings.Mid(strRec, 7, 2);
					strSeq = Strings.Mid(strRec, 9, 4);
				}
				if (Conversion.Val(strType) == 1 && Conversion.Val(strSeq) == 2)
				{
					GetPlanDate = Strings.Mid(strRec, 69, 2) + "/" + Strings.Mid(strRec, 71, 2) + "/" + Strings.Mid(strRec, 73, 4);
				}
				return GetPlanDate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetPlanDate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPlanDate;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strRecord = "";
			string strSSN = "";
			try
			{
				if (!ts.EndOfStream)
				{
					Detail.Visible = true;
					txtPOFF.Visible = false;
					strRecord = ts.ReadLine();
					if (Conversion.Val(Strings.Mid(strRecord, 7, 2)) == 1)
					{
						// plan
						txtSourceCode.Visible = false;
						lblSource.Visible = false;
						lblTaxYear.Visible = true;
						txtTaxYear.Visible = true;
						lblPlan.Visible = true;
						lblIRS.Visible = true;
						lblAmount.Text = "Total";
						lblFundID.Text = "Paydate";
						txtRecordTypeCode.Text = Strings.Mid(strRecord, 7, 2);
						txtRecordTypeDescription.Text = "Plan";
						txtName.Text = Strings.Mid(strRecord, 26, 30);
						txtSSN.Text = Strings.Mid(strRecord, 17, 9);
						strRecord = ts.ReadLine();
						txtFundID.Text = Strings.Mid(strRecord, 69, 2) + "/" + Strings.Mid(strRecord, 71, 2) + "/" + Strings.Mid(strRecord, 73, 4);
						txtAmount.Text = Strings.Format(Conversion.Val(Strings.Mid(strRecord, 26, 10)) / 100, "#,###,##0.00");
						txtTaxYear.Text = Strings.Mid(strRecord, 77, 1);
					}
					else if (Conversion.Val(Strings.Mid(strRecord, 7, 2)) == 2)
					{
						// contribution
						lblTaxYear.Visible = true;
						txtTaxYear.Visible = true;
						lblAmount.Text = "Amount";
						lblPlan.Visible = false;
						lblIRS.Visible = false;
						txtSourceCode.Visible = true;
						lblSource.Visible = true;
						lblFundID.Text = "Fund ID";
						txtRecordTypeCode.Text = Strings.Mid(strRecord, 7, 2);
						txtRecordTypeDescription.Text = "Contribution";
						txtName.Text = Strings.Mid(strRecord, 26, 30);
						strSSN = Strings.Mid(strRecord, 17, 9);
						strSSN = Strings.Mid(strSSN, 1, 3) + "-" + Strings.Mid(strSSN, 4, 2) + "-" + Strings.Mid(strSSN, 6);
						txtSSN.Text = strSSN;
						txtPlanNumber.Text = Strings.Mid(strRecord, 1, 6);
						strRecord = ts.ReadLine();
						// read second cont record
						txtFundID.Text = Strings.Mid(strRecord, 13, 2);
						txtSourceCode.Text = Strings.Mid(strRecord, 15, 2);
						txtAmount.Text = Strings.Format(Conversion.Val(Strings.Mid(strRecord, 26, 10)) / 100, "#,###,##0.00");
						txtTaxYear.Text = Strings.Mid(strRecord, 36, 1);
					}
					else if (Conversion.Val(Strings.Mid(strRecord, 7, 2)) == 3)
					{
						// loan
						lblTaxYear.Visible = false;
						txtTaxYear.Visible = false;
						lblAmount.Text = "Amount";
						lblPlan.Visible = false;
						lblIRS.Visible = false;
						txtSourceCode.Visible = false;
						lblFundID.Text = "Loan #";
						lblSource.Visible = false;
						txtRecordTypeCode.Text = Strings.Mid(strRecord, 7, 2);
						txtRecordTypeDescription.Text = "Loan";
						txtName.Text = Strings.Mid(strRecord, 26, 30);
						strSSN = Strings.Mid(strRecord, 17, 9);
						strSSN = Strings.Mid(strSSN, 1, 3) + "-" + Strings.Mid(strSSN, 4, 2) + "-" + Strings.Mid(strSSN, 6);
						txtSSN.Text = strSSN;
						txtPlanNumber.Text = Strings.Mid(strRecord, 1, 6);
						strRecord = ts.ReadLine();
						// read second load record
						txtAmount.Text = Strings.Format(Conversion.Val(Strings.Mid(strRecord, 26, 10)) / 100, "#,###,##0.00");
						if (fecherFoundation.Strings.UCase(Strings.Mid(strRecord, 76, 4)) == "POFF")
						{
							txtPOFF.Visible = true;
						}
						txtFundID.Text = Strings.Mid(strRecord, 14, 3);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				ts.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Detail_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
			txtFileName.Text = FCConvert.ToString(this.Fields["grpHeader"].Value) + "  " + strPayDate;
		}

		

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
