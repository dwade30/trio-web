//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckViewer.
	/// </summary>
	partial class frmCheckViewer
	{
		public ARViewer ARViewer21;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEmail;
		public fecherFoundation.FCToolStripMenuItem mnuEMailRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuRTF;
		public fecherFoundation.FCToolStripMenuItem mnuExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuHTML;
		public fecherFoundation.FCToolStripMenuItem mnuExcel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
        public fecherFoundation.FCToolStripMenuItem mnuPrintMenu;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNonNegotiable;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ARViewer21 = new Global.ARViewer();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuEmail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEMailRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEmailPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExportPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHTML = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExcel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintMenu = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintNonNegotiable = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintNonNegotiable = new fecherFoundation.FCButton();
            this.toolBar1 = new Wisej.Web.ToolBar();
            this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
            this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
            this.toolBarButtonPrintNonNegotiable = new Wisej.Web.ToolBarButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintNonNegotiable)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 624);
            this.BottomPanel.Size = new System.Drawing.Size(673, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.ARViewer21);
            this.ClientArea.Location = new System.Drawing.Point(0, 42);
            this.ClientArea.Size = new System.Drawing.Size(693, 624);
            this.ClientArea.Controls.SetChildIndex(this.ARViewer21, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintNonNegotiable);
            this.TopPanel.Controls.Add(this.toolBar1);
            this.TopPanel.Size = new System.Drawing.Size(693, 42);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.toolBar1, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintNonNegotiable, 0);
            // 
            // ARViewer21
            // 
            this.ARViewer21.Dock = Wisej.Web.DockStyle.Fill;
            this.ARViewer21.Name = "ARViewer21";
            this.ARViewer21.ScrollBars = false;
            this.ARViewer21.Size = new System.Drawing.Size(672, 603);
            this.ARViewer21.TabIndex = 1001;
            this.ARViewer21.KeyDown += new Wisej.Web.KeyEventHandler(this.ARViewer21_KeyDownEvent);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEmail,
            this.mnuExport,
            this.mnuPrintMenu});
            this.MainMenu1.Name = null;
            // 
            // mnuEmail
            // 
            this.mnuEmail.Index = 0;
            this.mnuEmail.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEMailRTF,
            this.mnuEmailPDF});
            this.mnuEmail.Name = "mnuEmail";
            this.mnuEmail.Text = "EMail";
            // 
            // mnuEMailRTF
            // 
            this.mnuEMailRTF.Index = 0;
            this.mnuEMailRTF.Name = "mnuEMailRTF";
            this.mnuEMailRTF.Text = "EMail as Rich Text";
            this.mnuEMailRTF.Click += new System.EventHandler(this.mnuEMailRTF_Click);
            // 
            // mnuEmailPDF
            // 
            this.mnuEmailPDF.Index = 1;
            this.mnuEmailPDF.Name = "mnuEmailPDF";
            this.mnuEmailPDF.Text = "EMail as PDF";
            this.mnuEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
            // 
            // mnuExport
            // 
            this.mnuExport.Index = 1;
            this.mnuExport.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRTF,
            this.mnuExportPDF,
            this.mnuHTML,
            this.mnuExcel});
            this.mnuExport.Name = "mnuExport";
            this.mnuExport.Text = "Export";
            // 
            // mnuRTF
            // 
            this.mnuRTF.Index = 0;
            this.mnuRTF.Name = "mnuRTF";
            this.mnuRTF.Text = "Export as Rich Text";
            this.mnuRTF.Click += new System.EventHandler(this.mnuRTF_Click);
            // 
            // mnuExportPDF
            // 
            this.mnuExportPDF.Index = 1;
            this.mnuExportPDF.Name = "mnuExportPDF";
            this.mnuExportPDF.Text = "Export as PDF";
            this.mnuExportPDF.Click += new System.EventHandler(this.mnuExportPDF_Click);
            // 
            // mnuHTML
            // 
            this.mnuHTML.Index = 2;
            this.mnuHTML.Name = "mnuHTML";
            this.mnuHTML.Text = "Export as HTML";
            this.mnuHTML.Click += new System.EventHandler(this.mnuHTML_Click);
            // 
            // mnuExcel
            // 
            this.mnuExcel.Index = 3;
            this.mnuExcel.Name = "mnuExcel";
            this.mnuExcel.Text = "Export as Excel";
            this.mnuExcel.Click += new System.EventHandler(this.mnuExcel_Click);
            // 
            // mnuPrintMenu
            // 
            this.mnuPrintMenu.Index = 2;
            this.mnuPrintMenu.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuPrintNonNegotiable});
            this.mnuPrintMenu.Name = "mnuPrintMenu";
            this.mnuPrintMenu.Text = "Print";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Negotiable Checks";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintNonNegotiable
            // 
            this.mnuPrintNonNegotiable.Index = 1;
            this.mnuPrintNonNegotiable.Name = "mnuPrintNonNegotiable";
            this.mnuPrintNonNegotiable.Text = "Print Non-Negotiable Checks";
            this.mnuPrintNonNegotiable.Click += new System.EventHandler(this.mnuPrintNonNegotiable_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSepar2,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 0;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Location = new System.Drawing.Point(520, 62);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(133, 43);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdPrintNonNegotiable
            // 
            this.cmdPrintNonNegotiable.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintNonNegotiable.Location = new System.Drawing.Point(495, -30);
            this.cmdPrintNonNegotiable.Name = "cmdPrintNonNegotiable";
            this.cmdPrintNonNegotiable.Size = new System.Drawing.Size(198, 24);
            this.cmdPrintNonNegotiable.TabIndex = 1;
            this.cmdPrintNonNegotiable.Text = "Print Non-Negotiable Checks";
            this.cmdPrintNonNegotiable.Visible = false;
            this.cmdPrintNonNegotiable.Click += new System.EventHandler(this.mnuPrintNonNegotiable_Click);
            // 
            // toolBar1
            // 
            this.toolBar1.AutoSize = false;
            this.toolBar1.BackColor = System.Drawing.Color.FromArgb(244, 247, 249);
            this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
            this.toolBarButtonEmailPDF,
            this.toolBarButtonEmailRTF,
            this.toolBarButtonExportPDF,
            this.toolBarButtonExportRTF,
            this.toolBarButtonExportHTML,
            this.toolBarButtonExportExcel,
            this.toolBarButtonPrint,
            this.toolBarButtonPrintNonNegotiable});
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.Size = new System.Drawing.Size(693, 40);
            this.toolBar1.TabIndex = 1;
            this.toolBar1.TabStop = false;
            // 
            // toolBarButtonEmailPDF
            // 
            this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
            this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
            this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
            // 
            // toolBarButtonEmailRTF
            // 
            this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
            this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
            this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
            // 
            // toolBarButtonExportPDF
            // 
            this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
            this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
            this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
            // 
            // toolBarButtonExportRTF
            // 
            this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
            this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
            this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
            // 
            // toolBarButtonExportHTML
            // 
            this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
            this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
            this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
            // 
            // toolBarButtonExportExcel
            // 
            this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
            this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
            this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
            // 
            // toolBarButtonPrint
            // 
            this.toolBarButtonPrint.AppearanceKey = "toolbarButton";
            this.toolBarButtonPrint.Margin = new Wisej.Web.Padding(20, 2, 10, 2);
            this.toolBarButtonPrint.Name = "toolBarButtonPrint";
            this.toolBarButtonPrint.Padding = new Wisej.Web.Padding(10, 0, 10, 0);
            this.toolBarButtonPrint.Text = " Print Negotiable Checks ";
            // 
            // toolBarButtonPrintNonNegotiable
            // 
            this.toolBarButtonPrintNonNegotiable.AppearanceKey = "toolbarButton";
            this.toolBarButtonPrintNonNegotiable.Margin = new Wisej.Web.Padding(20, 2, 10, 2);
            this.toolBarButtonPrintNonNegotiable.Name = "toolBarButtonPrintNonNegotiable";
            this.toolBarButtonPrintNonNegotiable.Padding = new Wisej.Web.Padding(10, 0, 10, 0);
            this.toolBarButtonPrintNonNegotiable.Text = "Print Non Negotiable Checks";
            // 
            // frmCheckViewer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(693, 666);
            this.FillColor = 0;
            this.Menu = this.MainMenu1;
            this.Name = "frmCheckViewer";
            this.Text = "Report Viewer";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCheckViewer_Load);
            this.Activated += new System.EventHandler(this.frmCheckViewer_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCheckViewer_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintNonNegotiable)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdPrint;
        private FCButton cmdPrintNonNegotiable;
        private ToolBar toolBar1;
        private ToolBarButton toolBarButtonEmailPDF;
        private ToolBarButton toolBarButtonEmailRTF;
        private ToolBarButton toolBarButtonExportPDF;
        private ToolBarButton toolBarButtonExportRTF;
        private ToolBarButton toolBarButtonExportHTML;
        private ToolBarButton toolBarButtonExportExcel;

        private ToolBarButton toolBarButtonPrint;

        private ToolBarButton toolBarButtonPrintNonNegotiable;
        //private ToolBarButton toolBarButtonPrint;
    }
}