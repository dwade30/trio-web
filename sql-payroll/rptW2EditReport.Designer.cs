﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2EditReport.
	/// </summary>
	partial class rptW2EditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2EditReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTTLWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFTXGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMEDGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSTXGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.txtEmpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMQGE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRHMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRHTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRHDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRHPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFederalEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddressField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtContact = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExtension = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtContactMethod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTTLWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFTXGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotSSGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMedGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSTXGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblW2Count = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtW2Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBox12Total = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBox12Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBox14Total = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBox14Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBox10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblEmployee = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTTLWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFTXGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMEDGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSTXGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMQGE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddressField)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContact)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExtension)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTTLWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFTXGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSSGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSTXGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblW2Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox12Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox12Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox14Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployee,
            this.txtAddress,
            this.txtCityStateZip,
            this.txtSSN,
            this.txtPen,
            this.txtDef,
            this.txtTTLWage,
            this.txtFTXGross,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.txtSSGross,
            this.txtMEDGross,
            this.txtSTXGross,
            this.txtFedTax,
            this.txtSSTax,
            this.txtMedTax,
            this.txtStateTax,
            this.SubReport1,
            this.txtEmpID,
            this.txtStatEmployee,
            this.txtMQGE,
            this.txtDepCare,
            this.Label39});
			this.Detail.Height = 1.197917F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2083333F;
			this.txtEmployee.Left = 0.03125F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmployee.Text = "Field1";
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 2F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.2083333F;
			this.txtAddress.Left = 0.125F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress.Text = "Field1";
			this.txtAddress.Top = 0.2083333F;
			this.txtAddress.Width = 2.0625F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.Height = 0.2083333F;
			this.txtCityStateZip.Left = 0.125F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCityStateZip.Text = "Field1";
			this.txtCityStateZip.Top = 0.4166667F;
			this.txtCityStateZip.Width = 2.0625F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.2083333F;
			this.txtSSN.Left = 0.125F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSSN.Text = "Field1";
			this.txtSSN.Top = 0.625F;
			this.txtSSN.Width = 1.28125F;
			// 
			// txtPen
			// 
			this.txtPen.Height = 0.2083333F;
			this.txtPen.Left = 1.40625F;
			this.txtPen.Name = "txtPen";
			this.txtPen.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtPen.Text = null;
			this.txtPen.Top = 0.625F;
			this.txtPen.Width = 0.75F;
			// 
			// txtDef
			// 
			this.txtDef.Height = 0.2083333F;
			this.txtDef.Left = 2.25F;
			this.txtDef.Name = "txtDef";
			this.txtDef.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDef.Text = null;
			this.txtDef.Top = 0.625F;
			this.txtDef.Width = 0.71875F;
			// 
			// txtTTLWage
			// 
			this.txtTTLWage.Height = 0.2083333F;
			this.txtTTLWage.Left = 2.1875F;
			this.txtTTLWage.Name = "txtTTLWage";
			this.txtTTLWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTTLWage.Text = null;
			this.txtTTLWage.Top = 0F;
			this.txtTTLWage.Width = 1.1875F;
			// 
			// txtFTXGross
			// 
			this.txtFTXGross.Height = 0.2083333F;
			this.txtFTXGross.Left = 4.125F;
			this.txtFTXGross.Name = "txtFTXGross";
			this.txtFTXGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFTXGross.Text = null;
			this.txtFTXGross.Top = 0F;
			this.txtFTXGross.Width = 0.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.2083333F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.4375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label11.Text = "FTX Gross";
			this.Label11.Top = 0F;
			this.Label11.Width = 0.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.2083333F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.4375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label12.Text = "SS Gross";
			this.Label12.Top = 0.2083333F;
			this.Label12.Width = 0.6875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.2083333F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.4375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label13.Text = "MED Gross";
			this.Label13.Top = 0.4166667F;
			this.Label13.Width = 0.6875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.2083333F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.4375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label14.Text = "STX Gross";
			this.Label14.Top = 0.625F;
			this.Label14.Width = 0.6875F;
			// 
			// txtSSGross
			// 
			this.txtSSGross.Height = 0.2083333F;
			this.txtSSGross.Left = 4.125F;
			this.txtSSGross.Name = "txtSSGross";
			this.txtSSGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSSGross.Text = null;
			this.txtSSGross.Top = 0.2083333F;
			this.txtSSGross.Width = 0.875F;
			// 
			// txtMEDGross
			// 
			this.txtMEDGross.Height = 0.2083333F;
			this.txtMEDGross.Left = 4.125F;
			this.txtMEDGross.Name = "txtMEDGross";
			this.txtMEDGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMEDGross.Text = null;
			this.txtMEDGross.Top = 0.4166667F;
			this.txtMEDGross.Width = 0.875F;
			// 
			// txtSTXGross
			// 
			this.txtSTXGross.Height = 0.2083333F;
			this.txtSTXGross.Left = 4.125F;
			this.txtSTXGross.Name = "txtSTXGross";
			this.txtSTXGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSTXGross.Text = null;
			this.txtSTXGross.Top = 0.625F;
			this.txtSTXGross.Width = 0.875F;
			// 
			// txtFedTax
			// 
			this.txtFedTax.Height = 0.2083333F;
			this.txtFedTax.Left = 5.1875F;
			this.txtFedTax.Name = "txtFedTax";
			this.txtFedTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFedTax.Text = null;
			this.txtFedTax.Top = 0F;
			this.txtFedTax.Width = 0.96875F;
			// 
			// txtSSTax
			// 
			this.txtSSTax.Height = 0.2083333F;
			this.txtSSTax.Left = 6.15625F;
			this.txtSSTax.Name = "txtSSTax";
			this.txtSSTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtSSTax.Text = null;
			this.txtSSTax.Top = 0F;
			this.txtSSTax.Width = 0.90625F;
			// 
			// txtMedTax
			// 
			this.txtMedTax.Height = 0.2083333F;
			this.txtMedTax.Left = 7.0625F;
			this.txtMedTax.Name = "txtMedTax";
			this.txtMedTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedTax.Text = null;
			this.txtMedTax.Top = 0F;
			this.txtMedTax.Width = 0.90625F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.2083333F;
			this.txtStateTax.Left = 7.96875F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 0F;
			this.txtStateTax.Width = 0.90625F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.1041667F;
			this.SubReport1.Left = 0.03125F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 1.083333F;
			this.SubReport1.Width = 9.6875F;
			// 
			// txtEmpID
			// 
			this.txtEmpID.Height = 0.1875F;
			this.txtEmpID.Left = 2.4375F;
			this.txtEmpID.Name = "txtEmpID";
			this.txtEmpID.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmpID.Text = null;
			this.txtEmpID.Top = 0.1875F;
			this.txtEmpID.Visible = false;
			this.txtEmpID.Width = 0.75F;
			// 
			// txtStatEmployee
			// 
			this.txtStatEmployee.Height = 0.2083333F;
			this.txtStatEmployee.Left = 1.40625F;
			this.txtStatEmployee.Name = "txtStatEmployee";
			this.txtStatEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtStatEmployee.Text = null;
			this.txtStatEmployee.Top = 0.875F;
			this.txtStatEmployee.Width = 1.5625F;
			// 
			// txtMQGE
			// 
			this.txtMQGE.Height = 0.1875F;
			this.txtMQGE.Left = 2.25F;
			this.txtMQGE.Name = "txtMQGE";
			this.txtMQGE.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMQGE.Text = null;
			this.txtMQGE.Top = 0.4375F;
			this.txtMQGE.Width = 0.75F;
			// 
			// txtDepCare
			// 
			this.txtDepCare.Height = 0.2083333F;
			this.txtDepCare.Left = 5.6875F;
			this.txtDepCare.Name = "txtDepCare";
			this.txtDepCare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDepCare.Text = null;
			this.txtDepCare.Top = 0.625F;
			this.txtDepCare.Width = 0.875F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.2083333F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 5.1875F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label39.Text = "Box 10";
			this.Label39.Top = 0.625F;
			this.Label39.Width = 0.4375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label18,
            this.txtRHMuniName,
            this.Label19,
            this.txtRHTime,
            this.txtRHDate,
            this.txtRHPage,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.txtFederalEIN,
            this.txtStateEIN,
            this.txtState,
            this.txtName,
            this.txtAddressField,
            this.txtCity,
            this.txtContact,
            this.txtPhone,
            this.txtExtension,
            this.txtFax,
            this.txtContactMethod,
            this.txtEmail});
			this.ReportHeader.Height = 3.291667F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			// 
			// Label18
			// 
			this.Label18.Height = 0.2083333F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 1.4375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label18.Text = "PAYROLL -  W2 Edit Report";
			this.Label18.Top = 0F;
			this.Label18.Width = 6.875F;
			// 
			// txtRHMuniName
			// 
			this.txtRHMuniName.Height = 0.2083333F;
			this.txtRHMuniName.Left = 0F;
			this.txtRHMuniName.Name = "txtRHMuniName";
			this.txtRHMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtRHMuniName.Text = null;
			this.txtRHMuniName.Top = 0F;
			this.txtRHMuniName.Width = 1.4375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.2083333F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.4375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label19.Text = "W2 File";
			this.Label19.Top = 0.2083333F;
			this.Label19.Width = 6.875F;
			// 
			// txtRHTime
			// 
			this.txtRHTime.Height = 0.2083333F;
			this.txtRHTime.Left = 0F;
			this.txtRHTime.Name = "txtRHTime";
			this.txtRHTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtRHTime.Text = null;
			this.txtRHTime.Top = 0.2083333F;
			this.txtRHTime.Width = 1.4375F;
			// 
			// txtRHDate
			// 
			this.txtRHDate.Height = 0.2083333F;
			this.txtRHDate.Left = 8.3125F;
			this.txtRHDate.Name = "txtRHDate";
			this.txtRHDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtRHDate.Text = null;
			this.txtRHDate.Top = 0F;
			this.txtRHDate.Width = 1.4375F;
			// 
			// txtRHPage
			// 
			this.txtRHPage.Height = 0.2083333F;
			this.txtRHPage.Left = 8.3125F;
			this.txtRHPage.Name = "txtRHPage";
			this.txtRHPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtRHPage.Text = null;
			this.txtRHPage.Top = 0.2083333F;
			this.txtRHPage.Width = 1.4375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.2083333F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label20.Text = "Federal EIN";
			this.Label20.Top = 0.7083333F;
			this.Label20.Width = 0.96875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.2083333F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.125F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label21.Text = "State EIN";
			this.Label21.Top = 0.9166667F;
			this.Label21.Width = 0.96875F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.2083333F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label22.Text = "State";
			this.Label22.Top = 1.125F;
			this.Label22.Width = 0.96875F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.2083333F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.125F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label23.Text = "Name";
			this.Label23.Top = 1.333333F;
			this.Label23.Width = 0.96875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.2083333F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.125F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label24.Text = "Address";
			this.Label24.Top = 1.541667F;
			this.Label24.Width = 0.96875F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.2083333F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label25.Text = "City/State/Zip";
			this.Label25.Top = 1.75F;
			this.Label25.Width = 1.0625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.2083333F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label26.Text = "Contact";
			this.Label26.Top = 2F;
			this.Label26.Width = 0.96875F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.2083333F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label27.Text = "Telephone";
			this.Label27.Top = 2.208333F;
			this.Label27.Width = 0.96875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.2083333F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label28.Text = "Extension";
			this.Label28.Top = 2.416667F;
			this.Label28.Width = 0.96875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.2083333F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.125F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label29.Text = "Fax";
			this.Label29.Top = 2.625F;
			this.Label29.Width = 0.96875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.2083333F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.125F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label30.Text = "Contact Preference";
			this.Label30.Top = 2.833333F;
			this.Label30.Width = 1.25F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.2083333F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label31.Text = "EMail";
			this.Label31.Top = 3.041667F;
			this.Label31.Width = 0.96875F;
			// 
			// txtFederalEIN
			// 
			this.txtFederalEIN.Height = 0.2083333F;
			this.txtFederalEIN.Left = 1.4375F;
			this.txtFederalEIN.Name = "txtFederalEIN";
			this.txtFederalEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFederalEIN.Text = null;
			this.txtFederalEIN.Top = 0.7083333F;
			this.txtFederalEIN.Width = 3.53125F;
			// 
			// txtStateEIN
			// 
			this.txtStateEIN.Height = 0.2083333F;
			this.txtStateEIN.Left = 1.4375F;
			this.txtStateEIN.Name = "txtStateEIN";
			this.txtStateEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtStateEIN.Text = null;
			this.txtStateEIN.Top = 0.9166667F;
			this.txtStateEIN.Width = 3.53125F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.2083333F;
			this.txtState.Left = 1.4375F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtState.Text = null;
			this.txtState.Top = 1.125F;
			this.txtState.Width = 3.53125F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.2083333F;
			this.txtName.Left = 1.4375F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtName.Text = null;
			this.txtName.Top = 1.333333F;
			this.txtName.Width = 3.53125F;
			// 
			// txtAddressField
			// 
			this.txtAddressField.Height = 0.2083333F;
			this.txtAddressField.Left = 1.4375F;
			this.txtAddressField.Name = "txtAddressField";
			this.txtAddressField.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddressField.Text = null;
			this.txtAddressField.Top = 1.541667F;
			this.txtAddressField.Width = 3.53125F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.2083333F;
			this.txtCity.Left = 1.4375F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCity.Text = null;
			this.txtCity.Top = 1.75F;
			this.txtCity.Width = 3.53125F;
			// 
			// txtContact
			// 
			this.txtContact.Height = 0.2083333F;
			this.txtContact.Left = 1.4375F;
			this.txtContact.Name = "txtContact";
			this.txtContact.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtContact.Text = null;
			this.txtContact.Top = 1.958333F;
			this.txtContact.Width = 3.53125F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.2083333F;
			this.txtPhone.Left = 1.4375F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 2.166667F;
			this.txtPhone.Width = 3.53125F;
			// 
			// txtExtension
			// 
			this.txtExtension.Height = 0.2083333F;
			this.txtExtension.Left = 1.4375F;
			this.txtExtension.Name = "txtExtension";
			this.txtExtension.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtExtension.Text = null;
			this.txtExtension.Top = 2.375F;
			this.txtExtension.Width = 3.53125F;
			// 
			// txtFax
			// 
			this.txtFax.Height = 0.2083333F;
			this.txtFax.Left = 1.4375F;
			this.txtFax.Name = "txtFax";
			this.txtFax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFax.Text = null;
			this.txtFax.Top = 2.583333F;
			this.txtFax.Width = 3.53125F;
			// 
			// txtContactMethod
			// 
			this.txtContactMethod.Height = 0.2083333F;
			this.txtContactMethod.Left = 1.4375F;
			this.txtContactMethod.Name = "txtContactMethod";
			this.txtContactMethod.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtContactMethod.Text = null;
			this.txtContactMethod.Top = 2.833333F;
			this.txtContactMethod.Width = 3.53125F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.2083333F;
			this.txtEmail.Left = 1.4375F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 3.041667F;
			this.txtEmail.Width = 3.53125F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.txtTotTTLWage,
            this.txtTotFTXGross,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Label35,
            this.txtTotSSGross,
            this.txtTotMedGross,
            this.txtTotSTXGross,
            this.txtTotFedTax,
            this.txtTotSSTax,
            this.txtTotMedTax,
            this.txtTotStateTax,
            this.lblW2Count,
            this.txtW2Count,
            this.lblBox12Total,
            this.txtBox12Total,
            this.lblBox14Total,
            this.txtBox14Total,
            this.lblBox10,
            this.txtDependentCare});
			this.ReportFooter.Height = 1.375F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.78125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Totals";
			this.Field1.Top = 0F;
			this.Field1.Width = 0.71875F;
			// 
			// txtTotTTLWage
			// 
			this.txtTotTTLWage.Height = 0.2083333F;
			this.txtTotTTLWage.Left = 2.1875F;
			this.txtTotTTLWage.Name = "txtTotTTLWage";
			this.txtTotTTLWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotTTLWage.Text = null;
			this.txtTotTTLWage.Top = 0F;
			this.txtTotTTLWage.Width = 1.1875F;
			// 
			// txtTotFTXGross
			// 
			this.txtTotFTXGross.Height = 0.2083333F;
			this.txtTotFTXGross.Left = 4.125F;
			this.txtTotFTXGross.Name = "txtTotFTXGross";
			this.txtTotFTXGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotFTXGross.Text = null;
			this.txtTotFTXGross.Top = 0F;
			this.txtTotFTXGross.Width = 0.875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.2083333F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 3.4375F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label32.Text = "FTX Gross";
			this.Label32.Top = 0F;
			this.Label32.Width = 0.6875F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.2083333F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.4375F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label33.Text = "SS Gross";
			this.Label33.Top = 0.2083333F;
			this.Label33.Width = 0.6875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.2083333F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 3.4375F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label34.Text = "MED Gross";
			this.Label34.Top = 0.4166667F;
			this.Label34.Width = 0.6875F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.2083333F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 3.4375F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label35.Text = "STX Gross";
			this.Label35.Top = 0.625F;
			this.Label35.Width = 0.6875F;
			// 
			// txtTotSSGross
			// 
			this.txtTotSSGross.Height = 0.2083333F;
			this.txtTotSSGross.Left = 4.125F;
			this.txtTotSSGross.Name = "txtTotSSGross";
			this.txtTotSSGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotSSGross.Text = null;
			this.txtTotSSGross.Top = 0.2083333F;
			this.txtTotSSGross.Width = 0.875F;
			// 
			// txtTotMedGross
			// 
			this.txtTotMedGross.Height = 0.2083333F;
			this.txtTotMedGross.Left = 4.125F;
			this.txtTotMedGross.Name = "txtTotMedGross";
			this.txtTotMedGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotMedGross.Text = null;
			this.txtTotMedGross.Top = 0.4166667F;
			this.txtTotMedGross.Width = 0.875F;
			// 
			// txtTotSTXGross
			// 
			this.txtTotSTXGross.Height = 0.2083333F;
			this.txtTotSTXGross.Left = 4.125F;
			this.txtTotSTXGross.Name = "txtTotSTXGross";
			this.txtTotSTXGross.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotSTXGross.Text = null;
			this.txtTotSTXGross.Top = 0.625F;
			this.txtTotSTXGross.Width = 0.875F;
			// 
			// txtTotFedTax
			// 
			this.txtTotFedTax.Height = 0.2083333F;
			this.txtTotFedTax.Left = 5.1875F;
			this.txtTotFedTax.Name = "txtTotFedTax";
			this.txtTotFedTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotFedTax.Text = null;
			this.txtTotFedTax.Top = 0F;
			this.txtTotFedTax.Width = 0.96875F;
			// 
			// txtTotSSTax
			// 
			this.txtTotSSTax.Height = 0.2083333F;
			this.txtTotSSTax.Left = 6.15625F;
			this.txtTotSSTax.Name = "txtTotSSTax";
			this.txtTotSSTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotSSTax.Text = null;
			this.txtTotSSTax.Top = 0F;
			this.txtTotSSTax.Width = 0.90625F;
			// 
			// txtTotMedTax
			// 
			this.txtTotMedTax.Height = 0.2083333F;
			this.txtTotMedTax.Left = 7.0625F;
			this.txtTotMedTax.Name = "txtTotMedTax";
			this.txtTotMedTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotMedTax.Text = null;
			this.txtTotMedTax.Top = 0F;
			this.txtTotMedTax.Width = 0.90625F;
			// 
			// txtTotStateTax
			// 
			this.txtTotStateTax.Height = 0.2083333F;
			this.txtTotStateTax.Left = 7.96875F;
			this.txtTotStateTax.Name = "txtTotStateTax";
			this.txtTotStateTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotStateTax.Text = null;
			this.txtTotStateTax.Top = 0F;
			this.txtTotStateTax.Width = 0.90625F;
			// 
			// lblW2Count
			// 
			this.lblW2Count.Height = 0.2F;
			this.lblW2Count.HyperLink = null;
			this.lblW2Count.Left = 6.433333F;
			this.lblW2Count.Name = "lblW2Count";
			this.lblW2Count.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblW2Count.Text = "Number of W2\'s";
			this.lblW2Count.Top = 0.3F;
			this.lblW2Count.Width = 1.1F;
			// 
			// txtW2Count
			// 
			this.txtW2Count.Height = 0.1666667F;
			this.txtW2Count.Left = 7.566667F;
			this.txtW2Count.Name = "txtW2Count";
			this.txtW2Count.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtW2Count.Text = null;
			this.txtW2Count.Top = 0.3F;
			this.txtW2Count.Width = 0.9666666F;
			// 
			// lblBox12Total
			// 
			this.lblBox12Total.Height = 0.2F;
			this.lblBox12Total.HyperLink = null;
			this.lblBox12Total.Left = 6.433333F;
			this.lblBox12Total.Name = "lblBox12Total";
			this.lblBox12Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBox12Total.Text = "Total for Box 12";
			this.lblBox12Total.Top = 0.5F;
			this.lblBox12Total.Width = 1.1F;
			// 
			// txtBox12Total
			// 
			this.txtBox12Total.Height = 0.1666667F;
			this.txtBox12Total.Left = 7.566667F;
			this.txtBox12Total.Name = "txtBox12Total";
			this.txtBox12Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtBox12Total.Text = null;
			this.txtBox12Total.Top = 0.5F;
			this.txtBox12Total.Width = 0.9666666F;
			// 
			// lblBox14Total
			// 
			this.lblBox14Total.Height = 0.2F;
			this.lblBox14Total.HyperLink = null;
			this.lblBox14Total.Left = 6.433333F;
			this.lblBox14Total.Name = "lblBox14Total";
			this.lblBox14Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBox14Total.Text = "Total for Box 14";
			this.lblBox14Total.Top = 0.7F;
			this.lblBox14Total.Width = 1.1F;
			// 
			// txtBox14Total
			// 
			this.txtBox14Total.Height = 0.1666667F;
			this.txtBox14Total.Left = 7.566667F;
			this.txtBox14Total.Name = "txtBox14Total";
			this.txtBox14Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtBox14Total.Text = null;
			this.txtBox14Total.Top = 0.7F;
			this.txtBox14Total.Width = 0.9666666F;
			// 
			// lblBox10
			// 
			this.lblBox10.Height = 0.2F;
			this.lblBox10.HyperLink = null;
			this.lblBox10.Left = 6.433333F;
			this.lblBox10.Name = "lblBox10";
			this.lblBox10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBox10.Text = "Total for Box 10";
			this.lblBox10.Top = 0.9F;
			this.lblBox10.Width = 1.1F;
			// 
			// txtDependentCare
			// 
			this.txtDependentCare.Height = 0.1666667F;
			this.txtDependentCare.Left = 7.566667F;
			this.txtDependentCare.Name = "txtDependentCare";
			this.txtDependentCare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDependentCare.Text = null;
			this.txtDependentCare.Top = 0.9F;
			this.txtDependentCare.Width = 0.9666666F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblEmployee,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.txtLabel,
            this.txtMuniName,
            this.txtSubLabel,
            this.txtTime,
            this.txtDate,
            this.txtPage});
			this.PageHeader.Height = 0.9583333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblEmployee
			// 
			this.lblEmployee.Height = 0.2083333F;
			this.lblEmployee.HyperLink = null;
			this.lblEmployee.Left = 0.03125F;
			this.lblEmployee.Name = "lblEmployee";
			this.lblEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.lblEmployee.Text = "Employee - - - - - - - -";
			this.lblEmployee.Top = 0.75F;
			this.lblEmployee.Width = 1.96875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.40625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label4.Text = "TTL Wages";
			this.Label4.Top = 0.75F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.53125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label5.Text = "Wage Breakdown";
			this.Label5.Top = 0.75F;
			this.Label5.Width = 1.40625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.2083333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.40625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label6.Text = "Fed Tax";
			this.Label6.Top = 0.75F;
			this.Label6.Width = 0.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2083333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.3125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label7.Text = "SS Tax";
			this.Label7.Top = 0.75F;
			this.Label7.Width = 0.78125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.2083333F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 7.15625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label8.Text = "Med Tax";
			this.Label8.Top = 0.75F;
			this.Label8.Width = 0.78125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.2083333F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 8.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label9.Text = "State Tax";
			this.Label9.Top = 0.75F;
			this.Label9.Width = 0.78125F;
			// 
			// txtLabel
			// 
			this.txtLabel.Height = 0.2083333F;
			this.txtLabel.HyperLink = null;
			this.txtLabel.Left = 1.4375F;
			this.txtLabel.Name = "txtLabel";
			this.txtLabel.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtLabel.Text = "PAYROLL -  W2 Edit Report";
			this.txtLabel.Top = 0F;
			this.txtLabel.Width = 6.875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.2083333F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtSubLabel
			// 
			this.txtSubLabel.Height = 0.2083333F;
			this.txtSubLabel.HyperLink = null;
			this.txtSubLabel.Left = 1.4375F;
			this.txtSubLabel.Name = "txtSubLabel";
			this.txtSubLabel.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtSubLabel.Text = "Live File";
			this.txtSubLabel.Top = 0.2083333F;
			this.txtSubLabel.Width = 6.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.3125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.3125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptW2EditReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5208333F;
			this.PageSettings.Margins.Right = 0.5208333F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 8.5F;
			this.PageSettings.PaperWidth = 11F;
			this.PrintWidth = 9.78125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTTLWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFTXGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMEDGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSTXGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMQGE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRHPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddressField)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContact)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExtension)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTTLWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFTXGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSSGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSTXGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblW2Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox12Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox12Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox14Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBox14Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTTLWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFTXGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMEDGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSTXGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMQGE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepCare;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRHMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRHTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRHDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRHPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddressField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContact;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExtension;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactMethod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTTLWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFTXGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSSGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMedGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSTXGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblW2Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtW2Count;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBox12Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox12Total;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBox14Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBox14Total;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBox10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSubLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
