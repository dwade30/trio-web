//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptAccountingSummaryTrustReversal.
	/// </summary>
	public partial class srptAccountingSummaryTrustReversal : FCSectionReport
	{
		public srptAccountingSummaryTrustReversal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptAccountingSummaryTrustReversal InstancePtr
		{
			get
			{
				return (srptAccountingSummaryTrustReversal)Sys.GetInstance(typeof(srptAccountingSummaryTrustReversal));
			}
		}

		protected srptAccountingSummaryTrustReversal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo?.Dispose();
                rsAccountInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAccountingSummaryTrustReversal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Summary Type Binder Values
		// P - Paid Section
		// C - Checking Account Section
		// X - Done witrh report
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		string strSummaryType = "";
		bool blnFirstRecord;
		string strHeaderType;
		// vbPorter upgrade warning: curCredits As Decimal	OnWrite(int, Decimal)
		Decimal curCredits;
		// vbPorter upgrade warning: curDebits As Decimal	OnWrite(int, Decimal)
		Decimal curDebits;
		// vbPorter upgrade warning: curPaidTotal As Decimal	OnWrite(int, Decimal)
		Decimal curPaidTotal;
		// vbPorter upgrade warning: curFederalTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFederalTaxWH;
		// vbPorter upgrade warning: curFICATaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFICATaxWH;
		// vbPorter upgrade warning: curMedicareTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curMedicareTaxWH;
		// vbPorter upgrade warning: curStateTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curStateTaxWH;
		// vbPorter upgrade warning: curDeductionsWH As Decimal	OnWrite
		Decimal curDeductionsWH;
		bool blnShowedPaidLabel;
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		int intMultiFundNumber;
		bool pboolMuliFund;
		bool boolPreviousFiscal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("SummaryTypeBinder");
			boolPreviousFiscal = rptPayrollAccountingChargesTrustReversal.InstancePtr.boolPreviousFiscal;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				CheckRecord:
				;
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				CheckRecord:
				;
				rsAccountInfo.MoveNext();
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["SummaryTypeBinder"].Value = strSummaryType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strSummaryType = "P";
			strHeaderType = "P";
			blnFirstRecord = true;
			SetAccountInfoRecordset();
			curPaidTotal = 0;
			curCredits = 0;
			curDebits = 0;
			// curGrossPay = 0
			curFederalTaxWH = 0;
			curStateTaxWH = 0;
			curFICATaxWH = 0;
			curMedicareTaxWH = 0;
			curDeductionsWH = 0;
			blnShowedPaidLabel = false;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strSummaryType == "P")
			{
				ShowTrustRecord();
			}
			else if (strSummaryType == "C")
			{
				ShowCheckingRecord();
			}
		}

		private void SetAccountInfoRecordset()
		{
			if (strSummaryType == "P")
			{
				// MATTHEW MULTI CALL ID 79810 11/1/2005
				rsAccountInfo.OpenRecordset("SELECT MultiFundNumber, TrustCategoryID, TrustDeductionAccountNumber, SUM(TrustAmount) as TrustAmountTotal, TrustDeductionDescription FROM tblCheckDetail WHERE TrustRecord = 1 AND CheckNumber = " + FCConvert.ToString(rptPayrollAccountingChargesTrustReversal.InstancePtr.lngCheckNumber) + " AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesTrustReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesTrustReversal.InstancePtr.intPayRunID) + " GROUP BY TrustCategoryID, TrustDeductionDescription, TrustDeductionAccountNumber, MultiFundNumber HAVING SUM(TrustAmount) <> 0");
				// rsAccountInfo.OpenRecordset "SELECT TrustCategoryID, TrustDeductionAccountNumber, SUM(TrustAmount) as TrustAmountTotal, TrustDeductionDescription FROM tblCheckDetail WHERE TrustRecord = 1 AND CheckNumber = " & rptPayrollAccountingChargesTrustReversal.lngCheckNumber & " AND PayDate = '" & rptPayrollAccountingChargesTrustReversal.datPayDate & "' AND PayRunID = " & rptPayrollAccountingChargesTrustReversal.intPayRunID & " GROUP BY TrustCategoryID, TrustDeductionDescription, TrustDeductionAccountNumber HAVING SUM(TrustAmount) <> 0"
				if (!rsAccountInfo.EndOfFile())
				{
					intMultiFundNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsAccountInfo.Get_Fields_Int32("MultiFundNumber"))));
				}
				else
				{
					intMultiFundNumber = 0;
				}
			}
			else if (strSummaryType == "C")
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
			}
		}

		private void IncrementSummaryType()
		{
			if (strSummaryType == "P")
			{
				strSummaryType = "C";
			}
			else if (strSummaryType == "C")
			{
				strSummaryType = "X";
			}
		}

		private void ShowCheckingRecord()
		{
			// VB6 Bad Scope Dim:
			string strText = "";
			// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToDouble(
			int intStart;
			
			string strAccount;
			fldDescription.Text = "Checking";
			strAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("account"));
			if (intMultiFundNumber == 0)
			{
				// fldAccount = ShowAccountCorrectly(strAccount)
			}
			else
			{
				// ***********************************************************
				// MATTHEW MULTI CALL ID 79810 11/1/2005
				// If pboolMuliFund Then
				// NEED TO C
				strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
				// corey 01/30/2006 Need to format fund
				if (Strings.Left(fecherFoundation.Strings.Trim(strText), 2) == "**")
				{
					Strings.MidSet(ref strText, 5, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
				}
				else
				{
					Strings.MidSet(ref strText, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
				}
				fldAccount.Text = strText;
				// End If
				strAccount = strText;
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				string strOVAccount = "";
				string strOVSCHAccount = "";
				strOVAccount = "";
				strOVSCHAccount = "";
				if (boolPreviousFiscal)
				{
                    using (clsDRWrapper rsTemp = new clsDRWrapper())
                    {
                        rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
                        if (!rsTemp.EndOfFile())
                        {
                            strOVAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
                        }

                        // Call rsTemp.OpenRecordset("select * from standardaccounts where code = 'SAP'", "twbd0000.vb1")
                        // If Not rsTemp.EndOfFile Then
                        // strOVSCHAccount = rsTemp.Fields("account")
                        // End If
                        if (strOVAccount != string.Empty)
                        {
                            intStart = FCConvert.ToInt16(
                                3 + Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1);
                            Strings.MidSet(ref strAccount, intStart,
                                Strings.Left(modAccountTitle.Statics.Ledger, 3).Length, strOVAccount);
                            fldAccount.Text = strText;
                        }
                    }
                }
			}
			fldAccount.Text = ShowAccountCorrectly(ref strAccount);
			fldDebits.Text = Strings.Format(curPaidTotal, "#,##0.00");
			curDebits += curPaidTotal;
			fldCredits.Text = "";
		}

		private void ShowTrustRecord()
		{
			fldDescription.Text = rsAccountInfo.Get_Fields_String("TrustDeductionDescription");
			fldAccount.Text = ShowAccountCorrectly_1(rsAccountInfo.Get_Fields_String("TrustDeductionAccountNumber"));
			fldCredits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("TrustAmountTotal")), "#,##0.00");
			fldDebits.Text = "";
			curCredits += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
			curPaidTotal += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
			switch (rsAccountInfo.Get_Fields_Int32("TrustCategoryID"))
			{
				case 1:
					{
						curFederalTaxWH += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
						break;
					}
				case 2:
					{
						curFICATaxWH += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
						break;
					}
				case 3:
					{
						curMedicareTaxWH += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
						break;
					}
				case 4:
					{
						curStateTaxWH += FCConvert.ToDecimal(rsAccountInfo.Get_Fields("TrustAmountTotal"));
						break;
					}
			}
			//end switch
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTotalDebits.Text = Strings.Format(curDebits, "#,##0.00");
			fldTotalCredits.Text = Strings.Format(curCredits, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (strSummaryType == "C")
			{
				if (blnShowedPaidLabel)
				{
					lblTitle.Visible = false;
					lblTitle.Text = "";
				}
				else
				{
					lblTitle.Visible = true;
					lblTitle.Text = "Returned" + Strings.StrDup(22, "-");
					blnShowedPaidLabel = true;
				}
			}
			else
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Paid" + Strings.StrDup(26, "-");
				blnShowedPaidLabel = true;
			}
			strHeaderType = strSummaryType;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			
			fldFederalTaxWH.Text = Strings.Format(curFederalTaxWH, "#,##0.00");
			fldStateTaxWH.Text = Strings.Format(curStateTaxWH, "#,##0.00");
			fldFICATaxWH.Text = Strings.Format(curFICATaxWH, "#,##0.00");
			fldMedicareTaxWH.Text = Strings.Format(curMedicareTaxWH, "#,##0.00");
			fldDeductionsWH.Text = Strings.Format(curDeductionsWH, "#,##0.00");
			fldNetPay.Text = Strings.Format(curPaidTotal, "#,##0.00");
			fldPeriod.Text = Strings.Format(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate.Month, "00");
			fldJournalNumber.Text = Strings.Format(frmVoidCheck.InstancePtr.lngJournalNumber, "0000");
		}

		private string ShowAccountCorrectly_1(string strAccount)
		{
			return ShowAccountCorrectly(ref strAccount);
		}
		private string ShowAccountCorrectly(ref string strAccount)
		{
			string ShowAccountCorrectly = "";
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			if (!modValidateAccount.AccountValidate(strAccount))
			{
				ShowAccountCorrectly = "**" + strAccount;
				rptPayrollAccountingChargesTrustReversal.InstancePtr.blnShowBadAccountLabel = true;
			}
			else
			{
				ShowAccountCorrectly = strAccount;
			}
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
			return ShowAccountCorrectly;
		}
	}
}
