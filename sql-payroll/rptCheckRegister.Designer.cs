﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckRegister.
	/// </summary>
	partial class rptCheckRegister
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckRegister));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReprint = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldRegularCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRegularTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTATotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDDTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVoidedCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTACount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDDCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEmployeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.CheckType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCheckType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalDD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTATotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDDTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVoidedCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDDCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CheckType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldCheck,
            this.fldNetPay,
            this.fldDate,
            this.fldPayee,
            this.fldDirectDeposit,
            this.fldCheckAmount});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.19F;
			this.fldCheck.Left = 0.8125F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.59375F;
			// 
			// fldNetPay
			// 
			this.fldNetPay.Height = 0.19F;
			this.fldNetPay.Left = 3.28125F;
			this.fldNetPay.Name = "fldNetPay";
			this.fldNetPay.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldNetPay.Text = "Field1";
			this.fldNetPay.Top = 0F;
			this.fldNetPay.Width = 0.90625F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 4.25F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.59375F;
			// 
			// fldPayee
			// 
			this.fldPayee.Height = 0.19F;
			this.fldPayee.Left = 5.03125F;
			this.fldPayee.Name = "fldPayee";
			this.fldPayee.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldPayee.Text = "yyyy";
			this.fldPayee.Top = 0F;
			this.fldPayee.Width = 2.46875F;
			// 
			// fldDirectDeposit
			// 
			this.fldDirectDeposit.Height = 0.19F;
			this.fldDirectDeposit.Left = 1.4375F;
			this.fldDirectDeposit.Name = "fldDirectDeposit";
			this.fldDirectDeposit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDirectDeposit.Text = "Field1";
			this.fldDirectDeposit.Top = 0F;
			this.fldDirectDeposit.Width = 0.84375F;
			// 
			// fldCheckAmount
			// 
			this.fldCheckAmount.Height = 0.19F;
			this.fldCheckAmount.Left = 2.3125F;
			this.fldCheckAmount.Name = "fldCheckAmount";
			this.fldCheckAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheckAmount.Text = "Field1";
			this.fldCheckAmount.Top = 0F;
			this.fldCheckAmount.Width = 0.90625F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanShrink = true;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label13,
            this.Line1,
            this.lblPayDate,
            this.Label19,
            this.Label20,
            this.lblReprint});
			this.PageHeader.Height = 0.9479167F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.229F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.Label1.Text = "Payroll Check Register";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.688F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Check";
			this.Label9.Top = 0.75F;
			this.Label9.Width = 0.5625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.4375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "D / D";
			this.Label10.Top = 0.75F;
			this.Label10.Width = 0.84375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "Date";
			this.Label11.Top = 0.75F;
			this.Label11.Width = 0.59375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.03125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label13.Text = "Employee";
			this.Label13.Top = 0.75F;
			this.Label13.Width = 1.84375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.21875F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = null;
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 4.6875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.28125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label19.Text = "Check";
			this.Label19.Top = 0.75F;
			this.Label19.Width = 0.9375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.25F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label20.Text = "Amount";
			this.Label20.Top = 0.75F;
			this.Label20.Width = 0.9375F;
			// 
			// lblReprint
			// 
			this.lblReprint.Height = 0.21875F;
			this.lblReprint.HyperLink = null;
			this.lblReprint.Left = 1.5F;
			this.lblReprint.Name = "lblReprint";
			this.lblReprint.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.lblReprint.Text = "**** REPRINT ****";
			this.lblReprint.Top = 0.4375F;
			this.lblReprint.Visible = false;
			this.lblReprint.Width = 4.6875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldRegularCount,
            this.Label22,
            this.fldGrandTotal,
            this.Label23,
            this.fldRegularTotal,
            this.Label24,
            this.Label25,
            this.fldTATotal,
            this.Label26,
            this.fldDDTotal,
            this.fldVoidedCount,
            this.fldTACount,
            this.fldDDCount,
            this.Line9,
            this.fldTotalCount,
            this.Line10,
            this.Label32,
            this.Label33,
            this.Line11,
            this.Label34,
            this.fldEmployeeTotal});
			this.GroupFooter1.Height = 1.927083F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// fldRegularCount
			// 
			this.fldRegularCount.Height = 0.1875F;
			this.fldRegularCount.Left = 4.375F;
			this.fldRegularCount.Name = "fldRegularCount";
			this.fldRegularCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldRegularCount.Text = "Field1";
			this.fldRegularCount.Top = 0.40625F;
			this.fldRegularCount.Width = 0.59375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.53125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" +
    "har-set: 1";
			this.Label22.Text = "Total";
			this.Label22.Top = 1.40625F;
			this.Label22.Width = 0.84375F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 3.40625F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldGrandTotal.Text = "Field1";
			this.fldGrandTotal.Top = 1.40625F;
			this.fldGrandTotal.Width = 0.90625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2.53125F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label23.Text = "Regular";
			this.Label23.Top = 0.40625F;
			this.Label23.Width = 0.6875F;
			// 
			// fldRegularTotal
			// 
			this.fldRegularTotal.Height = 0.1875F;
			this.fldRegularTotal.Left = 3.40625F;
			this.fldRegularTotal.Name = "fldRegularTotal";
			this.fldRegularTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldRegularTotal.Text = "Field1";
			this.fldRegularTotal.Top = 0.40625F;
			this.fldRegularTotal.Width = 0.90625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label24.Text = "Checks:";
			this.Label24.Top = 0.40625F;
			this.Label24.Width = 0.59375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.53125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label25.Text = "T & A";
			this.Label25.Top = 1F;
			this.Label25.Width = 0.6875F;
			// 
			// fldTATotal
			// 
			this.fldTATotal.Height = 0.1875F;
			this.fldTATotal.Left = 3.40625F;
			this.fldTATotal.Name = "fldTATotal";
			this.fldTATotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTATotal.Text = "Field1";
			this.fldTATotal.Top = 1F;
			this.fldTATotal.Width = 0.90625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.53125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label26.Text = "D / D";
			this.Label26.Top = 0.59375F;
			this.Label26.Width = 0.6875F;
			// 
			// fldDDTotal
			// 
			this.fldDDTotal.Height = 0.1875F;
			this.fldDDTotal.Left = 3.40625F;
			this.fldDDTotal.Name = "fldDDTotal";
			this.fldDDTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDDTotal.Text = "Field1";
			this.fldDDTotal.Top = 0.59375F;
			this.fldDDTotal.Width = 0.90625F;
			// 
			// fldVoidedCount
			// 
			this.fldVoidedCount.Height = 0.1875F;
			this.fldVoidedCount.Left = 4.375F;
			this.fldVoidedCount.Name = "fldVoidedCount";
			this.fldVoidedCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldVoidedCount.Text = "Field1";
			this.fldVoidedCount.Top = 1.1875F;
			this.fldVoidedCount.Width = 0.59375F;
			// 
			// fldTACount
			// 
			this.fldTACount.Height = 0.1875F;
			this.fldTACount.Left = 4.375F;
			this.fldTACount.Name = "fldTACount";
			this.fldTACount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTACount.Text = "Field1";
			this.fldTACount.Top = 1F;
			this.fldTACount.Width = 0.59375F;
			// 
			// fldDDCount
			// 
			this.fldDDCount.Height = 0.1875F;
			this.fldDDCount.Left = 4.375F;
			this.fldDDCount.Name = "fldDDCount";
			this.fldDDCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDDCount.Text = "Field1";
			this.fldDDCount.Top = 0.59375F;
			this.fldDDCount.Width = 0.59375F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 2.53125F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1.375F;
			this.Line9.Width = 2.46875F;
			this.Line9.X1 = 2.53125F;
			this.Line9.X2 = 5F;
			this.Line9.Y1 = 1.375F;
			this.Line9.Y2 = 1.375F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 4.375F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 1.40625F;
			this.fldTotalCount.Width = 0.59375F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 2.53125F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 0.375F;
			this.Line10.Width = 2.4375F;
			this.Line10.X1 = 2.53125F;
			this.Line10.X2 = 4.96875F;
			this.Line10.Y1 = 0.375F;
			this.Line10.Y2 = 0.375F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 3.375F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.Label32.Text = "Summary";
			this.Label32.Top = 0.15625F;
			this.Label32.Width = 0.71875F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.53125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label33.Text = "Voided";
			this.Label33.Top = 1.1875F;
			this.Label33.Width = 0.6875F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 2.53125F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 0.78125F;
			this.Line11.Width = 2.4375F;
			this.Line11.X1 = 2.53125F;
			this.Line11.X2 = 4.96875F;
			this.Line11.Y1 = 0.78125F;
			this.Line11.Y2 = 0.78125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 2.53125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label34.Text = "Employee";
			this.Label34.Top = 0.8125F;
			this.Label34.Width = 0.6875F;
			// 
			// fldEmployeeTotal
			// 
			this.fldEmployeeTotal.Height = 0.1875F;
			this.fldEmployeeTotal.Left = 3.40625F;
			this.fldEmployeeTotal.Name = "fldEmployeeTotal";
			this.fldEmployeeTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldEmployeeTotal.Text = "Field1";
			this.fldEmployeeTotal.Top = 0.8125F;
			this.fldEmployeeTotal.Width = 0.90625F;
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.CheckType,
            this.lblCheckType,
            this.Line6});
			this.GroupHeader2.DataField = "CheckType";
			this.GroupHeader2.Height = 0.4375F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			// 
			// CheckType
			// 
			this.CheckType.DataField = "CheckType";
			this.CheckType.Height = 0.1666667F;
			this.CheckType.Left = 0F;
			this.CheckType.Name = "CheckType";
			this.CheckType.Text = "Field1";
			this.CheckType.Top = 0.25F;
			this.CheckType.Visible = false;
			this.CheckType.Width = 0.8333333F;
			// 
			// lblCheckType
			// 
			this.lblCheckType.Height = 0.1875F;
			this.lblCheckType.HyperLink = null;
			this.lblCheckType.Left = 2.40625F;
			this.lblCheckType.Name = "lblCheckType";
			this.lblCheckType.Style = "font-weight: bold; text-align: center";
			this.lblCheckType.Text = "Label19";
			this.lblCheckType.Top = 0.21875F;
			this.lblCheckType.Width = 2.6875F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 2.40625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.40625F;
			this.Line6.Width = 2.71875F;
			this.Line6.X1 = 2.40625F;
			this.Line6.X2 = 5.125F;
			this.Line6.Y1 = 0.40625F;
			this.Line6.Y2 = 0.40625F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label21,
            this.fldTotalDD,
            this.Line7,
            this.fldTotalCheckAmount,
            this.fldTotalNetPay});
			this.GroupFooter2.Height = 0.1979167F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.40625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.Label21.Text = "Total";
			this.Label21.Top = 0.03125F;
			this.Label21.Width = 0.5625F;
			// 
			// fldTotalDD
			// 
			this.fldTotalDD.Height = 0.19F;
			this.fldTotalDD.Left = 1.375F;
			this.fldTotalDD.Name = "fldTotalDD";
			this.fldTotalDD.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldTotalDD.Text = "Field1";
			this.fldTotalDD.Top = 0.03125F;
			this.fldTotalDD.Width = 0.90625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0F;
			this.Line7.Width = 3.25F;
			this.Line7.X1 = 1F;
			this.Line7.X2 = 4.25F;
			this.Line7.Y1 = 0F;
			this.Line7.Y2 = 0F;
			// 
			// fldTotalCheckAmount
			// 
			this.fldTotalCheckAmount.Height = 0.19F;
			this.fldTotalCheckAmount.Left = 2.3125F;
			this.fldTotalCheckAmount.Name = "fldTotalCheckAmount";
			this.fldTotalCheckAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldTotalCheckAmount.Text = "Field1";
			this.fldTotalCheckAmount.Top = 0.03125F;
			this.fldTotalCheckAmount.Width = 0.90625F;
			// 
			// fldTotalNetPay
			// 
			this.fldTotalNetPay.Height = 0.19F;
			this.fldTotalNetPay.Left = 3.28125F;
			this.fldTotalNetPay.Name = "fldTotalNetPay";
			this.fldTotalNetPay.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldTotalNetPay.Text = "Field1";
			this.fldTotalNetPay.Top = 0.03125F;
			this.fldTotalNetPay.Width = 0.90625F;
			// 
			// rptCheckRegister
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTATotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDDTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVoidedCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDDCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CheckType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReprint;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTATotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDDTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVoidedCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTACount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDDCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CheckType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckType;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNetPay;
	}
}
