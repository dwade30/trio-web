﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsPayCat
	{
		//=========================================================
		private int lngCatID;
		private int intCatNum;
		private string strDescription = string.Empty;
		private double dblFactor;
		private string strType = string.Empty;
		private int intTaxStatus;
		private bool boolIsSick;
		private bool boolIsVacation;
		private bool boolIsOther;
		private bool boolIsOT;
		private bool boolIsHoliday;
		private bool boolMSRS;
		private bool boolUnemp;
		private bool boolWorkersComp;

		public clsPayCat() : base()
		{
			lngCatID = 0;
			intCatNum = 0;
			strDescription = "";
			dblFactor = 1;
			strType = "Hours";
		}

		public bool WorkersComp
		{
			set
			{
				boolWorkersComp = value;
			}
			get
			{
				bool WorkersComp = false;
				WorkersComp = boolWorkersComp;
				return WorkersComp;
			}
		}

		public bool UnEmployment
		{
			set
			{
				boolUnemp = value;
			}
			get
			{
				bool UnEmployment = false;
				UnEmployment = boolUnemp;
				return UnEmployment;
			}
		}

		public bool MSR
		{
			set
			{
				boolMSRS = value;
			}
			get
			{
				bool MSR = false;
				MSR = boolMSRS;
				return MSR;
			}
		}

		public bool IsSick
		{
			set
			{
				boolIsSick = value;
			}
			get
			{
				bool IsSick = false;
				IsSick = boolIsSick;
				return IsSick;
			}
		}

		public bool IsVacation
		{
			set
			{
				boolIsVacation = value;
			}
			get
			{
				bool IsVacation = false;
				IsVacation = boolIsVacation;
				return IsVacation;
			}
		}

		public bool IsOther
		{
			set
			{
				boolIsOther = value;
			}
			get
			{
				bool IsOther = false;
				boolIsOther = IsOther;
				return IsOther;
			}
		}

		public bool IsOT
		{
			set
			{
				boolIsOT = value;
			}
			get
			{
				bool IsOT = false;
				IsOT = boolIsOT;
				return IsOT;
			}
		}

		public bool IsHoliday
		{
			set
			{
				boolIsHoliday = value;
			}
			get
			{
				bool IsHoliday = false;
				IsHoliday = boolIsHoliday;
				return IsHoliday;
			}
		}

		public int TaxStatus
		{
			set
			{
				intTaxStatus = value;
			}
			get
			{
				int TaxStatus = 0;
				TaxStatus = intTaxStatus;
				return TaxStatus;
			}
		}

		public string PayType
		{
			set
			{
				strType = value;
			}
			get
			{
				string PayType = "";
				PayType = strType;
				return PayType;
			}
		}

		public double Multiplier
		{
			set
			{
				dblFactor = value;
			}
			get
			{
				double Multiplier = 0;
				Multiplier = dblFactor;
				return Multiplier;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int CatNum
		{
			set
			{
				intCatNum = value;
			}
			get
			{
				int CatNum = 0;
				CatNum = intCatNum;
				return CatNum;
			}
		}

		public int ID
		{
			set
			{
				lngCatID = value;
			}
			// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToInt32(
			get
			{
				int ID = 0;
				ID = lngCatID;
				return ID;
			}
		}
	}
}
