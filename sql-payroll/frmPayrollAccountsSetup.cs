//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace TWPY0000
{
	public partial class frmPayrollAccountsSetup : BaseForm
	{
		public frmPayrollAccountsSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayrollAccountsSetup InstancePtr
		{
			get
			{
				return (frmPayrollAccountsSetup)Sys.GetInstance(typeof(frmPayrollAccountsSetup));
			}
		}

		protected frmPayrollAccountsSetup _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         3/28/03
		// This form will be used to setup all the accounting accounts
		// that will be needed in payroll
		// ********************************************************
		int DescriptionCol;
		int AccountTypeCol;
		int AccountCol;
		int NumberCol;
		int AccountTitleCol;
		bool blnEditFlag;
		private clsHistory clsHistoryClass = new clsHistory();
		bool boolOKToExit;
		string strCashAccount = "";
		string strSchoolCashAccount = "";
		// vbPorter upgrade warning: intCashRow As int	OnWriteFCConvert.ToInt32(
		int intCashRow;
		int intSchoolCashRow;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		DialogResult intDataChanged;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void frmPayrollAccountsSetup_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			// vsAccounts.Select 1, AccountTypeCol
			// Call ForceFormToResize(Me)
			this.Refresh();
		}

		private void frmPayrollAccountsSetup_Load(object sender, System.EventArgs e)
		{
			int counter;
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// vsElasticLight1.ResizeFonts = elrfNone
			NumberCol = 0;
			DescriptionCol = 1;
			AccountTypeCol = 2;
			AccountCol = 3;
			AccountTitleCol = 4;
			vsAccounts.ColHidden(0, true);
			vsAccounts.TextMatrix(0, NumberCol, "");
			vsAccounts.TextMatrix(0, DescriptionCol, "Category");
			vsAccounts.TextMatrix(0, AccountTypeCol, "Acct Type");
            //FC:FINAL:BSE #2606 set allowed keys for client side check 
            vsAccounts.ColAllowedKeys(AccountTypeCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',69,71,77,82,101,103,109,114");
            vsAccounts.TextMatrix(0, AccountCol, "Account");
			vsAccounts.TextMatrix(0, AccountTitleCol, "Account Title");
			vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsAccounts.Cols - 1, true);
			vsAccounts.ColAlignment(AccountTypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// show a border between the titles and the data input section of the grid
			// vsAccounts.Select 0, 0, 0, vsAccounts.Cols - 1
			// Call vsAccounts.CellBorder(RGB(1, 1, 1), -1, -1, -1, 1, -1, -1)
			LoadGrids();
			clsHistoryClass.SetGridIDColumn("PY", "vsAccounts", 0);
			clsHistoryClass.Init = this;
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
			intDataChanged = 0;
			//this.vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsAccounts.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, Grid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void LoadGrids()
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsBudInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// If gboolTownAccounts Or Not gboolSchoolAccounts Then
				rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts ORDER BY ID");
				// Else
				// rsAccountInfo.OpenRecordset "select * from tblpayrollaccounts where not code = 'EM' and not code = 'EF' order by id"
				// End If
				while (!rsAccountInfo.EndOfFile())
				{
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsAccountInfo.Get_Fields("code"))) != "VD")
					{
						vsAccounts.AddItem("");
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, NumberCol, FCConvert.ToString(rsAccountInfo.Get_Fields("ID")));
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, DescriptionCol, FCConvert.ToString(rsAccountInfo.Get_Fields("Description")));
						// If gboolTownAccounts Then
						if (FCConvert.ToString(rsAccountInfo.Get_Fields("Code")) == "CC" && modGlobalVariables.Statics.gboolBudgetary)
						{
							rsBudInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CP'", "TWBD0000.vb1");
							if (rsBudInfo.BeginningOfFile() != true && rsBudInfo.BeginningOfFile() != true)
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsBudInfo.Get_Fields("Account"))) != "" && Conversion.Val(rsBudInfo.Get_Fields("Account")) != 0)
								{
									if (Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)) != 0)
									{
										strCashAccount = rsBudInfo.Get_Fields("Account") + "-00";
									}
									else
									{
										strCashAccount = FCConvert.ToString(rsBudInfo.Get_Fields("Account"));
									}
								}
								else
								{
									strCashAccount = "";
								}
							}
							else
							{
								strCashAccount = "";
							}
							intCashRow = (vsAccounts.Rows - 1);
							if (strCashAccount != "" && FCConvert.ToString(rsAccountInfo.Get_Fields("account")).Length > 2)
							{
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTypeCol, "G");
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, Strings.Format(Strings.Mid(FCConvert.ToString(rsAccountInfo.Get_Fields("Account")), 3, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strCashAccount);
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTitleCol, modAccountTitle.ReturnAccountDescription("G " + vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol)));
							}
							else
							{
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTypeCol, "G");
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, "");
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTitleCol, "");
							}
						}
						else
						{
							if (FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) != "" && FCConvert.ToString(rsAccountInfo.Get_Fields("account")).Length > 1)
							{
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTypeCol, Strings.Left(FCConvert.ToString(rsAccountInfo.Get_Fields("Account")), 1));
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, Strings.Right(FCConvert.ToString(rsAccountInfo.Get_Fields("Account")), FCConvert.ToString(rsAccountInfo.Get_Fields("Account")).Length - 2));
								if (modGlobalVariables.Statics.gboolBudgetary)
								{
									vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTitleCol, modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields("Account")));
								}
								else
								{
									vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTitleCol, string.Empty);
								}
							}
							else
							{
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTypeCol, "G");
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, "");
								vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountTitleCol, "");
							}
						}
					}
					rsAccountInfo.MoveNext();
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrids", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmPayrollAccountsSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPayrollAccountsSetup_Resize(object sender, System.EventArgs e)
		{
			vsAccounts.ColWidth(1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.21));
			vsAccounts.ColWidth(2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(3, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.3));
			// vsAccounts.ColWidth(4) = vsAccounts.Width * 0.4
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsAccounts.Select(0, 0);
				SaveChanges();
				if (intDataChanged == (DialogResult)2)
				{
					e.Cancel = true;
					return;
				}
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuFileValidAccounts_Click(object sender, System.EventArgs e)
		{
			string strAcct = "";

			if (vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) != string.Empty)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText, vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol));
				if (strAcct != string.Empty)
				{
					vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, modAccountTitle.ReturnAccountDescription(strAcct));
					strAcct = Strings.Mid(strAcct, 3);
					vsAccounts.TextMatrix(vsAccounts.Row, vsAccounts.Col, strAcct);
				}
			}
		}

		private void vsAccounts_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsAccounts.EditMask = "";
			if (vsAccounts.Col == AccountCol)
			{
				if (Strings.InStr(1, vsAccounts.TextMatrix(vsAccounts.Row, vsAccounts.Col), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					vsAccounts.TextMatrix(vsAccounts.Row, vsAccounts.Col, "");
				}
			}
		}

		private void vsAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsAccounts.Col == AccountCol)
			{
				if (fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, DescriptionCol)) == "EMPLOYERS FICA" || fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, DescriptionCol)) == "EMPLOYERS MEDICARE")
				{
					vsAccounts.EditMask = CreateFormat(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol));
				}
				else
				{
					vsAccounts.EditMask = CreateFormat(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol), false);
				}
			}
			else
			{
				vsAccounts.EditMask = "";
			}
			// If Trim(vsAccounts.EditMask) = vbNullString Then vsAccounts.TextMatrix(Row, AccountCol) = vbNullString
		}

		private void vsAccounts_ChangeEdit(object sender, System.EventArgs e)
		{
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				if (vsAccounts.Col == AccountCol)
				{
					vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, modAccountTitle.ReturnAccountDescription(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText));
				}
			}
		}

		private void vsAccounts_Enter(object sender, System.EventArgs e)
		{
			if (vsAccounts.Col != AccountCol)
				cmdValidAccounts.Visible = false;
		}

		private void vsAccounts_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			string strAcct = "";
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vsAccounts.Row < vsAccounts.Rows - 1)
				{
					if (vsAccounts.Col < AccountCol)
					{
						vsAccounts.Col = vsAccounts.Col + 1;
					}
					else
					{
						vsAccounts.Row += 1;
						vsAccounts.Col = AccountTypeCol;
					}
				}
				else
				{
					if (vsAccounts.Col < AccountCol)
					{
						vsAccounts.Col = vsAccounts.Col + 1;
					}
				}
			}
		}

		private void vsAccounts_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsAccounts.Col == AccountCol)
			{
				if (KeyAscii == 120 && fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol)) != "M")
				{
					KeyAscii -= 32;
				}
				else if (KeyAscii == 88 || KeyAscii == 8 || (KeyAscii >= 48 && KeyAscii <= 57))
				{
					// do nothing
				}
				else
				{
					// MATTHEW 5/20/04
					// KeyAscii = 0
					if (modGlobalVariables.Statics.gboolBudgetary)
						KeyAscii = 0;
				}
			}
			else if (vsAccounts.Col == AccountTypeCol)
			{
				if ( (KeyAscii == 101 || KeyAscii == 103 || KeyAscii == 109 || KeyAscii == 114))
				{
					KeyAscii -= 32;
				}
				
				else if ((KeyAscii == 69 || KeyAscii == 71 || KeyAscii == 82 || KeyAscii == 77))
				{
					// do nothing
				}
				
				else
				{
					if (KeyAscii == 109)
						KeyAscii = 77;
					if (KeyAscii != 77)
					{
						KeyAscii = 0;
					}
				}
			}
			intDataChanged = (DialogResult)1;
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsAccounts.EditMaxLength = 0;
			cmdValidAccounts.Visible = false;
			if (vsAccounts.Col == AccountTypeCol)
			{
				vsAccounts.EditMaxLength = 1;
				vsAccounts.EditCell();
				vsAccounts.EditSelStart = 0;
				vsAccounts.EditSelLength = 1;
			}
			else if (vsAccounts.Col == AccountTitleCol)
			{
				if (vsAccounts.Row < vsAccounts.Rows - 1)
				{
					vsAccounts.Select(vsAccounts.Row + 1, AccountTypeCol);
				}
				else
				{
					vsAccounts.Select(vsAccounts.Row, AccountCol);
				}
			}
			else if (vsAccounts.Col == AccountCol)
			{
				vsAccounts.EditCell();
				vsAccounts.EditSelStart = 0;
				vsAccounts.EditSelLength = 0;
				cmdValidAccounts.Visible = true;
			}
		}

		private void vsAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: Ans As int	OnWrite(DialogResult)
			DialogResult Ans = 0;
			if (!modGlobalVariables.Statics.gboolBudgetary)
			{
				if (vsAccounts.Col == 2)
				{
					if (fecherFoundation.Strings.Trim(vsAccounts.EditText) != "M")
					{
						MessageBox.Show("Only M accounts can be selected", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsAccounts.EditText = "";
						e.Cancel = true;
						return;
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol)) != "M")
					{
						MessageBox.Show("Only M accounts can be selected", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsAccounts.EditText = "";
						// Cancel = True
						return;
					}
				}
			}
			if (vsAccounts.Col == AccountTypeCol)
			{
				if (vsAccounts.Row == intCashRow)
				{
					if (vsAccounts.EditText != "G" )
					{
						MessageBox.Show("Your cash account must be a G account.", "Invalid Account Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
					}
				}
			}
			else if (vsAccounts.Col == AccountCol)
			{
				if (vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) == "M")
					return;
				if (Strings.InStr(1, vsAccounts.EditText, "X", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsAccounts.EditText, " ", CompareConstants.vbBinaryCompare) == 0)
				{
					if (modValidateAccount.AccountValidate(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText))
					{
						vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, modAccountTitle.ReturnAccountDescription(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText));
						e.Cancel = false;
					}
					else
					{
						Ans = MessageBox.Show("This is an invalid account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (Ans == DialogResult.Yes)
						{
							vsAccounts.EditText = "";
							vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
						}
						else
						{
							e.Cancel = true;
							vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
						}
					}
					if (vsAccounts.Row == intCashRow)
					{
						if (Strings.Right(vsAccounts.EditText, FCConvert.ToInt32(vsAccounts.EditText.Length - (1 + Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))) != strCashAccount)
						{
							MessageBox.Show("Your cash account must match the Payroll Cash Account in Budgetary.  The only piece of the account you may change is the fund.", "Invalid Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
				}
				else
				{
					if (Strings.InStr(1, vsAccounts.EditText, " ", CompareConstants.vbBinaryCompare) == 0)
					{
						if (fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, DescriptionCol)) != "EMPLOYERS FICA" && fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, DescriptionCol)) != "EMPLOYERS MEDICARE")
						{
							Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the Employers FICA and Employers Medicare account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (Ans == DialogResult.Yes)
							{
								vsAccounts.EditText = "";
								vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
							}
							else
							{
								e.Cancel = true;
								vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
							}
						}
						else
						{
							if (modAccountTitle.Statics.ExpDivFlag)
							{
								if (modBudgetaryAccounting.GetDepartment(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText) != Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))), "X"))
								{
									Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (Ans == DialogResult.Yes)
									{
										vsAccounts.EditText = "";
										vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
									}
									else
									{
										e.Cancel = true;
										vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
									}
								}
								else
								{
									if (modAccountTitle.Statics.ObjFlag)
									{
										if (Strings.InStr(1, modBudgetaryAccounting.GetExpense(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0)
										{
											Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											if (Ans == DialogResult.Yes)
											{
												vsAccounts.EditText = "";
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
											else
											{
												e.Cancel = true;
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
										}
									}
									else
									{
										if (Strings.InStr(1, modBudgetaryAccounting.GetExpense(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0 || Strings.InStr(1, modBudgetaryAccounting.GetObject(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0)
										{
											Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											if (Ans == DialogResult.Yes)
											{
												vsAccounts.EditText = "";
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
											else
											{
												e.Cancel = true;
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
										}
									}
								}
							}
							else
							{
								if (modBudgetaryAccounting.GetDepartment(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText) != Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))), "X") || modBudgetaryAccounting.GetExpDivision(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText) != Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "X"))
								{
									Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department and division section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (Ans == DialogResult.Yes)
									{
										vsAccounts.EditText = "";
										vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
									}
									else
									{
										e.Cancel = true;
										vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
									}
								}
								else
								{
									if (modAccountTitle.Statics.ObjFlag)
									{
										if (Strings.InStr(1, modBudgetaryAccounting.GetExpense(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0)
										{
											Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											if (Ans == DialogResult.Yes)
											{
												vsAccounts.EditText = "";
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
											else
											{
												e.Cancel = true;
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
										}
									}
									else
									{
										if (Strings.InStr(1, modBudgetaryAccounting.GetExpense(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0 || Strings.InStr(1, modBudgetaryAccounting.GetObject(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol) + " " + vsAccounts.EditText), "X", CompareConstants.vbBinaryCompare) != 0)
										{
											Ans = MessageBox.Show("This is an invalid account.  You may only use Xs in the department section of the account.  Do you wish to continue?", "Invalid Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											if (Ans == DialogResult.Yes)
											{
												vsAccounts.EditText = "";
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
											else
											{
												e.Cancel = true;
												vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
											}
										}
									}
								}
							}
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Row, AccountTitleCol, "");
						//App.DoEvents();
						e.Cancel = false;
					}
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			if (boolOKToExit)
				Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int x;
			boolOKToExit = false;
			vsAccounts.Select(0, 0);
			for (counter = 1; counter <= (vsAccounts.Rows - 1); counter++)
			{
				if (vsAccounts.TextMatrix(counter, 1) == "Employers FICA")
				{
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(counter, AccountTypeCol))) != "E" || fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(counter, AccountCol)) == string.Empty)
					{
						if (modGlobalVariables.Statics.gboolBudgetary )
						{
							MessageBox.Show("Employers FICA must be an expense account. Save was NOT completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
				}
				if (vsAccounts.TextMatrix(counter, 1) == "Employers Medicare")
				{
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(counter, AccountTypeCol))) != "E" || fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(counter, AccountCol)) == string.Empty)
					{
						if (modGlobalVariables.Statics.gboolBudgetary)
						{
							MessageBox.Show("Employers Medicare must be an expense account. Save was NOT completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
				}
				if (!modGlobalVariables.Statics.gboolBudgetary)
				{
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(vsAccounts.TextMatrix(counter, AccountTypeCol))) != "M")
					{
						MessageBox.Show("Account type must be of a type of M", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsAccounts.Select(counter, AccountTypeCol);
						return;
					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Setup Payroll Accounts", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts ORDER BY ID");
			for (counter = 1; counter <= (vsAccounts.Rows - 1); counter++)
			{
				if (rsAccountInfo.FindFirstRecord("ID", Conversion.Val(vsAccounts.TextMatrix(counter, NumberCol))))
				{
					rsAccountInfo.Edit();
					rsAccountInfo.Set_Fields("Account", vsAccounts.TextMatrix(counter, AccountTypeCol) + " " + vsAccounts.TextMatrix(counter, AccountCol));
					rsAccountInfo.Update(true);
				}
			}

			intDataChanged = 0;
			clsHistoryClass.Compare();
			// CHECK TO SEE IF THE CALCULATION HAS ALREADY BEEN RUN
			if (modGlobalRoutines.CheckIfPayProcessCompleted("Calculation", "Calculation", true))
			{
				// CHECK TO SEE IF THE VERIFY AND ACCEPT HAS BEEN RUN
				if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "VerifyAccept", true))
				{
					// ACCEPT HAS BEEN DONE SO DO NOT CLEAR OUT THE CALCULATION FLAG
				}
				else
				{
					// CLEAR OUT THE CALCULATION FIELD
					modGlobalRoutines.UpdatePayrollStepTable("EditTotals");
				}
			}
			MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			boolOKToExit = true;
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private string CreateFormat(string strType, bool blnAllowLetters = true)
		{
			string CreateFormat = "";
			string strTemp = "";
			int counter = 0;
			if (strType == "E")
			{
				strTemp = modAccountTitle.Statics.Exp;
			}
			else if (strType == "R")
			{
				strTemp = modAccountTitle.Statics.Rev;
			}
			else if (strType == "G")
			{
				strTemp = modAccountTitle.Statics.Ledger;
			}
			else if (strType == "P")
			{
				strTemp = modNewAccountBox.Statics.PSegmentSize;
			}
			else if (strType == "V")
			{
				strTemp = modNewAccountBox.Statics.VSegmentSize;
			}
			else if (strType == "L")
			{
				strTemp = modNewAccountBox.Statics.LSegmentSize;
			}
			else
			{
				strTemp = "";
			}
			if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
				return CreateFormat;
			do
			{
				counter = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, 2))));
				strTemp = Strings.Right(strTemp, strTemp.Length - 2);
				if (counter > 0)
				{
					for (counter = counter; counter >= 1; counter--)
					{
						if (blnAllowLetters)
						{
							CreateFormat = CreateFormat + "A";
						}
						else
						{
							CreateFormat = CreateFormat + "0";
						}
					}
					CreateFormat = CreateFormat + "-";
				}
			}
			while (strTemp != "");
			CreateFormat = Strings.Left(CreateFormat, CreateFormat.Length - 1);
			return CreateFormat;
		}

		private string GetAccount(ref string x)
		{
			string GetAccount = "";
			GetAccount = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			return GetAccount;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsAccounts.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (vsAccounts.Cols - 1); intCols++)
				{
					if (intCols == 2 || intCols == 3)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsAccounts";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsAccounts.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}

        private void vsAccounts_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                //FC:FINAL:SBE - #2614 - changing KeyCode during KeyDown is not supported on server side. Change CharacterCasing for specified columns as in KeyDown/KeyPress event
                int col = vsAccounts.GetFlexColIndex(e.ColumnIndex);
                if (col == AccountCol || col == AccountTypeCol)
                {
                    if (e.Control is TextBox)
                    {
                        (e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
                    }
                    else if (e.Control is MaskedTextBox)
                    {
                        (e.Control as MaskedTextBox).CharacterCasing = CharacterCasing.Upper;
                    }
                }
                else
                {
                    if (e.Control is TextBox)
                    {
                        (e.Control as TextBox).CharacterCasing = CharacterCasing.Normal;
                    }
                    else if (e.Control is MaskedTextBox)
                    {
                        (e.Control as MaskedTextBox).CharacterCasing = CharacterCasing.Normal;
                    }

                }

                //FC:FINAL:SBE - #2614 - implement KeyDown / KeyUp on client side (KeyCode = 0 is not supported on server side)
                JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
                CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
                DynamicObject customClientData = new DynamicObject();
                customClientData["vsAccounts_Col"] = col;
                customClientData["vsAccounts_AccountCol"] = AccountCol;
                customClientData["vsAccounts_AccountTypeCol"] = AccountTypeCol;
                customClientData["gboolTownAccounts"] = true;
                customClientData["gboolSchoolAccounts"] = false;
                customClientData["gboolBudgetary"] = modGlobalVariables.Statics.gboolBudgetary;
                customClientData["accountTypeCol_Value"] = fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, AccountTypeCol));
                customClientData["descriptionCol_Value"] = fecherFoundation.Strings.UCase(vsAccounts.TextMatrix(vsAccounts.Row, DescriptionCol));
                customProperties.SetCustomPropertiesValue(e.Control, customClientData);
                if (e.Control.UserData.vsAccountsKeyDown == null)
                {
                    e.Control.UserData.vsAccountsKeyDown = "vsAccountsKeyDown";
                    JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
                    JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
                    keyDownEvent.Event = "keydown";
                    keyDownEvent.JavaScript = "frmPayrollAccountsSetup_vsAccounts_KeyDown(this, e);";
                    clientEvents.Add(keyDownEvent);
                }

                e.Control.KeyDown -= vsAccounts_KeyDownEdit;
                e.Control.KeyPress -= vsAccounts_KeyPressEdit;
                e.Control.KeyDown += vsAccounts_KeyDownEdit;
                e.Control.KeyPress += vsAccounts_KeyPressEdit;
            }
        }
    }
}
