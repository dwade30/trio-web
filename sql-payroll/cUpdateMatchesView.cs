//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cUpdateMatchesView
	{
		//=========================================================
		private cEmployeeService empServ = new cEmployeeService();
		private cDeductionController dedServ = new cDeductionController();
		private int lngCurrentUserID;
		private Dictionary<string, cEmployee> collAllowedEmployees = new Dictionary<string, cEmployee>();
		private cGenericCollection collEmpsWithMatch = new cGenericCollection();
		private cGenericCollection collFilteredEmps = new cGenericCollection();
		private cGenericCollection collMatches = new cGenericCollection();
		private cGenericCollection collDepartments = new cGenericCollection();
		private cGenericCollection collSequences = new cGenericCollection();
		private cGenericCollection collCodeOnes = new cGenericCollection();
		private cGenericCollection collCodeTwos = new cGenericCollection();
		private cGenericCollection collGroups = new cGenericCollection();
		private cGenericCollection collFullTime = new cGenericCollection();
		private cGenericCollection collAmountTypes = new cGenericCollection();
		private cGenericCollection collMaxTypes = new cGenericCollection();
		private cGenericCollection collStatus = new cGenericCollection();
		private cGenericCollection collFrequencies = new cGenericCollection();
		private int intUseDepartment;
		private int intUseSequenceNumber;
		private int intUseCode1;
		private int intUseCode2;
		private int intUseGroupID;
		private int intUseFullTime;
		private int lngCurrentMatch;
		private int lngCurrentSequence;
		private string strCurrentCode1 = "";
		private string strCurrentCode2 = "";
		private string strCurrentGroupID = "";
		private int intCurrentFullTime;
		private string strExpenseTemplate = "";
		private string strRevenueTemplate = "";
		private string strLedgerTemplate = "";
		private string strAccountTemplate = "";
		private int intAccountTypeToShow;
		private int[] SegmentLength = new int[5 + 1];

		public delegate void CurrentMatchChangedEventHandler();

		public event CurrentMatchChangedEventHandler CurrentMatchChanged;

		public delegate void MatchesChangedEventHandler();

		public event MatchesChangedEventHandler MatchesChanged;

		public delegate void DepartmentsChangedEventHandler();

		public event DepartmentsChangedEventHandler DepartmentsChanged;

		public delegate void SequencesChangedEventHandler();

		public event SequencesChangedEventHandler SequencesChanged;

		public delegate void CodeOnesChangedEventHandler();

		public event CodeOnesChangedEventHandler CodeOnesChanged;

		public delegate void CodeTwosChangedEventHandler();

		public event CodeTwosChangedEventHandler CodeTwosChanged;

		public delegate void FullTimesChangedEventHandler();

		public event FullTimesChangedEventHandler FullTimesChanged;

		public delegate void GroupIDsChangedEventHandler();

		public event GroupIDsChangedEventHandler GroupIDsChanged;

		public delegate void AmountTypesChangedEventHandler();

		public event AmountTypesChangedEventHandler AmountTypesChanged;

		public delegate void MaxTypesChangedEventHandler();

		public event MaxTypesChangedEventHandler MaxTypesChanged;

		public delegate void StatusesChangedEventHandler();

		public event StatusesChangedEventHandler StatusesChanged;

		public delegate void FrequenciesChangedEventHandler();

		public event FrequenciesChangedEventHandler FrequenciesChanged;

		public delegate void FilteredEmployeesChangedEventHandler();

		public event FilteredEmployeesChangedEventHandler FilteredEmployeesChanged;

		public delegate void AccountTypeChangedEventHandler();

		public event AccountTypeChangedEventHandler AccountTypeChanged;

		public int GetSegmentLength(int intSegment)
		{
			int GetSegmentLength = 0;
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegmentLength = SegmentLength[intSegment - 1];
			}
			return GetSegmentLength;
		}

		public int CurrentCodeTwoIndex
		{
			set
			{
				if (value > 0)
				{
					if (collCodeTwos.ItemCount() >= value)
					{
						if (value != intUseCode2)
						{
							intUseCode2 = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentCodeTwoIndex = 0;
				CurrentCodeTwoIndex = intUseCode2;
				return CurrentCodeTwoIndex;
			}
		}

		public int CurrentDepartmentIndex
		{
			set
			{
				if (value > 0)
				{
					if (collDepartments.ItemCount() >= value)
					{
						if (value != intUseDepartment)
						{
							intUseDepartment = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentDepartmentIndex = 0;
				CurrentDepartmentIndex = intUseDepartment;
				return CurrentDepartmentIndex;
			}
		}

		public int CurrentCodeOneIndex
		{
			set
			{
				if (value > 0)
				{
					if (collCodeOnes.ItemCount() >= value)
					{
						if (value != intUseCode1)
						{
							intUseCode1 = value;
							FilterEmployees();
						}
					}
				}
			}
			get
			{
				int CurrentCodeOneIndex = 0;
				CurrentCodeOneIndex = intUseCode1;
				return CurrentCodeOneIndex;
			}
		}

		public int CurrentFullTimePartTime
		{
			set
			{
				if (value > 0 && value < 4)
				{
					if (intUseFullTime != value)
					{
						intUseFullTime = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentFullTimePartTime = 0;
				CurrentFullTimePartTime = intUseFullTime;
				return CurrentFullTimePartTime;
			}
		}

		public int CurrentGroupIDIndex
		{
			set
			{
				if (collGroups.ItemCount() >= value)
				{
					if (intUseGroupID != value)
					{
						intUseGroupID = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentGroupIDIndex = 0;
				CurrentGroupIDIndex = intUseGroupID;
				return CurrentGroupIDIndex;
			}
		}

		public int CurrentSequenceIndex
		{
			set
			{
				if (collSequences.ItemCount() >= value)
				{
					if (intUseSequenceNumber != value)
					{
						intUseSequenceNumber = value;
						FilterEmployees();
					}
				}
			}
			get
			{
				int CurrentSequenceIndex = 0;
				CurrentSequenceIndex = intUseSequenceNumber;
				return CurrentSequenceIndex;
			}
		}

		public cGenericCollection FilteredEmployees
		{
			get
			{
				cGenericCollection FilteredEmployees = null;
				FilteredEmployees = collFilteredEmps;
				return FilteredEmployees;
			}
		}

		public cGenericCollection Frequencies
		{
			get
			{
				cGenericCollection Frequencies = null;
				Frequencies = collFrequencies;
				return Frequencies;
			}
		}

		public cGenericCollection Statuses
		{
			get
			{
				cGenericCollection Statuses = null;
				Statuses = collStatus;
				return Statuses;
			}
		}

		public cGenericCollection AmountTypes
		{
			get
			{
				cGenericCollection AmountTypes = null;
				AmountTypes = collAmountTypes;
				return AmountTypes;
			}
		}

		public cGenericCollection MaxTypes
		{
			get
			{
				cGenericCollection MaxTypes = null;
				MaxTypes = collMaxTypes;
				return MaxTypes;
			}
		}

		public int AccountTypeToShow
		{
			get
			{
				int AccountTypeToShow = 0;
				AccountTypeToShow = intAccountTypeToShow;
				return AccountTypeToShow;
			}
			set
			{
				if (intAccountTypeToShow != value)
				{
					intAccountTypeToShow = value;
					switch (intAccountTypeToShow)
					{
						case 0:
							{
								// None
								strAccountTemplate = "000000";
								break;
							}
						case 1:
							{
								strAccountTemplate = strExpenseTemplate;
								break;
							}
						case 2:
							{
								strAccountTemplate = strRevenueTemplate;
								break;
							}
						case 3:
							{
								strAccountTemplate = strLedgerTemplate;
								break;
							}
					}
					//end switch
					UpdateSegmentLengths(strAccountTemplate);
					if (this.AccountTypeChanged != null)
						this.AccountTypeChanged();
					//App.DoEvents();
				}
			}
		}

		private void UpdateSegmentLengths(string strTemplate)
		{
			string strRemainder;
			int intCount;
			int intLength = 0;
			int intSegment;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			strRemainder = strTemplate;
			intCount = 0;
			intSegment = 0;
			for (x = 0; x <= (Information.UBound(SegmentLength, 1)); x++)
			{
				SegmentLength[x] = 0;
			}
			while (intCount < 5)
			{
				intCount += 1;
				if (strRemainder.Length >= 2)
				{
					intLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strRemainder, 2))));
					if (strRemainder.Length > 2)
					{
						strRemainder = Strings.Mid(strRemainder, 3);
					}
					else
					{
						strRemainder = "";
					}
				}
				else
				{
					strRemainder = "";
					intLength = 0;
				}
				if (intLength > 0)
				{
					intSegment += 1;
					SegmentLength[intSegment - 1] = intLength;
				}
			}
		}

		public cGenericCollection FullTimes
		{
			get
			{
				cGenericCollection FullTimes = null;
				FullTimes = collFullTime;
				return FullTimes;
			}
		}

		public cGenericCollection Groups
		{
			get
			{
				cGenericCollection Groups = null;
				Groups = collGroups;
				return Groups;
			}
		}

		public cGenericCollection CodeTwos
		{
			get
			{
				cGenericCollection CodeTwos = null;
				CodeTwos = collCodeTwos;
				return CodeTwos;
			}
		}

		public cGenericCollection CodeOnes
		{
			get
			{
				cGenericCollection CodeOnes = null;
				CodeOnes = collCodeOnes;
				return CodeOnes;
			}
		}

		public cGenericCollection Sequences
		{
			get
			{
				cGenericCollection Sequences = null;
				Sequences = collSequences;
				return Sequences;
			}
		}

		public cGenericCollection Departments
		{
			get
			{
				cGenericCollection Departments = null;
				Departments = collDepartments;
				return Departments;
			}
		}

		public cGenericCollection Matches
		{
			get
			{
				cGenericCollection Matches = null;
				Matches = collMatches;
				return Matches;
			}
		}

		private void ClearValues()
		{
			intUseDepartment = 1;
			intUseSequenceNumber = 1;
			intUseCode1 = 1;
			intUseCode2 = 1;
			intUseGroupID = 1;
			intUseFullTime = 1;
		}

		private void FillLists()
		{
			collDepartments.ClearList();
			collSequences.ClearList();
			collCodeOnes.ClearList();
			collCodeTwos.ClearList();
			collGroups.ClearList();
			collFullTime.ClearList();
			collDepartments = empServ.GetDepartmentsUsed();
			collDepartments.MoveFirst();
			collDepartments.InsertItemBefore("");
			collSequences = empServ.GetSequenceNumbersUsed();
			collSequences.MoveFirst();
			collSequences.InsertItemBefore("");
			collCodeOnes = empServ.GetCodeOnesUsed();
			collCodeOnes.MoveFirst();
			collCodeOnes.InsertItemBefore("");
			collCodeTwos = empServ.GetCodeTwosUsed();
			collCodeTwos.MoveFirst();
			collCodeTwos.InsertItemBefore("");
			collFullTime.AddItem("");
			collFullTime.AddItem("Full Time");
			collFullTime.AddItem("Part Time");
			collGroups = empServ.GetGroupIDsUsed();
			collGroups.MoveFirst();
			collGroups.InsertItemBefore("");
			FillAmountTypes();
			FillMaxTypes();
			FillStatuses();
			FillFrequencies();
			if (this.AmountTypesChanged != null)
				this.AmountTypesChanged();
			if (this.MaxTypesChanged != null)
				this.MaxTypesChanged();
			if (this.StatusesChanged != null)
				this.StatusesChanged();
			if (this.FrequenciesChanged != null)
				this.FrequenciesChanged();
			if (this.DepartmentsChanged != null)
				this.DepartmentsChanged();
			if (this.SequencesChanged != null)
				this.SequencesChanged();
			if (this.CodeOnesChanged != null)
				this.CodeOnesChanged();
			if (this.CodeTwosChanged != null)
				this.CodeTwosChanged();
			if (this.FullTimesChanged != null)
				this.FullTimesChanged();
			if (this.GroupIDsChanged != null)
				this.GroupIDsChanged();
		}

		private void FillAmountTypes()
		{
			collAmountTypes.ClearList();
			cCodeDescription cd;
			cd = new cCodeDescription();
			cd.ID = 1;
			cd.Code = "D";
			cd.Description = "Dollars";
			collAmountTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 2;
			cd.Code = "%G";
			cd.Description = "% of Gross";
			collAmountTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 3;
			cd.Code = "%D";
			cd.Description = "% of Deduction";
			collAmountTypes.AddItem(cd);
		}

		private void FillMaxTypes()
		{
			collMaxTypes.ClearList();
			cCodeDescription cd;
			// vsMatch.ColComboList(8, "#1;Dollars|#2;Hours|#3;Period|#4;Month|#5;Quarter|#6;Calendar|#7;Fiscal|#8;Life");
			cd = new cCodeDescription();
			cd.ID = 1;
			cd.Code = "Dollars";
			cd.Description = "Dollars";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 2;
			cd.Code = "Hours";
			cd.Description = "Hours";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 3;
			cd.Code = "Period";
			cd.Description = "Period";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 4;
			cd.Code = "Month";
			cd.Description = "Month";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 5;
			cd.Code = "Quarter";
			cd.Description = "Quarter";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 6;
			cd.Code = "Calendar";
			cd.Description = "Calendar";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 7;
			cd.Code = "Fiscal";
			cd.Description = "Fiscal";
			collMaxTypes.AddItem(cd);
			cd = new cCodeDescription();
			cd.ID = 8;
			cd.Code = "Life";
			cd.Description = "Life";
			collMaxTypes.AddItem(cd);
		}

		private void FillStatuses()
		{
			collStatus.ClearList();
			collStatus.AddItem("");
			collStatus.AddItem("Active");
			collStatus.AddItem("Inactive");
		}

		private void FillFrequencies()
		{
			collFrequencies = dedServ.GetFrequencyCodes();
			collFrequencies.MoveFirst();
			cCodeDescription cd = new cCodeDescription();
			cd.ID = 0;
			cd.Code = "";
			cd.Description = "";
			collFrequencies.InsertItemBefore(cd);
		}

		private void LoadMatches()
		{
			collMatches = dedServ.GetUsedMatches();
			if (this.MatchesChanged != null)
				this.MatchesChanged();
		}

		public void SetCurrentMatch(int lngID)
		{
			if (lngID == lngCurrentMatch)
			{
				return;
			}
			collMatches.MoveFirst();
			cDeductionSetup dedSet;
			while (collMatches.IsCurrent())
			{
				//App.DoEvents();
				dedSet = (cDeductionSetup)collMatches.GetCurrentItem();
				if (dedSet.ID == lngID)
				{
					lngCurrentMatch = dedSet.ID;
					GetEmployeesWithMatch();
					if (this.CurrentMatchChanged != null)
						this.CurrentMatchChanged();
					FilterEmployees();
				}
				collMatches.MoveNext();
			}
		}

		public int CurrentMatch
		{
			get
			{
				int CurrentMatch = 0;
				CurrentMatch = lngCurrentMatch;
				return CurrentMatch;
			}
		}

		public void InitializeValues(int lngUserID)
		{
			lngCurrentUserID = lngUserID;
			LoadMatches();
			FillLists();
			GetAllowedEmployees();
			collMatches.MoveFirst();
			if (collMatches.IsCurrent())
			{
				cDeductionSetup dedSet;
				dedSet = (cDeductionSetup)collMatches.GetCurrentItem();
				lngCurrentMatch = dedSet.ID;
				if (this.CurrentMatchChanged != null)
					this.CurrentMatchChanged();
				GetEmployeesWithMatch();
			}
			ClearValues();
			FilterEmployees();
		}

		public void UpdateMatches(bool boolUseAmount, double dblAmount, string strAmountType, bool boolUseMaxAmount, double dblMaxAmount, int lngMaxType, bool boolUseStatus, string strStatus, bool boolUseFrequency, int lngFrequency, bool boolUseAccount, string strAccount)
		{
			if (boolUseAmount)
			{
				if (strAmountType == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid amount type", null, null);
				}
			}
			if (boolUseMaxAmount)
			{
				if (lngMaxType < 1)
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid max amount type", null, null);
				}
			}
			if (boolUseStatus)
			{
				if (fecherFoundation.Strings.Trim(strStatus) == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid status", null, null);
				}
			}
			if (boolUseAccount)
			{
				if (fecherFoundation.Strings.Trim(strAccount) == "")
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Invalid Account", null, null);
				}
			}
			collFilteredEmps.MoveFirst();
			cEmployee emp;
			clsDRWrapper rsSave = new clsDRWrapper();
			while (collFilteredEmps.IsCurrent())
			{
				emp = (cEmployee)collFilteredEmps.GetCurrentItem();
				rsSave.OpenRecordset("select * from tblemployersmatch where employeenumber = '" + emp.EmployeeNumber + "' and deductioncode = " + FCConvert.ToString(lngCurrentMatch), "Payroll");
				while (!rsSave.EndOfFile())
				{
					rsSave.Edit();
					if (boolUseAmount)
					{
						rsSave.Set_Fields("Amount", dblAmount);
						rsSave.Set_Fields("AmountType", strAmountType);
					}
					if (boolUseMaxAmount)
					{
						rsSave.Set_Fields("MaxAmount", dblMaxAmount);
						rsSave.Set_Fields("DollarsHours", lngMaxType);
					}
					if (boolUseStatus)
					{
						rsSave.Set_Fields("Status", strStatus);
					}
					if (boolUseFrequency)
					{
						rsSave.Set_Fields("OldFrequency", FCConvert.ToString(Conversion.Val(rsSave.Get_Fields("FrequencyCode"))));
						rsSave.Set_Fields("FrequencyCode", lngFrequency);
					}
					if (boolUseAccount)
					{
						rsSave.Set_Fields("Account", strAccount);
					}
					rsSave.Update();
					rsSave.MoveNext();
				}
				collFilteredEmps.MoveNext();
			}
		}

		public cUpdateMatchesView() : base()
		{
			strExpenseTemplate = modAccountTitle.Statics.Exp;
			strRevenueTemplate = modAccountTitle.Statics.Rev;
			strLedgerTemplate = modAccountTitle.Statics.Ledger;
		}

		private void GetAllowedEmployees()
		{
			collAllowedEmployees = empServ.GetEmployeesPermissableAsDictionary(lngCurrentUserID, "EmployeeNumber", false);
		}

		private void GetEmployeesWithMatch()
		{
			collEmpsWithMatch = dedServ.GetEmployeesUsingMatch(lngCurrentMatch);
		}

		private void FilterEmployees()
		{
			collFilteredEmps.ClearList();
			collEmpsWithMatch.MoveFirst();
			cEmployee empl;
			string strTemp = "";
			bool boolUse = false;
			int lngTemp = 0;
			string strEmp = "";
			while (collEmpsWithMatch.IsCurrent())
			{
				strEmp = FCConvert.ToString(collEmpsWithMatch.GetCurrentItem());
				if (collAllowedEmployees.ContainsKey(strEmp))
				{
					empl = (cEmployee)collAllowedEmployees[strEmp];
					boolUse = true;
					if (intUseCode1 > 1)
					{
						strTemp = FCConvert.ToString(collCodeOnes.GetItemByIndex(intUseCode1));
						if (fecherFoundation.Strings.Trim(empl.Code1) != fecherFoundation.Strings.Trim(strTemp))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseCode2 > 1)
					{
						strTemp = FCConvert.ToString(collCodeTwos.GetItemByIndex(intUseCode2));
						if (fecherFoundation.Strings.Trim(empl.Code2) != fecherFoundation.Strings.Trim(strTemp))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseDepartment > 1)
					{
						strTemp = FCConvert.ToString(collDepartments.GetItemByIndex(intUseDepartment));
						if (fecherFoundation.Strings.Trim(strTemp) != fecherFoundation.Strings.Trim(empl.Department))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseFullTime > 1)
					{
						if (intUseFullTime == 2)
						{
							// fulltime
							if (empl.IsPartTime)
							{
								boolUse = false;
							}
						}
						else
						{
							// parttime
							if (!empl.IsPartTime)
							{
								boolUse = false;
							}
						}
					}
					if (boolUse && intUseGroupID > 1)
					{
						strTemp = FCConvert.ToString(collGroups.GetItemByIndex(intUseGroupID));
						if (fecherFoundation.Strings.Trim(strTemp) != fecherFoundation.Strings.Trim(empl.GroupID))
						{
							boolUse = false;
						}
					}
					if (boolUse && intUseSequenceNumber > 1)
					{
						lngTemp = FCConvert.ToInt32(collSequences.GetItemByIndex(intUseSequenceNumber));
						if (lngTemp != empl.SequenceNumber)
						{
							boolUse = false;
						}
					}
					if (boolUse)
					{
						collFilteredEmps.AddItem(empl);
					}
				}
				collEmpsWithMatch.MoveNext();
			}
			if (this.FilteredEmployeesChanged != null)
				this.FilteredEmployeesChanged();
		}
	}
}
