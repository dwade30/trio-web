//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCheckRegister : BaseForm
	{
		public frmCheckRegister()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckRegister InstancePtr
		{
			get
			{
				return (frmCheckRegister)Sys.GetInstance(typeof(frmCheckRegister));
			}
		}

		protected frmCheckRegister _InstancePtr = null;
		public string strPrinterName = "";
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsMultipleWarrants = new clsDRWrapper();
			public clsDRWrapper rsMultipleWarrants_AutoInitialized = null;
			public clsDRWrapper rsMultipleWarrants
			{
				get
				{
					if ( rsMultipleWarrants_AutoInitialized == null)
					{
						 rsMultipleWarrants_AutoInitialized = new clsDRWrapper();
					}
					return rsMultipleWarrants_AutoInitialized;
				}
				set
				{
					 rsMultipleWarrants_AutoInitialized = value;
				}
			}
		DateTime datPayDate;
		int intPayRunID;
        public List<string> batchReports;

        private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			int counter;
			int lngJournal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cmbReprint.Visible = false;
                lblReprint.Visible = false;
                TryAgain:
				;
				bool executePrintReports = false;
				if (cmbReprint.Text == "Initial Run")
				{
					rsMultipleWarrants.OpenRecordset("SELECT DISTINCT PayDate, PayRunID From tblPayrollSteps WHERE Checks = 1 AND isnull(CheckRegister, 0) = 0");
					if (rsMultipleWarrants.EndOfFile() != true && rsMultipleWarrants.BeginningOfFile() != true)
					{
						if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank")) == 0)
						{
							rsInfo.OpenRecordset("SELECT * FROM tblPayrollSteps WHERE PayDate = '" + rsMultipleWarrants.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMultipleWarrants.Get_Fields("PayRunID"));
							if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
							{
								lngJournal = FCConvert.ToInt32(rsInfo.Get_Fields("JournalNumber"));
							}
							rsInfo.OpenRecordset("SELECT * FROM Banks WHERE Name <> ''", "TWBD0000.vb1");
							if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
							{
								cboBanks.Clear();
								do
								{
									cboBanks.AddItem(rsInfo.Get_Fields("ID") + " - " + rsInfo.Get_Fields_String("Name"));
									rsInfo.MoveNext();
								}
								while (rsInfo.EndOfFile() != true);
								cboBanks.SelectedIndex = 0;
							}
							else
							{
								MessageBox.Show("You must set up your banks before you may continue with this process.", "No Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								Close();
								return;
							}
							rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
							if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
							{
								if (Conversion.Val(rsMasterJournal.Get_Fields("BankNumber")) == 0)
								{
									// do nothing
								}
								else
								{
									for (counter = 0; counter <= cboBanks.Items.Count - 1; counter++)
									{
										if (Conversion.Val(fecherFoundation.Strings.Trim(Strings.Left(cboBanks.Items[counter].ToString(), 2))) == Conversion.Val(rsMasterJournal.Get_Fields("BankNumber")))
										{
											cboBanks.SelectedIndex = counter;
											executePrintReports = true;
											goto PrintReports;
										}
									}
								}
							}
							else
							{
								// do nothing
							}
							fraBanks.Visible = true;
							cmbReprint.Visible = false;
                            lblReprint.Visible = false;
                            //cmdCancel.Visible = false;
                            cmdProcess.Visible = false;
							cboBanks.Focus();
							return;
						}
						else
						{
							executePrintReports = true;
							goto PrintReports;
						}
					}
					else
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
						{
							cmbReprint.Text = "Reprint Register";
							goto TryAgain;
						}
						else
						{
							MessageBox.Show("There is no unprocessed information for this Check Register.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						modDavesSweetCode.Statics.blnReportCompleted = true;
						Close();
						return;
					}
				}
				else
				{
					datPayDate = DateTime.Today;
					intPayRunID = 1;
					if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						frmSelectDateInfo.InstancePtr.Init2("Date Selection", -1, -1, -1, ref datPayDate, ref intPayRunID, true);
					}
					else
					{
						datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
						intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
						modGlobalVariables.Statics.gboolCancelSelected = false;
					}
					// Call frmSelectDateInfo.Init("Please select the pay date you wish to have reported for.", , , , datPayDate, intPayRunID)
					if (modGlobalVariables.Statics.gboolCancelSelected)
					{
						modGlobalVariables.Statics.gboolCancelSelected = false;
						Close();
						return;
					}
					else
					{
						if (datPayDate.ToOADate() != 0)
						{
							cmbReprint.Visible = false;
                            lblReprint.Visible = false;
                            //cmdCancel.Visible = false;
                            cmdProcess.Visible = false;
							if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
							{
								strPrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
							}
							else
							{
								fecherFoundation.Information.Err().Clear();
                                strPrinterName = ""; //FCGlobal.Printer.DeviceName;
                            }
							if (modGlobalConstants.Statics.gboolPrintALLPayRuns == false)
							{
								rsMultipleWarrants.OpenRecordset("SELECT DISTINCT PayDate, PayRunID From tblPayrollSteps WHERE CheckRegister = 1 AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID));
							}
							else
							{
								rsMultipleWarrants.OpenRecordset("SELECT DISTINCT PayDate From tblPayrollSteps WHERE CheckRegister = 1 AND PayDate = '" + FCConvert.ToString(datPayDate) + "'");
							}
							this.Hide();
							if (rsMultipleWarrants.RecordCount() > 1)
							{
								do
								{
									modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
									// rptCheckRegister.PrintReport False
									rptCheckRegister.InstancePtr.Unload();
									CheckAgain2:
									;
									if (modDavesSweetCode.JobComplete("rptCheckRegister"))
									{
										rsMultipleWarrants.MoveNext();
									}
									else
									{
										goto CheckAgain2;
									}
								}
								while (rsMultipleWarrants.EndOfFile() != true);
							}
							else
							{
								if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
								{
									rptCheckRegister.InstancePtr.Unload();
									modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
									// rptCheckRegister.PrintReport False
								}
								else
								{
									// rptCheckRegister.Show , MDIParent
									if (rsMultipleWarrants.RecordCount() == 0)
									{
										MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
									else
									{
										rptCheckRegister.InstancePtr.Unload();
										modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
										// rptCheckRegister.PrintReport False
									}
								}
							}
						}
						else
						{
							Close();
							return;
						}
					}
				}
				PrintReports:
				;
				if (executePrintReports)
				{
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						strPrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					}
					else
					{
						fecherFoundation.Information.Err().Clear();
                        strPrinterName = ""; //FCGlobal.Printer.DeviceName;
					}
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
					{
						this.Hide();
					}
					if (rsMultipleWarrants.RecordCount() > 1)
					{
						// rptCheckRegister.FCGlobal.FCGlobal.Printer.DeviceName = strPrinterName
						do
						{
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
							rptCheckRegister.InstancePtr.Unload();
							CheckAgain:
							;
							if (modDavesSweetCode.JobComplete("rptCheckRegister"))
							{
								rsMultipleWarrants.MoveNext();
							}
							else
							{
								goto CheckAgain;
							}
						}
						while (rsMultipleWarrants.EndOfFile() != true);
					}
					else
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
						{
							rptCheckRegister.InstancePtr.Unload();
							// rptCheckRegister.FCGlobal.FCGlobal.Printer.DeviceName = strPrinterName
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
							modGlobalRoutines.UpdatePayrollStepTable("CheckRegister", "", "", rsMultipleWarrants.Get_Fields_DateTime("PayDate"), rsMultipleWarrants.Get_Fields("PayRunID"));
						}
						else
						{
							rptCheckRegister.InstancePtr.Unload();
							// rptCheckRegister.FCGlobal.FCGlobal.Printer.DeviceName = strPrinterName
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
							modGlobalRoutines.UpdatePayrollStepTable("CheckRegister", "", "", rsMultipleWarrants.Get_Fields_DateTime("PayDate"), rsMultipleWarrants.Get_Fields("PayRunID"));
						}
					}
					
					Close();
					executePrintReports = false;
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmdProcess", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void Command2_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void Command3_Click(object sender, System.EventArgs e)
		{
			string strDefaultPrinterName;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (cboBanks.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a bank to use for your check rec before you may continue.", "No Bank Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
					{
						strPrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					}
					else
					{
						fecherFoundation.Information.Err().Clear();
                        strPrinterName = "";// FCGlobal.Printer.DeviceName;
					}
					if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
					{
						this.Hide();
					}
					if (rsMultipleWarrants.RecordCount() > 1)
					{
						do
						{
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
							rptCheckRegister.InstancePtr.Unload();
							CheckAgain:
							;
							if (modDavesSweetCode.JobComplete("rptCheckRegister"))
							{
								rsMultipleWarrants.MoveNext();
							}
							else
							{
								goto CheckAgain;
							}
						}
						while (rsMultipleWarrants.EndOfFile() != true);
					}
					else
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
						{
							rptCheckRegister.InstancePtr.Unload();
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
						}
						else
						{
							// rptCheckRegister.Show , MDIParent
							rptCheckRegister.InstancePtr.Unload();
							modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName, batchReports: batchReports);
							// rptCheckRegister.PrintReport False
						}
					}
					Close();
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Command3_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void frmCheckRegister_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (modGlobalVariables.Statics.gstrMQYProcessing != "NONE" || modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				cmdProcess_Click();
			}

			this.Refresh();
		}

		private void frmCheckRegister_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCheckRegister_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
		}
    }
}
