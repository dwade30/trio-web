﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cEmployerRecord
	{
		//=========================================================
		private string strEmployerName = string.Empty;
		private string strEIN = string.Empty;
		private string strAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strContactTelephone = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string Zip4
		{
			set
			{
				strZip4 = value;
			}
			get
			{
				string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}

		public string Name
		{
			set
			{
				strEmployerName = value;
			}
			get
			{
				string Name = "";
				Name = strEmployerName;
				return Name;
			}
		}

		public string EIN
		{
			set
			{
				strEIN = value;
			}
			get
			{
				string EIN = "";
				EIN = strEIN;
				return EIN;
			}
		}

		public string Address
		{
			set
			{
				strAddress = value;
			}
			get
			{
				string Address = "";
				Address = strAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string PostalCode
		{
			get
			{
				string PostalCode = "";
				string strReturn;
				strReturn = strZip;
				if (fecherFoundation.Strings.Trim(strZip4) != "")
				{
					strReturn += " " + strZip4;
				}
				PostalCode = strReturn;
				return PostalCode;
			}
		}

		public string Telephone
		{
			set
			{
				strContactTelephone = value;
			}
			get
			{
				string Telephone = "";
				Telephone = strContactTelephone;
				return Telephone;
			}
		}
	}
}
