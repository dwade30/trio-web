//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Windows.Forms;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;
using Control = Wisej.Web.Control;
using DataGridViewCell = Wisej.Web.DataGridViewCell;
using DataGridViewCellFormattingEventArgs = Wisej.Web.DataGridViewCellFormattingEventArgs;
using DialogResult = Wisej.Web.DialogResult;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;

namespace TWPY0000
{
	public partial class frmEmployeeMaster : BaseForm
	{
        private int lastW4Year = 0;
		public frmEmployeeMaster()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkFirstCheckOnly = new System.Collections.Generic.List<FCCheckBox>();
			this.chkFirstCheckOnly.AddControlArrayElement(this.chkFirstCheckOnly_1, 1);
			this.chkFirstCheckOnly.AddControlArrayElement(this.chkFirstCheckOnly_0, 0);
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

            txtSSN.AllowOnlyNumericInput();
            txtFederalStatus.AllowOnlyNumericInput();
            txtStateStatus.AllowOnlyNumericInput();
            txtFederalTaxes.AllowOnlyNumericInput();
            txtStateTaxes.AllowOnlyNumericInput();
            txtBLSWorksiteID.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',45, 48..57");
            txtStdDay.AllowOnlyNumericInput();
            txtStdWk.AllowOnlyNumericInput();
            txtSeqNumber.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',45, 49..57");
            this.Label37 = new System.Collections.Generic.List<FCLabel>();
			this.Label37.AddControlArrayElement(Label37_0, 0);
			this.Label37.AddControlArrayElement(Label37_1, 1);
			this.Label37.AddControlArrayElement(Label37_2, 2);
			this.Label37.AddControlArrayElement(Label37_4, 4);
			this.Label37.AddControlArrayElement(Label37_5, 5);
            //FC:FINAL:AM:#3850 - use mask
           // this.txtSSN.Mask = "999-99-9999";
            txtW4Date.Validating += TxtW4Date_Validating;
        }

        private void TxtW4Date_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (boolCantEdit)
            {
                return;
            }
            if (txtW4Date.Text.IsDate() )
            {
                var w4Date = txtW4Date.Text.ToDate();
                if (w4Date.Year > 2019)
                {
                    chkW4MultJobs.Enabled = true;
                    txtFederalOtherIncome.Enabled = true;
                    txtFederalOtherDependents.Enabled = true;
                    lblOtherDeductions.Enabled = true;
                    lblOtherIncome.Enabled = true;
                    txtFederalAdditionalDeduction.Enabled = true;
                }
                else
                {
                    chkW4MultJobs.Checked = false;
                    chkW4MultJobs.Enabled = false;
                    txtFederalOtherIncome.Text = "";
                    txtFederalOtherIncome.Enabled = false;
                    txtFederalOtherDependents.Text = "";
                    txtFederalOtherDependents.Enabled = false;
                    txtFederalAdditionalDeduction.Enabled = false;                    
                    lblOtherDeductions.Enabled = false;
                    lblOtherIncome.Enabled = false;
                    cboFedPayStatus.Focus();

                }
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmEmployeeMaster InstancePtr
		{
			get
			{
				return (frmEmployeeMaster)Sys.GetInstance(typeof(frmEmployeeMaster));
			}
		}

		protected frmEmployeeMaster _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTEW S. LARRABEE AND DAN C. SOLTESZ
		// DATE:       MAY 02,2001
		//
		// NOTES: MODIFIED ON AUGUST 8/19/2004
		// ADDED THE COMMENTS SECTION TO THE EMPLOYEE MASTER
		// THIS FRAME MUST BE IN THE FRONT
		//
		//
		// **************************************************
		// private local variables
		private bool gboolDontClose;
		//FC:FINAL:DSE:#i2056 Use property for auto-instantiating variable
		private clsDRWrapper _rsMaster = null;
		private clsDRWrapper rsMaster
		{
			set
			{
				_rsMaster = value;
			}
			get
			{
				if(_rsMaster == null)
				{
					_rsMaster = new clsDRWrapper();
				}
				return _rsMaster;
			}
		}
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		// vbPorter upgrade warning: intWidth As int	OnWriteFCConvert.ToInt32(
		private int intWidth;
		private int intErrCount;
		// how many boxes on form are blank
		public bool ActiveForm;
		// has EmployeeMaster been active is so when we do so flag so when we call search we clean up first
		private bool DeleteFlag;
		// we just want to delete and not add a user
		public bool boolFromMaster;
		private string NewCurrentEmployee = "";
		private bool NewFlag;
		private int intChildID;
		private bool boolChild;
		private bool boolInvalidData;
		private bool boolError;
		private int intDeptLength;
		private int intDivLength;
		private string strFormat = string.Empty;
		// vbPorter upgrade warning: strSplit As object	OnWrite(string())
		private string[] strSplit;
		private bool boolCantEdit;

        private SSNViewType ssnViewPermission = SSNViewType.None;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private string strCurrentEmployee = "";

		private void SetupGridSchedule()
		{
			string strTemp = "";
			GridSchedule.TextMatrix(0, 0, "Schedule");
			GridSchedule.TextMatrix(0, 1, "Week");
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from schedules order by name", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
                strTemp = "#0;None|";
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("id"))) + ";" + rsLoad.Get_Fields("name") + "|";
					rsLoad.MoveNext();
				}
				//strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                strTemp = strTemp.Substring(0, strTemp.Length - 1);
				GridSchedule.ColComboList(0, strTemp);
			}
			else
			{
				GridSchedule.Visible = false;
			}
		}
		// vbPorter upgrade warning: lngScheduleID As int	OnWrite(double, int)
		private void SetWeekforSchedule(int lngScheduleID)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			int intTemp;
			bool boolFound;
			int intFirst;
			boolFound = false;
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(GridSchedule.TextMatrix(1, 1))));
			intFirst = 0;
			rsLoad.OpenRecordset("select * from scheduleweeks where scheduleid = " + FCConvert.ToString(lngScheduleID) + " order by orderno", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("orderno"))) + ";" + rsLoad.Get_Fields("name") + "|";
					if (intFirst == 0)
					{
						intFirst = FCConvert.ToInt16(rsLoad.Get_Fields("orderno"));
					}
					if (FCConvert.ToInt32(rsLoad.Get_Fields("orderno")) == intTemp)
					{
						boolFound = true;
					}
					rsLoad.MoveNext();
				}
			}
			else
			{
				strTemp = "#0;None|";
			}
			//strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            strTemp = strTemp.Substring(0, strTemp.Length - 1);

            GridSchedule.ColComboList(1, strTemp);

            if (!boolFound)
			{
				GridSchedule.TextMatrix(1, 1, FCConvert.ToString(intFirst));
			}
		}

		private void ResizeGridSchedule()
		{
			int GridWidth = 0;
			if (GridSchedule.Visible)
			{
				GridWidth = GridSchedule.WidthOriginal;
				GridSchedule.ColWidth(0, FCConvert.ToInt32(0.8 * GridWidth));
				//GridSchedule.Height = GridSchedule.RowHeight(0) * 2 + 60;
			}
		}

		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void GetDeptDivLengths()
		{
			clsDRWrapper rsLengths = new clsDRWrapper();
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				rsLengths.OpenRecordset("Select * from Budgetary", "budgetary");
				if (!rsLengths.EndOfFile())
				{
					intDeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(fecherFoundation.Strings.Trim(rsLengths.Get_Fields_String("Expenditure") + " "), 2))));
					intDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(fecherFoundation.Strings.Trim(rsLengths.Get_Fields_String("Expenditure") + " "), 3, 2))));
				}
			}
			else
			{
				rsLengths.OpenRecordset("Select DeptLength,DivLength from tblFieldLengths", "Twpy0000.vb1");
				if (!rsLengths.EndOfFile())
				{
					intDeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsLengths.Get_Fields_Int32("DeptLength") + ""))));
					intDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsLengths.Get_Fields_Int32("DivLength") + ""))));
				}
				else
				{
					MessageBox.Show("Employee Field lengths have not been set up. Employee Dept/Div will not be correct until information on this screen has been entered.", "TRIO Sofware", MessageBoxButtons.OK, MessageBoxIcon.Information);
					intDeptLength = 0;
					intDivLength = 0;
					boolError = true;
				}
			}
			strFormat = string.Empty;
			strFormat = Strings.StrDup(intDeptLength, "#");
			strFormat += "-";
			strFormat += Strings.StrDup(intDivLength, "#");
		}

		private void cboType_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cboType_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// record that this form is now dirty and there is data to save
				intDataChanged += 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				// vbPorter upgrade warning: intResponse As int	OnWrite(DialogResult)
				DialogResult intResponse = 0;
				if (intDataChanged > 0 && !boolCantEdit)
				{
					if (strCurrentEmployee != modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber)
					{
						// current employee has changed outside of this form.  Do not save
						return;
					}
					gboolDontClose = false;
					intResponse = MessageBox.Show("Current changes have not been saved. Do so now?", "Employee Add / Update", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intResponse == DialogResult.Yes)
					{
						// save all changes
						if (NotValidData())
						{
							gboolDontClose = true;
							return;
						}
						else
						{
							mnuSave_Click();
						}
					}
					else if (intResponse == DialogResult.Cancel)
					{
						gboolDontClose = true;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cboCheck_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboFedDollarPercent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboFedDollarPercent_Leave(object sender, System.EventArgs e)
		{
			if (cboFedDollarPercent.Text == string.Empty)
			{
				MessageBox.Show("Federal, No Selection for Dollars or Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboFedDollarPercent.SelectedIndex = 0;
			}
			if (Conversion.Val(txtFederalTaxes.Text) > 100 && cboFedDollarPercent.Text == "Percent")
			{
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboFedDollarPercent.Focus();
			}
		}

		private void cboFedPayStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboPayFrequency_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboPayFrequency_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboPayFrequency.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
		}

		private void cboSex_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboState_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboStateDollarPercent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboStateDollarPercent_Leave(object sender, System.EventArgs e)
		{
			if (cboStateDollarPercent.Text == string.Empty)
			{
				MessageBox.Show("State, No Selection for Dollars or Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboStateDollarPercent.SelectedIndex = 0;
			}
			if (Conversion.Val(txtStateTaxes.Text) > 100 && cboStateDollarPercent.Text == "Percent")
			{
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboStateDollarPercent.Focus();
			}
		}

		private void cboStatePayStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkFicaExempt_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkMedicare_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkFTOrPT_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkUnemploymentExempt_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkW2DefIncome_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkW2Pen_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkW2StatutoryEmployee_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkWorkersCompExempt_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraComments.Visible = false;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			// SAVE THE COMMENTS FROM THE MASTER SCREEN.
			// MATTHEW 08/06/2004
			rsData.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
			if (rsData.EndOfFile())
			{
				MessageBox.Show("Employee information must be saved before comments can be added.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				rsData.Edit();
				rsData.Set_Fields("Comments", fecherFoundation.Strings.Trim(txtComments.Text));
				rsData.Update();
				// lblComments.Visible = Trim(txtComments) <> vbNullString
				ImgComment.Visible = fecherFoundation.Strings.Trim(txtComments.Text) != string.Empty;
				clsAuditChangesReporting clsRChanges = new clsAuditChangesReporting();
				clsRChanges.AddChange("Edited Comment");
				clsRChanges.SaveToAuditChangesTable("Employee Add/Update", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()), "", "twpy0000.vb1");
			}
			fraComments.Visible = false;
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmEmployeeMaster_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolError)
				{
					Close();
					return;
				}
				// Call ForceFormToResize(Me)
				GetDeptDivLengths();
				if (boolError)
				{
					Close();
					return;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyDown";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// make the enter key work like the tab
				if (KeyCode == Keys.Return)
				{
					if (this.ActiveControl.Text == txtComments.Text)
					{
					}
					else
					{
						KeyCode = (Keys)0;
						Support.SendKeys("{TAB}", false);
						return;
					}
				}
				// insert key pressed
				// If KeyCode = 45 Then
				// If Me.ActiveControl = txtCity Then
				// txtCity.Text = DefaultInfo.City
				// Call SetComboByIndex(cboState, DefaultInfo.StateID)
				// txtZip.Text = DefaultInfo.Zip
				// ElseIf Me.ActiveControl = cboState Then
				// Call SetComboByIndex(cboState, DefaultInfo.StateID)
				// txtZip.Text = DefaultInfo.Zip
				// ElseIf Me.ActiveControl = txtZip Then
				// txtZip.Text = DefaultInfo.Zip
				// End If
				// 
				// THIS INDICATES T
				// intDataChanged = intDataChanged + 1
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				// we are trying to set a combo box to an entry that is
				// not in the list
				if (FCConvert.ToDouble(fecherFoundation.Information.Err(ex).Description) == 383)
				{
					/*? Resume Next; */
				}
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// was esc key pressed
				// this is the keypress and the keycode is not the same.
				if (KeyAscii == Keys.Escape)
					Close();
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        public void LoadFederalPayStatusCombo()
        {
            var statusService = new FederalPayStatusService();
            rsMaster.OpenRecordset("SELECT * FROM tblPayStatuses WHERE Type='Federal' order by statuscode", "TWPY0000.vb1");            
            while (!rsMaster.EndOfFile())
            {
                // display Filing status
                var payStatus = new FederalPayStatus()
                {
                    Description = rsMaster.Get_Fields_String("Description"),
                    Id = rsMaster.Get_Fields_Int32("Id"),
                    StatusType = statusService.StatusFromCode(rsMaster.Get_Fields_Int32("StatusCode")),
                    StatusCode = rsMaster.Get_Fields_Int32("StatusCode"),
                    UseTable = rsMaster.Get_Fields_String("UseTable")
                };
                switch (payStatus.StatusType)
                {
                    case FederalPayStatusType.Married:
                        payStatus.DeductionType = FederalDeductionType.Married;
                        break;
                    default:
                        payStatus.DeductionType = FederalDeductionType.Single;
                        break;
                }
                cboFedPayStatus.Items.Add(payStatus);

                
                rsMaster.MoveNext();
            }
            
        }

		public void LoadPayStatusCombo(ref FCComboBox ComboName, string strType)
		{
			rsMaster.OpenRecordset("SELECT * FROM tblPayStatuses WHERE Type='" + strType + "'", "TWPY0000.vb1");
			if (!rsMaster.EndOfFile())
			{
				rsMaster.MoveLast();
				rsMaster.MoveFirst();
			}
			
			while (!rsMaster.EndOfFile())
			{
				// display Filing status
				ComboName.AddItem(FCConvert.ToString(rsMaster.Get_Fields("Description")));
				ComboName.ItemData(ComboName.NewIndex, FCConvert.ToInt32(rsMaster.Get_Fields("ID")));
				rsMaster.MoveNext();
			}
			rsMaster = null;
		}

		private void frmEmployeeMaster_Load(object sender, System.EventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vsElasticLight1.Enabled = True
				modGlobalRoutines.CheckCurrentEmployee();
				ActiveForm = true;
				DeleteFlag = false;
				NewFlag = false;
				intWidth = cboPayFrequency.WidthOriginal;
				boolFromMaster = false;
				boolInvalidData = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;                        
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        break;
                }

                if (ssnViewPermission != SSNViewType.Full)
                {
                    cmdNew.Enabled = false;
                    cmdNew.Visible = false;
                    mnuNew.Visible = false;
                    mnuNew.Enabled = false;
                }
				else
                {
                    txtSSN.Mask = "999-99-9999";
                }

				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.TAXSTATUSCODES)) != "F")
				{
					cmdPay.Visible = false;
				}
				boolCantEdit = false;
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					cmdNew.Visible = false;
					cmdSave.Visible = false;
					mnuSaveExit.Enabled = false;
					cmdSave.Visible = false;
					cmdTemporaryOverrides.Visible = false;
					cboStatus.Enabled = false;
					cboCheck.Enabled = false;
					cboFedDollarPercent.Enabled = false;
					cboFedPayStatus.Enabled = false;
					cboPayFrequency.Enabled = false;
					cboSex.Enabled = false;
					cboState.Enabled = false;
					cboStateDollarPercent.Enabled = false;
					cboStatePayStatus.Enabled = false;
					cboStatus.Enabled = false;
					chkFicaExempt.Enabled = false;
					chkFTOrPT.Enabled = false;
					chkMedicare.Enabled = false;
					chkPrint.Enabled = false;
					chkUnemploymentExempt.Enabled = false;
					chkW2DefIncome.Enabled = false;
					chkW2Pen.Enabled = false;
					chkW2StatutoryEmployee.Enabled = false;
					chkWorkersCompExempt.Enabled = false;
					chkFirstCheckOnly[0].Enabled = false;
					chkFirstCheckOnly[1].Enabled = false;
					chkFirstCheckOnly[2].Enabled = false;
					txtAddress1.Enabled = false;
					txtAddress2.Enabled = false;
					txtBLSWorksiteID.Enabled = false;
					txtCity.Enabled = false;
					txtCode1.Enabled = false;
					txtCode2.Enabled = false;
					txtComments.Enabled = false;
					txtDateAnniversary.Enabled = false;
					txtDateBirth.Enabled = false;
					txtDateHire.Enabled = false;
					txtDeptDiv.Enabled = false;
					txtDesignation.Enabled = false;
					txtEmail.Enabled = false;
					txtFederalStatus.Enabled = false;
					txtFederalTaxes.Enabled = false;
					txtFirstName.Enabled = false;
					txtGroupID.Enabled = false;
					txtLastName.Enabled = false;
					txtMiddleName.Enabled = false;
					txtPhone.Enabled = false;
					txtRetirementDate.Enabled = false;
					txtSeqNumber.Enabled = false;
					txtSSN.Enabled = false;
					txtStateStatus.Enabled = false;
					txtStateTaxes.Enabled = false;
					txtStdDay.Enabled = false;
					txtStdWk.Enabled = false;
					txtTerminationDate.Enabled = false;
					txtWorkCompCode.Enabled = false;
					txtZip.Enabled = false;
					txtZip4.Enabled = false;
                    txtW4Date.Enabled = false;
                    chkW4MultJobs.Enabled = false;
                    txtFederalAdditionalDeduction.Enabled = false;
                    txtFederalOtherIncome.Enabled = false;
                    txtFederalOtherDependents.Enabled = false;
                }

                if (!boolCantEdit)
                {
                    if (ssnViewPermission != SSNViewType.Full)
                    {
                        txtSSN.Enabled = false;
                    }
                }
                modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				// load filing status
				//LoadPayStatusCombo(ref cboFedPayStatus, "FEDERAL");
                LoadFederalPayStatusCombo();
				LoadPayStatusCombo(ref cboStatePayStatus, "STATE");
				SetupGridSchedule();
				rsMaster.OpenRecordset("SELECT * FROM tblFrequencyCodes order by ID", "TWPY0000.vb1");
				if (!rsMaster.EndOfFile())
				{
					rsMaster.MoveLast();
					rsMaster.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblFrequencyCodes table
					for (intCounter = 1; intCounter <= (rsMaster.RecordCount()); intCounter++)
					{
						cboPayFrequency.AddItem(rsMaster.Get_Fields("FrequencyCode") + "    - " + rsMaster.Get_Fields("Description"));
						cboPayFrequency.ItemData(cboPayFrequency.NewIndex, FCConvert.ToInt32(rsMaster.Get_Fields("ID")));
						rsMaster.MoveNext();
					}
				}
				rsMaster = null;

				// load the state combo box
				//FC:FINAL:DSE:#i2056 A property cannot be passed as a ref parameter
				clsDRWrapper temp = this.rsMaster;
				modGlobalRoutines.LoadStates(ref cboState, ref temp);
				this.rsMaster = temp;
				// Get the data from the database
				ShowData();
				if (!boolError)
				{
					modMultiPay.Statics.gstrEmployeeCaption = lblEmployeeNumber.Text;
					if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
					{
						MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
						lblEmployeeNumber.Text = "";
					}
					else if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == "-1")
					{
						// this is a new record
						lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption;
					}
					else
					{
						lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
					}
					intDataChanged = 0;
				}
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
				// -------------------------------------------------------------------
				// Dave 12/14/2006---------------------------------------------------
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				clsHistoryClass.SetGridIDColumn("PY");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        public void SetFederalPayStatusById(int id)
        {
            foreach (FederalPayStatus item in cboFedPayStatus.Items)
            {
                if (item.Id == id)
                {
                    cboFedPayStatus.SelectedItem = item;
                    break;
                }
            }
        }
        public void SetFederalPayStatus(FederalPayStatusType statusType)
        {
            foreach (FederalPayStatus item in cboFedPayStatus.Items)
            {
                if (item.StatusType == statusType)
                {
                    cboFedPayStatus.SelectedItem = item;
                    break;
                }
            }
        }
		public void SetId(ref FCComboBox ComboName, string strType)
		{
			// routine to set pay status by ID
			for (intCounter = 0; intCounter <= ComboName.Items.Count - 1; intCounter++)
			{
				if (ComboName.ItemData(intCounter) == FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsMaster.Get_Fields(strType) + " ")))
				{
					ComboName.SelectedIndex = intCounter;
					break;
				}
			}
		}

		public void SetCheckbox(ref FCCheckBox chkName, string strType)
		{
			// should check boxes be checked or unchecked according to DB
			if (rsMaster.Get_Fields(strType) == true)
			{
				chkName.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkName.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					lblEmployeeNumber.Text = FCConvert.ToString(lblEmployeeNumber.Tag);
					boolError = true;
					return;
				}
				else if (Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
				{
					mnuNew_Click();
					if (boolError)
						return;
				}
				else
				{
					modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				}
				strCurrentEmployee = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				// get all of the records from the database
				boolChild = false;
				rsMaster.OpenRecordset("select open1,open2 from tbldefaultinformation", "twpy0000.vb1");
				if (!rsMaster.EndOfFile())
				{
					lblCode1.Text = FCConvert.ToString(rsMaster.Get_Fields("open1"));
					lblCode2.Text = FCConvert.ToString(rsMaster.Get_Fields("open2"));
				}
				if (fecherFoundation.Strings.Trim(lblCode1.Text) == "")
				{
					lblCode1.Text = "Code 1";
				}
				if (fecherFoundation.Strings.Trim(lblCode2.Text) == "")
				{
					lblCode2.Text = "Code 2";
				}

                rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "'", "TWPY0000.vb1");
				if (!rsMaster.EndOfFile())
				{
					GridSchedule.TextMatrix(1, 0, FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("scheduleid"))));
					SetWeekforSchedule(FCConvert.ToInt32(Conversion.Val(rsMaster.Get_Fields("scheduleid"))));
					GridSchedule.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("currentscheduleweek"))));
					txtFirstName.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("FirstName") + " ");
					txtDesignation.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Desig") + " ");
					txtLastName.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("LastName") + " ");
					txtMiddleName.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("MiddleName") + " ");
					txtAddress1.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Address1") + " ");
					txtAddress2.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Address2") + " ");
					txtCity.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("City") + " ");
					txtPhone.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Phone") + " ");
					txtEmail.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Email") + " ");
					if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("State")))) == 0)
					{
						modGlobalRoutines.SetComboByName(ref cboState, fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("State"))));
					}
					else
					{
						modGlobalRoutines.SetComboByIndex(ref cboState, FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(rsMaster.Get_Fields("State") + " "))));
					}
					txtZip.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("Zip") + " ");
					txtZip4.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Zip4") + " ");
                    switch (ssnViewPermission)
                    {
                        case SSNViewType.Full:
                            txtSSN.Text = Strings.Format(fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("SSN") + " "), "000-00-0000");                            
                            break;
                        case SSNViewType.Masked:
                            txtSSN.Text = "***-**-" + rsMaster.Get_Fields_String("SSN").Right(4);                            
                            break;
                        default:
                            txtSSN.Text =  "***-**-****";
                            break;
                    }                    
					cboStatus.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Status") + " ");
					txtDateHire.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("DateHire"))), "MM/dd/yyyy");
					txtDateAnniversary.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("DateAnniversary"))) + " ";
                    if (!rsMaster.Get_Fields_DateTime("DateBirth").IsEmptyDate())
                    {
                        txtDateBirth.Text =
                            Strings.Format(
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("DateBirth"))),
                                "MM/dd/yyyy");
                    }
                    else
                    {
                        txtDateBirth.Text = "";
                    }
                    txtTerminationDate.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("DateTerminated"))), "MM/dd/yyyy");
					txtRetirementDate.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("DateRetired"))), "MM/dd/yyyy");
					txtComments.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Comments")));
					// lblComments.Visible = Trim(txtComments) <> vbNullString
					ImgComment.Visible = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("comments"))) != string.Empty;
					if (modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID != modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber)
					{
						boolChild = true;
						rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
						if (rsMaster.EndOfFile())
						{
							boolChild = false;
							// no data has yet to be saved for this Multi Pay
							rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "'", "TWPY0000.vb1");
						}
					}
					if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("Check")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Check"))) != string.Empty)
					{
						cboCheck.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Check")));
					}
					if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("Sex")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Sex"))) != string.Empty)
					{
						// And Val(.Fields("Sex")) <> 0 Then
						cboSex.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Sex")));
					}
					// set check boxes
					SetCheckbox(ref chkW2Pen, "W2Pen");
					SetCheckbox(ref chkW2DefIncome, "W2DefIncome");
					SetCheckbox(ref chkW2StatutoryEmployee, "W2StatutoryEmployee");
					SetCheckbox(ref chkUnemploymentExempt, "UnemploymentExempt");
					SetCheckbox(ref chkWorkersCompExempt, "WorkersCompExempt");
					SetCheckbox(ref chkFicaExempt, "FicaExempt");
					SetCheckbox(ref chkMedicare, "MedicareExempt");
					SetCheckbox(ref chkPrint, "PrintPayRate");
					SetCheckbox(ref chkFTOrPT, "FTOrPT");
					SetCheckbox(ref chkFirstCheckOnly_0, "boolFedFirstCheckOnly");
					SetCheckbox(ref chkFirstCheckOnly_1, "boolStateFirstCheckOnly");

					if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("mqge")))
					{
						chkMQGE.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkMQGE.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					txtFederalTaxes.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("AddFed") + " ");
					if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("FedDollarPercent")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("FedDollarPercent"))) != string.Empty)
					{
						cboFedDollarPercent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("FedDollarPercent")));
					}

                    txtFederalAdditionalDeduction.Text = rsMaster.Get_Fields_Double("AddFederalDeduction").ToString();
                    txtFederalOtherIncome.Text = rsMaster.Get_Fields_Double("FederalOtherIncome").ToString();
                    txtFederalOtherDependents.Text = rsMaster.Get_Fields_Int32("FederalOtherDependents").ToString();
                    lastW4Year = 2019;
                    if (rsMaster.Get_Fields_DateTime("W4Date") != DateTime.MinValue && rsMaster.Get_Fields_DateTime("W4Date") != DateTime.FromOADate(0))
                    {
                        txtW4Date.Text = rsMaster.Get_Fields_DateTime("W4Date").FormatAndPadShortDate();
                        lastW4Year = rsMaster.Get_Fields_DateTime("W4Date").Year;
                        if (lastW4Year > 2019)
                        {                           
                            lblOtherDependents.Visible = true;
                            lblOtherDeductions.Enabled = true;
                            lblOtherIncome.Enabled = true;
                            chkW4MultJobs.Checked = rsMaster.Get_Fields_Boolean("W4MultipleJobs");
                            if (!boolCantEdit)
                            {
                                chkW4MultJobs.Enabled = true;
                                txtFederalOtherIncome.Enabled = true;
                                txtFederalOtherDependents.Enabled = true;                               
                            }
                        }
                        else
                        {
                            chkW4MultJobs.Checked = false;
                            chkW4MultJobs.Enabled = false;
                            txtFederalOtherIncome.Enabled = false;
                            txtFederalOtherDependents.Enabled = false;
                            lblOtherDeductions.Enabled = false;
                            lblOtherIncome.Enabled = false;
                            lblOtherDependents.Visible = false;
                        }
                    }
                    else
                    {
                        chkW4MultJobs.Checked = false;
                        chkW4MultJobs.Enabled = false;
                        txtFederalOtherIncome.Enabled = false;
                        txtFederalOtherDependents.Enabled = false;
                        lblOtherDeductions.Enabled = false;
                        lblOtherIncome.Enabled = false;
                    }

                    txtStateTaxes.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("AddState") + " ");
					if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("StateDollarPercent")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("StateDollarPercent"))) != string.Empty)
					{
						cboStateDollarPercent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("StateDollarPercent")));
					}
					
					// put current status in combo box
                    SetFederalPayStatusById(rsMaster.Get_Fields_Int32("FedFilingStatusID"));
					//SetId(ref cboFedPayStatus, "FedFilingStatusID");
					SetId(ref cboStatePayStatus, "StateFilingStatusID");

					txtFederalStatus.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_Int32("FedStatus") + " ");
					txtStateStatus.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_Int32("StateStatus") + " ");
					SetId(ref cboPayFrequency, "FreqCodeID");
					txtSeqNumber.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("SeqNumber") + " ");
					txtGroupID.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("GroupID") + " ");
					txtCode1.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Code1") + " ");
					txtCode2.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("Code2") + " ");
					txtBLSWorksiteID.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("BLSWorksiteID") + " ");
					txtDeptDiv.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("DeptDiv") + " ");
					if (modGlobalVariables.Statics.gboolHaveDivision)
					{
						lblDeptDiv.Text = "Home Dept/Div";
					}
					else
					{
						lblDeptDiv.Text = "Home Dept";
					}
					// ****************************************************************************
					txtWorkCompCode.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields_String("WorkCompCode") + " ");
					// ****************************************************************************
					
					txtStdDay.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("HrsDay") + " ");
					txtStdWk.Text = fecherFoundation.Strings.Trim(rsMaster.Get_Fields("HrsWk") + " ");
				}

				intDataChanged = 0;
				rsMaster = null;
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeMaster_Resize(object sender, System.EventArgs e)
		{
			ResizeGridSchedule();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				gboolDontClose = false;
				SaveChanges();
				modGlobalVariables.Statics.gboolDontChangeMenus = false;
				if (gboolDontClose)
				{
					modGlobalVariables.Statics.gboolDontChangeMenus = true;
					e.Cancel = true;
					return;
				}
				if (boolError)
				{
					frmEmployeeSearch.InstancePtr.Show(App.MainForm);
				}
				else
				{
					//MDIParent.InstancePtr.Show();
				}
				boolError = false;
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void GridSchedule_ComboCloseUp(object sender, EventArgs e)
		{
			int lngCol;
			int lngRow;
			// 
			lngRow = GridSchedule.Row;
			lngCol = GridSchedule.Col;
			if (lngRow > 0)
			{
				switch (lngCol)
				{
					case 0:
						{
							// schedule
							// vbPorter upgrade warning: lngID As int	OnWrite(string)
							int lngID = 0;
							lngID = FCConvert.ToInt32(GridSchedule.ComboData());
							SetWeekforSchedule(lngID);
							break;
						}
					case 1:
						{
							// week
							break;
						}
				}
				//end switch
			}
		}

		private void GridSchedule_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridSchedule[e.ColumnIndex, e.RowIndex];
            int lngCol;
			string strTemp;
			lngCol = GridSchedule.GetFlexColIndex(e.ColumnIndex);
			strTemp = "";
			switch (lngCol)
			{
				case 0:
					{
						strTemp = "Schedule to use for this employee";
						break;
					}
				case 1:
					{
						strTemp = "Current week rotation to use for this employee";
						break;
					}
			}
			//end switch
			//ToolTip1.SetToolTip(GridSchedule, strTemp);
			cell.ToolTipText = strTemp;
		}

		private void ImgComment_Click(object sender, System.EventArgs e)
		{
			mnuComments_Click();
		}
		// Private Sub lblCode1_DblClick()
		// lblCode1.Text = InputBox("Enter In New Description", "Change")
		// If lblCode1.Text = vbNullString Then lblCode1.Text = "Open 1"
		//
		// intDataChanged = intDataChanged + 1
		// End Sub
		//
		// Private Sub lblCode2_DblClick()
		// lblCode2.Text = InputBox("Enter In New Description", "Change")
		// If lblCode2.Text = vbNullString Then lblCode2.Text = "Open 2"
		// intDataChanged = intDataChanged + 1
		// End Sub
		private void mnuDelete_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (FCConvert.ToDouble(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) < 1)
					return;
				// Delete the field length information from the database
				if (MessageBox.Show("This action will delete Employee #" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + " infomation. Continue?", "Payroll Standard Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsMaster.Execute("Delete from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "Payroll");
					DeleteFlag = true;
					mnuNew_Click();
					intDataChanged = 0;
					MessageBox.Show("Delete of record was successful", "Payroll Standard Master", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuComments_Click(object sender, System.EventArgs e)
		{
			fraComments.Visible = true;
			fraComments.BringToFront();
		}

		public void mnuComments_Click()
		{
			mnuComments_Click(mnuComments, new System.EventArgs());
		}

		public void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (boolInvalidData == true)
					return;
				// validate that there is not changes that need to be saved
				// Call SaveChanges
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool ValidateEmployeeNumber(string intNumber)
		{
			bool ValidateEmployeeNumber = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ValidateEmployeeNumber";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber ='" + intNumber + "'", "TWPY0000.vb1");
				if (rsMaster.EndOfFile())
				{
					ValidateEmployeeNumber = false;
				}
				else
				{
					ValidateEmployeeNumber = true;
				}
				rsMaster = null;
				return ValidateEmployeeNumber;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return ValidateEmployeeNumber;
			}
		}

		public void mnuNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolError = false;
				if (DeleteFlag == false)
				{
					NewEmployee:
					;
					NewCurrentEmployee = Interaction.InputBox("Enter Employee Number", "New Employee", null);
					if (fecherFoundation.Strings.Trim(NewCurrentEmployee).Length >= 1)
					{
						if (Strings.Left(NewCurrentEmployee, 1) == "-" || Strings.Left(NewCurrentEmployee, 1) == "+")
						{
							MessageBox.Show("Invalid Employee Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							goto NewEmployee;
						}
						else
						{
							if (NewCurrentEmployee == string.Empty)
							{
								boolError = true;
								return;
							}
							else
							{
								txtLastName.Focus();
								rsMaster.OpenRecordset("Select * from tblFieldLengths", "Twpy0000.vb1");
								if (!rsMaster.EndOfFile())
								{
									if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("LengthRequired")))
									{
										if (fecherFoundation.Strings.Trim(NewCurrentEmployee).Length != FCConvert.ToInt32(rsMaster.Get_Fields("EmployeeNumber")))
										{
											MessageBox.Show("The employee length of " + rsMaster.Get_Fields("EmployeeNumber") + " is required.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            //FC:FINAL:SBE - #i2477 - in original the form is reloaded, and prompt to enter Employee number is displayed.
                                            //We are not initializing again the form, when it is unloaded (FCForm does not execute the InitializeComponent()/Ex() anymore)
                                            //To have the same behavior as in the original applcation, we force the Inputbox using goto statement
                                            goto NewEmployee;
                                            boolError = true;
											this.Unload();
                                            frmEmployeeSearch.InstancePtr.Show(App.MainForm);
											return;
										}
									}
									if (FCConvert.ToString(rsMaster.Get_Fields_String("EmployeeType")) == "Numeric")
									{
										if (!Information.IsNumeric(NewCurrentEmployee))
										{
											MessageBox.Show("The employee type must be Numeric.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            //FC:FINAL:SBE - #i2477 - in original the form is reloaded, and prompt to enter Employee number is displayed.
                                            //We are not initializing again the form, when it is unloaded (FCForm does not execute the InitializeComponent()/Ex() anymore)
                                            //To have the same behavior as in the original applcation, we force the Inputbox using goto statement
                                            goto NewEmployee;
                                            boolError = true;
											frmEmployeeSearch.InstancePtr.Show(App.MainForm);
											return;
										}
									}
								}
							}
							rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + NewCurrentEmployee + "'", "TWPY0000.vb1");
							while (ValidateEmployeeNumber(NewCurrentEmployee) == true)
							{
								if (DialogResult.OK == MessageBox.Show("Employee Number " + NewCurrentEmployee + " Is Already Being Used By " + rsMaster.Get_Fields_String("FirstName") + " " + rsMaster.Get_Fields_String("LastName") + " " + rsMaster.Get_Fields_String("Desig"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information))
								{
									boolError = true;
									frmEmployeeSearch.InstancePtr.Show(App.MainForm);
									return;
								}
								else
								{
									NewCurrentEmployee = Interaction.InputBox("Enter Employee Number", "New Employee", null);
								}
							}
							lblEmployeeNumber.Text = "Employee  " + NewCurrentEmployee;
						}
					}
					else
					{
						boolError = true;
						return;
					}
				}
				// clear out the controls but do nothing to the database
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						// this means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCTextBox).Text = string.Empty;
						}
					}
					else if (ControlName is FCComboBox)
					{
						// this means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							// ControlName.text = vbNullString
							if (FCConvert.ToString(ControlName.Tag) == "STAY")
							{
								if ((ControlName as FCComboBox).ListCount > 0)
									(ControlName as FCComboBox).ListIndex = 0;
							}
						}
					}
					else if (ControlName is FCCheckBox)
					{
						// this means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCCheckBox).CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					ControlName = null;
				}
				txtDateHire.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtDateAnniversary.Text = Strings.Format(DateTime.Today, "MM/dd");
				// txtDateBirth.Text = Format(Date, "MM/dd/yyyy")
				txtWorkCompCode.Text = "0";
				// maine has an ID number of 25
				modGlobalRoutines.SetComboByIndex(ref cboState, 25);
				chkPrint.CheckState = Wisej.Web.CheckState.Checked;
				chkFTOrPT.CheckState = Wisej.Web.CheckState.Unchecked;
				txtFederalStatus.Text = "0";
				txtStateStatus.Text = "0";
				txtSeqNumber.Text = "01";
				txtStdWk.Text = "40";
				txtStdDay.Text = "8";
				cboSex.Text = "N/A";
				txtGroupID.Text = FCConvert.ToString(0);
				chkFirstCheckOnly[0].CheckState = Wisej.Web.CheckState.Checked;
				chkFirstCheckOnly[1].CheckState = Wisej.Web.CheckState.Checked;
				//chkFirstCheckOnly[2].CheckState = Wisej.Web.CheckState.Checked;
				GridSchedule.TextMatrix(1, 0, "0");
				GridSchedule.TextMatrix(1, 1, "0");

                chkW4MultJobs.Enabled = false;
                txtFederalAdditionalDeduction.Enabled = false;
                txtFederalOtherDependents.Enabled = false;
                lblOtherIncome.Enabled = false;
                lblOtherDeductions.Enabled = false;

				// form is now dirty
				intDataChanged += 1;
				DeleteFlag = false;
				NewFlag = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				// this is here so that if the form is not yet shown the
				// setfocus line of code will not crash
				if (fecherFoundation.Information.Err(ex).Number == 5)
				{
					/*? Resume Next; */
				}
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuPayFrequency_Click(object sender, System.EventArgs e)
		{
			frmFrequencyCodes.InstancePtr.Show();
			boolFromMaster = true;
			intDataChanged += 1;
		}

		private void mnuPayStatus_Click(object sender, System.EventArgs e)
		{
			//frmPayStatuses.InstancePtr.Show();
            StaticSettings.GlobalCommandDispatcher.Send(new ShowTaxTables());
			boolFromMaster = true;
			intDataChanged += 1;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int x;
			try
			{

				fecherFoundation.Information.Err().Clear();
				intErrCount = 0;
				GridSchedule.Row = 0;

				if (NotValidData() == true)
					return;

				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				string strEmpNumToUse;
				strEmpNumToUse = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				if (NewFlag)
				{
					strEmpNumToUse = NewCurrentEmployee;
					// Reset all information pertianing to changes and start again
					intTotalNumberOfControls = 0;
					clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
					clsReportChanges.Reset();
					clsReportChanges.AddChange("New Employee #" + strEmpNumToUse);
					clsReportChanges.SaveToAuditChangesTable("Employee Add/Update", strEmpNumToUse, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
					clsReportChanges.Reset();
				}
				else
				{
					clsReportChanges.SaveToAuditChangesTable("Employee Add/Update", strEmpNumToUse, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
					// Reset all information pertianing to changes and start again
					intTotalNumberOfControls = 0;
					clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
					clsReportChanges.Reset();
				}
				// Initialize all the control and old data values
				FillControlInformationClass();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				if (NewFlag == true)
				{
					rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber ='" + NewCurrentEmployee + "'", "TWPY0000.vb1");
				}
				else
				{
					// get all of the records from the database
					rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber ='" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				}
				if (rsMaster.EndOfFile())
				{
					rsMaster.AddNew();
					rsMaster.Set_Fields("dataentry", false);
				}
				else
				{
					rsMaster.Edit();
				}
				if (NewFlag == true)
				{
					rsMaster.SetData("EmployeeNumber", NewCurrentEmployee);
				}
				else
				{
					rsMaster.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				}
				rsMaster.SetData("ChildRecord", boolChild);
				rsMaster.SetData("FirstName", txtFirstName.Text);
				rsMaster.SetData("LastName", txtLastName.Text);
				rsMaster.SetData("MiddleName", txtMiddleName.Text);
				rsMaster.SetData("Desig", txtDesignation.Text);
				rsMaster.SetData("Address1", txtAddress1.Text);
				rsMaster.SetData("Address2", txtAddress2.Text);
				rsMaster.SetData("City", txtCity.Text);
				rsMaster.SetData("Phone", txtPhone.Text);
				rsMaster.SetData("Email", txtEmail.Text);
				rsMaster.Set_Fields("Scheduleid", FCConvert.ToString(Conversion.Val(GridSchedule.TextMatrix(1, 0))));
				rsMaster.Set_Fields("currentscheduleweek", FCConvert.ToString(Conversion.Val(GridSchedule.TextMatrix(1, 1))));
				// 25 is the ID for the state of Maine
				rsMaster.SetData("State", (cboState.SelectedIndex < 0 ? 25 : cboState.ItemData(cboState.SelectedIndex)));
				rsMaster.SetData("Zip", txtZip.Text);
				rsMaster.SetData("Zip4", txtZip4.Text);
                if (ssnViewPermission == SSNViewType.Full)
                {
                    rsMaster.SetData("SSN", txtSSN.Text.Replace("-", ""));
                }

                rsMaster.SetData("Status", cboStatus.Text);
				if (txtDateHire.Text == string.Empty)
				{
					rsMaster.SetData("DateHire", null);
				}
				else
				{
					rsMaster.SetData("DateHire", txtDateHire.Text);
				}
				if (txtDateAnniversary.Text != string.Empty)
					rsMaster.SetData("DateAnniversary", txtDateAnniversary.Text);
				// If txtDateBirth <> vbNullString Then rsMaster.SetData "DateBirth", txtDateBirth
				if (txtDateBirth.Text == string.Empty)
				{
					rsMaster.SetData("DateBirth", null);
				}
				else
				{
					rsMaster.SetData("DateBirth", txtDateBirth.Text);
				}
				if (!Information.IsDate(txtTerminationDate.Text) || txtTerminationDate.Text == string.Empty)
				{
					rsMaster.SetData("DateTerminated", null);
				}
				else
				{
					rsMaster.SetData("DateTerminated", txtTerminationDate.Text);
				}
				if (!Information.IsDate(txtRetirementDate.Text) || txtRetirementDate.Text == string.Empty)
				{
					rsMaster.SetData("DateRetired", null);
				}
				else
				{
					rsMaster.SetData("DateRetired", txtRetirementDate.Text);
				}
				rsMaster.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
				rsMaster.SetData("Check", cboCheck.Text);
				rsMaster.SetData("Sex", cboSex.Text);
				rsMaster.SetData("W2Pen", FCConvert.ToInt32(chkW2Pen.CheckState));
				rsMaster.SetData("W2DefIncome", FCConvert.ToInt32(chkW2DefIncome.CheckState));
				rsMaster.SetData("W2StatutoryEmployee", FCConvert.ToInt32(chkW2StatutoryEmployee.CheckState));
				rsMaster.SetData("UnemploymentExempt", FCConvert.ToInt32(chkUnemploymentExempt.CheckState));
				rsMaster.SetData("WorkersCompExempt", FCConvert.ToInt32(chkWorkersCompExempt.CheckState));
				rsMaster.SetData("FicaExempt", FCConvert.ToInt32(chkFicaExempt.CheckState));
				rsMaster.SetData("MedicareExempt", FCConvert.ToInt32(chkMedicare.CheckState));
				rsMaster.SetData("PrintPayRate", FCConvert.ToInt32(chkPrint.CheckState));
				rsMaster.SetData("FTOrPT", FCConvert.ToInt32(chkFTOrPT.CheckState));
				rsMaster.SetData("AddFed", Conversion.Val(txtFederalTaxes.Text));
				rsMaster.SetData("AddState", Conversion.Val(txtStateTaxes.Text));
				rsMaster.SetData("FedDollarPercent", cboFedDollarPercent.Text);
				rsMaster.SetData("StateDollarPercent", cboStateDollarPercent.Text);
				rsMaster.SetData("boolFedFirstCheckOnly", FCConvert.ToInt32(chkFirstCheckOnly[0].CheckState));
				rsMaster.SetData("boolStateFirstCheckOnly", FCConvert.ToInt32(chkFirstCheckOnly[1].CheckState));
				rsMaster.SetData("AddFederalDeduction",txtFederalAdditionalDeduction.Text.ToDoubleValue());
                rsMaster.SetData("FederalOtherIncome", txtFederalOtherIncome.Text.ToDoubleValue());
                rsMaster.SetData("FederalOtherDependents", txtFederalOtherDependents.Text.ToIntegerValue());
                rsMaster.SetData("W4MultipleJobs",chkW4MultJobs.Checked);
                rsMaster.SetData("FederalOtherDependents",txtFederalOtherDependents.Text.ToIntegerValue());
                if (string.IsNullOrWhiteSpace(txtW4Date.Text) || !txtW4Date.Text.IsDate())
                {
                    rsMaster.SetData("W4Date" , null);
                }
                else
                {
                    rsMaster.SetData("W4Date", txtW4Date.Text.ToDate());
                }
				if (chkMQGE.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsMaster.Set_Fields("mqge", true);
				}
				else
				{
					rsMaster.Set_Fields("mqge", false);
				}
                if (cboFedPayStatus.SelectedItem != null)
				//if (cboFedPayStatus.SelectedIndex >= 0)
                {
                    var fedPayStatus = (FederalPayStatus)cboFedPayStatus.SelectedItem;
					rsMaster.SetData("FedFilingStatusID", fedPayStatus.Id);

				}
				if (cboStatePayStatus.SelectedIndex >= 0)
				{
					rsMaster.SetData("StateFilingStatusID", cboStatePayStatus.ItemData(cboStatePayStatus.SelectedIndex));
				}
				
				rsMaster.SetData("FedStatus", Conversion.Val(txtFederalStatus.Text));
				rsMaster.SetData("StateStatus", Conversion.Val(txtStateStatus.Text));
				if (cboPayFrequency.SelectedIndex >= 0)
				{
					rsMaster.SetData("FreqCodeID", cboPayFrequency.ItemData(cboPayFrequency.SelectedIndex));
				}
				rsMaster.SetData("SeqNumber", Conversion.Val(txtSeqNumber.Text));
				rsMaster.SetData("GroupID", txtGroupID.Text);
				// Matthew call id 79766 10/25/2005
				rsMaster.SetData("Code1", txtCode1.Text);
				rsMaster.SetData("Code2", txtCode2.Text);
				// rsMaster.SetData "Code1", Val(txtCode1)
				// rsMaster.SetData "Code2", Val(txtCode2)
				rsMaster.SetData("BLSWorksiteID", txtBLSWorksiteID.Text);
				// If rsMaster.Fields("descriptioncode1") <> lblCode1.Text Then
				// Call rsCodeDescription.Execute("update tbldefaultinformation set open1 = '" & lblCode1.Text & "'", "twpy0000.vb1")
				// End If
				// If rsMaster.Fields("descriptioncode2") <> lblCode2.Text Then
				// Call rsCodeDescription.Execute("update tbldefaultinformation set open2 = '" & lblCode2.Text & "'", "twpy0000.vb1")
				// End If
				// rsMaster.SetData "DescriptionCode1", lblCode1
				// 
				// 
				// rsMaster.SetData "DescriptionCode2", lblCode2
				rsMaster.SetData("DeptDiv", txtDeptDiv.Text);
				string strTemp;
				strTemp = txtDeptDiv.Text;
				strTemp = fecherFoundation.Strings.Trim(strTemp);
				// Dim x As Integer
				if (strTemp != string.Empty)
				{
					x = Strings.InStr(1, strTemp, "-", CompareConstants.vbTextCompare);
					if (x > 0)
					{
						strTemp = Strings.Mid(strTemp, 1, x - 1);
					}
				}
				rsMaster.Set_Fields("department", strTemp);
				rsMaster.SetData("WorkCompCode", txtWorkCompCode.Text);
				rsMaster.SetData("HrsDay", txtStdDay.Text);
				rsMaster.SetData("HrsWk", txtStdWk.Text);
				if (intChildID != 0)
					rsMaster.SetData("ChildRecord", 1);
				rsMaster.Update();
				// rsCodeDescription.Execute "Update tblEmployeeMaster Set DescriptionCode1 = '" & lblCode1 & "'", "TWPY0000.vb1"
				// rsCodeDescription.Execute "Update tblEmployeeMaster Set DescriptionCode2 = '" & lblCode2 & "'", "TWPY0000.vb1"
				// all changes have been updated
				intDataChanged = 0;
				if (NewFlag == true)
				{
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = NewCurrentEmployee;
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = txtLastName.Text;
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = txtFirstName.Text;
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = txtDesignation.Text;
					modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = txtMiddleName.Text;
					modGlobalRoutines.ShowEmployeeInformation(true);
				}
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = txtLastName.Text;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = txtFirstName.Text;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = txtMiddleName.Text;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = txtDesignation.Text;
				if (fraComments.Visible == true)
				{
					cmdSave_Click();
				}
				NewFlag = false;
				modGlobalRoutines.ShowEmployeeInformation(true);
				// Call clsHistoryClass.Compare
				MessageBox.Show("Save was successful", "Payroll Employee Master", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuSave", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "NotValidData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolInvalidData = true;
				string strAnnTemp;
				// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToDouble(
				int intMonth;
				// hold month
				int intDay;
				// hold day
				// vbPorter upgrade warning: intCheckFormat As int	OnWriteFCConvert.ToDouble(
				int intCheckFormat;
				/* Control ControlName = new Control(); */
				foreach (Control ControlName in this.GetAllControls())
				{
					if (ControlName is FCComboBox)
					{
						if (fecherFoundation.FCUtils.IsNull(ControlName) == true)
						{
							MessageBox.Show("Please Select Value From Drop-Down Menu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							NotValidData = true;
							return NotValidData;
						}
					}
				}
				strAnnTemp = FCConvert.ToString(modDateRoutines.StripDateSlashes(txtDateAnniversary));
				intCheckFormat = FCConvert.ToInt16(Conversion.Val(strAnnTemp) / 1000);
				intMonth = FCConvert.ToInt16(Conversion.Val(strAnnTemp) / 100);
				intDay = (FCUtils.iMod(Conversion.Val(strAnnTemp), 100));
				// 10000
				if (txtLastName.Text == string.Empty)
				{
					NotValidData = true;
					MessageBox.Show("Please Enter Last Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLastName.Focus();
					return NotValidData;
				}
				if (txtFirstName.Text == string.Empty)
				{
					NotValidData = true;
					MessageBox.Show("Please Enter First Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtFirstName.Focus();
					return NotValidData;
				}

                if (ssnViewPermission == SSNViewType.Full)
                {
                    if (txtSSN.Text.Length < 11)
                    {
                        NotValidData = true;
                        MessageBox.Show("Invalid Social Security Number", "Error", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        txtSSN.Text = "999-99-9999";
                        txtSSN.Focus();
                        return NotValidData;
                    }
                }

				if (txtDeptDiv.Text == string.Empty)
                {
                    // If Val(gstrDepartmentNum) <> 0 Then
                    NotValidData = true;
                    MessageBox.Show("Please Enter In Dept/Div", "Missing Dept/Div", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDeptDiv.Focus();
                    return NotValidData;
                    // End If
                }
                else
                {
                    // vbPorter upgrade warning: strTemp As object	OnWrite(string())
                    string[] strTemp = null;
                    strTemp = Strings.Split(txtDeptDiv.Text, "-", -1, CompareConstants.vbBinaryCompare);
                    if (Information.UBound(strTemp, 1) > 0)
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[0])).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) || fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[1])).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))
                        {
                            MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            txtDeptDiv.Text = string.Empty;
                            txtDeptDiv.Focus();
                            NotValidData = true;
                            return NotValidData;
                        }
                    }
                    else
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[0])).Length > intDeptLength)
                        {
                            MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            txtDeptDiv.Text = string.Empty;
                            txtDeptDiv.Focus();
                            NotValidData = true;
                            return NotValidData;
                        }
                    }
                }
				if (ValidateTextBox(ref txtFederalStatus) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtFederalStatus.Focus();
					return NotValidData;
				}
				if (ValidateTextBox(ref txtStateStatus) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtStateStatus.Focus();
					return NotValidData;
				}
				if (ValidateTextBox(ref txtSeqNumber) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtSeqNumber.Focus();
					return NotValidData;
				}
				if (ValidateTextBox(ref txtWorkCompCode) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtWorkCompCode.Focus();
					return NotValidData;
				}
				else if (fecherFoundation.Strings.Trim(txtWorkCompCode.Text).Length > 5)
				{
					intErrCount += 1;
					NotValidData = true;
					txtWorkCompCode.Focus();
					MessageBox.Show("Workers Comp Code cannot be more then 5 characters.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return NotValidData;
				}
				if (ValidateTextBox(ref txtStdWk) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtStdWk.Focus();
					return NotValidData;
				}
				if (ValidateTextBox(ref txtStdDay) == false)
				{
					intErrCount += 1;
					NotValidData = true;
					txtStdDay.Focus();
					return NotValidData;
				}

                //if (ValidateTextBox(ref txtFederalOtherDependents) == false)
                //{
                //    intErrCount += 1;
                //    txtFederalOtherDependents.Focus();
                //    return true;
                //}
				if (intErrCount == 1)
				{
					MessageBox.Show("Check Empty Field", "Error, " + FCConvert.ToString(intErrCount) + " Value Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (intErrCount > 1)
				{
					MessageBox.Show("Check Empty Fields", "Error, " + FCConvert.ToString(intErrCount) + " Values Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				if (Conversion.Val(txtFederalTaxes.Text) > 100 && cboFedDollarPercent.Text == "Percent")
				{
					NotValidData = true;
					MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtFederalTaxes.Focus();
					return NotValidData;
				}
				if (Conversion.Val(txtStateTaxes.Text) > 100 && cboStateDollarPercent.Text == "Percent")
				{
					NotValidData = true;
					MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStateTaxes.Focus();
					return NotValidData;
				}
				if (fecherFoundation.Strings.Trim(txtDateHire.Text) != string.Empty)
				{
					if (!Information.IsDate(txtDateHire.Text))
					{
						NotValidData = true;
						MessageBox.Show("Invalid Hire Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtDateHire.Focus();
						return NotValidData;
					}
				}
				if (fecherFoundation.Strings.Trim(txtDateBirth.Text) != string.Empty)
				{
					if (!Information.IsDate(txtDateBirth.Text))
					{
						NotValidData = true;
						MessageBox.Show("Invalid Birth Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtDateBirth.Focus();
						return NotValidData;
					}
				}

                if (string.IsNullOrWhiteSpace(txtW4Date.Text) || !txtW4Date.Text.IsDate() || txtW4Date.Text.ToDate() == DateTime.MinValue)
                {
                        MessageBox.Show("Invalid W4 Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtW4Date.Focus();
                        return true;
                }
                else
                {
                    
                    FederalPayStatus fedSelectedItem = (FederalPayStatus)cboFedPayStatus.SelectedItem;
                    if (fedSelectedItem != null)
                    {
                        if (fedSelectedItem.StatusType == FederalPayStatusType.HeadOfHousehold &&
                            txtW4Date.Text.ToDate().Year < 2020)
                        {
                            MessageBox.Show(
                                @"Head of Household federal filing status is invalid when the W-4 date is prior to 01/01/2020",
                                "Invalid Federal Filing Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            cboFedPayStatus.Focus();
                            return true;
                        }
                    }

                    if (txtW4Date.Text.ToDate().Year > 2020)
                    {
                        if (ValidateTextBox(ref txtFederalOtherDependents) == false)
                        {
                            intErrCount += 1;
                            txtFederalOtherDependents.Focus();
                            return true;
                        }
                    }
                }

                NotValidData = false;
				boolInvalidData = false;
				return NotValidData;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return NotValidData;
			}
		}

		public bool ValidateTextBox(ref FCTextBox TxtBox)
		{
			bool ValidateTextBox = false;
			if (TxtBox == string.Empty)
			{
				ValidateTextBox = false;
				if (TxtBox.GetName() == "txtSeqNumber")
				{
					MessageBox.Show("Please enter a sequence number", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					TxtBox.Focus();
				}
				else
				{
					MessageBox.Show("Please Enter Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				ValidateTextBox = true;
			}
			return ValidateTextBox;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = "frmEmployeeMaster";
				Close();
				// bug call id 5745
				if (gboolDontClose)
				{
				}
				else
				{
					frmEmployeeSearch.InstancePtr.Show(App.MainForm);
				}
			}
		}

		private void mnuTemporaryOverrides_Click(object sender, System.EventArgs e)
		{
			frmTempEmployeeSetup.InstancePtr.Show(App.MainForm);
		}

		private void txtAddress1_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtAddress2_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBLSWorksiteID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
			}
			else if (KeyAscii == Keys.Insert)
			{
			}
			else if (KeyAscii == Keys.Back)
			{
				// If Len(txtBLSWorksiteID) = 2 Then txtBLSWorksiteID = vbNullString
				// Exit Sub
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// Private Sub txtBLSWorksiteID_KeyUp(KeyCode As Integer, Shift As Integer)
		// If Len(txtBLSWorksiteID) = 1 Then
		// txtBLSWorksiteID = txtBLSWorksiteID & "-"
		// KeyAscii = 0
		// txtBLSWorksiteID.SelStart = 2
		// End If
		// End Sub
		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtPhone_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtEmail_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtCode1_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroupID_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroupID_Enter(object sender, System.EventArgs e)
		{
			txtGroupID.SelectionStart = 0;
			txtGroupID.SelectionLength = 10;
		}

		private void txtCode1_Enter(object sender, System.EventArgs e)
		{
			txtCode1.SelectionStart = 0;
			txtCode1.SelectionLength = 10;
		}

		private void txtCode2_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBLSWorksiteID_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBLSWorksiteID_Enter(object sender, System.EventArgs e)
		{
			txtBLSWorksiteID.SelectionStart = 0;
			txtBLSWorksiteID.SelectionLength = 10;
		}

		private void txtCode2_Enter(object sender, System.EventArgs e)
		{
			txtCode2.SelectionStart = 0;
			txtCode2.SelectionLength = 10;
		}

		private void txtDate_Change()
		{
			intDataChanged += 1;
		}

		private void txtDateAnniversary_TextChanged(object sender, System.EventArgs e)
		{
			if ((txtDateAnniversary.Text.Length == 2) && Strings.Right(txtDateAnniversary.Text, 1) != "/")
			{
				txtDateAnniversary.Text = txtDateAnniversary.Text + "/";
				txtDateAnniversary.SelectionStart = txtDateAnniversary.Text.Length;
				txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
			}
			intDataChanged += 1;
		}

		private void txtDateAnniversary_Enter(object sender, System.EventArgs e)
		{
			txtDateAnniversary.SelectionStart = 0;
			txtDateAnniversary.SelectionLength = 10;
		}

		private void txtDateAnniversary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (txtDateAnniversary.SelectedText.Length == 5)
				txtDateAnniversary.Text = string.Empty;
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
			{
				KeyAscii = (Keys)0;
			}
			if (KeyAscii == Keys.Back && Strings.Right(txtDateAnniversary.Text, 1) == "/")
			{
				txtDateAnniversary.Text = Strings.Left(txtDateAnniversary.Text, txtDateAnniversary.Text.Length - 2);
				txtDateAnniversary.SelectionStart = txtDateAnniversary.Text.Length;
				txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDateAnniversary_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			if (fecherFoundation.Strings.Trim(txtDateAnniversary.Text).Length > 5)
			{
				MessageBox.Show("Invalid Date Format" + "\r" + "MM/DD", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDateAnniversary.SelectionStart = 0;
				txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
				e.Cancel = true;
				return;
			}
			if (!Information.IsDate(txtDateAnniversary.Text))
			{
				MessageBox.Show("Invalid Date Format" + "\r" + "MM/DD", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDateAnniversary.Text = "";
				return;
			}
			strAnnTemp = Strings.Format(txtDateAnniversary.Text, "MM/dd/yyyy");
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Anniversary Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtDateAnniversary.Text = "10/10";
							txtDateAnniversary.SelectionStart = 0;
							txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Anniversary Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtDateAnniversary.Text = "10/10";
							txtDateAnniversary.SelectionStart = 0;
							txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Anniversary Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtDateAnniversary.Text = "10/10";
							txtDateAnniversary.SelectionStart = 0;
							txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
						}
						break;
					}
				default:
					{
						// not even a month
						e.Cancel = true;
						MessageBox.Show("Invalid Anniversary Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtDateAnniversary.Text = "10/10";
						txtDateAnniversary.SelectionStart = 0;
						txtDateAnniversary.SelectionLength = txtDateAnniversary.Text.Length;
						break;
					}
			}
			//end switch
		}

		

		private void txtGroupID_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			ToolTip1.SetToolTip(txtGroupID, string.Empty);
		}

       

        private void txtTerminationDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToDouble(
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToDouble(
			int intDay;
			// hold day
			int intYear;
			// hold year
			// ST
			strAnnTemp = FCConvert.ToString(modDateRoutines.StripDateSlashes(txtTerminationDate));
			intMonth = FCConvert.ToInt16(Conversion.Val(strAnnTemp) / 1000000);
			intDay = FCConvert.ToInt16((FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) / 10000.0);
			intYear = (FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) % 10000;
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Termination Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Termination Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Termination Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				default:
					{
						// not even a month
						// Cancel = True
						// Call MsgBox("Invalid Termination Date", vbOKOnly, "Error")
						return;
					}
			}
			//end switch
			if (intYear < 1800 || intYear > 2200)
			{
				e.Cancel = true;
				MessageBox.Show("Invalid Termination Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (txtTerminationDate.Text == string.Empty)
			{
				MessageBox.Show("Please Enter In Date Of Termination", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
			else if (txtTerminationDate.Text.Length < 9)
			{
				MessageBox.Show("Invalid Date Of Termination", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
		}

		private void txtRetirementDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToDouble(
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToDouble(
			int intDay;
			// hold day
			int intYear;
			// hold year
			strAnnTemp = FCConvert.ToString(modDateRoutines.StripDateSlashes(txtRetirementDate));
			intMonth = FCConvert.ToInt16(Conversion.Val(strAnnTemp) / 1000000);
			intDay = FCConvert.ToInt16((FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) / 10000.0);
			intYear = (FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) % 10000;
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Retirement Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Retirement Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							e.Cancel = true;
							MessageBox.Show("Invalid Retirement Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						break;
					}
				default:
					{
						// not even a month
						// Cancel = True
						// Call MsgBox("Invalid Termination Date", vbOKOnly, "Error")
						return;
					}
			}
			//end switch
			if (intYear < 1800 || intYear > 2200)
			{
				e.Cancel = true;
				MessageBox.Show("Invalid Retirement Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (txtRetirementDate.Text == string.Empty)
			{
				MessageBox.Show("Please Enter In Date Of Retirement", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
			else if (txtRetirementDate.Text.Length < 9)
			{
				MessageBox.Show("Invalid Date Of Retirement", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
		}

		private void txtDateBirth_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strAnnTemp = "";
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToDouble(
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToDouble(
			int intDay = 0;
			// hold day
			int intYear = 0;
			// hold year
			if (fecherFoundation.Strings.Trim(txtDateBirth.Text) != string.Empty)
			{
				strAnnTemp = FCConvert.ToString(modDateRoutines.StripDateSlashes(txtDateBirth.Text));
				intMonth = FCConvert.ToInt16(Conversion.Val(strAnnTemp) / 1000000);
				intDay = FCConvert.ToInt16((FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) / 10000.0);
				intYear = (FCUtils.iMod(Conversion.Val(strAnnTemp), 1000000)) % 10000;
				switch (intMonth)
				{
					case 2:
						{
							// feb can't be more then 29 days
							if (intDay > 29)
							{
								e.Cancel = true;
								MessageBox.Show("Invalid Birth Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							break;
						}
					case 1:
					case 3:
					case 5:
					case 7:
					case 8:
					case 10:
					case 12:
						{
							// these months no more then 31 days
							if (intDay > 31)
							{
								e.Cancel = true;
								MessageBox.Show("Invalid Birth Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							break;
						}
					case 4:
					case 6:
					case 9:
					case 11:
						{
							// these months no more then 30 days
							if (intDay > 30)
							{
								e.Cancel = true;
								MessageBox.Show("Invalid Birth Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							break;
						}
					default:
						{
							// not even a month
							e.Cancel = true;
							MessageBox.Show("Invalid Birth Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
				}
				//end switch
				if (intYear < 1800 || intYear > 2200)
				{
					e.Cancel = true;
					MessageBox.Show("Invalid Birth Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (txtDateBirth.Text == string.Empty)
				{
					MessageBox.Show("Please Enter In Date Of Birth", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				else if (txtDateBirth.Text.Length < 9)
				{
					MessageBox.Show("Invalid Date Of Birth", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtDateHire_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtDateHire.Text.Length == 2 || txtDateHire.Text.Length == 5) && Strings.Right(txtDateHire.Text, 1) != "/")
        //	{
        //		if (Strings.Left(txtDateHire.Text, 1) == "/")
        //		{
        //		}
        //		else
        //		{
        //			txtDateHire.Text = txtDateHire.Text + "/";
        //			txtDateHire.SelectionStart = txtDateHire.Text.Length;
        //			txtDateHire.SelectionLength = txtDateHire.Text.Length;
        //		}
        //	}
        //	intDataChanged += 1;
        //}

        //private void txtDateHire_Enter(object sender, System.EventArgs e)
        //{
        //	txtDateHire.SelectionStart = 0;
        //	txtDateHire.SelectionLength = 10;
        //}

        //private void txtDateHire_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if (txtDateHire.SelectedText.Length == 10)
        //		txtDateHire.Text = string.Empty;
        //	if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
        //	{
        //		KeyAscii = (Keys)0;
        //	}
        //	if (fecherFoundation.Strings.Trim(txtDateHire.Text).Length >= 10 && KeyAscii != Keys.Back)
        //	{
        //		KeyAscii = (Keys)0;
        //		return;
        //	}
        //	if (KeyAscii == Keys.Back && Strings.Right(txtDateHire.Text, 1) == "/")
        //	{
        //		if (fecherFoundation.Strings.Trim(txtDateHire.Text) == "/")
        //		{
        //			txtDateHire.Text = string.Empty;
        //			KeyAscii = (Keys)0;
        //		}
        //		else
        //		{
        //			txtDateHire.Text = Strings.Left(txtDateHire.Text, txtDateHire.Text.Length - 2);
        //			txtDateHire.SelectionStart = txtDateHire.Text.Length;
        //			txtDateHire.SelectionLength = txtDateHire.Text.Length;
        //			KeyAscii = (Keys)0;
        //		}
        //	}
        //	intDataChanged += 1;
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void txtDateHire_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtDateHire.Text))
			{
				if (fecherFoundation.Strings.Trim(txtDateAnniversary.Text) == string.Empty)
				{
					txtDateAnniversary.Text = Strings.Left(Strings.Format(txtDateHire.Text, "MM/dd/yyyy"), 5);
				}
			}
		}

		private void txtDeptDiv_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDeptDiv_DoubleClick(object sender, System.EventArgs e)
		{
			frmDeptDiv.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (modGlobalVariables.Statics.gstrDepartmentNum != string.Empty)
			{
				txtDeptDiv.Text = modGlobalVariables.Statics.gstrDepartmentNum + "-" + modGlobalVariables.Statics.gstrDivisionNum;
			}
		}

		private void txtDeptDiv_Enter(object sender, System.EventArgs e)
		{
			txtDeptDiv.SelectionStart = 0;
			// txtDeptDiv.SelLength = 10
		}

		private void txtDeptDiv_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F5)
			{
				frmDeptDiv.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.gstrDepartmentNum != string.Empty)
				{
					txtDeptDiv.Text = modGlobalVariables.Statics.gstrDepartmentNum + "-" + modGlobalVariables.Statics.gstrDivisionNum;
				}
			}
		}

		private void txtDeptDiv_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int I;
			if (txtDeptDiv.Text.Length == intDeptLength && Strings.Right(txtDeptDiv.Text, 1) == "-" && KeyCode == Keys.Delete)
			{
				// delete key was pressed
				txtDeptDiv.Text = Strings.Left(txtDeptDiv.Text, intDeptLength - 1);
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length;
				txtDeptDiv.SelectionLength = txtDeptDiv.Text.Length;
			}
			else if (txtDeptDiv.Text.Length == intDeptLength && KeyCode == Keys.Back)
			{
				// backspace key was pressed
				txtDeptDiv.Text = Strings.Left(txtDeptDiv.Text, intDeptLength - 1);
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length;
				txtDeptDiv.SelectionLength = txtDeptDiv.Text.Length;
			}
			else if (txtDeptDiv.Text.Length == intDeptLength)
			{
				if (modGlobalVariables.Statics.gboolHaveDivision)
					txtDeptDiv.Text = txtDeptDiv.Text + "-";
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length;
				txtDeptDiv.SelectionLength = txtDeptDiv.Text.Length;
			}
			else if (txtDeptDiv.Text.Length > intDeptLength + intDivLength + 1)
			{
				txtDeptDiv.Text = Strings.Left(txtDeptDiv.Text, intDeptLength + intDivLength + 1);
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length;
				txtDeptDiv.SelectionLength = txtDeptDiv.Text.Length;
			}
			else if (txtDeptDiv.Text.Length > intDeptLength && !modGlobalVariables.Statics.gboolHaveDivision)
			{
				txtDeptDiv.Text = Strings.Left(txtDeptDiv.Text, intDeptLength);
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length + 1;
				txtDeptDiv.SelectionLength = 1;
			}
			else if (Strings.Right(txtDeptDiv.Text, 1) == "-")
			{
				// txtDeptDiv.Text = Left(txtDeptDiv.Text, intDeptLength)
				// txtDeptDiv.SelStart = Len(txtDeptDiv.Text)
				// txtDeptDiv.SelLength = Len(txtDeptDiv.Text)
			}
			else if (KeyCode == Keys.Left || KeyCode == Keys.Right)
			{
			}
			else
			{
				string strTemp = "";
				strSplit = Strings.Split(fecherFoundation.Strings.Trim(txtDeptDiv.Text), "-", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strSplit, 1) > 0)
				{
					strTemp = strSplit[0] + strSplit[1];
					txtDeptDiv.Text = string.Empty;
					for (I = 1; I <= intDeptLength; I++)
					{
						if (strTemp.Length != 0)
						{
							txtDeptDiv.Text = txtDeptDiv.Text + Strings.Left(strTemp, 1);
							strTemp = Strings.Mid(strTemp, 2, strTemp.Length - 1);
						}
					}
					for (I = 1; I <= intDivLength; I++)
					{
						if (strTemp.Length != 0)
						{
							if (I == 1)
								txtDeptDiv.Text = txtDeptDiv.Text + "-";
							txtDeptDiv.Text = txtDeptDiv.Text + Strings.Left(strTemp, 1);
							strTemp = Strings.Mid(strTemp, 2, strTemp.Length - 1);
						}
					}
				}
				txtDeptDiv.SelectionStart = txtDeptDiv.Text.Length;
				txtDeptDiv.SelectionLength = txtDeptDiv.Text.Length;
			}
		}

		private void txtDesignation_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtFederalStatus_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtFederalStatus_Enter(object sender, System.EventArgs e)
		{
			txtFederalStatus.SelectionStart = 0;
			txtFederalStatus.SelectionLength = 10;
		}

		private void txtFederalTaxes_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtFederalTaxes_Enter(object sender, System.EventArgs e)
		{
			txtFederalTaxes.SelectionStart = 0;
			txtFederalTaxes.SelectionLength = 10;
		}

		private void txtFirstName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtLastName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtSeqNumber_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtSeqNumber_Enter(object sender, System.EventArgs e)
		{
			txtSeqNumber.SelectionStart = 0;
			txtSeqNumber.SelectionLength = 10;
		}

		//private void txtSSN_TextChanged(object sender, System.EventArgs e)
		//{
		//	if ((txtSSN.Text.Length == 3 || txtSSN.Text.Length == 6) && Strings.Right(txtSSN.Text, 1) != "-")
		//	{
		//		txtSSN.Text = txtSSN.Text + "-";
		//		txtSSN.SelectionStart = txtSSN.Text.Length;
		//		txtSSN.SelectionLength = txtSSN.Text.Length;
		//	}
		//	intDataChanged += 1;
		//}

		//private void txtSSN_Enter(object sender, System.EventArgs e)
		//{
		//	txtSSN.SelectionStart = 0;
		//	txtSSN.SelectionLength = 11;
		//}

		//private void txtSSN_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		//{
		//	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
		//	if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
		//	{
		//		KeyAscii = (Keys)0;
		//	}
		//	if (fecherFoundation.Strings.Trim(txtSSN.Text).Length >= 11 && KeyAscii != Keys.Back)
		//	{
		//		KeyAscii = (Keys)0;
		//		return;
		//	}
		//	if (KeyAscii == Keys.Back && Strings.Right(txtSSN.Text, 1) == "-")
		//	{
		//		txtSSN.Text = Strings.Left(txtSSN.Text, txtSSN.Text.Length - 2);
		//		txtSSN.SelectionStart = txtSSN.Text.Length;
		//		txtSSN.SelectionLength = txtSSN.Text.Length;
		//		KeyAscii = (Keys)0;
		//	}
		//	intDataChanged += 1;
		//	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		//}

		//private void txtSSN_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		//{
		//	if (txtSSN.Text.Length < 11 && fecherFoundation.Strings.Trim(txtSSN.Text) != string.Empty)
		//	{
		//		MessageBox.Show("Invalid Social Security Number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		txtSSN.Text = "999-99-9999";
		//		txtSSN.SelectionStart = 0;
		//		txtSSN.SelectionLength = txtSSN.Text.Length;
		//		e.Cancel = true;
		//	}
		//}

		private void txtStateStatus_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtStateStatus_Enter(object sender, System.EventArgs e)
		{
			txtStateStatus.SelectionStart = 0;
			txtStateStatus.SelectionLength = 10;
		}

		private void txtStateTaxes_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtStateTaxes_Enter(object sender, System.EventArgs e)
		{
			txtStateTaxes.SelectionStart = 0;
			txtStateTaxes.SelectionLength = 10;
		}

		private void txtStdDay_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtStdDay_Enter(object sender, System.EventArgs e)
		{
			txtStdDay.SelectionStart = 0;
			txtStdDay.SelectionLength = 10;
		}

		private void txtStdWk_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtStdWk_Enter(object sender, System.EventArgs e)
		{
			txtStdWk.SelectionStart = 0;
			txtStdWk.SelectionLength = 10;
		}

		private void txtWorkCompCode_TextChanged(object sender, System.EventArgs e)
		{
			// intDataChanged = intDataChanged + 1
		}

		private void txtWorkCompCode_Enter(object sender, System.EventArgs e)
		{
			// txtWorkCompCode.SelStart = 0
			// txtWorkCompCode.SelLength = 10
		}

		private void txtZip_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			txtZip.SelectionStart = 0;
			txtZip.SelectionLength = 10;
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> 8 Then
			// KeyAscii = 0
			// End If
			// 
			// If Len(Trim(txtZip)) >= 10 And KeyAscii <> 8 Then
			// KeyAscii = 0
			// Exit Sub
			// End If
			// 
			// If Len(txtZip.Text) = 5 And Right(txtZip.Text, 1) <> "-" And KeyAscii <> 8 Then
			// txtZip.Text = txtZip.Text & "-"
			// txtZip.SelStart = Len(txtZip.Text)
			// txtZip.SelLength = Len(txtZip.Text)
			// End If
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtZip_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If Len(txtZip) < 5 And Len(txtZip) <> 0 Then
			// Call MsgBox("Invalid Zip Code", vbOKOnly, "Error")
			// Cancel = True
			// End If
			// 
			// If Len(txtZip) > 5 And Mid(txtZip.Text, 6, 1) <> "-" Then
			// MsgBox "Invalid Zip Code, Check Format", vbOKOnly, "Error"
			// txtZip.SelStart = 0
			// txtZip.SelLength = Len(txtZip.Text)
			// Cancel = True
			// End If
		}

		private void txtDeptDiv_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtDeptDiv.Text == string.Empty)
			{
				MessageBox.Show("Please Enter Dept/Div", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				fraComments.Visible = false;
				e.Cancel = true;
			}
			else
			{
				// vbPorter upgrade warning: strTemp As object	OnWrite(string())
				string[] strTemp;
				strTemp = Strings.Split(txtDeptDiv.Text, "-", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strTemp, 1) > 0)
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[0])).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) || fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[1])).Length != Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))
					{
						MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptDiv.Text = string.Empty;
						txtDeptDiv.Focus();
						e.Cancel = true;
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp[0])).Length > intDeptLength)
					{
						MessageBox.Show("Invalid Dept/Div format. Please reenter Dept/Div.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptDiv.Text = string.Empty;
						txtDeptDiv.Focus();
						e.Cancel = true;
					}
				}
			}
		}

		private void txtFederalStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalStatus_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtFederalStatus.Text == string.Empty)
			{
				e.Cancel = true;
				MessageBox.Show("Please Enter Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtFederalTaxes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// 46 = . 8 = backspace
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalTaxes_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtFederalTaxes.Text) > 100 && cboFedDollarPercent.Text == "Percent")
			{
				e.Cancel = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtSeqNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSeqNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtSeqNumber.Text == string.Empty)
			{
				e.Cancel = true;
				MessageBox.Show("Please Sequence Number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtStateStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStateStatus_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtStateStatus.Text == string.Empty)
			{
				e.Cancel = true;
				MessageBox.Show("Please Enter Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtStateTaxes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// 46 = . 8 = backspace
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStateTaxes_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtStateTaxes.Text) > 100 && cboStateDollarPercent.Text == "Percent")
			{
				e.Cancel = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtStdDay_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStdDay_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtStdDay.Text == string.Empty)
			{
				MessageBox.Show("Please Enter In Hours In Standard Day", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void txtStdWk_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStdWk_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtStdWk.Text == string.Empty)
			{
				MessageBox.Show("Please Enter In Hours In Standard Week", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void txtWorkCompCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> 8 Then
			// KeyAscii = 0
			// End If
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtWorkCompCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtWorkCompCode.Text == string.Empty)
			{
				e.Cancel = true;
				MessageBox.Show("Please enter a workers comp code", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void LockMultiControls(bool boolState)
		{
			txtFirstName.Enabled = !boolState;
			txtLastName.Enabled = !boolState;
			txtDesignation.Enabled = !boolState;
			txtAddress1.Enabled = !boolState;
			txtAddress2.Enabled = !boolState;
			txtCity.Enabled = !boolState;
			txtPhone.Enabled = !boolState;
			txtEmail.Enabled = !boolState;
			cboState.Enabled = !boolState;
			txtZip.Enabled = !boolState;
			txtSSN.Enabled = !boolState;
			txtDateHire.Enabled = !boolState;
			txtDateAnniversary.Enabled = !boolState;
			txtDateBirth.Enabled = !boolState;
			txtTerminationDate.Enabled = !boolState;
			cboSex.Enabled = !boolState;
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;

			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtLastName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Last Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFirstName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "First Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMiddleName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Middle Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDesignation";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Designation";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCity";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "City";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboState";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtZip";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Zip Code";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtZip4";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "4 Digit Zip Code Extension";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAddress1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAddress2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkW2DefIncome";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W-2 Deferred Income";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkW2StatutoryEmployee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W-2 Statutory Employee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkW2Pen";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "W-2 Retirement";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkMQGE";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Medicare Qualified Government Employee";
			intTotalNumberOfControls += 1;
			//Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			//clsControlInfo[intTotalNumberOfControls].ControlName = "txtSSN";
			//clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			//clsControlInfo[intTotalNumberOfControls].DataDescription = "Social Security Number";
			//intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDateHire";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Date Hired";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDateAnniversary";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Anniversary Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTerminationDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Termination Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRetirementDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Retirement Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboSex";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Sex";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboFedPayStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Filing Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboStatePayStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State Filing Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFederalStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Number of Dependents";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStateStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State Number of Dependents";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFederalTaxes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Additional";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStateTaxes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State Additional";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboFedDollarPercent";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Taxes";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboStateDollarPercent";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State Taxes";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkFirstCheckOnly";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal AFCO";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkFirstCheckOnly";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].Index = 1;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State AFCO";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkFicaExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FICA Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkMedicare";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Medicare Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkUnemploymentExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FUTA Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkWorkersCompExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Workers Comp Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboCheck";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Check";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboPayFrequency";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Pay Frequency";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkFTOrPT";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Part Time Emp";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkPrint";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Print Pay Rate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtWorkCompCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Work Comp Code";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtBLSWorksiteID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "BLS Worksite ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStdDay";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Hours in Standard Day";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStdWk";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Hours in Standard Week";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtSeqNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Seq Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeptDiv";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Home Dept/Div";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtGroupID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Group ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCode1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Code 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCode2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Code 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtPhone";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Phone";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtEmail";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "E-mail";
			intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFederalOtherDependents";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Number of Other Dependents";
            intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFederalAdditionalDeduction";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Additional Deduction";
            intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFederalOtherIncome";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "Federal Other Income";
            intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtW4Date";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "W-4 Date";
            intTotalNumberOfControls += 1;

            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chkW4MultJobs";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "W-4 Multiple Jobs Option (c)";
            intTotalNumberOfControls += 1;
        }

        private void fcCheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
