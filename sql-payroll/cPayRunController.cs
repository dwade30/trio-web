//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cPayRunController
	{
		//=========================================================
		public cPayRun GetPayRunByID(int lngID)
		{
			cPayRun GetPayRunByID = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tblpayrollsteps where id = " + FCConvert.ToString(lngID), "Payroll");
			if (!rsLoad.EndOfFile())
			{
				cPayRun cPay = new cPayRun();
				cPay.PayDate = (DateTime)rsLoad.Get_Fields("Paydate");
				cPay.PayRunID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("payrunid"))));
				cPay.BankID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
				if (Information.IsDate(rsLoad.Get_Fields("weekenddate")))
				{
					if (Convert.ToDateTime(rsLoad.Get_Fields("weekenddate")).ToOADate() != 0)
					{
						cPay.PeriodEnd = (DateTime)rsLoad.Get_Fields("weekenddate");
					}
				}
				if (Information.IsDate(rsLoad.Get_Fields("periodstartdate")))
				{
					if (Convert.ToDateTime(rsLoad.Get_Fields("periodstartdate")).ToOADate() != 0)
					{
						cPay.PeriodStart = (DateTime)rsLoad.Get_Fields("periodstartdate");
					}
				}
				GetPayRunByID = cPay;
			}
			else
			{
				GetPayRunByID = null;
			}
			return GetPayRunByID;
		}

		public cPayRun GetPayRun(DateTime dtPayDate, int intPayrunID)
		{
			cPayRun GetPayRun = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunID), "Payroll");
			if (!rsLoad.EndOfFile())
			{
				GetPayRun = GetPayRunByID(rsLoad.Get_Fields("ID"));
			}
			else
			{
				GetPayRun = null;
			}
			return GetPayRun;
		}
	}
}
