﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;

namespace TWPY0000
{
    public class clsReportPrinterFunctions
    {
        //=========================================================
        string strPrinterFont = "";
        // name of printer font to use
        string strPrinterDeviceName = "";
        // device name of current printer
        bool boolMustResetDefault;
        bool boolCancelSelected;
        // vbPorter upgrade warning: 'Return' As string	OnWrite(bool)
        //public string CancelButtonSelected
        //{
        //    get
        //    {
        //        string CancelButtonSelected = "";
        //        CancelButtonSelected = FCConvert.ToString(boolCancelSelected);
        //        return CancelButtonSelected;
        //    }
        //}

        //public string PrinterFont
        //{
        //    get
        //    {
        //        string PrinterFont = "";
        //        PrinterFont = strPrinterFont;
        //        return PrinterFont;
        //    }
        //    set
        //    {
        //        strPrinterFont = value;
        //    }
        //}

        //public string PrinterName
        //{
        //    get
        //    {
        //        string PrinterName = "";
        //        PrinterName = strPrinterDeviceName;
        //        return PrinterName;
        //    }
        //    set
        //    {
        //        strPrinterDeviceName = value;
        //    }
        //}

        public clsReportPrinterFunctions() : base()
        {
            strPrinterFont = "";
            strPrinterDeviceName = "";
            boolMustResetDefault = false;
        }

        //public void SetPrinter(ref object obPrinter, ref string strPrintName)
        //{
        //    Printer X = new Printer();
        //    /*? For Each */
        //    foreach (FCPrinter item in FCGlobal.Printers)
        //    {
        //        if (fecherFoundation.Strings.UCase(X.Name) == fecherFoundation.Strings.UCase(strPrintName))
        //        {
        //            FCGlobal.Printer.DeviceName = X.Name;
        //            strPrinterDeviceName = FCGlobal.Printer.DeviceName;
        //        }
        //    }
        //    /*? Next */
        //}

        //public void ChoosePrinter(FCPrinter obPrinter)
        //{
        //    int intResponse;
        //    /*? On Error Resume Next  */
        //    fecherFoundation.Information.Err().Clear();
        //    // corey 09/07/2005 #76671
        //    intResponse = modPrinterDialogBox.ShowAdvancedPrinterDialog(App.MainForm.Handle.ToInt32());
        //    // 0 = cancel
        //    // 1 = printer selected
        //    // 2 = dll not registered
        //    if (intResponse == 2)
        //    {
        //        //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
        //        // MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
        //        //MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
        //        if (fecherFoundation.Information.Err().Number == 0)
        //        {
        //            boolCancelSelected = false;
        //            obPrinter.DeviceName = FCGlobal.Printer.DeviceName;
        //            strPrinterDeviceName = FCGlobal.Printer.DeviceName;
        //        }
        //        else
        //        {
        //            boolCancelSelected = true;
        //        }
        //    }
        //    else if (intResponse == 0)
        //    {
        //        // cancel
        //        boolCancelSelected = true;
        //    }
        //    else if (intResponse == 1)
        //    {
        //        boolCancelSelected = false;
        //        // corey 09/07/2005 #76671
        //        obPrinter.DeviceName = FCGlobal.Printer.DeviceName;
        //        strPrinterDeviceName = obPrinter.DeviceName;
        //        obPrinter.PaperBin = FCGlobal.Printer.PaperBin;
        //    }
        //}
        // vbPorter upgrade warning: dblWidth As double	OnReadFCConvert.ToInt32(
        // vbPorter upgrade warning: dblHeight As double	OnReadFCConvert.ToInt32(
        //public bool SetPageSize(ref FCPrinter obPrinter, ref double dblWidth, ref double dblHeight, ref string strFormName, bool boolUnitsInInches = false, bool boolShowError = false)
        //{
        //    bool SetPageSize = false;
        //    bool boolShow;
        //    // vbPorter upgrade warning: lngPaperWidth As int	OnWriteFCConvert.ToDouble(
        //    int lngPaperWidth = 0;
        //    // vbPorter upgrade warning: lngPaperHeight As int	OnWriteFCConvert.ToDouble(
        //    int lngPaperHeight = 0;
        //    int intReturn = 0;
        //    try
        //    {
        //        // On Error GoTo ErrorHandler
        //        fecherFoundation.Information.Err().Clear();
        //        SetPageSize = false;

        //        if (boolUnitsInInches)
        //        {
        //            lngPaperWidth = FCConvert.ToInt32(dblWidth * 25400);
        //            lngPaperHeight = FCConvert.ToInt32(dblHeight * 25400);
        //        }
        //        else
        //        {
        //            lngPaperWidth = FCConvert.ToInt32(dblWidth);
        //            lngPaperHeight = FCConvert.ToInt32(dblHeight);
        //        }
        //        intReturn = modCustomPageSize.SelectForm(strFormName, App.MainForm.Handle.ToInt32(), lngPaperWidth, lngPaperHeight);
        //        if (intReturn > 0)
        //        {
        //            obPrinter.PaperSize = FCConvert.ToInt16(intReturn);
        //            SetPageSize = true;
        //        }
        //        else
        //        {
        //            SetPageSize = false;
        //            if (boolShowError)
        //            {
        //                MessageBox.Show("Error. Could not access form on printer.", "Form Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //            }
        //        }
        //        return SetPageSize;
        //    }
        //    catch (Exception ex)
        //    {
        //        // ErrorHandler:
        //        if (boolShowError)
        //        {
        //            MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Setting Page Size", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //        }
        //    }
        //    return SetPageSize;
        //}

        //public string GetFont(string strPrinterName, int intCPI, string strFont = "")
        //{
        //    string GetFont = "";
        //    // searches the named printer for the needed font
        //    FCPrinter p = new FCPrinter();
        //    int X;
        //    // loop counter
        //    string strTempFont = "";
        //    string strCPIFont;
        //    GetFont = "";
        //    // must do it this way since an activereports printer object can't access the fonts
        //    /*? For Each */
        //    foreach (FCPrinter printer in FCGlobal.Printers)
        //    {
        //        if (fecherFoundation.Strings.UCase(printer.DeviceName) == fecherFoundation.Strings.UCase(strPrinterName))
        //        {
        //            break;
        //        }
        //    }
        //    /*? Next */
        //    strCPIFont = "";
        //    for (X = 0; X <= p.FontCount - 1; X++)
        //    {
        //        strTempFont = FCGlobal.Printer.Fonts[X];
        //        if (fecherFoundation.Strings.UCase(Strings.Right(strTempFont, 3)) == "CPI")
        //        {
        //            strTempFont = Strings.Mid(strTempFont, 1, strTempFont.Length - 3);
        //            if (Conversion.Val(Strings.Right(strTempFont, 2)) == intCPI || Conversion.Val(Strings.Right(strTempFont, 3)) == intCPI)
        //            {
        //                strCPIFont = FCGlobal.Printer.Fonts[X];
        //                strPrinterFont = strCPIFont;
        //                if (strFont != string.Empty)
        //                {
        //                    if (Strings.InStr(1, strCPIFont, strFont, CompareConstants.vbTextCompare) > 0)
        //                    {
        //                        // got the right one
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    // X
        //    GetFont = strCPIFont;
        //    return GetFont;
        //}
        // vbPorter upgrade warning: obReport As object	OnWrite(srptDDStub1, srptLaserCheckBody1, srptLaserStub1, rptLaserCheck1, srptAddressStub, srptLaserMailerPayStub)
        public void SetReportFontsByTag(FCSectionReport obReport, string strTag, string strFont = "", bool boolBold = false)
        {
            // accepts an activereport  and the tag value to look for
            // if you specify the font it will change the font
            // if you specify bold, it will bold it
            int X;
            int Y;
            for (Y = 0; Y <= obReport.Sections.Count - 1; Y++)
            {
                for (X = 0; X <= obReport.Sections[Y].Controls.Count - 1; X++)
                {
                    if (fecherFoundation.Strings.UCase(FCConvert.ToString(obReport.Sections[Y].Controls[X].Tag)) == fecherFoundation.Strings.UCase(strTag))
                    {
                        if (strFont != string.Empty)
                        {
                            //FC:FINAL:SBE
                            var currentFont = (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font;
                            (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new System.Drawing.Font(strFont, currentFont.Size, boolBold ? currentFont.Style & System.Drawing.FontStyle.Bold : currentFont.Style, currentFont.Unit);
                            //(obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font( strFont, (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
                            //(obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Bold = boolBold;
                        }
                    }
                }
                // X
            }
            // Y
        }

        //public void SetReportFonts(FCSectionReport obReport, int intCPI = 0, string strFont = "", string strPrinterName = "")
        //{
        //    // accepts an activereport  and the characters per inch requested
        //    // if you wish to have a specific font, you may name the font as well (not the actual font name, just roman or courier, etc)
        //    // if it is not found, a different font of the same cpi will be used
        //    // if you don't specify intCPI then you must specify strfont (as the actual font name)
        //    // in this case the controls fonts will just be set to the font passed in
        //    // if you have specific needs, you should use the SetReportFontsByTag function
        //    // it will take a specific tag argument and set the correct font and font settings
        //    string strFonttoUse = "";
        //    string strPrint = "";
        //    int X;
        //    int Y;
        //    if (intCPI > 0)
        //    {
        //        // if it has been passed in then lets get the printer font
        //        strPrint = strPrinterName;
        //        if (strPrint == string.Empty)
        //            strPrint = FCGlobal.Printer.DeviceName;
        //        if (GetFont(strPrint, intCPI, strFont) != string.Empty)
        //        {
        //            strFonttoUse = PrinterFont;
        //        }
        //        else
        //        {
        //        }
        //    }
        //    else
        //    {
        //        strFonttoUse = strFont;
        //    }
        //    if (strFonttoUse != string.Empty)
        //    {
        //        for (Y = 0; Y <= obReport.Sections.Count - 1; Y++)
        //        {
        //            // now set each box and label to the correct printer font
        //            for (X = 0; X <= obReport.Sections[Y].Controls.Count - 1; X++)
        //            {
        //                // any field I marked with textbox must have its font changed. Bold is a field that also has bold font
        //                if (FCConvert.ToString(obReport.Sections[Y].Controls[X].Tag) == "textbox")
        //                {
        //                    (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font(strFonttoUse, (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
        //                }
        //                else if (FCConvert.ToString(obReport.Sections[Y].Controls[X].Tag) == "bold")
        //                {
        //                    //FC:FINAL:SBE
        //                    var currentFont = (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font;
        //                    (obReport.Sections[Y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new System.Drawing.Font(strFont, currentFont.Size, FontStyle.Bold);
        //                    //obReport.Sections[Y].Controls[X].Font = new Font( strFonttoUse, obReport.Sections[Y].Controls[X].Font.Size);
        //                    //obReport.Sections[Y].Controls[X].Font.Bold = true;
        //                }
        //            }
        //            // X
        //        }
        //        // Y
        //    }
        //}

        //public void ReportPrint(FCSectionReport obReport, bool boolShowPrinterDialog = false)
        //{
        //    // takes a report object and will open the print range form if necessary
        //    if (obReport.Document.Pages.Count > 1 && !boolShowPrinterDialog)
        //    {
        //        int intNumberOfPages = 1;
        //        int intPageStart = 1;
        //        if (!frmNumPages.InstancePtr.Init(ref intNumberOfPages, ref intPageStart, obReport))
        //        {
        //            return;
        //        }
        //    }
        //    obReport.PrintReport(boolShowPrinterDialog);
        //}

    }
}
