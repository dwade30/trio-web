//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2AdditionalInfoSetup : BaseForm
	{
		public frmW2AdditionalInfoSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2AdditionalInfoSetup InstancePtr
		{
			get
			{
				return (frmW2AdditionalInfoSetup)Sys.GetInstance(typeof(frmW2AdditionalInfoSetup));
			}
		}

		protected frmW2AdditionalInfoSetup _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:
		//
		// NOTES:
		//
		//
		//
		// **************************************************
		// private local variables
		// Private dbLengths       As DAO.Database
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsEmployee = new clsDRWrapper();
		private int intCounter;
		private int intDataChanged;
		private bool boolLoading;
		const int ID = 0;
		const int RowNumber = 1;
		const int Code = 2;
		const int Description = 3;
		const int BOX12 = 4;
		const int BOX14 = 5;
		const int FED = 6;
		const int FICA = 7;
		const int MEDICARE = 8;
		const int State = 9;
		const int ThirdParty = 10;
		const int ADDTOW2S = 11;
		private bool pboolDataChanged;
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object SaveChanges()
		{
			object SaveChanges = null;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int lngNewID;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsBox12And14 = new clsDRWrapper();
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveChanges = false;
				vsData.Select(0, 0);
				rsData.OpenRecordset("Select * from tblW2AdditionalInfo");
				rsBox12And14.OpenRecordset("Select * from tblW2Box12And14");
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					if (rsData.FindFirstRecord("ID", vsData.TextMatrix(intCounter, ID)))
					{
						rsData.Edit();
						rsData.Set_Fields("RowNumber", vsData.TextMatrix(intCounter, RowNumber));
						rsData.Set_Fields("Code", vsData.TextMatrix(intCounter, Code));
						rsData.Set_Fields("Description", vsData.TextMatrix(intCounter, Description));
						rsData.Set_Fields("Box12", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX12)))));
						rsData.Set_Fields("Box14", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX14)))));
						rsData.Set_Fields("Federal", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, FED)))));
						rsData.Set_Fields("FICA", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, FICA)))));
						rsData.Set_Fields("Medicare", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, MEDICARE)))));
						rsData.Set_Fields("State", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, State)))));
						rsData.Set_Fields("ThirdParty", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, ThirdParty)))));
						rsData.Update();
						// 
						if (rsBox12And14.FindFirstRecord("DeductionNumber", FCConvert.ToInt32(vsData.TextMatrix(intCounter, ID)) + 9000))
						{
							if (FCConvert.CBool(Conversion.Val(vsData.TextMatrix(intCounter, ADDTOW2S))))
							{
								rsBox12And14.Edit();
								rsBox12And14.Set_Fields("Code", vsData.TextMatrix(intCounter, Code));
								rsBox12And14.Set_Fields("Box12", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX12)))));
								rsBox12And14.Set_Fields("Box14", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX14)))));
								rsBox12And14.Update();
							}
							else
							{
								rsBox12And14.Delete();
							}
						}
						else
						{
							if (FCConvert.CBool(Conversion.Val(vsData.TextMatrix(intCounter, ADDTOW2S))))
							{
								rsBox12And14.AddNew();
								rsBox12And14.Set_Fields("Code", vsData.TextMatrix(intCounter, Code));
								rsBox12And14.Set_Fields("Box12", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX12)))));
								rsBox12And14.Set_Fields("Box14", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX14)))));
								rsBox12And14.Set_Fields("DeductionNumber", FCConvert.ToInt32(vsData.TextMatrix(intCounter, ID)) + 9000);
								rsBox12And14.Update();
							}
							else
							{
							}
							// MsgBox "Error Saving tblW2Box12And14 Codes.", vbCritical + vbOKOnly, "TRIO Software"
							// Exit Function
						}
						// End If
					}
					else
					{
						rsData.OpenRecordset("Select * from tblW2AdditionalInfo where Description = '99999'");
						// If rsData.EndOfFile Then
						// If vsData.TextMatrix(intCounter, RowNumber) = 0 Then
						rsData.AddNew();
						rsData.Set_Fields("RowNumber", vsData.TextMatrix(intCounter, RowNumber));
						rsData.Set_Fields("Code", vsData.TextMatrix(intCounter, Code));
						rsData.Set_Fields("Description", vsData.TextMatrix(intCounter, Description));
						rsData.Set_Fields("Box12", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX12)))));
						rsData.Set_Fields("Box14", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, BOX14)))));
						rsData.Set_Fields("Federal", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, FED)))));
						rsData.Set_Fields("FICA", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, FICA)))));
						rsData.Set_Fields("Medicare", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, MEDICARE)))));
						rsData.Set_Fields("State", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, State)))));
						rsData.Set_Fields("ThirdParty", FCConvert.CBool(FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, ThirdParty)))));
						rsData.Update();
						SaveChanges = true;
						// Exit Function
						vsData.Rows = 1;
						ShowData();
						// End If
						// End If
						// MsgBox "Error Saving Codes.", vbCritical + vbOKOnly, "TRIO Software"
					}
				}
				pboolDataChanged = false;
				MessageBox.Show("Save completed Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveChanges = true;
				return SaveChanges;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return SaveChanges;
			}
		}

		private void frmW2AdditionalInfoSetup_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2AdditionalInfoSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			if (KeyAscii == Keys.Escape)
			{
				mnuExit_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmW2AdditionalInfoSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2AdditionalInfoSetup properties;
			//frmW2AdditionalInfoSetup.ScaleWidth	= 11145;
			//frmW2AdditionalInfoSetup.ScaleHeight	= 6645;
			//frmW2AdditionalInfoSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				pboolDataChanged = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				SetGridProperties();
				// open the forms global database connection
				// Set dbLengths = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsData.DefaultDB = "TWPY0000.vb1";
				// Get the data from the database
				boolLoading = true;
				ShowData();
				boolLoading = false;
				intDataChanged = 0;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsW2Box12And14 = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblW2AdditionalInfo");
				rsW2Box12And14.OpenRecordset("Select * from tblW2Box12And14");
				if (!rsData.EndOfFile())
				{
					rsData.MoveLast();
					rsData.MoveFirst();
				}
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					vsData.Rows += 1;
					vsData.TextMatrix(intCounter, ID, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.TextMatrix(intCounter, RowNumber, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.TextMatrix(intCounter, Code, FCConvert.ToString(rsData.Get_Fields("Code")));
					vsData.TextMatrix(intCounter, Description, FCConvert.ToString(rsData.Get_Fields("Description")));
					vsData.TextMatrix(intCounter, BOX12, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Box12")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, BOX14, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Box14")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, FED, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields("Federal")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, FICA, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields("FICA")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, MEDICARE, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Medicare")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, State, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields("State")) ? 1 : 0)));
					vsData.TextMatrix(intCounter, ThirdParty, FCConvert.ToString((FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ThirdParty")) ? 1 : 0)));
					if (rsW2Box12And14.FindFirstRecord("DeductionNumber", rsData.Get_Fields("ID") + 9000))
					{
						vsData.TextMatrix(intCounter, ADDTOW2S, FCConvert.ToString(1));
					}
					else
					{
						vsData.TextMatrix(intCounter, ADDTOW2S, FCConvert.ToString(0));
					}
					rsData.MoveNext();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2AdditionalInfoSetup_Resize(object sender, System.EventArgs e)
		{
			vsData.Cols = 12;
			vsData.ColWidth(0, vsData.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.05));
			vsData.ColWidth(Code, FCConvert.ToInt32(vsData.WidthOriginal * 0.07));
			vsData.ColWidth(Description, FCConvert.ToInt32(vsData.WidthOriginal * 0.28));
			vsData.ColWidth(BOX12, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(BOX14, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(FED, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(FICA, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(MEDICARE, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(State, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(ThirdParty, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			// vsData.ColWidth(addtow2s) = vsData.Width * 0.08
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intReturn As int	OnWrite(DialogResult)
				DialogResult intReturn = 0;
				vsData.Select(0, 0);
				if (pboolDataChanged)
				{
					intReturn = MessageBox.Show("Save changes to current screen?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intReturn == DialogResult.Yes)
					{
						if (!FCConvert.ToBoolean(SaveChanges()))
						{
							e.Cancel = true;
							return;
						}
					}
					else if (intReturn == DialogResult.No)
					{
					}
					else
					{
						e.Cancel = true;
						return;
					}
					//MDIParent.InstancePtr.Show();
					// set focus back to the menu options of the MDIParent
					// CallByName MDIParent, "Grid_GotFocus", VbMethod
				}
				else
				{
					//MDIParent.InstancePtr.Show();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// MATTHEW 11/29/2005 CALL ID 82618
			clsDRWrapper rsData = new clsDRWrapper();
			if (vsData.Row > 0)
			{
				rsData.OpenRecordset("Select * from tblW2AdditionalInfoData where AdditionalDeductionID = 900" + this.vsData.TextMatrix(vsData.Row, 0));
				if (rsData.EndOfFile())
				{
					// ASK IF YOU WAN
					if (MessageBox.Show("Are you sure you wish to delete this item: " + vsData.TextMatrix(vsData.Row, 3), null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("PY", "Delete of Additional W2 Info", "Code: " + vsData.TextMatrix(vsData.Row, 3));
						rsData.Execute("Delete from tblW2AdditionalInfo where ID = " + vsData.TextMatrix(vsData.Row, 0), "Payroll");
						vsData.RemoveItem(vsData.Row);
					}
				}
				else
				{
					MessageBox.Show("This item is being used by employee " + rsData.Get_Fields("EmployeeNumber") + ". This entry must be cleared before a delete can be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else
			{
				MessageBox.Show("A valid row must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			int lngNewID;
			// Dim rsData As New clsDRWrapper
			// Call rsData.OpenRecordset("Select * from tblW2AdditionalInfo where Description = '99999'")
			// If rsData.EndOfFile Then
			// rsData.AddNew
			// rsData.Update
			// End If
			// 
			// Call rsData.OpenRecordset("Select Max(ID) as MaxID from tblW2AdditionalInfo")
			// lngNewID = rsData.Fields("MaxID")
			// Call rsData.OpenRecordset("Select * from tblW2Box12And14")
			// rsData.AddNew
			// rsData.Fields("DeductionNumber") = lngNewID + 9000
			// rsData.Update
			vsData.Rows += 1;
			vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(0));
			vsData.TextMatrix(vsData.Rows - 1, RowNumber, FCConvert.ToString(0));
			vsData.TextMatrix(vsData.Rows - 1, ADDTOW2S, FCConvert.ToString(1));
			vsData.Select(vsData.Rows - 1, 2);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveChanges();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(SaveChanges()))
			{
				mnuExit_Click();
			}
		}

		private void SetGridProperties()
		{
			vsData.Cols = 12;
			vsData.Rows = 1;
			vsData.FixedCols = 2;
			vsData.ColHidden(0, true);
			vsData.ColHidden(1, true);
			vsData.FixedRows = 1;
			vsData.ExtendLastCol = true;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.ColAlignment(RowNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.TextMatrix(0, ID, "ID");
			vsData.TextMatrix(0, RowNumber, "#");
			vsData.TextMatrix(0, Code, "Code");
			vsData.TextMatrix(0, Description, "Description");
			vsData.TextMatrix(0, BOX12, "Box 12");
			vsData.TextMatrix(0, BOX14, "Box 14");
			vsData.TextMatrix(0, FED, "Federal");
			vsData.TextMatrix(0, FICA, "FICA");
			vsData.TextMatrix(0, MEDICARE, "Medicare");
			vsData.TextMatrix(0, State, "State");
			vsData.TextMatrix(0, ThirdParty, "3rd Party");
			vsData.TextMatrix(0, ADDTOW2S, "W-2s");
			vsData.ColDataType(BOX12, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(BOX14, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(FED, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(FICA, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(MEDICARE, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(State, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(ThirdParty, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.ColDataType(ADDTOW2S, FCGrid.DataTypeSettings.flexDTBoolean);
			// .ColWidth(RowNumber) = 500
			// .ColWidth(Description) = 2300
			// .ColWidth(BOX12) = 800
			// .ColWidth(BOX14) = 800
			// .ColWidth(FED) = 800
			// .ColWidth(FICA) = 800
			// .ColWidth(MEDICARE) = 800
			// .ColWidth(State) = 800
			// .ColWidth(ThirdParty) = 800
			// .ColWidth(ADDTOW2S) = 800
		}

		private void vsData_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsData.Col == BOX12)
			{
				if (FCConvert.CBool(vsData.TextMatrix(vsData.Row, BOX12)))
				{
					vsData.TextMatrix(vsData.Row, BOX14, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Row, BOX14, FCConvert.ToString(1));
				}
			}
			else if (vsData.Col == BOX14)
			{
				if (FCConvert.CBool(vsData.TextMatrix(vsData.Row, BOX14)))
				{
					vsData.TextMatrix(vsData.Row, BOX12, FCConvert.ToString(0));
				}
				else
				{
					vsData.TextMatrix(vsData.Row, BOX12, FCConvert.ToString(1));
				}
			}
			if (vsData.Col == 10)
			{
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					if (intCounter != vsData.Row)
					{
						vsData.TextMatrix(intCounter, 10, FCConvert.ToString(false));
					}
				}
			}
			if (!boolLoading)
				pboolDataChanged = true;
		}
	}
}
