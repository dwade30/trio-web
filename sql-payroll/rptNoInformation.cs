﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNoInformation.
	/// </summary>
	public partial class rptNoInformation : BaseSectionReport
	{
		public rptNoInformation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "No Information Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNoInformation InstancePtr
		{
			get
			{
				return (rptNoInformation)Sys.GetInstance(typeof(rptNoInformation));
			}
		}

		protected rptNoInformation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNoInformation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;

		public void Init(string strTitle)
		{
			// vbPorter upgrade warning: Label1 As object	OnWrite(string)
			Label1.Text = strTitle;
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDuplexPrinting.DuplexPrintReport(rptNoInformation.InstancePtr);
			}
			else
			{
				modDuplexPrinting.DuplexPrintReport(rptNoInformation.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName);
			}
			// rptNoInformation.PrintReport False
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblPayDate.Text = "Pay Date: " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			blnFirstRecord = false;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
