﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptClearEIC.
	/// </summary>
	partial class rptClearEIC
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptClearEIC));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCaption = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNumber,
				this.txtEmployeeName
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.lblCaption
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size" + ": 11pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL -  Cleared EIC Settings";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblCaption
			// 
			this.lblCaption.Height = 0.2083333F;
			this.lblCaption.HyperLink = null;
			this.lblCaption.Left = 1.4375F;
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Style = "font-family: \'Tahoma\'; font-size" + ": 11pt; font-weight: bold; text-align: center";
			this.lblCaption.Text = null;
			this.lblCaption.Top = 0.1666667F;
			this.lblCaption.Width = 3.625F;
			// 
			// txtEmployeeNumber
			// 
			this.txtEmployeeNumber.Height = 0.1666667F;
			this.txtEmployeeNumber.Left = 0.3125F;
			this.txtEmployeeNumber.Name = "txtEmployeeNumber";
			this.txtEmployeeNumber.Text = null;
			this.txtEmployeeNumber.Top = 0F;
			this.txtEmployeeNumber.Width = 0.84375F;
			// 
			// txtEmployeeName
			// 
			this.txtEmployeeName.Height = 0.1666667F;
			this.txtEmployeeName.Left = 1.25F;
			this.txtEmployeeName.Name = "txtEmployeeName";
			this.txtEmployeeName.Text = null;
			this.txtEmployeeName.Top = 0F;
			this.txtEmployeeName.Width = 2F;
			// 
			// rptClearEIC
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCaption;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
