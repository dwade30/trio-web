//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPayCategories.
	/// </summary>
	partial class frmPayCategories
	{
		public fecherFoundation.FCGrid vsPayCategories;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdSave;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP5;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.vsPayCategories = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 545);
            this.BottomPanel.Size = new System.Drawing.Size(1013, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsPayCategories);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Size = new System.Drawing.Size(1033, 558);
            this.ClientArea.Controls.SetChildIndex(this.cmdRefresh, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsPayCategories, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(1033, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(218, 28);
            this.HeaderText.Text = "Pay Category Setup";
            // 
            // vsPayCategories
            // 
            this.vsPayCategories.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayCategories.Cols = 9;
            this.vsPayCategories.ColumnHeadersHeight = 90;
            this.vsPayCategories.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.vsPayCategories.Location = new System.Drawing.Point(30, 30);
            this.vsPayCategories.Name = "vsPayCategories";
            this.vsPayCategories.Rows = 50;
            this.vsPayCategories.Size = new System.Drawing.Size(975, 515);
            this.vsPayCategories.StandardTab = false;
            this.vsPayCategories.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsPayCategories.TabIndex = 6;
            this.vsPayCategories.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsPayCategories_KeyDownEdit);
            this.vsPayCategories.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsPayCategories_KeyPressEdit);
            this.vsPayCategories.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsPayCategories_AfterEdit);
            this.vsPayCategories.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsPayCategories_CellFormatting);
            this.vsPayCategories.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.VsPayCategories_EditingControlShowing);
            this.vsPayCategories.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsPayCategories_ValidateEdit);
            this.vsPayCategories.CurrentCellChanged += new System.EventHandler(this.vsPayCategories_RowColChange);
            this.vsPayCategories.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayCategories_KeyDownEvent);
            this.vsPayCategories.KeyUp += new Wisej.Web.KeyEventHandler(this.vsPayCategories_KeyUpEvent);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Location = new System.Drawing.Point(944, 387);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(40, 24);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Location = new System.Drawing.Point(866, 387);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
            this.cmdRefresh.TabIndex = 3;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(895, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(110, 24);
            this.cmdNew.TabIndex = 2;
            this.cmdNew.Text = "New Category";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(764, 387);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 1;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(452, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP1,
            this.mnuRefresh,
            this.mnuSP2,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP3,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP5,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Category";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Category";
            this.mnuDelete.Visible = false;
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = 3;
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Visible = false;
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 4;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            this.mnuSP2.Visible = false;
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 5;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Visible = false;
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = 6;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Visible = false;
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 7;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 8;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                       ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 9;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                      ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP5
            // 
            this.mnuSP5.Index = 10;
            this.mnuSP5.Name = "mnuSP5";
            this.mnuSP5.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 11;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmPayCategories
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1033, 618);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPayCategories";
            this.Text = "Pay Category Setup";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPayCategories_Load);
            this.Activated += new System.EventHandler(this.frmPayCategories_Activated);
            this.Resize += new System.EventHandler(this.frmPayCategories_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPayCategories_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPayCategories_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
	}
}