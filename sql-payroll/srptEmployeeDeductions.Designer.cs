﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeDeductions.
	/// </summary>
	partial class srptEmployeeDeductions
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptEmployeeDeductions));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountDP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFreqTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLimit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountDP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFreqTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDedCode,
				this.txtAmount,
				this.txtAmountDP,
				this.txtFreqTax,
				this.txtLimit,
				this.txtCurrent,
				this.txtMTD,
				this.txtFiscal,
				this.txtCalendar,
				this.txtQTD,
				this.txtLTD,
				this.txtPer
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape3,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33
			});
			this.ReportHeader.Height = 0.5F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Shape3
			// 
			this.Shape3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape3.Height = 0.3125F;
			this.Shape3.Left = 0F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.15625F;
			this.Shape3.Width = 7.125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.21875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label21.Text = "Deductions";
			this.Label21.Top = 0.15625F;
			this.Label21.Width = 1.625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.0625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label22.Text = "Name";
			this.Label22.Top = 0.3125F;
			this.Label22.Width = 0.5F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label23.Text = "Amount";
			this.Label23.Top = 0.3125F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.5F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label24.Text = "Typ";
			this.Label24.Top = 0.3125F;
			this.Label24.Width = 0.25F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.21875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3.375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label25.Text = "Current";
			this.Label25.Top = 0.3125F;
			this.Label25.Width = 0.5F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.21875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.9375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label26.Text = "MTD";
			this.Label26.Top = 0.3125F;
			this.Label26.Width = 0.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.21875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.875F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label27.Text = "Calendar";
			this.Label27.Top = 0.3125F;
			this.Label27.Width = 0.5625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.21875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.25F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label28.Text = "Fiscal";
			this.Label28.Top = 0.3125F;
			this.Label28.Width = 0.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.21875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.625F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label29.Text = "QTD";
			this.Label29.Top = 0.3125F;
			this.Label29.Width = 0.5F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.21875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 6.75F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label30.Text = "LTD";
			this.Label30.Top = 0.3125F;
			this.Label30.Width = 0.375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.21875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 2.625F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label31.Text = "Limit";
			this.Label31.Top = 0.3125F;
			this.Label31.Width = 0.375F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.21875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 1.8125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label32.Text = "FRQ/TX";
			this.Label32.Top = 0.3125F;
			this.Label32.Width = 0.5F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.0625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label33.Text = "Per";
			this.Label33.Top = 0.3125F;
			this.Label33.Width = 0.3125F;
			// 
			// txtDedCode
			// 
			this.txtDedCode.Height = 0.1875F;
			this.txtDedCode.Left = 0.0625F;
			this.txtDedCode.Name = "txtDedCode";
			this.txtDedCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDedCode.Text = null;
			this.txtDedCode.Top = 0F;
			this.txtDedCode.Width = 0.8125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 0.875F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat");
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.5625F;
			// 
			// txtAmountDP
			// 
			this.txtAmountDP.Height = 0.1875F;
			this.txtAmountDP.Left = 1.5F;
			this.txtAmountDP.Name = "txtAmountDP";
			this.txtAmountDP.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmountDP.Text = null;
			this.txtAmountDP.Top = 0F;
			this.txtAmountDP.Width = 0.1875F;
			// 
			// txtFreqTax
			// 
			this.txtFreqTax.Height = 0.1875F;
			this.txtFreqTax.Left = 1.8125F;
			this.txtFreqTax.Name = "txtFreqTax";
			this.txtFreqTax.OutputFormat = resources.GetString("txtFreqTax.OutputFormat");
			this.txtFreqTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFreqTax.Text = null;
			this.txtFreqTax.Top = 0F;
			this.txtFreqTax.Width = 0.375F;
			// 
			// txtLimit
			// 
			this.txtLimit.Height = 0.1875F;
			this.txtLimit.Left = 2.3125F;
			this.txtLimit.Name = "txtLimit";
			this.txtLimit.OutputFormat = resources.GetString("txtLimit.OutputFormat");
			this.txtLimit.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLimit.Text = null;
			this.txtLimit.Top = 0F;
			this.txtLimit.Width = 0.6875F;
			// 
			// txtCurrent
			// 
			this.txtCurrent.Height = 0.1875F;
			this.txtCurrent.Left = 3.4375F;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.OutputFormat = resources.GetString("txtCurrent.OutputFormat");
			this.txtCurrent.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtCurrent.Text = null;
			this.txtCurrent.Top = 0F;
			this.txtCurrent.Width = 0.4375F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.1875F;
			this.txtMTD.Left = 3.875F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.OutputFormat = resources.GetString("txtMTD.OutputFormat");
			this.txtMTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMTD.Text = null;
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.625F;
			// 
			// txtFiscal
			// 
			this.txtFiscal.Height = 0.1875F;
			this.txtFiscal.Left = 5.1875F;
			this.txtFiscal.Name = "txtFiscal";
			this.txtFiscal.OutputFormat = resources.GetString("txtFiscal.OutputFormat");
			this.txtFiscal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFiscal.Text = null;
			this.txtFiscal.Top = 0F;
			this.txtFiscal.Width = 0.625F;
			// 
			// txtCalendar
			// 
			this.txtCalendar.Height = 0.1875F;
			this.txtCalendar.Left = 5.8125F;
			this.txtCalendar.Name = "txtCalendar";
			this.txtCalendar.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtCalendar.Text = null;
			this.txtCalendar.Top = 0F;
			this.txtCalendar.Width = 0.625F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.1875F;
			this.txtQTD.Left = 4.5F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtQTD.Text = null;
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 0.625F;
			// 
			// txtLTD
			// 
			this.txtLTD.Height = 0.1875F;
			this.txtLTD.Left = 6.4375F;
			this.txtLTD.Name = "txtLTD";
			this.txtLTD.OutputFormat = resources.GetString("txtLTD.OutputFormat");
			this.txtLTD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLTD.Text = null;
			this.txtLTD.Top = 0F;
			this.txtLTD.Width = 0.6875F;
			// 
			// txtPer
			// 
			this.txtPer.Height = 0.1875F;
			this.txtPer.Left = 3.0625F;
			this.txtPer.Name = "txtPer";
			this.txtPer.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPer.Text = null;
			this.txtPer.Top = 0F;
			this.txtPer.Width = 0.3125F;
			// 
			// srptEmployeeDeductions
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountDP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFreqTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountDP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFreqTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLimit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPer;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
