﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsHistory
	{
		//=========================================================
		// ***************************************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 27,2003
		//
		// MODIFIED BY:
		// NOTES: clsHISTORY
		//
		// DESCRIPTION:
		// This class will allow for an audit history of every changed field on a form. This will show down the load and save of each form, especially if there is a grid with a bunch of rows.
		//
		// LIMITATIONS:
		// This class assumes that there are fewer then 10 grids on a form.
		//
		// DECLARATION:
		// Private clsHistoryClass As New clsHistory
		//
		// SAVE ROUTINE:
		// Add this call to your save routine. This will compare the original values to the values that are currently in each control and if there is a difference then it will record the change in the table AuditHistory.
		// intColumnNumber is the row that will hold the row in the grid with the captions.
		// Call clsHistoryClass.Compare(intColumnNumber)
		//
		// FORM LOAD:
		// Add this call to your Form Load routine. This tells the class what module you are, a grid name to use and the ID column that is the ID for a grid so that it know what the identity field is. There can be multiple calls to this function if there are multiple grids.
		// Call clsHistoryClass.SetGridIDColumn("PY", "vsPayCodes", 4)
		//
		// DELETE ROUTINE:
		// Add this call to your delete function if you have a grid on the form. This will do nothing more then add an entry to the Audit History table as the class does not track deleted rows in a grid.
		// Send the actual object of the grid
		// Call clsHistoryClass.AddAuditHistoryEntry(vsPayCodes)
		// ***************************************************************************************************
		private struct ArrayElement
		{
			public string Name;
			public string OriginalValue;
			public string NewValue;
			public int GridRowID;
			public bool Changed;
		};

		private struct GridIDs
		{
			public string Name;
			public int GridRowID;
		};

		private ArrayElement[] aryControls = null;
		private GridIDs[] aryGridIDFields = new GridIDs[100 + 1];
		// vbPorter upgrade warning: pForm As Form	OnWrite(Form)
		private FCForm pForm = null;
		/* private Control ControlName = new Control(); */
private bool pboolUseRoutine;
		private int intCounter;
		private int intRow;
		private int intCol;
		private int intGridIDColumn;
		private string strModule = "";

		public FCForm Init
		{
			set
			{
				// THIS ROUTINE WILL LOAD AN ARRAY WITH THE NAME OF EACH CONTROL AND
				// THE INITIAL VALUE. THIS WILL BE USED TO COMPARE THE VALUE WITH
				// DURING A SAVE TO SEE IF A CHANGE HAS BEEN MADE.
				// SET THE FORM INTO A VARIABLE
				pForm = value;
				intCounter = 0;
				aryControls = new ArrayElement[0 + 1];
				foreach (Control ControlName in FCUtils.GetAllControls(pForm))
				{
					//FC:FINAL:AM:#2578 - performance
                    //if (ControlName is FCButton)
					//{
					//}
					//else if (ControlName is FCLabel)
					//{
					//	// IF THIS CONTROL IS A LABEL THEN SAVE IT'S CAPTION.
					//	// DON'T THINK WE HAVE MANY OF THESE SO WE MAY WANT TO GET
					//	// RID OF THIS OPTION TO SPEED THINGS UP.
					//	// PY HAS TWO LABELS THAT WE CAN CHANGE THE CAPTION ON THE FLY
					//	// THESE SHOULD BE ON THE EMPLOYEE INFORMATION SCREEN.
					//	// ReDim Preserve aryControls(UBound(aryControls) + 1)
					//	// aryControls(intCounter).Name = ControlName.Name
					//	// aryControls(intCounter).OriginalValue = ControlName.Text
					//	// intCounter = intCounter + 1
					//}
					//else if (ControlName is FCFrame)
					//{
					//}
					//else if (ControlName is FCToolStripMenuItem)
					//{
					//}
					//else if (ControlName is FCLine)
					//{
					//}
					//else if (ControlName is FCCheckBox)
					//{
					//}
					//else if (ControlName is ImageList)
					//{
					//}
					//else if (ControlName is FCPictureBox)
					//{
					//}
					//else if (ControlName is FCDriveListBox)
					//{
					//}
					//else if (ControlName is FCDirListBox)
					//{
					//}
					//else if (ControlName is FCFileListBox)
					//{
					//}
					//else if (ControlName is FCPictureBox)
					//{
					//	// ElseIf TypeOf ControlName Is ComboBox Then
					//}
					//else if (ControlName is FCListBox)
					//{
					//}
					//else if (ControlName is FCRadioButton)
					//{
					//}
					//else if (ControlName is FCTabControl)
					//{
					//}
					//else if (ControlName is FCDriveListBox)
					//{
					//}
					//else if (ControlName is FCFileListBox)
					//{
					//}
					//else if (ControlName is FCDirListBox)
					//{
					//}
					//else if (ControlName is FCGrid || modHistory.IsVersion7FlexGrid(ControlName))
                    if(ControlName is FCGrid)
					{
						// GET THE GRID COLUMN THAT IS THE ID COLUMN
						// NEED TO SAVE THIS INSTEAD OF THE GRID ROW BECAUSE
						// IF WE WERE TO SAVE THE GRID ROW AND THEN THE USER DELETES A ROW
						// THEN THERE WOULD BE NO WAY TO FIND THE EXACT ROW/COLUMN COMBINATION
						// IN THE ARRAY AND WE WOULD LOSE THAT DATA.
						intGridIDColumn = GetGridIDColumn(ControlName.GetName());
                        FCGrid grid = ControlName as FCGrid;
						for (intRow = 0; intRow <= grid.Rows - 1; intRow++)
						{
							for (intCol = 0; intCol <= grid.Cols - 1; intCol++)
							{
								// IF THE COLUMN ISN'T THE ID COLUMN THEN SAVE THE CELLS DATA
								// If intCol <> intGridIDColumn And Val(ControlName.TextMatrix(intRow, intGridIDColumn)) <> 0 Then
								if (!grid.ColHidden(intCol))
								{
									Array.Resize(ref aryControls, Information.UBound(aryControls, 1) + 1 + 1);
									aryControls[intCounter].GridRowID = FCConvert.ToInt32(Conversion.Val(grid.TextMatrix(intRow, intGridIDColumn)));
									aryControls[intCounter].Name = ControlName.GetName() + " - Row ID: " + grid.TextMatrix(intRow, intGridIDColumn) + " - Column Number: " + FCConvert.ToString(intCol);
									aryControls[intCounter].OriginalValue = grid.TextMatrix(intRow, intCol);
									intCounter += 1;
								}
							}
						}
					}
					else if(ControlName as TextBoxBase != null)
					{
						// THIS SHOULD CATCH EVERY OTHER CONTROL THAT HAS A TEXT PROPERTY
						Array.Resize(ref aryControls, Information.UBound(aryControls, 1) + 1 + 1);
						aryControls[intCounter].Name = ControlName.GetName();
						aryControls[intCounter].OriginalValue = ControlName.Text;
						intCounter += 1;
					}
				}
			}
		}

		public void Compare(string strExempt = "", int intCaptionRow = 0)
		{
			// THIS ROUTINE IS USED TO COMPARE THE INITAL VALUE OF THE CONTROL OR GRID CELL
			// WITH ITS CURRENT VALUE TO SEE IF A CHANGE HAS BEEN MADE.
			string[] strAry = null;
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			bool boolSkip = false;
			intCounter = 0;
			foreach (Control ControlName in FCUtils.GetAllControls(pForm))
			{
                
                if (ControlName is FCGrid)
                {
					// COMPARE THE CONTROL AND LOOK FOR A CHANGE
					// GET THE AUTO ID COLUMN FOR THIS GRID
					intGridIDColumn = GetGridIDColumn(ControlName.GetName());
					strAry = Strings.Split(strExempt, ",", -1, CompareConstants.vbTextCompare);
                    FCGrid grid = ControlName as FCGrid;
                    // CYCLE THRU THE GRIDS ROWS AND COLUMNS AND COMPARE EACH CELL
                    for (intRow = 0; intRow <= grid.Rows - 1; intRow++)
					{
						if (intRow <= grid.FixedRows - 1)
						{
						}
						else
						{
							for (intCol = 0; intCol <= grid.Cols - 1; intCol++)
							{
								boolSkip = false;
								for (X = 0; X <= (Information.UBound(strAry, 1)); X++)
								{
									if (Conversion.Val(strAry[X]) == intCol)
									{
										boolSkip = true;
									}
								}
								// X
								// If InStr(1, strExempt, intCol) Then
								if (boolSkip)
								{
									// MsgBox "A"
								}
								else
								{
									if (intCol != intGridIDColumn)
									{
										// COMPARE THE CONTROL AND LOOK FOR A CHANGE
										// NAME IS "NAME--VALUE IN (GRIDROWID)--COLUMN"
										if (fecherFoundation.Strings.Trim(grid.TextMatrix(intRow, intGridIDColumn)) == string.Empty || grid.TextMatrix(intRow, intGridIDColumn) == "0")
										{
											// THIS IS A NEW ROW
											FindElement(ControlName, ControlName.GetName() + " - New Row in Grid", grid.TextMatrix(intRow, intCol), true, intCol, intCaptionRow);
										}
										else
										{
											FindElement(ControlName, ControlName.GetName() + " - Row ID: " + grid.TextMatrix(intRow, intGridIDColumn) + " - Column Number: " + FCConvert.ToString(intCol), grid.TextMatrix(intRow, intCol), true, intCol, intCaptionRow);
										}
									}
								}
							}
						}
					}
				}
				else if(ControlName as TextBoxBase != null)
				{
					// COMPARE THE CONTROL AND LOOK FOR A CHANGE
					if (ControlName.Enabled)
					{
						FindElement(ControlName, ControlName.GetName(), ControlName.Text, false, intCaptionRow);
					}
				}
			}
		}

		private void FindElement(Control Control, string NameofControl, string ValueOfControl, bool boolGridItem = false, int intColumnNumber = 0, int intCaptionRow = 0)
		{
			// THIS FINDS THE CURRENT CONTROL AND COMPARES THE OLD VALUE WITH THE NEW
			// OR CURRENT ONE AND IF A DIFFERENCE IS FOUND THEN AN ENTRY IS MADE INTO
			// THE TABLE AUDIT HISTORY
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			for (intCount = 0; intCount <= (Information.UBound(aryControls, 1) - 1); intCount++)
			{
				if (aryControls[intCount].Name == NameofControl)
				{
					// THE CONTROL HAS BEEN FOUND IN THE ARRAY SO SAVE THE NEW VALUE INTO THE ARRAY
					aryControls[intCount].NewValue = ValueOfControl;
					if (aryControls[intCount].NewValue != aryControls[intCount].OriginalValue)
					{
						// THERE IS A DIFFERENCE SO NOW FIND OUT IF THE CONTROL IS A GRID
						if (boolGridItem)
						{
							// SAVE A GRID ITEM INTO THE AUDIT HISTORY TABLE
							// Call AddAuditHistoryEntry(strModule, pForm.Name & " - " & aryControls(intCount).Name, "Edit Record", "Original Value: " & aryControls(intCount).OriginalValue, "New Value: " & aryControls(intCount).NewValue, "Record ID: " & Val(ControlName.TextMatrix(intRow, intGridIDColumn)))
							AddAuditHistoryEntry(strModule, pForm.Name + " - " + aryControls[intCount].Name, "Edit Record", "Original Value: " + aryControls[intCount].OriginalValue, "New Value: " + aryControls[intCount].NewValue);
						}
						else
						{
							// SAVE A REGULAR CONTROL ITEM INTO THE AUDIT HISTORY TABLE
							AddAuditHistoryEntry(strModule, pForm.Name + " - " + aryControls[intCount].Name, "Edit Record", "Original Value: " + aryControls[intCount].OriginalValue, "New Value: " + aryControls[intCount].NewValue);
						}
						// THE ORIGINAL VALUE IS NOW THE NEW SAVED VALUE
						aryControls[intCount].OriginalValue = aryControls[intCount].NewValue;
					}
					return;
				}
			}
			if (boolGridItem)
			{
				// THIS CONTROL CANNOT BE FOUND IN THE ARRAY SO THIS MUST BE A NEW RECORD IN A GRID
				// If Val(ControlName.TextMatrix(intRow, intGridIDColumn)) <> 0 Then
				// ADD A NEW RECORD ITEM INTO THE AUDIT HISTORY TABLE
				// Call AddAuditHistoryEntry(strModule, NameOfControl, "New Record", "Value=" & ValueOfControl, "", "ID Column = " & Val(ControlName.TextMatrix(intRow, intGridIDColumn)))
				// MATTHEW 5/11/04 CHANGED TO MAKE THIS A BIT MORE READABLE
				if ((Control as FCGrid).ColHidden(intColumnNumber))
				{
				}
				else
				{
					if ((Control as FCGrid).Rows < 1 || (Control as FCGrid).TextMatrix(intCaptionRow, intColumnNumber) == string.Empty)
					{
						AddAuditHistoryEntry(strModule, pForm.Name + " - " + NameofControl, "New Record", "Value=" + ValueOfControl);
						// Call AddAuditHistoryEntry(strModule, pForm.Name & " - " & NameOfControl, "New Record", "Value=" & ValueOfControl, "", "Record ID: " & Val(ControlName.TextMatrix(intRow, intGridIDColumn)))
					}
					else
					{
						// Call AddAuditHistoryEntry(strModule, pForm.Name & " - " & NameOfControl, "New Record", ControlName.TextMatrix(0, intColumnNumber) & ": " & ValueOfControl, "", "Record ID: " & Val(ControlName.TextMatrix(intRow, intGridIDColumn)))
						AddAuditHistoryEntry(strModule, pForm.Name + " - " + NameofControl, "New Record", (Control as FCGrid).TextMatrix(intCaptionRow, intColumnNumber) + ": " + ValueOfControl);
					}
				}
				// End If
			}
		}

		public void SetGridIDColumn(string strModuleID, string strGridName = "", int intColNumber = 0)
		{
			// THIS ROUTINE SETS SOME PRIVATE VARIABLES WITH THE AUTO ID COLUMN NUMBERS
			// FOR ANY GRID ON THE FORM THAT THIS CLASS IS USED ON. NEED TO SAVE THIS
			// INSTEAD OF THE GRID ROW BECAUSE IF WE WERE TO SAVE THE GRID ROW AND THEN
			// THE USER DELETES A ROW THEN THERE WOULD BE NO WAY TO FIND THE EXACT
			// ROW/COLUMN COMBINATION IN THE ARRAY AND WE WOULD LOSE THAT DATA.
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			// SET THE MODULE THAT WE ARE USING FOR THIS CLASS. PY = PAYROLL, CK = CLERK
			strModule = strModuleID;
			if (strGridName != string.Empty)
			{
				// CHECK TO SEE IF THIS GRIDS ID HAS ALREADY BEEN ENTERED
				for (intCount = 0; intCount <= (Information.UBound(aryGridIDFields, 1) - 1); intCount++)
				{
					if (aryGridIDFields[intCount].Name == strGridName)
					{
						// FOR SOME REASON THIS GRID NAME ALREADY EXIST SO CHANGE ITS
						// ID FIELD NUMBER. THIS WILL ALSO FORCE EVERY GRID ON A FORM
						// TO not HAVE THE SAME NAME WHICH SHOULD NEVER HAPPEN ANYWAY
						aryGridIDFields[intCount].GridRowID = intColNumber;
						return;
					}
				}
				// THIS GRID WAS NOT FOUND SO ADD IT TO THE NEXT EMPTY ELEMENT
				for (intCount = 0; intCount <= (Information.UBound(aryGridIDFields, 1) - 1); intCount++)
				{
					if (aryGridIDFields[intCount].Name == string.Empty)
					{
						aryGridIDFields[intCount].Name = strGridName;
						aryGridIDFields[intCount].GridRowID = intColNumber;
						return;
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWriteFCConvert.ToInt32(
		private int GetGridIDColumn(string strGridName)
		{
			int GetGridIDColumn = 0;
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			// SEARCH THRU THE GRID ID ARRAY AND FIND THE ID COLUMN TO USE IN THE
			// NAME FIELD OF THE CONTROL ARRAY.
			for (intCount = 0; intCount <= (Information.UBound(aryGridIDFields, 1) - 1); intCount++)
			{
				if (aryGridIDFields[intCount].Name == strGridName)
				{
					GetGridIDColumn = aryGridIDFields[intCount].GridRowID;
					return GetGridIDColumn;
				}
			}
			return GetGridIDColumn;
		}

		public void AddAuditHistoryEntry(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsAuditHistory = new clsDRWrapper();
				clsDRWrapper rsCreateTable = new clsDRWrapper();
				string strDatabaseName;
				bool boolNotFound;
				// THIS WILL CREATE THE DATABASE NAME WITH THE MODULE INITIALS THAT ARE PASSED IN
				strDatabaseName = "TW" + strModule + "0000.vb1";
				TryAgain:
				;
				// CHECK TO SEE IF THIS TABLE EXISTS IT THE CURRENT DATABASE
				// THIS WILL OPEN THE AUDIT HISTORY TABLE IN THE SPECIFIED DATABASE
				rsAuditHistory.OpenRecordset("SELECT * FROM AuditHistory WHERE ID = 0", strDatabaseName);
				rsAuditHistory.AddNew();
				rsAuditHistory.Set_Fields("boolUseSecurity", modGlobalConstants.Statics.clsSecurityClass.Using_Security());
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					rsAuditHistory.Set_Fields("EmployeeNumber", string.Empty);
				}
				else
				{
					//if (fecherFoundation.Strings.Trim(MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Text) == string.Empty)
					if (fecherFoundation.Strings.Trim(App.MainForm.StatusBarText2) == string.Empty)
					{
					}
					else
					{
						// THIS IS AN EMPLOYEE SPECIFIC SCREEN SO SAVE THE EMPLOYEE NAME AND NUMBER
						rsAuditHistory.Set_Fields("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig);
					}
				}
				rsAuditHistory.Set_Fields("UserID", modGlobalVariables.Statics.gstrPayrollName);
				rsAuditHistory.Set_Fields("AuditHistoryDate", DateTime.Now);
				rsAuditHistory.Set_Fields("AuditHistoryTime", fecherFoundation.DateAndTime.TimeOfDay);
				rsAuditHistory.Set_Fields("Description1", strDescription1);
				rsAuditHistory.Set_Fields("Description2", strDescription2);
				rsAuditHistory.Set_Fields("Description3", strDescription3);
				rsAuditHistory.Set_Fields("Description4", strDescription4);
				rsAuditHistory.Set_Fields("Description5", strDescription5);
				rsAuditHistory.Update();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// DISPLAY ERROR MESSAGE
				MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Audit History Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: strGridName As object	OnWrite(FCGridCtl.VSFlexGrid, FCGrid.vsFlexGrid, string)
		public void AddAuditHistoryDeleteEntry(ref FCGrid strGridName)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsAuditHistory = new clsDRWrapper();
				clsDRWrapper rsCreateTable = new clsDRWrapper();
				string strDatabaseName = "";
				bool boolNotFound;
				int intCounter;
				int intFound;
				// THIS WILL OPEN THE AUDIT HISTORY TABLE IN THE SPECIFIED DATABASE
				rsAuditHistory.OpenRecordset("SELECT * FROM AuditHistory WHERE ID = 0", "TW" + strModule + "0000.vb1");
				rsAuditHistory.AddNew();
				rsAuditHistory.Set_Fields("UserID", modGlobalVariables.Statics.gstrPayrollName);
				rsAuditHistory.Set_Fields("AuditHistoryDate", DateTime.Now);
				rsAuditHistory.Set_Fields("AuditHistoryTime", fecherFoundation.DateAndTime.TimeOfDay);
				rsAuditHistory.Set_Fields("Description1", strGridName.Parent.Name + " - " + strGridName.Name);
				rsAuditHistory.Set_Fields("Description2", "Delete Row");
				// 3 AS THIS IS THE 3RD DESCRIPTION FIELD
				intFound = 3;
				for (intCounter = 0; intCounter <= strGridName.Cols - 1; intCounter++)
				{
					if (strGridName.ColHidden(intCounter))
					{
					}
					else
					{
						rsAuditHistory.Set_Fields("Description" + intFound, strGridName.TextMatrix(0, intCounter) + ": " + strGridName.TextMatrix(strGridName.Row, intCounter));
						intFound += 1;
					}
					// THERE ARE ONLY 5 DESCRIPTION FIELDS SO EVEN IF THERE ARE MORE COLUMNS
					// IN THE GRID WE CANNOT SAVE ANY MORE INFORMATION.
					if (intFound == 6)
						break;
				}
				rsAuditHistory.Update();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// DISPLAY ERROR MESSAGE
				MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Audit History Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
