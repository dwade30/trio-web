//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2PersonEdit : BaseForm
	{
		public frmW2PersonEdit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2PersonEdit InstancePtr
		{
			get
			{
				return (frmW2PersonEdit)Sys.GetInstance(typeof(frmW2PersonEdit));
			}
		}

		protected frmW2PersonEdit _InstancePtr = null;
		//=========================================================
		private bool boolLoad;
		private clsDRWrapper rsDeductions = new clsDRWrapper();
		// vbPorter upgrade warning: strEmpNumber As string	OnWrite
		private string strEmpNumber = "";
		bool blnUnloadMe;
		const int CNSTROWEMPLOYEENUMBER = 0;
		const int CNSTROWEMPLOYEENAME = 1;
		const int CNSTROWEMPLOYEEMIDDLE = 2;
		const int CNSTROWEMPLOYEELAST = 3;
		const int CNSTROWEMPLOYEEDESIG = 4;
		const int CNSTROWADDRESS = 5;
		const int CNSTROWCITY = 6;
		const int CNSTROWSTATE = 7;
		const int CNSTROWZIP = 8;
		const int CNSTROWSSN = 9;
		const int CNSTROWTOTALGROSS = 10;
		const int CNSTROWFEDERALWAGE = 11;
		const int CNSTROWFICAWAGE = 12;
		const int CNSTROWMEDICAREWAGE = 13;
		const int CNSTROWSTATEWAGE = 14;
		const int CNSTROWFEDERALTAX = 15;
		const int CNSTROWFICATAX = 16;
		const int CNSTROWMEDICARETAX = 17;
		const int CNSTROWSTATETAX = 18;
		const int CNSTROWW2RETIREMENT = 19;
		const int CNSTROWW2DEFERRED = 20;
		const int CNSTROWW2STATUTORYEMP = 21;
		const int CNSTROWW2SEQNUMBER = 22;
		const int CNSTROWW2DEPTDIV = 23;
		const int CNSTROWW2MQGE = 24;

		private void frmW2PersonEdit_Activated(object sender, System.EventArgs e)
		{
			if (boolLoad)
			{
				LoadGrid();
				LoadDeductionGrid();
				//FC:FINAL:DSE:#i2196 Recompute grid column widths
				frmW2PersonEdit_Resize(sender, e);
				boolLoad = false;
			}
			// Call ForceFormToResize(Me)
		}

		private void frmW2PersonEdit_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				blnUnloadMe = true;
				boolLoad = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2PersonEdit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
			else if (KeyCode == Keys.Insert)
			{
				// insert key has been pressed
				if (this.ActiveControl.GetName() == "vsDeductions")
				{
					vsDeductions.Rows += 1;
					vsDeductions.Select(vsDeductions.Rows - 1, 1);
				}
				else if (this.ActiveControl.GetName() == "vsMatch")
				{
					vsMatch.Rows += 1;
					vsMatch.Select(vsMatch.Rows - 1, 1);
				}
			}
		}

		private void frmW2PersonEdit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			//FC:FINAL:DSE:#i2196 Columns ReadOnly flag lost if Editable property is set after setting FixedCols
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsData.Cols = 3;
			vsData.Rows = 25;
			vsData.FixedCols = 2;
			vsData.FixedRows = 0;
			//FC:FINAL:DSE:#i2196 Columns ReadOnly flag lost if Editable property is set after setting FixedCols
			//vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			rsData.OpenRecordset("Select * from tblW2EditTable where ID = " + FCConvert.ToString(modGlobalVariables.Statics.glngW2EditTableID));
			vsData.TextMatrix(CNSTROWEMPLOYEENUMBER, 1, "Employee Number");
			vsData.TextMatrix(CNSTROWEMPLOYEENAME, 1, "Employee First");
			vsData.TextMatrix(CNSTROWEMPLOYEEMIDDLE, 1, "Employee Middle");
			vsData.TextMatrix(CNSTROWEMPLOYEELAST, 1, "Employee Last");
			vsData.TextMatrix(CNSTROWEMPLOYEEDESIG, 1, "Employee Designation");
			vsData.TextMatrix(CNSTROWADDRESS, 1, "Address");
			// .TextMatrix(CNSTROWCITYSTATEZIP, 1) = "City / State / Zip"
			vsData.TextMatrix(CNSTROWCITY, 1, "City");
			vsData.TextMatrix(CNSTROWSTATE, 1, "State");
			vsData.TextMatrix(CNSTROWZIP, 1, "Zip");
			vsData.TextMatrix(CNSTROWSSN, 1, "SSN");
			vsData.TextMatrix(CNSTROWTOTALGROSS, 1, "Total Gross");
			vsData.TextMatrix(CNSTROWFEDERALWAGE, 1, "Federal (Box 1) Wage");
			vsData.TextMatrix(CNSTROWFICAWAGE, 1, "FICA Wage");
			vsData.TextMatrix(CNSTROWMEDICAREWAGE, 1, "Medicare Wage");
			vsData.TextMatrix(CNSTROWSTATEWAGE, 1, "State Wage");
			vsData.TextMatrix(CNSTROWFEDERALTAX, 1, "Federal Tax");
			vsData.TextMatrix(CNSTROWFICATAX, 1, "FICA Tax");
			vsData.TextMatrix(CNSTROWMEDICARETAX, 1, "Medicare Tax");
			vsData.TextMatrix(CNSTROWSTATETAX, 1, "State Tax");
			vsData.TextMatrix(CNSTROWW2RETIREMENT, 1, "W-2 Retirement");
			vsData.TextMatrix(CNSTROWW2DEFERRED, 1, "W-2 Deferred");
			vsData.TextMatrix(CNSTROWW2STATUTORYEMP, 1, "W-2 Statutory Emp");
			vsData.TextMatrix(CNSTROWW2DEPTDIV, 1, "Dept/Div");
			vsData.TextMatrix(CNSTROWW2SEQNUMBER, 1, "Sequence");
			vsData.TextMatrix(CNSTROWW2MQGE, 1, "MQGE");
			if (!rsData.EndOfFile())
			{
				vsData.TextMatrix(CNSTROWEMPLOYEENUMBER, 2, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
				strEmpNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
				// .TextMatrix(CNSTROWEMPLOYEENAME, 2) = rsData.Fields("EmployeeName")
				vsData.TextMatrix(CNSTROWEMPLOYEENAME, 2, FCConvert.ToString(rsData.Get_Fields("firstNAME")));
				vsData.TextMatrix(CNSTROWEMPLOYEEMIDDLE, 2, FCConvert.ToString(rsData.Get_Fields("middlename")));
				vsData.TextMatrix(CNSTROWEMPLOYEELAST, 2, FCConvert.ToString(rsData.Get_Fields("lastname")));
				vsData.TextMatrix(CNSTROWEMPLOYEEDESIG, 2, FCConvert.ToString(rsData.Get_Fields("desig")));
				vsData.TextMatrix(CNSTROWADDRESS, 2, FCConvert.ToString(rsData.Get_Fields_String("Address")));
				// .TextMatrix(CNSTROWCITYSTATEZIP, 2) = rsData.Fields("CityStateZip")
				vsData.TextMatrix(CNSTROWCITY, 2, FCConvert.ToString(rsData.Get_Fields("city")));
				vsData.TextMatrix(CNSTROWSTATE, 2, FCConvert.ToString(rsData.Get_Fields("state")));
				vsData.TextMatrix(CNSTROWZIP, 2, FCConvert.ToString(rsData.Get_Fields_String("zip")));
				vsData.TextMatrix(CNSTROWSSN, 2, FCConvert.ToString(rsData.Get_Fields_String("SSN")));
				vsData.TextMatrix(CNSTROWTOTALGROSS, 2, Strings.Format(rsData.Get_Fields_Double("TotalGross"), "0.00"));
				vsData.TextMatrix(CNSTROWFEDERALWAGE, 2, Strings.Format(rsData.Get_Fields_Double("FederalWage"), "0.00"));
				vsData.TextMatrix(CNSTROWFICAWAGE, 2, Strings.Format(rsData.Get_Fields("FICAWage"), "0.00"));
				vsData.TextMatrix(CNSTROWMEDICAREWAGE, 2, Strings.Format(rsData.Get_Fields_Double("MedicareWage"), "0.00"));
				vsData.TextMatrix(CNSTROWSTATEWAGE, 2, Strings.Format(rsData.Get_Fields_Double("StateWage"), "0.00"));
				vsData.TextMatrix(CNSTROWFEDERALTAX, 2, Strings.Format(rsData.Get_Fields_Double("FederalTax"), "0.00"));
				vsData.TextMatrix(CNSTROWFICATAX, 2, Strings.Format(rsData.Get_Fields("FICATax"), "0.00"));
				vsData.TextMatrix(CNSTROWMEDICARETAX, 2, Strings.Format(rsData.Get_Fields("MedicareTax"), "0.00"));
				vsData.TextMatrix(CNSTROWSTATETAX, 2, Strings.Format(rsData.Get_Fields_Double("StateTax"), "0.00"));
				// .TextMatrix(CNSTROWW2RETIREMENT, 2) = rsData.Fields("W2Retirement")
				if (FCConvert.CBool(rsData.Get_Fields_Boolean("w2retirement")))
				{
					vsData.TextMatrix(CNSTROWW2RETIREMENT, 2, "Yes");
				}
				else
				{
					vsData.TextMatrix(CNSTROWW2RETIREMENT, 2, "No");
				}
				// .TextMatrix(CNSTROWW2DEFERRED, 2) = rsData.Fields("W2Deferred")
				if (FCConvert.CBool(rsData.Get_Fields_Boolean("w2deferred")))
				{
					vsData.TextMatrix(CNSTROWW2DEFERRED, 2, "Yes");
				}
				else
				{
					vsData.TextMatrix(CNSTROWW2DEFERRED, 2, "No");
				}
				// .TextMatrix(CNSTROWW2STATUTORYEMP, 2) = rsData.Fields("W2StatutoryEmployee")
				if (FCConvert.CBool(rsData.Get_Fields_Boolean("w2statutoryemployee")))
				{
					vsData.TextMatrix(CNSTROWW2STATUTORYEMP, 2, "Yes");
				}
				else
				{
					vsData.TextMatrix(CNSTROWW2STATUTORYEMP, 2, "No");
				}
				vsData.TextMatrix(CNSTROWW2SEQNUMBER, 2, FCConvert.ToString(Conversion.Val(rsData.Get_Fields("seqnumber"))));
				vsData.TextMatrix(CNSTROWW2DEPTDIV, 2, FCConvert.ToString(rsData.Get_Fields("deptdiv")));
				// .TextMatrix(CNSTROWW2MQGE, 2) = rsData.Fields("mqge")
				if (FCConvert.CBool(rsData.Get_Fields_Boolean("mqge")))
				{
					vsData.TextMatrix(CNSTROWW2MQGE, 2, "Yes");
				}
				else
				{
					vsData.TextMatrix(CNSTROWW2MQGE, 2, "No");
				}
			}
			else
			{
				strEmpNumber = FCConvert.ToString(0);
			}
			vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsData.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarComplete;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void LoadDeductionGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsMatch = new clsDRWrapper();
			clsDRWrapper rsDesc = new clsDRWrapper();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			vsDeductions.Cols = 4;
			vsDeductions.Rows = 1;
            //FC:FINAL:AM:#2781 - first column is hidden
            //vsDeductions.FixedCols = 1;
            vsDeductions.FrozenCols = 2;
			vsDeductions.FixedRows = 1;
			vsMatch.Cols = 4;
			vsMatch.Rows = 1;
            //vsMatch.FixedCols = 1;
            vsMatch.FrozenCols = 2;
			vsMatch.FixedRows = 1;
			vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			rsData.OpenRecordset("Select * from tblW2Deductions where EmployeeNumber = '" + strEmpNumber + "'");
			rsMatch.OpenRecordset("Select * from tblW2Matches where EmployeeNumber = '" + strEmpNumber + "'");
			rsDesc.OpenRecordset("select * from tblw2AdditionalInfo order by ID", "twpy0000.vb1");
			rsDeductions.OpenRecordset("Select * from tblDeductionSetup Order by DeductionNumber");
			vsDeductions.TextMatrix(0, 1, "Ded");
			vsDeductions.TextMatrix(0, 2, "Description");
			vsDeductions.TextMatrix(0, 3, "CYTD Amt");


            if (!rsDeductions.EndOfFile())
            {
                rsDeductions.MoveLast();
                rsDeductions.MoveFirst();
                // makes this column work like a combo box.
                // this fills the box with the values from the tblPayCategories table
                // This forces this column to work as a combo box
                var comboList = "";
                for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
                {
                    if (intCounter == 1)
                    {
                        comboList += "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(15 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description");
                    }
                    else
                    {
                        comboList += "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(15 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductions.Get_Fields("Description");
                    }
                    rsDeductions.MoveNext();
                }

                vsDeductions.ColComboList(1, comboList);
                vsMatch.ColComboList(1, comboList);
            }

            //vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsDeductions.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            //FC:FINAL:SGA - #2802 - last column must be right align
            vsDeductions.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				vsDeductions.Rows += 1;
				vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
				vsDeductions.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")));
				rsDeductions.FindFirstRecord("ID", rsData.Get_Fields_Int32("DeductionNumber"));
				if (rsDeductions.NoMatch && Conversion.Val(rsData.Get_Fields("deductionnumber")) > 9000)
				{
					if (rsDesc.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields("deductionnumber")) - 9000))
					{
						vsDeductions.TextMatrix(intCounter, 2, FCConvert.ToString(rsDesc.Get_Fields_String("description")));
					}
				}
				else
				{
					vsDeductions.TextMatrix(intCounter, 2, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
				}
				vsDeductions.TextMatrix(intCounter, 3, Strings.Format(rsData.Get_Fields_Double("CYTDAmount"), "0.00"));
				// .Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 1) = rsData.Fields("DeductionNumber")
				rsData.MoveNext();
			}
			if (vsDeductions.Rows > 1)
			{
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsDeductions.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			vsMatch.TextMatrix(0, 1, "Ded");
			vsMatch.TextMatrix(0, 2, "Description");
			vsMatch.TextMatrix(0, 3, "CYTD Amt");
			//vsMatch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsMatch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            //FC:FINAL:SGA - #2802 - last column must be right align
            vsMatch.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
			{
				vsMatch.Rows += 1;
				vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(rsMatch.Get_Fields("ID")));
				vsMatch.TextMatrix(intCounter, 1, FCConvert.ToString(rsMatch.Get_Fields_Int32("DeductionNumber")));
				rsDeductions.FindFirstRecord("ID", rsMatch.Get_Fields_Int32("DeductionNumber"));
				if (rsDeductions.NoMatch && Conversion.Val(rsMatch.Get_Fields("deductionnumber")) > 9000)
				{
					if (rsDesc.FindFirstRecord("ID", Conversion.Val(rsMatch.Get_Fields("deductionnumber")) - 9000))
					{
						vsMatch.TextMatrix(intCounter, 2, FCConvert.ToString(rsDesc.Get_Fields_String("description")));
					}
				}
				else
				{
					vsMatch.TextMatrix(intCounter, 2, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
				}
				vsMatch.TextMatrix(intCounter, 3, Strings.Format(rsMatch.Get_Fields_Double("CYTDAmount"), "0.00"));
				rsMatch.MoveNext();
			}
			if (vsMatch.Rows > 1)
			{
				vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsMatch.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
        }

        //FC:FINAL:SGA - #2802 - prevent RowColChange event one time from Resize
        private bool firstResize = false;
		private void frmW2PersonEdit_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, this.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(this.WidthOriginal * 0.16));
			vsData.ColWidth(2, FCConvert.ToInt32(this.WidthOriginal * 0.37));
			vsDeductions.ColWidth(0, vsDeductions.WidthOriginal * 0);
			vsDeductions.ColWidth(1, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.15));
			vsDeductions.ColWidth(2, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.45));
			vsDeductions.ColWidth(3, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.3));
			vsMatch.ColWidth(0, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(1, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.15));
			vsMatch.ColWidth(2, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.45));
			vsMatch.ColWidth(3, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.3));
            firstResize = true;
        }

		private void Form_Unload(object sender, FormClosedEventArgs e)
		{
			frmW2Edit.InstancePtr.Show(App.MainForm);
		}

		private void mnuAddDeduction_Click(object sender, System.EventArgs e)
		{
			vsDeductions.Focus();
			vsDeductions.Rows += 1;
			vsDeductions.TextMatrix(vsDeductions.Rows - 1, 0, FCConvert.ToString(0));
			vsDeductions.Select(vsDeductions.Rows - 1, 1);
			// vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Rows - 1, 3) = TRIOCOLORGRAYBACKGROUND
		}

		private void mnuAddEmpMatchCode_Click(object sender, System.EventArgs e)
		{
			vsMatch.Focus();
			vsMatch.Rows += 1;
			vsMatch.TextMatrix(vsMatch.Rows - 1, 0, FCConvert.ToString(0));
			vsMatch.Select(vsMatch.Rows - 1, 1);
			// vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Rows - 1, 3) = TRIOCOLORGRAYBACKGROUND
		}

		private void mnuDeleteDeductionCode_Click(object sender, System.EventArgs e)
		{
			if (vsDeductions.Row > 0)
			{
				if (MessageBox.Show("This will remove code: " + vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsDeductions.Row, 1) + ". Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblW2Deductions where ID = " + FCConvert.ToString(Conversion.Val(vsDeductions.TextMatrix(vsDeductions.Row, 0))), "TWPY0000.vb1");
					vsDeductions.RemoveItem(vsDeductions.Row);
					//App.DoEvents();
					vsDeductions.Row = 0;
					vsDeductions_RowColChange(null, null);
				}
			}
			else
			{
				MessageBox.Show("Deduction Code must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuDeleteEmployee_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsReportHeader = new clsDRWrapper();
			if (MessageBox.Show("Are you sure you wish to delete this Employee's W-2 Information?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				modGlobalFunctions.AddCYAEntry_6("PY", "Delete employee from W-2 Edit screen", "EmployeeNumber=" + this.vsData.TextMatrix(vsData.Row, vsData.Col));
				rsReportHeader.Execute("Delete from tblW2EditTable where EmployeeNumber = '" + this.vsData.TextMatrix(vsData.Row, vsData.Col) + "'", "Payroll");
				rsReportHeader.Execute("Delete from tblW2Deductions where EmployeeNumber = '" + this.vsData.TextMatrix(vsData.Row, vsData.Col) + "'", "Payroll");
				rsReportHeader.Execute("Delete from tblW2Matches where EmployeeNumber = '" + this.vsData.TextMatrix(vsData.Row, vsData.Col) + "'", "Payroll");
				Close();
				frmW2Edit.InstancePtr.Show(App.MainForm);
			}
		}

		private void mnuDeleteMatch_Click(object sender, System.EventArgs e)
		{
			if (vsMatch.Row > 0)
			{
				if (MessageBox.Show("This will remove code: " + vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsMatch.Row, 1) + ". Continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsData = new clsDRWrapper();
					rsData.Execute("Delete from tblW2Matches where ID = " + FCConvert.ToString(Conversion.Val(vsMatch.TextMatrix(vsMatch.Row, 0))), "TWPY0000.vb1");
					vsMatch.RemoveItem(vsMatch.Row);
					//App.DoEvents();
					vsMatch.Row = 0;
					vsMatch_RowColChange(null, null);
				}
			}
			else
			{
				MessageBox.Show("Employer Match Code must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				vsData.Select(0, 0);
				vsDeductions.Select(0, 0);
				vsMatch.Select(0, 0);
				// ************************************************************
				clsDRWrapper rsReportHeader = new clsDRWrapper();
				rsReportHeader.OpenRecordset("Select * from tblW2EditTable where ID = " + FCConvert.ToString(modGlobalVariables.Statics.glngW2EditTableID));
				if (rsReportHeader.EndOfFile())
				{
					rsReportHeader.AddNew();
				}
				else
				{
					rsReportHeader.Edit();
				}
				rsReportHeader.Set_Fields("EmployeeNumber", vsData.TextMatrix(CNSTROWEMPLOYEENUMBER, 2));
				rsReportHeader.Set_Fields("EmployeeName", fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTROWEMPLOYEENAME, 2) + " " + vsData.TextMatrix(CNSTROWEMPLOYEEMIDDLE, 2)) + " " + vsData.TextMatrix(CNSTROWEMPLOYEELAST, 2) + " " + vsData.TextMatrix(CNSTROWEMPLOYEEDESIG, 2)));
				rsReportHeader.Set_Fields("firstname", vsData.TextMatrix(CNSTROWEMPLOYEENAME, 2));
				rsReportHeader.Set_Fields("middlename", vsData.TextMatrix(CNSTROWEMPLOYEEMIDDLE, 2));
				rsReportHeader.Set_Fields("lastname", vsData.TextMatrix(CNSTROWEMPLOYEELAST, 2));
				rsReportHeader.Set_Fields("Desig", vsData.TextMatrix(CNSTROWEMPLOYEEDESIG, 2));
				rsReportHeader.Set_Fields("Address", vsData.TextMatrix(CNSTROWADDRESS, 2));
				rsReportHeader.Set_Fields("CityStateZip", fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTROWCITY, 2) + ", " + vsData.TextMatrix(CNSTROWSTATE, 2) + " " + vsData.TextMatrix(CNSTROWZIP, 2)));
				rsReportHeader.Set_Fields("City", vsData.TextMatrix(CNSTROWCITY, 2));
				rsReportHeader.Set_Fields("state", vsData.TextMatrix(CNSTROWSTATE, 2));
				rsReportHeader.Set_Fields("zip", vsData.TextMatrix(CNSTROWZIP, 2));
				rsReportHeader.Set_Fields("SSN", vsData.TextMatrix(CNSTROWSSN, 2));
				if (fecherFoundation.Strings.Trim(vsData.TextMatrix(CNSTROWW2RETIREMENT, 2)) == string.Empty)
				{
					rsReportHeader.Set_Fields("W2Retirement", false);
				}
				else
				{
					if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTROWW2RETIREMENT, 2)) == "YES")
					{
						rsReportHeader.Set_Fields("w2retirement", true);
					}
					else
					{
						rsReportHeader.Set_Fields("w2retirement", false);
					}
					// .Fields("W2Retirement") = vsData.TextMatrix(CNSTROWW2RETIREMENT, 2)
				}

				if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTROWW2DEFERRED, 2)) == "YES")
				{
					rsReportHeader.Set_Fields("w2deferred", true);
				}
				else
				{
					rsReportHeader.Set_Fields("w2deferred", false);
				}

				if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTROWW2STATUTORYEMP, 2)) == "YES")
				{
					rsReportHeader.Set_Fields("w2statutoryemployee", true);
				}
				else
				{
					rsReportHeader.Set_Fields("w2statutoryemployee", false);
				}

				if (fecherFoundation.Strings.UCase(vsData.TextMatrix(CNSTROWW2MQGE, 2)) == "YES")
				{
					rsReportHeader.Set_Fields("mqge", true);
				}
				else
				{
					rsReportHeader.Set_Fields("mqge", false);
				}
				rsReportHeader.Set_Fields("deptdiv", vsData.TextMatrix(CNSTROWW2DEPTDIV, 2));
				rsReportHeader.Set_Fields("seqnumber", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(CNSTROWW2SEQNUMBER, 2))));
				rsReportHeader.Set_Fields("TotalGross", (vsData.TextMatrix(CNSTROWTOTALGROSS, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWTOTALGROSS, 2)));
				rsReportHeader.Set_Fields("FederalWage", (vsData.TextMatrix(CNSTROWFEDERALWAGE, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWFEDERALWAGE, 2)));
				rsReportHeader.Set_Fields("FICAWage", (vsData.TextMatrix(CNSTROWFICAWAGE, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWFICAWAGE, 2)));
				rsReportHeader.Set_Fields("MedicareWage", (vsData.TextMatrix(CNSTROWMEDICAREWAGE, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWMEDICAREWAGE, 2)));
				rsReportHeader.Set_Fields("StateWage", (vsData.TextMatrix(CNSTROWSTATEWAGE, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWSTATEWAGE, 2)));
				rsReportHeader.Set_Fields("FederalTax", (vsData.TextMatrix(CNSTROWFEDERALTAX, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWFEDERALTAX, 2)));
				rsReportHeader.Set_Fields("FICATax", (vsData.TextMatrix(CNSTROWFICATAX, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWFICATAX, 2)));
				rsReportHeader.Set_Fields("MedicareTax", (vsData.TextMatrix(CNSTROWMEDICARETAX, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWMEDICARETAX, 2)));
				rsReportHeader.Set_Fields("StateTax", (vsData.TextMatrix(CNSTROWSTATETAX, 2) == string.Empty ? "0" : vsData.TextMatrix(CNSTROWSTATETAX, 2)));
				rsReportHeader.Update();
				modGlobalVariables.Statics.glngW2EditTableID = FCConvert.ToInt32(rsReportHeader.Get_Fields("ID"));
				// ************************************************************
				rsData.OpenRecordset("Select * from tblW2Deductions where EmployeeNumber = '" + strEmpNumber + "'");
				for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsDeductions.TextMatrix(intCounter, 1)) > 0)
					{
						if (Conversion.Val(vsDeductions.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							rsData.AddNew();
						}
						else
						{
							rsData.FindFirstRecord("ID", Conversion.Val(vsDeductions.TextMatrix(intCounter, 0)));
							if (rsData.NoMatch)
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
						}
						rsData.Set_Fields("EmployeeNumber", strEmpNumber);
						rsData.Set_Fields("DeductionNumber", vsDeductions.TextMatrix(intCounter, 1));
						rsData.Set_Fields("CYTDAmount", vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3));
						rsData.Set_Fields("Box12", true);
						rsData.Update();
						vsDeductions.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
					}
				}
				// ***************************************************************
				rsData.OpenRecordset("Select * from tblW2Matches where EmployeeNumber = '" + strEmpNumber + "'");
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsMatch.TextMatrix(intCounter, 1)) > 0)
					{
						if (Conversion.Val(vsMatch.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							rsData.AddNew();
						}
						else
						{
							rsData.FindFirstRecord("ID", vsMatch.TextMatrix(intCounter, 0));
							if (rsData.NoMatch)
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
						}
						rsData.Set_Fields("EmployeeNumber", strEmpNumber);
						rsData.Set_Fields("DeductionNumber", vsMatch.TextMatrix(intCounter, 1) + " ");
						if (Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3)) != 0)
						{
							rsData.Set_Fields("cytdamount", FCConvert.ToDouble(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3)));
						}
						else
						{
							rsData.Set_Fields("CYTDAmount", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 3))));
						}
						rsData.Set_Fields("Box14", true);
						rsData.Update();
						vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				modGlobalRoutines.UpdateW2StatusData("Contact");
				modGlobalRoutines.UpdateW2StatusData("Update");
				MessageBox.Show("Update completed successfully", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				if (blnUnloadMe == true)
				{
					Close();
					// frmW2Edit.Show , MDIParent
				}
				blnUnloadMe = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		public void mnuProcess_Click()
		{
			mnuProcess_Click(mnuProcess, new System.EventArgs());
		}

		private void SaveContinue_Click(object sender, System.EventArgs e)
		{
			blnUnloadMe = false;
			mnuProcess_Click();
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// If Row = 16 Then
			// vsData.EditMask = "0"
			// Else
			// vsData.EditMask = vbNullString
			// End If
		}

		private void vsData_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			// If Row = 16 Then
			// If KeyAscii = 49 Or KeyAscii = 50 Or KeyAscii = 51 Or KeyAscii = 8 Then
			// 
			// Else
			// KeyAscii = 0
			// End If
			// End If
		}

		private void vsData_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			// If vsData.MouseRow = 16 Then
			// vsData.ToolTipText = "Enter '1' for E Mail/Internet" & "      '2' for Fax" & "      '3' for Postal Service"
			// Else
			// vsData.ToolTipText = vbNullString
			// End If
		}

		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsData.Row)
			{
				case CNSTROWW2DEFERRED:
				case CNSTROWW2RETIREMENT:
				case CNSTROWW2STATUTORYEMP:
				case CNSTROWW2MQGE:
					{
						// vsData.ComboList = "True|False"
						vsData.ComboList = "Yes|No";
						break;
					}
				default:
					{
						vsData.ComboList = "";
						break;
					}
			}
			//end switch
		}

		private void vsDeductions_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void vsMatch_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void vsDeductions_RowColChange(object sender, System.EventArgs e)
		{
            if (!firstResize)
            {
                return;
            }
            if (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) == 0)
			{
				vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				if (vsDeductions.Col == 3)
				{
                    vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			rsDeductions.FindFirstRecord("ID", Conversion.Val(vsDeductions.TextMatrix(vsDeductions.Row, 1)));
			if (rsDeductions.NoMatch)
			{
			}
			else
			{
				vsDeductions.TextMatrix(vsDeductions.Row, 2, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
				// vsDeductions.Select vsDeductions.Row, 3
			}
		}

		private void vsMatch_RowColChange(object sender, System.EventArgs e)
		{
            if (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, vsMatch.Col)) == 0)
	        {
				vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				if (vsMatch.Col == 3)
				{
					vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsMatch.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			rsDeductions.FindFirstRecord("ID", Conversion.Val(vsMatch.TextMatrix(vsMatch.Row, 1)));
			if (rsDeductions.NoMatch)
			{
			}
			else
			{
				vsMatch.TextMatrix(vsMatch.Row, 2, FCConvert.ToString(rsDeductions.Get_Fields("Description")));
				// vsMatch.Select vsMatch.Row, 3
			}
		}
	}
}
