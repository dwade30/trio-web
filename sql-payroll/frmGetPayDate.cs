//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmGetPayDate : BaseForm
	{
		public frmGetPayDate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGetPayDate InstancePtr
		{
			get
			{
				return (frmGetPayDate)Sys.GetInstance(typeof(frmGetPayDate));
			}
		}

		protected frmGetPayDate _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strPayDate;

		public string Init(string strMessage = "")
		{
			string Init = "";
			strPayDate = "";
			lblMessage.Text = strMessage;
			Init = "";
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strPayDate;
			return Init;
		}

		private void frmGetPayDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetPayDate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetPayDate properties;
			//frmGetPayDate.FillStyle	= 0;
			//frmGetPayDate.ScaleWidth	= 3885;
			//frmGetPayDate.ScaleHeight	= 2205;
			//frmGetPayDate.LinkTopic	= "Form2";
			//frmGetPayDate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			cmbPayDate.Clear();
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select paydate from tblcheckdetail where checkvoid = 0 and not msrsadjustrecord = 1 and not adjustrecord = 1 and not taadjustrecord = 1 group by paydate order by paydate desc", "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
                cmbPayDate.AddItem(FCConvert.ToString(clsLoad.Get_Fields("paydate")));
                clsLoad.MoveNext();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			strPayDate = cmbPayDate.Text;
			Close();
		}
	}
}
