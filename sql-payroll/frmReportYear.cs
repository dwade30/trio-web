﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmReportYear : BaseForm
	{
		public frmReportYear()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReportYear InstancePtr
		{
			get
			{
				return (frmReportYear)Sys.GetInstance(typeof(frmReportYear));
			}
		}

		protected frmReportYear _InstancePtr = null;
		//=========================================================
		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (cmbMonthly.Text == "Calander")
			{
				modGlobalVariables.Statics.gstrMQYProcessing = "MONTHLY";
				//} else if (optNone.Value==true) {
				//	modGlobalVariables.Statics.gstrMQYProcessing = "NONE";
			}
			else if (cmbMonthly.Text == "Fiscal")
			{
				modGlobalVariables.Statics.gstrMQYProcessing = "QUARTERLY";
			}
			else
			{
				modGlobalVariables.Statics.gstrMQYProcessing = "YEARLY";
			}
		}

		private void frmReportYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void optMonthly_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbMonthly.Text = "Calander";
			//optYearly.Value = false;
			//optNone.Value = false;
		}

		private void optNone_Click()
		{
			cmbMonthly.Text = "";
			//optYearly.Value = false;
			//optNone.Value = true;
		}

		private void optQuarterly_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbMonthly.Text = "Fiscal";
			//optYearly.Value = false;
			//optNone.Value = false;
		}

		private void optYearly_Click()
		{
			cmbMonthly.Text = "";
			//optYearly.Value = true;
			//optNone.Value = false;
		}

		private void frmReportYear_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportYear properties;
			//frmReportYear.ScaleWidth	= 3885;
			//frmReportYear.ScaleHeight	= 2475;
			//frmReportYear.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
