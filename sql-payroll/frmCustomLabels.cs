//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCustomLabels : BaseForm
	{
		public frmCustomLabels()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomLabels InstancePtr
		{
			get
			{
				return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
			}
		}

		protected frmCustomLabels _InstancePtr = null;
		//=========================================================
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomLabels.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomLabels.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomLabels, "Births")
		//
		//
		int intCounter;
		// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToInt32(
		int intStart;
		int intEnd;
		// vbPorter upgrade warning: intID As int	OnWriteFCConvert.ToInt32(
		int intID;
		string strTemp = "";
		bool boolPrintPreview;
		bool boolSaveReport;
		clsPrintLabel labLabel = new clsPrintLabel();
		private int intAssessIndex;

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strDesc = "";
			// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
			//double dblSizeRatio;
			// OptLabelType(0).Enabled = True
			// OptLabelType(1).Enabled = True
			// OptLabelType(2).Enabled = True
			// OptLabelType(4).Enabled = True
			//dblSizeRatio = (Line2.X2 - Line2.X1) / 1440;
			vsLayout.Clear();
			if (cmbLabelType.SelectedIndex < 0)
				return;
			// Select Case .ListIndex
			switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
			{
				case modLabels.CNSTLBLTYPE4013:
					{
						strDesc = "Style 4013.  15/16 in. x 3.5 in. This type is a continuous sheet of labels used with a dot matrix.";
						// vsLayout.ColWidth(0) = 3.5 * 1440
						// framLaserOptions.Visible = False
						vsLayout.Rows = 6;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(3.5F * 1440);
						//Line1.X1 = FCConvert.ToSingle(3.5 * dblSizeRatio);
						//Line1.X2 = Line1.X1;
						//Line1.Y1 = 0;
						//Line1.Y2 = vsLayout.HeightOriginal;
						break;
					}
				case modLabels.CNSTLBLTYPE4014:
					{
						strDesc = "Style 4014.  1 7/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix.";
						// vsLayout.ColWidth(0) = 4 * 1440
						// framLaserOptions.Visible = False
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4 * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE4030:
					{
						strDesc = "Style 4030.  15/16 in. x 3.5 in. 2 Across. This type is a continuous sheet of labels used with a dot matrix.";
						// framLaserOptions.Visible = False
						vsLayout.Rows = 6;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(3.5F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(3.5F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5160:
					{
						strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in.";
						// vsLayout.ColWidth(0) = 2.625 * 1440
						// framLaserOptions.Visible = True
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(2.63F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(2.63F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5161:
					{
						strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in.";
						// framLaserOptions.Visible = True
						// vsLayout.ColWidth(0) = 4 * 1440
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5162:
					{
						strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in.";
						// framLaserOptions.Visible = True
						// vsLayout.ColWidth(0) = 4 * 1440
						// 1440/240(row height)
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5163:
					{
						strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in.";
						// framLaserOptions.Visible = True
						// vsLayout.ColWidth(0) = 4 * 1440
						// 1440/240(row height)
						vsLayout.Rows = 13;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5026:
					{
						strDesc = "Style 5026, 5027. Sheet of 2 X 9.  1 in. X 3.5 in.";
						// framLaserOptions.Visible = True
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(3.5F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(3.5F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE5066:
					{
						strDesc = "Style 5066, 5266, 5366, 5666, 5766, 5866, 5966, 8366. Sheet of 2 X 15.  .66 in. X 3.5 in.";
						// framLaserOptions.Visible = True
						vsLayout.Rows = 5;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(3.5F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(3.5F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPEASSESSMENT:
					{
						strDesc = "Assessment labels 7/16 in. X 5 in. This type is a continuous sheet of labels used with a dot matrix.";
						// OptLabelType(0).Enabled = False
						// OptLabelType(1).Enabled = False
						// OptLabelType(2).Enabled = False
						// OptLabelType(4).Enabled = False
						// framLaserOptions.Visible = False
						// If Not OptLabelType(3).Value Then
						// OptLabelType(3).Value = True
						// End If
						break;
					}
				case modLabels.CNSTLBLTYPE4031:
					{
						strDesc = "Style 4031.  15/16 in. x 3.5 in. 3 Across. This type is a continuous sheet of labels used with a wide dot matrix.";
						// framLaserOptions.Visible = False
						vsLayout.Rows = 6;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(3.5F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(3.5F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE4033:
					{
						strDesc = "Style 4033.  1 7/16 in. x 4 in. 3 Across. This type is a continuous sheet of labels used with a wide dot matrix.";
						// framLaserOptions.Visible = False
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case modLabels.CNSTLBLTYPE4065:
					{
						strDesc = "Style 4065.  15/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix.";
						// framLaserOptions.Visible = False
						vsLayout.Rows = 6;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle(4F * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
			}
			//end switch
			lblDescription.Text = strDesc;
		}

		private void FillLabelTypeCombo()
		{
			int x;
			cmbLabelType.Clear();
			for (x = 0; x <= labLabel.TypeCount - 1; x++)
			{
				// If labLabel.Visible(x) Then
				// If labLabel.IsDymoLabel(x) Then
				// labLabel.Visible(x) = False
				// End If
				// Else
				// If labLabel.ID(x) = CNSTLBLTYPEASSESSMENT Then
				// labLabel.Visible(x) = False
				// ElseIf labLabel.ID(x) = CNSTLBLTYPE4033 Then
				// labLabel.Visible(x) = True
				// ElseIf labLabel.ID(x) = CNSTLBLTYPE4031 Then
				// labLabel.Visible(x) = True
				// ElseIf labLabel.ID(x) = CNSTLBLTYPE4065 Then
				// labLabel.Visible(x) = True
				// ElseIf labLabel.IsDymoLabel(x) Then
				// labLabel.Visible(x) = False
				// End If
				// End If
				if (labLabel.Get_Visible(x))
				{
					cmbLabelType.AddItem(labLabel.Get_Caption(x));
					cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabel.Get_ID(x));
					if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPEASSESSMENT)
					{
						intAssessIndex = cmbLabelType.NewIndex;
					}
				}
			}
			// x
			// .AddItem ("Avery 4013") '0
			// .ItemData(.NewIndex) = 0
			// .AddItem ("Avery 4014") '1
			// .ItemData(.NewIndex) = 1
			// 
			// .AddItem ("Avery 4030") '2
			// .ItemData(.NewIndex) = 2
			// 
			// .AddItem ("Avery 4031") '9
			// .ItemData(.NewIndex) = 9
			// .AddItem ("Avery 4033") '10
			// .ItemData(.NewIndex) = 10
			// .AddItem ("Avery 4065")
			// .ItemData(.NewIndex) = 11
			// 
			// .AddItem ("Avery 5160,5260,5970") '3
			// .ItemData(.NewIndex) = 3
			// .AddItem ("Avery 5161,5261,5661") '4
			// .ItemData(.NewIndex) = 4
			// .AddItem ("Avery 5162,5262,5662") '5
			// .ItemData(.NewIndex) = 5
			// .AddItem ("Avery 5163,5263,5663") '6
			// .ItemData(.NewIndex) = 6
			// .AddItem ("Assessment Labels") '8
			// .ItemData(.NewIndex) = 8
			// intAssessIndex = .NewIndex
		}

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			if (cmbReport.Text == "Delete Saved Label")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobalVariables.DEFAULTDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.Text == "Show Saved Label")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				rs.OpenRecordset("Select * from tblCustomReports where ID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)), modGlobalVariables.DEFAULTDATABASE);
				if (!rs.EndOfFile())
				{
					cmbType.SelectedIndex = rs.Get_Fields_Int16("LabelType");
				}
				vsLayout.Clear();
				vsLayout.Rows = 2;
				vsLayout.Cols = 0;
				rs.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)) + " Order by RowID, ColumnID", modGlobalVariables.DEFAULTDATABASE);
				while (!rs.EndOfFile())
				{
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) == 1)
					{
						if (rs.Get_Fields_Int16("ColumnID") >= 0)
						{
							vsLayout.Cols += 1;
						}
					}
					else
					{
						if (rs.Get_Fields_Int16("RowID") >= vsLayout.Rows)
						{
							vsLayout.Rows += 1;
                            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
                            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
						}
					}
					vsLayout.TextMatrix(rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), FCConvert.ToString(rs.Get_Fields_String("DisplayText")));
					vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), rs.Get_Fields("FieldID"));
					vsLayout.ColWidth(rs.Get_Fields_Int16("ColumnID"), FCConvert.ToInt32(rs.Get_Fields("Width")));
					rs.MoveNext();
				}
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string strReturn;
			clsDRWrapper RSLayout = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			int intLabelType = 0;
			strReturn = Interaction.InputBox("Enter name for new report.", "New Custom Report", null);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobalVariables.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					MessageBox.Show("A report by that name already exists. A different name must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					if (cmbType.Text == "Avery 5260  (3 X 10)")
					{
						intLabelType = 0;
					}
					else if (cmbType.Text == "Avery 5261 (2 X 10)")
					{
						intLabelType = 1;
					}
					else if (cmbType.Text == "Avery 5263 (2 X 5)")
					{
						intLabelType = 2;
					}
					else if (cmbType.Text == "Avery 5262 (2 X 7)")
					{
						intLabelType = 3;
					}
					rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,LabelType) VALUES ('" + strReturn + "','" + FCConvert.ToString(modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL)) + "','" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'," + FCConvert.ToString(intLabelType) + ")", modGlobalVariables.DEFAULTDATABASE);
					rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGlobalVariables.DEFAULTDATABASE);
					if (!rsSave.EndOfFile())
					{
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobalVariables.DEFAULTDATABASE);
						for (intRow = 1; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
						{
							for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
							{
								RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobalVariables.DEFAULTDATABASE);
								RSLayout.AddNew();
								RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
								RSLayout.Set_Fields("RowID", intRow);
								RSLayout.Set_Fields("ColumnID", intCol);
								if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									RSLayout.Set_Fields("FieldID", -1);
								}
								else
								{
									RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
								}
								RSLayout.Set_Fields("Width", frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol));
								RSLayout.Set_Fields("DisplayText", frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
								RSLayout.Update();
							}
						}
					}
					LoadCombo();
					MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			clsDRWrapper rsSQL = new clsDRWrapper();
			// CLEAR THE FIELDS WIDTH ARRAY
			for (intCounter = 0; intCounter <= 49; intCounter++)
			{
				modCustomReport.Statics.strFieldWidth[intCounter] = 0;
			}
			// PREPARE THE SQL TO SHOW THE REPORT
			bool executeNoFields = false;
			if (cmbReport.Text == "Create New Label")
			{
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				BuildSQL();
				// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			else
			{
				// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// GET THE SAVED SQL STATEMENT FOR THE CUSTOM REPORT
				rsSQL.OpenRecordset("Select SQL from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobalVariables.DEFAULTDATABASE);
				if (!rsSQL.EndOfFile())
				{
					modCustomReport.Statics.strCustomSQL = FCConvert.ToString(rsSQL.Get_Fields_String("SQL"));
					if (mnuEliminateDuplicates.Checked)
					{
						// get rid of select and add select distinct
						modCustomReport.Statics.strCustomSQL = Strings.Mid(modCustomReport.Statics.strCustomSQL, 7);
						modCustomReport.Statics.strCustomSQL = "Select distinct " + modCustomReport.Statics.strCustomSQL;
					}
				}
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			modCustomReport.GetNumberOfFields(modCustomReport.Statics.strCustomSQL);
			if (modCustomReport.Statics.strPreSetReport == string.Empty)
			{
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			NoFields:
			;
			if (executeNoFields)
			{
				executeNoFields = false;
				MessageBox.Show("No fields were selected to display.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			if (!boolSaveReport)
			{
				modRegistry.SaveRegistryKey("PYLastLabelType", FCConvert.ToString(cmbLabelType.ItemData(cmbLabelType.SelectedIndex)), "PY");
				// SHOW THE REPORT
				if (boolPrintPreview)
				{
					rptCustomLabels.InstancePtr.Init();
				}
				else
				{
					modDuplexPrinting.DuplexPrintReport(rptCustomLabels.InstancePtr);
					// rptCustomLabels.PrintReport
				}
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			vsWhere.Select(0, 0);
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			// THIS IS THE FOR LOOP TO GET THE FIELDS THAT WERE SELECTED.
			// FOR DISPLAY
			for (intRow = 1; intRow <= (vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (vsLayout.Cols - 1); intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							break;
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.Items[intRow].Text == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (modCustomReport.Statics.strPreSetReport == string.Empty)
			{
				if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
				{
					modCustomReport.Statics.intNumberOfSQLFields = -1;
					return;
				}
			}
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			if (mnuEliminateDuplicates.Checked)
			{
				modCustomReport.Statics.strCustomSQL = "Select distinct " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			}
			else
			{
				if (FCConvert.ToString(fraFields.Tag) == "EmployeeInformation")
				{
					// vbPorter upgrade warning: strTemp As object	OnWrite(string())
					string[] strTemp;
					strTemp = Strings.Split(modCustomReport.Statics.strCustomSQL, "BaseRate", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strTemp, 1) > 0)
					{
						modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + ",tblemployeeMaster.EmployeeNumber ,tblPayrollDistribution.RecordNumber FROM (tblEmployeeMaster LEFT JOIN tblPayrollDistribution ON tblEmployeeMaster.EmployeeNumber = tblPayrollDistribution.EmployeeNumber) INNER JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE tblEmployeeMaster.EmployeeNumber <>'' AND RecordNumber = 1";
					}
					else
					{
						modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + ",tblemployeemaster.employeenumber FROM tblEmployeeMaster INNER JOIN States ON (convert(int, isnull(tblEmployeeMaster.State, 0)) = States.ID) ";
					}
				}
				else
				{
					modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
				}
			}
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (fecherFoundation.Strings.Trim(strSort) != string.Empty)
			{
				modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strTemp = "";
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (modCustomReport.Statics.strFields[intCounter] == "LastName&','&FirstName+Space(1)&Desig AS FullName")
							{
							}
							else
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if (Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) != 0)
									{
										strWhere += fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
									}
									else
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
									}
								}
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOL WRAPPED AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " >= #" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "# and " + fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " <= #" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "#";
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = #" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "#";
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 3) + "*'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = " + vsWhere.TextMatrix(intCounter, 3);
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								if (intCounter != 23)
								{
									if (Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) != 0)
									{
										strWhere += fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "*'";
									}
									else
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "*'";
									}
								}
								else
								{
									if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(vsWhere.TextMatrix(intCounter, 1))) == "f" || fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(vsWhere.TextMatrix(intCounter, 1))) == "female")
									{
										strWhere += " Sex = 'Female' ";
									}
									else
									{
										strWhere += " Sex = 'Male' ";
									}
								}
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + modCustomReport.Statics.strFields[intCounter] + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							// If strWhere = Then
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if (Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) != 0)
									{
										strWhere += fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "zzz'";
									}
									else
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + modCustomReport.Statics.strFields[intCounter] + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "zzz'";
									}
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if (Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) != 0)
									{
										strWhere += fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
									}
									else
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
									}
								}
							}
							break;
						}
					case modCustomReport.GRIDBOOLEAN:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								strTemp = fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))));
								if (strWhere != " ")
									strWhere += " and ";
								if (Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) != 0)
								{
									strWhere += fecherFoundation.Strings.Trim(Strings.Left(modCustomReport.Statics.strFields[intCounter], Strings.InStr(1, modCustomReport.Statics.strFields[intCounter], " as ", CompareConstants.vbBinaryCompare) - 1)) + " = ";
								}
								else
								{
									strWhere += modCustomReport.Statics.strFields[intCounter] + " = ";
								}
								if (strTemp == "t" || strTemp == "true")
								{
									strWhere += " 1 ";
								}
								else
								{
									strWhere += " 0 ";
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (strWhere != " ")
			{
				modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
			}
		}
		// Private Sub Form_Activate()
		// Call ForceFormToResize(Me)
		// End Sub
		private void frmCustomLabels_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						// Call mnuAddRow_Click
						break;
					}
				case Keys.F3:
					{
						mnuAddColumn_Click();
						break;
					}
				case Keys.F4:
					{
						// Call mnuDeleteRow_Click
						break;
					}
				case Keys.F5:
					{
						mnuDeleteColumn_Click();
						break;
					}
				case Keys.Escape:
					{
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLabels properties;
			//frmCustomLabels.ScaleWidth	= 9045;
			//frmCustomLabels.ScaleHeight	= 7950;
			//frmCustomLabels.LinkTopic	= "Form1";
			//frmCustomLabels.LockControls	= -1  'True;
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			// vsElasticLight1.Enabled = True
			FillLabelTypeCombo();
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("tblCustomReports", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "ID", dbLong
			// .CreateTableField "ReportName", dbText
			// .CreateTableField "Type", dbText
			// .CreateTableField "LabelType", dbInteger
			// .CreateTableField "SQL", dbMemo
			// .CreateTableField "LastUpdated", dbDate
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "ID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "ReportName", True
			// .SetFieldAllowZeroLength "Type", True
			// .SetFieldAllowZeroLength "SQL", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// Else
			// append a field to a table
			// .UpdateDatabaseTable "tblCustomReports", DEFAULTDATABASE
			// .AddTableField "LabelType", dbInteger
			// End If
			// End With
			// 
			// With rsCreateTable
			// CREATE A NEW TABLE IF IT DOESN'T EXIST
			// If .CreateNewDatabaseTable("tblReportLayout", DEFAULTDATABASE) Then
			// CREATE THE NEW FIELDS
			// .CreateTableField "ID", dbLong
			// .CreateTableField "ReportID", dbInteger
			// .CreateTableField "RowID", dbInteger
			// .CreateTableField "ColumnID", dbInteger
			// .CreateTableField "FieldID", dbInteger
			// .CreateTableField "Width", dbInteger
			// .CreateTableField "DisplayText", dbText
			// .CreateTableField "LastUpdated", dbDate
			// 
			// SET THE PROPERTIES OF THE NEW FIELDS
			// .SetFieldAttribute "ID", dbAutoIncrField
			// .SetFieldDefaultValue "LastUpdated", "NOW()"
			// .SetFieldAllowZeroLength "DisplayText", True
			// 
			// DO THE ACTUAL CREATION OF THE TABLE
			// .UpdateTableCreation
			// End If
			// End With
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            // vsLayout.MergeRow(1) = True
            cmbType.SelectedIndex = 0;
            optType_Click(0);
			FillLabelTypeCombo();
			int intType = 0;
			string strType = "";
			int x;
			if (modCustomReport.Statics.strPreSetReport == string.Empty)
			{
				fraSort.Enabled = true;
				fraFields.Enabled = true;
				//fraNotes.Enabled = true;
				fraReports.Enabled = true;
				fraMessage.Visible = false;
				vsLayout.Visible = true;
				//fraNotes.Visible = true;
				strType = fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("PYLastLabelType", "PY", FCConvert.ToString(4))));
				intType = FCConvert.ToInt32(Math.Round(Conversion.Val(strType)));
				cmbLabelType.SelectedIndex = 4;
				// the 5161 labels
				if (strType != string.Empty)
				{
					if (intType >= 0)
					{
						for (x = 0; x <= cmbLabelType.Items.Count - 1; x++)
						{
							if (cmbLabelType.ItemData(x) == intType)
							{
								cmbLabelType.SelectedIndex = x;
							}
						}
						// x
					}
				}
			}
			else
			{
				fraSort.Enabled = false;
				fraFields.Enabled = false;
				//fraNotes.Enabled = false;
				fraReports.Visible = false;
				fraMessage.Visible = true;
				vsLayout.Visible = false;
				//fraNotes.Visible = false;
			}
			// 
			// fraNotes.Enabled = strPreSetReport = vbNullString
			// fraReports.Enabled = strPreSetReport = vbNullString
			// fraMessage.Visible = Not strPreSetReport = vbNullString
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modCustomReport.Statics.strPreSetReport = string.Empty;
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from tblCustomReports where Type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobalVariables.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			// Dim intCounter As Integer
			// For intCounter = 0 To lstFields.ListCount - 1
			// lstFields.Selected(intCounter) = True
			// Next
		}

		private void mnuEliminateDuplicates_Click(object sender, System.EventArgs e)
		{
			mnuEliminateDuplicates.Checked = !mnuEliminateDuplicates.Checked;
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
			}
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			int intCount = 0;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					intCount += 1;
				}
				lstSort.SetSelected(intCounter, true);
			}
			if (intCount == lstSort.Items.Count)
			{
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					lstSort.SetSelected(intCounter, false);
				}
			}
			if (lstSort.Items.Count > 0)
				lstSort.SelectedIndex = 0;
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.Items[lstFields.SelectedIndex].Text);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	bool blnTempSelected = false;
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart].Text = strTemp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		blnTempSelected = lstSort.Selected(lstSort.SelectedIndex);
		//		lstSort.SetSelected(lstSort.ListIndex, lstSort.Selected(intStart));
		//		lstSort.SetSelected(intStart, blnTempSelected);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Row < 0)
				vsLayout.Row = 0;
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		public void mnuAddColumn_Click()
		{
			mnuAddColumn_Click(mnuAddColumn, new System.EventArgs());
		}

		private void mnuAddRow_Click()
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			vsWhere.Select(0, 0);
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteRow_Click()
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = false;
			cmdPrint_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = true;
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Label") || (cmbReport.Text == "Delete Saved Label"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			fraSort.Enabled = Index == 0;
			fraFields.Enabled = Index == 0;
			fraWhere.Enabled = Index == 0;
			fraType.Enabled = Index == 0;
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// NUMBER OF ROWS IS HIGHT / 240 (HEIGHT OF ONE ROW) PLUS ONE FOR
			// THE HEADER ROW   1440 = 1 INCH
			// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
			//double dblSizeRatio;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// THESE DIMENTIONS ARE A BIT DIFFERENT THEN THE NUMBERS OUTLINED
			// IN MS WORD BECAUSE WE ARE USING A DIFFERENT FONT HERE THEN
			// THE MS WORD DEFAULT FONT.
			// 
			//dblSizeRatio = (Line2.X2 - Line2.X1) / 1440;
			// line2 is invisible and is 1440 wide before resizing
			// this lets us know how much we have been resized
			vsLayout.Clear();
			switch (Index)
			{
				case 0:
					{
						// Avery 5160 (3 X 10)  Height = 1" Width = 2.63"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(2.63F * 1440);
                        //Line1.X1 = FCConvert.ToSingle((2.63 * 1440) * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case 1:
					{
						// Avery 5161 (2 X 10)  Height = 1" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle((4 * 1440) * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case 2:
					{
						// Avery 5163 (2 X 5)  Height = 2" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 13;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle((4 * 1440) * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
				case 3:
					{
						// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
                        Line1.LeftOriginal = FCConvert.ToInt32(4F * 1440);
                        //Line1.X1 = FCConvert.ToSingle((4 * 1440) * dblSizeRatio);
                        //Line1.X2 = Line1.X1;
                        //Line1.Y1 = 0;
                        //Line1.Y2 = vsLayout.HeightOriginal;
                        break;
					}
			}
			//end switch
			for (intCounter = 0; intCounter <= (vsLayout.Rows - 1); intCounter++)
			{
				vsLayout.MergeRow(intCounter, true);
			}
		}

		public void optType_Click(int Index)
		{
			optType_CheckedChanged(Index, cmbType, new System.EventArgs());
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
				case modCustomReport.GRIDBOOLEAN:
					{
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (vsWhere.Col == 2)
			{
				if (FCConvert.ToInt32(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
				{
					vsWhere.Col = 1;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (fecherFoundation.Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!FCConvert.ToBoolean(modCustomReport.IsValidDate(vsWhere.EditText)))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}
	}
}
