﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptPayTotalsMisc.
	/// </summary>
	public partial class srptPayTotalsMisc : FCSectionReport
	{
		public srptPayTotalsMisc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptPayTotalsMisc InstancePtr
		{
			get
			{
				return (srptPayTotalsMisc)Sys.GetInstance(typeof(srptPayTotalsMisc));
			}
		}

		protected srptPayTotalsMisc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptPayTotalsMisc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strEmp;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strEmp = FCConvert.ToString(this.UserData);
			double dblTemp;
			double dblTemp2;
			txtCurFedTax.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurFicaTax.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurMedTax.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurStateTax.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurFedWage.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurFicaWage.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurMedWage.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurStateWage.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDFedTax.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDFicaTax.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDMedTax.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDStateTax.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDFedWage.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDFicaWage.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDMedWage.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDStateWage.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtQTDFedTax.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmp), "#,###,##0.00");
			txtQTDFicaTax.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmp), "#,###,##0.00");
			txtQTDMedTax.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmp), "#,###,##0.00");
			txtQTDStateTax.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmp), "#,###,##0.00");
			txtQTDFedWage.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmp), "#,###,##0.00");
			txtQTDFicaWage.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmp), "#,###,##0.00");
			txtQTDMedWage.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmp), "#,###,##0.00");
			txtQTDStateWage.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmp), "#,###,##0.00");
			txtFYTDFedTax.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDFicaTax.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDMedTax.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDStateTax.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDFedWage.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDFicaWage.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDMedWage.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDStateWage.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDFedTax.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDFicaTax.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDMedTax.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDStateTax.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDFedWage.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDFicaWage.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDMedWage.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDStateWage.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			//dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, FCConvert.ToDateTime("12:00:00 AM"));
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, DateTime.FromOADate(0));
			txtCurTotalGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			//dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, FCConvert.ToDateTime("12:00:00 AM"));
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, DateTime.FromOADate(0));
			txtMTDTotalGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp);
			txtQTDTotalGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			//dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, FCConvert.ToDateTime("12:00:00 AM"));
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, DateTime.FromOADate(0));
			txtFYTDTotalGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			//dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, FCConvert.ToDateTime("12:00:00 AM"));
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, strEmp, DateTime.FromOADate(0));
			txtCYTDTotalGross.Text = Strings.Format(dblTemp, "#,###,##0.00");
			txtCurEmployersMatch.Text = Strings.Format(modCoreysSweeterCode.GetCurrentMatchTotal(-1, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDEmployersMatch.Text = Strings.Format(modCoreysSweeterCode.GetMTDMatchTotal(-1, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtQTDEmployersMatch.Text = Strings.Format(modCoreysSweeterCode.GetQTDMatchTotal(-1, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtFYTDEmployersMatch.Text = Strings.Format(modCoreysSweeterCode.GetFYTDMatchTotal(-1, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDEmployersMatch.Text = Strings.Format(modCoreysSweeterCode.GetCYTDMatchTotal(-1, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurUnemployment.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDUnemployment.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtQTDUnemployment.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, strEmp), "#,###,##0.00");
			txtFYTDUnemployment.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDUnemployment.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEUNEMPLOYMENT, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCurMSRS.Text = Strings.Format(modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtMTDMSRS.Text = Strings.Format(modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");

			txtQTDMSRS.Text = Strings.Format(modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS, strEmp), "#,###,##0.00");
			txtFYTDMSRS.Text = Strings.Format(modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
			txtCYTDMSRS.Text = Strings.Format(modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMSRSGROSS, strEmp, DateTime.FromOADate(0)), "#,###,##0.00");
		}
	}
}
