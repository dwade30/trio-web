//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTaxStatusCodes.
	/// </summary>
	public partial class rptTaxStatusCodes : BaseSectionReport
	{
		public rptTaxStatusCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Tax Status Codes";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTaxStatusCodes InstancePtr
		{
			get
			{
				return (rptTaxStatusCodes)Sys.GetInstance(typeof(rptTaxStatusCodes));
			}
		}

		protected rptTaxStatusCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxStatusCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 26,2001
		//
		// NOTES: One visible problem with this code is that it references
		// the form frmTaxStatusCodes directly. If this report is called from
		// any other form then this one then the data will not display correctly.
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter == frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				txtDescription.Text = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
				txtTaxStatusCode.Text = Strings.Left(FCConvert.ToString(frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1);
				chkFed.Checked = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3);
				//chkFICA.Checked = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4);
				chkMed.Checked = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5);
				chkState.Checked = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6);
				chkLocal.Checked = frmTaxStatusCodes.InstancePtr.vsTaxStatusCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7);
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Tax Status Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (this.Document.Printer != null)
				{
					//this.Document.Printer.PrintQuality = ddPQMedium;
				}
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Payroll Tax Status Codes";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}