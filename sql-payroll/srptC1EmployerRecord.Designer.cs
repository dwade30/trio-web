﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1EmployerRecord.
	/// </summary>
	partial class srptC1EmployerRecord
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1EmployerRecord));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYearCovered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCompany = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFedID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUCAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWithholdingID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLastMonthinQuarter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalEmployees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearCovered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompany)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastMonthinQuarter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label7,
				this.Label8,
				this.txtYearCovered,
				this.txtCompany,
				this.txtAddress,
				this.txtCityStateZip,
				this.txtFedID,
				this.Label9,
				this.txtUCAccount,
				this.Label10,
				this.txtWithholdingID,
				this.Label11,
				this.txtLastMonthinQuarter,
				this.Label12,
				this.txtTotalEmployees
			});
			this.Detail.Height = 1.041667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: center";
			this.Label1.Text = "Employer Record";
			this.Label1.Top = 0.02083333F;
			this.Label1.Width = 3.916667F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Company";
			this.Label2.Top = 0.4479167F;
			this.Label2.Width = 1.03125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Address";
			this.Label3.Top = 0.6354167F;
			this.Label3.Width = 1.03125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "City, State Zip";
			this.Label4.Top = 0.8229167F;
			this.Label4.Width = 1.15625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Year Covered";
			this.Label7.Top = 0.2604167F;
			this.Label7.Width = 1.09375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.104167F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Federal ID";
			this.Label8.Top = 0.4479167F;
			this.Label8.Width = 1.03125F;
			// 
			// txtYearCovered
			// 
			this.txtYearCovered.Height = 0.1875F;
			this.txtYearCovered.Left = 1.260417F;
			this.txtYearCovered.Name = "txtYearCovered";
			this.txtYearCovered.Text = null;
			this.txtYearCovered.Top = 0.2604167F;
			this.txtYearCovered.Width = 0.9895833F;
			// 
			// txtCompany
			// 
			this.txtCompany.Height = 0.1875F;
			this.txtCompany.Left = 1.260417F;
			this.txtCompany.Name = "txtCompany";
			this.txtCompany.Text = null;
			this.txtCompany.Top = 0.4479167F;
			this.txtCompany.Width = 2.802083F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.260417F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0.6354167F;
			this.txtAddress.Width = 2.802083F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.Height = 0.1875F;
			this.txtCityStateZip.Left = 1.260417F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Text = null;
			this.txtCityStateZip.Top = 0.8229167F;
			this.txtCityStateZip.Width = 2.802083F;
			// 
			// txtFedID
			// 
			this.txtFedID.Height = 0.1875F;
			this.txtFedID.Left = 5.71875F;
			this.txtFedID.Name = "txtFedID";
			this.txtFedID.Text = null;
			this.txtFedID.Top = 0.4479167F;
			this.txtFedID.Width = 1.552083F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.104167F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "UC Account";
			this.Label9.Top = 0.6354167F;
			this.Label9.Width = 1.03125F;
			// 
			// txtUCAccount
			// 
			this.txtUCAccount.Height = 0.1875F;
			this.txtUCAccount.Left = 5.71875F;
			this.txtUCAccount.Name = "txtUCAccount";
			this.txtUCAccount.Text = null;
			this.txtUCAccount.Top = 0.6354167F;
			this.txtUCAccount.Width = 1.552083F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.104167F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "MRS Withholding ID";
			this.Label10.Top = 0.8229167F;
			this.Label10.Width = 1.59375F;
			// 
			// txtWithholdingID
			// 
			this.txtWithholdingID.Height = 0.1875F;
			this.txtWithholdingID.Left = 5.71875F;
			this.txtWithholdingID.Name = "txtWithholdingID";
			this.txtWithholdingID.Text = null;
			this.txtWithholdingID.Top = 0.8229167F;
			this.txtWithholdingID.Width = 1.552083F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "Last Month in Qtr";
			this.Label11.Top = 0.2604167F;
			this.Label11.Width = 1.34375F;
			// 
			// txtLastMonthinQuarter
			// 
			this.txtLastMonthinQuarter.Height = 0.1875F;
			this.txtLastMonthinQuarter.Left = 3.760417F;
			this.txtLastMonthinQuarter.Name = "txtLastMonthinQuarter";
			this.txtLastMonthinQuarter.Text = null;
			this.txtLastMonthinQuarter.Top = 0.2604167F;
			this.txtLastMonthinQuarter.Width = 0.3020833F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.104167F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "Total Employees";
			this.Label12.Top = 0.2604167F;
			this.Label12.Width = 1.59375F;
			// 
			// txtTotalEmployees
			// 
			this.txtTotalEmployees.Height = 0.1875F;
			this.txtTotalEmployees.Left = 5.71875F;
			this.txtTotalEmployees.Name = "txtTotalEmployees";
			this.txtTotalEmployees.Text = null;
			this.txtTotalEmployees.Top = 0.2604167F;
			this.txtTotalEmployees.Width = 1.552083F;
			// 
			// srptC1EmployerRecord
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearCovered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompany)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastMonthinQuarter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearCovered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompany;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLastMonthinQuarter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalEmployees;
	}
}
