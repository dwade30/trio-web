//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmLoadBack : BaseForm
	{
		public frmLoadBack()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmLoadBack InstancePtr
		{
			get
			{
				return (frmLoadBack)Sys.GetInstance(typeof(frmLoadBack));
			}
		}

		protected frmLoadBack _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLDESC = 0;
		const int CNSTGRIDCOLMTD = 1;
		const int CNSTGRIDCOLQTD = 2;
		const int CNSTGRIDCOLFTD = 3;
		const int CNSTGRIDCOLCTD = 4;
		const int CNSTGRIDCOLTYPE = 5;
		const int CNSTGRIDCOLCODE = 6;
		const int CNSTGRIDCOLCODETYPE = 7;
		const int CNSTGRIDCOLRECID = 8;
		const int CNSTTYPEPAY = 0;
		const int CNSTTYPETAX = 1;
		const int CNSTTYPEWAGE = 2;
		const int CNSTTYPEVACSICK = 3;
		const int CNSTTYPEDEDUCTION = 4;
		const int CNSTTYPEMATCH = 5;
		const int CNSTTYPEPAYCAT = 6;
		const int CNSTCODETYPEACCRUED = 0;
		const int CNSTCODETYPEUSED = 1;
		const int CNSTCODETYPEDEDUCTION = 0;
		const int CNSTCODETYPEMATCH = 1;
		const int CNSTCODETYPEFED = 0;
		const int CNSTCODETYPEFICA = 1;
		const int CNSTCODETYPEMEDICARE = 2;
		const int CNSTCODETYPESTATE = 3;
		private string strEmployeeNumber = string.Empty;
		private bool boolEditQTD;
		private bool boolEditCTD;
		private bool boolEditFTD;
		private int intFTDSameAs;
		// column that fiscal should equal
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		public void Init()
		{
			strEmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
			this.Show(App.MainForm);
		}

		private void frmLoadBack_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLoadBack_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLoadBack properties;
			//frmLoadBack.FillStyle	= 0;
			//frmLoadBack.ScaleWidth	= 9300;
			//frmLoadBack.ScaleHeight	= 8025;
			//frmLoadBack.LinkTopic	= "Form2";
			//frmLoadBack.LockControls	= -1  'True;
			//frmLoadBack.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			T2KPayDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			LoadTotals();
            //FC:FINAL:BSE:#4169 set column backcolor
            Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            // Dave 12/14/2006---------------------------------------------------
            // This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
            FillControlInformationClass();
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
		}

		private void frmLoadBack_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			UpdateValues(Grid.Row);
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			int lngCol;
			lngCol = Grid.Col;
			switch (lngCol)
			{
				case CNSTGRIDCOLMTD:
					{
						Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case CNSTGRIDCOLQTD:
					{
						if (boolEditQTD)
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case CNSTGRIDCOLCTD:
					{
						if (boolEditCTD)
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbd;
						}
						break;
					}
				case CNSTGRIDCOLFTD:
					{
						if (boolEditFTD)
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							Grid.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.41 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMTD, FCConvert.ToInt32(0.14 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLQTD, FCConvert.ToInt32(0.14 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFTD, FCConvert.ToInt32(0.14 * GridWidth));
		}

		private void SetupGrid()
		{
			int lngRow = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.Cols = 9;
				Grid.Rows = 1;
				Grid.ColHidden(CNSTGRIDCOLCODE, true);
				Grid.ColHidden(CNSTGRIDCOLTYPE, true);
				Grid.ColHidden(CNSTGRIDCOLCODETYPE, true);
				Grid.ColHidden(CNSTGRIDCOLRECID, true);
				Grid.TextMatrix(0, CNSTGRIDCOLMTD, "MTD");
				Grid.TextMatrix(0, CNSTGRIDCOLQTD, "QTD");
				Grid.TextMatrix(0, CNSTGRIDCOLFTD, "FYTD");
				Grid.TextMatrix(0, CNSTGRIDCOLCTD, "CYTD");
                Grid.ColAlignment(CNSTGRIDCOLMTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.ColAlignment(CNSTGRIDCOLQTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.ColAlignment(CNSTGRIDCOLFTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.ColAlignment(CNSTGRIDCOLCTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
                Grid.Rows = 2;
				Grid.TextMatrix(1, CNSTGRIDCOLDESC, "Gross Pay");
				Grid.TextMatrix(1, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEPAY));
				Grid.RowData(1, false);
				Grid.Select(1, 0, lngRow, Grid.Cols - 1);
				Grid.CellBorder(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK), -1, -1, -1, 2, -1, -1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Federal Tax");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPETAX));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEFED));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Fica Tax");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPETAX));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEFICA));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Medicare Tax");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPETAX));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEMEDICARE));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "State Tax");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPETAX));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPESTATE));
				Grid.Select(lngRow, 0, lngRow, Grid.Cols - 1);
				Grid.CellBorder(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK), -1, -1, -1, 2, -1, -1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Federal Wage");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEWAGE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEFED));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Fica Wage");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEWAGE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEFICA));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "Medicare Wage");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEWAGE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEMEDICARE));
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "State Wage");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEWAGE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPESTATE));
				Grid.Select(lngRow, 0, lngRow, Grid.Cols - 1);
				Grid.CellBorder(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK), -1, -1, -1, 2, -1, -1);
				// deductions
				rsLoad.OpenRecordset("select * from tblDeductionSetup order by deductionnumber", "twpy0000.vb1");
				rsTemp.OpenRecordset("select * from tblemployeedeductions where employeenumber = '" + strEmployeeNumber + "' order by RECORDNUMBER", "twpy0000.vb1");
				while (!rsTemp.EndOfFile())
				{
					//App.DoEvents();
					if (rsLoad.FindFirstRecord("ID", rsTemp.Get_Fields("deductioncode")))
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.RowData(lngRow, false);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, rsLoad.Get_Fields("deductionnumber") + " " + rsLoad.Get_Fields_String("description"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEDEDUCTION));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(rsLoad.Get_Fields("ID")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEDEDUCTION));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID, FCConvert.ToString(rsTemp.Get_Fields("ID")));
					}
					rsTemp.MoveNext();
				}
				Grid.Select(lngRow, 0, lngRow, Grid.Cols - 1);
				Grid.CellBorder(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK), -1, -1, -1, 2, -1, -1);
                rsLoad.OpenRecordset("select * from tblDeductionSetup order by deductionnumber", "twpy0000.vb1");
				rsTemp.OpenRecordset("select * from tblemployersmatch where employeenumber = '" + strEmployeeNumber + "' order by recordnumber", "twpy0000.vb1");
				while (!rsTemp.EndOfFile())
				{
					//App.DoEvents();
					if (rsLoad.FindFirstRecord("ID", rsTemp.Get_Fields("deductioncode")))
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.RowData(lngRow, false);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, rsLoad.Get_Fields("deductionnumber") + " " + rsLoad.Get_Fields_String("description") + " Match");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(CNSTTYPEMATCH));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(rsLoad.Get_Fields("ID")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(CNSTCODETYPEMATCH));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID, FCConvert.ToString(rsTemp.Get_Fields("ID")));
					}
					rsTemp.MoveNext();
				}

				Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLMTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLQTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLFTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLCTD, FCGrid.AlignmentSettings.flexAlignRightCenter);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuReload_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//App.DoEvents();
			if (!Information.IsDate(T2KPayDate.Text))
			{
				MessageBox.Show("You must enter a valid pay date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (strEmployeeNumber == string.Empty)
			{
				MessageBox.Show("You must select an employee first", "No Employee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			LoadTotals();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
			frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			strEmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
			SetupGrid();
			LoadTotals();
            //FC:FINAL:BSE:#4169 set column backcolor
            Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            // Dave 12/14/2006---------------------------------------------------
            // This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
            FillControlInformationClass();
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			int counter;
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
		}

		private void T2KPayDate_Change(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string)
			DateTime dtTemp;
			if (Information.IsDate(T2KPayDate.Text))
			{
				dtTemp = FCConvert.ToDateTime(T2KPayDate.Text);
				lblFiscalStart.Text = Strings.Format(modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtTemp), "MM/dd/yyyy");
				lblFiscalEnd.Text = Strings.Format(modCoreysSweeterCode.GetLastFiscalDay(dtTemp), "MM/dd/yyyy");
				CheckDateRelationship();
			}
		}

		private void LoadTotals()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			DateTime dtFiscalStart;
			// vbPorter upgrade warning: dtCalendarStart As DateTime	OnWrite(string)
			DateTime dtCalendarStart;
			DateTime dtQuarterStart = DateTime.FromOADate(0);
			// vbPorter upgrade warning: dtMonthStart As DateTime	OnWrite(string)
			DateTime dtMonthStart;
			// vbPorter upgrade warning: dtPayDate As DateTime	OnWrite(string)
			DateTime dtPayDate;
			DateTime dtTemp = DateTime.FromOADate(0);
			double dblQTD = 0;
			double dblMTD = 0;
			double dblCTD = 0;
			double dblFTD = 0;
			double dblCurrent;
			string strSQL;
			int lngTempID = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lblEmployeeName.Text = "";
				lblEmployeeNumber.Text = "";
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber != string.Empty)
				{
					lblEmployeeName.Text = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
					lblEmployeeNumber.Text = strEmployeeNumber;
				}
				else
				{
					return;
				}
				if (!Information.IsDate(T2KPayDate.Text))
				{
					MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				dtPayDate = FCConvert.ToDateTime(T2KPayDate.Text);
				dtMonthStart = FCConvert.ToDateTime(FCConvert.ToString(dtPayDate.Month) + "/1/" + FCConvert.ToString(dtPayDate.Year));
				dtFiscalStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtPayDate);
				modGlobalRoutines.GetDatesForAQuarter(ref dtQuarterStart, ref dtTemp, dtPayDate);
				dtCalendarStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
				strSQL = "select sum(FederalTaxWh) as fedtax,sum(StateTaxWh) as statetax,sum(medicaretaxwh) as medicaretax,sum(ficataxwh) as ficatax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(2, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtax"))), "0.00"));
					Grid.TextMatrix(3, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatax"))), "0.00"));
					Grid.TextMatrix(5, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetax"))), "0.00"));
					Grid.TextMatrix(4, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretax"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(2, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(3, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(4, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(5, CNSTGRIDCOLMTD, "0.00");
				}
				strSQL = "select sum(FederalTaxWh) as fedtax,sum(StateTaxWh) as statetax,sum(medicaretaxwh) as medicaretax,sum(ficataxwh) as ficatax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(2, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtax"))), "0.00"));
					Grid.TextMatrix(3, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatax"))), "0.00"));
					Grid.TextMatrix(5, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetax"))), "0.00"));
					Grid.TextMatrix(4, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretax"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(2, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(3, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(4, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(5, CNSTGRIDCOLQTD, "0.00");
				}
				strSQL = "select sum(FederalTaxWh) as fedtax,sum(StateTaxWh) as statetax,sum(medicaretaxwh) as medicaretax,sum(ficataxwh) as ficatax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(2, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtax"))), "0.00"));
					Grid.TextMatrix(3, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatax"))), "0.00"));
					Grid.TextMatrix(5, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetax"))), "0.00"));
					Grid.TextMatrix(4, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretax"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(2, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(3, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(4, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(5, CNSTGRIDCOLCTD, "0.00");
				}
				strSQL = "select sum(FederalTaxWh) as fedtax,sum(StateTaxWh) as statetax,sum(medicaretaxwh) as medicaretax,sum(ficataxwh) as ficatax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(2, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtax"))), "0.00"));
					Grid.TextMatrix(3, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatax"))), "0.00"));
					Grid.TextMatrix(5, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetax"))), "0.00"));
					Grid.TextMatrix(4, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretax"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(2, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(3, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(4, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(5, CNSTGRIDCOLFTD, "0.00");
				}
				strSQL = "select sum(FederalTaxgross) as fedtot,sum(StateTaxgross) as statetot,sum(medicaretaxgross) as medicaretot,sum(ficataxgross) as ficatot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(6, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					Grid.TextMatrix(7, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					Grid.TextMatrix(9, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
					Grid.TextMatrix(8, CNSTGRIDCOLMTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretot"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(6, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(7, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(8, CNSTGRIDCOLMTD, "0.00");
					Grid.TextMatrix(9, CNSTGRIDCOLMTD, "0.00");
				}
				strSQL = "select sum(FederalTaxgross) as fedtot,sum(StateTaxgross) as statetot,sum(medicaretaxgross) as medicaretot,sum(ficataxgross) as ficatot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(6, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					Grid.TextMatrix(7, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					Grid.TextMatrix(9, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
					Grid.TextMatrix(8, CNSTGRIDCOLQTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretot"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(6, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(7, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(8, CNSTGRIDCOLQTD, "0.00");
					Grid.TextMatrix(9, CNSTGRIDCOLQTD, "0.00");
				}
				strSQL = "select sum(FederalTaxgross) as fedtot,sum(StateTaxgross) as statetot,sum(medicaretaxgross) as medicaretot,sum(ficataxgross) as ficatot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(6, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					Grid.TextMatrix(7, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					Grid.TextMatrix(9, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
					Grid.TextMatrix(8, CNSTGRIDCOLCTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretot"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(6, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(7, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(8, CNSTGRIDCOLCTD, "0.00");
					Grid.TextMatrix(9, CNSTGRIDCOLCTD, "0.00");
				}
				strSQL = "select sum(FederalTaxgross) as fedtot,sum(StateTaxgross) as statetot,sum(medicaretaxgross) as medicaretot,sum(ficataxgross) as ficatot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(6, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					Grid.TextMatrix(7, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					Grid.TextMatrix(9, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
					Grid.TextMatrix(8, CNSTGRIDCOLFTD, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medicaretot"))), "0.00"));
				}
				else
				{
					Grid.TextMatrix(6, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(7, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(8, CNSTGRIDCOLFTD, "0.00");
					Grid.TextMatrix(9, CNSTGRIDCOLFTD, "0.00");
				}
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEDEDUCTION)
					{
						lngTempID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
						// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtMonthStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
						// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
						// If Not rsLoad.EndOfFile Then
						// dblMTD = Val(rsLoad.Fields("tot"))
						// Else
						// dblMTD = 0
						// End If
						dblMTD = modCoreysSweeterCode.GetMTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtQuarterStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
						// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
						// If Not rsLoad.EndOfFile Then
						// dblQTD = Val(rsLoad.Fields("tot"))
						// Else
						// dblQTD = 0
						// End If
						dblQTD = modCoreysSweeterCode.GetQTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtCalendarStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
						// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
						// If Not rsLoad.EndOfFile Then
						// dblCTD = Val(rsLoad.Fields("tot"))
						// Else
						// dblCTD = 0
						// End If
						dblCTD = modCoreysSweeterCode.GetCYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
						// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
						// If Not rsLoad.EndOfFile Then
						// dblFTD = Val(rsLoad.Fields("tot"))
						// Else
						// dblFTD = 0
						// End If
						dblFTD = modCoreysSweeterCode.GetFYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD, Strings.Format(dblMTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD, Strings.Format(dblQTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD, Strings.Format(dblCTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD, Strings.Format(dblFTD, "0.00"));
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEMATCH)
					{
						lngTempID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
						dblMTD = modCoreysSweeterCode.GetMTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						dblQTD = modCoreysSweeterCode.GetQTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						dblCTD = modCoreysSweeterCode.GetCYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " + FCConvert.ToString(lngTempID) + " and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and matchrecord = 1";
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblFTD = 0;
						}
						dblFTD = modCoreysSweeterCode.GetFYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD, Strings.Format(dblMTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD, Strings.Format(dblQTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD, Strings.Format(dblCTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD, Strings.Format(dblFTD, "0.00"));
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEPAY)
					{
						strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblMTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblMTD = 0;
						}
						strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblQTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblQTD = 0;
						}
						strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblCTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblCTD = 0;
						}
						strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblFTD = 0;
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD, Strings.Format(dblMTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD, Strings.Format(dblQTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD, Strings.Format(dblCTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD, Strings.Format(dblFTD, "0.00"));
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEPAYCAT)
					{
						lngTempID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
						strSQL = "select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and distributionrecord = 1 and distpaycategory = " + FCConvert.ToString(lngTempID);
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblMTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblMTD = 0;
						}
						strSQL = "select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and distributionrecord = 1 and distpaycategory = " + FCConvert.ToString(lngTempID);
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblQTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblQTD = 0;
						}
						strSQL = "select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and distributionrecord = 1 and distpaycategory = " + FCConvert.ToString(lngTempID);
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblCTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblCTD = 0;
						}
						strSQL = "select sum(distgrosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and distributionrecord = 1 and distpaycategory = " + FCConvert.ToString(lngTempID);
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
						}
						else
						{
							dblFTD = 0;
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD, Strings.Format(dblMTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD, Strings.Format(dblQTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD, Strings.Format(dblCTD, "0.00"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD, Strings.Format(dblFTD, "0.00"));
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPETAX)
					{
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEWAGE)
					{
					}
				}
				// lngRow
				CheckDateRelationship();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadTotals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			int lngRow;
			DateTime dtFiscalStart;
			// vbPorter upgrade warning: dtCalendarStart As DateTime	OnWrite(string)
			DateTime dtCalendarStart;
			DateTime dtQuarterStart = DateTime.FromOADate(0);
			// vbPorter upgrade warning: dtMonthStart As DateTime	OnWrite(string)
			DateTime dtMonthStart;
			// vbPorter upgrade warning: dtPayDate As DateTime	OnWrite(string)
			DateTime dtPayDate;
			DateTime dtTemp = DateTime.FromOADate(0);
			double dblQTD = 0;
			double dblMTD = 0;
			double dblCTD = 0;
			double dblFTD = 0;
			double dblCurrent;
			string strSQL;
			int lngTempID = 0;
			string strFieldName = "";
			double dblNetMTD;
			double dblNetQTD;
			double dblNetCTD;
			double dblNetFTD;
			double dblTemp;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				Grid.Row = 0;
				//App.DoEvents();
				dblNetMTD = 0;
				dblNetQTD = 0;
				dblNetCTD = 0;
				dblNetFTD = 0;
				if (!Information.IsDate(T2KPayDate.Text))
				{
					MessageBox.Show("Invalid Pay Date", "Invalid Pay Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				if (strEmployeeNumber == string.Empty)
				{
					MessageBox.Show("You must select an employee first", "No Employee Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Load of Back Information", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				modGlobalFunctions.AddCYAEntry_6("PY", "Saved Load Back", "Employee " + strEmployeeNumber, "As of " + T2KPayDate.Text);
				dtPayDate = FCConvert.ToDateTime(T2KPayDate.Text);
				dtMonthStart = FCConvert.ToDateTime(FCConvert.ToString(dtPayDate.Month) + "/1/" + FCConvert.ToString(dtPayDate.Year));
				dtFiscalStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtPayDate);
				modGlobalRoutines.GetDatesForAQuarter(ref dtQuarterStart, ref dtTemp, dtPayDate);
				dtCalendarStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEPAY)
					{
						dblNetMTD += Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD));
						dblNetQTD += Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD));
						dblNetCTD += Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD));
						dblNetFTD += Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD));
					}
					else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEDEDUCTION
						|| Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPETAX)
					{
						dblNetMTD -= Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD));
						dblNetQTD -= Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD));
						dblNetCTD -= Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD));
						dblNetFTD -= Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD));
					}
				}
				// lngRow
				rsSave.OpenRecordset("SELECT * from tblcheckdetail where ID = 0", "twpy0000.vb1");
				strSQL = "select sum(NETPay) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					dblMTD = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				else
				{
					dblMTD = 0;
				}
				if (dblMTD != dblNetMTD)
				{
					rsSave.AddNew();
					MakeDetailRecord(ref rsSave);
					rsSave.Set_Fields("adjustrecord", true);
					rsSave.Set_Fields("paydate", dtMonthStart);
					rsSave.Set_Fields("employeenumber", strEmployeeNumber);
					rsSave.Set_Fields("netPay", dblNetMTD - dblMTD);
					rsSave.Set_Fields("TotalRecord", true);
					rsSave.Update();
				}
				if (fecherFoundation.DateAndTime.DateDiff("d", dtQuarterStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
				{
					strSQL = "select sum(netpay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
					rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsLoad.EndOfFile())
					{
						dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						dblFTD = 0;
					}
					if (dblFTD != dblNetFTD)
					{
						rsSave.AddNew();
						MakeDetailRecord(ref rsSave);
						rsSave.Set_Fields("adjustrecord", true);
						rsSave.Set_Fields("paydate", dtFiscalStart);
						rsSave.Set_Fields("employeenumber", strEmployeeNumber);
						rsSave.Set_Fields("netpay", dblNetFTD - dblFTD);
						rsSave.Set_Fields("totalrecord", true);
						rsSave.Update();
					}
				}
				strSQL = "select sum(netpay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					dblQTD = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				else
				{
					dblQTD = 0;
				}
				if (dblQTD != dblNetQTD)
				{
					rsSave.AddNew();
					MakeDetailRecord(ref rsSave);
					rsSave.Set_Fields("adjustrecord", true);
					rsSave.Set_Fields("paydate", dtQuarterStart);
					rsSave.Set_Fields("employeenumber", strEmployeeNumber);
					rsSave.Set_Fields("netpay", dblNetQTD - dblQTD);
					rsSave.Set_Fields("totalrecord", true);
					rsSave.Update();
				}
				if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
				{
					strSQL = "select sum(netpay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
					rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsLoad.EndOfFile())
					{
						dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						dblFTD = 0;
					}
					if (dblFTD != dblNetFTD)
					{
						rsSave.AddNew();
						MakeDetailRecord(ref rsSave);
						rsSave.Set_Fields("adjustrecord", true);
						rsSave.Set_Fields("paydate", dtFiscalStart);
						rsSave.Set_Fields("employeenumber", strEmployeeNumber);
						rsSave.Set_Fields("netpay", dblNetFTD - dblFTD);
						rsSave.Set_Fields("totalrecord", true);
						rsSave.Update();
					}
				}
				strSQL = "select sum(netpay) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					dblCTD = Conversion.Val(rsLoad.Get_Fields("tot"));
				}
				else
				{
					dblCTD = 0;
				}
				if (dblCTD != dblNetCTD)
				{
					rsSave.AddNew();
					MakeDetailRecord(ref rsSave);
					rsSave.Set_Fields("adjustrecord", true);
					rsSave.Set_Fields("paydate", dtCalendarStart);
					rsSave.Set_Fields("employeenumber", strEmployeeNumber);
					rsSave.Set_Fields("netpay", dblNetCTD - dblCTD);
					rsSave.Set_Fields("totalrecord", true);
					rsSave.Update();
				}
				if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
				{
					strSQL = "select sum(netpay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
					rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsLoad.EndOfFile())
					{
						dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
					}
					else
					{
						dblFTD = 0;
					}
					if (dblFTD != dblNetFTD)
					{
						rsSave.AddNew();
						MakeDetailRecord(ref rsSave);
						rsSave.Set_Fields("adjustrecord", true);
						rsSave.Set_Fields("paydate", dtFiscalStart);
						rsSave.Set_Fields("employeenumber", strEmployeeNumber);
						rsSave.Set_Fields("netpay", dblNetFTD - dblFTD);
						rsSave.Set_Fields("totalrecord", true);
						rsSave.Update();
					}
				}
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (FCConvert.CBool(Grid.RowData(lngRow)) || Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEPAY)
					{
						dblMTD = Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD));
						dblQTD = Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD));
						dblCTD = Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD));
						dblFTD = Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD));
						if (dblMTD > dblQTD)
						{
							MessageBox.Show("Month to date value is greater than the quarter to date value for " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
						if (dblQTD > dblCTD)
						{
							MessageBox.Show("Quarter to date value is greater than the calendar year to date value for " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
						if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
						{
							if (dblMTD > dblFTD)
							{
								MessageBox.Show("Month to date value is greater than the fiscal year to date value for " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveData;
							}
						}
						if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
						{
							if (dblQTD > dblFTD)
							{
								MessageBox.Show("Quarter to date value is greater than the fiscal year to date value for " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveData;
							}
						}
						if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
						{
							if (dblCTD > dblFTD)
							{
								MessageBox.Show("Calendar year to date is greater than the fiscal year to date value for " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveData;
							}
						}
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEDEDUCTION)
						{
							lngTempID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
							// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtMonthStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblMTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblMTD = 0
							// End If
							dblMTD = modCoreysSweeterCode.GetMTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblMTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtMonthStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblMTD);
								rsSave.Set_Fields("deductionrecord", true);
								rsSave.Set_Fields("deddeductionnumber", lngTempID);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtQuarterStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
							{
								// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("deductionrecord", true);
									rsSave.Set_Fields("deddeductionnumber", lngTempID);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Update();
								}
							}
							// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtQuarterStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblQTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblQTD = 0
							// End If
							dblQTD = modCoreysSweeterCode.GetQTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblQTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtQuarterStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)) - dblQTD);
								rsSave.Set_Fields("deductionrecord", true);
								rsSave.Set_Fields("deddeductionnumber", lngTempID);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
							{
								// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("deductionrecord", true);
									rsSave.Set_Fields("deddeductionnumber", lngTempID);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Update();
								}
							}
							// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtCalendarStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblCTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblCTD = 0
							// End If
							dblCTD = modCoreysSweeterCode.GetCYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblCTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtCalendarStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)) - dblCTD);
								rsSave.Set_Fields("deductionrecord", true);
								rsSave.Set_Fields("deddeductionnumber", lngTempID);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
							{
								// strSQL = "select sum(dedamount) as tot from tblcheckdetail where deddeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and deductionrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDDeductionTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("dedamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("deductionrecord", true);
									rsSave.Set_Fields("deddeductionnumber", lngTempID);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Update();
								}
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEMATCH)
						{
							lngTempID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
							// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtMonthStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblMTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblMTD = 0
							// End If
							dblMTD = modCoreysSweeterCode.GetMTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblMTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtMonthStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblMTD);
								rsSave.Set_Fields("matchrecord", true);
								rsSave.Set_Fields("matchdeductionnumber", lngTempID);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtQuarterStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
							{
								// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("matchrecord", true);
									rsSave.Set_Fields("matchdeductionnumber", lngTempID);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Update();
								}
							}
							// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtQuarterStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblQTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblQTD = 0
							// End If
							dblQTD = modCoreysSweeterCode.GetQTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblQTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtQuarterStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)) - dblQTD);
								rsSave.Set_Fields("matchdeductionnumber", lngTempID);
								rsSave.Set_Fields("matchrecord", true);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
							{
								// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("matchrecord", true);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Set_Fields("matchdeductionnumber", lngTempID);
									rsSave.Update();
								}
							}
							// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtCalendarStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
							// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
							// If Not rsLoad.EndOfFile Then
							// dblCTD = Val(rsLoad.Fields("tot"))
							// Else
							// dblCTD = 0
							// End If
							dblCTD = modCoreysSweeterCode.GetCYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
							if (dblCTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtCalendarStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)) - dblCTD);
								rsSave.Set_Fields("matchrecord", true);
								rsSave.Set_Fields("matchdeductionnumber", lngTempID);
								rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
							{
								// strSQL = "select sum(matchamount) as tot from tblcheckdetail where matchdeductionnumber = " & lngTempID & " and checkvoid = 0 and paydate between '" & dtFiscalStart & "' and '" & dtPayDate & "' and employeenumber = '" & strEmployeeNumber & "' and matchrecord"
								// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
								// If Not rsLoad.EndOfFile Then
								// dblFTD = Val(rsLoad.Fields("tot"))
								// Else
								// dblFTD = 0
								// End If
								dblFTD = modCoreysSweeterCode.GetFYTDMatchTotal(lngTempID, strEmployeeNumber, dtPayDate, FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("matchamount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("matchrecord", true);
									rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLRECID))));
									rsSave.Set_Fields("matchdeductionnumber", lngTempID);
									rsSave.Update();
								}
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEPAY)
						{
							strSQL = "select sum(GrossPay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblMTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblMTD = 0;
							}
							if (dblMTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtMonthStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("GrossPay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblMTD);
								rsSave.Set_Fields("TotalRecord", true);
								rsSave.Update();
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtMonthStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("distGrossPay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblMTD);
								rsSave.Set_Fields("distributionrecord", true);
								rsSave.Set_Fields("distpaycategory", 1);
								// regular
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtQuarterStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
							{
								strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("grosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("distgrosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("DISTRIBUTIONrecord", true);
									rsSave.Set_Fields("distpaycategory", 1);
									rsSave.Update();
								}
							}
							strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblQTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblQTD = 0;
							}
							if (dblQTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtQuarterStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("grosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)) - dblQTD);
								rsSave.Set_Fields("totalrecord", true);
								rsSave.Update();
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtQuarterStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("distgrosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)) - dblQTD);
								rsSave.Set_Fields("DISTRIBUTIONrecord", true);
								rsSave.Set_Fields("distpaycategory", 1);
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
							{
								strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("grosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("distgrosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("DISTRIBUTIONrecord", true);
									rsSave.Set_Fields("distpaycategory", 1);
									rsSave.Update();
								}
							}
							strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblCTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblCTD = 0;
							}
							if (dblCTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtCalendarStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("grosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)) - dblCTD);
								rsSave.Set_Fields("totalrecord", true);
								rsSave.Update();
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtCalendarStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("distgrosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)) - dblCTD);
								rsSave.Set_Fields("DISTRIBUTIONrecord", true);
								rsSave.Set_Fields("distpaycategory", 1);
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
							{
								strSQL = "select sum(grosspay) as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("grosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields("distgrosspay", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("DISTRIBUTIONrecord", true);
									rsSave.Set_Fields("distpaycategory", 1);
									rsSave.Update();
								}
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPETAX
							|| Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPEWAGE)
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == CNSTTYPETAX)
							{
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEFED)
								{
									strFieldName = "FederalTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEFICA)
								{
									strFieldName = "FicaTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEMEDICARE)
								{
									strFieldName = "MedicareTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPESTATE)
								{
									strFieldName = "StateTax";
								}
								strFieldName += "WH";
							}
							else
							{
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEFED)
								{
									strFieldName = "FederalTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEFICA)
								{
									strFieldName = "FicaTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPEMEDICARE)
								{
									strFieldName = "MedicareTax";
								}
								else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == CNSTCODETYPESTATE)
								{
									strFieldName = "StateTax";
								}
								strFieldName += "Gross";
							}
							strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where isnull(checkvoid,0) = 0 and paydate between '" + FCConvert.ToString(dtMonthStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblMTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblMTD = 0;
							}
							if (dblMTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtMonthStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblMTD);
								rsSave.Set_Fields("TotalRecord", true);
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtQuarterStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtMonthStart) > 0)
							{
								strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
								}
							}
							strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtQuarterStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblQTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblQTD = 0;
							}
							if (dblQTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtQuarterStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD)) - dblQTD);
								rsSave.Set_Fields("totalrecord", true);
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtFiscalStart) > 0 && fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) > 0)
							{
								strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
								}
							}
							strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtCalendarStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
							rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
							if (!rsLoad.EndOfFile())
							{
								dblCTD = Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								dblCTD = 0;
							}
							if (dblCTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)))
							{
								rsSave.AddNew();
								MakeDetailRecord(ref rsSave);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("paydate", dtCalendarStart);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD)) - dblCTD);
								rsSave.Set_Fields("totalrecord", true);
								rsSave.Update();
							}
							if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) > 0)
							{
								strSQL = "select sum(" + strFieldName + ") as tot from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtFiscalStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and totalrecord = 1";
								rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									dblFTD = Conversion.Val(rsLoad.Get_Fields("tot"));
								}
								else
								{
									dblFTD = 0;
								}
								if (dblFTD != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)))
								{
									rsSave.AddNew();
									MakeDetailRecord(ref rsSave);
									rsSave.Set_Fields("adjustrecord", true);
									rsSave.Set_Fields("paydate", dtFiscalStart);
									rsSave.Set_Fields("employeenumber", strEmployeeNumber);
									rsSave.Set_Fields(strFieldName, Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD)) - dblFTD);
									rsSave.Set_Fields("totalrecord", true);
									rsSave.Update();
								}
							}
						}
					}
					Grid.RowData(lngRow, false);
				}
				// lngRow
				SaveData = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void MakeDetailRecord(ref clsDRWrapper rsSave)
		{
			rsSave.Set_Fields("checknumber", 0);
			rsSave.Set_Fields("journalnumber", 0);
			rsSave.Set_Fields("warrantnumber", 0);
			rsSave.Set_Fields("accountingperiod", 0);
			rsSave.Set_Fields("federaltaxwh", 0);
			rsSave.Set_Fields("statetaxwh", 0);
			rsSave.Set_Fields("ficataxwh", 0);
			rsSave.Set_Fields("medicaretaxwh", 0);
			rsSave.Set_Fields("federaltaxgross", 0);
			rsSave.Set_Fields("statetaxgross", 0);
			rsSave.Set_Fields("ficataxgross", 0);
			rsSave.Set_Fields("medicaretaxgross", 0);
			rsSave.Set_Fields("grosspay", 0);
			rsSave.Set_Fields("distlinecode", "");
			rsSave.Set_Fields("distaccountnumber", "");
			rsSave.Set_Fields("distpaycategory", 0);
			rsSave.Set_Fields("disttaxcode", 0);
			rsSave.Set_Fields("distm", "");
			rsSave.Set_Fields("diste", 0);
			rsSave.Set_Fields("distw", "");
			rsSave.Set_Fields("distwccode", "");
			rsSave.Set_Fields("distpaycode", "");
			rsSave.Set_Fields("distpayrate", 0);
			rsSave.Set_Fields("distnumtaxperiods", 0);
			rsSave.Set_Fields("disthours", 0);
			rsSave.Set_Fields("distgrosspay", 0);
			rsSave.Set_Fields("disthoursdefault", 0);
			rsSave.Set_Fields("deddeductionnumber", 0);
			rsSave.Set_Fields("dedtaxcode", 0);
			rsSave.Set_Fields("dedamount", 0);
			rsSave.Set_Fields("dedhitlimit", 0);
			rsSave.Set_Fields("dedreduced", 0);
			rsSave.Set_Fields("matchdeductionnumber", 0);
			rsSave.Set_Fields("matchtaxcode", 0);
			rsSave.Set_Fields("matchamount", 0);
			rsSave.Set_Fields("matchhitmax", 0);
			rsSave.Set_Fields("matchaccountnumber", "");
			rsSave.Set_Fields("ddbanknumber", 0);
			rsSave.Set_Fields("ddaccountnumber", "");
			rsSave.Set_Fields("ddamount", 0);
			rsSave.Set_Fields("numberchecks", 0);
			rsSave.Set_Fields("numberdirectdeposits", 0);
			rsSave.Set_Fields("numberboth", 0);
			rsSave.Set_Fields("vsaccrued", 0);
			rsSave.Set_Fields("vsused", 0);
			rsSave.Set_Fields("vsrecord", false);
			rsSave.Set_Fields("masterrecord", "");
			rsSave.Set_Fields("deductionrecord", false);
			rsSave.Set_Fields("matchrecord", false);
			rsSave.Set_Fields("bankrecord", false);
			rsSave.Set_Fields("distributionrecord", false);
			rsSave.Set_Fields("trustrecord", false);
			rsSave.Set_Fields("trustrecipientid", 0);
			rsSave.Set_Fields("trustamount", 0);
			rsSave.Set_Fields("trustcategoryid", 0);
			rsSave.Set_Fields("payrunid", 0);
			rsSave.Set_Fields("checkvoid", false);
			rsSave.Set_Fields("basepayrate", 0);
			rsSave.Set_Fields("MSRSAdjustRecord", false);
			rsSave.Set_Fields("unempadjustrecord", false);
			rsSave.Set_Fields("taadjustrecord", false);
			rsSave.Set_Fields("distautoid", 0);
			rsSave.Set_Fields("sequencenumber", 0);
			rsSave.Set_Fields("netpay", 0);
			rsSave.Set_Fields("adjustrecord", false);
			rsSave.Set_Fields("totalrecord", false);
		}

		private void CheckDateRelationship()
		{
			// checks to see which columns might be the same values and which should not be editable
			// vbPorter upgrade warning: dtPayDate As DateTime	OnWrite(string)
			DateTime dtPayDate;
			// vbPorter upgrade warning: dtMonthStart As DateTime	OnWrite(string)
			DateTime dtMonthStart;
			DateTime dtFiscalStart;
			DateTime dtQuarterStart = DateTime.FromOADate(0);
			// vbPorter upgrade warning: dtCalendarStart As DateTime	OnWrite(string)
			DateTime dtCalendarStart;
			DateTime dtTemp = DateTime.FromOADate(0);
			int lngRow;
			if (!Information.IsDate(T2KPayDate.Text))
			{
				return;
			}
			boolEditFTD = true;
			boolEditQTD = true;
			boolEditCTD = true;
			dtPayDate = FCConvert.ToDateTime(T2KPayDate.Text);
			dtMonthStart = FCConvert.ToDateTime(FCConvert.ToString(dtPayDate.Month) + "/1/" + FCConvert.ToString(dtPayDate.Year));
			dtFiscalStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtPayDate);
			modGlobalRoutines.GetDatesForAQuarter(ref dtQuarterStart, ref dtTemp, dtPayDate);
			dtCalendarStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtCalendarStart) == 0)
			{
				boolEditFTD = false;
				intFTDSameAs = CNSTGRIDCOLCTD;
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, Grid.BackColorFixed);
			}
			if (fecherFoundation.DateAndTime.DateDiff("d", dtCalendarStart, dtQuarterStart) == 0)
			{
				boolEditCTD = false;
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLCTD, Grid.Rows - 1, CNSTGRIDCOLCTD, Grid.BackColorFixed);
			}
			if (fecherFoundation.DateAndTime.DateDiff("d", dtFiscalStart, dtQuarterStart) == 0)
			{
				boolEditFTD = false;
				intFTDSameAs = CNSTGRIDCOLQTD;
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, Grid.BackColorFixed);
			}
			if (fecherFoundation.DateAndTime.DateDiff("d", dtMonthStart, dtQuarterStart) == 0)
			{
				boolEditQTD = false;
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLQTD, Grid.Rows - 1, CNSTGRIDCOLQTD, Grid.BackColorFixed);
			}
			if (fecherFoundation.DateAndTime.DateDiff("d", dtMonthStart, dtFiscalStart) == 0)
			{
				boolEditFTD = false;
				intFTDSameAs = CNSTGRIDCOLMTD;
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, Grid.BackColorFixed);
			}
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (!boolEditQTD)
				{
					Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD, Grid.TextMatrix(lngRow, CNSTGRIDCOLMTD));
				}
				if (!boolEditCTD)
				{
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCTD, Grid.TextMatrix(lngRow, CNSTGRIDCOLQTD));
				}
				if (!boolEditFTD)
				{
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFTD, Grid.TextMatrix(lngRow, intFTDSameAs));
				}
			}
			// lngRow
			if (boolEditQTD)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLQTD, Grid.Rows - 1, CNSTGRIDCOLQTD, Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLMTD));
			}
			if (boolEditCTD)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLCTD, Grid.Rows - 1, CNSTGRIDCOLCTD, Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLMTD));
			}
			if (boolEditFTD)
			{
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFTD, Grid.Rows - 1, CNSTGRIDCOLFTD, Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLMTD));
			}
		}
		// vbPorter upgrade warning: lngRow As object	OnWriteFCConvert.ToInt32(
		private void UpdateValues(object lngRow)
		{
			if (!boolEditQTD)
			{
				Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLQTD, Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLMTD));
			}
			if (!boolEditCTD)
			{
				Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLCTD, Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLQTD));
			}
			if (!boolEditFTD)
			{
				Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLFTD, Grid.TextMatrix(FCConvert.ToInt32(lngRow), intFTDSameAs));
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "T2KPayDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "As of Pay Date";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			for (intRows = 1; intRows <= (Grid.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (Grid.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "Grid";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = Grid.TextMatrix(intRows, 0) + " " + Grid.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}
	}
}
