//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPrintBankSetup.
	/// </summary>
	partial class frmPrintBankSetup
	{
		public fecherFoundation.FCComboBox cmbStyle;
		public fecherFoundation.FCLabel lblStyle;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbtion1;
		public fecherFoundation.FCLabel lbltion1;
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCCheckBox chkBankName;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbStyle = new fecherFoundation.FCComboBox();
            this.lblStyle = new fecherFoundation.FCLabel();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.cmbtion1 = new fecherFoundation.FCComboBox();
            this.lbltion1 = new fecherFoundation.FCLabel();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.chkBankName = new fecherFoundation.FCCheckBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 317);
            this.BottomPanel.Size = new System.Drawing.Size(655, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkBankName);
            this.ClientArea.Controls.Add(this.cmbStyle);
            this.ClientArea.Controls.Add(this.lblStyle);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbtion1);
            this.ClientArea.Controls.Add(this.lbltion1);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Size = new System.Drawing.Size(655, 257);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(655, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(256, 30);
            this.HeaderText.Text = "Print Bank Information";
            // 
            // cmbStyle
            // 
            this.cmbStyle.AutoSize = false;
            this.cmbStyle.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStyle.FormattingEnabled = true;
            this.cmbStyle.Items.AddRange(new object[] {
            "1 Across",
            "2 Across",
            "3 Across"});
            this.cmbStyle.Location = new System.Drawing.Point(164, 210);
            this.cmbStyle.Name = "cmbStyle";
            this.cmbStyle.Size = new System.Drawing.Size(180, 40);
            this.cmbStyle.TabIndex = 21;
            this.cmbStyle.Text = "1 Across";
            // 
            // lblStyle
            // 
            this.lblStyle.AutoSize = true;
            this.lblStyle.Location = new System.Drawing.Point(30, 224);
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Size = new System.Drawing.Size(47, 15);
            this.lblStyle.TabIndex = 22;
            this.lblStyle.Text = "STYLE";
            // 
            // cmbType
            // 
            this.cmbType.AutoSize = false;
            this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Listing",
            "Labels"});
            this.cmbType.Location = new System.Drawing.Point(164, 90);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(180, 40);
            this.cmbType.TabIndex = 23;
            this.cmbType.Text = "Listing";
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 104);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 15);
            this.lblType.TabIndex = 24;
            this.lblType.Text = "TYPE";
            // 
            // cmbtion1
            // 
            this.cmbtion1.AutoSize = false;
            this.cmbtion1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbtion1.FormattingEnabled = true;
            this.cmbtion1.Items.AddRange(new object[] {
            "All Banks",
            "Range of Banks"});
            this.cmbtion1.Location = new System.Drawing.Point(164, 30);
            this.cmbtion1.Name = "cmbtion1";
            this.cmbtion1.Size = new System.Drawing.Size(180, 40);
            this.cmbtion1.TabIndex = 25;
            this.cmbtion1.Text = "All Banks";
            this.cmbtion1.SelectedIndexChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // lbltion1
            // 
            this.lbltion1.AutoSize = true;
            this.lbltion1.Location = new System.Drawing.Point(30, 44);
            this.lbltion1.Name = "lbltion1";
            this.lbltion1.Size = new System.Drawing.Size(81, 15);
            this.lbltion1.TabIndex = 26;
            this.lbltion1.Text = "REPORT ON";
            // 
            // cmbSort
            // 
            this.cmbSort.AutoSize = false;
            this.cmbSort.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.Items.AddRange(new object[] {
            "By Name",
            "By Number"});
            this.cmbSort.Location = new System.Drawing.Point(164, 150);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(180, 40);
            this.cmbSort.TabIndex = 27;
            this.cmbSort.Text = "By Name";
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 164);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(42, 15);
            this.lblSort.TabIndex = 28;
            this.lblSort.Text = "SORT";
            // 
            // chkBankName
            // 
            this.chkBankName.Location = new System.Drawing.Point(364, 200);
            this.chkBankName.Name = "chkBankName";
            this.chkBankName.Size = new System.Drawing.Size(186, 27);
            this.chkBankName.TabIndex = 20;
            this.chkBankName.Text = "Print Bank Name only";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(537, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(90, 24);
            this.cmdPrint.TabIndex = 11;
            this.cmdPrint.Text = "Print Report";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtStart);
            this.Frame3.Controls.Add(this.txtEnd);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Location = new System.Drawing.Point(364, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(263, 150);
            this.Frame3.TabIndex = 15;
            this.Frame3.Text = "Range";
            // 
            // txtStart
            // 
            this.txtStart.AutoSize = false;
            this.txtStart.Enabled = false;
            this.txtStart.LinkItem = null;
            this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStart.LinkTopic = null;
            this.txtStart.Location = new System.Drawing.Point(123, 30);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(120, 40);
            this.txtStart.TabIndex = 2;
            this.txtStart.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStart_KeyPress);
            this.txtStart.Enter += new System.EventHandler(this.txtStart_Enter);
            // 
            // txtEnd
            // 
            this.txtEnd.AutoSize = false;
            this.txtEnd.Enabled = false;
            this.txtEnd.LinkItem = null;
            this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEnd.LinkTopic = null;
            this.txtEnd.Location = new System.Drawing.Point(123, 90);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(120, 40);
            this.txtEnd.TabIndex = 3;
            this.txtEnd.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEnd_KeyPress);
            this.txtEnd.Enter += new System.EventHandler(this.txtEnd_Enter);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(40, 16);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "FROM";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(30, 16);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "TO";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process                            ";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(260, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // frmPrintBankSetup
            // 
            this.AcceptButton = this.cmdPrint;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(655, 425);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrintBankSetup";
            this.Text = "Print Bank Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPrintBankSetup_Load);
            this.Activated += new System.EventHandler(this.frmPrintBankSetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintBankSetup_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}