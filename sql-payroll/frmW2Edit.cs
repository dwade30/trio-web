//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2Edit : BaseForm
	{
		public frmW2Edit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2Edit InstancePtr
		{
			get
			{
				return (frmW2Edit)Sys.GetInstance(typeof(frmW2Edit));
			}
		}

		protected frmW2Edit _InstancePtr = null;
		//=========================================================
		private bool boolLoad;
		const int ID = 8;

		private void frmW2Edit_Activated(object sender, System.EventArgs e)
		{
			// If boolLoad Then
			// Call LoadGrid
			// boolLoad = False
			// End If
			// Call ForceFormToResize(Me)
		}

		private void frmW2Edit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2Edit properties;
			//frmW2Edit.ScaleWidth	= 8985;
			//frmW2Edit.ScaleHeight	= 7290;
			//frmW2Edit.LinkTopic	= "Form1";
			//frmW2Edit.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				//FC:FINAL:DSE:#i2197 Add expand button
				vsData.AddExpandButton();
				LoadGrid();
				// vsData.Font.Size = 8
				boolLoad = true;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2Edit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmW2Edit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsW2Matches = new clsDRWrapper();
			clsDRWrapper rsW2Deductions = new clsDRWrapper();
			string strEmployee = "";
			string strOrderBy = "";
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			// vsData.ColDataType(6) = flexDTBoolean
			// vsData.ColDataType(7) = flexDTBoolean
			vsData.Cols = 9;
			vsData.Rows = 2;
			vsData.OutlineCol = 0;
			vsData.FixedRows = 1;
			vsData.FixedCols = 1;
            // vsData.Editable = flexEDKbdMouse
            vsData.Redraw = false;
			vsData.SetFontSize(8);
			vsData.TextMatrix(0, 1, "Emp #");
			vsData.TextMatrix(0, 2, "Employee Name");
			vsData.TextMatrix(0, 3, "Address");
			vsData.TextMatrix(0, 4, "City/State/Zip");
			vsData.TextMatrix(0, 5, "SSN");
			vsData.TextMatrix(0, 6, "Ret.");
			vsData.TextMatrix(0, 7, "Def.");
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			pbrStatus.Value = 0;
			switch (modCoreysSweeterCode.GetReportSequence())
			{
				case 0:
					{
						// name
						strOrderBy = " lastname,firstname ";
						break;
					}
				case 1:
					{
						// number
						strOrderBy = " employeenumber ";
						break;
					}
				case 2:
					{
						// seqnumber
						strOrderBy = " seqnumber,lastname,firstname ";
						break;
					}
				case 3:
					{
						// deptdiv
						strOrderBy = " deptdiv,lastname,firstname ";
						break;
					}
			}
			//end switch
			rsData.OpenRecordset("SELECT * from tblW2EditTable order by " + strOrderBy, "TWPY0000.vb1");
			rsW2Deductions.OpenRecordset("SELECT tblDeductionSetup.Description, tblDeductionSetup.DeductionNumber as RealDeductionNumber, tblW2Deductions.DeductionNumber, tblW2Deductions.ID, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, tblW2Deductions.CYTDAmount FROM tblDeductionSetup INNER JOIN tblW2Deductions ON tblDeductionSetup.ID = tblW2Deductions.DeductionNumber Order by EmployeeNumber");
			// corey 12/29/2005 call 84534  should match ID not dednumber
			// Call rsW2Matches.OpenRecordset("SELECT tblDeductionSetup.Description, tblW2Matches.Code, tblW2Matches.EmployeeNumber, tblW2Matches.ID, tblW2Matches.CYTDAmount, tblW2Matches.DeductionNumber FROM tblDeductionSetup INNER JOIN tblW2Matches ON tblDeductionSetup.DeductionNumber = tblW2Matches.DeductionNumber Order by EmployeeNumber")
			rsW2Matches.OpenRecordset("SELECT tblDeductionSetup.Description, tblW2Matches.Code,TBLDEDUCTIONSETUP.DEDUCTIONNUMBER AS RealDeductionNumber, tblW2Matches.EmployeeNumber, tblW2Matches.ID, tblW2Matches.CYTDAmount, tblW2Matches.DeductionNumber FROM tblDeductionSetup INNER JOIN tblW2Matches ON tblDeductionSetup.ID = tblW2Matches.DeductionNumber Order by EmployeeNumber");
			if (!rsData.EndOfFile())
			{
				rsData.MoveLast();
				rsData.MoveFirst();
				pbrStatus.Maximum = rsData.RecordCount() + 1;
				lblStatus.Text = "Loading Employee Information Records";
				pbrStatus.Value = 1;
				this.Refresh();
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					// If rsData.Fields("EmployeeNumber") = "070" Then MsgBox "A"
					lblStatus.Text = "Loading record  " + FCConvert.ToString(intCounter) + " of " + FCConvert.ToString(rsData.RecordCount() + 1);
					this.Refresh();
					vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.TextMatrix(vsData.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
					vsData.TextMatrix(vsData.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("EmployeeName")));
					vsData.TextMatrix(vsData.Rows - 1, 2, fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsData.Get_Fields("lastname") + ", " + rsData.Get_Fields("FIRSTNAME") + " " + rsData.Get_Fields("middlename")) + " " + rsData.Get_Fields("desig")));
					vsData.TextMatrix(vsData.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields_String("Address")));
					vsData.TextMatrix(vsData.Rows - 1, 4, FCConvert.ToString(rsData.Get_Fields_String("CityStateZip")));
					vsData.TextMatrix(vsData.Rows - 1, 5, Strings.Format(Strings.Replace(FCConvert.ToString(rsData.Get_Fields_String("SSN")), "-", "", 1, -1, CompareConstants.vbTextCompare), "000-00-0000"));
					vsData.TextMatrix(vsData.Rows - 1, 6, FCConvert.ToString(rsData.Get_Fields_Boolean("W2Retirement")));
					vsData.TextMatrix(vsData.Rows - 1, 7, FCConvert.ToString(rsData.Get_Fields_Boolean("W2Deferred")));
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 5, vsData.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 6, vsData.Rows - 1, 7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 0);
					vsData.IsSubtotal(vsData.Rows - 1, true);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vsData.Rows += 1;
					// ADD THE WAGES CAPTIONS
					vsData.TextMatrix(vsData.Rows - 1, 1, "Wages");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 1);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					vsData.TextMatrix(vsData.Rows - 1, 2, "Total Wage    Fed (Box 1) Wage");
					vsData.TextMatrix(vsData.Rows - 1, 3, "FICA Wage");
					vsData.TextMatrix(vsData.Rows - 1, 4, "Med Wage");
					vsData.TextMatrix(vsData.Rows - 1, 5, "State Wage");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 0, vsData.Rows - 1, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 2);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    // ADD THE WAGE DATA
                    vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					vsData.TextMatrix(vsData.Rows - 1, 2, Strings.Format(rsData.Get_Fields_Double("TotalGross"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 2, fecherFoundation.Strings.Trim(vsData.TextMatrix(vsData.Rows - 1, 2)) + Strings.StrDup(23 - Strings.Format(rsData.Get_Fields_Double("FederalWage"), "#,##0.00").Length, " ") + Strings.Format(rsData.Get_Fields_Double("FederalWage"), "#,##0.00"));
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.TextMatrix(vsData.Rows - 1, 3, Strings.Format(rsData.Get_Fields("FICAWage"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(rsData.Get_Fields_Double("MedicareWage"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 5, Strings.Format(rsData.Get_Fields_Double("StateWage"), "#,##0.00"));
					vsData.RowOutlineLevel(vsData.Rows - 1, 3);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					// ADD THE TAX CAPTIONS
					vsData.TextMatrix(vsData.Rows - 1, 1, "Taxes");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 1);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					vsData.TextMatrix(vsData.Rows - 1, 2, "Fed Tax");
					vsData.TextMatrix(vsData.Rows - 1, 3, "FICA Tax");
					vsData.TextMatrix(vsData.Rows - 1, 4, "Med Tax");
					vsData.TextMatrix(vsData.Rows - 1, 5, "State Tax");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 0, vsData.Rows - 1, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 2);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    // ADD THE TAX DATA
                    vsData.TextMatrix(vsData.Rows - 1, 2, Strings.Format(rsData.Get_Fields_Double("FederalTax"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 3, Strings.Format(rsData.Get_Fields("FICATax"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(rsData.Get_Fields("MedicareTax"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, 5, Strings.Format(rsData.Get_Fields_Double("StateTax"), "#,##0.00"));
					vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.RowOutlineLevel(vsData.Rows - 1, 3);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					// ****************************************************************
					// ADD THE BOX 12 CAPTIONS
					vsData.TextMatrix(vsData.Rows - 1, 1, "Deductions");
					vsData.RowOutlineLevel(vsData.Rows - 1, 1);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					vsData.TextMatrix(vsData.Rows - 1, 2, "");
					vsData.TextMatrix(vsData.Rows - 1, 3, "Code");
					vsData.TextMatrix(vsData.Rows - 1, 4, "Amount");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 2);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
					// ADD THE BOX 12 DATA
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					rsW2Deductions.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
					if (!rsW2Deductions.NoMatch)
					{
						NextDeduction:
						;
						vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(rsW2Deductions.Get_Fields_Int32("DeductionNumber")));
						vsData.TextMatrix(vsData.Rows - 1, 2, rsW2Deductions.Get_Fields("RealDeductionNumber") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Deductions.Get_Fields("Description"))));
						vsData.TextMatrix(vsData.Rows - 1, 3, FCConvert.ToString(rsW2Deductions.Get_Fields("Code")));
						vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(rsW2Deductions.Get_Fields_Double("CYTDAmount"), "#,##0.00"));
						vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(rsW2Deductions.Get_Fields("ID")));
						vsData.RowOutlineLevel(vsData.Rows - 1, 3);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
                        vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
						rsW2Deductions.FindNextRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
						if (!rsW2Deductions.NoMatch)
							goto NextDeduction;
					}
					// ****************************************************************
					// ****************************************************************
					// ADD THE BOX 14 CAPTIONS
					vsData.TextMatrix(vsData.Rows - 1, 1, "Employer Match");
					vsData.RowOutlineLevel(vsData.Rows - 1, 1);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					vsData.TextMatrix(vsData.Rows - 1, 2, "");
					vsData.TextMatrix(vsData.Rows - 1, 3, "Code");
					vsData.TextMatrix(vsData.Rows - 1, 4, "Amount");
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsData.RowOutlineLevel(vsData.Rows - 1, 2);
					vsData.IsSubtotal(vsData.Rows - 1, true);
					vsData.Rows += 1;
                    vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
					// ADD THE BOX 14 ITEMS
					vsData.TextMatrix(vsData.Rows - 1, 1, "");
					rsW2Matches.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
					if (!rsW2Matches.NoMatch)
					{
						NextMatch:
						;
						vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(rsW2Matches.Get_Fields_Int32("DeductionNumber")));
						vsData.TextMatrix(vsData.Rows - 1, 2, rsW2Matches.Get_Fields("RealDeductionNumber") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Matches.Get_Fields("Description"))));
						vsData.TextMatrix(vsData.Rows - 1, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Matches.Get_Fields("Code"))));
						vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(rsW2Matches.Get_Fields_Double("CYTDAmount"), "#,##0.00"));
						vsData.TextMatrix(vsData.Rows - 1, ID, FCConvert.ToString(rsW2Matches.Get_Fields("ID")));
						vsData.RowOutlineLevel(vsData.Rows - 1, 3);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
                        vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4 , FCGrid.AlignmentSettings.flexAlignRightCenter);
						rsW2Matches.FindNextRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
						if (!rsW2Matches.NoMatch)
							goto NextMatch;
					}
					// ****************************************************************
					NextEmployee:
					;
					pbrStatus.Value = pbrStatus.Value + 1;
					this.Refresh();
					rsData.MoveNext();
				}
			}
			// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
			vsData.Outline(-1);
			vsData.Outline(1);
			vsData.Redraw = true;
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (vsData.Rows == 2)
				{
				}
				else
				{
					vsData.IsCollapsed(intCounter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			pbrStatus.Visible = false;
			lblStatus.Visible = false;
			modColorScheme.ColorGrid(vsData, 1);
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				vsData.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 0, vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0));
			}
			// intCounter
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			vsData.ColHidden(8, true);
		}

		private void frmW2Edit_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = vsData.WidthOriginal;
            //FC:FINAL:AM:#2687 - increase width for 2nd column (this displays the outline)
            //vsData.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.07));
            //vsData.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.067));
            vsData.ColWidth(0, 0);
            vsData.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.1));
            vsData.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.235));
			vsData.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.2));
			vsData.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.16));
			vsData.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.12));
			vsData.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.08));
			vsData.ColWidth(7, vsData.ColWidth(6));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.glngW2EditTableID = 0;
			frmW2PersonEdit.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrintPreview_Click()
		{
			frmReportViewer.InstancePtr.Init(rptW2EditReport.InstancePtr, showModal: this.Modal);
			//rptW2EditReport.InstancePtr.Show(App.MainForm);
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// SAVE THE INFORMATION INTO THE MASTER SCREEN
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsW2Deductions = new clsDRWrapper();
			clsDRWrapper rsW2Matches = new clsDRWrapper();
			string strEmployeeNumber = "";
			int intRowOutLineLevel;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsData.OpenRecordset("Select * from tblW2EditTable", "TWPY0000.vb1");
				rsW2Deductions.OpenRecordset("Select * from tblW2Deductions", "TWPY0000.vb1");
				rsW2Matches.OpenRecordset("Select * from tblW2Matches", "TWPY0000.vb1");
				vsData.Select(0, 0);
				for (intRow = 1; intRow <= (vsData.Rows - 2); intRow++)
				{
					if (vsData.RowOutlineLevel(intRow) == 0)
					{
						if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, ID)) == string.Empty)
						{
						}
						else
						{
							rsData.FindFirstRecord("ID", fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, ID)));
							if (rsData.NoMatch)
							{
								intRow += 6;
							}
							else
							{
								rsData.Edit();
								rsData.Set_Fields("EmployeeNumber", vsData.TextMatrix(intRow, 1));
								strEmployeeNumber = vsData.TextMatrix(intRow, 1);
								rsData.Set_Fields("EmployeeName", vsData.TextMatrix(intRow, 2));
								rsData.Set_Fields("Address", vsData.TextMatrix(intRow, 3));
								rsData.Set_Fields("CityStateZip", vsData.TextMatrix(intRow, 4));
								rsData.Set_Fields("SSN", vsData.TextMatrix(intRow, 5));
								rsData.Set_Fields("W2Retirement", vsData.TextMatrix(intRow, 6));
								rsData.Set_Fields("W2Deferred", vsData.TextMatrix(intRow, 7));
								intRow += 3;
								rsData.Set_Fields("TotalGross", FCConvert.ToString(modGlobalRoutines.CheckForBlank(fecherFoundation.Strings.Trim(Strings.Left(vsData.TextMatrix(intRow, 2), 16)))).Replace(",", ""));
								rsData.Set_Fields("FederalWage", FCConvert.ToString(modGlobalRoutines.CheckForBlank(fecherFoundation.Strings.Trim(Strings.Right(vsData.TextMatrix(intRow, 2), 16)))).Replace(",", ""));
								rsData.Set_Fields("FICAWage", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 3))).Replace(",", ""));
								rsData.Set_Fields("MedicareWage", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
								rsData.Set_Fields("StateWage", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 5))).Replace(",", ""));
								intRow += 3;
								rsData.Set_Fields("FederalTax", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 2))).Replace(",", ""));
								rsData.Set_Fields("FICATax", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 3))).Replace(",", ""));
								rsData.Set_Fields("MedicareTax", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
								rsData.Set_Fields("StateTax", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 5))).Replace(",", ""));
								rsData.Update();
							}
							intRow += 3;
							// SAVE THE DEDUCTION RECORDS
							if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, 2)) == string.Empty)
							{
							}
							else
							{
								while (vsData.RowOutlineLevel(intRow) == 3)
								{
									rsW2Deductions.FindFirstRecord("ID", vsData.TextMatrix(intRow, ID));
									if (rsW2Deductions.NoMatch)
									{
									}
									else
									{
										rsW2Deductions.Edit();
										rsW2Deductions.Set_Fields("EmployeeNumber", strEmployeeNumber);
										rsW2Deductions.Set_Fields("Box12", true);
										rsW2Deductions.Set_Fields("DeductionNumber", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intRow, 0))));
										rsW2Deductions.Set_Fields("Code", vsData.TextMatrix(intRow, 3));
										rsW2Deductions.Set_Fields("CYTDAmount", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
										rsW2Deductions.Update();
									}
									if (intRow + 1 < vsData.Rows - 1)
									{
										intRow += 1;
									}
									else
									{
										goto ExitDeductions;
									}
								}
							}
							ExitDeductions:
							;
							intRow += 2;
							// SAVE THE MATCH RECORDS
							if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, 2)) == string.Empty)
							{
							}
							else
							{
								while (vsData.RowOutlineLevel(intRow) == 3)
								{
									rsW2Matches.FindFirstRecord("ID", vsData.TextMatrix(intRow, ID));
									if (rsW2Matches.NoMatch)
									{
									}
									else
									{
										rsW2Matches.Edit();
										rsW2Matches.Set_Fields("EmployeeNumber", strEmployeeNumber);
										rsW2Matches.Set_Fields("Box14", true);
										rsW2Matches.Set_Fields("DeductionNumber", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intRow, 0))));
										rsW2Matches.Set_Fields("Code", vsData.TextMatrix(intRow, 3));
										rsW2Matches.Set_Fields("CYTDAmount", FCConvert.ToString(modGlobalRoutines.CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
										rsW2Matches.Update();
									}
									if (intRow + 1 < vsData.Rows - 1)
									{
										intRow += 1;
									}
									else
									{
										goto ExitMatch;
									}
								}
							}
							ExitMatch:
							;
							intRow -= 1;
						}
					}
				}
				modGlobalRoutines.UpdateW2StatusData("Update");
				MessageBox.Show("Update of W-2 information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsData_DblClick(object sender, System.EventArgs e)
		{
			if (vsData.RowOutlineLevel(vsData.MouseRow) == 0)
			{
				int lngRow = 0;
				lngRow = vsData.MouseRow;
				if (lngRow == 0)
					return;
				modGlobalVariables.Statics.glngW2EditTableID = FCConvert.ToInt32(vsData.TextMatrix(vsData.MouseRow, ID));
				Close();
				frmW2PersonEdit.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MessageBox.Show("Double Click on the White rows to edit information for each employee.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
