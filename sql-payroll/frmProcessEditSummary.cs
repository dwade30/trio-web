//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmProcessEditSummary : BaseForm
	{
		public frmProcessEditSummary()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmProcessEditSummary InstancePtr
		{
			get
			{
				return (frmProcessEditSummary)Sys.GetInstance(typeof(frmProcessEditSummary));
			}
		}

		protected frmProcessEditSummary _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       DECEMBER 31, 2002
		//
		// MODIFIED BY:
		// NOTES:
		//
		//
		// **************************************************
		private int intPrintSequence;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;

		private void frmProcessEditSummary_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmProcessEditSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmProcessEditSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProcessEditSummary properties;
			//frmProcessEditSummary.ScaleWidth	= 6105;
			//frmProcessEditSummary.ScaleHeight	= 4080;
			//frmProcessEditSummary.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsDefault = new clsDRWrapper();
				rsDefault.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
				if (rsDefault.EndOfFile())
				{
					intPrintSequence = 0;
				}
				else
				{
					// Employee Name = 0
					// Employee Number = 1
					// Sequence Number = 2
					// Department / Div = 3
					intPrintSequence = FCConvert.ToInt16(rsDefault.Get_Fields_Int32("DataEntrySequence"));
				}
				// set the grid column headers/widths/etc....
				SetGridProperties();
				LoadData();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsGrid.FixedCols = 0;
				vsGrid.FixedRows = 0;
				vsGrid.Cols = 3;
				vsGrid.Rows = 1;
				// Adjust the widths of the columns to be a
				// percentage of the grid with itself
				vsGrid.ColWidth(0, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.57));
				vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
				vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
				vsGrid.FixedRows = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter = 0;
				string strCompareNumber = "";
				double dblTotal = 0;
				double dblDollarsTotal = 0;
				double dblOtherTotal;
				double dblOtherDollarsTotal;
				string strEmployeeNumber = "";
				string strDivField = "";
				string strDeptField = "";
				string strCompareField = "";
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				string strDisplayName = "";
				int intDeptLength;
				int intDivLength;
				string strFind;
				strFind = "";
				int lngID;
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				rsMaster.OpenRecordset("select * from payrollpermissions where userID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
				while (!rsMaster.EndOfFile())
				{
					switch (rsMaster.Get_Fields_Int32("type"))
					{
						case CNSTPYTYPEDEPTDIV:
							{
								strFind += "not deptdiv = '" + rsMaster.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEGROUP:
							{
								strFind += "not groupid = '" + rsMaster.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEIND:
							{
								strFind += "not employeenumber = '" + rsMaster.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPESEQ:
							{
								strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("val"))) + " and ";
								break;
							}
						case CNSTPYTYPEDEPARTMENT:
							{
								strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("val"))) + " and ";
								break;
							}
					}
					//end switch
					rsMaster.MoveNext();
				}
				if (strFind != string.Empty)
				{
					strFind = Strings.Mid(strFind, 1, strFind.Length - 4);
					strFind = " where " + strFind;
				}
				// get all employees
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster " + strFind, modGlobalVariables.DEFAULTDATABASE);
				if (intPrintSequence == 0)
				{
					// Employee Name = 0
					// get the distributions for Employee Name
					// Call rsData.OpenRecordset("Select tblPayrollDistribution.*,tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID where DefaultHours <> HoursWeek OR tblPayCategories.Type = 'Dollars' Order by LastName,FirstName", DEFAULTDATABASE)
					// Call rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours <> [HoursWeek]) AND BaseRate <> 0) Or ((tblPayCategories.Type = 'DOLLARS' AND HoursWeek <> 0))) ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName", DEFAULTDATABASE)
					rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours <> [HoursWeek]) AND BaseRate <> 0) Or ((tblPayCategories.Type = 'DOLLARS' AND HoursWeek <> 0))) and accountcode <> '1' ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName", modGlobalVariables.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
						return;
					}
					else
					{
						strCompareField = "tblPayrollDistribution.EmployeeNumber";
						strCompareNumber = FCConvert.ToString(rsData.Get_Fields("tblPayrollDistribution.EmployeeNumber"));
						intCounter = 1;
					}
				}
				else if (intPrintSequence == 1)
				{
					// Employee Number = 1
					// get the distributions for Employee Name
					// Call rsData.OpenRecordset("Select tblPayrollDistribution.*,tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where DefaultHours <> HoursWeek Order by tblPayrollDistribution.EmployeeNumber", DEFAULTDATABASE)
					rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours) <> [HoursWeek] AND BaseRate <> 0) Or ((tblPayCategories.Type = 'DOLLARS' AND HoursWeek <> 0))) and accountcode <> '1' ORDER BY tblPayrollDistribution.EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
						return;
					}
					else
					{
						strCompareField = "tblPayrollDistribution.EmployeeNumber";
						strCompareNumber = FCConvert.ToString(rsData.Get_Fields("tblPayrollDistribution.EmployeeNumber"));
						intCounter = 1;
					}
				}
				else if (intPrintSequence == 2)
				{
					// Sequence Number = 2
					// get the distributions for Employee Name
					// Call rsData.OpenRecordset("Select tblPayrollDistribution.*,tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where DefaultHours <> HoursWeek Order by SeqNumber", DEFAULTDATABASE)
					rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours) <> [HoursWeek] AND BaseRate <> 0) Or ((tblPayCategories.Type = 'DOLLARS' AND HoursWeek <> 0))) and accountcode <> '1' ORDER BY SeqNumber", modGlobalVariables.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
						return;
					}
					else
					{
						strCompareField = "SeqNumber";
						strCompareNumber = FCConvert.ToString(rsData.Get_Fields("SeqNumber"));
						intCounter = 1;
					}
				}
				else if (intPrintSequence == 3)
				{
					// Department / Div = 3
					// get the length of the dept and division from Field Lenghts
					rsData.OpenRecordset("Select * from tblFieldLengths", modGlobalVariables.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
						intDeptLength = 0;
						intDivLength = 0;
					}
					else
					{
						intDeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int32("DeptLength"))));
						intDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int32("DivLength"))));
					}
					// get the distributions for department/division
					rsData.OpenRecordset("SELECT tblPayrollDistribution.*, tblEmployeeMaster.*, tblPayCategories.Type FROM (tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) INNER JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID Where (((tblPayrollDistribution.DefaultHours) <> [HoursWeek] AND BaseRate <> 0) Or ((tblPayCategories.Type = 'DOLLARS' AND HoursWeek <> 0))) and accountcode <> '1' ORDER BY DEPTDIV", modGlobalVariables.DEFAULTDATABASE);
					// Call rsData.OpenRecordset("Select tblPayrollDistribution.EmployeeNumber,tblPayrollDistribution.*,tblPayCategories.Type from tblPayrollDistribution LEFT JOIN tblPayCategories ON tblPayrollDistribution.CAT = tblPayCategories.ID where ((DefaultHours <> HoursWeek) AND BaseRate <> 0)  OR ((tblPayCategories.Type)= 'Dollars') and accountcode <> '1' Order by AccountNumber", DEFAULTDATABASE)
					if (rsData.EndOfFile())
					{
						return;
					}
					else
					{
						// strCompareField = "AccountNumber"
						// strDeptField = Trim(Mid(rsData.Fields("AccountNumber"), 3, intDeptLength))
						// strDivField = Trim(Mid(rsData.Fields("AccountNumber"), 3 + intDeptLength, intDivLength))
						strCompareField = "DeptDiv";
						// strCompareNumber = strDeptField & " - " & strDivField
						strCompareNumber = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
						intCounter = 1;
					}
				}
				if (intPrintSequence == 0)
				{
					vsGrid.TextMatrix(0, 0, "Employee Name");
				}
				else if (intPrintSequence == 1)
				{
					vsGrid.TextMatrix(0, 0, "Employee Number");
				}
				else if (intPrintSequence == 2)
				{
					vsGrid.TextMatrix(0, 0, "Sequence Number");
				}
				else if (intPrintSequence == 3)
				{
					vsGrid.TextMatrix(0, 0, "Dept / Div");
				}
				vsGrid.TextMatrix(0, 1, "Hours");
				vsGrid.TextMatrix(0, 2, "Dollars");
				dblOtherTotal = 0;
				dblOtherDollarsTotal = 0;
				while (!rsData.EndOfFile())
				{
					strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("tblPayrollDistribution.EmployeeNumber"));
					rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);
					if (rsMaster.NoMatch)
					{
						goto NextRecord;
					}
					else
					{
						// check the status and frequency
						if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
						{
							goto NextRecord;
						}
						else
						{
							if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
							{
								// If vsGrid.TextMatrix(0, 0) = "Dept / Div" Then
								// everything is ok so add this to the totals
								// If strDeptField = vbNullString And strDivField = vbNullString Then
								// Else
								// not check the account number
								// If Left(rsData.Fields("AccountNumber"), 1) = "E" Then
								// If strDeptField <> Trim(Mid(rsData.Fields("AccountNumber"), 2, intDeptLength)) And
								// strDivField <> Trim(Mid(rsData.Fields("AccountNumber"), 2 + intDeptLength, intDivLength)) Then
								// 
								// vsGrid.Rows = vsGrid.Rows + 1
								// vsGrid.TextMatrix(intCounter, 0) = strDeptField & " - " & strDivField
								// vsGrid.TextMatrix(intCounter, 1) = Format(dblTotal, "0.00")
								// vsGrid.TextMatrix(intCounter, 2) = Format(dblDollarsTotal, "0.00")
								// 
								// 
								// intCounter = intCounter + 1
								// strDeptField = Trim(Mid(rsData.Fields("AccountNumber"), 2, intDeptLength))
								// strDivField = Trim(Mid(rsData.Fields("AccountNumber"), 2 + intDeptLength, intDivLength))
								// strCompareNumber = strDeptField & " - " & strDivField
								// dblTotal = 0
								// dblDollarsTotal = 0
								// End If
								// 
								// If rsData.Fields("Type") = "Dollars" Then
								// dblDollarsTotal = dblDollarsTotal + rsData.Fields("BaseRate")
								// Else
								// dblTotal = dblTotal + Val(rsData.Fields("HoursWeek"))
								// End If
								// strDisplayName = strCompareNumber
								// Else
								// If rsData.Fields("Type") = "Dollars" Then
								// dblOtherDollarsTotal = dblOtherDollarsTotal + rsData.Fields("BaseRate")
								// Else
								// dblOtherTotal = dblOtherTotal + Val(rsData.Fields("HoursWeek"))
								// End If
								// End If
								// End If
								// Else
								// everything is ok so add this to the totals
								if (strDisplayName == string.Empty)
								{
									strCompareNumber = FCConvert.ToString(rsData.Get_Fields(strCompareField));
								}
								if (strCompareNumber != FCConvert.ToString(rsData.Get_Fields(strCompareField)))
								{
									vsGrid.Rows += 1;
									vsGrid.TextMatrix(intCounter, 0, strDisplayName);
									vsGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
									vsGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "0.00"));
									intCounter += 1;
									strCompareNumber = FCConvert.ToString(rsData.Get_Fields(strCompareField));
									dblTotal = 0;
									dblDollarsTotal = 0;
								}
								if (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars")
								{
									dblDollarsTotal += Conversion.Val(rsData.Get_Fields_Decimal("BaseRate"));
								}
								else
								{
									dblTotal += Conversion.Val(rsData.Get_Fields_Double("HoursWeek"));
								}
								if (intPrintSequence == 0)
								{
									strDisplayName = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
								}
								else
								{
									strDisplayName = strCompareNumber;
								}
								// End If
							}
							else
							{
								goto NextRecord;
							}
						}
					}
					NextRecord:
					;
					rsData.MoveNext();
				}
				// now set the last record
				vsGrid.Rows += 1;
				vsGrid.TextMatrix(intCounter, 0, strDisplayName);
				vsGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
				vsGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "0.00"));
				if (intPrintSequence == 3)
				{
					if (Conversion.Val(dblOtherTotal) != 0)
					{
						intCounter += 1;
						vsGrid.Rows += 1;
						vsGrid.TextMatrix(intCounter, 0, "Other");
						vsGrid.TextMatrix(intCounter, 1, Strings.Format(dblOtherTotal, "0.00"));
						vsGrid.TextMatrix(intCounter, 2, Strings.Format(dblOtherDollarsTotal, "0.00"));
					}
				}
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsGrid.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, vsGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private bool ApplyThisDistribution(int intCode)
		{
			bool ApplyThisDistribution = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ApplyThisDistribution";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				ApplyThisDistribution = true;
				return ApplyThisDistribution;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return ApplyThisDistribution;
			}
		}

		private void frmProcessEditSummary_Resize(object sender, System.EventArgs e)
		{
			// Adjust the widths of the columns to be a
			// percentage of the grid with itself
			vsGrid.ColWidth(0, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.57));
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			Close();
		}

		private void mnuTotals_Click(object sender, System.EventArgs e)
		{
			frmProcessEditTotals.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
