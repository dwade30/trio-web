﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsESHPCheckRecord
	{
		//=========================================================
		private string strSSN = string.Empty;
		private string strEmployeeName = string.Empty;
		private int lngCheckNumber;
		private DateTime dtCheckDate;
		private double dblCheckAmount;
		private bool boolDirectDeposit;
		private string strPDFFileName = string.Empty;
		private bool boolVoided;
		private int intPayrun;

		public int Payrun
		{
			set
			{
				intPayrun = value;
			}
			get
			{
				int Payrun = 0;
				Payrun = intPayrun;
				return Payrun;
			}
		}

		public bool Voided
		{
			set
			{
				boolVoided = value;
			}
			get
			{
				bool Voided = false;
				Voided = boolVoided;
				return Voided;
			}
		}

		public string SocialSecurityNumber
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SocialSecurityNumber = "";
				SocialSecurityNumber = strSSN;
				return SocialSecurityNumber;
			}
		}

		public string EMPLOYEENAME
		{
			set
			{
				strEmployeeName = value;
			}
			get
			{
				string EMPLOYEENAME = "";
				EMPLOYEENAME = strEmployeeName;
				return EMPLOYEENAME;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public DateTime CheckDate
		{
			set
			{
				dtCheckDate = value;
			}
			get
			{
				DateTime CheckDate = System.DateTime.Now;
				CheckDate = dtCheckDate;
				return CheckDate;
			}
		}

		public double CheckAmount
		{
			set
			{
				dblCheckAmount = value;
			}
			get
			{
				double CheckAmount = 0;
				CheckAmount = dblCheckAmount;
				return CheckAmount;
			}
		}

		public bool DIRECTDEPOSIT
		{
			set
			{
				boolDirectDeposit = value;
			}
			get
			{
				bool DIRECTDEPOSIT = false;
				DIRECTDEPOSIT = boolDirectDeposit;
				return DIRECTDEPOSIT;
			}
		}

		public string PDFFile
		{
			set
			{
				strPDFFileName = value;
			}
			get
			{
				string PDFFile = "";
				PDFFile = strPDFFileName;
				return PDFFile;
			}
		}

		public void SetPDFFileName()
		{
			string strTemp;
			strTemp = Strings.Format(dtCheckDate, "MMddyyyy") + "_" + FCConvert.ToString(intPayrun) + "_";
			if (!DIRECTDEPOSIT)
			{
				strTemp += FCConvert.ToString(lngCheckNumber);
			}
			else
			{
				strTemp += "D" + FCConvert.ToString(lngCheckNumber);
			}
			strPDFFileName = strTemp;
		}
	}
}
