//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckRecipientsCategories.
	/// </summary>
	partial class frmCheckRecipientsCategories
	{
		public FCGrid vsCategories;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNewCategory;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCategory;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsCategories = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 535);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsCategories);
            this.ClientArea.Size = new System.Drawing.Size(1078, 475);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(330, 30);
            this.HeaderText.Text = "Check Recipients Categories";
            // 
            // vsCategories
            // 
            this.vsCategories.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsCategories.Cols = 8;
            this.vsCategories.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsCategories.FixedCols = 0;
            this.vsCategories.Location = new System.Drawing.Point(30, 30);
            this.vsCategories.Name = "vsCategories";
            this.vsCategories.ReadOnly = false;
            this.vsCategories.RowHeadersVisible = false;
            this.vsCategories.Rows = 50;
            this.vsCategories.Size = new System.Drawing.Size(1020, 436);
			this.vsCategories.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsCategories.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsCategories_KeyDownEdit);
            this.vsCategories.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsCategories_KeyPressEdit);
            this.vsCategories.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsCategories_AfterEdit);
            this.vsCategories.CurrentCellChanged += new System.EventHandler(this.vsCategories_RowColChange);
            this.vsCategories.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsCategories_MouseDownEvent);
            this.vsCategories.KeyDown += new Wisej.Web.KeyEventHandler(this.vsCategories_KeyDownEvent);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNewCategory,
            this.mnuDeleteCategory,
            this.mnuSP1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNewCategory
            // 
            this.mnuNewCategory.Index = 0;
            this.mnuNewCategory.Name = "mnuNewCategory";
            this.mnuNewCategory.Text = "New ";
            this.mnuNewCategory.Click += new System.EventHandler(this.mnuNewCategory_Click);
            // 
            // mnuDeleteCategory
            // 
            this.mnuDeleteCategory.Index = 1;
            this.mnuDeleteCategory.Name = "mnuDeleteCategory";
            this.mnuDeleteCategory.Text = "Delete ";
            this.mnuDeleteCategory.Click += new System.EventHandler(this.mnuDeleteCategory_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                    ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 5;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(998, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(48, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNewCategory_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(1052, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(62, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteCategory_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(494, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmCheckRecipientsCategories
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 643);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCheckRecipientsCategories";
            this.Text = "Check Recipients Categories";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCheckRecipientsCategories_Load);
            this.Activated += new System.EventHandler(this.frmCheckRecipientsCategories_Activated);
            this.Resize += new System.EventHandler(this.frmCheckRecipientsCategories_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCheckRecipientsCategories_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckRecipientsCategories_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdNew;
	}
}