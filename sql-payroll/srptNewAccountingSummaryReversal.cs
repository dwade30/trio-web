//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptAccountingSummaryReversal.
	/// </summary>
	public partial class srptAccountingSummaryReversal : FCSectionReport
	{
		public srptAccountingSummaryReversal()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.Name = "ActiveReport1";
            if (_InstancePtr == null)
				_InstancePtr = this;
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
        }

		public static srptAccountingSummaryReversal InstancePtr
		{
			get
			{
				return (srptAccountingSummaryReversal)Sys.GetInstance(typeof(srptAccountingSummaryReversal));
			}
		}

		protected srptAccountingSummaryReversal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTaxAmountInfo?.Dispose();
				rsAccountInfo?.Dispose();
                rsTaxAmountInfo = null;
                rsAccountInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAccountingSummaryReversal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Summary Type Binder Values
		// G - Gross Pay Section
		// T - Taxes Section
		// M - Employers Share of FICA and Medicare
		// D - Deductions Section
		// E - Employer's Match Section
		// P - Paid Section
		// C - Checking Account Section
		// X - Done witrh report
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		
		string strSummaryType = "";
		bool blnFirstRecord;
		string strHeaderType;
		// vbPorter upgrade warning: curCredits As Decimal	OnWrite(int, Decimal)
		Decimal curCredits;
		// vbPorter upgrade warning: curDebits As Decimal	OnWrite(int, Decimal)
		Decimal curDebits;
		// vbPorter upgrade warning: curPaidTotal As Decimal	OnWrite(int, Decimal)
		Decimal curPaidTotal;
		// vbPorter upgrade warning: curGrossPay As Decimal	OnWrite(int, Decimal)
		Decimal curGrossPay;
		// vbPorter upgrade warning: curFederalTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFederalTaxWH;
		// vbPorter upgrade warning: curFICATaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFICATaxWH;
		// vbPorter upgrade warning: curFicaMatch As Decimal	OnWrite(int, Decimal)
		Decimal curFicaMatch;
		// vbPorter upgrade warning: curMedMatch As Decimal	OnWrite(int, Decimal)
		Decimal curMedMatch;
		// vbPorter upgrade warning: curMedicareTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curMedicareTaxWH;
		// vbPorter upgrade warning: curStateTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curStateTaxWH;
		// vbPorter upgrade warning: curDeductionsWH As Decimal	OnWrite(int, Decimal)
		Decimal curDeductionsWH;
		bool blnShowedPaidLabel;
		Decimal curDeductionMatch;
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		bool boolFICAMedSameAccount;
		string strTableToUse = "";
		bool boolSeparateAccounts;
		string strTaxType = "";
		bool boolPreviousFiscal;
		int lngBank;
		// ***********************************************************
		// MATTHEW MULTI
		bool pboolMuliFund;
		bool boolTaxesAgain;
		clsDRWrapper rsTaxAmountInfo = new clsDRWrapper();
		// ***********************************************************
		bool boolSchoolTaxes;
		// vbPorter upgrade warning: dtPayDat As DateTime	OnWrite(string, DateTime)
		DateTime dtPayDat;
		int intPayRunID;
		// vbPorter upgrade warning: intArrayID As int	OnWriteFCConvert.ToInt32(
		int intArrayID;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("SummaryTypeBinder");
			boolPreviousFiscal = rptPayrollAccountingChargesReversal.InstancePtr.boolPreviousFiscal;
			modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
			modGlobalVariables.Statics.SortedAccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
			intArrayID = 0;
			if (modGlobalConstants.Statics.gboolBD)
			{
				modAccountTitle.SetAccountFormats();
			}
			lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank"))));
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				CheckRecord:
				;
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
                
                if (!boolTaxesAgain)
					rsAccountInfo.MoveNext();
                // ***********************************************************
                CheckRecord:
                ;
                if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["SummaryTypeBinder"].Value = strSummaryType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strAry As string()	OnRead(string, DateTime)
			string[] strAry = null;
			if (FCConvert.ToString(this.UserData) != string.Empty)
			{
				strTableToUse = FCConvert.ToString(this.UserData);
				strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
				strTableToUse = strAry[0];
				dtPayDat = FCConvert.ToDateTime(strAry[1]);
				intPayRunID = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
			}
			else
			{
				strTableToUse = "tblTempPayProcess";
				dtPayDat = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
			}
			strSummaryType = "G";
			strHeaderType = "G";
			blnFirstRecord = true;
			SetAccountInfoRecordset();
			curPaidTotal = 0;
			curCredits = 0;
			curDebits = 0;
			curGrossPay = 0;
			curFederalTaxWH = 0;
			curStateTaxWH = 0;
			curFICATaxWH = 0;
			curFicaMatch = 0;
			curMedMatch = 0;
			curMedicareTaxWH = 0;
			curDeductionsWH = 0;
			blnShowedPaidLabel = false;
			boolSeparateAccounts = false;
			
		}

		private void Detail_Format(object sender, EventArgs e)
        {
            Detail.Visible = true;
			if (strSummaryType == "G")
			{
				ShowGrossPayRecord();
			}
			else if (strSummaryType == "T")
			{
				ShowTaxRecord();
			}
			else if ((strSummaryType == "M") || (strSummaryType == "SM"))
			{
				ShowEmployerMatchMedicareTaxRecord();
				// ************
				// MATTHEW
			}
			else if ((strSummaryType == "F") || (strSummaryType == "SF"))
			{
				ShowEmployerMatchFICATaxRecord();
				// ************
			}
			else if (strSummaryType == "D")
			{
				ShowDeductionRecord();
			}
			else if (strSummaryType == "E")
			{
				ShowEmployerMatchRecord();
			}
			else if (strSummaryType == "P")
			{
				ShowTrustRecord();
			}
			else if (strSummaryType == "C")
			{
				ShowCheckingRecord();
			}
		}

		private void SetAccountInfoRecordset()
		{
			if (strSummaryType == "G")
			{
				rsAccountInfo.OpenRecordset("SELECT DistAccountNumber, SUM(DistGrossPay) as TotalPay FROM tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND distributionrecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " GROUP BY DistAccountNumber HAVING SUM(DistGrossPay) <> 0 ORDER BY DistAccountNumber");
			}
			else if (strSummaryType == "T")
			{
				GetPayrollAccountsInfo();
			}
			else if (strSummaryType == "M")
			{
				rsAccountInfo.Execute("Delete from tblTemptax", "twpy0000.vb1");
				// SetupEmployerMatchInfo
				modGlobalRoutines.SetupTempTaxTable(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate, rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID, "tblCheckDetail", modGlobalVariables.Statics.glngCheckNumberToVoid);
				rsAccountInfo.OpenRecordset("SELECT SUM(MedicareTax) AS MedicareTaxTotal,sum(EmployerMedicareTax) as EMployerMedicareTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(MedicareTax) <> 0 or sum(EmployerMedicareTax) <> 0 ORDER BY DeptDiv");
				// End If
			}
			else if (strSummaryType == "F")
			{
				// If Not boolFICAMedSameAccount Then
				rsAccountInfo.OpenRecordset("SELECT SUM(FICATax) AS FICATaxTotal,SUM(EmployerFicaTax) as EmployerFicaTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(FICATax) <> 0 or sum(EMployerFicaTax) <> 0 ORDER BY DeptDiv");
				// End If
				// ************
			}

			else if (strSummaryType == "SF")
			{
				rsAccountInfo.OpenRecordset("SELECT SUM(FICATax) AS FICATaxTotal,sum(EMployerFIcaTax) as EmployerFicaTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(FICATax) <> 0 or sum(EmployerFIcaTax)  <> 0 ORDER BY DeptDiv");
			}
			else if (strSummaryType == "D")
			{
				rsAccountInfo.OpenRecordset("SELECT DeductionAccountNumber, SUM(DedAmount) as TotalPay, DeductionTitle FROM tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND deductionrecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " and employeenumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' GROUP BY DeductionAccountNumber, DeductionTitle HAVING SUM(DedAmount) <> 0 ORDER BY DeductionAccountNumber");
			}
			else if (strSummaryType == "E")
			{
				rsAccountInfo.OpenRecordset("SELECT SUM(TotalMatchAmount) AS GrandTotalMatchAmount, MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber FROM (SELECT MatchGLAccountNumber as MatchAccountNumber, SUM(MatchAmount) * -1 as TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND matchrecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchGLAccountNumber UNION SELECT MatchAccountNumber, Sum(MatchAmount) AS TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND matchrecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " GROUP BY MatchAccountNumber, MatchDeductionTitle, MatchDeductionNumber) as temp GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber ORDER BY MatchAccountNumber, SUM(TotalMatchAmount)");
			}
			else if (strSummaryType == "P")
			{
				rsAccountInfo.OpenRecordset("SELECT TrustCategoryID, TrustDeductionAccountNumber, SUM(TrustAmount) as TrustAmountTotal, TrustDeductionDescription FROM tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND TrustRecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " GROUP BY TrustCategoryID, TrustDeductionDescription, TrustDeductionAccountNumber HAVING SUM(TrustAmount) <> 0 ORDER BY TrustCategoryID");
			}
			else if (strSummaryType == "C")
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
			}
		}

		private void IncrementSummaryType()
		{
			if (strSummaryType == "G")
			{
				strSummaryType = "T";
				strTaxType = "FT";
			}
			else if (strSummaryType == "T")
			{
                strSummaryType = "M";
			}
			else if (strSummaryType == "M")
			{
				// ************
				// MATTHEW
				// If boolFICAMedSameAccount Then
				// strSummaryType = "D"
				// Else
				strSummaryType = "F";
				// End If
			}
			else if (strSummaryType == "F")
			{
				// ************
				
					strSummaryType = "D";
				
			}
			else if (strSummaryType == "SM")
			{
				strSummaryType = "SF";
			}
			else if (strSummaryType == "SF")
			{
				strSummaryType = "D";
			}
			else if (strSummaryType == "D")
			{
				strSummaryType = "E";
			}
			else if (strSummaryType == "E")
			{
				strSummaryType = "P";
			}
			else if (strSummaryType == "P")
			{
				strSummaryType = "C";
			}
			else if (strSummaryType == "C")
			{

				strSummaryType = "X";
				
			}
			else if (strSummaryType == "SC")
			{
				strSummaryType = "X";
			}
		}

		private void ShowGrossPayRecord()
		{
			// - "AutoDim"
			fldDescription.Text = modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields_String("DistAccountNumber"));
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("DistAccountNumber"));
			fldCredits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")), "#,##0.00");
			fldDebits.Text = "";
			curCredits += rsAccountInfo.Get_Fields_Double("TotalPay").ToDecimal();
			curGrossPay += rsAccountInfo.Get_Fields_Double("TotalPay").ToDecimal();
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("distaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = rsAccountInfo.Get_Fields_Double("totalpay").ToDecimal();
		}

		private void ShowDeductionRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("DeductionTitle");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("DeductionAccountNumber"));
			fldDebits.Text = rsAccountInfo.Get_Fields_Double("TotalPay").ToDecimal().FormatAsCurrencyNoSymbol();
			fldCredits.Text = "";
			curDebits += rsAccountInfo.Get_Fields_Double("TotalPay").ToDecimal();
			curDeductionsWH += rsAccountInfo.Get_Fields_Double("TotalPay").ToDecimal();
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deductionaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = -FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")));
		}	

		private void ShowCheckingRecord()
		{
			// VB6 Bad Scope Dim:
			// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToDouble(
			int intStart;
            using (clsDRWrapper rsNetPay = new clsDRWrapper())
            {
                if (!modGlobalConstants.Statics.gboolBD)
                {
                    fldDescription.Text = "Checking";
                    fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields("Account"));

                    rsNetPay.OpenRecordset(
                        "SELECT SUM(NetPay) as NetPayTotal,MultiFundNumber FROM tblCheckDetail WHERE CheckNumber = " +
                        FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) +
                        " AND TotalRecord = 1 AND EmployeeNumber = '" +
                        rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' and PayDate = '" +
                        FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) +
                        "' AND PayRunID = " +
                        FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) +
                        " Group By MultiFundNumber");

                    // ******************************************************************************
                    if (rsNetPay.EndOfFile() != true && rsNetPay.BeginningOfFile() != true)
                    {
                        fldDebits.Text =
                            Strings.Format(rsNetPay.Get_Fields_Double("NetPayTotal").ToDecimal() + curPaidTotal,
                                "#,##0.00");
                        curDebits += rsNetPay.Get_Fields_Double("NetPayTotal").ToDecimal() + curPaidTotal;
                    }
                    else
                    {
                        fldDebits.Text = Strings.Format(curPaidTotal, "#,##0.00");
                        curDebits += curPaidTotal;
                    }

                    fldCredits.Text = "";
                    // ******************************************************************************
                    // MATTHEW MULTI CALL ID 79810 10/27/2005
                    string strText = "";
                    strText = FCConvert.ToString(rsAccountInfo.Get_Fields("account"));
                    if (pboolMuliFund)
                    {
                        // strText = rsAccountInfo.Fields("Account")
                        Strings.MidSet(ref strText, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length,
                            Strings.Format(rsNetPay.Get_Fields_Int32("MultiFundNumber"),
                                Strings.StrDup(
                                    FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))),
                                    "0")));
                        fldAccount.Text = strText;
                    }

                    if (modGlobalConstants.Statics.gboolBD)
                    {
                        string strOVAccount = "";
                        string strOVSCHAccount = "";
                        strOVAccount = "";
                        strOVSCHAccount = "";
                        if (boolPreviousFiscal)
                        {
                            clsDRWrapper rsTemp = new clsDRWrapper();
                            rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
                            if (!rsTemp.EndOfFile())
                            {
                                strOVAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
                            }

                            if (strOVAccount != string.Empty)
                            {
                                intStart = FCConvert.ToInt16(
                                    3 + Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1);
                                Strings.MidSet(ref strText, intStart,
                                    Strings.Left(modAccountTitle.Statics.Ledger, 3).Length, strOVAccount);
                                fldAccount.Text = strText;
                            }
                        }
                    }

                    // ******************************************************************************
                }
                else
                {
                    GetDTDFRecords();
                }
            }
        }

		private void ShowTrustRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("TrustDeductionDescription");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("TrustDeductionAccountNumber"));
			fldCredits.Text = rsAccountInfo.Get_Fields_Double("TrustAmountTotal").ToDecimal().FormatAsCurrencyNoSymbol();
			fldDebits.Text = "";
			curCredits += rsAccountInfo.Get_Fields_Double("TrustAmountTotal").ToDecimal();
			curPaidTotal += rsAccountInfo.Get_Fields_Double("TrustAmountTotal").ToDecimal();
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("trustdeductionaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = rsAccountInfo.Get_Fields_Double("TrustAmountTotal").ToDecimal();
		}

		private void ShowEmployerMatchRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("MatchDeductionTitle");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("MatchAccountNumber"));
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("matchaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			if (rsAccountInfo.Get_Fields("GrandTotalMatchAmount") > 0)
			{
				fldCredits.Text = rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal().FormatAsCurrencyNoSymbol();
				fldDebits.Text = "";
				curCredits += rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal();
			}
			else
			{
				fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal() * -1, "#,##0.00");
				fldCredits.Text = "";
				curDebits += FCConvert.ToDecimal(rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal() * -1);
				curDeductionMatch += (rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal() * -1);
			}
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = rsAccountInfo.Get_Fields_Double("GrandTotalMatchAmount").ToDecimal();
		}

		private void ShowTaxRecord()
		{
			// rsAmountInfo.OpenRecordset "SELECT SUM(FederalTaxWH) as FederalTaxTotal, SUM(StateTaxWH) as StateTaxTotal, SUM(LocalTaxWH) as LocalTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, SUM(FICATaxWH) as FICATaxTotal FROm tblCheckDetail WHERE TotalRecord = 1 AND PayDate = #" & rptPayrollAccountingCharges.datPayDate & "# AND PayRunID = " & rptPayrollAccountingCharges.intPayRunID
			// ***********************************************************
			// MATTHEW MULTI CALL ID 79810 10/27/2005
			if (!boolTaxesAgain)
			{
				
					rsTaxAmountInfo.OpenRecordset("SELECT Sum(FederalTaxWH) AS FederalTaxTotal, Sum(StateTaxWH) AS StateTaxTotal, Sum(LocalTaxWH) AS LocalTaxTotal, Sum(MedicareTaxWH) AS MedicareTaxTotal, Sum(FICATaxWH) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, MultiFundNumber From tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) + " AND TotalRecord = 1 AND EmployeeNumber = '" + rptPayrollAccountingChargesReversal.InstancePtr.strEmployeeNumber + "' AND PayDate = '" + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) + "' AND PayRunID = " + FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID) + " GROUP BY MultiFundNumber");
					if (rsTaxAmountInfo.RecordCount() > 1)
					{
						boolTaxesAgain = true;
					}
					else
					{
						boolTaxesAgain = false;
					}
				
			}
			// ***********************************************************
			double curAmt = 0;

			string vbPorterVar = rsAccountInfo.Get_Fields("Code");
			if (vbPorterVar == "FT")
			{
				fldDescription.Text = "Fed Tax W/H";
				fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields("Account"));
				fldDebits.Text = rsTaxAmountInfo.Get_Fields_Double("FederalTaxTotal").ToDecimal().FormatAsCurrencyNoSymbol();
				fldCredits.Text = "";
				curDebits += rsTaxAmountInfo.Get_Fields_Double("FederalTaxTotal").ToDecimal();
				curAmt = rsTaxAmountInfo.Get_Fields_Double("FederalTaxTotal");
				curFederalTaxWH += rsTaxAmountInfo.Get_Fields_Double("FederalTaxTotal").ToDecimal();
			}
			else if (vbPorterVar == "ST")
			{
				fldDescription.Text = "State Tax W/H";
				fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields("Account"));
				fldDebits.Text = rsTaxAmountInfo.Get_Fields_Double("StateTaxTotal").ToDecimal().FormatAsCurrencyNoSymbol();
				fldCredits.Text = "";
				curDebits += rsTaxAmountInfo.Get_Fields_Double("StateTaxTotal").ToDecimal();
				curStateTaxWH += rsTaxAmountInfo.Get_Fields_Double("StateTaxTotal").ToDecimal();
				curAmt = rsTaxAmountInfo.Get_Fields_Double("StateTaxTotal");
			}
			else if (vbPorterVar == "MW")
			{
				fldDescription.Text = "Medicare W/H";
				fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields("Account"));
				fldDebits.Text = (rsTaxAmountInfo.Get_Fields_Double("MedicareTaxTotal").ToDecimal() + rsTaxAmountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal()).FormatAsCurrencyNoSymbol();
				fldCredits.Text = "";
				curDebits += rsTaxAmountInfo.Get_Fields_Double("MedicareTaxTotal").ToDecimal() + rsTaxAmountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal();
				curAmt = rsTaxAmountInfo.Get_Fields_Double("MedicareTaxTotal") + rsTaxAmountInfo.Get_Fields_Double("EmployerMedicareTaxTotal");
				curMedicareTaxWH += rsTaxAmountInfo.Get_Fields_Double("MedicareTaxTotal").ToDecimal();
				curMedMatch += rsTaxAmountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal();
			}
			else if (vbPorterVar == "FW")
			{
				fldDescription.Text = "Soc Sec W/H";
				fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields("Account"));
				fldDebits.Text = (rsTaxAmountInfo.Get_Fields_Double("FICATaxTotal").ToDecimal() + rsTaxAmountInfo.Get_Fields_Double("employerficataxtotal").ToDecimal()).FormatAsCurrencyNoSymbol();
				fldCredits.Text = "";
				curAmt = rsTaxAmountInfo.Get_Fields_Double("FICATaxTotal") + rsTaxAmountInfo.Get_Fields_Double("employerficataxtotal");
				curDebits += rsTaxAmountInfo.Get_Fields_Double("FICATaxTotal").ToDecimal() + rsTaxAmountInfo.Get_Fields_Double("employerficataxtotal").ToDecimal();
				curFICATaxWH += rsTaxAmountInfo.Get_Fields_Double("FICATaxTotal").ToDecimal();
				curFicaMatch += rsTaxAmountInfo.Get_Fields_Double("employerficataxtotal").ToDecimal();
			}
			// ***********************************************************
			// MATTHEW MULTI CALL ID 79810 10/27/2005
			string strText = "";
			if (pboolMuliFund)
			{
				strText = fldAccount.Text;
				if (Strings.Left(fecherFoundation.Strings.Trim(fldAccount.Text), 2) == "**")
				{
					// corey 01/03/2006 Need to format fund
					Strings.MidSet(ref strText, 5, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(rsTaxAmountInfo.Get_Fields("multifundnumber"), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
				}
				else
				{
					Strings.MidSet(ref strText, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(rsTaxAmountInfo.Get_Fields_Int32("MultiFundNumber"), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
				}
				fldAccount.Text = strText;
			}
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("account"));
			if (strText != string.Empty)
			{
				modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
			}
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = curAmt.ToDecimal() * -1;
		
			if (!rsTaxAmountInfo.EndOfFile())
			{
				rsTaxAmountInfo.MoveNext();
				if (rsTaxAmountInfo.EndOfFile())
				{
					boolTaxesAgain = false;
				}
				else
				{
					boolTaxesAgain = true;
				}
			}
			
			// ***********************************************************
		}

		private void ShowEmployerMatchMedicareTaxRecord()
		{
			// - "AutoDim"
			fldDescription.Text = modAccountTitle.ReturnAccountDescription(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
			fldAccount.Text = ShowAccountCorrectly(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
			fldDebits.Text = "";
			fldCredits.Text = rsAccountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal().FormatAsCurrencyNoSymbol();
			curCredits += rsAccountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal();
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deptdiv"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = rsAccountInfo.Get_Fields_Double("EmployerMedicareTaxTotal").ToDecimal();
		}
		// ************
		// MATTHEW
		private void ShowEmployerMatchFICATaxRecord()
		{
			// - "AutoDim"
			if (boolFICAMedSameAccount)
			{
			}
			else
			{
				fldDescription.Text = modAccountTitle.ReturnAccountDescription(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
				fldAccount.Text = ShowAccountCorrectly(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
				fldDebits.Text = "";
				fldCredits.Text = rsAccountInfo.Get_Fields_Double("EmployerFICATaxTotal").ToDecimal().FormatAsCurrencyNoSymbol();
				curCredits += rsAccountInfo.Get_Fields_Double("EmployerFICATaxTotal").ToDecimal();
				Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
				intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
				modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deptdiv"));
				modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = rsAccountInfo.Get_Fields_Double("EmployerFICATaxTotal").ToDecimal();
			}
		}
		// ************
		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTotalDebits.Text = Strings.Format(curDebits, "#,##0.00");
			fldTotalCredits.Text = Strings.Format(curCredits, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (strSummaryType == "G")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Gross Pay" + Strings.StrDup(21, "-");
			}
			else if (strSummaryType == "T")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Taxes" + Strings.StrDup(25, "-");
			}
			else if (strSummaryType == "M")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "F")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "SM")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "SF")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "D")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Deductions" + Strings.StrDup(20, "-");
			}
			else if (strSummaryType == "E")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Employer Match" + Strings.StrDup(16, "-");
			}
			else if (strSummaryType == "C")
			{
				if (blnShowedPaidLabel)
				{
					lblTitle.Visible = false;
					lblTitle.Text = "";
				}
				else
				{
					lblTitle.Visible = true;
					lblTitle.Text = "Paid" + Strings.StrDup(26, "-");
					blnShowedPaidLabel = true;
				}
			}
			else
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Paid" + Strings.StrDup(26, "-");
				blnShowedPaidLabel = true;
			}
			strHeaderType = strSummaryType;
		}

		private void GetPayrollAccountsInfo()
		{
            using (clsDRWrapper rsAmountInfo = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "";

                rsAmountInfo.OpenRecordset(
                    "SELECT SUM(FederalTaxWH) as FederalTaxTotal, SUM(StateTaxWH) as StateTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, SUM(FICATaxWH) as FICATaxTotal FROm tblCheckDetail WHERE CheckNumber = " +
                    FCConvert.ToString(modGlobalVariables.Statics.glngCheckNumberToVoid) +
                    " AND TotalRecord = 1 AND PayDate = '" +
                    FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate) +
                    "' AND PayRunID = " +
                    FCConvert.ToString(rptPayrollAccountingChargesReversal.InstancePtr.intPayRunID));
                if (Conversion.Val(rsAmountInfo.Get_Fields("FederalTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'FT' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("StateTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'ST' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("MedicareTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'MW' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("FICATaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'FW' ";
                }

                // MATTHEW 3/22/2005
                // CALL ID 65616
                if (fecherFoundation.Strings.Trim(strSQL).Length > 3)
                {
                    strSQL = Strings.Right(strSQL, strSQL.Length - 3);
                    rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE " + strSQL + "ORDER BY ID");
                }
            }
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			
			fldGrossPay.Text = Strings.Format(curGrossPay * -1, "#,##0.00");
			fldFederalTaxWH.Text = Strings.Format(curFederalTaxWH * -1, "#,##0.00");
			fldStateTaxWH.Text = Strings.Format(curStateTaxWH * -1, "#,##0.00");
			fldFICATaxWH.Text = Strings.Format(curFICATaxWH * -1, "#,##0.00");
			fldMedicareTaxWH.Text = Strings.Format(curMedicareTaxWH * -1, "#,##0.00");
			fldDeductionsWH.Text = Strings.Format(curDeductionsWH * -1, "#,##0.00");
			fldNetPay.Text = Strings.Format((curGrossPay - curFederalTaxWH - curStateTaxWH - curFICATaxWH - curMedicareTaxWH - curDeductionsWH) * -1, "#,##0.00");
			fldFICAMatch.Text = Strings.Format(curFicaMatch * -1, "#,##0.00");
			fldMedicareMatch.Text = Strings.Format(curMedMatch * -1, "#,##0.00");
			fldDeductionMatch.Text = Strings.Format(curDeductionMatch * -1, "#,##0.00");
			fldPeriod.Text = Strings.Format(rptPayrollAccountingChargesReversal.InstancePtr.datPayDate.Month, "00");
			fldPeriod.Text = Strings.Format(frmCheckReturn.InstancePtr.lngPeriodNumber, "00");
			fldJournalNumber.Text = Strings.Format(frmCheckReturn.InstancePtr.lngJournalNumber, "0000");
		}
		
		private string ShowAccountCorrectly(string strAccount)
		{
			string ShowAccountCorrectly = "";
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			if (!modValidateAccount.AccountValidate(strAccount))
			{
				ShowAccountCorrectly = "**" + strAccount;
				rptPayrollAccountingChargesReversal.InstancePtr.blnShowBadAccountLabel = true;
			}
			else
			{
				ShowAccountCorrectly = strAccount;
			}
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
			return ShowAccountCorrectly;
		}
		
		private void GetDTDFRecords()
		{
            using (clsDRWrapper rspayrollaccounts = new clsDRWrapper())
            {
                // Dim rsSchoolAccounts As New clsDRWrapper
                double dblValue;
                string strCashAcct;
                string strSchoolCashAcct;
                dblValue = 0;
                rspayrollaccounts.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
                // rsPayrollAccounts.FindFirstRecord "Description", "'Cash / Checking'"
                strCashAcct = "";
                strSchoolCashAcct = "";
                if (!rspayrollaccounts.EndOfFile())
                {
                    strCashAcct = FCConvert.ToString(rspayrollaccounts.Get_Fields("account"));
                }
            }

            intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			string strOVAccount;
			string strOVSCHAccount;
			strOVAccount = "";
			strOVSCHAccount = "";
			if (boolPreviousFiscal)
			{
                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
                    if (!rsTemp.EndOfFile())
                    {
                        strOVAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
                    }

                    rsTemp.OpenRecordset("select * from standardaccounts where code = 'SAP'", "twbd0000.vb1");
                    if (!rsTemp.EndOfFile())
                    {
                        strOVSCHAccount = FCConvert.ToString(rsTemp.Get_Fields("account"));
                    }
                }
            }
			if (modGlobalConstants.Statics.gboolBD)
			{
				// this sets up the cash accounts for the funds if the user has BD
				modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
				// ftSchoolFundArray = CalcFundCash(AccountStrut(), True)
			}
			else
			{
				// create a cash entry into the account M CASH for these accounts
				modGlobalVariables.Statics.ftFundArray = modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
			}
			int lngTownCashIndex = 0;
			int lngSchoolCashIndex;
			int lngCurrCashIndex;
			int lngTempIndex;
			string strCurrAcct = "";
			string strTempAcct = "";
			int lngSwapIndex;
			string strAcctOR = "";
            strAcctOR = "";
            if (modGlobalConstants.Statics.gboolBD)
            {
                // strAcctOR = GetBankOverride(lngBank, True)
                strAcctOR = strOVAccount;
            }
            if (strAcctOR != string.Empty)
            {
                // had default cash account that may need to be split
                modBudgetaryAccounting.CalcCashFundsFromOverride(ref strAcctOR, ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(dtPayDat, "MM/dd/yyyy") + " PY", "PY", false);
            }
            else
            {
                if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(dtPayDat, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, lngTownCashIndex, true))
                {
                }
            }

			int x;
			string strDesc = "";
			string strCred = "";
			string strDeb = "";
			string strAcct = "";
			string strText = "";
			if (Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) > intArrayID)
			{
				for (x = intArrayID + 1; x <= Information.UBound(modGlobalVariables.Statics.AccountStrut, 1); x++)
				{
					strDesc += fecherFoundation.Strings.Trim(modGlobalVariables.Statics.AccountStrut[x].Description) + "\r\n";
					if (Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount) >= 0)
					{
						strCred += Strings.Format(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount), "#,###,##0.00") + "\r\n";
						curCredits += FCConvert.ToDecimal(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount));
						strDeb += "\r\n";
					}
					else
					{
						strCred += "\r\n";
						strDeb += Strings.Format(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount) * -1, "#,###,##0.00") + "\r\n";
						curDebits += FCConvert.ToDecimal(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount) * -1);
					}
					strText = modGlobalVariables.Statics.AccountStrut[x].Account;
					strAcct += ShowAccountCorrectly(strText) + "\r\n";
				}
				// x
			}
			fldAccount.Text = strAcct;
			fldDescription.Text = strDesc;
			fldDebits.Text = strDeb;
			fldCredits.Text = strCred;
		}
	}
}
