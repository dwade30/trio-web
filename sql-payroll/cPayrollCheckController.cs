//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;

namespace TWPY0000
{
	public class cPayrollCheckController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public cGenericCollection GetPayDistributionsByEmployeeDateRange(string strEmployeeNumber, string strStartDate, string strEndDate)
		{
			cGenericCollection GetPayDistributionsByEmployeeDateRange = null;
			return GetPayDistributionsByEmployeeDateRange;
		}

		private void FillPayDistribution(ref cPayDistribution payDist, ref clsDRWrapper rsLoad)
		{
			if (!(payDist == null) && !(rsLoad == null))
			{
				payDist.AccountingPeriod = rsLoad.Get_Fields_Int32("AccountingPeriod");
				payDist.AccountNumber = rsLoad.Get_Fields_String("DistAccountNumber");
				payDist.BasePayrate = Conversion.Val(rsLoad.Get_Fields_Double("BasePayRate"));
				payDist.CheckNumber = Conversion.Val(rsLoad.Get_Fields("CheckNumber"));
				payDist.DistE = rsLoad.Get_Fields_Boolean("DistE");
				payDist.DistLineCode = FCConvert.ToInt32(rsLoad.Get_Fields_String("DistLineCode"));
				payDist.DistM = rsLoad.Get_Fields_String("DistM");
				payDist.DistributionRecordID = rsLoad.Get_Fields_Int32("DistAutoID");
				payDist.DistU = rsLoad.Get_Fields_String("DistU");
				payDist.EmployeeName = rsLoad.Get_Fields_String("EmployeeName");
				payDist.EmployeeNumber = rsLoad.Get_Fields("EmployeeNumber");
				payDist.FicaTaxableGross = Conversion.Val(rsLoad.Get_Fields("FicaTaxGross"));
				payDist.GrossPay = Conversion.Val(rsLoad.Get_Fields("DistGrossPay"));
				payDist.Hours = Conversion.Val(rsLoad.Get_Fields("DistHours"));
				payDist.ID = rsLoad.Get_Fields("ID");
				payDist.IsAdjustmentRecord = rsLoad.Get_Fields_Boolean("AdjustRecord");
				payDist.IsDeleted = false;
				payDist.IsVoid = rsLoad.Get_Fields_Boolean("CheckVoid");
				payDist.JournalNumber = Conversion.Val(rsLoad.Get_Fields("JournalNumber"));
				payDist.MasterRecord = rsLoad.Get_Fields_String("MasterRecord");
				payDist.MedicareTaxableGross = Conversion.Val(rsLoad.Get_Fields("MedicareTaxGross"));
				payDist.MSRSID = rsLoad.Get_Fields_Int32("MSRSID");
				payDist.Multiplier = Conversion.Val(rsLoad.Get_Fields("distmult"));
				payDist.NumberTaxPeriods = Conversion.Val(rsLoad.Get_Fields("distnumtaxperiods"));
				payDist.PayCategory = rsLoad.Get_Fields_Int32("DistPayCategory");
				payDist.PayCode = Conversion.Val(rsLoad.Get_Fields("distpaycode"));
				payDist.PayDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PayDate"));
				payDist.PayRate = Conversion.Val(rsLoad.Get_Fields("distpayrate"));
				payDist.PayrunID = rsLoad.Get_Fields("Payrunid");
				payDist.ReportType = Conversion.Val(rsLoad.Get_Fields("reporttype"));
				payDist.SequenceNumber = rsLoad.Get_Fields_Int32("SequenceNumber");
				payDist.StatusCode = rsLoad.Get_Fields_String("StatusCode");
				payDist.TaxCode = rsLoad.Get_Fields("disttaxcode");
				payDist.WarrantNumber = rsLoad.Get_Fields_Int32("WarrantNumber");
				payDist.WCCode = rsLoad.Get_Fields_String("DistWCCode");
			}
		}
	}
}
