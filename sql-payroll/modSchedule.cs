﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modSchedule
	{
		//=========================================================
		public enum OverTimeOrder
		{
			InOrderWorked = 0,
			ScheduledFirst = 1,
		}

		public enum SchedulePaidOption
		{
			DayOfShiftStart = 0,
			DayWorked = 1,
		}

		public enum ScheduleShiftType
		{
			UserDefined = 0,
			// can't delete anything other than userdefined
			Regular = 1,
			Sick = 2,
			Vacation = 3,
			Overtime = 4,
			Holiday = 5,
			// can't edit > holiday
			GiftHoliday = 6,
			SecurityServices = 7,
			Court = 8,
			CallOut = 9,
		}

		public enum ShiftFactorType
		{
			Factor = 0,
			Units = 1,
		}

		public enum ShiftPerType
		{
			PerShift = 0,
			PerDay = 1,
			PerWeek = 2,
			PerPayPeriod = 3,
		}

		public static int DayOfYear(DateTime dtDate)
		{
			int DayOfYear = 0;
			int intReturn;
			// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
			int intYear;
			// vbPorter upgrade warning: intTemp As int	OnWrite(long)
			int intTemp;
			DateTime dtTemp;
			intYear = dtDate.Year;
			intTemp = fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime("1/1/" + FCConvert.ToString(intYear)), dtDate);
			intReturn = intTemp + 1;
			DayOfYear = intReturn;
			return DayOfYear;
		}

		public static int WeekOfYear(DateTime dtDate)
		{
			int WeekOfYear = 0;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp;
			// vbPorter upgrade warning: intReturn As int	OnWriteFCConvert.ToInt32(
			int intReturn;
			// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
			int intYear;
			intYear = dtDate.Year;
			intTemp = DayOfYear(dtDate);
			intTemp += fecherFoundation.DateAndTime.Weekday(FCConvert.ToDateTime("1/1/" + FCConvert.ToString(intYear)), DayOfWeek.Sunday) - 1;
			// day of year / 7 only works if year started on sunday
			intReturn = FCConvert.ToInt32(Conversion.Int(intTemp / 7.0));
			if (intTemp % 7 != 0)
			{
				// intReturn = (intTemp Mod 7) + 1
				intReturn += 1;
			}
			WeekOfYear = intReturn;
			return WeekOfYear;
		}

		public static DateTime GetWeekBeginningDate(DateTime dtDate)
		{
			DateTime GetWeekBeginningDate = System.DateTime.Now;
			DateTime dtTemp;
			// vbPorter upgrade warning: intDay As int	OnWriteFCConvert.ToInt32(
			int intDay;
			intDay = fecherFoundation.DateAndTime.Weekday(dtDate, DayOfWeek.Sunday);
			if (intDay != FCConvert.ToInt32(DayOfWeek.Sunday))
			{
				dtTemp = fecherFoundation.DateAndTime.DateAdd("d", FCConvert.ToInt32(DayOfWeek.Sunday) - intDay, dtDate);
			}
			else
			{
				dtTemp = dtDate;
			}
			GetWeekBeginningDate = dtTemp;
			return GetWeekBeginningDate;
		}

		public static void AdvanceWeekRotations()
		{
			clsScheduleList tSched = new clsScheduleList();
			tSched.LoadSchedules();
			if (tSched.AdvanceWeekRotations())
			{
				MessageBox.Show("Week rotations advanced one week", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public static void ReverseWeekRotations()
		{
			clsScheduleList tSched = new clsScheduleList();
			tSched.LoadSchedules();
			if (tSched.ReverseWeekRotations())
			{
				MessageBox.Show("Week rotations reversed one week", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
