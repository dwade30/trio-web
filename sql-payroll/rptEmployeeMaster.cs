//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeMaster.
	/// </summary>
	public partial class rptEmployeeMaster : BaseSectionReport
	{
		public rptEmployeeMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee Reports";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptEmployeeMaster InstancePtr
		{
			get
			{
				return (rptEmployeeMaster)Sys.GetInstance(typeof(rptEmployeeMaster));
			}
		}

		protected rptEmployeeMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsProcessing?.Dispose();
                rsProcessing = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmployeeMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// private local variables
		int intpage;
		int intCounter;
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		string strEmployee = "";
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
                int intW4Year = 0;

                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsProcessing.OpenRecordset(
                        "SELECT tblEmployeeMaster.*, tblPayStatuses.Description AS FedDes, tblPayStatuses_1.Description AS StateDes, tblPayStatuses_2.Description AS LocalDes FROM ((tblEmployeeMaster LEFT JOIN tblPayStatuses ON tblEmployeeMaster.FedFilingStatusID = tblPayStatuses.ID) LEFT JOIN tblPayStatuses AS tblPayStatuses_1 ON tblEmployeeMaster.StateFilingStatusID = tblPayStatuses_1.ID) LEFT JOIN tblPayStatuses AS tblPayStatuses_2 ON tblEmployeeMaster.LocalFilingStatusID = tblPayStatuses_2.ID WHERE tblEmployeeMaster.EmployeeNumber = '" +
                        strEmployee + "'", "TWPY0000.vb1");
                    if (!rsProcessing.EndOfFile())
                    {
                        if (FCConvert.ToString(rsProcessing.Get_Fields_String("Address2")) == string.Empty)
                        {
                            strTemp = rsProcessing.Get_Fields_String("Address1") + "\r\n" +
                                      rsProcessing.Get_Fields_String("City") + ", " +
                                      FCConvert.ToString(modGlobalRoutines.GetDefaultStateName(
                                          FCConvert.ToInt16(Conversion.Val(rsProcessing.Get_Fields_String("State"))))) +
                                      " " + rsProcessing.Get_Fields_String("zip");
                        }
                        else
                        {
                            strTemp = rsProcessing.Get_Fields_String("Address1") + "\r\n" +
                                      rsProcessing.Get_Fields_String("Address2") + "\r\n" +
                                      rsProcessing.Get_Fields_String("City") + ", " +
                                      FCConvert.ToString(modGlobalRoutines.GetDefaultStateName(
                                          FCConvert.ToInt32(Conversion.Val(rsProcessing.Get_Fields_String("State"))))) +
                                      " " + rsProcessing.Get_Fields_String("zip");
                        }

                        txtAddress.Text = strTemp;
                        txtStatus.Text = rsProcessing.Get_Fields_String("Status");
                        // txtEmployee.Text = "Employee  " & rsProcessing.Fields("EmployeeNumber")
                        txtFullName.Text = rsProcessing.Get_Fields_String("FirstName") + " " +
                                           rsProcessing.Get_Fields_String("LastName") + " " +
                                           rsProcessing.Get_Fields_String("Desig");
                        if (ssnViewPermission == SSNViewType.Full)
                        {
                            txtSSN.Text = rsProcessing.Get_Fields_String("SSN");
                        }
                        else if (ssnViewPermission == SSNViewType.Masked)
                        {
                            txtSSN.Text = "***-**-" + rsProcessing.Get_Fields_String("SSN").Right(4);
                        }
                        else
                        {
                            txtSSN.Text = "***-**-****";
                        }

                        txtDateHired.Text = FCConvert.ToString(rsProcessing.Get_Fields("DateHire"));
                        txtAnnDate.Text = rsProcessing.Get_Fields_String("DateAnniversary");
                    if (!rsProcessing.Get_Fields_DateTime("DateBirth").IsEmptyDate())
                    {
                        txtBirthDate.Text = rsProcessing.Get_Fields_DateTime("DateBirth").FormatAndPadShortDate();
                    }
                    else
                    {
                        txtBirthDate.Text = "";
                    }

                    txtFedTaxes.Text = FCConvert.ToString(rsProcessing.Get_Fields("AddFed"));
                        txtFedTaxesDesc.Text = rsProcessing.Get_Fields_String("FedDollarPercent");
                        txtStateTaxes.Text = FCConvert.ToString(rsProcessing.Get_Fields("AddState"));
                        txtStateTaxesDesc.Text = rsProcessing.Get_Fields_String("StateDollarPercent");
                        txtGroup.Text = rsProcessing.Get_Fields_String("Groupid");
                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("w2defincome")))
                        {
                            txtDeferredIncome.Text = "Yes";
                        }
                        else
                        {
                            txtDeferredIncome.Text = "No";
                        }

                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("workerscompexempt")))
                        {
                            txtWorkersCompExempt.Text = "Yes";
                        }
                        else
                        {
                            txtWorkersCompExempt.Text = "No";
                        }

                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("medicareexempt")))
                        {
                            txtMedicareExempt.Text = "Yes";
                        }
                        else
                        {
                            txtMedicareExempt.Text = "No";
                        }

                        txtDeptDiv.Text = rsProcessing.Get_Fields_String("deptdiv");
                        txtWCompCode.Text = rsProcessing.Get_Fields_String("workcompcode");
                        txtFedDep.Text = "    " + rsProcessing.Get_Fields_Int32("FedStatus");
                        txtStateDep.Text = "    " + rsProcessing.Get_Fields_Int32("StateStatus");
                        txtFedStatus.Text = rsProcessing.Get_Fields_String("FedDes");
                        txtStateStatus.Text = rsProcessing.Get_Fields_String("StateDes");

                        var tempDate = rsProcessing.Get_Fields_DateTime("W4Date");
                        if (tempDate == DateTime.FromOADate(0) || tempDate == DateTime.MinValue)
                        {
                            intW4Year = 2019;
                            txtW4Date.Text = "";
                        }
                        else
                        {
                            intW4Year = tempDate.Year;
                            txtW4Date.Text = tempDate.FormatAndPadShortDate();
                        }

                        if (intW4Year < 2020)
                        {
                            lblDependents.Text = "# of Dep";
                            txtOtherIncome.Text = "";
                            txtOtherDeduction.Text = "";
                            lblOtherIncome.Visible = false;
                            lblOtherDeduction.Visible = false;
                            lblMultipleJobs.Visible = false;
                            txtMultipleJobs.Text = "";
                        }
                        else
                        {
                            lblOtherIncome.Visible = true;
                            lblOtherDeduction.Visible = true;
                            lblDependents.Text = "# of QC/OD";
                            txtFedDep.Text = rsProcessing.Get_Fields_String("FedStatus") + @"/" +
                                             rsProcessing.Get_Fields_Int32("FederalOtherDependents").ToString();
                            txtOtherIncome.Text = "$" + rsProcessing.Get_Fields_Double("FederalOtherIncome")
                                .FormatAsCurrencyNoSymbol();
                            txtOtherDeduction.Text = "$" + rsProcessing.Get_Fields_Double("AddFederalDeduction")
                                .FormatAsCurrencyNoSymbol();
                            lblMultipleJobs.Visible = true;
                            if (rsProcessing.Get_Fields_Boolean("W4MultipleJobs"))
                            {
                                txtMultipleJobs.Text = "Yes";
                            }
                            else
                            {
                                txtMultipleJobs.Text = "No";
                            }
                        }

                        txtSex.Text = rsProcessing.Get_Fields_String("Sex");
                        txtSEQ.Text = FCConvert.ToString(rsProcessing.Get_Fields_Int32("SeqNumber"));
                        txtCode1.Text = rsProcessing.Get_Fields_String("Code1");
                        txtCode2.Text = rsProcessing.Get_Fields_String("Code1");
                        lblPartTime.Visible = rsProcessing.Get_Fields_Boolean("FTOrPT");
                        // ADD
                        txtPhone.Text = rsProcessing.Get_Fields_String("Phone");
                        txtEmail.Text = rsProcessing.Get_Fields_String("Email");
                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("w2pen")))
                        {
                            txtRetFlag.Text = "Yes";
                        }
                        else
                        {
                            txtRetFlag.Text = "No";
                        }

                        txtSCHHours.Text = "Day: " + rsProcessing.Get_Fields_Double("HrsDay") + "   Week: " +
                                           rsProcessing.Get_Fields_Double("HrsWk");
                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("unemploymentexempt")))
                        {
                            txtUnempExempt.Text = "Yes";
                        }
                        else
                        {
                            txtUnempExempt.Text = "No";
                        }

                        if (FCConvert.ToBoolean(rsProcessing.Get_Fields_Boolean("Ficaexempt")))
                        {
                            txtFICAExempt.Text = "Yes";
                        }
                        else
                        {
                            txtFICAExempt.Text = "No";
                        }

                        rsTemp.OpenRecordset("Select * from tblMiscUpdate where EmployeeNumber = '" + strEmployee +
                                             "'");
                        if (rsTemp.EndOfFile())
                        {
                        }
                        else
                        {
                            if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields("Unemployment"))) ==
                                "N")
                            {
                                txtStateUnemployment.Text = "No";
                            }
                            else if (fecherFoundation.Strings.UCase(
                                FCConvert.ToString(rsTemp.Get_Fields("Unemployment"))) == "Y")
                            {
                                txtStateUnemployment.Text = "Yes";
                            }
                            else if (fecherFoundation.Strings.UCase(
                                FCConvert.ToString(rsTemp.Get_Fields("Unemployment"))) == "S")
                            {
                                txtStateUnemployment.Text = "S";
                            }

                            txtNatureCode.Text = rsTemp.Get_Fields_String("NatureCode");
                        }

                        rsTemp.OpenRecordset(
                            "Select CD,BaseRate,PayCode,Rate from tblPayrollDistribution INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID where EmployeeNumber = '" +
                            strEmployee + "' and RecordNumber = 1");
                        if (rsTemp.EndOfFile())
                        {
                        }
                        else
                        {
                            if (FCConvert.ToInt32(rsTemp.Get_Fields_String("PayCode")) == 0)
                            {
                                txtPayRate.Text = Strings.Format(rsTemp.Get_Fields_Decimal("BaseRate"), "$0.00");
                            }
                            else
                            {
                                txtPayRate.Text = Strings.Format(rsTemp.Get_Fields("Rate"), "$0.00");
                            }
                        }

                        rsTemp.OpenRecordset("Select FrequencyCode from tblFrequencyCodes where ID = " +
                                             rsProcessing.Get_Fields("FreqCodeID"));
                        if (!rsTemp.EndOfFile())
                            txtPayFrequency.Text = rsTemp.Get_Fields_String("FrequencyCode");
                    }
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_ReportStart";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsProcessing.DefaultDB = "TWPY0000.vb1";
				modGlobalVariables.Statics.rsDeductionID.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
				strEmployee = FCConvert.ToString(this.UserData);
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        break;
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Payroll Direct Deposit";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
