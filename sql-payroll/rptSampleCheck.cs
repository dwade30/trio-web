//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSampleCheck.
	/// </summary>
	public partial class rptSampleCheck : BaseSectionReport
	{
		public rptSampleCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "SampleCheck";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSampleCheck InstancePtr
		{
			get
			{
				return (rptSampleCheck)Sys.GetInstance(typeof(rptSampleCheck));
			}
		}

		protected rptSampleCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSampleCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CheckUseCol = 2;
		const int CheckDescCol = 1;
		const int CheckXCol = 3;
		const int CheckYCol = 4;
		const int CheckTop = 4800;
		int intNumFields;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			string strFont = "";
			string strTemp = "";
			//clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
			GrapeCity.ActiveReports.SectionReportModel.TextBox ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            using (clsDRWrapper rsFieldInfo = new clsDRWrapper())
            {
                GrapeCity.ActiveReports.SectionReportModel.Shape ctl2 =
                    new GrapeCity.ActiveReports.SectionReportModel.Shape();

                Line1.Y1 = CheckTop;
                Line1.Y2 = CheckTop;
                lblHelp.Top = CheckTop - lblHelp.Height;
                // create the fields
                intNumFields = 0;
                for (X = 1; X <= (frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.Rows - 1); X++)
                {
                    if (FCConvert.CBool(frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(X, CheckUseCol))
                    )
                    {
                        intNumFields += 1;
                        strTemp = "CustomField" + FCConvert.ToString(intNumFields);
                        ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>(strTemp);
                        ctl.Top = FCConvert.ToSingle(
                            240 * Conversion.Val(
                                frmCreateCustomCheckFormat.InstancePtr.vsCheckFields
                                    .TextMatrix(X, CheckYCol)) + CheckTop) / 1440F;
                        ctl.Left = FCConvert.ToSingle(
                            144 * Conversion.Val(
                                frmCreateCustomCheckFormat.InstancePtr.vsCheckFields
                                    .TextMatrix(X, CheckXCol))) / 1440F;
                        ctl.WordWrap = false;
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                        rsFieldInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = '" +
                                                  frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(X,
                                                      CheckDescCol) + "'");
                        if (rsFieldInfo.EndOfFile() != true && rsFieldInfo.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) > 0 &&
                                Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) > 0)
                            {
                                ctl.Height = rsFieldInfo.Get_Fields_Int16("FieldHeight");
                                ctl.Width = rsFieldInfo.Get_Fields_Int16("FieldWidth");
                                ctl2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
                                ctl2.Top = ctl.Top;
                                ctl2.Left = ctl.Left;
                                ctl2.Height = ctl.Height;
                                ctl2.Width = ctl.Width;
                                Detail.Controls.Add(ctl2);
                            }
                            else
                            {
                                ctl.Width = 2880 / 1440F;
                                ctl.Height = 240 / 1440F;
                            }
                        }

                        //strTemp = "CustomField" + FCConvert.ToString(intNumFields);
                        //// ctl.Name = "HeaderRow" & CRow & "Col" & CCol
                        //ctl.Name = strTemp;
                        Fields.Add(ctl.Name);
                        // ctl.DataField = ctl.Name
                        if (strFont != string.Empty)
                        {
                            ctl.Font = new Font(strFont, ctl.Font.Size);
                        }

                        ctl.Text = frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(X, CheckDescCol);
                        //Detail.Controls.Add(ctl);
                    }
                }
            }
        }

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// Unload Me
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// Dim x As Integer
			// 
			// For x = 1 To intNumFields
			// Next x
		}

		
	}
}
