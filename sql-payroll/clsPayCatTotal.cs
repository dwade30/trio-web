﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class clsPayCatTotal
	{
		//=========================================================
		private int lngPayCat;
		private double dblScheduledHours;
		private double dblActualHours;
		private double dblHoursPaid;
		private double dblAmountPaid;
		private double dblDayScheduledHours;
		private double dblDayActualHours;
		private double dblDayHoursPaid;
		private double dblDayAmountPaid;
		private double dblShiftScheduledHours;
		private double dblShiftActualHours;
		private double dblShiftHoursPaid;
		private double dblShiftAmountPaid;
		private FCCollection BreakDownList = new FCCollection();
		private string strAcct = string.Empty;

		public void Clear()
		{
			dblScheduledHours = 0;
			dblActualHours = 0;
			dblHoursPaid = 0;
			dblAmountPaid = 0;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					BreakDownList.Remove(1);
				}
				// x
			}
			ClearDay();
			ClearShift();
			// If Not IsEmpty(BreakDownList) Then
			// Dim x As Integer
			// Dim tCat As clsPayCatTotal
			// For x = 1 To BreakDownList.Count
			// Set tCat = BreakDownList(x)
			// If Not tCat Is Nothing Then
			// tCat.ClearDay
			// tCat.ClearShift
			// End If
			// Next x
			// End If
		}

		public void ClearDay()
		{
			dblDayScheduledHours = 0;
			dblDayActualHours = 0;
			dblDayHoursPaid = 0;
			dblDayAmountPaid = 0;
			ClearShift();
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				clsPayCatTotal tCat;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						tCat.ClearDay();
					}
				}
				// x
			}
		}

		public void ClearShift()
		{
			dblShiftScheduledHours = 0;
			dblShiftActualHours = 0;
			dblShiftHoursPaid = 0;
			dblShiftAmountPaid = 0;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				clsPayCatTotal tCat;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						tCat.ClearShift();
					}
				}
				// x
			}
		}

		public string Account
		{
			set
			{
				strAcct = value;
			}
			get
			{
				string Account = "";
				Account = strAcct;
				return Account;
			}
		}

		public int PayCat
		{
			set
			{
				lngPayCat = value;
			}
			get
			{
				int PayCat = 0;
				PayCat = lngPayCat;
				return PayCat;
			}
		}

		public int BreakDownCount
		{
			get
			{
				int BreakDownCount = 0;
				int intReturn;
				intReturn = 0;
				if (!FCUtils.IsEmpty(BreakDownList))
				{
					intReturn = BreakDownList.Count;
				}
				BreakDownCount = intReturn;
				return BreakDownCount;
			}
		}

		public clsPayCatTotal Get_BreakdownByIndex(int intIndex)
		{
			clsPayCatTotal BreakdownByIndex = null;
			clsPayCatTotal tCat;
			tCat = null;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				if (intIndex > 0 && intIndex <= BreakDownList.Count)
				{
					tCat = BreakDownList[intIndex];
				}
			}
			BreakdownByIndex = tCat;
			return BreakdownByIndex;
		}

		public void Set_BreakdownScheduledHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ScheduledHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ScheduledHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownScheduledHours(string strValue)
		{
			double BreakdownScheduledHours = 0;
			if (strValue == "")
				BreakdownScheduledHours = dblScheduledHours;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							ScheduledHours = tCat.ScheduledHours;
							return BreakdownScheduledHours;
						}
					}
				}
				// x
			}
			return BreakdownScheduledHours;
		}

		public double ScheduledHours
		{
			set
			{
				dblScheduledHours = value;
			}
			get
			{
				double ScheduledHours = 0;
				ScheduledHours = dblScheduledHours;
				return ScheduledHours;
			}
		}

		public double ActualHours
		{
			set
			{
				dblActualHours = value;
			}
			get
			{
				double ActualHours = 0;
				ActualHours = dblActualHours;
				return ActualHours;
			}
		}

		public void Set_BreakdownActualHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ActualHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ActualHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownActualHours(string strValue)
		{
			double BreakdownActualHours = 0;
			// If strValue = "" Then BreakdownActualHours = dblActualHours
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownActualHours = tCat.ActualHours;
							return BreakdownActualHours;
						}
					}
				}
				// x
			}
			return BreakdownActualHours;
		}

		public void Set_BreakdownHoursPaid(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.HoursPaid = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.HoursPaid = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownHoursPaid(string strValue)
		{
			double BreakdownHoursPaid = 0;
			if (strValue == "")
				BreakdownHoursPaid = dblHoursPaid;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownHoursPaid = tCat.HoursPaid;
							return BreakdownHoursPaid;
						}
					}
				}
				// x
			}
			return BreakdownHoursPaid;
		}

		public double HoursPaid
		{
			set
			{
				dblHoursPaid = value;
			}
			get
			{
				double HoursPaid = 0;
				HoursPaid = dblHoursPaid;
				return HoursPaid;
			}
		}

		public double AmountPaid
		{
			set
			{
				dblAmountPaid = value;
			}
			get
			{
				double AmountPaid = 0;
				AmountPaid = dblAmountPaid;
				return AmountPaid;
			}
		}

		public void Set_BreakdownAmountPaid(string strValue, double dblAmount)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.AmountPaid = dblAmount;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.AmountPaid = dblAmount;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownAmountPaid(string strValue)
		{
			double BreakdownAmountPaid = 0;
			if (strValue == "")
				BreakdownAmountPaid = dblAmountPaid;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownAmountPaid = tCat.AmountPaid;
							return BreakdownAmountPaid;
						}
					}
				}
				// x
			}
			return BreakdownAmountPaid;
		}

		public double DayScheduledHours
		{
			set
			{
				dblDayScheduledHours = value;
			}
			get
			{
				double DayScheduledHours = 0;
				DayScheduledHours = dblDayScheduledHours;
				return DayScheduledHours;
			}
		}

		public void Set_BreakdownDayScheduledHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.DayScheduledHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.DayScheduledHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownDayScheduledHours(string strValue)
		{
			double BreakdownDayScheduledHours = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownDayScheduledHours = tCat.DayScheduledHours;
							return BreakdownDayScheduledHours;
						}
					}
				}
				// x
			}
			return BreakdownDayScheduledHours;
		}

		public double DayActualHours
		{
			set
			{
				dblDayActualHours = value;
			}
			get
			{
				double DayActualHours = 0;
				DayActualHours = dblDayActualHours;
				return DayActualHours;
			}
		}

		public void Set_BreakdownDayActualHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.DayActualHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.DayActualHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownDayActualHours(string strValue)
		{
			double BreakdownDayActualHours = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownDayActualHours = tCat.DayActualHours;
							return BreakdownDayActualHours;
						}
					}
				}
				// x
			}
			return BreakdownDayActualHours;
		}

		public double DayHoursPaid
		{
			set
			{
				dblDayHoursPaid = value;
			}
			get
			{
				double DayHoursPaid = 0;
				DayHoursPaid = dblDayHoursPaid;
				return DayHoursPaid;
			}
		}

		public void Set_BreakdownDayHoursPaid(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.DayHoursPaid = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.DayHoursPaid = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownDayHoursPaid(string strValue)
		{
			double BreakdownDayHoursPaid = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownDayHoursPaid = tCat.DayHoursPaid;
							return BreakdownDayHoursPaid;
						}
					}
				}
				// x
			}
			return BreakdownDayHoursPaid;
		}

		public double DayAmountPaid
		{
			set
			{
				dblDayAmountPaid = value;
			}
			get
			{
				double DayAmountPaid = 0;
				DayAmountPaid = dblDayAmountPaid;
				return DayAmountPaid;
			}
		}

		public void Set_BreakdownDayAmountPaid(string strValue, double dblAmount)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.DayAmountPaid = dblAmount;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.DayAmountPaid = dblAmount;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownDayAmountPaid(string strValue)
		{
			double BreakdownDayAmountPaid = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownDayAmountPaid = tCat.DayAmountPaid;
							return BreakdownDayAmountPaid;
						}
					}
				}
				// x
			}
			return BreakdownDayAmountPaid;
		}

		public double ShiftAmountPaid
		{
			set
			{
				dblShiftAmountPaid = value;
			}
			get
			{
				double ShiftAmountPaid = 0;
				ShiftAmountPaid = dblShiftAmountPaid;
				return ShiftAmountPaid;
			}
		}

		public void Set_BreakdownShiftAmountPaid(string strValue, double dblAmount)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ShiftAmountPaid = dblAmount;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ShiftAmountPaid = dblAmount;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownShiftAmountPaid(string strValue)
		{
			double BreakdownShiftAmountPaid = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownShiftAmountPaid = tCat.ShiftAmountPaid;
							return BreakdownShiftAmountPaid;
						}
					}
				}
				// x
			}
			return BreakdownShiftAmountPaid;
		}

		public double ShiftScheduledHours
		{
			set
			{
				dblShiftScheduledHours = value;
			}
			get
			{
				double ShiftScheduledHours = 0;
				ShiftScheduledHours = dblShiftScheduledHours;
				return ShiftScheduledHours;
			}
		}

		public void Set_BreakdownShiftScheduledHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ShiftScheduledHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ShiftScheduledHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownShiftScheduledHours(string strValue)
		{
			double BreakdownShiftScheduledHours = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownShiftScheduledHours = tCat.ShiftScheduledHours;
							return BreakdownShiftScheduledHours;
						}
					}
				}
				// x
			}
			return BreakdownShiftScheduledHours;
		}

		public double ShiftActualHours
		{
			set
			{
				dblShiftActualHours = value;
			}
			get
			{
				double ShiftActualHours = 0;
				ShiftActualHours = dblShiftActualHours;
				return ShiftActualHours;
			}
		}

		public void Set_BreakdownShiftActualHours(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ShiftActualHours = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ShiftActualHours = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownShiftActualHours(string strValue)
		{
			double BreakdownShiftActualHours = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownShiftActualHours = tCat.ShiftActualHours;
							return BreakdownShiftActualHours;
						}
					}
				}
				// x
			}
			return BreakdownShiftActualHours;
		}

		public double ShiftHoursPaid
		{
			set
			{
				dblShiftHoursPaid = value;
			}
			get
			{
				double ShiftHoursPaid = 0;
				ShiftHoursPaid = dblShiftHoursPaid;
				return ShiftHoursPaid;
			}
		}

		public void Set_BreakdownShiftHoursPaid(string strValue, double dblHours)
		{
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							tCat.ShiftHoursPaid = dblHours;
							return;
						}
					}
				}
				// x
			}
			// didn't find it
			tCat = new clsPayCatTotal();
			tCat.Account = strValue;
			tCat.ShiftHoursPaid = dblHours;
			tCat.PayCat = PayCat;
			BreakDownList.Add(tCat);
		}

		public double Get_BreakdownShiftHoursPaid(string strValue)
		{
			double BreakdownShiftHoursPaid = 0;
			clsPayCatTotal tCat;
			if (!FCUtils.IsEmpty(BreakDownList))
			{
				int x;
				for (x = 1; x <= BreakDownList.Count; x++)
				{
					tCat = BreakDownList[x];
					if (!(tCat == null))
					{
						if (tCat.Account == strValue)
						{
							BreakdownShiftHoursPaid = tCat.ShiftHoursPaid;
							return BreakdownShiftHoursPaid;
						}
					}
				}
				// x
			}
			return BreakdownShiftHoursPaid;
		}
	}
}
