//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cCustomCheckController
	{
		//=========================================================
		public cCustomCheckFormat GetFormat(int lngID)
		{
			cCustomCheckFormat GetFormat = null;
			cCustomCheckFormat customFormat = new cCustomCheckFormat();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from customchecks where id = " + FCConvert.ToString(lngID), "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				customFormat.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
				customFormat.CheckPosition = FCConvert.ToString(rsLoad.Get_Fields("checkposition"));
				customFormat.CheckType = FCConvert.ToString(rsLoad.Get_Fields("CheckType"));
				customFormat.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
				customFormat.ScannedCheckFile = FCConvert.ToString(rsLoad.Get_Fields_String("ScannedCheckFile"));
				customFormat.UseDoubleStub = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseDoubleStub"));
				customFormat.UseTwoStubs = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseTwoStubs"));
				customFormat.VoidAfterMessage = FCConvert.ToString(rsLoad.Get_Fields_String("VoidAfterMessage"));
				customFormat.ShowDistributionRate = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ShowIndividualPayRates"));
				FillDetails(ref customFormat);
			}
			GetFormat = customFormat;
			return GetFormat;
		}

		public cGenericCollection GetFormats()
		{
			cGenericCollection GetFormats = null;
			cGenericCollection colFormats = new cGenericCollection();
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from customchecks order by description", "Payroll");
			cCustomCheckFormat customFormat;
			while (!rsLoad.EndOfFile())
			{
				customFormat = new cCustomCheckFormat();
				customFormat.ID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
				customFormat.CheckPosition = FCConvert.ToString(rsLoad.Get_Fields("checkposition"));
				customFormat.CheckType = FCConvert.ToString(rsLoad.Get_Fields("CheckType"));
				customFormat.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
				customFormat.ScannedCheckFile = FCConvert.ToString(rsLoad.Get_Fields_String("ScannedCheckFile"));
				customFormat.UseDoubleStub = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseDoubleStub"));
				customFormat.UseTwoStubs = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseTwoStubs"));
				customFormat.VoidAfterMessage = FCConvert.ToString(rsLoad.Get_Fields_String("VoidAfterMessage"));
				customFormat.ShowDistributionRate = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ShowIndividualPayRates"));
				FillDetails(ref customFormat);
				colFormats.AddItem(customFormat);
				rsLoad.MoveNext();
			}
			GetFormats = colFormats;
			return GetFormats;
		}

		private void FillDetails(ref cCustomCheckFormat checkFormat)
		{
			if (!(checkFormat == null))
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from CustomCheckFields where FormatID = " + checkFormat.ID, "Payroll");
				cCustomCheckField checkField;
				checkFormat.CheckFields.ClearList();
				while (!rsLoad.EndOfFile())
				{
					checkField = new cCustomCheckField();
					checkField.CheckType = FCConvert.ToString(rsLoad.Get_Fields("CheckType"));
					checkField.ControlName = FCConvert.ToString(rsLoad.Get_Fields_String("ControlName"));
					checkField.DefaultXPosition = Conversion.Val(rsLoad.Get_Fields_Double("DefaultXPosition"));
					checkField.DefaultYPosition = Conversion.Val(rsLoad.Get_Fields_Double("DefaultYPosition"));
					checkField.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
					checkField.FieldHeight = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("FieldHeight"))));
					checkField.FieldWidth = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("FieldWidth"))));
					checkField.FormatID = FCConvert.ToInt32(rsLoad.Get_Fields("FormatID"));
					checkField.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					checkField.IncludeField = FCConvert.ToBoolean(rsLoad.Get_Fields("Include"));
					checkField.XPosition = Conversion.Val(rsLoad.Get_Fields_Double("XPosition"));
					checkField.YPosition = Conversion.Val(rsLoad.Get_Fields_Double("YPosition"));
					checkFormat.CheckFields.AddItem(checkField);
					rsLoad.MoveNext();
				}
			}
		}
	}
}
