//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDirectDepositSetup.
	/// </summary>
	public partial class rptDirectDepositSetup : BaseSectionReport
	{
		public rptDirectDepositSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Direct Deposit Setup Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDirectDepositSetup InstancePtr
		{
			get
			{
				return (rptDirectDepositSetup)Sys.GetInstance(typeof(rptDirectDepositSetup));
			}
		}

		protected rptDirectDepositSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDirectDepositSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strAcctType = "";
				while (!rsData.EndOfFile())
				{
					if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
					{
						rsData.MoveNext();
					}
					else
					{
						break;
					}
				}
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				if (FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")).Length >= 8)
				{
					txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + " - " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
				}
				else
				{
					txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(8 - FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")).Length, " ") + " - " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
				}
				BankNumData.Text = FCConvert.ToString(rsData.Get_Fields_Int32("RecordNumber"));
				txtBankName.Text = rsData.Get_Fields_String("Name");
				txtAmount.Text = Strings.Format(rsData.Get_Fields("Amount"), "0.00");
				txtAmountType.Text = rsData.Get_Fields_String("Type");
                var bankAccount = "";
                switch (bankAccountViewPermission)
                {
                    case BankAccountViewType.Full:
                        bankAccount = rsData.Get_Fields_String("Account");
                        break;
                    case BankAccountViewType.Masked:
                        bankAccount = "******" + rsData.Get_Fields_String("Account").Right(4);
                        break;
                    default:
                        bankAccount = "**********";
                        break;
                }
				txtAccount.Text = bankAccount;
				if (Strings.Left(rsData.Get_Fields("accounttype") + " ", 1) == "3")
				{
					if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("prenote")))
					{
						strAcctType = "32 - Savings";
					}
					else
					{
						strAcctType = "33 - Savings Prenote";
					}
				}
				else
				{
					if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Prenote")))
					{
						strAcctType = "22 - Checking";
					}
					else
					{
						strAcctType = "23 - Checking Prenote";
					}
				}
				txtAccountType.Text = strAcctType;
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intRow As object	OnWrite
			object intRow;
			// - "AutoDim"
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			intpage = 0;
			rsData.OpenRecordset("SELECT tblBanks.Name,tblBanks.RecordNumber, tblDirectDeposit.*, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName FROM (tblDirectDeposit INNER JOIN tblBanks ON tblDirectDeposit.Bank = tblBanks.ID) INNER JOIN tblEmployeeMaster ON tblDirectDeposit.EmployeeNumber = tblEmployeeMaster.EmployeeNumber ORDER BY tblemployeemaster.lastname,tblemployeemaster.firstname,TBLDIRECTDEPOSIT.ROWNUMBER", modGlobalVariables.DEFAULTDATABASE);
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewBankAccountNumbers.ToInteger()))
            {
                case "F":
                    bankAccountViewPermission = BankAccountViewType.Full;
                    break;
                case "P":
                    bankAccountViewPermission = BankAccountViewType.Masked;
                    break;
                default:
                    bankAccountViewPermission = BankAccountViewType.None;
                    break;
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}
	}
}
