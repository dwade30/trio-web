//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmBanks.
	/// </summary>
	partial class frmBanks
	{
		public fecherFoundation.FCGrid vsBanks;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdExit;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsBanks = new fecherFoundation.FCGrid();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdRefresh = new fecherFoundation.FCButton();
			this.cmdExit = new fecherFoundation.FCButton();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBanks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 376);
			this.BottomPanel.Size = new System.Drawing.Size(637, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBanks);
			this.ClientArea.Controls.Add(this.cmdRefresh);
			this.ClientArea.Controls.Add(this.cmdExit);
			this.ClientArea.Size = new System.Drawing.Size(637, 316);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(637, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(242, 30);
			this.HeaderText.Text = "Direct Deposit Banks";
			// 
			// vsBanks
			// 
			this.vsBanks.AllowSelection = false;
			this.vsBanks.AllowUserToResizeColumns = false;
			this.vsBanks.AllowUserToResizeRows = false;
			this.vsBanks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsBanks.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBanks.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBanks.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBanks.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBanks.BackColorSel = System.Drawing.Color.Empty;
			this.vsBanks.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsBanks.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBanks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBanks.ColumnHeadersHeight = 30;
			this.vsBanks.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBanks.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBanks.DragIcon = null;
			this.vsBanks.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsBanks.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBanks.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBanks.FrozenCols = 0;
			this.vsBanks.GridColor = System.Drawing.Color.Empty;
			this.vsBanks.GridColorFixed = System.Drawing.Color.Empty;
			this.vsBanks.Location = new System.Drawing.Point(30, 30);
			this.vsBanks.Name = "vsBanks";
			this.vsBanks.OutlineCol = 0;
			this.vsBanks.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBanks.RowHeightMin = 0;
			this.vsBanks.Rows = 50;
			this.vsBanks.ScrollTipText = null;
			this.vsBanks.ShowColumnVisibilityMenu = false;
			this.vsBanks.Size = new System.Drawing.Size(579, 277);
			this.vsBanks.StandardTab = true;
			this.vsBanks.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsBanks.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBanks.TabIndex = 0;
			this.vsBanks.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsBanks_KeyDownEdit);
			this.vsBanks.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsBanks_AfterEdit);
			this.vsBanks.CurrentCellChanged += new System.EventHandler(this.vsBanks_RowColChange);
			this.vsBanks.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsBanks_MouseMoveEvent);
			this.vsBanks.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBanks_KeyDownEvent);
			this.vsBanks.Enter += new System.EventHandler(this.vsBanks_Enter);
			this.vsBanks.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsBanks_ClickEvent);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(269, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(85, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdRefresh
			// 
			this.cmdRefresh.AppearanceKey = "toolbarButton";
			this.cmdRefresh.Location = new System.Drawing.Point(320, 129);
			this.cmdRefresh.Name = "cmdRefresh";
			this.cmdRefresh.Size = new System.Drawing.Size(114, 23);
			this.cmdRefresh.TabIndex = 4;
			this.cmdRefresh.Text = "Refresh";
			this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
			// 
			// cmdExit
			// 
			this.cmdExit.AppearanceKey = "toolbarButton";
			this.cmdExit.Location = new System.Drawing.Point(320, 177);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.Size = new System.Drawing.Size(114, 23);
			this.cmdExit.TabIndex = 6;
			this.cmdExit.Text = "Exit";
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP1,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP3,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New Bank";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 2;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Enabled = false;
			this.mnuPrint.Index = 3;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Visible = false;
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 4;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print/Preview";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 5;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 6;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                            ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 7;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit            ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = 8;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 9;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(357, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Shortcut.Insert;
			this.cmdNew.Size = new System.Drawing.Size(81, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New Bank";
			this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(444, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(59, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(509, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(100, 24);
			this.cmdPrint.TabIndex = 3;
			this.cmdPrint.Text = "Print/Preview";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// frmBanks
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(637, 484);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBanks";
			this.Text = "Direct Deposit Banks";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmBanks_Load);
			this.Activated += new System.EventHandler(this.frmBanks_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBanks_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBanks_KeyPress);
			this.Resize += new System.EventHandler(this.frmBanks_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBanks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdPrint;
        private FCButton cmdDelete;
        private FCButton cmdNew;
    }
}