﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPYTaxStatusCode
	{
		//=========================================================
		private int lngRecordID;
		private string strTaxStatusCode = string.Empty;
		private string strDescription = string.Empty;
		private bool boolFedExempt;
		private bool boolFicaExempt;
		private bool boolMedicareExempt;
		private bool boolStateExempt;
		private string strLastUserID = string.Empty;
		private string strLastUpdate = "";
		private bool boolPertains;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string TaxStatusCode
		{
			set
			{
				strTaxStatusCode = value;
			}
			get
			{
				string TaxStatusCode = "";
				TaxStatusCode = strTaxStatusCode;
				return TaxStatusCode;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public bool IsFederalExempt
		{
			set
			{
				boolFedExempt = value;
			}
			get
			{
				bool IsFederalExempt = false;
				IsFederalExempt = boolFedExempt;
				return IsFederalExempt;
			}
		}

		public bool IsMedicareExempt
		{
			set
			{
				boolMedicareExempt = value;
			}
			get
			{
				bool IsMedicareExempt = false;
				IsMedicareExempt = boolMedicareExempt;
				return IsMedicareExempt;
			}
		}

		public bool IsStateExempt
		{
			set
			{
				boolStateExempt = value;
			}
			get
			{
				bool IsStateExempt = false;
				IsStateExempt = boolStateExempt;
				return IsStateExempt;
			}
		}

		public string LastUserID
		{
			set
			{
				strLastUserID = value;
			}
			get
			{
				string LastUserID = "";
				LastUserID = strLastUserID;
				return LastUserID;
			}
		}

		public string LastUpdate
		{
			set
			{
				if (fecherFoundation.Information.IsDate(value))
				{
					strLastUpdate = value;
				}
				else
				{
					strLastUpdate = "";
				}
			}
			get
			{
				string LastUpdate = "";
				LastUpdate = strLastUpdate;
				return LastUpdate;
			}
		}

		public bool Pertains
		{
			set
			{
				boolPertains = value;
			}
			get
			{
				bool Pertains = false;
				Pertains = boolPertains;
				return Pertains;
			}
		}
	}
}
