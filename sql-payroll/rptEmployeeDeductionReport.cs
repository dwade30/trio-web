//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeDeductionReport.
	/// </summary>
	public partial class rptEmployeeDeductionReport : BaseSectionReport
	{
		public rptEmployeeDeductionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee Deduction Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptEmployeeDeductionReport InstancePtr
		{
			get
			{
				return (rptEmployeeDeductionReport)Sys.GetInstance(typeof(rptEmployeeDeductionReport));
			}
		}

		protected rptEmployeeDeductionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmployeeDeductionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper clsDeduction = new clsDRWrapper();
		string strSQL1 = "";
		string strSQL2 = "";
		int intpage;
		bool boolHistorical;
		string strSQLMatch = "";
		bool boolWeekly;
		bool boolMonthly;
		bool boolQuarterly;
		bool boolYearly;
		string strOrderBy2 = "";
		string strIncludeWhere;
		// excludes records with zero values for the week,month,quarter etc.
		// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string, DateTime)
		DateTime dtStartDate;
		DateTime dtEndDate;
		DateTime dtSaveEndDate;
		int intDay;
		int lngYear;
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
		int intMonth;
		string strWhere;
		int intQuarter;
		int intPayrun;
		string strMTDQuery;
		string strQTDQuery;
		string strCurQuery;
		string strYTDQuery;
		string strFISQuery;
		string strLTDQuery;
		string strMTDQuery2;
		string strQTDQuery2;
		string strCurQuery2;
		string strYTDQuery2;
		string strFISQuery2;
		string strLTDQuery2;
		string strDedRepEmpQuery = "";
		bool boolRegRecs;
		bool boolMatchRecs;
		bool boolFirstGroup;

		public void Init(List<string> batchReports = null)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
			}
			else
			{
				modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: true, strAttachmentName: "EmployeeDeductions");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("DeductionReports");
			}
		}
		// vbPorter upgrade warning: strDate As string	OnWrite(DateTime)
		private string LastDayOfMonth(string strDate)
		{
			string LastDayOfMonth = "";
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth;
			// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
			int intYear;
			string strFirst;
			intMonth = FCConvert.ToDateTime(strDate).Month;
			intYear = FCConvert.ToDateTime(strDate).Year;
			strFirst = FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(intYear);
			// vbPorter upgrade warning: strLast As string	OnWrite(DateTime)
			string strLast;
			strLast = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(strFirst)));
			strLast = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(strLast)));
			LastDayOfMonth = strLast;
			return LastDayOfMonth;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			string strSQL;
			int intReturn;
			string strProcess = "";
			bool boolNoData;
			// clsTemp = new clsDRWrapper();
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			boolWeekly = false;
			boolMonthly = false;
			boolQuarterly = false;
			boolYearly = false;
			boolFirstGroup = true;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Employee Deduction Report";
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			boolHistorical = false;
			lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
			intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
			
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				// must do as current pay date
				// get month to date info
				lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
				intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
				dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
				dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				boolHistorical = true;
			}
			else
			{
				// must choose a paydate to run for
				dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayrun = modGlobalVariables.Statics.gintCurrentPayRun;
				frmSelectDateInfo.InstancePtr.Init2("Select Pay Date For Report", -1, -1, -1, ref dtEndDate, ref intPayrun, false);
				if (modGlobalVariables.Statics.gboolCancelSelected)
					return;
				lngYear = dtEndDate.Year;
				intMonth = dtEndDate.Month;
				// dtStartDate = intMonth & "/1/" & lngYear
				// If dtEndDate <> gdatCurrentPayDate Then boolHistorical = True
				dtStartDate = dtEndDate;
				boolHistorical = true;
			}

			boolWeekly = true;
			dtSaveEndDate = dtEndDate;
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				txtSecondTitle.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
			}
			else
			{
				txtSecondTitle.Text = "Pay Date " + Strings.Format(dtEndDate, "MM/dd/yyyy");
			}
			if (intMonth >= 1 && intMonth <= 3)
			{
				intQuarter = 1;
			}
			else if (intMonth >= 4 && intMonth <= 6)
			{
				intQuarter = 2;
			}
			else if (intMonth >= 7 && intMonth <= 9)
			{
				intQuarter = 3;
			}
			else if (intMonth >= 10 && intMonth <= 12)
			{
				intQuarter = 4;
			}
			// MTD Totals
			strSQL = "Select employeenumber as enum,deddeductionnumber as deddeductionnumber1, sum(dedamount) as MTDDeduction from tblcheckdetail WHERE paydate between '" + FCConvert.ToString(dtStartDate.Month) + "/1/" + FCConvert.ToString(dtStartDate.Year) + "' and '" + LastDayOfMonth(FCConvert.ToString(dtStartDate)) + "' AND checkvoid = 0 group by employeenumber,deddeductionnumber";
			strMTDQuery2 = " (" + strSQL + ") as DeductionReportMTDQuery ";
			// QTD Totals
			modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, lngYear);
			dtEndDate = dtSaveEndDate;
			// don't want to end of quarter just up to the chosen paydate
			strSQL = "Select employeenumber as empnum,deddeductionnumber as deddeductionnumber2,sum(dedamount) as QTDDeduction from tblcheckdetail WHERE paydate >= '" + FCConvert.ToString(dtStartDate) + "' and paydate <= '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,deddeductionnumber";
			strQTDQuery2 = " (" + strSQL + ") as DeductionReportQTDQuery ";
			// YTD calendar Totals
			dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
			dtEndDate = dtSaveEndDate;
			strSQL = "Select employeenumber as emnum,deddeductionnumber as deddeductionnumber3,sum(dedamount) as YTDCalendarDeduction from tblcheckdetail WHERE paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,deddeductionnumber";
			strYTDQuery2 = " (" + strSQL + ") as DeductionReportYTDCalQuery ";
			// YTD Fiscal Totals
			dtEndDate = dtSaveEndDate;
			dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
			strSQL = "Select employeenumber as EmployeeNo,deddeductionnumber as deddeductionnumber4,sum(dedamount) as YTDFiscalDeduction from tblcheckdetail WHERE paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,deddeductionnumber";
			strFISQuery2 = " (" + strSQL + ") as DeductionReportYTDFisQuery ";
			// LTD Totals
			dtEndDate = dtSaveEndDate;
			strSQL = "Select Employeenumber as EmpNo,deddeductionnumber as deddeductionnumber5,sum(dedAmount) as LTDDeduction from tblcheckdetail WHERE paydate <= '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,deddeductionnumber";
			strLTDQuery2 = " (" + strSQL + ") as DeductionReportLTDQuery ";
			// Current Totals
			dtEndDate = dtSaveEndDate;
			// MATTHEW 8/10/2005 CALL ID 73715
			// If gboolAllPayRuns Then
			// MATTHEW 8/16/2005 CALL ID 75130
			if (modGlobalVariables.Statics.gboolAllPayRuns || modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				strSQL = "Select Employeenumber as EmNo,deddeductionnumber,sum(dedAmount) as CurDeduction from tblcheckdetail WHERE paydate = '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,deddeductionnumber having sum(dedamount) > 0";
			}
			else
			{
				strSQL = "Select Employeenumber as EmNo,deddeductionnumber,sum(dedAmount) as CurDeduction from tblcheckdetail WHERE paydate = '" + FCConvert.ToString(dtEndDate) + "' and PayRunID = " + FCConvert.ToString(intPayrun) + " and checkvoid = 0 group by employeenumber,deddeductionnumber having sum(dedamount) > 0";
			}
			strCurQuery2 = " (" + strSQL + ") as DeductionReportCurQuery ";
			rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			modGlobalVariables.Statics.gintSequenceBy = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("reportsequence"))));
			BuildSQLStatements("");
			// Call SetPrintProperties(Me)
			intpage = 1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modGlobalRoutines.UpdatePayrollStepTable("DeductionReports");
			}
		}

		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
		private void Detail_Format(object sender, EventArgs e)
		{
			//clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolNoData;
			string strWhere = "";
			string strSQL;
			if (modGlobalVariables.Statics.gboolCancelSelected)
			{
				this.Cancel();
				return;
			}
			
			switch (modGlobalVariables.Statics.gintSequenceBy)
			{
				case 0:
					{
						// employee name
						strOrderBy2 = " ,lastname,firstname ";
						break;
					}
				case 1:
					{
						// employee number
						strOrderBy2 = " ,employeenumber ";
						break;
					}
				case 2:
					{
						// sequence
						strOrderBy2 = ",SeqNumber,lastname,firstname  ";
						break;
					}
				case 3:
					{
						// dept/div
						// strOrderBy2 = ",tblemployeemaster.lastname,tblemployeemaster.firstname "
						strOrderBy2 = ",DeptDiv,lastname,firstname  ";
						break;
					}
			}
			//end switch
			strSQL = "select * from " + strDedRepEmpQuery + " order by deddeductionnumber" + strOrderBy2;
			SubReport1.Visible = true;
			SubReport1.Report = new srptEmployeeDeductionReport();
			SubReport1.Report.UserData = strSQL + "@" + FCConvert.ToString(boolHistorical);
			strSQL = strSQLMatch + strOrderBy2;
			SubReport2.Visible = true;
			SubReport2.Report = new srptEmployeeDeductionReport();
			SubReport2.Report.UserData = strSQL + "@" + FCConvert.ToString(boolHistorical);
			// use @ as a separator since commas are all through the sql statement
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			intpage += 1;
		}

		private void BuildSQLStatements(string strCurrentGroup)
		{
			string strSQL;
			string[] strTemp = null;
			strWhere = " where ";
			// strIncludeWhere = "and (calendarytdtotal > 0 or fiscalytdtotal > 0)"
			strIncludeWhere = " and (ytdfiscaldeduction > 0 or ytdCalendardeduction > 0)";
			// strSQL = "Select tblemployeemaster.employeenumber,lastname,firstname,seqnumber,deptdiv,deddeductionnumber5 as deddeductionnumber,curdeduction,qtddeduction,mtddeduction,ytdfiscaldeduction,ytdcalendardeduction,ltddeduction,false as ThisIsMatchRecord,limitreached,true as HistoricalRecord from tblemployeemaster "
			// strSQL = strSQL & " left join (DeductionReportLTDQuery left join (DeductionReportYTDCalQuery left join (DeductionReportQTDQuery left join (DeductionReportMTDQuery left join (DeductionReportCurQuery left join DeductionReportYTDFisQuery"
			// strSQL = strSQL & " on (DeductionReportYTDFisQuery.employeeno = DeductionReportCurQuery.emno) and (DeductionReportYTDFisQuery.deddeductionnumber4 = DeductionReportCurQuery.deddeductionnumber)) on (DeductionReportCurQuery.emno = DeductionReportMTDQuery.enum) and (DeductionReportCurQuery.deddeductionnumber = DeductionReportMTDQuery.deddeductionnumber1))"
			// strSQL = strSQL & " on (DeductionReportMTDQuery.enum = DeductionReportQTDQuery.empnum) and (DeductionReportMTDQuery.deddeductionnumber1 = DeductionReportQTDQUery.deddeductionnumber2)) on (DeductionReportQTDQuery.empnum = DeductionREportYTDCalQuery.emnum) and (DeductionREportQTDQuery.deddeductionnumber2 = DeductionReportYTDCalQuery.deddeductionnumber3))"
			// strSQL = strSQL & " on (DeductionReportYTDCalQuery.emnum = DeductionREportLTDQuery.empno) and (DeductionReportYTDCalQuery.deddeductionnumber3 = DeductionReportLTDQuery.deddeductionnumber5)) on (DeductionReportLTDQuery.empno = tblemployeemaster.employeenumber) where deddeductionnumber5 > 0 " & strIncludeWhere & " order by deddeductionnumber5 " & strOrderBy2
			// MATTHEW 8/8/2005 CALL ID 73715
			// strSQL = "Select tblemployeemaster.employeenumber,lastname,firstname,middlename,desig,seqnumber,deptdiv,deddeductionnumber5 as deddeductionnumber,curdeduction,qtddeduction,mtddeduction,ytdfiscaldeduction,ytdcalendardeduction,ltddeduction,0 as ThisIsMatchRecord,limitreached,1 as HistoricalRecord FROM tblemployeemaster LEFT JOIN (" & strLTDQuery2 & " LEFT JOIN (" & strYTDQuery2 & " LEFT JOIN (" & strFISQuery2
			strSQL = "Select tblemployeemaster.employeenumber,lastname,firstname,middlename,desig,seqnumber,deptdiv,deddeductionnumber5 as deddeductionnumber,curdeduction,qtddeduction,mtddeduction,ytdfiscaldeduction,ytdcalendardeduction,ltddeduction,0 as ThisIsMatchRecord,1 as HistoricalRecord FROM tblemployeemaster LEFT JOIN (" + strLTDQuery2 + " LEFT JOIN (" + strYTDQuery2 + " LEFT JOIN (" + strFISQuery2;
			// strSQL = strSQL & " LEFT JOIN (DeductionReportQTDQuery LEFT JOIN (DeductionReportMTDQuery LEFT JOIN DeductionReportCurQuery ON (DeductionReportCurQuery.emno = DeductionReportMTDQuery.enum) AND (DeductionReportCurQuery.deddeductionnumber = DeductionReportMTDQuery.deddeductionnumber1)) ON (DeductionReportMTDQuery.enum = DeductionReportQTDQuery.empnum) AND (DeductionReportMTDQuery.deddeductionnumber1 = DeductionReportQTDQUery.deddeductionnumber2)) ON (DeductionReportQTDQuery.empnum = DeductionREportYTDCalQuery.emnum) AND (DeductionREportQTDQuery.deddeductionnumber2 = DeductionReportYTDCalQuery.deddeductionnumber3)) ON (DeductionReportYTDCalQuery.emnum = DeductionReportYTDFisQuery.employeeno) AND (DeductionReportYTDCalQuery.deddeductionnumber3 ="
			strSQL += " LEFT JOIN (" + strQTDQuery2 + " LEFT JOIN (" + strMTDQuery2 + " LEFT JOIN " + strCurQuery2 + " ON (DeductionReportCurQuery.emno = DeductionReportMTDQuery.enum) AND (DeductionReportCurQuery.deddeductionnumber = DeductionReportMTDQuery.deddeductionnumber1)) ON (DeductionReportMTDQuery.enum = DeductionReportQTDQuery.empnum) AND (DeductionReportMTDQuery.deddeductionnumber1 = DeductionReportQTDQUery.deddeductionnumber2)) ON (DeductionReportQTDQuery.empnum = DeductionREportYTDFisQuery.employeeno) AND (DeductionREportQTDQuery.deddeductionnumber2 = DeductionReportYTDFisQuery.deddeductionnumber4)) ON (DeductionReportYTDCalQuery.emnum = DeductionReportYTDFisQuery.employeeno) AND (DeductionReportYTDCalQuery.deddeductionnumber3 =";
			strSQL += " DeductionReportYTDFisQuery.deddeductionnumber4)) ON (DeductionReportYTDCalQuery.emnum = DeductionREportLTDQuery.EmpNo) AND (DeductionReportYTDCalQuery.deddeductionnumber3 = DeductionReportLTDQuery.deddeductionnumber5)) ON DeductionREportLTDQuery.EmpNo = tblemployeemaster.employeenumber WHERE (ytdfiscaldeduction > 0 or ytdCalendardeduction > 0)";
			// & strIncludeWhere
			// strSQL = strSQL & " DeductionReportYTDFisQuery.deddeductionnumber4)) ON (DeductionReportYTDFisQuery.employeeno = DeductionREportLTDQuery.EmpNo) AND (DeductionReportYTDFisQuery.deddeductionnumber4 = DeductionReportLTDQuery.deddeductionnumber5)) ON DeductionREportLTDQuery.EmpNo = tblemployeemaster.employeenumber WHERE (ytdfiscaldeduction > 0 or ytdCalendardeduction > 0)" ' & strIncludeWhere
			// End If
			strDedRepEmpQuery = " (" + strSQL + ") as DeductionReportEmpQuery ";
			strIncludeWhere = "";
			// historical so we must get it from checkdetail
			dtEndDate = dtSaveEndDate;
			lngYear = dtEndDate.Year;
			intMonth = dtEndDate.Month;
			dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
			// MTD Totals
			// strSQL = "Select employeenumber as enum,MATCHdeductionnumber as deddeductionnumber3,dedhitlimit as LimitReached, sum(matchamount) as MTDDeduction from tblcheckdetail " & strWhere & " paydate >= '" & dtStartDate & "' and paydate <= '" & dtEndDate & "' and checkvoid = 0 group by employeenumber,matchdeductionnumber, dedhitlimit"
			strSQL = "Select employeenumber as enum,MATCHdeductionnumber as deddeductionnumber3, sum(matchamount) as MTDDeduction from tblcheckdetail " + strWhere + " paydate >= '" + FCConvert.ToString(dtStartDate) + "' and paydate <= '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber";
			strMTDQuery = " (" + strSQL + ") as MTDQuery ";
			// QTD Totals
			modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, lngYear);
			dtEndDate = dtSaveEndDate;
			// don't want to end of quarter just up to the chosen paydate
			strSQL = "Select employeenumber as empnum,matchdeductionnumber as deddeductionnumber2,sum(matchamount) as QTDDeduction from tblcheckdetail " + strWhere + " paydate >= '" + FCConvert.ToString(dtStartDate) + "' and paydate <= '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber";
			strQTDQuery = " (" + strSQL + ") as QTDQuery ";
			// YTD Calendar Totals
			dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngYear));
			dtEndDate = dtSaveEndDate;
			strSQL = "Select employeenumber as emnum,matchdeductionnumber as deddeductionnumber,sum(matchamount) as YTDCalendarDeduction from tblcheckdetail " + strWhere + " paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber";
			strYTDQuery = " (" + strSQL + ") as YTDQuery ";
			// YTD Fiscal Totals
			dtEndDate = dtSaveEndDate;
			dtStartDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtEndDate);
			strSQL = "Select employeenumber as EmployeeNo,matchdeductionnumber as deddeductionnumber4,sum(matchamount) as YTDFiscalDeduction from tblcheckdetail " + strWhere + " paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber";
			strFISQuery = " (" + strSQL + ") as FisQuery ";
			// LTD Totals
			dtEndDate = dtSaveEndDate;
			strSQL = "Select Employeenumber as EmpNo,matchdeductionnumber as deddeductionnumber5,sum(matchAmount) as LTDDeduction from tblcheckdetail " + strWhere + " paydate <= '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber";
			strLTDQuery = " (" + strSQL + ") as LTDQuery ";
			// Current
			dtEndDate = dtSaveEndDate;
			strSQL = "Select Employeenumber as EmNo,matchdeductionnumber as deddeductionnumber1,sum(matchAmount) as CurDeduction,matchdeductiontitle as description from tblcheckdetail " + strWhere + " paydate = '" + FCConvert.ToString(dtEndDate) + "' and checkvoid = 0 group by employeenumber,matchdeductionnumber,matchdeductiontitle having sum(matchamount) > 0";
			strCurQuery = " (" + strSQL + ") as CurQuery ";
			// strSQL = "SELECT tblemployeemaster.employeenumber,lastname,firstname,middlename,desig,deptdiv,seqnumber,deddeductionnumber5 as deddeductionnumber,description,curdeduction,qtddeduction,mtddeduction,ytdfiscaldeduction,ytdcalendardeduction,ltddeduction,1 as ThisIsMatchRecord,limitreached, 1 as historicalrecord from tblemployeemaster inner join (" & strLTDQuery & " left join (" & strYTDQuery & " left join (" & strFISQuery & " left join (" & strQTDQuery & " left join (" & strMTDQuery & " left join " & strCurQuery & " on "
			strSQL = "SELECT tblemployeemaster.employeenumber,lastname,firstname,middlename,desig,deptdiv,seqnumber,deddeductionnumber5 as deddeductionnumber,description,curdeduction,qtddeduction,mtddeduction,ytdfiscaldeduction,ytdcalendardeduction,ltddeduction,1 as ThisIsMatchRecord, 1 as historicalrecord from tblemployeemaster inner join (" + strLTDQuery + " left join (" + strYTDQuery + " left join (" + strFISQuery + " left join (" + strQTDQuery + " left join (" + strMTDQuery + " left join " + strCurQuery + " on ";
			// strSQL = strSQL & " (mtdquery.enum = curquery.emno) and (MTDQuery.deddeductionnumber3 = curquery.deddeductionnumber1)) on (MTDQuery.enum = QTDQuery.empnum) and (MTDQuery.deddeductionnumber3 = QTDQuery.deddeductionnumber2)) on (QTDQuery.empnum = YTDQuery.emnum) and (QTDQuery.deddeductionnumber2 = YTDQuery.deddeductionnumber)) on (YTDQuery.emnum = FisQuery.employeeno) and (YTDQuery.deddeductionnumber = FisQuery.deddeductionnumber4)) on "
			strSQL += " (mtdquery.enum = curquery.emno) and (MTDQuery.deddeductionnumber3 = curquery.deddeductionnumber1)) on (MTDQuery.enum = QTDQuery.empnum) and (MTDQuery.deddeductionnumber3 = QTDQuery.deddeductionnumber2)) on (QTDQuery.empnum = FISQuery.employeeno) and (QTDQuery.deddeductionnumber2 = FISQuery.deddeductionnumber4)) on (YTDQuery.emnum = FisQuery.employeeno) and (YTDQuery.deddeductionnumber = FisQuery.deddeductionnumber4)) on ";
			// strSQL = strSQL & " (FisQuery.employeeno = LTDQuery.empno) and (FisQuery.deddeductionnumber4 = LTDQuery.deddeductionnumber5))  on (ltdQuery.empno = tblemployeemaster.employeenumber) WHERE DEDdeductionnumber > 0 order by DEDdeductionnumber " & strOrderBy2
			strSQL += " (YTDQuery.emnum = LTDQuery.empno) and (YTDQuery.deddeductionnumber = LTDQuery.deddeductionnumber5))  on (ltdQuery.empno = tblemployeemaster.employeenumber) WHERE (YTDFISCALDEDUCTION > 0 or ytdcalendardeduction > 0) and deddeductionnumber5 > 0 order by DEDdeductionnumber5 " + strOrderBy2;
			// strSQL = strSQL & " (FisQuery.employeeno = LTDQuery.empno) and (FisQuery.deddeductionnumber4 = LTDQuery.deddeductionnumber5))  on (ltdQuery.empno = tblemployeemaster.employeenumber) WHERE (YTDFISCALDEDUCTION > 0 or ytdcalendardeduction > 0) and deddeductionnumber5 > 0 order by DEDdeductionnumber5 " & strOrderBy2
			strSQLMatch = strSQL;
			// End If
		}

		
	}
}
