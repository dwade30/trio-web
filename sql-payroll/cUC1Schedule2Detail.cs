﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public class cUC1Schedule2Detail
	{
		//=========================================================
		private string strName = "";
		private string strSSN = string.Empty;
		private double dblUCWages;
		private bool boolSeasonal;
		private bool[] boolMonthEmployment = new bool[3 + 1];
		private bool boolFemale;
		private string strLastName = string.Empty;
		private string strFirstName = string.Empty;
		private string strMiddleInitial = string.Empty;
		private double dblExcessWages;
		private double dblTaxableWages;
		private string strDesig = string.Empty;

		public string Designation
		{
			set
			{
				strDesig = value;
			}
			get
			{
				string Designation = "";
				Designation = strDesig;
				return Designation;
			}
		}

		public double ExcessWages
		{
			set
			{
				dblExcessWages = value;
			}
			get
			{
				double ExcessWages = 0;
				ExcessWages = dblExcessWages;
				return ExcessWages;
			}
		}

		public double TaxableWages
		{
			set
			{
				dblTaxableWages = value;
			}
			get
			{
				double TaxableWages = 0;
				TaxableWages = dblTaxableWages;
				return TaxableWages;
			}
		}

		public void Set_EmployedDuringMonth(int intIndex, bool boolEmployed)
		{
			if (intIndex > 0 && intIndex < 4)
			{
				boolMonthEmployment[intIndex - 1] = boolEmployed;
			}
		}

		public bool Get_EmployedDuringMonth(int intIndex)
		{
			bool EmployedDuringMonth = false;
			if (intIndex > 0 && intIndex < 4)
			{
				EmployedDuringMonth = boolMonthEmployment[intIndex - 1];
			}
			return EmployedDuringMonth;
		}

		public bool Female
		{
			set
			{
				boolFemale = value;
			}
			get
			{
				bool Female = false;
				Female = boolFemale;
				return Female;
			}
		}

		public string LastName
		{
			set
			{
				strLastName = value;
			}
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
		}

		public string FirstName
		{
			set
			{
				strFirstName = value;
			}
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
		}

		public string MiddleName
		{
			set
			{
				strMiddleInitial = value;
			}
			get
			{
				string MiddleName = "";
				MiddleName = strMiddleInitial;
				return MiddleName;
			}
		}

		public string FullName
		{
			get
			{
				string FullName = "";
				FullName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strFirstName + " " + Strings.Left(strMiddleInitial + " ", 1)) + " " + strLastName) + " " + strDesig);
				return FullName;
			}
		}

		public string LastFirst
		{
			get
			{
				string LastFirst = "";
				LastFirst = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strLastName + " " + fecherFoundation.Strings.Trim(strFirstName + " " + Strings.Left(strMiddleInitial + " ", 1))) + " " + strDesig);
				return LastFirst;
			}
		}

		public string LastFirstNoSuffix
		{
			get
			{
				string LastFirstNoSuffix = "";
				LastFirstNoSuffix = ((strLastName.Trim() + " " + strFirstName.Trim() + " " + (strMiddleInitial + " ").Left(1))).Trim();
				return LastFirstNoSuffix;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public double UCWages
		{
			set
			{
				dblUCWages = value;
			}
			get
			{
				double UCWages = 0;
				UCWages = dblUCWages;
				return UCWages;
			}
		}

		public bool IsSeasonal
		{
			set
			{
				boolSeasonal = value;
			}
			get
			{
				bool IsSeasonal = false;
				IsSeasonal = boolSeasonal;
				return IsSeasonal;
			}
		}
	}
}
