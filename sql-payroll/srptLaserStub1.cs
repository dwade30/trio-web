﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptLaserStub1.
	/// </summary>
	public partial class srptLaserStub1 : FCSectionReport
	{
		public srptLaserStub1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptLaserStub1 InstancePtr
		{
			get
			{
				return (srptLaserStub1)Sys.GetInstance(typeof(srptLaserStub1));
			}
		}

		protected srptLaserStub1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLaserStub1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolShowPayPeriod;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// Dim x As Integer
			// If UCase(MuniName) = "RANGELEY" Then
			// For x = 1 To Me.Detail.Controls.Count - 1
			// If UCase(Me.Detail.Controls[x].Tag) = "TEXT" Or UCase(Me.Detail.Controls[x].Tag) = "BOLD" Then
			// Select Case LCase(Me.Detail.Controls[x].Name)
			// Case "txtdirectdepositlabel", "lbldeposit1", "lbldeposit2", "lbldeposit3"
			// Me.Detail.Controls[x].Visible = False
			// Case "lbldeposit4", "lbldeposit5", "lbldeposit6", "lbldeposit7", "lbldeposit8", "lbldeposit9"
			// Me.Detail.Controls[x].Visible = False
			// Case "lbldepositamount1", "lbldepositamount2", "lbldepositamount3", "lbldepositamount4", "lbldepositamount5", "lbldepositamount6"
			// Me.Detail.Controls[x].Visible = False
			// Case "lbldepositamount7", "lbldepositamount8", "lbldepositamount9"
			// Me.Detail.Controls[x].Visible = False
			// Case "txtdirectdepchklabel", "txtdirectdepositsavlabel", "lblcheckamount", "txtchkamount", "txtdirectdepsav", "txtdirectdeposit"
			// Me.Detail.Controls[x].Visible = False
			// Case "txtdirectdepositlabel"
			// Me.Detail.Controls[x].Visible = False
			// Case Else
			// Me.Detail.Controls[x].Top = Me.Detail.Controls[x].Top + 780
			// End Select
			// End If
			// Next x
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais")
				{
					Detail.Height = 5220 / 1440F;
					Detail.CanShrink = false;
					for (x = 1; x <= this.Detail.Controls.Count - 1; x++)
					{
						if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "TEXT" || fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "BOLD")
						{
							if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdate")
							{
								txtDate.Left = 5400 / 1440F;
							}
							else if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtcheckno")
							{
							}
							else
							{
								this.Detail.Controls[x].Top += 200 / 1440F;
							}
						}
					}
					// x
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
					lblDeptDiv.Visible = false;
					txtDeptDiv.Visible = false;
					// txtEmployeeNo.Top = txtDeptDiv.Top
					txtEmployeeNo.Left = 3400 / 1440F;
					txtEmployeeNo.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					txtEmployeeNo.Top = txtName.Top;
					// txtName.Top = txtEmployeeNo.Top
					txtName.Left = 6120 / 1440F;
				}
				// MATTHEW 8/31/2004
				// COREY AND I COMMENTED THIS BACK IN FOR VASSALBORO BECAUSE THEIR
				// CHECKS WHICH WERE SET TO STANDARD WHERE NOT REALLY SHOWING CORRECTLY.
				// COREY COMMENTED THIS OUT AT FIRST BECAUSE SOMETIMES WITH SOME TOWNS
				// USING THESE PRINTER FONTS WOULD MOVE FIELDS AROUND. IF WE RUN INTO
				// THIS AGAIN THEN WE CAN WRAP THESE FOUR LINES OF CODE WITH THE MUNINAME
				// EQUAL TO VASSALBORO.
				boolShowPayPeriod = (this.ParentReport as rptLaserCheck1).PrintPayPeriod;
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "RANGELEY" && Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 29)) == 0 && Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 34)) == 0)
				{
					for (x = 1; x <= this.Detail.Controls.Count - 1; x++)
					{
						if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "TEXT" || fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "BOLD")
						{
							if ((fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdepositlabel") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit1") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit2") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit3"))
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if ((fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit4") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit5") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit6") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit7") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit8") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldeposit9"))
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if ((fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount1") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount2") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount3") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount4") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount5") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount6"))
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if ((fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount7") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount8") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lbldepositamount9"))
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if ((fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdepchklabel") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdepositsavlabel") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "lblcheckamount") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtchkamount") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdepsav") || (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdeposit"))
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtdirectdepositlabel")
							{
								this.Detail.Controls[x].Visible = false;
							}
							else if (fecherFoundation.Strings.LCase(this.Detail.Controls[x].Name) == "txtemployeeno")
							{
								this.Detail.Controls[x].Top += 780 / 1440F;
							}
							else
							{
								this.Detail.Controls[x].Top += 780 / 1440F;
							}
						}
					}
					// x
					txtEmployeeNo.Top = txtName.Top;
				}
				if (FCConvert.ToString(this.UserData) != string.Empty && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "HALLOWELL")
				{
					clsPrt.SetReportFontsByTag(this, "Text", FCConvert.ToString(this.UserData));
					clsPrt.SetReportFontsByTag(this, "bold", FCConvert.ToString(this.UserData), true);
				}
				else
				{
					// if it's a dotmatrix
					if (Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 30)) == 0)
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "TEXT" || fecherFoundation.Strings.UCase(FCConvert.ToString(this.Detail.Controls[x].Tag)) == "BOLD")
							{
								(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font((this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Name, 9);
							}
						}
						// x
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Laser Stub ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// Fill textboxes from grid
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			bool boolPrintTest = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!boolPrintTest)
				{
					Line1.Visible = false;
					cPayRun pRun;
					pRun = (this.ParentReport as rptLaserCheck1).PayRun;
					if (!boolShowPayPeriod)
					{
						lblCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
					}
					else
					{
						if (!(pRun == null))
						{
							if (Information.IsDate(pRun.PeriodStart) && Information.IsDate(pRun.PeriodEnd))
							{
								lblCheckMessage.Text = "Pay Period: " + Strings.Format(pRun.PeriodStart, "MM/dd/yyyy") + " to " + Strings.Format(pRun.PeriodEnd, "MM/dd/yyyy");
							}
						}
					}
					txtPayDesc1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 0);
					txtPayDesc2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 0);
					txtPayDesc3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 0);
					txtPayDesc4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 0);
					txtPayDesc5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 0);
					txtPayDesc6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 0);
					txtPayDesc7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 0);
					txtPayDesc8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 0);
					txtPayDesc9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 0);
					txtHours1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 1);
					txtHours2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 1);
					txtHours3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 1);
					txtHours4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 1);
					txtHours5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 1);
					txtHours6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 1);
					txtHours7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 1);
					txtHours8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 1);
					txtHours9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 1);
					txtPayAmount1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 2);
					txtPayAmount2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 2);
					txtPayAmount3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 2);
					txtPayAmount4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 2);
					txtPayAmount5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 2);
					txtPayAmount6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 2);
					txtPayAmount7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 2);
					txtPayAmount8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 2);
					txtPayAmount9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 2);
					txtDedDesc1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 3);
					txtDedDesc2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 3);
					txtDedDesc3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 3);
					txtDedDesc4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 3);
					txtDedDesc5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 3);
					txtDedDesc6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 3);
					txtDedDesc7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 3);
					txtDedDesc8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 3);
					txtDedDesc9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 3);
					txtDedAmount1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 4);
					txtDedAmount2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 4);
					txtDedAmount3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 4);
					txtDedAmount4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 4);
					txtDedAmount5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 4);
					txtDedAmount6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 4);
					txtDedAmount7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 4);
					txtDedAmount8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 4);
					txtDedAmount9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 4);
					txtDedYTD1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 5);
					txtDedYTD2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 5);
					txtDedYTD3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 5);
					txtDedYTD4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 5);
					txtDedYTD5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 5);
					txtDedYTD6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 5);
					txtDedYTD7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 5);
					txtDedYTD8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 5);
					txtDedYTD9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 5);
					lblDeposit1.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 7);
					lblDeposit2.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 7);
					lblDeposit3.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 7);
					lblDeposit4.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 7);
					lblDeposit5.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 7);
					lblDeposit6.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 7);
					lblDeposit7.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 7);
					lblDeposit8.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 7);
					lblDeposit9.Text = rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 7);
					lblDepositAmount1.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 6), "#,###,##0.00");
					lblDepositAmount2.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 6), "#,###,##0.00");
					lblDepositAmount3.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 6), "#,###,##0.00");
					lblDepositAmount4.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 6), "#,###,##0.00");
					lblDepositAmount5.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 6), "#,###,##0.00");
					lblDepositAmount6.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 6), "#,###,##0.00");
					lblDepositAmount7.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 6), "#,###,##0.00");
					lblDepositAmount8.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 6), "#,###,##0.00");
					lblDepositAmount9.Text = Strings.Format(rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 6), "#,###,##0.00");
					// If LCase(MuniName) <> "rangeley" Then
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(0, 6)) <= 0)
					{
						lblDepositAmount1.Visible = false;
						lblDeposit1.Visible = false;
					}
					else
					{
						lblDepositAmount1.Visible = true;
						lblDeposit1.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(1, 6)) <= 0)
					{
						lblDepositAmount2.Visible = false;
						lblDeposit2.Visible = false;
					}
					else
					{
						lblDepositAmount2.Visible = true;
						lblDeposit2.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(2, 6)) <= 0)
					{
						lblDepositAmount3.Visible = false;
						lblDeposit3.Visible = false;
					}
					else
					{
						lblDepositAmount3.Visible = true;
						lblDeposit3.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(3, 6)) <= 0)
					{
						lblDepositAmount4.Visible = false;
						lblDeposit4.Visible = false;
					}
					else
					{
						lblDepositAmount4.Visible = true;
						lblDeposit4.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(4, 6)) <= 0)
					{
						lblDepositAmount5.Visible = false;
						lblDeposit5.Visible = false;
					}
					else
					{
						lblDepositAmount5.Visible = true;
						lblDeposit5.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(5, 6)) <= 0)
					{
						lblDepositAmount6.Visible = false;
						lblDeposit6.Visible = false;
					}
					else
					{
						lblDepositAmount6.Visible = true;
						lblDeposit6.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(6, 6)) <= 0)
					{
						lblDepositAmount7.Visible = false;
						lblDeposit7.Visible = false;
					}
					else
					{
						lblDepositAmount7.Visible = true;
						lblDeposit7.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(7, 6)) <= 0)
					{
						lblDepositAmount8.Visible = false;
						lblDeposit8.Visible = false;
					}
					else
					{
						lblDepositAmount8.Visible = true;
						lblDeposit8.Visible = true;
					}
					if (Conversion.Val(rptLaserCheck1.InstancePtr.Grid.TextMatrix(8, 6)) <= 0)
					{
						lblDepositAmount9.Visible = false;
						lblDeposit9.Visible = false;
					}
					else
					{
						lblDepositAmount9.Visible = true;
						lblDeposit9.Visible = true;
					}
					// End If
					dblDirectDepChk = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 29));
					dblDirectDepSav = Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 34));
					dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
					txtDate.Text = "DATE " + Strings.Format(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 24), "MM/dd/yyyy");
					txtCurrentGross.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 5);
					txtCurrentFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 6);
					txtCurrentFica.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 7);
					txtCurrentState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 8);
					txtCurrentDeductions.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 23);
					txtYTDDeds.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 42);
					txtCurrentNet.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9);
					txtVacationBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 0);
					txtSickBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 1);
					txtOtherBalance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 33);
					// MATTHEW 8/18/2004
					// ADD THE THREE NEW 'OTHER' VACATION/SICK CODES
					lblCode1.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 39);
					lblCode2.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 40);
					lblCode3.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 41);
					// WE DO NOT WANT THE OTHER FIELDS TO SHOW IF THERE
					// WERE NO TYPES DEFINED BY THE USER.
					if (lblCode1.Text != string.Empty)
					{
						lblCode1Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 36);
					}
					else
					{
						lblCode1Balance.Text = string.Empty;
					}
					if (lblCode2.Text != string.Empty)
					{
						lblCode2Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 37);
					}
					else
					{
						lblCode2Balance.Text = string.Empty;
					}
					if (lblCode3.Text != string.Empty)
					{
						lblCode3Balance.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 38);
					}
					else
					{
						lblCode3Balance.Text = string.Empty;
					}
					txtYTDGross.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 13);
					txtYTDFed.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 14);
					txtYTDFICA.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 15);
					txtYTDState.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 16);
					txtYTDNet.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 17);
					txtTotalHours.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 3);
					txtTotalAmount.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 4);
					txtEMatchCurrent.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 25);
					txtEmatchYTD.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 26);
					txtEmployeeNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 2);
					txtName.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 18);
					txtCheckNo.Text = rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 12);
					if (dblDirectDeposit > 0)
					{
						// If LCase(MuniName) <> "rangeley" Then
						txtDirectDeposit.Visible = true;
						txtDirectDepositLabel.Visible = true;
						txtChkAmount.Visible = true;
						lblCheckAmount.Visible = true;
						txtDirectDepChkLabel.Visible = true;
						txtDirectDepositSavLabel.Visible = true;
						txtDirectDepSav.Visible = true;
						txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
						txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
						// End If
						txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)) - dblDirectDeposit, "0.00");
					}
					else
					{
						txtDirectDeposit.Visible = false;
						txtDirectDepositLabel.Visible = false;
						txtChkAmount.Visible = false;
						lblCheckAmount.Visible = false;
						txtChkAmount.Text = Strings.Format(Conversion.Val(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 9)), "0.00");
					}
					if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31)) == string.Empty)
					{
						lblPayRate.Visible = false;
						txtPayRate.Visible = false;
					}
					else
					{
						lblPayRate.Visible = true;
						txtPayRate.Visible = true;
						txtPayRate.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 31));
					}
					if (fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 43)) == string.Empty)
					{
						lblDeptDiv.Visible = false;
						txtDeptDiv.Visible = false;
					}
					else
					{
						lblDeptDiv.Visible = true;
						txtDeptDiv.Visible = true;
						txtDeptDiv.Text = fecherFoundation.Strings.Trim(rptLaserCheck1.InstancePtr.GridMisc.TextMatrix(0, 43));
					}
				}
				else
				{
					Line1.Visible = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Laser Stub DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
