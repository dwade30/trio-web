//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmTAAdjustments : BaseForm
	{
		public frmTAAdjustments()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTAAdjustments InstancePtr
		{
			get
			{
				return (frmTAAdjustments)Sys.GetInstance(typeof(frmTAAdjustments));
			}
		}

		protected frmTAAdjustments _InstancePtr = null;
		//=========================================================
		private clsGridAccount clsGA = new clsGridAccount();
		const int CNSTVSDATACOLACCOUNT = 10;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void frmTAAdjustments_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmTAAdjustments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTAAdjustments properties;
			//frmTAAdjustments.ScaleWidth	= 9165;
			//frmTAAdjustments.ScaleHeight	= 6690;
			//frmTAAdjustments.LinkTopic	= "Form1";
			//frmTAAdjustments.LockControls	= -1  'True;
			//End Unmaped Properties
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsGA.GRID7Light = vsData;
				clsGA.DefaultAccountType = "G";
				clsGA.OnlyAllowDefaultType = false;
				clsGA.Validation = true;
				clsGA.AccountCol = CNSTVSDATACOLACCOUNT;
				// Me.vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				modAccountTitle.SetAccountFormats();
				// Call GetFormats
				LoadGrid();
				SetGridProperties();
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
                // ---------------------------------------------------------------------

                vsData.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
                vsData.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsData.ColAlignment(13, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            }
            catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmTAAdjustments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmTAAdjustments_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsRecipient = new clsDRWrapper();
			clsDRWrapper rsCat = new clsDRWrapper();
			clsDRWrapper rsDeduction = new clsDRWrapper();
			vsData.Cols = 15;
			vsData.FixedRows = 1;
			vsData.FixedCols = 0;
			vsData.TextMatrix(0, 0, "New Adjustment");
			vsData.ColWidth(0, 0);
			vsData.TextMatrix(0, 1, "Delete");
			vsData.ColWidth(1, 600);
			vsData.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
			vsData.TextMatrix(0, 2, "Check Recipient");
			vsData.ColWidth(2, 2400);
			vsData.TextMatrix(0, 3, "Recipient ID");
			vsData.ColWidth(3, 0);
			vsData.TextMatrix(0, 4, "Check Recipient Category");
			vsData.ColWidth(4, 2400);
			vsData.TextMatrix(0, 5, "Category ID");
			vsData.ColWidth(5, 0);
			vsData.TextMatrix(0, 6, "Deduction Desc");
			vsData.ColWidth(6, 0);
			vsData.TextMatrix(0, 7, "Deduction ID");
			vsData.ColWidth(7, 0);
			vsData.TextMatrix(0, 8, "Amount");
			vsData.ColWidth(8, 1000);
			vsData.TextMatrix(0, 9, "Check Number");
			vsData.ColWidth(9, 1200);
			vsData.TextMatrix(0, 10, "Deduction Account #");
			vsData.ColWidth(10, 2000);
			vsData.TextMatrix(0, 11, "User");
			vsData.ColWidth(11, 1200);
			vsData.TextMatrix(0, 12, "Date");
			vsData.ColWidth(12, 1200);
			vsData.TextMatrix(0, 13, "Time");
			vsData.ColWidth(13, 1200);
			vsData.TextMatrix(0, 14, "ID");
			vsData.ColWidth(14, 0);
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 10, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			rsRecipient.OpenRecordset("Select * from tblRecipients");
			rsCat.OpenRecordset("Select * from tblCategories");
			rsDeduction.OpenRecordset("Select * from tblDeductionSetup");
			// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
			rsData.OpenRecordset("Select * from tblTAReduction Order by tblTAReductionDate,tblTAReductionTime", "TWPY0000.vb1");
			vsData.Rows = 1;
			while (!rsData.EndOfFile())
			{
				vsData.Rows += 1;
				vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(false));
				vsData.TextMatrix(vsData.Rows - 1, 1, FCConvert.ToString(FCConvert.ToInt32(Wisej.Web.CheckState.Unchecked)));
				rsRecipient.FindFirstRecord("ID", rsData.Get_Fields_Int32("RecipientID"));
				if (rsRecipient.NoMatch)
				{
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("RecipientID")) == 0)
					{
						vsData.TextMatrix(vsData.Rows - 1, 2, "");
					}
					else
					{
						vsData.TextMatrix(vsData.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_Int32("RecipientID")));
					}
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 2, FCConvert.ToString(rsRecipient.Get_Fields_String("Name")));
				}
				vsData.TextMatrix(vsData.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields_Int32("RecipientID")));
				rsCat.FindFirstRecord("ID", rsData.Get_Fields_Int32("CategoryID"));
				if (rsCat.NoMatch)
				{
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("CategoryID")) == 0)
					{
						vsData.TextMatrix(vsData.Rows - 1, 4, "");
					}
					else
					{
						vsData.TextMatrix(vsData.Rows - 1, 4, FCConvert.ToString(rsData.Get_Fields_Int32("CategoryID")));
					}
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 4, FCConvert.ToString(rsCat.Get_Fields("Description")));
				}
				vsData.TextMatrix(vsData.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("CategoryID")));
				rsDeduction.FindFirstRecord("ID", rsData.Get_Fields_Int32("DeductionID"));
				if (rsDeduction.NoMatch)
				{
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("DeductionID")) == 0)
					{
						vsData.TextMatrix(vsData.Rows - 1, 6, "");
					}
					else
					{
						vsData.TextMatrix(vsData.Rows - 1, 6, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionID")));
					}
				}
				else
				{
					vsData.TextMatrix(vsData.Rows - 1, 6, FCConvert.ToString(rsDeduction.Get_Fields("Description")));
				}
				vsData.TextMatrix(vsData.Rows - 1, 7, FCConvert.ToString(rsData.Get_Fields_Int32("DeductionID")));
				vsData.TextMatrix(vsData.Rows - 1, 8, FCConvert.ToString(rsData.Get_Fields("Amount")));
				vsData.TextMatrix(vsData.Rows - 1, 9, FCConvert.ToString(rsData.Get_Fields("CheckNumber")));
				vsData.TextMatrix(vsData.Rows - 1, 10, FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber")));
				vsData.TextMatrix(vsData.Rows - 1, 11, FCConvert.ToString(rsData.Get_Fields("UserID")));
				vsData.TextMatrix(vsData.Rows - 1, 12, FCConvert.ToString((rsData.Get_Fields_DateTime("tblTAReductionDate").Date == DateTime.Parse("12/30/1899")) || (rsData.Get_Fields_DateTime("tblTAReductionDate").Date == DateTime.MinValue) ? "" : Strings.Format(rsData.Get_Fields_DateTime("tblTAReductionDate"), "MM/dd/yyyy")));
				vsData.TextMatrix(vsData.Rows - 1, 13, (Strings.Format(rsData.Get_Fields_DateTime("tblTAReductionTime"), "h:mm:ss tt") == "12:00:00 AM" ? "" : Strings.Format(rsData.Get_Fields_DateTime("tblTAReductionTime"), "h:mm:ss tt")));
				vsData.TextMatrix(vsData.Rows - 1, 14, FCConvert.ToString(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}
		}

		private void frmTAAdjustments_Resize(object sender, System.EventArgs e)
		{
			vsData.Cols = 15;
			vsData.FrozenCols = 9;
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.05));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
			vsData.ColWidth(4, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
			vsData.ColWidth(8, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(9, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(10, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
			vsData.ColWidth(11, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(12, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(13, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			vsData.Rows += 1;
			vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(true));
			vsData.Select(vsData.Rows - 1, 2);
			// Dave 12/14/2006---------------------------------------------------
			// Add change record for adding a row to the grid
			clsReportChanges.AddChange("Added Row to Adjust T&A Records");
			// ------------------------------------------------------------------
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool InvalidData()
		{
			bool InvalidData = false;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			vsData.Select(0, 0);
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (Conversion.Val(vsData.TextMatrix(intCounter, 1)) < 0)
				{
					// this record will be deleted so do no checking
				}
				else
				{
					if (Conversion.Val(vsData.TextMatrix(intCounter, 3)) == 0)
					{
						MessageBox.Show("A Recipient must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsData.Select(intCounter, 3);
						InvalidData = true;
						return InvalidData;
					}
					if (Conversion.Val(vsData.TextMatrix(intCounter, 5)) == 0)
					{
						MessageBox.Show("A Cateogry must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsData.Select(intCounter, 5);
						InvalidData = true;
						return InvalidData;
					}
					if (Conversion.Val(vsData.TextMatrix(intCounter, 7)) == 0)
					{
						// MsgBox "A Deduction must be selected.", vbCritical + vbOKOnly, "TRIO Software"
						// vsData.Select intCounter, 7
						// InvalidData = True
						// Exit Function
					}
					if (Conversion.Val(vsData.TextMatrix(intCounter, 8)) == 0)
					{
						MessageBox.Show("An Amount must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsData.Select(intCounter, 8);
						InvalidData = true;
						return InvalidData;
					}
					if (vsData.TextMatrix(intCounter, 10) == string.Empty)
					{
						MessageBox.Show("An Account Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsData.Select(intCounter, 10);
						InvalidData = true;
						return InvalidData;
					}
				}
			}
			return InvalidData;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDelete = new clsDRWrapper();
			string strSQL = "";
			int x;
			if (InvalidData())
				return;
			vsData.Select(0, 0);
			rsData.OpenRecordset("Select * from tblTAReduction", "TWPY0000.vb1");
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (FCConvert.CBool(vsData.TextMatrix(intCounter, 0)))
				{
					if (Conversion.Val(vsData.TextMatrix(intCounter, 1)) == 0)
					{
						if (vsData.TextMatrix(intCounter, 14) == string.Empty)
						{
							// then this is a new record
							rsData.AddNew();
							modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Add New Entry");
							strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
							strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
							strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
							strSQL += "," + vsData.TextMatrix(intCounter, 3);
							strSQL += "," + vsData.TextMatrix(intCounter, 5);
							strSQL += "," + vsData.TextMatrix(intCounter, 7);
							strSQL += "," + vsData.TextMatrix(intCounter, 8);
							strSQL += "," + vsData.TextMatrix(intCounter, 9);
							strSQL += ",'" + vsData.TextMatrix(intCounter, 10) + "'";
							strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
							strSQL += ")";
							rsDelete.Execute(strSQL, "Twpy0000.vb1");
						}
						else
						{
							rsData.FindFirstRecord("ID", vsData.TextMatrix(intCounter, 14));
							if (rsData.NoMatch)
							{
								rsData.AddNew();
								modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Add New Entry");
								strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
								strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
								strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
								strSQL += "," + vsData.TextMatrix(intCounter, 3);
								strSQL += "," + vsData.TextMatrix(intCounter, 5);
								strSQL += "," + vsData.TextMatrix(intCounter, 7);
								strSQL += "," + vsData.TextMatrix(intCounter, 8);
								strSQL += "," + vsData.TextMatrix(intCounter, 9);
								strSQL += ",'" + vsData.TextMatrix(intCounter, 10) + "'";
								strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
								strSQL += ")";
								rsDelete.Execute(strSQL, "Twpy0000.vb1");
							}
							else
							{
								rsData.Edit();
								modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Edit Entry");
								strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
								strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
								strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
								strSQL += "," + vsData.TextMatrix(intCounter, 3);
								strSQL += "," + vsData.TextMatrix(intCounter, 5);
								strSQL += "," + vsData.TextMatrix(intCounter, 7);
								strSQL += "," + FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 8)) - Conversion.Val(rsData.Get_Fields("amount")));
								strSQL += "," + vsData.TextMatrix(intCounter, 9);
								strSQL += ",'" + vsData.TextMatrix(intCounter, 10) + "'";
								strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
								strSQL += ")";
								rsDelete.Execute(strSQL, "Twpy0000.vb1");
							}
						}
						rsData.Set_Fields("RecipientID", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 3))));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "RecipientID", FCConvert.ToString(rsData.Get_Fields_Int32("RecipientID")));
						rsData.Set_Fields("CategoryID", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 5))));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "CategoryID", FCConvert.ToString(rsData.Get_Fields_Int32("CategoryID")));
						rsData.Set_Fields("DeductionID", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 7))));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "DeductionID", FCConvert.ToString(rsData.Get_Fields_Int32("DeductionID")));
						rsData.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 8))));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Amount", FCConvert.ToString(rsData.Get_Fields("Amount")));
						rsData.Set_Fields("CheckNumber", FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 9))));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "CheckNumber", FCConvert.ToString(rsData.Get_Fields("CheckNumber")));
						rsData.Set_Fields("DeductionAccountNumber", vsData.TextMatrix(intCounter, 10));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Account Number", rsData.Get_Fields_String("DeductionAccountNumber"));
						rsData.Set_Fields("UserID", vsData.TextMatrix(intCounter, 11));
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "UserID", FCConvert.ToString(rsData.Get_Fields("UserID")));
						if (vsData.TextMatrix(intCounter, 12) == string.Empty)
						{
							rsData.Set_Fields("Date", DateTime.Today);
						}
						else
						{
							rsData.Set_Fields("Date", vsData.TextMatrix(intCounter, 12));
						}
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Date", FCConvert.ToString(rsData.Get_Fields("Date")));
						if (vsData.TextMatrix(intCounter, 13) == string.Empty)
						{
							rsData.Set_Fields("Time", fecherFoundation.DateAndTime.TimeOfDay);
						}
						else
						{
							rsData.Set_Fields("Time", vsData.TextMatrix(intCounter, 13));
						}
						modGlobalFunctions.AddCYAEntry_6("PY", "TA Reduction Adjustment", "Time ", FCConvert.ToString(rsData.Get_Fields("Time")));
						rsData.Update();
					}
					else
					{
						// delete this record
						// Dave 12/14/2006---------------------------------------------------
						// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
						modAuditReporting.RemoveGridRow_8("vsData", intTotalNumberOfControls - 1, vsData.Row, clsControlInfo);
						// We then add a change record saying the row was deleted
						clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsData.Row) + " from Adjust T&A Records");
						// -------------------------------------------------------------------
						if (vsData.TextMatrix(intCounter, 14) == string.Empty)
						{
							vsData.RemoveItem(intCounter);
						}
						else
						{
							strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
							strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
							strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
							strSQL += "," + vsData.TextMatrix(intCounter, 3);
							strSQL += "," + vsData.TextMatrix(intCounter, 5);
							strSQL += "," + vsData.TextMatrix(intCounter, 7);
							strSQL += "," + FCConvert.ToString(Conversion.Val(vsData.TextMatrix(intCounter, 8)) * -1);
							strSQL += "," + vsData.TextMatrix(intCounter, 9);
							strSQL += ",'" + vsData.TextMatrix(intCounter, 10) + "'";
							strSQL += "," + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
							strSQL += ")";
							rsDelete.Execute(strSQL, "Twpy0000.vb1");
							rsDelete.Execute("Delete from tblTAReduction where ID = " + vsData.TextMatrix(intCounter, 14), "Payroll");
							modGlobalFunctions.AddCYAEntry_8("PY", "TA Reduction Adjustment", "Delete ID", vsData.TextMatrix(intCounter, 14));
						}
					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Adjust T&A Records", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
            // ----------------------------------------------------------------
            //Close();
            MessageBox.Show("Save Successful");
		}

		private void vsData_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsData.TextMatrix(vsData.Row, 0, FCConvert.ToString(true));
			if (vsData.Col == 2)
			{
				vsData.TextMatrix(vsData.Row, 3, FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.Row, 2)));
			}
			if (vsData.Col == 4)
			{
				vsData.TextMatrix(vsData.Row, 5, FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.Row, 4)));
			}
			if (vsData.Col == 6)
			{
				vsData.TextMatrix(vsData.Row, 7, FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.Row, 6)));
			}
			if (vsData.Col == 8)
			{
				vsData.TextMatrix(vsData.Row, 8, Strings.Format(vsData.TextMatrix(vsData.Row, 8), "0.00"));
			}
		}

		private void SetGridProperties()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblRecipients order by Name");
			if (!rsData.EndOfFile())
			{
				rsData.MoveLast();
				rsData.MoveFirst();
				// makes this column work like a combo box.
				// this fills the box with the values from the tblPayCategories table
				// This forces this column to work as a combo box
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					if (intCounter == 1)
					{
						vsData.ColComboList(2, vsData.ColComboList(2) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_String("Name"));
					}
					else
					{
						vsData.ColComboList(2, vsData.ColComboList(2) + "|#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_String("Name"));
					}
					rsData.MoveNext();
				}
			}
			rsData.OpenRecordset("Select * from tblCategories order by Description");
			if (!rsData.EndOfFile())
			{
				rsData.MoveLast();
				rsData.MoveFirst();
				// makes this column work like a combo box.
				// this fills the box with the values from the tblPayCategories table
				// This forces this column to work as a combo box
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					if (intCounter == 1)
					{
						vsData.ColComboList(4, vsData.ColComboList(4) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields("Description"));
					}
					else
					{
						vsData.ColComboList(4, vsData.ColComboList(4) + "|#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields("Description"));
					}
					rsData.MoveNext();
				}
			}
			rsData.OpenRecordset("Select * from tblDeductionSetup order by Description");
			if (!rsData.EndOfFile())
			{
				rsData.MoveLast();
				rsData.MoveFirst();
				// makes this column work like a combo box.
				// this fills the box with the values from the tblPayCategories table
				// This forces this column to work as a combo box
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					if (intCounter == 1)
					{
						vsData.ColComboList(6, vsData.ColComboList(6) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields("Description"));
					}
					else
					{
						vsData.ColComboList(6, vsData.ColComboList(6) + "|#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields("Description"));
					}
					rsData.MoveNext();
				}
			}
		}
		// Private Sub vsData_KeyUpEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// If Col = 10 Then
		// Call CheckAccountKeyCode(vsData, Row, Col, KeyCode, 0)
		// KeyCode = 0
		// End If
		// End Sub
		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Col == 12 || vsData.Col == 13)
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
		}
		// Private Sub vsData_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Col = 10 Then
		// Cancel = CheckAccountValidate(vsData, Row, Col, Cancel)
		// Exit Sub
		// End If
		// End Sub
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsData.Rows - 1); intRows++)
			{
				for (intCols = 2; intCols <= (vsData.Cols - 1); intCols++)
				{
					if (intCols != 14 && intCols != 3 && intCols != 5)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsData";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsData.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}
	}
}
