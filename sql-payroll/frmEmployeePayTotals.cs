//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmEmployeePayTotals : BaseForm
	{
		public frmEmployeePayTotals()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEmployeePayTotals InstancePtr
		{
			get
			{
				return (frmEmployeePayTotals)Sys.GetInstance(typeof(frmEmployeePayTotals));
			}
		}

		protected frmEmployeePayTotals _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEEp
		// DATE:       MAY 07,2001
		//
		// Modified by: DAN C. SOLTESZ    on 06/11/2001
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsPayTotals = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private bool boolStandardDeduction;
		private bool boolNew;
		private bool boolCategories;
		// vbPorter upgrade warning: intNumRowsLocked As int	OnWriteFCConvert.ToInt32(
		private int intNumRowsLocked;
		private int intFedWageRow;
		private int intMinWage;
		private int intMaxWage;
		private int intSaveStartBorder;
		private bool boolFromMatch;
		private string intChildID = string.Empty;
		private bool pboolFormLoading;
		private bool boolUseTemp;
		const int RECNUMBER = 0;
		const int DESC = 1;
		const int Amount = 2;
		const int MTDTotal = 3;
		const int QTDTotal = 4;
		const int FISCALTOTAL = 5;
		const int CALENDARTOTAL = 6;
		const int ID = 7;
		const int PayCategory = 8;
		const int StandardPayTotal = 9;
		const int CHANGEDFIELD = 10;

		private void frmEmployeePayTotals_Resize(object sender, System.EventArgs e)
		{
			// vsPayTotals.ColWidth(RECNUMBER) = vsPayTotals.Width * 0.05
			vsPayTotals.ColWidth(DESC, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.20));
			vsPayTotals.ColWidth(Amount, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.10));
			vsPayTotals.ColWidth(MTDTotal, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.10));
			vsPayTotals.ColWidth(QTDTotal, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.10));
			vsPayTotals.ColWidth(FISCALTOTAL, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.10));
			vsPayTotals.ColWidth(CALENDARTOTAL, FCConvert.ToInt32(vsPayTotals.WidthOriginal * 0.10));
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				LoadGrid();
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			// unload the form
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		private void frmEmployeePayTotals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployeePayTotals_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				boolUseTemp = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from tblpayrollsteps where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' order by payrunid desc", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("VerifyAccept")))
					{
						boolUseTemp = true;
					}
				}
				modGlobalRoutines.Statics.gboolShowVerifyTotalsMessage = false;
				boolFromMatch = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				// open the forms global database connection
				rsPayTotals.DefaultDB = "TWPY0000.vb1";
				// set the grid column headers/widths/etc....
				SetGridProperties();
				modMultiPay.Statics.gstrEmployeeCaption = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modMultiPay.SetMultiPayCaptions(this);
				// Load the grid with data
				LoadGrid();
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					lblEmployeeNumber.Text = "";
				}
				else
				{
					lblEmployeeNumber.Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				}
				intNumRowsLocked = vsPayTotals.Rows;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void EnableButtons(bool boolState)
		{
			cmdDelete.Enabled = boolState;
			cmdNew.Enabled = boolState;
			cmdRefresh.Enabled = boolState;
			//cmdSave.Enabled = boolState;
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				vsPayTotals.Rows = 1;
				vsPayTotals.Enabled = true;
				EnableButtons(true);
				pboolFormLoading = true;
				rsPayTotals.OpenRecordset("Select * from tblPayCategories order by CategoryNumber", "TWPY0000.vb1");
				LoadCategoryNames();
				rsPayTotals.OpenRecordset("Select * from tblStandardPayTotals order by RecordNumber", "TWPY0000.vb1");
				LoadStandardNames();
				LoadData();
				AddMatchData();
				AddMSRSData("DistM");
				AddMSRSData("DistU");
				pboolFormLoading = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void AddMatchData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				string strSQL1;
				string strSQL2;
				DateTime dtStart = DateTime.FromOADate(0);
				DateTime dtEnd = DateTime.FromOADate(0);
				vsPayTotals.Rows += 1;
				lngRow = vsPayTotals.Rows - 1;
				vsPayTotals.TextMatrix(lngRow, DESC, "Employer's Match");
				strSQL1 = "select sum(matchamount) as tot from tblcheckdetail where checkvoid = 0 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and matchrecord = 1";
				// cytd
				strSQL2 = strSQL1 + " and paydate between '1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsPayTotals.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("tot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
				}
				// mtd
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsPayTotals.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("tot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
				}
				// qtd
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsPayTotals.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("tot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
				}
				// fytd
				dtStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsPayTotals.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("tot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
				}
				// current
				if (boolUseTemp)
				{
					strSQL1 = "select sum(matchamount) as tot from tbltemppayprocess where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and matchrecord = 1";
					strSQL2 = strSQL1;
				}
				else
				{
					strSQL2 = strSQL1 + " and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				}
				rsPayTotals.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("tot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
				}
				boolFromMatch = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddMatchData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddMSRSData(string strType)
		{
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			DateTime dtFStart;
			double dblTD;
			double dblDed = 0;
			bool boolUnemp = false;
			if (fecherFoundation.Strings.UCase(strType) == "DISTU")
			{
				boolUnemp = true;
			}
			else
			{
				boolUnemp = false;
			}
			modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, modGlobalVariables.Statics.gdatCurrentPayDate);
			dtFStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
			// TH
			if (!boolUseTemp)
			{
				if (boolUnemp)
				{
					rsPayTotals.OpenRecordset("Select sum(dedamount) as Totded from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 AND PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and CheckVoid = 0)", "TWPY0000.vb1");
					if (!rsPayTotals.EndOfFile())
					{
						dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
					}
					else
					{
						dblDed = 0;
					}
				}
				rsPayTotals.OpenRecordset("Select sum(DistGrossPay) as TotalSum from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND " + strType + " <> 'N' AND PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and CheckVoid = 0)", "TWPY0000.vb1");
			}
			else
			{
				if (boolUnemp)
				{
					rsPayTotals.OpenRecordset("Select sum(dedamount) as Totded from tbltemppayprocess where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 and checkvoid = 0)", "TWPY0000.vb1");
					if (!rsPayTotals.EndOfFile())
					{
						dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
					}
					else
					{
						dblDed = 0;
					}
				}
				rsPayTotals.OpenRecordset("select sum(distgrosspay) as totalsum from tbltemppayprocess where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and " + strType + " <> 'N' ", "twpy0000.vb1");
			}
			if (!rsPayTotals.EndOfFile())
			{
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				vsPayTotals.Rows += 1;
				vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, RECNUMBER, FCConvert.ToString(vsPayTotals.Rows - 1));
				if (strType == "DistM")
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, DESC, "MainePERS Earnings");
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, Amount, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum"))));
				}
				else
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, DESC, "Unemployment Earnings");
					if (Conversion.Val(rsPayTotals.Get_Fields("totalsum")) > dblDed)
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, Amount, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum")) - dblDed));
					}
					else
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, Amount, FCConvert.ToString(0));
					}
				}
			}
			// mtd
			if (boolUnemp)
			{
				rsPayTotals.OpenRecordset("Select sum(Dedamount) as Totded from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 and month(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "  AND Year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") and CheckVoid = 0", "TWPY0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
				}
				else
				{
					dblDed = 0;
				}
			}
			rsPayTotals.OpenRecordset("Select sum(DistGrossPay) as TotalSum from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND " + strType + " <> 'N' AND month(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "  AND Year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") and CheckVoid = 0", "TWPY0000.vb1");
			if (!rsPayTotals.EndOfFile())
			{
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				if (!boolUnemp)
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, MTDTotal, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum"))));
				}
				else
				{
					if (Conversion.Val(rsPayTotals.Get_Fields("totalsum")) > dblDed)
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, MTDTotal, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("totalsum")) - dblDed));
					}
					else
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, MTDTotal, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, MTDTotal, FCConvert.ToString(0));
			}
			// qtd
			if (boolUnemp)
			{
				rsPayTotals.OpenRecordset("Select sum(Dedamount) as Totded from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 and PayDate >= '" + FCConvert.ToString(dtStart) + "' AND PayDate <= '" + FCConvert.ToString(dtEnd) + "') and CheckVoid = 0", "TWPY0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
				}
				else
				{
					dblDed = 0;
				}
			}
			rsPayTotals.OpenRecordset("Select sum(DistGrossPay) as TotalSum from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND " + strType + " <> 'N' AND PayDate >= '" + FCConvert.ToString(dtStart) + "' AND PayDate <= '" + FCConvert.ToString(dtEnd) + "') and CheckVoid = 0", "TWPY0000.vb1");
			if (!rsPayTotals.EndOfFile())
			{
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				if (!boolUnemp)
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, QTDTotal, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum"))));
				}
				else
				{
					if (Conversion.Val(rsPayTotals.Get_Fields("totalsum")) > dblDed)
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, QTDTotal, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("totalsum")) - dblDed));
					}
					else
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, QTDTotal, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, QTDTotal, FCConvert.ToString(0));
			}
			// fytd
			if (boolUnemp)
			{
				rsPayTotals.OpenRecordset("Select sum(Dedamount) as Totded from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 and PayDate >= '" + FCConvert.ToString(dtFStart) + "' AND PayDate <= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "') and CheckVoid = 0", "TWPY0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
				}
				else
				{
					dblDed = 0;
				}
			}
			rsPayTotals.OpenRecordset("Select sum(DistGrossPay) as TotalSum from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND " + strType + " <> 'N' AND PayDate >= '" + FCConvert.ToString(dtFStart) + "' AND PayDate <= '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "') and CheckVoid = 0", "TWPY0000.vb1");
			if (!rsPayTotals.EndOfFile())
			{
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				if (!boolUnemp)
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, FISCALTOTAL, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum"))));
				}
				else
				{
					if (Conversion.Val(rsPayTotals.Get_Fields("totalsum")) > dblDed)
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, FISCALTOTAL, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("totalsum")) - dblDed));
					}
					else
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, FISCALTOTAL, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, FISCALTOTAL, FCConvert.ToString(0));
			}
			// ytd
			if (boolUnemp)
			{
				rsPayTotals.OpenRecordset("Select sum(Dedamount) as Totded from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND uexempt = 1 and deductionrecord = 1 and year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") and CheckVoid = 0", "TWPY0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					dblDed = Conversion.Val(rsPayTotals.Get_Fields("totded"));
				}
				else
				{
					dblDed = 0;
				}
			}
			rsPayTotals.OpenRecordset("Select sum(DistGrossPay) as TotalSum from tblCheckDetail where (EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' AND " + strType + " <> 'N' AND year(PayDate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + ") and CheckVoid = 0", "TWPY0000.vb1");
			if (!rsPayTotals.EndOfFile())
			{
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				if (!boolUnemp)
				{
					vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, CALENDARTOTAL, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("TotalSum"))));
				}
				else
				{
					if (Conversion.Val(rsPayTotals.Get_Fields("totalsum")) > dblDed)
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, CALENDARTOTAL, FCConvert.ToString(Conversion.Val(rsPayTotals.Get_Fields("totalsum")) - dblDed));
					}
					else
					{
						vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, CALENDARTOTAL, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				vsPayTotals.TextMatrix(vsPayTotals.Rows - 1, CALENDARTOTAL, FCConvert.ToString(0));
			}
		}

		private void LoadCategoryNames()
		{
			if (!rsPayTotals.EndOfFile())
			{
				// this will make the recordcount property work correctly
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				// set the number of rows in the grid
				vsPayTotals.Rows += rsPayTotals.RecordCount();
				// fill the grid
				for (intCounter = 1; intCounter <= (vsPayTotals.Rows - 1); intCounter++)
				{
					vsPayTotals.TextMatrix(intCounter, RECNUMBER, FCConvert.ToString(intCounter));
					vsPayTotals.TextMatrix(intCounter, DESC, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("Description") + " "));
					vsPayTotals.TextMatrix(intCounter, Amount, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, MTDTotal, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, QTDTotal, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, FISCALTOTAL, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, CALENDARTOTAL, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, ID, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, PayCategory, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("ID") + " "));
					rsPayTotals.MoveNext();
				}
			}
		}

		private void LoadStandardNames()
		{
			// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToInt32(
			int intStart = 0;
			if (!rsPayTotals.EndOfFile())
			{
				// this will make the recordcount property work correctly
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				// set the number of rows in the grid
				vsPayTotals.Rows += rsPayTotals.RecordCount();
				intStart = (vsPayTotals.Rows - rsPayTotals.RecordCount());
				intSaveStartBorder = intStart;
				vsPayTotals.Select(intStart, RECNUMBER, intStart, PayCategory);
				vsPayTotals.CellBorder(ColorTranslator.FromOle(Information.RGB(0, 0, 0)), -1, 1, -1, -1, -1, -1);
				intFedWageRow = 0;
				intMinWage = 0;
				intMaxWage = 0;
				// FILL
				for (intCounter = (vsPayTotals.Rows - rsPayTotals.RecordCount()); intCounter <= (vsPayTotals.Rows - 1); intCounter++)
				{
					vsPayTotals.TextMatrix(intCounter, RECNUMBER, FCConvert.ToString(intCounter));
					vsPayTotals.TextMatrix(intCounter, DESC, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("Description") + " "));
					if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(vsPayTotals.TextMatrix(intCounter, DESC))) == "FEDERAL WAGE")
					{
						intFedWageRow = intCounter;
					}
					if (Strings.Right(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(vsPayTotals.TextMatrix(intCounter, DESC))), QTDTotal) == "WAGE")
					{
						if (intMinWage == 0)
						{
							intMinWage = intCounter;
						}
						else
						{
							intMaxWage = intCounter;
						}
					}
					vsPayTotals.TextMatrix(intCounter, Amount, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, MTDTotal, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, QTDTotal, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, FISCALTOTAL, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, CALENDARTOTAL, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, ID, FCConvert.ToString(0));
					vsPayTotals.TextMatrix(intCounter, PayCategory, FCConvert.ToString(0));
					// Standard Pay Total Caption ID
					vsPayTotals.TextMatrix(intCounter, StandardPayTotal, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("ID") + " "));
					if (intStart + 5 == intCounter)
					{
						vsPayTotals.Select(intCounter, 0, intCounter, PayCategory);
						vsPayTotals.CellBorder(ColorTranslator.FromOle(Information.RGB(0, 0, 0)), -1, 1, -1, -1, -1, -1);
					}
					rsPayTotals.MoveNext();
				}
				vsPayTotals.Select(vsPayTotals.Rows - 3, RECNUMBER, vsPayTotals.Rows - 3, PayCategory);
				vsPayTotals.CellBorder(ColorTranslator.FromOle(Information.RGB(0, 0, 200)), -1, 1, -1, -1, -1, -1);
				vsPayTotals.Select(vsPayTotals.Rows - 1, RECNUMBER, vsPayTotals.Rows - 1, PayCategory);
				vsPayTotals.CellBorder(ColorTranslator.FromOle(Information.RGB(0, 0, 0)), -1, 1, -1, -1, -1, -1);
			}
		}

		private void LoadCategoryData()
		{
			if (!rsPayTotals.EndOfFile())
			{
				// this will make the recordcount property work correctly
				rsPayTotals.MoveLast();
				rsPayTotals.MoveFirst();
				// fill the grid
				for (intCounter = 1; intCounter <= (vsPayTotals.Rows - 1); intCounter++)
				{
					if (!rsPayTotals.EndOfFile())
					{
						vsPayTotals.TextMatrix(intCounter, Amount, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("CurrentTotal") + " "));
						vsPayTotals.TextMatrix(intCounter, MTDTotal, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("MTDTotal") + " "));
						vsPayTotals.TextMatrix(intCounter, QTDTotal, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("QTDTotal") + " "));
						vsPayTotals.TextMatrix(intCounter, FISCALTOTAL, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("FiscalYTDTotal") + " "));
						vsPayTotals.TextMatrix(intCounter, CALENDARTOTAL, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("CalendarYTDTotal") + " "));
						vsPayTotals.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsPayTotals.Get_Fields("tblPayTotals.ID") + " "));
						rsPayTotals.MoveNext();
					}
					else
					{
						vsPayTotals.TextMatrix(intCounter, Amount, FCConvert.ToString(0));
						vsPayTotals.TextMatrix(intCounter, MTDTotal, FCConvert.ToString(0));
						vsPayTotals.TextMatrix(intCounter, QTDTotal, FCConvert.ToString(0));
						vsPayTotals.TextMatrix(intCounter, FISCALTOTAL, FCConvert.ToString(0));
						vsPayTotals.TextMatrix(intCounter, CALENDARTOTAL, FCConvert.ToString(0));
						vsPayTotals.TextMatrix(intCounter, ID, FCConvert.ToString(0));
					}
				}
			}
		}

		private void LoadData()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL1;
			string strSQL2;
			// vbPorter upgrade warning: lngRow As int	OnWrite(int, bool)
			int lngRow;
			double dblCYTDTotal;
			double dblFYTDTotal;
			double dblMTDTotal;
			double dblQTDTotal;
			double dblCTDTotal;
			DateTime dtStart = DateTime.FromOADate(0);
			DateTime dtEnd = DateTime.FromOADate(0);
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				dblCYTDTotal = 0;
				dblFYTDTotal = 0;
				dblMTDTotal = 0;
				dblQTDTotal = 0;
				dblCTDTotal = 0;
				strSQL1 = "Select sum(distgrosspay) as tot,distpaycategory from tblcheckdetail where checkvoid = 0 and distributionrecord = 1 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'";
				// CYTD
				strSQL2 = strSQL1 + " and paydate between '" + "1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by distpaycategory order by distpaycategory ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory)) > 0)
						{
							if (rsLoad.FindFirstRecord("distpaycategory", Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory))))
							{
								vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("tot"))), "0.00"));
								dblCYTDTotal += Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
							}
						}
						else
						{
							break;
						}
					}
					// lngRow
				}
				else
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
					}
					// lngRow
				}
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by distpaycategory order by distpaycategory ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory)) > 0)
						{
							if (rsLoad.FindFirstRecord("distpaycategory", Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory))))
							{
								vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("tot"))), "0.00"));
								dblMTDTotal += Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
							}
						}
						else
						{
							break;
						}
					}
					// lngRow
				}
				else
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
					}
					// lngRow
				}
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by distpaycategory order by distpaycategory ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory)) > 0)
						{
							if (rsLoad.FindFirstRecord("Distpaycategory", Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory))))
							{
								vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("tot"))), "0.00"));
								dblQTDTotal += Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
							}
						}
						else
						{
							break;
						}
					}
					// lngRow
				}
				else
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
					}
					// lngRow
				}
				dtStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by distpaycategory order by distpaycategory ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory)) > 0)
						{
							if (rsLoad.FindFirstRecord("distpaycategory", Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory))))
							{
								vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("tot"))), "0.00"));
								dblFYTDTotal += Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
							}
						}
						else
						{
							break;
						}
					}
					// lngRow
				}
				else
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
					}
					// lngRow
				}
				if (boolUseTemp)
				{
					strSQL1 = "Select sum(distgrosspay) as tot,distpaycategory from tbltemppayprocess where distributionrecord = 1 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'";
					strSQL2 = strSQL1 + " group by distpaycategory order by distpaycategory ";
				}
				else
				{
					strSQL2 = strSQL1 + " and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by distpaycategory order by distpaycategory ";
				}
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					for (lngRow = 1; lngRow <= vsPayTotals.Rows - 1; lngRow++)
					{
						if (Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory)) > 0)
						{
							if (rsLoad.FindFirstRecord("distpaycategory", Conversion.Val(vsPayTotals.TextMatrix(lngRow, PayCategory))))
							{
								vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("tot"))), "0.00"));
								dblCTDTotal += Conversion.Val(rsLoad.Get_Fields("tot"));
							}
							else
							{
								vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
							}
						}
						else
						{
							break;
						}
					}
					// lngRow
				}
				else
				{
					for (lngRow = 1; lngRow <= (vsPayTotals.Rows == 1 ? -1 : 0); lngRow++)
					{
						vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
					}
					// lngRow
				}
				lngRow = vsPayTotals.Rows - 3;
				vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(dblFYTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(dblQTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(dblMTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(dblCYTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(dblCTDTotal, "0.00"));
				lngRow = vsPayTotals.Rows - 2;
				
				//EIC removed
				vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
				vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
				vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
				vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
				vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
				
				lngRow = vsPayTotals.Rows - 1;
				vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(dblFYTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(dblQTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(dblMTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(dblCYTDTotal, "0.00"));
				vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(dblCTDTotal, "0.00"));
				// TAX
				lngRow = vsPayTotals.Rows - 13;
				// cytd
				strSQL1 = "select sum(federaltaxwh) as fedtot,sum(ficataxwh) as ficatot,sum(medicaretaxwh) as medtot,sum(statetaxwh) as statetot from tblcheckdetail where checkvoid = 0 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ";
				strSQL2 = strSQL1 + " and paydate between '1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, CALENDARTOTAL, "0.00");
				}
				// mtd
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, MTDTotal, "0.00");
				}
				// qtd
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, QTDTotal, "0.00");
				}
				// fiscal
				dtStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, FISCALTOTAL, "0.00");
				}
				// current
				if (boolUseTemp)
				{
					strSQL1 = "select sum(federaltaxwh) as fedtot,sum(ficataxwh) as ficatot,sum(medicaretaxwh) as medtot,sum(statetaxwh) as statetot from tbltemppayprocess where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ";
					strSQL2 = strSQL1;
				}
				else
				{
					strSQL2 = strSQL1 + " and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				}
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, Amount, "0.00");
				}
				// taxable gross
				lngRow = vsPayTotals.Rows - 8;
				// cytd
				strSQL1 = "select sum(federaltaxgross) as fedtot,sum(ficataxgross) as ficatot,sum(medicaretaxgross) as medtot,sum(statetaxgross) as statetot from tblcheckdetail where checkvoid = 0 and totalrecord = 1 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ";
				strSQL2 = strSQL1 + " and paydate between '1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, CALENDARTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, CALENDARTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, CALENDARTOTAL, "0.00");
				}
				// mtd
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Month) + "/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, MTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, MTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, MTDTotal, "0.00");
				}
				// qtd
				modGlobalRoutines.GetDatesForAQuarter(ref dtStart, ref dtEnd, modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, QTDTotal, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, QTDTotal, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, QTDTotal, "0.00");
				}
				// fiscal
				dtStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				strSQL2 = strSQL1 + " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, FISCALTOTAL, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, FISCALTOTAL, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, FISCALTOTAL, "0.00");
				}
				// current
				if (boolUseTemp)
				{
					strSQL1 = "select sum(federaltaxgross) as fedtot,sum(ficataxgross) as ficatot,sum(medicaretaxgross) as medtot,sum(statetaxgross) as statetot from tbltemppayprocess where totalrecord = 1 and employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' ";
					strSQL2 = strSQL1;
				}
				else
				{
					strSQL2 = strSQL1 + " and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ";
				}
				rsLoad.OpenRecordset(strSQL2, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					vsPayTotals.TextMatrix(lngRow, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fedtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 1, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ficatot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 2, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("medtot"))), "0.00"));
					vsPayTotals.TextMatrix(lngRow + 3, Amount, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("statetot"))), "0.00"));
				}
				else
				{
					vsPayTotals.TextMatrix(lngRow, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 1, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 2, Amount, "0.00");
					vsPayTotals.TextMatrix(lngRow + 3, Amount, "0.00");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsPayTotals.WordWrap = true;
				vsPayTotals.Cols = 11;
				vsPayTotals.FixedCols = 2;
				vsPayTotals.ColHidden(ID, true);
				vsPayTotals.ColHidden(PayCategory, true);
				vsPayTotals.ColHidden(StandardPayTotal, true);
				vsPayTotals.ColHidden(CHANGEDFIELD, true);
				modGlobalRoutines.CenterGridCaptions(ref vsPayTotals);
				// set column 0 properties
				vsPayTotals.ColAlignment(RECNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsPayTotals.ColWidth(RECNUMBER) = 400
				vsPayTotals.ColHidden(RECNUMBER, true);
				// set column 1 properties
				vsPayTotals.ColAlignment(DESC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsPayTotals.ColWidth(DESC) = 2000
				vsPayTotals.TextMatrix(0, DESC, "Description");
				// set column 2 properties
				vsPayTotals.ColAlignment(Amount, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsPayTotals.ColWidth(Amount) = 1200
				vsPayTotals.TextMatrix(0, Amount, "Current");
				vsPayTotals.ColFormat(Amount, "#,##0.00;(#,###.00)");
				// set column 3 properties
				vsPayTotals.ColAlignment(MTDTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsPayTotals.ColWidth(MTDTotal) = 1150
				vsPayTotals.TextMatrix(0, MTDTotal, "MTD Total");
				vsPayTotals.ColFormat(MTDTotal, "#,##0.00;(#,###.00)");
				// set column 4 properties
				vsPayTotals.ColAlignment(QTDTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsPayTotals.ColWidth(QTDTotal) = 1150
				vsPayTotals.TextMatrix(0, QTDTotal, "QTD Total");
				vsPayTotals.ColFormat(QTDTotal, "#,##0.00;(#,###.00)");
				// set column 5 properties
				vsPayTotals.ColAlignment(FISCALTOTAL, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsPayTotals.ColWidth(FISCALTOTAL) = 1150
				vsPayTotals.TextMatrix(0, FISCALTOTAL, "Fiscal Total");
				vsPayTotals.ColFormat(FISCALTOTAL, "#,##0.00;(#,###.00)");
				// set column 6 properties
				vsPayTotals.ColAlignment(CALENDARTOTAL, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsPayTotals.ColWidth(CALENDARTOTAL) = 1150
				vsPayTotals.TextMatrix(0, CALENDARTOTAL, "Calendar Total");
				vsPayTotals.ColFormat(CALENDARTOTAL, "#,##0.00;(#,###.00)");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuCaption1_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(1));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = FCConvert.ToBoolean(0);
			LoadGrid();
			lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
		}

		private void mnuCaption2_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(2));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption3_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(3));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption4_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(4));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption5_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(5));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption6_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(6));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption7_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(7));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption8_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(8));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption9_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToString(modMultiPay.MultiPayCaption(9));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = intChildID;
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblEmployeeNumber.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptPayTotals.InstancePtr.Init(ref modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
		}

		public void LockGridCells(int intStart)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LockGridCells";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsPayTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, DESC, RECNUMBER, intStart + 1, DESC, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				if (intStart < vsPayTotals.Rows - 1)
				{
					vsPayTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intStart + 1, DESC, (vsPayTotals.Rows - 1), DESC, Color.White);
				}
				vsPayTotals.FixedCols = 1;
				intNumRowsLocked = intStart;
				vsPayTotals.Refresh();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
	}
}
