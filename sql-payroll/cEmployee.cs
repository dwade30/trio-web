﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWPY0000
{
	public class cEmployee
	{
		//=========================================================
		private int lngRecordID;
		private string strEmployeeNumber = string.Empty;
		private string strSSN = string.Empty;
		private string strFirstName = "";
		private string strMiddleName = "";
		private string strLastName = "";
		private string strDesignation = "";
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;
		private string strEmail = string.Empty;
		private string strPhone = string.Empty;
		private string strDateHired = string.Empty;
		private string strAnniversaryDate = string.Empty;
		private string strBirthDate = string.Empty;
		private string strTerminationDate = string.Empty;
		private string strRetirementDate = string.Empty;
		private string strSex = string.Empty;
		private string strStatus = string.Empty;
		private string strDepDiv = string.Empty;
		private string strDepartment = string.Empty;
		private int intSequenceNumber;
		private string strGroupID = string.Empty;
		private string strCheck = string.Empty;
		private bool boolW2Pen;
		private bool boolW2DefIncome;
		private bool boolUnemploymentExempt;
		private bool boolFicaExempt;
		private double dblAddFed;
		private string strFedDollarPercent = string.Empty;
		private double dblAddState;
		private string strStateDollarPercent = string.Empty;
		private int lngFederalFilingStatusID;
		private int lngStateFilingStatusID;
		private int intFederalStatus;
		private int intStateStatus;
		private int lngFrequencyCodeID;
		private string strCode1 = string.Empty;
		private string strCode2 = string.Empty;
		private bool boolMedicareExempt;
		private string strWorkersCompCode = string.Empty;
		private double dblHoursPerDay;
		private double dblHoursPerWeek;
		private bool boolChildRecord;
		private bool boolUseParentDeductions;
		private bool boolPrintPayRate;
		private bool boolWorkersCompExempt;
		private double dblPeriodTaxes;
		private double dblPeriodDeductions;
		private string strMSRSType = string.Empty;
		private bool boolFederalFirstCheckOnly;
		private bool boolStateFirstCheckOnly;
		private string strBLSWorkSiteID = string.Empty;
		private bool boolW2StatutoryEmployee;
		private double dblContractAmount;
		private double dblContractPeriods;
		private double dblContractActualPeriods;
		private bool boolDeductionPercent;
		private bool boolFTorPT;
		private bool boolMqge;
		private int lngScheduleID;
		private int intCurrentScheduleWeek;
		private cGenericCollection listDistributions = new cGenericCollection();

		public cGenericCollection DistributionRecords
		{
			get
			{
				cGenericCollection DistributionRecords = null;
				DistributionRecords = listDistributions;
				return DistributionRecords;
			}
		}

		public string DepartmentDivision
		{
			set
			{
				strDepDiv = value;
			}
			get
			{
				string DepartmentDivision = "";
				DepartmentDivision = strDepDiv;
				return DepartmentDivision;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public int SequenceNumber
		{
			set
			{
				intSequenceNumber = value;
			}
			get
			{
				int SequenceNumber = 0;
				SequenceNumber = intSequenceNumber;
				return SequenceNumber;
			}
		}

		public string GroupID
		{
			set
			{
				strGroupID = value;
			}
			get
			{
				string GroupID = "";
				GroupID = strGroupID;
				return GroupID;
			}
		}

		public string Check
		{
			set
			{
				strCheck = value;
			}
			get
			{
				string Check = "";
				Check = strCheck;
				return Check;
			}
		}

		public bool W2Pension
		{
			set
			{
				boolW2Pen = value;
			}
			get
			{
				bool W2Pension = false;
				W2Pension = boolW2Pen;
				return W2Pension;
			}
		}

		public bool W2DeferredIncome
		{
			set
			{
				boolW2DefIncome = value;
			}
			get
			{
				bool W2DeferredIncome = false;
				W2DeferredIncome = boolW2DefIncome;
				return W2DeferredIncome;
			}
		}

		public bool IsUnemploymentExempt
		{
			set
			{
				boolUnemploymentExempt = value;
			}
			get
			{
				bool IsUnemploymentExempt = false;
				IsUnemploymentExempt = boolUnemploymentExempt;
				return IsUnemploymentExempt;
			}
		}

		public bool IsFicaExempt
		{
			set
			{
				boolFicaExempt = value;
			}
			get
			{
				bool IsFicaExempt = false;
				IsFicaExempt = boolFicaExempt;
				return IsFicaExempt;
			}
		}

		public double AdditionalFederal
		{
			set
			{
				dblAddFed = value;
			}
			get
			{
				double AdditionalFederal = 0;
				AdditionalFederal = dblAddFed;
				return AdditionalFederal;
			}
		}

		public string FederalDollarPercent
		{
			set
			{
				strFedDollarPercent = value;
			}
			get
			{
				string FederalDollarPercent = "";
				FederalDollarPercent = strFedDollarPercent;
				return FederalDollarPercent;
			}
		}

		public double AdditionalState
		{
			set
			{
				dblAddState = value;
			}
			get
			{
				double AdditionalState = 0;
				AdditionalState = dblAddState;
				return AdditionalState;
			}
		}

		public string StateDollarPercent
		{
			set
			{
				strStateDollarPercent = value;
			}
			get
			{
				string StateDollarPercent = "";
				StateDollarPercent = strStateDollarPercent;
				return StateDollarPercent;
			}
		}

		public int FederalFilingStatusID
		{
			set
			{
				lngFederalFilingStatusID = value;
			}
			get
			{
				int FederalFilingStatusID = 0;
				FederalFilingStatusID = lngFederalFilingStatusID;
				return FederalFilingStatusID;
			}
		}

		public int StateFilingStatusID
		{
			set
			{
				lngStateFilingStatusID = value;
			}
			get
			{
				int StateFilingStatusID = 0;
				StateFilingStatusID = lngStateFilingStatusID;
				return StateFilingStatusID;
			}
		}

		public int FederalStatus
		{
			set
			{
				intFederalStatus = value;
			}
			get
			{
				int FederalStatus = 0;
				FederalStatus = intFederalStatus;
				return FederalStatus;
			}
		}

		public int StateStatus
		{
			set
			{
				intStateStatus = value;
			}
			get
			{
				int StateStatus = 0;
				StateStatus = intStateStatus;
				return StateStatus;
			}
		}

		public int FrequencyCodeID
		{
			set
			{
				lngFrequencyCodeID = value;
			}
			get
			{
				int FrequencyCodeID = 0;
				FrequencyCodeID = lngFrequencyCodeID;
				return FrequencyCodeID;
			}
		}

		public string Code1
		{
			set
			{
				strCode1 = value;
			}
			get
			{
				string Code1 = "";
				Code1 = strCode1;
				return Code1;
			}
		}

		public string Code2
		{
			set
			{
				strCode2 = value;
			}
			get
			{
				string Code2 = "";
				Code2 = strCode2;
				return Code2;
			}
		}

		public bool IsMedicareExempt
		{
			set
			{
				boolMedicareExempt = value;
			}
			get
			{
				bool IsMedicareExempt = false;
				IsMedicareExempt = boolMedicareExempt;
				return IsMedicareExempt;
			}
		}

		public string WorkersCompCode
		{
			set
			{
				strWorkersCompCode = value;
			}
			get
			{
				string WorkersCompCode = "";
				WorkersCompCode = strWorkersCompCode;
				return WorkersCompCode;
			}
		}

		public double HoursPerDay
		{
			set
			{
				dblHoursPerDay = value;
			}
			get
			{
				double HoursPerDay = 0;
				HoursPerDay = dblHoursPerDay;
				return HoursPerDay;
			}
		}

		public double HoursPerWeek
		{
			set
			{
				dblHoursPerWeek = value;
			}
			get
			{
				double HoursPerWeek = 0;
				HoursPerWeek = dblHoursPerWeek;
				return HoursPerWeek;
			}
		}

		public bool IsChildRecord
		{
			set
			{
				boolChildRecord = value;
			}
			get
			{
				bool IsChildRecord = false;
				IsChildRecord = boolChildRecord;
				return IsChildRecord;
			}
		}

		public bool UseParentDeductions
		{
			set
			{
				boolUseParentDeductions = value;
			}
			get
			{
				bool UseParentDeductions = false;
				UseParentDeductions = boolUseParentDeductions;
				return UseParentDeductions;
			}
		}

		public bool PrintPayRate
		{
			set
			{
				boolPrintPayRate = value;
			}
			get
			{
				bool PrintPayRate = false;
				PrintPayRate = boolPrintPayRate;
				return PrintPayRate;
			}
		}

		public bool IsWorkersCompExempt
		{
			set
			{
				boolWorkersCompExempt = value;
			}
			get
			{
				bool IsWorkersCompExempt = false;
				IsWorkersCompExempt = boolWorkersCompExempt;
				return IsWorkersCompExempt;
			}
		}

		public double PeriodTaxes
		{
			set
			{
				dblPeriodTaxes = value;
			}
			get
			{
				double PeriodTaxes = 0;
				PeriodTaxes = dblPeriodTaxes;
				return PeriodTaxes;
			}
		}

		public double PeriodDeductions
		{
			set
			{
				dblPeriodDeductions = value;
			}
			get
			{
				double PeriodDeductions = 0;
				PeriodDeductions = dblPeriodDeductions;
				return PeriodDeductions;
			}
		}

		public string MSRSType
		{
			set
			{
				strMSRSType = value;
			}
			get
			{
				string MSRSType = "";
				MSRSType = strMSRSType;
				return MSRSType;
			}
		}

		public bool FederalFirstCheckOnly
		{
			set
			{
				boolFederalFirstCheckOnly = value;
			}
			get
			{
				bool FederalFirstCheckOnly = false;
				FederalFirstCheckOnly = boolFederalFirstCheckOnly;
				return FederalFirstCheckOnly;
			}
		}

		public bool StateFirstCheckOnly
		{
			set
			{
				boolStateFirstCheckOnly = value;
			}
			get
			{
				bool StateFirstCheckOnly = false;
				StateFirstCheckOnly = boolStateFirstCheckOnly;
				return StateFirstCheckOnly;
			}
		}

		public string BLSWorkSiteID
		{
			set
			{
				strBLSWorkSiteID = value;
			}
			get
			{
				string BLSWorkSiteID = "";
				BLSWorkSiteID = strBLSWorkSiteID;
				return BLSWorkSiteID;
			}
		}

		public bool W2StatutoryEmployee
		{
			set
			{
				boolW2StatutoryEmployee = value;
			}
			get
			{
				bool W2StatutoryEmployee = false;
				W2StatutoryEmployee = boolW2StatutoryEmployee;
				return W2StatutoryEmployee;
			}
		}

		public double ContractAmount
		{
			set
			{
				dblContractAmount = value;
			}
			get
			{
				double ContractAmount = 0;
				ContractAmount = dblContractAmount;
				return ContractAmount;
			}
		}

		public double ContractPeriods
		{
			set
			{
				dblContractPeriods = value;
			}
			get
			{
				double ContractPeriods = 0;
				ContractPeriods = dblContractPeriods;
				return ContractPeriods;
			}
		}

		public double ContractActualPeriods
		{
			set
			{
				dblContractActualPeriods = value;
			}
			get
			{
				double ContractActualPeriods = 0;
				ContractActualPeriods = dblContractActualPeriods;
				return ContractActualPeriods;
			}
		}

		public bool DeductionPercent
		{
			set
			{
				boolDeductionPercent = value;
			}
			get
			{
				bool DeductionPercent = false;
				DeductionPercent = boolDeductionPercent;
				return DeductionPercent;
			}
		}

		public bool IsPartTime
		{
			set
			{
				boolFTorPT = value;
			}
			get
			{
				bool IsPartTime = false;
				IsPartTime = boolFTorPT;
				return IsPartTime;
			}
		}

		public bool IsMQGE
		{
			set
			{
				boolMqge = value;
			}
			get
			{
				bool IsMQGE = false;
				IsMQGE = boolMqge;
				return IsMQGE;
			}
		}

		public int ScheduleID
		{
			set
			{
				lngScheduleID = value;
			}
			get
			{
				int ScheduleID = 0;
				ScheduleID = lngScheduleID;
				return ScheduleID;
			}
		}

		public int CurrentScheduleWeek
		{
			set
			{
				intCurrentScheduleWeek = value;
			}
			get
			{
				int CurrentScheduleWeek = 0;
				CurrentScheduleWeek = intCurrentScheduleWeek;
				return CurrentScheduleWeek;
			}
		}

		public string FullName
		{
			get
			{
				string FullName = "";
				FullName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strFirstName + " " + strMiddleName) + " " + strLastName) + " " + strDesignation);
				return FullName;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public string SSN
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SSN = "";
				SSN = strSSN;
				return SSN;
			}
		}

		public string FirstName
		{
			set
			{
				strFirstName = value;
			}
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
		}

		public string MiddleName
		{
			set
			{
				strMiddleName = value;
			}
			get
			{
				string MiddleName = "";
				MiddleName = strMiddleName;
				return MiddleName;
			}
		}

		public string LastName
		{
			set
			{
				strLastName = value;
			}
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
		}

		public string Designation
		{
			set
			{
				strDesignation = value;
			}
			get
			{
				string Designation = "";
				Designation = strDesignation;
				return Designation;
			}
		}

		public string Address1
		{
			set
			{
				strAddress1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddress2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string Zip4
		{
			set
			{
				strZip4 = value;
			}
			get
			{
				string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}

		public string Email
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
		}

		public string Phone
		{
			set
			{
				strPhone = value;
			}
			get
			{
				string Phone = "";
				Phone = strPhone;
				return Phone;
			}
		}

		public string DateHired
		{
			set
			{
				strDateHired = value;
			}
			get
			{
				string DateHired = "";
				DateHired = strDateHired;
				return DateHired;
			}
		}

		public string AnniversaryDate
		{
			set
			{
				strAnniversaryDate = value;
			}
			get
			{
				string AnniversaryDate = "";
				AnniversaryDate = strAnniversaryDate;
				return AnniversaryDate;
			}
		}

		public string BirthDate
		{
			set
			{
				strBirthDate = value;
			}
			get
			{
				string BirthDate = "";
				BirthDate = strBirthDate;
				return BirthDate;
			}
		}

		public string TerminationDate
		{
			set
			{
				strTerminationDate = value;
			}
			get
			{
				string TerminationDate = "";
				TerminationDate = strTerminationDate;
				return TerminationDate;
			}
		}

		public string RetirementDate
		{
			set
			{
				strRetirementDate = value;
			}
			get
			{
				string RetirementDate = "";
				RetirementDate = strRetirementDate;
				return RetirementDate;
			}
		}

		public string Sex
		{
			set
			{
				strSex = value;
			}
			get
			{
				string Sex = "";
				Sex = strSex;
				return Sex;
			}
		}

		public string Status
		{
			set
			{
				strStatus = value;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}
	}
}
