//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmDirectDeposit : BaseForm
	{
		public frmDirectDeposit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDirectDeposit InstancePtr
		{
			get
			{
				return (frmDirectDeposit)Sys.GetInstance(typeof(frmDirectDeposit));
			}
		}

		protected frmDirectDeposit _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: DAN C. SOLTESZ
		// DATE:       JULY 3,2001
		//
		// NOTES:  When using a flex grid set up constants for
		// the columns so if changes need to be made to the position
		// or new columns added all you have to do is change the constants
		// and not have to search through all the code and look
		// for each number.
		//
		// The dirty field is used to indicate if a row has been
		// changed or is new.  A ".." indicates its dirty and a
		// "." indicates its a new row and dirty.  This saves time
		// when doing saves.  We're only saving data that needs to be
		//
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsDirectDeposit = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		public bool boolFromDirectDeposit;
		private double dblAcctTotalPercent;
		const int CNSTGRIDCOLBAR = 0;
		const int Bank = 1;
		const int Account = 2;
		const int Amount = 3;
		const int DTYPE = 4;
		const int ID = 5;
		const int DIRTY = 6;
		const int CNSTGRIDCOLPRENOTE = 7;
		const int AccountType = 8;
		private bool boolAllTrue;
		private bool boolCantEdit;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;
		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void frmDirectDeposit_Resize(object sender, System.EventArgs e)
		{
			VSDeposit.ColWidth(CNSTGRIDCOLBAR, FCConvert.ToInt32(0.03 * VSDeposit.WidthOriginal));
			VSDeposit.ColWidth(Bank, FCConvert.ToInt32(VSDeposit.WidthOriginal * 0.38));
			VSDeposit.ColWidth(Account, FCConvert.ToInt32(VSDeposit.WidthOriginal * 0.14));
			VSDeposit.ColWidth(Amount, FCConvert.ToInt32(VSDeposit.WidthOriginal * 0.1));
			VSDeposit.ColWidth(DTYPE, FCConvert.ToInt32(VSDeposit.WidthOriginal * 0.11));
			VSDeposit.ColWidth(CNSTGRIDCOLPRENOTE, FCConvert.ToInt32(VSDeposit.WidthOriginal * 0.1));
            // .ColWidth(AccountType) = .Width * 0.14
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				SaveChanges();
				if (NotValidData())
					return;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				LoadGrid();
				modGlobalRoutines.SetEmployeeCaption(lblEmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "Twpy0000.vb1");
				if (!rsMaster.EndOfFile())
				{
					fraWarning.Visible = FCConvert.ToString(rsMaster.Get_Fields_String("Check")) == "Regular";
				}
				int counter;
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
			}
		}

		private void frmDirectDeposit_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call SetGridProperties
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmDirectDeposit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return && this.ActiveControl.GetName() == this.VSDeposit.Name)
			{
				Support.SendKeys("{TAB}", false);
				KeyCode = (Keys)0;
			}
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDirectDeposit_Load(object sender, System.EventArgs e)
		{
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolCantEdit = false;
				boolFromDirectDeposit = false;
				boolAllTrue = true;
				modGlobalFunctions.SetFixedSize(this, 0);
				
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewBankAccountNumbers.ToInteger()))
                {
                    case "F":
                        bankAccountViewPermission = BankAccountViewType.Full;
                        break;
                    case "P":
                        bankAccountViewPermission = BankAccountViewType.Masked;
                        break;
                    default:
                        bankAccountViewPermission = BankAccountViewType.None;
                        break;
                }
                // set grid properties
                SetGridProperties();
				LoadGrid();
				// set focus to grid so you don't see acct control
				VSDeposit.Select(0, 0);
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "Twpy0000.vb1");
				if (!rsMaster.EndOfFile())
				{
					fraWarning.Visible = FCConvert.ToString(rsMaster.Get_Fields_String("Check")) == "Regular";
				}
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					lblEmployeeNumber.Text = "";
				}
				else if (FCConvert.ToBoolean(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == true)
				{
					// this is a new record
					lblEmployeeNumber.Text = "";
				}
				else
				{
					lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				}
				// lblWarning.ForeColor = TRIOCOLORRED
				fraWarning.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					mnuAddBank.Enabled = false;
					mnuDelete.Enabled = false;
					mnuSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					mnuBanks.Enabled = false;
					VSDeposit.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				clsHistoryClass.SetGridIDColumn("PY", "vsDeposit", 4);
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				VSDeposit.Cols = 9;
				VSDeposit.Rows = 1;
                if (bankAccountViewPermission != BankAccountViewType.Full)
                {
                    VSDeposit.Columns[1].ReadOnly = true;
                }

                VSDeposit.ExtendLastCol = true;
				modGlobalRoutines.CenterGridCaptions(ref VSDeposit);
				// set column bank properties
				VSDeposit.ColWidth(Bank, 370);
				VSDeposit.ColAlignment(Bank, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.TextMatrix(0, Bank, "Bank");
				// set column account properties
				VSDeposit.ColWidth(Account, 1300);
				VSDeposit.ColAlignment(Account, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.TextMatrix(0, Account, "Account");
				// set column amount properties
				VSDeposit.ColWidth(Amount, 1150);
				VSDeposit.ColAlignment(Amount, FCGrid.AlignmentSettings.flexAlignRightCenter);
				VSDeposit.TextMatrix(0, Amount, "Amount");
				VSDeposit.ColFormat(Amount, "#0.00");
                //FC:FINAL:BSE #2598 set allowed keys for client side check 
                VSDeposit.ColAllowedKeys(Amount, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
                // set column dtype properties
                VSDeposit.ColWidth(DTYPE, 1100);
				VSDeposit.ColAlignment(DTYPE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.TextMatrix(0, DTYPE, "Amt.Type");
				VSDeposit.ColDataType(CNSTGRIDCOLPRENOTE, FCGrid.DataTypeSettings.flexDTBoolean);
				VSDeposit.TextMatrix(0, CNSTGRIDCOLPRENOTE, "Prenote");
                VSDeposit.ColAlignment(CNSTGRIDCOLPRENOTE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.ColAlignment(AccountType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.TextMatrix(0, AccountType, "Acct.Type");
				// set column ID properties
				VSDeposit.ColHidden(ID, true);
				// set column dirty properties
				VSDeposit.ColHidden(DIRTY, true);
				string strTemp = "";
				VSDeposit.ColComboList(Bank, string.Empty);
				rsDirectDeposit.OpenRecordset("Select * from tblBanks order by Name", "TWPY0000.vb1");
				strTemp = "";
				if (!rsDirectDeposit.EndOfFile())
				{
					rsDirectDeposit.MoveLast();
					rsDirectDeposit.MoveFirst();
					string tempname = "";
					string tempaddress = "";
					string tempcity = "";
					// makes this column work like a combo box.
					// this fills the box with the values from the tblBanks table
					// This forces this column to work as a combo box
					// frmWait.Init "Loading Direct Deposit Information", True, rsDirectDeposit.RecordCount, True
					for (intCounter = 1; intCounter <= (rsDirectDeposit.RecordCount()); intCounter++)
					{
						//App.DoEvents();
						tempname = FCConvert.ToString(rsDirectDeposit.Get_Fields_String("Name"));
						tempaddress = FCConvert.ToString(rsDirectDeposit.Get_Fields_String("Address1"));
						// tempcity = rsDirectDeposit.Fields("City")
						// make data look nice in combo box
						// looks like there are columns in the combo box
						tempname = TestString(ref tempname, 25);
						// tempaddress = TestString(tempaddress, 20)
						tempaddress = TestString(ref tempaddress, 30);
						// tempcity = TestString(tempcity, 10)
						// If intCounter = 1 Then
						// strTemp = strTemp & "#" & rsDirectDeposit.Fields("ID") & ";" & tempname & vbTab & tempaddress & vbTab & tempcity & vbTab & IIf(Trim(rsDirectDeposit.Fields("RecordNumber")) <> "", Format(rsDirectDeposit.Fields("RecordNumber"), "00000"), "")
						// Else
						// strTemp = strTemp & "|#" & rsDirectDeposit.Fields("ID") & ";" & tempname & vbTab & tempaddress & vbTab & tempcity & vbTab & IIf(Trim(rsDirectDeposit.Fields("RecordNumber")) <> "", Format(rsDirectDeposit.Fields("RecordNumber"), "00000"), "")
						// End If
						if (intCounter == 1)
						{
							strTemp += "#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : "");
						}
						else
						{
							strTemp += "|#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : "");
						}
						rsDirectDeposit.MoveNext();
						// frmWait.IncrementProgress
					}
					VSDeposit.ColComboList(Bank, strTemp);
				}
				VSDeposit.ColComboList(DTYPE, "#1;Dollars|#2;Percent");
				VSDeposit.ColComboList(AccountType, "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTRECORDREGULAR) + ";Checking|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPESAVINGS) + ";Savings");
				frmWait.InstancePtr.Unload();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridHeight()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridHeight";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
				// With vsDeposit
				// If ((.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * .Rows) + 75) < (Me.Height * 0.5) Then
				// .Height = .Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * .Rows + 75
				// Else
				// .Height = (Me.Height * 0.5)
				// End If
				// End With
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void LoadCombo()
		{
			// if coming from banks then we need to refresh combo so there current
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadCombo";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				VSDeposit.ColComboList(Bank, string.Empty);
				rsDirectDeposit.OpenRecordset("Select * from tblBanks order by ID", "TWPY0000.vb1");
				if (!rsDirectDeposit.EndOfFile())
				{
					rsDirectDeposit.MoveLast();
					rsDirectDeposit.MoveFirst();
					string tempname = "";
					string tempaddress = "";
					string tempcity = "";
					// makes this column work like a combo box.
					// this fills the box with the values from the tblBanks table
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDirectDeposit.RecordCount()); intCounter++)
					{
						tempname = FCConvert.ToString(rsDirectDeposit.Get_Fields_String("Name"));
						tempaddress = FCConvert.ToString(rsDirectDeposit.Get_Fields_String("Address1"));
						// tempcity = rsDirectDeposit.Fields("City")
						// make data look nice in combo box
						// looks like there are columns in the combo box
						tempname = TestString(ref tempname, 25);
						tempaddress = TestString(ref tempaddress, 30);
						// tempcity = TestString(tempcity, 10)
						// If intCounter = 1 Then
						// .ColComboList(BANK, .ColComboList(BANK) & "#" & rsDirectDeposit.Fields("ID") & ";" & tempname & tempaddress & tempcity & IIf(Trim(rsDirectDeposit.Fields("RecordNumber")) <> "", Format(rsDirectDeposit.Fields("RecordNumber"), "00000"), "")
						// Else
						// .ColComboList(BANK, .ColComboList(BANK) & "|#" & rsDirectDeposit.Fields("ID") & ";" & tempname & tempaddress & tempcity & IIf(Trim(rsDirectDeposit.Fields("RecordNumber")) <> "", Format(rsDirectDeposit.Fields("RecordNumber"), "00000"), "")
						// End If
						//FC:FINAL:DSE:#i2222 ColComboList should not be created differently, or else the old grid value would not be shown
						//if (intCounter == 1)
						//{
						//	VSDeposit.ColComboList(Bank, VSDeposit.ColComboList(Bank) + "#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + tempaddress + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : ""));
						//}
						//else
						//{
						//	VSDeposit.ColComboList(Bank, VSDeposit.ColComboList(Bank) + "|#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + tempaddress + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : ""));
						//}
						if (intCounter == 1)
						{
							VSDeposit.ColComboList(Bank, VSDeposit.ColComboList(Bank) + "#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : ""));
						}
						else
						{
							VSDeposit.ColComboList(Bank, VSDeposit.ColComboList(Bank) + "|#" + rsDirectDeposit.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDirectDeposit.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsDirectDeposit.Get_Fields_Int32("RecordNumber"), "00000") : ""));
						}
						rsDirectDeposit.MoveNext();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private string TestString(ref string TempString, int spaces)
		{
			string TestString = "";
			// vbPorter upgrade warning: AddSpace As int	OnWriteFCConvert.ToInt32(
			int AddSpace = 0;
			// spaces then need to be added so all are equal
			int intCount;
			if (TempString.Length < spaces)
			{
				AddSpace = (spaces - TempString.Length);
			}
			for (intCount = 1; intCount <= AddSpace; intCount++)
			{
				TempString += " ";
			}
			TestString = TempString;
			return TestString;
		}

		private void mnuAddBank_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuAddBank_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				VSDeposit.AddItem("", VSDeposit.Rows);
				VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpAlignment, VSDeposit.Rows - 1, Bank, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, Bank, "");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, Account, "");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, Amount, "0");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, DTYPE, "Dollars");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, ID, "0");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, DIRTY, ".");
				VSDeposit.TextMatrix(VSDeposit.Rows - 1, CNSTGRIDCOLPRENOTE, FCConvert.ToString(false));
				VSDeposit.Select(VSDeposit.Rows - 1, Bank);
				intDataChanged += 1;
				SetGridHeight();
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Direct Deposit");
				// ------------------------------------------------------------------
				VSDeposit.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuAddBank_Click()
		{
			mnuAddBank_Click(mnuAddBank, new System.EventArgs());
		}

		private void mnuBanks_Click(object sender, System.EventArgs e)
		{
			frmBanks.InstancePtr.Show();
			boolFromDirectDeposit = true;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (VSDeposit.Row <= 0)
				{
					MessageBox.Show("Direct Deposit record must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (VSDeposit.Col < 0)
					return;
				if (MessageBox.Show("This action will delete account " + VSDeposit.TextMatrix(VSDeposit.Row, Account) + "\r" + VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, VSDeposit.Row, VSDeposit.Col) + ". Continue?", "Payroll Direct Deposit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsDirectDeposit.OpenRecordset("Select * From tblDirectDeposit where ID = " + VSDeposit.TextMatrix(VSDeposit.Row, ID), "TWPY0000.vb1");
					if (!rsDirectDeposit.EndOfFile())
					{
						rsDirectDeposit.Execute("Delete from tblDirectDeposit where ID = " + VSDeposit.TextMatrix(VSDeposit.Row, ID), "Payroll");
					}
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref VSDeposit);
					// Dave 12/14/2006---------------------------------------------------
					// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
					modAuditReporting.RemoveGridRow_8("vsDeposit", intTotalNumberOfControls - 1, VSDeposit.Row, clsControlInfo);
					// We then add a change record saying the row was deleted
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(VSDeposit.Row) + " from Direct Deposit");
					// -------------------------------------------------------------------
					VSDeposit.RemoveItem(VSDeposit.Row);
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Direct Deposit", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				SetGridHeight();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool SaveData()
		{
			bool SaveData = false;
			int x;
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				VSDeposit.Select(0, 0);
				if (NotValidData() == true)
					return SaveData;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Direct Deposit", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				rsSave.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "twpy0000.vb1");
				rsSave.Edit();
				rsSave.Set_Fields("minimumcheck", FCConvert.ToString(Conversion.Val(txtMinCheck.Text)));
				rsSave.Update();
				rsDirectDeposit.OpenRecordset("Select * From tblDirectDeposit where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "TWPY0000.vb1");
				for (intCounter = 1; intCounter <= (VSDeposit.Rows - 1); intCounter++)
				{
					// only save data that has been changed or is new, more efficient
					// If Right(vsDeposit.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, DIRTY), 1) = "." Then
					rsDirectDeposit.FindFirstRecord("ID", VSDeposit.TextMatrix(intCounter, ID));
					if (rsDirectDeposit.NoMatch)
					{
						// if user added a new record to grid then add a record to database also
						rsDirectDeposit.AddNew();
						rsDirectDeposit.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
					}
					else
					{
						// just saving data that was there on load
						rsDirectDeposit.Edit();
					}
					rsDirectDeposit.Set_Fields("RowNumber", intCounter);
					//FC:FINAL:DDU:#i2047 - fixed save of bank name from grid
					//rsDirectDeposit.SetData("Bank", VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Bank));
					string[] aryData = Strings.Split(VSDeposit.ColComboList(Bank), "|", -1, CompareConstants.vbBinaryCompare);
					for (int intCount = 0; intCount <= Information.UBound(aryData, 1); intCount++)
					{
						string[] aryData2 = Strings.Split(FCConvert.ToString(aryData[intCount]), ";", -1, CompareConstants.vbBinaryCompare);
						//FC:FINAL:DDU:#i2388 - check for numeric comboitem when newly added and after that update everyone else with the existing name
						if (Information.IsNumeric(VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Bank)))
						{
                            if (aryData2[0].Replace("#", "") == VSDeposit.TextMatrix(intCounter, Bank).Replace("#", ""))
                            {
                                int temp = FCConvert.ToInt32(Conversion.Val(aryData2[0].Trim('#')));
                                rsDirectDeposit.SetData("Bank", temp);
                                break;
							}
                        }
						//else if (aryData2[1].Contains(FCConvert.ToString(VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, Bank))))
                        else if (aryData2[1] == VSDeposit.TextMatrix( intCounter, Bank))
                        {
							int temp = FCConvert.ToInt32(Conversion.Val(aryData2[0].Trim('#')));
							rsDirectDeposit.SetData("Bank", temp);
							break;
						}
					}

                    if (bankAccountViewPermission == BankAccountViewType.Full)
                    {
                        rsDirectDeposit.SetData("Account", VSDeposit.TextMatrix(intCounter, Account));
                    }

                    rsDirectDeposit.SetData("Amount", VSDeposit.TextMatrix(intCounter, Amount));
					rsDirectDeposit.SetData("Type", VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, DTYPE));
					rsDirectDeposit.Set_Fields("prenote", FCConvert.CBool(VSDeposit.TextMatrix(intCounter, CNSTGRIDCOLPRENOTE)));
					// rsDirectDeposit.SetData "AccountType", vsDeposit.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, AccountType)
					if (fecherFoundation.Strings.Trim(VSDeposit.TextMatrix(intCounter, AccountType)) != string.Empty)
					{
						rsDirectDeposit.Set_Fields("AccountType", VSDeposit.TextMatrix(intCounter, AccountType) + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTRECORDREGULAR));
					}
					rsDirectDeposit.Update();
					// End If
					// the row that was dirty is nolonger dirty
					VSDeposit.TextMatrix(intCounter, DIRTY, "");
				}
				intDataChanged = 0;
				//clsHistoryClass.Compare();
				SaveData = true;
				MessageBox.Show("Save was successful", "Payroll Direct Deposit", MessageBoxButtons.OK, MessageBoxIcon.Information);
				LoadGrid();
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			VSDeposit.Select(0, 0);
			dblAcctTotalPercent = 0;
			// go through the grid and make sure all data is valid
			for (intCounter = (VSDeposit.Rows - 1); intCounter >= 1; intCounter--)
			{
				if (VSDeposit.TextMatrix(intCounter, Bank) == string.Empty)
				{
					MessageBox.Show("Select Item From List", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VSDeposit.Select(intCounter, Bank);
					NotValidData = true;
					return NotValidData;
				}
				if (VSDeposit.TextMatrix(intCounter, Account) == string.Empty && bankAccountViewPermission == BankAccountViewType.Full)
				{
					if (MessageBox.Show("No Account Was Entered, Continue Save?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
					{
						VSDeposit.Select(intCounter, Account);
						NotValidData = true;
						return NotValidData;
					}
				}
				if (VSDeposit.TextMatrix(intCounter, Amount) == string.Empty)
				{
					MessageBox.Show("Enter Amount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VSDeposit.Select(intCounter, Amount);
					NotValidData = true;
					return NotValidData;
				}
				if (VSDeposit.TextMatrix(intCounter, DTYPE) == string.Empty)
				{
					MessageBox.Show("Select Item From List", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VSDeposit.Select(intCounter, DTYPE);
					NotValidData = true;
					return NotValidData;
				}
				if (intCounter == DTYPE || intCounter == Amount)
				{
					if (FCConvert.ToString(VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, DTYPE)) == "Percent" && Conversion.Val(VSDeposit.TextMatrix(intCounter, Amount)) > 100)
					{
						MessageBox.Show("Amount Cannot Be Greater Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VSDeposit.Select(intCounter, DTYPE);
						NotValidData = true;
						return NotValidData;
					}
				}
				if (VSDeposit.TextMatrix(intCounter, DTYPE) == "2")
				{
					dblAcctTotalPercent += FCConvert.ToDouble(VSDeposit.TextMatrix(intCounter, Amount));
					if (dblAcctTotalPercent > 100)
					{
						MessageBox.Show("Amount Totals Cannot Be Greater Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VSDeposit.Select(intCounter, DTYPE);
						NotValidData = true;
						return NotValidData;
					}
				}
			}
			NotValidData = false;
			return NotValidData;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void txtAcct1_KeyPress(Keys KeyAscii)
		{
			if (KeyAscii == Keys.Tab)
			{
				VSDeposit.Select(VSDeposit.Row, Amount);
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				// Call SaveChanges
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public bool SaveChanges()
		{
			bool SaveChanges = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges = false;
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						SaveChanges = SaveData();
					}
					else
					{
						SaveChanges = true;
					}
				}
				else
				{
					SaveChanges = true;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return SaveChanges;
			}
			return SaveChanges;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!SaveChanges())
			{
				e.Cancel = true;
				return;
			}
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void txtMinCheck_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			intDataChanged = 1;
		}

		private void VSDeposit_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// a change was made so mark as dirty
			VSDeposit.TextMatrix(VSDeposit.Row, DIRTY, "..");
			intDataChanged += 1;
		}

		private void VSDeposit_AfterMoveRow(object sender, EventArgs e)
		{
			intDataChanged = 1;
		}

		private void VSDeposit_ClickEvent(object sender, DataGridViewCellEventArgs e)
		{
            if (e.RowIndex > -1)
            {
                int lngRow;
                int lngCol;
                lngRow = VSDeposit.MouseRow;
                lngCol = VSDeposit.MouseCol;
                if (lngRow == 0)
                {
                    if (lngCol == CNSTGRIDCOLPRENOTE)
                    {
                        for (lngRow = 1; lngRow <= VSDeposit.Rows - 1; lngRow++)
                        {
                            VSDeposit.TextMatrix(lngRow, CNSTGRIDCOLPRENOTE, FCConvert.ToString(boolAllTrue));
                        }
                        // lngRow
                        boolAllTrue = !boolAllTrue;
                    }
                }
            }
		}

		private void VSDeposit_ComboDropDown(object sender, System.EventArgs e)
		{
			if (VSDeposit.Col == Bank)
			{
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				int lngID = 0;
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(VSDeposit.TextMatrix(VSDeposit.Row, Bank))));
				// lngID = Val(gridPropertyCode.TextMatrix(0, 0))
				if (lngID > 0)
				{
					for (x = 0; x <= (VSDeposit.ComboCount - 1); x++)
					{
						if (Conversion.Val(VSDeposit.ComboData(x)) == lngID)
						{
							VSDeposit.ComboIndex = x;
						}
					}
					// x
				}
			}
		}

		private void VSDeposit_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (VSDeposit.Col == AccountType && VSDeposit.Row == VSDeposit.Rows - 1)
			{
				if ((KeyCode == (Keys)9 || KeyCode == (Keys)13) && !boolCantEdit)
				{
					// tab or enter
					mnuAddBank_Click();
					KeyCode = 0;
				}
			}
		}

		private void VSDeposit_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (VSDeposit.Col == AccountType)
			{
				if ((KeyCode == (Keys)9 || KeyCode == (Keys)13) && !boolCantEdit)
				{
					// tab or enter
					mnuAddBank_Click();
					KeyCode = 0;
				}
			}
		}

		private void VSDeposit_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (VSDeposit.Col == Amount)
			{
				if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 46 && KeyAscii != 8)
				{
					KeyAscii = 0;
				}
			}
			intDataChanged += 1;
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select minimumcheck from tblemployeemaster where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "'", "Twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					txtMinCheck.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("minimumcheck")), "0.00");
				}
				else
				{
					txtMinCheck.Text = "0.00";
				}
				rsDirectDeposit.OpenRecordset("Select * From tblDirectDeposit where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RowNumber", "TWPY0000.vb1");
				if (!rsDirectDeposit.EndOfFile())
				{
					rsDirectDeposit.MoveLast();
					rsDirectDeposit.MoveFirst();
					VSDeposit.Rows = 1;
					for (intCounter = 1; intCounter <= (rsDirectDeposit.RecordCount()); intCounter++)
					{
						VSDeposit.AddItem("", VSDeposit.Rows);
						VSDeposit.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, Bank, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						////FC:FINAL:DDU:#i2047 - fixed show of bank name on grid
						//VSDeposit.TextMatrix(intCounter, Bank, VSDeposit.ColComboList(rsDirectDeposit.Get_Fields_Int32("Bank")));
						string[] aryData = Strings.Split(VSDeposit.ColComboList(Bank), "|", -1, CompareConstants.vbBinaryCompare);
						for (int intCount = 0; intCount <= Information.UBound(aryData, 1); intCount++)
						{
							string[] aryData2 = Strings.Split(FCConvert.ToString(aryData[intCount]), ";", -1, CompareConstants.vbBinaryCompare);
							if (aryData2[0] == FCConvert.ToString("#" + rsDirectDeposit.Get_Fields_Int32("Bank")))
							{
								VSDeposit.TextMatrix(intCounter, Bank, aryData2[1]);
								break;
							}
						}

                        var bankAccount = "";
                        switch (bankAccountViewPermission)
                        {
                            case BankAccountViewType.Full:
                                bankAccount = rsDirectDeposit.Get_Fields_String("Account");
                                break;
                            case BankAccountViewType.Masked:
                                bankAccount = "******" + rsDirectDeposit.Get_Fields_String("Account").Right(4);
                                break;
                            default:
                                bankAccount = "**********";
                                break;
                        }

                        if (string.IsNullOrWhiteSpace(rsDirectDeposit.Get_Fields_String("Account")))
                        {
                            bankAccount = "";
                        }
						VSDeposit.TextMatrix(intCounter, Account, bankAccount);
						VSDeposit.TextMatrix(intCounter, Amount, FCConvert.ToString(rsDirectDeposit.Get_Fields("Amount")));
						VSDeposit.TextMatrix(intCounter, DTYPE, FCConvert.ToString(rsDirectDeposit.Get_Fields("Type")));
						VSDeposit.TextMatrix(intCounter, ID, FCConvert.ToString(rsDirectDeposit.Get_Fields("ID")));
						VSDeposit.TextMatrix(intCounter, CNSTGRIDCOLPRENOTE, FCConvert.ToString(rsDirectDeposit.Get_Fields_Boolean("Prenote")));
						if (Strings.Left(rsDirectDeposit.Get_Fields_String("AccountType") + "  ", 1) == FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPECHECKING)
							|| Strings.Left(rsDirectDeposit.Get_Fields_String("AccountType") + "  ", 1) == FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPESAVINGS))
						{
							VSDeposit.TextMatrix(intCounter, AccountType, Conversion.Val(Strings.Left(FCConvert.ToString(rsDirectDeposit.Get_Fields_String("AccountType")), 1)));
						}
						else
						{
						}
						rsDirectDeposit.MoveNext();
					}
				}
				else
				{
					VSDeposit.Rows = 1;
				}
				SetGridHeight();
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void VSDeposit_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			dblAcctTotalPercent = 0;
			if (VSDeposit.Col == Amount)
			{
				for (intCounter = 1; intCounter <= (VSDeposit.Rows - 1); intCounter++)
				{
					if (VSDeposit.TextMatrix(intCounter, DTYPE) == "Percent" || VSDeposit.TextMatrix(intCounter, DTYPE) == "2")
					{
						if (intCounter == VSDeposit.Row)
						{
							dblAcctTotalPercent += Conversion.Val(VSDeposit.EditText);
						}
						else
						{
							dblAcctTotalPercent += FCConvert.ToDouble(VSDeposit.TextMatrix(intCounter, Amount));
						}
					}
				}
			}
			if (VSDeposit.Col == DTYPE)
			{
				for (intCounter = 1; intCounter <= (VSDeposit.Rows - 1); intCounter++)
				{
					if (intCounter == VSDeposit.Row)
					{
						if (VSDeposit.EditText == "Percent" || VSDeposit.EditText == "2")
						{
							dblAcctTotalPercent += FCConvert.ToDouble(VSDeposit.TextMatrix(intCounter, Amount));
						}
					}
					else
					{
						if (VSDeposit.TextMatrix(intCounter, DTYPE) == "Percent" || VSDeposit.TextMatrix(intCounter, DTYPE) == "2")
						{
							dblAcctTotalPercent += FCConvert.ToDouble(VSDeposit.TextMatrix(intCounter, Amount));
						}
					}
				}
			}
			if (dblAcctTotalPercent > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (VSDeposit.Rows - 1); intRows++)
			{
				for (intCols = 0; intCols <= (VSDeposit.Cols - 1); intCols++)
				{
					if (intCols != 5 && intCols != 2) // skip id and account
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsDeposit";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + VSDeposit.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMinCheck";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Minimum Check Amount";
			intTotalNumberOfControls += 1;
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }
    }
}
