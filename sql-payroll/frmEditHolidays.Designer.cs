﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditHolidays.
	/// </summary>
	partial class frmEditHolidays
	{
		public fecherFoundation.FCComboBox cmbShiftType;
		public fecherFoundation.FCTextBox txtThisYear;
		public fecherFoundation.FCComboBox cmbWeekDay;
		public fecherFoundation.FCComboBox cmbDay;
		public fecherFoundation.FCComboBox cmbMonth;
		public fecherFoundation.FCComboBox cmbHolidayType;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblLast;
		public fecherFoundation.FCLabel lblOf;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblDay;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbShiftType = new fecherFoundation.FCComboBox();
            this.txtThisYear = new fecherFoundation.FCTextBox();
            this.cmbWeekDay = new fecherFoundation.FCComboBox();
            this.cmbDay = new fecherFoundation.FCComboBox();
            this.cmbMonth = new fecherFoundation.FCComboBox();
            this.cmbHolidayType = new fecherFoundation.FCComboBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblLast = new fecherFoundation.FCLabel();
            this.lblOf = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblDay = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 467);
            this.BottomPanel.Size = new System.Drawing.Size(539, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbShiftType);
            this.ClientArea.Controls.Add(this.txtThisYear);
            this.ClientArea.Controls.Add(this.cmbWeekDay);
            this.ClientArea.Controls.Add(this.cmbDay);
            this.ClientArea.Controls.Add(this.cmbMonth);
            this.ClientArea.Controls.Add(this.cmbHolidayType);
            this.ClientArea.Controls.Add(this.txtName);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblLast);
            this.ClientArea.Controls.Add(this.lblOf);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblDay);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(539, 407);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(539, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbShiftType
            // 
            this.cmbShiftType.AutoSize = false;
            this.cmbShiftType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbShiftType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbShiftType.FormattingEnabled = true;
            this.cmbShiftType.Location = new System.Drawing.Point(163, 359);
            this.cmbShiftType.Name = "cmbShiftType";
            this.cmbShiftType.Size = new System.Drawing.Size(348, 40);
            this.cmbShiftType.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.cmbShiftType, "This shift will replace any Regular shifts worked on this holiday");
            // 
            // txtThisYear
            // 
            this.txtThisYear.AutoSize = false;
            this.txtThisYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtThisYear.Enabled = false;
            this.txtThisYear.LinkItem = null;
            this.txtThisYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtThisYear.LinkTopic = null;
            this.txtThisYear.Location = new System.Drawing.Point(163, 299);
            this.txtThisYear.Name = "txtThisYear";
            this.txtThisYear.Size = new System.Drawing.Size(348, 40);
            this.txtThisYear.TabIndex = 5;
            this.txtThisYear.Text = "Text1";
            this.ToolTip1.SetToolTip(this.txtThisYear, null);
            // 
            // cmbWeekDay
            // 
            this.cmbWeekDay.AutoSize = false;
            this.cmbWeekDay.BackColor = System.Drawing.SystemColors.Window;
            this.cmbWeekDay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbWeekDay.FormattingEnabled = true;
            this.cmbWeekDay.Location = new System.Drawing.Point(316, 150);
            this.cmbWeekDay.Name = "cmbWeekDay";
            this.cmbWeekDay.Size = new System.Drawing.Size(195, 40);
            this.cmbWeekDay.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbWeekDay, null);
            this.cmbWeekDay.Visible = false;
            this.cmbWeekDay.SelectedIndexChanged += new System.EventHandler(this.cmbWeekDay_SelectedIndexChanged);
            // 
            // cmbDay
            // 
            this.cmbDay.AutoSize = false;
            this.cmbDay.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDay.FormattingEnabled = true;
            this.cmbDay.Location = new System.Drawing.Point(163, 150);
            this.cmbDay.Name = "cmbDay";
            this.cmbDay.Size = new System.Drawing.Size(137, 40);
            this.cmbDay.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbDay, null);
            this.cmbDay.SelectedIndexChanged += new System.EventHandler(this.cmbDay_SelectedIndexChanged);
            // 
            // cmbMonth
            // 
            this.cmbMonth.AutoSize = false;
            this.cmbMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Location = new System.Drawing.Point(163, 239);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(137, 40);
            this.cmbMonth.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbMonth, null);
            this.cmbMonth.SelectedIndexChanged += new System.EventHandler(this.cmbMonth_SelectedIndexChanged);
            // 
            // cmbHolidayType
            // 
            this.cmbHolidayType.AutoSize = false;
            this.cmbHolidayType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHolidayType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbHolidayType.FormattingEnabled = true;
            this.cmbHolidayType.Location = new System.Drawing.Point(163, 90);
            this.cmbHolidayType.Name = "cmbHolidayType";
            this.cmbHolidayType.Size = new System.Drawing.Size(348, 40);
            this.cmbHolidayType.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbHolidayType, null);
            this.cmbHolidayType.SelectedIndexChanged += new System.EventHandler(this.cmbHolidayType_SelectedIndexChanged);
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.LinkItem = null;
            this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtName.LinkTopic = null;
            this.txtName.Location = new System.Drawing.Point(163, 30);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(348, 40);
            this.txtName.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtName, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 373);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(66, 15);
            this.Label4.TabIndex = 13;
            this.Label4.Text = "SHIFT TYPE";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // lblLast
            // 
            this.lblLast.Location = new System.Drawing.Point(187, 161);
            this.lblLast.Name = "lblLast";
            this.lblLast.Size = new System.Drawing.Size(40, 16);
            this.lblLast.TabIndex = 11;
            this.lblLast.Text = "LAST";
            this.ToolTip1.SetToolTip(this.lblLast, null);
            this.lblLast.Visible = false;
            // 
            // lblOf
            // 
            this.lblOf.Location = new System.Drawing.Point(163, 210);
            this.lblOf.Name = "lblOf";
            this.lblOf.Size = new System.Drawing.Size(137, 15);
            this.lblOf.TabIndex = 10;
            this.lblOf.Text = "OF";
            this.lblOf.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblOf, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 313);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 15);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "THIS YEAR";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // lblDay
            // 
            this.lblDay.Location = new System.Drawing.Point(326, 160);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(33, 20);
            this.lblDay.TabIndex = 8;
            this.lblDay.Text = "DAY";
            this.ToolTip1.SetToolTip(this.lblDay, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(54, 15);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "TYPE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(54, 15);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(172, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(174, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.ToolTip1.SetToolTip(this.cmdSaveContinue, null);
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // frmEditHolidays
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(539, 575);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditHolidays";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmEditHolidays_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditHolidays_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSaveContinue;
    }
}
