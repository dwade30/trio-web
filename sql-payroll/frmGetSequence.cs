//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public partial class frmGetSequence : BaseForm
	{
		public frmGetSequence()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.lblTo = new System.Collections.Generic.List<FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_3, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGetSequence InstancePtr
		{
			get
			{
				return (frmGetSequence)Sys.GetInstance(typeof(frmGetSequence));
			}
		}

		protected frmGetSequence _InstancePtr = null;
		//=========================================================
		string strSequence;

		private void FillReportNumbers()
		{
			cboReportNumber.Clear();
			cboReportNumber.AddItem("All");
			cboReportNumber.AddItem("None");
			int x;
			for (x = 1; x <= 10; x++)
			{
				cboReportNumber.AddItem(x.ToString());
			}
			// x
        }

		private void cboSequenceRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch (cboSequenceRange.SelectedIndex)
			{
				case 0:
					{
						// all
						fraSequenceRangeSelection.Visible = false;
						fraSequenceSingleSelection.Visible = false;
						// chkSame.Visible = False
						break;
					}
				case 1:
					{
						// single
						fraSequenceSingleSelection.Visible = true;
						fraSequenceRangeSelection.Visible = false;
						// chkSame.Visible = True
						break;
					}
				case 2:
					{
						// range
						fraSequenceRangeSelection.Visible = true;
						fraSequenceSingleSelection.Visible = false;
						// chkSame.Visible = True
						break;
					}
			}
			//end switch
		}

		private void frmGetSequence_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmGetSequence_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetSequence properties;
			//frmGetSequence.ScaleWidth	= 5880;
			//frmGetSequence.ScaleHeight	= 3675;
			//frmGetSequence.LinkTopic	= "Form1";
			//frmGetSequence.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			FillReportNumbers();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public string Init(bool boolBLS = false)
		{
			string Init = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			strSequence = "";
			clsLoad.OpenRecordset("SELECT DISTINCT SeqNumber FROM tblEmployeeMaster ORDER BY SeqNumber");
			if (!clsLoad.EndOfFile())
			{
                this.LoadForm();
                    
				while (!clsLoad.EndOfFile())
				{
					cboSingleSequence.AddItem(FCConvert.ToString(clsLoad.Get_Fields("seqnumber")));
					cboStartSequence.AddItem(FCConvert.ToString(clsLoad.Get_Fields("seqnumber")));
					cboEndSequence.AddItem(FCConvert.ToString(clsLoad.Get_Fields("seqnumber")));
					clsLoad.MoveNext();
				}
				cboSingleSequence.SelectedIndex = 0;
				cboEndSequence.SelectedIndex = cboEndSequence.Items.Count - 1;
				cboStartSequence.SelectedIndex = 0;
				cboSequenceRange.SelectedIndex = 0;
				cboReportNumber.SelectedIndex = 0;
				fraBLS.Visible = boolBLS;
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				// no sequences assigned. Just print all
				strSequence = "-1";
			}
			Init = strSequence;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			switch (cboSequenceRange.SelectedIndex)
			{
				case 0:
					{
						// all
						strSequence = "-1";
						break;
					}
				case 1:
					{
						// single
						strSequence = FCConvert.ToString(Conversion.Val(cboSingleSequence.Text));
						break;
					}
				case 2:
					{
						// range
						strSequence = FCConvert.ToString(Conversion.Val(cboStartSequence.Text)) + "," + FCConvert.ToString(Conversion.Val(cboEndSequence.Text));
						break;
					}
			}
			//end switch
			// gboolSameAsC1WithHolding = chkSame
			modGlobalRoutines.Statics.gstrReportID = cboReportNumber.Text;
			Close();
		}
	}
}
