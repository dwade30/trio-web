﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmMQYFlag : BaseForm
	{
		public frmMQYFlag()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMQYFlag InstancePtr
		{
			get
			{
				return (frmMQYFlag)Sys.GetInstance(typeof(frmMQYFlag));
			}
		}

		protected frmMQYFlag _InstancePtr = null;
		//=========================================================
		int intReturnValue;
		// when it unloads, the init function won't be able to tell what was picked
		bool boolDontChangeGlobalString;
		public string strNoneCaption = string.Empty;
		public string strMonthlyCaption = string.Empty;
		public string strQuarterlyCaption = string.Empty;
		public string strYearlyCaption = string.Empty;
		public string strWeeklyCaption = string.Empty;

		public int Init(string strNoneCaption/*= "Individual"*/, string strMonthlyCaption/*= "Monthly"*/, string strQuarterlyCaption/*= "Quarterly"*/, string strYearlyCaption/*= "Yearly"*/, string strWeeklyCaption/*= "Weekly"*/, bool boolDontSetGlobalString/*= false*/, ref DateTime dtReturndate/*= DateTime.FromOADate(0)*/, ref int lngReturnYear/*= 0*/, ref int intMonthQuarter/*= 0*/, ref int PayRunID/*= 0*/)
		{
			int Init = 0;
			// 4 is weekly/none 1 is monthly 2 is quarterly and 3 is yearly
			int lngYear = 0;
			int intQuarter = 0;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth = 0;
			DateTime dtDate;
			int intPayRun = 0;
			this.strNoneCaption = strNoneCaption;
			this.strMonthlyCaption = strMonthlyCaption;
			this.strQuarterlyCaption = strQuarterlyCaption;
			this.strYearlyCaption = strYearlyCaption;
			this.strWeeklyCaption = strWeeklyCaption;
			// ********************************************************************************************************
			// THIS WI
			// MATTHEW 4/27/2005 CALL ID 67565
			//FC:FINAL:DDU:#i2424 - fixed adding or removing items in cmbPer
			if (!cmbPer.Items.Contains(strWeeklyCaption) && cmbPer.Items.Contains("Weekly"))
			{
				cmbPer.Items.Remove("Weekly");
				if (cmbPer.Items.Count > 0)
				{
					cmbPer.Items.Insert(0, strWeeklyCaption);
				}
				else
				{
					cmbPer.Items.Add(strWeeklyCaption);
				}
			}
			if (!cmbPer.Items.Contains(strMonthlyCaption) && cmbPer.Items.Contains("Monthly"))
			{
				cmbPer.Items.Remove("Monthly");
				if (cmbPer.Items.Count > 1)
				{
					cmbPer.Items.Insert(1, strMonthlyCaption);
				}
				else
				{
					cmbPer.Items.Add(strMonthlyCaption);
				}
			}
			switch (modGlobalVariables.Statics.gdatCurrentPayDate.Month)
			{
				case 1:
				case 2:
				case 3:
					{
						if (!cmbPer.Items.Contains(strQuarterlyCaption) && cmbPer.Items.Contains("Quarterly"))
						{
							cmbPer.Items.Remove("Quarterly");
							if (cmbPer.Items.Count > 2)
							{
								cmbPer.Items.Insert(2, strQuarterlyCaption);
							}
							else
							{
								cmbPer.Items.Add(strQuarterlyCaption);
							}
						}
						break;
					}
				case 4:
				case 5:
				case 6:
					{
						if (!cmbPer.Items.Contains(strQuarterlyCaption) && cmbPer.Items.Contains("Quarterly"))
						{
							cmbPer.Items.Remove("Quarterly");
							if (cmbPer.Items.Count > 2)
							{
								cmbPer.Items.Insert(2, strQuarterlyCaption);
							}
							else
							{
								cmbPer.Items.Add(strQuarterlyCaption);
							}
						}
						break;
					}
				case 7:
				case 8:
				case 9:
					{
						if (!cmbPer.Items.Contains(strQuarterlyCaption) && cmbPer.Items.Contains("Quarterly"))
						{
							cmbPer.Items.Remove("Quarterly");
							if (cmbPer.Items.Count > 2)
							{
								cmbPer.Items.Insert(2, strQuarterlyCaption);
							}
							else
							{
								cmbPer.Items.Add(strQuarterlyCaption);
							}
						}
						break;
					}
				case 10:
				case 11:
				case 12:
					{
						if (!cmbPer.Items.Contains(strQuarterlyCaption) && cmbPer.Items.Contains("Quarterly"))
						{
							cmbPer.Items.Remove("Quarterly");
							if (cmbPer.Items.Count > 2)
							{
								cmbPer.Items.Insert(2, strQuarterlyCaption);
							}
							else
							{
								cmbPer.Items.Add(strQuarterlyCaption);
							}
						}
						break;
					}
			}
			//end switch
			if (!cmbPer.Items.Contains(strYearlyCaption) && cmbPer.Items.Contains("Yearly"))
			{
				cmbPer.Items.Remove("Yearly");
				if (cmbPer.Items.Count > 3)
				{
					cmbPer.Items.Insert(3, strYearlyCaption);
				}
				else
				{
					cmbPer.Items.Add(strYearlyCaption);
				}
			}
			if (!cmbPer.Items.Contains(strNoneCaption) && cmbPer.Items.Contains("Individual"))
			{
				cmbPer.Items.Remove("Individual");
                //FC:FINAL:DSE:#i2425 Exception when inserting on a non-existing position
                //cmbPer.Items.Insert(4, strNoneCaption);
                //FC:FINAL:AM:#4220 - don't add empty item
                if (!String.IsNullOrEmpty(strNoneCaption))
                {
                    if (cmbPer.Items.Count > 4)
                    {
                        cmbPer.Items.Insert(4, strNoneCaption);
                    }
                    else
                    {
                        cmbPer.Items.Add(strNoneCaption);
                    }
                }
			}
			// ********************************************************************************************************
			boolDontChangeGlobalString = boolDontSetGlobalString;
			if (strNoneCaption == string.Empty)
			{
				if (cmbPer.Items.IndexOf("Individual") != -1)
				{
					cmbPer.Items.Remove("Individual");
				}
			}
			if (strYearlyCaption == string.Empty)
			{
				if (cmbPer.Items.IndexOf("Yearly") != -1)
				{
					cmbPer.Items.Remove("Yearly");
				}
			}
			if (strQuarterlyCaption == string.Empty)
			{
				if (cmbPer.Items.IndexOf("Quarterly") != -1)
				{
					cmbPer.Items.Remove("Quarterly");
				}
			}
			Init = 1;
            // default to weekly
            cmbPer.SelectedIndex = 0;
            intReturnValue = -1;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = intReturnValue;
			// Matthew 2/24/2004
			modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = false;
			if (intReturnValue < 0)
			{
				return Init;
			}
			modGlobalVariables.Statics.gintMQFCType = intReturnValue;
			// If intReturnValue = 0 Then
			// THEN THESE ARE WEEKLY REPORTS SO WE NOW NEED TO SEE IF THEY WANT TO REPORT ON ALL
			// PAY RUN ID'S
			// Dim strreturn As String
			// strreturn = MsgBox("Do you wish to report on ALL Pay Runs for the current Pay Date?", vbQuestion + vbYesNo, "TRIO Software")
			// gboolPrintALLPayRuns = strreturn = 6
			// End If
			if (boolDontSetGlobalString)
			{
				// call form to get proper date/year/month etc.
				switch (intReturnValue)
				{
					case 0:
						{
							// weekly
							dtDate = DateTime.Today;
							if (dtReturndate.ToOADate() != 0)
							{
								intPayRun = 0;
								// bug call id 5056
								frmSelectDateInfo.InstancePtr.Init2("Date Selection", -1, -1, -1, ref dtDate, ref intPayRun, false);
								dtReturndate = dtDate;
								PayRunID = intPayRun;
							}
							break;
						}
					case 1:
						{
							// monthly
							if (lngReturnYear != 0)
							{
								lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
								intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
								// bug call id 5056
								// Call frmSelectDateInfo.Init("Select the Month and Year for the report", lngYear, , intMonth)
								//FC:FINAL:DSE:#i2421 Use date parameter by reference
								DateTime tmpArg = DateTime.FromOADate(0);
								frmSelectDateInfo.InstancePtr.Init5("Date Selection", ref lngYear, -1, ref intMonth, ref tmpArg, -1, false);
								lngReturnYear = lngYear;
								intMonthQuarter = intMonth;
								// dtReturndate = intMonth & "/1/" & lngYear
							}
							break;
						}
					case 2:
						{
							// quarterly
							lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
							if (intMonthQuarter != 0)
							{
								intQuarter = 1;
								// bug call id 5056
								// Call frmSelectDateInfo.Init("Select the Quarter and Year for the report", lngYear, intQuarter)
								//FC:FINAL:DSE:#i2421 Use date parameter by reference
								DateTime tmpArg = DateTime.FromOADate(0);
								frmSelectDateInfo.InstancePtr.Init3("Date Selection", ref lngYear, ref intQuarter, -1,ref tmpArg, -1, false);
								lngReturnYear = lngYear;
								intMonthQuarter = intQuarter;
								// dtReturndate = (intQuarter * 3) - 2 & "/1/" & lngYear
							}
							break;
						}
					case 3:
						{
							// yearly
							lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
							if (lngReturnYear != 0)
							{
								// bug call id 5056
								// Call frmSelectDateInfo.Init("Select the Year for the report", lngYear)
								//FC:FINAL:DSE:#i2421 Use date parameter by reference
								DateTime tmpArg = DateTime.FromOADate(0);
								frmSelectDateInfo.InstancePtr.Init4("Date Selection", ref lngYear, -1, -1, ref tmpArg, -1, false);
								lngReturnYear = lngYear;
								// dtReturndate = "1/1/" & lngYear
							}
							break;
						}
					case 4:
						{
							// fiscal year
							lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
							if (lngReturnYear != 0)
							{
								// bug call id 5056
								// Call frmSelectDateInfo.Init("Select the Fiscal Year for the report", lngYear)
								//FC:FINAL:DSE:#i2421 Use date parameter by reference
								DateTime tmpArg = DateTime.FromOADate(0);
								frmSelectDateInfo.InstancePtr.Init4("Date Selection", ref lngYear, -1, -1,ref tmpArg , -1, false);
								lngReturnYear = lngYear;
							}
							break;
						}
				}
				//end switch
			}
			return Init;
		}

		private void cmdProcess_Click()
		{
			string strTemp = "";
			if (cmbPer.Text == "Weekly" || (strWeeklyCaption != string.Empty && cmbPer.Text == strWeeklyCaption))
			{
				strTemp = "WEEKLY";
				intReturnValue = 0;
			}
			else if (cmbPer.Text == "Monthly" || (strMonthlyCaption != string.Empty && cmbPer.Text == strMonthlyCaption))
			{
				strTemp = "MONTHLY";
				intReturnValue = 1;
			}
			else if (cmbPer.Text == "Quarterly" || (strQuarterlyCaption != string.Empty && cmbPer.Text == strQuarterlyCaption))
			{
				strTemp = "QUARTERLY";
				intReturnValue = 2;
			}
			else if (cmbPer.Text == "Yearly" || (strYearlyCaption != string.Empty && cmbPer.Text == strYearlyCaption))
			{
				strTemp = "YEARLY";
				intReturnValue = 3;
			}
			else if (cmbPer.Text == "None" || (strNoneCaption != string.Empty && cmbPer.Text == strNoneCaption))
			{
				strTemp = "NONE";
				intReturnValue = 4;
			}
			if (!boolDontChangeGlobalString)
			{
				modGlobalVariables.Statics.gstrMQYProcessing = strTemp;
			}
			Close();
		}

		private void frmMQYFlag_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmMQYFlag_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMQYFlag_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMQYFlag properties;
			//frmMQYFlag.ScaleWidth	= 3885;
			//frmMQYFlag.ScaleHeight	= 2355;
			//frmMQYFlag.LinkTopic	= "Form1";
			//frmMQYFlag.LockControls	= -1  'True;
			//End Unmaped Properties
			// vsElasticLight2.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmMQYFlag_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			intReturnValue = -1;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
