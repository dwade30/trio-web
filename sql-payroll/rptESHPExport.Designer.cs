﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptESHPExport.
	/// </summary>
	partial class rptESHPExport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptESHPExport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtEmployeeNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFICA = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVacationBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSickBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblEMatch = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEMatchCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmatchYTD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepChkLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepSav = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositSavLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDepositAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeposit8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDDeds = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDDeds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape5,
            this.Shape4,
            this.Shape3,
            this.Shape2,
            this.Shape1,
            this.txtEmployeeNo,
            this.txtName,
            this.txtCheckNo,
            this.txtPay,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.txtCurrentGross,
            this.Label11,
            this.txtCurrentDeductions,
            this.txtCurrentNet,
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtCurrentFed,
            this.txtCurrentFica,
            this.txtCurrentState,
            this.txtPayDesc1,
            this.txtHours1,
            this.txtPayAmount1,
            this.txtDedDesc1,
            this.txtDedAmount1,
            this.txtDedYTD1,
            this.lblTotalPay,
            this.txtTotalHours,
            this.txtTotalAmount,
            this.txtYTDGross,
            this.txtYTDFed,
            this.txtYTDFICA,
            this.txtYTDState,
            this.txtYTDNet,
            this.Label12,
            this.Label13,
            this.Label14,
            this.txtVacationBalance,
            this.txtSickBalance,
            this.lblEMatch,
            this.txtEMatchCurrent,
            this.txtEmatchYTD,
            this.txtDirectDepositLabel,
            this.txtDirectDeposit,
            this.txtChkAmount,
            this.lblCheckAmount,
            this.lblPayRate,
            this.txtPayRate,
            this.Label29,
            this.txtOtherBalance,
            this.txtDate,
            this.txtDirectDepChkLabel,
            this.txtDirectDepSav,
            this.txtDirectDepositSavLabel,
            this.lblCheckMessage,
            this.lblCode1,
            this.lblCode2,
            this.lblCode1Balance,
            this.lblCode2Balance,
            this.lblCode3,
            this.lblCode3Balance,
            this.txtPayDesc2,
            this.txtHours2,
            this.txtPayAmount2,
            this.txtDedDesc2,
            this.txtDedAmount2,
            this.txtDedYTD2,
            this.txtPayDesc3,
            this.txtHours3,
            this.txtPayAmount3,
            this.txtDedDesc3,
            this.txtDedAmount3,
            this.txtDedYTD3,
            this.txtPayDesc4,
            this.txtHours4,
            this.txtPayAmount4,
            this.txtDedDesc4,
            this.txtDedAmount4,
            this.txtDedYTD4,
            this.txtPayDesc5,
            this.txtHours5,
            this.txtPayAmount5,
            this.txtDedDesc5,
            this.txtDedAmount5,
            this.txtDedYTD5,
            this.txtPayDesc6,
            this.txtHours6,
            this.txtPayAmount6,
            this.txtDedDesc6,
            this.txtDedAmount6,
            this.txtDedYTD6,
            this.txtPayDesc7,
            this.txtHours7,
            this.txtPayAmount7,
            this.txtDedDesc7,
            this.txtDedAmount7,
            this.txtDedYTD7,
            this.txtPayDesc8,
            this.txtHours8,
            this.txtPayAmount8,
            this.txtDedDesc8,
            this.txtDedAmount8,
            this.txtDedYTD8,
            this.txtPayDesc9,
            this.txtHours9,
            this.txtPayAmount9,
            this.txtDedDesc9,
            this.txtDedAmount9,
            this.txtDedYTD9,
            this.txtPayDesc10,
            this.txtHours10,
            this.txtPayAmount10,
            this.txtDedDesc10,
            this.txtDedAmount10,
            this.txtDedYTD10,
            this.txtPayDesc11,
            this.txtHours11,
            this.txtPayAmount11,
            this.txtDedDesc11,
            this.txtDedAmount11,
            this.txtDedYTD11,
            this.txtPayDesc12,
            this.txtHours12,
            this.txtPayAmount12,
            this.txtDedDesc12,
            this.txtDedAmount12,
            this.txtDedYTD12,
            this.txtPayDesc13,
            this.txtHours13,
            this.txtPayAmount13,
            this.txtDedDesc13,
            this.txtDedAmount13,
            this.txtDedYTD13,
            this.txtPayDesc14,
            this.txtHours14,
            this.txtPayAmount14,
            this.txtDedDesc14,
            this.txtDedAmount14,
            this.txtDedYTD14,
            this.txtPayDesc15,
            this.txtHours15,
            this.txtPayAmount15,
            this.txtDedDesc15,
            this.txtDedAmount15,
            this.txtDedYTD15,
            this.txtPayDesc16,
            this.txtHours16,
            this.txtPayAmount16,
            this.txtDedDesc16,
            this.txtDedAmount16,
            this.txtDedYTD16,
            this.txtPayDesc17,
            this.txtHours17,
            this.txtPayAmount17,
            this.txtDedDesc17,
            this.txtDedAmount17,
            this.txtDedYTD17,
            this.txtPayDesc18,
            this.txtHours18,
            this.txtPayAmount18,
            this.txtDedDesc18,
            this.txtDedAmount18,
            this.txtDedYTD18,
            this.txtPayDesc19,
            this.txtHours19,
            this.txtPayAmount19,
            this.txtDedDesc19,
            this.txtDedAmount19,
            this.txtDedYTD19,
            this.txtPayDesc20,
            this.txtHours20,
            this.txtPayAmount20,
            this.txtDedDesc20,
            this.txtDedAmount20,
            this.txtDedYTD20,
            this.txtPayDesc21,
            this.txtHours21,
            this.txtPayAmount21,
            this.txtDedDesc21,
            this.txtDedAmount21,
            this.txtDedYTD21,
            this.txtPayDesc22,
            this.txtHours22,
            this.txtPayAmount22,
            this.txtDedDesc22,
            this.txtDedAmount22,
            this.txtDedYTD22,
            this.txtPayDesc23,
            this.txtHours23,
            this.txtPayAmount23,
            this.txtDedDesc23,
            this.txtDedAmount23,
            this.txtDedYTD23,
            this.Label30,
            this.Label31,
            this.lblDepositAmount1,
            this.lblDepositAmount3,
            this.lblDeposit3,
            this.lblDeposit1,
            this.lblDepositAmount2,
            this.lblDeposit2,
            this.lblDepositAmount4,
            this.lblDepositAmount6,
            this.lblDeposit6,
            this.lblDeposit4,
            this.lblDepositAmount5,
            this.lblDeposit5,
            this.lblDepositAmount7,
            this.lblDepositAmount9,
            this.lblDeposit9,
            this.lblDeposit7,
            this.lblDepositAmount8,
            this.lblDeposit8,
            this.txtYTDDeds,
            this.lblDeptDiv,
            this.txtDeptDiv});
			this.Detail.Height = 5.90625F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Shape5
			// 
			this.Shape5.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this.Shape5.Height = 1.177083F;
			this.Shape5.Left = 5.625F;
			this.Shape5.LineColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 2.177083F;
			this.Shape5.Width = 2.791667F;
			// 
			// Shape4
			// 
			this.Shape4.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this.Shape4.Height = 1.177083F;
			this.Shape4.Left = 5.614583F;
			this.Shape4.LineColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 0.6770833F;
			this.Shape4.Width = 2.8125F;
			// 
			// Shape3
			// 
			this.Shape3.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this.Shape3.Height = 4.072917F;
			this.Shape3.Left = 4.083333F;
			this.Shape3.LineColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.625F;
			this.Shape3.Width = 0.7916667F;
			// 
			// Shape2
			// 
			this.Shape2.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this.Shape2.Height = 4.072917F;
			this.Shape2.Left = 1.84375F;
			this.Shape2.LineColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.625F;
			this.Shape2.Width = 0.75F;
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this.Shape1.Height = 4.072917F;
			this.Shape1.Left = 0F;
			this.Shape1.LineColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.6041667F;
			this.Shape1.Width = 1.0625F;
			// 
			// txtEmployeeNo
			// 
			this.txtEmployeeNo.Height = 0.1666667F;
			this.txtEmployeeNo.HyperLink = null;
			this.txtEmployeeNo.Left = 0.0625F;
			this.txtEmployeeNo.Name = "txtEmployeeNo";
			this.txtEmployeeNo.Style = "text-align: right";
			this.txtEmployeeNo.Tag = "text";
			this.txtEmployeeNo.Text = "EMPLOYEE";
			this.txtEmployeeNo.Top = 0.1666667F;
			this.txtEmployeeNo.Width = 1.5F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 1.625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 3.9375F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.1666667F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 7.0625F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = "CHECK";
			this.txtCheckNo.Top = 0.1666667F;
			this.txtCheckNo.Width = 1.375F;
			// 
			// txtPay
			// 
			this.txtPay.Height = 0.1666667F;
			this.txtPay.HyperLink = null;
			this.txtPay.Left = 0.0625F;
			this.txtPay.Name = "txtPay";
			this.txtPay.Style = "text-align: left";
			this.txtPay.Tag = "text";
			this.txtPay.Text = "PAY";
			this.txtPay.Top = 0.6875F;
			this.txtPay.Width = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "text-align: right";
			this.Label1.Tag = "text";
			this.Label1.Text = "HOURS";
			this.Label1.Top = 0.6875F;
			this.Label1.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: right";
			this.Label2.Tag = "text";
			this.Label2.Text = "AMOUNT";
			this.Label2.Top = 0.6875F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: left";
			this.Label3.Tag = "text";
			this.Label3.Text = "DEDUCTIONS";
			this.Label3.Top = 0.6875F;
			this.Label3.Width = 1.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Tag = "text";
			this.Label4.Text = "CURRENT";
			this.Label4.Top = 0.6875F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: right";
			this.Label5.Tag = "text";
			this.Label5.Text = "YTD";
			this.Label5.Top = 0.6875F;
			this.Label5.Width = 0.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.8125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Tag = "text";
			this.Label6.Text = "CURRENT";
			this.Label6.Top = 0.6875F;
			this.Label6.Width = 0.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: left";
			this.Label7.Tag = "text";
			this.Label7.Text = "GROSS";
			this.Label7.Top = 0.8541667F;
			this.Label7.Width = 0.75F;
			// 
			// txtCurrentGross
			// 
			this.txtCurrentGross.Height = 0.1666667F;
			this.txtCurrentGross.HyperLink = null;
			this.txtCurrentGross.Left = 6.8125F;
			this.txtCurrentGross.Name = "txtCurrentGross";
			this.txtCurrentGross.Style = "text-align: right";
			this.txtCurrentGross.Tag = "text";
			this.txtCurrentGross.Text = "0.00";
			this.txtCurrentGross.Top = 0.8541667F;
			this.txtCurrentGross.Width = 0.75F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.75F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "text-align: left";
			this.Label11.Tag = "text";
			this.Label11.Text = "NET";
			this.Label11.Top = 1.854167F;
			this.Label11.Width = 0.5625F;
			// 
			// txtCurrentDeductions
			// 
			this.txtCurrentDeductions.Height = 0.1666667F;
			this.txtCurrentDeductions.HyperLink = null;
			this.txtCurrentDeductions.Left = 6.8125F;
			this.txtCurrentDeductions.Name = "txtCurrentDeductions";
			this.txtCurrentDeductions.Style = "text-align: right";
			this.txtCurrentDeductions.Tag = "text";
			this.txtCurrentDeductions.Text = "0.00";
			this.txtCurrentDeductions.Top = 1.520833F;
			this.txtCurrentDeductions.Width = 0.75F;
			// 
			// txtCurrentNet
			// 
			this.txtCurrentNet.Height = 0.1666667F;
			this.txtCurrentNet.HyperLink = null;
			this.txtCurrentNet.Left = 6.8125F;
			this.txtCurrentNet.Name = "txtCurrentNet";
			this.txtCurrentNet.Style = "text-align: right";
			this.txtCurrentNet.Tag = "text";
			this.txtCurrentNet.Text = "0.00";
			this.txtCurrentNet.Top = 1.854167F;
			this.txtCurrentNet.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "text-align: left";
			this.Label18.Tag = "text";
			this.Label18.Text = "FEDERAL";
			this.Label18.Top = 1.020833F;
			this.Label18.Width = 0.9166667F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "text-align: left";
			this.Label19.Tag = "text";
			this.Label19.Text = "FICA/MED";
			this.Label19.Top = 1.1875F;
			this.Label19.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 5.625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "text-align: left";
			this.Label20.Tag = "text";
			this.Label20.Text = "STATE";
			this.Label20.Top = 1.354167F;
			this.Label20.Width = 0.75F;
			// 
			// txtCurrentFed
			// 
			this.txtCurrentFed.Height = 0.1666667F;
			this.txtCurrentFed.HyperLink = null;
			this.txtCurrentFed.Left = 6.8125F;
			this.txtCurrentFed.Name = "txtCurrentFed";
			this.txtCurrentFed.Style = "text-align: right";
			this.txtCurrentFed.Tag = "text";
			this.txtCurrentFed.Text = "0.00";
			this.txtCurrentFed.Top = 1.020833F;
			this.txtCurrentFed.Width = 0.75F;
			// 
			// txtCurrentFica
			// 
			this.txtCurrentFica.Height = 0.1666667F;
			this.txtCurrentFica.HyperLink = null;
			this.txtCurrentFica.Left = 6.8125F;
			this.txtCurrentFica.Name = "txtCurrentFica";
			this.txtCurrentFica.Style = "text-align: right";
			this.txtCurrentFica.Tag = "text";
			this.txtCurrentFica.Text = "0.00";
			this.txtCurrentFica.Top = 1.1875F;
			this.txtCurrentFica.Width = 0.75F;
			// 
			// txtCurrentState
			// 
			this.txtCurrentState.Height = 0.1666667F;
			this.txtCurrentState.HyperLink = null;
			this.txtCurrentState.Left = 6.8125F;
			this.txtCurrentState.Name = "txtCurrentState";
			this.txtCurrentState.Style = "text-align: right";
			this.txtCurrentState.Tag = "text";
			this.txtCurrentState.Text = "0.00";
			this.txtCurrentState.Top = 1.354167F;
			this.txtCurrentState.Width = 0.75F;
			// 
			// txtPayDesc1
			// 
			this.txtPayDesc1.Height = 0.1666667F;
			this.txtPayDesc1.HyperLink = null;
			this.txtPayDesc1.Left = 0.0625F;
			this.txtPayDesc1.Name = "txtPayDesc1";
			this.txtPayDesc1.Style = "text-align: left";
			this.txtPayDesc1.Tag = "text";
			this.txtPayDesc1.Text = null;
			this.txtPayDesc1.Top = 0.8541667F;
			this.txtPayDesc1.Width = 1F;
			// 
			// txtHours1
			// 
			this.txtHours1.Height = 0.1666667F;
			this.txtHours1.HyperLink = null;
			this.txtHours1.Left = 1.125F;
			this.txtHours1.Name = "txtHours1";
			this.txtHours1.Style = "text-align: right";
			this.txtHours1.Tag = "text";
			this.txtHours1.Text = null;
			this.txtHours1.Top = 0.8541667F;
			this.txtHours1.Width = 0.6875F;
			// 
			// txtPayAmount1
			// 
			this.txtPayAmount1.Height = 0.1666667F;
			this.txtPayAmount1.HyperLink = null;
			this.txtPayAmount1.Left = 1.875F;
			this.txtPayAmount1.Name = "txtPayAmount1";
			this.txtPayAmount1.Style = "text-align: right";
			this.txtPayAmount1.Tag = "text";
			this.txtPayAmount1.Text = null;
			this.txtPayAmount1.Top = 0.8541667F;
			this.txtPayAmount1.Width = 0.6875F;
			// 
			// txtDedDesc1
			// 
			this.txtDedDesc1.Height = 0.1666667F;
			this.txtDedDesc1.HyperLink = null;
			this.txtDedDesc1.Left = 2.625F;
			this.txtDedDesc1.Name = "txtDedDesc1";
			this.txtDedDesc1.Style = "text-align: left";
			this.txtDedDesc1.Tag = "text";
			this.txtDedDesc1.Text = null;
			this.txtDedDesc1.Top = 0.8541667F;
			this.txtDedDesc1.Width = 1.4375F;
			// 
			// txtDedAmount1
			// 
			this.txtDedAmount1.Height = 0.1666667F;
			this.txtDedAmount1.HyperLink = null;
			this.txtDedAmount1.Left = 4.125F;
			this.txtDedAmount1.Name = "txtDedAmount1";
			this.txtDedAmount1.Style = "text-align: right";
			this.txtDedAmount1.Tag = "text";
			this.txtDedAmount1.Text = null;
			this.txtDedAmount1.Top = 0.8541667F;
			this.txtDedAmount1.Width = 0.75F;
			// 
			// txtDedYTD1
			// 
			this.txtDedYTD1.Height = 0.1666667F;
			this.txtDedYTD1.HyperLink = null;
			this.txtDedYTD1.Left = 4.875F;
			this.txtDedYTD1.Name = "txtDedYTD1";
			this.txtDedYTD1.Style = "text-align: right";
			this.txtDedYTD1.Tag = "text";
			this.txtDedYTD1.Text = null;
			this.txtDedYTD1.Top = 0.8541667F;
			this.txtDedYTD1.Width = 0.6875F;
			// 
			// lblTotalPay
			// 
			this.lblTotalPay.Height = 0.1666667F;
			this.lblTotalPay.HyperLink = null;
			this.lblTotalPay.Left = 0.0625F;
			this.lblTotalPay.Name = "lblTotalPay";
			this.lblTotalPay.Style = "text-align: left";
			this.lblTotalPay.Tag = "text";
			this.lblTotalPay.Text = "TOTAL";
			this.lblTotalPay.Top = 4.666667F;
			this.lblTotalPay.Width = 0.5F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1666667F;
			this.txtTotalHours.HyperLink = null;
			this.txtTotalHours.Left = 1.0625F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "text-align: right";
			this.txtTotalHours.Tag = "text";
			this.txtTotalHours.Text = "0.00";
			this.txtTotalHours.Top = 4.666667F;
			this.txtTotalHours.Width = 0.75F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.HyperLink = null;
			this.txtTotalAmount.Left = 1.875F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "text-align: right";
			this.txtTotalAmount.Tag = "text";
			this.txtTotalAmount.Text = "0.00";
			this.txtTotalAmount.Top = 4.666667F;
			this.txtTotalAmount.Width = 0.6875F;
			// 
			// txtYTDGross
			// 
			this.txtYTDGross.Height = 0.1666667F;
			this.txtYTDGross.HyperLink = null;
			this.txtYTDGross.Left = 7.572917F;
			this.txtYTDGross.Name = "txtYTDGross";
			this.txtYTDGross.Style = "text-align: right";
			this.txtYTDGross.Tag = "text";
			this.txtYTDGross.Text = "0.00";
			this.txtYTDGross.Top = 0.8541667F;
			this.txtYTDGross.Width = 0.8125F;
			// 
			// txtYTDFed
			// 
			this.txtYTDFed.Height = 0.1666667F;
			this.txtYTDFed.HyperLink = null;
			this.txtYTDFed.Left = 7.572917F;
			this.txtYTDFed.Name = "txtYTDFed";
			this.txtYTDFed.Style = "text-align: right";
			this.txtYTDFed.Tag = "text";
			this.txtYTDFed.Text = "0.00";
			this.txtYTDFed.Top = 1.020833F;
			this.txtYTDFed.Width = 0.8125F;
			// 
			// txtYTDFICA
			// 
			this.txtYTDFICA.Height = 0.1666667F;
			this.txtYTDFICA.HyperLink = null;
			this.txtYTDFICA.Left = 7.572917F;
			this.txtYTDFICA.Name = "txtYTDFICA";
			this.txtYTDFICA.Style = "text-align: right";
			this.txtYTDFICA.Tag = "text";
			this.txtYTDFICA.Text = "0.00";
			this.txtYTDFICA.Top = 1.1875F;
			this.txtYTDFICA.Width = 0.8125F;
			// 
			// txtYTDState
			// 
			this.txtYTDState.Height = 0.1666667F;
			this.txtYTDState.HyperLink = null;
			this.txtYTDState.Left = 7.572917F;
			this.txtYTDState.Name = "txtYTDState";
			this.txtYTDState.Style = "text-align: right";
			this.txtYTDState.Tag = "text";
			this.txtYTDState.Text = "0.00";
			this.txtYTDState.Top = 1.354167F;
			this.txtYTDState.Width = 0.8125F;
			// 
			// txtYTDNet
			// 
			this.txtYTDNet.Height = 0.1666667F;
			this.txtYTDNet.HyperLink = null;
			this.txtYTDNet.Left = 7.572917F;
			this.txtYTDNet.Name = "txtYTDNet";
			this.txtYTDNet.Style = "text-align: right";
			this.txtYTDNet.Tag = "text";
			this.txtYTDNet.Text = "0.00";
			this.txtYTDNet.Top = 1.854167F;
			this.txtYTDNet.Width = 0.8125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 7.635417F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: right";
			this.Label12.Tag = "text";
			this.Label12.Text = "BALANCE";
			this.Label12.Top = 2.177083F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: left";
			this.Label13.Tag = "text";
			this.Label13.Text = "VACATION";
			this.Label13.Top = 2.34375F;
			this.Label13.Width = 1.989583F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "text-align: left";
			this.Label14.Tag = "text";
			this.Label14.Text = "SICK";
			this.Label14.Top = 2.510417F;
			this.Label14.Width = 1.989583F;
			// 
			// txtVacationBalance
			// 
			this.txtVacationBalance.Height = 0.1666667F;
			this.txtVacationBalance.HyperLink = null;
			this.txtVacationBalance.Left = 7.760417F;
			this.txtVacationBalance.Name = "txtVacationBalance";
			this.txtVacationBalance.Style = "text-align: right";
			this.txtVacationBalance.Tag = "text";
			this.txtVacationBalance.Text = "0.00";
			this.txtVacationBalance.Top = 2.34375F;
			this.txtVacationBalance.Width = 0.625F;
			// 
			// txtSickBalance
			// 
			this.txtSickBalance.Height = 0.1666667F;
			this.txtSickBalance.HyperLink = null;
			this.txtSickBalance.Left = 7.760417F;
			this.txtSickBalance.Name = "txtSickBalance";
			this.txtSickBalance.Style = "text-align: right";
			this.txtSickBalance.Tag = "text";
			this.txtSickBalance.Text = "0.00";
			this.txtSickBalance.Top = 2.510417F;
			this.txtSickBalance.Width = 0.625F;
			// 
			// lblEMatch
			// 
			this.lblEMatch.Height = 0.1666667F;
			this.lblEMatch.HyperLink = null;
			this.lblEMatch.Left = 2.625F;
			this.lblEMatch.Name = "lblEMatch";
			this.lblEMatch.Style = "text-align: left";
			this.lblEMatch.Tag = "text";
			this.lblEMatch.Text = "E/MATCH";
			this.lblEMatch.Top = 4.666667F;
			this.lblEMatch.Width = 0.9375F;
			// 
			// txtEMatchCurrent
			// 
			this.txtEMatchCurrent.Height = 0.1666667F;
			this.txtEMatchCurrent.HyperLink = null;
			this.txtEMatchCurrent.Left = 4.125F;
			this.txtEMatchCurrent.Name = "txtEMatchCurrent";
			this.txtEMatchCurrent.Style = "text-align: right";
			this.txtEMatchCurrent.Tag = "text";
			this.txtEMatchCurrent.Text = "0.00";
			this.txtEMatchCurrent.Top = 4.666667F;
			this.txtEMatchCurrent.Width = 0.75F;
			// 
			// txtEmatchYTD
			// 
			this.txtEmatchYTD.Height = 0.1666667F;
			this.txtEmatchYTD.HyperLink = null;
			this.txtEmatchYTD.Left = 4.875F;
			this.txtEmatchYTD.Name = "txtEmatchYTD";
			this.txtEmatchYTD.Style = "text-align: right";
			this.txtEmatchYTD.Tag = "text";
			this.txtEmatchYTD.Text = "0.00";
			this.txtEmatchYTD.Top = 4.666667F;
			this.txtEmatchYTD.Width = 0.6875F;
			// 
			// txtDirectDepositLabel
			// 
			this.txtDirectDepositLabel.Height = 0.1666667F;
			this.txtDirectDepositLabel.HyperLink = null;
			this.txtDirectDepositLabel.Left = 0.0625F;
			this.txtDirectDepositLabel.Name = "txtDirectDepositLabel";
			this.txtDirectDepositLabel.Style = "text-align: right";
			this.txtDirectDepositLabel.Tag = "text";
			this.txtDirectDepositLabel.Text = "DIRECT DEPOSIT";
			this.txtDirectDepositLabel.Top = 4.958333F;
			this.txtDirectDepositLabel.Visible = false;
			this.txtDirectDepositLabel.Width = 1.4375F;
			// 
			// txtDirectDeposit
			// 
			this.txtDirectDeposit.Height = 0.1666667F;
			this.txtDirectDeposit.HyperLink = null;
			this.txtDirectDeposit.Left = 1.177083F;
			this.txtDirectDeposit.Name = "txtDirectDeposit";
			this.txtDirectDeposit.Style = "text-align: right";
			this.txtDirectDeposit.Tag = "text";
			this.txtDirectDeposit.Text = "0.00";
			this.txtDirectDeposit.Top = 5.125F;
			this.txtDirectDeposit.Visible = false;
			this.txtDirectDeposit.Width = 0.75F;
			// 
			// txtChkAmount
			// 
			this.txtChkAmount.Height = 0.1666667F;
			this.txtChkAmount.HyperLink = null;
			this.txtChkAmount.Left = 1.177083F;
			this.txtChkAmount.Name = "txtChkAmount";
			this.txtChkAmount.Style = "text-align: right";
			this.txtChkAmount.Tag = "text";
			this.txtChkAmount.Text = "0.00";
			this.txtChkAmount.Top = 5.458333F;
			this.txtChkAmount.Width = 0.75F;
			// 
			// lblCheckAmount
			// 
			this.lblCheckAmount.Height = 0.1666667F;
			this.lblCheckAmount.HyperLink = null;
			this.lblCheckAmount.Left = 0.0625F;
			this.lblCheckAmount.Name = "lblCheckAmount";
			this.lblCheckAmount.Style = "text-align: left";
			this.lblCheckAmount.Tag = "text";
			this.lblCheckAmount.Text = "CHECK AMOUNT";
			this.lblCheckAmount.Top = 5.458333F;
			this.lblCheckAmount.Width = 1.125F;
			// 
			// lblPayRate
			// 
			this.lblPayRate.Height = 0.1666667F;
			this.lblPayRate.HyperLink = null;
			this.lblPayRate.Left = 1.625F;
			this.lblPayRate.Name = "lblPayRate";
			this.lblPayRate.Style = "text-align: left";
			this.lblPayRate.Tag = "text";
			this.lblPayRate.Text = "PAY RATE:";
			this.lblPayRate.Top = 0.3958333F;
			this.lblPayRate.Width = 0.875F;
			// 
			// txtPayRate
			// 
			this.txtPayRate.Height = 0.1666667F;
			this.txtPayRate.HyperLink = null;
			this.txtPayRate.Left = 2.5F;
			this.txtPayRate.Name = "txtPayRate";
			this.txtPayRate.Style = "text-align: right";
			this.txtPayRate.Tag = "text";
			this.txtPayRate.Text = null;
			this.txtPayRate.Top = 0.3958333F;
			this.txtPayRate.Width = 0.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1666667F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.625F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "text-align: left";
			this.Label29.Tag = "text";
			this.Label29.Text = "OTHER";
			this.Label29.Top = 2.677083F;
			this.Label29.Width = 1.989583F;
			// 
			// txtOtherBalance
			// 
			this.txtOtherBalance.Height = 0.1666667F;
			this.txtOtherBalance.HyperLink = null;
			this.txtOtherBalance.Left = 7.760417F;
			this.txtOtherBalance.Name = "txtOtherBalance";
			this.txtOtherBalance.Style = "text-align: right";
			this.txtOtherBalance.Tag = "text";
			this.txtOtherBalance.Text = "0.00";
			this.txtOtherBalance.Top = 2.677083F;
			this.txtOtherBalance.Width = 0.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 5.625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "text";
			this.txtDate.Text = "DATE";
			this.txtDate.Top = 0.1666667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtDirectDepChkLabel
			// 
			this.txtDirectDepChkLabel.Height = 0.1666667F;
			this.txtDirectDepChkLabel.HyperLink = null;
			this.txtDirectDepChkLabel.Left = 0.0625F;
			this.txtDirectDepChkLabel.Name = "txtDirectDepChkLabel";
			this.txtDirectDepChkLabel.Style = "text-align: left";
			this.txtDirectDepChkLabel.Tag = "text";
			this.txtDirectDepChkLabel.Text = "CHECKING";
			this.txtDirectDepChkLabel.Top = 5.125F;
			this.txtDirectDepChkLabel.Visible = false;
			this.txtDirectDepChkLabel.Width = 1.125F;
			// 
			// txtDirectDepSav
			// 
			this.txtDirectDepSav.Height = 0.1666667F;
			this.txtDirectDepSav.HyperLink = null;
			this.txtDirectDepSav.Left = 1.177083F;
			this.txtDirectDepSav.Name = "txtDirectDepSav";
			this.txtDirectDepSav.Style = "text-align: right";
			this.txtDirectDepSav.Tag = "text";
			this.txtDirectDepSav.Text = "0.00";
			this.txtDirectDepSav.Top = 5.291667F;
			this.txtDirectDepSav.Visible = false;
			this.txtDirectDepSav.Width = 0.75F;
			// 
			// txtDirectDepositSavLabel
			// 
			this.txtDirectDepositSavLabel.Height = 0.1666667F;
			this.txtDirectDepositSavLabel.HyperLink = null;
			this.txtDirectDepositSavLabel.Left = 0.0625F;
			this.txtDirectDepositSavLabel.Name = "txtDirectDepositSavLabel";
			this.txtDirectDepositSavLabel.Style = "text-align: left";
			this.txtDirectDepositSavLabel.Tag = "text";
			this.txtDirectDepositSavLabel.Text = "SAVINGS";
			this.txtDirectDepositSavLabel.Top = 5.291667F;
			this.txtDirectDepositSavLabel.Visible = false;
			this.txtDirectDepositSavLabel.Width = 1.125F;
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Height = 0.1666667F;
			this.lblCheckMessage.HyperLink = null;
			this.lblCheckMessage.Left = 0.0625F;
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Style = "font-size: 10pt; text-align: right";
			this.lblCheckMessage.Text = null;
			this.lblCheckMessage.Top = 5.71875F;
			this.lblCheckMessage.Width = 8.333333F;
			// 
			// lblCode1
			// 
			this.lblCode1.Height = 0.1666667F;
			this.lblCode1.HyperLink = null;
			this.lblCode1.Left = 5.625F;
			this.lblCode1.Name = "lblCode1";
			this.lblCode1.Style = "font-size: 10pt; text-align: left";
			this.lblCode1.Tag = "text";
			this.lblCode1.Text = "Code1";
			this.lblCode1.Top = 2.833333F;
			this.lblCode1.Width = 1.989583F;
			// 
			// lblCode2
			// 
			this.lblCode2.Height = 0.1666667F;
			this.lblCode2.HyperLink = null;
			this.lblCode2.Left = 5.625F;
			this.lblCode2.Name = "lblCode2";
			this.lblCode2.Style = "font-size: 10pt; text-align: left";
			this.lblCode2.Tag = "text";
			this.lblCode2.Text = "Code2";
			this.lblCode2.Top = 3F;
			this.lblCode2.Width = 1.989583F;
			// 
			// lblCode1Balance
			// 
			this.lblCode1Balance.Height = 0.1666667F;
			this.lblCode1Balance.HyperLink = null;
			this.lblCode1Balance.Left = 7.760417F;
			this.lblCode1Balance.Name = "lblCode1Balance";
			this.lblCode1Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode1Balance.Tag = "text";
			this.lblCode1Balance.Text = "0.00";
			this.lblCode1Balance.Top = 2.833333F;
			this.lblCode1Balance.Width = 0.625F;
			// 
			// lblCode2Balance
			// 
			this.lblCode2Balance.Height = 0.1666667F;
			this.lblCode2Balance.HyperLink = null;
			this.lblCode2Balance.Left = 7.760417F;
			this.lblCode2Balance.Name = "lblCode2Balance";
			this.lblCode2Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode2Balance.Tag = "text";
			this.lblCode2Balance.Text = "0.00";
			this.lblCode2Balance.Top = 3F;
			this.lblCode2Balance.Width = 0.625F;
			// 
			// lblCode3
			// 
			this.lblCode3.Height = 0.1666667F;
			this.lblCode3.HyperLink = null;
			this.lblCode3.Left = 5.625F;
			this.lblCode3.Name = "lblCode3";
			this.lblCode3.Style = "font-size: 10pt; text-align: left";
			this.lblCode3.Tag = "text";
			this.lblCode3.Text = "Code3";
			this.lblCode3.Top = 3.166667F;
			this.lblCode3.Width = 1.989583F;
			// 
			// lblCode3Balance
			// 
			this.lblCode3Balance.Height = 0.1666667F;
			this.lblCode3Balance.HyperLink = null;
			this.lblCode3Balance.Left = 7.760417F;
			this.lblCode3Balance.Name = "lblCode3Balance";
			this.lblCode3Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode3Balance.Tag = "text";
			this.lblCode3Balance.Text = "0.00";
			this.lblCode3Balance.Top = 3.166667F;
			this.lblCode3Balance.Width = 0.625F;
			// 
			// txtPayDesc2
			// 
			this.txtPayDesc2.Height = 0.1666667F;
			this.txtPayDesc2.HyperLink = null;
			this.txtPayDesc2.Left = 0.0625F;
			this.txtPayDesc2.Name = "txtPayDesc2";
			this.txtPayDesc2.Style = "text-align: left";
			this.txtPayDesc2.Tag = "text";
			this.txtPayDesc2.Text = null;
			this.txtPayDesc2.Top = 1.020833F;
			this.txtPayDesc2.Width = 1F;
			// 
			// txtHours2
			// 
			this.txtHours2.Height = 0.1666667F;
			this.txtHours2.HyperLink = null;
			this.txtHours2.Left = 1.125F;
			this.txtHours2.Name = "txtHours2";
			this.txtHours2.Style = "text-align: right";
			this.txtHours2.Tag = "text";
			this.txtHours2.Text = null;
			this.txtHours2.Top = 1.020833F;
			this.txtHours2.Width = 0.6875F;
			// 
			// txtPayAmount2
			// 
			this.txtPayAmount2.Height = 0.1666667F;
			this.txtPayAmount2.HyperLink = null;
			this.txtPayAmount2.Left = 1.875F;
			this.txtPayAmount2.Name = "txtPayAmount2";
			this.txtPayAmount2.Style = "text-align: right";
			this.txtPayAmount2.Tag = "text";
			this.txtPayAmount2.Text = null;
			this.txtPayAmount2.Top = 1.020833F;
			this.txtPayAmount2.Width = 0.6875F;
			// 
			// txtDedDesc2
			// 
			this.txtDedDesc2.Height = 0.1666667F;
			this.txtDedDesc2.HyperLink = null;
			this.txtDedDesc2.Left = 2.625F;
			this.txtDedDesc2.Name = "txtDedDesc2";
			this.txtDedDesc2.Style = "text-align: left";
			this.txtDedDesc2.Tag = "text";
			this.txtDedDesc2.Text = null;
			this.txtDedDesc2.Top = 1.020833F;
			this.txtDedDesc2.Width = 1.4375F;
			// 
			// txtDedAmount2
			// 
			this.txtDedAmount2.Height = 0.1666667F;
			this.txtDedAmount2.HyperLink = null;
			this.txtDedAmount2.Left = 4.125F;
			this.txtDedAmount2.Name = "txtDedAmount2";
			this.txtDedAmount2.Style = "text-align: right";
			this.txtDedAmount2.Tag = "text";
			this.txtDedAmount2.Text = null;
			this.txtDedAmount2.Top = 1.020833F;
			this.txtDedAmount2.Width = 0.75F;
			// 
			// txtDedYTD2
			// 
			this.txtDedYTD2.Height = 0.1666667F;
			this.txtDedYTD2.HyperLink = null;
			this.txtDedYTD2.Left = 4.875F;
			this.txtDedYTD2.Name = "txtDedYTD2";
			this.txtDedYTD2.Style = "text-align: right";
			this.txtDedYTD2.Tag = "text";
			this.txtDedYTD2.Text = null;
			this.txtDedYTD2.Top = 1.020833F;
			this.txtDedYTD2.Width = 0.6875F;
			// 
			// txtPayDesc3
			// 
			this.txtPayDesc3.Height = 0.1666667F;
			this.txtPayDesc3.HyperLink = null;
			this.txtPayDesc3.Left = 0.0625F;
			this.txtPayDesc3.Name = "txtPayDesc3";
			this.txtPayDesc3.Style = "text-align: left";
			this.txtPayDesc3.Tag = "text";
			this.txtPayDesc3.Text = null;
			this.txtPayDesc3.Top = 1.1875F;
			this.txtPayDesc3.Width = 1F;
			// 
			// txtHours3
			// 
			this.txtHours3.Height = 0.1666667F;
			this.txtHours3.HyperLink = null;
			this.txtHours3.Left = 1.125F;
			this.txtHours3.Name = "txtHours3";
			this.txtHours3.Style = "text-align: right";
			this.txtHours3.Tag = "text";
			this.txtHours3.Text = null;
			this.txtHours3.Top = 1.1875F;
			this.txtHours3.Width = 0.6875F;
			// 
			// txtPayAmount3
			// 
			this.txtPayAmount3.Height = 0.1666667F;
			this.txtPayAmount3.HyperLink = null;
			this.txtPayAmount3.Left = 1.875F;
			this.txtPayAmount3.Name = "txtPayAmount3";
			this.txtPayAmount3.Style = "text-align: right";
			this.txtPayAmount3.Tag = "text";
			this.txtPayAmount3.Text = null;
			this.txtPayAmount3.Top = 1.1875F;
			this.txtPayAmount3.Width = 0.6875F;
			// 
			// txtDedDesc3
			// 
			this.txtDedDesc3.Height = 0.1666667F;
			this.txtDedDesc3.HyperLink = null;
			this.txtDedDesc3.Left = 2.625F;
			this.txtDedDesc3.Name = "txtDedDesc3";
			this.txtDedDesc3.Style = "text-align: left";
			this.txtDedDesc3.Tag = "text";
			this.txtDedDesc3.Text = null;
			this.txtDedDesc3.Top = 1.1875F;
			this.txtDedDesc3.Width = 1.4375F;
			// 
			// txtDedAmount3
			// 
			this.txtDedAmount3.Height = 0.1666667F;
			this.txtDedAmount3.HyperLink = null;
			this.txtDedAmount3.Left = 4.125F;
			this.txtDedAmount3.Name = "txtDedAmount3";
			this.txtDedAmount3.Style = "text-align: right";
			this.txtDedAmount3.Tag = "text";
			this.txtDedAmount3.Text = null;
			this.txtDedAmount3.Top = 1.1875F;
			this.txtDedAmount3.Width = 0.75F;
			// 
			// txtDedYTD3
			// 
			this.txtDedYTD3.Height = 0.1666667F;
			this.txtDedYTD3.HyperLink = null;
			this.txtDedYTD3.Left = 4.875F;
			this.txtDedYTD3.Name = "txtDedYTD3";
			this.txtDedYTD3.Style = "text-align: right";
			this.txtDedYTD3.Tag = "text";
			this.txtDedYTD3.Text = null;
			this.txtDedYTD3.Top = 1.1875F;
			this.txtDedYTD3.Width = 0.6875F;
			// 
			// txtPayDesc4
			// 
			this.txtPayDesc4.Height = 0.1666667F;
			this.txtPayDesc4.HyperLink = null;
			this.txtPayDesc4.Left = 0.0625F;
			this.txtPayDesc4.Name = "txtPayDesc4";
			this.txtPayDesc4.Style = "text-align: left";
			this.txtPayDesc4.Tag = "text";
			this.txtPayDesc4.Text = null;
			this.txtPayDesc4.Top = 1.354167F;
			this.txtPayDesc4.Width = 1F;
			// 
			// txtHours4
			// 
			this.txtHours4.Height = 0.1666667F;
			this.txtHours4.HyperLink = null;
			this.txtHours4.Left = 1.125F;
			this.txtHours4.Name = "txtHours4";
			this.txtHours4.Style = "text-align: right";
			this.txtHours4.Tag = "text";
			this.txtHours4.Text = null;
			this.txtHours4.Top = 1.354167F;
			this.txtHours4.Width = 0.6875F;
			// 
			// txtPayAmount4
			// 
			this.txtPayAmount4.Height = 0.1666667F;
			this.txtPayAmount4.HyperLink = null;
			this.txtPayAmount4.Left = 1.875F;
			this.txtPayAmount4.Name = "txtPayAmount4";
			this.txtPayAmount4.Style = "text-align: right";
			this.txtPayAmount4.Tag = "text";
			this.txtPayAmount4.Text = null;
			this.txtPayAmount4.Top = 1.354167F;
			this.txtPayAmount4.Width = 0.6875F;
			// 
			// txtDedDesc4
			// 
			this.txtDedDesc4.Height = 0.1666667F;
			this.txtDedDesc4.HyperLink = null;
			this.txtDedDesc4.Left = 2.625F;
			this.txtDedDesc4.Name = "txtDedDesc4";
			this.txtDedDesc4.Style = "text-align: left";
			this.txtDedDesc4.Tag = "text";
			this.txtDedDesc4.Text = null;
			this.txtDedDesc4.Top = 1.354167F;
			this.txtDedDesc4.Width = 1.4375F;
			// 
			// txtDedAmount4
			// 
			this.txtDedAmount4.Height = 0.1666667F;
			this.txtDedAmount4.HyperLink = null;
			this.txtDedAmount4.Left = 4.125F;
			this.txtDedAmount4.Name = "txtDedAmount4";
			this.txtDedAmount4.Style = "text-align: right";
			this.txtDedAmount4.Tag = "text";
			this.txtDedAmount4.Text = null;
			this.txtDedAmount4.Top = 1.354167F;
			this.txtDedAmount4.Width = 0.75F;
			// 
			// txtDedYTD4
			// 
			this.txtDedYTD4.Height = 0.1666667F;
			this.txtDedYTD4.HyperLink = null;
			this.txtDedYTD4.Left = 4.875F;
			this.txtDedYTD4.Name = "txtDedYTD4";
			this.txtDedYTD4.Style = "text-align: right";
			this.txtDedYTD4.Tag = "text";
			this.txtDedYTD4.Text = null;
			this.txtDedYTD4.Top = 1.354167F;
			this.txtDedYTD4.Width = 0.6875F;
			// 
			// txtPayDesc5
			// 
			this.txtPayDesc5.Height = 0.1666667F;
			this.txtPayDesc5.HyperLink = null;
			this.txtPayDesc5.Left = 0.0625F;
			this.txtPayDesc5.Name = "txtPayDesc5";
			this.txtPayDesc5.Style = "text-align: left";
			this.txtPayDesc5.Tag = "text";
			this.txtPayDesc5.Text = null;
			this.txtPayDesc5.Top = 1.520833F;
			this.txtPayDesc5.Width = 1F;
			// 
			// txtHours5
			// 
			this.txtHours5.Height = 0.1666667F;
			this.txtHours5.HyperLink = null;
			this.txtHours5.Left = 1.125F;
			this.txtHours5.Name = "txtHours5";
			this.txtHours5.Style = "text-align: right";
			this.txtHours5.Tag = "text";
			this.txtHours5.Text = null;
			this.txtHours5.Top = 1.520833F;
			this.txtHours5.Width = 0.6875F;
			// 
			// txtPayAmount5
			// 
			this.txtPayAmount5.Height = 0.1666667F;
			this.txtPayAmount5.HyperLink = null;
			this.txtPayAmount5.Left = 1.875F;
			this.txtPayAmount5.Name = "txtPayAmount5";
			this.txtPayAmount5.Style = "text-align: right";
			this.txtPayAmount5.Tag = "text";
			this.txtPayAmount5.Text = null;
			this.txtPayAmount5.Top = 1.520833F;
			this.txtPayAmount5.Width = 0.6875F;
			// 
			// txtDedDesc5
			// 
			this.txtDedDesc5.Height = 0.1666667F;
			this.txtDedDesc5.HyperLink = null;
			this.txtDedDesc5.Left = 2.625F;
			this.txtDedDesc5.Name = "txtDedDesc5";
			this.txtDedDesc5.Style = "text-align: left";
			this.txtDedDesc5.Tag = "text";
			this.txtDedDesc5.Text = null;
			this.txtDedDesc5.Top = 1.520833F;
			this.txtDedDesc5.Width = 1.4375F;
			// 
			// txtDedAmount5
			// 
			this.txtDedAmount5.Height = 0.1666667F;
			this.txtDedAmount5.HyperLink = null;
			this.txtDedAmount5.Left = 4.125F;
			this.txtDedAmount5.Name = "txtDedAmount5";
			this.txtDedAmount5.Style = "text-align: right";
			this.txtDedAmount5.Tag = "text";
			this.txtDedAmount5.Text = null;
			this.txtDedAmount5.Top = 1.520833F;
			this.txtDedAmount5.Width = 0.75F;
			// 
			// txtDedYTD5
			// 
			this.txtDedYTD5.Height = 0.1666667F;
			this.txtDedYTD5.HyperLink = null;
			this.txtDedYTD5.Left = 4.875F;
			this.txtDedYTD5.Name = "txtDedYTD5";
			this.txtDedYTD5.Style = "text-align: right";
			this.txtDedYTD5.Tag = "text";
			this.txtDedYTD5.Text = null;
			this.txtDedYTD5.Top = 1.520833F;
			this.txtDedYTD5.Width = 0.6875F;
			// 
			// txtPayDesc6
			// 
			this.txtPayDesc6.Height = 0.1666667F;
			this.txtPayDesc6.HyperLink = null;
			this.txtPayDesc6.Left = 0.0625F;
			this.txtPayDesc6.Name = "txtPayDesc6";
			this.txtPayDesc6.Style = "text-align: left";
			this.txtPayDesc6.Tag = "text";
			this.txtPayDesc6.Text = null;
			this.txtPayDesc6.Top = 1.6875F;
			this.txtPayDesc6.Width = 1F;
			// 
			// txtHours6
			// 
			this.txtHours6.Height = 0.1666667F;
			this.txtHours6.HyperLink = null;
			this.txtHours6.Left = 1.125F;
			this.txtHours6.Name = "txtHours6";
			this.txtHours6.Style = "text-align: right";
			this.txtHours6.Tag = "text";
			this.txtHours6.Text = null;
			this.txtHours6.Top = 1.6875F;
			this.txtHours6.Width = 0.6875F;
			// 
			// txtPayAmount6
			// 
			this.txtPayAmount6.Height = 0.1666667F;
			this.txtPayAmount6.HyperLink = null;
			this.txtPayAmount6.Left = 1.875F;
			this.txtPayAmount6.Name = "txtPayAmount6";
			this.txtPayAmount6.Style = "text-align: right";
			this.txtPayAmount6.Tag = "text";
			this.txtPayAmount6.Text = null;
			this.txtPayAmount6.Top = 1.6875F;
			this.txtPayAmount6.Width = 0.6875F;
			// 
			// txtDedDesc6
			// 
			this.txtDedDesc6.Height = 0.1666667F;
			this.txtDedDesc6.HyperLink = null;
			this.txtDedDesc6.Left = 2.625F;
			this.txtDedDesc6.Name = "txtDedDesc6";
			this.txtDedDesc6.Style = "text-align: left";
			this.txtDedDesc6.Tag = "text";
			this.txtDedDesc6.Text = null;
			this.txtDedDesc6.Top = 1.6875F;
			this.txtDedDesc6.Width = 1.4375F;
			// 
			// txtDedAmount6
			// 
			this.txtDedAmount6.Height = 0.1666667F;
			this.txtDedAmount6.HyperLink = null;
			this.txtDedAmount6.Left = 4.125F;
			this.txtDedAmount6.Name = "txtDedAmount6";
			this.txtDedAmount6.Style = "text-align: right";
			this.txtDedAmount6.Tag = "text";
			this.txtDedAmount6.Text = null;
			this.txtDedAmount6.Top = 1.6875F;
			this.txtDedAmount6.Width = 0.75F;
			// 
			// txtDedYTD6
			// 
			this.txtDedYTD6.Height = 0.1666667F;
			this.txtDedYTD6.HyperLink = null;
			this.txtDedYTD6.Left = 4.875F;
			this.txtDedYTD6.Name = "txtDedYTD6";
			this.txtDedYTD6.Style = "text-align: right";
			this.txtDedYTD6.Tag = "text";
			this.txtDedYTD6.Text = null;
			this.txtDedYTD6.Top = 1.6875F;
			this.txtDedYTD6.Width = 0.6875F;
			// 
			// txtPayDesc7
			// 
			this.txtPayDesc7.Height = 0.1666667F;
			this.txtPayDesc7.HyperLink = null;
			this.txtPayDesc7.Left = 0.0625F;
			this.txtPayDesc7.Name = "txtPayDesc7";
			this.txtPayDesc7.Style = "text-align: left";
			this.txtPayDesc7.Tag = "text";
			this.txtPayDesc7.Text = null;
			this.txtPayDesc7.Top = 1.854167F;
			this.txtPayDesc7.Width = 1F;
			// 
			// txtHours7
			// 
			this.txtHours7.Height = 0.1666667F;
			this.txtHours7.HyperLink = null;
			this.txtHours7.Left = 1.125F;
			this.txtHours7.Name = "txtHours7";
			this.txtHours7.Style = "text-align: right";
			this.txtHours7.Tag = "text";
			this.txtHours7.Text = null;
			this.txtHours7.Top = 1.854167F;
			this.txtHours7.Width = 0.6875F;
			// 
			// txtPayAmount7
			// 
			this.txtPayAmount7.Height = 0.1666667F;
			this.txtPayAmount7.HyperLink = null;
			this.txtPayAmount7.Left = 1.875F;
			this.txtPayAmount7.Name = "txtPayAmount7";
			this.txtPayAmount7.Style = "text-align: right";
			this.txtPayAmount7.Tag = "text";
			this.txtPayAmount7.Text = null;
			this.txtPayAmount7.Top = 1.854167F;
			this.txtPayAmount7.Width = 0.6875F;
			// 
			// txtDedDesc7
			// 
			this.txtDedDesc7.Height = 0.1666667F;
			this.txtDedDesc7.HyperLink = null;
			this.txtDedDesc7.Left = 2.625F;
			this.txtDedDesc7.Name = "txtDedDesc7";
			this.txtDedDesc7.Style = "text-align: left";
			this.txtDedDesc7.Tag = "text";
			this.txtDedDesc7.Text = null;
			this.txtDedDesc7.Top = 1.854167F;
			this.txtDedDesc7.Width = 1.4375F;
			// 
			// txtDedAmount7
			// 
			this.txtDedAmount7.Height = 0.1666667F;
			this.txtDedAmount7.HyperLink = null;
			this.txtDedAmount7.Left = 4.125F;
			this.txtDedAmount7.Name = "txtDedAmount7";
			this.txtDedAmount7.Style = "text-align: right";
			this.txtDedAmount7.Tag = "text";
			this.txtDedAmount7.Text = null;
			this.txtDedAmount7.Top = 1.854167F;
			this.txtDedAmount7.Width = 0.75F;
			// 
			// txtDedYTD7
			// 
			this.txtDedYTD7.Height = 0.1666667F;
			this.txtDedYTD7.HyperLink = null;
			this.txtDedYTD7.Left = 4.875F;
			this.txtDedYTD7.Name = "txtDedYTD7";
			this.txtDedYTD7.Style = "text-align: right";
			this.txtDedYTD7.Tag = "text";
			this.txtDedYTD7.Text = null;
			this.txtDedYTD7.Top = 1.854167F;
			this.txtDedYTD7.Width = 0.6875F;
			// 
			// txtPayDesc8
			// 
			this.txtPayDesc8.Height = 0.1666667F;
			this.txtPayDesc8.HyperLink = null;
			this.txtPayDesc8.Left = 0.0625F;
			this.txtPayDesc8.Name = "txtPayDesc8";
			this.txtPayDesc8.Style = "text-align: left";
			this.txtPayDesc8.Tag = "text";
			this.txtPayDesc8.Text = null;
			this.txtPayDesc8.Top = 2.020833F;
			this.txtPayDesc8.Width = 1F;
			// 
			// txtHours8
			// 
			this.txtHours8.Height = 0.1666667F;
			this.txtHours8.HyperLink = null;
			this.txtHours8.Left = 1.125F;
			this.txtHours8.Name = "txtHours8";
			this.txtHours8.Style = "text-align: right";
			this.txtHours8.Tag = "text";
			this.txtHours8.Text = null;
			this.txtHours8.Top = 2.020833F;
			this.txtHours8.Width = 0.6875F;
			// 
			// txtPayAmount8
			// 
			this.txtPayAmount8.Height = 0.1666667F;
			this.txtPayAmount8.HyperLink = null;
			this.txtPayAmount8.Left = 1.875F;
			this.txtPayAmount8.Name = "txtPayAmount8";
			this.txtPayAmount8.Style = "text-align: right";
			this.txtPayAmount8.Tag = "text";
			this.txtPayAmount8.Text = null;
			this.txtPayAmount8.Top = 2.020833F;
			this.txtPayAmount8.Width = 0.6875F;
			// 
			// txtDedDesc8
			// 
			this.txtDedDesc8.Height = 0.1666667F;
			this.txtDedDesc8.HyperLink = null;
			this.txtDedDesc8.Left = 2.625F;
			this.txtDedDesc8.Name = "txtDedDesc8";
			this.txtDedDesc8.Style = "text-align: left";
			this.txtDedDesc8.Tag = "text";
			this.txtDedDesc8.Text = null;
			this.txtDedDesc8.Top = 2.020833F;
			this.txtDedDesc8.Width = 1.4375F;
			// 
			// txtDedAmount8
			// 
			this.txtDedAmount8.Height = 0.1666667F;
			this.txtDedAmount8.HyperLink = null;
			this.txtDedAmount8.Left = 4.125F;
			this.txtDedAmount8.Name = "txtDedAmount8";
			this.txtDedAmount8.Style = "text-align: right";
			this.txtDedAmount8.Tag = "text";
			this.txtDedAmount8.Text = null;
			this.txtDedAmount8.Top = 2.020833F;
			this.txtDedAmount8.Width = 0.75F;
			// 
			// txtDedYTD8
			// 
			this.txtDedYTD8.Height = 0.1666667F;
			this.txtDedYTD8.HyperLink = null;
			this.txtDedYTD8.Left = 4.875F;
			this.txtDedYTD8.Name = "txtDedYTD8";
			this.txtDedYTD8.Style = "text-align: right";
			this.txtDedYTD8.Tag = "text";
			this.txtDedYTD8.Text = null;
			this.txtDedYTD8.Top = 2.020833F;
			this.txtDedYTD8.Width = 0.6875F;
			// 
			// txtPayDesc9
			// 
			this.txtPayDesc9.Height = 0.1666667F;
			this.txtPayDesc9.HyperLink = null;
			this.txtPayDesc9.Left = 0.0625F;
			this.txtPayDesc9.Name = "txtPayDesc9";
			this.txtPayDesc9.Style = "text-align: left";
			this.txtPayDesc9.Tag = "text";
			this.txtPayDesc9.Text = null;
			this.txtPayDesc9.Top = 2.177083F;
			this.txtPayDesc9.Width = 1F;
			// 
			// txtHours9
			// 
			this.txtHours9.Height = 0.1666667F;
			this.txtHours9.HyperLink = null;
			this.txtHours9.Left = 1.125F;
			this.txtHours9.Name = "txtHours9";
			this.txtHours9.Style = "text-align: right";
			this.txtHours9.Tag = "text";
			this.txtHours9.Text = null;
			this.txtHours9.Top = 2.177083F;
			this.txtHours9.Width = 0.6875F;
			// 
			// txtPayAmount9
			// 
			this.txtPayAmount9.Height = 0.1666667F;
			this.txtPayAmount9.HyperLink = null;
			this.txtPayAmount9.Left = 1.875F;
			this.txtPayAmount9.Name = "txtPayAmount9";
			this.txtPayAmount9.Style = "text-align: right";
			this.txtPayAmount9.Tag = "text";
			this.txtPayAmount9.Text = null;
			this.txtPayAmount9.Top = 2.177083F;
			this.txtPayAmount9.Width = 0.6875F;
			// 
			// txtDedDesc9
			// 
			this.txtDedDesc9.Height = 0.1666667F;
			this.txtDedDesc9.HyperLink = null;
			this.txtDedDesc9.Left = 2.625F;
			this.txtDedDesc9.Name = "txtDedDesc9";
			this.txtDedDesc9.Style = "text-align: left";
			this.txtDedDesc9.Tag = "text";
			this.txtDedDesc9.Text = null;
			this.txtDedDesc9.Top = 2.177083F;
			this.txtDedDesc9.Width = 1.4375F;
			// 
			// txtDedAmount9
			// 
			this.txtDedAmount9.Height = 0.1666667F;
			this.txtDedAmount9.HyperLink = null;
			this.txtDedAmount9.Left = 4.125F;
			this.txtDedAmount9.Name = "txtDedAmount9";
			this.txtDedAmount9.Style = "text-align: right";
			this.txtDedAmount9.Tag = "text";
			this.txtDedAmount9.Text = null;
			this.txtDedAmount9.Top = 2.177083F;
			this.txtDedAmount9.Width = 0.75F;
			// 
			// txtDedYTD9
			// 
			this.txtDedYTD9.Height = 0.1666667F;
			this.txtDedYTD9.HyperLink = null;
			this.txtDedYTD9.Left = 4.875F;
			this.txtDedYTD9.Name = "txtDedYTD9";
			this.txtDedYTD9.Style = "text-align: right";
			this.txtDedYTD9.Tag = "text";
			this.txtDedYTD9.Text = null;
			this.txtDedYTD9.Top = 2.177083F;
			this.txtDedYTD9.Width = 0.6875F;
			// 
			// txtPayDesc10
			// 
			this.txtPayDesc10.Height = 0.1666667F;
			this.txtPayDesc10.HyperLink = null;
			this.txtPayDesc10.Left = 0.0625F;
			this.txtPayDesc10.Name = "txtPayDesc10";
			this.txtPayDesc10.Style = "text-align: left";
			this.txtPayDesc10.Tag = "text";
			this.txtPayDesc10.Text = null;
			this.txtPayDesc10.Top = 2.34375F;
			this.txtPayDesc10.Width = 1F;
			// 
			// txtHours10
			// 
			this.txtHours10.Height = 0.1666667F;
			this.txtHours10.HyperLink = null;
			this.txtHours10.Left = 1.125F;
			this.txtHours10.Name = "txtHours10";
			this.txtHours10.Style = "text-align: right";
			this.txtHours10.Tag = "text";
			this.txtHours10.Text = null;
			this.txtHours10.Top = 2.34375F;
			this.txtHours10.Width = 0.6875F;
			// 
			// txtPayAmount10
			// 
			this.txtPayAmount10.Height = 0.1666667F;
			this.txtPayAmount10.HyperLink = null;
			this.txtPayAmount10.Left = 1.875F;
			this.txtPayAmount10.Name = "txtPayAmount10";
			this.txtPayAmount10.Style = "text-align: right";
			this.txtPayAmount10.Tag = "text";
			this.txtPayAmount10.Text = null;
			this.txtPayAmount10.Top = 2.34375F;
			this.txtPayAmount10.Width = 0.6875F;
			// 
			// txtDedDesc10
			// 
			this.txtDedDesc10.Height = 0.1666667F;
			this.txtDedDesc10.HyperLink = null;
			this.txtDedDesc10.Left = 2.625F;
			this.txtDedDesc10.Name = "txtDedDesc10";
			this.txtDedDesc10.Style = "text-align: left";
			this.txtDedDesc10.Tag = "text";
			this.txtDedDesc10.Text = null;
			this.txtDedDesc10.Top = 2.34375F;
			this.txtDedDesc10.Width = 1.4375F;
			// 
			// txtDedAmount10
			// 
			this.txtDedAmount10.Height = 0.1666667F;
			this.txtDedAmount10.HyperLink = null;
			this.txtDedAmount10.Left = 4.125F;
			this.txtDedAmount10.Name = "txtDedAmount10";
			this.txtDedAmount10.Style = "text-align: right";
			this.txtDedAmount10.Tag = "text";
			this.txtDedAmount10.Text = null;
			this.txtDedAmount10.Top = 2.34375F;
			this.txtDedAmount10.Width = 0.75F;
			// 
			// txtDedYTD10
			// 
			this.txtDedYTD10.Height = 0.1666667F;
			this.txtDedYTD10.HyperLink = null;
			this.txtDedYTD10.Left = 4.875F;
			this.txtDedYTD10.Name = "txtDedYTD10";
			this.txtDedYTD10.Style = "text-align: right";
			this.txtDedYTD10.Tag = "text";
			this.txtDedYTD10.Text = null;
			this.txtDedYTD10.Top = 2.34375F;
			this.txtDedYTD10.Width = 0.6875F;
			// 
			// txtPayDesc11
			// 
			this.txtPayDesc11.Height = 0.1666667F;
			this.txtPayDesc11.HyperLink = null;
			this.txtPayDesc11.Left = 0.0625F;
			this.txtPayDesc11.Name = "txtPayDesc11";
			this.txtPayDesc11.Style = "text-align: left";
			this.txtPayDesc11.Tag = "text";
			this.txtPayDesc11.Text = null;
			this.txtPayDesc11.Top = 2.510417F;
			this.txtPayDesc11.Width = 1F;
			// 
			// txtHours11
			// 
			this.txtHours11.Height = 0.1666667F;
			this.txtHours11.HyperLink = null;
			this.txtHours11.Left = 1.125F;
			this.txtHours11.Name = "txtHours11";
			this.txtHours11.Style = "text-align: right";
			this.txtHours11.Tag = "text";
			this.txtHours11.Text = null;
			this.txtHours11.Top = 2.510417F;
			this.txtHours11.Width = 0.6875F;
			// 
			// txtPayAmount11
			// 
			this.txtPayAmount11.Height = 0.1666667F;
			this.txtPayAmount11.HyperLink = null;
			this.txtPayAmount11.Left = 1.875F;
			this.txtPayAmount11.Name = "txtPayAmount11";
			this.txtPayAmount11.Style = "text-align: right";
			this.txtPayAmount11.Tag = "text";
			this.txtPayAmount11.Text = null;
			this.txtPayAmount11.Top = 2.510417F;
			this.txtPayAmount11.Width = 0.6875F;
			// 
			// txtDedDesc11
			// 
			this.txtDedDesc11.Height = 0.1666667F;
			this.txtDedDesc11.HyperLink = null;
			this.txtDedDesc11.Left = 2.625F;
			this.txtDedDesc11.Name = "txtDedDesc11";
			this.txtDedDesc11.Style = "text-align: left";
			this.txtDedDesc11.Tag = "text";
			this.txtDedDesc11.Text = null;
			this.txtDedDesc11.Top = 2.510417F;
			this.txtDedDesc11.Width = 1.4375F;
			// 
			// txtDedAmount11
			// 
			this.txtDedAmount11.Height = 0.1666667F;
			this.txtDedAmount11.HyperLink = null;
			this.txtDedAmount11.Left = 4.125F;
			this.txtDedAmount11.Name = "txtDedAmount11";
			this.txtDedAmount11.Style = "text-align: right";
			this.txtDedAmount11.Tag = "text";
			this.txtDedAmount11.Text = null;
			this.txtDedAmount11.Top = 2.510417F;
			this.txtDedAmount11.Width = 0.75F;
			// 
			// txtDedYTD11
			// 
			this.txtDedYTD11.Height = 0.1666667F;
			this.txtDedYTD11.HyperLink = null;
			this.txtDedYTD11.Left = 4.875F;
			this.txtDedYTD11.Name = "txtDedYTD11";
			this.txtDedYTD11.Style = "text-align: right";
			this.txtDedYTD11.Tag = "text";
			this.txtDedYTD11.Text = null;
			this.txtDedYTD11.Top = 2.510417F;
			this.txtDedYTD11.Width = 0.6875F;
			// 
			// txtPayDesc12
			// 
			this.txtPayDesc12.Height = 0.1666667F;
			this.txtPayDesc12.HyperLink = null;
			this.txtPayDesc12.Left = 0.0625F;
			this.txtPayDesc12.Name = "txtPayDesc12";
			this.txtPayDesc12.Style = "text-align: left";
			this.txtPayDesc12.Tag = "text";
			this.txtPayDesc12.Text = null;
			this.txtPayDesc12.Top = 2.677083F;
			this.txtPayDesc12.Width = 1F;
			// 
			// txtHours12
			// 
			this.txtHours12.Height = 0.1666667F;
			this.txtHours12.HyperLink = null;
			this.txtHours12.Left = 1.125F;
			this.txtHours12.Name = "txtHours12";
			this.txtHours12.Style = "text-align: right";
			this.txtHours12.Tag = "text";
			this.txtHours12.Text = null;
			this.txtHours12.Top = 2.677083F;
			this.txtHours12.Width = 0.6875F;
			// 
			// txtPayAmount12
			// 
			this.txtPayAmount12.Height = 0.1666667F;
			this.txtPayAmount12.HyperLink = null;
			this.txtPayAmount12.Left = 1.875F;
			this.txtPayAmount12.Name = "txtPayAmount12";
			this.txtPayAmount12.Style = "text-align: right";
			this.txtPayAmount12.Tag = "text";
			this.txtPayAmount12.Text = null;
			this.txtPayAmount12.Top = 2.677083F;
			this.txtPayAmount12.Width = 0.6875F;
			// 
			// txtDedDesc12
			// 
			this.txtDedDesc12.Height = 0.1666667F;
			this.txtDedDesc12.HyperLink = null;
			this.txtDedDesc12.Left = 2.625F;
			this.txtDedDesc12.Name = "txtDedDesc12";
			this.txtDedDesc12.Style = "text-align: left";
			this.txtDedDesc12.Tag = "text";
			this.txtDedDesc12.Text = null;
			this.txtDedDesc12.Top = 2.677083F;
			this.txtDedDesc12.Width = 1.4375F;
			// 
			// txtDedAmount12
			// 
			this.txtDedAmount12.Height = 0.1666667F;
			this.txtDedAmount12.HyperLink = null;
			this.txtDedAmount12.Left = 4.125F;
			this.txtDedAmount12.Name = "txtDedAmount12";
			this.txtDedAmount12.Style = "text-align: right";
			this.txtDedAmount12.Tag = "text";
			this.txtDedAmount12.Text = null;
			this.txtDedAmount12.Top = 2.677083F;
			this.txtDedAmount12.Width = 0.75F;
			// 
			// txtDedYTD12
			// 
			this.txtDedYTD12.Height = 0.1666667F;
			this.txtDedYTD12.HyperLink = null;
			this.txtDedYTD12.Left = 4.875F;
			this.txtDedYTD12.Name = "txtDedYTD12";
			this.txtDedYTD12.Style = "text-align: right";
			this.txtDedYTD12.Tag = "text";
			this.txtDedYTD12.Text = null;
			this.txtDedYTD12.Top = 2.677083F;
			this.txtDedYTD12.Width = 0.6875F;
			// 
			// txtPayDesc13
			// 
			this.txtPayDesc13.Height = 0.1666667F;
			this.txtPayDesc13.HyperLink = null;
			this.txtPayDesc13.Left = 0.0625F;
			this.txtPayDesc13.Name = "txtPayDesc13";
			this.txtPayDesc13.Style = "text-align: left";
			this.txtPayDesc13.Tag = "text";
			this.txtPayDesc13.Text = null;
			this.txtPayDesc13.Top = 2.84375F;
			this.txtPayDesc13.Width = 1F;
			// 
			// txtHours13
			// 
			this.txtHours13.Height = 0.1666667F;
			this.txtHours13.HyperLink = null;
			this.txtHours13.Left = 1.125F;
			this.txtHours13.Name = "txtHours13";
			this.txtHours13.Style = "text-align: right";
			this.txtHours13.Tag = "text";
			this.txtHours13.Text = null;
			this.txtHours13.Top = 2.84375F;
			this.txtHours13.Width = 0.6875F;
			// 
			// txtPayAmount13
			// 
			this.txtPayAmount13.Height = 0.1666667F;
			this.txtPayAmount13.HyperLink = null;
			this.txtPayAmount13.Left = 1.875F;
			this.txtPayAmount13.Name = "txtPayAmount13";
			this.txtPayAmount13.Style = "text-align: right";
			this.txtPayAmount13.Tag = "text";
			this.txtPayAmount13.Text = null;
			this.txtPayAmount13.Top = 2.84375F;
			this.txtPayAmount13.Width = 0.6875F;
			// 
			// txtDedDesc13
			// 
			this.txtDedDesc13.Height = 0.1666667F;
			this.txtDedDesc13.HyperLink = null;
			this.txtDedDesc13.Left = 2.625F;
			this.txtDedDesc13.Name = "txtDedDesc13";
			this.txtDedDesc13.Style = "text-align: left";
			this.txtDedDesc13.Tag = "text";
			this.txtDedDesc13.Text = null;
			this.txtDedDesc13.Top = 2.84375F;
			this.txtDedDesc13.Width = 1.4375F;
			// 
			// txtDedAmount13
			// 
			this.txtDedAmount13.Height = 0.1666667F;
			this.txtDedAmount13.HyperLink = null;
			this.txtDedAmount13.Left = 4.125F;
			this.txtDedAmount13.Name = "txtDedAmount13";
			this.txtDedAmount13.Style = "text-align: right";
			this.txtDedAmount13.Tag = "text";
			this.txtDedAmount13.Text = null;
			this.txtDedAmount13.Top = 2.84375F;
			this.txtDedAmount13.Width = 0.75F;
			// 
			// txtDedYTD13
			// 
			this.txtDedYTD13.Height = 0.1666667F;
			this.txtDedYTD13.HyperLink = null;
			this.txtDedYTD13.Left = 4.875F;
			this.txtDedYTD13.Name = "txtDedYTD13";
			this.txtDedYTD13.Style = "text-align: right";
			this.txtDedYTD13.Tag = "text";
			this.txtDedYTD13.Text = null;
			this.txtDedYTD13.Top = 2.84375F;
			this.txtDedYTD13.Width = 0.6875F;
			// 
			// txtPayDesc14
			// 
			this.txtPayDesc14.Height = 0.1666667F;
			this.txtPayDesc14.HyperLink = null;
			this.txtPayDesc14.Left = 0.0625F;
			this.txtPayDesc14.Name = "txtPayDesc14";
			this.txtPayDesc14.Style = "text-align: left";
			this.txtPayDesc14.Tag = "text";
			this.txtPayDesc14.Text = null;
			this.txtPayDesc14.Top = 3.010417F;
			this.txtPayDesc14.Width = 1F;
			// 
			// txtHours14
			// 
			this.txtHours14.Height = 0.1666667F;
			this.txtHours14.HyperLink = null;
			this.txtHours14.Left = 1.125F;
			this.txtHours14.Name = "txtHours14";
			this.txtHours14.Style = "text-align: right";
			this.txtHours14.Tag = "text";
			this.txtHours14.Text = null;
			this.txtHours14.Top = 3.010417F;
			this.txtHours14.Width = 0.6875F;
			// 
			// txtPayAmount14
			// 
			this.txtPayAmount14.Height = 0.1666667F;
			this.txtPayAmount14.HyperLink = null;
			this.txtPayAmount14.Left = 1.875F;
			this.txtPayAmount14.Name = "txtPayAmount14";
			this.txtPayAmount14.Style = "text-align: right";
			this.txtPayAmount14.Tag = "text";
			this.txtPayAmount14.Text = null;
			this.txtPayAmount14.Top = 3.010417F;
			this.txtPayAmount14.Width = 0.6875F;
			// 
			// txtDedDesc14
			// 
			this.txtDedDesc14.Height = 0.1666667F;
			this.txtDedDesc14.HyperLink = null;
			this.txtDedDesc14.Left = 2.625F;
			this.txtDedDesc14.Name = "txtDedDesc14";
			this.txtDedDesc14.Style = "text-align: left";
			this.txtDedDesc14.Tag = "text";
			this.txtDedDesc14.Text = null;
			this.txtDedDesc14.Top = 3.010417F;
			this.txtDedDesc14.Width = 1.4375F;
			// 
			// txtDedAmount14
			// 
			this.txtDedAmount14.Height = 0.1666667F;
			this.txtDedAmount14.HyperLink = null;
			this.txtDedAmount14.Left = 4.125F;
			this.txtDedAmount14.Name = "txtDedAmount14";
			this.txtDedAmount14.Style = "text-align: right";
			this.txtDedAmount14.Tag = "text";
			this.txtDedAmount14.Text = null;
			this.txtDedAmount14.Top = 3.010417F;
			this.txtDedAmount14.Width = 0.75F;
			// 
			// txtDedYTD14
			// 
			this.txtDedYTD14.Height = 0.1666667F;
			this.txtDedYTD14.HyperLink = null;
			this.txtDedYTD14.Left = 4.875F;
			this.txtDedYTD14.Name = "txtDedYTD14";
			this.txtDedYTD14.Style = "text-align: right";
			this.txtDedYTD14.Tag = "text";
			this.txtDedYTD14.Text = null;
			this.txtDedYTD14.Top = 3.010417F;
			this.txtDedYTD14.Width = 0.6875F;
			// 
			// txtPayDesc15
			// 
			this.txtPayDesc15.Height = 0.1666667F;
			this.txtPayDesc15.HyperLink = null;
			this.txtPayDesc15.Left = 0.0625F;
			this.txtPayDesc15.Name = "txtPayDesc15";
			this.txtPayDesc15.Style = "text-align: left";
			this.txtPayDesc15.Tag = "text";
			this.txtPayDesc15.Text = null;
			this.txtPayDesc15.Top = 3.177083F;
			this.txtPayDesc15.Width = 1F;
			// 
			// txtHours15
			// 
			this.txtHours15.Height = 0.1666667F;
			this.txtHours15.HyperLink = null;
			this.txtHours15.Left = 1.125F;
			this.txtHours15.Name = "txtHours15";
			this.txtHours15.Style = "text-align: right";
			this.txtHours15.Tag = "text";
			this.txtHours15.Text = null;
			this.txtHours15.Top = 3.177083F;
			this.txtHours15.Width = 0.6875F;
			// 
			// txtPayAmount15
			// 
			this.txtPayAmount15.Height = 0.1666667F;
			this.txtPayAmount15.HyperLink = null;
			this.txtPayAmount15.Left = 1.875F;
			this.txtPayAmount15.Name = "txtPayAmount15";
			this.txtPayAmount15.Style = "text-align: right";
			this.txtPayAmount15.Tag = "text";
			this.txtPayAmount15.Text = null;
			this.txtPayAmount15.Top = 3.177083F;
			this.txtPayAmount15.Width = 0.6875F;
			// 
			// txtDedDesc15
			// 
			this.txtDedDesc15.Height = 0.1666667F;
			this.txtDedDesc15.HyperLink = null;
			this.txtDedDesc15.Left = 2.625F;
			this.txtDedDesc15.Name = "txtDedDesc15";
			this.txtDedDesc15.Style = "text-align: left";
			this.txtDedDesc15.Tag = "text";
			this.txtDedDesc15.Text = null;
			this.txtDedDesc15.Top = 3.177083F;
			this.txtDedDesc15.Width = 1.4375F;
			// 
			// txtDedAmount15
			// 
			this.txtDedAmount15.Height = 0.1666667F;
			this.txtDedAmount15.HyperLink = null;
			this.txtDedAmount15.Left = 4.125F;
			this.txtDedAmount15.Name = "txtDedAmount15";
			this.txtDedAmount15.Style = "text-align: right";
			this.txtDedAmount15.Tag = "text";
			this.txtDedAmount15.Text = null;
			this.txtDedAmount15.Top = 3.177083F;
			this.txtDedAmount15.Width = 0.75F;
			// 
			// txtDedYTD15
			// 
			this.txtDedYTD15.Height = 0.1666667F;
			this.txtDedYTD15.HyperLink = null;
			this.txtDedYTD15.Left = 4.875F;
			this.txtDedYTD15.Name = "txtDedYTD15";
			this.txtDedYTD15.Style = "text-align: right";
			this.txtDedYTD15.Tag = "text";
			this.txtDedYTD15.Text = null;
			this.txtDedYTD15.Top = 3.177083F;
			this.txtDedYTD15.Width = 0.6875F;
			// 
			// txtPayDesc16
			// 
			this.txtPayDesc16.Height = 0.1666667F;
			this.txtPayDesc16.HyperLink = null;
			this.txtPayDesc16.Left = 0.0625F;
			this.txtPayDesc16.Name = "txtPayDesc16";
			this.txtPayDesc16.Style = "text-align: left";
			this.txtPayDesc16.Tag = "text";
			this.txtPayDesc16.Text = null;
			this.txtPayDesc16.Top = 3.34375F;
			this.txtPayDesc16.Width = 1F;
			// 
			// txtHours16
			// 
			this.txtHours16.Height = 0.1666667F;
			this.txtHours16.HyperLink = null;
			this.txtHours16.Left = 1.125F;
			this.txtHours16.Name = "txtHours16";
			this.txtHours16.Style = "text-align: right";
			this.txtHours16.Tag = "text";
			this.txtHours16.Text = null;
			this.txtHours16.Top = 3.34375F;
			this.txtHours16.Width = 0.6875F;
			// 
			// txtPayAmount16
			// 
			this.txtPayAmount16.Height = 0.1666667F;
			this.txtPayAmount16.HyperLink = null;
			this.txtPayAmount16.Left = 1.875F;
			this.txtPayAmount16.Name = "txtPayAmount16";
			this.txtPayAmount16.Style = "text-align: right";
			this.txtPayAmount16.Tag = "text";
			this.txtPayAmount16.Text = null;
			this.txtPayAmount16.Top = 3.34375F;
			this.txtPayAmount16.Width = 0.6875F;
			// 
			// txtDedDesc16
			// 
			this.txtDedDesc16.Height = 0.1666667F;
			this.txtDedDesc16.HyperLink = null;
			this.txtDedDesc16.Left = 2.625F;
			this.txtDedDesc16.Name = "txtDedDesc16";
			this.txtDedDesc16.Style = "text-align: left";
			this.txtDedDesc16.Tag = "text";
			this.txtDedDesc16.Text = null;
			this.txtDedDesc16.Top = 3.34375F;
			this.txtDedDesc16.Width = 1.4375F;
			// 
			// txtDedAmount16
			// 
			this.txtDedAmount16.Height = 0.1666667F;
			this.txtDedAmount16.HyperLink = null;
			this.txtDedAmount16.Left = 4.125F;
			this.txtDedAmount16.Name = "txtDedAmount16";
			this.txtDedAmount16.Style = "text-align: right";
			this.txtDedAmount16.Tag = "text";
			this.txtDedAmount16.Text = null;
			this.txtDedAmount16.Top = 3.34375F;
			this.txtDedAmount16.Width = 0.75F;
			// 
			// txtDedYTD16
			// 
			this.txtDedYTD16.Height = 0.1666667F;
			this.txtDedYTD16.HyperLink = null;
			this.txtDedYTD16.Left = 4.875F;
			this.txtDedYTD16.Name = "txtDedYTD16";
			this.txtDedYTD16.Style = "text-align: right";
			this.txtDedYTD16.Tag = "text";
			this.txtDedYTD16.Text = null;
			this.txtDedYTD16.Top = 3.34375F;
			this.txtDedYTD16.Width = 0.6875F;
			// 
			// txtPayDesc17
			// 
			this.txtPayDesc17.Height = 0.1666667F;
			this.txtPayDesc17.HyperLink = null;
			this.txtPayDesc17.Left = 0.0625F;
			this.txtPayDesc17.Name = "txtPayDesc17";
			this.txtPayDesc17.Style = "text-align: left";
			this.txtPayDesc17.Tag = "text";
			this.txtPayDesc17.Text = null;
			this.txtPayDesc17.Top = 3.510417F;
			this.txtPayDesc17.Width = 1F;
			// 
			// txtHours17
			// 
			this.txtHours17.Height = 0.1666667F;
			this.txtHours17.HyperLink = null;
			this.txtHours17.Left = 1.125F;
			this.txtHours17.Name = "txtHours17";
			this.txtHours17.Style = "text-align: right";
			this.txtHours17.Tag = "text";
			this.txtHours17.Text = null;
			this.txtHours17.Top = 3.510417F;
			this.txtHours17.Width = 0.6875F;
			// 
			// txtPayAmount17
			// 
			this.txtPayAmount17.Height = 0.1666667F;
			this.txtPayAmount17.HyperLink = null;
			this.txtPayAmount17.Left = 1.875F;
			this.txtPayAmount17.Name = "txtPayAmount17";
			this.txtPayAmount17.Style = "text-align: right";
			this.txtPayAmount17.Tag = "text";
			this.txtPayAmount17.Text = null;
			this.txtPayAmount17.Top = 3.510417F;
			this.txtPayAmount17.Width = 0.6875F;
			// 
			// txtDedDesc17
			// 
			this.txtDedDesc17.Height = 0.1666667F;
			this.txtDedDesc17.HyperLink = null;
			this.txtDedDesc17.Left = 2.625F;
			this.txtDedDesc17.Name = "txtDedDesc17";
			this.txtDedDesc17.Style = "text-align: left";
			this.txtDedDesc17.Tag = "text";
			this.txtDedDesc17.Text = null;
			this.txtDedDesc17.Top = 3.510417F;
			this.txtDedDesc17.Width = 1.4375F;
			// 
			// txtDedAmount17
			// 
			this.txtDedAmount17.Height = 0.1666667F;
			this.txtDedAmount17.HyperLink = null;
			this.txtDedAmount17.Left = 4.125F;
			this.txtDedAmount17.Name = "txtDedAmount17";
			this.txtDedAmount17.Style = "text-align: right";
			this.txtDedAmount17.Tag = "text";
			this.txtDedAmount17.Text = null;
			this.txtDedAmount17.Top = 3.510417F;
			this.txtDedAmount17.Width = 0.75F;
			// 
			// txtDedYTD17
			// 
			this.txtDedYTD17.Height = 0.1666667F;
			this.txtDedYTD17.HyperLink = null;
			this.txtDedYTD17.Left = 4.875F;
			this.txtDedYTD17.Name = "txtDedYTD17";
			this.txtDedYTD17.Style = "text-align: right";
			this.txtDedYTD17.Tag = "text";
			this.txtDedYTD17.Text = null;
			this.txtDedYTD17.Top = 3.510417F;
			this.txtDedYTD17.Width = 0.6875F;
			// 
			// txtPayDesc18
			// 
			this.txtPayDesc18.Height = 0.1666667F;
			this.txtPayDesc18.HyperLink = null;
			this.txtPayDesc18.Left = 0.0625F;
			this.txtPayDesc18.Name = "txtPayDesc18";
			this.txtPayDesc18.Style = "text-align: left";
			this.txtPayDesc18.Tag = "text";
			this.txtPayDesc18.Text = null;
			this.txtPayDesc18.Top = 3.677083F;
			this.txtPayDesc18.Width = 1F;
			// 
			// txtHours18
			// 
			this.txtHours18.Height = 0.1666667F;
			this.txtHours18.HyperLink = null;
			this.txtHours18.Left = 1.125F;
			this.txtHours18.Name = "txtHours18";
			this.txtHours18.Style = "text-align: right";
			this.txtHours18.Tag = "text";
			this.txtHours18.Text = null;
			this.txtHours18.Top = 3.677083F;
			this.txtHours18.Width = 0.6875F;
			// 
			// txtPayAmount18
			// 
			this.txtPayAmount18.Height = 0.1666667F;
			this.txtPayAmount18.HyperLink = null;
			this.txtPayAmount18.Left = 1.875F;
			this.txtPayAmount18.Name = "txtPayAmount18";
			this.txtPayAmount18.Style = "text-align: right";
			this.txtPayAmount18.Tag = "text";
			this.txtPayAmount18.Text = null;
			this.txtPayAmount18.Top = 3.677083F;
			this.txtPayAmount18.Width = 0.6875F;
			// 
			// txtDedDesc18
			// 
			this.txtDedDesc18.Height = 0.1666667F;
			this.txtDedDesc18.HyperLink = null;
			this.txtDedDesc18.Left = 2.625F;
			this.txtDedDesc18.Name = "txtDedDesc18";
			this.txtDedDesc18.Style = "text-align: left";
			this.txtDedDesc18.Tag = "text";
			this.txtDedDesc18.Text = null;
			this.txtDedDesc18.Top = 3.677083F;
			this.txtDedDesc18.Width = 1.4375F;
			// 
			// txtDedAmount18
			// 
			this.txtDedAmount18.Height = 0.1666667F;
			this.txtDedAmount18.HyperLink = null;
			this.txtDedAmount18.Left = 4.125F;
			this.txtDedAmount18.Name = "txtDedAmount18";
			this.txtDedAmount18.Style = "text-align: right";
			this.txtDedAmount18.Tag = "text";
			this.txtDedAmount18.Text = null;
			this.txtDedAmount18.Top = 3.677083F;
			this.txtDedAmount18.Width = 0.75F;
			// 
			// txtDedYTD18
			// 
			this.txtDedYTD18.Height = 0.1666667F;
			this.txtDedYTD18.HyperLink = null;
			this.txtDedYTD18.Left = 4.875F;
			this.txtDedYTD18.Name = "txtDedYTD18";
			this.txtDedYTD18.Style = "text-align: right";
			this.txtDedYTD18.Tag = "text";
			this.txtDedYTD18.Text = null;
			this.txtDedYTD18.Top = 3.677083F;
			this.txtDedYTD18.Width = 0.6875F;
			// 
			// txtPayDesc19
			// 
			this.txtPayDesc19.Height = 0.1666667F;
			this.txtPayDesc19.HyperLink = null;
			this.txtPayDesc19.Left = 0.0625F;
			this.txtPayDesc19.Name = "txtPayDesc19";
			this.txtPayDesc19.Style = "text-align: left";
			this.txtPayDesc19.Tag = "text";
			this.txtPayDesc19.Text = null;
			this.txtPayDesc19.Top = 3.84375F;
			this.txtPayDesc19.Width = 1F;
			// 
			// txtHours19
			// 
			this.txtHours19.Height = 0.1666667F;
			this.txtHours19.HyperLink = null;
			this.txtHours19.Left = 1.125F;
			this.txtHours19.Name = "txtHours19";
			this.txtHours19.Style = "text-align: right";
			this.txtHours19.Tag = "text";
			this.txtHours19.Text = null;
			this.txtHours19.Top = 3.84375F;
			this.txtHours19.Width = 0.6875F;
			// 
			// txtPayAmount19
			// 
			this.txtPayAmount19.Height = 0.1666667F;
			this.txtPayAmount19.HyperLink = null;
			this.txtPayAmount19.Left = 1.875F;
			this.txtPayAmount19.Name = "txtPayAmount19";
			this.txtPayAmount19.Style = "text-align: right";
			this.txtPayAmount19.Tag = "text";
			this.txtPayAmount19.Text = null;
			this.txtPayAmount19.Top = 3.84375F;
			this.txtPayAmount19.Width = 0.6875F;
			// 
			// txtDedDesc19
			// 
			this.txtDedDesc19.Height = 0.1666667F;
			this.txtDedDesc19.HyperLink = null;
			this.txtDedDesc19.Left = 2.625F;
			this.txtDedDesc19.Name = "txtDedDesc19";
			this.txtDedDesc19.Style = "text-align: left";
			this.txtDedDesc19.Tag = "text";
			this.txtDedDesc19.Text = null;
			this.txtDedDesc19.Top = 3.84375F;
			this.txtDedDesc19.Width = 1.4375F;
			// 
			// txtDedAmount19
			// 
			this.txtDedAmount19.Height = 0.1666667F;
			this.txtDedAmount19.HyperLink = null;
			this.txtDedAmount19.Left = 4.125F;
			this.txtDedAmount19.Name = "txtDedAmount19";
			this.txtDedAmount19.Style = "text-align: right";
			this.txtDedAmount19.Tag = "text";
			this.txtDedAmount19.Text = null;
			this.txtDedAmount19.Top = 3.84375F;
			this.txtDedAmount19.Width = 0.75F;
			// 
			// txtDedYTD19
			// 
			this.txtDedYTD19.Height = 0.1666667F;
			this.txtDedYTD19.HyperLink = null;
			this.txtDedYTD19.Left = 4.875F;
			this.txtDedYTD19.Name = "txtDedYTD19";
			this.txtDedYTD19.Style = "text-align: right";
			this.txtDedYTD19.Tag = "text";
			this.txtDedYTD19.Text = null;
			this.txtDedYTD19.Top = 3.84375F;
			this.txtDedYTD19.Width = 0.6875F;
			// 
			// txtPayDesc20
			// 
			this.txtPayDesc20.Height = 0.1666667F;
			this.txtPayDesc20.HyperLink = null;
			this.txtPayDesc20.Left = 0.0625F;
			this.txtPayDesc20.Name = "txtPayDesc20";
			this.txtPayDesc20.Style = "text-align: left";
			this.txtPayDesc20.Tag = "text";
			this.txtPayDesc20.Text = null;
			this.txtPayDesc20.Top = 4.010417F;
			this.txtPayDesc20.Width = 1F;
			// 
			// txtHours20
			// 
			this.txtHours20.Height = 0.1666667F;
			this.txtHours20.HyperLink = null;
			this.txtHours20.Left = 1.125F;
			this.txtHours20.Name = "txtHours20";
			this.txtHours20.Style = "text-align: right";
			this.txtHours20.Tag = "text";
			this.txtHours20.Text = null;
			this.txtHours20.Top = 4.010417F;
			this.txtHours20.Width = 0.6875F;
			// 
			// txtPayAmount20
			// 
			this.txtPayAmount20.Height = 0.1666667F;
			this.txtPayAmount20.HyperLink = null;
			this.txtPayAmount20.Left = 1.875F;
			this.txtPayAmount20.Name = "txtPayAmount20";
			this.txtPayAmount20.Style = "text-align: right";
			this.txtPayAmount20.Tag = "text";
			this.txtPayAmount20.Text = null;
			this.txtPayAmount20.Top = 4.010417F;
			this.txtPayAmount20.Width = 0.6875F;
			// 
			// txtDedDesc20
			// 
			this.txtDedDesc20.Height = 0.1666667F;
			this.txtDedDesc20.HyperLink = null;
			this.txtDedDesc20.Left = 2.625F;
			this.txtDedDesc20.Name = "txtDedDesc20";
			this.txtDedDesc20.Style = "text-align: left";
			this.txtDedDesc20.Tag = "text";
			this.txtDedDesc20.Text = null;
			this.txtDedDesc20.Top = 4.010417F;
			this.txtDedDesc20.Width = 1.4375F;
			// 
			// txtDedAmount20
			// 
			this.txtDedAmount20.Height = 0.1666667F;
			this.txtDedAmount20.HyperLink = null;
			this.txtDedAmount20.Left = 4.125F;
			this.txtDedAmount20.Name = "txtDedAmount20";
			this.txtDedAmount20.Style = "text-align: right";
			this.txtDedAmount20.Tag = "text";
			this.txtDedAmount20.Text = null;
			this.txtDedAmount20.Top = 4.010417F;
			this.txtDedAmount20.Width = 0.75F;
			// 
			// txtDedYTD20
			// 
			this.txtDedYTD20.Height = 0.1666667F;
			this.txtDedYTD20.HyperLink = null;
			this.txtDedYTD20.Left = 4.875F;
			this.txtDedYTD20.Name = "txtDedYTD20";
			this.txtDedYTD20.Style = "text-align: right";
			this.txtDedYTD20.Tag = "text";
			this.txtDedYTD20.Text = null;
			this.txtDedYTD20.Top = 4.010417F;
			this.txtDedYTD20.Width = 0.6875F;
			// 
			// txtPayDesc21
			// 
			this.txtPayDesc21.Height = 0.1666667F;
			this.txtPayDesc21.HyperLink = null;
			this.txtPayDesc21.Left = 0.0625F;
			this.txtPayDesc21.Name = "txtPayDesc21";
			this.txtPayDesc21.Style = "text-align: left";
			this.txtPayDesc21.Tag = "text";
			this.txtPayDesc21.Text = null;
			this.txtPayDesc21.Top = 4.177083F;
			this.txtPayDesc21.Width = 1F;
			// 
			// txtHours21
			// 
			this.txtHours21.Height = 0.1666667F;
			this.txtHours21.HyperLink = null;
			this.txtHours21.Left = 1.125F;
			this.txtHours21.Name = "txtHours21";
			this.txtHours21.Style = "text-align: right";
			this.txtHours21.Tag = "text";
			this.txtHours21.Text = null;
			this.txtHours21.Top = 4.177083F;
			this.txtHours21.Width = 0.6875F;
			// 
			// txtPayAmount21
			// 
			this.txtPayAmount21.Height = 0.1666667F;
			this.txtPayAmount21.HyperLink = null;
			this.txtPayAmount21.Left = 1.875F;
			this.txtPayAmount21.Name = "txtPayAmount21";
			this.txtPayAmount21.Style = "text-align: right";
			this.txtPayAmount21.Tag = "text";
			this.txtPayAmount21.Text = null;
			this.txtPayAmount21.Top = 4.177083F;
			this.txtPayAmount21.Width = 0.6875F;
			// 
			// txtDedDesc21
			// 
			this.txtDedDesc21.Height = 0.1666667F;
			this.txtDedDesc21.HyperLink = null;
			this.txtDedDesc21.Left = 2.625F;
			this.txtDedDesc21.Name = "txtDedDesc21";
			this.txtDedDesc21.Style = "text-align: left";
			this.txtDedDesc21.Tag = "text";
			this.txtDedDesc21.Text = null;
			this.txtDedDesc21.Top = 4.177083F;
			this.txtDedDesc21.Width = 1.4375F;
			// 
			// txtDedAmount21
			// 
			this.txtDedAmount21.Height = 0.1666667F;
			this.txtDedAmount21.HyperLink = null;
			this.txtDedAmount21.Left = 4.125F;
			this.txtDedAmount21.Name = "txtDedAmount21";
			this.txtDedAmount21.Style = "text-align: right";
			this.txtDedAmount21.Tag = "text";
			this.txtDedAmount21.Text = null;
			this.txtDedAmount21.Top = 4.177083F;
			this.txtDedAmount21.Width = 0.75F;
			// 
			// txtDedYTD21
			// 
			this.txtDedYTD21.Height = 0.1666667F;
			this.txtDedYTD21.HyperLink = null;
			this.txtDedYTD21.Left = 4.875F;
			this.txtDedYTD21.Name = "txtDedYTD21";
			this.txtDedYTD21.Style = "text-align: right";
			this.txtDedYTD21.Tag = "text";
			this.txtDedYTD21.Text = null;
			this.txtDedYTD21.Top = 4.177083F;
			this.txtDedYTD21.Width = 0.6875F;
			// 
			// txtPayDesc22
			// 
			this.txtPayDesc22.Height = 0.1666667F;
			this.txtPayDesc22.HyperLink = null;
			this.txtPayDesc22.Left = 0.0625F;
			this.txtPayDesc22.Name = "txtPayDesc22";
			this.txtPayDesc22.Style = "text-align: left";
			this.txtPayDesc22.Tag = "text";
			this.txtPayDesc22.Text = null;
			this.txtPayDesc22.Top = 4.34375F;
			this.txtPayDesc22.Width = 1F;
			// 
			// txtHours22
			// 
			this.txtHours22.Height = 0.1666667F;
			this.txtHours22.HyperLink = null;
			this.txtHours22.Left = 1.125F;
			this.txtHours22.Name = "txtHours22";
			this.txtHours22.Style = "text-align: right";
			this.txtHours22.Tag = "text";
			this.txtHours22.Text = null;
			this.txtHours22.Top = 4.34375F;
			this.txtHours22.Width = 0.6875F;
			// 
			// txtPayAmount22
			// 
			this.txtPayAmount22.Height = 0.1666667F;
			this.txtPayAmount22.HyperLink = null;
			this.txtPayAmount22.Left = 1.875F;
			this.txtPayAmount22.Name = "txtPayAmount22";
			this.txtPayAmount22.Style = "text-align: right";
			this.txtPayAmount22.Tag = "text";
			this.txtPayAmount22.Text = null;
			this.txtPayAmount22.Top = 4.34375F;
			this.txtPayAmount22.Width = 0.6875F;
			// 
			// txtDedDesc22
			// 
			this.txtDedDesc22.Height = 0.1666667F;
			this.txtDedDesc22.HyperLink = null;
			this.txtDedDesc22.Left = 2.625F;
			this.txtDedDesc22.Name = "txtDedDesc22";
			this.txtDedDesc22.Style = "text-align: left";
			this.txtDedDesc22.Tag = "text";
			this.txtDedDesc22.Text = null;
			this.txtDedDesc22.Top = 4.34375F;
			this.txtDedDesc22.Width = 1.4375F;
			// 
			// txtDedAmount22
			// 
			this.txtDedAmount22.Height = 0.1666667F;
			this.txtDedAmount22.HyperLink = null;
			this.txtDedAmount22.Left = 4.125F;
			this.txtDedAmount22.Name = "txtDedAmount22";
			this.txtDedAmount22.Style = "text-align: right";
			this.txtDedAmount22.Tag = "text";
			this.txtDedAmount22.Text = null;
			this.txtDedAmount22.Top = 4.34375F;
			this.txtDedAmount22.Width = 0.75F;
			// 
			// txtDedYTD22
			// 
			this.txtDedYTD22.Height = 0.1666667F;
			this.txtDedYTD22.HyperLink = null;
			this.txtDedYTD22.Left = 4.875F;
			this.txtDedYTD22.Name = "txtDedYTD22";
			this.txtDedYTD22.Style = "text-align: right";
			this.txtDedYTD22.Tag = "text";
			this.txtDedYTD22.Text = null;
			this.txtDedYTD22.Top = 4.34375F;
			this.txtDedYTD22.Width = 0.6875F;
			// 
			// txtPayDesc23
			// 
			this.txtPayDesc23.Height = 0.1666667F;
			this.txtPayDesc23.HyperLink = null;
			this.txtPayDesc23.Left = 0.0625F;
			this.txtPayDesc23.Name = "txtPayDesc23";
			this.txtPayDesc23.Style = "text-align: left";
			this.txtPayDesc23.Tag = "text";
			this.txtPayDesc23.Text = null;
			this.txtPayDesc23.Top = 4.5F;
			this.txtPayDesc23.Width = 1F;
			// 
			// txtHours23
			// 
			this.txtHours23.Height = 0.1666667F;
			this.txtHours23.HyperLink = null;
			this.txtHours23.Left = 1.125F;
			this.txtHours23.Name = "txtHours23";
			this.txtHours23.Style = "text-align: right";
			this.txtHours23.Tag = "text";
			this.txtHours23.Text = null;
			this.txtHours23.Top = 4.5F;
			this.txtHours23.Width = 0.6875F;
			// 
			// txtPayAmount23
			// 
			this.txtPayAmount23.Height = 0.1666667F;
			this.txtPayAmount23.HyperLink = null;
			this.txtPayAmount23.Left = 1.875F;
			this.txtPayAmount23.Name = "txtPayAmount23";
			this.txtPayAmount23.Style = "text-align: right";
			this.txtPayAmount23.Tag = "text";
			this.txtPayAmount23.Text = null;
			this.txtPayAmount23.Top = 4.5F;
			this.txtPayAmount23.Width = 0.6875F;
			// 
			// txtDedDesc23
			// 
			this.txtDedDesc23.Height = 0.1666667F;
			this.txtDedDesc23.HyperLink = null;
			this.txtDedDesc23.Left = 2.625F;
			this.txtDedDesc23.Name = "txtDedDesc23";
			this.txtDedDesc23.Style = "text-align: left";
			this.txtDedDesc23.Tag = "text";
			this.txtDedDesc23.Text = null;
			this.txtDedDesc23.Top = 4.5F;
			this.txtDedDesc23.Width = 1.4375F;
			// 
			// txtDedAmount23
			// 
			this.txtDedAmount23.Height = 0.1666667F;
			this.txtDedAmount23.HyperLink = null;
			this.txtDedAmount23.Left = 4.125F;
			this.txtDedAmount23.Name = "txtDedAmount23";
			this.txtDedAmount23.Style = "text-align: right";
			this.txtDedAmount23.Tag = "text";
			this.txtDedAmount23.Text = null;
			this.txtDedAmount23.Top = 4.5F;
			this.txtDedAmount23.Width = 0.75F;
			// 
			// txtDedYTD23
			// 
			this.txtDedYTD23.Height = 0.1666667F;
			this.txtDedYTD23.HyperLink = null;
			this.txtDedYTD23.Left = 4.875F;
			this.txtDedYTD23.Name = "txtDedYTD23";
			this.txtDedYTD23.Style = "text-align: right";
			this.txtDedYTD23.Tag = "text";
			this.txtDedYTD23.Text = null;
			this.txtDedYTD23.Top = 4.5F;
			this.txtDedYTD23.Width = 0.6875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1666667F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "text-align: left";
			this.Label30.Tag = "text";
			this.Label30.Text = "DEDUCTIONS";
			this.Label30.Top = 1.520833F;
			this.Label30.Width = 0.9479167F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1666667F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 7.635417F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "text-align: right";
			this.Label31.Tag = "text";
			this.Label31.Text = "YTD";
			this.Label31.Top = 0.6875F;
			this.Label31.Width = 0.75F;
			// 
			// lblDepositAmount1
			// 
			this.lblDepositAmount1.Height = 0.1666667F;
			this.lblDepositAmount1.HyperLink = null;
			this.lblDepositAmount1.Left = 3.364583F;
			this.lblDepositAmount1.Name = "lblDepositAmount1";
			this.lblDepositAmount1.Style = "text-align: right";
			this.lblDepositAmount1.Tag = "text";
			this.lblDepositAmount1.Text = "0.00";
			this.lblDepositAmount1.Top = 5.125F;
			this.lblDepositAmount1.Visible = false;
			this.lblDepositAmount1.Width = 0.75F;
			// 
			// lblDepositAmount3
			// 
			this.lblDepositAmount3.Height = 0.1666667F;
			this.lblDepositAmount3.HyperLink = null;
			this.lblDepositAmount3.Left = 3.364583F;
			this.lblDepositAmount3.Name = "lblDepositAmount3";
			this.lblDepositAmount3.Style = "text-align: right";
			this.lblDepositAmount3.Tag = "text";
			this.lblDepositAmount3.Text = "0.00";
			this.lblDepositAmount3.Top = 5.458333F;
			this.lblDepositAmount3.Visible = false;
			this.lblDepositAmount3.Width = 0.75F;
			// 
			// lblDeposit3
			// 
			this.lblDeposit3.Height = 0.1666667F;
			this.lblDeposit3.HyperLink = null;
			this.lblDeposit3.Left = 2F;
			this.lblDeposit3.Name = "lblDeposit3";
			this.lblDeposit3.Style = "text-align: left";
			this.lblDeposit3.Tag = "text";
			this.lblDeposit3.Text = "Deposit #3";
			this.lblDeposit3.Top = 5.458333F;
			this.lblDeposit3.Visible = false;
			this.lblDeposit3.Width = 1.375F;
			// 
			// lblDeposit1
			// 
			this.lblDeposit1.Height = 0.1666667F;
			this.lblDeposit1.HyperLink = null;
			this.lblDeposit1.Left = 2F;
			this.lblDeposit1.Name = "lblDeposit1";
			this.lblDeposit1.Style = "text-align: left";
			this.lblDeposit1.Tag = "text";
			this.lblDeposit1.Text = "Deposit #1";
			this.lblDeposit1.Top = 5.125F;
			this.lblDeposit1.Visible = false;
			this.lblDeposit1.Width = 1.375F;
			// 
			// lblDepositAmount2
			// 
			this.lblDepositAmount2.Height = 0.1666667F;
			this.lblDepositAmount2.HyperLink = null;
			this.lblDepositAmount2.Left = 3.364583F;
			this.lblDepositAmount2.Name = "lblDepositAmount2";
			this.lblDepositAmount2.Style = "text-align: right";
			this.lblDepositAmount2.Tag = "text";
			this.lblDepositAmount2.Text = "0.00";
			this.lblDepositAmount2.Top = 5.291667F;
			this.lblDepositAmount2.Visible = false;
			this.lblDepositAmount2.Width = 0.75F;
			// 
			// lblDeposit2
			// 
			this.lblDeposit2.Height = 0.1666667F;
			this.lblDeposit2.HyperLink = null;
			this.lblDeposit2.Left = 2F;
			this.lblDeposit2.Name = "lblDeposit2";
			this.lblDeposit2.Style = "text-align: left";
			this.lblDeposit2.Tag = "text";
			this.lblDeposit2.Text = "Deposit #2";
			this.lblDeposit2.Top = 5.291667F;
			this.lblDeposit2.Visible = false;
			this.lblDeposit2.Width = 1.375F;
			// 
			// lblDepositAmount4
			// 
			this.lblDepositAmount4.Height = 0.1666667F;
			this.lblDepositAmount4.HyperLink = null;
			this.lblDepositAmount4.Left = 5.552083F;
			this.lblDepositAmount4.Name = "lblDepositAmount4";
			this.lblDepositAmount4.Style = "text-align: right";
			this.lblDepositAmount4.Tag = "text";
			this.lblDepositAmount4.Text = "0.00";
			this.lblDepositAmount4.Top = 5.125F;
			this.lblDepositAmount4.Visible = false;
			this.lblDepositAmount4.Width = 0.75F;
			// 
			// lblDepositAmount6
			// 
			this.lblDepositAmount6.Height = 0.1666667F;
			this.lblDepositAmount6.HyperLink = null;
			this.lblDepositAmount6.Left = 5.552083F;
			this.lblDepositAmount6.Name = "lblDepositAmount6";
			this.lblDepositAmount6.Style = "text-align: right";
			this.lblDepositAmount6.Tag = "text";
			this.lblDepositAmount6.Text = "0.00";
			this.lblDepositAmount6.Top = 5.458333F;
			this.lblDepositAmount6.Visible = false;
			this.lblDepositAmount6.Width = 0.75F;
			// 
			// lblDeposit6
			// 
			this.lblDeposit6.Height = 0.1666667F;
			this.lblDeposit6.HyperLink = null;
			this.lblDeposit6.Left = 4.1875F;
			this.lblDeposit6.Name = "lblDeposit6";
			this.lblDeposit6.Style = "text-align: left";
			this.lblDeposit6.Tag = "text";
			this.lblDeposit6.Text = "Deposit #6";
			this.lblDeposit6.Top = 5.458333F;
			this.lblDeposit6.Visible = false;
			this.lblDeposit6.Width = 1.375F;
			// 
			// lblDeposit4
			// 
			this.lblDeposit4.Height = 0.1666667F;
			this.lblDeposit4.HyperLink = null;
			this.lblDeposit4.Left = 4.1875F;
			this.lblDeposit4.Name = "lblDeposit4";
			this.lblDeposit4.Style = "text-align: left";
			this.lblDeposit4.Tag = "text";
			this.lblDeposit4.Text = "Deposit #4";
			this.lblDeposit4.Top = 5.125F;
			this.lblDeposit4.Visible = false;
			this.lblDeposit4.Width = 1.375F;
			// 
			// lblDepositAmount5
			// 
			this.lblDepositAmount5.Height = 0.1666667F;
			this.lblDepositAmount5.HyperLink = null;
			this.lblDepositAmount5.Left = 5.552083F;
			this.lblDepositAmount5.Name = "lblDepositAmount5";
			this.lblDepositAmount5.Style = "text-align: right";
			this.lblDepositAmount5.Tag = "text";
			this.lblDepositAmount5.Text = "0.00";
			this.lblDepositAmount5.Top = 5.291667F;
			this.lblDepositAmount5.Visible = false;
			this.lblDepositAmount5.Width = 0.75F;
			// 
			// lblDeposit5
			// 
			this.lblDeposit5.Height = 0.1666667F;
			this.lblDeposit5.HyperLink = null;
			this.lblDeposit5.Left = 4.1875F;
			this.lblDeposit5.Name = "lblDeposit5";
			this.lblDeposit5.Style = "text-align: left";
			this.lblDeposit5.Tag = "text";
			this.lblDeposit5.Text = "Deposit #5";
			this.lblDeposit5.Top = 5.291667F;
			this.lblDeposit5.Visible = false;
			this.lblDeposit5.Width = 1.375F;
			// 
			// lblDepositAmount7
			// 
			this.lblDepositAmount7.Height = 0.1666667F;
			this.lblDepositAmount7.HyperLink = null;
			this.lblDepositAmount7.Left = 7.677083F;
			this.lblDepositAmount7.Name = "lblDepositAmount7";
			this.lblDepositAmount7.Style = "text-align: right";
			this.lblDepositAmount7.Tag = "text";
			this.lblDepositAmount7.Text = "0.00";
			this.lblDepositAmount7.Top = 5.125F;
			this.lblDepositAmount7.Visible = false;
			this.lblDepositAmount7.Width = 0.75F;
			// 
			// lblDepositAmount9
			// 
			this.lblDepositAmount9.Height = 0.1666667F;
			this.lblDepositAmount9.HyperLink = null;
			this.lblDepositAmount9.Left = 7.677083F;
			this.lblDepositAmount9.Name = "lblDepositAmount9";
			this.lblDepositAmount9.Style = "text-align: right";
			this.lblDepositAmount9.Tag = "text";
			this.lblDepositAmount9.Text = "0.00";
			this.lblDepositAmount9.Top = 5.458333F;
			this.lblDepositAmount9.Visible = false;
			this.lblDepositAmount9.Width = 0.75F;
			// 
			// lblDeposit9
			// 
			this.lblDeposit9.Height = 0.1666667F;
			this.lblDeposit9.HyperLink = null;
			this.lblDeposit9.Left = 6.3125F;
			this.lblDeposit9.Name = "lblDeposit9";
			this.lblDeposit9.Style = "text-align: left";
			this.lblDeposit9.Tag = "text";
			this.lblDeposit9.Text = "Deposit #9";
			this.lblDeposit9.Top = 5.458333F;
			this.lblDeposit9.Visible = false;
			this.lblDeposit9.Width = 1.375F;
			// 
			// lblDeposit7
			// 
			this.lblDeposit7.Height = 0.1666667F;
			this.lblDeposit7.HyperLink = null;
			this.lblDeposit7.Left = 6.3125F;
			this.lblDeposit7.Name = "lblDeposit7";
			this.lblDeposit7.Style = "text-align: left";
			this.lblDeposit7.Tag = "text";
			this.lblDeposit7.Text = "Deposit #7";
			this.lblDeposit7.Top = 5.125F;
			this.lblDeposit7.Visible = false;
			this.lblDeposit7.Width = 1.375F;
			// 
			// lblDepositAmount8
			// 
			this.lblDepositAmount8.Height = 0.1666667F;
			this.lblDepositAmount8.HyperLink = null;
			this.lblDepositAmount8.Left = 7.677083F;
			this.lblDepositAmount8.Name = "lblDepositAmount8";
			this.lblDepositAmount8.Style = "text-align: right";
			this.lblDepositAmount8.Tag = "text";
			this.lblDepositAmount8.Text = "0.00";
			this.lblDepositAmount8.Top = 5.291667F;
			this.lblDepositAmount8.Visible = false;
			this.lblDepositAmount8.Width = 0.75F;
			// 
			// lblDeposit8
			// 
			this.lblDeposit8.Height = 0.1666667F;
			this.lblDeposit8.HyperLink = null;
			this.lblDeposit8.Left = 6.3125F;
			this.lblDeposit8.Name = "lblDeposit8";
			this.lblDeposit8.Style = "text-align: left";
			this.lblDeposit8.Tag = "text";
			this.lblDeposit8.Text = "Deposit #8";
			this.lblDeposit8.Top = 5.291667F;
			this.lblDeposit8.Visible = false;
			this.lblDeposit8.Width = 1.375F;
			// 
			// txtYTDDeds
			// 
			this.txtYTDDeds.Height = 0.1666667F;
			this.txtYTDDeds.HyperLink = null;
			this.txtYTDDeds.Left = 7.572917F;
			this.txtYTDDeds.Name = "txtYTDDeds";
			this.txtYTDDeds.Style = "text-align: right";
			this.txtYTDDeds.Tag = "text";
			this.txtYTDDeds.Text = "0.00";
			this.txtYTDDeds.Top = 1.520833F;
			this.txtYTDDeds.Width = 0.8125F;
			// 
			// lblDeptDiv
			// 
			this.lblDeptDiv.Height = 0.1666667F;
			this.lblDeptDiv.HyperLink = null;
			this.lblDeptDiv.Left = 3.1875F;
			this.lblDeptDiv.Name = "lblDeptDiv";
			this.lblDeptDiv.Style = "text-align: left";
			this.lblDeptDiv.Tag = "text";
			this.lblDeptDiv.Text = "Department-Division:";
			this.lblDeptDiv.Top = 0.3958333F;
			this.lblDeptDiv.Width = 1.375F;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.Height = 0.1666667F;
			this.txtDeptDiv.HyperLink = null;
			this.txtDeptDiv.Left = 4.625F;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Style = "text-align: left";
			this.txtDeptDiv.Tag = "text";
			this.txtDeptDiv.Text = null;
			this.txtDeptDiv.Top = 0.3958333F;
			this.txtDeptDiv.Width = 1.8125F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Visible = false;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptESHPExport
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepositAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeposit8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDDeds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFICA;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEMatch;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEMatchCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmatchYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtOtherBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepChkLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepSav;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositSavLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepositAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeposit8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDDeds;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl Grid;
		//private GrapeCity.ActiveReports.SectionReportModel.ARControl GridMisc;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
