﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmScheduleWeeks : BaseForm
	{
		public frmScheduleWeeks()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Grid = new System.Collections.Generic.List<FCGrid>();
			this.Grid.AddControlArrayElement(Grid_0, 0);
			this.Grid.AddControlArrayElement(Grid_1, 1);
			this.Grid.AddControlArrayElement(Grid_2, 2);
			this.Grid.AddControlArrayElement(Grid_3, 3);
			this.Grid.AddControlArrayElement(Grid_4, 4);
			this.Grid.AddControlArrayElement(Grid_5, 5);
			this.Grid.AddControlArrayElement(Grid_6, 6);
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmScheduleWeeks InstancePtr
		{
			get
			{
				return (frmScheduleWeeks)Sys.GetInstance(typeof(frmScheduleWeeks));
			}
		}

		protected frmScheduleWeeks _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDSCHEDULECOLID = 0;
		const int CNSTGRIDSCHEDULECOLNAME = 1;
		const int CNSTGRIDSCHEDULECOLOPTION = 2;
		const int CNSTGRIDSCHEDULECOLINDEX = 3;
		const int cnstgridcolID = 0;
		const int CNSTGRIDCOLSHIFTTYPE = 1;
		const int CNSTGRIDCOLSTART = 2;
		const int CNSTGRIDCOLEND = 3;
		const int CNSTGRIDCOLINDEX = 4;
		const int CNSTGRIDCOLLUNCH = 5;
		const int CNSTGRIDCOLACCOUNT = 6;
		const int CNSTGridWeeksCOLMOVE = 0;
		const int CNSTGridWeeksCOLNAME = 1;
		const int CNSTGridWeeksCOLINDEX = 2;
		const int CNSTGridWeeksCOLOVERTIME = 3;
		const int CNSTGridWeeksCOLLIMIT = 4;
		const int CNSTGridWeeksCOLORDER = 5;
		private bool boolLoading;
		private string strCurrentWeekName = "";
		private clsScheduleList ScheduleList = new clsScheduleList();
		private int lngRegularShiftID;
		clsShiftTypeList ShiftList = new clsShiftTypeList();

		public void Init()
		{
			this.Show(App.MainForm);
		}

		private void SetupGridSchedule()
		{
			GridSchedule.Cols = 4;
			GridSchedule.Rows = 1;
			GridSchedule.TextMatrix(0, CNSTGRIDSCHEDULECOLNAME, "Schedule");
			GridSchedule.TextMatrix(0, CNSTGRIDSCHEDULECOLOPTION, "Hours Paid To");
			string strTemp = "";
			strTemp = "#0;Day shift starts|#1;Day worked";
			GridSchedule.ColComboList(CNSTGRIDSCHEDULECOLOPTION, strTemp);
			GridSchedule.ColHidden(CNSTGRIDSCHEDULECOLID, true);
			GridSchedule.ColHidden(CNSTGRIDSCHEDULECOLINDEX, true);
		}

		private void ResizeGridSchedule()
		{
			int GridWidth = 0;
			GridWidth = GridSchedule.WidthOriginal;
			GridSchedule.ColWidth(CNSTGRIDSCHEDULECOLNAME, FCConvert.ToInt32(0.6 * GridWidth));
		}

		private void Grid_ComboCloseUp(int Index, object sender, EventArgs e)
		{
			int lngRow;
			int lngCol;
			int lngID = 0;
			lngRow = Grid[Index].Row;
			lngCol = Grid[Index].Col;
			if (lngCol == CNSTGRIDCOLSHIFTTYPE)
			{
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid[Index].ComboData())));
				if (lngID > 0)
				{
					clsShiftType tShift;
					tShift = ShiftList.GetShiftTypeByType(lngID);
					if (!(tShift == null))
					{
						Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLLUNCH, FCConvert.ToString(tShift.LunchTime));
						Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, tShift.Account);
					}
				}
			}
		}

		private void Grid_ComboCloseUp(object sender, EventArgs e)
		{
			int index = Grid.GetIndex((FCGrid)((sender as FCListViewComboBox).Parent));
			Grid_ComboCloseUp(index, sender, e);
		}

		private void GridSchedule_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			int lngRow;
			lngRow = e.OldRow;
			if (!boolLoading)
			{
				if (e.OldRow > 0)
				{
					// Save info
					UpdateWeek();
				}
				int intindex = 0;
				intindex = FCConvert.ToInt32(Math.Round(Conversion.Val(GridSchedule.TextMatrix(e.NewRow, CNSTGRIDSCHEDULECOLINDEX))));
				clsSchedule tSchedule = new clsSchedule();
				tSchedule = ScheduleList.GetScheduleByIndex(ref intindex);
				tSchedule.MoveFirst();
				intindex = tSchedule.GetCurrentIndex;
				LoadGridWeeks();
				LoadGrids(ref intindex, ref tSchedule);
			}
		}

		private void GridSchedule_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intOption As int	OnWrite(string)
			int intOption = 0;
			intOption = FCConvert.ToInt32(GridSchedule.ComboData());
			clsSchedule tSched;
			tSched = ScheduleList.GetCurrentSchedule();
			if (!(tSched == null))
			{
				tSched.PaidOption = (modSchedule.SchedulePaidOption)intOption;
			}
		}

		private void GridSchedule_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddSchedule();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteSchedule();
						break;
					}
			}
			//end switch
		}

		private void AddSchedule()
		{
			UpdateWeek();
			clsSchedule tSchedule;
			int intindex;
			intindex = ScheduleList.AddSchedule("New Schedule", modSchedule.SchedulePaidOption.DayOfShiftStart);
			boolLoading = true;
			tSchedule = ScheduleList.GetCurrentSchedule();
			int lngRow;
			GridSchedule.Rows += 1;
			lngRow = GridSchedule.Rows - 1;
			GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLID, FCConvert.ToString(0));
			GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLINDEX, FCConvert.ToString(intindex));
			GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLNAME, "New Schedule");
			GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLOPTION, FCConvert.ToString(FCConvert.ToInt32(modSchedule.SchedulePaidOption.DayOfShiftStart)));
			LoadGridWeeks();
			tSchedule.MoveFirst();
			intindex = tSchedule.GetCurrentIndex;
			LoadGrids(ref intindex, ref tSchedule);
			boolLoading = false;
		}

		private void LoadScheduleInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				int lngRow;
				ScheduleList.LoadSchedules();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmScheduleWeeks_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmScheduleWeeks_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmScheduleWeeks properties;
			//frmScheduleWeeks.FillStyle	= 0;
			//frmScheduleWeeks.ScaleWidth	= 9300;
			//frmScheduleWeeks.ScaleHeight	= 7560;
			//frmScheduleWeeks.LinkTopic	= "Form2";
			//frmScheduleWeeks.LockControls	= -1  'True;
			//frmScheduleWeeks.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoading = true;
			SetupGrid();
			SetupGridWeeks();
			SetupGridSchedule();
			LoadScheduleInfo();
			LoadGridSchedules();
			LoadGridWeeks();
			int intindex = 0;
			clsSchedule parentschedule;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				parentschedule.MoveFirst();
				intindex = parentschedule.GetCurrentIndex;
				LoadGrids(ref intindex, ref parentschedule);
			}
			boolLoading = false;
		}

		private void SetupGrid()
		{
			int x;
			string strTemp;
			int intindex = 0;
			clsShiftType tShift;
			ShiftList.LoadTypes();
			strTemp = "";
			ShiftList.MoveFirst();
			// strTemp = "#0;None"
			if (ShiftList.GetCurrentIndex() < 0)
			{
				strTemp = "#0;None";
			}
			else
			{
				intindex = ShiftList.GetCurrentIndex();
				while (intindex >= 0)
				{
					tShift = ShiftList.GetCurrentShiftType();
					if (tShift.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Regular))
					{
						lngRegularShiftID = tShift.ShiftID;
					}
					if (!(tShift == null))
					{
						strTemp += "#" + FCConvert.ToString(tShift.ShiftID) + ";" + tShift.ShiftName + "|";
					}
					ShiftList.MoveNext();
					intindex = ShiftList.GetCurrentIndex();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					// get rid of last |
				}
				else
				{
					strTemp = "#0;None";
				}
			}
			for (x = 0; x <= 6; x++)
			{
				Grid[x].Cols = 7;
				Grid[x].ColHidden(cnstgridcolID, true);
				Grid[x].ColHidden(CNSTGRIDCOLINDEX, true);
				Grid[x].ColHidden(CNSTGRIDCOLLUNCH, true);
				Grid[x].ColHidden(CNSTGRIDCOLACCOUNT, true);
				Grid[x].ColComboList(CNSTGRIDCOLSTART, "...");
				Grid[x].ColComboList(CNSTGRIDCOLEND, "...");
				Grid[x].ColComboList(CNSTGRIDCOLSHIFTTYPE, strTemp);
				Grid[x].TextMatrix(0, CNSTGRIDCOLSHIFTTYPE, "Shift");
				Grid[x].TextMatrix(0, CNSTGRIDCOLSTART, "From");
				Grid[x].TextMatrix(0, CNSTGRIDCOLEND, "To");
			}
			// x
		}

		private void LoadGridSchedules()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridSchedule.Rows = 1;
				int lngRow;
				clsSchedule tSchedule;
				int intTemp = 0;
				intTemp = -1;
				ScheduleList.MoveFirst();
				while (ScheduleList.GetCurrentIndex() >= 0)
				{
					tSchedule = ScheduleList.GetCurrentSchedule();
					if (!(tSchedule == null))
					{
						GridSchedule.Rows += 1;
						lngRow = GridSchedule.Rows - 1;
						intTemp = ScheduleList.GetCurrentIndex();
						GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLINDEX, FCConvert.ToString(intTemp));
						GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLID, FCConvert.ToString(tSchedule.ScheduleID));
						GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLNAME, tSchedule.ScheduleName);
						GridSchedule.TextMatrix(lngRow, CNSTGRIDSCHEDULECOLOPTION, tSchedule.PaidOption);
					}
					ScheduleList.MoveNext();
				}
				ScheduleList.MoveFirst();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridSchedules", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGridWeeks()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridWeeks.Rows = 1;
				int lngRow;
				clsScheduleWeek tWeek;
				clsSchedule parentschedule;
				int intTemp = 0;
				intTemp = -1;
				parentschedule = ScheduleList.GetCurrentSchedule();
				if (!(parentschedule == null))
				{
					parentschedule.MoveFirst();
					while (parentschedule.GetCurrentIndex >= 0)
					{
						tWeek = parentschedule.GetCurrentWeek();
						if (!(tWeek == null))
						{
							GridWeeks.Rows += 1;
							lngRow = GridWeeks.Rows - 1;
							intTemp = parentschedule.GetCurrentIndex;
							GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLINDEX, FCConvert.ToString(intTemp));
							GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLNAME, tWeek.WeekName);
							GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLOVERTIME, FCConvert.ToString(tWeek.OvertimeStart));
							GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLLIMIT, FCConvert.ToString(tWeek.AlwaysPayHours));
							GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLORDER, FCConvert.ToString(tWeek.OrderNumber));
							parentschedule.MoveNext();
						}
					}
				}
				if (GridWeeks.Rows > 1)
				{
					GridWeeks.Col = CNSTGridWeeksCOLORDER;
					GridWeeks.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridWeeks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			int x;
			//Frame1.Width = this.WidthOriginal - Frame1.Left - Frame1.Left;
			for (x = 0; x <= 6; x++)
			{
				GridWidth = Grid[x].WidthOriginal;
				Grid[x].ColWidth(CNSTGRIDCOLSHIFTTYPE, FCConvert.ToInt32(0.5 * GridWidth));
				Grid[x].ColWidth(CNSTGRIDCOLSTART, FCConvert.ToInt32(0.23 * GridWidth));
			}
			// x
		}

		private void UpdateWeek()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsScheduleWeek tWeek;
				clsSchedule parentschedule;
				parentschedule = ScheduleList.GetCurrentSchedule();
				if (!(parentschedule == null))
				{
					tWeek = parentschedule.GetCurrentWeek();
					tWeek.Unused = false;
					tWeek.WeekName = strCurrentWeekName;
					tWeek.ScheduleID = parentschedule.ScheduleID;
					clsScheduleDay tDay;
					clsWorkShift tShift;
					int x;
					// vbPorter upgrade warning: lngRow As int	OnWriteFCConvert.ToInt32(
					int lngRow;
					for (x = 1; x <= 7; x++)
					{
						tDay = tWeek.GetDay(x);
						if (!(tDay == null))
						{
							tDay.DayOfWeek = x;
							for (lngRow = 1; lngRow <= (Grid[x - 1].Rows - 1); lngRow++)
							{
								tShift = tDay.GetShiftByIndex(FCConvert.ToInt32(Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLINDEX)));
								if (!(tShift == null))
								{
									tShift.ActualEnd = "Unworked";
									tShift.ActualStart = "Unworked";
									tShift.ScheduledEnd = Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLEND);
									tShift.ScheduledStart = Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLSTART);
									tShift.ShiftType = FCConvert.ToInt32(Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLSHIFTTYPE));
									tShift.LunchTime = Conversion.Val(Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLLUNCH));
									tShift.Account = Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT);
								}
								else
								{
									// shouldn't ever get here
								}
							}
							// lngRow
						}
						else
						{
							// shouldn't ever get here
						}
					}
					// x
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGrids(ref int intindex, ref clsSchedule parentschedule)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				clsScheduleWeek tWeek;
				clsScheduleDay tDay;
				if (!(parentschedule == null))
				{
					tWeek = parentschedule.GetWeekByIndex(intindex);
					clsWorkShift tShift;
					int lngRow;
					if (!(tWeek == null))
					{
						strCurrentWeekName = tWeek.WeekName;
						for (x = 1; x <= 7; x++)
						{
							Grid[x - 1].Rows = 1;
							tDay = tWeek.GetDay(x);
							if (!(tDay == null))
							{
								tDay.MoveFirst();
								while (tDay.GetCurrentIndex() >= 0)
								{
									tShift = tDay.GetCurrentShift();
									if (!(tShift == null))
									{
										Grid[x - 1].Rows += 1;
										lngRow = Grid[x - 1].Rows - 1;
										Grid[x - 1].TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(tShift.ID));
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLSHIFTTYPE, FCConvert.ToString(tShift.ShiftType));
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLSTART, tShift.ScheduledStart);
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLEND, tShift.ScheduledEnd);
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(tDay.GetCurrentIndex()));
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLLUNCH, FCConvert.ToString(tShift.LunchTime));
										Grid[x - 1].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, tShift.Account);
									}
									tDay.MoveNext();
								}
							}
						}
						// x
						lblWeek.Text = tWeek.WeekName;
						lblSchedule.Text = parentschedule.ScheduleName;
					}
					else
					{
						// clear the grids
						for (x = 1; x <= 7; x++)
						{
							Grid[x - 1].Rows = 1;
						}
						// x
						strCurrentWeekName = "";
						lblWeek.Text = "";
						lblSchedule.Text = "";
					}
				}
				else
				{
					// clear the grids
					for (x = 1; x <= 7; x++)
					{
						Grid[x - 1].Rows = 1;
					}
					// x
					strCurrentWeekName = "";
					lblWeek.Text = "";
					lblSchedule.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrids", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmScheduleWeeks_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridWeeks();
			ResizeGridSchedule();
		}

		private void ReshowFrame()
		{
			int lngWidth;
			int lngOffset;
			int lngPos;
			int lngMax;
			double dblPct = 0;
			// vbPorter upgrade warning: lngLeft As int	OnWriteFCConvert.ToDouble(
			int lngLeft = 0;
			lngOffset = 75;
			//lngWidth = framGrids.WidthOriginal;
			//lngPos = HScroll1.Value;
			//lngMax = lngWidth - (Frame1.WidthOriginal - lngOffset);
			//if (lngMax > 0)
			//{
			//	dblPct = lngPos / 100.0;
			//	lngLeft = FCConvert.ToInt32(-(dblPct * lngMax));
			//	framGrids.LeftOriginal = lngLeft;
			//}
			//else
			//{
			//	framGrids.LeftOriginal = lngOffset;
			//}
		}

		private void Grid_CellButtonClick(int Index, object sender, EventArgs e)
		{
			int lngCol;
			int lngRow;
			lngRow = Grid[Index].Row;
			string strSend;
			string strFrom;
			string strTo;
			string strReturn;
			string strAccount;
			strFrom = Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLSTART);
			strTo = Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLEND);
			strSend = strFrom + "," + strTo + "," + FCConvert.ToString(Conversion.Val(Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLLUNCH))) + "," + Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT);
			strReturn = frmGetScheduleTime.InstancePtr.Init(ref strSend, true);
			string[] strAry = null;
			strAry = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
			strFrom = strAry[0];
			strFrom = strAry[0];
			strTo = strAry[1];
			strAccount = strAry[3];
			Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, strAccount);
			Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLSTART, strFrom);
			Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLEND, strTo);
			Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLLUNCH, FCConvert.ToString(Conversion.Val(strAry[2])));
			clsScheduleWeek tWeek;
			clsScheduleDay tDay;
			clsSchedule parentschedule;
			int intDayIndex;
			clsWorkShift tShift;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				tWeek = parentschedule.GetCurrentWeek();
				tDay = tWeek.GetDay(Index + 1);
				tShift = tDay.GetShiftByIndex(FCConvert.ToInt32(Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLINDEX)));
				tShift.ScheduledEnd = strTo;
				tShift.ScheduledStart = strFrom;
				tShift.LunchTime = Conversion.Val(strAry[2]);
				tShift.ShiftType = FCConvert.ToInt32(Grid[Index].TextMatrix(lngRow, CNSTGRIDCOLSHIFTTYPE));
				tShift.Account = strAccount;
			}
		}

		private void Grid_CellButtonClick(object sender, EventArgs e)
		{
			int index = Grid.GetIndex((FCGrid)((sender as Button).Parent.Parent));
			Grid_CellButtonClick(index, sender, e);
		}

		private void Grid_KeyDownEvent(int Index, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddShift(Index);
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						RemoveShift(Index);
						break;
					}
			}
			//end switch
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int index = Grid.GetIndex((FCGrid)sender);
			Grid_KeyDownEvent(index, e);
		}

		private void AddShift(int intindex)
		{
			// VB6 Bad Scope Dim:
			int lngRow;
			int intShiftIndex = 0;
			clsScheduleWeek tWeek;
			clsScheduleDay tDay;
			clsSchedule parentschedule;
			clsShiftType tShift;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				tWeek = parentschedule.GetCurrentWeek();
				tDay = tWeek.GetDay(intindex + 1);
				intShiftIndex = tDay.AddShift(0, "1:00 AM", "1:00 AM", "Unworked", "Unworked", 0, 0, "");
				Grid[intindex].Rows += 1;
				lngRow = Grid[intindex].Rows - 1;
				Grid[intindex].TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(0));
				Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(intShiftIndex));
				Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLEND, "1:00 AM");
				Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLSHIFTTYPE, FCConvert.ToString(lngRegularShiftID));
				Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLSTART, "1:00 AM");
				tShift = ShiftList.GetShiftTypeByType(lngRegularShiftID);
				if (!(tShift == null))
				{
					Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLLUNCH, FCConvert.ToString(tShift.LunchTime));
					Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, tShift.Account);
				}
				else
				{
					Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLLUNCH, FCConvert.ToString(0));
					Grid[intindex].TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, "");
				}
			}
		}

		private void RemoveShift(int intindex)
		{
			clsScheduleWeek tWeek;
			clsScheduleDay tDay;
			clsSchedule parentschedule;
			int intShiftIndex = 0;
			clsWorkShift tShift;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				tWeek = parentschedule.GetCurrentWeek();
				if (!(tWeek == null))
				{
					tDay = tWeek.GetDay(intindex + 1);
					if (!(tDay == null))
					{
						intShiftIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid[intindex].TextMatrix(Grid[intindex].Row, CNSTGRIDCOLINDEX))));
						tShift = tDay.GetShiftByIndex(intShiftIndex);
						if (!(tShift == null))
						{
							tDay.DeleteShift(intShiftIndex);
							Grid[intindex].RemoveItem(Grid[intindex].Row);
						}
					}
				}
			}
		}

		private void GridSchedule_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridSchedule.Row > 0)
			{
				clsSchedule tSched;
				switch (GridSchedule.Col)
				{
					case CNSTGRIDSCHEDULECOLNAME:
						{
							tSched = ScheduleList.GetCurrentSchedule();
							if (!(tSched == null))
							{
								tSched.ScheduleName = GridSchedule.EditText;
							}
							// Case CNSTGRIDSCHEDULECOLOPTION
							// Set tSched = ScheduleList.GetCurrentSchedule
							// If Not tSched Is Nothing Then
							// tSched.PaidOption = GridSchedule.ComboData
							// End If
							break;
						}
				}
				//end switch
			}
		}

		private void GridWeeks_AfterMoveRow(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// need to reorder the weeks
			if (GridWeeks.Rows > 1)
			{
				clsScheduleWeek tWeek;
				clsSchedule parentschedule;
				parentschedule = ScheduleList.GetCurrentSchedule();
				if (!(parentschedule == null))
				{
					tWeek = parentschedule.GetCurrentWeek();
					for (x = 1; x <= (GridWeeks.Rows - 1); x++)
					{
						tWeek = parentschedule.GetWeekByIndex(FCConvert.ToInt16(Conversion.Val(GridWeeks.TextMatrix(x, CNSTGridWeeksCOLINDEX))));
						if (!(tWeek == null))
						{
							tWeek.OrderNumber = x;
						}
					}
					// x
				}
			}
		}

		private void GridWeeks_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			int lngRow;
			lngRow = e.OldRow;
			if (!boolLoading)
			{
				if (e.OldRow > 0)
				{
					// Save info
					UpdateWeek();
				}
				int intindex = 0;
				intindex = FCConvert.ToInt32(Math.Round(Conversion.Val(GridWeeks.TextMatrix(e.NewRow, CNSTGridWeeksCOLINDEX))));
				clsSchedule tSchedule;
				tSchedule = ScheduleList.GetCurrentSchedule();
				if (!(tSchedule == null))
				{
					LoadGrids(ref intindex, ref tSchedule);
				}
				else
				{
					lblSchedule.Text = "";
					lblWeek.Text = "";
				}
			}
		}

		private void GridWeeks_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddWeek();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteWeek();
						break;
					}
			}
			//end switch
		}

		private void GridWeeks_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridWeeks[e.ColumnIndex, e.RowIndex];
            int lngCol;
			string strTemp;
			lngCol = GridWeeks.GetFlexColIndex(e.ColumnIndex);
			strTemp = "";
			switch (lngCol)
			{
				case CNSTGridWeeksCOLINDEX:
					{
						break;
					}
				case CNSTGridWeeksCOLLIMIT:
					{
						strTemp = "Number of hours to pay for regardless of actual hours scheduled";
						break;
					}
				case CNSTGridWeeksCOLMOVE:
					{
						strTemp = "Drag rows to reorder weeks";
						break;
					}
				case CNSTGridWeeksCOLNAME:
					{
						strTemp = "Name for week such as 1,one,A etc.";
						break;
					}
				case CNSTGridWeeksCOLOVERTIME:
					{
						strTemp = "Number of hours before overtime is paid";
						break;
					}
			}
			//end switch
			//ToolTip1.SetToolTip(GridWeeks, strTemp);
			cell.ToolTipText = strTemp;
		}

		private void GridWeeks_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			clsScheduleWeek tWeek;
			clsSchedule tSched;
			if (GridWeeks.Row > 0)
			{
				switch (GridWeeks.Col)
				{
					case CNSTGridWeeksCOLNAME:
						{
							strCurrentWeekName = GridWeeks.EditText;
							break;
						}
					case CNSTGridWeeksCOLLIMIT:
						{
							tSched = ScheduleList.GetCurrentSchedule();
							if (!(tSched == null))
							{
								tWeek = tSched.GetCurrentWeek();
								if (!(tWeek == null))
								{
									tWeek.AlwaysPayHours = Conversion.Val(GridWeeks.EditText);
								}
							}
							break;
						}
					case CNSTGridWeeksCOLOVERTIME:
						{
							tSched = ScheduleList.GetCurrentSchedule();
							if (!(tSched == null))
							{
								tWeek = tSched.GetCurrentWeek();
								if (!(tWeek == null))
								{
									tWeek.OvertimeStart = Conversion.Val(GridWeeks.EditText);
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			ReshowFrame();
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			ReshowFrame();
		}

		private void mnuAddWeek_Click(object sender, System.EventArgs e)
		{
			AddWeek();
		}

		private void AddWeek()
		{
			// VB6 Bad Scope Dim:
			int lngRow = 0;
			int intWeekNo = 0;
			int intindex = 0;
			clsSchedule parentschedule;
			UpdateWeek();
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				intWeekNo = parentschedule.HighestWeek();
				intWeekNo += 1;
				parentschedule.AddWeek(intWeekNo, intWeekNo.ToString());
				intindex = parentschedule.GetCurrentIndex;
				// Dim strTemp As String
				// strTemp = GridWeekDD.ColComboList(0)
				// strTemp = strTemp & "|#" & intWeekNo & ";" & intWeekNo
				// boolLoading = True
				// GridWeekDD.ColComboList(0) = strTemp
				// GridWeekDD.TextMatrix(1, 0) = intWeekNo
				// boolLoading = False
				// LoadGrids (intWeekNo)
				boolLoading = true;
				LoadGrids(ref intindex, ref parentschedule);
				GridWeeks.Rows += 1;
				lngRow = GridWeeks.Rows - 1;
				GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLINDEX, FCConvert.ToString(intindex));
				GridWeeks.TextMatrix(lngRow, CNSTGridWeeksCOLNAME, intWeekNo.ToString());
				GridWeeks.Row = lngRow;
			}
			boolLoading = false;
		}

		private void mnuAddSchedule_Click(object sender, System.EventArgs e)
		{
			AddSchedule();
		}

		private void mnuDeleteSchedule_Click(object sender, System.EventArgs e)
		{
			DeleteSchedule();
		}

		private void mnuDeleteWeek_Click(object sender, System.EventArgs e)
		{
			DeleteWeek();
		}

		private void DeleteWeek()
		{
			clsSchedule parentschedule;
			clsScheduleWeek tWeek;
			int intindex = 0;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				tWeek = parentschedule.GetCurrentWeek();
				if (!(tWeek == null))
				{
					if (MessageBox.Show("This will delete the selected week" + "\r\n" + "Do you want to continue?", "Delete Week?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					{
						tWeek.Unused = true;
						parentschedule.MoveFirst();
						intindex = parentschedule.GetCurrentIndex;
						LoadGridWeeks();
						LoadGrids(ref intindex, ref parentschedule);
					}
				}
				else
				{
					MessageBox.Show("No current week", "No Week", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("No current schedule", "No Schedule", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void DeleteSchedule()
		{
			clsSchedule parentschedule;
			int intindex = 0;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				parentschedule.Unused = true;
				LoadGridSchedules();
				parentschedule = ScheduleList.GetCurrentSchedule();
				if (!(parentschedule == null))
				{
					intindex = parentschedule.GetCurrentIndex;
					LoadGridWeeks();
					LoadGrids(ref intindex, ref parentschedule);
				}
			}
			else
			{
				MessageBox.Show("No current schedule", "No Schedule", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			ScheduleList.MoveFirst();
			clsSchedule parentschedule;
			parentschedule = ScheduleList.GetCurrentSchedule();
			if (!(parentschedule == null))
			{
				parentschedule.MoveFirst();
				int intindex = 0;
				intindex = parentschedule.GetCurrentIndex;
				LoadGrids(ref intindex, ref parentschedule);
			}
			else
			{
				GridWeeks.Rows = 1;
				int x;
				for (x = 0; x <= 6; x++)
				{
					Grid[x].Rows = 1;
				}
				// x
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				Close();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				UpdateWeek();
				SaveInfo = ScheduleList.SaveSchedules();
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void SetupGridWeeks()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridWeeks.Rows = 1;
				GridWeeks.Cols = 6;
				GridWeeks.ColAlignment(CNSTGridWeeksCOLNAME, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				GridWeeks.ColHidden(CNSTGridWeeksCOLINDEX, true);
				GridWeeks.ColHidden(CNSTGridWeeksCOLORDER, true);
				GridWeeks.TextMatrix(0, CNSTGridWeeksCOLNAME, "Week");
				GridWeeks.TextMatrix(0, CNSTGridWeeksCOLOVERTIME, "OT After");
				GridWeeks.TextMatrix(0, CNSTGridWeeksCOLLIMIT, "OV Hours");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridWeeks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridWeeks()
		{
			// vbPorter upgrade warning: GridWidth As object	OnWriteFCConvert.ToInt32(
			int GridWidth = GridWeeks.WidthOriginal;
			GridWeeks.ColWidth(CNSTGridWeeksCOLMOVE, FCConvert.ToInt32(0.1 * GridWidth));
			GridWeeks.ColWidth(CNSTGridWeeksCOLNAME, FCConvert.ToInt32(0.24 * GridWidth));
			GridWeeks.ColWidth(CNSTGridWeeksCOLOVERTIME, FCConvert.ToInt32(0.3 * GridWidth));
		}
	}
}
