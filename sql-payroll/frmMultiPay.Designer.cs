//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmMultiPay.
	/// </summary>
	partial class frmMultiPay
	{
		public FCGrid vsMultiPay;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsMultiPay = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdNewMulti = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMultiPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewMulti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 402);
            this.BottomPanel.Size = new System.Drawing.Size(801, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsMultiPay);
            this.ClientArea.Size = new System.Drawing.Size(801, 342);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdNewMulti);
            this.TopPanel.Size = new System.Drawing.Size(801, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewMulti, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(113, 30);
            this.HeaderText.Text = "Multi Pay";
            // 
            // vsMultiPay
            // 
            this.vsMultiPay.AllowSelection = false;
            this.vsMultiPay.AllowUserToResizeColumns = false;
            this.vsMultiPay.AllowUserToResizeRows = false;
            this.vsMultiPay.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMultiPay.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsMultiPay.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsMultiPay.BackColorBkg = System.Drawing.Color.Empty;
            this.vsMultiPay.BackColorFixed = System.Drawing.Color.Empty;
            this.vsMultiPay.BackColorSel = System.Drawing.Color.Empty;
            this.vsMultiPay.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsMultiPay.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsMultiPay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsMultiPay.ColumnHeadersHeight = 30;
            this.vsMultiPay.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsMultiPay.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsMultiPay.DragIcon = null;
            this.vsMultiPay.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsMultiPay.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsMultiPay.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsMultiPay.FrozenCols = 0;
            this.vsMultiPay.GridColor = System.Drawing.Color.Empty;
            this.vsMultiPay.GridColorFixed = System.Drawing.Color.Empty;
            this.vsMultiPay.Location = new System.Drawing.Point(30, 30);
            this.vsMultiPay.Name = "vsMultiPay";
            this.vsMultiPay.OutlineCol = 0;
            this.vsMultiPay.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsMultiPay.RowHeightMin = 0;
            this.vsMultiPay.Rows = 50;
            this.vsMultiPay.ScrollTipText = null;
            this.vsMultiPay.ShowColumnVisibilityMenu = false;
            this.vsMultiPay.Size = new System.Drawing.Size(743, 302);
            this.vsMultiPay.StandardTab = true;
            this.vsMultiPay.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsMultiPay.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsMultiPay.TabIndex = 0;
            this.vsMultiPay.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMultiPay_ValidateEdit);
            this.vsMultiPay.CurrentCellChanged += new System.EventHandler(this.vsMultiPay_RowColChange);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuDelete,
            this.mnuSP1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "New Multi Pay";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Multi Pay";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                           ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                           ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 5;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNewMulti
            // 
            this.cmdNewMulti.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewMulti.AppearanceKey = "toolbarButton";
            this.cmdNewMulti.Location = new System.Drawing.Point(537, 29);
            this.cmdNewMulti.Name = "cmdNewMulti";
            this.cmdNewMulti.Size = new System.Drawing.Size(108, 24);
            this.cmdNewMulti.TabIndex = 1;
            this.cmdNewMulti.Text = "New Multi Pay";
            this.cmdNewMulti.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(651, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(122, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete Multi Pay";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(346, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmMultiPay
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(801, 510);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmMultiPay";
            this.Text = "Multi Pay";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMultiPay_Load);
            this.Activated += new System.EventHandler(this.frmMultiPay_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultiPay_KeyPress);
            this.Resize += new System.EventHandler(this.frmMultiPay_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMultiPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewMulti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdNewMulti;
	}
}