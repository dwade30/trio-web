//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSetSeasonalUnempY.
	/// </summary>
	public partial class rptSetSeasonalUnempY : BaseSectionReport
	{
		public rptSetSeasonalUnempY()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Set Seasonal Unemployment (S) to Regular Unemployment (Y)";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSetSeasonalUnempY InstancePtr
		{
			get
			{
				return (rptSetSeasonalUnempY)Sys.GetInstance(typeof(rptSetSeasonalUnempY));
			}
		}

		protected rptSetSeasonalUnempY _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsExec?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSetSeasonalUnempY	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       FEBRUARY 23, 2005
		//
		// NOTES:
		//
		//
		// **************************************************
		int intpage;
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsExec = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// CHEC
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			txtName.Text = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
			// rsData.Edit
			// rsData.Fields("Unemployment") = "Y"
			// rsData.Update
			rsExec.Execute("update tblmiscupdate set unemployment = 'Y' where employeenumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "Payroll");
			if (!rsData.EndOfFile())
				rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Set Seasonal Unemployment (S) to Regular Unemployment (Y)";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsData.OpenRecordset("SELECT tblMiscUpdate.Unemployment, tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, tblEmployeeMaster.MiddleName FROM tblMiscUpdate INNER JOIN tblEmployeeMaster ON tblMiscUpdate.EmployeeNumber = tblEmployeeMaster.EmployeeNumber WHERE tblMiscUpdate.Unemployment='S' Order by LastName,FirstName,MiddleName");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Set Seasonal Unemployment (S) to Regular Unemployment (Y)";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		
	}
}
