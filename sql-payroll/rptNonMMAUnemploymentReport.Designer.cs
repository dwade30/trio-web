﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNonMMAUnemploymentReport.
	/// </summary>
	partial class rptNonMMAUnemploymentReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNonMMAUnemploymentReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEmployer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldReportNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldQuarter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPreparer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTelephoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFederalID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMemberCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeasonalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFirst = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeasonal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSocialSecurity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeasonal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFem = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFemOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFemTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFemThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReportNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuarter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreparer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTelephoneNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMemberCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSocialSecurity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldSocialSecurity,
				this.fldName,
				this.fldSeasonal,
				this.fldFem,
				this.fldOne,
				this.fldTwo,
				this.fldThree,
				this.fldGross,
				this.fldTaxable,
				this.fldExcess
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label10,
				this.Label11,
				this.Label13,
				this.Line1,
				this.Label14,
				this.fldEmployer,
				this.Label15,
				this.fldReportNumber,
				this.Label16,
				this.fldQuarter,
				this.Label17,
				this.fldPreparer,
				this.Label18,
				this.fldTelephoneNumber,
				this.Label19,
				this.fldFederalID,
				this.Label20,
				this.fldStateID,
				this.Label21,
				this.fldMemberCode,
				this.Label22,
				this.fldSeasonalCode,
				this.Label38,
				this.lblFirst,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.lblSeasonal,
				this.Label48
			});
			this.PageHeader.Height = 2.354167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.Label49,
				this.Label50,
				this.fldTotalOne,
				this.fldTotalTwo,
				this.fldTotalThree,
				this.fldTotalGross,
				this.fldTotalTaxable,
				this.fldTotalExcess,
				this.fldFemOne,
				this.fldFemTwo,
				this.fldFemThree
			});
			this.GroupFooter1.Height = 0.4375F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Non MMA Unemployment Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.71875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "Social";
			this.Label10.Top = 1.96875F;
			this.Label10.Width = 0.96875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.03125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label11.Text = "Employee\'s Name";
			this.Label11.Top = 2.15625F;
			this.Label11.Width = 1.78125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.40625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label13.Text = "Fem";
			this.Label13.Top = 2.15625F;
			this.Label13.Width = 0.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 2.34375F;
			this.Line1.Width = 7.71875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.71875F;
			this.Line1.Y1 = 2.34375F;
			this.Line1.Y2 = 2.34375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label14.Text = "Employer:";
			this.Label14.Top = 0.4375F;
			this.Label14.Width = 0.75F;
			// 
			// fldEmployer
			// 
			this.fldEmployer.Height = 0.19F;
			this.fldEmployer.Left = 0.8125F;
			this.fldEmployer.Name = "fldEmployer";
			this.fldEmployer.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldEmployer.Text = "Field1";
			this.fldEmployer.Top = 0.4375F;
			this.fldEmployer.Width = 2.15625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label15.Text = "Report No.";
			this.Label15.Top = 0.65625F;
			this.Label15.Width = 0.9375F;
			// 
			// fldReportNumber
			// 
			this.fldReportNumber.Height = 0.19F;
			this.fldReportNumber.Left = 1.03125F;
			this.fldReportNumber.Name = "fldReportNumber";
			this.fldReportNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldReportNumber.Text = "Field1";
			this.fldReportNumber.Top = 0.65625F;
			this.fldReportNumber.Width = 2.15625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label16.Text = "Quarter";
			this.Label16.Top = 0.8125F;
			this.Label16.Width = 0.9375F;
			// 
			// fldQuarter
			// 
			this.fldQuarter.Height = 0.19F;
			this.fldQuarter.Left = 1.03125F;
			this.fldQuarter.Name = "fldQuarter";
			this.fldQuarter.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldQuarter.Text = "Field2";
			this.fldQuarter.Top = 0.8125F;
			this.fldQuarter.Width = 2.15625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label17.Text = "Preparer";
			this.Label17.Top = 0.96875F;
			this.Label17.Width = 0.9375F;
			// 
			// fldPreparer
			// 
			this.fldPreparer.Height = 0.19F;
			this.fldPreparer.Left = 1.03125F;
			this.fldPreparer.Name = "fldPreparer";
			this.fldPreparer.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPreparer.Text = "Field3";
			this.fldPreparer.Top = 0.96875F;
			this.fldPreparer.Width = 2.15625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label18.Text = "Telephone No.";
			this.Label18.Top = 1.125F;
			this.Label18.Width = 0.9375F;
			// 
			// fldTelephoneNumber
			// 
			this.fldTelephoneNumber.Height = 0.19F;
			this.fldTelephoneNumber.Left = 1.03125F;
			this.fldTelephoneNumber.Name = "fldTelephoneNumber";
			this.fldTelephoneNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldTelephoneNumber.Text = "Field4";
			this.fldTelephoneNumber.Top = 1.125F;
			this.fldTelephoneNumber.Width = 2.15625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label19.Text = "Federal ID";
			this.Label19.Top = 1.28125F;
			this.Label19.Width = 0.9375F;
			// 
			// fldFederalID
			// 
			this.fldFederalID.Height = 0.19F;
			this.fldFederalID.Left = 1.03125F;
			this.fldFederalID.Name = "fldFederalID";
			this.fldFederalID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldFederalID.Text = "Field5";
			this.fldFederalID.Top = 1.28125F;
			this.fldFederalID.Width = 2.15625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label20.Text = "State ID";
			this.Label20.Top = 1.4375F;
			this.Label20.Width = 0.9375F;
			// 
			// fldStateID
			// 
			this.fldStateID.Height = 0.19F;
			this.fldStateID.Left = 1.03125F;
			this.fldStateID.Name = "fldStateID";
			this.fldStateID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStateID.Text = "Field6";
			this.fldStateID.Top = 1.4375F;
			this.fldStateID.Width = 2.15625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label21.Text = "Member Code";
			this.Label21.Top = 1.59375F;
			this.Label21.Width = 0.9375F;
			// 
			// fldMemberCode
			// 
			this.fldMemberCode.Height = 0.19F;
			this.fldMemberCode.Left = 1.03125F;
			this.fldMemberCode.Name = "fldMemberCode";
			this.fldMemberCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMemberCode.Text = "Field7";
			this.fldMemberCode.Top = 1.59375F;
			this.fldMemberCode.Width = 2.15625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label22.Text = "Seasonal Code";
			this.Label22.Top = 1.75F;
			this.Label22.Width = 0.96875F;
			// 
			// fldSeasonalCode
			// 
			this.fldSeasonalCode.Height = 0.19F;
			this.fldSeasonalCode.Left = 1.03125F;
			this.fldSeasonalCode.Name = "fldSeasonalCode";
			this.fldSeasonalCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldSeasonalCode.Text = "Field8";
			this.fldSeasonalCode.Top = 1.75F;
			this.fldSeasonalCode.Width = 2.15625F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label38.Text = "Security No";
			this.Label38.Top = 2.15625F;
			this.Label38.Width = 0.96875F;
			// 
			// lblFirst
			// 
			this.lblFirst.Height = 0.1875F;
			this.lblFirst.HyperLink = null;
			this.lblFirst.Left = 3.78125F;
			this.lblFirst.Name = "lblFirst";
			this.lblFirst.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblFirst.Text = "1st";
			this.lblFirst.Top = 2.15625F;
			this.lblFirst.Width = 0.375F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 4.15625F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label43.Text = "2nd";
			this.Label43.Top = 2.15625F;
			this.Label43.Width = 0.375F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 4.53125F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label44.Text = "3rd";
			this.Label44.Top = 2.15625F;
			this.Label44.Width = 0.375F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 2.84375F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label45.Text = "Season";
			this.Label45.Top = 2.15625F;
			this.Label45.Width = 0.53125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 4.9375F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label46.Text = "Gross Wages";
			this.Label46.Top = 2.15625F;
			this.Label46.Width = 1F;
			// 
			// lblSeasonal
			// 
			this.lblSeasonal.Height = 0.1875F;
			this.lblSeasonal.HyperLink = null;
			this.lblSeasonal.Left = 5.96875F;
			this.lblSeasonal.Name = "lblSeasonal";
			this.lblSeasonal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblSeasonal.Text = "Taxable Wages";
			this.lblSeasonal.Top = 2.15625F;
			this.lblSeasonal.Width = 0.875F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 6.875F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label48.Text = "Excess";
			this.Label48.Top = 2.15625F;
			this.Label48.Width = 0.875F;
			// 
			// fldSocialSecurity
			// 
			this.fldSocialSecurity.Height = 0.1875F;
			this.fldSocialSecurity.Left = 0F;
			this.fldSocialSecurity.Name = "fldSocialSecurity";
			this.fldSocialSecurity.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldSocialSecurity.Text = "Field1";
			this.fldSocialSecurity.Top = 0F;
			this.fldSocialSecurity.Width = 0.96875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldName.Text = "Field2";
			this.fldName.Top = 0F;
			this.fldName.Width = 1.8125F;
			// 
			// fldSeasonal
			// 
			this.fldSeasonal.Height = 0.1875F;
			this.fldSeasonal.Left = 2.84375F;
			this.fldSeasonal.Name = "fldSeasonal";
			this.fldSeasonal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldSeasonal.Text = "Field2";
			this.fldSeasonal.Top = 0F;
			this.fldSeasonal.Width = 0.53125F;
			// 
			// fldFem
			// 
			this.fldFem.Height = 0.1875F;
			this.fldFem.Left = 3.40625F;
			this.fldFem.Name = "fldFem";
			this.fldFem.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldFem.Text = "Field2";
			this.fldFem.Top = 0F;
			this.fldFem.Width = 0.375F;
			// 
			// fldOne
			// 
			this.fldOne.Height = 0.1875F;
			this.fldOne.Left = 3.78125F;
			this.fldOne.Name = "fldOne";
			this.fldOne.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldOne.Text = "Field2";
			this.fldOne.Top = 0F;
			this.fldOne.Width = 0.375F;
			// 
			// fldTwo
			// 
			this.fldTwo.Height = 0.1875F;
			this.fldTwo.Left = 4.15625F;
			this.fldTwo.Name = "fldTwo";
			this.fldTwo.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldTwo.Text = "Field3";
			this.fldTwo.Top = 0F;
			this.fldTwo.Width = 0.375F;
			// 
			// fldThree
			// 
			this.fldThree.Height = 0.1875F;
			this.fldThree.Left = 4.53125F;
			this.fldThree.Name = "fldThree";
			this.fldThree.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldThree.Text = "Field4";
			this.fldThree.Top = 0F;
			this.fldThree.Width = 0.375F;
			// 
			// fldGross
			// 
			this.fldGross.Height = 0.1875F;
			this.fldGross.Left = 4.9375F;
			this.fldGross.Name = "fldGross";
			this.fldGross.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldGross.Text = "Field4";
			this.fldGross.Top = 0F;
			this.fldGross.Width = 1F;
			// 
			// fldTaxable
			// 
			this.fldTaxable.Height = 0.1875F;
			this.fldTaxable.Left = 6.03125F;
			this.fldTaxable.Name = "fldTaxable";
			this.fldTaxable.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTaxable.Text = "Field4";
			this.fldTaxable.Top = 0F;
			this.fldTaxable.Width = 0.8125F;
			// 
			// fldExcess
			// 
			this.fldExcess.Height = 0.1875F;
			this.fldExcess.Left = 6.875F;
			this.fldExcess.Name = "fldExcess";
			this.fldExcess.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExcess.Text = "Field4";
			this.fldExcess.Top = 0F;
			this.fldExcess.Width = 0.875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 3.71875F;
			this.Line2.X1 = 4F;
			this.Line2.X2 = 7.71875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 3.03125F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label49.Text = "Totals";
			this.Label49.Top = 0.03125F;
			this.Label49.Width = 0.71875F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 3.03125F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label50.Text = "Females";
			this.Label50.Top = 0.21875F;
			this.Label50.Width = 0.71875F;
			// 
			// fldTotalOne
			// 
			this.fldTotalOne.Height = 0.1875F;
			this.fldTotalOne.Left = 3.78125F;
			this.fldTotalOne.Name = "fldTotalOne";
			this.fldTotalOne.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldTotalOne.Text = "Field2";
			this.fldTotalOne.Top = 0.03125F;
			this.fldTotalOne.Width = 0.375F;
			// 
			// fldTotalTwo
			// 
			this.fldTotalTwo.Height = 0.1875F;
			this.fldTotalTwo.Left = 4.15625F;
			this.fldTotalTwo.Name = "fldTotalTwo";
			this.fldTotalTwo.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldTotalTwo.Text = "Field3";
			this.fldTotalTwo.Top = 0.03125F;
			this.fldTotalTwo.Width = 0.375F;
			// 
			// fldTotalThree
			// 
			this.fldTotalThree.Height = 0.1875F;
			this.fldTotalThree.Left = 4.53125F;
			this.fldTotalThree.Name = "fldTotalThree";
			this.fldTotalThree.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldTotalThree.Text = "Field4";
			this.fldTotalThree.Top = 0.03125F;
			this.fldTotalThree.Width = 0.375F;
			// 
			// fldTotalGross
			// 
			this.fldTotalGross.Height = 0.1875F;
			this.fldTotalGross.Left = 4.9375F;
			this.fldTotalGross.Name = "fldTotalGross";
			this.fldTotalGross.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalGross.Text = "Field4";
			this.fldTotalGross.Top = 0.03125F;
			this.fldTotalGross.Width = 1F;
			// 
			// fldTotalTaxable
			// 
			this.fldTotalTaxable.Height = 0.1875F;
			this.fldTotalTaxable.Left = 6.03125F;
			this.fldTotalTaxable.Name = "fldTotalTaxable";
			this.fldTotalTaxable.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalTaxable.Text = "Field4";
			this.fldTotalTaxable.Top = 0.03125F;
			this.fldTotalTaxable.Width = 0.8125F;
			// 
			// fldTotalExcess
			// 
			this.fldTotalExcess.Height = 0.1875F;
			this.fldTotalExcess.Left = 6.875F;
			this.fldTotalExcess.Name = "fldTotalExcess";
			this.fldTotalExcess.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalExcess.Text = "Field4";
			this.fldTotalExcess.Top = 0.03125F;
			this.fldTotalExcess.Width = 0.875F;
			// 
			// fldFemOne
			// 
			this.fldFemOne.Height = 0.1875F;
			this.fldFemOne.Left = 3.78125F;
			this.fldFemOne.Name = "fldFemOne";
			this.fldFemOne.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldFemOne.Text = "Field2";
			this.fldFemOne.Top = 0.21875F;
			this.fldFemOne.Width = 0.375F;
			// 
			// fldFemTwo
			// 
			this.fldFemTwo.Height = 0.1875F;
			this.fldFemTwo.Left = 4.15625F;
			this.fldFemTwo.Name = "fldFemTwo";
			this.fldFemTwo.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldFemTwo.Text = "Field3";
			this.fldFemTwo.Top = 0.21875F;
			this.fldFemTwo.Width = 0.375F;
			// 
			// fldFemThree
			// 
			this.fldFemThree.Height = 0.1875F;
			this.fldFemThree.Left = 4.53125F;
			this.fldFemThree.Name = "fldFemThree";
			this.fldFemThree.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldFemThree.Text = "Field4";
			this.fldFemThree.Top = 0.21875F;
			this.fldFemThree.Width = 0.375F;
			// 
			// rptNonMMAUnemploymentReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmployer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReportNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuarter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreparer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTelephoneNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMemberCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSocialSecurity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeasonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFemThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSocialSecurity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFem;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcess;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmployer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReportNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuarter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPreparer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTelephoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateID;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMemberCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeasonalCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFirst;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeasonal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFemOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFemTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFemThree;
	}
}
