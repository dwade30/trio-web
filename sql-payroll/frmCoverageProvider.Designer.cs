//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCoverageProvider.
	/// </summary>
	partial class frmCoverageProvider
	{
		public fecherFoundation.FCComboBox cmbPlans;
		public fecherFoundation.FCFrame framPlan;
		public fecherFoundation.FCCheckBox chk1095B;
		public fecherFoundation.FCComboBox cmbPlanMonth;
		public fecherFoundation.FCTextBox txtPlan;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framProvider;
		public fecherFoundation.FCTextBox txtPhone;
		public fecherFoundation.FCTextBox txtEIN;
		public fecherFoundation.FCTextBox txtPostalCode;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtProviderName;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label9;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddPlan;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbPlans = new fecherFoundation.FCComboBox();
            this.framPlan = new fecherFoundation.FCFrame();
            this.chk1095B = new fecherFoundation.FCCheckBox();
            this.cmbPlanMonth = new fecherFoundation.FCComboBox();
            this.txtPlan = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.framProvider = new fecherFoundation.FCFrame();
            this.txtPhone = new fecherFoundation.FCTextBox();
            this.txtEIN = new fecherFoundation.FCTextBox();
            this.txtPostalCode = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtProviderName = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddPlan = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAddPlan = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framPlan)).BeginInit();
            this.framPlan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1095B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framProvider)).BeginInit();
            this.framProvider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 590);
            this.BottomPanel.Size = new System.Drawing.Size(798, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbPlans);
            this.ClientArea.Controls.Add(this.framPlan);
            this.ClientArea.Controls.Add(this.framProvider);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(818, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
            this.ClientArea.Controls.SetChildIndex(this.framProvider, 0);
            this.ClientArea.Controls.SetChildIndex(this.framPlan, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPlans, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddPlan);
            this.TopPanel.Size = new System.Drawing.Size(818, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddPlan, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(233, 28);
            this.HeaderText.Text = "ACA Coverage Setup";
            // 
            // cmbPlans
            // 
            this.cmbPlans.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPlans.Location = new System.Drawing.Point(194, 30);
            this.cmbPlans.Name = "cmbPlans";
            this.cmbPlans.Size = new System.Drawing.Size(394, 40);
            this.cmbPlans.TabIndex = 19;
            this.cmbPlans.SelectedIndexChanged += new System.EventHandler(this.cmbPlans_SelectedIndexChanged);
            // 
            // framPlan
            // 
            this.framPlan.Controls.Add(this.chk1095B);
            this.framPlan.Controls.Add(this.cmbPlanMonth);
            this.framPlan.Controls.Add(this.txtPlan);
            this.framPlan.Controls.Add(this.Label2);
            this.framPlan.Controls.Add(this.Label1);
            this.framPlan.Location = new System.Drawing.Point(30, 90);
            this.framPlan.Name = "framPlan";
            this.framPlan.Size = new System.Drawing.Size(748, 150);
            this.framPlan.TabIndex = 1;
            this.framPlan.Text = "Plan";
            // 
            // chk1095B
            // 
            this.chk1095B.Location = new System.Drawing.Point(443, 30);
            this.chk1095B.Name = "chk1095B";
            this.chk1095B.Size = new System.Drawing.Size(101, 22);
            this.chk1095B.TabIndex = 6;
            this.chk1095B.Text = "Use 1095-B";
            this.ToolTip1.SetToolTip(this.chk1095B, "Whether or not to print 1095-B or just 1095-C");
            this.chk1095B.CheckedChanged += new System.EventHandler(this.chk1095B_CheckedChanged);
            // 
            // cmbPlanMonth
            // 
            this.cmbPlanMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPlanMonth.Location = new System.Drawing.Point(172, 90);
            this.cmbPlanMonth.Name = "cmbPlanMonth";
            this.cmbPlanMonth.Size = new System.Drawing.Size(251, 40);
            this.cmbPlanMonth.TabIndex = 5;
            this.cmbPlanMonth.SelectedIndexChanged += new System.EventHandler(this.cmbPlanMonth_SelectedIndexChanged);
            // 
            // txtPlan
            // 
            this.txtPlan.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlan.Location = new System.Drawing.Point(172, 30);
            this.txtPlan.Name = "txtPlan";
            this.txtPlan.Size = new System.Drawing.Size(251, 40);
            this.txtPlan.TabIndex = 2;
            this.txtPlan.TextChanged += new System.EventHandler(this.txtPlan_TextChanged);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(110, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "START MONTH";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(45, 16);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "NAME";
            // 
            // framProvider
            // 
            this.framProvider.Controls.Add(this.txtPhone);
            this.framProvider.Controls.Add(this.txtEIN);
            this.framProvider.Controls.Add(this.txtPostalCode);
            this.framProvider.Controls.Add(this.txtState);
            this.framProvider.Controls.Add(this.txtCity);
            this.framProvider.Controls.Add(this.txtAddress);
            this.framProvider.Controls.Add(this.txtProviderName);
            this.framProvider.Controls.Add(this.Label10);
            this.framProvider.Controls.Add(this.Label8);
            this.framProvider.Controls.Add(this.Label7);
            this.framProvider.Controls.Add(this.Label6);
            this.framProvider.Controls.Add(this.Label5);
            this.framProvider.Controls.Add(this.Label4);
            this.framProvider.Controls.Add(this.Label3);
            this.framProvider.Location = new System.Drawing.Point(30, 260);
            this.framProvider.Name = "framProvider";
            this.framProvider.Size = new System.Drawing.Size(748, 330);
            this.framProvider.TabIndex = 2;
            this.framProvider.Text = "Provider";
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhone.Location = new System.Drawing.Point(559, 90);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(169, 40);
            this.txtPhone.TabIndex = 21;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // txtEIN
            // 
            this.txtEIN.BackColor = System.Drawing.SystemColors.Window;
            this.txtEIN.Location = new System.Drawing.Point(559, 30);
            this.txtEIN.MaxLength = 9;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Size = new System.Drawing.Size(169, 40);
            this.txtEIN.TabIndex = 17;
            this.txtEIN.TextChanged += new System.EventHandler(this.txtEIN_TextChanged);
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtPostalCode.Location = new System.Drawing.Point(142, 270);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(251, 40);
            this.txtPostalCode.TabIndex = 15;
            this.txtPostalCode.TextChanged += new System.EventHandler(this.txtPostalCode_TextChanged);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(142, 210);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(251, 40);
            this.txtState.TabIndex = 13;
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(142, 150);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(251, 40);
            this.txtCity.TabIndex = 11;
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.Location = new System.Drawing.Point(142, 90);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(251, 40);
            this.txtAddress.TabIndex = 9;
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
            // 
            // txtProviderName
            // 
            this.txtProviderName.BackColor = System.Drawing.SystemColors.Window;
            this.txtProviderName.Location = new System.Drawing.Point(142, 30);
            this.txtProviderName.Name = "txtProviderName";
            this.txtProviderName.Size = new System.Drawing.Size(251, 40);
            this.txtProviderName.TabIndex = 7;
            this.txtProviderName.TextChanged += new System.EventHandler(this.txtProviderName_TextChanged);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(423, 104);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(74, 16);
            this.Label10.TabIndex = 22;
            this.Label10.Text = "TELEPHONE";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(423, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(30, 16);
            this.Label8.TabIndex = 18;
            this.Label8.Text = "EIN";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 284);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(27, 16);
            this.Label7.TabIndex = 16;
            this.Label7.Text = "ZIP";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 224);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(39, 16);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "STATE";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 164);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(30, 16);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "CITY";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 104);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(59, 16);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "ADDRESS";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(45, 16);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "NAME";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(130, 16);
            this.Label9.TabIndex = 20;
            this.Label9.Text = "COVERAGE PLAN";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddPlan,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddPlan
            // 
            this.mnuAddPlan.Index = 0;
            this.mnuAddPlan.Name = "mnuAddPlan";
            this.mnuAddPlan.Text = "Add New Plan";
            this.mnuAddPlan.Click += new System.EventHandler(this.mnuAddPlan_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddPlan
            // 
            this.cmdAddPlan.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddPlan.Location = new System.Drawing.Point(754, 29);
            this.cmdAddPlan.Name = "cmdAddPlan";
            this.cmdAddPlan.Size = new System.Drawing.Size(104, 24);
            this.cmdAddPlan.TabIndex = 1;
            this.cmdAddPlan.Text = "Add New Plan";
            this.cmdAddPlan.Click += new System.EventHandler(this.mnuAddPlan_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(364, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmCoverageProvider
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(818, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCoverageProvider";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ACA Coverage Setup";
            this.Load += new System.EventHandler(this.frmCoverageProvider_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCoverageProvider_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framPlan)).EndInit();
            this.framPlan.ResumeLayout(false);
            this.framPlan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1095B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framProvider)).EndInit();
            this.framProvider.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
        private FCButton cmdAddPlan;
    }
}
