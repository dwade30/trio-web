﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2AdditionalInfoData.
	/// </summary>
	partial class rptW2AdditionalInfoData
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2AdditionalInfoData));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.chkBox12 = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkBox14 = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkFederal = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkFICA = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkState = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkMedicare = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.chkSickPay = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFederal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFICA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMedicare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSickPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNumber,
            this.txtAmount,
            this.txtName});
            this.Detail.Height = 0.1979167F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // txtNumber
            // 
            this.txtNumber.Height = 0.1875F;
            this.txtNumber.Left = 0.0625F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
            this.txtNumber.Text = null;
            this.txtNumber.Top = 0F;
            this.txtNumber.Width = 0.5F;
            // 
            // txtAmount
            // 
            this.txtAmount.Height = 0.1875F;
            this.txtAmount.Left = 3.25F;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Style = "font-size: 8pt; ddo-char-set: 1";
            this.txtAmount.Text = null;
            this.txtAmount.Top = 0F;
            this.txtAmount.Width = 1.5F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1875F;
            this.txtName.Left = 0.75F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-size: 8pt; ddo-char-set: 1";
            this.txtName.Text = null;
            this.txtName.Top = 0F;
            this.txtName.Width = 2.4375F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuniName,
            this.txtCaption,
            this.txtDate,
            this.lblPage,
            this.txtTime});
            this.PageHeader.Height = 0.8333333F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0.0625F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-size: 8pt; ddo-char-set: 1";
            this.txtMuniName.Text = "MuniName";
            this.txtMuniName.Top = 0.0625F;
            this.txtMuniName.Width = 2F;
            // 
            // txtCaption
            // 
            this.txtCaption.Height = 0.1875F;
            this.txtCaption.Left = 0.0625F;
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center";
            this.txtCaption.Text = "Account";
            this.txtCaption.Top = 0.0625F;
            this.txtCaption.Width = 7.8125F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 6.75F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.txtDate.Text = "Date";
            this.txtDate.Top = 0.0625F;
            this.txtDate.Width = 1.0625F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.75F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.lblPage.Text = "Label5";
            this.lblPage.Top = 0.25F;
            this.lblPage.Width = 1.0625F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0.0625F;
            this.txtTime.Name = "txtTime";
            this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
            this.txtTime.Style = "font-family: \'Courier\'; font-size: 8pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 1.375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.lblAmount,
            this.Label2,
            this.Line1,
            this.Label6,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.txtCode,
            this.txtDescription,
            this.chkBox12,
            this.chkBox14,
            this.chkFederal,
            this.chkFICA,
            this.chkState,
            this.chkMedicare,
            this.chkSickPay,
            this.Label7,
            this.Binder});
            this.GroupHeader1.DataField = "Binder";
            this.GroupHeader1.Height = 1.34375F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.Shape1.Height = 0.5416667F;
            this.Shape1.Left = 0.03125F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 0F;
            this.Shape1.Width = 7.90625F;
            // 
            // lblAmount
            // 
            this.lblAmount.Height = 0.1666667F;
            this.lblAmount.HyperLink = null;
            this.lblAmount.Left = 3.25F;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.lblAmount.Text = "Amount";
            this.lblAmount.Top = 1.125F;
            this.lblAmount.Width = 0.8125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.0625F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label2.Text = "Number";
            this.Label2.Top = 1.125F;
            this.Label2.Width = 0.5625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.03125F;
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.291667F;
            this.Line1.Width = 7.9375F;
            this.Line1.X1 = 0.03125F;
            this.Line1.X2 = 7.96875F;
            this.Line1.Y1 = 1.291667F;
            this.Line1.Y2 = 1.291667F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1666667F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.75F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label6.Text = "Employee";
            this.Label6.Top = 1.125F;
            this.Label6.Width = 1.71875F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1666667F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.8125F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label8.Text = "Description";
            this.Label8.Top = 0.08333334F;
            this.Label8.Width = 1.5F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1666667F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 2.375F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label9.Text = "Box 12";
            this.Label9.Top = 0.08333334F;
            this.Label9.Width = 0.59375F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1666667F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 3.03125F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label10.Text = "Box 14";
            this.Label10.Top = 0.08333334F;
            this.Label10.Width = 0.53125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1666667F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 3.625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label11.Text = "Federal";
            this.Label11.Top = 0.08333334F;
            this.Label11.Width = 0.8125F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1666667F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 4.4375F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label12.Text = "FICA";
            this.Label12.Top = 0.08333334F;
            this.Label12.Width = 0.8125F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1666667F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 5.28125F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label13.Text = "State";
            this.Label13.Top = 0.08333334F;
            this.Label13.Width = 0.8125F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1666667F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.125F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label14.Text = "Medicare";
            this.Label14.Top = 0.08333334F;
            this.Label14.Width = 0.8125F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1666667F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 7F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label15.Text = "Sick Pay";
            this.Label15.Top = 0.08333334F;
            this.Label15.Width = 0.8125F;
            // 
            // txtCode
            // 
            this.txtCode.Height = 0.1666667F;
            this.txtCode.Left = 0.15625F;
            this.txtCode.Name = "txtCode";
            this.txtCode.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
            this.txtCode.Text = null;
            this.txtCode.Top = 0.3333333F;
            this.txtCode.Width = 0.5625F;
            // 
            // txtDescription
            // 
            this.txtDescription.Height = 0.1666667F;
            this.txtDescription.Left = 0.8125F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
            this.txtDescription.Text = null;
            this.txtDescription.Top = 0.3333333F;
            this.txtDescription.Width = 1.5F;
            // 
            // chkBox12
            // 
            this.chkBox12.Height = 0.125F;
            this.chkBox12.Left = 2.53125F;
            this.chkBox12.Name = "chkBox12";
            this.chkBox12.Style = "";
            this.chkBox12.Text = null;
            this.chkBox12.Top = 0.3333333F;
            this.chkBox12.Width = 0.1875F;
            // 
            // chkBox14
            // 
            this.chkBox14.Height = 0.125F;
            this.chkBox14.Left = 3.1875F;
            this.chkBox14.Name = "chkBox14";
            this.chkBox14.Style = "";
            this.chkBox14.Text = null;
            this.chkBox14.Top = 0.3333333F;
            this.chkBox14.Width = 0.1875F;
            // 
            // chkFederal
            // 
            this.chkFederal.Height = 0.125F;
            this.chkFederal.Left = 3.8125F;
            this.chkFederal.Name = "chkFederal";
            this.chkFederal.Style = "";
            this.chkFederal.Text = null;
            this.chkFederal.Top = 0.3333333F;
            this.chkFederal.Width = 0.1875F;
            // 
            // chkFICA
            // 
            this.chkFICA.Height = 0.125F;
            this.chkFICA.Left = 4.59375F;
            this.chkFICA.Name = "chkFICA";
            this.chkFICA.Style = "";
            this.chkFICA.Text = null;
            this.chkFICA.Top = 0.3333333F;
            this.chkFICA.Width = 0.1875F;
            // 
            // chkState
            // 
            this.chkState.Height = 0.125F;
            this.chkState.Left = 5.4375F;
            this.chkState.Name = "chkState";
            this.chkState.Style = "";
            this.chkState.Text = null;
            this.chkState.Top = 0.3333333F;
            this.chkState.Width = 0.1875F;
            // 
            // chkMedicare
            // 
            this.chkMedicare.Height = 0.125F;
            this.chkMedicare.Left = 6.28125F;
            this.chkMedicare.Name = "chkMedicare";
            this.chkMedicare.Style = "";
            this.chkMedicare.Text = null;
            this.chkMedicare.Top = 0.3333333F;
            this.chkMedicare.Width = 0.1875F;
            // 
            // chkSickPay
            // 
            this.chkSickPay.Height = 0.125F;
            this.chkSickPay.Left = 7.15625F;
            this.chkSickPay.Name = "chkSickPay";
            this.chkSickPay.Style = "";
            this.chkSickPay.Text = null;
            this.chkSickPay.Top = 0.3333333F;
            this.chkSickPay.Width = 0.1875F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1666667F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.15625F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label7.Text = "Code";
            this.Label7.Top = 0.08333334F;
            this.Label7.Width = 0.59375F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Binder
            // 
            this.Binder.Height = 0.2F;
            this.Binder.Left = 0.0619998F;
            this.Binder.Name = "Binder";
            this.Binder.Text = "Binder";
            this.Binder.Top = 0.657F;
            this.Binder.Visible = false;
            this.Binder.Width = 1F;
            // 
            // rptW2AdditionalInfoData
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.2222222F;
            this.PageSettings.Margins.Right = 0.2222222F;
            this.PageSettings.Margins.Top = 0.05F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFederal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFICA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMedicare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSickPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkBox12;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkBox14;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFederal;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFICA;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkState;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkSickPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
    }
}
