//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW3C : BaseForm
	{
		public frmW3C()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW3C InstancePtr
		{
			get
			{
				return (frmW3C)Sys.GetInstance(typeof(frmW3C));
			}
		}

		protected frmW3C _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolReturn;
		private bool boolReturnAdjust;
		private string strReturnDate = "";
		private string strExplanation = "";
		private int intPayer;
		private string strContact = "";
		private string strEmail = "";
		private string strPhone = "";
		private string strFax = "";
		private string strIncorrectEIN = "";
		private string strIncorrectEstablishment = "";
		private string strIncorrectStateID = "";
		private string strEstablishment = "";
		private clsW2C clsW2CSummary;
		private string strTitle = "";

		public bool Init(clsW2C clsW)
		{
			bool Init = false;
			Init = false;
			clsW2CSummary = clsW;
			boolReturn = false;
			this.Show(FCForm.FormShowEnum.Modal);
			if (boolReturn)
			{
				clsW.EmploymentReturnAdjustment = boolReturnAdjust;
				clsW.EmpReturnDate = strReturnDate;
				clsW.Email = strEmail;
				clsW.ContactPerson = strContact;
				clsW.PhoneNumber = strPhone;
				clsW.FaxNumber = strFax;
				clsW.Explanation = strExplanation;
				clsW.IncorrectEIN = strIncorrectEIN;
				clsW.IncorrectEstablishment = strIncorrectEstablishment;
				clsW.IncorrectStateID = strIncorrectStateID;
				clsW.Establishment = strEstablishment;
				clsW.ContactTitle = strTitle;
			}
			Init = boolReturn;
			return Init;
		}

		private void frmW3C_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmW3C_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW3C properties;
			//frmW3C.FillStyle	= 0;
			//frmW3C.ScaleWidth	= 9300;
			//frmW3C.ScaleHeight	= 7605;
			//frmW3C.LinkTopic	= "Form2";
			//frmW3C.LockControls	= -1  'True;
			//frmW3C.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadData();
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp = "";
				rsLoad.OpenRecordset("select * from tblw3information", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					txtContactPerson.Text = FCConvert.ToString(rsLoad.Get_Fields_String("ContactPerson"));
					txtEmail.Text = FCConvert.ToString(rsLoad.Get_Fields("EMail"));
					txtTitle.Text = FCConvert.ToString(rsLoad.Get_Fields("contacttitle"));
					strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("PhoneNumber"));
					strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Right("0000000000" + strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
					txtPhoneNumber.Text = strTemp;
					strTemp = FCConvert.ToString(rsLoad.Get_Fields("faxnumber"));
					strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Right("0000000000" + strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
					txtFaxNumber.Text = strTemp;
					// optKindOfPayer(Val(rsLoad.Fields("KindOfPayer"))).Value = True
					if (Conversion.Val(rsLoad.Get_Fields("kindofpayer")) != 6)
					{
                        if (FCConvert.ToInt32(rsLoad.Get_Fields_Int16("KindOfPayer")) != 7)
                        {
                            cmbKindOfPayer.SelectedIndex = FCConvert.ToInt32(rsLoad.Get_Fields_Int16("KindOfPayer"));
                        }
                        else
                        {
                            cmbKindOfPayer.SelectedIndex = 6;
                        }
					}
					else
					{
						// chkThirdPartySickPay.Value = vbChecked
						cmbKindOfPayer.Text = "941";
					}
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("kindofemployer"))) == "N")
					{
						cmbFedGovt.Text = "None Apply";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("kindofemployer"))) == "T")
					{
						cmbFedGovt.Text = "501c non-govt";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("kindofemployer"))) == "S")
					{
						cmbFedGovt.Text = "State/Local non 501c";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("kindofemployer"))) == "Y")
					{
						cmbFedGovt.Text = "State/local 501c";
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("kindofemployer"))) == "F")
					{
						cmbFedGovt.Text = "Federal govt";
					}
					else
					{
						cmbFedGovt.Text = "State/Local non 501c";
					}
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ThirdPartySickPay")))
					{
						chkThirdPartySickPay.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkThirdPartySickPay.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + " in LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			boolReturn = false;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				boolReturn = true;
				Close();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				int x;
				rsSave.OpenRecordset("select * from tblw3information", "twpy0000.vb1");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("ContactPerson", txtContactPerson.Text);
				rsSave.Set_Fields("ContactTitle", txtTitle.Text);
				strTitle = txtTitle.Text;
				clsW2CSummary.ContactTitle = txtTitle.Text;
				strContact = txtContactPerson.Text;
				clsW2CSummary.ContactPerson = strContact;
				rsSave.Set_Fields("Email", txtEmail.Text);
				clsW2CSummary.Email = txtEmail.Text;
				strEmail = txtEmail.Text;
				string strTemp;
				strTemp = fecherFoundation.Strings.Trim(Strings.Replace(txtPhoneNumber.Text, "(", "", 1, -1, CompareConstants.vbTextCompare));
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare));
				if (strTemp != string.Empty)
				{
					rsSave.Set_Fields("phonenumber", txtPhoneNumber.Text);
				}
				else
				{
					rsSave.Set_Fields("phonenumber", "");
				}
				strPhone = txtPhoneNumber.Text;
				clsW2CSummary.PhoneNumber = strPhone;
				strTemp = fecherFoundation.Strings.Trim(Strings.Replace(txtFaxNumber.Text, "(", "", 1, -1, CompareConstants.vbTextCompare));
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare));
				if (strTemp != string.Empty)
				{
					rsSave.Set_Fields("faxnumber", txtFaxNumber.Text);
				}
				else
				{
					rsSave.Set_Fields("faxnumber", "");
				}
				strFax = txtFaxNumber.Text;
				clsW2CSummary.FaxNumber = strFax;
				for (x = 0; x <= 7; x++)
				{
					if (x != 6)
					{
                        if (cmbKindOfPayer.SelectedIndex == x)
                        {
                            intPayer = x;
                        }
                        else if (cmbKindOfPayer.SelectedIndex == 6)
                        {
                            intPayer = 7;
                        }
					}
				}
				// x
				rsSave.Set_Fields("KindOfPayer", intPayer);
				// Dim strTemp As String
				if (cmbFedGovt.Text == "None Apply")
				{
					strTemp = "N";
				}
				else if (cmbFedGovt.Text == "State/local 501c")
				{
					strTemp = "T";
				}
				else if (cmbFedGovt.Text == "State/Local non 501c")
				{
					strTemp = "S";
				}
				else if (cmbFedGovt.Text == "Federal govt")
				{
					strTemp = "F";
				}
				else if (cmbFedGovt.Text == "State / Local non 501c")
				{
					strTemp = "Y";
				}
				else
				{
					strTemp = "S";
				}
				rsSave.Set_Fields("KindOfEmployer", strTemp);
				clsW2CSummary.KindOfEmployer = strTemp;
				if (chkThirdPartySickPay.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("ThirdPartySickPay", true);
				}
				else
				{
					rsSave.Set_Fields("ThirdPartySickPay", false);
				}
				clsW2CSummary.ThirdPartySick = chkThirdPartySickPay.CheckState == Wisej.Web.CheckState.Checked;
				if (chkEmpReturnAdjusted.CheckState == CheckState.Checked)
				{
					boolReturnAdjust = true;
					clsW2CSummary.EmploymentReturnAdjustment = true;
					if (Information.IsDate(t2kAdjustment.Text))
					{
						strReturnDate = t2kAdjustment.Text;
					}
					else
					{
						strReturnDate = "";
					}
				}
				else
				{
					clsW2CSummary.EmploymentReturnAdjustment = false;
					boolReturnAdjust = false;
					strReturnDate = "";
				}
				strIncorrectEIN = txtIncorrectEIN.Text;
				strIncorrectEstablishment = txtEstablishmentNumber.Text;
				strIncorrectStateID = txtIncorrectStateID.Text;
				strEstablishment = txtEstablishment.Text;
				strExplanation = txtDecreases.Text;
				clsW2CSummary.Establishment = strEstablishment;
				clsW2CSummary.IncorrectEstablishment = strIncorrectEstablishment;
				clsW2CSummary.IncorrectStateID = strIncorrectStateID;
				clsW2CSummary.TypeOfPayer = intPayer;
				clsW2CSummary.IncorrectEIN = strIncorrectEIN;
				clsW2CSummary.FaxNumber = strFax;
				clsW2CSummary.PhoneNumber = strPhone;
				rsSave.Update();
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
