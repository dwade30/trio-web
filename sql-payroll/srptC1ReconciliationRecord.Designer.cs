﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1ReconciliationlRecord.
	/// </summary>
	partial class srptC1ReconciliationlRecord
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1ReconciliationlRecord));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDatePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label3,
				this.txtDatePaid,
				this.Label8,
				this.txtWH,
				this.Label12,
				this.txtPayment
			});
			this.Detail.Height = 0.4895833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: center";
			this.Label1.Text = "Reconciliation Record";
			this.Label1.Top = 0.02083333F;
			this.Label1.Width = 3.916667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Date Paid";
			this.Label3.Top = 0.28125F;
			this.Label3.Width = 0.90625F;
			// 
			// txtDatePaid
			// 
			this.txtDatePaid.Height = 0.1666667F;
			this.txtDatePaid.Left = 1.072917F;
			this.txtDatePaid.Name = "txtDatePaid";
			this.txtDatePaid.Style = "text-align: left";
			this.txtDatePaid.Text = null;
			this.txtDatePaid.Top = 0.28125F;
			this.txtDatePaid.Width = 1.177083F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Withholding Amount";
			this.Label8.Top = 0.28125F;
			this.Label8.Width = 1.59375F;
			// 
			// txtWH
			// 
			this.txtWH.Height = 0.1666667F;
			this.txtWH.Left = 4.072917F;
			this.txtWH.Name = "txtWH";
			this.txtWH.Style = "text-align: right";
			this.txtWH.Text = null;
			this.txtWH.Top = 0.28125F;
			this.txtWH.Width = 0.9895833F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "Payment Amount";
			this.Label12.Top = 0.28125F;
			this.Label12.Width = 1.34375F;
			// 
			// txtPayment
			// 
			this.txtPayment.Height = 0.1666667F;
			this.txtPayment.Left = 6.572917F;
			this.txtPayment.Name = "txtPayment";
			this.txtPayment.Style = "text-align: right";
			this.txtPayment.Text = null;
			this.txtPayment.Top = 0.28125F;
			this.txtPayment.Width = 0.8020833F;
			// 
			// srptC1ReconciliationlRecord
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment;
	}
}
