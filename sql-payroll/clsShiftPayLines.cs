//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsShiftPayLines
	{
		//=========================================================
		private int lngBreakdownID;
		private int lngPayCatID;
		private modSchedule.ShiftFactorType intFactorOrUnits;
		private double dblFactor;
		private double dblAfterUnits;
		private double dblUpToUnits;
		private double dblMaxUnits;
		private modSchedule.ShiftPerType intMaxType;
		private bool boolUnused;
		private int lngShiftID;

		public int ShiftID
		{
			set
			{
				lngShiftID = value;
			}
			get
			{
				int ShiftID = 0;
				ShiftID = lngShiftID;
				return ShiftID;
			}
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public int BreakdownID
		{
			set
			{
				lngBreakdownID = value;
			}
			get
			{
				int BreakdownID = 0;
				BreakdownID = lngBreakdownID;
				return BreakdownID;
			}
		}

		public int PayCategory
		{
			set
			{
				lngPayCatID = value;
			}
			get
			{
				int PayCategory = 0;
				PayCategory = lngPayCatID;
				return PayCategory;
			}
		}

		public modSchedule.ShiftFactorType FactorOrUnits
		{
			set
			{
				intFactorOrUnits = value;
			}
			get
			{
				modSchedule.ShiftFactorType FactorOrUnits = (modSchedule.ShiftFactorType)0;
				FactorOrUnits = intFactorOrUnits;
				return FactorOrUnits;
			}
		}

		public double FactorUnits
		{
			set
			{
				dblFactor = value;
			}
			get
			{
				double FactorUnits = 0;
				FactorUnits = dblFactor;
				return FactorUnits;
			}
		}

		public double AfterUnits
		{
			set
			{
				dblAfterUnits = value;
			}
			get
			{
				double AfterUnits = 0;
				AfterUnits = dblAfterUnits;
				return AfterUnits;
			}
		}

		public double UpToUnits
		{
			set
			{
				dblUpToUnits = value;
			}
			get
			{
				double UpToUnits = 0;
				UpToUnits = dblUpToUnits;
				return UpToUnits;
			}
		}

		public double MaxUnits
		{
			set
			{
				dblMaxUnits = value;
			}
			get
			{
				double MaxUnits = 0;
				MaxUnits = dblMaxUnits;
				return MaxUnits;
			}
		}

		public modSchedule.ShiftPerType MaxType
		{
			set
			{
				intMaxType = value;
			}
			get
			{
				modSchedule.ShiftPerType MaxType = (modSchedule.ShiftPerType)0;
				MaxType = intMaxType;
				return MaxType;
			}
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		public void LoadPayLine(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from shiftbreakdowns where ID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					lngBreakdownID = lngID;
					lngPayCatID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PayCatID"))));
					intFactorOrUnits = (modSchedule.ShiftFactorType)FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("FactorOrUnits"))));
					dblFactor = Conversion.Val(rsLoad.Get_Fields_Double("FactorUnits"));
					dblAfterUnits = Conversion.Val(rsLoad.Get_Fields_Double("AfterUnits"));
					dblUpToUnits = Conversion.Val(rsLoad.Get_Fields_Double("UptoUnits"));
					dblMaxUnits = Conversion.Val(rsLoad.Get_Fields_Double("MaxUnits"));
					intMaxType = (modSchedule.ShiftPerType)FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("MaxType"))));
					lngShiftID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ShiftID"))));
					boolUnused = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadPayLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Delete()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "Delete from shiftbreakdowns where breakdownid = " + FCConvert.ToString(lngBreakdownID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				boolUnused = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Delete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool Save()
		{
			bool Save = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Save = false;
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!boolUnused)
				{
					strSQL = "select * from shiftbreakdowns where breakdownid = " + FCConvert.ToString(lngBreakdownID);
					rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("PayCatID", lngPayCatID);
					rsSave.Set_Fields("FactorOrUnits", intFactorOrUnits);
					rsSave.Set_Fields("FactorUnits", dblFactor);
					rsSave.Set_Fields("AfterUnits", dblAfterUnits);
					rsSave.Set_Fields("UpToUnits", dblUpToUnits);
					rsSave.Set_Fields("MaxUnits", dblMaxUnits);
					rsSave.Set_Fields("MaxType", intMaxType);
					rsSave.Set_Fields("ShiftID", lngShiftID);
					rsSave.Update();
					lngBreakdownID = FCConvert.ToInt32(rsSave.Get_Fields("Breakdownid"));
				}
				else
				{
					Delete();
				}
				Save = true;
				return Save;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Save;
		}
	}
}
