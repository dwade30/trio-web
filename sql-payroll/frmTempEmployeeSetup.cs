//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmTempEmployeeSetup : BaseForm
	{
		public frmTempEmployeeSetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:BSE #2564 Allow only numbers
            this.txtFederalTaxes.AllowOnlyNumericInput(true);
            this.txtStateTaxes.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTempEmployeeSetup InstancePtr
		{
			get
			{
				return (frmTempEmployeeSetup)Sys.GetInstance(typeof(frmTempEmployeeSetup));
			}
		}

		protected frmTempEmployeeSetup _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLDIRTY = 0;
		const int CNSTGRIDCOLROWNUM = 1;
		const int CNSTGRIDCOLFIXEDCOL = 2;
		const int CNSTGRIDCOLDEDNUMBER = 3;
		const int CNSTGRIDCOLDESCRIPTION = 4;
		const int CNSTGRIDCOLTAX = 5;
		const int CNSTGRIDCOLAMOUNT = 6;
		const int CNSTGRIDCOLAMOUNTTYPE = 7;
		const int CNSTGRIDCOLPERCENT = 8;
		const int CNSTGRIDCOLFREQ = 9;
		const int CNSTGRIDCOLLIMIT = 10;
		const int CNSTGRIDCOLLIMITTYPE = 11;
		const int CNSTGRIDCOLPRIORITY = 12;
		const int cnstgridcolID = 13;
		const int CNSTGRIDCOLDEDID = 14;
		const int CNSTGRIDCOLDEFAULT = 15;
		const int CNSTGRIDCOLLOAN = 16;
		const int CNSTGRIDCOLPARENTID = 17;
		const int CNSTGRIDMATCHCOLID = 12;
		const int CNSTGRIDMatchCOLRecordNumber = 1;
		const int CNSTGRIDMatchCOLMatchID = 2;
		const int CNSTGRIDMatchCOLDescription = 3;
		const int CNSTGRIDMatchCOLAmount = 4;
		const int CNSTGRIDMatchCOLAmountType = 5;
		const int CNSTGRIDMatchCOLChargeAccount = 6;
		const int CNSTGRIDMatchCOLMaxAmount = 7;
		const int CNSTGRIDMatchCOLLimitType = 8;
		const int CNSTGRIDMatchCOLTaxStatus = 9;
		const int CNSTGRIDMatchCOLFrequency = 10;
		const int CNSTGRIDMatchCOLStatus = 11;
		const int CNSTGRIDMatchCOLParentID = 13;
		private clsDRWrapper rsDeductionSetup = new clsDRWrapper();
		private string strCurEmployee = string.Empty;
		private bool boolDataChanged;
		private bool boolCantEdit;

		private void frmTempEmployeeSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTempEmployeeSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTempEmployeeSetup properties;
			//frmTempEmployeeSetup.FillStyle	= 0;
			//frmTempEmployeeSetup.ScaleWidth	= 9300;
			//frmTempEmployeeSetup.ScaleHeight	= 7710;
			//frmTempEmployeeSetup.LinkTopic	= "Form2";
			//frmTempEmployeeSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
			strCurEmployee = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
			rsDeductionSetup.OpenRecordset("Select * from tblDeductionSetup Order by ID", "TWPY0000.vb1");
			SetupDedGrid();
			LoadGrid();
			SetupMatchGrid();
			LoadMatchGrid();
			LoadTaxInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				Close();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			SaveInfo = false;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsSave = new clsDRWrapper();
			vsDeductions.Row = -1;
			vsMatch.Row = -1;
			if (NotValidData())
			{
				return SaveInfo;
			}
			try
			{
				rsSave.OpenRecordset("select * from deductionoverrides where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
				for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsDeductions.TextMatrix(intCounter, cnstgridcolID)) > 0)
					{
						if (rsSave.FindFirstRecord("ID", Conversion.Val(vsDeductions.TextMatrix(intCounter, cnstgridcolID))))
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("recordnumber", intCounter);
					rsSave.Set_Fields("EmployeeNumber", strCurEmployee);
					rsSave.Set_Fields("DeductionNumber", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDID));
					rsSave.Set_Fields("DeductionCode", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDNUMBER));
					rsSave.Set_Fields("Description", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDESCRIPTION));
					// rsSave.Fields("TaxStatusCode") = vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX)
					rsSave.Set_Fields("Priority", FCConvert.ToString(Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPRIORITY))));
					if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLAMOUNTTYPE)) == "Levy")
					{
						rsSave.Set_Fields("FrequencyCode", 1);
						rsSave.Set_Fields("Priority", 9);
					}
					else
					{
						rsSave.Set_Fields("FrequencyCode", FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDCOLFREQ))));
						rsSave.Set_Fields("Priority", FCConvert.ToString(Conversion.Val(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDCOLPRIORITY))));
					}
					// rsSave.Fields("frequencycode") = vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLFREQ)
					rsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT))));
					rsSave.Set_Fields("amounttype", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE));
					rsSave.Set_Fields("OrigID", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPARENTID));
					rsSave.Set_Fields("PercentType", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT));
					rsSave.Set_Fields("Limit", FCConvert.ToString(Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT))));
					rsSave.Set_Fields("limittype", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE));
					rsSave.Set_Fields("ICMALoan", vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN));
					rsSave.Update();
					vsDeductions.TextMatrix(intCounter, cnstgridcolID, FCConvert.ToString(rsSave.Get_Fields("ID")));
				}
				// intCounter
				rsSave.OpenRecordset("select * from matchoverrides where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsMatch.TextMatrix(intCounter, CNSTGRIDMATCHCOLID)) > 0)
					{
						if (rsSave.FindFirstRecord("ID", Conversion.Val(vsMatch.TextMatrix(intCounter, CNSTGRIDMATCHCOLID))))
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("recordnumber", intCounter);
					rsSave.Set_Fields("Employeenumber", strCurEmployee);
					rsSave.Set_Fields("Description", vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLDescription));
					rsSave.Set_Fields("Status", fecherFoundation.Strings.Trim(FCConvert.ToString(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDMatchCOLStatus))));
					rsSave.Set_Fields("DeductionCode", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDMatchCOLMatchID))));
					rsSave.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDMatchCOLAmount))));
					rsSave.Set_Fields("AmountType", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDMatchCOLAmountType));
					rsSave.Set_Fields("Account", vsMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, CNSTGRIDMatchCOLChargeAccount));
					rsSave.Set_Fields("MaxAmount", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDMatchCOLMaxAmount))));
					rsSave.Set_Fields("DollarsHours", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDMatchCOLLimitType))));
					rsSave.Set_Fields("FrequencyCode", FCConvert.ToString(Conversion.Val(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, CNSTGRIDMatchCOLFrequency))));
					rsSave.Set_Fields("OrigID", FCConvert.ToString(Conversion.Val(vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLParentID))));
					rsSave.Update();
					vsMatch.TextMatrix(intCounter, CNSTGRIDMATCHCOLID, FCConvert.ToString(rsSave.Get_Fields("ID")));
				}
				// intCounter
				rsSave.OpenRecordset("select * from withholdingoverrides where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Employeenumber", strCurEmployee);
				rsSave.Set_Fields("AddFed", FCConvert.ToString(Conversion.Val(txtFederalTaxes.Text)));
				rsSave.Set_Fields("AddState", FCConvert.ToString(Conversion.Val(txtStateTaxes.Text)));
				rsSave.Set_Fields("FedDollarPercent", cboFedDollarPercent.Text);
				rsSave.Set_Fields("StateDollarPercent", cboStateDollarPercent.Text);
				rsSave.Update();
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + " in SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return SaveInfo;
			}
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			double dblAmtPercentTotal;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			dblAmtPercentTotal = 0;
			for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
			{
				if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent")
				{
					dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
				}
				if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN).Length > 3)
				{
					MessageBox.Show("ICMA loan numbers cannot be greater than 3 digits", "Bad Loan Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return NotValidData;
					NotValidData = true;
				}
			}
			if (dblAmtPercentTotal > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				NotValidData = true;
				return NotValidData;
			}
			if (Conversion.Val(txtFederalTaxes.Text) > 100 && cboFedDollarPercent.Text == "Percent")
			{
				NotValidData = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtFederalTaxes.Focus();
				return NotValidData;
			}
			if (Conversion.Val(txtStateTaxes.Text) > 100 && cboStateDollarPercent.Text == "Percent")
			{
				NotValidData = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStateTaxes.Focus();
				return NotValidData;
			}
			NotValidData = false;
			return NotValidData;
		}

		private void SetupDedGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsDeductions = new clsDRWrapper();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				// set grid properties
				vsDeductions.WordWrap = true;
				vsDeductions.Cols = 18;
				//FC:FINAL:DDU:#i2394 - changed fixedCols to 1 from 5 to delete extra space at the end of grid
				vsDeductions.FixedCols = 1;
				vsDeductions.FixedRows = 1;
				vsDeductions.ColHidden(CNSTGRIDCOLDIRTY, true);
				vsDeductions.ColHidden(CNSTGRIDCOLROWNUM, true);
				vsDeductions.ColHidden(CNSTGRIDCOLPARENTID, true);
				vsDeductions.ColHidden(cnstgridcolID, true);
				vsDeductions.ColHidden(CNSTGRIDCOLDEDID, true);
				vsDeductions.ColHidden(CNSTGRIDCOLDEFAULT, true);
				vsDeductions.ColHidden(CNSTGRIDCOLPRIORITY, true);
				vsDeductions.ColHidden(CNSTGRIDCOLTAX, true);
				//vsDeductions.RowHeight(0) *= 2;
				modGlobalRoutines.CenterGridCaptions(ref vsDeductions);
				// set column 0 properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDIRTY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDIRTY, 400);
				// set column cnstgridcolrownum properties
				vsDeductions.ColAlignment(CNSTGRIDCOLROWNUM, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLROWNUM, 400);
				// set column cnstgridcoldednumber properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDEDNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDEDNUMBER, 500);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDEDNUMBER, "Ded");
				rsDeductions.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// This forces this column to work as a combo box
					// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
					int intCounter2;
					for (intCounter2 = 1; intCounter2 <= (rsDeductions.RecordCount()); intCounter2++)
					{
						if (intCounter2 == 1)
						{
							vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER, vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						else
						{
							vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER, vsDeductions.ColComboList(CNSTGRIDCOLDEDNUMBER) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						rsDeductions.MoveNext();
					}
				}
				// set column cnstgridcoldescription properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDESCRIPTION, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDESCRIPTION, 1500);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				// set column cnstgridcoltax properties
				// vsDeductions.ColAlignment(CNSTGRIDCOLTAX) = flexAlignLeftCenter
				// vsDeductions.ColWidth(CNSTGRIDCOLTAX) = 300
				// vsDeductions.TextMatrix(0, CNSTGRIDCOLTAX) = "Tax"
				rsDeductions.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							vsDeductions.ColComboList(CNSTGRIDCOLTAX, vsDeductions.ColComboList(CNSTGRIDCOLTAX) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						else
						{
							vsDeductions.ColComboList(CNSTGRIDCOLTAX, vsDeductions.ColComboList(CNSTGRIDCOLTAX) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("TaxStatusCode") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))) != "" ? "\t" + rsDeductions.Get_Fields("Description") : ""));
						}
						rsDeductions.MoveNext();
					}
				}
				vsDeductions.ColComboList(CNSTGRIDCOLPERCENT, "Gross|Net");
				// set column cnstgridcolamount properties
				vsDeductions.ColAlignment(CNSTGRIDCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLAMOUNT, 1100);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLAMOUNT, "Amount");
				// vsDeductions.ColFormat(CNSTGRIDCOLAMOUNT) = "(#,###.00)"
                //FC:FINAL:BSE #2564 set allowed keys for client side check
                vsDeductions.ColAllowedKeys(CNSTGRIDCOLAMOUNT, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				// set column cnstgridcolamounttype properties
				vsDeductions.ColAlignment(CNSTGRIDCOLAMOUNTTYPE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLAMOUNTTYPE, 800);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLAMOUNTTYPE, "Amount Type");
				vsDeductions.ColComboList(CNSTGRIDCOLAMOUNTTYPE, "#1;Dollars|#2;Percent|#3;Levy");
				// set column cnstgridcolpercent properties
				vsDeductions.ColAlignment(CNSTGRIDCOLPERCENT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLPERCENT, 600);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLPERCENT, " % of");
				// vsDeductions.ColComboList(cnstgridcolpercent, "#1;Gross"
				// will add in the NET option for version 84
				// vsDeductions.ColComboList(cnstgridcolpercent, "#1;Gross|#2;Net"
				// set column cnstgridcolfreq properties
				vsDeductions.ColAlignment(CNSTGRIDCOLFREQ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLFREQ, 470);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLFREQ, "Freq");
				rsDeductions.OpenRecordset("Select * from tblFrequencyCodes order by ID", "TWPY0000.vb1");
				if (!rsDeductions.EndOfFile())
				{
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// makes this column work like a combo box.
					// this fills the box with the values from the tblFrequencyCodes table
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						switch (rsDeductions.Get_Fields_Int32("ID"))
						{
							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 8:
							case 9:
							case 10:
							case 11:
							case 12:
							case 13:
							case 14:
							case 15:
							case 16:
							case 17:
							case 18:
							case 19:
							case 20:
							case 21:
								{
									if (intCounter == 1)
									{
										vsDeductions.ColComboList(CNSTGRIDCOLFREQ, vsDeductions.ColComboList(CNSTGRIDCOLFREQ) + "#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("FrequencyCode") + "\t" + rsDeductions.Get_Fields("Description"));
									}
									else
									{
										vsDeductions.ColComboList(CNSTGRIDCOLFREQ, vsDeductions.ColComboList(CNSTGRIDCOLFREQ) + "|#" + rsDeductions.Get_Fields("ID") + ";" + rsDeductions.Get_Fields("FrequencyCode") + "\t" + rsDeductions.Get_Fields("Description"));
									}
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
						rsDeductions.MoveNext();
					}
				}
				// set column cnstgridcollimit properties
				vsDeductions.ColAlignment(CNSTGRIDCOLLIMIT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLLIMIT, 1100);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLIMIT, "Limit");
				vsDeductions.ColFormat(CNSTGRIDCOLLIMIT, "#,##0.00;(#,###.00)");
				// set column cnstgridcollimittype properties
				vsDeductions.ColAlignment(CNSTGRIDCOLLIMITTYPE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLLIMITTYPE, 900);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLIMITTYPE, "Limit Type");
				vsDeductions.ColComboList(CNSTGRIDCOLLIMITTYPE, "#1;Life|#2;Fiscal|#3;Calendar|#4;Quarter|#5;Month|#6;Period|#7;% Gross");
				// set column cnstgridcolpriority properties
				vsDeductions.ColAlignment(CNSTGRIDCOLPRIORITY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLPRIORITY, 650);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLPRIORITY, "Priority");
				// set column 14 properties
				vsDeductions.ColAlignment(CNSTGRIDCOLDEFAULT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDeductions.ColWidth(CNSTGRIDCOLDEFAULT, 700);
				vsDeductions.TextMatrix(0, CNSTGRIDCOLDEFAULT, "Default");
				vsDeductions.ColDataType(CNSTGRIDCOLDEFAULT, FCGrid.DataTypeSettings.flexDTBoolean);
				// icma loan number
				vsDeductions.TextMatrix(0, CNSTGRIDCOLLOAN, "ICMA Loan");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + " in SetupDedGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTempEmployeeSetup_Resize(object sender, System.EventArgs e)
		{
			vsDeductions.RowHeight(0, FCConvert.ToInt32(vsDeductions.HeightOriginal * 0.17));
			vsDeductions.ColWidth(CNSTGRIDCOLFIXEDCOL, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.025));
			vsDeductions.ColWidth(CNSTGRIDCOLDEDNUMBER, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.05));
			vsDeductions.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.14));
			vsDeductions.ColWidth(CNSTGRIDCOLTAX, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.05));
			vsDeductions.ColWidth(CNSTGRIDCOLAMOUNT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLAMOUNTTYPE, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLPERCENT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.07));
			vsDeductions.ColWidth(CNSTGRIDCOLFREQ, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.06));
			vsDeductions.ColWidth(CNSTGRIDCOLLIMIT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.13));
			vsDeductions.ColWidth(CNSTGRIDCOLLIMITTYPE, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.09));
			vsDeductions.ColWidth(CNSTGRIDCOLPRIORITY, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.08));
			vsDeductions.ColWidth(CNSTGRIDCOLDEFAULT, FCConvert.ToInt32(vsDeductions.WidthOriginal * 0.08));
			ResizeMatchGrid();
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			// clears out the last opened form so that none open when
			// a new employee has been chosen
			modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
			frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			lblEmployeeNumber.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
			strCurEmployee = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
			LoadGrid();
		}

		private void vsDeductions_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsDeductions.Col == CNSTGRIDCOLAMOUNTTYPE)
			{
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLAMOUNT, Color.White);
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLFREQ, Color.White);
				// vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLLIMITTYPE) = vbWhite
				if (vsDeductions.EditText == "Percent")
				{
					if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
					{
						vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
					}
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
				}
				else if (vsDeductions.EditText == "Levy")
				{
					// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
					int intCounter;
					vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLFREQ);
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "3" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Levy")
						{
							if (vsDeductions.Row != intCounter)
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE, "Dollars");
								vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE);
								return;
							}
						}
					}
					// disable a few of the fields
					vsDeductions.Select(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE);
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLFREQ, "1");
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNT, "0.00");
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLAMOUNT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLFREQ, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
		}

		private void vsDeductions_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// allow the entry of a decimal point
			if (KeyAscii == 46)
				return;
			// allow the entry of a backspace
			if (KeyAscii == 8)
				return;
			if (vsDeductions.Col == CNSTGRIDCOLAMOUNT || vsDeductions.Col == CNSTGRIDCOLLIMIT)
			{
				// if the user is in the AMOUNT or LIMIT columns then do not
				// allow the entry of a character
				if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 45)
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsDeductions_KeyUpEvent(object sender, KeyEventArgs e)
		{
			// this variable is used in the form frmHelp to determine what data to show
			modGlobalVariables.Statics.gstrHelpFieldName = string.Empty;
			if (e.KeyCode == Keys.F9)
			{
				switch (vsDeductions.Col)
				{
					case CNSTGRIDCOLDEDNUMBER:
						{
							modGlobalVariables.Statics.gstrHelpFieldName = "DEDUCTIONCODE";
							break;
						}
					case CNSTGRIDCOLTAX:
						{
							modGlobalVariables.Statics.gstrHelpFieldName = "TAXSTATUSCODE";
							break;
						}
					case CNSTGRIDCOLAMOUNTTYPE:
						{
							modGlobalVariables.Statics.gstrHelpFieldName = "AMOUNTTYPE";
							break;
						}
					case CNSTGRIDCOLFREQ:
						{
							modGlobalVariables.Statics.gstrHelpFieldName = "FREQUENCYCODE";
							break;
						}
					case CNSTGRIDCOLLIMITTYPE:
						{
							modGlobalVariables.Statics.gstrHelpFieldName = "LIMITCODE";
							break;
						}
					default:
						{
							break;
						}
				}
				//end switch
				frmHelp.InstancePtr.Show();
			}
		}

		private void vsDeductions_RowColChange(object sender, System.EventArgs e)
		{
			// if there are values in the total grid do not allow the user
			// to alter the deduction codes for that record
			if (vsDeductions.Row >= 1)
			{
				vsDeductions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsDeductions.Editable = (FCConvert.ToInt32(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, vsDeductions.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) ? FCGrid.EditableSettings.flexEDKbdMouse : FCGrid.EditableSettings.flexEDNone;
				// need code here to lock this one row
				if (vsDeductions.Col == CNSTGRIDCOLDEDNUMBER)
				{
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsDeductions_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpText, vsDeductions.Row, CNSTGRIDCOLDEDNUMBER)) == string.Empty)
			{
				return;
			}
			boolDataChanged = true;
			if (vsDeductions.Col == CNSTGRIDCOLAMOUNT)
			{
				if (Conversion.Val(vsDeductions.EditText) == 0 && vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE) != "Levy")
				{
					MessageBox.Show("Changing the amount to zero will force the application to ALWAYS use the amount from the Deduction Setup Screen. If a value of zero is required, the user should change the frequency code to 'N'", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			if (vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLDEDNUMBER) == string.Empty)
			{
				vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpChecked, vsDeductions.Row, CNSTGRIDCOLDEFAULT, false);
				return;
			}
		}

		private void vsDeductions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsDeductions_ValidateEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				bool boolCancelSave = false;
				double dblAmtPercentTotal;
				dblAmtPercentTotal = 0;
				// vbPorter upgrade warning: dblPercentNetTotal As int	OnWrite(int, double)
				double dblPercentNetTotal;
				dblPercentNetTotal = 0;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNT)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "2")
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) != "Net")
							{
								if (intCounter == vsDeductions.Row)
								{
									dblAmtPercentTotal += Conversion.Val(vsDeductions.EditText);
								}
								else
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
							else
							{
								if (intCounter == vsDeductions.Row)
								{
									dblPercentNetTotal += Conversion.Val(vsDeductions.EditText);
								}
								else
								{
									dblPercentNetTotal += Conversion.Val(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
						}
					}
				}
				if (vsDeductions.Col == CNSTGRIDCOLAMOUNTTYPE)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (intCounter == vsDeductions.Row)
						{
							if (vsDeductions.EditText == "Percent" || vsDeductions.EditText == "2")
							{
								if (vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT) != "Net")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
								else
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
							}
						}
						else
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent" || vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "2")
							{
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) != "Net")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
								else
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
							}
						}
					}
				}
				if (vsDeductions.Col == CNSTGRIDCOLPERCENT)
				{
					for (intCounter = 1; intCounter <= (vsDeductions.Rows - 1); intCounter++)
					{
						if (intCounter == vsDeductions.Row)
						{
							if (vsDeductions.EditText == "Net")
							{
								dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2" && vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
								{
									e.Cancel = true;
									boolCancelSave = true;
									MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else
							{
								dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
							}
						}
						else
						{
							if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE) == "Percent")
							{
								if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) == "Net")
								{
									dblPercentNetTotal += FCConvert.ToInt32(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
									if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "2" && vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX) != "T")
									{
										e.Cancel = true;
										boolCancelSave = true;
										MessageBox.Show("Only taxable deductions can be of type % Net", "Incorrect Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								else if (vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT) == "Gross")
								{
									dblAmtPercentTotal += FCConvert.ToDouble(vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT));
								}
							}
						}
					}
					// intCounter
				}
				if (dblAmtPercentTotal > 100)
				{
					MessageBox.Show("Amount Totals For Percent Gross Cannot Be Greater Than 100", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					boolCancelSave = true;
				}
				if (dblPercentNetTotal > 100)
				{
					MessageBox.Show("Amount Total for Percent Net Cannot be Greater than 100", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					boolCancelSave = true;
				}
				if (vsDeductions.Col == CNSTGRIDCOLLOAN)
				{
					if (vsDeductions.EditText.Length > 3)
					{
						MessageBox.Show("ICMA loan numbers cannot be greater than 3 digits", "Bad Load Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						boolCancelSave = true;
					}
				}
				if (boolCancelSave == true)
				{
					if (vsDeductions.Col != CNSTGRIDCOLAMOUNTTYPE)
					{
						if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE)) == "Percent" || fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLAMOUNTTYPE)) == "2")
						{
							if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
							}
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
						}
						else
						{
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
					}
					else
					{
						if (vsDeductions.EditText == "Percent")
						{
							if (fecherFoundation.Strings.Trim(vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT)) == "")
							{
								vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "Gross");
							}
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, Color.White);
						}
						else
						{
							vsDeductions.TextMatrix(vsDeductions.Row, CNSTGRIDCOLPERCENT, "");
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsDeductions.Row, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadTaxInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				bool boolOverride;
				rsLoad.OpenRecordset("select * from WithholdingOverrides where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
				boolOverride = true;
				if (rsLoad.EndOfFile())
				{
					rsLoad.OpenRecordset("select * from tblEmployeeMaster where employeenumber = '" + strCurEmployee + "'", "twpy0000.vb1");
				}
				if (!rsLoad.EndOfFile())
				{
					txtFederalTaxes.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("AddFed")));
					if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("FedDollarPercent")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("FedDollarPercent"))) != string.Empty)
					{
						cboFedDollarPercent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("FedDollarPercent")));
					}
					txtStateTaxes.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("AddState")));
					if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("StateDollarPercent")) == false && fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("StateDollarPercent"))) != string.Empty)
					{
						cboStateDollarPercent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("StateDollarPercent")));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadTaxInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsDeductions = new clsDRWrapper();
				bool boolOverride;
				vsDeductions.Rows = 1;
				// get all of the records from the database
				rsDeductions.OpenRecordset("select * from deductionoverrides where employeenumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RecordNumber", "twpy0000.vb1");
				boolOverride = true;
				if (rsDeductions.EndOfFile())
				{
					rsDeductions.OpenRecordset("Select * from tblEmployeeDeductions Where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' Order by RecordNumber", "TWPY0000.vb1");
					boolOverride = false;
				}
				if (!rsDeductions.EndOfFile())
				{
					// this will make the recordcount property work correctly
					// THIS WILL MAKE THE RECORD COUNT
					rsDeductions.MoveLast();
					rsDeductions.MoveFirst();
					// set the number of rows in the grid
					vsDeductions.Rows = rsDeductions.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsDeductions.RecordCount()); intCounter++)
					{
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDIRTY, FCConvert.ToString(intCounter));
						if (intCounter != FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("RecordNumber") + " ")))
						{
							// rsTemp.Execute ("Update tblEmployeeDeductions Set RecordNumber = " & intCounter & " where ID = " & Trim(rsDeductions.Fields("ID") & " "))
							// rsDeductions.Edit
							// rsDeductions.Fields("recordnumber") = intCounter
							// rsDeductions.Update
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLROWNUM, FCConvert.ToString(intCounter));
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLROWNUM, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("RecordNumber") + " "));
						}
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDNUMBER, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionCode") + " "))));
						// ***Matthew
						// rsDeductionSetup.FindFirstRecord "DeductionNumber", rsDeductions.Fields("DeductionNumber")
						rsDeductionSetup.FindFirstRecord("ID", rsDeductions.Get_Fields_Int32("DeductionCode"));
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDESCRIPTION, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " ") == string.Empty ? fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Description") + " ") : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Description") + " ")));
						// WE WANT THE CODE FROM THE SETUP SCREEN.
						// vsDeductions.TextMatrix(intCounter, cnstgridcoltax) = IIf(Val(Trim(rsDeductions.Fields("TaxStatusCode") & " ")) = 0, Trim(rsDeductionSetup.Fields("TaxStatusCode") & " "), Trim(rsDeductions.Fields("TaxStatusCode") & " "))
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLTAX, modGlobalRoutines.GetDeductionTaxStatusCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("DeductionCode") + " "))))));
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLOAN, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("ICMALoan"))));
						if (FCConvert.ToString(rsDeductions.Get_Fields_String("AmountType")) == "Levy")
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("Amount"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("AmountType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("PercentType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLFREQ, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("FrequencyCode"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields_Double("Limit"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_String("LimitType"))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPRIORITY, FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("Priority"))));
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLAMOUNT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLFREQ, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNT, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Amount") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Amount") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Amount") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLAMOUNTTYPE, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " ") == string.Empty ? fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_String("AmountType") + " ") : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("AmountType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPERCENT, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("PercentType") + " ") == string.Empty ? "" : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("PercentType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLFREQ, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("FrequencyCode") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("FrequencyCode") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMIT, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_Double("Limit") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Double("Limit") + " ")))));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLLIMITTYPE, (fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " ") == string.Empty ? "" : fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("LimitType") + " ")));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPRIORITY, (FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " ")))) == 0 ? FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields("Priority") + " "))) : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("Priority") + " ")))));
							if (FCConvert.ToString(rsDeductions.Get_Fields("amounttype")) == "Dollars")
							{
								vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLPERCENT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
						}
						if (boolOverride)
						{
							vsDeductions.TextMatrix(intCounter, cnstgridcolID, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("ID") + " "));
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPARENTID, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_Int32("OrigID") + " "));
						}
						else
						{
							vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLPARENTID, fecherFoundation.Strings.Trim(rsDeductions.Get_Fields("ID") + " "));
							vsDeductions.TextMatrix(intCounter, cnstgridcolID, FCConvert.ToString(0));
						}
						vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEDID, fecherFoundation.Strings.Trim(rsDeductionSetup.Get_Fields_Int32("DeductionNumber") + " "));
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEDNUMBER, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLOAN, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// is this record using the default information
						// vsDeductions.TextMatrix(intCounter, CNSTGRIDCOLDEFAULT) = IsDefaultDedutionGrid(intCounter)
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLDEFAULT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMIT, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDCOLLIMITTYPE, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						rsDeductions.MoveNext();
					}
					//FC:FINAL:DDU:#i2066 - set fixed col to correct background color
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLFIXEDCOL, vsDeductions.Rows - 1, CNSTGRIDCOLFIXEDCOL, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLDESCRIPTION, vsDeductions.Rows - 1, CNSTGRIDCOLDESCRIPTION, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDCOLTAX, vsDeductions.Rows - 1, CNSTGRIDCOLTAX, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				boolDataChanged = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadMatchGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadMatchGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
				int intCount;
				string[,] aryDescription = null;
				clsDRWrapper rsMatch = new clsDRWrapper();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				bool boolOverride;
				vsMatch.Rows = 1;
				vsMatch.Select(0, 0);
				rsMatch.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					aryDescription = new string[rsMatch.RecordCount() + 1, 2 + 1];
					for (intCounter = 0; intCounter <= (rsMatch.RecordCount() - 1); intCounter++)
					{
						aryDescription[intCounter, 0] = FCConvert.ToString(rsMatch.Get_Fields("ID"));
						aryDescription[intCounter, 1] = FCConvert.ToString(rsMatch.Get_Fields("Description"));
						rsMatch.MoveNext();
					}
				}
				rsMatch.OpenRecordset("select * from matchoverrides where employeenumber = '" + strCurEmployee + "' order by recordnumber ", "twpy0000.vb1");
				boolOverride = true;
				if (rsMatch.EndOfFile())
				{
					boolOverride = false;
					rsMatch.OpenRecordset("Select * from tblEmployersMatch Where EmployeeNumber = '" + strCurEmployee + "' Order by RecordNumber", "TWPY0000.vb1");
				}
				// get all of the records from the database
				if (!rsMatch.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// set the number of rows in the grid
					vsMatch.Rows = rsMatch.RecordCount() + 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						vsMatch.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						// If intCounter <> Trim(rsMatch.Fields("RecordNumber") & " ") Then
						// rsMatch.Execute ("Update tblEmployersMatch Set RecordNumber = " & intCounter & " where ID = " & Trim(rsMatch.Fields("ID") & " "))
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLRecordNumber, FCConvert.ToString(intCounter));
						// Else
						// vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLRecordNumber) = Trim(rsMatch.Fields("RecordNumber") & " ")
						// End If
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLMatchID, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " "))));
						for (intCount = 0; intCount <= (Information.UBound(aryDescription, 1) - 1); intCount++)
						{
							if (FCConvert.ToDouble(aryDescription[intCount, 0]) == Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " ")))
							{
								break;
							}
						}
						if (intCount == Information.UBound(aryDescription, 1))
						{
							vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLDescription, string.Empty);
						}
						else
						{
							vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLDescription, aryDescription[intCount, 1]);
						}
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmount, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Amount") + " "))));
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmountType, fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("AmountType") + " "));
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLChargeAccount, fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Account") + " "));
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLMaxAmount, FCConvert.ToString((FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Double("MaxAmount") + " ")))) == 0 ? "0" : FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Double("MaxAmount") + " "))))));
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLLimitType, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("DollarsHours") + " "))));
						// vsMatch.TextMatrix(intCounter, 9) = IIf(Trim(rsMatch.Fields("TaxStatusCode") & " ") = vbNullString, Trim(rsMatch.Fields("TaxStatusCode") & " "), Trim(rsMatch.Fields("TaxStatusCode") & " "))
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLTaxStatus, modGlobalRoutines.GetDeductionTaxStatusCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsMatch.Get_Fields_Int32("DeductionCode") + " "))))));
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLFrequency, (fecherFoundation.Strings.Trim(rsMatch.Get_Fields("FrequencyCode") + " ") == string.Empty ? fecherFoundation.Strings.Trim(rsMatch.Get_Fields("FrequencyCode") + " ") : fecherFoundation.Strings.Trim(rsMatch.Get_Fields("FrequencyCode") + " ")));
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDMatchCOLTaxStatus, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLStatus, fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("Status") + " "));
						if (boolOverride)
						{
							vsMatch.TextMatrix(intCounter, CNSTGRIDMATCHCOLID, fecherFoundation.Strings.Trim(rsMatch.Get_Fields("ID") + " "));
							vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLParentID, FCConvert.ToString(Conversion.Val(rsMatch.Get_Fields("origid"))));
						}
						else
						{
							vsMatch.TextMatrix(intCounter, CNSTGRIDMATCHCOLID, FCConvert.ToString(0));
							vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLParentID, FCConvert.ToString(Conversion.Val(rsMatch.Get_Fields("ID"))));
						}
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDMatchCOLMatchID, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDMatchCOLLimitType, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDMatchCOLMaxAmount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, CNSTGRIDMatchCOLChargeAccount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						//vsMatch.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, CNSTGRIDMatchCOLFrequency, vsMatch.Rows - 1, CNSTGRIDMatchCOLFrequency, 9);
						rsMatch.MoveNext();
					}
					vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, CNSTGRIDMatchCOLDescription, vsMatch.Rows - 1, CNSTGRIDMatchCOLDescription, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				if (vsMatch.Rows > 2)
					vsMatch.Select(1, CNSTGRIDMatchCOLDescription);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ResizeMatchGrid()
		{
			vsMatch.ColWidth(0, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(1, vsMatch.WidthOriginal * 0);
			vsMatch.ColWidth(2, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.05));
			vsMatch.ColWidth(3, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.20));
			vsMatch.ColWidth(4, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.08));
			vsMatch.ColWidth(5, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.06));
			vsMatch.ColWidth(6, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.17));
			vsMatch.ColWidth(7, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.12));
			vsMatch.ColWidth(8, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.07));
			vsMatch.ColWidth(9, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.05));
			vsMatch.ColWidth(10, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.06));
			vsMatch.ColWidth(11, FCConvert.ToInt32(vsMatch.WidthOriginal * 0.08));
		}

		private void SetupMatchGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetupMatchGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strList = "";
				clsDRWrapper rsMatch = new clsDRWrapper();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				// set grid properties
				vsMatch.WordWrap = true;
				vsMatch.Cols = 14;
				//FC:FINAL:DDU:#i2394 - changed fixedCols to 1 from 2 to delete extra space at the end of grid
				vsMatch.FixedCols = 1;
				vsMatch.ColHidden(0, true);
				vsMatch.ColHidden(CNSTGRIDMatchCOLRecordNumber, true);
				vsMatch.ColHidden(CNSTGRIDMATCHCOLID, true);
				vsMatch.ColHidden(CNSTGRIDMatchCOLParentID, true);
				//vsMatch.RowHeight(0) *= 2;
				modGlobalRoutines.CenterGridCaptions(ref vsMatch);
				// set column 0 properties
				vsMatch.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(0, 400);
				// set column 1 properties
				vsMatch.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(1, 400);
				// set column 2 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLMatchID, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLMatchID, 500);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLMatchID, "Ded");
				rsMatch.OpenRecordset("Select * from tblDeductionSetup order by DeductionNumber", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// This forces this column to work as a combo box
					// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
					int intCounter2;
					for (intCounter2 = 1; intCounter2 <= (rsMatch.RecordCount()); intCounter2++)
					{
						if (intCounter2 == 1)
						{
							vsMatch.ColComboList(CNSTGRIDMatchCOLMatchID, vsMatch.ColComboList(CNSTGRIDMatchCOLMatchID) + "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("Description"))) != "" ? "\t" + rsMatch.Get_Fields("Description") : ""));
						}
						else
						{
							vsMatch.ColComboList(CNSTGRIDMatchCOLMatchID, vsMatch.ColComboList(CNSTGRIDMatchCOLMatchID) + "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields_Int32("DeductionNumber") + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("Description"))) != "" ? "\t" + rsMatch.Get_Fields("Description") : ""));
						}
						rsMatch.MoveNext();
					}
				}
				// set column 3 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLDescription, 1400);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLDescription, "Description");
				// set column 4 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLAmount, 1100);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLAmount, "Amount");
				vsMatch.ColFormat(CNSTGRIDMatchCOLAmount, "#,##0.00;(#,###.00)");
                //FC:FINAL:BSE #2564 set allowed keys for client side check
                vsMatch.ColAllowedKeys(CNSTGRIDMatchCOLAmount, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
				// set column 5 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLAmountType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLAmountType, 550);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLAmountType, "Type");
				vsMatch.ColComboList(CNSTGRIDMatchCOLAmountType, "#1;D" + "\t" + "Dollars|#2;%G" + "\t" + "% of Gross|#3;%D" + "\t" + "% of Deduction");
				// set column 6 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLChargeAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLChargeAccount, 1200);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLChargeAccount, "Charge Account");
				// set column 7 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLMaxAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLMaxAmount, 1300);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLMaxAmount, "Max Amount");
				vsMatch.ColFormat(CNSTGRIDMatchCOLMaxAmount, "#,##0.00;(#,###.00)");
				// set column 8 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLLimitType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLLimitType, 700);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLLimitType, "Type");
				vsMatch.ColComboList(CNSTGRIDMatchCOLLimitType, "#1;Dollars|#2;Hours");
				// set column 9 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLTaxStatus, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLTaxStatus, 450);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLTaxStatus, "Tax");
				rsMatch.OpenRecordset("Select * from tblTaxStatusCodes order by ID", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						if (intCounter == 3)
						{
							vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus, vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus) + "|#0; " + "\t" + "");
							vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus, vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus) + "|#00; " + "\t" + "   EXEMPT FROM");
							// vsMatch.ColComboList(9, vsMatch.ColComboList(9) & "|#000; " & vbTab & ""
						}
						if (intCounter == 1)
						{
							vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus, vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus) + "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " "));
						}
						else
						{
							vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus, vsMatch.ColComboList(CNSTGRIDMatchCOLTaxStatus) + "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("TaxStatusCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " "));
						}
						rsMatch.MoveNext();
					}
				}
				// set column 10 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLFrequency, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLFrequency, 500);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLFrequency, "Freq");
				rsMatch.OpenRecordset("Select * from tblFrequencyCodes order by ID", "TWPY0000.vb1");
				if (!rsMatch.EndOfFile())
				{
					rsMatch.MoveLast();
					rsMatch.MoveFirst();
					strList = "";
					// This forces this column to work as a combo box
					for (intCounter = 1; intCounter <= (rsMatch.RecordCount()); intCounter++)
					{
						if (intCounter == 1)
						{
							strList += "#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("FrequencyCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " ");
						}
						else
						{
							strList += "|#" + rsMatch.Get_Fields("ID") + ";" + rsMatch.Get_Fields("FrequencyCode") + "\t" + fecherFoundation.Strings.Trim(rsMatch.Get_Fields("Description") + " ");
						}
						rsMatch.MoveNext();
					}
				}
				vsMatch.ColComboList(CNSTGRIDMatchCOLFrequency, strList);
				// vsMatch.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, 10, vsMatch.Rows - 1, 10) = 6
				// set column 11 properties
				vsMatch.ColAlignment(CNSTGRIDMatchCOLStatus, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMatch.ColWidth(CNSTGRIDMatchCOLStatus, 700);
				vsMatch.TextMatrix(0, CNSTGRIDMatchCOLStatus, "Status");
				vsMatch.ColComboList(CNSTGRIDMatchCOLStatus, "#1;Active|#2;Inactive");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsMatch_RowColChange(object sender, System.EventArgs e)
		{
			if (vsMatch.Row < 1)
				return;
			if (vsMatch.Row >= 1)
				vsMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsMatch.Editable = (FCConvert.ToInt32(vsMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsMatch.Row, vsMatch.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) ? FCGrid.EditableSettings.flexEDKbdMouse : FCGrid.EditableSettings.flexEDNone;
		}

		private void vsMatch_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If Col = 6 Then Cancel = CheckAccountValidate(vsMatch, Row, Col, Cancel)
			double dblAmtPercentTotal;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			dblAmtPercentTotal = 0;
			if (vsMatch.Col == CNSTGRIDMatchCOLAmount)
			{
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmountType) == "Percent" || vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmountType) == "2")
					{
						if (intCounter == vsMatch.Row)
						{
							dblAmtPercentTotal += Conversion.Val(vsMatch.EditText);
						}
						else
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmount));
						}
					}
				}
			}
			if (vsMatch.Col == CNSTGRIDMatchCOLAmountType)
			{
				for (intCounter = 1; intCounter <= (vsMatch.Rows - 1); intCounter++)
				{
					if (intCounter == vsMatch.Row)
					{
						if (vsMatch.EditText == "Percent" || vsMatch.EditText == "2")
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmount));
						}
					}
					else
					{
						if (vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmountType) == "Percent" || vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmountType) == "2")
						{
							dblAmtPercentTotal += FCConvert.ToDouble(vsMatch.TextMatrix(intCounter, CNSTGRIDMatchCOLAmount));
						}
					}
				}
			}
			if (dblAmtPercentTotal > 100)
			{
				MessageBox.Show("Amount Totals For Percent Cannot Be Greater Than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void vsMatch_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsMatch.Col == CNSTGRIDMatchCOLAmountType)
			{
				if (vsMatch.EditText == "%D")
				{
					clsDRWrapper rsCheck = new clsDRWrapper();
					rsCheck.OpenRecordset("Select * from tblEmployeeDeductions where EmployeeNumber = '" + strCurEmployee + "' and DeductionCode = " + vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, CNSTGRIDMatchCOLMatchID), "Twpy0000.vb1");
					if (rsCheck.EndOfFile())
					{
						MessageBox.Show("Deduction Number has not been set up for this employee.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsMatch.Cell(FCGrid.CellPropertySettings.flexcpText, vsMatch.Row, CNSTGRIDMatchCOLAmountType, "D");
					}
				}
			}
		}

		private void txtFederalTaxes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// 46 = . 8 = backspace
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStateTaxes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalTaxes_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtFederalTaxes.Text) > 100 && cboFedDollarPercent.Text == "Percent")
			{
				e.Cancel = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtStateTaxes_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtStateTaxes.Text) > 100 && cboStateDollarPercent.Text == "Percent")
			{
				e.Cancel = true;
				MessageBox.Show("Can't Withold More Than 100 Percent", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
