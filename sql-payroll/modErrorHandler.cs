﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modErrorHandler
	{
		//=========================================================
		public static void SetErrorHandler(Exception ex)
		{
			// Dim clsErrorHandler As clsErrorHandlers
			// Set clsErrorHandler = New clsErrorHandlers
			// 
			// Call clsErrorHandler.SetErrorHandler
			// 
			// Set clsErrorHandler = Nothing
			MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
		}

		public class StaticVariables
		{
			// Variables for global error handler
			public string gstrStack = "";
			public string gstrCurrentRoutine = string.Empty;
			public string gstrFormName = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
