﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Part4Plan.
	/// </summary>
	partial class srptC1Part4Plan
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1Part4Plan));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtWH1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSN19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWH19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAmended1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmended19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMonthStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDayStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMonthEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDayEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOriginalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmendedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmendedTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Barcode1,
            this.Label73,
            this.Label67,
            this.Label6,
            this.txtName,
            this.Label7,
            this.txtWithholdingAccount,
            this.Label9,
            this.Label11,
            this.Line2,
            this.Label14,
            this.Label15,
            this.txtSSN1,
            this.txtName1,
            this.Label18,
            this.txtWH1,
            this.txtSSN2,
            this.txtName2,
            this.txtWH2,
            this.txtSSN3,
            this.txtName3,
            this.txtWH3,
            this.txtSSN4,
            this.txtName4,
            this.txtWH4,
            this.txtSSN5,
            this.txtName5,
            this.txtWH5,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.txtSSN6,
            this.txtName6,
            this.txtWH6,
            this.Label29,
            this.txtSSN7,
            this.txtName7,
            this.txtWH7,
            this.Label31,
            this.txtSSN8,
            this.txtName8,
            this.txtWH8,
            this.Label33,
            this.txtSSN9,
            this.txtName9,
            this.txtWH9,
            this.Label35,
            this.txtSSN10,
            this.txtName10,
            this.txtWH10,
            this.Label37,
            this.txtSSN11,
            this.txtName11,
            this.txtWH11,
            this.Label39,
            this.txtSSN12,
            this.txtName12,
            this.txtWH12,
            this.Label41,
            this.txtSSN13,
            this.txtName13,
            this.txtWH13,
            this.Label43,
            this.txtSSN14,
            this.txtName14,
            this.txtWH14,
            this.Label45,
            this.txtSSN15,
            this.txtName15,
            this.txtWH15,
            this.Label47,
            this.txtSSN16,
            this.txtName16,
            this.txtWH16,
            this.Label49,
            this.txtSSN17,
            this.txtName17,
            this.txtWH17,
            this.Label51,
            this.txtSSN18,
            this.txtName18,
            this.txtWH18,
            this.Label53,
            this.txtSSN19,
            this.txtName19,
            this.txtWH19,
            this.Label55,
            this.Label65,
            this.Label66,
            this.Label70,
            this.Label71,
            this.Label72,
            this.txtAmended1,
            this.txtAmended2,
            this.txtAmended3,
            this.txtAmended4,
            this.txtAmended5,
            this.txtAmended6,
            this.txtAmended7,
            this.txtAmended8,
            this.txtAmended9,
            this.txtAmended10,
            this.txtAmended11,
            this.txtAmended12,
            this.txtAmended13,
            this.txtAmended14,
            this.txtAmended15,
            this.txtAmended16,
            this.txtAmended17,
            this.txtAmended18,
            this.txtAmended19,
            this.txtDateStart,
            this.txtDateEnd,
            this.Field25,
            this.txtMonthStart,
            this.txtDayStart,
            this.txtYearStart,
            this.txtMonthEnd,
            this.txtDayEnd,
            this.txtYearEnd,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Label76,
            this.Label77,
            this.Label78,
            this.Label79,
            this.Label80,
            this.txtOriginalTotal,
            this.txtAmendedTotal,
            this.Label81,
            this.Label82});
            this.Detail.Height = 10.16667F;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Barcode1
            // 
            this.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
            this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode1.Height = 0.6458F;
            this.Barcode1.Left = 5.576F;
            this.Barcode1.Name = "Barcode1";
            this.Barcode1.QuietZoneBottom = 0F;
            this.Barcode1.QuietZoneLeft = 0F;
            this.Barcode1.QuietZoneRight = 0F;
            this.Barcode1.QuietZoneTop = 0F;
            this.Barcode1.Text = "2106201";
            this.Barcode1.Top = 0F;
            this.Barcode1.Width = 1.666667F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.1979167F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 7.249306F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-family: \\000027Courier\\000027; font-size: 12pt; font-weight: bold; text-alig" +
    "n: right";
            this.Label73.Text = "15";
            this.Label73.Top = 0.1770833F;
            this.Label73.Width = 0.25F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.2500016F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 0.239F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: top";
            this.Label67.Text = "Total of column D";
            this.Label67.Top = 9.217F;
            this.Label67.Width = 1.45832F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2500016F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.23F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: middle";
            this.Label6.Text = "Name";
            this.Label6.Top = 0.521F;
            this.Label6.Width = 0.6944862F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.2500016F;
            this.txtName.Left = 1.105F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName.Top = 0.531F;
            this.txtName.Width = 3.361111F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.3333349F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.23F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: middle";
            this.Label7.Text = "Withholding Account No.";
            this.Label7.Top = 0.9480001F;
            this.Label7.Width = 0.6944445F;
            // 
            // txtWithholdingAccount
            // 
            this.txtWithholdingAccount.Height = 0.2500016F;
            this.txtWithholdingAccount.Left = 1.104514F;
            this.txtWithholdingAccount.Name = "txtWithholdingAccount";
            this.txtWithholdingAccount.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.txtWithholdingAccount.Text = "99 99999999";
            this.txtWithholdingAccount.Top = 1.020833F;
            this.txtWithholdingAccount.Width = 1.527778F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2500016F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.3128471F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt";
            this.Label9.Text = "SCHEDULE 2  (FORM 941 ME)";
            this.Label9.Top = 0F;
            this.Label9.Width = 3.027778F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2708349F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 4.084F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: middle";
            this.Label11.Text = "Quarterly Period Covered";
            this.Label11.Top = 1.032F;
            this.Label11.Width = 1.25F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1.632F;
            this.Line2.Visible = false;
            this.Line2.Width = 7.499306F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.499306F;
            this.Line2.Y1 = 1.632F;
            this.Line2.Y2 = 1.632F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.23F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.9070001F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label14.Text = "Payee Name (Last, First, MI)";
            this.Label14.Top = 2.125F;
            this.Label14.Width = 1.861111F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.23F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 3.475F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt";
            this.Label15.Text = "Social Security Number";
            this.Label15.Top = 2.125F;
            this.Label15.Width = 1.217F;
            // 
            // txtSSN1
            // 
            this.txtSSN1.Height = 0.2812516F;
            this.txtSSN1.Left = 3.505F;
            this.txtSSN1.Name = "txtSSN1";
            this.txtSSN1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN1.Text = "999999999";
            this.txtSSN1.Top = 2.355F;
            this.txtSSN1.Width = 1.122F;
            // 
            // txtName1
            // 
            this.txtName1.CanGrow = false;
            this.txtName1.Height = 0.2812516F;
            this.txtName1.Left = 0.1875002F;
            this.txtName1.Name = "txtName1";
            this.txtName1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName1.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName1.Top = 2.354584F;
            this.txtName1.Width = 2.9855F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.2812516F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 0F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \\000027Tahoma\\000027";
            this.Label18.Text = "a.";
            this.Label18.Top = 2.354584F;
            this.Label18.Width = 0.02777784F;
            // 
            // txtWH1
            // 
            this.txtWH1.Height = 0.2812516F;
            this.txtWH1.Left = 4.607F;
            this.txtWH1.Name = "txtWH1";
            this.txtWH1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH1.Text = "99999999.99";
            this.txtWH1.Top = 2.355F;
            this.txtWH1.Width = 1.277778F;
            // 
            // txtSSN2
            // 
            this.txtSSN2.Height = 0.2812516F;
            this.txtSSN2.Left = 3.505F;
            this.txtSSN2.Name = "txtSSN2";
            this.txtSSN2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN2.Text = "999999999";
            this.txtSSN2.Top = 2.698F;
            this.txtSSN2.Width = 1.04F;
            // 
            // txtName2
            // 
            this.txtName2.CanGrow = false;
            this.txtName2.Height = 0.2812516F;
            this.txtName2.Left = 0.1875002F;
            this.txtName2.Name = "txtName2";
            this.txtName2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName2.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName2.Top = 2.698334F;
            this.txtName2.Width = 2.9855F;
            // 
            // txtWH2
            // 
            this.txtWH2.Height = 0.2812516F;
            this.txtWH2.Left = 4.607F;
            this.txtWH2.Name = "txtWH2";
            this.txtWH2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH2.Text = "99999999.99";
            this.txtWH2.Top = 2.698F;
            this.txtWH2.Width = 1.277778F;
            // 
            // txtSSN3
            // 
            this.txtSSN3.Height = 0.2812516F;
            this.txtSSN3.Left = 3.505F;
            this.txtSSN3.Name = "txtSSN3";
            this.txtSSN3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN3.Text = "999999999";
            this.txtSSN3.Top = 3.031333F;
            this.txtSSN3.Width = 1.04F;
            // 
            // txtName3
            // 
            this.txtName3.CanGrow = false;
            this.txtName3.Height = 0.2812516F;
            this.txtName3.Left = 0.1875002F;
            this.txtName3.Name = "txtName3";
            this.txtName3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName3.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName3.Top = 3.031667F;
            this.txtName3.Width = 2.9855F;
            // 
            // txtWH3
            // 
            this.txtWH3.Height = 0.2812516F;
            this.txtWH3.Left = 4.607F;
            this.txtWH3.Name = "txtWH3";
            this.txtWH3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH3.Text = "99999999.99";
            this.txtWH3.Top = 3.031333F;
            this.txtWH3.Width = 1.277778F;
            // 
            // txtSSN4
            // 
            this.txtSSN4.Height = 0.2812516F;
            this.txtSSN4.Left = 3.505F;
            this.txtSSN4.Name = "txtSSN4";
            this.txtSSN4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN4.Text = "999999999";
            this.txtSSN4.Top = 3.364666F;
            this.txtSSN4.Width = 1.3405F;
            // 
            // txtName4
            // 
            this.txtName4.CanGrow = false;
            this.txtName4.Height = 0.2812516F;
            this.txtName4.Left = 0.1875002F;
            this.txtName4.Name = "txtName4";
            this.txtName4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName4.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName4.Top = 3.365F;
            this.txtName4.Width = 2.9855F;
            // 
            // txtWH4
            // 
            this.txtWH4.Height = 0.2812516F;
            this.txtWH4.Left = 4.607F;
            this.txtWH4.Name = "txtWH4";
            this.txtWH4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH4.Text = "99999999.99";
            this.txtWH4.Top = 3.364666F;
            this.txtWH4.Width = 1.277778F;
            // 
            // txtSSN5
            // 
            this.txtSSN5.Height = 0.2812516F;
            this.txtSSN5.Left = 3.505F;
            this.txtSSN5.Name = "txtSSN5";
            this.txtSSN5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN5.Text = "999999999";
            this.txtSSN5.Top = 3.698F;
            this.txtSSN5.Width = 1.278F;
            // 
            // txtName5
            // 
            this.txtName5.CanGrow = false;
            this.txtName5.Height = 0.2812516F;
            this.txtName5.Left = 0.1875002F;
            this.txtName5.Name = "txtName5";
            this.txtName5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName5.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName5.Top = 3.698334F;
            this.txtName5.Width = 2.9855F;
            // 
            // txtWH5
            // 
            this.txtWH5.Height = 0.2812516F;
            this.txtWH5.Left = 4.607F;
            this.txtWH5.Name = "txtWH5";
            this.txtWH5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH5.Text = "99999999.99";
            this.txtWH5.Top = 3.698F;
            this.txtWH5.Width = 1.277778F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.2812516F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \\000027Tahoma\\000027";
            this.Label24.Text = "b.";
            this.Label24.Top = 2.698334F;
            this.Label24.Width = 0.02777784F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.2812516F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 0F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \\000027Tahoma\\000027";
            this.Label25.Text = "c.";
            this.Label25.Top = 3.031667F;
            this.Label25.Width = 0.02777784F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.2812516F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \\000027Tahoma\\000027";
            this.Label26.Text = "d.";
            this.Label26.Top = 3.365F;
            this.Label26.Width = 0.02777784F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.2812516F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 0F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \\000027Tahoma\\000027";
            this.Label27.Text = "e.";
            this.Label27.Top = 3.698334F;
            this.Label27.Width = 0.02777784F;
            // 
            // txtSSN6
            // 
            this.txtSSN6.Height = 0.2812516F;
            this.txtSSN6.Left = 3.505F;
            this.txtSSN6.Name = "txtSSN6";
            this.txtSSN6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN6.Text = "999999999";
            this.txtSSN6.Top = 4.031333F;
            this.txtSSN6.Width = 1.3405F;
            // 
            // txtName6
            // 
            this.txtName6.CanGrow = false;
            this.txtName6.Height = 0.2812516F;
            this.txtName6.Left = 0.1875002F;
            this.txtName6.Name = "txtName6";
            this.txtName6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName6.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName6.Top = 4.031667F;
            this.txtName6.Width = 2.9855F;
            // 
            // txtWH6
            // 
            this.txtWH6.Height = 0.2812516F;
            this.txtWH6.Left = 4.607F;
            this.txtWH6.Name = "txtWH6";
            this.txtWH6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH6.Text = "99999999.99";
            this.txtWH6.Top = 4.031334F;
            this.txtWH6.Width = 1.277778F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.2812516F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 0F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \\000027Tahoma\\000027";
            this.Label29.Text = "f.";
            this.Label29.Top = 4.031667F;
            this.Label29.Width = 0.02777784F;
            // 
            // txtSSN7
            // 
            this.txtSSN7.Height = 0.2812516F;
            this.txtSSN7.Left = 3.505F;
            this.txtSSN7.Name = "txtSSN7";
            this.txtSSN7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN7.Text = "999999999";
            this.txtSSN7.Top = 4.364666F;
            this.txtSSN7.Width = 1.3405F;
            // 
            // txtName7
            // 
            this.txtName7.CanGrow = false;
            this.txtName7.Height = 0.2812516F;
            this.txtName7.Left = 0.1875002F;
            this.txtName7.Name = "txtName7";
            this.txtName7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName7.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName7.Top = 4.365F;
            this.txtName7.Width = 2.9855F;
            // 
            // txtWH7
            // 
            this.txtWH7.Height = 0.2812516F;
            this.txtWH7.Left = 4.607F;
            this.txtWH7.Name = "txtWH7";
            this.txtWH7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH7.Text = "99999999.99";
            this.txtWH7.Top = 4.364666F;
            this.txtWH7.Width = 1.277778F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.2812516F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 0F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \\000027Tahoma\\000027";
            this.Label31.Text = "g.";
            this.Label31.Top = 4.365F;
            this.Label31.Width = 0.02777784F;
            // 
            // txtSSN8
            // 
            this.txtSSN8.Height = 0.2812516F;
            this.txtSSN8.Left = 3.505F;
            this.txtSSN8.Name = "txtSSN8";
            this.txtSSN8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN8.Text = "999999999";
            this.txtSSN8.Top = 4.698F;
            this.txtSSN8.Width = 1.3405F;
            // 
            // txtName8
            // 
            this.txtName8.CanGrow = false;
            this.txtName8.Height = 0.2812516F;
            this.txtName8.Left = 0.1875002F;
            this.txtName8.Name = "txtName8";
            this.txtName8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName8.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName8.Top = 4.698334F;
            this.txtName8.Width = 2.9855F;
            // 
            // txtWH8
            // 
            this.txtWH8.Height = 0.2812516F;
            this.txtWH8.Left = 4.607F;
            this.txtWH8.Name = "txtWH8";
            this.txtWH8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH8.Text = "99999999.99";
            this.txtWH8.Top = 4.698F;
            this.txtWH8.Width = 1.277778F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.2812516F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 0F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \\000027Tahoma\\000027";
            this.Label33.Text = "h.";
            this.Label33.Top = 4.698334F;
            this.Label33.Width = 0.02777784F;
            // 
            // txtSSN9
            // 
            this.txtSSN9.CanGrow = false;
            this.txtSSN9.Height = 0.2812516F;
            this.txtSSN9.Left = 3.505F;
            this.txtSSN9.Name = "txtSSN9";
            this.txtSSN9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN9.Text = "999999999";
            this.txtSSN9.Top = 5.031333F;
            this.txtSSN9.Width = 1.3405F;
            // 
            // txtName9
            // 
            this.txtName9.CanGrow = false;
            this.txtName9.Height = 0.2812516F;
            this.txtName9.Left = 0.1875002F;
            this.txtName9.Name = "txtName9";
            this.txtName9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName9.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName9.Top = 5.031667F;
            this.txtName9.Width = 2.9855F;
            // 
            // txtWH9
            // 
            this.txtWH9.CanGrow = false;
            this.txtWH9.Height = 0.2812516F;
            this.txtWH9.Left = 4.607F;
            this.txtWH9.Name = "txtWH9";
            this.txtWH9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH9.Text = "99999999.99";
            this.txtWH9.Top = 5.031334F;
            this.txtWH9.Width = 1.277778F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.2812516F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 0F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \\000027Tahoma\\000027";
            this.Label35.Text = "i.";
            this.Label35.Top = 5.031667F;
            this.Label35.Width = 0.02777784F;
            // 
            // txtSSN10
            // 
            this.txtSSN10.CanGrow = false;
            this.txtSSN10.Height = 0.2812516F;
            this.txtSSN10.Left = 3.505F;
            this.txtSSN10.Name = "txtSSN10";
            this.txtSSN10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN10.Text = "999999999";
            this.txtSSN10.Top = 5.364666F;
            this.txtSSN10.Width = 1.3405F;
            // 
            // txtName10
            // 
            this.txtName10.CanGrow = false;
            this.txtName10.Height = 0.2812516F;
            this.txtName10.Left = 0.1875002F;
            this.txtName10.Name = "txtName10";
            this.txtName10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName10.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName10.Top = 5.365F;
            this.txtName10.Width = 2.9855F;
            // 
            // txtWH10
            // 
            this.txtWH10.CanGrow = false;
            this.txtWH10.Height = 0.2812516F;
            this.txtWH10.Left = 4.607F;
            this.txtWH10.Name = "txtWH10";
            this.txtWH10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH10.Text = "99999999.99";
            this.txtWH10.Top = 5.364666F;
            this.txtWH10.Width = 1.277778F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.2812516F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 0F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \\000027Tahoma\\000027";
            this.Label37.Text = "j.";
            this.Label37.Top = 5.365F;
            this.Label37.Width = 0.02777784F;
            // 
            // txtSSN11
            // 
            this.txtSSN11.CanGrow = false;
            this.txtSSN11.Height = 0.2812516F;
            this.txtSSN11.Left = 3.505F;
            this.txtSSN11.Name = "txtSSN11";
            this.txtSSN11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN11.Text = "999999999";
            this.txtSSN11.Top = 5.698F;
            this.txtSSN11.Width = 1.278F;
            // 
            // txtName11
            // 
            this.txtName11.CanGrow = false;
            this.txtName11.Height = 0.2812516F;
            this.txtName11.Left = 0.1875002F;
            this.txtName11.Name = "txtName11";
            this.txtName11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName11.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName11.Top = 5.698334F;
            this.txtName11.Width = 2.9855F;
            // 
            // txtWH11
            // 
            this.txtWH11.CanGrow = false;
            this.txtWH11.Height = 0.2812516F;
            this.txtWH11.Left = 4.607F;
            this.txtWH11.Name = "txtWH11";
            this.txtWH11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH11.Text = "99999999.99";
            this.txtWH11.Top = 5.698F;
            this.txtWH11.Width = 1.277778F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.2812516F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 0F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-family: \\000027Tahoma\\000027";
            this.Label39.Text = "k.";
            this.Label39.Top = 5.698334F;
            this.Label39.Width = 0.02777784F;
            // 
            // txtSSN12
            // 
            this.txtSSN12.CanGrow = false;
            this.txtSSN12.Height = 0.2812516F;
            this.txtSSN12.Left = 3.505F;
            this.txtSSN12.Name = "txtSSN12";
            this.txtSSN12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN12.Text = "999999999";
            this.txtSSN12.Top = 6.031333F;
            this.txtSSN12.Width = 1.278F;
            // 
            // txtName12
            // 
            this.txtName12.CanGrow = false;
            this.txtName12.Height = 0.2812516F;
            this.txtName12.Left = 0.1875002F;
            this.txtName12.Name = "txtName12";
            this.txtName12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName12.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName12.Top = 6.031667F;
            this.txtName12.Width = 2.9855F;
            // 
            // txtWH12
            // 
            this.txtWH12.CanGrow = false;
            this.txtWH12.Height = 0.2812516F;
            this.txtWH12.Left = 4.607F;
            this.txtWH12.Name = "txtWH12";
            this.txtWH12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH12.Text = "99999999.99";
            this.txtWH12.Top = 6.031334F;
            this.txtWH12.Width = 1.277778F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.2812516F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 0F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-family: \\000027Tahoma\\000027";
            this.Label41.Text = "l.";
            this.Label41.Top = 6.031667F;
            this.Label41.Width = 0.02777784F;
            // 
            // txtSSN13
            // 
            this.txtSSN13.CanGrow = false;
            this.txtSSN13.Height = 0.2812516F;
            this.txtSSN13.Left = 3.505F;
            this.txtSSN13.Name = "txtSSN13";
            this.txtSSN13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN13.Text = "999999999";
            this.txtSSN13.Top = 6.364666F;
            this.txtSSN13.Width = 1.278F;
            // 
            // txtName13
            // 
            this.txtName13.CanGrow = false;
            this.txtName13.Height = 0.2812516F;
            this.txtName13.Left = 0.1875002F;
            this.txtName13.Name = "txtName13";
            this.txtName13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName13.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName13.Top = 6.365F;
            this.txtName13.Width = 2.9855F;
            // 
            // txtWH13
            // 
            this.txtWH13.CanGrow = false;
            this.txtWH13.Height = 0.2812516F;
            this.txtWH13.Left = 4.607F;
            this.txtWH13.Name = "txtWH13";
            this.txtWH13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH13.Text = "99999999.99";
            this.txtWH13.Top = 6.364666F;
            this.txtWH13.Width = 1.277778F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.2812516F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 0F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \\000027Tahoma\\000027";
            this.Label43.Text = "m.";
            this.Label43.Top = 6.365F;
            this.Label43.Width = 0.02777784F;
            // 
            // txtSSN14
            // 
            this.txtSSN14.CanGrow = false;
            this.txtSSN14.Height = 0.2812516F;
            this.txtSSN14.Left = 3.505F;
            this.txtSSN14.Name = "txtSSN14";
            this.txtSSN14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN14.Text = "999999999";
            this.txtSSN14.Top = 6.698F;
            this.txtSSN14.Width = 1.278F;
            // 
            // txtName14
            // 
            this.txtName14.CanGrow = false;
            this.txtName14.Height = 0.2812516F;
            this.txtName14.Left = 0.1875002F;
            this.txtName14.Name = "txtName14";
            this.txtName14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName14.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName14.Top = 6.698334F;
            this.txtName14.Width = 2.9855F;
            // 
            // txtWH14
            // 
            this.txtWH14.CanGrow = false;
            this.txtWH14.Height = 0.2812516F;
            this.txtWH14.Left = 4.607F;
            this.txtWH14.Name = "txtWH14";
            this.txtWH14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH14.Text = "99999999.99";
            this.txtWH14.Top = 6.698F;
            this.txtWH14.Width = 1.277778F;
            // 
            // Label45
            // 
            this.Label45.Height = 0.2812516F;
            this.Label45.HyperLink = null;
            this.Label45.Left = 0F;
            this.Label45.Name = "Label45";
            this.Label45.Style = "font-family: \\000027Tahoma\\000027";
            this.Label45.Text = "n.";
            this.Label45.Top = 6.698334F;
            this.Label45.Width = 0.02777784F;
            // 
            // txtSSN15
            // 
            this.txtSSN15.CanGrow = false;
            this.txtSSN15.Height = 0.2812516F;
            this.txtSSN15.Left = 3.505F;
            this.txtSSN15.Name = "txtSSN15";
            this.txtSSN15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN15.Text = "999999999";
            this.txtSSN15.Top = 7.031668F;
            this.txtSSN15.Width = 1.3405F;
            // 
            // txtName15
            // 
            this.txtName15.CanGrow = false;
            this.txtName15.Height = 0.2812516F;
            this.txtName15.Left = 0.1875002F;
            this.txtName15.Name = "txtName15";
            this.txtName15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName15.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName15.Top = 7.031667F;
            this.txtName15.Width = 2.9855F;
            // 
            // txtWH15
            // 
            this.txtWH15.CanGrow = false;
            this.txtWH15.Height = 0.2812516F;
            this.txtWH15.Left = 4.607F;
            this.txtWH15.Name = "txtWH15";
            this.txtWH15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH15.Text = "99999999.99";
            this.txtWH15.Top = 7.032001F;
            this.txtWH15.Width = 1.277778F;
            // 
            // Label47
            // 
            this.Label47.Height = 0.2812516F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 0F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-family: \\000027Tahoma\\000027";
            this.Label47.Text = "o.";
            this.Label47.Top = 7.031667F;
            this.Label47.Width = 0.02777784F;
            // 
            // txtSSN16
            // 
            this.txtSSN16.CanGrow = false;
            this.txtSSN16.Height = 0.2812516F;
            this.txtSSN16.Left = 3.505F;
            this.txtSSN16.Name = "txtSSN16";
            this.txtSSN16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN16.Text = "999999999";
            this.txtSSN16.Top = 7.365F;
            this.txtSSN16.Width = 1.278F;
            // 
            // txtName16
            // 
            this.txtName16.CanGrow = false;
            this.txtName16.Height = 0.2812516F;
            this.txtName16.Left = 0.1875002F;
            this.txtName16.Name = "txtName16";
            this.txtName16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName16.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName16.Top = 7.365F;
            this.txtName16.Width = 2.9855F;
            // 
            // txtWH16
            // 
            this.txtWH16.CanGrow = false;
            this.txtWH16.Height = 0.2812516F;
            this.txtWH16.Left = 4.607F;
            this.txtWH16.Name = "txtWH16";
            this.txtWH16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH16.Text = "99999999.99";
            this.txtWH16.Top = 7.365333F;
            this.txtWH16.Width = 1.277778F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.2812516F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 0F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-family: \\000027Tahoma\\000027";
            this.Label49.Text = "p.";
            this.Label49.Top = 7.365F;
            this.Label49.Width = 0.02777784F;
            // 
            // txtSSN17
            // 
            this.txtSSN17.CanGrow = false;
            this.txtSSN17.Height = 0.2812516F;
            this.txtSSN17.Left = 3.505F;
            this.txtSSN17.Name = "txtSSN17";
            this.txtSSN17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN17.Text = "999999999";
            this.txtSSN17.Top = 7.698335F;
            this.txtSSN17.Width = 1.278F;
            // 
            // txtName17
            // 
            this.txtName17.CanGrow = false;
            this.txtName17.Height = 0.2812516F;
            this.txtName17.Left = 0.1875002F;
            this.txtName17.Name = "txtName17";
            this.txtName17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName17.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName17.Top = 7.698334F;
            this.txtName17.Width = 2.9855F;
            // 
            // txtWH17
            // 
            this.txtWH17.CanGrow = false;
            this.txtWH17.Height = 0.2812516F;
            this.txtWH17.Left = 4.607F;
            this.txtWH17.Name = "txtWH17";
            this.txtWH17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH17.Text = "99999999.99";
            this.txtWH17.Top = 7.698667F;
            this.txtWH17.Width = 1.277778F;
            // 
            // Label51
            // 
            this.Label51.Height = 0.2812516F;
            this.Label51.HyperLink = null;
            this.Label51.Left = 0F;
            this.Label51.Name = "Label51";
            this.Label51.Style = "font-family: \\000027Tahoma\\000027";
            this.Label51.Text = "q.";
            this.Label51.Top = 7.698334F;
            this.Label51.Width = 0.02777784F;
            // 
            // txtSSN18
            // 
            this.txtSSN18.CanGrow = false;
            this.txtSSN18.Height = 0.2812516F;
            this.txtSSN18.Left = 3.505F;
            this.txtSSN18.Name = "txtSSN18";
            this.txtSSN18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN18.Text = "999999999";
            this.txtSSN18.Top = 8.031668F;
            this.txtSSN18.Width = 1.3405F;
            // 
            // txtName18
            // 
            this.txtName18.CanGrow = false;
            this.txtName18.Height = 0.2812516F;
            this.txtName18.Left = 0.1875002F;
            this.txtName18.Name = "txtName18";
            this.txtName18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName18.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName18.Top = 8.031668F;
            this.txtName18.Width = 2.9855F;
            // 
            // txtWH18
            // 
            this.txtWH18.CanGrow = false;
            this.txtWH18.Height = 0.2812516F;
            this.txtWH18.Left = 4.607F;
            this.txtWH18.Name = "txtWH18";
            this.txtWH18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH18.Text = "99999999.99";
            this.txtWH18.Top = 8.032001F;
            this.txtWH18.Width = 1.277778F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.2812516F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 0F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-family: \\000027Tahoma\\000027";
            this.Label53.Text = "r.";
            this.Label53.Top = 8.031668F;
            this.Label53.Width = 0.02777784F;
            // 
            // txtSSN19
            // 
            this.txtSSN19.CanGrow = false;
            this.txtSSN19.Height = 0.2812516F;
            this.txtSSN19.Left = 3.505F;
            this.txtSSN19.Name = "txtSSN19";
            this.txtSSN19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtSSN19.Text = "999999999";
            this.txtSSN19.Top = 8.355F;
            this.txtSSN19.Width = 1.278F;
            // 
            // txtName19
            // 
            this.txtName19.CanGrow = false;
            this.txtName19.Height = 0.2812516F;
            this.txtName19.Left = 0.188F;
            this.txtName19.Name = "txtName19";
            this.txtName19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName19.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName19.Top = 8.355F;
            this.txtName19.Width = 2.9855F;
            // 
            // txtWH19
            // 
            this.txtWH19.CanGrow = false;
            this.txtWH19.Height = 0.2812516F;
            this.txtWH19.Left = 4.607F;
            this.txtWH19.Name = "txtWH19";
            this.txtWH19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtWH19.Text = "99999999.99";
            this.txtWH19.Top = 8.355332F;
            this.txtWH19.Width = 1.277778F;
            // 
            // Label55
            // 
            this.Label55.Height = 0.2812516F;
            this.Label55.HyperLink = null;
            this.Label55.Left = 0F;
            this.Label55.Name = "Label55";
            this.Label55.Style = "font-family: \\000027Tahoma\\000027";
            this.Label55.Text = "s.";
            this.Label55.Top = 8.365F;
            this.Label55.Width = 0.02777784F;
            // 
            // Label65
            // 
            this.Label65.Height = 0.3958349F;
            this.Label65.HyperLink = null;
            this.Label65.Left = 6.268836F;
            this.Label65.Name = "Label65";
            this.Label65.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label65.Text = "Amended Return Correct Withholding";
            this.Label65.Top = 2.061F;
            this.Label65.Width = 0.8819447F;
            // 
            // Label66
            // 
            this.Label66.Height = 0.2500016F;
            this.Label66.HyperLink = null;
            this.Label66.Left = 0.2386805F;
            this.Label66.Name = "Label66";
            this.Label66.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: top";
            this.Label66.Text = "Total of columns C";
            this.Label66.Top = 8.916667F;
            this.Label66.Width = 1.236111F;
            // 
            // Label70
            // 
            this.Label70.Height = 0.2500016F;
            this.Label70.HyperLink = null;
            this.Label70.Left = 5.124F;
            this.Label70.Name = "Label70";
            this.Label70.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: middle";
            this.Label70.Text = "6.";
            this.Label70.Top = 8.896001F;
            this.Label70.Width = 0.1774864F;
            // 
            // Label71
            // 
            this.Label71.Height = 0.2500016F;
            this.Label71.HyperLink = null;
            this.Label71.Left = 5.12375F;
            this.Label71.Name = "Label71";
            this.Label71.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: middle";
            this.Label71.Text = "7.";
            this.Label71.Top = 9.229028F;
            this.Label71.Width = 0.1774864F;
            // 
            // Label72
            // 
            this.Label72.Height = 0.2500016F;
            this.Label72.HyperLink = null;
            this.Label72.Left = 0.4586805F;
            this.Label72.Name = "Label72";
            this.Label72.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt; font-weight: bold; text-align" +
    ": center";
            this.Label72.Text = "INDIVIDUAL EMPLOYEE / PAYEE WITHHOLDING REPORTING AND CORRECTIONS";
            this.Label72.Top = 1.625F;
            this.Label72.Width = 6.381945F;
            // 
            // txtAmended1
            // 
            this.txtAmended1.Height = 0.2812516F;
            this.txtAmended1.Left = 5.927F;
            this.txtAmended1.Name = "txtAmended1";
            this.txtAmended1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended1.Text = "99999999.99";
            this.txtAmended1.Top = 2.355F;
            this.txtAmended1.Width = 1.277778F;
            // 
            // txtAmended2
            // 
            this.txtAmended2.Height = 0.2812516F;
            this.txtAmended2.Left = 5.988F;
            this.txtAmended2.Name = "txtAmended2";
            this.txtAmended2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended2.Text = "99999999.99";
            this.txtAmended2.Top = 2.698F;
            this.txtAmended2.Width = 1.217F;
            // 
            // txtAmended3
            // 
            this.txtAmended3.Height = 0.2812516F;
            this.txtAmended3.Left = 5.988F;
            this.txtAmended3.Name = "txtAmended3";
            this.txtAmended3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended3.Text = "99999999.99";
            this.txtAmended3.Top = 3.031333F;
            this.txtAmended3.Width = 1.217F;
            // 
            // txtAmended4
            // 
            this.txtAmended4.Height = 0.2812516F;
            this.txtAmended4.Left = 5.988F;
            this.txtAmended4.Name = "txtAmended4";
            this.txtAmended4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended4.Text = "99999999.99";
            this.txtAmended4.Top = 3.364666F;
            this.txtAmended4.Width = 1.217F;
            // 
            // txtAmended5
            // 
            this.txtAmended5.Height = 0.2812516F;
            this.txtAmended5.Left = 5.988F;
            this.txtAmended5.Name = "txtAmended5";
            this.txtAmended5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended5.Text = "99999999.99";
            this.txtAmended5.Top = 3.698F;
            this.txtAmended5.Width = 1.217F;
            // 
            // txtAmended6
            // 
            this.txtAmended6.Height = 0.2812516F;
            this.txtAmended6.Left = 5.988F;
            this.txtAmended6.Name = "txtAmended6";
            this.txtAmended6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended6.Text = "99999999.99";
            this.txtAmended6.Top = 4.031334F;
            this.txtAmended6.Width = 1.217F;
            // 
            // txtAmended7
            // 
            this.txtAmended7.Height = 0.2812516F;
            this.txtAmended7.Left = 5.988F;
            this.txtAmended7.Name = "txtAmended7";
            this.txtAmended7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended7.Text = "99999999.99";
            this.txtAmended7.Top = 4.364666F;
            this.txtAmended7.Width = 1.217F;
            // 
            // txtAmended8
            // 
            this.txtAmended8.Height = 0.2812516F;
            this.txtAmended8.Left = 5.988F;
            this.txtAmended8.Name = "txtAmended8";
            this.txtAmended8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended8.Text = "99999999.99";
            this.txtAmended8.Top = 4.698F;
            this.txtAmended8.Width = 1.217F;
            // 
            // txtAmended9
            // 
            this.txtAmended9.CanGrow = false;
            this.txtAmended9.Height = 0.2812516F;
            this.txtAmended9.Left = 5.988F;
            this.txtAmended9.Name = "txtAmended9";
            this.txtAmended9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended9.Text = "99999999.99";
            this.txtAmended9.Top = 5.031334F;
            this.txtAmended9.Width = 1.217F;
            // 
            // txtAmended10
            // 
            this.txtAmended10.CanGrow = false;
            this.txtAmended10.Height = 0.2812516F;
            this.txtAmended10.Left = 5.988F;
            this.txtAmended10.Name = "txtAmended10";
            this.txtAmended10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended10.Text = "99999999.99";
            this.txtAmended10.Top = 5.364666F;
            this.txtAmended10.Width = 1.217F;
            // 
            // txtAmended11
            // 
            this.txtAmended11.CanGrow = false;
            this.txtAmended11.Height = 0.2812516F;
            this.txtAmended11.Left = 5.988F;
            this.txtAmended11.Name = "txtAmended11";
            this.txtAmended11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended11.Text = "99999999.99";
            this.txtAmended11.Top = 5.698F;
            this.txtAmended11.Width = 1.217F;
            // 
            // txtAmended12
            // 
            this.txtAmended12.CanGrow = false;
            this.txtAmended12.Height = 0.2812516F;
            this.txtAmended12.Left = 5.988F;
            this.txtAmended12.Name = "txtAmended12";
            this.txtAmended12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended12.Text = "99999999.99";
            this.txtAmended12.Top = 6.031334F;
            this.txtAmended12.Width = 1.217F;
            // 
            // txtAmended13
            // 
            this.txtAmended13.CanGrow = false;
            this.txtAmended13.Height = 0.2812516F;
            this.txtAmended13.Left = 5.988F;
            this.txtAmended13.Name = "txtAmended13";
            this.txtAmended13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended13.Text = "99999999.99";
            this.txtAmended13.Top = 6.364666F;
            this.txtAmended13.Width = 1.217F;
            // 
            // txtAmended14
            // 
            this.txtAmended14.CanGrow = false;
            this.txtAmended14.Height = 0.2812516F;
            this.txtAmended14.Left = 5.988F;
            this.txtAmended14.Name = "txtAmended14";
            this.txtAmended14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended14.Text = "99999999.99";
            this.txtAmended14.Top = 6.698F;
            this.txtAmended14.Width = 1.217F;
            // 
            // txtAmended15
            // 
            this.txtAmended15.CanGrow = false;
            this.txtAmended15.Height = 0.2812516F;
            this.txtAmended15.Left = 5.92F;
            this.txtAmended15.Name = "txtAmended15";
            this.txtAmended15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended15.Text = "99999999.99";
            this.txtAmended15.Top = 7.032001F;
            this.txtAmended15.Width = 1.277778F;
            // 
            // txtAmended16
            // 
            this.txtAmended16.CanGrow = false;
            this.txtAmended16.Height = 0.2812516F;
            this.txtAmended16.Left = 5.92F;
            this.txtAmended16.Name = "txtAmended16";
            this.txtAmended16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended16.Text = "99999999.99";
            this.txtAmended16.Top = 7.365333F;
            this.txtAmended16.Width = 1.277778F;
            // 
            // txtAmended17
            // 
            this.txtAmended17.CanGrow = false;
            this.txtAmended17.Height = 0.2812516F;
            this.txtAmended17.Left = 5.92F;
            this.txtAmended17.Name = "txtAmended17";
            this.txtAmended17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended17.Text = "99999999.99";
            this.txtAmended17.Top = 7.698667F;
            this.txtAmended17.Width = 1.277778F;
            // 
            // txtAmended18
            // 
            this.txtAmended18.CanGrow = false;
            this.txtAmended18.Height = 0.2812516F;
            this.txtAmended18.Left = 5.92F;
            this.txtAmended18.Name = "txtAmended18";
            this.txtAmended18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended18.Text = "99999999.99";
            this.txtAmended18.Top = 8.032001F;
            this.txtAmended18.Width = 1.277778F;
            // 
            // txtAmended19
            // 
            this.txtAmended19.CanGrow = false;
            this.txtAmended19.Height = 0.2812516F;
            this.txtAmended19.Left = 5.92F;
            this.txtAmended19.Name = "txtAmended19";
            this.txtAmended19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmended19.Text = "99999999.99";
            this.txtAmended19.Top = 8.355332F;
            this.txtAmended19.Width = 1.277778F;
            // 
            // txtDateStart
            // 
            this.txtDateStart.Height = 0.2500016F;
            this.txtDateStart.Left = 4.417014F;
            this.txtDateStart.Name = "txtDateStart";
            this.txtDateStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDateStart.Text = "99 99";
            this.txtDateStart.Top = 1.291667F;
            this.txtDateStart.Width = 0.8611112F;
            // 
            // txtDateEnd
            // 
            this.txtDateEnd.Height = 0.2500016F;
            this.txtDateEnd.Left = 6.135764F;
            this.txtDateEnd.Name = "txtDateEnd";
            this.txtDateEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDateEnd.Text = "99 99";
            this.txtDateEnd.Top = 1.291667F;
            this.txtDateEnd.Width = 0.7777777F;
            // 
            // Field25
            // 
            this.Field25.Height = 0.2708349F;
            this.Field25.Left = 6.281597F;
            this.Field25.Name = "Field25";
            this.Field25.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: cente" +
    "r; vertical-align: top";
            this.Field25.Text = "to";
            this.Field25.Top = 1.291667F;
            this.Field25.Visible = false;
            this.Field25.Width = 0.236403F;
            // 
            // txtMonthStart
            // 
            this.txtMonthStart.Height = 0.1389444F;
            this.txtMonthStart.Left = 1.357986F;
            this.txtMonthStart.Name = "txtMonthStart";
            this.txtMonthStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtMonthStart.Text = "99";
            this.txtMonthStart.Top = 1.493056F;
            this.txtMonthStart.Visible = false;
            this.txtMonthStart.Width = 0.3090141F;
            // 
            // txtDayStart
            // 
            this.txtDayStart.Height = 0.1389444F;
            this.txtDayStart.Left = 1.667014F;
            this.txtDayStart.Name = "txtDayStart";
            this.txtDayStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDayStart.Text = "99";
            this.txtDayStart.Top = 1.493056F;
            this.txtDayStart.Visible = false;
            this.txtDayStart.Width = 0.2709863F;
            // 
            // txtYearStart
            // 
            this.txtYearStart.Height = 0.2500016F;
            this.txtYearStart.Left = 5.182F;
            this.txtYearStart.Name = "txtYearStart";
            this.txtYearStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearStart.Text = "9999";
            this.txtYearStart.Top = 1.292F;
            this.txtYearStart.Width = 0.5004029F;
            // 
            // txtMonthEnd
            // 
            this.txtMonthEnd.Height = 0.2500016F;
            this.txtMonthEnd.Left = 3.434375F;
            this.txtMonthEnd.Name = "txtMonthEnd";
            this.txtMonthEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtMonthEnd.Text = "99";
            this.txtMonthEnd.Top = 0.8680556F;
            this.txtMonthEnd.Visible = false;
            this.txtMonthEnd.Width = 0.3086255F;
            // 
            // txtDayEnd
            // 
            this.txtDayEnd.Height = 0.2500016F;
            this.txtDayEnd.Left = 3.743403F;
            this.txtDayEnd.Name = "txtDayEnd";
            this.txtDayEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDayEnd.Text = "99";
            this.txtDayEnd.Top = 0.8680556F;
            this.txtDayEnd.Visible = false;
            this.txtDayEnd.Width = 0.5655973F;
            // 
            // txtYearEnd
            // 
            this.txtYearEnd.Height = 0.2500016F;
            this.txtYearEnd.Left = 6.834F;
            this.txtYearEnd.Name = "txtYearEnd";
            this.txtYearEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearEnd.Text = "9999";
            this.txtYearEnd.Top = 1.292F;
            this.txtYearEnd.Width = 0.3747368F;
            // 
            // Shape17
            // 
            this.Shape17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape17.Height = 0.139F;
            this.Shape17.Left = 0.01423603F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 0.01388889F;
            this.Shape17.Width = 0.139F;
            // 
            // Shape18
            // 
            this.Shape18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape18.Height = 0.139F;
            this.Shape18.Left = 0.01423603F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 9.861111F;
            this.Shape18.Width = 0.139F;
            // 
            // Shape19
            // 
            this.Shape19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape19.Height = 0.139F;
            this.Shape19.Left = 7.347F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 9.861111F;
            this.Shape19.Width = 0.139F;
            // 
            // Label76
            // 
            this.Label76.Height = 0.3139998F;
            this.Label76.HyperLink = null;
            this.Label76.Left = 5.058F;
            this.Label76.Name = "Label76";
            this.Label76.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label76.Text = "Original Return Withholding";
            this.Label76.Top = 2.061F;
            this.Label76.Width = 0.7986112F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.2120001F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 1.65F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "font-weight: bold";
            this.Label77.Text = "A";
            this.Label77.Top = 1.872F;
            this.Label77.Width = 0.1359029F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.2499999F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 4.013F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-weight: bold";
            this.Label78.Text = "B";
            this.Label78.Top = 1.875F;
            this.Label78.Width = 0.1323197F;
            // 
            // Label79
            // 
            this.Label79.Height = 0.2500016F;
            this.Label79.HyperLink = null;
            this.Label79.Left = 0.05800001F;
            this.Label79.Name = "Label79";
            this.Label79.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: top";
            this.Label79.Text = "6.";
            this.Label79.Top = 8.916667F;
            this.Label79.Width = 0.1111112F;
            // 
            // Label80
            // 
            this.Label80.Height = 0.2500016F;
            this.Label80.HyperLink = null;
            this.Label80.Left = 0.061F;
            this.Label80.Name = "Label80";
            this.Label80.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; vertical-align: top";
            this.Label80.Text = "7.";
            this.Label80.Top = 9.217F;
            this.Label80.Width = 0.1111112F;
            // 
            // txtOriginalTotal
            // 
            this.txtOriginalTotal.DataField = "";
            this.txtOriginalTotal.DistinctField = "";
            this.txtOriginalTotal.Height = 0.2500016F;
            this.txtOriginalTotal.Left = 5.655F;
            this.txtOriginalTotal.Name = "txtOriginalTotal";
            this.txtOriginalTotal.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtOriginalTotal.SummaryGroup = "";
            this.txtOriginalTotal.Text = "99999999999.99";
            this.txtOriginalTotal.Top = 8.879001F;
            this.txtOriginalTotal.Width = 1.611111F;
            // 
            // txtAmendedTotal
            // 
            this.txtAmendedTotal.Height = 0.2500016F;
            this.txtAmendedTotal.Left = 5.655F;
            this.txtAmendedTotal.Name = "txtAmendedTotal";
            this.txtAmendedTotal.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmendedTotal.Text = "99999999999.99";
            this.txtAmendedTotal.Top = 9.229028F;
            this.txtAmendedTotal.Width = 1.611111F;
            // 
            // Label81
            // 
            this.Label81.Height = 0.2090001F;
            this.Label81.HyperLink = null;
            this.Label81.Left = 5.396F;
            this.Label81.Name = "Label81";
            this.Label81.Style = "font-weight: bold";
            this.Label81.Text = "C";
            this.Label81.Top = 1.875F;
            this.Label81.Width = 0.1003194F;
            // 
            // Label82
            // 
            this.Label82.Height = 0.2090001F;
            this.Label82.HyperLink = null;
            this.Label82.Left = 6.659F;
            this.Label82.Name = "Label82";
            this.Label82.Style = "font-weight: bold";
            this.Label82.Text = "D";
            this.Label82.Top = 1.875F;
            this.Label82.Width = 0.1358194F;
            // 
            // srptC1Part4Plan
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.499F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWH19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmended19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmendedTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDayStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDayEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearEnd;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmendedTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
	}
}
