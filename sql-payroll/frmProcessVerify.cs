//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public partial class frmProcessVerify : BaseForm
	{
		public frmProcessVerify()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmProcessVerify InstancePtr
		{
			get
			{
				return (frmProcessVerify)Sys.GetInstance(typeof(frmProcessVerify));
			}
		}

		protected frmProcessVerify _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       JANUARY 08,2003
		//
		// NOTES:
		//
		// MODIFIED BY:
		// **************************************************
		// private local variables
		private clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private double dblDeductionTotal;
		// vbPorter upgrade warning: lngPeriodNumber As int	OnWrite(string)
		private int lngPeriodNumber;
		private int lngJournalNumber;
		private bool boolUnloadForm;
		private bool boolTaxesAgain;
		private bool pboolMultiFund;
		private bool boolFromSchool;
		private double dblGrossTotal;
		private double dblTATotal;
		int lngBank;
		bool boolACH;
		private bool boolEncumberContracts;

		private void frmProcessVerify_Activated(object sender, System.EventArgs e)
		{
			// Me.Height = Me.Height - 25
			if (boolUnloadForm)
			{
				boolUnloadForm = false;
				Close();
			}
		}

		private bool AllValidAccountNumbers()
		{
			bool AllValidAccountNumbers = false;
			clsDRWrapper rsData = new clsDRWrapper();
			if (!modGlobalVariables.Statics.gboolBudgetary)
			{
				AllValidAccountNumbers = true;
				return AllValidAccountNumbers;
			}
			// GET ALL RECORDS FROM THE TBLTEMPPAYPROCESS
			rsData.OpenRecordset("Select * from tblTempPayProcess", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				// VERIFY THE DISTRIBUTION RECORDS HAVE A VALID ACCOUNT NUMBER
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"))) != string.Empty)
				{
					if (modValidateAccount.AccountValidate(rsData.Get_Fields_String("DistAccountNumber")))
					{
					}
					else
					{
						MessageBox.Show("Employee: " + rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("EmployeeName") + "  has an invalid distribution account number: " + rsData.Get_Fields_String("DistAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DistributionRecord")))
					{
						MessageBox.Show("Employee: " + rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("EmployeeName") + "  has an invalid distribution account number: " + rsData.Get_Fields_String("DistAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				// VERIFY THE EMPLOYERS MATCH ACCOUNT NUMBERS ARE VALID
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber"))) != string.Empty)
				{
					if (modValidateAccount.AccountValidate(rsData.Get_Fields_String("MatchAccountNumber")))
					{
					}
					else
					{
						MessageBox.Show("Employee: " + rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("EmployeeName") + "  has an invalid match account number: " + rsData.Get_Fields_String("MatchAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("MatchRecord")))
					{
						MessageBox.Show("Employee: " + rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("EmployeeName") + "  has an invalid match account number: " + rsData.Get_Fields_String("MatchAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber"))) != string.Empty)
				{
					if (modValidateAccount.AccountValidate(rsData.Get_Fields_String("DeductionAccountNumber")))
					{
					}
					else
					{
						MessageBox.Show("Invalid deduction account number: " + rsData.Get_Fields_String("DeductionAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DeductionRecord")))
					{
						MessageBox.Show("Invalid deduction account number: " + rsData.Get_Fields_String("DeductionAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MatchGLAccountNumber"))) != string.Empty)
				{
					if (modValidateAccount.AccountValidate(rsData.Get_Fields_String("MatchGLAccountNumber")))
					{
					}
					else
					{
						MessageBox.Show("Invalid match GL account number: " + rsData.Get_Fields_String("MatchGLAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("MatchRecord")))
					{
						MessageBox.Show("Invalid match GL account number: " + rsData.Get_Fields_String("MatchGLAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("TrustDeductionAccountNumber"))) != string.Empty)
				{
					if (modValidateAccount.AccountValidate(rsData.Get_Fields_String("TrustDeductionAccountNumber")))
					{
					}
					else
					{
						MessageBox.Show("Invalid Trust and Agency deduction account number: " + rsData.Get_Fields_String("TrustDeductionAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("TrustRecord")))
					{
						MessageBox.Show("Invalid Trust and Agency deduction account number: " + rsData.Get_Fields_String("TrustDeductionAccountNumber") + "\r" + "\r" + "After this account has been fixed, the Data Edit and Calculate routine will need to be rerun.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						AllValidAccountNumbers = false;
						return AllValidAccountNumbers;
					}
				}
				rsData.MoveNext();
			}
			AllValidAccountNumbers = true;
			return AllValidAccountNumbers;
		}

		private void frmProcessVerify_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProcessVerify properties;
			//frmProcessVerify.ScaleWidth	= 8880;
			//frmProcessVerify.ScaleHeight	= 7575;
			//frmProcessVerify.LinkTopic	= "Form1";
			//frmProcessVerify.LockControls	= -1  'True;
			//End Unmaped Properties
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblDefaultInformation");
			if (rsData.EndOfFile())
			{
				pboolMultiFund = false;
				boolACH = false;
				boolEncumberContracts = false;
			}
			else
			{
				pboolMultiFund = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("UseMultiFund"));
				boolACH = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("achclearinghouse"));
				boolEncumberContracts = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("encumbercontracts"));
			}
			// ***********************************************************
			if (modGlobalConstants.Statics.gboolBD)
			{
				lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank"))));
				if (lngBank == 0)
				{
					// no bank yet
					lngBank = frmChooseBank.InstancePtr.Init();
				}
			}
			bool executeAllowToPass = false;
			if (AllValidAccountNumbers())
			{
				executeAllowToPass = true;
				goto AllowToPass;
			}
			else
			{
				string strReturn = "";
				if (MessageBox.Show("Verify all account numbers with the Budgetary database.", "TRIO Software", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.Cancel)
				{
					strReturn = Interaction.InputBox("Enter TRIO Code", "TRIO Software", null);
					if (strReturn == "TRIO2001")
					{
						executeAllowToPass = true;
						goto AllowToPass;
					}
				}
				boolUnloadForm = true;
			}
			AllowToPass:
			;
			if (executeAllowToPass)
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modGlobalFunctions.SetFixedSize(this, 0);
				//modGlobalFunctions.SetTRIOColors(this);
				SetPayCatGridProperties();
				LoadPayCatGrid();
				SetDeductionGridProperties();
				LoadDeductionGrid();
				SetMatchGridProperties();
				LoadMatchGrid();
				SetTotalsGridProperties();
                SetCheckSummaryGridProperties();
				LoadTotalsGrid();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				rsData.Execute("Update tblTempPayProcess Set PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", modGlobalVariables.DEFAULTDATABASE);
				rsData.Execute("Update tblTempPayProcess Set PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				boolUnloadForm = false;
				executeAllowToPass = false;
			}
		}

		private void frmProcessVerify_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void SetTotalsGridProperties()
		{
			vsTotals.Cols = 5;
			//FC:FINAL:DDU:#i2506 - set fixedCols before setting other settings to grid
			vsTotals.FixedCols = 5;
			vsTotals.Rows = 6;
			vsTotals.ColWidth(0, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.15));
			vsTotals.ColWidth(1, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.25));
			vsTotals.ColWidth(2, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.02));
			// Spacer
			vsTotals.ColWidth(3, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.18));
			vsTotals.ColWidth(4, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.25));
			vsTotals.TextMatrix(0, 0, "Taxes");
			vsTotals.TextMatrix(0, 1, "");
			vsTotals.TextMatrix(0, 2, "");
			vsTotals.TextMatrix(0, 3, "Totals");
			vsTotals.TextMatrix(0, 4, "");
			//vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, vsTotals.Cols - 1, 0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsTotals.Cols - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsTotals.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsTotals.Cols - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsTotals.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 4, vsTotals.Cols - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 4, vsTotals.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);

			vsTotals.MergeRow(0, true);
			
			// .MergeRow(16) = True
			vsTotals.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
		}


        private void ResizeCheckSummaryGrid()
        {
            var width = GridCheckSummary.WidthOriginal;
            GridCheckSummary.ColWidth((int)CheckSummaryColumns.Description, (width * 0.21).ToInteger());
            GridCheckSummary.ColWidth((int)CheckSummaryColumns.Amount, (width * 0.24).ToInteger());
            GridCheckSummary.ColWidth(2, (width * 0.02).ToInteger());
            // Spacer
            GridCheckSummary.ColWidth((int)TaxableGrossColumns.Description, (width * 0.2).ToInteger());
            GridCheckSummary.ColWidth((int)TaxableGrossColumns.Amount, (width * 0.25).ToInteger());
        }
        private void SetCheckSummaryGridProperties()
        {
            GridCheckSummary.Cols = 5;
            GridCheckSummary.Rows = 10;
            GridCheckSummary.FixedCols = 5;
            GridCheckSummary.ColAlignment((int) CheckSummaryColumns.Amount,
                FCGrid.AlignmentSettings.flexAlignRightCenter);
            GridCheckSummary.ColAlignment((int) TaxableGrossColumns.Amount,
                FCGrid.AlignmentSettings.flexAlignRightCenter);
            //GridCheckSummary.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);



            GridCheckSummary.TextMatrix(0, 0, "Check Summary");
            GridCheckSummary.TextMatrix(0, 1, "");
            GridCheckSummary.TextMatrix(0, 2, "");
            GridCheckSummary.TextMatrix(0, 3, "Taxable Gross");
            GridCheckSummary.TextMatrix(0, 4, "");
            GridCheckSummary.MergeRow(0, true);
            GridCheckSummary.MergeCol(0, true);
            GridCheckSummary.MergeCol(1, true);
            GridCheckSummary.MergeCol(3, true);
            GridCheckSummary.MergeCol(4, true);
            GridCheckSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;

        }

		private void LoadTotalsGrid()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			double dblFed = 0;
			double dblState = 0;
			double dblFica = 0;
			double dblMed = 0;
			double dblFedGross = 0;
			double dblStateGross = 0;
			double dblFicaGross = 0;
			double dblMedGross = 0;
			// vbPorter upgrade warning: intRegChecks As int	OnWriteFCConvert.ToDouble(
			int intRegChecks = 0;
			// vbPorter upgrade warning: intDD As int	OnWriteFCConvert.ToDouble(
			int intDD = 0;
			// vbPorter upgrade warning: intBothDDReg As int	OnWriteFCConvert.ToDouble(
			int intBothDDReg = 0;
			double dblMatchTotal = 0;
			double dblBothRegularTotal = 0;
			double dblBothDirectDepositTotal = 0;
			double dblRegularTotal = 0;
			double dblDirectDepositTotal = 0;
			// vbPorter upgrade warning: intTAChecks As int	OnWriteFCConvert.ToInt32(
			int intTAChecks = 0;
			int lngTARecipientID = 0;
			rsData.OpenRecordset("Select * from tblTempPayProcess where TotalRecord = 1", modGlobalVariables.DEFAULTDATABASE);
			dblGrossTotal = 0;
			dblTATotal = 0;
			while (!rsData.EndOfFile())
			{
				dblFed += Conversion.Val(rsData.Get_Fields("FederalTaxWH"));
				dblState += Conversion.Val(rsData.Get_Fields("StateTaxWH"));
				dblFica += Conversion.Val(rsData.Get_Fields("FICATaxWH"));
				dblMed += Conversion.Val(rsData.Get_Fields("MedicareTaxWH"));
				dblFedGross += Conversion.Val(rsData.Get_Fields("FederalTaxGross"));
				dblStateGross += Conversion.Val(rsData.Get_Fields("StateTaxGross"));
				dblFicaGross += Conversion.Val(rsData.Get_Fields("FICATaxGross"));
				dblMedGross += Conversion.Val(rsData.Get_Fields("MedicareTaxGross"));
				dblGrossTotal += Conversion.Val(rsData.Get_Fields("GrossPay"));
				intRegChecks += rsData.Get_Fields_Int32("NumberChecks");
				intDD += rsData.Get_Fields_Int32("NumberDirectDeposits");
				intBothDDReg += rsData.Get_Fields_Int32("NumberBoth");
				dblRegularTotal += Conversion.Val(rsData.Get_Fields_Double("RegularCheckAmount"));
				dblDirectDepositTotal += Conversion.Val(rsData.Get_Fields_Double("DirectDepositAmount"));
				dblBothRegularTotal += Conversion.Val(rsData.Get_Fields_Double("BothRegularCheckAmount"));
				dblBothDirectDepositTotal += Conversion.Val(rsData.Get_Fields_Double("BothDirectDepositAmount"));
				rsData.MoveNext();
			}
			rsData.OpenRecordset("Select sum(MatchAmount) as MatchTotal from tblTempPayProcess", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				dblMatchTotal = 0;
			}
			else
			{
				dblMatchTotal = Conversion.Val(rsData.Get_Fields("MatchTotal"));
			}
			// set the tax totals and their captions
			vsTotals.TextMatrix(1, 0, "Federal");
			vsTotals.TextMatrix(1, 1, Strings.Format(dblFed, "Currency"));
			vsTotals.TextMatrix(2, 0, "FICA");
			vsTotals.TextMatrix(2, 1, Strings.Format(dblFica, "Currency"));
			vsTotals.TextMatrix(3, 0, "Medicare");
			vsTotals.TextMatrix(3, 1, Strings.Format(dblMed, "Currency"));
			vsTotals.TextMatrix(4, 0, "State");
			vsTotals.TextMatrix(4, 1, Strings.Format(dblState, "Currency"));
			vsTotals.TextMatrix(1, 3, "Gross Pay");
			vsTotals.TextMatrix(1, 4, Strings.Format(dblGrossTotal, "Currency"));
			vsTotals.TextMatrix(2, 3, "Taxes");
			vsTotals.TextMatrix(2, 4, Strings.Format((dblFed + dblState + dblFica + dblMed), "Currency"));
			vsTotals.TextMatrix(3, 3, "Deductions");
			vsTotals.TextMatrix(3, 4, Strings.Format(dblDeductionTotal, "Currency"));
			vsTotals.TextMatrix(4, 3, "Net Pay");
			//vsTotals.TextMatrix(4, 4, Strings.Format((Conversion.Val(vsTotals.TextMatrix(1, 4)) - Conversion.Val(vsTotals.TextMatrix(2, 4)) - Conversion.Val(vsTotals.TextMatrix(3, 4))), "Currency"));
			vsTotals.TextMatrix(4, 4, (dblGrossTotal - (dblFed + dblState + dblFica + dblMed) - dblDeductionTotal).FormatAsCurrency());
			vsTotals.TextMatrix(5, 3, "Emp Match");
			vsTotals.TextMatrix(5, 4, Strings.Format(dblMatchTotal, "Currency"));

			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularCheckCount,(int)CheckSummaryColumns.Description, "Reg Check");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularCheckCount, (int)CheckSummaryColumns.Amount, intRegChecks.ToString());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularCheckAmount, (int)CheckSummaryColumns.Description, "  Amount");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularCheckAmount, (int)CheckSummaryColumns.Amount,  dblRegularTotal.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.DirectDepositCount, (int)CheckSummaryColumns.Description, "Direct Dep");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.DirectDepositCount, (int)CheckSummaryColumns.Amount, intDD.ToString());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.DirectDepositAmount, (int)CheckSummaryColumns.Description, "  Amount");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.DirectDepositAmount, (int)CheckSummaryColumns.Amount, dblDirectDepositTotal.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDCount, (int)CheckSummaryColumns.Description, "Reg & DD");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDCount, (int)CheckSummaryColumns.Amount, intBothDDReg.ToString());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDRegularAmount, (int)CheckSummaryColumns.Description, "  Reg Amt");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDRegularAmount, (int)CheckSummaryColumns.Amount, dblBothRegularTotal.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDDDAmount, (int)CheckSummaryColumns.Description, "  DD Amt");
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.RegularAndDDDDAmount, (int)CheckSummaryColumns.Amount,dblBothDirectDepositTotal.FormatAsCurrency());
			
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Federal, (int)TaxableGrossColumns.Description, "Federal");
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Federal, (int)TaxableGrossColumns.Amount,dblFedGross.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.State, (int)TaxableGrossColumns.Description, "State");
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.State, (int)TaxableGrossColumns.Amount, dblStateGross.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Fica, (int)TaxableGrossColumns.Description, "FICA");
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Fica, (int)TaxableGrossColumns.Amount, dblFicaGross.FormatAsCurrency());
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Medicare, (int)TaxableGrossColumns.Description, "Medicare");
			GridCheckSummary.TextMatrix((int)TaxableGrossRows.Medicare, (int)TaxableGrossColumns.Amount, dblMedGross.FormatAsCurrency());

			// Call rsData.OpenRecordset("SELECT tblTempPayProcess.TrustAmount, tblTempPayProcess.TrustRecipientID "
			// & "FROM tblTempPayProcess where tblTempPayProcess.TrustRecord = 1", DEFAULTDATABASE)
			rsData.OpenRecordset("SELECT tblTempPayProcess.TrustAmount, tblTempPayProcess.TrustRecipientID " + "FROM tblTempPayProcess WHERE (((tblTempPayProcess.TrustAmount)<>0) AND ((tblTempPayProcess.TrustRecord)=1)) Order by tblTempPayProcess.TrustRecipientID");
			lngTARecipientID = 0;
			intTAChecks = 0;
			dblTATotal = 0;
			while (!rsData.EndOfFile())
			{
				dblGrossTotal += Conversion.Val(rsData.Get_Fields_Double("TrustAmount"));
				dblTATotal += Conversion.Val(rsData.Get_Fields("trustamount"));
				if (lngTARecipientID != FCConvert.ToInt32(rsData.Get_Fields_Int32("TrustRecipientID")) && Conversion.Val(rsData.Get_Fields("trustamount")) != 0)
				{
					intTAChecks += 1;
					lngTARecipientID = FCConvert.ToInt16(rsData.Get_Fields_Int32("TrustRecipientID"));
				}
				rsData.MoveNext();
			}
			rsData.OpenRecordset("SELECT DDBankNumber FROM tblTempPayProcess WHERE DDAmount <> 0 AND BankRecord = 1 group by ddbanknumber ");
			if (!rsData.EndOfFile())
			{
				rsData.MoveLast();
				rsData.MoveFirst();
				if (!boolACH)
				{
					intTAChecks += rsData.RecordCount();
				}
				else
				{
					// just the ach check
					intTAChecks += 1;
				}
			}
			dblTATotal += dblDirectDepositTotal + dblBothDirectDepositTotal;
			GridCheckSummary.TextMatrix((int)CheckSummaryRows.TACheckCount, (int)CheckSummaryColumns.Description, "T/A Checks");
            GridCheckSummary.TextMatrix((int)CheckSummaryRows.TACheckCount, (int)CheckSummaryColumns.Amount, intTAChecks.ToString());
            GridCheckSummary.TextMatrix((int)CheckSummaryRows.TACheckAmount, (int)CheckSummaryColumns.Description, "  T/A Amt");
            GridCheckSummary.TextMatrix((int)CheckSummaryRows.TACheckAmount, (int)CheckSummaryColumns.Amount, dblTATotal.FormatAsCurrency());
		}

		private void SetPayCatGridProperties()
		{
			vsPayCatGrid.Cols = 3;
			vsPayCatGrid.Rows = 2;
			// SET THE HEADINGS FOR THE GRID
			vsPayCatGrid.ColWidth(0, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.4));
			vsPayCatGrid.ColWidth(1, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.22));
			vsPayCatGrid.ColWidth(2, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.22));
			vsPayCatGrid.TextMatrix(0, 0, "Pay Category");
			vsPayCatGrid.TextMatrix(0, 1, "Hours");
			vsPayCatGrid.TextMatrix(0, 2, "Dollars");
			vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsPayCatGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsPayCatGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsPayCatGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsPayCatGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, vsPayCatGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 2, modGlobalConstants.Statics.TRIOCOLORBLUE);
			//vsPayCatGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 2, Color.White);
			vsPayCatGrid.ColFormat(1, "0.00");
			vsPayCatGrid.ColFormat(2, "Currency");
		}

		private void SetDeductionGridProperties()
		{
			vsDeductionGrid.Cols = 4;
			vsDeductionGrid.Rows = 2;
			vsDeductionGrid.FixedCols = 4;
			vsDeductionGrid.TextMatrix(0, 0, "Deduction");
			vsDeductionGrid.TextMatrix(0, 1, "Taxable");
			vsDeductionGrid.TextMatrix(0, 2, "Pre-Tax");
			vsDeductionGrid.TextMatrix(0, 3, "Other");
			vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDeductionGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsDeductionGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsDeductionGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsDeductionGrid.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsDeductionGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, vsDeductionGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsDeductionGrid.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 3, modGlobalConstants.Statics.TRIOCOLORBLUE);
			//vsDeductionGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 3, Color.White);
			vsDeductionGrid.ColFormat(1, "Currency");
			vsDeductionGrid.ColFormat(2, "Currency");
			vsDeductionGrid.ColFormat(3, "Currency");
		}

		private void SetMatchGridProperties()
		{
			vsMatchGrid.Cols = 4;
			vsMatchGrid.Rows = 2;
			vsMatchGrid.FixedCols = 4;
			vsMatchGrid.TextMatrix(0, 0, "Employer Match");
			vsMatchGrid.TextMatrix(0, 1, "Taxable");
			vsMatchGrid.TextMatrix(0, 2, "Pre-Tax");
			vsMatchGrid.TextMatrix(0, 3, "Other");
			//vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsMatchGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsMatchGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsMatchGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsMatchGrid.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
           // vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, vsMatchGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
           //vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, vsMatchGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
           // vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsMatchGrid.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 3, modGlobalConstants.Statics.TRIOCOLORBLUE);
			//vsMatchGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 3, Color.White);
			vsMatchGrid.ColFormat(1, "Currency");
			vsMatchGrid.ColFormat(2, "Currency");
			vsMatchGrid.ColFormat(3, "Currency");
		}

		private void LoadPayCatGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadPayCatGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter = 0;
				int intCatNumber = 0;
				double dblTotal = 0;
				double dblDollarsTotal = 0;
				string strEmployeeNumber = "";
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				// get all employees
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster", modGlobalVariables.DEFAULTDATABASE);
				rsData.OpenRecordset("SELECT tblPayCategories.*, tblTempPayProcess.* FROM tblTempPayProcess INNER JOIN tblPayCategories ON tblTempPayProcess.DistPayCategory = tblPayCategories.ID Where distributionrecord = 1 ORDER BY tblTempPayProcess.DistPayCategory", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					return;
				}
				else
				{
					intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DistPayCategory"));
					intCounter = 1;
				}
				while (!rsData.EndOfFile())
				{
					strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
					rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);
					if (rsMaster.NoMatch)
					{
						goto NextRecord;
					}
					else
					{
						// check the status and frequency
						if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
						{
							goto NextRecord;
						}
						else
						{
							if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
							{
								// everything is ok so add this to the totals
								if (intCatNumber != FCConvert.ToInt32(rsData.Get_Fields_Int32("DistPayCategory")))
								{
									vsPayCatGrid.Rows += 1;
									vsPayCatGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollCategory(intCatNumber)));
									vsPayCatGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
									vsPayCatGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "Currency"));
									intCounter += 1;
									intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DistPayCategory"));
									dblTotal = 0;
									dblDollarsTotal = 0;
								}
								if (FCConvert.ToString(rsData.Get_Fields("Type")) == "Dollars")
								{
									dblDollarsTotal += rsData.Get_Fields("DistGrossPay");
								}
								else
								{
									dblDollarsTotal += rsData.Get_Fields("DistGrossPay");
									dblTotal += Conversion.Val(rsData.Get_Fields("DistHours"));
								}
							}
							else
							{
								goto NextRecord;
							}
						}
					}
					NextRecord:
					;
					rsData.MoveNext();
				}
				vsPayCatGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollCategory(intCatNumber)));
				vsPayCatGrid.TextMatrix(intCounter, 1, Strings.Format(dblTotal, "0.00"));
				vsPayCatGrid.TextMatrix(intCounter, 2, Strings.Format(dblDollarsTotal, "Currency"));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadDeductionGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadDistributionGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter = 0;
				int intCatNumber = 0;
				double dblTaxTotal = 0;
				double dblPreTotal = 0;
				double dblOtherTotal = 0;
				string strEmployeeNumber = "";
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				// get all employees
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster", modGlobalVariables.DEFAULTDATABASE);
				rsData.OpenRecordset("SELECT tblTempPayProcess.*, tblTaxStatusCodes.*, tblDeductionSetup.* FROM (tblTempPayProcess INNER JOIN tblDeductionSetup ON tblTempPayProcess.DedDeductionNumber = tblDeductionSetup.ID) INNER JOIN tblTaxStatusCodes ON tblDeductionSetup.TaxStatusCode = tblTaxStatusCodes.ID Where deductionrecord = 1 Order by tblDeductionSetup.ID", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					return;
				}
				else
				{
					intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DedDeductionNumber"));
					intCounter = 1;
				}
				dblDeductionTotal = 0;
				while (!rsData.EndOfFile())
				{
					strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
					rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);
					if (rsMaster.NoMatch)
					{
						goto NextRecord;
					}
					else
					{
						// check the status and frequency
						if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
						{
							goto NextRecord;
						}
						else
						{
							if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
							{
								// everything is ok so add this to the totals
								if (intCatNumber != FCConvert.ToInt32(rsData.Get_Fields_Int32("DedDeductionNumber")))
								{
									vsDeductionGrid.Rows += 1;
									vsDeductionGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollDeduction(intCatNumber)));
									vsDeductionGrid.TextMatrix(intCounter, 1, Strings.Format(dblTaxTotal, "0.00"));
									vsDeductionGrid.TextMatrix(intCounter, 2, Strings.Format(dblPreTotal, "0.00"));
									vsDeductionGrid.TextMatrix(intCounter, 3, Strings.Format(dblOtherTotal, "0.00"));
									intCounter += 1;
									intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DedDeductionNumber"));
									dblDeductionTotal += dblTaxTotal + dblPreTotal + dblOtherTotal;
									dblTaxTotal = 0;
									dblPreTotal = 0;
									dblOtherTotal = 0;
								}
								// 1 is the ID for Pre-Tax in tblTaxStatusCodes
								// 2 is the ID for Tax in tblTaxStatusCodes
								if (FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "2" || FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "T")
								{
									// T
									dblTaxTotal += rsData.Get_Fields("DedAmount");
								}
								else if (FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "1" || FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "P")
								{
									// P
									dblPreTotal += Conversion.Val(rsData.Get_Fields("DedAmount"));
								}
								else
								{
									dblOtherTotal += Conversion.Val(rsData.Get_Fields("DedAmount"));
								}
							}
							else
							{
								goto NextRecord;
							}
						}
					}
					NextRecord:
					;
					rsData.MoveNext();
				}
				// now set the last record
				vsDeductionGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollDeduction(intCatNumber)));
				vsDeductionGrid.TextMatrix(intCounter, 1, Strings.Format(dblTaxTotal, "0.00"));
				vsDeductionGrid.TextMatrix(intCounter, 2, Strings.Format(dblPreTotal, "0.00"));
				vsDeductionGrid.TextMatrix(intCounter, 3, Strings.Format(dblOtherTotal, "0.00"));
				dblDeductionTotal += dblTaxTotal + dblPreTotal + dblOtherTotal;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadMatchGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadDistributionGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int intCounter = 0;
				int intCatNumber = 0;
				double dblTaxTotal = 0;
				double dblPreTotal = 0;
				double dblOtherTotal = 0;
				string strEmployeeNumber = "";
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				// get all employees
				rsMaster.OpenRecordset("Select * from tblEmployeeMaster", modGlobalVariables.DEFAULTDATABASE);
				rsData.OpenRecordset("SELECT tblTempPayProcess.*, tblDeductionSetup.*, tblTaxStatusCodes.* FROM (tblTempPayProcess INNER JOIN tblDeductionSetup ON tblTempPayProcess.MatchDeductionNumber = tblDeductionSetup.ID) INNER JOIN tblTaxStatusCodes ON tblDeductionSetup.TaxStatusCode = tblTaxStatusCodes.ID WHERE (((tblTempPayProcess.MatchRecord)=1)) order by tblDeductionSetup.ID", modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					return;
				}
				else
				{
					intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("MatchDeductionNumber"));
					intCounter = 1;
				}
				while (!rsData.EndOfFile())
				{
					strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
					rsMaster.FindFirstRecord("EmployeeNumber", strEmployeeNumber);
					if (rsMaster.NoMatch)
					{
						goto NextRecord;
					}
					else
					{
						// check the status and frequency
						if (FCConvert.ToString(rsMaster.Get_Fields_String("Status")) != "Active")
						{
							goto NextRecord;
						}
						else
						{
							if (modGlobalRoutines.ApplyThisEmployee(rsMaster.Get_Fields("FreqCodeID"), strEmployeeNumber))
							{
								// everything is ok so add this to the totals
								if (intCatNumber != FCConvert.ToInt32(rsData.Get_Fields_Int32("MatchDeductionNumber")))
								{
									vsMatchGrid.Rows += 1;
									vsMatchGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollDeduction(intCatNumber)));
									vsMatchGrid.TextMatrix(intCounter, 1, Strings.Format(dblTaxTotal, "0.00"));
									vsMatchGrid.TextMatrix(intCounter, 2, Strings.Format(dblPreTotal, "0.00"));
									vsMatchGrid.TextMatrix(intCounter, 3, Strings.Format(dblOtherTotal, "0.00"));
									intCounter += 1;
									intCatNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("MatchDeductionNumber"));
									dblTaxTotal = 0;
									dblPreTotal = 0;
									dblOtherTotal = 0;
								}
								// 1 is the ID for Pre-Tax in tblTaxStatusCodes
								// 2 is the ID for Tax in tblTaxStatusCodes
								if (FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "2" || FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "T")
								{
									// T
									dblTaxTotal += rsData.Get_Fields("MatchAmount");
								}
								else if (FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "1" || FCConvert.ToString(rsData.Get_Fields("TaxStatusCode")) == "P")
								{
									// P
									dblPreTotal += Conversion.Val(rsData.Get_Fields("MatchAmount"));
								}
								else
								{
									dblOtherTotal += Conversion.Val(rsData.Get_Fields("MatchAmount"));
								}
							}
							else
							{
								goto NextRecord;
							}
						}
					}
					NextRecord:
					;
					rsData.MoveNext();
				}
				// now set the last record
				vsMatchGrid.TextMatrix(intCounter, 0, FCConvert.ToString(GetPayrollDeduction(intCatNumber)));
				vsMatchGrid.TextMatrix(intCounter, 1, Strings.Format(dblTaxTotal, "0.00"));
				vsMatchGrid.TextMatrix(intCounter, 2, Strings.Format(dblPreTotal, "0.00"));
				vsMatchGrid.TextMatrix(intCounter, 3, Strings.Format(dblOtherTotal, "0.00"));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object GetPayrollCategory(int intValue)
		{
			object GetPayrollCategory = null;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GetPayrollCategory";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select Description from tblPayCategories where ID = " + FCConvert.ToString(intValue), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					GetPayrollCategory = string.Empty;
				}
				else
				{
					GetPayrollCategory = rsData.Get_Fields("Description");
				}
				return GetPayrollCategory;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return GetPayrollCategory;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object GetPayrollDeduction(int intValue)
		{
			object GetPayrollDeduction = null;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GetPayrollCategory";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsData.OpenRecordset("Select Description from tblDeductionSetup where ID = " + FCConvert.ToString(intValue), modGlobalVariables.DEFAULTDATABASE);
				if (rsData.EndOfFile())
				{
					GetPayrollDeduction = string.Empty;
				}
				else
				{
					GetPayrollDeduction = rsData.Get_Fields("Description");
				}
				return GetPayrollDeduction;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return GetPayrollDeduction;
				clsDRWrapper rsData = new clsDRWrapper();
			}
		}

		private void frmProcessVerify_Resize(object sender, System.EventArgs e)
		{
			vsDeductionGrid.Cols = 4;
			vsDeductionGrid.ColWidth(0, FCConvert.ToInt32(vsDeductionGrid.WidthOriginal * 0.25));
			vsDeductionGrid.ColWidth(1, FCConvert.ToInt32(vsDeductionGrid.WidthOriginal * 0.21));
			vsDeductionGrid.ColWidth(2, FCConvert.ToInt32(vsDeductionGrid.WidthOriginal * 0.21));
			vsDeductionGrid.ColWidth(3, FCConvert.ToInt32(vsDeductionGrid.WidthOriginal * 0.21));
			vsMatchGrid.Cols = 4;
			vsMatchGrid.ColWidth(0, FCConvert.ToInt32(vsMatchGrid.WidthOriginal * 0.25));
			vsMatchGrid.ColWidth(1, FCConvert.ToInt32(vsMatchGrid.WidthOriginal * 0.21));
			vsMatchGrid.ColWidth(2, FCConvert.ToInt32(vsMatchGrid.WidthOriginal * 0.21));
			vsMatchGrid.ColWidth(3, FCConvert.ToInt32(vsMatchGrid.WidthOriginal * 0.21));
			vsPayCatGrid.ColWidth(0, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.4));
			vsPayCatGrid.ColWidth(1, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.22));
			vsPayCatGrid.ColWidth(2, FCConvert.ToInt32(vsPayCatGrid.WidthOriginal * 0.22));
			
			vsTotals.ColWidth(0, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.2));
			vsTotals.ColWidth(1, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.25));
			vsTotals.ColWidth(2, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.02));
			// Spacer
			vsTotals.ColWidth(3, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.2));
			vsTotals.ColWidth(4, FCConvert.ToInt32(vsTotals.WidthOriginal * 0.25));
            ResizeCheckSummaryGrid();
		}

		private bool GetJournalNumber()
		{
			bool GetJournalNumber = false;
			string strText = "";
			int intArrayID = 0;
			string strAccount = "";
			int intDeductionNumber = 0;
			string strDeductionTitle = "";
			// Dim dblValue            As Double
			// vbPorter upgrade warning: dblValue As Decimal	OnWrite(Decimal, string, int)
			Decimal dblValue = 0;
			clsDRWrapper rspayrollaccounts = new clsDRWrapper();
			clsDRWrapper rsSchoolAccounts = new clsDRWrapper();
			string strTemp = "";
			string strCashAcct = "";
			string strSchoolCashAcct = "";
			bool boolSeparateAccts;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngProject = 0;
			GetJournalNumber = false;
			boolSeparateAccts = false;
			// NOW ADD THE DATA TO BUDGETARY
			// ONLY WANT EXPENSE AND DEPRECIATION
			// If gboolBudgetary Then
			if (modGlobalConstants.Statics.gboolBD)
			{
				modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				modGlobalVariables.Statics.SortedAccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				intArrayID = 0;
				modAccountTitle.SetAccountFormats();
				// If lngBank = 0 Then
				// lngBank = Val(GetBankVariable("PayrollBank"))
				// End If
				// If lngBank = 0 Then
				// no bank yet
				// Screen.MousePointer = vbDefault
				// DoEvents
				// lngBank = frmChooseBank.Init
				// If lngBank <= 0 Then
				// MsgBox "No Bank Chosen" & vbNewLine & "Cannot Continue", vbExclamation, "No Bank"
				// Exit Function
				// End If
				// Screen.MousePointer = vbHourglass
				// End If
				rspayrollaccounts.Execute("update tblPayrollSteps set banknumber = " + FCConvert.ToString(lngBank) + " where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "twpy0000.vb1");
				// DISTRIBUTION RECORDS
				// rsData.OpenRecordset "SELECT DistAccountNumber,project, SUM(DistGrossPay) as TotalPay FROM tblTempPayProcess WHERE distributionrecord = 1 GROUP BY DistAccountNumber,project HAVING SUM(DistGrossPay) <> 0 ORDER BY DistAccountNumber,project"
				if (!boolEncumberContracts)
				{
					rsData.OpenRecordset("SELECT DistAccountNumber,project, SUM(DistGrossPay) as TotalPay, 0 as contract FROM tblTempPayProcess WHERE distributionrecord = 1 GROUP BY DistAccountNumber,project HAVING SUM(DistGrossPay) <> 0 ORDER BY DistAccountNumber,project");
				}
				else
				{
					rsData.OpenRecordset("SELECT DistAccountNumber,project,SUM(DistGrossPay) as TotalPay,0 as contract FROM tblTempPayProcess WHERE convert(int, isnull(contractid, 0)) = 0 and  distributionrecord = 1 GROUP BY DistAccountNumber,project HAVING SUM(DistGrossPay) <> 0  union all SELECT DistAccountNumber,project, SUM(DistGrossPay) as TotalPay,1 as contract FROM tblTempPayProcess WHERE convert(int, isnull(contractid, 0)) > 0 and  distributionrecord = 1 GROUP BY DistAccountNumber,project HAVING SUM(DistGrossPay) <> 0 ORDER BY DistAccountNumber,project", "twpy0000.vb1");
				}
				bool boolContract = false;
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
					lngProject = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("project"))));
					boolContract = false;
					boolContract = FCConvert.ToBoolean(rsData.Get_Fields("contract"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == rsData.Get_Fields_String("DistAccountNumber") && lngProject == Conversion.Val(rsData.Get_Fields("project")) && boolContract == FCConvert.CBool(rsData.Get_Fields("contract")))
						{
							dblValue += FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalPay"), "0.00"));
							// dblvalue = format(dblvalue,"0.00")
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
								modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
								modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
								modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll (Dist)";
								modGlobalVariables.Statics.AccountStrut[intArrayID].Project = "";
								if (boolContract)
								{
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "E";
									// boolContract = True
									// AccountStrut(intArrayID).Amount = -AccountStrut(intArrayID).Amount
									// dblValue = -dblValue
								}
								else
								{
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									// boolContract = False
								}
								if (lngProject > 0)
								{
									rsTemp.OpenRecordset("select * from projectmaster where id = " + FCConvert.ToString(lngProject), "twbd0000.vb1");
									if (!rsTemp.EndOfFile())
									{
										modGlobalVariables.Statics.AccountStrut[intArrayID].Project = FCConvert.ToString(rsTemp.Get_Fields("projectcode"));
									}
								}
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalPay"), "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
							lngProject = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("project"))));
							boolContract = FCConvert.ToBoolean(rsData.Get_Fields("CONTRACT"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll (Dist)";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Project = "";
						if (boolContract)
						{
							modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "E";
							// AccountStrut(intArrayID).Amount = -AccountStrut(intArrayID).Amount
							// dblValue = -dblValue
						}
						else
						{
							modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						}
						if (lngProject > 0)
						{
							rsTemp.OpenRecordset("select * from projectmaster where id = " + FCConvert.ToString(lngProject), "twbd0000.vb1");
							if (!rsTemp.EndOfFile())
							{
								modGlobalVariables.Statics.AccountStrut[intArrayID].Project = FCConvert.ToString(rsTemp.Get_Fields("projectcode"));
							}
						}
						intArrayID += 1;
					}
				}
				rsData.Execute("Update tblTempPayProcess Set PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'", modGlobalVariables.DEFAULTDATABASE);
				rsData.Execute("Update tblTempPayProcess Set PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				// NOW PASS THE TAX INFORMATION
				string strSQL = "";
				clsDRWrapper rsAmountInfo = new clsDRWrapper();
				strSQL = " ";

				if (boolSeparateAccts)
				{
					rsAmountInfo.OpenRecordset("SELECT fromschool,Sum(FederalTaxWH) AS FederalTaxTotal, Sum(StateTaxWH) AS StateTaxTotal, Sum(MedicareTaxWH) AS MedicareTaxTotal, Sum(FICATaxWH) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, MultiFundNumber From tblTempPayProcess Where TotalRecord = 1 GROUP BY fromschool,MultiFundNumber");
				}
				else
				{
					rsAmountInfo.OpenRecordset("SELECT 0 as fromschool,Sum(FederalTaxWH) AS FederalTaxTotal, Sum(StateTaxWH) AS StateTaxTotal, Sum(MedicareTaxWH) AS MedicareTaxTotal, Sum(FICATaxWH) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal, MultiFundNumber From tblTempPayProcess Where TotalRecord = 1 GROUP BY MultiFundNumber");
				}
				rsData.OpenRecordset("select * from tblpayrollaccounts order by id", "twpy0000.vb1");
				rsSchoolAccounts.OpenRecordset("select * from payrollschoolaccounts order by id", "twpy0000.vb1");
				if (rsData.EndOfFile())
				{
				}
				else
				{
					while (!rsAmountInfo.EndOfFile())
					{
						// Federal Tax WH
						if (!FCConvert.ToBoolean(rsAmountInfo.Get_Fields_Boolean("fromschool")))
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("FederalTaxTotal")) == 0)
							{
							}
							else
							{
								rsData.FindFirstRecord("CategoryID", 1);
								if (rsData.NoMatch)
								{
								}
								else
								{
									// GET THE NEW ACCOUNT NUMBER WITH THE DIFFERENT FUND OBTAINED FROM THE MASTER DEPT/DIV
									strText = FCConvert.ToString(rsData.Get_Fields("Account"));
									if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
									{
										// corey 01/03/2006 Need to format fund
										Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
										modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									}
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsAmountInfo.Get_Fields("FederalTaxTotal") * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(FedTWH)";
									intArrayID += 1;
								}
							}
						}
						else
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("federaltaxtotal")) != 0)
							{
								if (rsSchoolAccounts.FindFirstRecord("categoryid", 1))
								{
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsSchoolAccounts.Get_Fields("account"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsAmountInfo.Get_Fields("federaltaxtotal") * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(FedTWH)";
									intArrayID += 1;
								}
							}
						}
						// FICA TAX WH
						// MULTIPLY BY -2 BECAUSE WE NEED TO TAKE INTO ACCOUNT THAT THE
						// EMPLOYER IS MATCHING WHAT WAS WITHHEALD FROM THE EMPLOYEE
						if (!FCConvert.ToBoolean(rsAmountInfo.Get_Fields_Boolean("fromschool")))
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("FICATaxTotal")) == 0 && Conversion.Val(rsAmountInfo.Get_Fields("EmployerFicaTaxTotal")) == 0)
							{
							}
							else
							{
								rsData.FindFirstRecord("CategoryID", 2);
								if (rsData.NoMatch)
								{
								}
								else
								{
									// GET THE NEW ACCOUNT NUMBER WITH THE DIFFERENT FUND OBTAINED FROM THE MASTER DEPT/DIV
									strText = FCConvert.ToString(rsData.Get_Fields("Account"));
									if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
									{
										Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
										modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									}
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									// rsData.Fields("Account")
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format((rsAmountInfo.Get_Fields("FICATaxTotal") + rsAmountInfo.Get_Fields("EmployerFicaTaxTotal")) * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(FICAWH)";
									intArrayID += 1;
								}
							}
						}
						else
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("ficataxtotal")) != 0)
							{
								if (rsSchoolAccounts.FindFirstRecord("CategoryID", 2))
								{
									strText = FCConvert.ToString(rsSchoolAccounts.Get_Fields("account"));
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format((rsAmountInfo.Get_Fields("FICATaxTotal") + rsAmountInfo.Get_Fields("EmployerFicaTaxTotal")) * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(FICAWH)";
									intArrayID += 1;
								}
							}
						}
						// MEDICARE TAX WH
						// MULTIPLY BY -2 BECAUSE WE NEED TO TAKE INTO ACCOUNT THAT THE
						// EMPLOYER IS MATCHING WHAT WAS WITHHEALD FROM THE EMPLOYEE
						if (!FCConvert.ToBoolean(rsAmountInfo.Get_Fields_Boolean("fromschool")))
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("MedicareTaxTotal")) == 0)
							{
							}
							else
							{
								rsData.FindFirstRecord("CategoryID", 3);
								if (rsData.NoMatch)
								{
								}
								else
								{
									// GET THE NEW ACCOUNT NUMBER WITH THE DIFFERENT FUND OBTAINED FROM THE MASTER DEPT/DIV
									strText = FCConvert.ToString(rsData.Get_Fields("Account"));
									if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
									{
										Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
										modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									}
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									// rsData.Fields("Account")
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format((rsAmountInfo.Get_Fields("MedicareTaxTotal") + rsAmountInfo.Get_Fields("EmployerMedicareTaxTotal")) * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(MTaxWH)";
									intArrayID += 1;
								}
							}
						}
						else
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("medicaretaxtotal")) != 0)
							{
								if (rsSchoolAccounts.FindFirstRecord("CategoryID", 3))
								{
									strText = FCConvert.ToString(rsSchoolAccounts.Get_Fields("account"));
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format((rsAmountInfo.Get_Fields("medicaretaxtotal") + rsAmountInfo.Get_Fields("EmployerMedicareTaxTotal")) * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(MTaxWH)";
									intArrayID += 1;
								}
							}
						}
						// State Tax WH
						if (!FCConvert.ToBoolean(rsAmountInfo.Get_Fields_Boolean("fromschool")))
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("StateTaxTotal")) == 0)
							{
							}
							else
							{
								rsData.FindFirstRecord("CategoryID", 4);
								if (rsData.NoMatch)
								{
								}
								else
								{
									// GET THE NEW ACCOUNT NUMBER WITH THE DIFFERENT FUND OBTAINED FROM THE MASTER DEPT/DIV
									strText = FCConvert.ToString(rsData.Get_Fields("Account"));
									if (modGlobalVariables.Statics.gintUseGroupMultiFund != 0)
									{
										Strings.MidSet(ref strText, 3, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
										modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									}
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									// rsData.Fields("Account")
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsAmountInfo.Get_Fields("StateTaxTotal") * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(STaxWH)";
									intArrayID += 1;
								}
							}
						}
						else
						{
							if (Conversion.Val(rsAmountInfo.Get_Fields("statetaxtotal")) != 0)
							{
								if (rsSchoolAccounts.FindFirstRecord("CategoryID", 4))
								{
									strText = FCConvert.ToString(rsSchoolAccounts.Get_Fields("account"));
									Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
									modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
									modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
									modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
									modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsAmountInfo.Get_Fields("statetaxtotal") * -1, "0.00"));
									modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(STaxWH)";
									intArrayID += 1;
								}
							}
						}
						
						rsAmountInfo.MoveNext();
					}
				}
			
				rsData.Execute("delete from tblTempTax", "twpy0000.vb1");
				modGlobalRoutines.SetupTempTaxTable(modGlobalVariables.Statics.gdatCurrentPayDate, modGlobalVariables.Statics.gintCurrentPayRun, "tblTempPayProcess");
				rsData.OpenRecordset("SELECT SUM(MedicareTax) AS MedicareTaxTotal,sum(EMployerMedicareTax) as EmployerMedicareTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(MedicareTax) <> 0 ORDER BY DeptDiv");
				while (!rsData.EndOfFile())
				{
					if (Conversion.Val(rsData.Get_Fields("EmployerMedicareTaxTotal")) == 0)
					{
					}
					else
					{
						// this record is Medicare
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("EmployerMedicareTaxTotal"), "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(MTaxTot)";
						intArrayID += 1;
					}
					rsData.MoveNext();
				}
				rsData.OpenRecordset("SELECT SUM(FICATax) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(FICATax) <> 0 ORDER BY DeptDiv");
				while (!rsData.EndOfFile())
					{
						// this record is FICA
						if (Conversion.Val(rsData.Get_Fields("FICATaxTotal")) == 0)
						{
						}
						else
						{
							Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
							modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsData.Get_Fields("DeptDiv"));
							modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
							modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
							// AccountStrut(intArrayID).Amount = Format(rsData.Fields("FICATaxTotal"), "0.00")
							modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("EmployerFicaTaxTotal"), "0.00"));
							modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(FICATot)";
							intArrayID += 1;
						}
						rsData.MoveNext();
					}
				
				
				// DEDUCTION RECORDS
				dblValue = 0;
				rsData.OpenRecordset("SELECT DeductionTitle, DedDeductionNumber, DeductionAccountNumber, SUM(DedAmount) as TotalPay, DeductionTitle FROM tblTempPayProcess WHERE deductionrecord = 1 GROUP BY DeductionTitle, DedDeductionNumber, DeductionAccountNumber, DeductionTitle HAVING SUM(DedAmount) <> 0 ORDER BY DeductionAccountNumber,DedDeductionNumber");
				// Call rsdata.OpenRecordset("Select * from tblTempPayProcess where deductionrecord = 1 order by DistAccountNumber", DEFAULTDATABASE)
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber"));
					intDeductionNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DedDeductionNumber"));
					strDeductionTitle = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DeductionTitle")));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber")) && intDeductionNumber == FCConvert.ToInt32(rsData.Get_Fields_Int32("DedDeductionNumber")))
						{
							dblValue += FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalPay"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
								modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
								modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
								modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
								modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(Ded)";
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TotalPay"), "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("DeductionAccountNumber"));
							intDeductionNumber = FCConvert.ToInt16(rsData.Get_Fields_Int32("DedDeductionNumber"));
							strDeductionTitle = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DeductionTitle")));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(Ded)";
						intArrayID += 1;
					}
				}
				// EMPLOYERS MATCH RECORDS DEBITS
				dblValue = 0;
				rsData.OpenRecordset("SELECT SUM(TotalMatchAmount) AS GrandTotalMatchAmount, MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber FROM (SELECT MatchGLAccountNumber as MatchAccountNumber, SUM(MatchAmount) * -1 as TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM tblTempPayProcess WHERE matchrecord = 1 GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchGLAccountNumber UNION SELECT MatchAccountNumber, Sum(MatchAmount) AS TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM tblTempPayProcess WHERE matchrecord = 1 GROUP BY MatchAccountNumber, MatchDeductionTitle, MatchDeductionNumber) as Temp GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber ORDER BY MatchAccountNumber, SUM(TotalMatchAmount)");
				// Call rsdata.OpenRecordset("Select * from tblTempPayProcess where matchrecord = 1 order by DistAccountNumber", DEFAULTDATABASE)
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber")))
						{
							dblValue += FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("GrandTotalMatchAmount"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
								modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
								modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
								modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
								modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
								modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(MatchD)";
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("GrandTotalMatchAmount"), "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("MatchAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(MatchD)";
						intArrayID += 1;
					}
				}
				// may need to separate the trust and agency stuff
				// NOW GET THE TRUST AND AGANCY CHECKS
				rsData.OpenRecordset("SELECT TrustCategoryID, TrustDeductionAccountNumber, SUM(TrustAmount) as TrustAmountTotal, TrustDeductionDescription FROM tblTempPayProcess WHERE TrustRecord = 1 GROUP BY TrustCategoryID, TrustDeductionDescription, TrustDeductionAccountNumber HAVING SUM(TrustAmount) <> 0 ORDER BY TrustCategoryID");
				// Call rsdata.OpenRecordset("Select * from tblTempPayProcess where TrustRecord = 1 Order by TrustCategoryID", DEFAULTDATABASE)
				while (!rsData.EndOfFile())
				{
					if (Conversion.Val(rsData.Get_Fields("TrustAmountTotal")) == 0)
					{
					}
					else
					{
						// this record is Medicare
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						modGlobalVariables.Statics.AccountStrut[intArrayID].Account = rsData.Get_Fields_String("TrustDeductionAccountNumber");
						modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
						modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
						modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields("TrustAmountTotal"), "0.00"));
						modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll(TA)";
						intArrayID += 1;
					}
					rsData.MoveNext();
				}
				// NOW GET THE CHECKING SUM RECORD
				dblValue = 0;
				rspayrollaccounts.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
				// rsPayrollAccounts.FindFirstRecord "Description", "'Cash / Checking'"
				strCashAcct = "";
				strSchoolCashAcct = "";
				if (!rspayrollaccounts.EndOfFile())
				{
					strCashAcct = FCConvert.ToString(rspayrollaccounts.Get_Fields("account"));
				}
				
				if (Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) > 0)
				{
					Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) - 1 + 1);
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					// this sets up the cash accounts for the funds if the user has BD
					modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
					// ftSchoolFundArray = CalcFundCash(AccountStrut(), True)
				}
				else
				{
					// create a cash entry into the account M CASH for these accounts
					modGlobalVariables.Statics.ftFundArray = modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
				}
				int lngTownCashIndex = 0;
				int lngSchoolCashIndex;
				int lngCurrCashIndex;
				int lngTempIndex;
				string strCurrAcct = "";
				string strTempAcct = "";
				int lngSwapIndex;
				string strAcctOR = "";
				// If gintUseGroupMultiFund = 0 Then
				// If gboolTownAccounts Then
				strAcctOR = "";
				if (modGlobalConstants.Statics.gboolBD)
				{
					strAcctOR = modBudgetaryAccounting.GetBankOverride(ref lngBank, true);
				}
				if (strAcctOR != string.Empty)
				{
					// had default cash account that may need to be split
					modBudgetaryAccounting.CalcCashFundsFromOverride(ref strAcctOR, ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " PY", "PY", false);
				}
				else
				{
					if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, lngTownCashIndex, true))
					{
					}
				}
				// End If
				// If gboolSchoolAccounts Then
				// strAcctOR = ""
				// If gboolBD Then
				// strAcctOR = GetBankOverride(lngBank, False)
				// End If
				// If strAcctOR <> vbNullString Then
				// has default cash account that may need to be split
				// Call CalcCashFundsFromOverride(strAcctOR, AccountStrut, ftSchoolFundArray, , Format(gdatCurrentPayDate, "MM/dd/yyyy") & " PY", "PY", True)
				// ElseIf CalcDueToFrom(AccountStrut, ftSchoolFundArray, , Format(gdatCurrentPayDate, "MM/dd/yyyy") & " PYDTDF", , , "PY", , True, lngSchoolCashIndex, True) Then
				// If gboolTownAccounts And gboolUsesDueToDueFroms Then
				// must fix the cash entries by grouping by account
				// If lngTownCashIndex > 0 Then
				// For lngCurrCashIndex = lngTownCashIndex To UBound(AccountStrut) - 1
				// If UCase(AccountStrut(lngCurrCashIndex).AcctType) = "C" And AccountStrut(lngCurrCashIndex).Amount <> 0 Then
				// strCurrAcct = AccountStrut(lngCurrCashIndex).Account
				// For lngTempIndex = lngCurrCashIndex + 1 To UBound(AccountStrut)
				// If UCase(AccountStrut(lngTempIndex).AcctType) = "C" Then
				// strTempAcct = AccountStrut(lngTempIndex).Account
				// If strTempAcct = strCurrAcct Then
				// AccountStrut(lngCurrCashIndex).Amount = AccountStrut(lngCurrCashIndex).Amount + AccountStrut(lngTempIndex).Amount
				// AccountStrut(lngTempIndex).Amount = 0
				// End If
				// End If
				// Next lngTempIndex
				// End If
				// Next lngCurrCashIndex
				// For lngCurrCashIndex = UBound(AccountStrut) To lngTownCashIndex + 1 Step -1
				// If UCase(AccountStrut(lngCurrCashIndex).AcctType) = "C" Then
				// If AccountStrut(lngCurrCashIndex).Amount = 0 Then
				// remove it
				// If UBound(AccountStrut) > lngCurrCashIndex Then
				// must copy them
				// For lngTempIndex = lngCurrCashIndex To UBound(AccountStrut) - 1
				// AccountStrut(lngTempIndex).Account = AccountStrut(lngTempIndex + 1).Account
				// AccountStrut(lngTempIndex).AcctType = AccountStrut(lngTempIndex + 1).AcctType
				// AccountStrut(lngTempIndex).Amount = AccountStrut(lngTempIndex + 1).Amount
				// AccountStrut(lngTempIndex).Description = AccountStrut(lngTempIndex + 1).Description
				// AccountStrut(lngTempIndex).UseDueToFrom = AccountStrut(lngTempIndex + 1).UseDueToFrom
				// Next lngTempIndex
				// End If
				// ReDim Preserve AccountStrut(lngCurrCashIndex - 1) As FundType
				// End If
				// End If
				// Next lngCurrCashIndex
				// End If
				// End If
				// End If
				// End If
				// End If
				for (lngTempIndex = 0; lngTempIndex <= Information.UBound(modGlobalVariables.Statics.AccountStrut, 1); lngTempIndex++)
				{
					if (fecherFoundation.Strings.UCase(modGlobalVariables.Statics.AccountStrut[lngTempIndex].RCB) == "E")
					{
						modGlobalVariables.Statics.AccountStrut[lngTempIndex].Amount *= -1;
					}
				}
				// lngTempIndex
				// ENTER THE JOURNALS INTO BUDGETARY
				lngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modGlobalVariables.Statics.AccountStrut, "PY", 0, Convert.ToDateTime(Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy")), Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " Payroll");
				rsData.Execute("Update tblTempPayProcess Set JournalNumber = " + FCConvert.ToString(lngJournalNumber), modGlobalVariables.DEFAULTDATABASE);
			}
			if (modGlobalConstants.Statics.gboolBD && boolEncumberContracts)
			{
				rsData.OpenRecordset("SELECT DistAccountNumber,SUM(DistGrossPay) as TotalPay, contractid FROM tblTempPayProcess WHERE distributionrecord = 1 and contractid > 0 GROUP BY DistAccountNumber,contractid HAVING SUM(DistGrossPay) <> 0 ORDER BY contractid,distaccountnumber", "twpy0000.vb1");
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngEncumbranceID = 0;
				int lngEncumbranceJournal;
				clsContract conCT = new clsContract();
				double dblTotal = 0;
				if (!rsData.EndOfFile())
				{
					while (!rsData.EndOfFile())
					{
						if (conCT.ContractID != FCConvert.ToInt32(rsData.Get_Fields("contractid")))
						{
							conCT.LoadContract(FCConvert.ToInt32(rsData.Get_Fields("contractid")));
							lngEncumbranceID = conCT.EncumbranceID;
							lngEncumbranceJournal = conCT.EncumbranceJournal;
						}
						dblTotal = Conversion.Val(rsData.Get_Fields("totalpay"));
						rsSave.OpenRecordset("SELECT * FROM ENCUMBRANCEDETAIL where ACCOUNT = '" + rsData.Get_Fields("DISTACCOUNTNUMBER") + "' AND RECORDNUMBER = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
							rsSave.Set_Fields("liquidated", rsSave.Get_Fields("liquidated") + dblTotal);
							rsSave.Update();
							rsSave.Execute("update encumbrances set liquidated = liquidated + " + FCConvert.ToString(dblTotal) + " where ID = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
						}
						rsData.MoveNext();
					}
				}
			}
			GetJournalNumber = true;
			lngPeriodNumber = FCConvert.ToInt32(Strings.Left(Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy"), 2));
			rsData.Execute("Update tblTempPayProcess Set AccountingPeriod = " + FCConvert.ToString(lngPeriodNumber), modGlobalVariables.DEFAULTDATABASE);
			return GetJournalNumber;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.Show
			// set focus back to the menu options of the MDIParent
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}

		private void mnuDDReport_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrMQYProcessing = "WEEKLY";
			rptDirectDeposit.InstancePtr.Init(true);
		}

		private void mnuDDReportNotGrouped_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gstrMQYProcessing = "WEEKLY";
			rptDirectDeposit.InstancePtr.Init(true, boolDontGroup: true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptPayrollAccountingChargesPreview.InstancePtr.Init(this.Modal, true, lngBank);
		}

		private void mnuPrintScreen_Click(object sender, System.EventArgs e)
		{
			// rptPrintProcessVerify.Show 1, MDIParent
			// rptVerifyAndAccept.Show 1, MDIParent
			rptVerifyAndAccept.InstancePtr.Init(this.Modal);
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuProcess_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				int lngWarrant = 0;
				clsDRWrapper rsBDInfo = new clsDRWrapper();
				object lngManualWarrant = 0;
				// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
				DialogResult answer = 0;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				// CALL THE BACKUP ROUTINE SO THAT THERE WILL BE A COPY OF THE DATABASE AT THIS POINT
				// MATTHEW 4/15/04
				if (MessageBox.Show("This function will commit this pay calculation. You will NOT be able to make any further changes to this run after accepting. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					if (modGlobalVariables.Statics.gboolBudgetary && dblGrossTotal != 0)
					{
						if (lngBank == 0)
						{
							lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank"))));
						}
						if (lngBank == 0)
						{
							// no bank yet
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							//App.DoEvents();
							lngBank = frmChooseBank.InstancePtr.Init();
							if (lngBank <= 0)
							{
								MessageBox.Show("No Bank Chosen" + "\r\n" + "Cannot Continue", "No Bank", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
						}
						// don't have a warrant number yet so create one
						if (modDavesSweetCode.LockWarrant() == false)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							Close();
							return;
						}
						rsData.OpenRecordset("SELECT TOP 1 * FROM WarrantMaster ORDER BY WarrantNumber DESC", "TWBD0000.vb1");
						if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
						{
							rsData.MoveLast();
							rsData.MoveFirst();
							lngWarrant = rsData.Get_Fields_Int32("WarrantNumber") + 1;
						}
						else
						{
							lngWarrant = 1;
						}
						rsBDInfo.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1");
						if (rsBDInfo.EndOfFile() != true && rsBDInfo.BeginningOfFile() != true)
						{
							if (FCConvert.ToBoolean(rsBDInfo.Get_Fields_Boolean("AllowManualWarrantNumbers")))
							{
								SelectAgain:
								;
								lngManualWarrant = lngWarrant;
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
								if (frmInput.InstancePtr.Init(ref lngManualWarrant, "Select Warrant Number", "Please select the warrant number you wish to use.", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber))
								{
									if (FCConvert.ToInt32(lngManualWarrant) > 9999)
									{
										MessageBox.Show("Warrant numbers must be 4 characters or less", "Invalid Warrant Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										goto SelectAgain;
									}
									rsData.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + FCConvert.ToString(lngManualWarrant), "Budgetary");
									if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
									{
										answer = MessageBox.Show("This warrant number has already been used.  Are you sure you wish to use this Warrant Number?", "Use Warrant Number?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
										if (answer == DialogResult.No)
										{
											goto SelectAgain;
										}
										else
										{
											modGlobalFunctions.AddCYAEntry_6("PY", "Selected Already Used Warrant Number", "Process Verify");
											lngWarrant = FCConvert.ToInt32(lngManualWarrant);
										}
									}
									else
									{
										lngWarrant = FCConvert.ToInt32(lngManualWarrant);
									}
								}
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								if (FCConvert.ToInt32(lngManualWarrant) == 0)
								{
									// CANCEL BUTTON WAS SELECTED.
									FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
									modDavesSweetCode.UnlockWarrant();
									return;
								}
							}
						}
						rsData.OpenRecordset("SELECT * FROM WarrantMaster", "Budgetary");
						rsData.AddNew();
						rsData.Set_Fields("WarrantNumber", lngWarrant);
						rsData.Set_Fields("WarrantDate", DateTime.Today);
						rsData.Set_Fields("WarrantTime", fecherFoundation.DateAndTime.TimeOfDay);
						rsData.Set_Fields("WarrantOperator", modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
						rsData.Update(true);
						modDavesSweetCode.UnlockWarrant();
					}
					else
					{
						lngWarrant = 1;
					}
					rsData.Execute("Update tblTempPayProcess Set WarrantNumber = " + FCConvert.ToString(lngWarrant), modGlobalVariables.DEFAULTDATABASE);
					// CREATE JOURNAL NUMBER
					if (modGlobalConstants.Statics.gboolBD && dblGrossTotal != 0)
					{
						if (!GetJournalNumber())
						{
							return;
						}
					}
					// update the payroll steps table with the warrant and journal number
					// for Dave and some of his reports. Matthew 06/17/2003
					rsData.Execute("Update tblPayrollSteps Set WarrantNumber = " + Strings.Format(lngWarrant, "0000") + ",JournalNumber = " + Strings.Format(lngJournalNumber, "0000") + " Where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
					// UPDATE THE PAY TOTALS TABLE
					rsMaster.OpenRecordset("Select  EmployeeNumber from tblTempPayProcess group by employeenumber Order By EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
					while (!rsMaster.EndOfFile())
					{
						// UPDATE THE VACATION/SICK TABLE
						clsDRWrapper rsCheckDetail = new clsDRWrapper();
						rsData.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "'", modGlobalVariables.DEFAULTDATABASE);
						while (!rsData.EndOfFile())
						{
							rsData.Edit();
							rsData.Set_Fields("AccruedFiscal", Strings.Format(rsData.Get_Fields_Double("AccruedFiscal") + rsData.Get_Fields_Double("AccruedCurrent"), "0.0000"));
							rsData.Set_Fields("AccruedCalendar", Strings.Format(rsData.Get_Fields_Double("AccruedCalendar") + rsData.Get_Fields_Double("AccruedCurrent"), "0.0000"));
							rsData.Set_Fields("UsedFiscal", Strings.Format(rsData.Get_Fields_Double("UsedFiscal") + rsData.Get_Fields_Double("UsedCurrent"), "0.0000"));
							rsData.Set_Fields("UsedCalendar", Strings.Format(rsData.Get_Fields_Double("UsedCalendar") + rsData.Get_Fields_Double("UsedCurrent"), "0.0000"));
							rsData.Set_Fields("UsedBalance", Strings.Format(rsData.Get_Fields_Double("UsedBalance") + rsData.Get_Fields_Double("AccruedCurrent") - rsData.Get_Fields_Double("UsedCurrent"), "0.0000"));
							// Call rsCheckDetail.OpenRecordset("Select * from tblCheckDetail where EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "' AND VSRecord = 1 and VSTypeID = " & rsData.Fields("CodeID"), DEFAULTDATABASE)
							// If rsCheckDetail.EndOfFile Then
							// Else
							// rsCheckDetail.Edit
							// rsCheckDetail.Fields("VSBalance") = rsData.Fields("UsedBalance")
							// rsCheckDetail.Update
							// End If
							// Call rsCheckDetail.Execute("update tblCheckDetail set vsbalance = isnull(usedbalance,0) where EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "' AND VSRecord = 1 and VSTypeID = " & rsData.Fields("CodeID"), DEFAULTDATABASE)
							rsData.Update();
							rsData.MoveNext();
						}
						// UPDATE THE DEDUCTION TABLE
						// Call rsData.OpenRecordset("Select * from tblEmployeeDeductions where EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "'", DEFAULTDATABASE)
						// Call rsData.Execute("update tblEmployeeDeductions set LTDTotal = (LTDTotal + CurrentTotal),currenttotal = 0 where employeenumber = '" & rsMaster.Fields("employeenumber") & "'", "twpy0000.vb1")
						// 
						// Call rsData.Execute("update tblEmployersmatch set ltd = (ltd + currenttotal),currenttotal = 0 where employeenumber = '" & rsMaster.Fields("employeenumber") & "'", "twpy0000.vb1")
						rsMaster.MoveNext();
					}
					rsMaster.OpenRecordset("select sum(dedAmount) as totded, sum(matchamount) as totmatch, employeenumber,empdeductionid,deductionrecord from tbltemppayprocess where deductionrecord = 1 or matchrecord = 1 group by employeenumber,deductionrecord,empdeductionid ", "twpy0000.vb1");
					while (!rsMaster.EndOfFile())
					{
						if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("deductionrecord")))
						{
							rsData.Execute("update tblEmployeeDeductions set LTDTotal = (LTDTotal + " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("totded"))) + "),currenttotal = 0 where employeenumber = '" + rsMaster.Get_Fields("employeenumber") + "' and ID = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("empdeductionid"))), "twpy0000.vb1");
						}
						else
						{
							rsData.Execute("update tblEmployersmatch set ltd = (ltd + " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("totmatch"))) + "),currenttotal = 0 where employeenumber = '" + rsMaster.Get_Fields("employeenumber") + "' and ID = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("empdeductionid"))), "twpy0000.vb1");
						}
						rsMaster.MoveNext();
					}
					if (modGlobalConstants.Statics.gboolBD && dblGrossTotal != 0)
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Warrant number is " + Strings.Format(lngWarrant, "0000") + "                         " + "\r" + "\r" + "Journal number is " + Strings.Format(lngJournalNumber, "0000") + "\r" + "\r", "Warrant / Journal Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					}
					//DAO.Field FieldName = new DAO.Field();
					DateTime datPayDate;
					rsMaster.OpenRecordset("Select * from tblPayrollProcessSetup", modGlobalVariables.DEFAULTDATABASE);
					datPayDate = (DateTime)rsMaster.Get_Fields_DateTime("PayDate");
					// now move all records to the tblCheckDetail table and commit the pay run
					rsMaster.OpenRecordset("Select * from tblTempPayProcess", modGlobalVariables.DEFAULTDATABASE);
					rsData.OpenRecordset("Select * from tblCheckDetail where id = -1", modGlobalVariables.DEFAULTDATABASE);
					while (!rsMaster.EndOfFile())
					{
						rsData.AddNew();
						// For Each FieldName In rsMaster.AllFields
						// If FieldName.Name = "ID" Then
						// Else
						// If rsMaster.Fields(FieldName.Name) = vbNullString Then
						// rsData.Fields(FieldName.Name) = Null
						// Else
						// rsData.Fields(FieldName.Name) = rsMaster.Fields(FieldName.Name)
						// End If
						// End If
						// Next
						for (intCounter = 0; intCounter <= (rsMaster.FieldsCount - 1); intCounter++)
						{
							if (rsMaster.Get_FieldsIndexName(intCounter) == "ID")
							{
								// dop nothing
							}
							else
							{
								rsData.Set_Fields(rsMaster.Get_FieldsIndexName(intCounter), rsMaster.Get_Fields(rsMaster.Get_FieldsIndexName(intCounter)));
							}
						}
						rsData.Set_Fields("PayDate", datPayDate);
						rsData.Set_Fields("PayRunID", modGlobalVariables.Statics.gintCurrentPayRun);
						rsData.Update();
						rsMaster.MoveNext();
					}
					rsMaster.Execute("Delete from tblTempPayProcess", modGlobalVariables.DEFAULTDATABASE);
					modGlobalRoutines.UpdatePayrollStepTable("VerifyAccept");
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("The accept process was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// Screen.MousePointer = vbHourglass
					// frmCheckDetailAdjustment.Show , MDIParent
					// Screen.MousePointer = vbDefault
				}
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

        private enum CheckSummaryRows
        {
            RegularCheckCount = 1,
            RegularCheckAmount = 2,
            DirectDepositCount = 3,
            DirectDepositAmount = 4,
            RegularAndDDCount = 5,
            RegularAndDDRegularAmount = 6,
            RegularAndDDDDAmount = 7,
            TACheckCount = 8,
            TACheckAmount = 9
        }

        private enum CheckSummaryColumns
        {
            Description = 0,
            Amount = 1
        }

        private enum TaxableGrossColumns
        {
            Description = 3,
            Amount = 4
        }

        private enum TaxableGrossRows
        {
            Federal = 1,
            Fica = 2,
            Medicare = 3,
            State = 4
        }
	}
}
