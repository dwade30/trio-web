//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Part4Plain.
	/// </summary>
	public partial class srptC1Part4Plan : FCSectionReport
	{
		public srptC1Part4Plan()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1Part4Plan InstancePtr
		{
			get
			{
				return (srptC1Part4Plan)Sys.GetInstance(typeof(srptC1Part4Plan));
			}
		}

		protected srptC1Part4Plan _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsC1?.Dispose();
                clsC1 = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1Part4Plain	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		clsDRWrapper clsC1 = new clsDRWrapper();
		bool boolLastPage;
		double dblTotalWH;
		double dblLimit;
		int intpage;
		bool boolTestPrint;
		int intRecsPerPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsC1.EndOfFile();
			if (!eArgs.EOF)
			{
				// Barcode1.Caption = Right(EWRWageTotals.lngYear, 2) & "0620" & Format(intpage, "0")
				Barcode1.Text = "210620" + Strings.Format(intpage, "0");
				if (intpage > 9)
					Barcode1.Left = 8040 / 1440F;
				intpage += 1;
			}
			else if (intpage == 1 && boolTestPrint)
			{
				eArgs.EOF = false;
				intpage += 1;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			int intQuarterCovered;
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear = "";
			int x;
			boolTestPrint = modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest;
			intpage = 1;
			boolLastPage = false;
			intRecsPerPage = 19;
			dblTotalWH = 0;
			LoadInfo();
			if (!boolTestPrint)
			{
				txtName.Text = EWRRecord.EmployerName;
				txtWithholdingAccount.Text = Strings.Mid(EWRRecord.MRSWithholdingID, 1, 2) + " " + Strings.Mid(EWRRecord.MRSWithholdingID, 3);
				txtOriginalTotal.Visible = false;
				txtAmendedTotal.Visible = false;
				for (x = 1; x <= intRecsPerPage; x++)
				{
					Detail.Controls["txtAmended" + x].Visible = false;
				}

				intQuarterCovered = modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter;
				strYear = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
				Label9.Text = "SCHEDULE 2 (FORM 941 ME) " + strYear;
				dblLimit = modCoreysSweeterCode.Statics.EWRWageTotals.dblLimit;
				txtDayStart.Text = "01";
				switch (intQuarterCovered)
				{
					case 1:
						{
							// txtDateStart.Text = Format("01/01/" & strYear, "mm dd yyyy")
							txtDateStart.Text = "01 01";
							// txtDateEnd.Text = Format("03/31/" & strYear, "mm dd yyyy")
							txtDateEnd.Text = "03 31";
							txtMonthStart.Text = "01";
							txtMonthEnd.Text = "03";
							txtDayEnd.Text = "31";
							break;
						}
					case 2:
						{
							// txtDateStart.Text = Format("04/01/" & strYear, "mm dd yyyy")
							txtDateStart.Text = "04 01";
							// txtDateEnd.Text = Format("06/30/" & strYear, "mm dd yyyy")
							txtDateEnd.Text = "06 30";
							txtMonthStart.Text = "04";
							txtMonthEnd.Text = "06";
							txtDayEnd.Text = "30";
							break;
						}
					case 3:
						{
							// txtDateStart.Text = Format("07/01/" & strYear, "mm dd yyyy")
							txtDateStart.Text = "07 01";
							// txtDateEnd.Text = Format("09/30/" & strYear, "mm dd yyyy")
							txtDateEnd.Text = "09 30";
							txtMonthStart.Text = "07";
							txtMonthEnd.Text = "09";
							txtDayEnd.Text = "30";
							break;
						}
					case 4:
						{
							// txtDateStart.Text = Format("10/01/" & strYear, "mm dd yyyy")
							txtDateStart.Text = "10 01";
							// txtDateEnd.Text = Format("12/31/" & strYear, "mm dd yyyy")
							txtDateEnd.Text = "12 31";
							txtMonthStart.Text = "10";
							txtMonthEnd.Text = "12";
							txtDayEnd.Text = "31";
							break;
						}
				}
				//end switch
				txtYearStart.Text = strYear;
				txtYearEnd.Text = strYear;
			}
			else
			{
				strYear = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
			}
			strSQL = FCConvert.ToString(this.UserData);
			clsC1.OpenRecordset(strSQL, "twpy0000.vb1");
			Label9.Text = "SCHEDULE 2 (FORM 941ME) " + strYear;
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "Payroll");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

		private void ClearBoxes()
		{
			int x;
			for (x = 1; x <= intRecsPerPage; x++)
			{
				(this.Detail.Controls["txtName" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtSSN" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtWH" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				// Me.Detail.Controls("txtwage" & x).Text = ""
			}
			// x
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblPageWage;
			double dblPageWH = 0;
			double dblWage;
			double dblWH = 0;
			double dblGross;
			double dblExcess;
			int intCurrentLine;
			string strTemp = "";
			if (!clsC1.EndOfFile())
			{
				if (!boolTestPrint)
				{
					if (intpage > 9)
						Barcode1.Left = 8040 / 1440F;
					ClearBoxes();
					// up to intRecsPerPage on this page
					dblPageWH = 0;
					dblExcess = 0;
					intCurrentLine = 1;
					for (intCurrentLine = 1; intCurrentLine <= intRecsPerPage; intCurrentLine++)
					{
						if (clsC1.EndOfFile())
						{
							boolLastPage = true;
							break;
						}
						strTemp = fecherFoundation.Strings.Trim(clsC1.Get_Fields("lastname") + " " + clsC1.Get_Fields("firstname") + " " + fecherFoundation.Strings.Trim(Strings.Left(clsC1.Get_Fields("Middlename") + " ", 1)) + " " + clsC1.Get_Fields("desig"));
						(Detail.Controls["txtName" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
						strTemp = Strings.Format(clsC1.Get_Fields("ssn"), "000000000");
						strTemp = Strings.Left(strTemp, 3) + " " + Strings.Mid(strTemp, 4, 2) + " " + Strings.Right(strTemp, 4);
						// strTemp = Mid(strTemp, 1, 3) & "-" & Mid(strTemp, 4, 2) & "-" & Mid(strTemp, 6)
						(Detail.Controls["txtSSN" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
						dblWage = Conversion.Val(Strings.Format(clsC1.Get_Fields("qtdGrossPay"), "0.00"));
						dblWH = Conversion.Val(Strings.Format(clsC1.Get_Fields("qtdstatewh"), "0.00"));
						dblPageWH += dblWH;
						(Detail.Controls["txtWH" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblWH, "0.00");
						// Detail.Controls("txtwage" & intCurrentLine).Text = Format(dblWage, "0.00")
						clsC1.MoveNext();
					}
					// intCurrentLine
					// make this check in case the last record is the 23rd on this page
					if (clsC1.EndOfFile())
						boolLastPage = true;
					// keep track of grandtotals
					dblTotalWH += dblPageWH;
	                if (boolLastPage)
					{
						// show grand totals
                        txtOriginalTotal.Visible = true;
						txtOriginalTotal.Text = Strings.Format(dblTotalWH, "0.00");
						// fill totals for next subreport
						modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalStateWithheld = dblTotalWH;
					}
				}
				else
				{
					boolLastPage = true;
					clsC1.MoveLast();
					clsC1.MoveNext();
				}
			}
		}

		
	}
}
