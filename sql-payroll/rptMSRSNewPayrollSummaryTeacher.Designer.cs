﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSNewPayrollSummary.
	/// </summary>
	partial class rptMSRSNewPayrollSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMSRSNewPayrollSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtEmployerCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateCompleted = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPaidDates1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPreparer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableCompensation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFedFundPerc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGrantFundedCompensation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerContributions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalEmployeeContributions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerAndEmployeeCont = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBasicPremiums = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSupplemental = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDependent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalPremiums = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalDebitAndCredit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDebOrCredNum1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDebOrCredNum2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCompensationAdjustment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtContributionAdjustment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPurchaseAgreements = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustments = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalRemittance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDateSigned = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjustment8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAdjustment8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateCompleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDates1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreparer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableCompensation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedFundPerc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrantFundedCompensation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerContributions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployeeContributions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAndEmployeeCont)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasicPremiums)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSupplemental)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPremiums)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDebitAndCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebOrCredNum1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebOrCredNum2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompensationAdjustment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContributionAdjustment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseAgreements)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRemittance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateSigned)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.Label102,
				this.Label94,
				this.Label98,
				this.Label95,
				this.Label66,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label8,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Label22,
				this.Label23,
				this.Line15,
				this.Label24,
				this.Label29,
				this.Label30,
				this.Line17,
				this.txtEmployerCode,
				this.txtEmployerName,
				this.txtDateCompleted,
				this.txtTelephone,
				this.txtPaidDates1,
				this.txtPreparer,
				this.txtEarnableCompensation,
				this.Label53,
				this.txtFedFundPerc,
				this.Label54,
				this.Label55,
				this.Label56,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Label57,
				this.Line36,
				this.Label58,
				this.txtGrantFundedCompensation,
				this.Line37,
				this.Label59,
				this.txtEmployerContributions,
				this.Label61,
				this.Line38,
				this.Label62,
				this.txtTotalEmployeeContributions,
				this.Line39,
				this.Line40,
				this.Label63,
				this.Line41,
				this.Label64,
				this.txtEmployerAndEmployeeCont,
				this.Line42,
				this.Line43,
				this.Line44,
				this.Line45,
				this.Label67,
				this.Label68,
				this.Line46,
				this.Label69,
				this.txtBasicPremiums,
				this.Label71,
				this.Line47,
				this.Label72,
				this.txtSupplemental,
				this.Label74,
				this.Line48,
				this.Label75,
				this.txtDependent,
				this.Label76,
				this.Line49,
				this.Label77,
				this.txtTotalPremiums,
				this.Label78,
				this.Line50,
				this.Line51,
				this.Label79,
				this.Label80,
				this.Line52,
				this.Label81,
				this.txtTotalDebitAndCredit,
				this.Label83,
				this.Line53,
				this.Label84,
				this.txtDebOrCredNum1,
				this.Line54,
				this.Label86,
				this.txtDebOrCredNum2,
				this.Label88,
				this.Line55,
				this.Label89,
				this.txtCompensationAdjustment,
				this.Line56,
				this.Label91,
				this.Line57,
				this.Label92,
				this.txtContributionAdjustment,
				this.Line58,
				this.Line59,
				this.Line60,
				this.Label96,
				this.txtPurchaseAgreements,
				this.Label99,
				this.Line63,
				this.Label100,
				this.txtAdjustments,
				this.Line65,
				this.Line66,
				this.Label103,
				this.Label104,
				this.Line67,
				this.Label105,
				this.txtTotalRemittance,
				this.Line68,
				this.Line69,
				this.Label106,
				this.Label107,
				this.Label108,
				this.Label109,
				this.Line70,
				this.Line71,
				this.Line72,
				this.txtDateSigned,
				this.Label110,
				this.Label111,
				this.Label112,
				this.Label113,
				this.Label114,
				this.Label115,
				this.Label116,
				this.Label117,
				this.Label118
			});
			this.Detail.Height = 10.21875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAdjustment1,
				this.lblAdjustment2,
				this.lblAdjustment3,
				this.lblAdjustment4,
				this.lblAdjustment5,
				this.lblAdjustment6,
				this.lblAdjustment7,
				this.txtAdjustment1,
				this.txtAdjustment2,
				this.txtAdjustment3,
				this.txtAdjustment4,
				this.txtAdjustment5,
				this.txtAdjustment6,
				this.txtAdjustment7,
				this.lblAdjustment8,
				this.txtAdjustment8
			});
			this.ReportFooter.Height = 2.28125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.ReportFooter.Visible = false;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "";
			this.Label9.Text = "2.   EmployER Name";
			this.Label9.Top = 1.9375F;
			this.Label9.Width = 1.25F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 0.1875F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-size: 8.5pt; font-style: italic";
			this.Label102.Text = "(Attach a detailed explanation)";
			this.Label102.Top = 8.25F;
			this.Label102.Width = 2.75F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1875F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 0.1875F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-size: 8.5pt; font-style: italic";
			this.Label94.Text = "(Use the same % rate as indicated on line #6 above)";
			this.Label94.Top = 7.375F;
			this.Label94.Width = 3.8125F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1875F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 0.1875F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-size: 8.5pt; font-style: italic";
			this.Label98.Text = "(Additional members may be added as an attachment - indicate \'see attachment\')";
			this.Label98.Top = 7.8125F;
			this.Label98.Width = 5.9375F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1875F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 0.25F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "text-decoration: underline";
			this.Label95.Text = "Total of Purchase Agreements (Authorized by MSRS)";
			this.Label95.Top = 7.625F;
			this.Label95.Width = 4.0625F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1875F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.1875F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "";
			this.Label66.Text = "ADD ITEMS 6 & 7)";
			this.Label66.Top = 4.25F;
			this.Label66.Width = 2.625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
			this.Label1.Left = 3.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: right";
			this.Label1.Text = "MONTHLY PAYROLL SUMMARY REPORT";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.25F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4.375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 12pt; font-weight: bold; text-align: right; vertical-align: middle";
			this.Label2.Text = "FOR";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 1.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label3.Text = "Reports and contributions must be submitted no later than the 15th day";
			this.Label3.Top = 1.125F;
			this.Label3.Width = 4.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 9pt; font-weight: bold";
			this.Label4.Text = "46 State House Station";
			this.Label4.Top = 0.1666667F;
			this.Label4.Width = 2.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 9pt; font-weight: bold";
			this.Label5.Text = "Augusta, Maine 04333-0046";
			this.Label5.Top = 0.3333333F;
			this.Label5.Width = 2.375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 9pt; font-weight: bold";
			this.Label6.Text = "207-512-3100";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 2.5625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "1.   EmployER Code";
			this.Label8.Top = 1.6875F;
			this.Label8.Width = 1.25F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "3.   Pay Dates";
			this.Label10.Top = 2.1875F;
			this.Label10.Width = 1.375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "text-align: right";
			this.Label11.Text = "Date Form Completed";
			this.Label11.Top = 1.6875F;
			this.Label11.Width = 1.3125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: right";
			this.Label12.Text = "Telephone Number";
			this.Label12.Top = 1.9375F;
			this.Label12.Width = 1.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.5625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: right";
			this.Label13.Text = "Person Preparing Report";
			this.Label13.Top = 2.1875F;
			this.Label13.Width = 1.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.3125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.875F;
			this.Line1.Width = 2.5625F;
			this.Line1.X1 = 1.3125F;
			this.Line1.X2 = 3.875F;
			this.Line1.Y1 = 1.875F;
			this.Line1.Y2 = 1.875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.3125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2.125F;
			this.Line2.Width = 2.5625F;
			this.Line2.X1 = 1.3125F;
			this.Line2.X2 = 3.875F;
			this.Line2.Y1 = 2.125F;
			this.Line2.Y2 = 2.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.3125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.375F;
			this.Line3.Width = 2.5625F;
			this.Line3.X1 = 1.3125F;
			this.Line3.X2 = 3.875F;
			this.Line3.Y1 = 2.375F;
			this.Line3.Y2 = 2.375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.0625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.875F;
			this.Line4.Width = 1.8125F;
			this.Line4.X1 = 6.0625F;
			this.Line4.X2 = 7.875F;
			this.Line4.Y1 = 1.875F;
			this.Line4.Y2 = 1.875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 6.0625F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.125F;
			this.Line5.Width = 1.8125F;
			this.Line5.X1 = 6.0625F;
			this.Line5.X2 = 7.875F;
			this.Line5.Y1 = 2.125F;
			this.Line5.Y2 = 2.125F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 6.0625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 2.375F;
			this.Line6.Width = 1.8125F;
			this.Line6.X1 = 6.0625F;
			this.Line6.X2 = 7.875F;
			this.Line6.Y1 = 2.375F;
			this.Line6.Y2 = 2.375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.5F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label22.Text = "RETIREMENT FINANCIAL DATA";
			this.Label22.Top = 2.5625F;
			this.Label22.Width = 3.125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "";
			this.Label23.Text = "4.  Earnable Compensation - Total For All Covered EmployEES";
			this.Label23.Top = 2.875F;
			this.Label23.Width = 3.4375F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 6.0625F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 3.0625F;
			this.Line15.Width = 1.8125F;
			this.Line15.X1 = 6.0625F;
			this.Line15.X2 = 7.875F;
			this.Line15.Y1 = 3.0625F;
			this.Line15.Y2 = 3.0625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 5.875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "text-align: right";
			this.Label24.Text = "$";
			this.Label24.Top = 2.875F;
			this.Label24.Width = 0.1875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "";
			this.Label29.Text = "6.  EmployER Contributions on Grant Funded Compensation";
			this.Label29.Top = 3.375F;
			this.Label29.Width = 3.5F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 1.1875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "";
			this.Label30.Text = "% X Grant Funded Compensation (item 5)";
			this.Label30.Top = 3.5625F;
			this.Label30.Width = 2.625F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0.1875F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 3.6875F;
			this.Line17.Width = 1F;
			this.Line17.X1 = 0.1875F;
			this.Line17.X2 = 1.1875F;
			this.Line17.Y1 = 3.6875F;
			this.Line17.Y2 = 3.6875F;
			// 
			// txtEmployerCode
			// 
			this.txtEmployerCode.Height = 0.1875F;
			this.txtEmployerCode.HyperLink = null;
			this.txtEmployerCode.Left = 1.3125F;
			this.txtEmployerCode.Name = "txtEmployerCode";
			this.txtEmployerCode.Style = "";
			this.txtEmployerCode.Text = null;
			this.txtEmployerCode.Top = 1.6875F;
			this.txtEmployerCode.Width = 2.5625F;
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.Height = 0.1875F;
			this.txtEmployerName.HyperLink = null;
			this.txtEmployerName.Left = 1.3125F;
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Style = "";
			this.txtEmployerName.Text = null;
			this.txtEmployerName.Top = 1.9375F;
			this.txtEmployerName.Width = 2.5625F;
			// 
			// txtDateCompleted
			// 
			this.txtDateCompleted.Height = 0.1875F;
			this.txtDateCompleted.HyperLink = null;
			this.txtDateCompleted.Left = 6.0625F;
			this.txtDateCompleted.Name = "txtDateCompleted";
			this.txtDateCompleted.Style = "";
			this.txtDateCompleted.Text = null;
			this.txtDateCompleted.Top = 1.6875F;
			this.txtDateCompleted.Width = 1.8125F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.1875F;
			this.txtTelephone.HyperLink = null;
			this.txtTelephone.Left = 6.0625F;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "";
			this.txtTelephone.Text = null;
			this.txtTelephone.Top = 1.9375F;
			this.txtTelephone.Width = 1.8125F;
			// 
			// txtPaidDates1
			// 
			this.txtPaidDates1.Height = 0.1875F;
			this.txtPaidDates1.HyperLink = null;
			this.txtPaidDates1.Left = 1.3125F;
			this.txtPaidDates1.Name = "txtPaidDates1";
			this.txtPaidDates1.Style = "";
			this.txtPaidDates1.Text = null;
			this.txtPaidDates1.Top = 2.1875F;
			this.txtPaidDates1.Width = 3.125F;
			// 
			// txtPreparer
			// 
			this.txtPreparer.Height = 0.1875F;
			this.txtPreparer.HyperLink = null;
			this.txtPreparer.Left = 6.0625F;
			this.txtPreparer.Name = "txtPreparer";
			this.txtPreparer.Style = "";
			this.txtPreparer.Text = null;
			this.txtPreparer.Top = 2.1875F;
			this.txtPreparer.Width = 1.8125F;
			// 
			// txtEarnableCompensation
			// 
			this.txtEarnableCompensation.Height = 0.1875F;
			this.txtEarnableCompensation.HyperLink = null;
			this.txtEarnableCompensation.Left = 6.0625F;
			this.txtEarnableCompensation.Name = "txtEarnableCompensation";
			this.txtEarnableCompensation.Style = "text-align: right";
			this.txtEarnableCompensation.Text = null;
			this.txtEarnableCompensation.Top = 2.875F;
			this.txtEarnableCompensation.Width = 1.8125F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 9pt; font-weight: bold";
			this.Label53.Text = "MAINE PUBLIC EMPLOYEE RETIREMENT SYSTEM";
			this.Label53.Top = 0F;
			this.Label53.Width = 3.375F;
			// 
			// txtFedFundPerc
			// 
			this.txtFedFundPerc.Height = 0.1875F;
			this.txtFedFundPerc.HyperLink = null;
			this.txtFedFundPerc.Left = 0.8125F;
			this.txtFedFundPerc.Name = "txtFedFundPerc";
			this.txtFedFundPerc.Style = "";
			this.txtFedFundPerc.Text = null;
			this.txtFedFundPerc.Top = 3.5625F;
			this.txtFedFundPerc.Width = 0.375F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1666667F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-size: 9pt; font-weight: bold";
			this.Label54.Text = "1-800-451-9800 TTY:207-512-3102";
			this.Label54.Top = 0.6666667F;
			this.Label54.Width = 2.5625F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.25F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 5.875F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-size: 14.5pt; font-weight: bold; text-align: right; text-decoration: underli" + "ne";
			this.Label55.Text = "TEACHER PLAN";
			this.Label55.Top = 0.1875F;
			this.Label55.Width = 1.625F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 1.625F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label56.Text = "following the end of the month to be reported.";
			this.Label56.Top = 1.3125F;
			this.Label56.Width = 4.75F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 0.625F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 1.861111F;
			this.Line31.Width = 0.1875F;
			this.Line31.X1 = 0.625F;
			this.Line31.X2 = 0.8125F;
			this.Line31.Y1 = 1.861111F;
			this.Line31.Y2 = 1.861111F;
			// 
			// Line32
			// 
			this.Line32.Height = 0F;
			this.Line32.Left = 0.625F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 2.111111F;
			this.Line32.Width = 0.1875F;
			this.Line32.X1 = 0.625F;
			this.Line32.X2 = 0.8125F;
			this.Line32.Y1 = 2.111111F;
			this.Line32.Y2 = 2.111111F;
			// 
			// Line33
			// 
			this.Line33.Height = 0F;
			this.Line33.Left = 0F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 2.5F;
			this.Line33.Width = 7.875F;
			this.Line33.X1 = 0F;
			this.Line33.X2 = 7.875F;
			this.Line33.Y1 = 2.5F;
			this.Line33.Y2 = 2.5F;
			// 
			// Line34
			// 
			this.Line34.Height = 0F;
			this.Line34.Left = 0F;
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 2.534722F;
			this.Line34.Width = 7.875F;
			this.Line34.X1 = 0F;
			this.Line34.X2 = 7.875F;
			this.Line34.Y1 = 2.534722F;
			this.Line34.Y2 = 2.534722F;
			// 
			// Line35
			// 
			this.Line35.Height = 0F;
			this.Line35.Left = 3.208333F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 3.048611F;
			this.Line35.Width = 0.2291667F;
			this.Line35.X1 = 3.208333F;
			this.Line35.X2 = 3.4375F;
			this.Line35.Y1 = 3.048611F;
			this.Line35.Y2 = 3.048611F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "";
			this.Label57.Text = "5.  Grant Funded Compensation Only";
			this.Label57.Top = 3.125F;
			this.Label57.Width = 3.4375F;
			// 
			// Line36
			// 
			this.Line36.Height = 0F;
			this.Line36.Left = 6.0625F;
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 3.3125F;
			this.Line36.Width = 1.8125F;
			this.Line36.X1 = 6.0625F;
			this.Line36.X2 = 7.875F;
			this.Line36.Y1 = 3.3125F;
			this.Line36.Y2 = 3.3125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.875F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "text-align: right";
			this.Label58.Text = "$";
			this.Label58.Top = 3.125F;
			this.Label58.Width = 0.1875F;
			// 
			// txtGrantFundedCompensation
			// 
			this.txtGrantFundedCompensation.Height = 0.1875F;
			this.txtGrantFundedCompensation.HyperLink = null;
			this.txtGrantFundedCompensation.Left = 6.0625F;
			this.txtGrantFundedCompensation.Name = "txtGrantFundedCompensation";
			this.txtGrantFundedCompensation.Style = "text-align: right";
			this.txtGrantFundedCompensation.Text = null;
			this.txtGrantFundedCompensation.Top = 3.125F;
			this.txtGrantFundedCompensation.Width = 1.8125F;
			// 
			// Line37
			// 
			this.Line37.Height = 0F;
			this.Line37.Left = 6.0625F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 3.5625F;
			this.Line37.Width = 1.8125F;
			this.Line37.X1 = 6.0625F;
			this.Line37.X2 = 7.875F;
			this.Line37.Y1 = 3.5625F;
			this.Line37.Y2 = 3.5625F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 5.875F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "text-align: right";
			this.Label59.Text = "$";
			this.Label59.Top = 3.375F;
			this.Label59.Width = 0.1875F;
			// 
			// txtEmployerContributions
			// 
			this.txtEmployerContributions.Height = 0.1875F;
			this.txtEmployerContributions.HyperLink = null;
			this.txtEmployerContributions.Left = 6.0625F;
			this.txtEmployerContributions.Name = "txtEmployerContributions";
			this.txtEmployerContributions.Style = "text-align: right";
			this.txtEmployerContributions.Text = null;
			this.txtEmployerContributions.Top = 3.375F;
			this.txtEmployerContributions.Width = 1.8125F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 0F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "";
			this.Label61.Text = "7.  Total EmployEE Retirement Contributions (Do not include Purchase Agreements)";
			this.Label61.Top = 3.8125F;
			this.Label61.Width = 5.0625F;
			// 
			// Line38
			// 
			this.Line38.Height = 0F;
			this.Line38.Left = 6.0625F;
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 4F;
			this.Line38.Width = 1.8125F;
			this.Line38.X1 = 6.0625F;
			this.Line38.X2 = 7.875F;
			this.Line38.Y1 = 4F;
			this.Line38.Y2 = 4F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 5.875F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "text-align: right";
			this.Label62.Text = "$";
			this.Label62.Top = 3.8125F;
			this.Label62.Width = 0.1875F;
			// 
			// txtTotalEmployeeContributions
			// 
			this.txtTotalEmployeeContributions.Height = 0.1875F;
			this.txtTotalEmployeeContributions.HyperLink = null;
			this.txtTotalEmployeeContributions.Left = 6.0625F;
			this.txtTotalEmployeeContributions.Name = "txtTotalEmployeeContributions";
			this.txtTotalEmployeeContributions.Style = "text-align: right";
			this.txtTotalEmployeeContributions.Text = null;
			this.txtTotalEmployeeContributions.Top = 3.8125F;
			this.txtTotalEmployeeContributions.Width = 1.8125F;
			// 
			// Line39
			// 
			this.Line39.Height = 0F;
			this.Line39.Left = 0.6111111F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 3.548611F;
			this.Line39.Width = 0.1388889F;
			this.Line39.X1 = 0.6111111F;
			this.Line39.X2 = 0.75F;
			this.Line39.Y1 = 3.548611F;
			this.Line39.Y2 = 3.548611F;
			// 
			// Line40
			// 
			this.Line40.Height = 0F;
			this.Line40.Left = 0.9375F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 3.986111F;
			this.Line40.Width = 0.1388888F;
			this.Line40.X1 = 0.9375F;
			this.Line40.X2 = 1.076389F;
			this.Line40.Y1 = 3.986111F;
			this.Line40.Y2 = 3.986111F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1875F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 0F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "";
			this.Label63.Text = "8.  Total EmployER and EmployEE Contributions";
			this.Label63.Top = 4.0625F;
			this.Label63.Width = 3.0625F;
			// 
			// Line41
			// 
			this.Line41.Height = 0F;
			this.Line41.Left = 6.0625F;
			this.Line41.LineWeight = 3F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 4.4375F;
			this.Line41.Width = 1.8125F;
			this.Line41.X1 = 6.0625F;
			this.Line41.X2 = 7.875F;
			this.Line41.Y1 = 4.4375F;
			this.Line41.Y2 = 4.4375F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1875F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 5.875F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "text-align: right";
			this.Label64.Text = "$";
			this.Label64.Top = 4.25F;
			this.Label64.Width = 0.1875F;
			// 
			// txtEmployerAndEmployeeCont
			// 
			this.txtEmployerAndEmployeeCont.Height = 0.1875F;
			this.txtEmployerAndEmployeeCont.HyperLink = null;
			this.txtEmployerAndEmployeeCont.Left = 6.0625F;
			this.txtEmployerAndEmployeeCont.Name = "txtEmployerAndEmployeeCont";
			this.txtEmployerAndEmployeeCont.Style = "text-align: right";
			this.txtEmployerAndEmployeeCont.Text = null;
			this.txtEmployerAndEmployeeCont.Top = 4.25F;
			this.txtEmployerAndEmployeeCont.Width = 1.8125F;
			// 
			// Line42
			// 
			this.Line42.Height = 0F;
			this.Line42.Left = 0.9375F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 4.236111F;
			this.Line42.Width = 0.1388888F;
			this.Line42.X1 = 0.9375F;
			this.Line42.X2 = 1.076389F;
			this.Line42.Y1 = 4.236111F;
			this.Line42.Y2 = 4.236111F;
			// 
			// Line43
			// 
			this.Line43.Height = 0F;
			this.Line43.Left = 1.763889F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 4.236111F;
			this.Line43.Width = 0.138889F;
			this.Line43.X1 = 1.763889F;
			this.Line43.X2 = 1.902778F;
			this.Line43.Y1 = 4.236111F;
			this.Line43.Y2 = 4.236111F;
			// 
			// Line44
			// 
			this.Line44.Height = 0F;
			this.Line44.Left = 0F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 4.5F;
			this.Line44.Width = 7.875F;
			this.Line44.X1 = 0F;
			this.Line44.X2 = 7.875F;
			this.Line44.Y1 = 4.5F;
			this.Line44.Y2 = 4.5F;
			// 
			// Line45
			// 
			this.Line45.Height = 0F;
			this.Line45.Left = 0F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 4.534722F;
			this.Line45.Width = 7.875F;
			this.Line45.X1 = 0F;
			this.Line45.X2 = 7.875F;
			this.Line45.Y1 = 4.534722F;
			this.Line45.Y2 = 4.534722F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1875F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 2F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label67.Text = "GROUP LIFE INSURANCE FINANCIAL DATA";
			this.Label67.Top = 4.5625F;
			this.Label67.Width = 4F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1875F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 0F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "";
			this.Label68.Text = "9.  Basic Premiums - Active";
			this.Label68.Top = 4.875F;
			this.Label68.Width = 3.4375F;
			// 
			// Line46
			// 
			this.Line46.Height = 0F;
			this.Line46.Left = 6.0625F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 5.0625F;
			this.Line46.Width = 1.8125F;
			this.Line46.X1 = 6.0625F;
			this.Line46.X2 = 7.875F;
			this.Line46.Y1 = 5.0625F;
			this.Line46.Y2 = 5.0625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 5.875F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "text-align: right";
			this.Label69.Text = "$";
			this.Label69.Top = 4.875F;
			this.Label69.Width = 0.1875F;
			// 
			// txtBasicPremiums
			// 
			this.txtBasicPremiums.Height = 0.1875F;
			this.txtBasicPremiums.HyperLink = null;
			this.txtBasicPremiums.Left = 6.0625F;
			this.txtBasicPremiums.Name = "txtBasicPremiums";
			this.txtBasicPremiums.Style = "text-align: right";
			this.txtBasicPremiums.Text = null;
			this.txtBasicPremiums.Top = 4.875F;
			this.txtBasicPremiums.Width = 1.8125F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 0F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "";
			this.Label71.Text = "10.  Supplemental Premiums";
			this.Label71.Top = 5.125F;
			this.Label71.Width = 3.4375F;
			// 
			// Line47
			// 
			this.Line47.Height = 0F;
			this.Line47.Left = 6.0625F;
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 5.3125F;
			this.Line47.Width = 1.8125F;
			this.Line47.X1 = 6.0625F;
			this.Line47.X2 = 7.875F;
			this.Line47.Y1 = 5.3125F;
			this.Line47.Y2 = 5.3125F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 5.875F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "text-align: right";
			this.Label72.Text = "$";
			this.Label72.Top = 5.125F;
			this.Label72.Width = 0.1875F;
			// 
			// txtSupplemental
			// 
			this.txtSupplemental.Height = 0.1875F;
			this.txtSupplemental.HyperLink = null;
			this.txtSupplemental.Left = 6.0625F;
			this.txtSupplemental.Name = "txtSupplemental";
			this.txtSupplemental.Style = "text-align: right";
			this.txtSupplemental.Text = null;
			this.txtSupplemental.Top = 5.125F;
			this.txtSupplemental.Width = 1.8125F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "";
			this.Label74.Text = "11.  Dependent Premiums";
			this.Label74.Top = 5.375F;
			this.Label74.Width = 3.4375F;
			// 
			// Line48
			// 
			this.Line48.Height = 0F;
			this.Line48.Left = 6.0625F;
			this.Line48.LineWeight = 1F;
			this.Line48.Name = "Line48";
			this.Line48.Top = 5.5625F;
			this.Line48.Width = 1.8125F;
			this.Line48.X1 = 6.0625F;
			this.Line48.X2 = 7.875F;
			this.Line48.Y1 = 5.5625F;
			this.Line48.Y2 = 5.5625F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 5.875F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "text-align: right";
			this.Label75.Text = "$";
			this.Label75.Top = 5.375F;
			this.Label75.Width = 0.1875F;
			// 
			// txtDependent
			// 
			this.txtDependent.Height = 0.1875F;
			this.txtDependent.HyperLink = null;
			this.txtDependent.Left = 6.0625F;
			this.txtDependent.Name = "txtDependent";
			this.txtDependent.Style = "text-align: right";
			this.txtDependent.Text = null;
			this.txtDependent.Top = 5.375F;
			this.txtDependent.Width = 1.8125F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 0F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "";
			this.Label76.Text = "12.  Insurance Premiums Total";
			this.Label76.Top = 5.625F;
			this.Label76.Width = 3.4375F;
			// 
			// Line49
			// 
			this.Line49.Height = 0F;
			this.Line49.Left = 6.0625F;
			this.Line49.LineWeight = 3F;
			this.Line49.Name = "Line49";
			this.Line49.Top = 6F;
			this.Line49.Width = 1.8125F;
			this.Line49.X1 = 6.0625F;
			this.Line49.X2 = 7.875F;
			this.Line49.Y1 = 6F;
			this.Line49.Y2 = 6F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 5.875F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "text-align: right";
			this.Label77.Text = "$";
			this.Label77.Top = 5.8125F;
			this.Label77.Width = 0.1875F;
			// 
			// txtTotalPremiums
			// 
			this.txtTotalPremiums.Height = 0.1875F;
			this.txtTotalPremiums.HyperLink = null;
			this.txtTotalPremiums.Left = 6.0625F;
			this.txtTotalPremiums.Name = "txtTotalPremiums";
			this.txtTotalPremiums.Style = "text-align: right";
			this.txtTotalPremiums.Text = null;
			this.txtTotalPremiums.Top = 5.8125F;
			this.txtTotalPremiums.Width = 1.8125F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1875F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 0.1875F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "";
			this.Label78.Text = " ADD ITEMS 9 THRU 11)";
			this.Label78.Top = 5.8125F;
			this.Label78.Width = 2.625F;
			// 
			// Line50
			// 
			this.Line50.Height = 0F;
			this.Line50.Left = 0F;
			this.Line50.LineWeight = 1F;
			this.Line50.Name = "Line50";
			this.Line50.Top = 6.0625F;
			this.Line50.Width = 7.875F;
			this.Line50.X1 = 0F;
			this.Line50.X2 = 7.875F;
			this.Line50.Y1 = 6.0625F;
			this.Line50.Y2 = 6.0625F;
			// 
			// Line51
			// 
			this.Line51.Height = 0F;
			this.Line51.Left = 0F;
			this.Line51.LineWeight = 1F;
			this.Line51.Name = "Line51";
			this.Line51.Top = 6.097222F;
			this.Line51.Width = 7.875F;
			this.Line51.X1 = 0F;
			this.Line51.X2 = 7.875F;
			this.Line51.Y1 = 6.097222F;
			this.Line51.Y2 = 6.097222F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1875F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 2F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label79.Text = "ADJUSTMENTS (Enter Detail Explanation Below:)";
			this.Label79.Top = 6.125F;
			this.Label79.Width = 4F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1875F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 0.25F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "text-decoration: underline";
			this.Label80.Text = "Debit (DM) or Credit (CM) #";
			this.Label80.Top = 6.5F;
			this.Label80.Width = 1.875F;
			// 
			// Line52
			// 
			this.Line52.Height = 0F;
			this.Line52.Left = 6.0625F;
			this.Line52.LineWeight = 3F;
			this.Line52.Name = "Line52";
			this.Line52.Top = 6.6875F;
			this.Line52.Width = 1.8125F;
			this.Line52.X1 = 6.0625F;
			this.Line52.X2 = 7.875F;
			this.Line52.Y1 = 6.6875F;
			this.Line52.Y2 = 6.6875F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 5.875F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "text-align: right";
			this.Label81.Text = "$";
			this.Label81.Top = 6.5F;
			this.Label81.Width = 0.1875F;
			// 
			// txtTotalDebitAndCredit
			// 
			this.txtTotalDebitAndCredit.Height = 0.1875F;
			this.txtTotalDebitAndCredit.HyperLink = null;
			this.txtTotalDebitAndCredit.Left = 6.0625F;
			this.txtTotalDebitAndCredit.Name = "txtTotalDebitAndCredit";
			this.txtTotalDebitAndCredit.Style = "text-align: right";
			this.txtTotalDebitAndCredit.Text = null;
			this.txtTotalDebitAndCredit.Top = 6.5F;
			this.txtTotalDebitAndCredit.Width = 1.8125F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1875F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 0.1875F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-style: italic";
			this.Label83.Text = "(Please attach a copy of each DM or CM)";
			this.Label83.Top = 6.6875F;
			this.Label83.Width = 2.9375F;
			// 
			// Line53
			// 
			this.Line53.Height = 0F;
			this.Line53.Left = 2.4375F;
			this.Line53.LineWeight = 1F;
			this.Line53.Name = "Line53";
			this.Line53.Top = 6.6875F;
			this.Line53.Width = 1.0625F;
			this.Line53.X1 = 2.4375F;
			this.Line53.X2 = 3.5F;
			this.Line53.Y1 = 6.6875F;
			this.Line53.Y2 = 6.6875F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1875F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 2.25F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "text-align: right";
			this.Label84.Text = "#";
			this.Label84.Top = 6.5F;
			this.Label84.Width = 0.1875F;
			// 
			// txtDebOrCredNum1
			// 
			this.txtDebOrCredNum1.Height = 0.1875F;
			this.txtDebOrCredNum1.HyperLink = null;
			this.txtDebOrCredNum1.Left = 2.4375F;
			this.txtDebOrCredNum1.Name = "txtDebOrCredNum1";
			this.txtDebOrCredNum1.Style = "";
			this.txtDebOrCredNum1.Text = null;
			this.txtDebOrCredNum1.Top = 6.5F;
			this.txtDebOrCredNum1.Width = 1.0625F;
			// 
			// Line54
			// 
			this.Line54.Height = 0F;
			this.Line54.Left = 3.75F;
			this.Line54.LineWeight = 1F;
			this.Line54.Name = "Line54";
			this.Line54.Top = 6.6875F;
			this.Line54.Width = 1.0625F;
			this.Line54.X1 = 3.75F;
			this.Line54.X2 = 4.8125F;
			this.Line54.Y1 = 6.6875F;
			this.Line54.Y2 = 6.6875F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1875F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 3.5625F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "text-align: right";
			this.Label86.Text = "#";
			this.Label86.Top = 6.5F;
			this.Label86.Width = 0.1875F;
			// 
			// txtDebOrCredNum2
			// 
			this.txtDebOrCredNum2.Height = 0.1875F;
			this.txtDebOrCredNum2.HyperLink = null;
			this.txtDebOrCredNum2.Left = 3.75F;
			this.txtDebOrCredNum2.Name = "txtDebOrCredNum2";
			this.txtDebOrCredNum2.Style = "";
			this.txtDebOrCredNum2.Text = null;
			this.txtDebOrCredNum2.Top = 6.5F;
			this.txtDebOrCredNum2.Width = 1.0625F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 0F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "";
			this.Label88.Text = "14.  Adjustments to Prior Period Grant Funded Compensation:";
			this.Label88.Top = 6.9375F;
			this.Label88.Width = 3.75F;
			// 
			// Line55
			// 
			this.Line55.Height = 0F;
			this.Line55.Left = 6.0625F;
			this.Line55.LineWeight = 3F;
			this.Line55.Name = "Line55";
			this.Line55.Top = 7.125F;
			this.Line55.Width = 1.8125F;
			this.Line55.X1 = 6.0625F;
			this.Line55.X2 = 7.875F;
			this.Line55.Y1 = 7.125F;
			this.Line55.Y2 = 7.125F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 5.875F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "text-align: right";
			this.Label89.Text = "$";
			this.Label89.Top = 6.9375F;
			this.Label89.Width = 0.1875F;
			// 
			// txtCompensationAdjustment
			// 
			this.txtCompensationAdjustment.Height = 0.1875F;
			this.txtCompensationAdjustment.HyperLink = null;
			this.txtCompensationAdjustment.Left = 6.0625F;
			this.txtCompensationAdjustment.Name = "txtCompensationAdjustment";
			this.txtCompensationAdjustment.Style = "text-align: right";
			this.txtCompensationAdjustment.Text = null;
			this.txtCompensationAdjustment.Top = 6.9375F;
			this.txtCompensationAdjustment.Width = 1.8125F;
			// 
			// Line56
			// 
			this.Line56.Height = 0F;
			this.Line56.Left = 1.145833F;
			this.Line56.LineWeight = 1F;
			this.Line56.Name = "Line56";
			this.Line56.Top = 7.111111F;
			this.Line56.Width = 0.6666666F;
			this.Line56.X1 = 1.145833F;
			this.Line56.X2 = 1.8125F;
			this.Line56.Y1 = 7.111111F;
			this.Line56.Y2 = 7.111111F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 0F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "";
			this.Label91.Text = "15.  Adjustments to Prior Period Grant Funded EmployER Contributions:";
			this.Label91.Top = 7.1875F;
			this.Label91.Width = 4.3125F;
			// 
			// Line57
			// 
			this.Line57.Height = 0F;
			this.Line57.Left = 6.0625F;
			this.Line57.LineWeight = 3F;
			this.Line57.Name = "Line57";
			this.Line57.Top = 7.375F;
			this.Line57.Width = 1.8125F;
			this.Line57.X1 = 6.0625F;
			this.Line57.X2 = 7.875F;
			this.Line57.Y1 = 7.375F;
			this.Line57.Y2 = 7.375F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 5.875F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "text-align: right";
			this.Label92.Text = "$";
			this.Label92.Top = 7.1875F;
			this.Label92.Width = 0.1875F;
			// 
			// txtContributionAdjustment
			// 
			this.txtContributionAdjustment.Height = 0.1875F;
			this.txtContributionAdjustment.HyperLink = null;
			this.txtContributionAdjustment.Left = 6.0625F;
			this.txtContributionAdjustment.Name = "txtContributionAdjustment";
			this.txtContributionAdjustment.Style = "text-align: right";
			this.txtContributionAdjustment.Text = null;
			this.txtContributionAdjustment.Top = 7.1875F;
			this.txtContributionAdjustment.Width = 1.8125F;
			// 
			// Line58
			// 
			this.Line58.Height = 0F;
			this.Line58.Left = 1.145833F;
			this.Line58.LineWeight = 1F;
			this.Line58.Name = "Line58";
			this.Line58.Top = 7.361111F;
			this.Line58.Width = 0.6666666F;
			this.Line58.X1 = 1.145833F;
			this.Line58.X2 = 1.8125F;
			this.Line58.Y1 = 7.361111F;
			this.Line58.Y2 = 7.361111F;
			// 
			// Line59
			// 
			this.Line59.Height = 0F;
			this.Line59.Left = 3.027778F;
			this.Line59.LineWeight = 1F;
			this.Line59.Name = "Line59";
			this.Line59.Top = 7.361111F;
			this.Line59.Width = 0.1597223F;
			this.Line59.X1 = 3.027778F;
			this.Line59.X2 = 3.1875F;
			this.Line59.Y1 = 7.361111F;
			this.Line59.Y2 = 7.361111F;
			// 
			// Line60
			// 
			this.Line60.Height = 0F;
			this.Line60.Left = 6.0625F;
			this.Line60.LineWeight = 3F;
			this.Line60.Name = "Line60";
			this.Line60.Top = 7.8125F;
			this.Line60.Width = 1.8125F;
			this.Line60.X1 = 6.0625F;
			this.Line60.X2 = 7.875F;
			this.Line60.Y1 = 7.8125F;
			this.Line60.Y2 = 7.8125F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1875F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 5.875F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "text-align: right";
			this.Label96.Text = "$";
			this.Label96.Top = 7.625F;
			this.Label96.Width = 0.1875F;
			// 
			// txtPurchaseAgreements
			// 
			this.txtPurchaseAgreements.Height = 0.1875F;
			this.txtPurchaseAgreements.HyperLink = null;
			this.txtPurchaseAgreements.Left = 6.0625F;
			this.txtPurchaseAgreements.Name = "txtPurchaseAgreements";
			this.txtPurchaseAgreements.Style = "text-align: right";
			this.txtPurchaseAgreements.Text = null;
			this.txtPurchaseAgreements.Top = 7.625F;
			this.txtPurchaseAgreements.Width = 1.8125F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1875F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.25F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "text-decoration: underline";
			this.Label99.Text = "Adjustments - Other (Miscellaneous)";
			this.Label99.Top = 8.0625F;
			this.Label99.Width = 4.0625F;
			// 
			// Line63
			// 
			this.Line63.Height = 0F;
			this.Line63.Left = 6.0625F;
			this.Line63.LineWeight = 3F;
			this.Line63.Name = "Line63";
			this.Line63.Top = 8.25F;
			this.Line63.Width = 1.8125F;
			this.Line63.X1 = 6.0625F;
			this.Line63.X2 = 7.875F;
			this.Line63.Y1 = 8.25F;
			this.Line63.Y2 = 8.25F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 5.875F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "text-align: right";
			this.Label100.Text = "$";
			this.Label100.Top = 8.0625F;
			this.Label100.Width = 0.1875F;
			// 
			// txtAdjustments
			// 
			this.txtAdjustments.Height = 0.1875F;
			this.txtAdjustments.HyperLink = null;
			this.txtAdjustments.Left = 6.0625F;
			this.txtAdjustments.Name = "txtAdjustments";
			this.txtAdjustments.Style = "text-align: right";
			this.txtAdjustments.Text = null;
			this.txtAdjustments.Top = 8.0625F;
			this.txtAdjustments.Width = 1.8125F;
			// 
			// Line65
			// 
			this.Line65.Height = 0F;
			this.Line65.Left = 0F;
			this.Line65.LineWeight = 1F;
			this.Line65.Name = "Line65";
			this.Line65.Top = 8.5F;
			this.Line65.Width = 7.875F;
			this.Line65.X1 = 0F;
			this.Line65.X2 = 7.875F;
			this.Line65.Y1 = 8.5F;
			this.Line65.Y2 = 8.5F;
			// 
			// Line66
			// 
			this.Line66.Height = 0F;
			this.Line66.Left = 0F;
			this.Line66.LineWeight = 1F;
			this.Line66.Name = "Line66";
			this.Line66.Top = 8.534722F;
			this.Line66.Width = 7.875F;
			this.Line66.X1 = 0F;
			this.Line66.X2 = 7.875F;
			this.Line66.Y1 = 8.534722F;
			this.Line66.Y2 = 8.534722F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1875F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 2.75F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label103.Text = "TOTAL REMITTANCE";
			this.Label103.Top = 8.5625F;
			this.Label103.Width = 1.8125F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1875F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 3F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "";
			this.Label104.Text = "(ADD ITEMS 8,12,13,15,16 & 17)";
			this.Label104.Top = 8.75F;
			this.Label104.Width = 2.0625F;
			// 
			// Line67
			// 
			this.Line67.Height = 0F;
			this.Line67.Left = 6.0625F;
			this.Line67.LineWeight = 3F;
			this.Line67.Name = "Line67";
			this.Line67.Top = 8.9375F;
			this.Line67.Width = 1.8125F;
			this.Line67.X1 = 6.0625F;
			this.Line67.X2 = 7.875F;
			this.Line67.Y1 = 8.9375F;
			this.Line67.Y2 = 8.9375F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1875F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 5.875F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "text-align: right";
			this.Label105.Text = "$";
			this.Label105.Top = 8.75F;
			this.Label105.Width = 0.1875F;
			// 
			// txtTotalRemittance
			// 
			this.txtTotalRemittance.Height = 0.1875F;
			this.txtTotalRemittance.HyperLink = null;
			this.txtTotalRemittance.Left = 6.0625F;
			this.txtTotalRemittance.Name = "txtTotalRemittance";
			this.txtTotalRemittance.Style = "text-align: right";
			this.txtTotalRemittance.Text = null;
			this.txtTotalRemittance.Top = 8.75F;
			this.txtTotalRemittance.Width = 1.8125F;
			// 
			// Line68
			// 
			this.Line68.Height = 0F;
			this.Line68.Left = 0F;
			this.Line68.LineWeight = 1F;
			this.Line68.Name = "Line68";
			this.Line68.Top = 9.0625F;
			this.Line68.Width = 7.875F;
			this.Line68.X1 = 0F;
			this.Line68.X2 = 7.875F;
			this.Line68.Y1 = 9.0625F;
			this.Line68.Y2 = 9.0625F;
			// 
			// Line69
			// 
			this.Line69.Height = 0F;
			this.Line69.Left = 0F;
			this.Line69.LineWeight = 1F;
			this.Line69.Name = "Line69";
			this.Line69.Top = 9.097222F;
			this.Line69.Width = 7.875F;
			this.Line69.X1 = 0F;
			this.Line69.X2 = 7.875F;
			this.Line69.Y1 = 9.097222F;
			this.Line69.Y2 = 9.097222F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.375F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 0.0625F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "font-weight: bold; text-align: left";
			this.Label106.Text = "My signature certifies that I am the person responsible for the content of this r" + "eport, that I have reviewed the Summary and Detail Reports, and that they are co" + "mplete and accurate.";
			this.Label106.Top = 9.1875F;
			this.Label106.Width = 7.875F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1875F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 0.0625F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.Label107.Text = "Signature:";
			this.Label107.Top = 9.625F;
			this.Label107.Width = 0.875F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1875F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 0.0625F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.Label108.Text = "Title:";
			this.Label108.Top = 10F;
			this.Label108.Width = 0.875F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1875F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 5.4375F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.Label109.Text = "Date:";
			this.Label109.Top = 9.625F;
			this.Label109.Width = 0.5625F;
			// 
			// Line70
			// 
			this.Line70.Height = 0F;
			this.Line70.Left = 1.0625F;
			this.Line70.LineWeight = 1F;
			this.Line70.Name = "Line70";
			this.Line70.Top = 9.8125F;
			this.Line70.Width = 4.25F;
			this.Line70.X1 = 1.0625F;
			this.Line70.X2 = 5.3125F;
			this.Line70.Y1 = 9.8125F;
			this.Line70.Y2 = 9.8125F;
			// 
			// Line71
			// 
			this.Line71.Height = 0F;
			this.Line71.Left = 6F;
			this.Line71.LineWeight = 1F;
			this.Line71.Name = "Line71";
			this.Line71.Top = 9.8125F;
			this.Line71.Width = 1.875F;
			this.Line71.X1 = 6F;
			this.Line71.X2 = 7.875F;
			this.Line71.Y1 = 9.8125F;
			this.Line71.Y2 = 9.8125F;
			// 
			// Line72
			// 
			this.Line72.Height = 0F;
			this.Line72.Left = 1.0625F;
			this.Line72.LineWeight = 1F;
			this.Line72.Name = "Line72";
			this.Line72.Top = 10.1875F;
			this.Line72.Width = 4.25F;
			this.Line72.X1 = 1.0625F;
			this.Line72.X2 = 5.3125F;
			this.Line72.Y1 = 10.1875F;
			this.Line72.Y2 = 10.1875F;
			// 
			// txtDateSigned
			// 
			this.txtDateSigned.Height = 0.1875F;
			this.txtDateSigned.HyperLink = null;
			this.txtDateSigned.Left = 6F;
			this.txtDateSigned.Name = "txtDateSigned";
			this.txtDateSigned.Style = "text-align: left";
			this.txtDateSigned.Text = null;
			this.txtDateSigned.Top = 9.625F;
			this.txtDateSigned.Width = 1.875F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1875F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 0F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "text-align: left";
			this.Label110.Text = "13.";
			this.Label110.Top = 6.5F;
			this.Label110.Width = 0.25F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 0F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "text-align: left";
			this.Label111.Text = "16.";
			this.Label111.Top = 7.625F;
			this.Label111.Width = 0.25F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "text-align: left";
			this.Label112.Text = "17.";
			this.Label112.Top = 8.0625F;
			this.Label112.Width = 0.25F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 4.8125F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "text-align: right";
			this.Label113.Text = "TOTAL AMOUNT:";
			this.Label113.Top = 6.5F;
			this.Label113.Width = 1.0625F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1875F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 4.8125F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "text-align: right";
			this.Label114.Text = "TOTAL AMOUNT:";
			this.Label114.Top = 6.9375F;
			this.Label114.Width = 1.0625F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 4.8125F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "text-align: right";
			this.Label115.Text = "TOTAL AMOUNT:";
			this.Label115.Top = 7.1875F;
			this.Label115.Width = 1.0625F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 4.8125F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "text-align: right";
			this.Label116.Text = "TOTAL AMOUNT:";
			this.Label116.Top = 7.625F;
			this.Label116.Width = 1.0625F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 4.8125F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "text-align: right";
			this.Label117.Text = "TOTAL AMOUNT:";
			this.Label117.Top = 8.0625F;
			this.Label117.Width = 1.0625F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 4.8125F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "text-align: center";
			this.Label118.Text = "TOTAL";
			this.Label118.Top = 5.8125F;
			this.Label118.Width = 1.0625F;
			// 
			// lblAdjustment1
			// 
			this.lblAdjustment1.Height = 0.1875F;
			this.lblAdjustment1.HyperLink = null;
			this.lblAdjustment1.Left = 0.3125F;
			this.lblAdjustment1.Name = "lblAdjustment1";
			this.lblAdjustment1.Style = "";
			this.lblAdjustment1.Text = null;
			this.lblAdjustment1.Top = 0.5F;
			this.lblAdjustment1.Width = 2.6875F;
			// 
			// lblAdjustment2
			// 
			this.lblAdjustment2.Height = 0.1875F;
			this.lblAdjustment2.HyperLink = null;
			this.lblAdjustment2.Left = 0.3125F;
			this.lblAdjustment2.Name = "lblAdjustment2";
			this.lblAdjustment2.Style = "";
			this.lblAdjustment2.Text = null;
			this.lblAdjustment2.Top = 0.6875F;
			this.lblAdjustment2.Width = 2.6875F;
			// 
			// lblAdjustment3
			// 
			this.lblAdjustment3.Height = 0.1875F;
			this.lblAdjustment3.HyperLink = null;
			this.lblAdjustment3.Left = 0.3125F;
			this.lblAdjustment3.Name = "lblAdjustment3";
			this.lblAdjustment3.Style = "";
			this.lblAdjustment3.Text = null;
			this.lblAdjustment3.Top = 0.875F;
			this.lblAdjustment3.Width = 2.6875F;
			// 
			// lblAdjustment4
			// 
			this.lblAdjustment4.Height = 0.1875F;
			this.lblAdjustment4.HyperLink = null;
			this.lblAdjustment4.Left = 0.3125F;
			this.lblAdjustment4.Name = "lblAdjustment4";
			this.lblAdjustment4.Style = "";
			this.lblAdjustment4.Text = null;
			this.lblAdjustment4.Top = 1.0625F;
			this.lblAdjustment4.Width = 2.6875F;
			// 
			// lblAdjustment5
			// 
			this.lblAdjustment5.Height = 0.1875F;
			this.lblAdjustment5.HyperLink = null;
			this.lblAdjustment5.Left = 0.3125F;
			this.lblAdjustment5.Name = "lblAdjustment5";
			this.lblAdjustment5.Style = "";
			this.lblAdjustment5.Text = null;
			this.lblAdjustment5.Top = 1.25F;
			this.lblAdjustment5.Width = 2.6875F;
			// 
			// lblAdjustment6
			// 
			this.lblAdjustment6.Height = 0.1875F;
			this.lblAdjustment6.HyperLink = null;
			this.lblAdjustment6.Left = 0.3125F;
			this.lblAdjustment6.Name = "lblAdjustment6";
			this.lblAdjustment6.Style = "";
			this.lblAdjustment6.Text = null;
			this.lblAdjustment6.Top = 1.4375F;
			this.lblAdjustment6.Width = 2.6875F;
			// 
			// lblAdjustment7
			// 
			this.lblAdjustment7.Height = 0.1875F;
			this.lblAdjustment7.HyperLink = null;
			this.lblAdjustment7.Left = 0.3125F;
			this.lblAdjustment7.Name = "lblAdjustment7";
			this.lblAdjustment7.Style = "";
			this.lblAdjustment7.Text = null;
			this.lblAdjustment7.Top = 1.625F;
			this.lblAdjustment7.Width = 2.6875F;
			// 
			// txtAdjustment1
			// 
			this.txtAdjustment1.Height = 0.1875F;
			this.txtAdjustment1.HyperLink = null;
			this.txtAdjustment1.Left = 3.0625F;
			this.txtAdjustment1.Name = "txtAdjustment1";
			this.txtAdjustment1.Style = "text-align: right";
			this.txtAdjustment1.Text = null;
			this.txtAdjustment1.Top = 0.5F;
			this.txtAdjustment1.Width = 0.9375F;
			// 
			// txtAdjustment2
			// 
			this.txtAdjustment2.Height = 0.1875F;
			this.txtAdjustment2.HyperLink = null;
			this.txtAdjustment2.Left = 3.0625F;
			this.txtAdjustment2.Name = "txtAdjustment2";
			this.txtAdjustment2.Style = "text-align: right";
			this.txtAdjustment2.Text = null;
			this.txtAdjustment2.Top = 0.6875F;
			this.txtAdjustment2.Width = 0.9375F;
			// 
			// txtAdjustment3
			// 
			this.txtAdjustment3.Height = 0.1875F;
			this.txtAdjustment3.HyperLink = null;
			this.txtAdjustment3.Left = 3.0625F;
			this.txtAdjustment3.Name = "txtAdjustment3";
			this.txtAdjustment3.Style = "text-align: right";
			this.txtAdjustment3.Text = null;
			this.txtAdjustment3.Top = 0.875F;
			this.txtAdjustment3.Width = 0.9375F;
			// 
			// txtAdjustment4
			// 
			this.txtAdjustment4.Height = 0.1875F;
			this.txtAdjustment4.HyperLink = null;
			this.txtAdjustment4.Left = 3.0625F;
			this.txtAdjustment4.Name = "txtAdjustment4";
			this.txtAdjustment4.Style = "text-align: right";
			this.txtAdjustment4.Text = null;
			this.txtAdjustment4.Top = 1.0625F;
			this.txtAdjustment4.Width = 0.9375F;
			// 
			// txtAdjustment5
			// 
			this.txtAdjustment5.Height = 0.1875F;
			this.txtAdjustment5.HyperLink = null;
			this.txtAdjustment5.Left = 3.0625F;
			this.txtAdjustment5.Name = "txtAdjustment5";
			this.txtAdjustment5.Style = "text-align: right";
			this.txtAdjustment5.Text = null;
			this.txtAdjustment5.Top = 1.25F;
			this.txtAdjustment5.Width = 0.9375F;
			// 
			// txtAdjustment6
			// 
			this.txtAdjustment6.Height = 0.1875F;
			this.txtAdjustment6.HyperLink = null;
			this.txtAdjustment6.Left = 3.0625F;
			this.txtAdjustment6.Name = "txtAdjustment6";
			this.txtAdjustment6.Style = "text-align: right";
			this.txtAdjustment6.Text = null;
			this.txtAdjustment6.Top = 1.4375F;
			this.txtAdjustment6.Width = 0.9375F;
			// 
			// txtAdjustment7
			// 
			this.txtAdjustment7.Height = 0.1875F;
			this.txtAdjustment7.HyperLink = null;
			this.txtAdjustment7.Left = 3.0625F;
			this.txtAdjustment7.Name = "txtAdjustment7";
			this.txtAdjustment7.Style = "text-align: right";
			this.txtAdjustment7.Text = null;
			this.txtAdjustment7.Top = 1.625F;
			this.txtAdjustment7.Width = 0.9375F;
			// 
			// lblAdjustment8
			// 
			this.lblAdjustment8.Height = 0.1875F;
			this.lblAdjustment8.HyperLink = null;
			this.lblAdjustment8.Left = 0.3125F;
			this.lblAdjustment8.Name = "lblAdjustment8";
			this.lblAdjustment8.Style = "";
			this.lblAdjustment8.Text = null;
			this.lblAdjustment8.Top = 1.8125F;
			this.lblAdjustment8.Width = 2.6875F;
			// 
			// txtAdjustment8
			// 
			this.txtAdjustment8.Height = 0.1875F;
			this.txtAdjustment8.HyperLink = null;
			this.txtAdjustment8.Left = 3.0625F;
			this.txtAdjustment8.Name = "txtAdjustment8";
			this.txtAdjustment8.Style = "text-align: right";
			this.txtAdjustment8.Text = null;
			this.txtAdjustment8.Top = 1.8125F;
			this.txtAdjustment8.Width = 0.9375F;
			// 
			// rptMSRSNewPayrollSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateCompleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDates1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreparer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableCompensation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedFundPerc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrantFundedCompensation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerContributions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployeeContributions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAndEmployeeCont)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasicPremiums)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSupplemental)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPremiums)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDebitAndCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebOrCredNum1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebOrCredNum2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompensationAdjustment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContributionAdjustment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseAgreements)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRemittance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateSigned)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustment8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDateCompleted;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPaidDates1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPreparer;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableCompensation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFedFundPerc;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGrantFundedCompensation;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerContributions;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalEmployeeContributions;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerAndEmployeeCont;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBasicPremiums;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSupplemental;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDependent;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalPremiums;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalDebitAndCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDebOrCredNum1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDebOrCredNum2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCompensationAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtContributionAdjustment;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPurchaseAgreements;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustments;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalRemittance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDateSigned;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustment8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAdjustment8;
	}
}
