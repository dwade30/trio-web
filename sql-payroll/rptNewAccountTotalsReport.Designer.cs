namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewAccountTotalsReport.
	/// </summary>
	partial class rptNewAccountTotalsReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewAccountTotalsReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNumber,
				this.txtName,
				this.txtType,
				this.txtHours,
				this.txtAmount,
				this.txtTotal
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.lblPage,
				this.txtTime,
				this.fldCaption2
			});
			this.PageHeader.Height = 0.8333333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccountNumber,
				this.Label2,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4
			});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.40625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalHours,
				this.txtTotalAmount,
				this.txtTotalTotal,
				this.Line5
			});
			this.GroupFooter1.Height = 0.3333333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 7.8125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.3125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.53125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// fldCaption2
			// 
			this.fldCaption2.Height = 0.1875F;
			this.fldCaption2.Left = 0.0625F;
			this.fldCaption2.Name = "fldCaption2";
			this.fldCaption2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.fldCaption2.Text = "Account";
			this.fldCaption2.Top = 0.25F;
			this.fldCaption2.Width = 7.8125F;
			// 
			// txtAccountNumber
			// 
			this.txtAccountNumber.Height = 0.2F;
			this.txtAccountNumber.Left = 2F;
			this.txtAccountNumber.Name = "txtAccountNumber";
			this.txtAccountNumber.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtAccountNumber.Text = null;
			this.txtAccountNumber.Top = 0.03333334F;
			this.txtAccountNumber.Width = 5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.3333333F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.06666667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label2.Text = "Employee Number";
			this.Label2.Top = 0.03333334F;
			this.Label2.Width = 0.6333333F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.3333333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 1.2F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.Label6.Text = "Account #  Name";
			this.Label6.Top = 0.03333334F;
			this.Label6.Width = 0.7333333F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Type";
			this.Label7.Top = 0.2F;
			this.Label7.Width = 1.033333F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.933333F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label8.Text = "Hours";
			this.Label8.Top = 0.2F;
			this.Label8.Width = 0.5333334F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label9.Text = "Amount";
			this.Label9.Top = 0.2F;
			this.Label9.Width = 0.7333333F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.333333F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label10.Text = "Total";
			this.Label10.Top = 0.2F;
			this.Label10.Width = 0.7333333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.4F;
			this.Line1.Width = 7.9F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9F;
			this.Line1.Y1 = 0.4F;
			this.Line1.Y2 = 0.4F;
			// 
			// Line2
			// 
			this.Line2.Height = 0.4F;
			this.Line2.Left = 7.9F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 0F;
			this.Line2.X1 = 7.9F;
			this.Line2.X2 = 7.9F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0.4F;
			// 
			// Line3
			// 
			this.Line3.Height = 0.4F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 0F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 0F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0.4F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 7.9F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.9F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// txtNumber
			// 
			this.txtNumber.Height = 0.1875F;
			this.txtNumber.Left = 0.125F;
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtNumber.Text = null;
			this.txtNumber.Top = 0.03125F;
			this.txtNumber.Width = 0.5F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.8125F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtName.Text = null;
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 2.875F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.txtType.Text = null;
			this.txtType.Top = 0.03125F;
			this.txtType.Width = 1.875F;
			// 
			// txtHours
			// 
			this.txtHours.Height = 0.1875F;
			this.txtHours.Left = 4.8125F;
			this.txtHours.Name = "txtHours";
			this.txtHours.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtHours.Text = null;
			this.txtHours.Top = 0.03125F;
			this.txtHours.Width = 0.5F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 5.5F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.03125F;
			this.txtAmount.Width = 0.75F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 6.3125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.03125F;
			this.txtTotal.Width = 0.75F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1875F;
			this.txtTotalHours.Left = 4.75F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtTotalHours.Text = null;
			this.txtTotalHours.Top = 0.0625F;
			this.txtTotalHours.Width = 0.5625F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1875F;
			this.txtTotalAmount.Left = 5.5F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtTotalAmount.Text = null;
			this.txtTotalAmount.Top = 0.0625F;
			this.txtTotalAmount.Width = 0.75F;
			// 
			// txtTotalTotal
			// 
			this.txtTotalTotal.Height = 0.1875F;
			this.txtTotalTotal.Left = 6.3125F;
			this.txtTotalTotal.Name = "txtTotalTotal";
			this.txtTotalTotal.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtTotalTotal.Text = null;
			this.txtTotalTotal.Top = 0.0625F;
			this.txtTotalTotal.Width = 0.75F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.5F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.03125F;
			this.Line5.Width = 2.875F;
			this.Line5.X1 = 4.5F;
			this.Line5.X2 = 7.375F;
			this.Line5.Y1 = 0.03125F;
			this.Line5.Y2 = 0.03125F;
			// 
			// rptNewAccountTotalsReport
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEndedAndCanceled += ActiveReport_ReportEndedAndCanceled;
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.2222222F;
			this.PageSettings.Margins.Right = 0.2222222F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.90625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
	}
}