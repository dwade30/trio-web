﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTrustAndAgencyCodes.
	/// </summary>
	partial class RptTrustAndAgencyCodes
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RptTrustAndAgencyCodes));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRecipient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecipient)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldCheck,
            this.fldAmount,
            this.fldRecipient});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1666667F;
			this.fldDate.Left = 0.06666667F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 1.866667F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1666667F;
			this.fldCheck.Left = 2F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.7333333F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1666667F;
			this.fldAmount.Left = 2.8F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.9333333F;
			// 
			// fldRecipient
			// 
			this.fldRecipient.Height = 0.1666667F;
			this.fldRecipient.Left = 3.833333F;
			this.fldRecipient.Name = "fldRecipient";
			this.fldRecipient.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldRecipient.Text = "Field1";
			this.fldRecipient.Top = 0F;
			this.fldRecipient.Width = 2.633333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.lblPayDate,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Line1});
			this.PageHeader.Height = 0.7916667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.229F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.Label1.Text = "Trust & Agency Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.719F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.21875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.21875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.229F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = null;
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 3.719F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label8.Text = "Date";
			this.Label8.Top = 0.59375F;
			this.Label8.Width = 0.71875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.177F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Check";
			this.Label9.Top = 0.6F;
			this.Label9.Width = 0.733F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.177F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.8F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Amount";
			this.Label10.Top = 0.6F;
			this.Label10.Width = 0.933F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.177F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.833333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label11.Text = "Recipient";
			this.Label11.Top = 0.6F;
			this.Label11.Width = 0.933F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.78125F;
			this.Line1.Width = 5.96875F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 6F;
			this.Line1.Y1 = 0.78125F;
			this.Line1.Y2 = 0.78125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line2,
            this.Label12,
            this.fldFinalTotal,
            this.Label13,
            this.fldFinalCount});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.766667F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.123F;
			this.Line2.Width = 3.833333F;
			this.Line2.X1 = 1.766667F;
			this.Line2.X2 = 5.6F;
			this.Line2.Y1 = 0.123F;
			this.Line2.Y2 = 0.123F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.153F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.7F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.Label12.Text = "Final Total";
			this.Label12.Top = 0.157F;
			this.Label12.Width = 1F;
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.Height = 0.153F;
			this.fldFinalTotal.Left = 2.8F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.fldFinalTotal.Text = "Field1";
			this.fldFinalTotal.Top = 0.157F;
			this.fldFinalTotal.Width = 0.933F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.153F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.9F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
			this.Label13.Text = "Number Listed";
			this.Label13.Top = 0.157F;
			this.Label13.Width = 1.033F;
			// 
			// fldFinalCount
			// 
			this.fldFinalCount.Height = 0.153F;
			this.fldFinalCount.Left = 5.033F;
			this.fldFinalCount.Name = "fldFinalCount";
			this.fldFinalCount.Style = "font-weight: bold; text-align: right";
			this.fldFinalCount.Text = "Field1";
			this.fldFinalCount.Top = 0.157F;
			this.fldFinalCount.Width = 0.5F;
			// 
			// RptTrustAndAgencyCodes
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecipient)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRecipient;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCount;
	}
}
