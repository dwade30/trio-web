//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmACHBankInformation.
	/// </summary>
	partial class frmACHBankInformation
	{
		public fecherFoundation.FCComboBox cmbPrenote;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDateSelect;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtBankName;
		public fecherFoundation.FCTextBox txtACHRT;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtODFI;
		public fecherFoundation.FCTextBox txtImmediateOriginRT;
		public fecherFoundation.FCTextBox txtImmediateOriginName;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cmbAccountType;
		public fecherFoundation.FCCheckBox chkTestFile;
		public fecherFoundation.FCTextBox txtEmployerID;
		public fecherFoundation.FCTextBox txtEmpAccount;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCTextBox txtEmpRT;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame framACH;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdPrenote;
		public fecherFoundation.FCTextBox txtFileName;
		public Global.T2KDateBox T2KEffectiveEntryDate;
		public fecherFoundation.FCFrame framPayDate;
		public fecherFoundation.FCComboBox cboPayDateYear;
		public fecherFoundation.FCComboBox cboPayDate;
		public fecherFoundation.FCComboBox cboPayRunID;
		public fecherFoundation.FCLabel lblDateSelect_3;
		public fecherFoundation.FCLabel lblDateSelect_4;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSelect;
		public fecherFoundation.FCToolStripMenuItem mnuCreate;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbPrenote = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtBankName = new fecherFoundation.FCTextBox();
            this.txtACHRT = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtODFI = new fecherFoundation.FCTextBox();
            this.txtImmediateOriginRT = new fecherFoundation.FCTextBox();
            this.txtImmediateOriginName = new fecherFoundation.FCTextBox();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmbAccountType = new fecherFoundation.FCComboBox();
            this.chkTestFile = new fecherFoundation.FCCheckBox();
            this.txtEmployerID = new fecherFoundation.FCTextBox();
            this.txtEmpAccount = new fecherFoundation.FCTextBox();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.txtEmpRT = new fecherFoundation.FCTextBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.framACH = new fecherFoundation.FCFrame();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cmdPrenote = new fecherFoundation.FCButton();
            this.txtFileName = new fecherFoundation.FCTextBox();
            this.T2KEffectiveEntryDate = new Global.T2KDateBox();
            this.framPayDate = new fecherFoundation.FCFrame();
            this.cboPayDateYear = new fecherFoundation.FCComboBox();
            this.cboPayDate = new fecherFoundation.FCComboBox();
            this.cboPayRunID = new fecherFoundation.FCComboBox();
            this.lblDateSelect_3 = new fecherFoundation.FCLabel();
            this.lblDateSelect_4 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuSelect = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCreate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdCreate = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTestFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framACH)).BeginInit();
            this.framACH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrenote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayDate)).BeginInit();
            this.framPayDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 492);
            this.BottomPanel.Size = new System.Drawing.Size(798, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framACH);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Size = new System.Drawing.Size(818, 606);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.framACH, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCreate);
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Size = new System.Drawing.Size(818, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCreate, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(257, 30);
            this.HeaderText.Text = "ACH Bank Information";
            // 
            // cmbPrenote
            // 
            this.cmbPrenote.Items.AddRange(new object[] {
            "Only DD entries marked Prenote",
            "All DD Entries"});
            this.cmbPrenote.Location = new System.Drawing.Point(20, 30);
            this.cmbPrenote.Name = "cmbPrenote";
            this.cmbPrenote.Size = new System.Drawing.Size(288, 40);
            this.cmbPrenote.TabIndex = 32;
            this.cmbPrenote.Text = "Only DD entries marked Prenote";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.txtBankName);
            this.Frame1.Controls.Add(this.txtACHRT);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(460, 116);
            this.Frame1.TabIndex = 11;
            this.Frame1.Text = "Bank / Immediate Destination";
            // 
            // txtBankName
            // 
            this.txtBankName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBankName.Location = new System.Drawing.Point(20, 56);
            this.txtBankName.MaxLength = 23;
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(270, 40);
            // 
            // txtACHRT
            // 
            this.txtACHRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtACHRT.Location = new System.Drawing.Point(310, 56);
            this.txtACHRT.MaxLength = 9;
            this.txtACHRT.Name = "txtACHRT";
            this.txtACHRT.Size = new System.Drawing.Size(130, 40);
            this.txtACHRT.TabIndex = 1;
            this.txtACHRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(310, 29);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 19);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "R/T NUMBER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(108, 19);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "BANK NAME";
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.White;
            this.Frame4.Controls.Add(this.txtODFI);
            this.Frame4.Controls.Add(this.txtImmediateOriginRT);
            this.Frame4.Controls.Add(this.txtImmediateOriginName);
            this.Frame4.Controls.Add(this.Label11);
            this.Frame4.Controls.Add(this.Label10);
            this.Frame4.Controls.Add(this.Label9);
            this.Frame4.Location = new System.Drawing.Point(30, 166);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(620, 116);
            this.Frame4.TabIndex = 34;
            this.Frame4.Text = "Immediate Origin";
            this.ToolTip1.SetToolTip(this.Frame4, "If files are not sent to an intermediate entity this information should be the sa" +
        "me as the employer information");
            // 
            // txtODFI
            // 
            this.txtODFI.BackColor = System.Drawing.SystemColors.Window;
            this.txtODFI.Location = new System.Drawing.Point(470, 56);
            this.txtODFI.MaxLength = 9;
            this.txtODFI.Name = "txtODFI";
            this.txtODFI.Size = new System.Drawing.Size(130, 40);
            this.txtODFI.TabIndex = 4;
            this.txtODFI.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtODFI, "The R/T of the originator");
            // 
            // txtImmediateOriginRT
            // 
            this.txtImmediateOriginRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginRT.Location = new System.Drawing.Point(320, 56);
            this.txtImmediateOriginRT.MaxLength = 10;
            this.txtImmediateOriginRT.Name = "txtImmediateOriginRT";
            this.txtImmediateOriginRT.Size = new System.Drawing.Size(130, 40);
            this.txtImmediateOriginRT.TabIndex = 3;
            this.txtImmediateOriginRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtImmediateOriginRT, "Normally the same as the ODFI number but can be a unique identifier");
            // 
            // txtImmediateOriginName
            // 
            this.txtImmediateOriginName.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginName.Location = new System.Drawing.Point(20, 56);
            this.txtImmediateOriginName.MaxLength = 23;
            this.txtImmediateOriginName.Name = "txtImmediateOriginName";
            this.txtImmediateOriginName.Size = new System.Drawing.Size(281, 40);
            this.txtImmediateOriginName.TabIndex = 2;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(470, 30);
            this.Label11.Name = "Label11";
            this.Label11.TabIndex = 37;
            this.Label11.Text = "ODFI NUMBER";
            this.ToolTip1.SetToolTip(this.Label11, "Normally the Immediate Origin but can be a unique employer identifier");
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(320, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(130, 16);
            this.Label10.TabIndex = 36;
            this.Label10.Text = "IMMEDIATE ORIGIN #";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 30);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(80, 16);
            this.Label9.TabIndex = 35;
            this.Label9.Text = "BANK NAME";
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.cmbAccountType);
            this.Frame2.Controls.Add(this.chkTestFile);
            this.Frame2.Controls.Add(this.txtEmployerID);
            this.Frame2.Controls.Add(this.txtEmpAccount);
            this.Frame2.Controls.Add(this.txtEmployerName);
            this.Frame2.Controls.Add(this.txtEmpRT);
            this.Frame2.Controls.Add(this.Label6);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(30, 302);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(760, 168);
            this.Frame2.TabIndex = 14;
            this.Frame2.Text = "Employer Information";
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccountType.Location = new System.Drawing.Point(20, 108);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(150, 40);
            this.cmbAccountType.TabIndex = 9;
            // 
            // chkTestFile
            // 
            this.chkTestFile.Location = new System.Drawing.Point(190, 110);
            this.chkTestFile.Name = "chkTestFile";
            this.chkTestFile.Size = new System.Drawing.Size(193, 19);
            this.chkTestFile.TabIndex = 10;
            this.chkTestFile.Text = "Create Pre-Note / Test File";
            this.chkTestFile.Visible = false;
            // 
            // txtEmployerID
            // 
            this.txtEmployerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerID.Location = new System.Drawing.Point(610, 56);
            this.txtEmployerID.MaxLength = 9;
            this.txtEmployerID.Name = "txtEmployerID";
            this.txtEmployerID.Size = new System.Drawing.Size(130, 40);
            this.txtEmployerID.TabIndex = 8;
            this.txtEmployerID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtEmpAccount
            // 
            this.txtEmpAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpAccount.Location = new System.Drawing.Point(460, 56);
            this.txtEmpAccount.MaxLength = 17;
            this.txtEmpAccount.Name = "txtEmpAccount";
            this.txtEmpAccount.Size = new System.Drawing.Size(130, 40);
            this.txtEmpAccount.TabIndex = 7;
            this.txtEmpAccount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.Location = new System.Drawing.Point(20, 56);
            this.txtEmployerName.MaxLength = 23;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(270, 40);
            this.txtEmployerName.TabIndex = 5;
            // 
            // txtEmpRT
            // 
            this.txtEmpRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpRT.Location = new System.Drawing.Point(310, 56);
            this.txtEmpRT.MaxLength = 9;
            this.txtEmpRT.Name = "txtEmpRT";
            this.txtEmpRT.Size = new System.Drawing.Size(130, 40);
            this.txtEmpRT.TabIndex = 6;
            this.txtEmpRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(610, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(86, 16);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "EMPLOYER ID";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(460, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(120, 16);
            this.Label5.TabIndex = 17;
            this.Label5.Text = "ACCOUNT NUMBER";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(310, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(80, 16);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "R/T NUMBER";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 16);
            this.Label3.TabIndex = 16;
            this.Label3.Text = "NAME";
            // 
            // framACH
            // 
            this.framACH.AppearanceKey = "groupBoxLeftBorder";
            this.framACH.BackColor = System.Drawing.Color.White;
            this.framACH.Controls.Add(this.Frame3);
            this.framACH.Controls.Add(this.txtFileName);
            this.framACH.Controls.Add(this.T2KEffectiveEntryDate);
            this.framACH.Controls.Add(this.framPayDate);
            this.framACH.Controls.Add(this.Label8);
            this.framACH.Controls.Add(this.Label7);
            this.framACH.FormatCaption = false;
            this.framACH.Location = new System.Drawing.Point(30, 30);
            this.framACH.Name = "framACH";
            this.framACH.Size = new System.Drawing.Size(760, 462);
            this.framACH.TabIndex = 19;
            this.framACH.Text = "ACH Information";
            this.framACH.Visible = false;
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.White;
            this.Frame3.Controls.Add(this.cmdPrenote);
            this.Frame3.Controls.Add(this.cmbPrenote);
            this.Frame3.Location = new System.Drawing.Point(20, 372);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(548, 90);
            this.Frame3.TabIndex = 30;
            this.Frame3.Text = "Prenote";
            // 
            // cmdPrenote
            // 
            this.cmdPrenote.AppearanceKey = "actionButton";
            this.cmdPrenote.Location = new System.Drawing.Point(328, 30);
            this.cmdPrenote.Name = "cmdPrenote";
            this.cmdPrenote.Size = new System.Drawing.Size(200, 40);
            this.cmdPrenote.TabIndex = 31;
            this.cmdPrenote.Text = "Create Prenote File";
            this.cmdPrenote.Click += new System.EventHandler(this.cmdPrenote_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileName.Location = new System.Drawing.Point(175, 312);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(163, 40);
            this.txtFileName.TabIndex = 29;
            this.txtFileName.Text = "TWACHExp.txt";
            // 
            // T2KEffectiveEntryDate
            // 
            this.T2KEffectiveEntryDate.Location = new System.Drawing.Point(225, 30);
            this.T2KEffectiveEntryDate.MaxLength = 10;
            this.T2KEffectiveEntryDate.Name = "T2KEffectiveEntryDate";
            this.T2KEffectiveEntryDate.Size = new System.Drawing.Size(115, 22);
            this.T2KEffectiveEntryDate.TabIndex = 26;
            // 
            // framPayDate
            // 
            this.framPayDate.BackColor = System.Drawing.Color.White;
            this.framPayDate.Controls.Add(this.cboPayDateYear);
            this.framPayDate.Controls.Add(this.cboPayDate);
            this.framPayDate.Controls.Add(this.cboPayRunID);
            this.framPayDate.Controls.Add(this.lblDateSelect_3);
            this.framPayDate.Controls.Add(this.lblDateSelect_4);
            this.framPayDate.Location = new System.Drawing.Point(20, 90);
            this.framPayDate.Name = "framPayDate";
            this.framPayDate.Size = new System.Drawing.Size(318, 202);
            this.framPayDate.TabIndex = 20;
            this.framPayDate.Text = "Select Pay Date";
            // 
            // cboPayDateYear
            // 
            this.cboPayDateYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDateYear.Location = new System.Drawing.Point(20, 59);
            this.cboPayDateYear.Name = "cboPayDateYear";
            this.cboPayDateYear.Size = new System.Drawing.Size(168, 40);
            this.cboPayDateYear.TabIndex = 23;
            this.cboPayDateYear.SelectedIndexChanged += new System.EventHandler(this.cboPayDateYear_SelectedIndexChanged);
            // 
            // cboPayDate
            // 
            this.cboPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDate.Location = new System.Drawing.Point(20, 142);
            this.cboPayDate.Name = "cboPayDate";
            this.cboPayDate.Size = new System.Drawing.Size(168, 40);
            this.cboPayDate.TabIndex = 22;
            this.cboPayDate.SelectedIndexChanged += new System.EventHandler(this.cboPayDate_SelectedIndexChanged);
            // 
            // cboPayRunID
            // 
            this.cboPayRunID.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayRunID.Location = new System.Drawing.Point(208, 142);
            this.cboPayRunID.Name = "cboPayRunID";
            this.cboPayRunID.Size = new System.Drawing.Size(90, 40);
            this.cboPayRunID.TabIndex = 21;
            // 
            // lblDateSelect_3
            // 
            this.lblDateSelect_3.Location = new System.Drawing.Point(20, 30);
            this.lblDateSelect_3.Name = "lblDateSelect_3";
            this.lblDateSelect_3.Size = new System.Drawing.Size(80, 15);
            this.lblDateSelect_3.TabIndex = 25;
            this.lblDateSelect_3.Text = "SELECT YEAR";
            // 
            // lblDateSelect_4
            // 
            this.lblDateSelect_4.Location = new System.Drawing.Point(20, 113);
            this.lblDateSelect_4.Name = "lblDateSelect_4";
            this.lblDateSelect_4.Size = new System.Drawing.Size(181, 15);
            this.lblDateSelect_4.TabIndex = 24;
            this.lblDateSelect_4.Text = "SELECT PAY DATE AND RUN";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 326);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(92, 16);
            this.Label8.TabIndex = 28;
            this.Label8.Text = "ACH FILE NAME";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(140, 16);
            this.Label7.TabIndex = 27;
            this.Label7.Text = "EFFECTIVE ENTRY DATE";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelect,
            this.mnuCreate});
            this.MainMenu1.Name = null;
            // 
            // mnuSelect
            // 
            this.mnuSelect.Index = 0;
            this.mnuSelect.Name = "mnuSelect";
            this.mnuSelect.Text = "Select Paydate for ACH File";
            this.mnuSelect.Visible = false;
            this.mnuSelect.Click += new System.EventHandler(this.mnuSelect_Click);
            // 
            // mnuCreate
            // 
            this.mnuCreate.Enabled = false;
            this.mnuCreate.Index = 1;
            this.mnuCreate.Name = "mnuCreate";
            this.mnuCreate.Text = "Create ACH File";
            this.mnuCreate.Visible = false;
            this.mnuCreate.Click += new System.EventHandler(this.mnuCreate_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            this.mnuSepar2.Visible = false;
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Visible = false;
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Continue";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(321, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(178, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.Location = new System.Drawing.Point(474, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(190, 24);
            this.cmdSelect.TabIndex = 1;
            this.cmdSelect.Text = "Select Paydate for ACH File";
            this.cmdSelect.Visible = false;
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelect_Click);
            // 
            // cmdCreate
            // 
            this.cmdCreate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCreate.Location = new System.Drawing.Point(670, 29);
            this.cmdCreate.Name = "cmdCreate";
            this.cmdCreate.Size = new System.Drawing.Size(120, 24);
            this.cmdCreate.TabIndex = 2;
            this.cmdCreate.Text = "Create ACH File";
            this.cmdCreate.Visible = false;
            this.cmdCreate.Click += new System.EventHandler(this.mnuCreate_Click);
            // 
            // frmACHBankInformation
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(818, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu1;
            this.MinimizeBox = false;
            this.Name = "frmACHBankInformation";
            this.Text = "ACH Bank Information";
            this.Load += new System.EventHandler(this.frmACHBankInformation_Load);
            this.Activated += new System.EventHandler(this.frmACHBankInformation_Activated);
            this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmACHBankInformation_FormClosing);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmACHBankInformation_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTestFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framACH)).EndInit();
            this.framACH.ResumeLayout(false);
            this.framACH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrenote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayDate)).EndInit();
            this.framPayDate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdCreate;
		private FCButton cmdSelect;
	}
}
