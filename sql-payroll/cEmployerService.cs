//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEmployerService
	{
		//=========================================================
		public cEmployerRecord GetEmployerInfo()
		{
			cEmployerRecord GetEmployerInfo = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			cEmployerRecord empRecord = new cEmployerRecord();
			rsLoad.OpenRecordset("Select * from tblEmployerInfo", "Payroll");
			if (!rsLoad.EndOfFile())
			{
				empRecord.Address = FCConvert.ToString(rsLoad.Get_Fields("address1"));
				empRecord.City = FCConvert.ToString(rsLoad.Get_Fields_String("City"));
				empRecord.State = FCConvert.ToString(rsLoad.Get_Fields("State"));
				empRecord.Zip = FCConvert.ToString(rsLoad.Get_Fields("Zip"));
				empRecord.Zip4 = FCConvert.ToString(rsLoad.Get_Fields("zip4"));
				empRecord.EIN = FCConvert.ToString(rsLoad.Get_Fields_String("FederalEmployerID"));
				empRecord.Name = FCConvert.ToString(rsLoad.Get_Fields_String("EmployerName"));
				empRecord.Telephone = FCConvert.ToString(rsLoad.Get_Fields_String("TransmitterPhone"));
			}
			GetEmployerInfo = empRecord;
			return GetEmployerInfo;
		}
	}
}
