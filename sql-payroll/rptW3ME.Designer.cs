﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3ME.
	/// </summary>
	partial class rptW3ME
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW3ME));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaving1099 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtThirdPartyID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtThirdPartyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaving1099)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartyID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartyName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Barcode1,
            this.Label73,
            this.Shape20,
            this.Label124,
            this.txtName,
            this.txtName2,
            this.txtWithholdingAccount,
            this.txtLine1,
            this.txtLine2,
            this.txtDate,
            this.txtTitle,
            this.txtPhone,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label32,
            this.Label40,
            this.Label41,
            this.Line76,
            this.Label42,
            this.Label43,
            this.Label44,
            this.Label46,
            this.Label47,
            this.Label52,
            this.Label58,
            this.Label67,
            this.Label74,
            this.txtSaving1099,
            this.Line94,
            this.Line95,
            this.Line96,
            this.Label80,
            this.Label81,
            this.Label82,
            this.Label83,
            this.Label84,
            this.Label85,
            this.Label86,
            this.Label87,
            this.Label88,
            this.Label89,
            this.Label90,
            this.Label91,
            this.txtLine3,
            this.txtLine4,
            this.Label92,
            this.Label93,
            this.Label96,
            this.Label97,
            this.Label98,
            this.Label99,
            this.Label100,
            this.Label101,
            this.Label104,
            this.Label106,
            this.Label107,
            this.Label108,
            this.Label109,
            this.Label111,
            this.Label112,
            this.Label113,
            this.Label114,
            this.Label115,
            this.txtThirdPartyID,
            this.Label116,
            this.txtThirdPartyName,
            this.Label117,
            this.Label118,
            this.Label119,
            this.Line97,
            this.Label121,
            this.Label123,
            this.Line98,
            this.Label125,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Label126,
            this.Label127,
            this.Label128,
            this.Label129,
            this.Label130,
            this.Label131,
            this.Label132,
            this.Label133,
            this.Line99});
			this.Detail.Height = 10.02083F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Barcode1
			// 
			this.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
			this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.Barcode1.Height = 0.5F;
			this.Barcode1.Left = 5.666667F;
			this.Barcode1.Name = "Barcode1";
			this.Barcode1.QuietZoneBottom = 0F;
			this.Barcode1.QuietZoneLeft = 0F;
			this.Barcode1.QuietZoneRight = 0F;
			this.Barcode1.QuietZoneTop = 0F;
			this.Barcode1.Text = "11066V1";
			this.Barcode1.Top = 0.08333334F;
			this.Barcode1.Width = 1.583333F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1666667F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 7.249306F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: right" +
    "; vertical-align: top";
			this.Label73.Text = "15";
			this.Label73.Top = 0.1875F;
			this.Label73.Width = 0.25F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.9479167F;
			this.Shape20.Left = 0.014F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 3.516F;
			this.Shape20.Width = 7.461805F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.3958333F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-size: 8.5pt";
			this.Label124.Text = resources.GetString("Label124.Text");
			this.Label124.Top = 9.0625F;
			this.Label124.Width = 7.447917F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.05555556F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtName.Top = 2.020833F;
			this.txtName.Width = 3.104167F;
			// 
			// txtName2
			// 
			this.txtName2.Height = 0.1666667F;
			this.txtName2.Left = 0.05555556F;
			this.txtName2.Name = "txtName2";
			this.txtName2.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName2.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtName2.Top = 2.347222F;
			this.txtName2.Width = 3.125F;
			// 
			// txtWithholdingAccount
			// 
			this.txtWithholdingAccount.Height = 0.1666667F;
			this.txtWithholdingAccount.Left = 0.656F;
			this.txtWithholdingAccount.Name = "txtWithholdingAccount";
			this.txtWithholdingAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
			this.txtWithholdingAccount.Text = "99 999999999";
			this.txtWithholdingAccount.Top = 1.478F;
			this.txtWithholdingAccount.Width = 1.833333F;
			// 
			// txtLine1
			// 
			this.txtLine1.Height = 0.1666667F;
			this.txtLine1.Left = 5.822917F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtLine1.Text = "9999999999.99";
			this.txtLine1.Top = 1.197917F;
			this.txtLine1.Width = 1.5F;
			// 
			// txtLine2
			// 
			this.txtLine2.Height = 0.1666667F;
			this.txtLine2.Left = 5.826389F;
			this.txtLine2.Name = "txtLine2";
			this.txtLine2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtLine2.Text = "9999999999.99";
			this.txtLine2.Top = 1.534722F;
			this.txtLine2.Width = 1.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 0F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtDate.Text = "99-99-99";
			this.txtDate.Top = 3.072F;
			this.txtDate.Width = 0.75F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1666667F;
			this.txtTitle.Left = 3.687F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
			this.txtTitle.Text = "XXXXXXXXXXXXXXXXXXXXXX";
			this.txtTitle.Top = 3.071278F;
			this.txtTitle.Width = 2.416667F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1666667F;
			this.txtPhone.Left = 6.291167F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
			this.txtPhone.Text = "999-999-9999";
			this.txtPhone.Top = 3.071278F;
			this.txtPhone.Width = 1.125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.25F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.916667F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 11.5pt; font-weight: bold; text-align: center";
			this.Label26.Text = "MAINE REVENUE SERVICES";
			this.Label26.Top = 0F;
			this.Label26.Width = 3F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.25F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 1.916667F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label27.Text = "RECONCILIATION OF MAINE";
			this.Label27.Top = 0.1875F;
			this.Label27.Width = 3F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.25F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.9F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Arial\'; font-size: 16pt; font-weight: bold";
			this.Label28.Text = "2020";
			this.Label28.Top = 0.2708333F;
			this.Label28.Width = 0.6666667F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.25F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 1.75F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label29.Text = "INCOME TAX WITHHELD IN 2020";
			this.Label29.Top = 0.3958333F;
			this.Label29.Width = 3.416667F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1666667F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 7pt; vertical-align: middle";
			this.Label32.Text = "Date";
			this.Label32.Top = 3.27F;
			this.Label32.Width = 0.4166667F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1666667F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 6.291667F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 7pt; vertical-align: middle";
			this.Label40.Text = "Telephone";
			this.Label40.Top = 3.27F;
			this.Label40.Width = 0.5833333F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1666667F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 1.104167F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-size: 7pt; vertical-align: middle";
			this.Label41.Text = "Signature";
			this.Label41.Top = 3.27F;
			this.Label41.Width = 0.5833333F;
			// 
			// Line76
			// 
			this.Line76.Height = 0F;
			this.Line76.Left = 1.083F;
			this.Line76.LineWeight = 1F;
			this.Line76.Name = "Line76";
			this.Line76.Top = 3.232F;
			this.Line76.Width = 2.483334F;
			this.Line76.X1 = 1.083F;
			this.Line76.X2 = 3.566334F;
			this.Line76.Y1 = 3.232F;
			this.Line76.Y2 = 3.232F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1666667F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.666667F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-size: 7pt; vertical-align: middle";
			this.Label42.Text = "Title (Owner, President, Partner, member, etc.)";
			this.Label42.Top = 3.27F;
			this.Label42.Width = 2.208333F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.3333333F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 4.041667F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 7pt";
			this.Label43.Text = "Total Maine Income Tax withheld shown on payee statements";
			this.Label43.Top = 1.09375F;
			this.Label43.Width = 1.666667F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.25F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 4.041667F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-size: 7pt";
			this.Label44.Text = "Total Maine Withholding Tax reported for the year";
			this.Label44.Top = 1.427083F;
			this.Label44.Width = 1.416667F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1666667F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 3.791667F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-size: 7pt; text-align: right";
			this.Label46.Text = "1.";
			this.Label46.Top = 1.09375F;
			this.Label46.Width = 0.25F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1666667F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 3.791667F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-size: 7pt; text-align: right";
			this.Label47.Text = "2.";
			this.Label47.Top = 1.427083F;
			this.Label47.Width = 0.25F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1666667F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 0.05555556F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 7pt; vertical-align: middle";
			this.Label52.Text = "Name:";
			this.Label52.Top = 1.694444F;
			this.Label52.Width = 1.583333F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1666667F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 0.7F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt; font-weight: bold";
			this.Label58.Text = "FORM W-3ME";
			this.Label58.Top = 0F;
			this.Label58.Width = 1F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1666667F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 0.05555556F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-size: 7pt";
			this.Label67.Text = "Withholding Account Number:";
			this.Label67.Top = 1.197917F;
			this.Label67.Width = 1.416667F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1666667F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0.25F;
			this.Label74.MultiLine = false;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Courier New\'";
			this.Label74.Text = "TSC";
			this.Label74.Top = 9.854167F;
			this.Label74.Width = 0.4166667F;
			// 
			// txtSaving1099
			// 
			this.txtSaving1099.Height = 0.1666667F;
			this.txtSaving1099.Left = 4.06F;
			this.txtSaving1099.Name = "txtSaving1099";
			this.txtSaving1099.Style = "font-family: \'Courier New\'; text-align: left";
			this.txtSaving1099.Text = "X";
			this.txtSaving1099.Top = 0.7550001F;
			this.txtSaving1099.Width = 0.1666667F;
			// 
			// Line94
			// 
			this.Line94.Height = 0F;
			this.Line94.Left = 0.03125F;
			this.Line94.LineWeight = 1F;
			this.Line94.Name = "Line94";
			this.Line94.Top = 3.231722F;
			this.Line94.Width = 0.9520833F;
			this.Line94.X1 = 0.03125F;
			this.Line94.X2 = 0.9833333F;
			this.Line94.Y1 = 3.231722F;
			this.Line94.Y2 = 3.231722F;
			// 
			// Line95
			// 
			this.Line95.Height = 0F;
			this.Line95.Left = 3.687F;
			this.Line95.LineWeight = 1F;
			this.Line95.Name = "Line95";
			this.Line95.Top = 3.232F;
			this.Line95.Width = 2.416667F;
			this.Line95.X1 = 3.687F;
			this.Line95.X2 = 6.103667F;
			this.Line95.Y1 = 3.232F;
			this.Line95.Y2 = 3.232F;
			// 
			// Line96
			// 
			this.Line96.Height = 0F;
			this.Line96.Left = 6.312F;
			this.Line96.LineWeight = 1F;
			this.Line96.Name = "Line96";
			this.Line96.Top = 3.232F;
			this.Line96.Width = 1.180555F;
			this.Line96.X1 = 6.312F;
			this.Line96.X2 = 7.492555F;
			this.Line96.Y1 = 3.232F;
			this.Line96.Y2 = 3.232F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.5416667F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 0F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label80.Text = resources.GetString("Label80.Text");
			this.Label80.Top = 4.508F;
			this.Label80.Width = 7.447917F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.2083333F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 0F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label81.Text = "General Instructions";
			this.Label81.Top = 4.952F;
			this.Label81.Width = 7.4375F;
			// 
			// Label82
			// 
			this.Label82.Height = 1.096F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 0F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label82.Text = resources.GetString("Label82.Text");
			this.Label82.Top = 5.081F;
			this.Label82.Width = 7.4375F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1458333F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 0.1875F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-size: 6pt; font-weight: bold";
			this.Label83.Text = "When to File:";
			this.Label83.Top = 9.583333F;
			this.Label83.Width = 0.7604167F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1458333F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 3.611F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "font-size: 6pt; font-weight: bold";
			this.Label84.Text = "Where to File:";
			this.Label84.Top = 9.583F;
			this.Label84.Width = 0.9375F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1458333F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0.1875F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-size: 6pt; font-weight: bold";
			this.Label85.Text = "How to File:";
			this.Label85.Top = 9.708333F;
			this.Label85.Width = 0.9375F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1458333F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 0.875F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "font-size: 6pt";
			this.Label86.Text = "No later than March 1, 2020.";
			this.Label86.Top = 9.583333F;
			this.Label86.Width = 2F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1458333F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 4.36F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-size: 6pt";
			this.Label87.Text = "Mail by FIRST CLASS MAIL to:";
			this.Label87.Top = 9.583F;
			this.Label87.Width = 1.447917F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.3956674F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 5.823F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "font-size: 6pt; font-weight: bold";
			this.Label88.Text = "Maine Revenue Services\r\nP.O. Box 1064\r\nAugusta, ME 04332-1064";
			this.Label88.Top = 9.583333F;
			this.Label88.Width = 1.635333F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1458333F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0.875F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-size: 6pt";
			this.Label89.Text = "File Form W-3ME separately from your Form 941ME return.";
			this.Label89.Top = 9.708333F;
			this.Label89.Width = 6.3125F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1354167F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 1F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-size: 6pt; font-weight: bold; text-decoration: underline";
			this.Label90.Text = "DO NOT";
			this.Label90.Top = 9.854167F;
			this.Label90.Width = 0.5625F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1354167F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 1.5625F;
			this.Label91.MultiLine = false;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Arial\'; font-size: 6pt";
			this.Label91.Text = "include Form W-3ME in the same envelope with Form 941ME";
			this.Label91.Top = 9.854167F;
			this.Label91.Width = 5.833333F;
			// 
			// txtLine3
			// 
			this.txtLine3.Height = 0.1666667F;
			this.txtLine3.Left = 5.822917F;
			this.txtLine3.Name = "txtLine3";
			this.txtLine3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtLine3.Text = "9999999999.99";
			this.txtLine3.Top = 2.027778F;
			this.txtLine3.Width = 1.5F;
			// 
			// txtLine4
			// 
			this.txtLine4.Height = 0.1666667F;
			this.txtLine4.Left = 5.822917F;
			this.txtLine4.Name = "txtLine4";
			this.txtLine4.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtLine4.Text = "9999999999.99";
			this.txtLine4.Top = 2.368056F;
			this.txtLine4.Width = 1.5F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.375F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 4.041667F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "font-size: 7pt";
			this.Label92.Text = "Third-party payers of sick pay. (see instructions below). All others may skip thi" +
    "s line";
			this.Label92.Top = 1.802083F;
			this.Label92.Width = 1.416667F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1666667F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 3.791667F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-size: 7pt; text-align: right";
			this.Label93.Text = "3.";
			this.Label93.Top = 1.802083F;
			this.Label93.Width = 0.25F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.34375F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 4.041667F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-size: 7pt";
			this.Label96.Text = "Employers (see instructions below). All others may skip this line";
			this.Label96.Top = 2.197917F;
			this.Label96.Width = 1.510417F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1666667F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 3.791667F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-size: 7pt; text-align: right";
			this.Label97.Text = "4.";
			this.Label97.Top = 2.197917F;
			this.Label97.Width = 0.25F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1458333F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 1.146F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.Label98.Text = "www.maine.gov/revenue/netfile/gateway2.htm";
			this.Label98.Top = 4.766F;
			this.Label98.Width = 3.25F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.2083333F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label99.Text = "Specific Instructions";
			this.Label99.Top = 6.104167F;
			this.Label99.Width = 7.4375F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.2083333F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 0F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label100.Text = "Payee Statements";
			this.Label100.Top = 7.657001F;
			this.Label100.Width = 7.499306F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.2083333F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 0F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label101.Text = "Maine Revenue Services does not accept paper copies of payee statements (Forms W-" +
    "2 and 1099).";
			this.Label101.Top = 7.833333F;
			this.Label101.Width = 5.6875F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.3958333F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 0F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-size: 8.5pt";
			this.Label104.Text = resources.GetString("Label104.Text");
			this.Label104.Top = 8.291667F;
			this.Label104.Width = 7.25F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.2708333F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 0F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "font-size: 8.5pt";
			this.Label106.Text = resources.GetString("Label106.Text");
			this.Label106.Top = 8.75F;
			this.Label106.Width = 7.447917F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.2083333F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 0F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label107.Text = "Line 1.";
			this.Label107.Top = 6.4375F;
			this.Label107.Width = 0.9375F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.2083333F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 0F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label108.Text = "Line 2.";
			this.Label108.Top = 6.729167F;
			this.Label108.Width = 0.9375F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.2083333F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 0F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label109.Text = "Line 3.";
			this.Label109.Top = 7.006945F;
			this.Label109.Width = 0.9375F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.2708333F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 0.4375F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label111.Text = "Enter total Maine withholding reported for this account for for all quarters on F" +
    "orm 941ME.  If you amended one or more quarterly returns, include only the corre" +
    "cted amount.";
			this.Label111.Top = 6.729167F;
			this.Label111.Width = 7F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.2708333F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0.4375F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label112.Text = resources.GetString("Label112.Text");
			this.Label112.Top = 7.006945F;
			this.Label112.Width = 7F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.2083333F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label113.Text = "Line 4.";
			this.Label113.Top = 7.291667F;
			this.Label113.Width = 1.5625F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.2708333F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 0.4375F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label114.Text = "pay.Enter the amount of withholding remitted by the third party payer. Enter the " +
    "third party payer name and ID number in the spaces provided.";
			this.Label114.Top = 7.447917F;
			this.Label114.Width = 6.9375F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1458333F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 1.479167F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label115.Text = "Complete this line if you issue Forms W-2 that include withholding remitted to Ma" +
    "ine by a third party payer of sick";
			this.Label115.Top = 7.291667F;
			this.Label115.Width = 5.9375F;
			// 
			// txtThirdPartyID
			// 
			this.txtThirdPartyID.Height = 0.1666667F;
			this.txtThirdPartyID.Left = 5.729167F;
			this.txtThirdPartyID.Name = "txtThirdPartyID";
			this.txtThirdPartyID.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtThirdPartyID.Text = "999999999";
			this.txtThirdPartyID.Top = 2.673611F;
			this.txtThirdPartyID.Width = 1.041667F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0.06944445F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-size: 7pt";
			this.Label116.Text = "Third-party payer name";
			this.Label116.Top = 2.663194F;
			this.Label116.Width = 1.104167F;
			// 
			// txtThirdPartyName
			// 
			this.txtThirdPartyName.Height = 0.1666667F;
			this.txtThirdPartyName.Left = 1.083333F;
			this.txtThirdPartyName.Name = "txtThirdPartyName";
			this.txtThirdPartyName.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtThirdPartyName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtThirdPartyName.Top = 2.645833F;
			this.txtThirdPartyName.Width = 3.104167F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.2083333F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0.25F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-size: 9pt; font-weight: bold";
			this.Label117.Text = "Note:";
			this.Label117.Top = 3.601F;
			this.Label117.Width = 0.5208333F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.9583333F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0.625F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label118.Text = resources.GetString("Label118.Text");
			this.Label118.Top = 3.601F;
			this.Label118.Width = 6.8125F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.188F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 4.434F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-size: 8pt; ddo-char-set: 1";
			this.Label119.Text = "Check here if this is an AMENDED return";
			this.Label119.Top = 0.7550001F;
			this.Label119.Width = 2.292F;
			// 
			// Line97
			// 
			this.Line97.Height = 0F;
			this.Line97.Left = 1.1F;
			this.Line97.LineWeight = 1F;
			this.Line97.Name = "Line97";
			this.Line97.Top = 2.8125F;
			this.Line97.Width = 2.8F;
			this.Line97.X1 = 1.1F;
			this.Line97.X2 = 3.9F;
			this.Line97.Y1 = 2.8125F;
			this.Line97.Y2 = 2.8125F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 4.166667F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-size: 7pt";
			this.Label121.Text = "Third-party payer ID Number:";
			this.Label121.Top = 2.663194F;
			this.Label121.Width = 1.291667F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1458333F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 1.9375F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold";
			this.Label123.Text = "www.maine.gov/revenue";
			this.Label123.Top = 9.3125F;
			this.Label123.Width = 1.9375F;
			// 
			// Line98
			// 
			this.Line98.Height = 0F;
			this.Line98.Left = 0F;
			this.Line98.LineColor = System.Drawing.Color.FromArgb(192, 192, 192);
			this.Line98.LineWeight = 3F;
			this.Line98.Name = "Line98";
			this.Line98.Top = 9.541667F;
			this.Line98.Width = 7.499306F;
			this.Line98.X1 = 0F;
			this.Line98.X2 = 7.499306F;
			this.Line98.Y1 = 9.541667F;
			this.Line98.Y2 = 9.541667F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.2708333F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 0.4375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label125.Text = "Enter total Maine withholding reported on payee statements issued under this with" +
    "holding account number (e.g. box 17 of federal Form W-2).";
			this.Label125.Top = 6.4375F;
			this.Label125.Width = 7F;
			// 
			// Shape17
			// 
			this.Shape17.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape17.Height = 0.125F;
			this.Shape17.Left = 0.01388889F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 0.01388889F;
			this.Shape17.Width = 0.125F;
			// 
			// Shape18
			// 
			this.Shape18.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape18.Height = 0.125F;
			this.Shape18.Left = 0F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 9.854167F;
			this.Shape18.Width = 0.125F;
			// 
			// Shape19
			// 
			this.Shape19.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape19.Height = 0.125F;
			this.Shape19.Left = 7.361111F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 9.854167F;
			this.Shape19.Width = 0.125F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1666667F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.625F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 7pt; vertical-align: middle";
			this.Label126.Text = "Under penalties of perjury, I certify that the information contained on this retu" +
    "rn, report and attachment(s) is true and correct.";
			this.Label126.Top = 2.822917F;
			this.Label126.Width = 5.458333F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.2291667F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 2.4375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-size: 9pt; vertical-align: middle";
			this.Label127.Text = "Due March 1, 2021";
			this.Label127.Top = 0.5625F;
			this.Label127.Width = 1.520833F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.2083333F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 0.4375F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-size: 8.5pt; font-weight: bold; text-decoration: underline";
			this.Label128.Text = "Employers Only:";
			this.Label128.Top = 7.291667F;
			this.Label128.Width = 1.5625F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.2083333F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label129.Text = "Entity Information.";
			this.Label129.Top = 6.25F;
			this.Label129.Width = 1.5625F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.2708333F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 1.1875F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label130.Text = "Enter the company\'s withholding account number, name and address in the space pro" +
    "vided.";
			this.Label130.Top = 6.25F;
			this.Label130.Width = 5.25F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1458333F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 3.375F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt";
			this.Label131.Text = "(select Electronic Services)";
			this.Label131.Top = 9.3125F;
			this.Label131.Width = 1.9375F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.2708333F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label132.Text = resources.GetString("Label132.Text");
			this.Label132.Top = 7.979167F;
			this.Label132.Width = 7.375F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.4791667F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-size: 8.5pt; vertical-align: middle";
			this.Label133.Text = "Note: Electronic copies of payee statements must be filed with Maine Revenue Serv" +
    "ices no later than January 31. See instructions below.";
			this.Label133.Top = 0.5625F;
			this.Label133.Width = 2.583333F;
			// 
			// Line99
			// 
			this.Line99.Height = 0F;
			this.Line99.Left = 0F;
			this.Line99.LineWeight = 1F;
			this.Line99.Name = "Line99";
			this.Line99.Top = 1.034722F;
			this.Line99.Width = 7.499306F;
			this.Line99.X1 = 0F;
			this.Line99.X2 = 7.499306F;
			this.Line99.Y1 = 1.034722F;
			this.Line99.Y2 = 1.034722F;
			// 
			// rptW3ME
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaving1099)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartyID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtThirdPartyName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaving1099;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line94;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line95;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line96;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThirdPartyID;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThirdPartyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line98;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line99;
    }
}
