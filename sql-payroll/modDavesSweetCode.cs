//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modDavesSweetCode
	{
		//=========================================================
		// vbPorter upgrade warning: 'Return' As bool	OnWrite(int, bool)
		public static bool LockWarrant()
		{
			bool LockWarrant = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
				{
					rsTemp.Edit();
					rsTemp.Set_Fields("MasterLock", true);
					rsTemp.Set_Fields("OpID", modGlobalVariables.Statics.gstrUser);
					if (rsTemp.Update(false))
					{
						LockWarrant = FCConvert.ToBoolean(1);
					}
					else
					{
						LockWarrant = false;
					}
				}
				else
				{
					LockWarrant = false;
					modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
					rsTemp.Reset();
					return LockWarrant;
				}
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("MasterLock", true);
				rsTemp.Set_Fields("OpID", modGlobalVariables.Statics.gstrUser);
				if (rsTemp.Update(false))
				{
					LockWarrant = FCConvert.ToBoolean(1);
				}
				else
				{
					LockWarrant = false;
				}
			}
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != modGlobalVariables.Statics.gstrUser)
			{
				LockWarrant = false;
				modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
			}
			rsTemp.Reset();
			return LockWarrant;
		}

		public static void UnlockWarrant()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			rsTemp.Edit();
			rsTemp.Set_Fields("MasterLock", false);
			rsTemp.Set_Fields("OpID", "");
			rsTemp.Update(true);
			rsTemp.Reset();
		}
		// vbPorter upgrade warning: Newdate As string	OnWrite(DateTime, string)
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
		public static DateTime FindLastDay(string Newdate)
		{
			DateTime FindLastDay = System.DateTime.Now;
			// vbPorter upgrade warning: xdate As object	OnWrite(DateTime)
			object xdate;
			xdate = (fecherFoundation.DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(Newdate)));
			xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, (DateTime)xdate);
			FindLastDay = FCConvert.ToDateTime(Strings.Format(xdate, "MM/dd/yyyy"));
			return FindLastDay;
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
		public static DateTime FindFirstDay(string textdate)
		{
			DateTime FindFirstDay = System.DateTime.Now;
			// vbPorter upgrade warning: xdate As object	OnWrite(string)
			object xdate;
			xdate = textdate;
			FindFirstDay = FCConvert.ToDateTime(Strings.Format(FCConvert.ToString((Convert.ToDateTime(xdate).Month)) + "/01/" + FCConvert.ToString((Convert.ToDateTime(xdate).Year)), "MM/dd/yyyy"));
			return FindFirstDay;
		}
		// vbPorter upgrade warning: curYearToDateAmount As Decimal	OnWrite(double, Decimal)
		// vbPorter upgrade warning: curCurrentAmount As Decimal	OnWrite(double, Decimal)
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(int, Decimal)
		public static Decimal CalculateExcess(Decimal curYearToDateAmount, Decimal curCurrentAmount, Decimal curLimit)
		{
			Decimal CalculateExcess = 0;
			Decimal curBegBalance;
			if (curYearToDateAmount < curLimit)
			{
				CalculateExcess = 0;
			}
			else
			{
				curBegBalance = curYearToDateAmount - curCurrentAmount;
				if (curBegBalance >= curLimit)
				{
					CalculateExcess = curCurrentAmount;
				}
				else
				{
					CalculateExcess = curYearToDateAmount - curLimit;
				}
			}
			return CalculateExcess;
		}
		// Public Function GetBankVariable(ByVal strName As String)
		// Dim rsVariables As New clsDRWrapper
		// Dim ff As New FCFCFileSystem
		//
		// On Error GoTo ErrorHandler
		//
		// rsVariables.Reset
		// Call rsVariables.OpenRecordset("SELECT * FROM BankIndex", "CentralData", dbOpenDynaset, 0, dbOptimistic)
		//
		// If rsVariables.EndOfFile Then
		// GetBankVariable = vbNullString
		// Else
		// GetBankVariable = rsVariables.Fields(strName)
		// End If
		//
		// If IsNull(GetBankVariable) Then GetBankVariable = ""
		// Exit Function
		//
		// ErrorHandler:
		// MsgBox "Error " & Err.Number & vbNewLine & vbNewLine & Err.Description, vbCritical, "Errors Encountered"
		// End Function
		public static bool JobComplete(string X)
		{
			bool JobComplete = false;
			foreach (FCForm ff in FCGlobal.Statics.Forms)
			{
				if (ff.Name == X)
				{
					JobComplete = false;
					return JobComplete;
				}
			}
			JobComplete = true;
			System.Threading.Thread.Sleep(2000);
			return JobComplete;
		}

		public static int GetPayrollRunWarrantNumber(DateTime datPayDate, int lngPayRun)
		{
			int GetPayrollRunWarrantNumber = 0;
			clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			rsWarrantInfo.OpenRecordset("SELECT * FROM tblPayrollSteps WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayRun) + " AND Checks = 1");
			if (rsWarrantInfo.EndOfFile() != true && rsWarrantInfo.BeginningOfFile() != true)
			{
				GetPayrollRunWarrantNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsWarrantInfo.Get_Fields_Int32("WarrantNumber"))));
			}
			else
			{
				GetPayrollRunWarrantNumber = 0;
			}
			return GetPayrollRunWarrantNumber;
		}
		// Public Function MonthCalc(ByVal X As Integer) As String
		// Select Case X
		// Case 1
		// MonthCalc = "January"
		// Case 2
		// MonthCalc = "February"
		// Case 3
		// MonthCalc = "March"
		// Case 4
		// MonthCalc = "April"
		// Case 5
		// MonthCalc = "May"
		// Case 6
		// MonthCalc = "June"
		// Case 7
		// MonthCalc = "July"
		// Case 8
		// MonthCalc = "August"
		// Case 9
		// MonthCalc = "September"
		// Case 10
		// MonthCalc = "October"
		// Case 11
		// MonthCalc = "November"
		// Case 12
		// MonthCalc = "December"
		// End Select
		// End Function
		// Public Function GetExpense(x As String) As String
		// If Not ExpDivFlag Then
		// GetExpense = Mid(x, 5 + Val(Left(Exp, 2)) + Val(Mid(Exp, 3, 2)), Val(Mid(Exp, 5, 2)))
		// Else
		// GetExpense = Mid(x, 4 + Val(Left(Exp, 2)) + Val(Mid(Exp, 3, 2)), Val(Mid(Exp, 5, 2)))
		// End If
		// End Function
		// Public Function GetDepartment(x As String) As String
		// GetDepartment = Mid(x, 3, Val(Left(Exp, 2)))
		// End Function
		// Public Function GetExpDivision(x As String) As String
		// GetExpDivision = Mid(x, 4 + Val(Left(Exp, 2)), Val(Mid(Exp, 3, 2)))
		// End Function
		// Public Function GetObject(x As String) As String
		// If Not ExpDivFlag Then
		// GetObject = Mid(x, 6 + Val(Left(Exp, 2)) + Val(Mid(Exp, 3, 2)) + Val(Mid(Exp, 5, 2)), Val(Mid(Exp, 7, 2)))
		// Else
		// GetObject = Mid(x, 5 + Val(Left(Exp, 2)) + Val(Mid(Exp, 3, 2)) + Val(Mid(Exp, 5, 2)), Val(Mid(Exp, 7, 2)))
		// End If
		// End Function
		public static void UpdateReportStatus(string strField)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.OpenRecordset("SELECT * FROM tblPayrollSteps WHERE PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun));
			if (rsUpdate.EndOfFile() != true && rsUpdate.BeginningOfFile() != true)
			{
				rsUpdate.Edit();
				if (strField == "FTD940")
				{
					rsUpdate.Set_Fields("FTD940", true);
				}
				else if (strField == "FTD941")
				{
					rsUpdate.Set_Fields("FTD941", true);
				}
				else if (strField == "WAGE941")
				{
					rsUpdate.Set_Fields("WAGE941", true);
				}
				else if (strField == "MSRS")
				{
					rsUpdate.Set_Fields("MSRS", true);
				}
				else if (strField == "MMA")
				{
					rsUpdate.Set_Fields("MMA", true);
				}
				else if (strField == "NonMMA")
				{
					rsUpdate.Set_Fields("NonMMA", true);
				}
				else if (strField == "StateUnemployment")
				{
					rsUpdate.Set_Fields("StateUnemployment", true);
				}
				else if (strField == "FUTA")
				{
					rsUpdate.Set_Fields("FUTA", true);
				}
				rsUpdate.Update();
			}
		}

		public static string GetAccountDescription(string Acct)
		{
			string GetAccountDescription = "";
			// If InStr(2, Acct, "_") = 0 Or Left$(Acct, 1) = "M" Then
			// Select Case Left$(Acct, 1)
			// Case "E"
			// GetAccountDescription = GetExpenseName(GetExpense(Acct))
			// Case "G"
			// GetAccountDescription = GetLedger(Acct)
			// GetAccountDescription = GetFundName(GetFund(Acct))
			// Case "R"
			// GetAccountDescription = GetRev(Acct)
			// Case Else
			// GetAccountDescription = ""
			// End Select
			// Else
			// GetAccountDescription = ""
			// End If
			GetAccountDescription = modAccountTitle.ReturnAccountDescription(Acct);
			return GetAccountDescription;
		}

		public class StaticVariables
		{
			public bool blnReportCompleted;
			public bool blnReportStarted;
			public bool blnDataEntryCompleted;
			public bool blnWarrantCompleted;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
