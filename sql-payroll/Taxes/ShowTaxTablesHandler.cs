﻿using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.Payroll;

namespace TWPY0000
{
    public class ShowTaxTablesHandler : CommandHandler<ShowTaxTables>
    {
        private IView<ITaxTableViewModel> view;
        public ShowTaxTablesHandler(IView<ITaxTableViewModel> view)
        {
            this.view = view;
        }

        protected override void Handle(ShowTaxTables command)
        {
            view.Show();
        }
    }
}