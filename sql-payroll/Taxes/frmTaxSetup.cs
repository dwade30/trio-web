//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Payroll;

namespace TWPY0000
{
	public partial class frmTaxSetup : BaseForm, IView<ITaxTableViewModel>
	{
		public frmTaxSetup()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

        public frmTaxSetup(ITaxTableViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
            this.Resize += frmTaxSetup_Resize;
            this.cmbTaxYear.SelectedIndexChanged += CmbTaxYear_SelectedIndexChanged;
            this.cmbFederalTables.SelectedIndexChanged += CmbFederalTables_SelectedIndexChanged;
            this.cmbStateTables.SelectedIndexChanged += CmbStateTables_SelectedIndexChanged;
		}

        private void CmbStateTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxYear.SelectedIndex >= 0)
            {
                var groups = ViewModel.GetGroupForYear(cmbTaxYear.Text.ToIntegerValue());
                var stateGroup = groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == "US_ME");
                var selectedItem = (GenericDescriptionPair<string>) cmbStateTables.SelectedItem;
                TaxTable table = null;
                if (stateGroup != null && selectedItem != null)
                {
                    table = stateGroup.TaxTables.FirstOrDefault(t => t.TableName == selectedItem.ID);
                }
                FillStateTable(table);
            }
        }

        private void CmbFederalTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxYear.SelectedIndex >= 0)
            {
                var groups = ViewModel.GetGroupForYear(cmbTaxYear.Text.ToIntegerValue());
                var fedGroup = groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == "US");
                var selectedItem = (GenericDescriptionPair<string>)cmbFederalTables.SelectedItem;
                TaxTable table = null;
                if (selectedItem != null && fedGroup != null)
                {
                    table = fedGroup.TaxTables.FirstOrDefault(t => t.TableName == selectedItem.ID);
                }
                FillFederalTable(table);
            }
        }

        private void CmbTaxYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxYear.SelectedIndex >= 0)
            {
                var year = cmbTaxYear.Text.ToIntegerValue();
                LoadForYear(year);
            }
        }

        private void frmTaxSetup_Resize(object sender, EventArgs e)
        {
            ResizeTableGrids();
        }

        private void ResizeTableGrids()
        {
            ResizeFederalGrid();
            ResizeStateGrid();
        }

        private void ResizeFederalGrid()
        {
            FederalTaxTableGrid.ColWidth((int)TaxColumns.MinAmount, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.185));
            FederalTaxTableGrid.ColWidth((int)TaxColumns.MaxAmount, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.185));
            FederalTaxTableGrid.ColWidth((int)TaxColumns.BaseAmount, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.185));
            FederalTaxTableGrid.ColWidth((int)TaxColumns.Symbol, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.05));
            FederalTaxTableGrid.ColWidth((int)TaxColumns.Rate, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.185));
            FederalTaxTableGrid.ColWidth((int)TaxColumns.PayOver, FCConvert.ToInt32(FederalTaxTableGrid.WidthOriginal * 0.185));
        }
        private void ResizeStateGrid()
        {
            StateTaxTableGrid.ColWidth((int)TaxColumns.MinAmount, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.185));
            StateTaxTableGrid.ColWidth((int)TaxColumns.MaxAmount, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.185));
            StateTaxTableGrid.ColWidth((int)TaxColumns.BaseAmount, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.185));
            StateTaxTableGrid.ColWidth((int)TaxColumns.Symbol, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.05));
            StateTaxTableGrid.ColWidth((int)TaxColumns.Rate, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.185));
            StateTaxTableGrid.ColWidth((int)TaxColumns.PayOver, FCConvert.ToInt32(StateTaxTableGrid.WidthOriginal * 0.185));
        }
		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				Close();
			}
			catch (Exception ex)
			{
            }
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

        private void LoadForYear(int year)
        {
            var groups = ViewModel.GetGroupForYear(year);
            if (groups != null)
            {
                LoadFederal(groups);
			}
			
        }

        private void LoadFederal(TaxGroups groups)
        {
            LoadFederalSetup(groups);
            LoadFederalTables(groups);
            LoadStateSetup(groups);
            LoadStateTables(groups);
        }

        private void LoadFederalSetup(TaxGroups groups)
		{
            var federalLocality = new PayrollLocality("US");
            var fedGroup = groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == federalLocality.FullLocalityName);
            if (fedGroup != null)
            {
                var amountResult = fedGroup.TaxSetup.GetAmount("StandardDeduction");
                lblFedStandardDeduction.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "SingleDeduction");
                lblFedSingleStandardDeduction.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "MarriedDeduction");
                lblFedMarriedStandardDeduction.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "FicaMaximumWage");
                lblFicaMaxWage.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "FicaEmployeeRate");
                lblFicaPercentage.Text = (amountResult.Amount * 100).ToString();
                amountResult = fedGroup.TaxSetup.GetAmount( "FicaEmployerRate");
                lblFicaEmployerPercentage.Text = (amountResult.Amount * 100).ToString();
                amountResult = fedGroup.TaxSetup.GetAmount( "MedicareEmployeeRate");
                lblMedicarePercentage.Text = (amountResult.Amount * 100).ToString();
                amountResult = fedGroup.TaxSetup.GetAmount( "MedicareEmployerRate");
                lblMedicareEmployerPercentage.Text = (amountResult.Amount * 100).ToString();
                amountResult = fedGroup.TaxSetup.GetAmount( "MedicareThreshold");
                lblMedicareThreshold.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "MedicareThresholdRate");
                lblMedicareThresholdRate.Text = (amountResult.Amount * 100).ToString();
                amountResult = fedGroup.TaxSetup.GetAmount( "MedicareMaximumWage");
                lblMedicareMaxWage.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "QualifyingChildrenCredit");
                lblFedQualifyingChildren.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = fedGroup.TaxSetup.GetAmount( "OtherDependentsCredit");
                lblFedOtherDependents.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
            }
        }

        private void LoadFederalTables(TaxGroups groups)
        {
            FederalTaxTableGrid.Rows = 1;
            FederalTaxTableGrid.TextMatrix(0, (int) TaxColumns.MinAmount, "Annual pay is over");
            FederalTaxTableGrid.TextMatrix(0, (int) TaxColumns.MaxAmount, "But not over");
            FederalTaxTableGrid.TextMatrix(0, (int) TaxColumns.BaseAmount, "Amount");
            FederalTaxTableGrid.TextMatrix(0, (int) TaxColumns.Rate, "Percent %");
            FederalTaxTableGrid.TextMatrix(0, (int) TaxColumns.PayOver, "of annual pay over");
            var federalLocality = new PayrollLocality("US");
            var fedGroup = groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == federalLocality.FullLocalityName);
            
            cmbFederalTables.Items.Clear();
            if (fedGroup != null && fedGroup.TaxTables.Any())
            {
                foreach (var taxTable in fedGroup.TaxTables)
                {
                    cmbFederalTables.Items.Add(new GenericDescriptionPair<string>()
                        {Description = taxTable.TableDescription, ID = taxTable.TableName});
                }

                cmbFederalTables.SelectedIndex = 0;
            }

        }

        private void FillFederalTable(TaxTable table)
        {
            FederalTaxTableGrid.Rows = 1;
            if (table != null)
            {
                var entries = table.Entries.OrderBy(t => t.MinimumAmount);
                foreach (var entry in entries)
                {
                    FederalTaxTableGrid.Rows++;
                    var row = FederalTaxTableGrid.Rows - 1;
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.MinAmount,
                        entry.MinimumAmount.FormatAsCurrencyNoSymbol());
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.MaxAmount,
                        entry.MaximumAmount.FormatAsCurrencyNoSymbol());
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.BaseAmount, entry.TaxAmount.FormatAsCurrencyNoSymbol());
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.Symbol, "+");
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.Rate,
                        (entry.TaxPercentage * 100).ToString());
                    FederalTaxTableGrid.TextMatrix(row, (int) TaxColumns.PayOver,
                        entry.MinimumAmount.FormatAsCurrencyNoSymbol());
                }
            }
        }

        private void LoadStateSetup(TaxGroups groups)
        {
            var stateLocality = new PayrollLocality("US","ME");
            var stateGroup =
                groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == stateLocality.FullLocalityName);
            if (stateGroup != null)
            {
                var amountResult = stateGroup.TaxSetup.GetAmount("WithholdingAllowance");

                lblStateWithholdingAllowance.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = stateGroup.TaxSetup.GetAmount("SingleDeduction");
                lblStateSingleDeduction.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = stateGroup.TaxSetup.GetAmount("SingleFullDeductionWageLimit");
                lblStateSingleWageLimit.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = stateGroup.TaxSetup.GetAmount("SingleDeductionWageCutoff");
                lblStateSingleCutoff.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();

                amountResult = stateGroup.TaxSetup.GetAmount("MarriedDeduction");
                lblStateMarriedDeduction.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = stateGroup.TaxSetup.GetAmount("MarriedFullDeductionWageLimit");
                lblStateMarriedWageLimit.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
                amountResult = stateGroup.TaxSetup.GetAmount("MarriedDeductionWageCutoff");
                lblStateMarriedCutoff.Text = amountResult.Amount.FormatAsCurrencyNoSymbol();
            }
        }

        private void LoadStateTables(TaxGroups groups)
        {
            StateTaxTableGrid.Rows = 1;
            StateTaxTableGrid.TextMatrix(0, (int)TaxColumns.MinAmount, "Annual pay is over");
            StateTaxTableGrid.TextMatrix(0, (int)TaxColumns.MaxAmount, "But not over");
            StateTaxTableGrid.TextMatrix(0, (int)TaxColumns.BaseAmount, "Amount");
            StateTaxTableGrid.TextMatrix(0, (int)TaxColumns.Rate, "Percent %");
            StateTaxTableGrid.TextMatrix(0, (int)TaxColumns.PayOver, "of annual pay over");
            var stateLocality = new PayrollLocality("US","ME");
            var stateGroup =
                groups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == stateLocality.FullLocalityName);
            cmbStateTables.Items.Clear();
            if (stateGroup != null && stateGroup.TaxTables.Any())
            {
                foreach (var taxTable in stateGroup.TaxTables)
                {
                    cmbStateTables.Items.Add(new GenericDescriptionPair<string>()
                        {Description = taxTable.TableDescription, ID = taxTable.TableName});
                }

                cmbStateTables.SelectedIndex = 0;
            }
        }

        private void FillStateTable(TaxTable table)
        {
            StateTaxTableGrid.Rows = 1;
            if (table != null)
            {
                var entries = table.Entries.OrderBy(t => t.MinimumAmount);
                foreach (var entry in entries)
                {
                    StateTaxTableGrid.Rows++;
                    var row = StateTaxTableGrid.Rows - 1;
                    StateTaxTableGrid.TextMatrix(row,(int) TaxColumns.MinAmount, entry.MinimumAmount.FormatAsCurrencyNoSymbol());
                    StateTaxTableGrid.TextMatrix(row, (int) TaxColumns.MaxAmount,
                        entry.MaximumAmount.FormatAsCurrencyNoSymbol());
                    StateTaxTableGrid.TextMatrix(row, (int) TaxColumns.BaseAmount,
                        entry.TaxAmount.FormatAsCurrencyNoSymbol());
                    StateTaxTableGrid.TextMatrix(row, (int) TaxColumns.Symbol, "+");
                    StateTaxTableGrid.TextMatrix(row, (int) TaxColumns.Rate, (entry.TaxPercentage * 100).ToString());
                    StateTaxTableGrid.TextMatrix(row, (int) TaxColumns.PayOver,
                        entry.MinimumAmount.FormatAsCurrencyNoSymbol());
                }
            }
        }

		private void frmTaxSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTaxSetup_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, 0);
            FillYearCombo();
        }

        private void FillYearCombo()
        {
            var years = ViewModel.GetAvailableYears().OrderByDescending(y => y);
            cmbTaxYear.Clear();
            foreach (var year in years)
            {
                cmbTaxYear.AddItem(year.ToString());
            }

            if (years.Any())
            {
                cmbTaxYear.SelectedIndex = 0;
            }
        }
  

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		

        public ITaxTableViewModel ViewModel { get; set; }

        private void vsTaxTables_Click(object sender, EventArgs e)
        {

        }

        private enum TaxColumns
        {
            MinAmount = 0,
            MaxAmount = 1,
            BaseAmount = 2,
            Symbol = 3,
            Rate = 4,
            PayOver = 5
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void fcLabel16_Click(object sender, EventArgs e)
        {

        }

        private void lblStateWithholdingAllowance_Click(object sender, EventArgs e)
        {

        }

        private void fcGrid1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void fcLabel12_Click(object sender, EventArgs e)
        {

        }

        private void tabPayStatuses_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ClientArea_PanelCollapsed(object sender, EventArgs e)
        {

        }

        private void FederalTaxTablesPanel_PanelCollapsed(object sender, EventArgs e)
        {

        }
    }
}
