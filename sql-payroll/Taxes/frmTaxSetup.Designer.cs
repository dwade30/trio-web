//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmTaxSetup.
	/// </summary>
	partial class frmTaxSetup
	{
		public fecherFoundation.FCTabControl tabPayStatuses;
		public fecherFoundation.FCTabPage tabPayStatuses_Page1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTabPage tabPayStatuses_Page2;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
            if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.tabPayStatuses = new fecherFoundation.FCTabControl();
            this.tabPayStatuses_Page1 = new fecherFoundation.FCTabPage();
            this.federalAccordian = new Wisej.Web.Accordion();
            this.FederalTaxTablesPanel = new Wisej.Web.AccordionPanel();
            this.cmbFederalTables = new Wisej.Web.ComboBox();
            this.FederalTaxTableGrid = new fecherFoundation.FCGrid();
            this.fcLabel11 = new fecherFoundation.FCLabel();
            this.FederalDeductionsPanel = new Wisej.Web.AccordionPanel();
            this.fcFrame2 = new fecherFoundation.FCFrame();
            this.lblFedOtherDependents = new fecherFoundation.FCLabel();
            this.lblFedQualifyingChildren = new fecherFoundation.FCLabel();
            this.fcLabel8 = new fecherFoundation.FCLabel();
            this.fcLabel9 = new fecherFoundation.FCLabel();
            this.fcFrame1 = new fecherFoundation.FCFrame();
            this.lblMedicareThresholdRate = new fecherFoundation.FCLabel();
            this.lblMedicareThreshold = new fecherFoundation.FCLabel();
            this.lblMedicareEmployerPercentage = new fecherFoundation.FCLabel();
            this.lblMedicarePercentage = new fecherFoundation.FCLabel();
            this.lblMedicareMaxWage = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.fcLabel7 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.lblFicaEmployerPercentage = new fecherFoundation.FCLabel();
            this.lblFicaPercentage = new fecherFoundation.FCLabel();
            this.lblFicaMaxWage = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblFedStandardDeduction = new fecherFoundation.FCLabel();
            this.lblFedMarriedStandardDeduction = new fecherFoundation.FCLabel();
            this.lblFedSingleStandardDeduction = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.tabPayStatuses_Page2 = new fecherFoundation.FCTabPage();
            this.stateAccordian = new Wisej.Web.Accordion();
            this.stateTaxTablePanel = new Wisej.Web.AccordionPanel();
            this.cmbStateTables = new Wisej.Web.ComboBox();
            this.StateTaxTableGrid = new fecherFoundation.FCGrid();
            this.fcLabel12 = new fecherFoundation.FCLabel();
            this.stateDeductionPanel = new Wisej.Web.AccordionPanel();
            this.fcFrame4 = new fecherFoundation.FCFrame();
            this.lblStateMarriedCutoff = new fecherFoundation.FCLabel();
            this.lblStateMarriedWageLimit = new fecherFoundation.FCLabel();
            this.lblStateMarriedDeduction = new fecherFoundation.FCLabel();
            this.fcLabel20 = new fecherFoundation.FCLabel();
            this.fcLabel21 = new fecherFoundation.FCLabel();
            this.fcLabel22 = new fecherFoundation.FCLabel();
            this.fcFrame3 = new fecherFoundation.FCFrame();
            this.fcLabel14 = new fecherFoundation.FCLabel();
            this.lblStateSingleCutoff = new fecherFoundation.FCLabel();
            this.lblStateSingleWageLimit = new fecherFoundation.FCLabel();
            this.lblStateSingleDeduction = new fecherFoundation.FCLabel();
            this.fcLabel17 = new fecherFoundation.FCLabel();
            this.fcLabel18 = new fecherFoundation.FCLabel();
            this.fcLabel19 = new fecherFoundation.FCLabel();
            this.lblStateWithholdingAllowance = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmbTaxYear = new fecherFoundation.FCComboBox();
            this.fcLabel10 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.tabPayStatuses.SuspendLayout();
            this.tabPayStatuses_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.federalAccordian)).BeginInit();
            this.federalAccordian.SuspendLayout();
            this.FederalTaxTablesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FederalTaxTableGrid)).BeginInit();
            this.FederalDeductionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame2)).BeginInit();
            this.fcFrame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).BeginInit();
            this.fcFrame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            this.tabPayStatuses_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateAccordian)).BeginInit();
            this.stateAccordian.SuspendLayout();
            this.stateTaxTablePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StateTaxTableGrid)).BeginInit();
            this.stateDeductionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame4)).BeginInit();
            this.fcFrame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame3)).BeginInit();
            this.fcFrame3.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(3, 587);
            this.BottomPanel.Size = new System.Drawing.Size(812, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.fcLabel10);
            this.ClientArea.Controls.Add(this.cmbTaxYear);
            this.ClientArea.Controls.Add(this.tabPayStatuses);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(832, 606);
            this.ClientArea.PanelCollapsed += new System.EventHandler(this.ClientArea_PanelCollapsed);
            this.ClientArea.Controls.SetChildIndex(this.tabPayStatuses, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbTaxYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel10, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(832, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(123, 28);
            this.HeaderText.Text = "Tax Tables";
            // 
            // tabPayStatuses
            // 
            this.tabPayStatuses.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.tabPayStatuses.BorderStyle = Wisej.Web.BorderStyle.None;
            this.tabPayStatuses.Controls.Add(this.tabPayStatuses_Page1);
            this.tabPayStatuses.Controls.Add(this.tabPayStatuses_Page2);
            this.tabPayStatuses.Location = new System.Drawing.Point(30, 52);
            this.tabPayStatuses.Name = "tabPayStatuses";
            this.tabPayStatuses.PageInsets = new Wisej.Web.Padding(0, 35, 0, 0);
            this.tabPayStatuses.Size = new System.Drawing.Size(779, 535);
            this.tabPayStatuses.TabIndex = 13;
            this.tabPayStatuses.Text = "Federal";
            this.tabPayStatuses.SelectedIndexChanged += new System.EventHandler(this.tabPayStatuses_SelectedIndexChanged);
            // 
            // tabPayStatuses_Page1
            // 
            this.tabPayStatuses_Page1.Controls.Add(this.federalAccordian);
            this.tabPayStatuses_Page1.Location = new System.Drawing.Point(0, 35);
            this.tabPayStatuses_Page1.Name = "tabPayStatuses_Page1";
            this.tabPayStatuses_Page1.Size = new System.Drawing.Size(779, 500);
            this.tabPayStatuses_Page1.Text = "Federal";
            // 
            // federalAccordian
            // 
            this.federalAccordian.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.federalAccordian.BorderStyle = Wisej.Web.BorderStyle.None;
            this.federalAccordian.Collapsible = true;
            this.federalAccordian.Controls.Add(this.FederalTaxTablesPanel);
            this.federalAccordian.Controls.Add(this.FederalDeductionsPanel);
            this.federalAccordian.Location = new System.Drawing.Point(1, 6);
            this.federalAccordian.Name = "federalAccordian";
            this.federalAccordian.SelectedIndex = 0;
            this.federalAccordian.Size = new System.Drawing.Size(773, 491);
            this.federalAccordian.TabIndex = 22;
            // 
            // FederalTaxTablesPanel
            // 
            this.FederalTaxTablesPanel.AutoScroll = true;
            this.FederalTaxTablesPanel.Controls.Add(this.cmbFederalTables);
            this.FederalTaxTablesPanel.Controls.Add(this.FederalTaxTableGrid);
            this.FederalTaxTablesPanel.Controls.Add(this.fcLabel11);
            this.FederalTaxTablesPanel.Location = new System.Drawing.Point(0, 463);
            this.FederalTaxTablesPanel.Name = "FederalTaxTablesPanel";
            this.FederalTaxTablesPanel.RestoreBounds = new System.Drawing.Rectangle(0, 463, 773, 28);
            this.FederalTaxTablesPanel.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.FederalTaxTablesPanel.Size = new System.Drawing.Size(773, 28);
            this.FederalTaxTablesPanel.Text = "Tax Tables";
            this.FederalTaxTablesPanel.PanelCollapsed += new System.EventHandler(this.FederalTaxTablesPanel_PanelCollapsed);
            // 
            // cmbFederalTables
            // 
            this.cmbFederalTables.AutoSize = false;
            this.cmbFederalTables.DisplayMember = "Description";
            this.cmbFederalTables.Location = new System.Drawing.Point(81, 23);
            this.cmbFederalTables.Name = "cmbFederalTables";
            this.cmbFederalTables.Size = new System.Drawing.Size(366, 30);
            this.cmbFederalTables.TabIndex = 1005;
            this.cmbFederalTables.ValueMember = "ID";
            // 
            // FederalTaxTableGrid
            // 
            this.FederalTaxTableGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.FederalTaxTableGrid.Cols = 6;
            this.FederalTaxTableGrid.Location = new System.Drawing.Point(25, 80);
            this.FederalTaxTableGrid.Name = "FederalTaxTableGrid";
            this.FederalTaxTableGrid.Rows = 5;
            this.FederalTaxTableGrid.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.FederalTaxTableGrid.Size = new System.Drawing.Size(724, 1210);
            this.FederalTaxTableGrid.StandardTab = false;
            this.FederalTaxTableGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.FederalTaxTableGrid.TabIndex = 1004;
            this.FederalTaxTableGrid.Click += new System.EventHandler(this.vsTaxTables_Click);
            // 
            // fcLabel11
            // 
            this.fcLabel11.Location = new System.Drawing.Point(19, 27);
            this.fcLabel11.Name = "fcLabel11";
            this.fcLabel11.Size = new System.Drawing.Size(56, 26);
            this.fcLabel11.TabIndex = 1003;
            this.fcLabel11.Text = "TABLE";
            // 
            // FederalDeductionsPanel
            // 
            this.FederalDeductionsPanel.AutoScroll = true;
            this.FederalDeductionsPanel.Controls.Add(this.fcFrame2);
            this.FederalDeductionsPanel.Controls.Add(this.fcFrame1);
            this.FederalDeductionsPanel.Controls.Add(this.Frame2);
            this.FederalDeductionsPanel.Controls.Add(this.lblFedStandardDeduction);
            this.FederalDeductionsPanel.Controls.Add(this.lblFedMarriedStandardDeduction);
            this.FederalDeductionsPanel.Controls.Add(this.lblFedSingleStandardDeduction);
            this.FederalDeductionsPanel.Controls.Add(this.Label1);
            this.FederalDeductionsPanel.Controls.Add(this.fcLabel1);
            this.FederalDeductionsPanel.Controls.Add(this.fcLabel2);
            this.FederalDeductionsPanel.Location = new System.Drawing.Point(0, 0);
            this.FederalDeductionsPanel.Name = "FederalDeductionsPanel";
            this.FederalDeductionsPanel.Size = new System.Drawing.Size(773, 463);
            this.FederalDeductionsPanel.Text = "Deductions";
            // 
            // fcFrame2
            // 
            this.fcFrame2.Controls.Add(this.lblFedOtherDependents);
            this.fcFrame2.Controls.Add(this.lblFedQualifyingChildren);
            this.fcFrame2.Controls.Add(this.fcLabel8);
            this.fcFrame2.Controls.Add(this.fcLabel9);
            this.fcFrame2.Location = new System.Drawing.Point(19, 315);
            this.fcFrame2.Name = "fcFrame2";
            this.fcFrame2.Size = new System.Drawing.Size(730, 93);
            this.fcFrame2.TabIndex = 22;
            this.fcFrame2.Text = "Claim Dependents";
            // 
            // lblFedOtherDependents
            // 
            this.lblFedOtherDependents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFedOtherDependents.Location = new System.Drawing.Point(193, 58);
            this.lblFedOtherDependents.Name = "lblFedOtherDependents";
            this.lblFedOtherDependents.Size = new System.Drawing.Size(76, 15);
            this.lblFedOtherDependents.TabIndex = 64;
            this.lblFedOtherDependents.Text = "0";
            this.lblFedOtherDependents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFedQualifyingChildren
            // 
            this.lblFedQualifyingChildren.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFedQualifyingChildren.Location = new System.Drawing.Point(13, 58);
            this.lblFedQualifyingChildren.Name = "lblFedQualifyingChildren";
            this.lblFedQualifyingChildren.Size = new System.Drawing.Size(76, 15);
            this.lblFedQualifyingChildren.TabIndex = 63;
            this.lblFedQualifyingChildren.Text = "0";
            this.lblFedQualifyingChildren.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel8
            // 
            this.fcLabel8.Location = new System.Drawing.Point(13, 31);
            this.fcLabel8.Name = "fcLabel8";
            this.fcLabel8.Size = new System.Drawing.Size(156, 21);
            this.fcLabel8.TabIndex = 37;
            this.fcLabel8.Text = "QUALIFYING CHILDREN";
            // 
            // fcLabel9
            // 
            this.fcLabel9.Location = new System.Drawing.Point(193, 31);
            this.fcLabel9.Name = "fcLabel9";
            this.fcLabel9.Size = new System.Drawing.Size(140, 21);
            this.fcLabel9.TabIndex = 36;
            this.fcLabel9.Text = "OTHER DEPENDENTS";
            // 
            // fcFrame1
            // 
            this.fcFrame1.Controls.Add(this.lblMedicareThresholdRate);
            this.fcFrame1.Controls.Add(this.lblMedicareThreshold);
            this.fcFrame1.Controls.Add(this.lblMedicareEmployerPercentage);
            this.fcFrame1.Controls.Add(this.lblMedicarePercentage);
            this.fcFrame1.Controls.Add(this.lblMedicareMaxWage);
            this.fcFrame1.Controls.Add(this.fcLabel3);
            this.fcFrame1.Controls.Add(this.fcLabel4);
            this.fcFrame1.Controls.Add(this.fcLabel5);
            this.fcFrame1.Controls.Add(this.fcLabel6);
            this.fcFrame1.Controls.Add(this.fcLabel7);
            this.fcFrame1.FormatCaption = false;
            this.fcFrame1.Location = new System.Drawing.Point(20, 195);
            this.fcFrame1.Name = "fcFrame1";
            this.fcFrame1.Size = new System.Drawing.Size(729, 97);
            this.fcFrame1.TabIndex = 21;
            this.fcFrame1.Text = "Medicare";
            // 
            // lblMedicareThresholdRate
            // 
            this.lblMedicareThresholdRate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedicareThresholdRate.Location = new System.Drawing.Point(580, 57);
            this.lblMedicareThresholdRate.Name = "lblMedicareThresholdRate";
            this.lblMedicareThresholdRate.Size = new System.Drawing.Size(76, 15);
            this.lblMedicareThresholdRate.TabIndex = 65;
            this.lblMedicareThresholdRate.Text = "0.00";
            this.lblMedicareThresholdRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMedicareThreshold
            // 
            this.lblMedicareThreshold.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedicareThreshold.Location = new System.Drawing.Point(450, 57);
            this.lblMedicareThreshold.Name = "lblMedicareThreshold";
            this.lblMedicareThreshold.Size = new System.Drawing.Size(76, 15);
            this.lblMedicareThreshold.TabIndex = 64;
            this.lblMedicareThreshold.Text = "0.00";
            this.lblMedicareThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMedicareEmployerPercentage
            // 
            this.lblMedicareEmployerPercentage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedicareEmployerPercentage.Location = new System.Drawing.Point(273, 57);
            this.lblMedicareEmployerPercentage.Name = "lblMedicareEmployerPercentage";
            this.lblMedicareEmployerPercentage.Size = new System.Drawing.Size(76, 15);
            this.lblMedicareEmployerPercentage.TabIndex = 63;
            this.lblMedicareEmployerPercentage.Text = "0.00";
            this.lblMedicareEmployerPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMedicarePercentage
            // 
            this.lblMedicarePercentage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedicarePercentage.Location = new System.Drawing.Point(143, 57);
            this.lblMedicarePercentage.Name = "lblMedicarePercentage";
            this.lblMedicarePercentage.Size = new System.Drawing.Size(76, 15);
            this.lblMedicarePercentage.TabIndex = 62;
            this.lblMedicarePercentage.Text = "0.00";
            this.lblMedicarePercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMedicareMaxWage
            // 
            this.lblMedicareMaxWage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedicareMaxWage.Location = new System.Drawing.Point(13, 57);
            this.lblMedicareMaxWage.Name = "lblMedicareMaxWage";
            this.lblMedicareMaxWage.Size = new System.Drawing.Size(76, 15);
            this.lblMedicareMaxWage.TabIndex = 61;
            this.lblMedicareMaxWage.Text = "0.00";
            this.lblMedicareMaxWage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel3
            // 
            this.fcLabel3.Location = new System.Drawing.Point(580, 30);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(120, 30);
            this.fcLabel3.TabIndex = 59;
            this.fcLabel3.Text = "THRESHOLD RATE";
            // 
            // fcLabel4
            // 
            this.fcLabel4.Location = new System.Drawing.Point(450, 30);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(120, 30);
            this.fcLabel4.TabIndex = 58;
            this.fcLabel4.Text = "RATE THRESHOLD";
            // 
            // fcLabel5
            // 
            this.fcLabel5.Location = new System.Drawing.Point(273, 30);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(171, 30);
            this.fcLabel5.TabIndex = 57;
            this.fcLabel5.Text = "EMPLOYER PERCENTAGE";
            // 
            // fcLabel6
            // 
            this.fcLabel6.Location = new System.Drawing.Point(143, 30);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(104, 21);
            this.fcLabel6.TabIndex = 49;
            this.fcLabel6.Text = "PERCENTAGE";
            // 
            // fcLabel7
            // 
            this.fcLabel7.Location = new System.Drawing.Point(13, 30);
            this.fcLabel7.Name = "fcLabel7";
            this.fcLabel7.Size = new System.Drawing.Size(91, 21);
            this.fcLabel7.TabIndex = 48;
            this.fcLabel7.Text = "MAX WAGE";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.lblFicaEmployerPercentage);
            this.Frame2.Controls.Add(this.lblFicaPercentage);
            this.Frame2.Controls.Add(this.lblFicaMaxWage);
            this.Frame2.Controls.Add(this.Label21);
            this.Frame2.Controls.Add(this.Label8);
            this.Frame2.Controls.Add(this.Label7);
            this.Frame2.FormatCaption = false;
            this.Frame2.Location = new System.Drawing.Point(19, 86);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(730, 89);
            this.Frame2.TabIndex = 20;
            this.Frame2.Text = "FICA";
            // 
            // lblFicaEmployerPercentage
            // 
            this.lblFicaEmployerPercentage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFicaEmployerPercentage.Location = new System.Drawing.Point(273, 57);
            this.lblFicaEmployerPercentage.Name = "lblFicaEmployerPercentage";
            this.lblFicaEmployerPercentage.Size = new System.Drawing.Size(76, 15);
            this.lblFicaEmployerPercentage.TabIndex = 60;
            this.lblFicaEmployerPercentage.Text = "0.00";
            this.lblFicaEmployerPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFicaPercentage
            // 
            this.lblFicaPercentage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFicaPercentage.Location = new System.Drawing.Point(143, 57);
            this.lblFicaPercentage.Name = "lblFicaPercentage";
            this.lblFicaPercentage.Size = new System.Drawing.Size(76, 15);
            this.lblFicaPercentage.TabIndex = 59;
            this.lblFicaPercentage.Text = "0.00";
            this.lblFicaPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFicaMaxWage
            // 
            this.lblFicaMaxWage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFicaMaxWage.Location = new System.Drawing.Point(13, 57);
            this.lblFicaMaxWage.Name = "lblFicaMaxWage";
            this.lblFicaMaxWage.Size = new System.Drawing.Size(76, 15);
            this.lblFicaMaxWage.TabIndex = 58;
            this.lblFicaMaxWage.Text = "0.00";
            this.lblFicaMaxWage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(273, 30);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(188, 21);
            this.Label21.TabIndex = 57;
            this.Label21.Text = "EMPLOYER PERCENTAGE";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(143, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(104, 21);
            this.Label8.TabIndex = 49;
            this.Label8.Text = "PERCENTAGE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(13, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(91, 21);
            this.Label7.TabIndex = 48;
            this.Label7.Text = "MAX WAGE";
            // 
            // lblFedStandardDeduction
            // 
            this.lblFedStandardDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFedStandardDeduction.Location = new System.Drawing.Point(20, 52);
            this.lblFedStandardDeduction.Name = "lblFedStandardDeduction";
            this.lblFedStandardDeduction.Size = new System.Drawing.Size(162, 15);
            this.lblFedStandardDeduction.TabIndex = 25;
            this.lblFedStandardDeduction.Text = "0.00";
            this.lblFedStandardDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFedMarriedStandardDeduction
            // 
            this.lblFedMarriedStandardDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFedMarriedStandardDeduction.Location = new System.Drawing.Point(212, 52);
            this.lblFedMarriedStandardDeduction.Name = "lblFedMarriedStandardDeduction";
            this.lblFedMarriedStandardDeduction.Size = new System.Drawing.Size(162, 15);
            this.lblFedMarriedStandardDeduction.TabIndex = 24;
            this.lblFedMarriedStandardDeduction.Text = "0.00";
            this.lblFedMarriedStandardDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFedSingleStandardDeduction
            // 
            this.lblFedSingleStandardDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFedSingleStandardDeduction.Location = new System.Drawing.Point(407, 52);
            this.lblFedSingleStandardDeduction.Name = "lblFedSingleStandardDeduction";
            this.lblFedSingleStandardDeduction.Size = new System.Drawing.Size(162, 15);
            this.lblFedSingleStandardDeduction.TabIndex = 23;
            this.lblFedSingleStandardDeduction.Text = "0.00";
            this.lblFedSingleStandardDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(19, 21);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(179, 16);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "EXEMPTION ALLOWANCE";
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(212, 21);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(174, 15);
            this.fcLabel1.TabIndex = 17;
            this.fcLabel1.Text = "MARRIED STD DEDUCTION";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(407, 21);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(162, 15);
            this.fcLabel2.TabIndex = 18;
            this.fcLabel2.Text = "SINGLE STD DEDUCTION";
            // 
            // tabPayStatuses_Page2
            // 
            this.tabPayStatuses_Page2.Controls.Add(this.stateAccordian);
            this.tabPayStatuses_Page2.Location = new System.Drawing.Point(0, 35);
            this.tabPayStatuses_Page2.Name = "tabPayStatuses_Page2";
            this.tabPayStatuses_Page2.Size = new System.Drawing.Size(779, 500);
            this.tabPayStatuses_Page2.Text = "State";
            // 
            // stateAccordian
            // 
            this.stateAccordian.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.stateAccordian.BorderStyle = Wisej.Web.BorderStyle.None;
            this.stateAccordian.Collapsible = true;
            this.stateAccordian.Controls.Add(this.stateTaxTablePanel);
            this.stateAccordian.Controls.Add(this.stateDeductionPanel);
            this.stateAccordian.Location = new System.Drawing.Point(1, 3);
            this.stateAccordian.Name = "stateAccordian";
            this.stateAccordian.SelectedIndex = 0;
            this.stateAccordian.Size = new System.Drawing.Size(773, 494);
            this.stateAccordian.TabIndex = 69;
            // 
            // stateTaxTablePanel
            // 
            this.stateTaxTablePanel.AutoScroll = true;
            this.stateTaxTablePanel.Controls.Add(this.cmbStateTables);
            this.stateTaxTablePanel.Controls.Add(this.StateTaxTableGrid);
            this.stateTaxTablePanel.Controls.Add(this.fcLabel12);
            this.stateTaxTablePanel.Location = new System.Drawing.Point(0, 466);
            this.stateTaxTablePanel.Name = "stateTaxTablePanel";
            this.stateTaxTablePanel.RestoreBounds = new System.Drawing.Rectangle(0, 466, 773, 28);
            this.stateTaxTablePanel.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.stateTaxTablePanel.Size = new System.Drawing.Size(773, 28);
            this.stateTaxTablePanel.Text = "Tax Tables";
            // 
            // cmbStateTables
            // 
            this.cmbStateTables.AutoSize = false;
            this.cmbStateTables.DisplayMember = "Description";
            this.cmbStateTables.Location = new System.Drawing.Point(81, 15);
            this.cmbStateTables.Name = "cmbStateTables";
            this.cmbStateTables.Size = new System.Drawing.Size(366, 30);
            this.cmbStateTables.TabIndex = 1008;
            this.cmbStateTables.ValueMember = "ID";
            this.cmbStateTables.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // StateTaxTableGrid
            // 
            this.StateTaxTableGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.StateTaxTableGrid.Cols = 6;
            this.StateTaxTableGrid.Location = new System.Drawing.Point(8, 55);
            this.StateTaxTableGrid.Name = "StateTaxTableGrid";
            this.StateTaxTableGrid.Rows = 50;
            this.StateTaxTableGrid.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.StateTaxTableGrid.Size = new System.Drawing.Size(690, 1680);
            this.StateTaxTableGrid.StandardTab = false;
            this.StateTaxTableGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.StateTaxTableGrid.TabIndex = 1007;
            this.StateTaxTableGrid.Click += new System.EventHandler(this.fcGrid1_Click);
            // 
            // fcLabel12
            // 
            this.fcLabel12.Location = new System.Drawing.Point(19, 19);
            this.fcLabel12.Name = "fcLabel12";
            this.fcLabel12.Size = new System.Drawing.Size(56, 26);
            this.fcLabel12.TabIndex = 1006;
            this.fcLabel12.Text = "TABLE";
            this.fcLabel12.Click += new System.EventHandler(this.fcLabel12_Click);
            // 
            // stateDeductionPanel
            // 
            this.stateDeductionPanel.Controls.Add(this.fcFrame4);
            this.stateDeductionPanel.Controls.Add(this.fcFrame3);
            this.stateDeductionPanel.Controls.Add(this.lblStateWithholdingAllowance);
            this.stateDeductionPanel.Controls.Add(this.Label2);
            this.stateDeductionPanel.Location = new System.Drawing.Point(0, 0);
            this.stateDeductionPanel.Name = "stateDeductionPanel";
            this.stateDeductionPanel.Size = new System.Drawing.Size(773, 466);
            this.stateDeductionPanel.Text = "Deductions";
            // 
            // fcFrame4
            // 
            this.fcFrame4.Controls.Add(this.lblStateMarriedCutoff);
            this.fcFrame4.Controls.Add(this.lblStateMarriedWageLimit);
            this.fcFrame4.Controls.Add(this.lblStateMarriedDeduction);
            this.fcFrame4.Controls.Add(this.fcLabel20);
            this.fcFrame4.Controls.Add(this.fcLabel21);
            this.fcFrame4.Controls.Add(this.fcLabel22);
            this.fcFrame4.FormatCaption = false;
            this.fcFrame4.Location = new System.Drawing.Point(25, 208);
            this.fcFrame4.Name = "fcFrame4";
            this.fcFrame4.Size = new System.Drawing.Size(730, 89);
            this.fcFrame4.TabIndex = 72;
            this.fcFrame4.Text = "Married";
            // 
            // lblStateMarriedCutoff
            // 
            this.lblStateMarriedCutoff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateMarriedCutoff.Location = new System.Drawing.Point(435, 57);
            this.lblStateMarriedCutoff.Name = "lblStateMarriedCutoff";
            this.lblStateMarriedCutoff.Size = new System.Drawing.Size(130, 15);
            this.lblStateMarriedCutoff.TabIndex = 60;
            this.lblStateMarriedCutoff.Text = "0.00";
            this.lblStateMarriedCutoff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateMarriedWageLimit
            // 
            this.lblStateMarriedWageLimit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateMarriedWageLimit.Location = new System.Drawing.Point(183, 57);
            this.lblStateMarriedWageLimit.Name = "lblStateMarriedWageLimit";
            this.lblStateMarriedWageLimit.Size = new System.Drawing.Size(117, 15);
            this.lblStateMarriedWageLimit.TabIndex = 59;
            this.lblStateMarriedWageLimit.Text = "0.00";
            this.lblStateMarriedWageLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateMarriedDeduction
            // 
            this.lblStateMarriedDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateMarriedDeduction.Location = new System.Drawing.Point(13, 57);
            this.lblStateMarriedDeduction.Name = "lblStateMarriedDeduction";
            this.lblStateMarriedDeduction.Size = new System.Drawing.Size(105, 15);
            this.lblStateMarriedDeduction.TabIndex = 58;
            this.lblStateMarriedDeduction.Text = "0.00";
            this.lblStateMarriedDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel20
            // 
            this.fcLabel20.Location = new System.Drawing.Point(435, 30);
            this.fcLabel20.Name = "fcLabel20";
            this.fcLabel20.Size = new System.Drawing.Size(188, 21);
            this.fcLabel20.TabIndex = 57;
            this.fcLabel20.Text = "UPPER LIMIT";
            // 
            // fcLabel21
            // 
            this.fcLabel21.Location = new System.Drawing.Point(183, 30);
            this.fcLabel21.Name = "fcLabel21";
            this.fcLabel21.Size = new System.Drawing.Size(250, 21);
            this.fcLabel21.TabIndex = 49;
            this.fcLabel21.Text = "WAGE LIMIT FOR FULL DEDUCTION";
            // 
            // fcLabel22
            // 
            this.fcLabel22.Location = new System.Drawing.Point(13, 30);
            this.fcLabel22.Name = "fcLabel22";
            this.fcLabel22.Size = new System.Drawing.Size(160, 21);
            this.fcLabel22.TabIndex = 48;
            this.fcLabel22.Text = "STANDARD DEDUCTION";
            // 
            // fcFrame3
            // 
            this.fcFrame3.Controls.Add(this.fcLabel14);
            this.fcFrame3.Controls.Add(this.lblStateSingleCutoff);
            this.fcFrame3.Controls.Add(this.lblStateSingleWageLimit);
            this.fcFrame3.Controls.Add(this.lblStateSingleDeduction);
            this.fcFrame3.Controls.Add(this.fcLabel17);
            this.fcFrame3.Controls.Add(this.fcLabel18);
            this.fcFrame3.Controls.Add(this.fcLabel19);
            this.fcFrame3.FormatCaption = false;
            this.fcFrame3.Location = new System.Drawing.Point(25, 100);
            this.fcFrame3.Name = "fcFrame3";
            this.fcFrame3.Size = new System.Drawing.Size(730, 89);
            this.fcFrame3.TabIndex = 71;
            this.fcFrame3.Text = "Single";
            // 
            // fcLabel14
            // 
            this.fcLabel14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel14.Location = new System.Drawing.Point(68, -151);
            this.fcLabel14.Name = "fcLabel14";
            this.fcLabel14.Size = new System.Drawing.Size(105, 15);
            this.fcLabel14.TabIndex = 73;
            this.fcLabel14.Text = "0.00";
            this.fcLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateSingleCutoff
            // 
            this.lblStateSingleCutoff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateSingleCutoff.Location = new System.Drawing.Point(435, 57);
            this.lblStateSingleCutoff.Name = "lblStateSingleCutoff";
            this.lblStateSingleCutoff.Size = new System.Drawing.Size(130, 15);
            this.lblStateSingleCutoff.TabIndex = 60;
            this.lblStateSingleCutoff.Text = "0.00";
            this.lblStateSingleCutoff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateSingleWageLimit
            // 
            this.lblStateSingleWageLimit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateSingleWageLimit.Location = new System.Drawing.Point(183, 57);
            this.lblStateSingleWageLimit.Name = "lblStateSingleWageLimit";
            this.lblStateSingleWageLimit.Size = new System.Drawing.Size(117, 15);
            this.lblStateSingleWageLimit.TabIndex = 59;
            this.lblStateSingleWageLimit.Text = "0.00";
            this.lblStateSingleWageLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateSingleDeduction
            // 
            this.lblStateSingleDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateSingleDeduction.Location = new System.Drawing.Point(13, 57);
            this.lblStateSingleDeduction.Name = "lblStateSingleDeduction";
            this.lblStateSingleDeduction.Size = new System.Drawing.Size(105, 15);
            this.lblStateSingleDeduction.TabIndex = 58;
            this.lblStateSingleDeduction.Text = "0.00";
            this.lblStateSingleDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateSingleDeduction.Click += new System.EventHandler(this.fcLabel16_Click);
            // 
            // fcLabel17
            // 
            this.fcLabel17.Location = new System.Drawing.Point(435, 30);
            this.fcLabel17.Name = "fcLabel17";
            this.fcLabel17.Size = new System.Drawing.Size(188, 21);
            this.fcLabel17.TabIndex = 57;
            this.fcLabel17.Text = "UPPER LIMIT";
            // 
            // fcLabel18
            // 
            this.fcLabel18.Location = new System.Drawing.Point(183, 30);
            this.fcLabel18.Name = "fcLabel18";
            this.fcLabel18.Size = new System.Drawing.Size(250, 21);
            this.fcLabel18.TabIndex = 49;
            this.fcLabel18.Text = "WAGE LIMIT FOR FULL DEDUCTION";
            // 
            // fcLabel19
            // 
            this.fcLabel19.Location = new System.Drawing.Point(13, 30);
            this.fcLabel19.Name = "fcLabel19";
            this.fcLabel19.Size = new System.Drawing.Size(160, 21);
            this.fcLabel19.TabIndex = 48;
            this.fcLabel19.Text = "STANDARD DEDUCTION";
            // 
            // lblStateWithholdingAllowance
            // 
            this.lblStateWithholdingAllowance.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblStateWithholdingAllowance.Location = new System.Drawing.Point(25, 57);
            this.lblStateWithholdingAllowance.Name = "lblStateWithholdingAllowance";
            this.lblStateWithholdingAllowance.Size = new System.Drawing.Size(181, 1);
            this.lblStateWithholdingAllowance.TabIndex = 69;
            this.lblStateWithholdingAllowance.Text = "0.00";
            this.lblStateWithholdingAllowance.Click += new System.EventHandler(this.lblStateWithholdingAllowance_Click);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(25, 27);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(181, 16);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "WITHHOLDING ALLOWANCE";
            this.Label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSP1,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP3,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 0;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Index = 2;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 3;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 4;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                 ";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 5;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit             ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 6;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 7;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmbTaxYear
            // 
            this.cmbTaxYear.Location = new System.Drawing.Point(113, 6);
            this.cmbTaxYear.Name = "cmbTaxYear";
            this.cmbTaxYear.Size = new System.Drawing.Size(163, 30);
            this.cmbTaxYear.TabIndex = 1001;
            // 
            // fcLabel10
            // 
            this.fcLabel10.AutoSize = true;
            this.fcLabel10.Location = new System.Drawing.Point(32, 11);
            this.fcLabel10.Name = "fcLabel10";
            this.fcLabel10.Size = new System.Drawing.Size(68, 15);
            this.fcLabel10.TabIndex = 1002;
            this.fcLabel10.Text = "TAX YEAR";
            // 
            // frmTaxSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(832, 680);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmTaxSetup";
            this.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.Text = "Tax Tables";
            this.Load += new System.EventHandler(this.frmTaxSetup_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxSetup_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.tabPayStatuses.ResumeLayout(false);
            this.tabPayStatuses_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.federalAccordian)).EndInit();
            this.federalAccordian.ResumeLayout(false);
            this.FederalTaxTablesPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FederalTaxTableGrid)).EndInit();
            this.FederalDeductionsPanel.ResumeLayout(false);
            this.FederalDeductionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame2)).EndInit();
            this.fcFrame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).EndInit();
            this.fcFrame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.tabPayStatuses_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateAccordian)).EndInit();
            this.stateAccordian.ResumeLayout(false);
            this.stateTaxTablePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StateTaxTableGrid)).EndInit();
            this.stateDeductionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame4)).EndInit();
            this.fcFrame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fcFrame3)).EndInit();
            this.fcFrame3.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        private FCLabel fcLabel2;
        private FCLabel fcLabel1;
        private FCComboBox cmbTaxYear;
        private FCLabel fcLabel10;
        private Accordion federalAccordian;
        private AccordionPanel FederalTaxTablesPanel;
        private AccordionPanel FederalDeductionsPanel;
        public FCFrame fcFrame1;
        public FCLabel fcLabel3;
        public FCLabel fcLabel4;
        public FCLabel fcLabel5;
        public FCLabel fcLabel6;
        public FCLabel fcLabel7;
        private FCFrame fcFrame2;
        public FCLabel fcLabel8;
        public FCLabel fcLabel9;
        public FCFrame Frame2;
        public FCLabel Label21;
        public FCLabel Label8;
        public FCLabel Label7;
        private FCLabel lblFedStandardDeduction;
        private FCLabel lblFedMarriedStandardDeduction;
        private FCLabel lblFedSingleStandardDeduction;
        private FCLabel lblFicaEmployerPercentage;
        private FCLabel lblFicaPercentage;
        private FCLabel lblFicaMaxWage;
        private FCLabel lblMedicareThresholdRate;
        private FCLabel lblMedicareThreshold;
        private FCLabel lblMedicareEmployerPercentage;
        private FCLabel lblMedicarePercentage;
        private FCLabel lblMedicareMaxWage;
        private FCLabel lblFedOtherDependents;
        private FCLabel lblFedQualifyingChildren;
        private FCLabel fcLabel11;
        public FCGrid FederalTaxTableGrid;
        private ComboBox cmbFederalTables;
        private Accordion stateAccordian;
        private AccordionPanel stateTaxTablePanel;
        private AccordionPanel stateDeductionPanel;
        private ComboBox cmbStateTables;
        public FCGrid StateTaxTableGrid;
        private FCLabel fcLabel12;
        public FCFrame fcFrame3;
        private FCLabel lblStateSingleCutoff;
        private FCLabel lblStateSingleWageLimit;
        private FCLabel lblStateSingleDeduction;
        public FCLabel fcLabel17;
        public FCLabel fcLabel18;
        public FCLabel fcLabel19;
        public FCLabel lblStateWithholdingAllowance;
        public FCFrame fcFrame4;
        private FCLabel lblStateMarriedCutoff;
        private FCLabel lblStateMarriedWageLimit;
        private FCLabel lblStateMarriedDeduction;
        public FCLabel fcLabel20;
        public FCLabel fcLabel21;
        public FCLabel fcLabel22;
        private FCLabel fcLabel14;
    }
}