﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmChooseBank.
	/// </summary>
	partial class frmChooseBank
	{
		public FCGrid Grid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 332);
            this.BottomPanel.Size = new System.Drawing.Size(659, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(659, 272);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(659, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(160, 30);
            this.HeaderText.Text = "Choose Bank";
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 30);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.ReadOnly = true;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.Size = new System.Drawing.Size(596, 230);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 0;
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(248, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(174, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmChooseBank
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(659, 518);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChooseBank";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Choose Bank";
            this.Load += new System.EventHandler(this.frmChooseBank_Load);
            this.Activated += new System.EventHandler(this.frmChooseBank_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseBank_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
