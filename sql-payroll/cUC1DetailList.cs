﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class cUC1DetailList
	{
		//=========================================================
		private FCCollection lstDetails = new FCCollection();
		private int intCurrentIndex;

		public cUC1DetailList() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			if (!(lstDetails == null))
			{
				foreach (cUC1Schedule2Detail tRec in lstDetails)
				{
					lstDetails.Remove(1);
				}
				// tRec
			}
		}

		public void AddItem(ref cUC1Schedule2Detail tItem)
		{
			if (!(tItem == null))
			{
				lstDetails.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			if (!(lstDetails == null))
			{
				if (!FCUtils.IsEmpty(lstDetails))
				{
					if (lstDetails.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > lstDetails.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstDetails.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstDetails.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstDetails[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public int ItemCount()
		{
			int ItemCount = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				ItemCount = lstDetails.Count;
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public cUC1Schedule2Detail GetCurrentDetail()
		{
			cUC1Schedule2Detail GetCurrentDetail = null;
			cUC1Schedule2Detail tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstDetails[intCurrentIndex] == null))
					{
						tRec = lstDetails[intCurrentIndex];
					}
				}
			}
			GetCurrentDetail = tRec;
			return GetCurrentDetail;
		}

		public cUC1Schedule2Detail GetDetailByIndex(ref int intindex)
		{
			cUC1Schedule2Detail GetDetailByIndex = null;
			cUC1Schedule2Detail tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(lstDetails) && intindex > 0)
			{
				if (!(lstDetails[intindex] == null))
				{
					intCurrentIndex = intindex;
					tRec = lstDetails[intindex];
				}
			}
			GetDetailByIndex = tRec;
			return GetDetailByIndex;
		}

		public void CreateDetail(string strFirstName, string strMiddleName, string strLastName, string strDesig, string strSSN, double dblWages, bool boolSeasonal, bool boolFemale, bool boolEmployedM1, bool boolEmployedM2, bool boolEmployedM3, double dblExcess, double dblTaxable)
		{
			cUC1Schedule2Detail tRec = new cUC1Schedule2Detail();
			tRec.FirstName = strFirstName;
			tRec.MiddleName = strMiddleName;
			tRec.LastName = strLastName;
			tRec.Designation = strDesig;
			tRec.SSN = strSSN;
			tRec.UCWages = dblWages;
			tRec.IsSeasonal = boolSeasonal;
			tRec.Female = boolFemale;
			tRec.Set_EmployedDuringMonth(1, boolEmployedM1);
			tRec.Set_EmployedDuringMonth(2, boolEmployedM2);
			tRec.Set_EmployedDuringMonth(3, boolEmployedM3);
			tRec.ExcessWages = dblExcess;
			tRec.TaxableWages = dblTaxable;
			AddItem(ref tRec);
		}
	}
}
