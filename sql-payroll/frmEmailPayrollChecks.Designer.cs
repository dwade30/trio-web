﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmailPayrollChecks.
	/// </summary>
	partial class frmEmailPayrollChecks
	{
		public fecherFoundation.FCComboBox cmbHTML;
		public fecherFoundation.FCLabel lblHTML;
		public fecherFoundation.FCComboBox cmbPayDate;
		public fecherFoundation.FCComboBox cmbPayrun;
		public fecherFoundation.FCComboBox cboCheckSelection;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblNegotiable;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbHTML = new fecherFoundation.FCComboBox();
            this.lblHTML = new fecherFoundation.FCLabel();
            this.cmbPayDate = new fecherFoundation.FCComboBox();
            this.cmbPayrun = new fecherFoundation.FCComboBox();
            this.cboCheckSelection = new fecherFoundation.FCComboBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblNegotiable = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 277);
            this.BottomPanel.Size = new System.Drawing.Size(413, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbHTML);
            this.ClientArea.Controls.Add(this.lblHTML);
            this.ClientArea.Controls.Add(this.cmbPayDate);
            this.ClientArea.Controls.Add(this.cmbPayrun);
            this.ClientArea.Controls.Add(this.cboCheckSelection);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblNegotiable);
            this.ClientArea.Size = new System.Drawing.Size(433, 393);
            this.ClientArea.Controls.SetChildIndex(this.lblNegotiable, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboCheckSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPayrun, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPayDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblHTML, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbHTML, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(433, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(151, 28);
            this.HeaderText.Text = "Email Checks";
            // 
            // cmbHTML
            // 
            this.cmbHTML.Items.AddRange(new object[] {
            "HTML",
            "PDF attachment"});
            this.cmbHTML.Location = new System.Drawing.Point(30, 237);
            this.cmbHTML.Name = "cmbHTML";
            this.cmbHTML.Size = new System.Drawing.Size(207, 40);
            this.cmbHTML.TabIndex = 1001;
            // 
            // lblHTML
            // 
            this.lblHTML.AutoSize = true;
            this.lblHTML.Location = new System.Drawing.Point(30, 208);
            this.lblHTML.Name = "lblHTML";
            this.lblHTML.Size = new System.Drawing.Size(102, 15);
            this.lblHTML.TabIndex = 1;
            this.lblHTML.Text = "EMAIL FORMAT";
            // 
            // cmbPayDate
            // 
            this.cmbPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayDate.Location = new System.Drawing.Point(30, 59);
            this.cmbPayDate.Name = "cmbPayDate";
            this.cmbPayDate.Size = new System.Drawing.Size(205, 40);
            this.cmbPayDate.TabIndex = 2;
            this.cmbPayDate.SelectedIndexChanged += new System.EventHandler(this.cmbPayDate_SelectedIndexChanged);
            // 
            // cmbPayrun
            // 
            this.cmbPayrun.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPayrun.Location = new System.Drawing.Point(265, 59);
            this.cmbPayrun.Name = "cmbPayrun";
            this.cmbPayrun.Size = new System.Drawing.Size(140, 40);
            this.cmbPayrun.TabIndex = 1;
            // 
            // cboCheckSelection
            // 
            this.cboCheckSelection.BackColor = System.Drawing.SystemColors.Window;
            this.cboCheckSelection.Location = new System.Drawing.Point(30, 148);
            this.cboCheckSelection.Name = "cboCheckSelection";
            this.cboCheckSelection.Size = new System.Drawing.Size(207, 40);
            this.cboCheckSelection.TabIndex = 1002;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(101, 15);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "PAY DATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(265, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(87, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "PAY RUN";
            // 
            // lblNegotiable
            // 
            this.lblNegotiable.Location = new System.Drawing.Point(30, 119);
            this.lblNegotiable.Name = "lblNegotiable";
            this.lblNegotiable.Size = new System.Drawing.Size(205, 15);
            this.lblNegotiable.TabIndex = 3;
            this.lblNegotiable.Text = "CHECK FORMAT";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(127, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(175, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // frmEmailPayrollChecks
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(433, 453);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEmailPayrollChecks";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Email Checks";
            this.Load += new System.EventHandler(this.frmEmailPayrollChecks_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailPayrollChecks_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
