//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptESHPExport.
	/// </summary>
	public partial class rptESHPExport : BaseSectionReport
	{
		public rptESHPExport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ESHP";
			if (_InstancePtr == null)
				_InstancePtr = this;
			Grid = new FCGrid();
			Grid.Cols = 8;
			GridMisc = new FCGrid();
		}

		public static rptESHPExport InstancePtr
		{
			get
			{
				return (rptESHPExport)Sys.GetInstance(typeof(rptESHPExport));
			}
		}

		protected rptESHPExport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
				clsDeductions?.Dispose();
				clsDDDeductions?.Dispose();
				clsPay?.Dispose();
				clsVacSick?.Dispose();
				clsSick?.Dispose();
				clsVacSickOther1?.Dispose();
                clsVacSickOther2?.Dispose();
				clsVacSickOther3?.Dispose();
				clsVacation?.Dispose();
				clsEMatch?.Dispose();
				clsStates?.Dispose();
                clsYTDDeductions?.Dispose();
				clsBanks?.Dispose();
                clsLoad = null;
                clsDDDeductions = null;
                clsDeductions = null;
                clsPay = null;
                clsVacSick = null;
                clsSick = null;
                clsVacSickOther1 = null;
				clsVacSickOther2 = null;
                clsVacSickOther3 = null;
                clsVacation = null;
                clsEMatch = null;
                clsStates = null;
                clsYTDDeductions = null;
                clsBanks = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptESHPExport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTMAXSTUBLINES = 23;
		private int intMaxPayLine;
		private int intMaxDedLine;
		private clsDRWrapper clsLoad = new clsDRWrapper();
		string strTag;
		//clsDRWrapper clsExecute = new clsDRWrapper();
		bool boolReprinting;
		// vbPorter upgrade warning: lngCurrentCheckNumber As int	OnRead(string)
		int lngCurrentCheckNumber;
		DateTime dtPayDate;
		bool boolMoreThanNinePays;
		// if there are more than 9 pay categories
		bool boolMoreThanNineDeds;
		// if there are more than 9 deduction categories
		clsDRWrapper clsDeductions = new clsDRWrapper();
		clsDRWrapper clsPay = new clsDRWrapper();
		clsDRWrapper clsVacSick = new clsDRWrapper();
		clsDRWrapper clsSick = new clsDRWrapper();
		clsDRWrapper clsVacSickOther1 = new clsDRWrapper();
		clsDRWrapper clsVacSickOther2 = new clsDRWrapper();
		clsDRWrapper clsVacSickOther3 = new clsDRWrapper();
		clsDRWrapper clsVacation = new clsDRWrapper();
		clsDRWrapper clsEMatch = new clsDRWrapper();
		FCGrid Grid;
		FCGrid GridMisc;
		// vbPorter upgrade warning: intNumPays As int	OnWriteFCConvert.ToInt32(
		int intNumPays;
		// vbPorter upgrade warning: intNumDeds As int	OnWriteFCConvert.ToInt32(
		int intNumDeds;
		int[] arPayCodes = new int[100 + 1];
		int[] arDedCodes = new int[150 + 1];
		string[] arPayDescriptions = new string[100 + 1];
		string[] arPayTypes = new string[100 + 1];
		string[] arDedDescriptions = new string[150 + 1];
		int intMaxStubLines;
		clsDRWrapper clsStates = new clsDRWrapper();
		clsDRWrapper clsDDDeductions = new clsDRWrapper();
		private clsDRWrapper clsYTDDeductions = new clsDRWrapper();
		private int intMaxStubDDLines;
		private clsDRWrapper clsBanks = new clsDRWrapper();
		private int intPayrunToUse;
		private clsESHPChecks clsCheckList = new clsESHPChecks();
		private string strDirForExport = "";
		// vbPorter upgrade warning: strCurrentCheckNumber As string	OnWrite(string, int)
		private string strCurrentCheckNumber = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// Set Me.Printer = Nothing
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_QueryClose(ref int Cancel, ref int CloseMode)
		{
			if (MessageBox.Show("Continue file export?", "Create Files?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
			{
				frmESHPCreateFiles.InstancePtr.Init(ref clsCheckList);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			SaveFiles();
		}

		private void SaveFiles()
		{
			// export files
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				for (x = 0; x <= this.Document.Pages.Count - 1; x++)
				{
                    string fileName = strDirForExport + this.UserData.ToString()/*.Document.Pages[x].Tag*/+ ".pdf";
                    pe.Export(this.Document, fileName, (x + 1).ToString());
                }
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			intMaxPayLine = 1;
			intMaxDedLine = 1;
			clsCheckList.ClearList();
			clsCheckList.PayDate = dtPayDate;
			clsCheckList.Payrun = intPayrunToUse;
			// get the max number of pay lines and the max deduction lines
			// there won't be more since the parent report has already taken into
			// account what cnstmaxstublines is.  This can be changed to put the burden
			// on the stub but would have to be done to both stub sub reports.
			for (x = 1; x <= CNSTMAXSTUBLINES; x++)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, 2)) != string.Empty)
				{
					intMaxPayLine = x + 1;
				}
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(x, 4)) != string.Empty)
				{
					intMaxDedLine = x + 1;
				}
			}
			// x
		}

		private void FillInfo()
		{
			// fill textboxes from grid
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			int x;
			clsESHPCheckRecord tCheckRecord = new clsESHPCheckRecord();
			lblCheckMessage.Text = modGlobalVariables.Statics.gstrCheckMessage;
			for (x = 0; x <= intMaxPayLine - 1; x++)
			{
				(Detail.Controls["txtPayDesc" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 0);
				(Detail.Controls["txtHours" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 1);
				(Detail.Controls["txtPayAmount" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 2);
			}
			// x
			for (x = 0; x <= intMaxDedLine - 1; x++)
			{
				(Detail.Controls["txtDedDesc" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 3);
				(Detail.Controls["txtDedAmount" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 4);
				(Detail.Controls["txtDedYTD" + x + 1] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Grid.TextMatrix(x, 5);
			}
			// x
			// For x = 1 To Detail.Controls.Count
			// 
			// Next x
			dblDirectDepChk = Conversion.Val(GridMisc.TextMatrix(0, 29));
			dblDirectDepSav = Conversion.Val(GridMisc.TextMatrix(0, 34));
			dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
			txtDate.Text = "DATE " + Strings.Format(GridMisc.TextMatrix(0, 24), "MM/dd/yyyy");
			tCheckRecord.CheckDate = FCConvert.ToDateTime(Strings.Format(GridMisc.TextMatrix(0, 24), "MM/dd/yyyy"));
			tCheckRecord.SocialSecurityNumber = FCConvert.ToString(clsLoad.Get_Fields("ssn"));
			txtCurrentGross.Text = GridMisc.TextMatrix(0, 5);
			txtCurrentFed.Text = GridMisc.TextMatrix(0, 6);
			txtCurrentFica.Text = GridMisc.TextMatrix(0, 7);
			txtCurrentState.Text = GridMisc.TextMatrix(0, 8);
			txtCurrentDeductions.Text = GridMisc.TextMatrix(0, 23);
			txtCurrentNet.Text = GridMisc.TextMatrix(0, 9);
			tCheckRecord.CheckAmount = Conversion.Val(GridMisc.TextMatrix(0, 9));
			txtVacationBalance.Text = GridMisc.TextMatrix(0, 0);
			txtSickBalance.Text = GridMisc.TextMatrix(0, 1);
			txtOtherBalance.Text = GridMisc.TextMatrix(0, 33);
			txtYTDGross.Text = GridMisc.TextMatrix(0, 13);
			txtYTDFed.Text = GridMisc.TextMatrix(0, 14);
			txtYTDFICA.Text = GridMisc.TextMatrix(0, 15);
			txtYTDState.Text = GridMisc.TextMatrix(0, 16);
			txtYTDNet.Text = GridMisc.TextMatrix(0, 17);
			txtTotalHours.Text = GridMisc.TextMatrix(0, 3);
			txtTotalAmount.Text = GridMisc.TextMatrix(0, 4);
			txtEMatchCurrent.Text = GridMisc.TextMatrix(0, 25);
			txtEmatchYTD.Text = GridMisc.TextMatrix(0, 26);
			txtEmployeeNo.Text = GridMisc.TextMatrix(0, 2);
			txtName.Text = GridMisc.TextMatrix(0, 18);
			tCheckRecord.EMPLOYEENAME = GridMisc.TextMatrix(0, 18);
			if (dblDirectDeposit > 0)
			{
				tCheckRecord.DIRECTDEPOSIT = true;
			}
			else
			{
				tCheckRecord.DIRECTDEPOSIT = false;
			}
			tCheckRecord.CheckNumber = lngCurrentCheckNumber;
			tCheckRecord.Payrun = intPayrunToUse;
			txtCheckNo.Text = GridMisc.TextMatrix(0, 12);
			tCheckRecord.SetPDFFileName();
			strTag = tCheckRecord.PDFFile;
			// Me.Canvas.Tag = strTag
			if (dblDirectDeposit > 0)
			{
				txtDirectDeposit.Visible = true;
				txtDirectDepositLabel.Visible = true;
				txtChkAmount.Visible = true;
				lblCheckAmount.Visible = true;
				txtDirectDepChkLabel.Visible = true;
				txtDirectDepositSavLabel.Visible = true;
				txtDirectDepSav.Visible = true;
				txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
				txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
				txtChkAmount.Text = Strings.Format(Conversion.Val(GridMisc.TextMatrix(0, 9)) - dblDirectDeposit, "0.00");
			}
			else
			{
				txtDirectDeposit.Visible = false;
				txtDirectDepositLabel.Visible = false;
				txtChkAmount.Visible = false;
				lblCheckAmount.Visible = false;
				txtChkAmount.Text = Strings.Format(Conversion.Val(GridMisc.TextMatrix(0, 9)), "0.00");
			}
			if (fecherFoundation.Strings.Trim(GridMisc.TextMatrix(0, 31)) == string.Empty)
			{
				lblPayRate.Visible = false;
				txtPayRate.Visible = false;
			}
			else
			{
				lblPayRate.Visible = true;
				txtPayRate.Visible = true;
				txtPayRate.Text = fecherFoundation.Strings.Trim(GridMisc.TextMatrix(0, 31));
			}
			// MATTHEW 8/18/2004
			// ADD THE THREE NEW 'OTHER' VACATION/SICK CODES
			lblCode1.Text = GridMisc.TextMatrix(0, 39);
			lblCode2.Text = GridMisc.TextMatrix(0, 40);
			lblCode3.Text = GridMisc.TextMatrix(0, 41);
			// WE DO NOT WANT THE OTHER FIELDS TO SHOW IF THERE
			// WERE NO TYPES DEFINED BY THE USER.
			if (lblCode1.Text != string.Empty)
			{
				lblCode1Balance.Text = GridMisc.TextMatrix(0, 36);
			}
			else
			{
				lblCode1Balance.Text = string.Empty;
			}
			if (lblCode2.Text != string.Empty)
			{
				lblCode2Balance.Text = GridMisc.TextMatrix(0, 37);
			}
			else
			{
				lblCode2Balance.Text = string.Empty;
			}
			if (lblCode3.Text != string.Empty)
			{
				lblCode3Balance.Text = GridMisc.TextMatrix(0, 38);
			}
			else
			{
				lblCode3Balance.Text = string.Empty;
			}
			clsCheckList.AddItem(ref tCheckRecord);
		}
		// vbPorter upgrade warning: dtPayDateToUse As DateTime	OnWrite(string)
		// vbPorter upgrade warning: intPayrun As int	OnWriteFCConvert.ToDouble(
		public void Init(DateTime dtPayDateToUse, int intPayrun, string strExportDir)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				string strPayTemp = "";
				string strDedTemp = "";
				string strFonttoUse = "";
				int intReturn;
				string strOrderBy;
				string strWhere;
				int lngAlignment;
				string strVac1 = "";
				string strVac2 = "";
				string strVac3 = "";
				string strDed1;
				string strDed2;
				string strPayQuery1;
				string strPayQuery1a;
				string strPayQuery1b = "";
				string strPayQuery2;
				string strPayQuery3;
				string strPayQuery4;
				string strPayQuery5;
				string strPayQuery6;
				string strDDQuery1 = "";
				string strTandAQuery = "";
				string strTandAFed = "";
				string strTandAFica = "";
				string strTandAMed = "";
				string strTandAState = "";
				string strTandALocal = "";
				int lngACHBank;
                using (clsDRWrapper rsVacSickCodeTypes = new clsDRWrapper())
                {
                    int lngVacCodeID = 0;
                    int lngSickCodeID = 0;
                    bool boolTwoStubs;
                    // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
                    DateTime dtStart;
                    DateTime dtEnd;
                    strDirForExport = strExportDir;
                    intPayrunToUse = intPayrun;
                    clsBanks.OpenRecordset("select * from tblbanks order by ID", "twpy0000.vb1");
                    clsStates.OpenRecordset("Select ID,state from states order by id", "twpy0000.vb1");
                    strOrderBy =
                        " payrunid,tblcheckdetail.employeenumber,lastname,firstname,tblcheckdetail.masterrecord";
                    // checktype 0 = paychecks, 1 = directdeposit to bank,2 = T&A checks
                    intMaxStubLines = CNSTMAXSTUBLINES;
                    intMaxStubDDLines = 9;
                    //Application.DoEvents();
                    frmWait.InstancePtr.Init("Loading Check Information");
                    Grid.Rows = 100;
                    clsLoad.OpenRecordset("select max (deductionnumber) as maxnum from tbldeductionsetup",
                        "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        if (Conversion.Val(clsLoad.Get_Fields("maxnum") + "") > Grid.Rows)
                        {
                            Grid.Rows = Conversion.Val(clsLoad.Get_Fields("maxnum"));
                        }
                    }

                    GridMisc.Rows = 1;
                    GridMisc.Cols = 46;
                    GridMisc.TextMatrix(0, 30, 1);
                    dtPayDate = dtPayDateToUse;
                    GridMisc.TextMatrix(0, 24, dtPayDate);
                    GridMisc.TextMatrix(0, 28, 0);
                    // get descriptions for pay categories
                    clsLoad.OpenRecordset("select * from tblpaycategories", "twpy0000.vb1");
                    intNumPays = clsLoad.RecordCount();
                    while (!clsLoad.EndOfFile())
                    {
                        //Application.DoEvents();
                        arPayDescriptions[clsLoad.Get_Fields("categorynumber")] =
                            fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                        arPayCodes[clsLoad.Get_Fields("categorynumber")] = FCConvert.ToInt32(clsLoad.Get_Fields("ID"));
                        arPayTypes[clsLoad.Get_Fields("categorynumber")] =
                            FCConvert.ToString(clsLoad.Get_Fields("Type"));
                        Grid.TextMatrix(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("categorynumber")) - 1), 0,
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
                        clsLoad.MoveNext();
                    }

                    // get descriptions for deductions
                    clsLoad.OpenRecordset("select * from tbldeductionsetup", "twpy0000.vb1");
                    intNumDeds = clsLoad.RecordCount();
                    while (!clsLoad.EndOfFile())
                    {
                        //Application.DoEvents();
                        arDedCodes[clsLoad.Get_Fields("deductionnumber")] = FCConvert.ToInt32(clsLoad.Get_Fields("ID"));
                        arDedDescriptions[clsLoad.Get_Fields("deductionnumber")] =
                            FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        Grid.TextMatrix(clsLoad.Get_Fields_Int32("DeductionNumber") - 1, 3,
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
                        clsLoad.MoveNext();
                    }

                    // open the recordsets
                    // If boolReprint Then
                    // strWhere = " checknumber between " & lngStartingReprintNumber & " and " & lngEndingReprintNumber & " "
                    // Else
                    // strWhere = " checknumber = 0 "
                    // End If
                    strWhere = " checknumber > 0 ";
                    GridMisc.TextMatrix(0, 45, strWhere);
                    // If boolReprint Then
                    // strSQL = "select employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail where paydate = '" & dtPayDate & "' and payrunid = " & intpayruntouse & " and convert(int, isnull(ddamount, 0)) > 0  order by employeenumber,masterrecord"
                    strSQL =
                        "select tblcheckdetail.employeenumber as employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail inner join tbldirectdeposit on (convert(int, isnull(tblcheckdetail.ddbanknumber, '')) = tbldirectdeposit.bank) and  (tblcheckdetail.ddaccountnumber = tbldirectdeposit.account) and (tblcheckdetail.employeenumber = tbldirectdeposit.employeenumber) where checkvoid = 0 and  paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunToUse) +
                        "  and convert(int, isnull(ddamount, 0)) > 0 order by tblcheckdetail.employeenumber,masterrecord,rownumber";
                    // Else
                    // strSQL = "select employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail where paydate = '" & dtPayDate & "' and payrunid = " & intpayruntouse & " and " & strWhere & " and convert(int, isnull(ddamount, 0)) > 0 order by employeenumber,masterrecord"
                    // strSQL = "select tblcheckdetail.employeenumber as employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail inner join tbldirectdeposit on (convert(int, isnull(tblcheckdetail.ddbanknumber, '')) = tbldirectdeposit.bank) and (tblcheckdetail.ddaccountnumber = tbldirectdeposit.account) and  (tblcheckdetail.employeenumber = tbldirectdeposit.employeenumber) where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & intpayruntouse & " and " & strWhere & " and convert(int, isnull(ddamount, 0)) > 0 order by tblcheckdetail.employeenumber,masterrecord,rownumber"
                    // 
                    // End If
                    clsDDDeductions.OpenRecordset(strSQL, "twpy0000.vb1");
                    // get only total records to join with
                    // If boolReprint Then
                    strSQL =
                        "select employeenumber,masterrecord,sum(convert(int, isnull(ddamount, 0))) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunToUse) +
                        "  and (left(DDACCOUNTTYPE & '  ',2) = '22' or left(rtrim(isnull(ddaccounttype, ''))  & '  ',2) = '  ') group by employeenumber,masterrecord";
                    // Else
                    // strSQL = "select employeenumber,masterrecord,sum(convert(int, isnull(ddamount, 0))) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & intpayruntouse & " and " & strWhere & " and (left(DDACCOUNTTYPE & '  ',2) = '22' or left(rtrim(isnull(ddaccounttype, '')) & '  ',2) = '  ') group by employeenumber,masterrecord"
                    // End If
                    strPayQuery1 = "(" + strSQL + ") as CheckPayQuery1 ";
                    // If boolReprint Then
                    strSQL =
                        "select employeenumber,masterrecord as masterrecorda,sum(convert(int, isnull(ddamount, 0))) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunToUse) +
                        " and left(DDACCOUNTTYPE & '  ',2) = '32' group by employeenumber,masterrecord";
                    // Else
                    // strSQL = "select employeenumber,masterrecord as masterrecorda,sum(convert(int, isnull(ddamount, 0))) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & intpayruntouse & " and " & strWhere & " and left(DDACCOUNTTYPE & '  ',2) = '32' group by employeenumber,masterrecord"
                    // End If
                    strPayQuery1a = "(" + strSQL + ") as CheckPayQuery1a ";
                    // now combine these two as one so we can right join wiht the rest
                    dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
                    strSQL =
                        "select employeenumber,sum(convert(int, isnull(distgrosspay, 0))) as YTDGross from tblcheckdetail where checkvoid = 0  and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber HAVING SUM(convert(int, isnull(distgrosspay, 0))) > 0";
                    // strSQL = "select employeenumber,sum(CalendarYTDTotal) as YTDGross from tblpaytotals where paycategory > 0 group by employeenumber"
                    strPayQuery2 = "(" + strSQL + ") as CheckPayQuery2 ";
                    strSQL =
                        "select employeenumber,sum(convert(int, isnull(FederalTaxWH, 0))) as YTDFedTax from tblcheckdetail where checkvoid = 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber ";
                    // strSQL = "select employeenumber,CalendarYTDTotal as YTDFedTax from tblpaytotals where standardpaytotal = 1 "
                    strPayQuery3 = "(" + strSQL + ") as CheckPayQuery3 ";
                    strSQL =
                        "select employeenumber,sum(convert(int, isnull(ficataxwh, 0)) + convert(int, isnull(medicaretaxwh, 0))) as YTDFica from tblcheckdetail where checkvoid = 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber ";
                    // strSQL = "select employeenumber,sum(CalendarYTDTotal) as YTDFICA from tblpaytotals where standardpaytotal = 2 or standardpaytotal = 3 group by employeenumber"
                    strPayQuery4 = "(" + strSQL + ") as CheckPayQuery4 ";
                    strSQL =
                        "select employeenumber,sum(convert(int, isnull(statetaxwh, 0))) as ytdstatetax from tblcheckdetail where checkvoid = 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber ";
                    // strSQL = "select employeenumber,CalendarYTDTotal as YTDStateTax from tblpaytotals where standardpaytotal = 4 "
                    strPayQuery5 = "(" + strSQL + ") as CheckPayQuery5 ";
                    strSQL = "select * from " + strPayQuery1a + " right join (" + strPayQuery1 + " right join (" +
                             strPayQuery2 + " inner join (" + strPayQuery3 + " inner join (" + strPayQuery4 +
                             " inner join " + strPayQuery5 +
                             " on (checkpayquery4.employeenumber = checkpayquery5.employeenumber)) on (checkpayquery3.employeenumber = checkpayquery4.employeenumber))";
                    strSQL +=
                        " on (checkpayquery2.employeenumber = checkpayquery3.employeenumber)) on (checkpayquery1.employeenumber = checkpayquery2.employeenumber)) on (checkpayquery1a.employeenumber = checkpayquery1.employeenumber) and (checkpayquery1a.masterrecorda = checkpayquery1.masterrecord";
                    strPayQuery6 = "(" + strSQL + ")) as CheckPayQuery6 ";
                    strSQL = "select * from " + strPayQuery6 +
                             " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.checkpayquery5.employeenumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" +
                             FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) +
                             " and TotalRecord = 1 and " + strWhere + "  order by " + strOrderBy;
                    // *************
                    // Matthew 10/28/2003
                    // strSQL = "select * from " & strPayQuery6 & " right join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.checkpayquery5.employeenumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" & dtPayDate & "' and PayRunID = " & intpayruntouse & " and TotalRecord = 1 and " & strWhere & "  order by " & strOrderby
                    // *************
                    // strSQL = "select * from tblemployeemaster inner join(tblcheckDetail inner join CheckPayQuery6 on (tblcheckdetail.employeenumber = checkpayquery6.checkpayquery5.employeenumber) and (tblcheckdetail.masterrecord = checkpayquery6.masterrecord)) on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where tblCheckDetail.paydate = '" & dtPayDate & "' and PayRunID = " & intpayruntouse & " and TotalRecord = 1 and " & strWhere & "  order by " & strOrderBy
                    clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    strSQL =
                        "select employeenumber,masterrecord,distpaycategory,sum(distgrosspay) as dgrosspay,sum(disthours) as distributionhours from tblcheckdetail where paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) +
                        " and " + strWhere +
                        " and distributionrecord = 1 group by employeenumber,masterrecord,distpaycategory order by employeenumber,masterrecord,distpaycategory";
                    clsPay.OpenRecordset(strSQL, "twpy0000.vb1");
                    strSQL =
                        "select sum(dedamount) as YTDDeduction,deddeductionnumber as deductioncode,employeenumber from tblcheckdetail where checkvoid = 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber,deddeductionnumber ";
                    // strSQL = "select sum(Calendarytdtotal) as YTDDeduction,deductioncode,employeenumber from tblemployeedeductions group by employeenumber,deductioncode"
                    strDed1 = "(" + strSQL + ") as CheckDedQuery1 ";
                    clsYTDDeductions.OpenRecordset(strSQL + " order by employeenumber,deddeductionnumber ",
                        "twpy0000.vb1");
                    // *************************************************************************************************
                    // *************************************************************************************************
                    // MATTHEW 10/7/2004
                    // THIS WAS BASED ON THE DEDUCTION SQL AS I NEED TO GET THE MATCH AMOUNT FROM TBLCHECKDETAIL
                    // SO THAT IF TWO MATCH AMOUNTS WERE GOTTEN FROM DIFFERENT CHECKS THEN I COULD SHOW THE
                    // BREAKOUT WHERE IF IT CAME FROM THE MATCH SCREEN THIS THIS IS JUST THE SUM OF ALL CHECKS
                    // FOR THAT PAY DATE.
                    // strSQL = "select employeenumber,sum(CURRENTTOTAL) as CurrMatch,sum(CalendarYTDTotal) as YTDMatch from tblemployersmatch group by employeenumber"
                    // THIS WAS PROMPTED BY DEBBIE IN TOPSHAM CALL ID 56550
                    // strSQL = "select employeenumber,masterrecord,matchdeductionnumber,sum(matchamount) as CurrentMatchAmount from tblcheckdetail where paydate = '" & dtPayDate & "' and PayRunID = " & intpayruntouse & " and " & strWhere & " and  matchrecord = 1 group by employeenumber,masterrecord,matchdeductionnumber"
                    // COREY 1/7/05
                    // changed for camden. grouping by matchnumber gave total for each match which is not what we want
                    strSQL =
                        "select employeenumber,masterrecord,sum(matchamount) as CurrentMatchAmount from tblcheckdetail where paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) +
                        " and " + strWhere + " and  matchrecord = 1 group by employeenumber,masterrecord";
                    strDed2 = "(" + strSQL + ") AS CheckDedQuery2 ";
                    // strSQL = "select * from " & strDed2 & " RIGHT JOIN [select sum(Calendarytdtotal) as YTDMatch,employeenumber,DeductionCode from tblemployersMatch group by employeenumber,deductioncode]. AS CheckDedQuery1 ON (CheckDedQuery2.matchdeductionnumber = CheckDedQuery1.deductioncode) AND (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;"
                    // strSQL = "select * from " & strDed2 & " RIGHT JOIN [select sum(Calendarytdtotal) as YTDMatch,employeenumber,DeductionCode from tblemployersMatch group by employeenumber,deductioncode]. AS CheckDedQuery1 ON  (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;"
                    // COREY 1/7/05
                    // changed for camden. grouping by matchnumber gave total for each match which is not what we want
                    strSQL = "select * from " + strDed2 +
                             " right join [select sum(matchamount) as YTDMatch,employeenumber from tblcheckdetail where checkvoid = 0 and paydate between '" +
                             FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                             "' group by employeenumber]. as CheckDedQuery1 ON  (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;";
                    // strSQL = "select * from " & strDed2 & " RIGHT JOIN [select sum(Calendarytdtotal) as YTDMatch,employeenumber from tblemployersMatch group by employeenumber]. AS CheckDedQuery1 ON  (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;"
                    clsEMatch.OpenRecordset(strSQL, "twpy0000.vb1");
                    // *************************************************************************************************
                    // *************************************************************************************************
                    strSQL =
                        "select sum(dedamount) as YTDDeduction,deddeductionnumber as deductioncode,employeenumber from tblcheckdetail where checkvoid = 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) +
                        "' group by employeenumber,deddeductionnumber";
                    // strSQL = "select sum(Calendarytdtotal) as YTDDeduction,deductioncode,employeenumber from tblemployeedeductions group by employeenumber,deductioncode"
                    strDed1 = "(" + strSQL + ") as CheckDedQuery1 ";
                    clsYTDDeductions.OpenRecordset(strSQL + " order by employeenumber,deddeductionnumber ",
                        "twpy0000.vb1");
                    //Application.DoEvents();
                    strSQL =
                        "select employeenumber,masterrecord,deddeductionnumber,sum(dedamount) as deductionamount,empdeductionid from tblcheckdetail where paydate = '" +
                        FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) +
                        " and " + strWhere +
                        " and  deductionrecord = 1 group by employeenumber,masterrecord,deddeductionnumber,empdeductionid";
                    strDed2 = "(" + strSQL + ") as CheckDedQuery2 ";
                    // strSQL = "select * from " & strDed2 & " right join " & strDed1 & " on (checkdedquery1.employeenumber = checkdedquery2.employeenumber) and (checkdedquery2.deddeductionnumber = checkdedquery1.deductioncode) order by checkdedquery1.employeenumber,masterrecord DESC,deductioncode DESC"
                    strSQL = "select * from (select * from " + strDed2 + " right join " + strDed1 +
                             " on (checkdedquery1.employeenumber = checkdedquery2.employeenumber) and (checkdedquery2.deddeductionnumber = checkdedquery1.deductioncode)) as checkdedquery3  LEFT join tblemployeedeductions on (checkdedquery3.empdeductionid = tblemployeedeductions.ID) order by checkdedquery1.employeenumber,masterrecord DESC,recordnumber";
                    clsDeductions.OpenRecordset(strSQL, "twpy0000.vb1");
                    // Vacation sick information
                    rsVacSickCodeTypes.OpenRecordset("Select * from tblCodeTypes", "TWPY0000.vb1");
                    rsVacSickCodeTypes.FindFirstRecord("Description", "Vacation");
                    if (rsVacSickCodeTypes.NoMatch)
                    {
                        MessageBox.Show("Error finding Vacation ID code", "TRIO Software", MessageBoxButtons.OK,
                            MessageBoxIcon.Hand);
                    }
                    else
                    {
                        lngSickCodeID = FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID"));
                        strSQL =
                            "Select accruedCalendar as accruedCalendarvacation,usedCalendar as usedCalendarvacation,usedbalance as vacationbalance,employeenumber from tblvacationsick where selected = 1 and typeid = " +
                            FCConvert.ToString(lngSickCodeID);
                    }

                    clsVacation.OpenRecordset(strSQL, "twpy0000.vb1");
                    rsVacSickCodeTypes.FindFirstRecord("Description", "Sick");
                    if (rsVacSickCodeTypes.NoMatch)
                    {
                        MessageBox.Show("Error finding Sick ID code", "TRIO Software", MessageBoxButtons.OK,
                            MessageBoxIcon.Hand);
                    }
                    else
                    {
                        lngVacCodeID = FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID"));
                        strSQL =
                            "select accruedCalendar as accruedCalendarsick,usedCalendar as usedCalendarsick,usedbalance as sickbalance,employeenumber from tblvacationsick where selected = 1 and typeid = " +
                            FCConvert.ToString(lngVacCodeID);
                    }

                    clsSick.OpenRecordset(strSQL, "twpy0000.vb1");
                    //Application.DoEvents();
                    // ***********************************************************************************************************************
                    // MATTHEW 8/18/2004
                    // ***********************************************************************************************************************
                    // now get the other vacation sick codes
                    int intTypeNumber;
                    int intFound;
                    int intCode1 = 0;
                    int intCode2 = 0;
                    int intCode3 = 0;
                    intFound = 0;
                    rsVacSickCodeTypes.MoveFirst();
                    GridMisc.TextMatrix(0, 39, string.Empty);
                    GridMisc.TextMatrix(0, 40, string.Empty);
                    GridMisc.TextMatrix(0, 41, string.Empty);
                    strSQL =
                        "select accruedCalendar,usedCalendar,usedbalance,employeenumber from tblvacationsick where typeid = 99999999";
                    clsVacSickOther1.OpenRecordset(strSQL, "twpy0000.vb1");
                    clsVacSickOther2.OpenRecordset(strSQL, "twpy0000.vb1");
                    clsVacSickOther3.OpenRecordset(strSQL, "twpy0000.vb1");
                    for (intTypeNumber = 1; intTypeNumber <= 3; intTypeNumber++)
                    {
                        NextTry: ;
                        if (FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID")) == lngSickCodeID)
                        {
                            if (!rsVacSickCodeTypes.EndOfFile())
                                rsVacSickCodeTypes.MoveNext();
                            if (!rsVacSickCodeTypes.EndOfFile())
                                goto NextTry;
                        }
                        else if (FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID")) == lngVacCodeID)
                        {
                            if (!rsVacSickCodeTypes.EndOfFile())
                                rsVacSickCodeTypes.MoveNext();
                            if (!rsVacSickCodeTypes.EndOfFile())
                                goto NextTry;
                        }
                        else
                        {
                            strSQL =
                                "select accruedCalendar,usedCalendar,usedbalance,employeenumber from tblvacationsick where selected = 1 and  typeid = " +
                                rsVacSickCodeTypes.Get_Fields("ID");
                            if (intTypeNumber == 1)
                            {
                                clsVacSickOther1.OpenRecordset(strSQL, "twpy0000.vb1");
                                intCode1 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                                if (fecherFoundation.Strings
                                        .Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length >
                                    19)
                                {
                                    GridMisc.TextMatrix(0, 39,
                                        Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")),
                                            20));
                                }
                                else
                                {
                                    // MATTHEW 8/2/2005
                                    GridMisc.TextMatrix(0, 39, rsVacSickCodeTypes.Get_Fields("Description"));
                                }

                                intFound = 1;
                            }
                            else if (intTypeNumber == 2)
                            {
                                clsVacSickOther2.OpenRecordset(strSQL, "twpy0000.vb1");
                                intCode2 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                                if (fecherFoundation.Strings
                                        .Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length >
                                    19)
                                {
                                    GridMisc.TextMatrix(0, 40,
                                        Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")),
                                            20));
                                }
                                else
                                {
                                    // MATTHEW 8/2/2005
                                    GridMisc.TextMatrix(0, 40, rsVacSickCodeTypes.Get_Fields("Description"));
                                }

                                intFound = 2;
                            }
                            else if (intTypeNumber == 3)
                            {
                                clsVacSickOther3.OpenRecordset(strSQL, "twpy0000.vb1");
                                intCode3 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                                if (fecherFoundation.Strings
                                        .Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length >
                                    19)
                                {
                                    GridMisc.TextMatrix(0, 41,
                                        Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")),
                                            20));
                                }
                                else
                                {
                                    // MATTHEW 8/2/2005
                                    GridMisc.TextMatrix(0, 41, rsVacSickCodeTypes.Get_Fields("Description"));
                                }

                                intFound = 3;
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (!rsVacSickCodeTypes.EndOfFile())
                            rsVacSickCodeTypes.MoveNext();
                        if (rsVacSickCodeTypes.EndOfFile())
                            break;
                    }

                    // ***********************************************************************************************************************
                    if (intFound == 1)
                    {
                        strSQL =
                            "select SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " +
                            FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) +
                            " AND TypeID <> " + FCConvert.ToString(intCode1) + " group by employeenumber";
                    }
                    else if (intFound == 2)
                    {
                        strSQL =
                            "select SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " +
                            FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) +
                            " AND TypeID <> " + FCConvert.ToString(intCode1) + " AND TypeID <> " +
                            FCConvert.ToString(intCode2) + " group by employeenumber";
                    }
                    else if (intFound == 3)
                    {
                        strSQL =
                            "select SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " +
                            FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) +
                            " AND TypeID <> " + FCConvert.ToString(intCode1) + " AND TypeID <> " +
                            FCConvert.ToString(intCode2) + " AND TypeID <> " + FCConvert.ToString(intCode3) +
                            " group by employeenumber";
                    }
                    else
                    {
                        strSQL =
                            "select SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " +
                            FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) +
                            " group by employeenumber";
                    }

                    clsVacSick.OpenRecordset(strSQL, "twpy0000.vb1");
                    frmWait.InstancePtr.Unload();
                    if (!clsLoad.EndOfFile())
                    {
                        lngCurrentCheckNumber = FCConvert.ToInt32(clsLoad.Get_Fields("checknumber"));
                        if (Conversion.Val(clsLoad.Get_Fields("ddepositchkamount")) +
                            Conversion.Val(clsLoad.Get_Fields("ddepositsavamount")) > 0)
                        {
                            strCurrentCheckNumber = "D" + FCConvert.ToString(lngCurrentCheckNumber);
                        }
                        else
                        {
                            strCurrentCheckNumber = FCConvert.ToString(lngCurrentCheckNumber);
                        }
                    }
                }

                frmReportViewer.InstancePtr.Init(this, boolOverRideDuplex: true, boolAllowEmail: false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init");
			}
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= CNSTMAXSTUBLINES; x++)
			{
				(Detail.Controls["txtPayDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtHours" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtPayAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtDedDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtDedAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtDedYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			// x
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			int intRow = 0;
			double dblTotGross = 0;
			double dblTotHours = 0;
			double dblTotDeductions = 0;
			double dblNet = 0;
			string strSQL = "";
			int intTemp = 0;
			int intCurrentPayRow = 0;
			int intCurrentDedRow = 0;
			double dblExcessAmount = 0;
			double dblExcessHours = 0;
			double dblExcessYTD = 0;
			double dblDirectDeposit = 0;
			double dblDirectDepositChk = 0;
			double dblDirectDepositSav = 0;
			double dblTrustAmount;
			string strState = "";
			string strTemp = "";
			bool boolTemp = false;
			double dblTemp = 0;
			strTag = "";
			ClearFields();
			if (!clsLoad.EndOfFile())
			{
				// paychecks
				dblTotGross = 0;
				dblTotHours = 0;
				dblTotDeductions = 0;
				dblNet = 0;
				dblExcessAmount = 0;
				dblExcessHours = 0;
				dblExcessYTD = 0;
				dblDirectDeposit = 0;
				intTemp = intNumPays;
				if (intNumPays > intMaxStubLines)
					intTemp = intMaxStubLines;
				for (x = 1; x <= intMaxStubLines; x++)
				{
					Grid.TextMatrix(x - 1, 0, "");
					Grid.TextMatrix(x - 1, 1, "");
					Grid.TextMatrix(x - 1, 2, "");
				}
				// x
				intTemp = intNumDeds;
				if (intTemp > intMaxStubLines)
					intTemp = intMaxStubLines;
				for (x = 1; x <= intMaxStubLines; x++)
				{
					Grid.TextMatrix(x - 1, 3, "");
					Grid.TextMatrix(x - 1, 4, "");
					Grid.TextMatrix(x - 1, 5, "");
				}
				// x
				intCurrentPayRow = 0;
				intCurrentDedRow = 0;
				// do pay categories first
				if (clsPay.FindFirstRecord2("employeenumber,masterrecord", clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "," + clsLoad.Get_Fields("tblcheckdetail.Masterrecord"), ","))
				{
					while (clsPay.Get_Fields("employeenumber") == clsLoad.Get_Fields("tblemployeemaster.employeenumber") && (clsLoad.Get_Fields("tblcheckdetail.masterrecord") == clsPay.Get_Fields("masterrecord")))
					{
						intRow = 0;
						for (x = 1; x <= intNumPays; x++)
						{
							if (arPayCodes[x] == Conversion.Val(clsPay.Get_Fields("distpaycategory")))
							{
								intRow = x;
								break;
							}
						}
						// x
						if (intRow == 0)
							intRow = intMaxStubLines;
						intCurrentPayRow += 1;
						if (intCurrentPayRow < intMaxStubLines + 1)
						{
							Grid.TextMatrix(intCurrentPayRow - 1, 0, arPayDescriptions[intRow]);
							Grid.TextMatrix(intCurrentPayRow - 1, 2, Strings.Format(Conversion.Val(clsPay.Get_Fields("DGrossPay")), "0.00"));
							dblTotGross += Conversion.Val(clsPay.Get_Fields("dgrosspay"));
							if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "DOLLARS")
							{
								if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "NON-PAY (HOURS)")
								{
									dblTotHours += Conversion.Val(clsPay.Get_Fields("distributionhours"));
									Grid.TextMatrix(intCurrentPayRow - 1, 1, Strings.Format(Conversion.Val(clsPay.Get_Fields("DistributionHours")), "0.00"));
								}
								else
								{
									if (Conversion.Val(clsPay.Get_Fields("distributionhours")) < 0)
									{
										Grid.TextMatrix(intCurrentPayRow - 1, 1, Strings.Format(-1 * Conversion.Val(clsPay.Get_Fields("DistributionHours")), "0.00"));
									}
									else
									{
										Grid.TextMatrix(intCurrentPayRow - 1, 1, "+" + Strings.Format(Conversion.Val(clsPay.Get_Fields("DistributionHours")), "0.00"));
									}
								}
							}
							else
							{
								Grid.TextMatrix(intCurrentPayRow - 1, 1, "");
							}
							if (intCurrentPayRow == intMaxStubLines)
							{
								dblExcessAmount = Conversion.Val(clsPay.Get_Fields("dgrosspay"));
								if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "DOLLARS")
								{
									dblExcessHours = Conversion.Val(clsPay.Get_Fields("distributionhours"));
								}
								else
								{
									dblExcessHours = 0;
								}
							}
						}
						else
						{
							if ((Conversion.Val(clsPay.Get_Fields("dgrosspay")) > 0) || (Conversion.Val(clsPay.Get_Fields("distributionhours")) > 0))
							{
								dblTotGross += Conversion.Val(clsPay.Get_Fields("dgrosspay"));
								if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "DOLLARS")
								{
									dblTotHours += Conversion.Val(clsPay.Get_Fields("distributionhours"));
									dblExcessHours += Conversion.Val(clsPay.Get_Fields("distributionhours"));
								}
								dblExcessAmount += Conversion.Val(clsPay.Get_Fields("dgrosspay"));
								Grid.TextMatrix(intMaxStubLines - 1, 0, "Other");
								Grid.TextMatrix(intMaxStubLines - 1, 1, Strings.Format(dblExcessHours, "0.00"));
								Grid.TextMatrix(intMaxStubLines - 1, 2, Strings.Format(dblExcessAmount, "0.00"));
							}
						}
						clsPay.MoveNext();
						if (clsPay.EndOfFile())
							break;
					}
				}
				dblExcessAmount = 0;
				double dblTotalYTDDeductions = 0;
				dblTotalYTDDeductions = 0;
				if (FCConvert.ToInt32(clsLoad.Get_Fields("tblemployeemaster.employeenumber")) == 723)
				{
					dblTotalYTDDeductions = dblTotalYTDDeductions;
				}
				// do deductions next
				if (clsDeductions.FindFirstRecord2("checkdedquery1.employeenumber,masterrecord", clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "," + clsLoad.Get_Fields("tblcheckdetail.masterrecord"), ","))
				{
					// MATTHEW 8/16/2005 CALL ID 75130
					// Do While clsDeductions.Fields("checkdedquery1.employeenumber") = clsLoad.Fields("tblemployeemaster.employeenumber")
					while (clsDeductions.Get_Fields("checkdedquery1.employeenumber") == clsLoad.Get_Fields("tblemployeemaster.employeenumber") && (clsDeductions.Get_Fields("masterrecord") == clsLoad.Get_Fields("tblcheckdetail.masterrecord") || (fecherFoundation.Strings.Trim(FCConvert.ToString(clsDeductions.Get_Fields("masterrecord"))) == "" && fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("tblcheckdetail.masterrecord"))) == "P0")))
					{
						intRow = 0;
						for (x = 1; x <= 150; x++)
						{
							// If arDedCodes(X) = Val(clsDeductions.Fields("deddeductionnumber")) Then
							if (arDedCodes[x] == Conversion.Val(clsDeductions.Get_Fields("checkdedquery3.DeductionCode")))
							{
								intRow = x;
								break;
							}
						}
						// x
						if ((Conversion.Val(clsDeductions.Get_Fields("deductionamount")) > 0) || (Conversion.Val(clsDeductions.Get_Fields("ytddeduction")) > 0))
						{
							intCurrentDedRow += 1;
							if (intCurrentDedRow < intMaxStubLines + 1)
							{
								Grid.TextMatrix(intCurrentDedRow - 1, 3, arDedDescriptions[intRow]);
								Grid.TextMatrix(intCurrentDedRow - 1, 4, Strings.Format(Conversion.Val(clsDeductions.Get_Fields("DeductionAmount")), "0.00"));
								Grid.TextMatrix(intCurrentDedRow - 1, 5, Strings.Format(Conversion.Val(clsDeductions.Get_Fields("YTDDeduction")), "0.00"));
								dblTotDeductions += Conversion.Val(clsDeductions.Get_Fields("deductionamount"));
								dblTotalYTDDeductions += Conversion.Val(clsDeductions.Get_Fields("YTDDeduction"));
								if (intCurrentDedRow == intMaxStubLines)
								{
									dblExcessAmount = Conversion.Val(clsDeductions.Get_Fields("deductionamount"));
									dblExcessYTD = Conversion.Val(clsDeductions.Get_Fields("YTDDeduction"));
								}
							}
							else
							{
								dblTotDeductions += Conversion.Val(clsDeductions.Get_Fields("deductionamount"));
								dblTotalYTDDeductions += Conversion.Val(clsDeductions.Get_Fields("YTDDeduction"));
								dblExcessAmount += Conversion.Val(clsDeductions.Get_Fields("deductionamount"));
								dblExcessYTD += Conversion.Val(clsDeductions.Get_Fields("ytddeduction"));
								Grid.TextMatrix(intMaxStubLines - 1, 4, Strings.Format(dblExcessAmount, "0.00"));
								Grid.TextMatrix(intMaxStubLines - 1, 5, Strings.Format(dblExcessYTD, "0.00"));
								Grid.TextMatrix(intMaxStubLines - 1, 3, "Other");
							}
						}
						clsDeductions.MoveNext();
						if (clsDeductions.EndOfFile())
							break;
					}
				}
				dblTotalYTDDeductions = 0;
				if (!(clsYTDDeductions.EndOfFile() && clsDeductions.BeginningOfFile()))
				{
					clsDeductions.MoveFirst();
					// still need to get ytd total to figure ytd net
					if (clsYTDDeductions.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
					{
						while (clsYTDDeductions.Get_Fields("employeenumber") == clsLoad.Get_Fields("tblemployeemaster.employeenumber"))
						{
							dblTotalYTDDeductions += Conversion.Val(clsYTDDeductions.Get_Fields("YTDDeduction"));
							clsYTDDeductions.MoveNext();
							if (clsYTDDeductions.EndOfFile())
								break;
						}
					}
				}
				GridMisc.TextMatrix(0, 42, Strings.Format(dblTotalYTDDeductions, "0.00"));
				if (clsVacSick.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 33, Strings.Format(Conversion.Val(clsVacSick.Get_Fields("OtherBalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 33, "0.00");
				}
				if (clsVacation.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 0, Strings.Format(Conversion.Val(clsVacation.Get_Fields("vacationbalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 0, "0.00");
				}
				if (clsSick.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 1, Strings.Format(Conversion.Val(clsSick.Get_Fields("sickbalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 1, "0.00");
				}
				// 36 is code1
				if (clsVacSickOther1.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 36, Strings.Format(Conversion.Val(clsVacSickOther1.Get_Fields_Double("UsedBalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 36, "0.00");
				}
				// 37 is code2
				if (clsVacSickOther2.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 37, Strings.Format(Conversion.Val(clsVacSickOther2.Get_Fields_Double("UsedBalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 37, "0.00");
				}
				// 38 is code3
				if (clsVacSickOther3.FindFirstRecord("employeenumber", clsLoad.Get_Fields("tblemployeemaster.employeenumber")))
				{
					GridMisc.TextMatrix(0, 38, Strings.Format(Conversion.Val(clsVacSickOther3.Get_Fields_Double("UsedBalance")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 38, "0.00");
				}

				if (clsEMatch.FindFirstRecord2("CheckDedQuery1.employeenumber,masterrecord", clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "," + clsLoad.Get_Fields("tblcheckdetail.masterrecord"), ","))
				{
					GridMisc.TextMatrix(0, 25, Strings.Format(Conversion.Val(clsEMatch.Get_Fields("CurrentMatchAmount")), "0.00"));
					GridMisc.TextMatrix(0, 26, Strings.Format(Conversion.Val(clsEMatch.Get_Fields("YTDMatch")), "0.00"));
				}
				else
				{
					if (clsEMatch.FindFirstRecord2("CheckDedQuery1.employeenumber,masterrecord", clsLoad.Get_Fields("tblemployeemaster.employeenumber") + ",", ","))
					{
						GridMisc.TextMatrix(0, 25, Strings.Format(Conversion.Val(clsEMatch.Get_Fields("CurrentMatchAmount")), "0.00"));
						GridMisc.TextMatrix(0, 26, Strings.Format(Conversion.Val(clsEMatch.Get_Fields("YTDMatch")), "0.00"));
					}
					else
					{
						GridMisc.TextMatrix(0, 25, "0.00");
						GridMisc.TextMatrix(0, 26, "0.00");
					}
				}
				GridMisc.TextMatrix(0, 2, "EMPLOYEE " + clsLoad.Get_Fields("tblemployeemaster.employeenumber"));
				GridMisc.TextMatrix(0, 3, Strings.Format(dblTotHours, "0.00"));
				GridMisc.TextMatrix(0, 4, Strings.Format(dblTotGross, "0.00"));
				GridMisc.TextMatrix(0, 5, Strings.Format(dblTotGross, "0.00"));
				GridMisc.TextMatrix(0, 6, Strings.Format(Conversion.Val(clsLoad.Get_Fields("FederalTaxWH")), "0.00"));
				GridMisc.TextMatrix(0, 7, Strings.Format(Conversion.Val(clsLoad.Get_Fields("FICATaxWH")) + Conversion.Val(clsLoad.Get_Fields("MedicareTaxWH")), "0.00"));
				GridMisc.TextMatrix(0, 8, Strings.Format(Conversion.Val(clsLoad.Get_Fields("statetaxwh")), "0.00"));
				// txtCurrentDeductions.Caption = Format(dblTotDeductions, "0.00")
				GridMisc.TextMatrix(0, 23, Strings.Format(dblTotDeductions, "0.00"));
				dblNet = dblTotGross - Conversion.Val(clsLoad.Get_Fields("federaltaxwh")) - Conversion.Val(clsLoad.Get_Fields("medicaretaxwh")) - Conversion.Val(clsLoad.Get_Fields("ficataxwh")) - Conversion.Val(clsLoad.Get_Fields("statetaxwh"));
				dblNet -= dblTotDeductions;
				if (dblNet != Conversion.Val(clsLoad.Get_Fields("netpay")))
					dblNet = Conversion.Val(clsLoad.Get_Fields("netpay"));
				// dblDirectDeposit = Val(clsLoad.Fields("ddepositamount"))
				dblDirectDepositChk = Conversion.Val(clsLoad.Get_Fields("ddepositchkamount"));
				dblDirectDepositSav = Conversion.Val(clsLoad.Get_Fields("ddepositsavamount"));
				dblDirectDeposit = dblDirectDepositChk + dblDirectDepositSav;
				for (x = 1; x <= intMaxStubDDLines; x++)
				{
					Grid.TextMatrix(x - 1, 6, 0);
					Grid.TextMatrix(x - 1, 7, "");
				}
				// x
				if (dblDirectDeposit > 0)
				{
					boolTemp = false;
					if (!(clsDDDeductions.EndOfFile() && clsDDDeductions.BeginningOfFile()))
					{
						if (!clsDDDeductions.BeginningOfFile())
						{
							if (clsDDDeductions.EndOfFile())
								clsDDDeductions.MoveFirst();
						}
						if (!((clsDDDeductions.Get_Fields("employeenumber") == clsLoad.Get_Fields("tblemployeemaster.employeenumber")) && (clsDDDeductions.Get_Fields("masterrecord") == clsLoad.Get_Fields("tblcheckdetail.masterrecord"))))
						{
							if (clsDDDeductions.FindFirstRecord2("Employeenumber,masterrecord", clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "," + clsLoad.Get_Fields("tblcheckdetail.masterrecord"), ","))
							{
								boolTemp = true;
							}
						}
						else
						{
							boolTemp = true;
						}
					}
					if (boolTemp)
					{
						x = 1;
						dblTemp = 0;
						while (!clsDDDeductions.EndOfFile())
						{
							if (clsDDDeductions.Get_Fields("employeenumber") != clsLoad.Get_Fields("tblemployeemaster.employeenumber") || clsDDDeductions.Get_Fields("masterrecord") != clsLoad.Get_Fields("tblcheckdetail.masterrecord"))
							{
								// not the same check any more
								break;
							}
							if (x <= intMaxStubDDLines)
							{
								Grid.TextMatrix(x - 1, 6, Conversion.Val(clsDDDeductions.Get_Fields("ddepositamount")));
								if (x == intMaxStubDDLines)
									dblTemp = Conversion.Val(clsDDDeductions.Get_Fields("ddepositamount"));
								boolTemp = false;
								if (!(clsBanks.EndOfFile() && clsBanks.BeginningOfFile()))
								{
									if (!(clsBanks.Get_Fields("ID") == clsDDDeductions.Get_Fields("ddbanknumber")))
									{
										if (clsBanks.FindFirstRecord("ID", Conversion.Val(clsDDDeductions.Get_Fields("ddbanknumber"))))
										{
											boolTemp = true;
										}
									}
									else
									{
										boolTemp = true;
									}
								}
								if (boolTemp)
								{
									Grid.TextMatrix(x - 1, 7, fecherFoundation.Strings.Trim(FCConvert.ToString(clsBanks.Get_Fields("SHORTDESCRIPTION"))));
								}
								else
								{
									if (x <= intMaxStubLines)
									{
										Grid.TextMatrix(x - 1, 7, "Deposit #" + FCConvert.ToString(x));
									}
									else
									{
										Grid.TextMatrix(intMaxStubDDLines - 1, 7, "Other");
									}
								}
							}
							else
							{
								Grid.TextMatrix(intMaxStubDDLines - 1, 7, "Other");
								dblTemp += Conversion.Val(clsDDDeductions.Get_Fields("ddepositamount"));
								Grid.TextMatrix(intMaxStubDDLines - 1, 7, dblTemp);
							}
							x += 1;
							clsDDDeductions.MoveNext();
						}
					}
				}
				if (dblNet - dblDirectDeposit < 0.01)
				{
					GridMisc.TextMatrix(0, 10, "** VOID *** VOID *** VOID *** VOID *** VOID *** VOID *** VOID **");
					strTag = "INVALID CHECK";

					if (Strings.Format(dblNet, "0.00") == Strings.Format(dblDirectDeposit, "0.00"))
					{
						// MATTHEW 6/15/2004
						// the whole check is a direct deposit
						GridMisc.TextMatrix(0, 11, Strings.Format(dblDirectDeposit, "0.00"));
					}
					else
					{
						GridMisc.TextMatrix(0, 11, Strings.Format(dblNet - dblDirectDeposit, "0.00"));
					}
					GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
				}
				else
				{
					GridMisc.TextMatrix(0, 10, modConvertAmountToString.ConvertAmountToString(FCConvert.ToDecimal(dblNet - dblDirectDeposit)) + " ");
					if (GridMisc.TextMatrix(0, 10).Length > 64)
					{
						// just leave it alone
					}
					else
					{
						GridMisc.TextMatrix(0, 10, GridMisc.TextMatrix(0, 10) + Strings.StrDup(65 - GridMisc.TextMatrix(0, 10).Length, "*"));
					}
					GridMisc.TextMatrix(0, 11, Strings.Format(dblNet - dblDirectDeposit, "0.00"));
					GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
				}
				GridMisc.TextMatrix(0, 9, Strings.Format(dblNet, "0.00"));
				lngCurrentCheckNumber = FCConvert.ToInt32(clsLoad.Get_Fields("checknumber"));
				if (dblDirectDeposit > 0)
				{
					strCurrentCheckNumber = "D" + FCConvert.ToString(lngCurrentCheckNumber);
				}
				else
				{
					strCurrentCheckNumber = FCConvert.ToString(lngCurrentCheckNumber);
				}

				GridMisc.TextMatrix(0, 12, "CHECK " + FCConvert.ToString(lngCurrentCheckNumber));
				GridMisc.TextMatrix(0, 13, Strings.Format(Conversion.Val(clsLoad.Get_Fields("YTDGross")), "0.00"));
				GridMisc.TextMatrix(0, 14, Strings.Format(Conversion.Val(clsLoad.Get_Fields("YTDFedTax")), "0.00"));
				GridMisc.TextMatrix(0, 15, Strings.Format(Conversion.Val(clsLoad.Get_Fields("YTDFICA")), "0.00"));
				GridMisc.TextMatrix(0, 16, Strings.Format(Conversion.Val(clsLoad.Get_Fields("YTDStateTax")), "0.00"));
				GridMisc.TextMatrix(0, 17, Strings.Format(Conversion.Val(clsLoad.Get_Fields("ytdgross")) - Conversion.Val(clsLoad.Get_Fields("ytdfedtax")) - Conversion.Val(clsLoad.Get_Fields("ytdfica")) - Conversion.Val(clsLoad.Get_Fields("ytdstatetax")) - dblTotalYTDDeductions, "0.00"));
				GridMisc.TextMatrix(0, 18, clsLoad.Get_Fields("firstname") + " " + clsLoad.Get_Fields_String("MiddleName") + " " + clsLoad.Get_Fields("Lastname") + " " + clsLoad.Get_Fields("DESIG"));
				// GridMisc.TextMatrix(0, 18) = clsLoad.Fields("firstname") & " " & clsLoad.Fields("Lastname") & " " & clsLoad.Fields("DESIG")
				GridMisc.TextMatrix(0, 19, clsLoad.Get_Fields("address1"));
				GridMisc.TextMatrix(0, 20, clsLoad.Get_Fields("address2"));
				if (clsStates.FindFirstRecord("id", Conversion.Val(clsLoad.Get_Fields("state"))))
				{
					strState = FCConvert.ToString(clsStates.Get_Fields("State"));
				}
				else
				{
					strState = "";
				}

				GridMisc.TextMatrix(0, 21, clsLoad.Get_Fields("city") + "  " + strState + "  " + clsLoad.Get_Fields_String("zip"));
                if (GridMisc.TextMatrix(0, 20).Trim() == "")
                {
                    GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                    GridMisc.TextMatrix(0, 21, "");
                }

                if (GridMisc.TextMatrix(0, 19).Trim() == "")
                {
                    GridMisc.TextMatrix(0, 19, GridMisc.TextMatrix(0, 20));
                    GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                    GridMisc.TextMatrix(0, 21, "");
                }
				GridMisc.TextMatrix(0, 43, clsLoad.Get_Fields("deptdiv"));
				GridMisc.TextMatrix(0, 22, clsLoad.Get_Fields("tblemployeemaster.employeenumber"));
				GridMisc.TextMatrix(0, 27, "R");
				// regular
				// GridMisc.TextMatrix(0, 29) = Val(clsLoad.Fields("DDepositAmount"))
				GridMisc.TextMatrix(0, 29, dblDirectDepositChk);
				GridMisc.TextMatrix(0, 34, dblDirectDepositSav);
				GridMisc.TextMatrix(0, 35, Conversion.Val(clsLoad.Get_Fields_Double("EICAmount")));
				
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PrintPayRate")))
				{
					GridMisc.TextMatrix(0, 31, Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("BasePayRate")), "0.00"));
				}
				else
				{
					GridMisc.TextMatrix(0, 31, "");
				}

			}
			// lngCurrentCheckNumber = lngCurrentCheckNumber + 1
			FillInfo();
			clsLoad.MoveNext();
		}

		private string GetFileName(ref string strCheckNumber)
		{
			string GetFileName = "";
			string strFileName;
			strFileName = "";
			strFileName = Strings.Format(dtPayDate, "MMddyyyy") + "_" + FCConvert.ToString(intPayrunToUse) + "_" + strCheckNumber;
			GetFileName = strFileName;
			return GetFileName;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			this.UserData = GetFileName(ref strCurrentCheckNumber);
		}
	}
}
