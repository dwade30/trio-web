﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSampleCheck.
	/// </summary>
	partial class rptSampleCheck
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSampleCheck));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.lblHelp = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblHelp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHelp,
				this.Line1
			});
			this.Detail.Height = 7.78125F;
			this.Detail.Name = "Detail";
			// 
			// lblHelp
			// 
			this.lblHelp.Height = 0.1875F;
			this.lblHelp.HyperLink = null;
			this.lblHelp.Left = 2.46875F;
			this.lblHelp.Name = "lblHelp";
			this.lblHelp.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblHelp.Text = "This line represents the top of your check.";
			this.lblHelp.Top = 3.15625F;
			this.lblHelp.Width = 3F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.333333F;
			this.Line1.Width = 7.875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.875F;
			this.Line1.Y1 = 3.333333F;
			this.Line1.Y2 = 3.333333F;
			// 
			// rptSampleCheck
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.947917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHelp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHelp;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
