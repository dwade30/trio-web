﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEditContract.
	/// </summary>
	partial class frmEditContract
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public FCGrid gridDelete;
		public fecherFoundation.FCTextBox txtDescription;
		public Global.T2KDateBox t2kStartDate;
		public Global.T2KDateBox t2kEndDate;
		public FCGrid GridTotals;
		public FCGrid GridAccounts;
		public fecherFoundation.FCLabel lblEmployee;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_0;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.gridDelete = new fecherFoundation.FCGrid();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.t2kStartDate = new Global.T2KDateBox();
            this.t2kEndDate = new Global.T2KDateBox();
            this.GridTotals = new fecherFoundation.FCGrid();
            this.GridAccounts = new fecherFoundation.FCGrid();
            this.lblEmployee = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(504, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.gridDelete);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.t2kStartDate);
            this.ClientArea.Controls.Add(this.t2kEndDate);
            this.ClientArea.Controls.Add(this.GridTotals);
            this.ClientArea.Controls.Add(this.GridAccounts);
            this.ClientArea.Controls.Add(this.lblEmployee);
            this.ClientArea.Controls.Add(this.Label1_4);
            this.ClientArea.Controls.Add(this.Label1_3);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Size = new System.Drawing.Size(504, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(504, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(154, 30);
            this.HeaderText.Text = "Edit Contract";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // gridDelete
            // 
            this.gridDelete.AllowSelection = false;
            this.gridDelete.AllowUserToResizeColumns = false;
            this.gridDelete.AllowUserToResizeRows = false;
            this.gridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.gridDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.BackColorSel = System.Drawing.Color.Empty;
            this.gridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridDelete.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDelete.ColumnHeadersHeight = 30;
            this.gridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridDelete.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridDelete.DragIcon = null;
            this.gridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridDelete.FixedCols = 0;
            this.gridDelete.FixedRows = 0;
            this.gridDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.FrozenCols = 0;
            this.gridDelete.GridColor = System.Drawing.Color.Empty;
            this.gridDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.Location = new System.Drawing.Point(420, 203);
            this.gridDelete.Name = "gridDelete";
            this.gridDelete.OutlineCol = 0;
            this.gridDelete.ReadOnly = true;
            this.gridDelete.RowHeadersVisible = false;
            this.gridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDelete.RowHeightMin = 0;
            this.gridDelete.Rows = 0;
            this.gridDelete.ScrollTipText = null;
            this.gridDelete.ShowColumnVisibilityMenu = false;
            this.gridDelete.Size = new System.Drawing.Size(46, 21);
            this.gridDelete.StandardTab = true;
            this.gridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridDelete.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.gridDelete, null);
            this.gridDelete.Visible = false;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(177, 65);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(289, 40);
            this.txtDescription.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // t2kStartDate
            // 
            this.t2kStartDate.Location = new System.Drawing.Point(177, 125);
            this.t2kStartDate.Mask = "00/00/0000";
            this.t2kStartDate.MaxLength = 10;
            this.t2kStartDate.Name = "t2kStartDate";
            this.t2kStartDate.Size = new System.Drawing.Size(115, 40);
            this.t2kStartDate.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.t2kStartDate, null);
            // 
            // t2kEndDate
            // 
            this.t2kEndDate.Location = new System.Drawing.Point(177, 185);
            this.t2kEndDate.Mask = "00/00/0000";
            this.t2kEndDate.MaxLength = 10;
            this.t2kEndDate.Name = "t2kEndDate";
            this.t2kEndDate.Size = new System.Drawing.Size(115, 40);
            this.t2kEndDate.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.t2kEndDate, null);
            // 
            // GridTotals
            // 
            this.GridTotals.AllowSelection = false;
            this.GridTotals.AllowUserToResizeColumns = false;
            this.GridTotals.AllowUserToResizeRows = false;
            this.GridTotals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridTotals.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridTotals.BackColorBkg = System.Drawing.Color.Empty;
            this.GridTotals.BackColorFixed = System.Drawing.Color.Empty;
            this.GridTotals.BackColorSel = System.Drawing.Color.Empty;
            this.GridTotals.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridTotals.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridTotals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridTotals.ColumnHeadersHeight = 30;
            this.GridTotals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridTotals.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridTotals.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridTotals.DragIcon = null;
            this.GridTotals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridTotals.ExtendLastCol = true;
            this.GridTotals.FixedCols = 3;
            this.GridTotals.FixedRows = 0;
            this.GridTotals.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridTotals.FrozenCols = 0;
            this.GridTotals.GridColor = System.Drawing.Color.Empty;
            this.GridTotals.GridColorFixed = System.Drawing.Color.Empty;
            this.GridTotals.Location = new System.Drawing.Point(30, 462);
            this.GridTotals.Name = "GridTotals";
            this.GridTotals.OutlineCol = 0;
            this.GridTotals.ReadOnly = true;
            this.GridTotals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridTotals.RowHeightMin = 0;
            this.GridTotals.Rows = 1;
            this.GridTotals.ScrollTipText = null;
            this.GridTotals.ShowColumnVisibilityMenu = false;
            this.GridTotals.Size = new System.Drawing.Size(436, 57);
            this.GridTotals.StandardTab = true;
            this.GridTotals.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridTotals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridTotals.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.GridTotals, null);
            // 
            // GridAccounts
            // 
            this.GridAccounts.AllowSelection = false;
            this.GridAccounts.AllowUserToResizeColumns = false;
            this.GridAccounts.AllowUserToResizeRows = false;
            this.GridAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridAccounts.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorBkg = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.BackColorSel = System.Drawing.Color.Empty;
            this.GridAccounts.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridAccounts.Cols = 6;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridAccounts.ColumnHeadersHeight = 30;
            this.GridAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridAccounts.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridAccounts.DragIcon = null;
            this.GridAccounts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridAccounts.ExtendLastCol = true;
            this.GridAccounts.FixedCols = 0;
            this.GridAccounts.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.FrozenCols = 0;
            this.GridAccounts.GridColor = System.Drawing.Color.Empty;
            this.GridAccounts.GridColorFixed = System.Drawing.Color.Empty;
            this.GridAccounts.Location = new System.Drawing.Point(30, 245);
            this.GridAccounts.Name = "GridAccounts";
            this.GridAccounts.OutlineCol = 0;
            this.GridAccounts.RowHeadersVisible = false;
            this.GridAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridAccounts.RowHeightMin = 0;
            this.GridAccounts.Rows = 1;
            this.GridAccounts.ScrollTipText = null;
            this.GridAccounts.ShowColumnVisibilityMenu = false;
            this.GridAccounts.Size = new System.Drawing.Size(436, 197);
            this.GridAccounts.StandardTab = true;
            this.GridAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridAccounts.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.GridAccounts, "Use Insert and Delete to add and remove accounts");
            this.GridAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridAccounts_ValidateEdit);
            this.GridAccounts.CurrentCellChanged += new System.EventHandler(this.GridAccounts_RowColChange);
            this.GridAccounts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridAccounts_MouseMoveEvent);
            this.GridAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.GridAccounts_KeyDownEvent);
            // 
            // lblEmployee
            // 
            this.lblEmployee.Location = new System.Drawing.Point(30, 30);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(436, 15);
            this.lblEmployee.TabIndex = 8;
            this.lblEmployee.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblEmployee, null);
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(30, 199);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(129, 15);
            this.Label1_4.TabIndex = 5;
            this.Label1_4.Text = "END DATE";
            this.ToolTip1.SetToolTip(this.Label1_4, null);
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(30, 139);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(129, 15);
            this.Label1_3.TabIndex = 4;
            this.Label1_3.Text = "START DATE";
            this.ToolTip1.SetToolTip(this.Label1_3, null);
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 79);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(129, 15);
            this.Label1_0.TabIndex = 3;
            this.Label1_0.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.Label1_0, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(211, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmEditContract
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(504, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditContract";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Contract";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmEditContract_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditContract_KeyDown);
            this.Resize += new System.EventHandler(this.frmEditContract_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
    }
}
