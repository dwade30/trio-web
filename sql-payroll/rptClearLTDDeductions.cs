//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptClearLTDDeductions.
	/// </summary>
	public partial class rptClearLTDDeductions : BaseSectionReport
	{
		public rptClearLTDDeductions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Clear Life to Date Deduction Totals";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptClearLTDDeductions InstancePtr
		{
			get
			{
				return (rptClearLTDDeductions)Sys.GetInstance(typeof(rptClearLTDDeductions));
			}
		}

		protected rptClearLTDDeductions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptClearLTDDeductions	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       June 4,2001
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;
		clsDRWrapper rsData;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "ClearLTDDeductions", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            using (clsDRWrapper rsDeductions = new clsDRWrapper())
            {
                if (rsData.EndOfFile())
                {
                    eArgs.EOF = true;
                }
                else
                {
                    if (Conversion.Val(rsData.Get_Fields_Double("LTDTotal")) > 0)
                    {
                        txtNumber.Text = rsData.Get_Fields_String("EmployeeNumber");
                        txtName.Text = rsData.Get_Fields_String("FirstName") + " " +
                                       rsData.Get_Fields_String("MiddleName") + " " +
                                       rsData.Get_Fields_String("LastName");
                        txtOldValue.Text = Strings.Format(rsData.Get_Fields_Double("LTDTotal"), "0.00");
                        txtNewValue.Text = "0.00";
                        rsDeductions.OpenRecordset("SELECT * FROM tblEmployeeDeductions WHERE ID = " +
                                                   rsData.Get_Fields("ID"));
                        if (rsDeductions.EndOfFile() != true && rsDeductions.BeginningOfFile() != true)
                        {
                            rsDeductions.Edit();
                            rsDeductions.Set_Fields("LTDTotal", 0);
                            rsDeductions.Update();
                        }
                    }
                    else
                    {
                        if (rsData.EndOfFile())
                        {
                            eArgs.EOF = true;
                            return;
                        }
                        else
                        {
                            rsData.MoveNext();
                        }
                    }

                    if (!rsData.EndOfFile())
                        rsData.MoveNext();
                    intCounter += 1;
                    eArgs.EOF = false;
                }
            }
        }

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                using (rsData = new clsDRWrapper())
                {
                    rsData.OpenRecordset(
                        "Select  tblEmployeeDeductions.*, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, tblEmployeeMaster.MiddleName from tblEmployeeDeductions INNER JOIN tblEmployeeMaster ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber WHERE (((tblEmployeeMaster.SeqNumber)=" +
                        FCConvert.ToString(modGlobalVariables.Statics.gintSequenceNumber) +
                        ") AND ((tblEmployeeDeductions.DeductionCode)=" +
                        FCConvert.ToString(modGlobalVariables.Statics.gintDeductionNumber) + "))", "TWPY0000.vb1");
                    intCounter = 1;
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//FC:FINAL:DSE:#i2297 Call report initialization
			//ActiveReport_Initialize();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Clear Life to Date Deduction Totals";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		
	}
}