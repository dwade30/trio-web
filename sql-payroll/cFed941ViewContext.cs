﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cFed941ViewContext
	{
		//=========================================================
		private int intReportYear;
		private int intQuarter;
		private FCCollection collLiabilityDetails = new FCCollection();
		private string strSequence = string.Empty;

		public string Sequence
		{
			set
			{
				strSequence = value;
			}
			get
			{
				string Sequence = "";
				Sequence = strSequence;
				return Sequence;
			}
		}

		public FCCollection LiabilityCollection
		{
			get
			{
				return collLiabilityDetails;
			}
		}

		public int ReportQuarter
		{
			set
			{
				intQuarter = value;
			}
			get
			{
				int ReportQuarter = 0;
				ReportQuarter = intQuarter;
				return ReportQuarter;
			}
		}

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public void ClearLiabilityDetails()
		{
			if (!(collLiabilityDetails == null))
			{
				foreach (object tRec in collLiabilityDetails)
				{
					collLiabilityDetails.Remove(1);
				}
				// tRec
			}
		}

		public double TotalLiability()
		{
			double TotalLiability = 0;
			double dblAmount = 0;
			foreach (cTaxAdj941 tDetail in collLiabilityDetails)
			{
				dblAmount += tDetail.Amount;
			}
			TotalLiability = dblAmount;
			return TotalLiability;
		}

		public FCCollection LiabilitiesForMonth(int intMonth)
		{
			var theCollection = new FCCollection();
			int firstMonth = 0;
			switch (intQuarter)
			{
				case 1:
					{
						firstMonth = 1;
						break;
					}
				case 2:
					{
						firstMonth = 4;
						break;
					}
				case 3:
					{
						firstMonth = 7;
						break;
					}
				case 4:
					{
						firstMonth = 10;
						break;
					}
			}
			//end switch
			int intMonthToUse;
			intMonthToUse = firstMonth + (intMonth - 1);
			foreach (cTaxAdj941 tDetail in collLiabilityDetails)
			{
				if (FCConvert.ToDateTime(tDetail.EffectiveDate).Month == intMonthToUse)
				{
					theCollection.Add(tDetail);
				}
			}
			return theCollection;
		}
	}
}
