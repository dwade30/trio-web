//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Laser2x2.
	/// </summary>
	public partial class rptW2Laser2x2 : BaseSectionReport
	{
		public rptW2Laser2x2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W2 Laser";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2Laser2x2 InstancePtr
		{
			get
			{
				return (rptW2Laser2x2)Sys.GetInstance(typeof(rptW2Laser2x2));
			}
		}

		protected rptW2Laser2x2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsW2Matches?.Dispose();
				rsW2Master?.Dispose();
				rsW2Deductions?.Dispose();
				rsBox14?.Dispose();
				rsBox10?.Dispose();
				rsbox12?.Dispose();
                rsData = null;
                rsW2Matches = null;
                rsW2Master = null;
                rsW2Deductions = null;
                rsBox10 = null;
                rsBox14 = null;
                rsbox12 = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2Laser2x2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsW2Master = new clsDRWrapper();
		clsDRWrapper rsW2Deductions = new clsDRWrapper();
		clsDRWrapper rsW2Matches = new clsDRWrapper();
		clsDRWrapper rsbox12 = new clsDRWrapper();
		clsDRWrapper rsBox14 = new clsDRWrapper();
		clsDRWrapper rsBox10 = new clsDRWrapper();
		double dblLaserLineAdjustment;
		object ControlName;
		bool boolMove;
		bool boolThirdParty;
		// vbPorter upgrade warning: intArrayCounter As int	OnWriteFCConvert.ToInt32(
		int intArrayCounter;
		// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
		int intloop;
		int lngYearToUse;
		private int intStubCounter;
		private bool boolPrintTest;
		double dblHorizAdjust;

		private struct BoxEntries
		{
			public string Code;
			public double Amount;
			public bool ThirdParty;
		};

		private BoxEntries[] Box1214 = null;

		public void Init(bool boolTestPrint = false, double dblAdjustment = 0, double dblHorizAdjustment = 0)
		{
			dblLaserLineAdjustment = dblAdjustment;
			dblHorizAdjust = 120 * dblHorizAdjustment;
			boolPrintTest = boolTestPrint;
			intStubCounter = 0;
			frmReportViewer.InstancePtr.Init(this, "", 1, boolAllowEmail: false, showModal: true);
			if (!boolPrintTest)
			{
				rptW3Laser.InstancePtr.Init();
				if (lngYearToUse > 0)
				{
					frmW3ME.InstancePtr.Init(lngYearToUse, false);
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			boolThirdParty = false;
			bool boolFound;
			double dblTemp = 0;
			if (!boolPrintTest)
			{
				if (rsData.EndOfFile() && intStubCounter >= 4)
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					intStubCounter += 1;
					if (intStubCounter > 4)
						intStubCounter = 1;
					if (intStubCounter == 1)
					{
						if (rsW2Master.EndOfFile())
						{
						}
						else
						{
							txtFederalEIN.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
							txtEmployersName.Text = rsW2Master.Get_Fields_String("EmployersName") + "\n";
							txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address1") + "\n";
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("Address2"))) != string.Empty)
							{
								txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address2") + "\n";
							}
							txtEmployersName.Text = txtEmployersName.Text + fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("City"))) + ", " + rsW2Master.Get_Fields_String("State") + " " + rsW2Master.Get_Fields_String("Zip");
							txtState.Text = rsW2Master.Get_Fields_String("StateCode");
							txtEmployersStateEIN.Text = rsW2Master.Get_Fields_String("EmployersStateID");
						}
						txtSSN.Text = rsData.Get_Fields_String("SSN");
						// corey 01/23/05
						// separating last from first and middle
						txtEmployeesName.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Firstname") + " " + rsData.Get_Fields_String("MIddlename")) + " " + rsData.Get_Fields_String("lastname") + " " + rsData.Get_Fields_String("desig") + "\n";
						txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("Address") + "\n";
						txtEmployeesName.Text = txtEmployeesName.Text + rsData.Get_Fields_String("CityStateZip");
						// SET THE WAGES AND TAXES
						txtTotalWages.Text = rsData.Get_Fields_Double("FederalWage").FormatAsMoney();
						txtFederalTax.Text = rsData.Get_Fields_Double("FederalTax").FormatAsMoney();
						txtFICAWages.Text = rsData.Get_Fields_Double("FICAWage").FormatAsMoney();
						txtFICATax.Text = rsData.Get_Fields_Double("FICATax").FormatAsMoney();
						txtMedWages.Text = rsData.Get_Fields_Double("MedicareWage").FormatAsMoney();
						txtMedTaxes.Text = rsData.Get_Fields_Double("MedicareTax").FormatAsMoney();
						// PER RON AND KPORT 12/08/2004
						txtStateWages.Text = rsData.Get_Fields_Double("StateWage").FormatAsMoney();
						txtStateTax.Text = rsData.Get_Fields_Double("StateTax").FormatAsMoney();
						txtPen.Visible = rsData.Get_Fields_Boolean("W2Retirement");
						txtStatutoryEmployee.Visible = rsData.Get_Fields_Boolean("W2StatutoryEmployee");
						// ********************************************************************
						// SAVE W3 INFORMATION
						modGlobalVariables.Statics.W3Information.dblTotalWages += Conversion.Val(txtTotalWages.Text);
						modGlobalVariables.Statics.W3Information.dblFederalTax += Conversion.Val(txtFederalTax.Text);
						modGlobalVariables.Statics.W3Information.dblFicaWages += Conversion.Val(txtFICAWages.Text);
						modGlobalVariables.Statics.W3Information.dblFicaTax += Conversion.Val(txtFICATax.Text);
						modGlobalVariables.Statics.W3Information.dblMedWages += Conversion.Val(txtMedWages.Text);
						modGlobalVariables.Statics.W3Information.dblMedTaxes += Conversion.Val(txtMedTaxes.Text);
						// PER RON AND KPORT 12/08/2004
						modGlobalVariables.Statics.W3Information.dblStateWages += Conversion.Val(txtStateWages.Text);
						modGlobalVariables.Statics.W3Information.dblStateTax += Conversion.Val(txtStateTax.Text);
						// ********************************************************************
						// SET THE BOX 12 AND BOX 14 INFORMATION
						txtOther.Text = string.Empty;
						txt12a.Text = string.Empty;
						txt12aAmount.Text = string.Empty;
						txt12b.Text = string.Empty;
						txt12bAmount.Text = string.Empty;
						txt12c.Text = string.Empty;
						txt12cAmount.Text = string.Empty;
						txt12d.Text = string.Empty;
						txt12dAmount.Text = string.Empty;
						// If rsData.Fields("EmployeeNumber") = "016" Then
						// Else
						// GoTo Done14
						// End If
						rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box12 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
						rsW2Deductions.MoveLast();
						rsW2Deductions.MoveFirst();
						Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1];
						for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
						{
							Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
							Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
							Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
							rsW2Deductions.MoveNext();
						}
						rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and tblw2matches.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code ");
						rsW2Matches.MoveLast();
						rsW2Matches.MoveFirst();
						Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
						while (!rsW2Matches.EndOfFile())
						{
							boolFound = false;
							for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
							{
								if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
								{
									Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
									boolFound = true;
									break;
								}
								else if (Box1214[intloop].Code == "")
								{
									Box1214[intloop].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
									Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
									Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
									boolFound = true;
									break;
								}
							}
							// intloop
							rsW2Matches.MoveNext();
						}
						// now show box 12 info
						intCounter = 1;
						boolThirdParty = false;
						for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
						{
							if (!boolThirdParty)
							{
								if (Box1214[intArrayCounter].ThirdParty)
								{
									boolThirdParty = true;
								}
							}
						}
						for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
						{
							if (intCounter > 4)
								break;
							if (Box1214[intArrayCounter].Amount == 0)
								goto SkipEntry;
							if (Box1214[intArrayCounter].ThirdParty)
							{
								modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
							}
							if (intCounter == 1)
							{
								txt12a.Text = Box1214[intArrayCounter].Code;
								txt12aAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							else if (intCounter == 2)
							{
								txt12b.Text = Box1214[intArrayCounter].Code;
								txt12bAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							else if (intCounter == 3)
							{
								txt12c.Text = Box1214[intArrayCounter].Code;
								txt12cAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							else if (intCounter == 4)
							{
								txt12d.Text = Box1214[intArrayCounter].Code;
								txt12dAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							SkipEntry:
							;
						}
						// now fill box 14
						Box1214 = new BoxEntries[0 + 1];
						rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
						rsW2Deductions.MoveLast();
						rsW2Deductions.MoveFirst();
						Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
						for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
						{
							Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
							Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
							Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
							rsW2Deductions.MoveNext();
						}
						// Call rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE (((tblW2Matches.DeductionNumber) Not In (Select DeductionNumber from tblW2Deductions where Code <> '' AND EmployeeNumber ='" & rsData.Fields("EmployeeNumber") & "' AND tblW2Box12And14.Box14 = 1)))and tblw2box12and14.Box14 = 1 and tblw2matches.employeenumber = '" & rsData.Fields("employeenumber") & "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code ")
						rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE tblw2box12and14.Box14 = 1 and tblw2matches.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code ");
						rsW2Matches.MoveLast();
						rsW2Matches.MoveFirst();
						Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
						while (!rsW2Matches.EndOfFile())
						{
							boolFound = false;
							for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
							{
								if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
								{
									Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
									boolFound = true;
									break;
								}
								else if (Box1214[intloop].Code == "")
								{
									Box1214[intloop].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
									Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
									Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
									boolFound = true;
									break;
								}
							}
							// intloop
							rsW2Matches.MoveNext();
						}
						intCounter = 1;
						boolThirdParty = false;
						for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
						{
							if (!boolThirdParty)
							{
								if (Box1214[intArrayCounter].ThirdParty)
								{
									boolThirdParty = true;
								}
							}
						}
						for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
						{
							if (intCounter > 4)
								break;
							if (Box1214[intArrayCounter].Amount == 0)
								goto SkipMatchEntry;
							if (Box1214[intArrayCounter].ThirdParty)
							{
								modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
							}
							if (intCounter == 1)
							{
								txtOther.Text = Box1214[intArrayCounter].Code + " ";
								txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							else
							{
								txtOther.Text = txtOther.Text + "\r" + Box1214[intArrayCounter].Code + " ";
								txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
								intCounter += 1;
							}
							SkipMatchEntry:
							;
						}
						Done14:
						;
						// NOW FILL BOX 10
						dblTemp = 0;
						rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
						if (!rsBox10.EndOfFile())
						{
							dblTemp = Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
						}
						rsBox10.OpenRecordset("SELECT Sum(tblW2matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2matches ON tblW2Box12And14.DeductionNumber = tblW2matches.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2matches.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
						if (!rsBox10.EndOfFile())
						{
							dblTemp += Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
						}
						txtDependentCare.Text = Strings.Format(dblTemp, "0.00");
						
						rsData.MoveNext();
					}
					eArgs.EOF = false;
				}
                if (boolThirdParty != rsData.Get_Fields_Boolean("ThirdParty") && !boolThirdParty)
                {
                    boolThirdParty = true;
                }
                txtThirdParty.Visible = boolThirdParty;
            }
			else
			{
				intStubCounter += 1;
				if (intStubCounter > 4)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
				txt12a.Text = "X";
				txt12aAmount.Text = "999.99";
				txt12b.Text = "X";
				txt12bAmount.Text = "999.99";
				txt12c.Text = "X";
				txt12cAmount.Text = "999.99";
				txt12d.Text = "X";
				txt12dAmount.Text = "999.99";
				txtDependentCare.Text = "999.99";
				txtEmployeesName.Text = "First Middle Last" + "\r\n" + "Address" + "\r\n" + "City, ST Zip";
				txtEmployersName.Text = "Employer";
				txtEmployersStateEIN.Text = "XXXX";
				txtFederalEIN.Text = "XXXX";
				txtFederalTax.Text = "999.99";
				txtFICATax.Text = "999.99";
				txtFICAWages.Text = "999.99";
				txtMedTaxes.Text = "999.99";
				txtMedWages.Text = "999.99";
				txtSSN.Text = "999-99-9999";
				txtState.Text = "99";
				txtStateTax.Text = "999.99";
				txtStateWages.Text = "999.99";
				txtTotalWages.Text = "999.99";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strReportOrder = "";
			if (!boolPrintTest)
			{
				rsData.OpenRecordset("select * from tblCheckFormat", "twpy0000.vb1");
				if (rsData.EndOfFile())
				{
					dblLaserLineAdjustment = 0;
				}
				else
				{
					dblLaserLineAdjustment = FCConvert.ToDouble(Conversion.Val(rsData.Get_Fields_Double("W2Alignment")));
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("W2Alignment", "PY"))) != string.Empty)
				{
					dblLaserLineAdjustment = Conversion.Val(modRegistry.GetRegistryKey("W2Alignment", "PY"));
				}

				dblHorizAdjust = FCConvert.ToDouble(modGlobalRoutines.SetW2HorizAdjustment(this));
				rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 0)
				{
					// employee name
					strReportOrder = "LastName,FirstName,MiddleName";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 1)
				{
					// employee number
					strReportOrder = "EmployeeNumber";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 2)
				{
					strReportOrder = "SeqNumber,lastname,firstname";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 3)
				{
					strReportOrder = "deptdiv,lastname,firstname";
				}
				if (Conversion.Val(modGlobalVariables.Statics.gstrPrintAllW2s) == 0)
				{
					rsData.OpenRecordset("Select * from tblW2EditTable order by " + strReportOrder, "TWPY0000.vb1");
				}
				else
				{
					rsData.OpenRecordset("Select * from tblW2EditTable Where EmployeeNumber = '" + modGlobalVariables.Statics.gstrPrintAllW2s + "' order by " + strReportOrder, "TWPY0000.vb1");
				}
				if (!rsData.EndOfFile())
				{
					lngYearToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("year"))));
				}
				rsW2Master.OpenRecordset("Select * from tblW2Master", "TWPY0000.vb1");
				rsW2Deductions.OpenRecordset("Select * from  tblW2Deductions where Code <> '' Order by Code", "TWPY0000.vb1");
				// Call rsW2Matches.OpenRecordset("Select * from tblW2Matches where Code <> ''", "TWPY0000.vb1")
				// Call rsW2Matches.OpenRecordset("SELECT tblW2Matches.* From tblW2Matches, tblW2Deductions WHERE tblW2Matches.DeductionNumber<>[tblW2Deductions].[DeductionNumber] AND tblW2Matches.Code <> '' Order by tblW2Matches.Code", "TWPY0000.vb1")
				rsbox12.OpenRecordset("Select * from tblW2Box12AND14 Where Box12 = 1");
				rsBox14.OpenRecordset("Select * from tblW2Box12AND14 Where Box14 = 1");
			}
            else
            {
                foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
                {
                    ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
                    //ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
                }
            }
		}
	}
}
