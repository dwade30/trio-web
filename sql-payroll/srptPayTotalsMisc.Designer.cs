﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptPayTotalsMisc.
	/// </summary>
	partial class srptPayTotalsMisc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPayTotalsMisc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurFicaTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFicaTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFicaTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFicaTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFicaTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurFedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurFicaWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDFicaWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDFicaWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDFicaWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDFicaWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDTotalGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurEmployersMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDEmployersMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDEmployersMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDEmployersMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDEmployersMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurMSRS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDMSRS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDMSRS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDMSRS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDMSRS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTDUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFYTDUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCYTDUnemployment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFicaTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFicaWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDTotalGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurEmployersMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDEmployersMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDEmployersMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDEmployersMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDEmployersMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMSRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDUnemployment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDesc,
            this.txtCurFedTax,
            this.txtMTDFedTax,
            this.txtQTDFedTax,
            this.txtFYTDFedTax,
            this.txtCYTDFedTax,
            this.Line1,
            this.Field1,
            this.txtCurFicaTax,
            this.txtMTDFicaTax,
            this.txtQTDFicaTax,
            this.txtFYTDFicaTax,
            this.txtCYTDFicaTax,
            this.Field7,
            this.txtCurMedTax,
            this.txtMTDMedTax,
            this.txtQTDMedTax,
            this.txtFYTDMedTax,
            this.txtCYTDMedTax,
            this.Field13,
            this.txtCurStateTax,
            this.txtMTDStateTax,
            this.txtQTDStateTax,
            this.txtFYTDStateTax,
            this.txtCYTDStateTax,
            this.Field20,
            this.txtCurFedWage,
            this.txtMTDFedWage,
            this.txtQTDFedWage,
            this.txtFYTDFedWage,
            this.txtCYTDFedWage,
            this.Line2,
            this.Field26,
            this.txtCurFicaWage,
            this.txtMTDFicaWage,
            this.txtQTDFicaWage,
            this.txtFYTDFicaWage,
            this.txtCYTDFicaWage,
            this.Field32,
            this.txtCurMedWage,
            this.txtMTDMedWage,
            this.txtQTDMedWage,
            this.txtFYTDMedWage,
            this.txtCYTDMedWage,
            this.Field38,
            this.txtCurStateWage,
            this.txtMTDStateWage,
            this.txtQTDStateWage,
            this.txtFYTDStateWage,
            this.txtCYTDStateWage,
            this.Field45,
            this.txtCurTotalGross,
            this.txtMTDTotalGross,
            this.txtQTDTotalGross,
            this.txtFYTDTotalGross,
            this.txtCYTDTotalGross,
            this.Line3,
            this.Field52,
            this.txtCurTotalPaid,
            this.txtMTDTotalPaid,
            this.txtQTDTotalPaid,
            this.txtFYTDTotalPaid,
            this.txtCYTDTotalPaid,
            this.Line4,
            this.Field58,
            this.txtCurEmployersMatch,
            this.txtMTDEmployersMatch,
            this.txtQTDEmployersMatch,
            this.txtFYTDEmployersMatch,
            this.txtCYTDEmployersMatch,
            this.Field64,
            this.txtCurMSRS,
            this.txtMTDMSRS,
            this.txtQTDMSRS,
            this.txtFYTDMSRS,
            this.txtCYTDMSRS,
            this.Field70,
            this.txtCurUnemployment,
            this.txtMTDUnemployment,
            this.txtQTDUnemployment,
            this.txtFYTDUnemployment,
            this.txtCYTDUnemployment});
			this.Detail.Height = 3.5625F;
			this.Detail.Name = "Detail";
			// 
			// txtDesc
			// 
			this.txtDesc.Height = 0.1875F;
			this.txtDesc.Left = 0F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Text = "Federal Tax";
			this.txtDesc.Top = 0.125F;
			this.txtDesc.Width = 2.25F;
			// 
			// txtCurFedTax
			// 
			this.txtCurFedTax.Height = 0.1875F;
			this.txtCurFedTax.Left = 2.375F;
			this.txtCurFedTax.Name = "txtCurFedTax";
			this.txtCurFedTax.Style = "text-align: right";
			this.txtCurFedTax.Text = null;
			this.txtCurFedTax.Top = 0.125F;
			this.txtCurFedTax.Width = 0.9375F;
			// 
			// txtMTDFedTax
			// 
			this.txtMTDFedTax.Height = 0.1875F;
			this.txtMTDFedTax.Left = 3.375F;
			this.txtMTDFedTax.Name = "txtMTDFedTax";
			this.txtMTDFedTax.Style = "text-align: right";
			this.txtMTDFedTax.Text = null;
			this.txtMTDFedTax.Top = 0.125F;
			this.txtMTDFedTax.Width = 0.9375F;
			// 
			// txtQTDFedTax
			// 
			this.txtQTDFedTax.Height = 0.1875F;
			this.txtQTDFedTax.Left = 4.375F;
			this.txtQTDFedTax.Name = "txtQTDFedTax";
			this.txtQTDFedTax.Style = "text-align: right";
			this.txtQTDFedTax.Text = null;
			this.txtQTDFedTax.Top = 0.125F;
			this.txtQTDFedTax.Width = 0.9375F;
			// 
			// txtFYTDFedTax
			// 
			this.txtFYTDFedTax.Height = 0.1875F;
			this.txtFYTDFedTax.Left = 5.375F;
			this.txtFYTDFedTax.Name = "txtFYTDFedTax";
			this.txtFYTDFedTax.Style = "text-align: right";
			this.txtFYTDFedTax.Text = null;
			this.txtFYTDFedTax.Top = 0.125F;
			this.txtFYTDFedTax.Width = 0.9375F;
			// 
			// txtCYTDFedTax
			// 
			this.txtCYTDFedTax.Height = 0.1875F;
			this.txtCYTDFedTax.Left = 6.375F;
			this.txtCYTDFedTax.Name = "txtCYTDFedTax";
			this.txtCYTDFedTax.Style = "text-align: right";
			this.txtCYTDFedTax.Text = null;
			this.txtCYTDFedTax.Top = 0.125F;
			this.txtCYTDFedTax.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 7.3125F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.375F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Fica Tax";
			this.Field1.Top = 0.3125F;
			this.Field1.Width = 2.25F;
			// 
			// txtCurFicaTax
			// 
			this.txtCurFicaTax.Height = 0.1875F;
			this.txtCurFicaTax.Left = 2.375F;
			this.txtCurFicaTax.Name = "txtCurFicaTax";
			this.txtCurFicaTax.Style = "text-align: right";
			this.txtCurFicaTax.Text = null;
			this.txtCurFicaTax.Top = 0.3125F;
			this.txtCurFicaTax.Width = 0.9375F;
			// 
			// txtMTDFicaTax
			// 
			this.txtMTDFicaTax.Height = 0.1875F;
			this.txtMTDFicaTax.Left = 3.375F;
			this.txtMTDFicaTax.Name = "txtMTDFicaTax";
			this.txtMTDFicaTax.Style = "text-align: right";
			this.txtMTDFicaTax.Text = null;
			this.txtMTDFicaTax.Top = 0.3125F;
			this.txtMTDFicaTax.Width = 0.9375F;
			// 
			// txtQTDFicaTax
			// 
			this.txtQTDFicaTax.Height = 0.1875F;
			this.txtQTDFicaTax.Left = 4.375F;
			this.txtQTDFicaTax.Name = "txtQTDFicaTax";
			this.txtQTDFicaTax.Style = "text-align: right";
			this.txtQTDFicaTax.Text = null;
			this.txtQTDFicaTax.Top = 0.3125F;
			this.txtQTDFicaTax.Width = 0.9375F;
			// 
			// txtFYTDFicaTax
			// 
			this.txtFYTDFicaTax.Height = 0.1875F;
			this.txtFYTDFicaTax.Left = 5.375F;
			this.txtFYTDFicaTax.Name = "txtFYTDFicaTax";
			this.txtFYTDFicaTax.Style = "text-align: right";
			this.txtFYTDFicaTax.Text = null;
			this.txtFYTDFicaTax.Top = 0.3125F;
			this.txtFYTDFicaTax.Width = 0.9375F;
			// 
			// txtCYTDFicaTax
			// 
			this.txtCYTDFicaTax.Height = 0.1875F;
			this.txtCYTDFicaTax.Left = 6.375F;
			this.txtCYTDFicaTax.Name = "txtCYTDFicaTax";
			this.txtCYTDFicaTax.Style = "text-align: right";
			this.txtCYTDFicaTax.Text = null;
			this.txtCYTDFicaTax.Top = 0.3125F;
			this.txtCYTDFicaTax.Width = 0.9375F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 0F;
			this.Field7.Name = "Field7";
			this.Field7.Text = "Medicare Tax";
			this.Field7.Top = 0.5F;
			this.Field7.Width = 2.25F;
			// 
			// txtCurMedTax
			// 
			this.txtCurMedTax.Height = 0.1875F;
			this.txtCurMedTax.Left = 2.375F;
			this.txtCurMedTax.Name = "txtCurMedTax";
			this.txtCurMedTax.Style = "text-align: right";
			this.txtCurMedTax.Text = null;
			this.txtCurMedTax.Top = 0.5F;
			this.txtCurMedTax.Width = 0.9375F;
			// 
			// txtMTDMedTax
			// 
			this.txtMTDMedTax.Height = 0.1875F;
			this.txtMTDMedTax.Left = 3.375F;
			this.txtMTDMedTax.Name = "txtMTDMedTax";
			this.txtMTDMedTax.Style = "text-align: right";
			this.txtMTDMedTax.Text = null;
			this.txtMTDMedTax.Top = 0.5F;
			this.txtMTDMedTax.Width = 0.9375F;
			// 
			// txtQTDMedTax
			// 
			this.txtQTDMedTax.Height = 0.1875F;
			this.txtQTDMedTax.Left = 4.375F;
			this.txtQTDMedTax.Name = "txtQTDMedTax";
			this.txtQTDMedTax.Style = "text-align: right";
			this.txtQTDMedTax.Text = null;
			this.txtQTDMedTax.Top = 0.5F;
			this.txtQTDMedTax.Width = 0.9375F;
			// 
			// txtFYTDMedTax
			// 
			this.txtFYTDMedTax.Height = 0.1875F;
			this.txtFYTDMedTax.Left = 5.375F;
			this.txtFYTDMedTax.Name = "txtFYTDMedTax";
			this.txtFYTDMedTax.Style = "text-align: right";
			this.txtFYTDMedTax.Text = null;
			this.txtFYTDMedTax.Top = 0.5F;
			this.txtFYTDMedTax.Width = 0.9375F;
			// 
			// txtCYTDMedTax
			// 
			this.txtCYTDMedTax.Height = 0.1875F;
			this.txtCYTDMedTax.Left = 6.375F;
			this.txtCYTDMedTax.Name = "txtCYTDMedTax";
			this.txtCYTDMedTax.Style = "text-align: right";
			this.txtCYTDMedTax.Text = null;
			this.txtCYTDMedTax.Top = 0.5F;
			this.txtCYTDMedTax.Width = 0.9375F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0F;
			this.Field13.Name = "Field13";
			this.Field13.Text = "State Tax";
			this.Field13.Top = 0.6875F;
			this.Field13.Width = 2.25F;
			// 
			// txtCurStateTax
			// 
			this.txtCurStateTax.Height = 0.1875F;
			this.txtCurStateTax.Left = 2.375F;
			this.txtCurStateTax.Name = "txtCurStateTax";
			this.txtCurStateTax.Style = "text-align: right";
			this.txtCurStateTax.Text = null;
			this.txtCurStateTax.Top = 0.6875F;
			this.txtCurStateTax.Width = 0.9375F;
			// 
			// txtMTDStateTax
			// 
			this.txtMTDStateTax.Height = 0.1875F;
			this.txtMTDStateTax.Left = 3.375F;
			this.txtMTDStateTax.Name = "txtMTDStateTax";
			this.txtMTDStateTax.Style = "text-align: right";
			this.txtMTDStateTax.Text = null;
			this.txtMTDStateTax.Top = 0.6875F;
			this.txtMTDStateTax.Width = 0.9375F;
			// 
			// txtQTDStateTax
			// 
			this.txtQTDStateTax.Height = 0.1875F;
			this.txtQTDStateTax.Left = 4.375F;
			this.txtQTDStateTax.Name = "txtQTDStateTax";
			this.txtQTDStateTax.Style = "text-align: right";
			this.txtQTDStateTax.Text = null;
			this.txtQTDStateTax.Top = 0.6875F;
			this.txtQTDStateTax.Width = 0.9375F;
			// 
			// txtFYTDStateTax
			// 
			this.txtFYTDStateTax.Height = 0.1875F;
			this.txtFYTDStateTax.Left = 5.375F;
			this.txtFYTDStateTax.Name = "txtFYTDStateTax";
			this.txtFYTDStateTax.Style = "text-align: right";
			this.txtFYTDStateTax.Text = null;
			this.txtFYTDStateTax.Top = 0.6875F;
			this.txtFYTDStateTax.Width = 0.9375F;
			// 
			// txtCYTDStateTax
			// 
			this.txtCYTDStateTax.Height = 0.1875F;
			this.txtCYTDStateTax.Left = 6.375F;
			this.txtCYTDStateTax.Name = "txtCYTDStateTax";
			this.txtCYTDStateTax.Style = "text-align: right";
			this.txtCYTDStateTax.Text = null;
			this.txtCYTDStateTax.Top = 0.6875F;
			this.txtCYTDStateTax.Width = 0.9375F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 0F;
			this.Field20.Name = "Field20";
			this.Field20.Text = "Federal Wage";
			this.Field20.Top = 1.1875F;
			this.Field20.Width = 2.25F;
			// 
			// txtCurFedWage
			// 
			this.txtCurFedWage.Height = 0.1875F;
			this.txtCurFedWage.Left = 2.375F;
			this.txtCurFedWage.Name = "txtCurFedWage";
			this.txtCurFedWage.Style = "text-align: right";
			this.txtCurFedWage.Text = null;
			this.txtCurFedWage.Top = 1.1875F;
			this.txtCurFedWage.Width = 0.9375F;
			// 
			// txtMTDFedWage
			// 
			this.txtMTDFedWage.Height = 0.1875F;
			this.txtMTDFedWage.Left = 3.375F;
			this.txtMTDFedWage.Name = "txtMTDFedWage";
			this.txtMTDFedWage.Style = "text-align: right";
			this.txtMTDFedWage.Text = null;
			this.txtMTDFedWage.Top = 1.1875F;
			this.txtMTDFedWage.Width = 0.9375F;
			// 
			// txtQTDFedWage
			// 
			this.txtQTDFedWage.Height = 0.1875F;
			this.txtQTDFedWage.Left = 4.375F;
			this.txtQTDFedWage.Name = "txtQTDFedWage";
			this.txtQTDFedWage.Style = "text-align: right";
			this.txtQTDFedWage.Text = null;
			this.txtQTDFedWage.Top = 1.1875F;
			this.txtQTDFedWage.Width = 0.9375F;
			// 
			// txtFYTDFedWage
			// 
			this.txtFYTDFedWage.Height = 0.1875F;
			this.txtFYTDFedWage.Left = 5.375F;
			this.txtFYTDFedWage.Name = "txtFYTDFedWage";
			this.txtFYTDFedWage.Style = "text-align: right";
			this.txtFYTDFedWage.Text = null;
			this.txtFYTDFedWage.Top = 1.1875F;
			this.txtFYTDFedWage.Width = 0.9375F;
			// 
			// txtCYTDFedWage
			// 
			this.txtCYTDFedWage.Height = 0.1875F;
			this.txtCYTDFedWage.Left = 6.375F;
			this.txtCYTDFedWage.Name = "txtCYTDFedWage";
			this.txtCYTDFedWage.Style = "text-align: right";
			this.txtCYTDFedWage.Text = null;
			this.txtCYTDFedWage.Top = 1.1875F;
			this.txtCYTDFedWage.Width = 0.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.125F;
			this.Line2.Width = 7.3125F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.375F;
			this.Line2.Y1 = 1.125F;
			this.Line2.Y2 = 1.125F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 0F;
			this.Field26.Name = "Field26";
			this.Field26.Text = "Fica Wage";
			this.Field26.Top = 1.375F;
			this.Field26.Width = 2.25F;
			// 
			// txtCurFicaWage
			// 
			this.txtCurFicaWage.Height = 0.1875F;
			this.txtCurFicaWage.Left = 2.375F;
			this.txtCurFicaWage.Name = "txtCurFicaWage";
			this.txtCurFicaWage.Style = "text-align: right";
			this.txtCurFicaWage.Text = null;
			this.txtCurFicaWage.Top = 1.375F;
			this.txtCurFicaWage.Width = 0.9375F;
			// 
			// txtMTDFicaWage
			// 
			this.txtMTDFicaWage.Height = 0.1875F;
			this.txtMTDFicaWage.Left = 3.375F;
			this.txtMTDFicaWage.Name = "txtMTDFicaWage";
			this.txtMTDFicaWage.Style = "text-align: right";
			this.txtMTDFicaWage.Text = null;
			this.txtMTDFicaWage.Top = 1.375F;
			this.txtMTDFicaWage.Width = 0.9375F;
			// 
			// txtQTDFicaWage
			// 
			this.txtQTDFicaWage.Height = 0.1875F;
			this.txtQTDFicaWage.Left = 4.375F;
			this.txtQTDFicaWage.Name = "txtQTDFicaWage";
			this.txtQTDFicaWage.Style = "text-align: right";
			this.txtQTDFicaWage.Text = null;
			this.txtQTDFicaWage.Top = 1.375F;
			this.txtQTDFicaWage.Width = 0.9375F;
			// 
			// txtFYTDFicaWage
			// 
			this.txtFYTDFicaWage.Height = 0.1875F;
			this.txtFYTDFicaWage.Left = 5.375F;
			this.txtFYTDFicaWage.Name = "txtFYTDFicaWage";
			this.txtFYTDFicaWage.Style = "text-align: right";
			this.txtFYTDFicaWage.Text = null;
			this.txtFYTDFicaWage.Top = 1.375F;
			this.txtFYTDFicaWage.Width = 0.9375F;
			// 
			// txtCYTDFicaWage
			// 
			this.txtCYTDFicaWage.Height = 0.1875F;
			this.txtCYTDFicaWage.Left = 6.375F;
			this.txtCYTDFicaWage.Name = "txtCYTDFicaWage";
			this.txtCYTDFicaWage.Style = "text-align: right";
			this.txtCYTDFicaWage.Text = null;
			this.txtCYTDFicaWage.Top = 1.375F;
			this.txtCYTDFicaWage.Width = 0.9375F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.1875F;
			this.Field32.Left = 0F;
			this.Field32.Name = "Field32";
			this.Field32.Text = "Medicare Wage";
			this.Field32.Top = 1.5625F;
			this.Field32.Width = 2.25F;
			// 
			// txtCurMedWage
			// 
			this.txtCurMedWage.Height = 0.1875F;
			this.txtCurMedWage.Left = 2.375F;
			this.txtCurMedWage.Name = "txtCurMedWage";
			this.txtCurMedWage.Style = "text-align: right";
			this.txtCurMedWage.Text = null;
			this.txtCurMedWage.Top = 1.5625F;
			this.txtCurMedWage.Width = 0.9375F;
			// 
			// txtMTDMedWage
			// 
			this.txtMTDMedWage.Height = 0.1875F;
			this.txtMTDMedWage.Left = 3.375F;
			this.txtMTDMedWage.Name = "txtMTDMedWage";
			this.txtMTDMedWage.Style = "text-align: right";
			this.txtMTDMedWage.Text = null;
			this.txtMTDMedWage.Top = 1.5625F;
			this.txtMTDMedWage.Width = 0.9375F;
			// 
			// txtQTDMedWage
			// 
			this.txtQTDMedWage.Height = 0.1875F;
			this.txtQTDMedWage.Left = 4.375F;
			this.txtQTDMedWage.Name = "txtQTDMedWage";
			this.txtQTDMedWage.Style = "text-align: right";
			this.txtQTDMedWage.Text = null;
			this.txtQTDMedWage.Top = 1.5625F;
			this.txtQTDMedWage.Width = 0.9375F;
			// 
			// txtFYTDMedWage
			// 
			this.txtFYTDMedWage.Height = 0.1875F;
			this.txtFYTDMedWage.Left = 5.375F;
			this.txtFYTDMedWage.Name = "txtFYTDMedWage";
			this.txtFYTDMedWage.Style = "text-align: right";
			this.txtFYTDMedWage.Text = null;
			this.txtFYTDMedWage.Top = 1.5625F;
			this.txtFYTDMedWage.Width = 0.9375F;
			// 
			// txtCYTDMedWage
			// 
			this.txtCYTDMedWage.Height = 0.1875F;
			this.txtCYTDMedWage.Left = 6.375F;
			this.txtCYTDMedWage.Name = "txtCYTDMedWage";
			this.txtCYTDMedWage.Style = "text-align: right";
			this.txtCYTDMedWage.Text = null;
			this.txtCYTDMedWage.Top = 1.5625F;
			this.txtCYTDMedWage.Width = 0.9375F;
			// 
			// Field38
			// 
			this.Field38.Height = 0.1875F;
			this.Field38.Left = 0F;
			this.Field38.Name = "Field38";
			this.Field38.Text = "State Wage";
			this.Field38.Top = 1.75F;
			this.Field38.Width = 2.25F;
			// 
			// txtCurStateWage
			// 
			this.txtCurStateWage.Height = 0.1875F;
			this.txtCurStateWage.Left = 2.375F;
			this.txtCurStateWage.Name = "txtCurStateWage";
			this.txtCurStateWage.Style = "text-align: right";
			this.txtCurStateWage.Text = null;
			this.txtCurStateWage.Top = 1.75F;
			this.txtCurStateWage.Width = 0.9375F;
			// 
			// txtMTDStateWage
			// 
			this.txtMTDStateWage.Height = 0.1875F;
			this.txtMTDStateWage.Left = 3.375F;
			this.txtMTDStateWage.Name = "txtMTDStateWage";
			this.txtMTDStateWage.Style = "text-align: right";
			this.txtMTDStateWage.Text = null;
			this.txtMTDStateWage.Top = 1.75F;
			this.txtMTDStateWage.Width = 0.9375F;
			// 
			// txtQTDStateWage
			// 
			this.txtQTDStateWage.Height = 0.1875F;
			this.txtQTDStateWage.Left = 4.375F;
			this.txtQTDStateWage.Name = "txtQTDStateWage";
			this.txtQTDStateWage.Style = "text-align: right";
			this.txtQTDStateWage.Text = null;
			this.txtQTDStateWage.Top = 1.75F;
			this.txtQTDStateWage.Width = 0.9375F;
			// 
			// txtFYTDStateWage
			// 
			this.txtFYTDStateWage.Height = 0.1875F;
			this.txtFYTDStateWage.Left = 5.375F;
			this.txtFYTDStateWage.Name = "txtFYTDStateWage";
			this.txtFYTDStateWage.Style = "text-align: right";
			this.txtFYTDStateWage.Text = null;
			this.txtFYTDStateWage.Top = 1.75F;
			this.txtFYTDStateWage.Width = 0.9375F;
			// 
			// txtCYTDStateWage
			// 
			this.txtCYTDStateWage.Height = 0.1875F;
			this.txtCYTDStateWage.Left = 6.375F;
			this.txtCYTDStateWage.Name = "txtCYTDStateWage";
			this.txtCYTDStateWage.Style = "text-align: right";
			this.txtCYTDStateWage.Text = null;
			this.txtCYTDStateWage.Top = 1.75F;
			this.txtCYTDStateWage.Width = 0.9375F;
			// 
			// Field45
			// 
			this.Field45.Height = 0.1875F;
			this.Field45.Left = 0F;
			this.Field45.Name = "Field45";
			this.Field45.Text = "Total Gross Pay";
			this.Field45.Top = 2.25F;
			this.Field45.Width = 2.25F;
			// 
			// txtCurTotalGross
			// 
			this.txtCurTotalGross.Height = 0.1875F;
			this.txtCurTotalGross.Left = 2.375F;
			this.txtCurTotalGross.Name = "txtCurTotalGross";
			this.txtCurTotalGross.Style = "text-align: right";
			this.txtCurTotalGross.Text = null;
			this.txtCurTotalGross.Top = 2.25F;
			this.txtCurTotalGross.Width = 0.9375F;
			// 
			// txtMTDTotalGross
			// 
			this.txtMTDTotalGross.Height = 0.1875F;
			this.txtMTDTotalGross.Left = 3.375F;
			this.txtMTDTotalGross.Name = "txtMTDTotalGross";
			this.txtMTDTotalGross.Style = "text-align: right";
			this.txtMTDTotalGross.Text = null;
			this.txtMTDTotalGross.Top = 2.25F;
			this.txtMTDTotalGross.Width = 0.9375F;
			// 
			// txtQTDTotalGross
			// 
			this.txtQTDTotalGross.Height = 0.1875F;
			this.txtQTDTotalGross.Left = 4.375F;
			this.txtQTDTotalGross.Name = "txtQTDTotalGross";
			this.txtQTDTotalGross.Style = "text-align: right";
			this.txtQTDTotalGross.Text = null;
			this.txtQTDTotalGross.Top = 2.25F;
			this.txtQTDTotalGross.Width = 0.9375F;
			// 
			// txtFYTDTotalGross
			// 
			this.txtFYTDTotalGross.Height = 0.1875F;
			this.txtFYTDTotalGross.Left = 5.375F;
			this.txtFYTDTotalGross.Name = "txtFYTDTotalGross";
			this.txtFYTDTotalGross.Style = "text-align: right";
			this.txtFYTDTotalGross.Text = null;
			this.txtFYTDTotalGross.Top = 2.25F;
			this.txtFYTDTotalGross.Width = 0.9375F;
			// 
			// txtCYTDTotalGross
			// 
			this.txtCYTDTotalGross.Height = 0.1875F;
			this.txtCYTDTotalGross.Left = 6.375F;
			this.txtCYTDTotalGross.Name = "txtCYTDTotalGross";
			this.txtCYTDTotalGross.Style = "text-align: right";
			this.txtCYTDTotalGross.Text = null;
			this.txtCYTDTotalGross.Top = 2.25F;
			this.txtCYTDTotalGross.Width = 0.9375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.0625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.1875F;
			this.Line3.Width = 7.3125F;
			this.Line3.X1 = 0.0625F;
			this.Line3.X2 = 7.375F;
			this.Line3.Y1 = 2.1875F;
			this.Line3.Y2 = 2.1875F;
			// 
			// Field52
			// 
			this.Field52.Height = 0.1875F;
			this.Field52.Left = 0F;
			this.Field52.Name = "Field52";
			this.Field52.Text = "Total Paid";
			this.Field52.Top = 2.75F;
			this.Field52.Width = 2.25F;
			// 
			// txtCurTotalPaid
			// 
			this.txtCurTotalPaid.Height = 0.1875F;
			this.txtCurTotalPaid.Left = 2.375F;
			this.txtCurTotalPaid.Name = "txtCurTotalPaid";
			this.txtCurTotalPaid.Style = "text-align: right";
			this.txtCurTotalPaid.Text = null;
			this.txtCurTotalPaid.Top = 2.75F;
			this.txtCurTotalPaid.Width = 0.9375F;
			// 
			// txtMTDTotalPaid
			// 
			this.txtMTDTotalPaid.Height = 0.1875F;
			this.txtMTDTotalPaid.Left = 3.375F;
			this.txtMTDTotalPaid.Name = "txtMTDTotalPaid";
			this.txtMTDTotalPaid.Style = "text-align: right";
			this.txtMTDTotalPaid.Text = null;
			this.txtMTDTotalPaid.Top = 2.75F;
			this.txtMTDTotalPaid.Width = 0.9375F;
			// 
			// txtQTDTotalPaid
			// 
			this.txtQTDTotalPaid.Height = 0.1875F;
			this.txtQTDTotalPaid.Left = 4.375F;
			this.txtQTDTotalPaid.Name = "txtQTDTotalPaid";
			this.txtQTDTotalPaid.Style = "text-align: right";
			this.txtQTDTotalPaid.Text = null;
			this.txtQTDTotalPaid.Top = 2.75F;
			this.txtQTDTotalPaid.Width = 0.9375F;
			// 
			// txtFYTDTotalPaid
			// 
			this.txtFYTDTotalPaid.Height = 0.1875F;
			this.txtFYTDTotalPaid.Left = 5.375F;
			this.txtFYTDTotalPaid.Name = "txtFYTDTotalPaid";
			this.txtFYTDTotalPaid.Style = "text-align: right";
			this.txtFYTDTotalPaid.Text = null;
			this.txtFYTDTotalPaid.Top = 2.75F;
			this.txtFYTDTotalPaid.Width = 0.9375F;
			// 
			// txtCYTDTotalPaid
			// 
			this.txtCYTDTotalPaid.Height = 0.1875F;
			this.txtCYTDTotalPaid.Left = 6.375F;
			this.txtCYTDTotalPaid.Name = "txtCYTDTotalPaid";
			this.txtCYTDTotalPaid.Style = "text-align: right";
			this.txtCYTDTotalPaid.Text = null;
			this.txtCYTDTotalPaid.Top = 2.75F;
			this.txtCYTDTotalPaid.Width = 0.9375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.0625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.6875F;
			this.Line4.Width = 7.3125F;
			this.Line4.X1 = 0.0625F;
			this.Line4.X2 = 7.375F;
			this.Line4.Y1 = 2.6875F;
			this.Line4.Y2 = 2.6875F;
			// 
			// Field58
			// 
			this.Field58.Height = 0.1875F;
			this.Field58.Left = 0F;
			this.Field58.Name = "Field58";
			this.Field58.Text = "Employer\'s Match";
			this.Field58.Top = 2.9375F;
			this.Field58.Width = 2.25F;
			// 
			// txtCurEmployersMatch
			// 
			this.txtCurEmployersMatch.Height = 0.1875F;
			this.txtCurEmployersMatch.Left = 2.375F;
			this.txtCurEmployersMatch.Name = "txtCurEmployersMatch";
			this.txtCurEmployersMatch.Style = "text-align: right";
			this.txtCurEmployersMatch.Text = null;
			this.txtCurEmployersMatch.Top = 2.9375F;
			this.txtCurEmployersMatch.Width = 0.9375F;
			// 
			// txtMTDEmployersMatch
			// 
			this.txtMTDEmployersMatch.Height = 0.1875F;
			this.txtMTDEmployersMatch.Left = 3.375F;
			this.txtMTDEmployersMatch.Name = "txtMTDEmployersMatch";
			this.txtMTDEmployersMatch.Style = "text-align: right";
			this.txtMTDEmployersMatch.Text = null;
			this.txtMTDEmployersMatch.Top = 2.9375F;
			this.txtMTDEmployersMatch.Width = 0.9375F;
			// 
			// txtQTDEmployersMatch
			// 
			this.txtQTDEmployersMatch.Height = 0.1875F;
			this.txtQTDEmployersMatch.Left = 4.375F;
			this.txtQTDEmployersMatch.Name = "txtQTDEmployersMatch";
			this.txtQTDEmployersMatch.Style = "text-align: right";
			this.txtQTDEmployersMatch.Text = null;
			this.txtQTDEmployersMatch.Top = 2.9375F;
			this.txtQTDEmployersMatch.Width = 0.9375F;
			// 
			// txtFYTDEmployersMatch
			// 
			this.txtFYTDEmployersMatch.Height = 0.1875F;
			this.txtFYTDEmployersMatch.Left = 5.375F;
			this.txtFYTDEmployersMatch.Name = "txtFYTDEmployersMatch";
			this.txtFYTDEmployersMatch.Style = "text-align: right";
			this.txtFYTDEmployersMatch.Text = null;
			this.txtFYTDEmployersMatch.Top = 2.9375F;
			this.txtFYTDEmployersMatch.Width = 0.9375F;
			// 
			// txtCYTDEmployersMatch
			// 
			this.txtCYTDEmployersMatch.Height = 0.1875F;
			this.txtCYTDEmployersMatch.Left = 6.375F;
			this.txtCYTDEmployersMatch.Name = "txtCYTDEmployersMatch";
			this.txtCYTDEmployersMatch.Style = "text-align: right";
			this.txtCYTDEmployersMatch.Text = null;
			this.txtCYTDEmployersMatch.Top = 2.9375F;
			this.txtCYTDEmployersMatch.Width = 0.9375F;
			// 
			// Field64
			// 
			this.Field64.Height = 0.1875F;
			this.Field64.Left = 0F;
			this.Field64.Name = "Field64";
			this.Field64.Text = "MSRS Earnings";
			this.Field64.Top = 3.125F;
			this.Field64.Width = 2.25F;
			// 
			// txtCurMSRS
			// 
			this.txtCurMSRS.Height = 0.1875F;
			this.txtCurMSRS.Left = 2.375F;
			this.txtCurMSRS.Name = "txtCurMSRS";
			this.txtCurMSRS.Style = "text-align: right";
			this.txtCurMSRS.Text = null;
			this.txtCurMSRS.Top = 3.125F;
			this.txtCurMSRS.Width = 0.9375F;
			// 
			// txtMTDMSRS
			// 
			this.txtMTDMSRS.Height = 0.1875F;
			this.txtMTDMSRS.Left = 3.375F;
			this.txtMTDMSRS.Name = "txtMTDMSRS";
			this.txtMTDMSRS.Style = "text-align: right";
			this.txtMTDMSRS.Text = null;
			this.txtMTDMSRS.Top = 3.125F;
			this.txtMTDMSRS.Width = 0.9375F;
			// 
			// txtQTDMSRS
			// 
			this.txtQTDMSRS.Height = 0.1875F;
			this.txtQTDMSRS.Left = 4.375F;
			this.txtQTDMSRS.Name = "txtQTDMSRS";
			this.txtQTDMSRS.Style = "text-align: right";
			this.txtQTDMSRS.Text = null;
			this.txtQTDMSRS.Top = 3.125F;
			this.txtQTDMSRS.Width = 0.9375F;
			// 
			// txtFYTDMSRS
			// 
			this.txtFYTDMSRS.Height = 0.1875F;
			this.txtFYTDMSRS.Left = 5.375F;
			this.txtFYTDMSRS.Name = "txtFYTDMSRS";
			this.txtFYTDMSRS.Style = "text-align: right";
			this.txtFYTDMSRS.Text = null;
			this.txtFYTDMSRS.Top = 3.125F;
			this.txtFYTDMSRS.Width = 0.9375F;
			// 
			// txtCYTDMSRS
			// 
			this.txtCYTDMSRS.Height = 0.1875F;
			this.txtCYTDMSRS.Left = 6.375F;
			this.txtCYTDMSRS.Name = "txtCYTDMSRS";
			this.txtCYTDMSRS.Style = "text-align: right";
			this.txtCYTDMSRS.Text = null;
			this.txtCYTDMSRS.Top = 3.125F;
			this.txtCYTDMSRS.Width = 0.9375F;
			// 
			// Field70
			// 
			this.Field70.Height = 0.1875F;
			this.Field70.Left = 0F;
			this.Field70.Name = "Field70";
			this.Field70.Text = "Unemployment Earnings";
			this.Field70.Top = 3.3125F;
			this.Field70.Width = 2.25F;
			// 
			// txtCurUnemployment
			// 
			this.txtCurUnemployment.Height = 0.1875F;
			this.txtCurUnemployment.Left = 2.375F;
			this.txtCurUnemployment.Name = "txtCurUnemployment";
			this.txtCurUnemployment.Style = "text-align: right";
			this.txtCurUnemployment.Text = null;
			this.txtCurUnemployment.Top = 3.3125F;
			this.txtCurUnemployment.Width = 0.9375F;
			// 
			// txtMTDUnemployment
			// 
			this.txtMTDUnemployment.Height = 0.1875F;
			this.txtMTDUnemployment.Left = 3.375F;
			this.txtMTDUnemployment.Name = "txtMTDUnemployment";
			this.txtMTDUnemployment.Style = "text-align: right";
			this.txtMTDUnemployment.Text = null;
			this.txtMTDUnemployment.Top = 3.3125F;
			this.txtMTDUnemployment.Width = 0.9375F;
			// 
			// txtQTDUnemployment
			// 
			this.txtQTDUnemployment.Height = 0.1875F;
			this.txtQTDUnemployment.Left = 4.375F;
			this.txtQTDUnemployment.Name = "txtQTDUnemployment";
			this.txtQTDUnemployment.Style = "text-align: right";
			this.txtQTDUnemployment.Text = null;
			this.txtQTDUnemployment.Top = 3.3125F;
			this.txtQTDUnemployment.Width = 0.9375F;
			// 
			// txtFYTDUnemployment
			// 
			this.txtFYTDUnemployment.Height = 0.1875F;
			this.txtFYTDUnemployment.Left = 5.375F;
			this.txtFYTDUnemployment.Name = "txtFYTDUnemployment";
			this.txtFYTDUnemployment.Style = "text-align: right";
			this.txtFYTDUnemployment.Text = null;
			this.txtFYTDUnemployment.Top = 3.3125F;
			this.txtFYTDUnemployment.Width = 0.9375F;
			// 
			// txtCYTDUnemployment
			// 
			this.txtCYTDUnemployment.Height = 0.1875F;
			this.txtCYTDUnemployment.Left = 6.375F;
			this.txtCYTDUnemployment.Name = "txtCYTDUnemployment";
			this.txtCYTDUnemployment.Style = "text-align: right";
			this.txtCYTDUnemployment.Text = null;
			this.txtCYTDUnemployment.Top = 3.3125F;
			this.txtCYTDUnemployment.Width = 0.9375F;
			// 
			// srptPayTotalsMisc
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFicaTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurFicaWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDFicaWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDFicaWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDFicaWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDFicaWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDTotalGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurEmployersMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDEmployersMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDEmployersMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDEmployersMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDEmployersMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDMSRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTDUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFYTDUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCYTDUnemployment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurFicaTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFicaTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFicaTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFicaTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFicaTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurFedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFedWage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurFicaWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDFicaWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDFicaWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDFicaWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDFicaWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDTotalGross;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field58;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurEmployersMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDEmployersMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDEmployersMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDEmployersMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDEmployersMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field64;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDMSRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurUnemployment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTDUnemployment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDUnemployment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTDUnemployment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTDUnemployment;
	}
}
