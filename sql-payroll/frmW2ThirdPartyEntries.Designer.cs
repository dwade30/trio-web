//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2ThirdPartyEntries.
	/// </summary>
	partial class frmW2ThirdPartyEntries
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboType3;
		public fecherFoundation.FCComboBox cboType2;
		public fecherFoundation.FCComboBox cboType1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuReport;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboType3 = new fecherFoundation.FCComboBox();
            this.cboType2 = new fecherFoundation.FCComboBox();
            this.cboType1 = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.vsData = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdReport = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReport)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(755, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Size = new System.Drawing.Size(755, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdReport);
            this.TopPanel.Size = new System.Drawing.Size(755, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdReport, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(312, 30);
            this.HeaderText.Text = "Additional Emp W-2 Values";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.cboType3);
            this.Frame1.Controls.Add(this.cboType2);
            this.Frame1.Controls.Add(this.cboType1);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(584, 190);
            this.Frame1.TabIndex = 1;
            // 
            // cboType3
            // 
            this.cboType3.BackColor = System.Drawing.SystemColors.Window;
            this.cboType3.Location = new System.Drawing.Point(236, 150);
            this.cboType3.Name = "cboType3";
            this.cboType3.Size = new System.Drawing.Size(342, 40);
            this.cboType3.TabIndex = 7;
            this.cboType3.SelectedIndexChanged += new System.EventHandler(this.cboType3_SelectedIndexChanged);
            // 
            // cboType2
            // 
            this.cboType2.BackColor = System.Drawing.SystemColors.Window;
            this.cboType2.Location = new System.Drawing.Point(236, 90);
            this.cboType2.Name = "cboType2";
            this.cboType2.Size = new System.Drawing.Size(342, 40);
            this.cboType2.TabIndex = 5;
            this.cboType2.SelectedIndexChanged += new System.EventHandler(this.cboType2_SelectedIndexChanged);
            // 
            // cboType1
            // 
            this.cboType1.BackColor = System.Drawing.SystemColors.Window;
            this.cboType1.Location = new System.Drawing.Point(236, 30);
            this.cboType1.Name = "cboType1";
            this.cboType1.Size = new System.Drawing.Size(342, 40);
            this.cboType1.TabIndex = 3;
            this.cboType1.SelectedIndexChanged += new System.EventHandler(this.cboType1_SelectedIndexChanged);
            this.cboType1.DropDown += new System.EventHandler(this.cboType1_DropDown);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 164);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(150, 15);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "ADDITIONAL W2 CODE 3";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(150, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "ADDITIONAL W2 CODE 2";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(150, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "ADDITIONAL W2 CODE 1";
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 10;
            this.vsData.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsData.Location = new System.Drawing.Point(30, 210);
            this.vsData.Name = "vsData";
            this.vsData.Rows = 50;
            this.vsData.Size = new System.Drawing.Size(663, 512);
            this.vsData.TabIndex = 2;
            this.vsData.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsData_AfterEdit);
            this.vsData.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsData_MouseDownEvent);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuReport,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                            ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit          ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 2;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuReport
            // 
            this.mnuReport.Index = 3;
            this.mnuReport.Name = "mnuReport";
            this.mnuReport.Text = "Additional W2 Information Report";
            this.mnuReport.Click += new System.EventHandler(this.mnuReport_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(340, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdReport
            // 
            this.cmdReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdReport.Location = new System.Drawing.Point(501, 29);
            this.cmdReport.Name = "cmdReport";
            this.cmdReport.Size = new System.Drawing.Size(226, 24);
            this.cmdReport.TabIndex = 1;
            this.cmdReport.Text = "Additional W2 Information Report";
            this.cmdReport.Click += new System.EventHandler(this.mnuReport_Click);
            // 
            // frmW2ThirdPartyEntries
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(755, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmW2ThirdPartyEntries";
            this.Text = "Additional Emp W-2 Values";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmW2ThirdPartyEntries_Load);
            this.Activated += new System.EventHandler(this.frmW2ThirdPartyEntries_Activated);
            this.Resize += new System.EventHandler(this.frmW2ThirdPartyEntries_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW2ThirdPartyEntries_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReport)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdReport;
	}
}