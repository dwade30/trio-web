//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmACH.
	/// </summary>
	partial class frmACH
	{
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtEmpRT;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCTextBox txtEmpAccount;
		public fecherFoundation.FCTextBox txtEmployerID;
		public fecherFoundation.FCComboBox cmbAccountType;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtImmediateOriginName;
		public fecherFoundation.FCTextBox txtImmediateOriginRT;
		public fecherFoundation.FCTextBox txtODFI;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtACHRT;
		public fecherFoundation.FCTextBox txtBankName;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtEmpRT = new fecherFoundation.FCTextBox();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.txtEmpAccount = new fecherFoundation.FCTextBox();
            this.txtEmployerID = new fecherFoundation.FCTextBox();
            this.cmbAccountType = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtImmediateOriginName = new fecherFoundation.FCTextBox();
            this.txtImmediateOriginRT = new fecherFoundation.FCTextBox();
            this.txtODFI = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtACHRT = new fecherFoundation.FCTextBox();
            this.txtBankName = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 418);
            this.BottomPanel.Size = new System.Drawing.Size(998, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(1018, 534);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1018, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(257, 30);
            this.HeaderText.Text = "ACH Bank Information";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtEmpRT);
            this.Frame2.Controls.Add(this.txtEmployerName);
            this.Frame2.Controls.Add(this.txtEmpAccount);
            this.Frame2.Controls.Add(this.txtEmployerID);
            this.Frame2.Controls.Add(this.cmbAccountType);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label6);
            this.Frame2.Location = new System.Drawing.Point(30, 302);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(960, 116);
            this.Frame2.TabIndex = 12;
            this.Frame2.Text = "Employer Information";
            // 
            // txtEmpRT
            // 
            this.txtEmpRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpRT.Location = new System.Drawing.Point(310, 56);
            this.txtEmpRT.MaxLength = 9;
            this.txtEmpRT.Name = "txtEmpRT";
            this.txtEmpRT.Size = new System.Drawing.Size(130, 40);
            this.txtEmpRT.TabIndex = 17;
            this.txtEmpRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtEmployerName
            // 
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.Location = new System.Drawing.Point(20, 56);
            this.txtEmployerName.MaxLength = 23;
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(270, 40);
            this.txtEmployerName.TabIndex = 16;
            // 
            // txtEmpAccount
            // 
            this.txtEmpAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmpAccount.Location = new System.Drawing.Point(460, 56);
            this.txtEmpAccount.MaxLength = 17;
            this.txtEmpAccount.Name = "txtEmpAccount";
            this.txtEmpAccount.Size = new System.Drawing.Size(180, 40);
            this.txtEmpAccount.TabIndex = 15;
            this.txtEmpAccount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtEmployerID
            // 
            this.txtEmployerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerID.Location = new System.Drawing.Point(660, 56);
            this.txtEmployerID.MaxLength = 9;
            this.txtEmployerID.Name = "txtEmployerID";
            this.txtEmployerID.Size = new System.Drawing.Size(130, 40);
            this.txtEmployerID.TabIndex = 14;
            this.txtEmployerID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccountType.Location = new System.Drawing.Point(810, 56);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(130, 40);
            this.cmbAccountType.TabIndex = 13;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 16);
            this.Label3.TabIndex = 21;
            this.Label3.Text = "NAME";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(310, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(80, 16);
            this.Label4.TabIndex = 20;
            this.Label4.Text = "R/T NUMBER";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(460, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(130, 16);
            this.Label5.TabIndex = 19;
            this.Label5.Text = "ACCOUNT NUMBER";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(660, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(86, 16);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "EMPLOYER ID";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtImmediateOriginName);
            this.Frame4.Controls.Add(this.txtImmediateOriginRT);
            this.Frame4.Controls.Add(this.txtODFI);
            this.Frame4.Controls.Add(this.Label9);
            this.Frame4.Controls.Add(this.Label10);
            this.Frame4.Controls.Add(this.Label11);
            this.Frame4.Location = new System.Drawing.Point(30, 166);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(610, 116);
            this.Frame4.TabIndex = 5;
            this.Frame4.Text = "Immediate Origin";
            this.ToolTip1.SetToolTip(this.Frame4, "If files are not sent to an intermediate entity this information should be the sa" +
        "me as the employer information");
            // 
            // txtImmediateOriginName
            // 
            this.txtImmediateOriginName.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginName.Location = new System.Drawing.Point(20, 56);
            this.txtImmediateOriginName.MaxLength = 23;
            this.txtImmediateOriginName.Name = "txtImmediateOriginName";
            this.txtImmediateOriginName.Size = new System.Drawing.Size(270, 40);
            this.txtImmediateOriginName.TabIndex = 8;
            // 
            // txtImmediateOriginRT
            // 
            this.txtImmediateOriginRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginRT.Location = new System.Drawing.Point(310, 56);
            this.txtImmediateOriginRT.MaxLength = 10;
            this.txtImmediateOriginRT.Name = "txtImmediateOriginRT";
            this.txtImmediateOriginRT.Size = new System.Drawing.Size(130, 40);
            this.txtImmediateOriginRT.TabIndex = 7;
            this.txtImmediateOriginRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtImmediateOriginRT, "Normally the same as the ODFI number but can be a unique identifier");
            // 
            // txtODFI
            // 
            this.txtODFI.BackColor = System.Drawing.SystemColors.Window;
            this.txtODFI.Location = new System.Drawing.Point(460, 56);
            this.txtODFI.MaxLength = 9;
            this.txtODFI.Name = "txtODFI";
            this.txtODFI.Size = new System.Drawing.Size(130, 40);
            this.txtODFI.TabIndex = 6;
            this.txtODFI.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtODFI, "The R/T of the originator");
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 30);
            this.Label9.Name = "Label9";
            this.Label9.TabIndex = 11;
            this.Label9.Text = "BANK NAME";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(310, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(121, 16);
            this.Label10.TabIndex = 10;
            this.Label10.Text = "IMMEDIATE ORIGIN #";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(460, 30);
            this.Label11.Name = "Label11";
            this.Label11.TabIndex = 9;
            this.Label11.Text = "ODFI NUMBER";
            this.ToolTip1.SetToolTip(this.Label11, "Normally the Immediate Origin but can be a unique employer identifier");
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtACHRT);
            this.Frame1.Controls.Add(this.txtBankName);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(460, 116);
            this.Frame1.TabIndex = 13;
            this.Frame1.Text = "Bank / Immediate Destination";
            // 
            // txtACHRT
            // 
            this.txtACHRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtACHRT.Location = new System.Drawing.Point(310, 56);
            this.txtACHRT.MaxLength = 9;
            this.txtACHRT.Name = "txtACHRT";
            this.txtACHRT.Size = new System.Drawing.Size(130, 40);
            this.txtACHRT.TabIndex = 2;
            this.txtACHRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtBankName
            // 
            this.txtBankName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBankName.Location = new System.Drawing.Point(20, 56);
            this.txtBankName.MaxLength = 23;
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(270, 40);
            this.txtBankName.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.TabIndex = 4;
            this.Label1.Text = "BANK NAME";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(310, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 16);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "R/T NUMBER";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(454, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmACH
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1018, 594);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmACH";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ACH Bank Information";
            this.Load += new System.EventHandler(this.frmACH_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmACH_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
