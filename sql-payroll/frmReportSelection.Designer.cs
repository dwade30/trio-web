//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmReportSelection.
	/// </summary>
	partial class frmReportSelection
	{
		public fecherFoundation.FCComboBox cmbOption;
		public fecherFoundation.FCLabel lblOption;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCCheckBox chk941C1;
		public fecherFoundation.FCCheckBox chkOther;
		public fecherFoundation.FCCheckBox chkPayrollWarrant;
		public fecherFoundation.FCCheckBox chkTrust;
		public fecherFoundation.FCCheckBox chkPayTaxSummary;
		public fecherFoundation.FCCheckBox chkDeductions;
		public fecherFoundation.FCCheckBox chkBankList;
		public fecherFoundation.FCCheckBox chkAccounting;
		public fecherFoundation.FCCheckBox chkDataEntryForms;
		public fecherFoundation.FCCheckBox chkVacation;
		public fecherFoundation.FCCheckBox chkSick;
		public fecherFoundation.FCCheckBox chkAudit;
		public fecherFoundation.FCLabel chkCheckRegister;
		public fecherFoundation.FCLabel chkBackup;
		public fecherFoundation.FCLabel chkTempList;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuReset;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbOption = new fecherFoundation.FCComboBox();
			this.lblOption = new fecherFoundation.FCLabel();
			this.fraBanks = new fecherFoundation.FCFrame();
			this.chk941C1 = new fecherFoundation.FCCheckBox();
			this.chkOther = new fecherFoundation.FCCheckBox();
			this.chkPayrollWarrant = new fecherFoundation.FCCheckBox();
			this.chkTrust = new fecherFoundation.FCCheckBox();
			this.chkPayTaxSummary = new fecherFoundation.FCCheckBox();
			this.chkDeductions = new fecherFoundation.FCCheckBox();
			this.chkBankList = new fecherFoundation.FCCheckBox();
			this.chkAccounting = new fecherFoundation.FCCheckBox();
			this.chkDataEntryForms = new fecherFoundation.FCCheckBox();
			this.chkVacation = new fecherFoundation.FCCheckBox();
			this.chkSick = new fecherFoundation.FCCheckBox();
			this.chkAudit = new fecherFoundation.FCCheckBox();
			this.chkCheckRegister = new fecherFoundation.FCLabel();
			this.chkBackup = new fecherFoundation.FCLabel();
			this.chkTempList = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuReset = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip();
			this.cmdClearOptions = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
			this.fraBanks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chk941C1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayrollWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTrust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayTaxSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBankList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccounting)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryForms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVacation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSick)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAudit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearOptions)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 639);
			this.BottomPanel.Size = new System.Drawing.Size(411, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbOption);
			this.ClientArea.Controls.Add(this.lblOption);
			this.ClientArea.Controls.Add(this.fraBanks);
			this.ClientArea.Controls.Add(this.cmdNew);
			this.ClientArea.Controls.Add(this.cmdDelete);
			this.ClientArea.Size = new System.Drawing.Size(431, 628);
			this.ClientArea.Controls.SetChildIndex(this.cmdDelete, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdNew, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraBanks, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblOption, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbOption, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClearOptions);
			this.TopPanel.Size = new System.Drawing.Size(431, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClearOptions, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(269, 28);
			this.HeaderText.Text = "Full Set Report Selection";
			// 
			// cmbOption
			// 
			this.cmbOption.Items.AddRange(new object[] {
            "Weekly",
            "Monthly",
            "Quarterly",
            "Calendar"});
			this.cmbOption.Location = new System.Drawing.Point(137, 30);
			this.cmbOption.Name = "cmbOption";
			this.cmbOption.Size = new System.Drawing.Size(258, 40);
			this.cmbOption.TabIndex = 1001;
			this.cmbOption.SelectedIndexChanged += new System.EventHandler(this.optOption_CheckedChanged);
			// 
			// lblOption
			// 
			this.lblOption.AutoSize = true;
			this.lblOption.Location = new System.Drawing.Point(30, 44);
			this.lblOption.Name = "lblOption";
			this.lblOption.Size = new System.Drawing.Size(88, 15);
			this.lblOption.TabIndex = 1;
			this.lblOption.Text = "TIME PERIOD";
			// 
			// fraBanks
			// 
			this.fraBanks.Controls.Add(this.chk941C1);
			this.fraBanks.Controls.Add(this.chkOther);
			this.fraBanks.Controls.Add(this.chkPayrollWarrant);
			this.fraBanks.Controls.Add(this.chkTrust);
			this.fraBanks.Controls.Add(this.chkPayTaxSummary);
			this.fraBanks.Controls.Add(this.chkDeductions);
			this.fraBanks.Controls.Add(this.chkBankList);
			this.fraBanks.Controls.Add(this.chkAccounting);
			this.fraBanks.Controls.Add(this.chkDataEntryForms);
			this.fraBanks.Controls.Add(this.chkVacation);
			this.fraBanks.Controls.Add(this.chkSick);
			this.fraBanks.Controls.Add(this.chkAudit);
			this.fraBanks.Controls.Add(this.chkCheckRegister);
			this.fraBanks.Controls.Add(this.chkBackup);
			this.fraBanks.Controls.Add(this.chkTempList);
			this.fraBanks.Controls.Add(this.Label5);
			this.fraBanks.Controls.Add(this.Label4);
			this.fraBanks.Controls.Add(this.Label3);
			this.fraBanks.Controls.Add(this.Label2);
			this.fraBanks.Controls.Add(this.Label1);
			this.fraBanks.Location = new System.Drawing.Point(30, 80);
			this.fraBanks.Name = "fraBanks";
			this.fraBanks.Size = new System.Drawing.Size(365, 559);
			this.fraBanks.TabIndex = 19;
			this.fraBanks.Tag = "Employee #";
			this.fraBanks.Text = "Report Title";
			// 
			// chk941C1
			// 
			this.chk941C1.Location = new System.Drawing.Point(20, 512);
			this.chk941C1.Name = "chk941C1";
			this.chk941C1.Size = new System.Drawing.Size(73, 22);
			this.chk941C1.TabIndex = 30;
			this.chk941C1.Text = "941/C1";
			// 
			// chkOther
			// 
			this.chkOther.Location = new System.Drawing.Point(20, 351);
			this.chkOther.Name = "chkOther";
			this.chkOther.Size = new System.Drawing.Size(64, 22);
			this.chkOther.TabIndex = 13;
			this.chkOther.Text = "Other";
			this.chkOther.CheckedChanged += new System.EventHandler(this.chkOther_CheckedChanged);
			// 
			// chkPayrollWarrant
			// 
			this.chkPayrollWarrant.Location = new System.Drawing.Point(20, 475);
			this.chkPayrollWarrant.Name = "chkPayrollWarrant";
			this.chkPayrollWarrant.Size = new System.Drawing.Size(121, 22);
			this.chkPayrollWarrant.TabIndex = 15;
			this.chkPayrollWarrant.Text = "Payroll Warrant";
			this.chkPayrollWarrant.CheckedChanged += new System.EventHandler(this.chkPayrollWarrant_CheckedChanged);
			// 
			// chkTrust
			// 
			this.chkTrust.Location = new System.Drawing.Point(20, 30);
			this.chkTrust.Name = "chkTrust";
			this.chkTrust.Size = new System.Drawing.Size(163, 22);
			this.chkTrust.TabIndex = 5;
			this.chkTrust.Text = "Trust & Agency Report";
			this.chkTrust.CheckedChanged += new System.EventHandler(this.chkTrust_CheckedChanged);
			// 
			// chkPayTaxSummary
			// 
			this.chkPayTaxSummary.Location = new System.Drawing.Point(20, 92);
			this.chkPayTaxSummary.Name = "chkPayTaxSummary";
			this.chkPayTaxSummary.Size = new System.Drawing.Size(151, 22);
			this.chkPayTaxSummary.TabIndex = 6;
			this.chkPayTaxSummary.Text = "Pay & Tax Summary";
			this.chkPayTaxSummary.CheckedChanged += new System.EventHandler(this.chkPayTaxSummary_CheckedChanged);
			// 
			// chkDeductions
			// 
			this.chkDeductions.Location = new System.Drawing.Point(20, 129);
			this.chkDeductions.Name = "chkDeductions";
			this.chkDeductions.Size = new System.Drawing.Size(133, 22);
			this.chkDeductions.TabIndex = 7;
			this.chkDeductions.Text = "Deduction Report";
			this.chkDeductions.CheckedChanged += new System.EventHandler(this.chkDeductions_CheckedChanged);
			// 
			// chkBankList
			// 
			this.chkBankList.Location = new System.Drawing.Point(20, 166);
			this.chkBankList.Name = "chkBankList";
			this.chkBankList.Size = new System.Drawing.Size(113, 22);
			this.chkBankList.TabIndex = 8;
			this.chkBankList.Text = "Direct Deposit";
			this.chkBankList.CheckedChanged += new System.EventHandler(this.chkBankList_CheckedChanged);
			// 
			// chkAccounting
			// 
			this.chkAccounting.Location = new System.Drawing.Point(20, 203);
			this.chkAccounting.Name = "chkAccounting";
			this.chkAccounting.Size = new System.Drawing.Size(192, 22);
			this.chkAccounting.TabIndex = 9;
			this.chkAccounting.Text = "Payroll Accounting Charges";
			this.chkAccounting.CheckedChanged += new System.EventHandler(this.chkAccounting_CheckedChanged);
			// 
			// chkDataEntryForms
			// 
			this.chkDataEntryForms.Location = new System.Drawing.Point(20, 240);
			this.chkDataEntryForms.Name = "chkDataEntryForms";
			this.chkDataEntryForms.Size = new System.Drawing.Size(133, 22);
			this.chkDataEntryForms.TabIndex = 10;
			this.chkDataEntryForms.Text = "Data Entry Forms";
			this.chkDataEntryForms.CheckedChanged += new System.EventHandler(this.chkDataEntryForms_CheckedChanged);
			// 
			// chkVacation
			// 
			this.chkVacation.Location = new System.Drawing.Point(20, 277);
			this.chkVacation.Name = "chkVacation";
			this.chkVacation.Size = new System.Drawing.Size(82, 22);
			this.chkVacation.TabIndex = 11;
			this.chkVacation.Text = "Vacation";
			this.chkVacation.CheckedChanged += new System.EventHandler(this.chkVacation_CheckedChanged);
			// 
			// chkSick
			// 
			this.chkSick.Location = new System.Drawing.Point(20, 314);
			this.chkSick.Name = "chkSick";
			this.chkSick.Size = new System.Drawing.Size(56, 22);
			this.chkSick.TabIndex = 12;
			this.chkSick.Text = "Sick";
			this.chkSick.CheckedChanged += new System.EventHandler(this.chkSick_CheckedChanged);
			// 
			// chkAudit
			// 
			this.chkAudit.Checked = true;
			this.chkAudit.CheckState = Wisej.Web.CheckState.Checked;
			this.chkAudit.Location = new System.Drawing.Point(20, 388);
			this.chkAudit.Name = "chkAudit";
			this.chkAudit.Size = new System.Drawing.Size(104, 22);
			this.chkAudit.TabIndex = 14;
			this.chkAudit.Text = "Audit Report";
			this.ToolTip1.SetToolTip(this.chkAudit, "Required Data");
			this.chkAudit.CheckedChanged += new System.EventHandler(this.chkAudit_CheckedChanged);
			// 
			// chkCheckRegister
			// 
			this.chkCheckRegister.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chkCheckRegister.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.chkCheckRegister.Location = new System.Drawing.Point(20, 67);
			this.chkCheckRegister.Name = "chkCheckRegister";
			this.chkCheckRegister.Size = new System.Drawing.Size(173, 17);
			this.chkCheckRegister.TabIndex = 29;
			this.chkCheckRegister.Text = "CHECK REGISTER";
			// 
			// chkBackup
			// 
			this.chkBackup.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chkBackup.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.chkBackup.Location = new System.Drawing.Point(20, 450);
			this.chkBackup.Name = "chkBackup";
			this.chkBackup.Size = new System.Drawing.Size(173, 17);
			this.chkBackup.TabIndex = 28;
			this.chkBackup.Text = "BACKUP & DELETE AUDIT";
			// 
			// chkTempList
			// 
			this.chkTempList.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chkTempList.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.chkTempList.Location = new System.Drawing.Point(20, 425);
			this.chkTempList.Name = "chkTempList";
			this.chkTempList.Size = new System.Drawing.Size(173, 17);
			this.chkTempList.TabIndex = 27;
			this.chkTempList.Text = "TEMP LIST/DIST RESET";
			// 
			// Label5
			// 
			this.Label5.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.Label5.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.Label5.Location = new System.Drawing.Point(262, 450);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(90, 17);
			this.Label5.TabIndex = 25;
			this.Label5.Text = "(REQUIRED)";
			// 
			// Label4
			// 
			this.Label4.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.Label4.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.Label4.Location = new System.Drawing.Point(262, 425);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(90, 17);
			this.Label4.TabIndex = 24;
			this.Label4.Text = "(REQUIRED)";
			// 
			// Label3
			// 
			this.Label3.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.Label3.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.Label3.Location = new System.Drawing.Point(262, 398);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(90, 17);
			this.Label3.TabIndex = 23;
			this.Label3.Text = "(REQUIRED)";
			this.Label3.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.Label2.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.Label2.Location = new System.Drawing.Point(262, 67);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(90, 17);
			this.Label2.TabIndex = 22;
			this.Label2.Text = "(REQUIRED)";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(75, 16);
			this.Label1.TabIndex = 21;
			this.Label1.Text = "(REQUIRED)";
			this.Label1.Visible = false;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(173, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 17;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Location = new System.Drawing.Point(321, 247);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(55, 24);
			this.cmdDelete.TabIndex = 16;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Visible = false;
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Location = new System.Drawing.Point(270, 210);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(45, 24);
			this.cmdNew.TabIndex = 20;
			this.cmdNew.Text = "New";
			this.cmdNew.Visible = false;
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuReset,
            this.mnuSP1,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP3,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuReset
			// 
			this.mnuReset.Index = 0;
			this.mnuReset.Name = "mnuReset";
			this.mnuReset.Text = "Clear options";
			this.mnuReset.Click += new System.EventHandler(this.mnuReset_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 1;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Enabled = false;
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Enabled = false;
			this.mnuPrintPreview.Index = 3;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print/Preview";
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 4;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 5;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                                          ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 6;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit                         ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP3
			// 
			this.mnuSP3.Index = 7;
			this.mnuSP3.Name = "mnuSP3";
			this.mnuSP3.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 8;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdClearOptions
			// 
			this.cmdClearOptions.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClearOptions.Location = new System.Drawing.Point(303, 29);
			this.cmdClearOptions.Name = "cmdClearOptions";
			this.cmdClearOptions.Size = new System.Drawing.Size(100, 24);
			this.cmdClearOptions.TabIndex = 21;
			this.cmdClearOptions.Text = "Clear Options";
			this.cmdClearOptions.Click += new System.EventHandler(this.mnuReset_Click);
			// 
			// frmReportSelection
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(431, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReportSelection";
			this.Text = "Full Set Report Selection";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmReportSelection_Load);
			this.Activated += new System.EventHandler(this.frmReportSelection_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReportSelection_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
			this.fraBanks.ResumeLayout(false);
			this.fraBanks.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chk941C1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayrollWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTrust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayTaxSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBankList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccounting)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryForms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVacation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSick)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAudit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearOptions)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		private System.ComponentModel.IContainer components;
		private FCButton cmdClearOptions;
	}
}