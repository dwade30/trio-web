//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2CList.
	/// </summary>
	partial class frmW2CList
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbW2;
		public fecherFoundation.FCLabel lblW2;
		public fecherFoundation.FCTextBox txtYear;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuLoadList;
		public fecherFoundation.FCToolStripMenuItem mnuSaveList;
		public fecherFoundation.FCToolStripMenuItem mnuPrintTestPage;
		public fecherFoundation.FCToolStripMenuItem mnuPrintW3CTestPage;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.cmbW2 = new fecherFoundation.FCComboBox();
            this.lblW2 = new fecherFoundation.FCLabel();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLoadList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintTestPage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintW3CTestPage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdLoadList = new fecherFoundation.FCButton();
            this.cmdSaveList = new fecherFoundation.FCButton();
            this.cmdPrintW2C = new fecherFoundation.FCButton();
            this.cmdPrintW3C = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintW2C)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintW3C)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 518);
            this.BottomPanel.Size = new System.Drawing.Size(953, 85);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.cmbW2);
            this.ClientArea.Controls.Add(this.lblW2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(973, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblW2, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbW2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbType, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdLoadList);
            this.TopPanel.Controls.Add(this.cmdSaveList);
            this.TopPanel.Controls.Add(this.cmdPrintW2C);
            this.TopPanel.Controls.Add(this.cmdPrintW3C);
            this.TopPanel.Size = new System.Drawing.Size(973, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintW3C, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintW2C, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveList, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadList, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(65, 28);
            this.HeaderText.Text = "W-2C";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Paper",
            "Electronic"});
            this.cmbType.Location = new System.Drawing.Point(806, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(159, 40);
            this.cmbType.TabIndex = 4;
            this.cmbType.Text = "Paper";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(686, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(38, 15);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "TYPE";
            // 
            // cmbW2
            // 
            this.cmbW2.Items.AddRange(new object[] {
            "W-2",
            "W-2C"});
            this.cmbW2.Location = new System.Drawing.Point(211, 30);
            this.cmbW2.Name = "cmbW2";
            this.cmbW2.Size = new System.Drawing.Size(159, 40);
            this.cmbW2.TabIndex = 2;
            this.cmbW2.Text = "W-2";
            // 
            // lblW2
            // 
            this.lblW2.AutoSize = true;
            this.lblW2.Location = new System.Drawing.Point(30, 44);
            this.lblW2.Name = "lblW2";
            this.lblW2.Size = new System.Drawing.Size(133, 15);
            this.lblW2.TabIndex = 7;
            this.lblW2.Text = "CORRECTING FORM";
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(526, 30);
            this.txtYear.Name = "txtYear";
            this.txtYear.TabIndex = 3;
            // 
            // Grid
            // 
            this.Grid.Cols = 6;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 100);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(923, 418);
            this.Grid.TabIndex = 4;
            this.Grid.TabStop = false;
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_BeforeRowColChange);
            this.Grid.Click += new System.EventHandler(this.Grid_Click);
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            this.Grid.Validating += new System.ComponentModel.CancelEventHandler(this.Grid_Validating);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(400, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(61, 16);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "TAX YEAR";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuLoadList,
            this.mnuSaveList,
            this.mnuPrintTestPage,
            this.mnuPrintW3CTestPage,
            this.mnuSepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuLoadList
            // 
            this.mnuLoadList.Index = 0;
            this.mnuLoadList.Name = "mnuLoadList";
            this.mnuLoadList.Text = "Load List";
            this.mnuLoadList.Click += new System.EventHandler(this.mnuLoadList_Click);
            // 
            // mnuSaveList
            // 
            this.mnuSaveList.Index = 1;
            this.mnuSaveList.Name = "mnuSaveList";
            this.mnuSaveList.Text = "Save List";
            this.mnuSaveList.Click += new System.EventHandler(this.mnuSaveList_Click);
            // 
            // mnuPrintTestPage
            // 
            this.mnuPrintTestPage.Index = 2;
            this.mnuPrintTestPage.Name = "mnuPrintTestPage";
            this.mnuPrintTestPage.Text = "Print W-2C Test Page";
            this.mnuPrintTestPage.Click += new System.EventHandler(this.mnuPrintTestPage_Click);
            // 
            // mnuPrintW3CTestPage
            // 
            this.mnuPrintW3CTestPage.Index = 3;
            this.mnuPrintW3CTestPage.Name = "mnuPrintW3CTestPage";
            this.mnuPrintW3CTestPage.Text = "Print W-3C Test Page";
            this.mnuPrintW3CTestPage.Click += new System.EventHandler(this.mnuPrintW3CTestPage_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 4;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 5;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Continue";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 6;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 7;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(400, 24);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.TabStop = false;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdLoadList
            // 
            this.cmdLoadList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadList.Location = new System.Drawing.Point(471, 29);
            this.cmdLoadList.Name = "cmdLoadList";
            this.cmdLoadList.Size = new System.Drawing.Size(76, 24);
            this.cmdLoadList.TabIndex = 1;
            this.cmdLoadList.TabStop = false;
            this.cmdLoadList.Text = "Load List";
            this.cmdLoadList.Click += new System.EventHandler(this.mnuLoadList_Click);
            // 
            // cmdSaveList
            // 
            this.cmdSaveList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveList.Location = new System.Drawing.Point(553, 29);
            this.cmdSaveList.Name = "cmdSaveList";
            this.cmdSaveList.Size = new System.Drawing.Size(78, 24);
            this.cmdSaveList.TabIndex = 2;
            this.cmdSaveList.TabStop = false;
            this.cmdSaveList.Text = "Save List";
            this.cmdSaveList.Click += new System.EventHandler(this.mnuSaveList_Click);
            // 
            // cmdPrintW2C
            // 
            this.cmdPrintW2C.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintW2C.Location = new System.Drawing.Point(637, 29);
            this.cmdPrintW2C.Name = "cmdPrintW2C";
            this.cmdPrintW2C.Size = new System.Drawing.Size(150, 24);
            this.cmdPrintW2C.TabIndex = 3;
            this.cmdPrintW2C.TabStop = false;
            this.cmdPrintW2C.Text = "Print W-2C Test Page";
            this.cmdPrintW2C.Click += new System.EventHandler(this.mnuPrintTestPage_Click);
            // 
            // cmdPrintW3C
            // 
            this.cmdPrintW3C.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintW3C.Location = new System.Drawing.Point(793, 29);
            this.cmdPrintW3C.Name = "cmdPrintW3C";
            this.cmdPrintW3C.Size = new System.Drawing.Size(152, 24);
            this.cmdPrintW3C.TabIndex = 4;
            this.cmdPrintW3C.TabStop = false;
            this.cmdPrintW3C.Text = "Print W-3C Test Page";
            this.cmdPrintW3C.Click += new System.EventHandler(this.mnuPrintW3CTestPage_Click);
            // 
            // frmW2CList
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(973, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmW2CList";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "W-2C";
            this.Load += new System.EventHandler(this.frmW2CList_Load);
            this.Resize += new System.EventHandler(this.frmW2CList_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2CList_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintW2C)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintW3C)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdSaveList;
		private FCButton cmdLoadList;
		private FCButton cmdPrintW3C;
		private FCButton cmdPrintW2C;
	}
}