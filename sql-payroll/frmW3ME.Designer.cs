//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW3ME.
	/// </summary>
	partial class frmW3ME
	{
		public fecherFoundation.FCComboBox cmbElectronic;
		public fecherFoundation.FCLabel lblElectronic;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtThirdPartyAmount;
		public fecherFoundation.FCTextBox txtThirdPartyID;
		public fecherFoundation.FCTextBox txtThirdPartyName;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCCheckBox chk1099;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtReported;
		public fecherFoundation.FCTextBox txtWithheld;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCFrame framFile;
		public fecherFoundation.FCTextBox txtFilename;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtMRSID;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtContactTitle;
		public fecherFoundation.FCTextBox txtExtension;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCTextBox txtContactName;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbElectronic = new fecherFoundation.FCComboBox();
            this.lblElectronic = new fecherFoundation.FCLabel();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtThirdPartyAmount = new fecherFoundation.FCTextBox();
            this.txtThirdPartyID = new fecherFoundation.FCTextBox();
            this.txtThirdPartyName = new fecherFoundation.FCTextBox();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.chk1099 = new fecherFoundation.FCCheckBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtReported = new fecherFoundation.FCTextBox();
            this.txtWithheld = new fecherFoundation.FCTextBox();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.framFile = new fecherFoundation.FCFrame();
            this.txtFilename = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtMRSID = new fecherFoundation.FCTextBox();
            this.txtFederalID = new fecherFoundation.FCTextBox();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtContactTitle = new fecherFoundation.FCTextBox();
            this.txtExtension = new fecherFoundation.FCTextBox();
            this.txtPhone = new Global.T2KPhoneNumberBox();
            this.txtContactName = new fecherFoundation.FCTextBox();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1099)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framFile)).BeginInit();
            this.framFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(848, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chk1099);
            this.ClientArea.Controls.Add(this.Frame7);
            this.ClientArea.Controls.Add(this.cmbElectronic);
            this.ClientArea.Controls.Add(this.lblElectronic);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.framFile);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(848, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(848, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(94, 30);
            this.HeaderText.Text = "W-3ME";
            // 
            // cmbElectronic
            // 
            this.cmbElectronic.AutoSize = false;
            this.cmbElectronic.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbElectronic.FormattingEnabled = true;
            this.cmbElectronic.Items.AddRange(new object[] {
            "Electronic",
            "Paper"});
            this.cmbElectronic.Location = new System.Drawing.Point(114, 30);
            this.cmbElectronic.Name = "cmbElectronic";
            this.cmbElectronic.Size = new System.Drawing.Size(145, 40);
            this.cmbElectronic.TabIndex = 31;
            this.cmbElectronic.SelectedIndexChanged += new System.EventHandler(this.optElectronic_CheckedChanged);
            // 
            // lblElectronic
            // 
            this.lblElectronic.AutoSize = true;
            this.lblElectronic.Location = new System.Drawing.Point(30, 44);
            this.lblElectronic.Name = "lblElectronic";
            this.lblElectronic.Size = new System.Drawing.Size(60, 15);
            this.lblElectronic.TabIndex = 32;
            this.lblElectronic.Text = "FORMAT";
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtThirdPartyAmount);
            this.Frame7.Controls.Add(this.txtThirdPartyID);
            this.Frame7.Controls.Add(this.txtThirdPartyName);
            this.Frame7.Controls.Add(this.Label1_4);
            this.Frame7.Controls.Add(this.Label1_3);
            this.Frame7.Controls.Add(this.Label1_2);
            this.Frame7.Location = new System.Drawing.Point(30, 317);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(784, 140);
            this.Frame7.TabIndex = 30;
            this.Frame7.Text = "Third-Party Sick Pay";
            // 
            // txtThirdPartyAmount
            // 
            this.txtThirdPartyAmount.AutoSize = false;
            this.txtThirdPartyAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtThirdPartyAmount.LinkItem = null;
            this.txtThirdPartyAmount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtThirdPartyAmount.LinkTopic = null;
            this.txtThirdPartyAmount.Location = new System.Drawing.Point(494, 80);
            this.txtThirdPartyAmount.Name = "txtThirdPartyAmount";
            this.txtThirdPartyAmount.Size = new System.Drawing.Size(170, 40);
            this.txtThirdPartyAmount.TabIndex = 35;
            this.txtThirdPartyAmount.Text = "0";
            // 
            // txtThirdPartyID
            // 
			this.txtThirdPartyID.MaxLength = 9;
            this.txtThirdPartyID.AutoSize = false;
            this.txtThirdPartyID.BackColor = System.Drawing.SystemColors.Window;
            this.txtThirdPartyID.LinkItem = null;
            this.txtThirdPartyID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtThirdPartyID.LinkTopic = null;
            this.txtThirdPartyID.Location = new System.Drawing.Point(128, 80);
            this.txtThirdPartyID.Name = "txtThirdPartyID";
            this.txtThirdPartyID.Size = new System.Drawing.Size(170, 40);
            this.txtThirdPartyID.TabIndex = 33;
            // 
            // txtThirdPartyName
            // 
			this.txtThirdPartyName.MaxLength = 31;
            this.txtThirdPartyName.AutoSize = false;
            this.txtThirdPartyName.BackColor = System.Drawing.SystemColors.Window;
            this.txtThirdPartyName.LinkItem = null;
            this.txtThirdPartyName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtThirdPartyName.LinkTopic = null;
            this.txtThirdPartyName.Location = new System.Drawing.Point(128, 30);
            this.txtThirdPartyName.Name = "txtThirdPartyName";
            this.txtThirdPartyName.Size = new System.Drawing.Size(536, 40);
            this.txtThirdPartyName.TabIndex = 31;
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(318, 94);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(150, 16);
            this.Label1_4.TabIndex = 36;
            this.Label1_4.Text = "WITHHOLDING REMITTED";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 94);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(65, 16);
            this.Label1_3.TabIndex = 34;
            this.Label1_3.Text = "PAYER ID";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 44);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(85, 16);
            this.Label1_2.TabIndex = 32;
            this.Label1_2.Text = "PAYER NAME";
            // 
            // chk1099
            // 
            this.chk1099.Location = new System.Drawing.Point(30, 180);
            this.chk1099.Name = "chk1099";
            this.chk1099.Size = new System.Drawing.Size(641, 27);
            this.chk1099.TabIndex = 4;
            this.chk1099.Text = "Submitting Maine withholding data on form 1099 electronically or on magnetic medi" +
    "a";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtReported);
            this.Frame5.Controls.Add(this.txtWithheld);
            this.Frame5.Controls.Add(this.Label1_12);
            this.Frame5.Controls.Add(this.Label1_11);
            this.Frame5.Location = new System.Drawing.Point(30, 80);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(521, 90);
            this.Frame5.TabIndex = 24;
            this.Frame5.Text = "Maine Income Tax Withheld / Reported";
            // 
            // txtReported
            // 
            this.txtReported.AutoSize = false;
            this.txtReported.BackColor = System.Drawing.SystemColors.Window;
            this.txtReported.LinkItem = null;
            this.txtReported.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtReported.LinkTopic = null;
            this.txtReported.Location = new System.Drawing.Point(365, 30);
            this.txtReported.Name = "txtReported";
            this.txtReported.Size = new System.Drawing.Size(136, 40);
            this.txtReported.TabIndex = 3;
            this.txtReported.Text = "0";
            // 
            // txtWithheld
            // 
            this.txtWithheld.AutoSize = false;
            this.txtWithheld.BackColor = System.Drawing.SystemColors.Window;
            this.txtWithheld.LinkItem = null;
            this.txtWithheld.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtWithheld.LinkTopic = null;
            this.txtWithheld.Location = new System.Drawing.Point(113, 30);
            this.txtWithheld.Name = "txtWithheld";
            this.txtWithheld.Size = new System.Drawing.Size(136, 40);
            this.txtWithheld.TabIndex = 2;
            this.txtWithheld.Text = "0";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(269, 44);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(61, 16);
            this.Label1_12.TabIndex = 26;
            this.Label1_12.Text = "REPORTED";
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(20, 44);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(70, 16);
            this.Label1_11.TabIndex = 25;
            this.Label1_11.Text = "WITHHELD";
            // 
            // framFile
            // 
            this.framFile.Controls.Add(this.txtFilename);
            this.framFile.Controls.Add(this.Label2);
            this.framFile.Location = new System.Drawing.Point(30, 767);
            this.framFile.Name = "framFile";
            this.framFile.Size = new System.Drawing.Size(388, 90);
            this.framFile.TabIndex = 22;
            this.framFile.Text = "File";
            // 
            // txtFilename
            // 
			this.txtFilename.MaxLength = 7;
            this.txtFilename.AutoSize = false;
            this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilename.LinkItem = null;
            this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFilename.LinkTopic = null;
            this.txtFilename.Location = new System.Drawing.Point(206, 30);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(162, 40);
            this.txtFilename.TabIndex = 12;
            this.txtFilename.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFilename_KeyDown);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(170, 16);
            this.Label2.TabIndex = 23;
            this.Label2.Text = "FILENAME (NO EXTENSION)";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtMRSID);
            this.Frame3.Controls.Add(this.txtFederalID);
            this.Frame3.Controls.Add(this.Label1_10);
            this.Frame3.Controls.Add(this.Label1_8);
            this.Frame3.Location = new System.Drawing.Point(30, 617);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(548, 140);
            this.Frame3.TabIndex = 19;
            this.Frame3.Text = "Employer Account & Id Numbers";
            // 
            // txtMRSID
            // 
			this.txtMRSID.MaxLength = 11;
            this.txtMRSID.AutoSize = false;
            this.txtMRSID.BackColor = System.Drawing.SystemColors.Window;
            this.txtMRSID.LinkItem = null;
            this.txtMRSID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMRSID.LinkTopic = null;
            this.txtMRSID.Location = new System.Drawing.Point(366, 80);
            this.txtMRSID.Name = "txtMRSID";
            this.txtMRSID.Size = new System.Drawing.Size(162, 40);
            this.txtMRSID.TabIndex = 11;
            this.txtMRSID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRSID_KeyPress);
            // 
            // txtFederalID
            // 
			this.txtFederalID.MaxLength = 9;
            this.txtFederalID.AutoSize = false;
            this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalID.LinkItem = null;
            this.txtFederalID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFederalID.LinkTopic = null;
            this.txtFederalID.Location = new System.Drawing.Point(366, 30);
            this.txtFederalID.Name = "txtFederalID";
            this.txtFederalID.Size = new System.Drawing.Size(162, 40);
            this.txtFederalID.TabIndex = 10;
            this.txtFederalID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalID_KeyPress);
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 94);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(320, 16);
            this.Label1_10.TabIndex = 21;
            this.Label1_10.Text = "MAINE REVENUE SERVICES WITHHOLDING ACCOUNT ID";
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 44);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(140, 16);
            this.Label1_8.TabIndex = 20;
            this.Label1_8.Text = "FEDERAL EMPLOYER ID";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtContactTitle);
            this.Frame2.Controls.Add(this.txtExtension);
            this.Frame2.Controls.Add(this.txtPhone);
            this.Frame2.Controls.Add(this.txtContactName);
            this.Frame2.Controls.Add(this.Label1_1);
            this.Frame2.Controls.Add(this.Label1_7);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Location = new System.Drawing.Point(30, 467);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(784, 140);
            this.Frame2.TabIndex = 15;
            this.Frame2.Text = "Contact Information For Individual Responsible For Accuracy Of Form";
            // 
            // txtContactTitle
            // 
			this.txtContactTitle.MaxLength = 30;
            this.txtContactTitle.AutoSize = false;
            this.txtContactTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactTitle.LinkItem = null;
            this.txtContactTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactTitle.LinkTopic = null;
            this.txtContactTitle.Location = new System.Drawing.Point(87, 80);
            this.txtContactTitle.Name = "txtContactTitle";
            this.txtContactTitle.Size = new System.Drawing.Size(230, 40);
            this.txtContactTitle.TabIndex = 9;
            // 
            // txtExtension
            // 
			this.txtExtension.MaxLength = 4;
            this.txtExtension.AutoSize = false;
            this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtension.LinkItem = null;
            this.txtExtension.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExtension.LinkTopic = null;
            this.txtExtension.Location = new System.Drawing.Point(704, 30);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(60, 40);
            this.txtExtension.TabIndex = 8;
            this.txtExtension.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExtension_KeyPress);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(398, 30);
            this.txtPhone.Mask = "(999)000-0000";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(150, 40);
            this.txtPhone.TabIndex = 7;
            this.txtPhone.Text = "(   )    -";
            // 
            // txtContactName
            // 
			this.txtContactName.MaxLength = 30;
            this.txtContactName.AutoSize = false;
            this.txtContactName.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactName.LinkItem = null;
            this.txtContactName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactName.LinkTopic = null;
            this.txtContactName.Location = new System.Drawing.Point(87, 30);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(230, 40);
            this.txtContactName.TabIndex = 6;
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(35, 16);
            this.Label1_1.TabIndex = 29;
            this.Label1_1.Text = "TITLE";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(569, 44);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(110, 16);
            this.Label1_7.TabIndex = 18;
            this.Label1_7.Text = "EXT. OR MAILBOX";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(337, 44);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(45, 16);
            this.Label1_6.TabIndex = 17;
            this.Label1_6.Text = "PHONE";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 44);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(40, 16);
            this.Label1_5.TabIndex = 16;
            this.Label1_5.Text = "NAME";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtEmployerName);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Location = new System.Drawing.Point(30, 217);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(521, 90);
            this.Frame1.TabIndex = 13;
            this.Frame1.Text = "Employer";
            // 
            // txtEmployerName
            // 
			this.txtEmployerName.MaxLength = 44;
            this.txtEmployerName.AutoSize = false;
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.LinkItem = null;
            this.txtEmployerName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployerName.LinkTopic = null;
            this.txtEmployerName.Location = new System.Drawing.Point(116, 30);
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(385, 40);
            this.txtEmployerName.TabIndex = 5;
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 44);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 16);
            this.Label1_0.TabIndex = 14;
            this.Label1_0.Text = "EMPLOYER";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Continue";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(330, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(176, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmW3ME
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(848, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmW3ME";
            this.Text = "W-3ME";
            this.Load += new System.EventHandler(this.frmW3ME_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW3ME_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk1099)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framFile)).EndInit();
            this.framFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
