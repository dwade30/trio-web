//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptFUTA940Report.
	/// </summary>
	public partial class rptFUTA940Report : BaseSectionReport
	{
		public rptFUTA940Report()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "FUTA Report (940)";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptFUTA940Report InstancePtr
		{
			get
			{
				return (rptFUTA940Report)Sys.GetInstance(typeof(rptFUTA940Report));
			}
		}

		protected rptFUTA940Report _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptFUTA940Report	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		int lngReportYear;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			lblTaxYear.Text = "Tax Year: " + FCConvert.ToString(lngReportYear);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("FUTA");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldYTDPayroll As object	OnWrite(string)
			// vbPorter upgrade warning: fldExcess As object	OnWrite(string)
			// vbPorter upgrade warning: txtExemptPayments As object	OnWrite(string)
			// vbPorter upgrade warning: fldFUTABase As object	OnWrite(string)
			// vbPorter upgrade warning: fldTaxable As object	OnWrite(string)
			// vbPorter upgrade warning: fldGRTaxRate As object	OnWrite(string)
			// vbPorter upgrade warning: fldMXCreditRate As object	OnWrite(string)
			// vbPorter upgrade warning: fldSTTaxRate As object	OnWrite(string)
			// vbPorter upgrade warning: fldActualTaxRate As object	OnWrite(string)
			// vbPorter upgrade warning: fldGRTax As object	OnWrite(string)
			// vbPorter upgrade warning: fldMXCredit As object	OnWrite(string)
			// vbPorter upgrade warning: fldSTTax As object	OnWrite(string)
			// vbPorter upgrade warning: fldActualTax As object	OnWrite(string)
			// vbPorter upgrade warning: fldAdditionalCredit As object	OnWrite(string)
			clsDRWrapper rsYTDInfo = new clsDRWrapper();
			clsDRWrapper rsFUTAInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curTotalPayroll As Decimal	OnWrite(int, Decimal)
			Decimal curTotalPayroll;
			// vbPorter upgrade warning: curExcess As Decimal	OnWrite(int, Decimal)
			Decimal curExcess;
			// vbPorter upgrade warning: curFUTABase As Decimal	OnWrite
			Decimal curFUTABase;
			Decimal curTaxable;
			double curExempt;
			rsFUTAInfo.OpenRecordset("SELECT * FROM tblStandardLimits");
			if (rsFUTAInfo.EndOfFile() != true && rsFUTAInfo.BeginningOfFile() != true)
			{
				curFUTABase = FCConvert.ToDecimal(rsFUTAInfo.Get_Fields("FUTABase"));
			}
			else
			{
				rsFUTAInfo.AddNew();
				rsFUTAInfo.Update(true);
				rsFUTAInfo.OpenRecordset("SELECT * FROM tblStandardLimits");
				curFUTABase = 0;
			}
			rsYTDInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblCheckDetail.PayDate >= '1/1/" + FCConvert.ToString(lngReportYear) + "' AND tblCheckDetail.PayDate <= '12/31/" + FCConvert.ToString(lngReportYear) + "' AND CheckVoid = 0 GROUP BY tblEmployeeMaster.EmployeeNumber");
			curTotalPayroll = 0;
			curExcess = 0;
			if (rsYTDInfo.EndOfFile() != true && rsYTDInfo.BeginningOfFile() != true)
			{
				do
				{
					curTotalPayroll += FCConvert.ToDecimal(Conversion.Val(rsYTDInfo.Get_Fields("YTDGross")));
					rsYTDInfo.MoveNext();
				}
				while (rsYTDInfo.EndOfFile() != true);
			}
			fldYTDPayroll.Text = Strings.Format(curTotalPayroll, "#,##0.00");
			// get the excess amount
			rsYTDInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE tblEmployeeMaster.UnemploymentExempt = 0 AND tblCheckDetail.DistU <> 'N' AND tblCheckDetail.PayDate >= '1/1/" + FCConvert.ToString(lngReportYear) + "' AND tblCheckDetail.PayDate <= '12/31/" + FCConvert.ToString(lngReportYear) + "'  AND CheckVoid = 0  GROUP BY tblEmployeeMaster.EmployeeNumber");
			curExcess = 0;
			if (rsYTDInfo.EndOfFile() != true && rsYTDInfo.BeginningOfFile() != true)
			{
				do
				{
					curExcess += modDavesSweetCode.CalculateExcess(FCConvert.ToDecimal(Conversion.Val(rsYTDInfo.Get_Fields("YTDGross"))), FCConvert.ToDecimal(Conversion.Val(rsYTDInfo.Get_Fields("YTDGross"))), curFUTABase);
					rsYTDInfo.MoveNext();
				}
				while (rsYTDInfo.EndOfFile() != true);
			}
			fldExcess.Text = Strings.Format(curExcess, "#,##0.00");
			curExempt = 0;
			// get the exempt payments amount
			rsYTDInfo.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, SUM(tblCheckDetail.DistGrossPay) as YTDGross FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) WHERE (tblEmployeeMaster.UnemploymentExempt = 1 or tblCheckDetail.DistU = 'N') AND tblCheckDetail.PayDate >= '1/1/" + FCConvert.ToString(lngReportYear) + "' AND tblCheckDetail.PayDate <= '12/31/" + FCConvert.ToString(lngReportYear) + "'  AND CheckVoid = 0 GROUP BY tblEmployeeMaster.EmployeeNumber");
			curTotalPayroll = 0;
			if (rsYTDInfo.EndOfFile() != true && rsYTDInfo.BeginningOfFile() != true)
			{
				do
				{
					curExempt += Conversion.Val(rsYTDInfo.Get_Fields("ytdgross"));
					curTotalPayroll += FCConvert.ToDecimal(Conversion.Val(rsYTDInfo.Get_Fields("YTDGross")));
					rsYTDInfo.MoveNext();
				}
				while (rsYTDInfo.EndOfFile() != true);
			}
			curTaxable = FCConvert.ToDecimal(fldYTDPayroll.Text) - FCConvert.ToDecimal(fldExcess.Text) - FCConvert.ToDecimal(curExempt);
			txtExemptPayments.Text = Strings.Format(curTotalPayroll, "#,##0.00");
			fldFUTABase.Text = Strings.Format(curFUTABase, "#,##0.00");
			fldTaxable.Text = Strings.Format(curTaxable, "#,##0.00");
			fldGRTaxRate.Text = Strings.Format(rsFUTAInfo.Get_Fields("FUTARate"), "0.0000") + "%";
			fldMXCreditRate.Text = Strings.Format(rsFUTAInfo.Get_Fields("FUTAMaxCredit"), "0.0000") + "%";
			fldSTTaxRate.Text = Strings.Format(rsFUTAInfo.Get_Fields_Double("UnemploymentRate"), "0.0000") + "%";
			fldActualTaxRate.Text = Strings.Format(rsFUTAInfo.Get_Fields("FUTAActual"), "0.0000") + "%";
			fldGRTax.Text = Strings.Format(curTaxable * FCConvert.ToDecimal((rsFUTAInfo.Get_Fields("FUTARate") / 100)), "#,##0.00");
			fldMXCredit.Text = Strings.Format(curTaxable * (FCConvert.ToDecimal(rsFUTAInfo.Get_Fields("FUTAMaxCredit") / 100)), "#,##0.00");
			fldSTTax.Text = Strings.Format(Conversion.Val(curTaxable) * (rsFUTAInfo.Get_Fields_Double("UnemploymentRate") / 100), "#,##0.00");
			fldActualTax.Text = Strings.Format(curTaxable * (FCConvert.ToDecimal(rsFUTAInfo.Get_Fields("FUTAActual") / 100)), "#,##0.00");
			if (FCConvert.ToDecimal(fldMXCredit.Text) > FCConvert.ToDecimal(fldSTTax.Text))
			{
				fldAdditionalCredit.Text = Strings.Format(FCConvert.ToDecimal(fldMXCredit.Text) - FCConvert.ToDecimal(fldSTTax.Text), "#,##0.00");
			}
			else
			{
				fldAdditionalCredit.Text = "0.00";
			}
			rsYTDInfo.Dispose();
			rsFUTAInfo.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(int lngYear, List<string> batchReports = null)
		{
			lngReportYear = lngYear;
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDuplexPrinting.DuplexPrintReport(rptFUTA940Report.InstancePtr, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptFUTA940Report.PrintReport False
			}
			else
			{
				// rptFUTA940Report.Show , MDIParent
				modCoreysSweeterCode.CheckDefaultPrint(rptFUTA940Report.InstancePtr, boolAllowEmail: true, strAttachmentName: "FUTA940");
			}
		}

		
	}
}
