//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3ME.
	/// </summary>
	public partial class rptW3ME : BaseSectionReport
	{
		public rptW3ME()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W-3ME";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW3ME InstancePtr
		{
			get
			{
				return (rptW3ME)Sys.GetInstance(typeof(rptW3ME));
			}
		}

		protected rptW3ME _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW3ME	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolTestPrint;
		double dblWithheldAmount;
		double dblReportedAmount;
		bool boolSubmitting1099;
		private string strThirdPartyName = string.Empty;
		private string strThirdPartyID = string.Empty;
		private double dblThirdParty;
		private double dblThirdPartyEmployer;
		private bool boolPrintBlank;

		public void Init(ref double dblWithheld, ref double dblReported, ref bool bool1099, double dblThirdPartyWH, ref double dblThirdPartyEmployerWH, string strThirdParty, string strThirdPartyIDNum, bool modalDialog, bool boolPrintTest = false, bool boolBlankPrint = false)
		{
			boolTestPrint = boolPrintTest;
			boolPrintBlank = boolBlankPrint;
			dblWithheldAmount = dblWithheld;
			dblReportedAmount = dblReported;
			boolSubmitting1099 = bool1099;
			dblThirdParty = dblThirdPartyWH;
			dblThirdPartyEmployer = dblThirdPartyEmployerWH;
			strThirdPartyName = strThirdParty;
			strThirdPartyID = strThirdPartyIDNum;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "W-3ME", showModal: modalDialog);
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID =
                        Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid")), 1, 2) + " " +
                        Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid")), 3);
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!boolTestPrint && !boolPrintBlank)
			{
				LoadInfo();
				txtDate.Text = Strings.Format(DateTime.Today, "mm-dd-yy");
				txtName.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerName);
				txtWithholdingAccount.Text = EWRRecord.MRSWithholdingID;
				// txtUCEmployerAccount.Text = .StateUCAccount
				strTemp = EWRRecord.TransmitterPhone;
				strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
				txtPhone.Text = strTemp;
				txtTitle.Text = EWRRecord.TransmitterTitle;
				txtName2.Text = "";
				if (fecherFoundation.Strings.Trim(strThirdPartyID) != string.Empty)
				{
					strTemp = strThirdPartyID;
					txtThirdPartyID.Text = Strings.Mid(strTemp, 1, 2) + " " + Strings.Mid(strTemp, 3);
				}
				else
				{
					txtThirdPartyID.Text = "";
				}
				txtThirdPartyName.Text = strThirdPartyName;
				if (boolSubmitting1099)
				{
					txtSaving1099.Visible = true;
				}
				else
				{
					txtSaving1099.Visible = false;
				}
				txtLine1.Text = Strings.Format(dblWithheldAmount, "0.00");
				txtLine2.Text = Strings.Format(dblReportedAmount, "0.00");
				txtLine3.Text = Strings.Format(dblThirdParty, "0.00");
				txtLine4.Text = Strings.Format(dblThirdPartyEmployer, "0.00");
			}
			else if (boolPrintBlank)
			{
				txtDate.Text = "";
				txtName.Text = "";
				txtWithholdingAccount.Text = "";
				txtPhone.Text = "";
				txtTitle.Text = "";
				txtName2.Text = "";
				txtThirdPartyID.Text = "";
				txtThirdPartyName.Text = "";
				txtSaving1099.Visible = false;
				txtLine1.Text = "";
				txtLine2.Text = "";
				txtLine3.Text = "";
				txtLine4.Text = "";
			}
		}

		
	}
}
