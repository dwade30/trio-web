//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cPYTaxStatusCodeController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public Dictionary<object, object> GetTaxStatusCodesAsDictionary()
		{
			Dictionary<object, object> GetTaxStatusCodesAsDictionary = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Dictionary<object, object> dictCodes = new Dictionary<object, object>();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cPYTaxStatusCode tempCode;
				rsLoad.OpenRecordset("select * from tbltaxstatuscodes order by taxstatuscode", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					tempCode = new cPYTaxStatusCode();
					tempCode.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
					tempCode.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					tempCode.IsFederalExempt = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Fedexempt"));
					tempCode.IsMedicareExempt = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("MedicareExempt"));
					tempCode.IsStateExempt = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("StateExempt"));
					tempCode.LastUpdate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("LastUpdate"));
					tempCode.LastUserID = FCConvert.ToString(rsLoad.Get_Fields("LastUserID"));
					tempCode.Pertains = FCConvert.ToBoolean(rsLoad.Get_Fields_Int32("Pertains"));
					tempCode.TaxStatusCode = FCConvert.ToString(rsLoad.Get_Fields("TaxStatusCode"));
					dictCodes.Add(tempCode.TaxStatusCode, tempCode);
					rsLoad.MoveNext();
				}
				GetTaxStatusCodesAsDictionary = dictCodes;
				return GetTaxStatusCodesAsDictionary;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetTaxStatusCodesAsDictionary;
		}
	}
}
