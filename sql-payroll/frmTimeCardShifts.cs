//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmTimeCardShifts : BaseForm
	{
		public frmTimeCardShifts()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTimeCardShifts InstancePtr
		{
			get
			{
				return (frmTimeCardShifts)Sys.GetInstance(typeof(frmTimeCardShifts));
			}
		}

		protected frmTimeCardShifts _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int cnstgridcolID = 0;
		const int CNSTGRIDCOLSHIFT = 1;
		const int CNSTGRIDCOLTIMECLOCK = 2;

		private void frmTimeCardShifts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTimeCardShifts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTimeCardShifts properties;
			//frmTimeCardShifts.FillStyle	= 0;
			//frmTimeCardShifts.ScaleWidth	= 9300;
			//frmTimeCardShifts.ScaleHeight	= 7650;
			//frmTimeCardShifts.LinkTopic	= "Form2";
			//frmTimeCardShifts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void SetupGrid()
		{
			string strSQL = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strCombo = "";
			Grid.ColHidden(cnstgridcolID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLSHIFT, "Shift Type");
			Grid.TextMatrix(0, CNSTGRIDCOLTIMECLOCK, "");
			strSQL = "Select * from SHIFTtypes order by name";
			rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				strCombo += "#" + rsLoad.Get_Fields("shiftid") + ";" + rsLoad.Get_Fields_String("Name") + "|";
				rsLoad.MoveNext();
			}
			if (strCombo != "")
			{
				strCombo = Strings.Mid(strCombo, 1, strCombo.Length - 1);
			}
			Grid.ColComboList(CNSTGRIDCOLSHIFT, strCombo);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLSHIFT, FCConvert.ToInt32(0.48 * GridWidth));
		}

		private void frmTimeCardShifts_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void AddLine()
		{
			int lngRow;
			Grid.Rows += 1;
			lngRow = Grid.Rows - 1;
			Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(0));
			Grid.RowData(lngRow, true);
		}

		private void DeleteLine()
		{
			if (Grid.Row < 1)
				return;
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID)) > 0)
			{
				GridDelete.Rows += 1;
				GridDelete.TextMatrix(GridDelete.Rows - 1, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, cnstgridcolID))));
			}
			Grid.RemoveItem(Grid.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						AddLine();
						break;
					}
				case Keys.Delete:
					{
						DeleteLine();
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				Close();
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "Select * from TimeClockShifts";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				Grid.Rows = 1;
				int lngRow;
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, cnstgridcolID, FCConvert.ToString(rsLoad.Get_Fields("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSHIFT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("payrollshift"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLTIMECLOCK, FCConvert.ToString(rsLoad.Get_Fields("timecardshift")));
					Grid.RowData(lngRow, false);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveData = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
				int intRow;
				Grid.Col = 0;
				for (intRow = 0; intRow <= (GridDelete.Rows - 1); intRow++)
				{
					strSQL = "Delete from timeclockshifts where ID = " + GridDelete.TextMatrix(intRow, 0);
				}
				// intRow
				GridDelete.Rows = 0;
				for (intRow = 1; intRow <= (Grid.Rows - 1); intRow++)
				{
					if (FCConvert.CBool(Grid.RowData(intRow)))
					{
						if (Conversion.Val(Grid.TextMatrix(intRow, cnstgridcolID)) > 0)
						{
							strSQL = "update TimeClockshifts set payrollshift = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intRow, CNSTGRIDCOLSHIFT))) + ", timecardshift = '" + Grid.TextMatrix(intRow, CNSTGRIDCOLTIMECLOCK) + "' ";
							strSQL += " where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(intRow, cnstgridcolID)));
						}
						else
						{
							strSQL = "Insert into TimeClockShifts (PayrollShift,TimeCardShift) values (";
							strSQL += Grid.TextMatrix(intRow, CNSTGRIDCOLSHIFT) + ",'" + Grid.TextMatrix(intRow, CNSTGRIDCOLTIMECLOCK) + "')";
						}
						rsSave.Execute(strSQL, "twpy0000.vb1");
					}
				}
				// intRow
				SaveData = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
