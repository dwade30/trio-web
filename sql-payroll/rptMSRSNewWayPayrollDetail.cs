using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSNewWayPayrollDetail.
	/// </summary>
	public partial class rptMSRSNewWayPayrollDetail : BaseSectionReport
	{
		public rptMSRSNewWayPayrollDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MSRS Payroll Detail";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMSRSNewWayPayrollDetail InstancePtr
		{
			get
			{
				return (rptMSRSNewWayPayrollDetail)Sys.GetInstance(typeof(rptMSRSNewWayPayrollDetail));
			}
		}

		protected rptMSRSNewWayPayrollDetail _InstancePtr = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsMiscInfo?.Dispose();
				clsLoad?.Dispose();
				clsPayrate?.Dispose();
                clsMiscInfo = null;
                clsLoad = null;
                clsPayrate = null;
            }
			base.Dispose(disposing);
		}
        clsDRWrapper clsMiscInfo = new clsDRWrapper();
		clsDRWrapper clsLoad = new clsDRWrapper();
		clsDRWrapper clsPayrate = new clsDRWrapper();
		int PaybackCode;
		int RetirementCode;
		int LifeInsuranceCode;
		double RetirementAmount;
		double LifeInsuranceAmount;
		double dblTotalLifeInsPremium;
		string strCurrentEmployeeCode = "";
		double dblTotalEarnCompensation;
		double dblTotalEmployeeContributions;
		bool boolEndofReport;
		bool boolTeacherReport;
		bool boolRetiredSection;
		double dblEffBasInsRate;
		bool boolTeachers;
		bool boolRange;
		bool boolNoData;
		int intPageNumber;
		int lngReportType;
		DateTime dtBeginDate;
		DateTime dtEndDate;
		bool boolElectronicMSRS;

		clsMSRS clsMSRSObject;
		const int CONSTTEACHERS = 2;
		const int CONSTNONTEACHERS = 1;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("fldLifeInsPremium");
			this.Fields.Add("fldEarnedCompensation");
			this.Fields.Add("fldGrantComp");
			this.Fields.Add("fldEmployeeContributions");
			this.Fields["fldlifeinspremium"].Value = 0;
			this.Fields["fldearnedcompensation"].Value = 0;
			this.Fields["fldGrantComp"].Value = 0;
			this.Fields["fldemployeecontributions"].Value = 0;
			boolEndofReport = false;
			boolRetiredSection = false;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolNoData)
			{
				eArgs.EOF = true;
				return;
			}
			eArgs.EOF = clsLoad.EndOfFile();
			if (!clsLoad.EndOfFile())
			{
				this.Fields["fldemployeecontributions"].Value = clsLoad.Get_Fields_Double("EmployeeContributions");
                if (strCurrentEmployeeCode != clsLoad.Get_Fields_String("employeenumber"))
				{
                    this.Fields["fldLifeInsPremium"].Value = clsLoad.Get_Fields_Double("LifeInsurancePremium");
                    this.Fields["fldemployeecontributions"].Value = clsLoad.Get_Fields_Double("EmployeeContributions");
				}
				else
				{
					this.Fields["fldlifeinspremium"].Value = 0;
                }
				this.Fields["fldgrantcomp"].Value = 0;
               
				if (clsLoad.Get_Fields_String("statuscode").ToIntegerValue() != 53)
				{
                    if (clsLoad.Get_Fields_Double("employeecontributions") == 0)
					{
						this.Fields["fldearnedcompensation"].Value = 0;
					}
					else
					{
						this.Fields["fldearnedcompensation"].Value = clsLoad.Get_Fields_Double("EarnableCompensation");
					}
					// End If
				}
				else
				{
                    if (clsLoad.Get_Fields_Double("employeecontributions") == 0)
					{
						this.Fields["fldearnedcompensation"].Value = 0;
					}
					else
					{
						this.Fields["fldearnedcompensation"].Value = clsLoad.Get_Fields_Double("earnablecompensation");
					}
				}
				if (!clsLoad.EndOfFile())
				{
					if (clsLoad.Get_Fields_Boolean("retired") == true)
					{
						if (!boolRetiredSection)
						{
                            Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
						}
						else
						{
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
						}
					}
				}
				else
				{
					Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
			}
			else
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			if (eArgs.EOF)
				boolEndofReport = true;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
            int intPageIndex;
			int X;
			string strCopy = "";
			float lngXCoord = 0;
			float lngYCoord = 0;
			if (boolNoData)
				return;
			intPageIndex = this.Document.Pages.Count;
            int nrPages = this.Document.Pages.Count;
			for(int i = 0; i < nrPages; i++)
			{
				GrapeCity.ActiveReports.Document.Section.Page pg = (GrapeCity.ActiveReports.Document.Section.Page)Document.Pages[i].Clone();
                this.Document.Pages.Insert(intPageIndex, pg);
				intPageIndex += 1;
			}
			// pg
			intPageIndex = FCConvert.ToInt16(this.Document.Pages.Count / 2 - 1);
			X = 0;
			foreach (GrapeCity.ActiveReports.Document.Section.Page pg in this.Document.Pages)
			{
				//Application.DoEvents();
				if (X <= intPageIndex)
				{
					// first report
					strCopy = "MainePERS COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 1530 / 1440f;
					lngYCoord = 14850 / 1440f;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800 / 1440f, 270 / 1440f);
				}
				else
				{
					// second report
					strCopy = "EMPLOYER COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 1530 / 1440f;
					lngYCoord = 14850 / 1440f;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800 / 1440f, 270 / 1440f);
				}
				X += 1;
			}
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			int X;
			string strTemp;
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite
			DateTime dtTemp;
			string strWhere;
			// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
			DateTime dtStart;
			// vbPorter upgrade warning: dtEnd As DateTime	OnWrite
			DateTime dtEnd;
			string strSelect = "";
			string strSubQuery;
			string strSubQueryRetired;
			string strYTDQuery = "";
			string strQuery1 = "";
			string strLifeInsQuery = "";
			string strEContQuery = "";
			string strPaybackQuery = "";
			string strRange = "";
			string strTextRange = "";
			string strTeacher = "";
			string strTeacher2 = "";
			string strTotalECompSQL = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsTemp2 = new clsDRWrapper();
			clsDRWrapper clsTemp3 = new clsDRWrapper();
			clsDRWrapper clsTemp4 = new clsDRWrapper();
			clsDRWrapper clsTemp5 = new clsDRWrapper();
			clsDRWrapper clsSequenceCheck = new clsDRWrapper();
			string strWhere2;
			string strSQLTemp = "";
			string strSQLDeductionCodes;
			clsDRWrapper rsMSRSTempDetail = new clsDRWrapper();

			
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "PRESQUE ISLE")
			{
				Label36.Visible = true;
				txtDateOfHire.Visible = true;
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				lngReportType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("MSRSReportType"))));
			}
			boolNoData = false;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			strCurrentEmployeeCode = "";
			Shape1.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			Shape2.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			// clear the table
			clsTemp.Execute("delete from tblmsrstempdetailreport", "twpy0000.vb1");
			// now load the position and status codes
			// Call clsMiscInfo.OpenRecordset("select employeenumber,positioncode from tblmiscupdate order by employeenumber", "twpy0000.vb1")
			strSQL = "select * from tblmsrstable where reporttype = " + FCConvert.ToString(lngReportType);
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (lngReportType == modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER)
			{
				boolTeachers = true;
				txtTitle1.Text = "TEACHERS";
				txtTitle1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				strTeacher2 = " and LEFT(isnull(positioncode, ' '),1) = 'Y'";
				strTeacher = " and (((isnull(msrspayrollquery1.JobID, '')) = 'Y' and left(isnull(positioncode, ' '),1) = 'Y') or ((left((msrspayrollquery1.JobID & ' '),1) = 'Y' ) and len(isnull(msrspayrollquery1.JobID, '')) > 1))";
			}
			else
			{
				boolTeachers = false;
				strTeacher2 = " and left(isnull(positioncode, ' '),1) <> 'Y' ";
				strTeacher = " and (((isnull(msrspayrollquery1.JobID, '')) = 'Y' and left(isnull(positioncode, ' '),1) <> 'Y') or ((left((msrspayrollquery1.JobID & ' '),1) <> 'Y' ) and len(isnull(msrspayrollquery1.JobID, '')) > 1))";
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields("Range")))
			{
				boolRange = true;
				if (FCConvert.ToString(clsLoad.Get_Fields("rangestart")) == string.Empty || FCConvert.ToString(clsLoad.Get_Fields("rangeend")) == string.Empty)
				{
					boolRange = false;
				}
			}
			else
			{
				boolRange = false;
			}
			if (boolRange)
			{
				strRange = "  seqnumber between " + clsLoad.Get_Fields("rangestart") + " and " + clsLoad.Get_Fields("rangeend") + " ";
				strTextRange = " and seqnumber between '" + clsLoad.Get_Fields("rangestart") + "' and '" + clsLoad.Get_Fields("rangeend") + "' ";
				clsSequenceCheck.OpenRecordset("select employeenumber,seqnumber from tblEmployeeMaster where " + strRange + " order by employeenumber", "twpy0000.vb1");
			}
			else
			{
				strRange = "";
			}
			txtEmployerCode.Text = clsLoad.Get_Fields_String("EmployerCode");
			txtEmployerName.Text = clsLoad.Get_Fields_String("EmployerName");
			txtEffectiveBasicInsuranceRatePer1000.Text = Strings.Format(clsLoad.Get_Fields("effbasinsrate"), "0.00");
			dblEffBasInsRate = Conversion.Val(clsLoad.Get_Fields("effbasinsrate")) / 1000;
			strTemp = "";
			dtTemp = DateTime.FromOADate(0);
			dtEnd = DateTime.FromOADate(0);
			
			dtStart = FCConvert.ToDateTime("1/1/2900");
			for (X = 1; X <= 5; X++)
			{
				if (!clsLoad.IsFieldNull("PaidDatesEnding" + FCConvert.ToString(X)) && clsLoad.Get_Fields_DateTime("PaidDatesEnding" + FCConvert.ToString(X)).ToOADate() != dtTemp.ToOADate())
				{
					if (fecherFoundation.DateAndTime.DateDiff("d", dtEnd, (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X))) > 0)
					{
						dtEnd = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
					}
					if (fecherFoundation.DateAndTime.DateDiff("d", (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X)), dtStart) > 0)
					{
						dtStart = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
					}
					strTemp += Strings.Format(clsLoad.Get_Fields_DateTime("PaidDatesEnding" + FCConvert.ToString(X)), "MM/dd/yyyy") + " ";
				}
			}
			// X
			txtPaidDates.Text = strTemp;
			dtStart = FCConvert.ToDateTime(FCConvert.ToString(dtStart.Month) + "/1/" + FCConvert.ToString(dtStart.Year));
			strWhere = " paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' ";
			strWhere2 = strWhere;
			dtBeginDate = dtStart;
			dtEndDate = dtEnd;
			strSQL = "select sum(distgrosspay) as EarnableCompensation,sum(disthours) as MonthlyHours,employeenumber,distm,statuscode from tblcheckdetail  where reporttype = " + FCConvert.ToString(lngReportType) + " and distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 ";
			strSQL += " and " + strWhere + " group by employeenumber,distm,statuscode";
            clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
            clsTemp.OpenRecordset("select * from tblpaycategories where upper(isnull(type, '')) = 'HOURS' order by ID", "twpy0000.vb1");
			strTemp = "";
			while (!clsTemp.EndOfFile())
			{
				strTemp += clsTemp.Get_Fields("ID") + ",";
				clsTemp.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				strTemp = "(" + strTemp + ")";
			}
			strSQL = "select sum(disthours) as MonthlyHours,employeenumber,distm,statuscode from tblcheckdetail  where reporttype = " + lngReportType + " and distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 ";
			strSQL += "  and distpayCATEGORY in " + strTemp + " and " + strWhere + " group by employeenumber,distm,statuscode";
			clsTemp3.OpenRecordset(strSQL, "twpy0000.vb1");
			strSQL = "select sum(disthours) as adjusthours,employeenumber,distm,statuscode from tblcheckdetail where reporttype = " + lngReportType + " and distm <> 'N' and len(isnull(distm, '')) > 0 and checkvoid = 0 and msrsadjustrecord = 1 and " + strWhere + " group by employeenumber,distm,statuscode";
			clsTemp4.OpenRecordset(strSQL, "twpy0000.vb1");
			strSQL = "select * from tblmiscupdate inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblmiscupdate.employeenumber) where include = 1 order by tblmiscupdate.employeenumber";
			clsMiscInfo.OpenRecordset(strSQL, "twpy0000.vb1");
			bool boolFoundIt = false;
			double tempHours = 0;
			clsTemp2.OpenRecordset("select * from tblcheckdetail where distributionrecord = 1 and reporttype = " + lngReportType + " and " + strWhere + " order by employeenumber,paydate desc,REPORTTYPE DESC", "twpy0000.vb1");
			
			rsMSRSTempDetail.OpenRecordset("SELECT * FROM tblMSRSTempDetailReport", "TWPY0000.vb1");

			while (!clsLoad.EndOfFile())
			{
				// only do the ones that are supposed to be included in msrs
				if (clsMiscInfo.FindFirstRecord("employeenumber", clsLoad.Get_Fields_String("employeenumber")))
				{
					boolFoundIt = true;
                    if (fecherFoundation.Strings.Trim(strRange) != string.Empty)
					{
						boolFoundIt = false;
						if (clsSequenceCheck.FindFirstRecord("employeenumber", clsLoad.Get_Fields_String("employeenumber")))
						{
							boolFoundIt = true;
						}
					}
					if (boolFoundIt)
					{

						rsMSRSTempDetail.AddNew();
						rsMSRSTempDetail.Set_Fields("retired", false);
						rsMSRSTempDetail.Set_Fields("employeenumber", clsLoad.Get_Fields_String("employeenumber"));
						rsMSRSTempDetail.Set_Fields("earnablecompensation", clsLoad.Get_Fields_Double("earnablecompensation"));
						rsMSRSTempDetail.Set_Fields("positioncode", clsLoad.Get_Fields_String("distm"));
						rsMSRSTempDetail.Set_Fields("statuscode", clsLoad.Get_Fields_String("statuscode") == "__" ? "" : clsLoad.Get_Fields_String("statuscode"));
						strTemp = FCConvert.ToString(clsMiscInfo.Get_Fields_String("Lastname"));
						strTemp = modCoreysSweeterCode.escapequote(ref strTemp);
						rsMSRSTempDetail.Set_Fields("lastname", strTemp);
						strTemp = FCConvert.ToString(clsMiscInfo.Get_Fields_String("firstname"));
						strTemp = modCoreysSweeterCode.escapequote(ref strTemp);
						rsMSRSTempDetail.Set_Fields("firstname", strTemp);
						rsMSRSTempDetail.Set_Fields("desig", clsMiscInfo.Get_Fields_String("desig"));
						strTemp = FCConvert.ToString(clsMiscInfo.Get_Fields("middlename"));
						strTemp = modCoreysSweeterCode.escapequote(ref strTemp);
						rsMSRSTempDetail.Set_Fields("middlename", strTemp);
						rsMSRSTempDetail.Set_Fields("socialsecuritynumber", clsMiscInfo.Get_Fields_String("ssn"));
						rsMSRSTempDetail.Set_Fields("DateOfBirth", Information.IsDate(clsMiscInfo.Get_Fields("datebirth")) ? clsMiscInfo.Get_Fields("datebirth") : 0);
						rsMSRSTempDetail.Set_Fields("lifeinsurancelevel", Conversion.Val(clsMiscInfo.Get_Fields_String("LifeInsLevel")));
						rsMSRSTempDetail.Set_Fields("PlanCode", clsMiscInfo.Get_Fields_String("PlanCode"));
						rsMSRSTempDetail.Set_Fields("LifeInsuranceEPCode", clsMiscInfo.Get_Fields_String("Excess"));
						rsMSRSTempDetail.Set_Fields("PayPeriodsCode", clsMiscInfo.Get_Fields_String("PayPeriodsCode"));

						if (clsTemp2.FindFirstRecord2("employeenumber,statuscode,distm", clsLoad.Get_Fields_String("employeenumber") + "," + clsLoad.Get_Fields_String("statuscode") + "," + clsLoad.Get_Fields("distm"), ","))
						{
							if (Conversion.Val(clsTemp2.Get_Fields_Int32("MSRSID")) > 0)
							{
								clsTemp.OpenRecordset("select * from tblmsrsdistributiondetail where ID = " + clsTemp2.Get_Fields_Int32("msrsid"), "twpy0000.vb1");
							}
							else
							{
								clsTemp.OpenRecordset("select * from tblmsrsdistributiondetail where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "' and booldefault = 1", "twpy0000.vb1");
							}

							if (!clsTemp.EndOfFile())
							{
								rsMSRSTempDetail.Set_Fields("workweeksperyear", clsTemp.Get_Fields_Int32("workweeksperyear"));
							}
							else
							{
								if (Conversion.Val(clsTemp2.Get_Fields("workweeksperyear")) > 0)
								{
									rsMSRSTempDetail.Set_Fields("workweeksperyear", clsTemp2.Get_Fields_Int32("workweeksperyear"));
								}
								else
								{
									rsMSRSTempDetail.Set_Fields("workweeksperyear", 0);
								}
							}

							if (!clsTemp.EndOfFile())
							{
								rsMSRSTempDetail.Set_Fields("HOURLYDAILYCONTRACT", Conversion.Val(clsTemp.Get_Fields("hourlydailycontract")));
							}
							else
							{
								rsMSRSTempDetail.Set_Fields("HOURLYDAILYCONTRACT", Conversion.Val(clsTemp2.Get_Fields("hourlydailycontract")));
							}
							rsMSRSTempDetail.Set_Fields("FedCompCode", Conversion.Val(clsTemp2.Get_Fields("FedCompDollarPercent")));
							rsMSRSTempDetail.Set_Fields("FederalCompAmount", Conversion.Val(clsTemp2.Get_Fields_Double("FedCompAmount")));
						}
						else
						{
							rsMSRSTempDetail.Set_Fields("workweeksperyear", 0);
							rsMSRSTempDetail.Set_Fields("HOURLYDAILYCONTRACT", 0);
							rsMSRSTempDetail.Set_Fields("FedCompCode", "");
							rsMSRSTempDetail.Set_Fields("FederalCompAmount", 0);
						}

						rsMSRSTempDetail.Set_Fields("LifeInsuranceCode", clsMiscInfo.Get_Fields_String("LifeInsCode"));
						rsMSRSTempDetail.Set_Fields("PayFactor", Conversion.Val(clsMiscInfo.Get_Fields("PayFactor")));
						rsMSRSTempDetail.Set_Fields("MonthlyFactor", Conversion.Val(clsMiscInfo.Get_Fields("MonthlyFactor")));
						strTemp = fecherFoundation.Strings.Trim(Strings.Left(clsMiscInfo.Get_Fields_String("RateOfPay") + " ", 1));
						rsMSRSTempDetail.Set_Fields("PayOperator", strTemp);
						strTemp = fecherFoundation.Strings.Trim(Strings.Left(clsMiscInfo.Get_Fields_String("MonthlyHD") + " ", 1));
						rsMSRSTempDetail.Set_Fields("MonthlyOperator", strTemp);
						rsMSRSTempDetail.Set_Fields("hoursinaday", Conversion.Val(clsMiscInfo.Get_Fields("hrsday")));
						if (!clsTemp2.EndOfFile())
						{
							rsMSRSTempDetail.Set_Fields("fulltimeequivalent", Conversion.Val(clsTemp2.Get_Fields("fulltimeequivalent")));
						}
						else
						{
							rsMSRSTempDetail.Set_Fields("fulltimeequivalent", 0);
						}
						if (clsTemp3.FindFirstRecord("employeenumber", clsLoad.Get_Fields_String("employeenumber")))
						{
							tempHours = clsTemp3.Get_Fields_Double("monthlyhours");
							if (clsTemp3.Get_Fields("statuscode") != clsLoad.Get_Fields_String("statuscode") || clsTemp3.Get_Fields_String("distm") != clsLoad.Get_Fields_String("distm"))
							{
								if (clsTemp3.FindNextRecord3_1("employeenumber,statuscode,distm", clsLoad.Get_Fields_String("employeenumber") + "," + clsLoad.Get_Fields_String("statuscode") + "," + clsLoad.Get_Fields_String("distm"), ","))
								{
									tempHours = Conversion.Val(clsTemp3.Get_Fields("monthlyhours"));
								}
							}
							if (!clsTemp4.FindFirstRecord2("employeenumber,statuscode,distm", clsLoad.Get_Fields_String("employeenumber") + "," + clsLoad.Get_Fields_String("statuscode") + "," + clsLoad.Get_Fields_String("distm"), ","))
							{
								rsMSRSTempDetail.Set_Fields("monthlyhoursdays", tempHours);
							}
							else
							{
								rsMSRSTempDetail.Set_Fields("monthlyhoursdays", tempHours + Conversion.Val(clsTemp4.Get_Fields("adjusthours")));
							}
						}
						else
						{
							rsMSRSTempDetail.Set_Fields("monthlyhoursdays", Conversion.Val(clsLoad.Get_Fields_Int16("MonthlyHours")));
						}
						rsMSRSTempDetail.Set_Fields("WORKWEEKHOURS", clsMiscInfo.Get_Fields_Double("hrswk"));
						rsMSRSTempDetail.Set_Fields("MSRSJOBCATEGORY", clsMiscInfo.Get_Fields_Int32("MSRSJOBCATEGORY"));
						if (Information.IsDate(clsMiscInfo.Get_Fields("datehire")))
						{
							rsMSRSTempDetail.Set_Fields("DATEOFHIRE", clsMiscInfo.Get_Fields("datehire"));
						}
						else
						{
							rsMSRSTempDetail.Set_Fields("DATEOFHIRE", 0);
						}
						rsMSRSTempDetail.Update();
                    }
				}
				clsLoad.MoveNext();
			}
			// save the default position code incase we have to use it
			strSQL = "select * from tblmsrsdistributiondetail where boolDefault = 1 and reporTtype = " + lngReportType;
			clsLoad.OpenRecordset(strSQL, "Payroll");
			while (!clsLoad.EndOfFile())
			{
				clsTemp.Execute("update tblmsrstempdetailreport set positionid = '" + clsLoad.Get_Fields_String("position") + "' where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
				clsLoad.MoveNext();
			}
			// Now get values
			// payback deductions
			strSQL = "select * from tblDefaultDeductions where type = 'P'";
			clsLoad.OpenRecordset(strSQL, "Payroll");
			if (!clsLoad.EndOfFile())
			{
				if (clsLoad.RecordCount() == 1)
				{
					PaybackCode = clsLoad.Get_Fields_Int32("DeductionID");
				}
				else
				{
					// indicate that there are codes but more than one
					PaybackCode = 0;
				}
			}
			else
			{
				// indicate no codes
				PaybackCode = -1;
			}
			switch (PaybackCode)
			{
				case -1:
					{
						strTemp = " where deddeductionnumber = -10 ";
						// should come up with nothing
						break;
					}
				case 0:
					{
						strTemp = " where (deddeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid");
						clsLoad.MoveNext();
						while (!clsLoad.EndOfFile())
						{
							strTemp += " or deddeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid");
							clsLoad.MoveNext();
						}
						strTemp += ")";
						break;
					}
				default:
					{
						strTemp = " where deddeductionnumber = " + PaybackCode + " ";
						break;
					}
			}
			//end switch
			strSQL = "Select sum(dedamount) as PaybackAmount ,employeenumber from tblcheckdetail " + strTemp + " and " + strWhere + " and checkvoid = 0 group by employeenumber";
			clsLoad.OpenRecordset(strSQL, "Payroll");
			while (!clsLoad.EndOfFile())
			{
				clsTemp.Execute("update tblmsrstempdetailreport set paybackamount = " + clsLoad.Get_Fields_Double("Paybackamount") + " where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
				clsLoad.MoveNext();
			}
			// employeecontributions
			strSQL = "select * from tblDefaultDeductions where type = 'R'";
			clsLoad.OpenRecordset(strSQL, "Payroll");
			if (!clsLoad.EndOfFile())
			{
				if (clsLoad.RecordCount() == 1)
				{
					RetirementCode = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("deductionid"));
				}
				else
				{
					RetirementCode = 0;
				}
			}
			else
			{
				// indicate no codes
				RetirementCode = -1;
				RetirementAmount = 0;
			}
			switch (RetirementCode)
			{
				case -1:
					{
						strTemp = " where deddeductionnumber = - 10 ";
						break;
					}
				case 0:
					{
						strTemp = " where (deddeductionnumber = " + clsLoad.Get_Fields("deductionid");
						clsLoad.MoveNext();
						while (!clsLoad.EndOfFile())
						{
							strTemp += " or deddeductionnumber = " + clsLoad.Get_Fields("deductionid");
							clsLoad.MoveNext();
						}
						strTemp += ")";
						break;
					}
				default:
					{
						strTemp = " where deddeductionnumber = " + RetirementCode + " ";
						break;
					}
			}
			//end switch
			strSQLDeductionCodes = strTemp;
			strSQL = "select sum(dedamount) as EmployeeContribution,employeenumber from tblcheckdetail " + strTemp + " and " + strWhere + " and checkvoid = 0 group by employeenumber ";
			clsLoad.OpenRecordset(strSQL, "Payroll");
			while (!clsLoad.EndOfFile())
			{
                clsTemp.Execute("update tblmsrstempdetailreport set employeecontributions = " + clsLoad.Get_Fields_Double("employeecontribution") + " where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
				clsLoad.MoveNext();
			}
			
			strSQL = "Select * from tblDefaultdeductions where type = 'L'";
			clsLoad.OpenRecordset(strSQL, "Payroll");
			if (!clsLoad.EndOfFile())
			{
				if (clsLoad.RecordCount() == 1)
				{
					LifeInsuranceCode = clsLoad.Get_Fields_Int32("deductionid");
				}
				else
				{
					LifeInsuranceCode = 0;
				}
			}
			else
			{
				LifeInsuranceCode = -1;
			}
			switch (LifeInsuranceCode)
			{
				case -1:
					{
						strSQL = "select employeenumber,sum(dedamount) as LifeInsPremium from tblcheckdetail  where checkvoid = 0 AND employeenumber = '-1' group by employeenumber ";
                        break;
					}
				case 0:
					{
						strTemp = " where checkvoid = 0 and (deddeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid") + " or matchdeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid");
						clsLoad.MoveNext();
						while (!clsLoad.EndOfFile())
						{
							strTemp += " or deddeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid");
							strTemp += " or matchdeductionnumber = " + clsLoad.Get_Fields_Int32("deductionid");
							clsLoad.MoveNext();
						}
						strTemp += ")";
						strSQL = "select employeenumber,sum(convert(float, isnull(dedamount, 0)) + convert(float, isnull(MATCHAMOUNT, 0))) as LifeInsPremium from tblcheckdetail " + strTemp + " and " + strWhere + " and checkvoid = 0 group by employeenumber ";
                        break;
					}
				default:
					{
						strTemp = " where checkvoid = 0 and (deddeductionnumber = " + LifeInsuranceCode + " OR matchdeductionnumber = " + LifeInsuranceCode + ")";
						strSQL = "select employeenumber,sum(convert(float, isnull(dedamount, 0)) + convert(float, isnull(MATCHAMOUNT, 0))) as LifeInsPremium from tblcheckdetail " + strTemp + " and " + strWhere + " group by employeenumber ";
                        break;
					}
			}
			//end switch
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				clsTemp.Execute("update tblmsrstempdetailreport set lifeinsurancepremium = " + clsLoad.Get_Fields_Double("lifeinspremium") + " where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
				clsLoad.MoveNext();
			}
			// get payrate
			strSQL = "select  employeenumber,MSRS as JobID,msrsid,baserate as Payrate  from tblPayrollDistribution  group by employeenumber,msrs,baserate,msrsid,RECORDNUMBER order by recordnumber";
			clsPayrate.OpenRecordset(strSQL, "Payroll");
			
			txtActiveX.Visible = true;
			txtRetiredX.Visible = false;
			txtDate.Text = "RUN DATE " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "PAGE NO. 1";
			intPageNumber = 1;
			// now get the nonpaid active stuff
			// not sure if teachers are in nonpaid if so, this if statement has to go and
			// have to have a new field in the nonpaid table to indicate teachers
			// If Not boolTeachers Then
			// can't do a range of sequence on these since they have no sequence numbers
			// If Not boolRange Then
			if (strRange != string.Empty)
			{
				strRange = " and " + strRange;
			}
			strWhere = " where status = 'A' and include = 1 and reporttype = " + lngReportType + " " + strTeacher2 + strTextRange;
			strSubQuery = "select Lastname,FirstName,SocialSecurityNumber,InsuranceContributions,GrossMTD,RetirementContributions,'H' as payratecode,PayRate,DateofHire,DateOfBirth";
            strSubQuery += ",StatusCode,PositionCode,PositionCode as PositionID,PlanCode,LifeInsuranceScheduleEP,LifeInsuranceSchedulebz AS payperiodscode,LifeInsuranceLevel,PaybackAmount";
			strSubQuery += ",WorkWeeksPerYear,GrossCalendar,FederalCompAmount,'D' as FedCompCode,LifeInsuranceCode";
			strSubQueryRetired = strSubQuery;
			strSubQuery += ",recordnumber,0 as RetiredFlag,MonthlyHours,MSRSJobCategory,schedule,participation from tblmsrsnonpaid ";
			strSubQueryRetired += ",recordnumber,1 as RetiredFlag,MonthlyHours,MSRSJobCategory,schedule,participation from tblmsrsnonpaid ";
			strSQL = " Insert into tblMSRSTempDetailReport (";
			strSQL += "LastName,FirstName,SocialSecurityNumber,LifeInsurancePremium,EarnableCompensation";
			strSQL += ",EmployeeContributions,payratecode,PayRate,DateOfHire,DateOfBirth";
			strSQL += ",StatusCode,PositionCode,PositionID,PlanCode,LifeInsuranceEPCode,payperiodscode,LifeInsuranceLevel,PaybackAmount";
			strSQL += ",WorkWeeksPerYear,YTDEarnings,FederalCompAmount,FedCompCode,LifeInsuranceCode";
			strSQL += ",EmployeeNumber,Retired,MonthlyHoursDays,MSRSJobCategory,schedule,participation";
			strSQL += ") ";
			clsLoad.Execute(strSQL + strSubQuery + strWhere, "Payroll");
			// now do retirees
			strWhere = " where status = 'R' and include = 1 and reporttype = " + lngReportType + " " + strTeacher2 + strTextRange;
			clsLoad.Execute(strSQL + strSubQueryRetired + strWhere, "Payroll");
			// End If
			// a little pre-processing.  Figure the total Earned comp for the employee (not broken down by position code)
			// this is easier than adding another inner join statement so that it is inserted above
			clsLoad.OpenRecordset("select sum(earnablecompensation) as TotComp,employeenumber from tblmsrstempdetailreport group by employeenumber", "Payroll");
			while (!clsLoad.EndOfFile())
            {
                clsTemp.Execute("update tblmsrstempdetailreport set TotalEarnedCompensation = " + clsLoad.Get_Fields_Double("totcomp") + " where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
				clsLoad.MoveNext();
			}
            clsLoad.OpenRecordset("select employeenumber from tblmsrstempdetailreport  group by employeenumber", "Payroll");
			double dblTotEComp = 0;
			double dblContLeft = 0;
            double dblContRate = 0;
            double dblCalcCont = 0;
            double dblAllEarnedComp = 0;
			double dblCompRatio = 0;
            // now try and break out the contributions by positioncode.  Figure out the rate that they paid
			// then multiply that rate by each earnable compensation by position code.
			clsTemp5.OpenRecordset("select sum(distgrosspay) as totearnable, tblcheckdetail.employeenumber from tblemployeemaster inner join tblcheckdetail on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where checkvoid = 0 and " + strWhere2 + "  and distm <> 'N' and reporttype > " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE) + " group by tblcheckdetail.employeenumber", "Payroll");
			while (!clsLoad.EndOfFile())
			{
                clsTemp.OpenRecordset("select * from tblmsrstempdetailreport where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "'", "Payroll");
                if (clsTemp5.FindFirstRecord("employeenumber", clsLoad.Get_Fields_String("employeenumber")))
				{
					dblAllEarnedComp = clsTemp5.Get_Fields_Double("totearnable").RoundToMoney();
				}
				else
				{
					dblAllEarnedComp = 0;
				}
				dblTotEComp = 0;
				dblContLeft = 0;
				dblContRate = 0;
				if (!clsTemp.EndOfFile())
				{
					dblTotEComp = clsTemp.Get_Fields_Double("TotalEarnedCompensation");
					dblContLeft = clsTemp.Get_Fields_Double("EmployeeContributions");
                    if (dblTotEComp <= 0)
					{
						dblContRate = 0;
					}
					else
					{
						dblContRate = FCConvert.ToDouble(Strings.Format(dblContLeft / dblTotEComp, "0.0000"));
					}
				}
				if (dblAllEarnedComp > dblTotEComp)
				{
					while (!clsTemp.EndOfFile())
					{
                        // mixed breed of things that would put some on one report and some on another report
						// go through check by check
						dblContLeft = 0;
						dblCalcCont = 0;
						strSQLTemp = "select sum(distgrosspay)as EarnComp,paydate,payrunid,MASTERRECORD from tblcheckdetail where employeenumber = '" + clsTemp.Get_Fields("employeenumber") + "' and distm = '" + clsTemp.Get_Fields("positionid") + "' and (checkvoid = 0) and reporttype > " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE) + " and " + strWhere2 + " group by paydate,payrunid,masterrecord";
						clsTemp2.OpenRecordset(strSQLTemp, "Payroll");
						while (!clsTemp2.EndOfFile())
						{
                            strSQLTemp = "select sum(distgrosspay)as EarnComp from tblcheckdetail where employeenumber = '" + clsLoad.Get_Fields("employeenumber") + "' and paydate = '" + clsTemp2.Get_Fields("paydate") + "' and payrunid = " + clsTemp2.Get_Fields("payrunid") + " and masterrecord = '" + clsTemp2.Get_Fields("masterrecord") + "' and checkvoid = 0";
							clsTemp3.OpenRecordset(strSQLTemp, "Payroll");
							dblTotEComp = clsTemp2.Get_Fields_Double("earncomp");
							// total for that week for that position code
							dblAllEarnedComp = clsTemp3.Get_Fields_Double("earncomp");
							// get all msrs deductions for this check
							strSQLTemp = "select sum(dedamount) as ContSum from tblcheckdetail " + strSQLDeductionCodes + " and employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "' and paydate = '" + clsTemp2.Get_Fields("paydate") + "' and payrunid = " + clsTemp2.Get_Fields("payrunid") + " and masterrecord = '" + clsTemp2.Get_Fields("masterrecord") + "' and checkvoid = 0";
							clsTemp3.OpenRecordset(strSQLTemp, "Payroll");
							if (dblTotEComp == dblAllEarnedComp)
							{
								// this is the only job for this week
								// all of the contributions for this check can go to this job code
								if (!clsTemp.EndOfFile())
								{
									// add this to the months total
									dblContLeft += clsTemp3.Get_Fields_Double("contsum");
								}
							}
							else
							{
								// this isn't the only job for this week
								// have to calculate what goes toward this position
								if (!clsTemp.EndOfFile())
								{
									dblCalcCont = clsTemp3.Get_Fields_Double("contsum");
									dblCompRatio = 0;
									if (dblAllEarnedComp > 0)
									{
										dblCompRatio = dblTotEComp / dblAllEarnedComp;
									}

                                    dblCalcCont = (dblCalcCont * dblCompRatio).RoundToMoney();
									dblContLeft += dblCalcCont;
								}
							}
							clsTemp2.MoveNext();
						}
						clsTemp.Edit();
						clsTemp.Set_Fields("employeecontributions", dblContLeft.RoundToMoney());
						clsTemp.Update();
						clsTemp.MoveNext();
					}
				}
				else
				{
					while (!clsTemp.EndOfFile())
					{
						if (dblTotEComp > 0)
						{
							dblTotEComp -= clsTemp.Get_Fields_Double("earnablecompensation");
						}
						if (dblTotEComp <= 0)
						{
							clsTemp.Edit();
							clsTemp.Set_Fields("Employeecontributions", dblContLeft);
							clsTemp.Update();
							dblContLeft = 0;
						}
						else
						{
							if (clsTemp.Get_Fields_Double("earnablecompensation") > 0)
							{
								dblCalcCont =  (clsTemp.Get_Fields_Double("earnablecompensation") * dblContRate).RoundToMoney();
								if (dblCalcCont > dblContLeft)
									dblCalcCont = dblContLeft;
								dblContLeft -= dblCalcCont;
								clsTemp.Edit();
								clsTemp.Set_Fields("employeecontributions", dblCalcCont);
								clsTemp.Update();
							}
							else
							{
								clsTemp.Edit();
								clsTemp.Set_Fields("employeecontributions", 0);
								clsTemp.Update();
							}
						}
						clsTemp.MoveNext();
					}
				}
				clsLoad.MoveNext();
			}
			// 11/01/2005
			clsLoad.OpenRecordset("select employeenumber,status,position,schedule,participation,fulltimeequivalent from TBLmsrsdistributiondetail group by employeenumber,status,position,schedule,participation,fulltimeequivalent", "Payroll");
			while (!clsLoad.EndOfFile())
			{
				clsTemp.Execute("update tblmsrstempdetailreport set fulltimeequivalent = " + clsLoad.Get_Fields_Double("fulltimeequivalent") + " ,participation = '" + clsLoad.Get_Fields_String("participation") + "',schedule = '" + clsLoad.Get_Fields_String("schedule") + "' where employeenumber = '" + clsLoad.Get_Fields_String("employeenumber") + "' and statuscode = '" + clsLoad.Get_Fields_String("status") + "' and positioncode = '" + clsLoad.Get_Fields_String("position") + "'", "Payroll");
				clsLoad.MoveNext();
			}
			
			clsLoad.Execute("delete from tblmsrstempdetailreport where isnull(statuscode, '') = '65' or (isnull(statuscode, '') = '' and defaultstatuscode = '65')", "Payroll");
			clsLoad.OpenRecordset("select  * from tblMSRSTempDetailReport order by retired desc,lastname,firstname,middlename,PositionCode", "Payroll");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				boolNoData = true;
				if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					this.Close();
				}
				return;
			}
			dblTotalLifeInsPremium = 0;
			dblTotalEarnCompensation = 0;
			dblTotalEmployeeContributions = 0;
			clsTemp.Dispose();
			clsTemp2.Dispose();
			clsTemp3.Dispose();
			clsTemp4.Dispose();
			clsTemp5.Dispose();
			clsSequenceCheck.Dispose();
			rsMSRSTempDetail.Dispose();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("MSRS");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			double dblFedCompensation;
			double dblHrsAday = 0;
			double dblPayrate = 0;
			double dblWorkWeekHoursDays = 0;
			double dblCurLifeIns = 0;
            int intTemp = 0;
			if (boolNoData)
				return;
            if (clsLoad.Get_Fields_Boolean("retired") == true)
			{
                boolRetiredSection = true;
            }
			modCoreysSweeterCode.Statics.MSRSDetRec.RetirementPlan = clsLoad.Get_Fields_String("participation");
			modCoreysSweeterCode.Statics.MSRSDetRec.RateSchedule = clsLoad.Get_Fields_String("schedule").ToIntegerValue();
			if (clsLoad.Get_Fields_String("SocialSecurityNumber").Trim().Length > 4)
			{
				if (Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber").Trim(), 4, 1) == "-")
				{
					txtSocialSecurityNo.Text = clsLoad.Get_Fields_String("SocialSecurityNumber").Trim();
					strTemp = txtSocialSecurityNo.Text;
				}
				else
				{
					strTemp = Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 1, 3);
					strTemp += "-" + Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 4, 2);
					strTemp += "-" + Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 6);
					txtSocialSecurityNo.Text = strTemp;
				}
			}
			else
			{
				strTemp = Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 1, 3);
				strTemp += "-" + Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 4, 2);
				strTemp += "-" + Strings.Mid(clsLoad.Get_Fields_String("SocialSecurityNumber"), 6);
				txtSocialSecurityNo.Text = strTemp;
			}
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			modCoreysSweeterCode.Statics.MSRSDetRec.SSN = strTemp;
            txtDateOfHire.Text = "";
            if (modGlobalConstants.Statics.MuniName.Trim().ToUpper() == "PRESQUE ISLE")
			{
				if (Information.IsDate(clsLoad.Get_Fields("dateofhire")))
				{
					if (Convert.ToDateTime(clsLoad.Get_Fields("dateofhire")).ToOADate() != 0)
					{
						txtDateOfHire.Text = Strings.Format(clsLoad.Get_Fields("dateofhire"), "MM/dd/yyyy");
					}
				}
			}

			if (clsLoad.Get_Fields_String("Desig").Trim().ToUpper() == string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("lastname") + ", " + clsLoad.Get_Fields_String("firstname") + " " + clsLoad.Get_Fields_String("middlename")) + Strings.StrDup(30, " ");
			}
			else
			{
				strTemp = fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("lastname") + " " + fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("Desig")) + ", " + clsLoad.Get_Fields_String("firstname") + " " + clsLoad.Get_Fields_String("middlename")) + Strings.StrDup(30, " ");
			}
			modCoreysSweeterCode.Statics.MSRSDetRec.Name = Strings.Mid(strTemp, 1, 30);
			strTemp = Strings.Mid(strTemp, 1, 28);
			txtFirstLastMiddle.Text = strTemp;
			if (Information.IsDate(clsLoad.Get_Fields("dateofbirth")))
			{
				if (Convert.ToDateTime(clsLoad.Get_Fields("dateofbirth")).ToOADate() != 0)
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields("dateofbirth"));
					txtDateOfBirth.Text = Strings.Format(strTemp, "MM/dd/yy");
					strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					strTemp = Strings.Replace(strTemp, "/", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Right("00000000" + strTemp, 8);
				}
				else
				{
					txtDateOfBirth.Text = string.Empty;
					strTemp = "00000000";
				}
			}
			else
			{
				txtDateOfBirth.Text = "";
				strTemp = "00000000";
			}
			modCoreysSweeterCode.Statics.MSRSDetRec.DateOfBirth = strTemp;
			intTemp = clsLoad.Get_Fields_Int32("workweeksperyear");
			if (intTemp == 0)
			{
			}
			txtWorkWeeksPerYear.Text = clsLoad.Get_Fields_Int32("workweeksperyear").ToString();
			modCoreysSweeterCode.Statics.MSRSDetRec.WorkWeeksPerYear =clsLoad.Get_Fields_Int32("workweeksperyear");
            strTemp = "--";
			if (clsLoad.Get_Fields_String("plancode").Length == 2 && clsLoad.Get_Fields_String("plancode") != "--")
			{
				if (clsLoad.Get_Fields_Int32("msrsjobcategory") == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHER)
				{
					strTemp = "11001";
				}
				else if (clsLoad.Get_Fields_Int32("msrsjobcategory") == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYTEACHERBEFORE93)
				{
					strTemp = "11000";
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("plancode"))) == "AC" || fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("plancode"))) == "AN" || fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("Plancode"))) == "BC")
				{
					strTemp = "110" + fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("Plancode")));
				}
				else
				{
					if (Conversion.Val(clsLoad.Get_Fields("MSRSJOBCATEGORY")) == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYFIREFIGHTERLAWGOVERNMENT)
					{
						strTemp = "310" + clsLoad.Get_Fields("PLANCODE");
					}
					else if (Conversion.Val(clsLoad.Get_Fields("MSRSJOBCATEGORY")) == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYFIREFIGHTER)
					{
						strTemp = "310" + clsLoad.Get_Fields("plancode");
					}
					else if (Conversion.Val(clsLoad.Get_Fields("MSRSJOBCATEGORY")) == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYLAWENFORCEMENT)
					{
						// strTemp = "620" & .Fields("plancode")
						strTemp = "310" + clsLoad.Get_Fields("plancode");
					}
					else if (Conversion.Val(clsLoad.Get_Fields("MSRSJOBCATEGORY")) == modCoreysSweeterCode.CNSTMSRSPLANCATEGORYGENERALGOVERNMENT)
					{
						// strTemp = "730" & .Fields("plancode")
						strTemp = "310" + clsLoad.Get_Fields("plancode");
					}
					else
					{
						strTemp = "310" + clsLoad.Get_Fields("plancode");
					}
				}
			}
			else
			{
				strTemp = clsLoad.Get_Fields_String("plancode");
			}
			if (strTemp == string.Empty)
				strTemp = "--";
			txtPlanCode.Text = strTemp;
            modCoreysSweeterCode.Statics.MSRSDetRec.BenefitPlanCode = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
            modCoreysSweeterCode.Statics.MSRSDetRec.PositionCode = "";
			if (clsLoad.Get_Fields_String("positioncode") == string.Empty || clsLoad.Get_Fields_String("positioncode") == "Y")
			{
				if (clsLoad.Get_Fields_String("statuscode") != string.Empty)
				{
					txtEmployeeCodes.Text = clsLoad.Get_Fields_String("statuscode") + "-";
					modCoreysSweeterCode.Statics.MSRSDetRec.StatusCode = clsLoad.Get_Fields_String("statuscode");
				}
				else
				{
					txtEmployeeCodes.Text = clsLoad.Get_Fields_String("DefaultStatusCode") + "-";
					modCoreysSweeterCode.Statics.MSRSDetRec.StatusCode = clsLoad.Get_Fields_String("defaultstatuscode");
				}
				if (!clsMiscInfo.EndOfFile())
				{
					if (clsMiscInfo.FindFirstRecord("tblmiscupdate.employeenumber", clsLoad.Get_Fields_String("employeenumber")))
					{
						txtEmployeeCodes.Text = txtEmployeeCodes.Text + clsMiscInfo.Get_Fields_String("positioncode");
						modCoreysSweeterCode.Statics.MSRSDetRec.PositionCode =clsMiscInfo.Get_Fields_String("positioncode");
					}
				}
			}
			else
			{
				txtEmployeeCodes.Text = clsLoad.Get_Fields_String("StatusCode") + "-" + clsLoad.Get_Fields_String("Positioncode");
				modCoreysSweeterCode.Statics.MSRSDetRec.StatusCode = clsLoad.Get_Fields_String("statuscode");
				modCoreysSweeterCode.Statics.MSRSDetRec.PositionCode = clsLoad.Get_Fields_String("positioncode");
			}
			
			txtPlanStatusRateSched.Text = modCoreysSweeterCode.Statics.MSRSDetRec.RetirementPlan + " " + Strings.Right("000000" + modCoreysSweeterCode.Statics.MSRSDetRec.RateSchedule.ToString(), 6);
			modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackStatus = Strings.Replace(clsLoad.Get_Fields_String("lifeinsuranceepcode"), "-", " ", 1, -1, CompareConstants.vbTextCompare);
			modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceSchedule = Strings.Replace(clsLoad.Get_Fields_String("payperiodscode"), "-", " ", 1, -1, CompareConstants.vbTextCompare);
			txtRemarkCodes.Text = modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackStatus + modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceSchedule;
            txtEarnableCompensation.Text = clsLoad.Get_Fields_Double("EarnableCompensation").FormatAsCurrencyNoSymbol();
			modCoreysSweeterCode.Statics.MSRSDetRec.EarnableCompensation = clsLoad.Get_Fields_Double("earnablecompensation");
			if (Conversion.Val(clsLoad.Get_Fields("statuscode")) != 53)
			{
                if (Conversion.Val(this.Fields["fldearnedcompensation"].Value) == 0)
				{
					txtEarnableCompensation.Text = "0.00";
					modCoreysSweeterCode.Statics.MSRSDetRec.EarnableCompensation = 0;
				}
				else
				{
					dblTotalEarnCompensation += clsLoad.Get_Fields_Double("EarnableCompensation");
				}
			}
			else
			{
				if (Conversion.Val(this.Fields["fldemployeecontributions"].Value) == 0)
				{
				}
				else
				{
					dblTotalEarnCompensation += clsLoad.Get_Fields_Double("EarnableCompensation");
				}
			}
            if (clsLoad.Get_Fields_Double("fulltimeequivalent") > 0)
			{
				modCoreysSweeterCode.Statics.MSRSDetRec.FullTimeEquivalent = clsLoad.Get_Fields_Double("fulltimeequivalent");
				txtFullTimeEquivalent.Text = clsLoad.Get_Fields_Double("FulltimeEquivalent").FormatAsCurrencyNoSymbol();
			}
			else
			{
				txtFullTimeEquivalent.Text = "";
				modCoreysSweeterCode.Statics.MSRSDetRec.FullTimeEquivalent = 0;
			}
			if (clsLoad.Get_Fields_Double("PaybackAmount") > 0)
			{
				txtPaybackOfExcessContributions.Text = clsLoad.Get_Fields_Double("paybackamount").FormatAsCurrencyNoSymbol();
				modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackContributions = clsLoad.Get_Fields_Double("paybackamount");
			}
			else
			{
				txtPaybackOfExcessContributions.Text = "";
				modCoreysSweeterCode.Statics.MSRSDetRec.ExcessPaybackContributions = 0;
			}
			if (clsLoad.Get_Fields_Double("EmployeeContributions") > 0)
			{
				txtEmployeeContributions.Text = clsLoad.Get_Fields_Double("EmployeeContributions").FormatAsCurrencyNoSymbol();
				modCoreysSweeterCode.Statics.MSRSDetRec.EmployeeContributions = clsLoad.Get_Fields_Double("employeecontributions").RoundToMoney();
			}
			else
			{
				txtEmployeeContributions.Text = "";
				modCoreysSweeterCode.Statics.MSRSDetRec.EmployeeContributions = 0;
			}
            if (strCurrentEmployeeCode !=clsLoad.Get_Fields_String("EmployeeNumber"))
			{
				dblCurLifeIns = 0;
				if (clsLoad.Get_Fields_Double("lifeinsurancepremium") > 0)
				{
					txtLifeInsurancePremium.Text = clsLoad.Get_Fields_Double("LifeInsurancePremium").FormatAsCurrencyNoSymbol();
					dblCurLifeIns = clsLoad.Get_Fields_Double("lifeinsurancepremium");
                }
				else
				{
					dblCurLifeIns = 0;
					txtLifeInsurancePremium.Text = "";
				}
				modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsurancePremium = dblCurLifeIns;
				txtLifeInsuranceCode.Text = clsLoad.Get_Fields_String("lifeinsurancecode");
				txtLifeInsuranceLevel.Text = clsLoad.Get_Fields_Int32("LifeInsuranceLevel").ToString();
                modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceLevel = Conversion.Val(clsLoad.Get_Fields("lifeinsurancelevel"));
                dblTotalLifeInsPremium += dblCurLifeIns;
                if (dblCurLifeIns == 0)
				{
					txtLifeInsurancePremium.Text = "";
				}
				else
				{
					txtLifeInsurancePremium.Text = Strings.Format(dblCurLifeIns, "0.00");
				}
				this.Fields["fldLifeInsPremium"].Value = dblCurLifeIns;
			}
			else
			{
				txtLifeInsurancePremium.Text = "";
				if (txtLifeInsuranceCode.Text.Trim().ToUpper() != "I")
				{
					txtLifeInsuranceCode.Text = "OC";
					modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceCode = "OC";
				}
				txtLifeInsuranceLevel.Text = "";
                modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceLevel = 0;
				modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsurancePremium = 0;
			}
			modCoreysSweeterCode.Statics.MSRSDetRec.LifeInsuranceCode = Strings.Left(txtLifeInsuranceCode.Text + "   ", 3);
			dblTotalEmployeeContributions += clsLoad.Get_Fields_Double("employeecontributions");
            dblWorkWeekHoursDays = clsLoad.Get_Fields_Int32("workweekhours");
            dblHrsAday = Conversion.Val(clsLoad.Get_Fields("monthlyhoursdays"));
			if (FCConvert.ToString(clsLoad.Get_Fields("monthlyoperator")) == "D")
			{
				if (Conversion.Val(clsLoad.Get_Fields("monthlyfactor")) == 0)
				{
					dblHrsAday = 0;
				}
				else
				{
					dblHrsAday /= Conversion.Val(clsLoad.Get_Fields("monthlyfactor"));
				}
			}
			else
			{
				dblHrsAday *= Conversion.Val(clsLoad.Get_Fields("Monthlyfactor"));
			}
			MonthlyHoursorDays.Text = Strings.Format(dblHrsAday, "0.##");
			FullTimeWorkWeekHoursDays.Text = Strings.Format(dblWorkWeekHoursDays, "0.##");
			modCoreysSweeterCode.Statics.MSRSDetRec.MonthlyHoursDays = dblHrsAday;
			modCoreysSweeterCode.Statics.MSRSDetRec.FullTimeWorkWeekDaysHours = dblWorkWeekHoursDays;
            switch (clsLoad.Get_Fields_Int32("hourlydailycontract"))
			{
				case modCoreysSweeterCode.CNSTMSRSCONTRACT:
					{
						txtPayRateCode.Text = "C";
						// 09/22/2005
						MonthlyHoursorDays.Text = "";
						modCoreysSweeterCode.Statics.MSRSDetRec.MonthlyHoursDays = 0;
						break;
					}
				case modCoreysSweeterCode.CNSTMSRSDAILY:
					{
						txtPayRateCode.Text = "D";
						break;
					}
				case modCoreysSweeterCode.CNSTMSRSHOURLY:
					{
						txtPayRateCode.Text = "H";
						break;
					}
			}

			modCoreysSweeterCode.Statics.MSRSDetRec.PayRateCode = txtPayRateCode.Text;
            modCoreysSweeterCode.Statics.MSRSDetRec.PayRateCode = txtPayRateCode.Text;
            if (clsPayrate.FindFirstRecord2("JobID,employeenumber", clsLoad.Get_Fields_String("positioncode") + "," + clsLoad.Get_Fields_String("employeenumber"), ","))
			{
                dblPayrate = clsPayrate.Get_Fields_Double("Payrate");
			}
			else
			{
				// txtRateOfPay.Caption = ""
				if (clsPayrate.FindFirstRecord2("msrsid,employeenumber", modCoreysSweeterCode.CNSTDISTMDEFAULT + "," + clsLoad.Get_Fields_String("employeenumber"), ","))
				{
					dblPayrate = Conversion.Val(clsPayrate.Get_Fields_Double("payrate"));
				}
				else
				{
					dblPayrate = 0;
				}
			}
			// End If
			if (dblPayrate == 0 && Conversion.Val(clsLoad.Get_Fields("payrate")) > 0)
			{
				dblPayrate = Conversion.Val(clsLoad.Get_Fields("payrate"));
			}
			if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(txtPayRateCode.Text)) != "C")
			{
				if (FCConvert.ToString(clsLoad.Get_Fields("payoperator")) == "D")
				{
					dblPayrate /= Conversion.Val(clsLoad.Get_Fields("payfactor"));
				}
				else
				{
					dblPayrate *= Conversion.Val(clsLoad.Get_Fields("payfactor"));
				}
			}
			else
			{
				dblPayrate = 0;
			}
			if (dblPayrate > 0)
			{
				txtRateOfPay.Text = Strings.Format(dblPayrate, "0.0000");
			}
			else
			{
				txtRateOfPay.Text = "";
			}
			modCoreysSweeterCode.Statics.MSRSDetRec.PayRate = dblPayrate;
			strCurrentEmployeeCode =clsLoad.Get_Fields_String("EmployeeNumber");
			if (boolElectronicMSRS)
			{
				clsMSRSObject.SetDetailRecord();
			}
			clsLoad.MoveNext();
        }

		private void PageFooter_Format(object sender, EventArgs e)
		{
			if (boolEndofReport)
			{
				txtLifeInsGrandTotal.Text = dblTotalLifeInsPremium.FormatAsCurrencyNoSymbol();
				txtEarnedCompGrandTotal.Text = dblTotalEarnCompensation.FormatAsCurrencyNoSymbol();
				txtEmployeeContributionsGrandTotal.Text = dblTotalEmployeeContributions.FormatAsCurrencyNoSymbol();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "PAGE NO. " + intPageNumber;
			intPageNumber += 1;
			if (boolRetiredSection)
			{
				txtRetiredX.Visible = true;
				txtActiveX.Visible = false;
			}
		}

		public void Init(int lngTypeOfReport, bool boolUseElectronic, clsMSRS MSRSObject, List<string> batchReports = null)
		{
			lngReportType = lngTypeOfReport;
			boolElectronicMSRS = boolUseElectronic;
			if (boolElectronicMSRS)
			{
				clsMSRSObject = MSRSObject;
			}

			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modCoreysSweeterCode.CheckDefaultPrint(this, "", 1, boolAllowEmail: false, boolDontAllowCloseTilReportDone: true);
			}
			else
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
			}
		}

		
	}
}
