﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessSummary.
	/// </summary>
	public partial class rptPrintProcessSummary : BaseSectionReport
	{
		public rptPrintProcessSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Process Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPrintProcessSummary InstancePtr
		{
			get
			{
				return (rptPrintProcessSummary)Sys.GetInstance(typeof(rptPrintProcessSummary));
			}
		}

		protected rptPrintProcessSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintProcessSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		int intPageNumber;
		double dblNetPay;
		double dblGross;
		double dblFed;
		double dblState;
		double dblFica;
		double dblMed;
		double dblEMatch;
		double dblDistHours;
		double dblDeductions;
		int intTotalEmployees;
		bool boolNetPayZero;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			NextRecord:
			;
			if (intCounter < frmPrintPayrollProcess.InstancePtr.vsData.Rows)
			{
				// If .TextMatrix(intCounter, 0) = True Then
				// SHOW THE DATA
				if ((Conversion.Val(frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 10)) == 0 && boolNetPayZero) || !boolNetPayZero)
				{
					if (fecherFoundation.Strings.Trim(frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 1)).Length > 6)
					{
						txtEmployee.Text = Strings.Mid(fecherFoundation.Strings.Trim(frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 1)), 1, 4) + "+ " + frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 2);
					}
					else
					{
						txtEmployee.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 1) + Strings.StrDup(6 - fecherFoundation.Strings.Trim(frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 1)).Length, " ") + frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 2);
					}
                    
                    txtGross.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 3);
                    txtFederal.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 4);
                    txtFICA.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 5);
                    txtMedicare.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 6);
                    txtState.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 7);
                    txtDeductions.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 8);
                    txtNetPay.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 9);
                    txtMatch.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 10);
                    txtDistHours.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 11);
                    txtSickAccrued.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 12);
                    txtSickUsed.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 13);
                    txtVacAccrued.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 14);
                    txtVacUsed.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 15);
                    txtOtherAccrued.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 16);
                    txtOtherUsed.Text = frmPrintPayrollProcess.InstancePtr.vsData.TextMatrix(intCounter, 17);
                    txtNetPayTotal.Text = Strings.Format(Conversion.Val(txtNetPayTotal.Text) + Conversion.Val(txtNetPay.Text), "0.00");
					txtGrossTotal.Text = Strings.Format(Conversion.Val(txtGrossTotal.Text) + Conversion.Val(txtGross.Text), "0.00");
					txtFedTotal.Text = Strings.Format(Conversion.Val(txtFedTotal.Text) + Conversion.Val(txtFederal.Text), "0.00");
					txtStateTotal.Text = Strings.Format(Conversion.Val(txtStateTotal.Text) + Conversion.Val(txtState.Text), "0.00");
					txtFICATotal.Text = Strings.Format(Conversion.Val(txtFICATotal.Text) + Conversion.Val(txtFICA.Text), "0.00");
					txtMedTotal.Text = Strings.Format(Conversion.Val(txtMedTotal.Text) + Conversion.Val(txtMedicare.Text), "0.00");
					txtEMatchTotal.Text = Strings.Format(Conversion.Val(txtEMatchTotal.Text) + Conversion.Val(txtMatch.Text), "0.00");
					txtDistHoursTotal.Text = Strings.Format(Conversion.Val(txtDistHoursTotal.Text) + Conversion.Val(txtDistHours.Text), "0.00");
					txtDeductionTotal.Text = Strings.Format(Conversion.Val(txtDeductionTotal.Text) + Conversion.Val(txtDeductions.Text), "0.00");
					txtSickAccrTotal.Text = Strings.Format(Conversion.Val(txtSickAccrTotal.Text) + Conversion.Val(txtSickAccrued.Text), "0.0000");
					txtSickUsedTotal.Text = Strings.Format(Conversion.Val(txtSickUsedTotal.Text) + Conversion.Val(txtSickUsed.Text), "0.00");
					txtVacAccrTotal.Text = Strings.Format(Conversion.Val(txtVacAccrTotal.Text) + Conversion.Val(txtVacAccrued.Text), "0.0000");
					txtVacUsedTotal.Text = Strings.Format(Conversion.Val(txtVacUsedTotal.Text) + Conversion.Val(txtVacUsed.Text), "0.00");
					txtOtherAccrTotal.Text = Strings.Format(Conversion.Val(txtOtherAccrTotal.Text) + Conversion.Val(txtOtherAccrued.Text), "0.0000");
					txtOtherUsedTotal.Text = Strings.Format(Conversion.Val(txtOtherUsedTotal.Text) + Conversion.Val(txtOtherUsed.Text), "0.00");
					txtTotalTaxes.Text = Strings.Format(Conversion.Val(txtTotalTaxes.Text) + Conversion.Val(txtFederal.Text) + Conversion.Val(txtState.Text) + Conversion.Val(txtFICA.Text) + Conversion.Val(txtMedicare.Text), "0.00");
					intTotalEmployees += 1;
					txtTotalEmployees.Text = intTotalEmployees.ToString();
					intCounter += 1;
					eArgs.EOF = false;
				}
				else
				{
					// GOTO THE NEXT RECORD
					intCounter += 1;
					goto NextRecord;
				}
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intCounter = 2;
			intPageNumber = 0;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtPayDate.Text = "Pay Date: " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate);
		}

		public void Init(bool modalDialog, bool boolNetZero = false)
		{
			boolNetPayZero = boolNetZero;
			txtTitle.Text = "Pay Process Summary Report";
			if (boolNetPayZero)
			{
				txtTitle.Text = "Zero Net Pay Process Summary";
			}
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "PrintProcessSummary", showModal: modalDialog);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// frmProcessDisplay.Show , MDIParent
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

	
	}
}
