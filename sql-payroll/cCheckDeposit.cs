﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cCheckDeposit
	{
		//=========================================================
		private string strBankName = string.Empty;
		private double dblDepositAmount;
		private int lngBankNumber;

		public string Bank
		{
			set
			{
				strBankName = value;
			}
			get
			{
				string Bank = "";
				Bank = strBankName;
				return Bank;
			}
		}

		public double Amount
		{
			set
			{
				dblDepositAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblDepositAmount;
				return Amount;
			}
		}

		public int BankNumber
		{
			set
			{
				lngBankNumber = value;
			}
			get
			{
				int BankNumber = 0;
				BankNumber = lngBankNumber;
				return BankNumber;
			}
		}
	}
}
