//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewC1LaserFirst.
	/// </summary>
	public partial class srptNewC1LaserFirst : FCSectionReport
	{
		public srptNewC1LaserFirst()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptNewC1LaserFirst InstancePtr
		{
			get
			{
				return (srptNewC1LaserFirst)Sys.GetInstance(typeof(srptNewC1LaserFirst));
			}
		}

		protected srptNewC1LaserFirst _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptNewC1LaserFirst	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolPrintTest;

		private void GetTotalsFromRecords()
		{
			double dblTotStateWH = 0;
            double dblPayments = 0;

            double dblWHDue = 0;
            string strWhere = "";
            double dblTotalGross = 0;
            double dblTotalExcess = 0;
            double dblTaxWagePaid = 0;
            double dblRate = 0;
            double dblContributionsDue = 0;
            double dblCSSFRate = 0;
            double dblUCDue = 0;
            double dblCSSFAssessment = 0;
            if (!boolPrintTest)
			{
				// Field30.Text = ""
				// Field31.Text = ""
				// Field32.Text = ""
				txtLicense.Text = "";
				dblTotStateWH = modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalStateWithheld;
				txtLine4FirstMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthWorkers);
				txtLine4SecMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthWorkers);
				txtLine4ThirdMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthWorkers);
				txtLine5FirstMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthFemales);
				txtLine5SecMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthFemales);
				txtLine5ThirdMo.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthFemales);
				dblTotalGross = Conversion.Val(modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalGrossReportableWages);
				dblTotalExcess = Conversion.Val(modCoreysSweeterCode.Statics.EWRWageTotals.dblExcessWages);
				dblRate = Conversion.Val(modCoreysSweeterCode.Statics.EWRWageTotals.dblRate);
				dblCSSFRate = Conversion.Val(modCoreysSweeterCode.Statics.EWRWageTotals.dblCSSFRate);
				dblPayments = Conversion.Val(modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalPayments);
				dblWHDue = Conversion.Val(Strings.Format(dblTotStateWH - dblPayments, "0.00"));
				dblTaxWagePaid = Conversion.Val(Strings.Format(dblTotalGross - dblTotalExcess, "0.00"));
				dblUCDue = Conversion.Val(Strings.Format(dblRate * dblTaxWagePaid, "0.00"));
				dblCSSFAssessment = Conversion.Val(Strings.Format(dblCSSFRate * dblTaxWagePaid, "0.00"));
				dblContributionsDue = Conversion.Val(Strings.Format(dblUCDue + dblCSSFAssessment, "0.00"));
				txtLine1.Text = Strings.Format(dblTotStateWH, "0.00");
				// NO LONGER NEEDED BECAUSE OF CHECK BOX ON VOUCHER SCREEN.
				// ***********************************************************************************************
				// get the frequency of state tax
				// MATTHEW 8/3/2005 CALL ID 74280
				// Dim rsFreq As New clsDRWrapper
				// Call rsFreq.OpenRecordset("SELECT tblCategories.ID, tblRecipients.Freq FROM tblRecipients INNER JOIN tblCategories ON tblRecipients.RecptNumber = tblCategories.Include where tblCategories.ID = 4")
				// If Not rsFreq.EndOfFile Then
				// If rsFreq.Fields("Freq") = "Quarterly" Then
				// dblWHDue = dblPayments
				// dblPayments = 0
				// End If
				// End If
				// ***********************************************************************************************
				// 
				txtLine2.Text = Strings.Format(dblPayments, "0.00");
				txtLine3.Text = Strings.Format(dblWHDue, "0.00");
				txtLine6.Text = Strings.Format(dblTotalGross, "0.00");
				txtLine7.Text = Strings.Format(dblTotalExcess, "0.00");
				txtLine8.Text = Strings.Format(dblTaxWagePaid, "0.00");
				if (Strings.Right(Strings.Format(dblRate, ".00000"), 1) != "0")
				{
					txtLine9.Text = Strings.Format(dblRate, ".00000");
				}
				else
				{
					txtLine9.Text = Strings.Format(dblRate, ".0000");
				}
				if (Strings.Right(Strings.Format(dblCSSFRate, ".00000"), 1) != "0")
				{
					Field29.Text = Strings.Format(dblCSSFRate, ".00000");
				}
				else
				{
					Field29.Text = Strings.Format(dblCSSFRate, ".0000");
				}
				txtLine9b.Text = Strings.Format(dblUCDue, "0.00");
				txtLine9d.Text = Strings.Format(dblCSSFAssessment, "0.00");
				txtLine10.Text = Strings.Format(dblContributionsDue, "0.00");
				txtLine11.Text = Strings.Format(dblWHDue + dblContributionsDue, "0.00");
			}
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                    EWRRecord.TransmitterEmail = FCConvert.ToString(clsLoad.Get_Fields("transmitteremail"));
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                    EWRRecord.TransmitterEmail = "";
                }

                if (!boolPrintTest)
                {
                    txtSeasonalCode.Text = clsLoad.Get_Fields("seasonalcode");
                    if (Information.IsDate(clsLoad.Get_Fields("seasonalfrom")) &&
                        Information.IsDate(clsLoad.Get_Fields("seasonalto")))
                    {
                        txtSeasonStart.Text =
                            Strings.Format((DateTime) clsLoad.Get_Fields("seasonalfrom").Month, "00") + " " +
                            Strings.Format((DateTime) clsLoad.Get_Fields("seasonalfrom").Day, "00") + " " +
                            FCConvert.ToString((DateTime) clsLoad.Get_Fields("seasonalFrom").Year);
                        // Right(clsLoad.Fields("seasonalfrom"), 2)
                        txtSeasonEnd.Text = Strings.Format((DateTime) clsLoad.Get_Fields("seasonalto").Month, "00") +
                                            " " +
                                            Strings.Format((DateTime) clsLoad.Get_Fields("seasonalto").Day, "00") +
                                            " " + FCConvert.ToString((DateTime) clsLoad.Get_Fields("seasonalto").Year);
                        // Right(clsLoad.Fields("seasonalto"), 2)
                    }
                    else
                    {
                        txtSeasonStart.Text = "";
                        txtSeasonEnd.Text = "";
                    }
                }
                else
                {
                    txtState.Text = "XX";
                    txtPhone.Text = "";
                    txtDate.Text = "";
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intQuarterCovered = 0;
			string strTemp = "";
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, DateTime)
			DateTime dtTemp;
			string strSQL = "";
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                // vbPorter upgrade warning: dtStart As object	OnWrite(string)
                // vbPorter upgrade warning: dtEnd As DateTime	OnWrite(string)
                object dtStart = null;
                DateTime dtEnd = DateTime.FromOADate(0);
                boolPrintTest = modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest;
                LoadInfo();
                if (!boolPrintTest)
                {
                    txtEIN.Text = "";
                    GetTotalsFromRecords();
                    txtDate.Text = Strings.Format(DateTime.Today, "mm dd yyyy");
                    // txtFinal.Text = ""
                    // txtAddress1.Text = .EmployerAddress
                    // txtAddress2.Text = Trim(.EmployerCity & " " & .EmployerState & " " & .EmployerZip & " " & .EmployerZip4)
                    // txtEmail.Text = ""
                    txtEMail.Text = EWRRecord.TransmitterEmail;
                    // state wants it in all caps
                    txtName.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerName);
                    txtAddress.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerAddress);
                    txtCity.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerCity);
                    txtZip.Text = EWRRecord.EmployerZip;
                    txtWithholdingAccount.Text = Strings.Mid(EWRRecord.MRSWithholdingID, 1, 2) + " " +
                                                 Strings.Mid(EWRRecord.MRSWithholdingID, 3);
                    txtUCEmployerAccount.Text = EWRRecord.StateUCAccount;
                    strTemp = EWRRecord.TransmitterPhone;
                    strTemp = Strings.Mid(strTemp, 1, 3) + " " + Strings.Mid(strTemp, 4, 3) + " " +
                              Strings.Mid(strTemp, 7);
                    txtPhone.Text = strTemp;
                    txtTitle.Text = EWRRecord.TransmitterTitle;
                    Barcode1.Text =
                        Strings.Right(FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear), 2) +
                        "08500";
                    intQuarterCovered = modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter;
                    txtQuarterNum.Text = intQuarterCovered.ToString();
                    // txtDayStart.Text = "01"
                    switch (intQuarterCovered)
                    {
                        case 1:
                        {
                            txtPeriodStart.Text =
                                "01 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "03 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            dtTemp = FCConvert.ToDateTime(Strings.Format(
                                "05 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear),
                                "mm dd yyyy"));
                            dtTemp = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp);
                            dtStart = "01/01/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            dtEnd = FCConvert.ToDateTime(
                                "03/31/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear));
                            txtFileBefore.Text = "04 " + FCConvert.ToString(dtTemp.Day) + " " +
                                                 FCConvert.ToString(dtTemp.Year);
                            // Right(Year(dtTemp), 2)
                            break;
                        }
                        case 2:
                        {
                            txtPeriodStart.Text =
                                "04 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "06 30 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtFileBefore.Text =
                                "07 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            dtStart = "04/01/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            dtEnd = FCConvert.ToDateTime(
                                "06/30/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear));
                            break;
                        }
                        case 3:
                        {
                            txtPeriodStart.Text =
                                "07 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "09 30 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtFileBefore.Text =
                                "10 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            dtStart = "07/01/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            dtEnd = FCConvert.ToDateTime(
                                "09/30/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear));
                            break;
                        }
                        case 4:
                        {
                            txtPeriodStart.Text =
                                "10 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtPeriodEnd.Text =
                                "12 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // Right(EWRWageTotals.lngYear, 2)
                            txtFileBefore.Text =
                                "01 31 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear + 1);
                            // Right(EWRWageTotals.lngYear + 1, 2)
                            dtStart = "10/01/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            dtEnd = FCConvert.ToDateTime(
                                "12/31/" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear));
                            break;
                        }
                    }

                    //end switch
                    strSQL =
                        "select count(employeenumber) as thecount from (select employeenumber from tblcheckdetail where statetaxwh > 0 and paydate between '" +
                        dtStart + "' and '" + FCConvert.ToString(dtEnd) + "' group by employeenumber) as tbl1";
                    clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        txtNumberofPayees.Text = Conversion.Val(clsLoad.Get_Fields("thecount"));
                    }
                    else
                    {
                        txtNumberofPayees.Text = "0";
                    }
                }

                txtYear.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
            }
        }

		
	}
}
