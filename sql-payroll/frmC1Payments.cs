//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmC1Payments : BaseForm
	{
		public frmC1Payments()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmC1Payments InstancePtr
		{
			get
			{
				return (frmC1Payments)Sys.GetInstance(typeof(frmC1Payments));
			}
		}

		protected frmC1Payments _InstancePtr = null;
		//=========================================================
		// **********************************************************************************
		// **********************************************************************************
		int lngYear;
		int intLastMonth;
		double dblCredit;
		string strSequence = "";
		double dblLastCheck;
		// vbPorter upgrade warning: intGridRow As int	OnWriteFCConvert.ToInt32(
		int intGridRow;

		private void frmC1Payments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmC1Payments properties;
			//frmC1Payments.ScaleWidth	= 9225;
			//frmC1Payments.ScaleHeight	= 7680;
			//frmC1Payments.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			SetupGrid();
			// FillGrid
			LoadGrid();
        }

        private void frmC1Payments_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void lblLastCheck_CheckedChanged(object sender, System.EventArgs e)
		{
			if (lblLastCheck.CheckState == CheckState.Checked)
			{
				Grid.TextMatrix(intGridRow, 3, FCConvert.ToString(0));
			}
			else
			{
				Grid.TextMatrix(intGridRow, 3, FCConvert.ToString(dblLastCheck));
			}
		}

		public void lblLastCheck_Click()
		{
			lblLastCheck_CheckedChanged(lblLastCheck, new System.EventArgs());
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.Rows = 2;
				clsLoad.OpenRecordset("select * from tbl941PaymentSettings", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Conversion.Val(clsLoad.Get_Fields("ReportYear")) == lngYear && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strSequence)) == fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("Range")))) && FCConvert.ToInt32(clsLoad.Get_Fields("lastreportmonth")) == intLastMonth)
					{
						clsLoad.OpenRecordset("select * from tblstatepayments order by datepaid");
						while (!clsLoad.EndOfFile())
						{
							Grid.Rows += 1;
							lngRow = Grid.Rows - 1;
							if (Conversion.Val(clsLoad.Get_Fields("checknumber")) > 0)
							{
								Grid.TextMatrix(lngRow, 0, FCConvert.ToString(clsLoad.Get_Fields("checknumber")));
							}
							Grid.TextMatrix(lngRow, 1, Strings.Format(clsLoad.Get_Fields("datepaid"), "MM/dd/yyyy"));
							Grid.TextMatrix(lngRow, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("withheld"))), "#,###,##0.00"));
							Grid.TextMatrix(lngRow, 3, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("paid"))), "#,###,##0.00"));
							clsLoad.MoveNext();
						}
						dblLastCheck = Conversion.Val(Grid.TextMatrix(Grid.Rows - 1, 3));
						intGridRow = (Grid.Rows - 1);
						lblLastCheck.Text = "Last Payment Being Sent With This Report";
						if (Grid.Rows < 17)
							Grid.Rows = 17;
					}
					else
					{
						FillGrid();
					}
				}
				else
				{
					FillGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// MATTHEW CALL ID 74280 8/3/2005
			Grid.Select(0, 0);
			// SAVE PAYMENTS IN TABLE
			clsSave.Execute("delete from tblStatePayments", "twpy0000.vb1");
			clsSave.OpenRecordset("select * from tblstatepayments", "twpy0000.vb1");
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				// If Val(Grid.TextMatrix(x, 3)) > 0 Or Val(Grid.TextMatrix(x, 2)) > 0 Then
				// ONLY TAKE NON ZERO AMOUNTS
				if (Information.IsDate(Grid.TextMatrix(x, 1)))
				{
					clsSave.AddNew();
					clsSave.Set_Fields("Checknumber", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, 0))));
					if (Information.IsDate(Grid.TextMatrix(x, 1)))
					{
						clsSave.Set_Fields("DatePaid", Grid.TextMatrix(x, 1));
					}
					if (Conversion.Val(Grid.TextMatrix(x, 2)) > 0)
					{
						clsSave.Set_Fields("withheld", Strings.Format(FCConvert.ToDouble(Grid.TextMatrix(x, 2)), "0.00"));
					}
					else
					{
						clsSave.Set_Fields("withheld", 0);
					}
					if (Conversion.Val(Grid.TextMatrix(x, 3)) > 0)
					{
						clsSave.Set_Fields("Paid", Strings.Format(FCConvert.ToDouble(Grid.TextMatrix(x, 3)), "0.00"));
					}
					clsSave.Update();
				}
			}
			// x
			clsSave.OpenRecordset("select * from tbl941PaymentSettings", "twpy0000.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Edit();
			}
			else
			{
				clsSave.AddNew();
			}
			clsSave.Set_Fields("Reportyear", lngYear);
			clsSave.Set_Fields("lastReportMonth", intLastMonth);
			clsSave.Set_Fields("Range", strSequence);
			clsSave.Update();
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolCancelSelected = true;
			Close();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.16 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.28 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.28 * GridWidth));
			//Grid.Height = Grid.RowHeight(0) * Grid.Rows + 50;
		}

		private void SetupGrid()
		{
			//FC:FINAL:DDU:#2753 - fixed column alignments
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid.TextMatrix(0, 0, "Check");
			Grid.TextMatrix(1, 0, "ID");
			Grid.TextMatrix(0, 1, "Date");
			Grid.TextMatrix(1, 1, "Wages Paid");
			Grid.TextMatrix(0, 2, "Amount");
			Grid.TextMatrix(1, 2, "Withheld");
			Grid.TextMatrix(0, 3, "Check");
			Grid.TextMatrix(1, 3, "Amount");
        }

		private void FillGrid()
		{
			// FILLS GRID WITH ECH PAYDATE AND HOW MUCH WITHHOLDING. IT ASSUMES THE PAYMENT MADE IS THE SAME AS WITHHOLDING
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper rsCheckNumber = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string[] strAry = null;
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			string strRange = "";
			string strCheckNumber = "";
			double dblCheckAmount = 0;
			strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
			if (Information.UBound(strAry, 1) < 1)
			{
				// ALL OR SINGLE
				lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			}
			else
			{
				// RANGE
				lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
			}
			if (lngSeqStart >= 0)
			{
				strRange = " seqnumber between " + FCConvert.ToString(lngSeqStart) + " and " + FCConvert.ToString(lngSeqEnd) + " and ";
			}
			else
			{
				strRange = "";
			}
			Grid.Rows = 2;
			// FILL IN THE DATES FIRST
			strSQL = "select paydate from tblcheckdetail where month(paydate) between " + FCConvert.ToString(intLastMonth - 2) + " and " + FCConvert.ToString(intLastMonth) + " and year(paydate) = " + FCConvert.ToString(lngYear) + " group by paydate order by paydate";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				Grid.AddItem("\t" + Strings.Format(clsLoad.Get_Fields("paydate"), "MM/dd/yyyy"));
				clsLoad.MoveNext();
			}
			// NOW GET THE FINANCIAL INFO FOR EACH DATE
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				if (Information.IsDate(Grid.TextMatrix(x, 1)))
				{
					// MATTHEW 4/7/2005 CALL ID 66061
					rsCheckNumber.OpenRecordset("Select * from tblCategories where Description = 'State Tax'");
					if (rsCheckNumber.EndOfFile())
					{
						strCheckNumber = string.Empty;
						dblCheckAmount = 0;
					}
					else
					{
						if (FCConvert.ToInt32(rsCheckNumber.Get_Fields("Include")) == 0)
						{
							strCheckNumber = string.Empty;
							dblCheckAmount = 0;
						}
						else
						{
							rsCheckNumber.OpenRecordset("Select CheckNumber,Sum(TrustAmount) as SumOfTrustAmount from tblCheckDetail where TrustCategoryID = 4 and PayDate = '" + Grid.TextMatrix(x, 1) + "' AND CheckVoid = 0 Group By CheckNumber");
							if (rsCheckNumber.EndOfFile())
							{
								strCheckNumber = string.Empty;
								dblCheckAmount = 0;
							}
							else
							{
								if (rsCheckNumber.RecordCount() > 1)
								{
									strCheckNumber = "Multiple";
								}
								else
								{
									strCheckNumber = FCConvert.ToString(rsCheckNumber.Get_Fields("CheckNumber"));
								}
								rsCheckNumber.OpenRecordset("Select Sum(TrustAmount) as SumOfTrustAmount from tblCheckDetail where TrustCategoryID = 4 and PayDate = '" + Grid.TextMatrix(x, 1) + "' AND CheckVoid = 0");
								dblCheckAmount = rsCheckNumber.Get_Fields("SumOfTrustAmount");
							}
						}
					}
					strSQL = "SELECT Sum(tblCheckDetail.StateTaxWH) AS TotalSum, tblCheckDetail.PayDate FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where " + strRange + " PayDate = '" + Grid.TextMatrix(x, 1) + "' and checkvoid = 0 GROUP BY tblCheckDetail.PayDate";
					clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						Grid.TextMatrix(x, 0, strCheckNumber);
						Grid.TextMatrix(x, 2, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("totalsum"))), "0.00"));
						// If gboolSameAsC1WithHolding Then
						// Grid.TextMatrix(x, 3) = Format(Val(clsLoad.Fields("totalsum")), "0.00")
						// Else
						Grid.TextMatrix(x, 3, Strings.Format(dblCheckAmount, "0.00"));
						// End If
					}
				}
			}
			// x
			// *****************************************************************************
			// MATTHEW CALL ID 74280 8/3/2005
			dblLastCheck = Conversion.Val(Grid.TextMatrix(Grid.Rows - 1, 3));
			intGridRow = (Grid.Rows - 1);
			// lblLastCheck.Text = "Is check " & Grid.TextMatrix(intGridRow, 0) & " for " & dblLastCheck & " being sent with this return?"
			lblLastCheck.Text = "Last Payment Being Sent With This Report";
			// *****************************************************************************
			//FC:FINAL:DDU:#2753 - fixed column alignments
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, Grid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			if (Grid.Rows < 17)
				Grid.Rows = 17;
			ResizeGrid();
		}

		public void Init(int intLastMonthInQuarter, int lngYearCovered, string strSeq = "-1")
		{
			strSequence = strSeq;
			lngYear = lngYearCovered;
			intLastMonth = intLastMonthInQuarter;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuPayments_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 2; x <= (Grid.Rows - 1); x++)
			{
				Grid.TextMatrix(x, 3, Grid.TextMatrix(x, 2));
			}
			// x
			if (lblLastCheck.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblLastCheck_Click();
			}
		}

		private void mnuReload_Click(object sender, System.EventArgs e)
		{
			FillGrid();
		}
	}
}
