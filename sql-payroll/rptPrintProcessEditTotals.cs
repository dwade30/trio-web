﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPrintProcessEditTotals.
	/// </summary>
	public partial class rptPrintProcessEditTotals : BaseSectionReport
	{
		public rptPrintProcessEditTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Edit Totals Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
			//vsTotals = this.vsTotalsControl.Control as FCReportGrid;
		}

		public static rptPrintProcessEditTotals InstancePtr
		{
			get
			{
				return (rptPrintProcessEditTotals)Sys.GetInstance(typeof(rptPrintProcessEditTotals));
			}
		}

		protected rptPrintProcessEditTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintProcessEditTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		
		private int intCounter;
		private int intDataChanged;
		private double dblDeductionTotal;
		private int lngPeriodNumber;
		private int lngJournalNumber;
		FCReportGrid vsTotals;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            this.vsTotalsControl.Type = typeof(fecherFoundation.FCReportGrid);
            vsTotals = this.vsTotalsControl.Control as FCReportGrid;
            // vbPorter upgrade warning: Label2 As object	OnWrite(string)
            // vbPorter upgrade warning: Label3 As object	OnWrite(string)
            // vbPorter upgrade warning: Label7 As object	OnWrite(string)
            //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            PageCounter = 0;
			blnFirstRecord = true;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblPayDate.Text = "Pay Date: " + Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy");
			SetTotalsGridProperties();
			vsTotals.Cols = frmProcessEditTotals.InstancePtr.vsGrid.Cols;
			vsTotals.ColWidth(0, FCConvert.ToInt32(frmProcessEditTotals.InstancePtr.vsGrid.Width * 0.3));
			vsTotals.ColWidth(1, FCConvert.ToInt32(frmProcessEditTotals.InstancePtr.vsGrid.Width * 0.2));
			vsTotals.ColWidth(2, FCConvert.ToInt32(frmProcessEditTotals.InstancePtr.vsGrid.Width * 0.2));
		}

        private void ActiveReport_ReportEnd(object sender, System.EventArgs e)
        {
            this.vsTotals.Dispose();
            this.vsTotals = null;
            this.vsTotalsControl.Dispose();
            this.vsTotalsControl = null;
        }

        private void Detail_Format(object sender, EventArgs e)
		{
			LoadTotalsGrid();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void SetTotalsGridProperties()
		{
			this.vsTotals.Cols = frmProcessEditTotals.InstancePtr.vsGrid.Cols;
			this.vsTotals.Rows = frmProcessEditTotals.InstancePtr.vsGrid.Rows;
			this.vsTotals.Height = frmProcessEditTotals.InstancePtr.vsGrid.HeightOriginal;
			//this.vsTotals.MergeRow(0, true);
			//this.vsTotals.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			//this.vsTotals.FixedCols = frmProcessEditTotals.InstancePtr.vsGrid.Cols;
		}

		private void LoadTotalsGrid()
		{
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			for (intRow = 0; intRow <= (frmProcessEditTotals.InstancePtr.vsGrid.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmProcessEditTotals.InstancePtr.vsGrid.Cols - 1); intCol++)
				{   
     //               vsTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol, frmProcessEditTotals.InstancePtr.vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intRow, intCol));
					//vsTotals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol, frmProcessEditTotals.InstancePtr.vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, intCol));
					//vsTotals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol, frmProcessEditTotals.InstancePtr.vsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, intCol));
					vsTotals.TextMatrix(intRow, intCol, frmProcessEditTotals.InstancePtr.vsGrid.TextMatrix(intRow, intCol));
				}
			}
			//vsTotals.Height = vsTotals.Rows * vsTotals.RowHeight(0);
		}

	
	}
}
