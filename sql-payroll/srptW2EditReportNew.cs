//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptW2EditReport.
	/// </summary>
	public partial class srptW2EditReport : FCSectionReport
	{
		public srptW2EditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Sub W2 Edit Report";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.IsSubReport = true;
		}

		public static srptW2EditReport InstancePtr
		{
			get
			{
				return (srptW2EditReport)Sys.GetInstance(typeof(srptW2EditReport));
			}
		}

		protected srptW2EditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsW2Deductions_AutoInitialized?.Dispose();
                rsW2Deductions_AutoInitialized = null;
				rsW2Matches_AutoInitialized?.Dispose();
                rsW2Matches_AutoInitialized = null;
				rsBox14_AutoInitialized?.Dispose();
                rsBox14_AutoInitialized = null;

            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptW2EditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public int gintRowOutLineLevel;
		public int intRecordCount;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsW2Deductions = new clsDRWrapper();
			public clsDRWrapper rsW2Deductions_AutoInitialized = null;
			public clsDRWrapper rsW2Deductions
			{
				get
				{
					if ( rsW2Deductions_AutoInitialized == null)
					{
						 rsW2Deductions_AutoInitialized = new clsDRWrapper();
					}
					return rsW2Deductions_AutoInitialized;
				}
				set
				{
					 rsW2Deductions_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsW2Matches = new clsDRWrapper();
			public clsDRWrapper rsW2Matches_AutoInitialized = null;
			public clsDRWrapper rsW2Matches
			{
				get
				{
					if ( rsW2Matches_AutoInitialized == null)
					{
						 rsW2Matches_AutoInitialized = new clsDRWrapper();
					}
					return rsW2Matches_AutoInitialized;
				}
				set
				{
					 rsW2Matches_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsbox12 = new clsDRWrapper();
			//public clsDRWrapper rsbox12_AutoInitialized = null;
			//public clsDRWrapper rsbox12
			//{
			//	get
			//	{
			//		if ( rsbox12_AutoInitialized == null)
			//		{
			//			 rsbox12_AutoInitialized = new clsDRWrapper();
			//		}
			//		return rsbox12_AutoInitialized;
			//	}
			//	set
			//	{
			//		 rsbox12_AutoInitialized = value;
			//	}
			//}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsBox14 = new clsDRWrapper();
			public clsDRWrapper rsBox14_AutoInitialized = null;
			public clsDRWrapper rsBox14
			{
				get
				{
					if ( rsBox14_AutoInitialized == null)
					{
						 rsBox14_AutoInitialized = new clsDRWrapper();
					}
					return rsBox14_AutoInitialized;
				}
				set
				{
					 rsBox14_AutoInitialized = value;
				}
			}
		public bool boolFirst12;
		public bool boolFirst14;
		public bool boolBox12Done;
		public bool boolBox14Done;
		// vbPorter upgrade warning: boolLoadBox12 As bool	OnWrite(bool, int)
		private bool boolLoadBox12;
		// vbPorter upgrade warning: boolLoadBox14 As bool	OnWrite(int, bool)
		private bool boolLoadBox14;
		int intCounter;
		// vbPorter upgrade warning: intArrayCounter As int	OnWriteFCConvert.ToInt32(
		int intArrayCounter;
		// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
		int intloop;

		private struct BoxEntries
		{
			// vbPorter upgrade warning: Code As string	OnWrite(int, string)
			public string Code;
			public double Amount;
			public bool ThirdParty;
		};

		private BoxEntries[] Box1214 = null;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolFound = false;
			if (boolBox12Done && boolBox14Done)
			{
				eArgs.EOF = true;
				return;
			}
			// 
			// If rptW2EditReport.txtEmpID <> "1015" Then
			// eof = True
			// Exit Sub
			// Else
			// MsgBox " "
			// End If
			if (boolLoadBox12)
			{
				Box1214 = new BoxEntries[0 + 1];
				Box1214[0].Amount = 0;
				Box1214[0].Code = FCConvert.ToString(0);
				Box1214[0].ThirdParty = false;
				// If rptW2EditReport.txtEmpID = "280" Then
				// eof = eof
				// End If
				rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box12 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rptW2EditReport.InstancePtr.txtEmpID.Text + "'", "TWPY0000.vb1");
				rsW2Deductions.MoveLast();
				rsW2Deductions.MoveFirst();
				if (rsW2Deductions.RecordCount() > 1)
				{
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() - 1 + 1];
				}
				// ReDim Box1214(rsW2Deductions.RecordCount)
				for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
				{
					Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
					Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
					Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
					rsW2Deductions.MoveNext();
				}
				rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code, TBLW2BOX12AND14.BOX12 HAVING (((tblW2Matches.EmployeeNumber)='" + rptW2EditReport.InstancePtr.txtEmpID.Text + "')) AND ((tblW2Box12And14.Box12)=1)");
				rsW2Matches.MoveLast();
				rsW2Matches.MoveFirst();
				// ReDim Preserve Box1214(UBound(Box1214) + rsW2Matches.RecordCount)
				while (!rsW2Matches.EndOfFile())
				{
					boolFound = false;
					for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
					{
						if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("code")))
						{
							Box1214[intloop].Amount += Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
							boolFound = true;
							break;
						}
					}
					// intloop
					if (!boolFound)
					{
						if (Information.UBound(Box1214, 1) > 0 || (Box1214[0].Amount > 0))
						{
							Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + 1 + 1);
						}
						Box1214[Information.UBound(Box1214)].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
						Box1214[Information.UBound(Box1214)].Amount = Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
					}
					rsW2Matches.MoveNext();
				}
			}
			if (boolLoadBox14)
			{
				// now fill box 14
				Box1214 = new BoxEntries[0 + 1];
				Box1214[0].Amount = 0;
				Box1214[0].Code = "";
				Box1214[0].ThirdParty = false;
				rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rptW2EditReport.InstancePtr.txtEmpID.Text + "'", "TWPY0000.vb1");
				rsW2Deductions.MoveLast();
				rsW2Deductions.MoveFirst();
				if (rsW2Deductions.RecordCount() > 1)
				{
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() - 1 + 1];
				}
				for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
				{
					Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
					Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
					Box1214[intArrayCounter].ThirdParty = rsW2Deductions.Get_Fields_Boolean("ThirdPartySave");
					rsW2Deductions.MoveNext();
				}
				rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber  GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code, tblW2Box12And14.Box14 HAVING (((tblW2Matches.EmployeeNumber)='" + rptW2EditReport.InstancePtr.txtEmpID.Text + "')) AND ((tblW2Box12And14.Box14)=1)");
				// Call rsW2Deductions.OpenRecordset("SELECT tblW2Matches.Code, tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Matches ON tblW2Box12And14.DeductionNumber = tblW2Matches.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Matches.ThirdPartySave, tblW2Matches.Code, tblW2Matches.EmployeeNumber HAVING tblW2Matches.Code<>'' AND tblW2Matches.EmployeeNumber= '" & rptW2EditReport.txtEmpID & "'", "TWPY0000.vb1")
				rsW2Matches.MoveLast();
				rsW2Matches.MoveFirst();
				// ReDim Preserve Box1214(UBound(Box1214) + rsW2Matches.RecordCount)
				while (!rsW2Matches.EndOfFile())
				{
					boolFound = false;
					for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
					{
						if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("code")))
						{
							Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
							boolFound = true;
							break;
						}
					}
					// intloop
					if (!boolFound)
					{
						if (Information.UBound(Box1214, 1) > 0 || (Box1214[0].Amount > 0))
						{
							Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + 1 + 1);
						}
						Box1214[Information.UBound(Box1214)].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
						Box1214[Information.UBound(Box1214)].Amount = Conversion.Val(rsW2Matches.Get_Fields("sumofcytdamount"));
						Box1214[Information.UBound(Box1214)].ThirdParty = rsW2Matches.Get_Fields_Boolean("thirdpartysave");
					}
					rsW2Matches.MoveNext();
				}
				// For intArrayCounter = 0 To rsW2Matches.RecordCount - 1
				// For intLoop = 0 To rsW2Matches.RecordCount - 1
				// If Box1214(intLoop).Code = rsW2Matches.Fields("Code") Then
				// Box1214(intLoop).Amount = Box1214(intLoop).Amount + rsW2Matches.Fields("SumOfCYTDAmount")
				// GoTo Next14
				// End If
				// Next
				// 
				// For intLoop = 0 To UBound(Box1214)
				// If Box1214(intLoop).Code = vbNullString Then
				// Box1214(intLoop).Code = rsW2Matches.Fields("Code")
				// Box1214(intLoop).Amount = rsW2Matches.Fields("SumOfCYTDAmount")
				// Box1214(intLoop).ThirdParty = rsW2Matches.Fields("ThirdPartySave")
				// GoTo Next14
				// End If
				// Next
				// Next14:
				// rsW2Matches.MoveNext
				// Next
			}
			if (!boolBox12Done)
			{
				intCounter = 1;
				for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
				{
					if (intCounter > 4)
					{
						break;
					}
					if (Box1214[intArrayCounter].Amount == 0)
						goto SkipEntry;
					if (intCounter == 1)
					{
						txtCode.Text = "Box 12:   " + Box1214[intArrayCounter].Code + " ";
						txtCode.Text = txtCode.Text + Strings.StrDup(20 - (fecherFoundation.Strings.Trim(Box1214[intArrayCounter].Code).Length + fecherFoundation.Strings.Trim(Strings.Format(Box1214[intArrayCounter].Amount, "0.00")).Length), " ") + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
						modGlobalVariables.Statics.gdblBox12Total += Box1214[intArrayCounter].Amount;
						intCounter += 1;
					}
					else
					{
						txtCode.Text = txtCode.Text + "\r" + "          " + Box1214[intArrayCounter].Code + " ";
						txtCode.Text = txtCode.Text + Strings.StrDup(20 - (fecherFoundation.Strings.Trim(Box1214[intArrayCounter].Code).Length + fecherFoundation.Strings.Trim(Strings.Format(Box1214[intArrayCounter].Amount, "0.00")).Length), " ") + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
						modGlobalVariables.Statics.gdblBox12Total += Box1214[intArrayCounter].Amount;
						intCounter += 1;
					}
					SkipEntry:
					;
				}
				if (intCounter > 1)
					txtCode.Text = txtCode.Text + "\r" + " ";
				boolBox12Done = true;
				boolLoadBox12 = false;
				boolLoadBox14 = FCConvert.ToBoolean(1);
				eArgs.EOF = false;
				return;
			}
			if (!boolBox14Done)
			{
				intCounter = 1;
				txtCode.Text = "";
				for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1)); intArrayCounter++)
				{
					if (intCounter > 4)
					{
						break;
					}
					if (Box1214[intArrayCounter].Amount == 0)
						goto SkipMatchEntry;
					if (intCounter == 1)
					{
						txtCode.Text = "Box 14:   " + Box1214[intArrayCounter].Code + " ";
						txtCode.Text = txtCode.Text + Strings.StrDup(20 - (fecherFoundation.Strings.Trim(Box1214[intArrayCounter].Code).Length + fecherFoundation.Strings.Trim(Strings.Format(Box1214[intArrayCounter].Amount, "0.00")).Length), " ") + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
						modGlobalVariables.Statics.gdblBox14Total += Box1214[intArrayCounter].Amount;
						intCounter += 1;
					}
					else
					{
						txtCode.Text = txtCode.Text + "\r" + "          " + Box1214[intArrayCounter].Code + " ";
						txtCode.Text = txtCode.Text + Strings.StrDup(20 - (fecherFoundation.Strings.Trim(Box1214[intArrayCounter].Code).Length + fecherFoundation.Strings.Trim(Strings.Format(Box1214[intArrayCounter].Amount, "0.00")).Length), " ") + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
						modGlobalVariables.Statics.gdblBox14Total += Box1214[intArrayCounter].Amount;
						intCounter += 1;
					}
					SkipMatchEntry:
					;
				}
				if (Information.UBound(Box1214, 1) == 2)
				{
					if (Box1214[0].Amount == 0 && Box1214[1].Amount == 0)
					{
						// there was no entries for box 14 so we are done.
						txtCode.Text = string.Empty;
						eArgs.EOF = true;
						return;
					}
					else
					{
						boolBox12Done = true;
						boolBox14Done = true;
						boolLoadBox12 = false;
						boolLoadBox14 = false;
						eArgs.EOF = false;
						return;
					}
					// ElseIf UBound(Box1214) = 0 Then
					// txtCode = vbNullString
					// EOF = True
					// Exit Sub
				}
				else
				{
					boolBox12Done = true;
					boolBox14Done = true;
					boolLoadBox12 = false;
					boolLoadBox14 = false;
					eArgs.EOF = false;
					return;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolLoadBox12 = FCConvert.ToBoolean(1);
			Box1214 = new BoxEntries[0 + 1];
		}

		
	}
}
