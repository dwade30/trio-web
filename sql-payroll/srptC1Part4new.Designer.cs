﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Part4.
	/// </summary>
	partial class srptC1Part4
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1Part4));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUCAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSeasonal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSSN21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWage21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonal21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWH21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWagePageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWHPageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWHTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtWH1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYearStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWagePageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWHPageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWHTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line9,
            this.Label6,
            this.txtName,
            this.Label7,
            this.txtWithholdingAccount,
            this.Label8,
            this.txtUCAccount,
            this.Label9,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.txtSSN1,
            this.txtName1,
            this.txtWage1,
            this.Label18,
            this.txtSeasonal1,
            this.txtSSN2,
            this.txtName2,
            this.txtWage2,
            this.txtSeasonal2,
            this.txtWH2,
            this.txtSSN3,
            this.txtName3,
            this.txtWage3,
            this.txtSeasonal3,
            this.txtWH3,
            this.txtSSN4,
            this.txtName4,
            this.txtWage4,
            this.txtSeasonal4,
            this.txtWH4,
            this.txtSSN5,
            this.txtName5,
            this.txtWage5,
            this.txtSeasonal5,
            this.txtWH5,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.txtSSN6,
            this.txtName6,
            this.txtWage6,
            this.txtSeasonal6,
            this.txtWH6,
            this.Label29,
            this.txtSSN7,
            this.txtName7,
            this.txtWage7,
            this.txtSeasonal7,
            this.txtWH7,
            this.Label31,
            this.txtSSN8,
            this.txtName8,
            this.txtWage8,
            this.txtSeasonal8,
            this.txtWH8,
            this.Label33,
            this.txtSSN9,
            this.txtName9,
            this.txtWage9,
            this.txtSeasonal9,
            this.txtWH9,
            this.Label35,
            this.txtSSN10,
            this.txtName10,
            this.txtWage10,
            this.txtSeasonal10,
            this.txtWH10,
            this.Label37,
            this.txtSSN11,
            this.txtName11,
            this.txtWage11,
            this.txtSeasonal11,
            this.txtWH11,
            this.Label39,
            this.txtSSN12,
            this.txtName12,
            this.txtWage12,
            this.txtSeasonal12,
            this.txtWH12,
            this.Label41,
            this.txtSSN13,
            this.txtName13,
            this.txtWage13,
            this.txtSeasonal13,
            this.txtWH13,
            this.Label43,
            this.txtSSN14,
            this.txtName14,
            this.txtWage14,
            this.txtSeasonal14,
            this.txtWH14,
            this.Label45,
            this.txtSSN15,
            this.txtName15,
            this.txtWage15,
            this.txtSeasonal15,
            this.txtWH15,
            this.Label47,
            this.txtSSN16,
            this.txtName16,
            this.txtWage16,
            this.txtSeasonal16,
            this.txtWH16,
            this.Label49,
            this.txtSSN17,
            this.txtName17,
            this.txtWage17,
            this.txtSeasonal17,
            this.txtWH17,
            this.Label51,
            this.txtSSN18,
            this.txtName18,
            this.txtWage18,
            this.txtSeasonal18,
            this.txtWH18,
            this.Label53,
            this.txtSSN19,
            this.txtName19,
            this.txtWage19,
            this.txtSeasonal19,
            this.txtWH19,
            this.Label55,
            this.txtSSN20,
            this.txtName20,
            this.txtWage20,
            this.txtSeasonal20,
            this.txtWH20,
            this.Label57,
            this.txtSSN21,
            this.txtName21,
            this.txtWage21,
            this.txtSeasonal21,
            this.txtWH21,
            this.Label59,
            this.Label64,
            this.Label65,
            this.Label66,
            this.Label67,
            this.Label68,
            this.txtWagePageTotal,
            this.Label69,
            this.txtWageTotal,
            this.Label70,
            this.txtWHPageTotal,
            this.Label71,
            this.txtWHTotal,
            this.Line7,
            this.Barcode1,
            this.Label73,
            this.txtPeriodStart,
            this.txtPeriodEnd,
            this.Field25,
            this.Line3,
            this.Label75,
            this.Label74,
            this.Line2,
            this.txtWH1,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Label11,
            this.txtYearStart,
            this.txtYearEnd});
			this.Detail.Height = 10.11458F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Line9
			// 
			this.Line9.Height = 8F;
			this.Line9.Left = 7.5625F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1F;
			this.Line9.Visible = false;
			this.Line9.Width = 0F;
			this.Line9.X1 = 7.5625F;
			this.Line9.X2 = 7.5625F;
			this.Line9.Y1 = 1F;
			this.Line9.Y2 = 9F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.25F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 6pt; vertical-align: middle";
			this.Label6.Text = "Name";
			this.Label6.Top = 0.3159722F;
			this.Label6.Width = 0.4166667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 1.083333F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier New\'";
			this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtName.Top = 0.3159722F;
			this.txtName.Width = 3.583333F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.25F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.25F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 6pt; vertical-align: middle";
			this.Label7.Text = "Withholding Account No.";
			this.Label7.Top = 0.5729167F;
			this.Label7.Width = 0.75F;
			// 
			// txtWithholdingAccount
			// 
			this.txtWithholdingAccount.Height = 0.1666667F;
			this.txtWithholdingAccount.Left = 1.104167F;
			this.txtWithholdingAccount.Name = "txtWithholdingAccount";
			this.txtWithholdingAccount.Style = "font-family: \'Courier New\'";
			this.txtWithholdingAccount.Text = "99 999999999";
			this.txtWithholdingAccount.Top = 0.6666667F;
			this.txtWithholdingAccount.Width = 1.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.2916667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 6pt; vertical-align: middle";
			this.Label8.Text = "UC Employer Account No:";
			this.Label8.Top = 0.8784722F;
			this.Label8.Width = 0.75F;
			// 
			// txtUCAccount
			// 
			this.txtUCAccount.Height = 0.1666667F;
			this.txtUCAccount.Left = 1.083333F;
			this.txtUCAccount.Name = "txtUCAccount";
			this.txtUCAccount.Style = "font-family: \'Courier New\'; vertical-align: middle";
			this.txtUCAccount.Text = "9999999999";
			this.txtUCAccount.Top = 1.003472F;
			this.txtUCAccount.Width = 1.416667F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.Label9.Text = "SCHEDULE 2/C1 (Form 941/C1-ME)";
			this.Label9.Top = 0F;
			this.Label9.Width = 3.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.75F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-decoration: under" +
    "line";
			this.Label12.Text = "Quarterly Income Tax Withholding and Unemployment Compensation Wages Listing";
			this.Label12.Top = 1.541667F;
			this.Label12.Width = 6F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.25F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.083333F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 7pt";
			this.Label13.Text = "All employers designated SEASONAL by Department of Labor, see instructions for co" +
    "lumn 16 on page 7.";
			this.Label13.Top = 1.770833F;
			this.Label13.Width = 2.3125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.1666667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 7pt";
			this.Label14.Text = "14. Employee Name (Last, First, MI)";
			this.Label14.Top = 2.125F;
			this.Label14.Width = 2F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 7pt";
			this.Label15.Text = "15. Social Security Number";
			this.Label15.Top = 2.125F;
			this.Label15.Width = 1.416667F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.75F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Arial Narrow\'; font-size: 7pt";
			this.Label16.Text = "16. UC Gross Wages Paid";
			this.Label16.Top = 2.125F;
			this.Label16.Width = 1.333333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 5.395833F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Arial Narrow\'; font-size: 7pt; text-align: center";
			this.Label17.Text = "Seasonal";
			this.Label17.Top = 1.541667F;
			this.Label17.Visible = false;
			this.Label17.Width = 0.5F;
			// 
			// txtSSN1
			// 
			this.txtSSN1.Height = 0.1979167F;
			this.txtSSN1.Left = 2.189583F;
			this.txtSSN1.Name = "txtSSN1";
			this.txtSSN1.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN1.Text = "999999999";
			this.txtSSN1.Top = 2.322917F;
			this.txtSSN1.Width = 1.25F;
			// 
			// txtName1
			// 
			this.txtName1.CanGrow = false;
			this.txtName1.Height = 0.1979167F;
			this.txtName1.Left = 0.3229167F;
			this.txtName1.Name = "txtName1";
			this.txtName1.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName1.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName1.Top = 2.354167F;
			this.txtName1.Width = 1.833333F;
			// 
			// txtWage1
			// 
			this.txtWage1.Height = 0.1979167F;
			this.txtWage1.Left = 3.868056F;
			this.txtWage1.Name = "txtWage1";
			this.txtWage1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage1.Text = "9999999999.99";
			this.txtWage1.Top = 2.322917F;
			this.txtWage1.Width = 1.416667F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1979167F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.04166667F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'";
			this.Label18.Text = "a.";
			this.Label18.Top = 2.354167F;
			this.Label18.Width = 0.1666667F;
			// 
			// txtSeasonal1
			// 
			this.txtSeasonal1.Height = 0.1979167F;
			this.txtSeasonal1.HyperLink = null;
			this.txtSeasonal1.Left = 5.555555F;
			this.txtSeasonal1.Name = "txtSeasonal1";
			this.txtSeasonal1.Style = "font-family: \'Courier New\'";
			this.txtSeasonal1.Text = "X";
			this.txtSeasonal1.Top = 2.322917F;
			this.txtSeasonal1.Width = 0.1666667F;
			// 
			// txtSSN2
			// 
			this.txtSSN2.Height = 0.1979167F;
			this.txtSSN2.Left = 2.1875F;
			this.txtSSN2.Name = "txtSSN2";
			this.txtSSN2.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN2.Text = "999999999";
			this.txtSSN2.Top = 2.65625F;
			this.txtSSN2.Width = 1.25F;
			// 
			// txtName2
			// 
			this.txtName2.CanGrow = false;
			this.txtName2.Height = 0.1979167F;
			this.txtName2.Left = 0.3229167F;
			this.txtName2.Name = "txtName2";
			this.txtName2.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName2.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName2.Top = 2.677083F;
			this.txtName2.Width = 1.833333F;
			// 
			// txtWage2
			// 
			this.txtWage2.Height = 0.1979167F;
			this.txtWage2.Left = 3.868056F;
			this.txtWage2.Name = "txtWage2";
			this.txtWage2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage2.Text = "9999999999.99";
			this.txtWage2.Top = 2.65625F;
			this.txtWage2.Width = 1.416667F;
			// 
			// txtSeasonal2
			// 
			this.txtSeasonal2.Height = 0.1979167F;
			this.txtSeasonal2.HyperLink = null;
			this.txtSeasonal2.Left = 5.555555F;
			this.txtSeasonal2.Name = "txtSeasonal2";
			this.txtSeasonal2.Style = "font-family: \'Courier New\'";
			this.txtSeasonal2.Text = "X";
			this.txtSeasonal2.Top = 2.65625F;
			this.txtSeasonal2.Width = 0.1666667F;
			// 
			// txtWH2
			// 
			this.txtWH2.Height = 0.1979167F;
			this.txtWH2.Left = 5.888889F;
			this.txtWH2.Name = "txtWH2";
			this.txtWH2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH2.Text = "9999999999.99";
			this.txtWH2.Top = 2.65625F;
			this.txtWH2.Width = 1.416667F;
			// 
			// txtSSN3
			// 
			this.txtSSN3.Height = 0.1979167F;
			this.txtSSN3.Left = 2.1875F;
			this.txtSSN3.Name = "txtSSN3";
			this.txtSSN3.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN3.Text = "999999999";
			this.txtSSN3.Top = 2.989583F;
			this.txtSSN3.Width = 1.25F;
			// 
			// txtName3
			// 
			this.txtName3.CanGrow = false;
			this.txtName3.Height = 0.1979167F;
			this.txtName3.Left = 0.3229167F;
			this.txtName3.Name = "txtName3";
			this.txtName3.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName3.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName3.Top = 3.020833F;
			this.txtName3.Width = 1.833333F;
			// 
			// txtWage3
			// 
			this.txtWage3.Height = 0.1979167F;
			this.txtWage3.Left = 3.868056F;
			this.txtWage3.Name = "txtWage3";
			this.txtWage3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage3.Text = "9999999999.99";
			this.txtWage3.Top = 2.989583F;
			this.txtWage3.Width = 1.416667F;
			// 
			// txtSeasonal3
			// 
			this.txtSeasonal3.Height = 0.1979167F;
			this.txtSeasonal3.HyperLink = null;
			this.txtSeasonal3.Left = 5.555555F;
			this.txtSeasonal3.Name = "txtSeasonal3";
			this.txtSeasonal3.Style = "font-family: \'Courier New\'";
			this.txtSeasonal3.Text = "X";
			this.txtSeasonal3.Top = 2.989583F;
			this.txtSeasonal3.Width = 0.1666667F;
			// 
			// txtWH3
			// 
			this.txtWH3.Height = 0.1979167F;
			this.txtWH3.Left = 5.885417F;
			this.txtWH3.Name = "txtWH3";
			this.txtWH3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH3.Text = "9999999999.99";
			this.txtWH3.Top = 2.989583F;
			this.txtWH3.Width = 1.416667F;
			// 
			// txtSSN4
			// 
			this.txtSSN4.Height = 0.1979167F;
			this.txtSSN4.Left = 2.1875F;
			this.txtSSN4.Name = "txtSSN4";
			this.txtSSN4.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN4.Text = "999999999";
			this.txtSSN4.Top = 3.322917F;
			this.txtSSN4.Width = 1.25F;
			// 
			// txtName4
			// 
			this.txtName4.CanGrow = false;
			this.txtName4.Height = 0.1979167F;
			this.txtName4.Left = 0.3229167F;
			this.txtName4.Name = "txtName4";
			this.txtName4.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName4.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName4.Top = 3.354167F;
			this.txtName4.Width = 1.833333F;
			// 
			// txtWage4
			// 
			this.txtWage4.Height = 0.1979167F;
			this.txtWage4.Left = 3.868056F;
			this.txtWage4.Name = "txtWage4";
			this.txtWage4.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage4.Text = "9999999999.99";
			this.txtWage4.Top = 3.322917F;
			this.txtWage4.Width = 1.416667F;
			// 
			// txtSeasonal4
			// 
			this.txtSeasonal4.Height = 0.1979167F;
			this.txtSeasonal4.HyperLink = null;
			this.txtSeasonal4.Left = 5.555555F;
			this.txtSeasonal4.Name = "txtSeasonal4";
			this.txtSeasonal4.Style = "font-family: \'Courier New\'";
			this.txtSeasonal4.Text = "X";
			this.txtSeasonal4.Top = 3.322917F;
			this.txtSeasonal4.Width = 0.1666667F;
			// 
			// txtWH4
			// 
			this.txtWH4.Height = 0.1979167F;
			this.txtWH4.Left = 5.885417F;
			this.txtWH4.Name = "txtWH4";
			this.txtWH4.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH4.Text = "9999999999.99";
			this.txtWH4.Top = 3.322917F;
			this.txtWH4.Width = 1.416667F;
			// 
			// txtSSN5
			// 
			this.txtSSN5.Height = 0.1979167F;
			this.txtSSN5.Left = 2.1875F;
			this.txtSSN5.Name = "txtSSN5";
			this.txtSSN5.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN5.Text = "999999999";
			this.txtSSN5.Top = 3.65625F;
			this.txtSSN5.Width = 1.25F;
			// 
			// txtName5
			// 
			this.txtName5.CanGrow = false;
			this.txtName5.Height = 0.1979167F;
			this.txtName5.Left = 0.3229167F;
			this.txtName5.Name = "txtName5";
			this.txtName5.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName5.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName5.Top = 3.6875F;
			this.txtName5.Width = 1.8125F;
			// 
			// txtWage5
			// 
			this.txtWage5.Height = 0.1979167F;
			this.txtWage5.Left = 3.868056F;
			this.txtWage5.Name = "txtWage5";
			this.txtWage5.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage5.Text = "9999999999.99";
			this.txtWage5.Top = 3.65625F;
			this.txtWage5.Width = 1.416667F;
			// 
			// txtSeasonal5
			// 
			this.txtSeasonal5.Height = 0.1979167F;
			this.txtSeasonal5.HyperLink = null;
			this.txtSeasonal5.Left = 5.555555F;
			this.txtSeasonal5.Name = "txtSeasonal5";
			this.txtSeasonal5.Style = "font-family: \'Courier New\'";
			this.txtSeasonal5.Text = "X";
			this.txtSeasonal5.Top = 3.65625F;
			this.txtSeasonal5.Width = 0.1666667F;
			// 
			// txtWH5
			// 
			this.txtWH5.Height = 0.1979167F;
			this.txtWH5.Left = 5.885417F;
			this.txtWH5.Name = "txtWH5";
			this.txtWH5.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH5.Text = "9999999999.99";
			this.txtWH5.Top = 3.65625F;
			this.txtWH5.Width = 1.416667F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1979167F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.04166667F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'";
			this.Label24.Text = "b.";
			this.Label24.Top = 2.677083F;
			this.Label24.Width = 0.1666667F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1979167F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.04166667F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'";
			this.Label25.Text = "c.";
			this.Label25.Top = 3.020833F;
			this.Label25.Width = 0.1666667F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1979167F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.04166667F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'";
			this.Label26.Text = "d.";
			this.Label26.Top = 3.354167F;
			this.Label26.Width = 0.1666667F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1979167F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.04166667F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'";
			this.Label27.Text = "e.";
			this.Label27.Top = 3.6875F;
			this.Label27.Width = 0.1875F;
			// 
			// txtSSN6
			// 
			this.txtSSN6.Height = 0.1979167F;
			this.txtSSN6.Left = 2.1875F;
			this.txtSSN6.Name = "txtSSN6";
			this.txtSSN6.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN6.Text = "999999999";
			this.txtSSN6.Top = 3.989583F;
			this.txtSSN6.Width = 1.25F;
			// 
			// txtName6
			// 
			this.txtName6.CanGrow = false;
			this.txtName6.Height = 0.1979167F;
			this.txtName6.Left = 0.3229167F;
			this.txtName6.Name = "txtName6";
			this.txtName6.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName6.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName6.Top = 4.020833F;
			this.txtName6.Width = 1.833333F;
			// 
			// txtWage6
			// 
			this.txtWage6.Height = 0.1979167F;
			this.txtWage6.Left = 3.868056F;
			this.txtWage6.Name = "txtWage6";
			this.txtWage6.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage6.Text = "9999999999.99";
			this.txtWage6.Top = 3.989583F;
			this.txtWage6.Width = 1.416667F;
			// 
			// txtSeasonal6
			// 
			this.txtSeasonal6.Height = 0.1979167F;
			this.txtSeasonal6.HyperLink = null;
			this.txtSeasonal6.Left = 5.555555F;
			this.txtSeasonal6.Name = "txtSeasonal6";
			this.txtSeasonal6.Style = "font-family: \'Courier New\'";
			this.txtSeasonal6.Text = "X";
			this.txtSeasonal6.Top = 3.989583F;
			this.txtSeasonal6.Width = 0.1666667F;
			// 
			// txtWH6
			// 
			this.txtWH6.Height = 0.1979167F;
			this.txtWH6.Left = 5.885417F;
			this.txtWH6.Name = "txtWH6";
			this.txtWH6.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH6.Text = "9999999999.99";
			this.txtWH6.Top = 3.989583F;
			this.txtWH6.Width = 1.416667F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1979167F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.04166667F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'";
			this.Label29.Text = "f.";
			this.Label29.Top = 4.020833F;
			this.Label29.Width = 0.1666667F;
			// 
			// txtSSN7
			// 
			this.txtSSN7.Height = 0.1979167F;
			this.txtSSN7.Left = 2.1875F;
			this.txtSSN7.Name = "txtSSN7";
			this.txtSSN7.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN7.Text = "999999999";
			this.txtSSN7.Top = 4.322917F;
			this.txtSSN7.Width = 1.25F;
			// 
			// txtName7
			// 
			this.txtName7.CanGrow = false;
			this.txtName7.Height = 0.1979167F;
			this.txtName7.Left = 0.3229167F;
			this.txtName7.Name = "txtName7";
			this.txtName7.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName7.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName7.Top = 4.354167F;
			this.txtName7.Width = 1.833333F;
			// 
			// txtWage7
			// 
			this.txtWage7.Height = 0.1979167F;
			this.txtWage7.Left = 3.868056F;
			this.txtWage7.Name = "txtWage7";
			this.txtWage7.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage7.Text = "9999999999.99";
			this.txtWage7.Top = 4.322917F;
			this.txtWage7.Width = 1.416667F;
			// 
			// txtSeasonal7
			// 
			this.txtSeasonal7.Height = 0.1979167F;
			this.txtSeasonal7.HyperLink = null;
			this.txtSeasonal7.Left = 5.555555F;
			this.txtSeasonal7.Name = "txtSeasonal7";
			this.txtSeasonal7.Style = "font-family: \'Courier New\'";
			this.txtSeasonal7.Text = "X";
			this.txtSeasonal7.Top = 4.322917F;
			this.txtSeasonal7.Width = 0.1666667F;
			// 
			// txtWH7
			// 
			this.txtWH7.Height = 0.1979167F;
			this.txtWH7.Left = 5.885417F;
			this.txtWH7.Name = "txtWH7";
			this.txtWH7.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH7.Text = "9999999999.99";
			this.txtWH7.Top = 4.322917F;
			this.txtWH7.Width = 1.416667F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1979167F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.04166667F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'";
			this.Label31.Text = "g.";
			this.Label31.Top = 4.354167F;
			this.Label31.Width = 0.1666667F;
			// 
			// txtSSN8
			// 
			this.txtSSN8.Height = 0.1979167F;
			this.txtSSN8.Left = 2.1875F;
			this.txtSSN8.Name = "txtSSN8";
			this.txtSSN8.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN8.Text = "999999999";
			this.txtSSN8.Top = 4.65625F;
			this.txtSSN8.Width = 1.25F;
			// 
			// txtName8
			// 
			this.txtName8.CanGrow = false;
			this.txtName8.Height = 0.1979167F;
			this.txtName8.Left = 0.3229167F;
			this.txtName8.Name = "txtName8";
			this.txtName8.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName8.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName8.Top = 4.6875F;
			this.txtName8.Width = 1.833333F;
			// 
			// txtWage8
			// 
			this.txtWage8.Height = 0.1979167F;
			this.txtWage8.Left = 3.868056F;
			this.txtWage8.Name = "txtWage8";
			this.txtWage8.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage8.Text = "9999999999.99";
			this.txtWage8.Top = 4.65625F;
			this.txtWage8.Width = 1.416667F;
			// 
			// txtSeasonal8
			// 
			this.txtSeasonal8.Height = 0.1979167F;
			this.txtSeasonal8.HyperLink = null;
			this.txtSeasonal8.Left = 5.555555F;
			this.txtSeasonal8.Name = "txtSeasonal8";
			this.txtSeasonal8.Style = "font-family: \'Courier New\'";
			this.txtSeasonal8.Text = "X";
			this.txtSeasonal8.Top = 4.65625F;
			this.txtSeasonal8.Width = 0.1666667F;
			// 
			// txtWH8
			// 
			this.txtWH8.Height = 0.1979167F;
			this.txtWH8.Left = 5.885417F;
			this.txtWH8.Name = "txtWH8";
			this.txtWH8.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH8.Text = "9999999999.99";
			this.txtWH8.Top = 4.65625F;
			this.txtWH8.Width = 1.416667F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1979167F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.04166667F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'";
			this.Label33.Text = "h.";
			this.Label33.Top = 4.6875F;
			this.Label33.Width = 0.1666667F;
			// 
			// txtSSN9
			// 
			this.txtSSN9.Height = 0.1979167F;
			this.txtSSN9.Left = 2.1875F;
			this.txtSSN9.Name = "txtSSN9";
			this.txtSSN9.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN9.Text = "999999999";
			this.txtSSN9.Top = 4.989583F;
			this.txtSSN9.Width = 1.25F;
			// 
			// txtName9
			// 
			this.txtName9.CanGrow = false;
			this.txtName9.Height = 0.1979167F;
			this.txtName9.Left = 0.3229167F;
			this.txtName9.Name = "txtName9";
			this.txtName9.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName9.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName9.Top = 5.020833F;
			this.txtName9.Width = 1.833333F;
			// 
			// txtWage9
			// 
			this.txtWage9.Height = 0.1979167F;
			this.txtWage9.Left = 3.868056F;
			this.txtWage9.Name = "txtWage9";
			this.txtWage9.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage9.Text = "9999999999.99";
			this.txtWage9.Top = 4.989583F;
			this.txtWage9.Width = 1.416667F;
			// 
			// txtSeasonal9
			// 
			this.txtSeasonal9.Height = 0.1979167F;
			this.txtSeasonal9.HyperLink = null;
			this.txtSeasonal9.Left = 5.555555F;
			this.txtSeasonal9.Name = "txtSeasonal9";
			this.txtSeasonal9.Style = "font-family: \'Courier New\'";
			this.txtSeasonal9.Text = "X";
			this.txtSeasonal9.Top = 4.989583F;
			this.txtSeasonal9.Width = 0.1666667F;
			// 
			// txtWH9
			// 
			this.txtWH9.Height = 0.1979167F;
			this.txtWH9.Left = 5.885417F;
			this.txtWH9.Name = "txtWH9";
			this.txtWH9.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH9.Text = "9999999999.99";
			this.txtWH9.Top = 4.989583F;
			this.txtWH9.Width = 1.416667F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1979167F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.04166667F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'";
			this.Label35.Text = "i.";
			this.Label35.Top = 5.020833F;
			this.Label35.Width = 0.1666667F;
			// 
			// txtSSN10
			// 
			this.txtSSN10.Height = 0.1979167F;
			this.txtSSN10.Left = 2.1875F;
			this.txtSSN10.Name = "txtSSN10";
			this.txtSSN10.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN10.Text = "999999999";
			this.txtSSN10.Top = 5.322917F;
			this.txtSSN10.Width = 1.25F;
			// 
			// txtName10
			// 
			this.txtName10.CanGrow = false;
			this.txtName10.Height = 0.1979167F;
			this.txtName10.Left = 0.3229167F;
			this.txtName10.Name = "txtName10";
			this.txtName10.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName10.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName10.Top = 5.364583F;
			this.txtName10.Width = 1.833333F;
			// 
			// txtWage10
			// 
			this.txtWage10.Height = 0.1979167F;
			this.txtWage10.Left = 3.868056F;
			this.txtWage10.Name = "txtWage10";
			this.txtWage10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage10.Text = "9999999999.99";
			this.txtWage10.Top = 5.322917F;
			this.txtWage10.Width = 1.416667F;
			// 
			// txtSeasonal10
			// 
			this.txtSeasonal10.Height = 0.1979167F;
			this.txtSeasonal10.HyperLink = null;
			this.txtSeasonal10.Left = 5.555555F;
			this.txtSeasonal10.Name = "txtSeasonal10";
			this.txtSeasonal10.Style = "font-family: \'Courier New\'";
			this.txtSeasonal10.Text = "X";
			this.txtSeasonal10.Top = 5.322917F;
			this.txtSeasonal10.Width = 0.1666667F;
			// 
			// txtWH10
			// 
			this.txtWH10.Height = 0.1979167F;
			this.txtWH10.Left = 5.885417F;
			this.txtWH10.Name = "txtWH10";
			this.txtWH10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH10.Text = "9999999999.99";
			this.txtWH10.Top = 5.322917F;
			this.txtWH10.Width = 1.416667F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1979167F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 0.04166667F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'";
			this.Label37.Text = "j.";
			this.Label37.Top = 5.364583F;
			this.Label37.Width = 0.1666667F;
			// 
			// txtSSN11
			// 
			this.txtSSN11.Height = 0.1979167F;
			this.txtSSN11.Left = 2.1875F;
			this.txtSSN11.Name = "txtSSN11";
			this.txtSSN11.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN11.Text = "999999999";
			this.txtSSN11.Top = 5.65625F;
			this.txtSSN11.Width = 1.25F;
			// 
			// txtName11
			// 
			this.txtName11.CanGrow = false;
			this.txtName11.Height = 0.1979167F;
			this.txtName11.Left = 0.3229167F;
			this.txtName11.Name = "txtName11";
			this.txtName11.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName11.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName11.Top = 5.697917F;
			this.txtName11.Width = 1.833333F;
			// 
			// txtWage11
			// 
			this.txtWage11.Height = 0.1979167F;
			this.txtWage11.Left = 3.868056F;
			this.txtWage11.Name = "txtWage11";
			this.txtWage11.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage11.Text = "9999999999.99";
			this.txtWage11.Top = 5.65625F;
			this.txtWage11.Width = 1.416667F;
			// 
			// txtSeasonal11
			// 
			this.txtSeasonal11.Height = 0.1979167F;
			this.txtSeasonal11.HyperLink = null;
			this.txtSeasonal11.Left = 5.555555F;
			this.txtSeasonal11.Name = "txtSeasonal11";
			this.txtSeasonal11.Style = "font-family: \'Courier New\'";
			this.txtSeasonal11.Text = "X";
			this.txtSeasonal11.Top = 5.65625F;
			this.txtSeasonal11.Width = 0.1666667F;
			// 
			// txtWH11
			// 
			this.txtWH11.Height = 0.1979167F;
			this.txtWH11.Left = 5.885417F;
			this.txtWH11.Name = "txtWH11";
			this.txtWH11.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH11.Text = "9999999999.99";
			this.txtWH11.Top = 5.65625F;
			this.txtWH11.Width = 1.416667F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1979167F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.04166667F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'";
			this.Label39.Text = "k.";
			this.Label39.Top = 5.697917F;
			this.Label39.Width = 0.1666667F;
			// 
			// txtSSN12
			// 
			this.txtSSN12.Height = 0.1979167F;
			this.txtSSN12.Left = 2.1875F;
			this.txtSSN12.Name = "txtSSN12";
			this.txtSSN12.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN12.Text = "999999999";
			this.txtSSN12.Top = 5.989583F;
			this.txtSSN12.Width = 1.25F;
			// 
			// txtName12
			// 
			this.txtName12.CanGrow = false;
			this.txtName12.Height = 0.1979167F;
			this.txtName12.Left = 0.3229167F;
			this.txtName12.Name = "txtName12";
			this.txtName12.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName12.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName12.Top = 6.03125F;
			this.txtName12.Width = 1.833333F;
			// 
			// txtWage12
			// 
			this.txtWage12.Height = 0.1979167F;
			this.txtWage12.Left = 3.868056F;
			this.txtWage12.Name = "txtWage12";
			this.txtWage12.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage12.Text = "9999999999.99";
			this.txtWage12.Top = 5.989583F;
			this.txtWage12.Width = 1.416667F;
			// 
			// txtSeasonal12
			// 
			this.txtSeasonal12.Height = 0.1979167F;
			this.txtSeasonal12.HyperLink = null;
			this.txtSeasonal12.Left = 5.555555F;
			this.txtSeasonal12.Name = "txtSeasonal12";
			this.txtSeasonal12.Style = "font-family: \'Courier New\'";
			this.txtSeasonal12.Text = "X";
			this.txtSeasonal12.Top = 5.989583F;
			this.txtSeasonal12.Width = 0.1666667F;
			// 
			// txtWH12
			// 
			this.txtWH12.Height = 0.1979167F;
			this.txtWH12.Left = 5.885417F;
			this.txtWH12.Name = "txtWH12";
			this.txtWH12.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH12.Text = "9999999999.99";
			this.txtWH12.Top = 5.989583F;
			this.txtWH12.Width = 1.416667F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1979167F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.04166667F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'";
			this.Label41.Text = "l.";
			this.Label41.Top = 6.03125F;
			this.Label41.Width = 0.1666667F;
			// 
			// txtSSN13
			// 
			this.txtSSN13.Height = 0.1979167F;
			this.txtSSN13.Left = 2.1875F;
			this.txtSSN13.Name = "txtSSN13";
			this.txtSSN13.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN13.Text = "999999999";
			this.txtSSN13.Top = 6.322917F;
			this.txtSSN13.Width = 1.25F;
			// 
			// txtName13
			// 
			this.txtName13.CanGrow = false;
			this.txtName13.Height = 0.1979167F;
			this.txtName13.Left = 0.3229167F;
			this.txtName13.Name = "txtName13";
			this.txtName13.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName13.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName13.Top = 6.364583F;
			this.txtName13.Width = 1.833333F;
			// 
			// txtWage13
			// 
			this.txtWage13.Height = 0.1979167F;
			this.txtWage13.Left = 3.868056F;
			this.txtWage13.Name = "txtWage13";
			this.txtWage13.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage13.Text = "9999999999.99";
			this.txtWage13.Top = 6.322917F;
			this.txtWage13.Width = 1.416667F;
			// 
			// txtSeasonal13
			// 
			this.txtSeasonal13.Height = 0.1979167F;
			this.txtSeasonal13.HyperLink = null;
			this.txtSeasonal13.Left = 5.555555F;
			this.txtSeasonal13.Name = "txtSeasonal13";
			this.txtSeasonal13.Style = "font-family: \'Courier New\'";
			this.txtSeasonal13.Text = "X";
			this.txtSeasonal13.Top = 6.322917F;
			this.txtSeasonal13.Width = 0.1666667F;
			// 
			// txtWH13
			// 
			this.txtWH13.Height = 0.1979167F;
			this.txtWH13.Left = 5.885417F;
			this.txtWH13.Name = "txtWH13";
			this.txtWH13.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH13.Text = "9999999999.99";
			this.txtWH13.Top = 6.322917F;
			this.txtWH13.Width = 1.416667F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1979167F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.04166667F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'";
			this.Label43.Text = "m.";
			this.Label43.Top = 6.364583F;
			this.Label43.Width = 0.1666667F;
			// 
			// txtSSN14
			// 
			this.txtSSN14.Height = 0.1979167F;
			this.txtSSN14.Left = 2.1875F;
			this.txtSSN14.Name = "txtSSN14";
			this.txtSSN14.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN14.Text = "999999999";
			this.txtSSN14.Top = 6.65625F;
			this.txtSSN14.Width = 1.25F;
			// 
			// txtName14
			// 
			this.txtName14.CanGrow = false;
			this.txtName14.Height = 0.1979167F;
			this.txtName14.Left = 0.3229167F;
			this.txtName14.Name = "txtName14";
			this.txtName14.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName14.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName14.Top = 6.697917F;
			this.txtName14.Width = 1.833333F;
			// 
			// txtWage14
			// 
			this.txtWage14.Height = 0.1979167F;
			this.txtWage14.Left = 3.868056F;
			this.txtWage14.Name = "txtWage14";
			this.txtWage14.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage14.Text = "9999999999.99";
			this.txtWage14.Top = 6.65625F;
			this.txtWage14.Width = 1.416667F;
			// 
			// txtSeasonal14
			// 
			this.txtSeasonal14.Height = 0.1979167F;
			this.txtSeasonal14.HyperLink = null;
			this.txtSeasonal14.Left = 5.555555F;
			this.txtSeasonal14.Name = "txtSeasonal14";
			this.txtSeasonal14.Style = "font-family: \'Courier New\'";
			this.txtSeasonal14.Text = "X";
			this.txtSeasonal14.Top = 6.65625F;
			this.txtSeasonal14.Width = 0.1666667F;
			// 
			// txtWH14
			// 
			this.txtWH14.Height = 0.1979167F;
			this.txtWH14.Left = 5.885417F;
			this.txtWH14.Name = "txtWH14";
			this.txtWH14.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH14.Text = "9999999999.99";
			this.txtWH14.Top = 6.65625F;
			this.txtWH14.Width = 1.416667F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1979167F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.04166667F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'";
			this.Label45.Text = "n.";
			this.Label45.Top = 6.697917F;
			this.Label45.Width = 0.1666667F;
			// 
			// txtSSN15
			// 
			this.txtSSN15.Height = 0.1979167F;
			this.txtSSN15.Left = 2.1875F;
			this.txtSSN15.Name = "txtSSN15";
			this.txtSSN15.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN15.Text = "999999999";
			this.txtSSN15.Top = 6.989583F;
			this.txtSSN15.Width = 1.25F;
			// 
			// txtName15
			// 
			this.txtName15.CanGrow = false;
			this.txtName15.Height = 0.1979167F;
			this.txtName15.Left = 0.3229167F;
			this.txtName15.Name = "txtName15";
			this.txtName15.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName15.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName15.Top = 7.03125F;
			this.txtName15.Width = 1.833333F;
			// 
			// txtWage15
			// 
			this.txtWage15.Height = 0.1979167F;
			this.txtWage15.Left = 3.868056F;
			this.txtWage15.Name = "txtWage15";
			this.txtWage15.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage15.Text = "9999999999.99";
			this.txtWage15.Top = 6.989583F;
			this.txtWage15.Width = 1.416667F;
			// 
			// txtSeasonal15
			// 
			this.txtSeasonal15.Height = 0.1979167F;
			this.txtSeasonal15.HyperLink = null;
			this.txtSeasonal15.Left = 5.555555F;
			this.txtSeasonal15.Name = "txtSeasonal15";
			this.txtSeasonal15.Style = "font-family: \'Courier New\'";
			this.txtSeasonal15.Text = "X";
			this.txtSeasonal15.Top = 6.989583F;
			this.txtSeasonal15.Width = 0.1666667F;
			// 
			// txtWH15
			// 
			this.txtWH15.Height = 0.1979167F;
			this.txtWH15.Left = 5.885417F;
			this.txtWH15.Name = "txtWH15";
			this.txtWH15.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH15.Text = "9999999999.99";
			this.txtWH15.Top = 6.989583F;
			this.txtWH15.Width = 1.416667F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1979167F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0.04166667F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'";
			this.Label47.Text = "o.";
			this.Label47.Top = 7.03125F;
			this.Label47.Width = 0.1666667F;
			// 
			// txtSSN16
			// 
			this.txtSSN16.Height = 0.1979167F;
			this.txtSSN16.Left = 2.1875F;
			this.txtSSN16.Name = "txtSSN16";
			this.txtSSN16.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN16.Text = "999999999";
			this.txtSSN16.Top = 7.322917F;
			this.txtSSN16.Width = 1.25F;
			// 
			// txtName16
			// 
			this.txtName16.CanGrow = false;
			this.txtName16.Height = 0.1979167F;
			this.txtName16.Left = 0.3229167F;
			this.txtName16.Name = "txtName16";
			this.txtName16.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName16.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName16.Top = 7.364583F;
			this.txtName16.Width = 1.833333F;
			// 
			// txtWage16
			// 
			this.txtWage16.Height = 0.1979167F;
			this.txtWage16.Left = 3.868056F;
			this.txtWage16.Name = "txtWage16";
			this.txtWage16.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage16.Text = "9999999999.99";
			this.txtWage16.Top = 7.322917F;
			this.txtWage16.Width = 1.416667F;
			// 
			// txtSeasonal16
			// 
			this.txtSeasonal16.Height = 0.1979167F;
			this.txtSeasonal16.HyperLink = null;
			this.txtSeasonal16.Left = 5.555555F;
			this.txtSeasonal16.Name = "txtSeasonal16";
			this.txtSeasonal16.Style = "font-family: \'Courier New\'";
			this.txtSeasonal16.Text = "X";
			this.txtSeasonal16.Top = 7.322917F;
			this.txtSeasonal16.Width = 0.1666667F;
			// 
			// txtWH16
			// 
			this.txtWH16.Height = 0.1979167F;
			this.txtWH16.Left = 5.885417F;
			this.txtWH16.Name = "txtWH16";
			this.txtWH16.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH16.Text = "9999999999.99";
			this.txtWH16.Top = 7.322917F;
			this.txtWH16.Width = 1.416667F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1979167F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 0.04166667F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Tahoma\'";
			this.Label49.Text = "p.";
			this.Label49.Top = 7.364583F;
			this.Label49.Width = 0.1666667F;
			// 
			// txtSSN17
			// 
			this.txtSSN17.Height = 0.1979167F;
			this.txtSSN17.Left = 2.1875F;
			this.txtSSN17.Name = "txtSSN17";
			this.txtSSN17.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN17.Text = "999999999";
			this.txtSSN17.Top = 7.65625F;
			this.txtSSN17.Width = 1.25F;
			// 
			// txtName17
			// 
			this.txtName17.CanGrow = false;
			this.txtName17.Height = 0.1979167F;
			this.txtName17.Left = 0.3229167F;
			this.txtName17.Name = "txtName17";
			this.txtName17.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName17.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName17.Top = 7.697917F;
			this.txtName17.Width = 1.833333F;
			// 
			// txtWage17
			// 
			this.txtWage17.Height = 0.1979167F;
			this.txtWage17.Left = 3.868056F;
			this.txtWage17.Name = "txtWage17";
			this.txtWage17.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage17.Text = "9999999999.99";
			this.txtWage17.Top = 7.65625F;
			this.txtWage17.Width = 1.416667F;
			// 
			// txtSeasonal17
			// 
			this.txtSeasonal17.Height = 0.1979167F;
			this.txtSeasonal17.HyperLink = null;
			this.txtSeasonal17.Left = 5.555555F;
			this.txtSeasonal17.Name = "txtSeasonal17";
			this.txtSeasonal17.Style = "font-family: \'Courier New\'";
			this.txtSeasonal17.Text = "X";
			this.txtSeasonal17.Top = 7.65625F;
			this.txtSeasonal17.Width = 0.1666667F;
			// 
			// txtWH17
			// 
			this.txtWH17.Height = 0.1979167F;
			this.txtWH17.Left = 5.885417F;
			this.txtWH17.Name = "txtWH17";
			this.txtWH17.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH17.Text = "9999999999.99";
			this.txtWH17.Top = 7.65625F;
			this.txtWH17.Width = 1.416667F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1979167F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 0.04166667F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-family: \'Tahoma\'";
			this.Label51.Text = "q.";
			this.Label51.Top = 7.697917F;
			this.Label51.Width = 0.1666667F;
			// 
			// txtSSN18
			// 
			this.txtSSN18.Height = 0.1979167F;
			this.txtSSN18.Left = 2.1875F;
			this.txtSSN18.Name = "txtSSN18";
			this.txtSSN18.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN18.Text = "999999999";
			this.txtSSN18.Top = 7.989583F;
			this.txtSSN18.Width = 1.25F;
			// 
			// txtName18
			// 
			this.txtName18.CanGrow = false;
			this.txtName18.Height = 0.1979167F;
			this.txtName18.Left = 0.3229167F;
			this.txtName18.Name = "txtName18";
			this.txtName18.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName18.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName18.Top = 8.041667F;
			this.txtName18.Width = 1.833333F;
			// 
			// txtWage18
			// 
			this.txtWage18.Height = 0.1979167F;
			this.txtWage18.Left = 3.868056F;
			this.txtWage18.Name = "txtWage18";
			this.txtWage18.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage18.Text = "9999999999.99";
			this.txtWage18.Top = 7.989583F;
			this.txtWage18.Width = 1.416667F;
			// 
			// txtSeasonal18
			// 
			this.txtSeasonal18.Height = 0.1979167F;
			this.txtSeasonal18.HyperLink = null;
			this.txtSeasonal18.Left = 5.555555F;
			this.txtSeasonal18.Name = "txtSeasonal18";
			this.txtSeasonal18.Style = "font-family: \'Courier New\'";
			this.txtSeasonal18.Text = "X";
			this.txtSeasonal18.Top = 7.989583F;
			this.txtSeasonal18.Width = 0.1666667F;
			// 
			// txtWH18
			// 
			this.txtWH18.Height = 0.1979167F;
			this.txtWH18.Left = 5.885417F;
			this.txtWH18.Name = "txtWH18";
			this.txtWH18.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH18.Text = "9999999999.99";
			this.txtWH18.Top = 7.989583F;
			this.txtWH18.Width = 1.416667F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1979167F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0.04166667F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-family: \'Tahoma\'";
			this.Label53.Text = "r.";
			this.Label53.Top = 8.041667F;
			this.Label53.Width = 0.1666667F;
			// 
			// txtSSN19
			// 
			this.txtSSN19.Height = 0.1979167F;
			this.txtSSN19.Left = 2.1875F;
			this.txtSSN19.Name = "txtSSN19";
			this.txtSSN19.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN19.Text = "999999999";
			this.txtSSN19.Top = 8.322917F;
			this.txtSSN19.Width = 1.25F;
			// 
			// txtName19
			// 
			this.txtName19.CanGrow = false;
			this.txtName19.Height = 0.1979167F;
			this.txtName19.Left = 0.3229167F;
			this.txtName19.Name = "txtName19";
			this.txtName19.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName19.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName19.Top = 8.375F;
			this.txtName19.Width = 1.833333F;
			// 
			// txtWage19
			// 
			this.txtWage19.Height = 0.1979167F;
			this.txtWage19.Left = 3.868056F;
			this.txtWage19.Name = "txtWage19";
			this.txtWage19.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage19.Text = "9999999999.99";
			this.txtWage19.Top = 8.322917F;
			this.txtWage19.Width = 1.416667F;
			// 
			// txtSeasonal19
			// 
			this.txtSeasonal19.Height = 0.1979167F;
			this.txtSeasonal19.HyperLink = null;
			this.txtSeasonal19.Left = 5.555555F;
			this.txtSeasonal19.Name = "txtSeasonal19";
			this.txtSeasonal19.Style = "font-family: \'Courier New\'";
			this.txtSeasonal19.Text = "X";
			this.txtSeasonal19.Top = 8.322917F;
			this.txtSeasonal19.Width = 0.1666667F;
			// 
			// txtWH19
			// 
			this.txtWH19.Height = 0.1979167F;
			this.txtWH19.Left = 5.885417F;
			this.txtWH19.Name = "txtWH19";
			this.txtWH19.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH19.Text = "9999999999.99";
			this.txtWH19.Top = 8.322917F;
			this.txtWH19.Width = 1.416667F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1979167F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.04166667F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Tahoma\'";
			this.Label55.Text = "s.";
			this.Label55.Top = 8.375F;
			this.Label55.Width = 0.1666667F;
			// 
			// txtSSN20
			// 
			this.txtSSN20.Height = 0.1979167F;
			this.txtSSN20.Left = 2.1875F;
			this.txtSSN20.Name = "txtSSN20";
			this.txtSSN20.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN20.Text = "999999999";
			this.txtSSN20.Top = 8.65625F;
			this.txtSSN20.Width = 1.25F;
			// 
			// txtName20
			// 
			this.txtName20.CanGrow = false;
			this.txtName20.Height = 0.1979167F;
			this.txtName20.Left = 0.3229167F;
			this.txtName20.Name = "txtName20";
			this.txtName20.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName20.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName20.Top = 8.708333F;
			this.txtName20.Width = 1.833333F;
			// 
			// txtWage20
			// 
			this.txtWage20.Height = 0.1979167F;
			this.txtWage20.Left = 3.868056F;
			this.txtWage20.Name = "txtWage20";
			this.txtWage20.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage20.Text = "9999999999.99";
			this.txtWage20.Top = 8.65625F;
			this.txtWage20.Width = 1.416667F;
			// 
			// txtSeasonal20
			// 
			this.txtSeasonal20.Height = 0.1979167F;
			this.txtSeasonal20.HyperLink = null;
			this.txtSeasonal20.Left = 5.555555F;
			this.txtSeasonal20.Name = "txtSeasonal20";
			this.txtSeasonal20.Style = "font-family: \'Courier New\'";
			this.txtSeasonal20.Text = "X";
			this.txtSeasonal20.Top = 8.65625F;
			this.txtSeasonal20.Width = 0.1666667F;
			// 
			// txtWH20
			// 
			this.txtWH20.Height = 0.1979167F;
			this.txtWH20.Left = 5.885417F;
			this.txtWH20.Name = "txtWH20";
			this.txtWH20.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH20.Text = "9999999999.99";
			this.txtWH20.Top = 8.65625F;
			this.txtWH20.Width = 1.416667F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1979167F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0.04166667F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Tahoma\'";
			this.Label57.Text = "t.";
			this.Label57.Top = 8.708333F;
			this.Label57.Width = 0.1666667F;
			// 
			// txtSSN21
			// 
			this.txtSSN21.Height = 0.1979167F;
			this.txtSSN21.Left = 2.1875F;
			this.txtSSN21.Name = "txtSSN21";
			this.txtSSN21.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtSSN21.Text = "999999999";
			this.txtSSN21.Top = 8.989583F;
			this.txtSSN21.Width = 1.25F;
			// 
			// txtName21
			// 
			this.txtName21.CanGrow = false;
			this.txtName21.Height = 0.1979167F;
			this.txtName21.Left = 0.3229167F;
			this.txtName21.Name = "txtName21";
			this.txtName21.Style = "font-family: \'Courier New\'; font-size: 10pt";
			this.txtName21.Text = "XXXXXXXXXXXXXXXXXXX";
			this.txtName21.Top = 9.041667F;
			this.txtName21.Width = 1.833333F;
			// 
			// txtWage21
			// 
			this.txtWage21.Height = 0.1979167F;
			this.txtWage21.Left = 3.868056F;
			this.txtWage21.Name = "txtWage21";
			this.txtWage21.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWage21.Text = "9999999999.99";
			this.txtWage21.Top = 8.989583F;
			this.txtWage21.Width = 1.416667F;
			// 
			// txtSeasonal21
			// 
			this.txtSeasonal21.Height = 0.1979167F;
			this.txtSeasonal21.HyperLink = null;
			this.txtSeasonal21.Left = 5.555555F;
			this.txtSeasonal21.Name = "txtSeasonal21";
			this.txtSeasonal21.Style = "font-family: \'Courier New\'";
			this.txtSeasonal21.Text = "X";
			this.txtSeasonal21.Top = 8.989583F;
			this.txtSeasonal21.Width = 0.1666667F;
			// 
			// txtWH21
			// 
			this.txtWH21.Height = 0.1979167F;
			this.txtWH21.Left = 5.885417F;
			this.txtWH21.Name = "txtWH21";
			this.txtWH21.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH21.Text = "9999999999.99";
			this.txtWH21.Top = 8.989583F;
			this.txtWH21.Width = 1.416667F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1979167F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 0.04166667F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Tahoma\'";
			this.Label59.Text = "u.";
			this.Label59.Top = 9.041667F;
			this.Label59.Width = 0.1666667F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.3333333F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 6.166667F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "background-color: rgb(0,0,0); font-family: \'Tahoma\'; font-size: 9pt; font-weight:" +
    " bold; text-align: center";
			this.Label64.Text = "Income Tax Withholding";
			this.Label64.Top = 1.708333F;
			this.Label64.Width = 1.25F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.2916667F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 6.083333F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: center";
			this.Label65.Text = "Maine Income Tax Withheld in the Quarter";
			this.Label65.Top = 2.006944F;
			this.Label65.Width = 1.166667F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1666667F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.04166667F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-family: \'Tahoma\'; font-size: 7pt";
			this.Label66.Text = "17. Total of columns 15 and 16 on this page";
			this.Label66.Top = 9.34375F;
			this.Label66.Width = 2.1875F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1666667F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 0.04166667F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-family: \'Tahoma\'; font-size: 7pt";
			this.Label67.Text = "18. Total of columns 15 and 16 for ALL pages";
			this.Label67.Top = 9.677083F;
			this.Label67.Width = 2.3125F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1979167F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 2.197917F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "font-family: \'Tahoma\'";
			this.Label68.Text = "17a.";
			this.Label68.Top = 9.34375F;
			this.Label68.Width = 0.3333333F;
			// 
			// txtWagePageTotal
			// 
			this.txtWagePageTotal.Height = 0.1979167F;
			this.txtWagePageTotal.Left = 3.05625F;
			this.txtWagePageTotal.Name = "txtWagePageTotal";
			this.txtWagePageTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWagePageTotal.Text = "9999999999.99";
			this.txtWagePageTotal.Top = 9.34375F;
			this.txtWagePageTotal.Width = 1.416667F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1979167F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 2.197917F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Tahoma\'";
			this.Label69.Text = "18a.";
			this.Label69.Top = 9.677083F;
			this.Label69.Width = 0.3333333F;
			// 
			// txtWageTotal
			// 
			this.txtWageTotal.Height = 0.1979167F;
			this.txtWageTotal.Left = 3.05625F;
			this.txtWageTotal.Name = "txtWageTotal";
			this.txtWageTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWageTotal.Text = "9999999999.99";
			this.txtWageTotal.Top = 9.677083F;
			this.txtWageTotal.Width = 1.416667F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1979167F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 4.916667F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Tahoma\'";
			this.Label70.Text = "17b.";
			this.Label70.Top = 9.34375F;
			this.Label70.Width = 0.3333333F;
			// 
			// txtWHPageTotal
			// 
			this.txtWHPageTotal.Height = 0.1979167F;
			this.txtWHPageTotal.Left = 5.739583F;
			this.txtWHPageTotal.Name = "txtWHPageTotal";
			this.txtWHPageTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWHPageTotal.Text = "9999999999.99";
			this.txtWHPageTotal.Top = 9.34375F;
			this.txtWHPageTotal.Width = 1.416667F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1979167F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 4.916667F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-family: \'Tahoma\'";
			this.Label71.Text = "18b.";
			this.Label71.Top = 9.677083F;
			this.Label71.Width = 0.3333333F;
			// 
			// txtWHTotal
			// 
			this.txtWHTotal.Height = 0.1979167F;
			this.txtWHTotal.Left = 5.739583F;
			this.txtWHTotal.Name = "txtWHTotal";
			this.txtWHTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWHTotal.Text = "9999999999.99";
			this.txtWHTotal.Top = 9.677083F;
			this.txtWHTotal.Width = 1.416667F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 9.802083F;
			this.Line7.Visible = false;
			this.Line7.Width = 7.5F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 7.5F;
			this.Line7.Y1 = 9.802083F;
			this.Line7.Y2 = 9.802083F;
			// 
			// Barcode1
			// 
			this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.Barcode1.Height = 0.6875F;
			this.Barcode1.Left = 5.666667F;
			this.Barcode1.Name = "Barcode1";
			this.Barcode1.QuietZoneBottom = 0F;
			this.Barcode1.QuietZoneLeft = 0F;
			this.Barcode1.QuietZoneRight = 0F;
			this.Barcode1.QuietZoneTop = 0F;
			this.Barcode1.Top = 0F;
			this.Barcode1.Width = 1.666667F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1666667F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 7.25F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: right" +
    "";
			this.Label73.Text = "15";
			this.Label73.Top = 0.2083333F;
			this.Label73.Width = 0.25F;
			// 
			// txtPeriodStart
			// 
			this.txtPeriodStart.Height = 0.1666667F;
			this.txtPeriodStart.Left = 5.097222F;
			this.txtPeriodStart.Name = "txtPeriodStart";
			this.txtPeriodStart.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtPeriodStart.Text = "99 99";
			this.txtPeriodStart.Top = 1.166667F;
			this.txtPeriodStart.Width = 0.9375F;
			// 
			// txtPeriodEnd
			// 
			this.txtPeriodEnd.Height = 0.1666667F;
			this.txtPeriodEnd.Left = 6.402778F;
			this.txtPeriodEnd.Name = "txtPeriodEnd";
			this.txtPeriodEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtPeriodEnd.Text = "99 99";
			this.txtPeriodEnd.Top = 1.163194F;
			this.txtPeriodEnd.Width = 0.9166667F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1666667F;
			this.Field25.Left = 5.951389F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; vertical-align:" +
    " middle";
			this.Field25.Text = "-";
			this.Field25.Top = 1.166667F;
			this.Field25.Visible = false;
			this.Field25.Width = 0.25F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.479167F;
			this.Line3.Width = 7.5F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.5F;
			this.Line3.Y1 = 1.479167F;
			this.Line3.Y2 = 1.479167F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1666667F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 5.885417F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Tahoma\'; font-size: 7pt; text-align: right";
			this.Label75.Text = "17.";
			this.Label75.Top = 2.125F;
			this.Label75.Width = 0.2291667F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1666667F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Courier New\'";
			this.Label74.Text = "TSC";
			this.Label74.Top = 9.9375F;
			this.Label74.Visible = false;
			this.Label74.Width = 0.4166667F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 9.229167F;
			this.Line2.Visible = false;
			this.Line2.Width = 7.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 9.229167F;
			this.Line2.Y2 = 9.229167F;
			// 
			// txtWH1
			// 
			this.txtWH1.Height = 0.1979167F;
			this.txtWH1.Left = 5.885417F;
			this.txtWH1.Name = "txtWH1";
			this.txtWH1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtWH1.Text = "9999999999.99";
			this.txtWH1.Top = 2.322917F;
			this.txtWH1.Width = 1.416667F;
			// 
			// Shape17
			// 
			this.Shape17.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape17.Height = 0.125F;
			this.Shape17.Left = 0.01388889F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 0.01388889F;
			this.Shape17.Width = 0.125F;
			// 
			// Shape18
			// 
			this.Shape18.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape18.Height = 0.125F;
			this.Shape18.Left = 0.01041667F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 9.875F;
			this.Shape18.Width = 0.125F;
			// 
			// Shape19
			// 
			this.Shape19.BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.Shape19.Height = 0.125F;
			this.Shape19.Left = 7.361111F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 9.875F;
			this.Shape19.Width = 0.125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 6pt; vertical-align: middle";
			this.Label11.Text = "Period Covered";
			this.Label11.Top = 1.166667F;
			this.Label11.Width = 0.6666667F;
			// 
			// txtYearStart
			// 
			this.txtYearStart.Height = 0.1666667F;
			this.txtYearStart.Left = 5.6875F;
			this.txtYearStart.Name = "txtYearStart";
			this.txtYearStart.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtYearStart.Text = "9999";
			this.txtYearStart.Top = 1.166667F;
			this.txtYearStart.Width = 0.5F;
			// 
			// txtYearEnd
			// 
			this.txtYearEnd.Height = 0.1666667F;
			this.txtYearEnd.Left = 6.958333F;
			this.txtYearEnd.Name = "txtYearEnd";
			this.txtYearEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" +
    "ddle";
			this.txtYearEnd.Text = "9999";
			this.txtYearEnd.Top = 1.166667F;
			this.txtYearEnd.Width = 0.4375F;
			// 
			// srptC1Part4
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWage21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonal21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWagePageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWHPageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWHTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWH1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWage21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSeasonal21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWagePageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWHPageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWHTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWH1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearEnd;
	}
}
