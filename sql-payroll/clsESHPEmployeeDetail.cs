﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsESHPEmployeeDetail
	{
		//=========================================================
		private string strSSN = string.Empty;
		private string strFirstName = string.Empty;
		private string strMiddleInitial = string.Empty;
		private string strLastName = string.Empty;
		private string strLegalFirstName = string.Empty;
		private string strLegalMiddleName = string.Empty;
		private string strLegalLastName = string.Empty;
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZipCode = string.Empty;
		private string strHomePhone = string.Empty;
		private string strCellPhone = "";
		private string strWorkPhone = "";
		private string strPagerNumber = "";
		private string strFaxNumber = string.Empty;
		private string strEmail = string.Empty;
		private string strMaritalStatus = string.Empty;
		private string strMainPosition = string.Empty;
		private string strOrganizationNumber = "";
		private string strLeaveClassLink = string.Empty;

		public string FirstName
		{
			set
			{
				strFirstName = value;
			}
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
		}

		public string MiddleInitial
		{
			set
			{
				strMiddleInitial = value;
			}
			get
			{
				string MiddleInitial = "";
				MiddleInitial = strMiddleInitial;
				return MiddleInitial;
			}
		}

		public string LastName
		{
			set
			{
				strLastName = value;
			}
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
		}

		public string LegalFirstName
		{
			set
			{
				strLegalFirstName = value;
			}
			get
			{
				string LegalFirstName = "";
				LegalFirstName = strLegalFirstName;
				return LegalFirstName;
			}
		}

		public string LegalMiddleName
		{
			set
			{
				strLegalMiddleName = value;
			}
			get
			{
				string LegalMiddleName = "";
				LegalMiddleName = strLegalMiddleName;
				return LegalMiddleName;
			}
		}

		public string LegalLastName
		{
			set
			{
				strLegalLastName = value;
			}
			get
			{
				string LegalLastName = "";
				LegalLastName = strLegalLastName;
				return LegalLastName;
			}
		}

		public string Address1
		{
			set
			{
				strAddress1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddress2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string ZipCode
		{
			set
			{
				strZipCode = value;
			}
			get
			{
				string ZipCode = "";
				ZipCode = strZipCode;
				return ZipCode;
			}
		}

		public string HomePhone
		{
			set
			{
				strHomePhone = value;
			}
			get
			{
				string HomePhone = "";
				HomePhone = strHomePhone;
				return HomePhone;
			}
		}

		public string FaxNumber
		{
			set
			{
				strFaxNumber = value;
			}
			get
			{
				string FaxNumber = "";
				FaxNumber = strFaxNumber;
				return FaxNumber;
			}
		}

		public string Email
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
		}

		public string MainPosition
		{
			set
			{
				strMainPosition = value;
			}
			get
			{
				string MainPosition = "";
				MainPosition = strMainPosition;
				return MainPosition;
			}
		}

		public string MaritalStatus
		{
			set
			{
				strMaritalStatus = value;
			}
			get
			{
				string MaritalStatus = "";
				MaritalStatus = strMaritalStatus;
				return MaritalStatus;
			}
		}

		public string LeaveClassLink
		{
			set
			{
				strLeaveClassLink = value;
			}
			get
			{
				string LeaveClassLink = "";
				LeaveClassLink = strLeaveClassLink;
				return LeaveClassLink;
			}
		}

		public string SocialSecurityNumber
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SocialSecurityNumber = "";
				SocialSecurityNumber = strSSN;
				return SocialSecurityNumber;
			}
		}
	}
}
