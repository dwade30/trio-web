﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsCalcShifts
	{
		//=========================================================
		public bool IsUnworkedOrSick(DateTime dtDate, ref string strEmployeeNumber)
		{
			bool IsUnworkedOrSick = false;
			clsScheduleDay tDay = new clsScheduleDay();
			tDay.LoadEmployeeDay(dtDate, ref strEmployeeNumber);
			IsUnworkedOrSick = tDay.HasUnworkedOrSick();
			return IsUnworkedOrSick;
		}

		public bool CalcShift(clsWorkShift tShift, ref clsShiftType tSType, ref clsShiftTypeList ShiftList, ref clsPayTotalsByCat tTotals, ref double dblOvertimeStart, DateTime dtWorkDate, ref clsHolidaysList tHolidays, ref double dblScheduledRegular)
		{
			bool CalcShift = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				double dblSHours;
				double dblSOHours;
				double dblSRHours;
				double dblAHours;
				double dblAOHours = 0;
				double dblARHours;
				clsShiftPayLines tPayLine;
				// Dim tTot As New clsPayTotalsByCat
				DateTime dtStartDate;
				DateTime dtEndDate;
				double[] arySHours = new double[2 + 1];
				double[] aryAHours = new double[2 + 1];
				// vbPorter upgrade warning: dblTemp As double	OnWrite(long, string, double, int)
				double dblTemp = 0;
				// vbPorter upgrade warning: dtStartTemp As DateTime	OnWrite(string, DateTime)
				DateTime dtStartTemp;
				// vbPorter upgrade warning: dtEndTemp As DateTime	OnWrite(string)
				DateTime dtEndTemp;
				double dblCurTot;
				clsShiftType tempSType;
				clsPayCatTotal tCat;
				double dblTotalCourt;
				int lngCourtPayCat = 0;
				int lngRegCourtPayCat;
				int lngOTCourtPayCat;
				dblTotalCourt = 0;
				// hardcoded for penobscot county
				lngRegCourtPayCat = 13;
				lngOTCourtPayCat = 35;
				tempSType = tSType;
				CalcShift = false;
				tTotals.ClearShiftTotals();
				dtStartDate = dtWorkDate;
				dtEndDate = dtStartDate;
				// tTot.LoadCats
				if (Information.IsDate(tShift.ScheduledStart) && Information.IsDate(tShift.ScheduledEnd))
				{
					dblTemp = fecherFoundation.DateAndTime.DateDiff("n", tShift.ScheduledStart, tShift.ScheduledEnd);
					dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 60, "0.00"));
					if (dblTemp > 0)
					{
						arySHours[0] = dblTemp;
						arySHours[1] = 0;
					}
					else
					{
						// spans two days
						dtEndDate = fecherFoundation.DateAndTime.DateAdd("d", 1, dtStartDate);
						dtStartTemp = FCConvert.ToDateTime(Strings.Format(dtStartDate, "MM/dd/yyyy") + " " + tShift.ScheduledStart);
						dtEndTemp = FCConvert.ToDateTime(Strings.Format(dtEndDate, "MM/dd/yyyy") + " 12:00 AM");
						dblTemp = fecherFoundation.DateAndTime.DateDiff("n", dtStartTemp, dtEndTemp);
						dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 60, "0.00"));
						arySHours[0] = dblTemp;
						dtStartTemp = dtEndTemp;
						dtEndTemp = FCConvert.ToDateTime(Strings.Format(dtEndDate, "MM/dd/yyyy") + " " + tShift.ScheduledEnd);
						dblTemp = fecherFoundation.DateAndTime.DateDiff("n", dtStartTemp, dtEndTemp);
						dblTemp /= 60;
						dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp, "0.00"));
						arySHours[1] = dblTemp;
					}
					if (arySHours[0] >= tShift.LunchTime)
					{
						arySHours[0] -= tShift.LunchTime;
					}
					else if (arySHours[1] >= tShift.LunchTime)
					{
						arySHours[1] -= tShift.LunchTime;
					}
					else
					{
						arySHours[0] -= tShift.LunchTime;
						arySHours[1] += arySHours[0];
						arySHours[0] = 0;
						if (arySHours[1] < 0)
							arySHours[1] = 0;
					}
				}
				else
				{
					arySHours[0] = 0;
					arySHours[1] = 0;
				}
				dtEndDate = dtStartDate;
				if (Information.IsDate(tShift.ActualStart) && Information.IsDate(tShift.ActualEnd))
				{
					dblTemp = fecherFoundation.DateAndTime.DateDiff("n", tShift.ActualStart, tShift.ActualEnd);
					dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 60, "0.00"));
					if (dblTemp > 0)
					{
						aryAHours[0] = dblTemp;
						aryAHours[1] = 0;
					}
					else
					{
						// spans two days
						dtEndDate = fecherFoundation.DateAndTime.DateAdd("d", 1, dtStartDate);
						dtStartTemp = FCConvert.ToDateTime(Strings.Format(dtStartDate, "MM/dd/yyyy") + " " + tShift.ActualStart);
						dtEndTemp = FCConvert.ToDateTime(Strings.Format(dtEndDate, "MM/dd/yyyy") + " 12:00 AM");
						dblTemp = fecherFoundation.DateAndTime.DateDiff("n", dtStartTemp, dtEndTemp);
						dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 60, "0.00"));
						aryAHours[0] = dblTemp;
						dtStartTemp = dtEndTemp;
						dtEndTemp = FCConvert.ToDateTime(Strings.Format(dtEndDate, "MM/dd/yyyy") + " " + tShift.ActualEnd);
						dblTemp = fecherFoundation.DateAndTime.DateDiff("n", dtStartTemp, dtEndTemp);
						dblTemp /= 60;
						dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp, "0.00"));
						aryAHours[1] = dblTemp;
					}
					if (aryAHours[0] >= tShift.LunchTime)
					{
						aryAHours[0] -= tShift.LunchTime;
					}
					else if (aryAHours[1] >= tShift.LunchTime)
					{
						aryAHours[1] -= tShift.LunchTime;
					}
					else
					{
						aryAHours[0] -= tShift.LunchTime;
						aryAHours[1] += aryAHours[0];
						aryAHours[0] = 0;
						if (aryAHours[1] < 0)
							aryAHours[1] = 0;
					}
				}
				else
				{
					aryAHours[0] = 0;
					aryAHours[1] = 0;
				}
				if (tShift.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Court))
				{
					if (aryAHours[0] + aryAHours[1] > 0)
					{
						if (1.5 * (aryAHours[0] + aryAHours[1]) < 4)
						{
							dblTotalCourt = 4;
							lngCourtPayCat = lngRegCourtPayCat;
						}
						else
						{
							dblTotalCourt = aryAHours[0] + aryAHours[1];
							lngCourtPayCat = lngOTCourtPayCat;
						}
					}
					tCat = new clsPayCatTotal();
					tCat.PayCat = lngRegCourtPayCat;
					tCat.ActualHours = aryAHours[0] + aryAHours[1];
					tCat.DayActualHours = aryAHours[0] + aryAHours[1];
					tCat.ShiftActualHours = aryAHours[0] + aryAHours[1];
					tCat.Set_BreakdownActualHours(tShift.Account, aryAHours[0] + aryAHours[1]);
					tCat.Set_BreakdownDayActualHours(tShift.Account, aryAHours[0] + aryAHours[1]);
					tCat.Set_BreakdownShiftActualHours(tShift.Account, aryAHours[0] + aryAHours[1]);
					tTotals.AddToCat(tCat);
					tCat = new clsPayCatTotal();
					tCat.PayCat = lngCourtPayCat;
					tCat.AmountPaid = dblTotalCourt;
					tCat.Set_BreakdownAmountPaid(tShift.Account, dblTotalCourt);
					tTotals.AddToCat(tCat);
					CalcShift = true;
					return CalcShift;
				}
				int X;
				DateTime[] dtTemp = new DateTime[2 + 1];
				bool boolIsAHoliday = false;
				clsHoliday tHday;
				// vbPorter upgrade warning: intPayCatToUse As int	OnWriteFCConvert.ToInt32(
				int intPayCatToUse = 0;
				double dblFactorUnits = 0;
				double dblAfter = 0;
				double dblUpto = 0;
				double dblMax = 0;
				double dblRegularToAdd = 0;
				dtTemp[0] = dtStartDate;
				dtTemp[1] = dtEndDate;
				for (X = 0; X <= 1; X++)
				{
					if (aryAHours[X] > 0)
					{
						boolIsAHoliday = tHolidays.IsAHoliday(dtTemp[X]);
						if (boolIsAHoliday && tSType.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.Regular))
						{
							tHday = tHolidays.GetHolidayByDate(dtTemp[X]);
							if (!(tHday == null))
							{
								if (tHday.ShiftType != 0)
								{
									// if assigned a shift type
									tSType = ShiftList.GetShiftTypeByType(tHday.ShiftType);
								}
								else
								{
									MessageBox.Show("Shift type not found for holiday " + tHday.HolidayName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return CalcShift;
								}
							}
						}
						else
						{
							tSType = tempSType;
						}
						tSType.MoveFirst();
						// loop through every rule for this shift
						while (tSType.GetCurrentIndex >= 0)
						{
							tPayLine = tSType.GetCurrentBreakdown();
							if (!(tPayLine == null))
							{
								intPayCatToUse = tPayLine.PayCategory;
								if (tSType.OvertimePayCatID > 0)
								{
									switch (tPayLine.PayCategory)
									{
										case modCoreysSweeterCode.CNSTPAYCATREGULAR:
											{
												dblRegularToAdd = arySHours[X];
												if (!boolIsAHoliday)
												{
													// check for OT
													if (!(tTotals.Get_CategoryTotals(tTotals.RegularIndex) == null))
													{
														if (tTotals.Get_CategoryTotals(tTotals.RegularIndex).ActualHours + aryAHours[X] > dblOvertimeStart && dblOvertimeStart > 0)
														{
															if (tTotals.Get_CategoryTotals(tTotals.RegularIndex).ActualHours < dblOvertimeStart)
															{
																dblAOHours = aryAHours[X] - (dblOvertimeStart - tTotals.Get_CategoryTotals(tTotals.RegularIndex).ActualHours);
																dblARHours = aryAHours[X] - dblAOHours;
															}
															else
															{
																dblARHours = 0;
																dblAOHours = aryAHours[X];
															}
														}
													}
													else
													{
														dblAOHours = 0;
														dblARHours = aryAHours[X];
													}
												}
												else
												{
													dblAOHours = 0;
													dblARHours = aryAHours[X];
												}
												break;
											}
										default:
											{
												dblAOHours = 0;
												dblARHours = aryAHours[X];
												break;
											}
									}
									//end switch
									intPayCatToUse = tSType.OvertimePayCatID;
								}
								else
								{
									dblAOHours = 0;
									dblARHours = aryAHours[X];
									if (tPayLine.PayCategory == modCoreysSweeterCode.CNSTPAYCATREGULAR)
									{
										dblRegularToAdd = arySHours[X];
									}
								}
								dblAfter = tPayLine.AfterUnits;
								dblUpto = tPayLine.UpToUnits;
								dblMax = tPayLine.MaxUnits;
								if (dblAfter < aryAHours[X])
								{
									switch (tPayLine.FactorOrUnits)
									{
										case modSchedule.ShiftFactorType.Factor:
											{
												dblFactorUnits = tPayLine.FactorUnits;
												if (dblUpto <= aryAHours[X] && dblUpto > 0)
												{
													dblTemp = dblUpto - dblAfter;
												}
												else
												{
													dblTemp = aryAHours[X] - dblAfter;
												}
												dblTemp *= dblFactorUnits;
												// this is how many hours/dollars
												if (dblMax > 0)
												{
													switch (tPayLine.MaxType)
													{
														case modSchedule.ShiftPerType.PerDay:
															{
																// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours + dblTemp Then
																if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours + dblTemp)
																{
																	// If tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours < dblMax Then
																	if (tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours < dblMax)
																	{
																		// dblTemp = dblMax - tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours
																		dblTemp = dblMax - tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours;
																	}
																	else
																	{
																		dblTemp = 0;
																	}
																}
																break;
															}
														case modSchedule.ShiftPerType.PerShift:
															{
																// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).ShiftActualHours Then
																if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).ShiftActualHours)
																{
																	dblTemp = dblMax;
																}
																break;
															}
														case modSchedule.ShiftPerType.PerPayPeriod:
														case modSchedule.ShiftPerType.PerWeek:
															{
																// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).ActualHours + dblTemp Then
																if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours + dblTemp)
																{
																	// If tTot.CategoryTotals(tPayLine.PayCategory).ActualHours < dblMax Then
																	if (tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours < dblMax)
																	{
																		// dblTemp = dblMax - tTot.CategoryTotals(tPayLine.PayCategory).ActualHours
																		dblTemp = dblMax - tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours;
																	}
																	else
																	{
																		dblTemp = 0;
																	}
																}
																break;
															}
													}
													//end switch
												}
												tCat = new clsPayCatTotal();
												if (dblAOHours > 0)
												{
													tCat.PayCat = intPayCatToUse;
													tCat.ActualHours = dblAOHours;
													tCat.AmountPaid = dblAOHours;
													tCat.DayActualHours = dblAOHours;
													tCat.ShiftActualHours = dblAOHours;
													// tTot.AddToCat (tCat)
													tCat.ScheduledHours = arySHours[X];
													tCat.Set_BreakdownActualHours(tShift.Account, dblAOHours);
													tCat.Set_BreakdownAmountPaid(tShift.Account, dblAOHours);
													tCat.Set_BreakdownDayActualHours(tShift.Account, dblAOHours);
													tCat.Set_BreakdownShiftActualHours(tShift.Account, dblAOHours);
													tTotals.AddToCat(tCat);
													tCat.PayCat = tPayLine.PayCategory;
													tCat.AmountPaid = dblTemp - dblAOHours;
													tCat.ActualHours = dblTemp - dblAOHours;
													tCat.Set_BreakdownAmountPaid(tShift.Account, dblTemp - dblAOHours);
													tCat.Set_BreakdownActualHours(tShift.Account, dblTemp - dblAOHours);
													if (tCat.ActualHours > 0)
													{
														// Call tTot.AddToCat(tCat)
														tTotals.AddToCat(tCat);
													}
												}
												else
												{
													tCat.PayCat = tPayLine.PayCategory;
													tCat.ScheduledHours = arySHours[X];
													tCat.ActualHours = dblTemp;
													tCat.AmountPaid = dblTemp;
													tCat.DayActualHours = dblTemp;
													tCat.ShiftActualHours = dblTemp;
													tCat.Set_BreakdownScheduledHours(tShift.Account, arySHours[X]);
													tCat.Set_BreakdownActualHours(tShift.Account, dblTemp);
													tCat.Set_BreakdownAmountPaid(tShift.Account, dblTemp);
													tCat.Set_BreakdownDayActualHours(tShift.Account, dblTemp);
													tCat.Set_BreakdownShiftActualHours(tShift.Account, dblTemp);
													// Call tTot.AddToCat(tCat)
													tTotals.AddToCat(tCat);
												}
												break;
											}
										case modSchedule.ShiftFactorType.Units:
											{
												if (X == 0 || dblAfter > 0)
												{
													// if dblafter > 0 then must check to see if this gets applied
													if (!(X == 1 && (aryAHours[0] >= dblAfter || aryAHours[0] + aryAHours[1] < dblAfter)) && !(X == 0 && aryAHours[0] < dblAfter))
													{
														dblFactorUnits = tPayLine.FactorUnits;
														if (dblMax > 0)
														{
															switch (tPayLine.MaxType)
															{
																case modSchedule.ShiftPerType.PerDay:
																	{
																		// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours + dblFactorUnits Then
																		if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours + dblFactorUnits)
																		{
																			// If tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours < dblMax Then
																			if (tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours < dblMax)
																			{
																				// dblFactorUnits = dblMax - tTot.CategoryTotals(tPayLine.PayCategory).DayActualHours
																				dblFactorUnits = dblMax - tTotals.Get_CategoryTotals(tPayLine.PayCategory).DayActualHours;
																			}
																			else
																			{
																				dblFactorUnits = 0;
																			}
																		}
																		break;
																	}
																case modSchedule.ShiftPerType.PerShift:
																	{
																		// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).ShiftActualHours Then
																		if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).ShiftActualHours)
																		{
																			dblFactorUnits = dblMax;
																		}
																		break;
																	}
																case modSchedule.ShiftPerType.PerPayPeriod:
																case modSchedule.ShiftPerType.PerWeek:
																	{
																		// If dblMax < tTot.CategoryTotals(tPayLine.PayCategory).ActualHours + dblFactorUnits Then
																		if (dblMax < tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours + dblFactorUnits)
																		{
																			// If tTot.CategoryTotals(tPayLine.PayCategory).ActualHours < dblMax Then
																			if (tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours < dblMax)
																			{
																				// dblFactorUnits = dblMax - tTot.CategoryTotals(tPayLine.PayCategory).ActualHours
																				dblFactorUnits = dblMax - tTotals.Get_CategoryTotals(tPayLine.PayCategory).ActualHours;
																			}
																			else
																			{
																				dblFactorUnits = 0;
																			}
																		}
																		break;
																	}
															}
															//end switch
														}
														tCat = new clsPayCatTotal();
														tCat.PayCat = tPayLine.PayCategory;
														tCat.ActualHours = dblFactorUnits;
														tCat.AmountPaid = dblFactorUnits;
														tCat.DayActualHours = dblFactorUnits;
														tCat.ShiftActualHours = dblFactorUnits;
														tCat.ScheduledHours = arySHours[X];
														tCat.Set_BreakdownActualHours(tShift.Account, dblFactorUnits);
														tCat.Set_BreakdownAmountPaid(tShift.Account, dblFactorUnits);
														tCat.Set_BreakdownDayActualHours(tShift.Account, dblFactorUnits);
														tCat.Set_BreakdownShiftActualHours(tShift.Account, dblFactorUnits);
														tCat.Set_BreakdownScheduledHours(tShift.Account, arySHours[X]);
														// Call tTot.AddToCat(tCat)
														tTotals.AddToCat(tCat);
													}
												}
												break;
											}
									}
									//end switch
								}
							}
							tSType.MoveNext();
						}
						dblScheduledRegular += dblRegularToAdd;
					}
				}
				// X
				CalcShift = true;
				return CalcShift;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CalcShift", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalcShift;
		}
	}
}
