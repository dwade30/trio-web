//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW3Information.
	/// </summary>
	partial class frmW3Information
	{
		public fecherFoundation.FCComboBox cmbNoneApply;
		public fecherFoundation.FCLabel lblNoneApply;
		public fecherFoundation.FCComboBox cmbKindOfPayer;
		public fecherFoundation.FCLabel lblKindOfPayer;
		public fecherFoundation.FCCheckBox chkThirdPartySickPay;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtContactPerson;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtPhoneNumber;
		public fecherFoundation.FCTextBox txtFaxNumber;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbNoneApply = new fecherFoundation.FCComboBox();
            this.lblNoneApply = new fecherFoundation.FCLabel();
            this.cmbKindOfPayer = new fecherFoundation.FCComboBox();
            this.lblKindOfPayer = new fecherFoundation.FCLabel();
            this.chkThirdPartySickPay = new fecherFoundation.FCCheckBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtContactPerson = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txtPhoneNumber = new fecherFoundation.FCTextBox();
            this.txtFaxNumber = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkThirdPartySickPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 505);
            this.BottomPanel.Size = new System.Drawing.Size(535, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkThirdPartySickPay);
            this.ClientArea.Controls.Add(this.cmbNoneApply);
            this.ClientArea.Controls.Add(this.lblNoneApply);
            this.ClientArea.Controls.Add(this.cmbKindOfPayer);
            this.ClientArea.Controls.Add(this.lblKindOfPayer);
            this.ClientArea.Controls.Add(this.Frame7);
            this.ClientArea.Size = new System.Drawing.Size(535, 445);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(535, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "W3 Information";
            // 
            // cmbNoneApply
            // 
            this.cmbNoneApply.AutoSize = false;
            this.cmbNoneApply.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbNoneApply.FormattingEnabled = true;
            this.cmbNoneApply.Items.AddRange(new object[] {
            "None Apply",
            "State/Local non 501c",
            "501c non-govt",
            "State/local 501c",
            "Federal govt"});
            this.cmbNoneApply.Location = new System.Drawing.Point(219, 330);
            this.cmbNoneApply.Name = "cmbNoneApply";
            this.cmbNoneApply.Size = new System.Drawing.Size(298, 40);
            this.cmbNoneApply.TabIndex = 17;
            // 
            // lblNoneApply
            // 
            this.lblNoneApply.AutoSize = true;
            this.lblNoneApply.Location = new System.Drawing.Point(30, 344);
            this.lblNoneApply.Name = "lblNoneApply";
            this.lblNoneApply.Size = new System.Drawing.Size(129, 15);
            this.lblNoneApply.TabIndex = 18;
            this.lblNoneApply.Text = "KIND OF EMPLOYER";
            // 
            // cmbKindOfPayer
            // 
            this.cmbKindOfPayer.AutoSize = false;
            this.cmbKindOfPayer.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbKindOfPayer.FormattingEnabled = true;
            this.cmbKindOfPayer.Items.AddRange(new object[] {
            "941",
            "CT - 1",
            "Military",
            "Hshld Emp.",
            "943",
            "Medicare Govt Emp",
            "944"});
            this.cmbKindOfPayer.Location = new System.Drawing.Point(219, 270);
            this.cmbKindOfPayer.Name = "cmbKindOfPayer";
            this.cmbKindOfPayer.Size = new System.Drawing.Size(298, 40);
            this.cmbKindOfPayer.TabIndex = 19;
            // 
            // lblKindOfPayer
            // 
            this.lblKindOfPayer.AutoSize = true;
            this.lblKindOfPayer.Location = new System.Drawing.Point(30, 284);
            this.lblKindOfPayer.Name = "lblKindOfPayer";
            this.lblKindOfPayer.Size = new System.Drawing.Size(103, 15);
            this.lblKindOfPayer.TabIndex = 20;
            this.lblKindOfPayer.Text = "KIND OF PAYER";
            // 
            // chkThirdPartySickPay
            // 
            this.chkThirdPartySickPay.Location = new System.Drawing.Point(30, 394);
            this.chkThirdPartySickPay.Name = "chkThirdPartySickPay";
            this.chkThirdPartySickPay.Size = new System.Drawing.Size(170, 27);
            this.chkThirdPartySickPay.TabIndex = 16;
            this.chkThirdPartySickPay.Text = "Third-party sick pay";
            // 
            // Frame7
            // 
            this.Frame7.AppearanceKey = "groupBoxNoBorders";
            this.Frame7.Controls.Add(this.txtContactPerson);
            this.Frame7.Controls.Add(this.txtEmail);
            this.Frame7.Controls.Add(this.txtPhoneNumber);
            this.Frame7.Controls.Add(this.txtFaxNumber);
            this.Frame7.Controls.Add(this.Label9);
            this.Frame7.Controls.Add(this.Label10);
            this.Frame7.Controls.Add(this.Label13);
            this.Frame7.Controls.Add(this.Label14);
            this.Frame7.Location = new System.Drawing.Point(10, 0);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(507, 250);
            this.Frame7.TabIndex = 17;
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.AutoSize = false;
            this.txtContactPerson.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactPerson.LinkItem = null;
            this.txtContactPerson.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactPerson.LinkTopic = null;
            this.txtContactPerson.Location = new System.Drawing.Point(209, 30);
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(298, 40);
            this.txtContactPerson.TabIndex = 0;
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.LinkItem = null;
            this.txtEmail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmail.LinkTopic = null;
            this.txtEmail.Location = new System.Drawing.Point(209, 90);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(298, 40);
            this.txtEmail.TabIndex = 1;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.AutoSize = false;
            this.txtPhoneNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhoneNumber.LinkItem = null;
            this.txtPhoneNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPhoneNumber.LinkTopic = null;
            this.txtPhoneNumber.Location = new System.Drawing.Point(209, 150);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(298, 40);
            this.txtPhoneNumber.TabIndex = 2;
            // 
            // txtFaxNumber
            // 
            this.txtFaxNumber.AutoSize = false;
            this.txtFaxNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFaxNumber.LinkItem = null;
            this.txtFaxNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFaxNumber.LinkTopic = null;
            this.txtFaxNumber.Location = new System.Drawing.Point(209, 210);
            this.txtFaxNumber.Name = "txtFaxNumber";
            this.txtFaxNumber.Size = new System.Drawing.Size(298, 40);
            this.txtFaxNumber.TabIndex = 3;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(110, 18);
            this.Label9.TabIndex = 21;
            this.Label9.Text = "CONTACT PERSON";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 104);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(108, 18);
            this.Label10.TabIndex = 20;
            this.Label10.Text = "EMAIL ADDRESS";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 164);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(140, 18);
            this.Label13.TabIndex = 19;
            this.Label13.Text = "TELEPHONE NUMBER";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 224);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(98, 18);
            this.Label14.TabIndex = 18;
            this.Label14.Text = "FAX NUMBER";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(208, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // frmW3Information
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(535, 613);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmW3Information";
            this.Text = "W3 Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmW3Information_Load);
            this.Activated += new System.EventHandler(this.frmW3Information_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW3Information_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkThirdPartySickPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}