﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Diagnostics;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using SharedApplication.Telemetry;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEmployeeSearch : BaseForm
	{
        private readonly ITelemetryService _telemetryService;

        public frmEmployeeSearch()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }
        public frmEmployeeSearch(ITelemetryService telemetryService) : this()
		{
            _telemetryService = telemetryService;
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            if (modGlobalVariables.Statics.gboolEmployeeTypeNumeric)
            {
                this.txtEmployeeNumber.AllowOnlyNumericInput();
            }
            txtSeqNumber.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',45, 49..57");
            this.txtSSN.Mask = "999-99-9999";
        }


        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmEmployeeSearch InstancePtr
		{
			get
			{
				return (frmEmployeeSearch)Sys.GetInstance(typeof(frmEmployeeSearch));
			}
		}

		protected frmEmployeeSearch _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 02,2001
		//
		// NOTES: Left columns 6-8 in as if future columns are desired to
		// to be added then code would already exist. Simply change field names.
		//
		// MODIFIED BY: DAN C. SOLTESZ
		// DATE: May 28, 2001
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsSearch = new clsDRWrapper();
		private int intCounter;
		private int intDataChanged;
		private bool gboolFormLoading;
		// vbPorter upgrade warning: intSortColumn As int	OnWriteFCConvert.ToInt32(
		private int intSortColumn;
		private bool boolASC;
		private bool pboolDoubleClick;
		
		private bool boolDontUpdateGlobal;
		private int lngEmployeeID;
		private cEmployeeService empServ = new cEmployeeService();
        private System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
        private SSNViewType ssnViewPermission = SSNViewType.None;
        public int Init(bool boolDontChangeCurrentEmployee)
		{
			int Init = 0;
			boolDontUpdateGlobal = boolDontChangeCurrentEmployee;
			lngEmployeeID = 0;
            this.Show(FCForm.FormShowEnum.Modal);
			Init = lngEmployeeID;
			return Init;
		}

		private void cboType_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cboType_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// record that this form is now dirty and there is data to save
				intDataChanged = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		//private void cmdDelete_Click()
		//{
		//	try
		//	{
		//		// On Error GoTo CallErrorRoutine
		//		fecherFoundation.Information.Err().Clear();
		//		modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
		//		clsMousePointer Mouse;
		//		Mouse = new clsMousePointer();
		//		// Delete the field length information from the database
		//		if (MessageBox.Show("This action will delete Field length infomation. Continue?", "Payroll Field Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
		//		{
		//			rsSearch.Execute("Delete from tblEmployeeMaster", "Payroll");
		//			cmdNew_Click();
		//			MessageBox.Show("Delete of record was successful", "Payroll Field Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		// CallErrorRoutine:
		//		modErrorHandler.SetErrorHandler(ex);
		//		return;
		//	}
		//}

		private void chkShowActive_CheckedChanged(object sender, System.EventArgs e)
		{
			int lngRow;
			if (chkShowActive.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (lngRow = 1; lngRow <= vsSearch.Rows - 1; lngRow++)
				{
					if (fecherFoundation.Strings.LCase(vsSearch.TextMatrix(lngRow, 10)) != "active")
					{
						vsSearch.RowHidden(lngRow, true);
					}
				}
			}
			else
			{
				for (lngRow = 1; lngRow <= vsSearch.Rows - 1; lngRow++)
				{
					vsSearch.RowHidden(lngRow, false);
				}
			}
		}

		public void chkShowActive_Click()
		{
			chkShowActive_CheckedChanged(chkShowActive, new System.EventArgs());
		}

		private void cmdAddNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdAddNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				foreach (FCForm frm in FCGlobal.Statics.Forms)
				{
					if (frm.Name == "frmEmployeeMaster")
					{
						MessageBox.Show("Employee Master screen must be closed before a new employee can be added.", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);
						return;
					}
				}
				Close();
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(-1);
				frmEmployeeMaster.InstancePtr.Show(App.MainForm);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdAddNew_Click()
		{
			cmdAddNew_Click(cmdAddNew, new System.EventArgs());
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdClear_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// CLEAR OUT THE CONTROLS BUT DO NOTHING WITH THE DATABASE.
				vsSearch.Rows = 1;
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						// This means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCTextBox).Text = string.Empty;
						}
					}
					else if (ControlName is MaskedTextBox)
					{
						// This means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as MaskedTextBox).Mask = string.Empty;
						}
					}
					else if (ControlName is FCComboBox)
					{
						// This means that we never want to clear out this control
						if (FCConvert.ToString(ControlName.Tag) != "KEEP")
						{
							(ControlName as FCComboBox).Text = string.Empty;
						}
					}
					ControlName = null;
				}
				txtEmployeeNumber.Focus();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Employee Search/Format", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		//private void cmdNew_Click()
		//{
		//	try
		//	{
		//		// On Error GoTo CallErrorRoutine
		//		fecherFoundation.Information.Err().Clear();
		//		modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
		//		clsMousePointer Mouse;
		//		Mouse = new clsMousePointer();
		//		txtEmployeeNumber.Focus();
		//	}
		//	catch (Exception ex)
		//	{
		//		// CallErrorRoutine:
		//		modErrorHandler.SetErrorHandler(ex);
		//		return;
		//	}
		//}

		private void cmdSave_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();

                stopwatch.Reset();
                stopwatch.Start();
				
                // Get all of the records from the database
				rsSearch.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
				if (rsSearch.EndOfFile())
				{
					rsSearch.AddNew();
				}
				else
				{
					rsSearch.Edit();
				}
				rsSearch.Update();

                stopwatch.Stop();
                var metricVal = stopwatch.ElapsedMilliseconds;
                
                _telemetryService.TrackTiming("PY - frmEmployeeSearch - cmdSave_Click - select * from tblEmployeeMaster",stopwatch.ElapsedMilliseconds);

                // All changes have been updated
                intDataChanged = 0;
				MessageBox.Show("Save was successful", "Payroll Field Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdFind_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strFind;
				cGenericCollection listEmps;
				// Create the where clause
				// 
				strFind = "";

				if (intSortColumn == 0)
				{
					intSortColumn = 1;
				}

                var lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                //FCConvert.ToString(GetSortColumn(intSortColumn));

				listEmps = empServ.GetEmployeesPermissable(lngID, FCConvert.ToString(GetSortColumn(intSortColumn)), !boolASC);
				if (FCConvert.ToBoolean(empServ.HadError))
				{
					fecherFoundation.Information.Err().Raise(empServ.LastErrorNumber, "", empServ.LastErrorMessage, null, null);
					return;
				}
				vsSearch.Rows = 1;
				
				if (listEmps.ItemCount() < 1)
				{
					vsSearch.Rows = 1;
					if (!gboolFormLoading)
					{
						MessageBox.Show("No records with the search criteria were found.", "Payroll Employee Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					// rsSearch.MoveLast
					// rsSearch.MoveFirst
                    vsSearch.Rows = 1;
					listEmps.MoveFirst();
					cEmployee tempEmp;
					bool boolUse = false;
					vsSearch.Visible = false;
					while (listEmps.IsCurrent())
					{
						//App.DoEvents();
						boolUse = true;
						tempEmp = (cEmployee)listEmps.GetCurrentItem();
						if (txtEmployeeNumber.Text != string.Empty)
						{
							if (fecherFoundation.Strings.LCase(tempEmp.EmployeeNumber) != fecherFoundation.Strings.LCase(txtEmployeeNumber.Text))
							{
								boolUse = false;
							}
						}
						if (txtLastName.Text != string.Empty)
						{
							if (tempEmp.LastName != "")
							{
								if (Strings.InStr(1, fecherFoundation.Strings.LCase(tempEmp.LastName), fecherFoundation.Strings.LCase(txtLastName.Text), CompareConstants.vbBinaryCompare) == 0)
								{
									boolUse = false;
								}
							}
							else
							{
								boolUse = false;
							}
						}
						if (txtFirstName.Text != string.Empty)
						{
							if (tempEmp.FirstName != "")
							{
								if (Strings.InStr(1, fecherFoundation.Strings.LCase(tempEmp.FirstName), fecherFoundation.Strings.LCase(txtFirstName.Text), CompareConstants.vbBinaryCompare) == 0)
								{
									boolUse = false;
								}
							}
							else
							{
								boolUse = false;
							}
						}
						if (Conversion.Val(txtSeqNumber.Text) != 0)
						{
							if (tempEmp.SequenceNumber != Conversion.Val(txtSeqNumber.Text))
							{
								boolUse = false;
							}
						}
						if (txtSSN.ClipText != "")
						{
							if (tempEmp.SSN != "")
							{
								if (Strings.InStr(1, tempEmp.SSN.Replace("-", ""), txtSSN.Text.Replace("-", ""), CompareConstants.vbBinaryCompare) == 0)
								{
									boolUse = false;
								}
							}
							else
							{
								boolUse = false;
							}
						}
						if (txtDeptDiv.Text != "")
						{
							if (tempEmp.DepartmentDivision != txtDeptDiv.Text)
							{
								boolUse = false;
							}
						}
						if (boolUse)
						{
							vsSearch.Rows += 1;
							var lngRow = vsSearch.Rows - 1;
							vsSearch.TextMatrix(lngRow, 0, FCConvert.ToString(lngRow));
							vsSearch.TextMatrix(lngRow, 1, tempEmp.EmployeeNumber);
							vsSearch.TextMatrix(lngRow, 2, fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(tempEmp.LastName) + ", " + fecherFoundation.Strings.Trim(tempEmp.FirstName) + " " + fecherFoundation.Strings.Trim(tempEmp.MiddleName)) + " " + fecherFoundation.Strings.Trim(tempEmp.Designation)));							
							vsSearch.TextMatrix(lngRow, 4, FCConvert.ToString(tempEmp.SequenceNumber));
							vsSearch.TextMatrix(lngRow, 5, tempEmp.DepartmentDivision);
							vsSearch.TextMatrix(lngRow, 6, fecherFoundation.Strings.Trim(Strings.Left(tempEmp.Zip + "     ", 5)));
                            if (ssnViewPermission == SSNViewType.Full)
                            {
                                vsSearch.TextMatrix(lngRow, 3, Strings.Format(fecherFoundation.Strings.Trim(tempEmp.SSN), "000-00-0000"));
                            }
                            else if (ssnViewPermission == SSNViewType.Masked)
                            {
                                vsSearch.TextMatrix(lngRow, 3, fecherFoundation.Strings.Trim("***-**-" + tempEmp.SSN.Right(4)));
                            }
                            else
                            {
                                vsSearch.TextMatrix(lngRow, 3, fecherFoundation.Strings.Trim("***-**-****"));
                            }
                            vsSearch.TextMatrix(lngRow, 7, vsSearch.TextMatrix(lngRow, 3).Replace("-",""));
                            vsSearch.TextMatrix(lngRow, 8, fecherFoundation.Strings.Trim(tempEmp.DateHired));
							vsSearch.TextMatrix(lngRow, 9, tempEmp.EmployeeNumber);
							vsSearch.TextMatrix(lngRow, 10, tempEmp.Status);
							vsSearch.TextMatrix(lngRow, 11, FCConvert.ToString(tempEmp.ID));
                        }
						listEmps.MoveNext();
					}
					vsSearch.Visible = true;
					if (chkShowActive.CheckState == Wisej.Web.CheckState.Checked)
					{
						chkShowActive_Click();
					}
                }
				modGlobalVariables.Statics.gboolAutoSelectEmployee = false;
				if (vsSearch.Rows == 2)
				{
					// If gtypeCurrentEmployee <> vbNullString Then
					vsSearch.Row = 1;
					cmdSelect_Click();
					modGlobalVariables.Statics.gboolAutoSelectEmployee = true;
					// Else
					// Unload Me
					// End If
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdFind_Click()
		{
			cmdFind_Click(cmdFind, new System.EventArgs());
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object GetSortColumn(int intSortColumn)
		{
			object GetSortColumn = null;
			switch (intSortColumn)
			{
				case 1:
					{
						GetSortColumn = "EmployeeNumber";
						break;
					}
				case 2:
					{
						GetSortColumn = "LastName";
						break;
					}
				case 3:
					{
                        //if (ssnViewPermission == SSNViewType.Full)
                        //{
                        //    GetSortColumn = "SSN";
                        //}
                        //else
                        //{
                        
                        GetSortColumn = "EmployeeNumber";
                        
                        //}
                        break;
					}
				case 4:
					{
						GetSortColumn = "SeqNumber";
						break;
					}
				case 5:
					{
						GetSortColumn = "DeptDiv";
						break;
					}
				case 10:
					{
						GetSortColumn = "Status";
						break;
					}
				default:
					{
						GetSortColumn = "EmployeeNumber";
						break;
					}
			}
			//end switch
			return GetSortColumn;
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsSearch.Cols = 12;
				vsSearch.ColHidden(0, true);
				vsSearch.ColHidden(6, true);
				vsSearch.ColHidden(7, true);
				vsSearch.ColHidden(8, true);
				vsSearch.ColHidden(9, true);
				vsSearch.ColHidden(11, true);
				vsSearch.ExtendLastCol = true;
				//vsSearch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsSearch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				// set column 0 properties
				vsSearch.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(0, 400);
				// set column 1 properties
				vsSearch.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(1, 800);
				vsSearch.TextMatrix(0, 1, "Emp #");
				// set column 2 properties
				vsSearch.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(2, 2500);
				vsSearch.TextMatrix(0, 2, "Full Name");
				// set column 3 properties
				vsSearch.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(3, 1500);
				vsSearch.TextMatrix(0, 3, "SSN");
				// set column 4 properties
				vsSearch.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(4, 800);
				vsSearch.TextMatrix(0, 4, "Seq #");
				// set column 5 properties
				vsSearch.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(5, 1200);
				vsSearch.TextMatrix(0, 5, "Dept/Div");
				// set column 6 properties
				vsSearch.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(6, 700);
				vsSearch.TextMatrix(0, 6, "Zip");
				// set column 7 properties
				vsSearch.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(7, 1000);
				vsSearch.TextMatrix(0, 7, "SSN");
				// set column 8 properties
				vsSearch.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(8, 800);
				vsSearch.TextMatrix(0, 8, "Date Hire");
				// set column 8 properties
				vsSearch.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsSearch.ColWidth(10, 800);
				vsSearch.TextMatrix(0, 10, "Status");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSelect_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (vsSearch.Row < 1)
					return;
				if (vsSearch.Col < 0)
					return;
				if (vsSearch.TextMatrix(vsSearch.Row, 1) != string.Empty)
				{
					if (!boolDontUpdateGlobal)
					{
						modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = vsSearch.TextMatrix(vsSearch.Row, 1);
						modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = vsSearch.TextMatrix(vsSearch.Row, 2);
						modGlobalRoutines.SetCurrentEmployeeType(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig, false);
						// Master was open but we want to search for someone else and view them
						if (frmEmployeeMaster.InstancePtr.ActiveForm == true)
						{
							frmEmployeeMaster.InstancePtr.mnuExit_Click();
							frmEmployeeMaster.InstancePtr.Show(App.MainForm);
						}
						// update the global settings table with the current employee number
						// Call rsSearch.Execute("Update tblGlobalSettings Set LastEmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "',LastUserID = '" & gstrUser & "'")
						modGlobalRoutines.ShowEmployeeInformation(true);
						switch (modGlobalVariables.Statics.gstrEmployeeScreen)
                        {
                            case "frmEmployeeDeductions":
                                frmEmployeeDeductions.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmEmployeeMaster":
                                frmEmployeeMaster.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmEmployeePayTotals":
                                frmEmployeePayTotals.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmVacationSick":
                                frmVacationSick.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmMiscUpdate":
                                frmMiscUpdate.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmEmployersMatch":
                                frmEmployersMatch.InstancePtr.Show(App.MainForm);

                                break;
                            case "DataEntry":
                                frmPayrollDistribution.InstancePtr.Show(App.MainForm);

                                break;
                            case "frmPayrollDistributionDataEntry":

                                // frmPayrollDistributionDataEntry.Show , MDIParent
                                break;
                            case "frmPayrollDistribution":

                                // If frmPayrollDistribution.Visible = True Then
                                // Call frmPayrollDistribution.mnuExit_Click
                                frmPayrollDistribution.InstancePtr.Show(App.MainForm);
                                // Else
                                // frmPayrollDistribution.Show , MDIParent
                                // End If
                                break;
                            case "frmDirectDeposit":
                                frmDirectDeposit.InstancePtr.Show(App.MainForm);

                                break;
                        }
					}
					else
					{
						lngEmployeeID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSearch.TextMatrix(vsSearch.Row, 11))));
					}
				}
                //FC:FINAl:SBE - #4187 - update client side in order to activate the correct form
                FCUtils.ApplicationUpdate(App.MainForm);
				// If vsSearch.Rows > 2 Then
				if (!gboolFormLoading)
				{
					Close();
				}
				// ElseIf vsSearch.Row = 1 And pboolDoubleClick Then
				// Unload Me
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSelect_Click()
		{
			cmdSelect_Click(cmdSelect, new System.EventArgs());
		}

		private void frmEmployeeSearch_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				if (modGlobalVariables.Statics.gboolAutoSelectEmployee && gboolFormLoading)
				{
					Close();
					return;
				}
				gboolFormLoading = false;
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				cmdFind_Click();
				// Unload Me
			}
			if (KeyCode == Keys.F6)
				cmdSelect_Click();
			if (KeyCode == Keys.F5)
				RefreshScreen();
		}

		private void RefreshScreen()
		{
			cmdFind_Click();
		}

		private void frmEmployeeSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEmployeeSearch_Load(object sender, System.EventArgs e)
		{
			
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				gboolFormLoading = true;
				pboolDoubleClick = false;
				frmEmployeeMaster.InstancePtr.ActiveForm = false;
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;
                        txtSSN.Enabled = false;
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        txtSSN.Enabled = false;
                        break;
                }
                // you can only add an employee if your going to the master screen
                if (modGlobalVariables.Statics.gstrEmployeeScreen == "frmEmployeeMaster")
				{
					if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) == "F" && ssnViewPermission == SSNViewType.Full)
					{
						cmdAddNew.Visible = true;
						cmdNew.Visible = true;
					}
					else
					{
						cmdAddNew.Visible = false;
						cmdNew.Visible = false;
					}
				}
				else
				{
					cmdAddNew.Visible = false;
					cmdNew.Visible = false;
				}
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				rsSearch.DefaultDB = "TWPY0000.vb1";
				// set up the display grid
				SetGridProperties();
				vsSearch.Rows = 1;
				intDataChanged = 0;
				// get the type of employee number to format the control
				modGlobalRoutines.GetEmployeeType();
				// Get the data from the database
				boolASC = true;
                

                if (ssnViewPermission != SSNViewType.Full)
                {
                    cmdAddNew.Enabled = false;
                    cmdAddNew.Visible = false;
                }
                ShowData();
                
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				txtEmployeeNumber.Text = "";
				cmdFind_Click();
				if (modGlobalVariables.Statics.gboolAutoSelectEmployee)
				{
					return;
				}
				else
				{
					// Call SetGridHeight
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridHeight()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridHeight";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
				if (((vsSearch.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsSearch.Rows) + 75) < (this.HeightOriginal * 0.4))
				{
					vsSearch.Height = vsSearch.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsSearch.Rows + 75;
				}
				else
				{
					vsSearch.Height = FCConvert.ToInt32((this.HeightOriginal * 0.4) - 300);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmEmployeeSearch_Resize(object sender, System.EventArgs e)
		{
			vsSearch.Cols = 12;
			vsSearch.ColWidth(0, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.04));
			vsSearch.ColWidth(1, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.1));
			vsSearch.ColWidth(2, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.35));
			vsSearch.ColWidth(3, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.15));
			vsSearch.ColWidth(4, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.1));
			vsSearch.ColWidth(5, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.11));
			vsSearch.ColWidth(6, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.07));
			vsSearch.ColWidth(7, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.1));
			vsSearch.ColWidth(8, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.08));
			vsSearch.ColWidth(10, FCConvert.ToInt32(vsSearch.WidthOriginal * 0.08));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				// cannot show form when modal form is already displayed
				if (fecherFoundation.Information.Err(ex).Number == 401)
				{
					/*? Resume Next; */
				}
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			cmdAddNew_Click();
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuFind_Click(object sender, System.EventArgs e)
		{
			cmdFind_Click();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			RefreshScreen();
		}

		private void mnuSelect_Click(object sender, System.EventArgs e)
		{
			cmdSelect_Click();
		}

		private void txtEmployeeNumber_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtEmployeeNumber_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtEmployeeNumber.SelectionStart = 0;
				txtEmployeeNumber.SelectionLength = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsSearch_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsSearch.MouseRow == 0)
			{
				// sort the grid
				boolASC = !boolASC;
				intSortColumn = vsSearch.MouseCol;
				cmdFind_Click();
			}
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			pboolDoubleClick = true;
			cmdSelect_Click();
		}

		private void vsSearch_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == FCConvert.ToInt32(Keys.Return))
				cmdSelect_Click();
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsSearch_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsSearch[e.ColumnIndex, e.RowIndex];
            if (vsSearch.GetFlexRowIndex(e.RowIndex) == 0)
			{
				//ToolTip1.SetToolTip(vsSearch, "Click headings to sort by selected column.");
				cell.ToolTipText = "Click headings to sort by selected column.";
			}
			else
			{
                //ToolTip1.SetToolTip(vsSearch, "");
                cell.ToolTipText = "";
			}
		}
	}
}
