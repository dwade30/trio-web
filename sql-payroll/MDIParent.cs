//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Payroll;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	public class MDIParent// : BaseForm
	{
        public FCCommonDialog CommonDialog1;
        public MDIParent()
		{
			//
			// required for windows form designer support
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.CommonDialog1 = new FCCommonDialog();
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		//=========================================================
		private cEmployeeService empService = new cEmployeeService();
		private bool boolAllEmployeesAllowed;
		private int[] LabelNumber = new int[200 + 1];
		private int intExit;
		private string strFullSetDesc = "";
		private int lngFCode;
		private bool boolCanAccessAllDepartments;
		const string strTrio = "TRIO Software - Payroll";
		private bool cleanUpRoutinesMenuCreated = false;
		private bool UnemploymentMenuCreated = false;
		private bool RetirementMenuCreated = false;
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;

        public void Init()
		{
			modGlobalFunctions.LoadTRIOColors();
			App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.NavigationMenu.OriginName = "Payroll";
			App.MainForm.menuTree.ImageList = CreateImageList();
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-payroll");
			}
			App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
            MDIParent_Load();
            MainCaptions();
		}

		private ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-account-payable-processing", "account-payable-processing"),
				new ImageListEntry("menutree-contractors", "contractors"),
				new ImageListEntry("menutree-end-of-year", "end-of-year"),
				new ImageListEntry("menutree-eoy-fiscal", "eoy-fiscal"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-table-file-setup", "table-file-setup"),
				new ImageListEntry("menutree-file-maintenance", "file-maintenance")
			});
			return imageList;
		}

		public void ClearLabels()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ClearLabels";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetMenuOptions(string strMenu)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetMenuOptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As object	OnWriteFCConvert.ToInt32(
				int intCounter;
				ClearLabels();
				modGlobalRoutines.CloseChildForms();
				if (modGlobalVariables.Statics.gboolDontChangeMenus)
				{
					modGlobalVariables.Statics.gboolDontChangeMenus = false;
					return;
				}
				
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu1()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu1";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(1);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(1);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(1);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(1);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(1);
				}
                else if (vbPorterVar == "RETIREMENT")
				{
					RetirementActions(1);
				}
				else if (vbPorterVar == "CLEANUP")
				{
					modGlobalRoutines.ClearAuditDataToArchive();
					MessageBox.Show("Audit Archived", "Archived", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (vbPorterVar == "CONTRACTWAGES")
				{
					frmContractWages.InstancePtr.Init(true);
				}
				else if (vbPorterVar == "DEDUCTIONREPORTS")
				{
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(1);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(1);
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					frmUnemploymentReport.InstancePtr.strReportType = "M";
					frmUnemploymentReport.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					modGlobalRoutines.RunFullSetOfReports();
				}
				else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("EMPLOYEE");
					modGlobalRoutines.ShowEmployeeInformation(true);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(1);
				}
				else if (vbPorterVar == "PRINT")
				{
				}
				else if (vbPorterVar == "BANK")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(1);
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(1);
				}
				else if (vbPorterVar == "W2ADDITIONALINFO")
				{
					frmW2AdditionalInfoSetup.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "HISTORYFIX")
				{
					string strCode = "";
					strCode = Interaction.InputBox("Call TRIO and get the Mod Specific One Day Access Code and enter it here.", "Unemployment History Fix", null);
					if (fecherFoundation.Strings.Trim(strCode) == string.Empty || !Information.IsNumeric(strCode))
					{
						MessageBox.Show("Invalid code. Please Call TRIO", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(FCConvert.ToDouble(strCode)), "PY"))
						{
							if (MessageBox.Show("Are you sure you wish to update your database with the current settings from the Unemployment Setup screen?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								modGlobalFunctions.AddCYAEntry_6("PY", "Unemployment Fix", "Date=" + FCConvert.ToString(DateTime.Now), "User=" + modGlobalVariables.Statics.gstrUser, "One Data Code=" + strCode);
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								//App.DoEvents();
								// Corey 04/19/2005 uses the distribution screen now, not just the default
								modCoreysSweeterCode.UnemploymentHistoryFix();
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
								MessageBox.Show("Update of history files completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								// End If
							}
						}
						else
						{
							MessageBox.Show("Invalid code. Please Call TRIO", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu10()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu10";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// GET THE TAG VALUE TO
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(10);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(10);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					frmWarrant.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(10);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(10);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(10);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					// ARCHIVE
					CalendarActions(10);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(10);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu11()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu11";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(11);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(11);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					// frmReportViewer.Init rptAuditHistory
					modCoreysSweeterCode.CheckDefaultPrint(rptNewAuditHistory.InstancePtr, boolAllowEmail: true, strAttachmentName: "AuditHistory");
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(11);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(11);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(11);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(11);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(11);
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu12()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu12";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(12);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(12);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(12);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(12);
				}
				else if (vbPorterVar == "PROCESSREPORTS" && !cleanUpRoutinesMenuCreated)
				{
					CleanUpCaptions(App.MainForm.NavigationMenu.CurrentItem);
					//SetMenuOptions("CLEANUP");
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(12);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(12);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(12);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu13()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu13";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(13);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(13);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(13);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(13);
				}
				else if (vbPorterVar == "SYSTEM")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					rpt941.InstancePtr.Init();
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(13);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(13);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu14()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu14";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(14);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(14);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "SYSTEM")
				{
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(14);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(14);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					rpt940.InstancePtr.Init();
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(14);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(14);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "YEARLY")
					{
						frmReportViewer.InstancePtr.Init(rpt941.InstancePtr);
						//rpt941.InstancePtr.Show();
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu15()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu15";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(15);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(15);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(15);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(15);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS" && !RetirementMenuCreated)
				{
					RetirementCaptions(App.MainForm.NavigationMenu.CurrentItem);
					//SetMenuOptions("RETIREMENT");
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(15);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(15);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu16()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu16";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(16);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(16);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(16);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(16);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
					{
						//SetMenuOptions("MAIN");
					}
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(16);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					int intQ = 0;
					int lngY = 0;
					string strSequence = "";
					if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
					{
						modGlobalVariables.Statics.gboolCancelSelected = true;
						//FC:FINAL:DSE:#i2421 Use date parameter by reference
						DateTime tmpArg = DateTime.FromOADate(0);
						frmSelectDateInfo.InstancePtr.Init3("Select year and quarter", ref lngY, ref intQ, -1, ref tmpArg, -1, false);
						if (modGlobalVariables.Statics.gboolCancelSelected)
						{
							return;
						}
						modGlobalVariables.Statics.gboolCancelSelected = true;
					}
					else
					{
						switch (modGlobalVariables.Statics.gdatCurrentPayDate.Month)
						{
							case 1:
							case 2:
							case 3:
								{
									intQ = 1;
									break;
								}
							case 4:
							case 5:
							case 6:
								{
									intQ = 2;
									break;
								}
							case 7:
							case 8:
							case 9:
								{
									intQ = 3;
									break;
								}
							case 10:
							case 11:
							case 12:
								{
									intQ = 4;
									break;
								}
						}
						//end switch
						lngY = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					}
					strSequence = frmGetSequence.InstancePtr.Init();
					if (strSequence != string.Empty)
					{
						if (strSequence == "-1")
						{
							strSequence = "";
						}
						cFed941ViewContext fedView = new cFed941ViewContext();
						fedView.ReportQuarter = intQ;
						fedView.ReportYear = lngY;
						fedView.Sequence = strSequence;
						frm941TaxLiability.InstancePtr.Init(fedView);
						frmPrint941.InstancePtr.Init(fedView);
					}
					rpt941Wage.InstancePtr.Init(intQ, lngY);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu17()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu17";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int lngYear = 0;
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(17);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(17);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(17);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					int intQuarter = 0;
					string strSequence = "";
					lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
					{
						intQuarter = 1;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
					{
						intQuarter = 2;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
					{
						intQuarter = 3;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
					{
						intQuarter = 4;
					}
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init3("Select quarter and year for report", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
					strSequence = frmGetSequence.InstancePtr.Init();
					if (strSequence == string.Empty)
					{
						// cancelled
						return;
					}
					frmPrintMaine941.InstancePtr.Init(intQuarter, lngYear, strSequence);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu18()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu18";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS" && !UnemploymentMenuCreated)
				{
					UnemploymentReportCaptions(App.MainForm.NavigationMenu.CurrentItem);
					//SetMenuOptions("UNEMPLOYMENT");
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(18);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(18);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(18);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu19()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu19";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					//SetMenuOptions("PROCESS");
				}
				else if (vbPorterVar == "SYSTEM")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(19);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(19);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
					//SetMenuOptions("MAIN");
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(19);
				}
				else
				{
					//SetMenuOptions("MAIN");
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu2()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu2";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(2);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(2);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(2);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(2);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(2);
				}
                else if (vbPorterVar == "RETIREMENT")
				{
					RetirementActions(2);
				}
				else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("PROCESS");
				}
				else if (vbPorterVar == "CONTRACTWAGES")
				{
					frmContractWages.InstancePtr.Init(false);
				}
				else if (vbPorterVar == "DEDUCTIONREPORTS")
				{
				}
				else if (vbPorterVar == "W2ADDITIONALINFO")
				{
					frmW2ThirdPartyEntries.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "CLEANUP")
				{
					frmReportViewer.InstancePtr.Init(rptTempFlagReset.InstancePtr, boolAllowEmail: true, strAttachmentName: "TempFlagReset");
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					frmUnemploymentReport.InstancePtr.strReportType = "N";
					frmUnemploymentReport.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(2);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(2);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					RptTrustAndAgencyCodes.InstancePtr.Init();
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "BANK")
				{
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(2);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(2);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(2);
				}
				else if (vbPorterVar == "HISTORYFIX")
				{
					string strCode = "";
					strCode = Interaction.InputBox("Call TRIO and get the Mod Specific One Day Access Code and enter it here.", "MSRS History Fix", null);
					if (fecherFoundation.Strings.Trim(strCode) == string.Empty || !Information.IsNumeric(strCode))
					{
						MessageBox.Show("Invalid code. Please Call TRIO", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(FCConvert.ToDouble(strCode)), "PY"))
						{
							if (MessageBox.Show("This will update MSRS records based on the current settings in the distribution screen" + "\r\n" + "Are you sure you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								modGlobalFunctions.AddCYAEntry_6("PY", "MSRS History Fix");
								modCoreysSweeterCode.UpdateMSRS(true);
								MessageBox.Show("MSRS History Fix complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu3()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngYear = 0;
			int intQuarter = 0;
			string strSequence = "";
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu3";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(3);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(3);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(3);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(3);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(3);
				}
                else if (vbPorterVar == "RETIREMENT")
				{
					RetirementActions(3);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(3);
				}
				else if (vbPorterVar == "DEDUCTIONREPORTS")
				{
					//SetMenuOptions("APPLICATIONPRINTING");
				}
				else if (vbPorterVar == "CONTRACTWAGES")
				{
					//SetMenuOptions("EMPLOYEE");
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(3);
				}
				else if (vbPorterVar == "W2ADDITIONALINFO")
				{
					//SetMenuOptions("CALENDAR");
				}
				else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("CALENDAR");
				}
				else if (vbPorterVar == "CLEANUP")
				{
					//SetMenuOptions("PROCESSREPORTS");
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					frmCheckRegister.InstancePtr.Unload();
					frmCheckRegister.InstancePtr.Show(App.MainForm);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(3);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(3);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
				}
				else if (vbPorterVar == "COST")
				{
				}
				else if (vbPorterVar == "BANK")
				{
					//SetMenuOptions("EMPLOYEE");
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(3);
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 1 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 3)
					{
						intQuarter = 1;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 4 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 6)
					{
						intQuarter = 2;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 7 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 9)
					{
						intQuarter = 3;
					}
					else if (modGlobalVariables.Statics.gdatCurrentPayDate.Month >= 10 && modGlobalVariables.Statics.gdatCurrentPayDate.Month <= 12)
					{
						intQuarter = 4;
					}
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init3("Select quarter and year for report", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
					if (modGlobalVariables.Statics.gboolCancelSelected)
					{
						// cancelled
						return;
					}
					strSequence = frmGetSequence.InstancePtr.Init(true);
					if (strSequence == string.Empty)
					{
						// cancelled
						return;
					}
					clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("electronicc1")))
						{
							frmElectronicWageReporting.InstancePtr.Init(intQuarter, lngYear, strSequence);
							if (!modGlobalVariables.Statics.gboolCancelSelected && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
							{
								// UCase(MuniName) <> "JAY" And
								rptStateUnemploymentC1.InstancePtr.Init(intQuarter, lngYear, strSequence);
							}
						}
						else
						{
							frmElectronicWageReporting.InstancePtr.Init(intQuarter, lngYear, strSequence, true);
							if (modGlobalVariables.Statics.gboolCancelSelected)
							{
							}
							else
							{
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
								{
									// UCase(MuniName) <> "JAY" And
									rptStateUnemploymentC1.InstancePtr.Init(intQuarter, lngYear, strSequence);
								}
							}
						}
					}
					else
					{
						frmElectronicWageReporting.InstancePtr.Init(intQuarter, lngYear, strSequence, true);
						if (!modGlobalVariables.Statics.gboolCancelSelected && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WISCASSET SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "WESTPORT SCHOOLS" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ALNA SCHOOLS")
						{
							// UCase(MuniName) <> "JAY" And
							rptStateUnemploymentC1.InstancePtr.Init(intQuarter, lngYear, strSequence);
						}
					}
				}
				else if (vbPorterVar == "HISTORYFIX")
				{
					string strCode = "";
					strCode = Interaction.InputBox("Call TRIO and get the Mod Specific One Day Access Code and enter it here.", "MSRS History Fix", null);
					if (fecherFoundation.Strings.Trim(strCode) == string.Empty || !Information.IsNumeric(strCode))
					{
						MessageBox.Show("Invalid code. Please Call TRIO", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(FCConvert.ToDouble(strCode)), "PY"))
						{
							modGlobalFunctions.AddCYAEntry_6("PY", "Dist M Fix");
							FCFileSystem fso = new FCFileSystem();
							clsDRWrapper rsGood = new clsDRWrapper();
							clsDRWrapper rsBad = new clsDRWrapper();
							if (FCFileSystem.FileExists("MSRSTWPY0000.vb1"))
							{
							}
							else
							{
								MessageBox.Show("A copy of the PY database with the CORRECT Distribution MSRS settings must be named MSRSTWPY0000.vb1 and reside in the data folder.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
							rsGood.OpenRecordset("Select * from tblPayrollDistribution", "MSRSTWPY0000.vb1");
							rsBad.OpenRecordset("Select * from tblPayrollDistribution", "TWPY0000.vb1");
							while (!rsGood.EndOfFile())
							{
								if (rsBad.FindFirstRecord("ID", rsGood.Get_Fields("ID")))
								{
									rsBad.Edit();
									rsBad.Set_Fields("MSRS", rsGood.Get_Fields("MSRS"));
									if (FCConvert.ToString(rsGood.Get_Fields_Int32("MSRSID")) == string.Empty)
									{
										rsBad.Set_Fields("MSRSID", modCoreysSweeterCode.CNSTDISTMNOMSRS);
									}
									else
									{
										rsBad.Set_Fields("MSRSID", rsGood.Get_Fields_Int32("MSRSID"));
									}
									rsBad.Set_Fields("StatusCode", rsGood.Get_Fields_String("StatusCode"));
									rsBad.Set_Fields("GrantFunded", rsGood.Get_Fields_String("GrantFunded"));
									rsBad.Update();
								}
								rsGood.MoveNext();
							}
							MessageBox.Show("Dist M Fix complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu4()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu4";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(4);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(4);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(4);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(4);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(4);
				}
                else if (vbPorterVar == "RETIREMENT")
				{
					RetirementActions(4);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(4);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(4);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					modCustomReport.Statics.strPreSetReport = "PAYSUMMARY";
					frmCustomReport.InstancePtr.Show(App.MainForm);
					modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "PAYSUMMARY");
					frmCustomReport.InstancePtr.cmdPrint_Click();
					frmCustomReport.InstancePtr.Unload();
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					int lngYear = 0;
					if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
					{
						//FC:FINAL:DSE:#i2421 Use date parameter by reference
						DateTime tmpArg = DateTime.FromOADate(0);
						frmSelectDateInfo.InstancePtr.Init4("Input FUTA Year", ref lngYear, -1, -1, ref tmpArg, -1, false);
					}
					else
					{
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					}
					if (lngYear > 0)
					{
						rptFUTA940Report.InstancePtr.Init(lngYear);
					}
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					//SetMenuOptions("PROCESSREPORTS");
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "PRINT")
				{
				}
				else if (vbPorterVar == "COST")
				{
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(4);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(4);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(4);
				}
				else if (vbPorterVar == "HISTORYFIX")
				{
					//SetMenuOptions("FILE");
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu5()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu5";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(5);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(5);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(5);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(5);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(5);
				}
                else if (vbPorterVar == "UNEMPLOYMENT")
				{
					int lngYear = 0;
					int intQuarter = 0;
					if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
					{
						//FC:FINAL:DSE:#i2421 Use date parameter by reference
						DateTime tmpArg = DateTime.FromOADate(0);
						frmSelectDateInfo.InstancePtr.Init3("Select Quarter to Review", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
						if (intQuarter != 0 && lngYear != 0)
						{
							frmUnemploymentReview.InstancePtr.Init(ref intQuarter, ref lngYear);
						}
					}
					else
					{
						lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
						switch (modGlobalVariables.Statics.gdatCurrentPayDate.Month)
						{
							case 1:
							case 2:
							case 3:
								{
									intQuarter = 1;
									break;
								}
							case 4:
							case 5:
							case 6:
								{
									intQuarter = 2;
									break;
								}
							case 7:
							case 8:
							case 9:
								{
									intQuarter = 3;
									break;
								}
							case 10:
							case 11:
							case 12:
								{
									intQuarter = 4;
									break;
								}
						}
						//end switch
						frmUnemploymentReview.InstancePtr.Init(ref intQuarter, ref lngYear);
					}
				}
				else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("APPLICATIONPRINTING");
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(5);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(5);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					modCustomReport.Statics.strPreSetReport = "EMPLOYEEDEDUCTIONS";
					rptEmployeeDeductionReport.InstancePtr.Init();
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(5);
				}
				else if (vbPorterVar == "PRINT")
				{
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(5);
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "COST")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(5);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
					{
						frmReportViewer.InstancePtr.Init(rpt941.InstancePtr);
						//rpt941.InstancePtr.Show();
					}
					if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
					{
						frmReportViewer.InstancePtr.Init(rpt941.InstancePtr);
						//rpt941.InstancePtr.Show();
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu6()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu6";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper clsLoad = new clsDRWrapper();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(6);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(6);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(6);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(6);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(6);
				}
                else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("SETUP");
				}
				else if (vbPorterVar == "UNEMPLOYMENT")
				{
					//SetMenuOptions("PROCESSREPORTS");
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(6);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(6);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ACHClearinghouse")))
					{
						modCoreysSweeterCode.ShowACHReport(false);
					}
					else
					{
						rptDirectDeposit.InstancePtr.Init();
					}
				}
				else if (vbPorterVar == "CALCULATE")
				{
				}
				else if (vbPorterVar == "PRINT")
				{
					// Case "EMPLOYEE"
					// gtypeCurrentEmployee = "frmMiscUpdate"
					// 
					// If gtypeCurrentEmployee.EmployeeNumber = vbNullString Or Val(gtypeCurrentEmployee.EmployeeNumber) = -1 Then
					// frmEmployeeSearch.Show , MDIParent
					// Else
					// frmMiscUpdate.Show , MDIParent
					// End If
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(6);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(6);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(6);
				}
				else if (vbPorterVar == "MQYPROCESSING")
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
					{
						rpt940.InstancePtr.Init();
					}
					if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
					{
						rpt940.InstancePtr.Init();
					}
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu7()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu7";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(7);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(7);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(7);
				}
				else if (vbPorterVar == "TIMECARD")
				{
					TimeClockActions(7);
				}
				else if (vbPorterVar == "SCHEDULES")
				{
					ScheduleActions(7);
				}
                else if (vbPorterVar == "MAIN")
				{
					//SetMenuOptions("FILE");
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					// rptPayrollAccountingCharges.Init
					rptPayrollAccountingChargesPreview.InstancePtr.Init(false, false);
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(7);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(7);
				}
				else if (vbPorterVar == "PRINT")
				{
					// Case "EMPLOYEE"
					// gtypeCurrentEmployee = "frmEmployeePayTotals"
					// If gtypeCurrentEmployee.EmployeeNumber = vbNullString Or Val(gtypeCurrentEmployee.EmployeeNumber) = -1 Then
					// frmEmployeeSearch.Show , MDIParent
					// Else
					// frmEmployeePayTotals.Show , MDIParent
					// End If
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(7);
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(7);
				}
				else if (vbPorterVar == "BANK")
				{
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(7);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Menu8()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "Menu8"
			// Dim Mouse As clsMousePointer
			clsDRWrapper clsTemp = new clsDRWrapper();
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ResumeCode:
				;
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(8);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(8);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(8);
				}
				else if (vbPorterVar == "MAIN")
				{
					// 
					modBlockEntry.WriteYY();
					//- MDIParent.Close();
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(8);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(8);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					modCustomReport.Statics.strPreSetReport = "DATAENTRYFORMS";
					modCustomReport.Statics.strCustomTitle = "**** Data Entry Forms ****";
					clsTemp.OpenRecordset("select * from tblDefaultInformation", "TWPY0000.vb1");
					switch (clsTemp.Get_Fields_Int32("dataentrysequence"))
					{
						case 0:
							{
								// name
								modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by lastname,firstname,employeenumber ";
								break;
							}
						case 1:
							{
								// employeenumber
								modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by employeenumber ";
								break;
							}
						case 2:
							{
								// sequence
								modCustomReport.Statics.strCustomSQL = "select * from tblemployeemaster where dataentry = 1 order by sequence,lastname,firstname,employeenumber ";
								break;
							}
						case 3:
							{
								// dept/div
								modCustomReport.Statics.strCustomSQL = "Select * from tblemployeemaster where dataentry = 1 order by deptdiv,lastname,firstname,employeenumber ";
								break;
							}
					}
					//end switch
					frmReportViewer.InstancePtr.Init(rptDataEntryForms.InstancePtr, boolAllowEmail: true, strAttachmentName: "DataEntry");
					// 
					// Case "EMPLOYEE"
					// gtypeCurrentEmployee = "frmDirectDeposit"
					// 
					// If gtypeCurrentEmployee.EmployeeNumber = vbNullString Or Val(gtypeCurrentEmployee.EmployeeNumber) = -1 Then
					// frmEmployeeSearch.Show , MDIParent
					// Else
					// frmDirectDeposit.Show , MDIParent
					// End If
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(8);
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(8);
				}
				else if (vbPorterVar == "FILE")
				{
					FileActions(8);
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Menu8", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Menu9()
		{
			int intResponse;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Menu9";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "ACAFILING")
				{
					ACAFilingActions(9);
				}
				else if (vbPorterVar == "WORKSITEREPORTS")
				{
					WorksiteReportActions(9);
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					EmployeeActions(9);
				}
				else if (vbPorterVar == "MAIN")
				{
				}
				else if (vbPorterVar == "APPLICATIONPRINTING")
				{
					ApplicationPrintingActions(9);
				}
				else if (vbPorterVar == "FILE")
				{
					// frmAdjustMSRS.Show , MDIParent
					FileActions(9);
				}
				else if (vbPorterVar == "CALENDAR")
				{
					CalendarActions(9);
				}
				else if (vbPorterVar == "PROCESSREPORTS")
				{
					modCustomReport.Statics.strPreSetReport = "EMPLOYEEVACATION";
					// frmReportViewer.Init frmPrintParameters
					frmPrintParameters.InstancePtr.Show();
				}
				else if (vbPorterVar == "SETUP")
				{
					SetupActions(9);
					// Case "EMPLOYEE"
					// MATTHEW 4/20/2005
					// COMMENTED OUT BECAUSE IF YOU SELECT A PERSON, ENTER THE EMPLOYERS MATCH SCREEN, HIT THE ESC KEY,
					// THEN CLICK ON THE MENU OPTION TO SELECT A NEW PERSON THIS FORMSEXISTS SEEMS TO THINK THAT
					// THE FRMEMPLOYERSMATCH IS STILL OPEN OR THIS FUNCTION ACUTALLY OPENS IT AGAIN TO SEE IF IT IS DIRTY.
					// NEED
					// If FormsExist(Me) Then
					// 
					// Else
					// clears out the last opened form so that none open when
					// a new employee has been chosen
					// Call CloseChildForms
					// gtypeCurrentEmployee = vbNullString
					// 
					// frmEmployeeSearch.Show , MDIParent
					// End If
				}
				else if (vbPorterVar == "PROCESS")
				{
					ProcessActions(9);
				}
				else
				{
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void MDIParent_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
				AutoSizeMenu();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void AutoSizeMenu()
		{
			//int I;
			//// vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
			//int FullRowHeight = 0;
			//// this will store the height of the labels with text
			//// .RowHeight(0) = 0                   'the first row in the grid is height = 0
			//FullRowHeight = FCConvert.ToInt32(GRID.HeightOriginal / 20.0);
			//// find the average height of the row
			//for (I = 1; I <= 20; I++)
			//{
			//	// this just assigns the row height
			//	GRID.RowHeight(I, FullRowHeight);
			//}
			//GRID.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//GRID.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void MDIParent_Load()
		{

			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strReturn = "";
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("SELECT * FROM tblDefaultInformation", "Payroll");
				if (rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gboolNoMSRS = true;
					modGlobalVariables.Statics.gboolCheckNegBalances = false;
				}
				else
				{
					modGlobalVariables.Statics.gboolNoMSRS = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("NoMSRS"));
					modGlobalVariables.Statics.gboolCheckNegBalances = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("CheckNegBalances"));
				}
				rsData.OpenRecordset("Select * from tblPayrollProcessSetup", "Payroll");
				if (!rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gintUseGroupMultiFund = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int16("UseGroupMultiFund"))));
				}
				else
				{
					modGlobalVariables.Statics.gintUseGroupMultiFund = 0;
				}
				rsData.OpenRecordset("Select * from tblFieldLengths", "TWPY0000.vb1");
				if (!rsData.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolWeekly = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayFreqWeekly")) ? true : false);
					modGlobalRoutines.Statics.ggboolBiWeekly = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayFreqBiWeekly")) ? true : false);
				}
				rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				if (!rsData.EndOfFile())
				{
					modGlobalRoutines.Statics.ggboolPRBiWeekly = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("WeeklyPR")) ? true : false);
				}
				rsData.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (!rsData.EndOfFile())
				{
					modGlobalRoutines.Statics.ggintWeekNumber = FCConvert.ToInt16(rsData.Get_Fields("WeekNumber"));
					modGlobalRoutines.Statics.ggintNumberPayWeeks = FCConvert.ToInt16(rsData.Get_Fields_Int32("NumberOfPayWeeks"));
					modGlobalRoutines.Statics.ggboolSetupBiWeekly = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayBiWeeklys")) ? true : false);
					// pay run is to pay weeklys with bi's
					modGlobalRoutines.Statics.ggboolBiWeeklyDeds = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("BiWeeklyDeds")) ? true : false);
				}
				//SetMenuOptions("MAIN");
				//modGlobalFunctions.SetMenuBackColor();
				modErrorHandler.Statics.gstrFormName = "MDIParent";
				boolAllEmployeesAllowed = empService.AllEmployeesPermissable(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewSocialSecurityNumbers.ToInteger()))
                {
                    case "F":
                        ssnViewPermission = SSNViewType.Full;
                        break;
                    case "P":
                        ssnViewPermission = SSNViewType.Masked;
                        break;
                    default:
                        ssnViewPermission = SSNViewType.None;
                        break;
                }
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewBankAccountNumbers.ToInteger()))
                {
                    case "F":
                        bankAccountViewPermission = BankAccountViewType.Full;
                        break;
                    case "P":
                        bankAccountViewPermission = BankAccountViewType.Masked;
                        break;
                    default:
                        bankAccountViewPermission = BankAccountViewType.None;
                        break;
                }
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void GetCaption()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "GetCaption";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
				if (vbPorterVar == "MAIN")
				{
					//GetCaption = MainCaptions();
				}
				else if (vbPorterVar == "PROCESS")
				{
					//GetCaption = ProcessCaptions();
					// Case "PRINT"
					// GetCaption = PrintCaptions()
				}
				else if (vbPorterVar == "SETUP")
				{
					//GetCaption = SetupCaptions();
				}
				else if (vbPorterVar == "FILE")
				{
					//GetCaption = FileCaptions();
				}
				else if (vbPorterVar == "EMPLOYEE")
				{
					//GetCaption = EmployeeCaptions();
				}
				else if (vbPorterVar == "BANK")
				{
					//GetCaption = BankCaptions();
				}
				else
				{
					//GetCaption = MainCaptions();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void MainCaptions()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MainCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string menu = "MAIN";
				string imageKey = "";
				App.MainForm.Caption = "PAYROLL";
				string strTemp = "";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Main Menu]";
				for (int intCount = 1; intCount <= 6; intCount++)
				{
					lngFCode = 0;
					boolDisable = false;
					switch (intCount)
					{
						case 1:
							{
								imageKey = "account-payable-processing";
								strTemp = "Employee File Updates";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.EMPLOYEEFILEUPDATE;
								break;
							}
						case 2:
							{
								imageKey = "contractors";
								strTemp = "Payroll Processing";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.PAYROLLPROCESSING;
								break;
							}
						case 3:
							{
								imageKey = "end-of-year";
								strTemp = "Calendar End Of Year";
								LabelNumber[Strings.Asc("3")] = 3;
                                if (ssnViewPermission == SSNViewType.Full)
                                {
                                    lngFCode = modGlobalVariables.CALENDARENDOFYEAR;
                                }
                                else
                                {
                                    boolDisable = true;
                                }
                                break;
							}
						case 4:
							{
								imageKey = "printing";
								strTemp = "Printing";
								LabelNumber[Strings.Asc("4")] = 4;
								lngFCode = modGlobalVariables.MAINPRINTING;
								break;
							}
						case 5:
							{
								imageKey = "table-file-setup";
								strTemp = "Table and File Setups";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.TABLEANDFILESETUPS;
								break;
							}
						case 6:
							{
								imageKey = "file-maintenance";
								strTemp = "File Maintenance";
								LabelNumber[Strings.Asc("M")] = 6;
								lngFCode = modGlobalVariables.FILEMAINTENANCE;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 1, imageKey);
					switch (intCount)
					{
						case 1:
							EmployeeCaptions(newItem);
							break;
						case 2:
							ProcessCaptions(newItem);
							break;
						case 3:
							CalendarCaptions(newItem);
							break;
						case 4:
							ApplicationPrintingCaptions(newItem);
							break;
						case 5:
							SetupCaptions(newItem);
							break;
						case 6:
							FileCaptions(newItem);
							break;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void EmployeeCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "EmployeeCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "EMPLOYEE";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Employee File Updates]";
				for (int intCount = 1; intCount <= 15; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Employee Add / Update";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.EMPLOYEEADDUPDATE;
								break;
							}
						case 2:
							{
								strTemp = "Payroll Distribution";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.PAYROLLDISTRIBUTION;
								break;
							}
						case 3:
							{
								strTemp = "Deductions";
								LabelNumber[Strings.Asc("3")] = 3;
								lngFCode = modGlobalVariables.DEDUCTIONS;
								break;
							}
						case 4:
							{
								strTemp = "Employer's Match";
								LabelNumber[Strings.Asc("4")] = 4;
								lngFCode = modGlobalVariables.EMPLOYERSMATCH;
								break;
							}
						case 5:
							{
								strTemp = "Vacation / Sick";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.VACATIONSICK;
								break;
							}
						case 6:
							{
								strTemp = "MainePERS / Unemployment";
								LabelNumber[Strings.Asc("6")] = 6;
								lngFCode = modGlobalVariables.MSRSUNEMPLOYMENTINFO;
								break;
							}
						case 7:
							{
								strTemp = "Pay Totals";
								LabelNumber[Strings.Asc("7")] = 7;
								lngFCode = modGlobalVariables.PayTotals;
								break;
							}
						case 8:
							{
								strTemp = "Direct Deposit Breakdown";
								LabelNumber[Strings.Asc("8")] = 8;
                                if (bankAccountViewPermission == BankAccountViewType.Full)
                                {
                                    lngFCode = modGlobalVariables.DIRECTDEPOSITSECURITY;
                                }
                                else
                                {
                                    boolDisable = true;
                                }
                                break;
							}
						case 9:
							{
								strTemp = "Select Employee";
								LabelNumber[Strings.Asc("9")] = 9;
								// lngFCode = SELECTEMPLOYEE
								break;
							}
						case 10:
							{
								strTemp = "Employee Health Care";
								LabelNumber[Strings.Asc("A")] = 10;
                                if (ssnViewPermission == SSNViewType.Full)
                                {
                                    lngFCode = modGlobalVariables.CNSTHealthcareSetup;
                                }
                                else
                                {
                                    boolDisable = true;
                                }

                                break;
							}
						case 11:
							{
								strTemp = "Import Employee Health Care";
								LabelNumber[Strings.Asc("B")] = 11;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 12:
							{
								strTemp = "Non-Paid MainePERS Info";
								LabelNumber[Strings.Asc("C")] = 12;
								lngFCode = modGlobalVariables.NONPAIDMSRSINFO;
								break;
							}
						case 13:
							{
								strTemp = "Check Return";
								LabelNumber[Strings.Asc("D")] = 13;
								lngFCode = modGlobalVariables.CHECKRETURN;
								break;
							}
						case 14:
							{
								strTemp = "Contracts";
								LabelNumber[Strings.Asc("E")] = 14;
								lngFCode = modGlobalVariables.CONTRACTWAGES;
								break;
							}
						case 15:
							{
								strTemp = "Edit Pay Records";
								LabelNumber[Strings.Asc("F")] = 15;
								lngFCode = modGlobalVariables.EDITPAYRECORDS;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void ContractWageCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ContractWageCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "CONTRACTWAGES";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Contract Wages]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Load / Edit";
								LabelNumber[Strings.Asc("1")] = 1;
								// lngFCode = CUSTOMIZE
								break;
							}
						case 2:
							{
								strTemp = "Pay off";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.EMPLOYEEEDITSAVE;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, boolDisable, 2);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void ApplicationPrintingActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmEmpInfoSetup.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						frmCustomReport.InstancePtr.Unload();
						modCustomReport.Statics.strCustomTitle = "Custom Employee Report";
						frmCustomReport.InstancePtr.Show(App.MainForm);
						modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "EMPLOYEEINFO");
						break;
					}
				case 3:
					{
						frmCheckListing.InstancePtr.Show(App.MainForm);
						break;
					}
				case 4:
					{
						frmReportViewer.InstancePtr.Init(rptPayTotalsDetail.InstancePtr, boolAllowEmail: true, strAttachmentName: "PayTotals");
						// Case 5
						// Unload frmCustomReport
						// strCustomTitle = "Custom Pay Totals"
						// frmCustomReport.Show
						// Call SetFormFieldCaptions(frmCustomReport, "CUSTOMPAYTOTALS")
						break;
					}
				case 5:
					{
						frmReportViewer.InstancePtr.Init(rptDeductionDetail.InstancePtr, boolAllowEmail: true, strAttachmentName: "DeductionDetail");
						break;
					}
				case 6:
					{
						frmCustomMatchReports.InstancePtr.Show(App.MainForm);
						break;
					}
				case 7:
					{
						frmCustomDeductionReports.InstancePtr.Show(App.MainForm);
						break;
					}
				case 8:
					{
						frmCustomDistributionReports.InstancePtr.Show(App.MainForm);
						break;
					}
				case 9:
					{
						frmCustomReport.InstancePtr.Unload();
						modCustomReport.Statics.strPreSetReport = "";
						//App.DoEvents();
						modCustomReport.Statics.strCustomTitle = "Custom Vac / Sick Report";
						frmCustomReport.InstancePtr.Show(App.MainForm);
						modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "CUSTOMVACSICK");
						break;
					}
				case 10:
					{
						modGlobalVariables.Statics.gboolGroupBy = true;
						frmCustomReport.InstancePtr.Unload();
						modCustomReport.Statics.strCustomTitle = "Custom Check Detail Report";
						frmCustomReport.InstancePtr.Show(App.MainForm);
						modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "CUSTOMCHECKDETAIL");
						break;
					}
				case 11:
					{
						frmCustomLabels.InstancePtr.Show(App.MainForm);
						frmCustomLabels.InstancePtr.Text = "Payroll Custom Labels";
						modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "PAYROLLLABELS");
						break;
					}
				case 12:
					{
						frmCustomRetirementDeductions.InstancePtr.Show(App.MainForm);
						break;
					}
				case 13:
					{
						frmSumCustomCheckDetail.InstancePtr.Show(App.MainForm);
						break;
					}
				case 14:
					{
						frmReportViewer.InstancePtr.Init(rptDirectDepositSetup.InstancePtr, boolAllowEmail: true, strAttachmentName: "DirectDepositSetup");
						break;
					}
				case 15:
					{
						// frmReportViewer.Init rptBLSListing, , , , , , , , , , True, "BLSListing"
						//SetMenuOptions("WORKSITEREPORTS");
						break;
					}
				case 16:
					{
						cACASetupList lSetup;
						cACAService sACA = new cACAService();
						lSetup = sACA.GetAllPermissable(DateTime.Today.Year, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
						if (!(lSetup == null))
						{
							if (!(lSetup.ItemCount() < 1))
							{
								rptHealthCareSetup.InstancePtr.Init(ref lSetup);
								return;
							}
						}
						MessageBox.Show("No set up records were found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						break;
					}
				case 17:
					{
						int intMonth;
						modGlobalVariables.Statics.gboolCancelSelected = false;
						frmDateRange.InstancePtr.Show(FCForm.FormShowEnum.Modal);
						if (modGlobalVariables.Statics.gboolCancelSelected)
						{
							// cancelled
							return;
						}
						rptWCSummary.InstancePtr.Init();
						break;
					}
				case 18:
					{
						frmSetupAuditArchiveReport.InstancePtr.Show(App.MainForm);
						break;
					}
				case 19:
					{
						//SetMenuOptions("MAIN");
						break;
					}
			}
		}

		public void ApplicationPrintingCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PrintingCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "APPLICATIONPRINTING";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Printing]";
				modGlobalVariables.Statics.gboolGroupBy = false;
				for (int intCount = 1; intCount <= 18; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Employee Information";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "Custom Employee";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
						case 3:
							{
								strTemp = "Check Listing";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							}
						case 4:
							{
								strTemp = "Pay Totals Detail";
								LabelNumber[Strings.Asc("4")] = 4;
								// Case 5
								// Grid.TextMatrix(intCount, 1) = "5.  Custom Pay Totals Rpt"
								// LabelNumber(Asc("5")) = 5
								break;
							}
						case 5:
							{
								strTemp = "Deduction Detail";
								LabelNumber[Strings.Asc("5")] = 5;
								break;
							}
						case 6:
							{
								strTemp = "Employers Match";
								LabelNumber[Strings.Asc("6")] = 6;
								break;
							}
						case 7:
							{
								strTemp = "Deduction";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							}
						case 8:
							{
								strTemp = "Distribution";
								LabelNumber[Strings.Asc("8")] = 8;
								break;
							}
						case 9:
							{
								strTemp = "Custom Vac / Sick";
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							}
						case 10:
							{
								strTemp = "Custom Check Detail";
								LabelNumber[Strings.Asc("A")] = 10;
								break;
							}
						case 11:
							{
								strTemp = "Custom Payroll Labels";
								LabelNumber[Strings.Asc("B")] = 11;
								break;
							}
						case 12:
							{
								strTemp = "Custom Retirement Ded";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							}
						case 13:
							{
								strTemp = "Check Detail Summary";
								LabelNumber[Strings.Asc("D")] = 13;
								break;
							}
						case 14:
							{
								strTemp = "Direct Deposit Setup";
								LabelNumber[Strings.Asc("E")] = 14;
								break;
							}
						case 15:
							{
								// Grid.TextMatrix(intCount, 1) = "F.  BLS Worksite ID"
								strTemp = "Worksite Reports";
								LabelNumber[Strings.Asc("F")] = 15;
								break;
							}
						case 16:
							{
								// Grid.TextMatrix(intCount, 1) = "G. Multiple Worksite Summary"
								strTemp = "ACA Setup Report";
								LabelNumber[Strings.Asc("G")] = 16;
								break;
							}
						case 17:
							{
								strTemp = "W/C Summary";
								LabelNumber[Strings.Asc("H")] = 17;
								break;
							}
						case 18:
							{
								strTemp = "Audit Archive";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
					switch (intCount)
					{
						case 15:
							WorksiteReportCaptions(newItem);
							break;
					}
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void BankCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "BankCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "BANK";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Employee Bank File Maintenance]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Add / Update Bank";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "Print Bank Listing";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, boolDisable, 2);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void W2AdditionalInfoCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "CalendarCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// T
				string strTemp = "";
				string menu = "W2ADDITIONALINFO";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [W-2 Additional Information]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Setup Additional W-2 Codes";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.CLEAREICCODES;
								break;
							}
						case 2:
							{
								strTemp = "Additional Emp W-2 Values";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.CLEAREICCODES;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void CalendarActions(int intOption)
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper rsUpdate = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			int intCounter;
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			int x;
			switch (intOption)
			{
				case 2:
					{
						//SetMenuOptions("W2ADDITIONALINFO");
						break;
					}
				case 1:
					{
						modGlobalVariables.Statics.gstrW2Status = "SAVE";
						frmW2.InstancePtr.Show(App.MainForm);
						break;
					}
				case 3:
					{
						frmW2Edit.InstancePtr.Show(App.MainForm);
						break;
					}
				case 5:
					{
						// THIS HAS TO BE DONE BEFORE THE CONTACT INFORMATION SO THAT THE CODES
						// CAN GET SET IN THE TBLW2DEDUCTIONS AND TBLW2MATCHES TABLES
						ans = MessageBox.Show("This Process will merge the data from the Additional Info process with the saved data, do you wish to continue?", "Merge Data?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							// do nothing
						}
						else
						{
							MessageBox.Show("This process has been stopped.", "Process Stopped", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						rsData.OpenRecordset("Select W2MasterSaveDone from tblDefaultInformation", "TWPY0000.vb1");
						if (rsData.EndOfFile())
						{
							MessageBox.Show("Error checking for W-2 Master Information. Do a force update on the database.", "TRIO Software", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
							return;
						}
						else
						{
							if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("W2MasterSaveDone")))
							{
							}
							else
							{
								MessageBox.Show("Save of the Contact screen must be done to get Code fields updated in the deduction and match tables.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
						}
						// MATTHEW 12/16/2004
						// NOW CHECK TO MAKE SURE THAT THE MERGE ROUTINE HASN'T ALREADY BEEN WRITTEN
						rsData.OpenRecordset("Select W2MergeDataDone from tblW2StatusTable");
						if (rsData.EndOfFile())
						{
							modGlobalRoutines.MergeAdditionalW2Info();
							rsData.Execute("Update tblW2StatusTable Set W2MergeDataDone = '" + DateTime.Today.ToShortDateString() + "'", "Payroll");
						}
						else
						{
							if (Information.IsDate(rsData.Get_Fields("W2MergeDataDone")))
							{
								MessageBox.Show("Merge of Additional W-2 Information has already been done. Save of W-2 Information must be done again before merge.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							else
							{
								modGlobalRoutines.MergeAdditionalW2Info();
								rsData.Execute("Update tblW2StatusTable Set W2MergeDataDone = '" + DateTime.Today.ToShortDateString() + "'", "Payroll");
							}
						}
						break;
					}
				case 4:
					{
						// THIS HAS TO BE DONE AFTER THE MERGE SO THAT THE CODES
						// CAN GET SET IN THE TBLW2DEDUCTIONS AND TBLW2MATCHES TABLES
						frmW2Master.InstancePtr.Show(App.MainForm);
						break;
					}
				case 6:
					{
						rsData.OpenRecordset("Select W2MasterSaveDone from tblDefaultInformation", "TWPY0000.vb1");
						if (rsData.EndOfFile())
						{
							MessageBox.Show("Error checking for W-2 Master Information. Do a force update on the database.", "TRIO Software", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
							return;
						}
						else
						{
							if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("W2MasterSaveDone")))
							{
							}
							else
							{
								MessageBox.Show("Save of the Contact screen must be done to get Code fields updated in the deduction and match tables.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
						}
						// ************************************************************
						rsUpdate.Execute("Update tblW2Deductions Set Code = ''", "Payroll");
						rsUpdate.Execute("Update tblW2Matches Set Code = ''", "Payroll");
						// Call rsData.OpenRecordset("Select * from tblW2Box12And14 where Box10 = FALSE")
						rsData.OpenRecordset("Select * from tblW2Box12And14");
						while (!rsData.EndOfFile())
						{
							// If rsData.Fields("Box12") Or rsData.Fields("Box10") Then
							rsUpdate.Execute("Update tblW2Deductions Set Code = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))) + "' Where DeductionNumber = " + rsData.Get_Fields_Int32("DeductionNumber"), "Payroll");
							// ElseIf rsData.Fields("Box14") Then
							rsUpdate.Execute("Update tblW2Matches Set Code = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))) + "' Where DeductionNumber = " + rsData.Get_Fields_Int32("DeductionNumber"), "Payroll");
							// End If
							rsData.MoveNext();
						}
						// ***************************************************************
						frmReportViewer.InstancePtr.Init(rptW2EditReport.InstancePtr);
						break;
					}
				case 7:
					{
						int lngYear = 0;
						rsData.OpenRecordset("Select W2MasterSaveDone from tblDefaultInformation", "TWPY0000.vb1");
						if (rsData.EndOfFile())
						{
							MessageBox.Show("Error checking for W-2 Master Information. Do a force update on the database.", "TRIO Software", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
							return;
						}
						else
						{
							if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("W2MasterSaveDone")))
							{
							}
							else
							{
								MessageBox.Show("Save of the Contact screen must be done to get Code fields updated in the deduction and match tables.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
						}
						// ************************************************************
						// MAKE SURE THAT THE CODES ARE CORRECT
						rsUpdate.Execute("Update tblW2Deductions Set Code = ''", "Payroll");
						rsUpdate.Execute("Update tblW2Matches Set Code = ''", "Payroll");
						// Call rsData.OpenRecordset("Select * from tblW2Box12And14 where Box10 = FALSE")
						rsData.OpenRecordset("Select * from tblW2Box12And14");
						while (!rsData.EndOfFile())
						{
							// If rsData.Fields("Box12") Or rsData.Fields("Box10") Then
							rsUpdate.Execute("Update tblW2Deductions Set Code = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))) + "' Where DeductionNumber = " + rsData.Get_Fields_Int32("DeductionNumber"), "Payroll");
							// ElseIf rsData.Fields("Box14") Then
							rsUpdate.Execute("Update tblW2Matches Set Code = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))) + "' Where DeductionNumber = " + rsData.Get_Fields_Int32("DeductionNumber"), "Payroll");
							// End If
							rsData.MoveNext();
						}
						// ***************************************************************
						// PRINT THE W2'S
						modGlobalVariables.Statics.W3Information.dblTotalWages = 0;
						modGlobalVariables.Statics.W3Information.dblFederalTax = 0;
						modGlobalVariables.Statics.W3Information.dblFicaWages = 0;
						modGlobalVariables.Statics.W3Information.dblFicaTax = 0;
						modGlobalVariables.Statics.W3Information.dblMedWages = 0;
						modGlobalVariables.Statics.W3Information.dblMedTaxes = 0;
						modGlobalVariables.Statics.W3Information.dblStateWages = 0;
						modGlobalVariables.Statics.W3Information.dblStateTax = 0;
						modGlobalVariables.Statics.W3Information.dblThirdPartySick = 0;
						if (MessageBox.Show("Print All W-2s?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							modGlobalVariables.Statics.gstrPrintAllW2s = FCConvert.ToString(0);
						}
						else
						{
							modGlobalVariables.Statics.gstrPrintAllW2s = Interaction.InputBox("Enter employee number for single W-2.", "Print single W-2", null);
						}
						if (modGlobalVariables.Statics.gstrPrintAllW2s == string.Empty)
							return;
						rsData.OpenRecordset("Select * from tblDefaultInformation");
						if (rsData.EndOfFile())
						{
							rptW2Laser.InstancePtr.Init();
						}
						else
						{
							switch (rsData.Get_Fields_Int32("W2CheckType"))
							{
								case 0:
									{
										rptW2Laser.InstancePtr.Init();
										break;
									}
								case 4:
									{
										rptW2Laser2x2.InstancePtr.Init();
										break;
									}
							}
							//end switch
						}
						object ControlName;
						clsDRWrapper rsW2EditTable = new clsDRWrapper();
						clsDRWrapper rsW2Archive = new clsDRWrapper();
						rsW2EditTable.OpenRecordset("Select * from tblW2EditTable", "TWPY0000.vb1");
						if (!rsW2EditTable.EndOfFile())
						{
							rsW2Archive.OpenRecordset("Select * from tblW2Archive Where [Year] = " + FCConvert.ToString(Conversion.Val(rsW2EditTable.Get_Fields("Year"))), "TWPY0000.vb1");
							if (rsW2Archive.EndOfFile())
							{
							}
							else
							{
								if (MessageBox.Show("Do you wish to replace archive information for the year: " + rsW2EditTable.Get_Fields("Year") + "?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									rsW2Archive.Execute("Delete from tblW2Archive Where Year = " + rsW2EditTable.Get_Fields("Year"), "TWPY0000.vb1");
									rsW2Archive.Execute("delete from tblw2archivedeductions where [year] = " + rsW2EditTable.Get_Fields("year"), "twpy0000.vb1");
									rsW2Archive.Execute("delete from tblw2archivematches where [year] = " + rsW2EditTable.Get_Fields("year"), "twpy0000.vb1");
								}
								else
								{
									goto DontSave;
								}
							}
							// SEND ALL OF THE INFORMATION TO THE ARCHIVE FILE
							frmWait.InstancePtr.Init("Saving W-2 information to Archive file", false);
							frmWait.InstancePtr.Show(App.MainForm);
							System.Threading.Thread.Sleep(1000);
                            lngYear = 0;
							rsW2Archive.OpenRecordset("Select * from tblW2Archive Where ID = 1", "TWPY0000.vb1");
							while (!rsW2EditTable.EndOfFile())
							{
								//App.DoEvents();
								rsW2Archive.AddNew();
								for (x = 0; x <= rsW2EditTable.FieldsCount - 1; x++)
								{
									//App.DoEvents();
									if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) != "ID")
									{
										if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) == "YEAR")
										{
											if (Conversion.Val(rsW2EditTable.Get_Fields("Year")) > 0)
											{
												lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsW2EditTable.Get_Fields("Year"))));
											}
											rsW2Archive.Set_Fields("Year", lngYear);
										}
										else
										{
											if (!fecherFoundation.FCUtils.IsNull(rsW2EditTable.Get_Fields(rsW2EditTable.Get_FieldsIndexName(x))) && FCConvert.ToString(rsW2EditTable.Get_FieldsIndexValue(x)) != string.Empty)
											{
												rsW2Archive.Set_Fields(rsW2EditTable.Get_FieldsIndexName(x), rsW2EditTable.Get_FieldsIndexValue(x));
											}
										}
									}
								}
								// x
								rsW2Archive.Update();
								if (!rsW2EditTable.EndOfFile())
									rsW2EditTable.MoveNext();
							}
							rsW2Archive.OpenRecordset("select * from tblw2archivedeductions where ID = 0", "twpy0000.vb1");
							rsW2EditTable.OpenRecordset("select * from tblw2deductions", "twpy0000.vb1");
							while (!rsW2EditTable.EndOfFile())
							{
								//App.DoEvents();
								rsW2Archive.AddNew();
								for (x = 0; x <= rsW2EditTable.FieldsCount - 1; x++)
								{
									if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) != "ID")
									{
										if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) == "YEAR")
										{
											rsW2Archive.Set_Fields("Year", lngYear);
										}
										else
										{
											if (FCConvert.ToString(rsW2EditTable.Get_FieldsIndexValue(x)) != string.Empty)
											{
												rsW2Archive.Set_Fields(rsW2EditTable.Get_FieldsIndexName(x), rsW2EditTable.Get_FieldsIndexValue(x));
											}
										}
									}
								}
								// x
								rsW2Archive.Update();
								rsW2EditTable.MoveNext();
							}
							rsW2Archive.OpenRecordset("select * from tblw2archivematches where ID = 0", "twpy0000.vb1");
							rsW2EditTable.OpenRecordset("select * from tblw2matches", "twpy0000.vb1");
							while (!rsW2EditTable.EndOfFile())
							{
								//App.DoEvents();
								rsW2Archive.AddNew();
								for (x = 0; x <= rsW2EditTable.FieldsCount - 1; x++)
								{
									//App.DoEvents();
									if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) != "ID")
									{
										if (fecherFoundation.Strings.UCase(rsW2EditTable.Get_FieldsIndexName(x)) == "YEAR")
										{
											rsW2Archive.Set_Fields("Year", lngYear);
										}
										else
										{
											if (FCConvert.ToString(rsW2EditTable.Get_FieldsIndexValue(x)) != string.Empty)
											{
												rsW2Archive.Set_Fields(rsW2EditTable.Get_FieldsIndexName(x), rsW2EditTable.Get_FieldsIndexValue(x));
											}
										}
									}
								}
								// x
								rsW2Archive.Update();
								rsW2EditTable.MoveNext();
							}
							frmWait.InstancePtr.Unload();
							MessageBox.Show("Data Archived", "Archived", MessageBoxButtons.OK, MessageBoxIcon.Information);
							DontSave:
							;
							// ASK IF THEY WANT THE INFORMATION IN THE EDIT TABLE TO BE DELETED
							// If MsgBox("Do you want to clear out the W-2 Edit tables?", vbQuestion + vbYesNo) = vbYes Then
							// Call rsW2EditTable.Execute("Delete from tblW2EditTable", "TWPY0000.vb1")
							rsData.Execute("Update tblW2StatusTable SET SaveInformation = NULL", "TWPY0000.vb1");
							// End If
						}
						modGlobalRoutines.UpdateW2StatusData("Print");
						frmWait.InstancePtr.Unload();
						break;
					}
				case 8:
					{
						rsData.OpenRecordset("Select * from tblW2Master");
						if (rsData.EndOfFile())
						{
							MessageBox.Show("Employee Pin on the W-2 Contact Information screen must be filled in to complete electronic filing of W-2s.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("EmployeePIN"))) == string.Empty)
						{
							MessageBox.Show("Employee Pin on the W-2 Contact Information screen must be filled in to complete electronic filing of W-2s.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						else
						{
							modCoreysSweeterCode.CreateElectronicW2File();
							modGlobalRoutines.UpdateW2StatusData("CreateElectronic");
						}
						break;
					}
				case 9:
					{
						Update941W2Info();
						break;
					}
				case 10:
					{
						rptW2Adjustments.InstancePtr.Init();
						break;
					}
				case 11:
					{
						frmW2CList.InstancePtr.Show(App.MainForm);
						break;
					}
				case 12:
					{
						//SetMenuOptions("ACAFILING");
						break;
					}
			}
		}

		public void CalendarCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "CalendarCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "CALENDAR";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Calendar End Of Year]";
				for (int intCount = 1; intCount <= 12; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
						{
							strTemp = "Save W-2 Information";
							LabelNumber[Strings.Asc("1")] = 1;
							break;
						}
						case 2:
						{
							strTemp = "W-2 Additional Information";
							LabelNumber[Strings.Asc("2")] = 2;
							break;
						}
						case 3:
						{
							strTemp = "Update W-2 Information";
							LabelNumber[Strings.Asc("3")] = 3;
							break;
						}
						case 4:
						{
							strTemp = "Edit W-2 Contact Info";
							LabelNumber[Strings.Asc("4")] = 4;
							break;
						}
						case 5:
						{
							strTemp = "Merge Additional W-2 Info";
							LabelNumber[Strings.Asc("5")] = 5;
							break;
						}
						case 6:
						{
							strTemp = "Print W-2 Edit Report";
							LabelNumber[Strings.Asc("6")] = 6;
							break;
						}
						case 7:
						{
							strTemp = "Print W-2s";
							LabelNumber[Strings.Asc("7")] = 7;
							break;
						}
						case 8:
						{
							strTemp = "Create Electronic W-2";
							LabelNumber[Strings.Asc("8")] = 8;
							break;
						}
						case 9:
						{
							strTemp = "Update Adjustment Records";
							LabelNumber[Strings.Asc("9")] = 9;
							break;
						}
						case 10:
						{
							strTemp = "Adjustment Report";
							LabelNumber[Strings.Asc("A")] = 10;
							break;
						}
						case 11:
						{
							strTemp = "W-2C";
							LabelNumber[Strings.Asc("B")] = 11;
							break;
						}
						case 12:
						{
							strTemp = "Health Care Filing";
							LabelNumber[Strings.Asc("C")] = 12;
							lngFCode = modGlobalVariables.CNSTHealthcareSetup;
							break;
						}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
					switch (intCount)
					{
						case 2:
							W2AdditionalInfoCaptions(newItem);
							break;
						case 12:
							ACAFilingCaptions(newItem);
							break;
					}
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void TimeClockActions(int intMenu)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				switch (intMenu)
				{
					case 1:
						{
							frmTimeCardSetup.InstancePtr.Show(App.MainForm);
							break;
						}
					case 2:
						{
							frmTimeCardShifts.InstancePtr.Show(App.MainForm);
							break;
						}
					case 3:
						{
							//SetMenuOptions("SETUP");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In TimeClockActions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupActions(int intMenu)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				switch (intMenu)
				{
					case 1:
						{
							frmDeductionSetup.InstancePtr.Show(App.MainForm);
							break;
						}
					case 2:
						{
                            //modGlobalVariables.Statics.gstrActiveTaxTableCode = string.Empty;
                            //frmPayStatuses.InstancePtr.Show();
                            StaticSettings.GlobalCommandDispatcher.Send(new ShowTaxTables());
							break;
						}
					case 3:
						{
							frmPayCategories.InstancePtr.Show(App.MainForm);
							break;
						}
					case 4:
						{
							frmStandardLimits.InstancePtr.Show(App.MainForm);
							break;
						}
					case 5:
						{
							frmVacationCodes.InstancePtr.Show();
							break;
						}
					case 6:
						{
							frmCheckRecipients.InstancePtr.Show();
							break;
						}
					case 7:
						{
							frmCheckRecipientsCategories.InstancePtr.Show(App.MainForm);
							break;
						}
					case 8:
						{
							frmPayrollAccountsSetup.InstancePtr.Show(App.MainForm);
							break;
						}
					case 9:
						{
							// frmSchoolPayrollAccounts.Show , MDIParent
							break;
						}
					case 10:
						{
							frmReportSelection.InstancePtr.Show(App.MainForm);
							break;
						}
					case 11:
						{
							frmEmployeeDataEntry.InstancePtr.Show(App.MainForm);
							break;
						}
					case 12:
						{
							frmPayCodes.InstancePtr.Show(App.MainForm);
							break;
						}
					case 13:
						{
							frmClearLTDDeductions.InstancePtr.Show(App.MainForm);
							break;
						}
					case 14:
						{
							if (MessageBox.Show("Are you sure you wish to set ALL seasonal unemployment (S) people to regular unemployment (Y)?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								modGlobalFunctions.AddCYAEntry_6("PY", "Ran the function that sets all Seasonal unemployment people to regular.");
								frmReportViewer.InstancePtr.Init(rptSetSeasonalUnempY.InstancePtr, boolAllowEmail: true, strAttachmentName: "SeasonalUnemployment");
							}
							break;
						}
					case 15:
						{
							frmPurgeDatabaseData.InstancePtr.Show(App.MainForm);
							break;
						}
					case 16:
						{
							//GRID.RowHidden(16, false);
							//SetMenuOptions("SCHEDULES");
							break;
						}
					case 17:
						{
							//GRID.RowHidden(16, false);
							//SetMenuOptions("TIMECARD");
							break;
						}
					case 18:
						{
							//GRID.RowHidden(16, false);
							frmCoverageProvider.InstancePtr.Show();
							break;
						}
					case 19:
						{
							//GRID.RowHidden(16, false);
							//SetMenuOptions("MAIN");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupActions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


        public void SetupCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetupCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "SETUP";
				bool boolDisable = false;
                bool hidden = false;
				App.MainForm.Text = strTrio + "     [Table and File Setups]";
				for (int intCount = 1; intCount <= 18; intCount++)
				{
					lngFCode = 0;
					boolDisable = false;
                    hidden = false;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Deduction Setup";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.DEDUCTIONSETUP;
								break;
							}
						case 2:
							{
								strTemp = "Tax Tables";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
						case 3:
							{
								strTemp = "Pay Category Setup";
								LabelNumber[Strings.Asc("3")] = 3;
								lngFCode = modGlobalVariables.PAYCATEGORYSETUP;
								break;
							}
						case 4:
							{
								strTemp = "Standard Limits/Rates";
								LabelNumber[Strings.Asc("4")] = 4;
								lngFCode = modGlobalVariables.STANDARDLIMITSRATES;
								break;
							}
						case 5:
							{
								strTemp = "Vacation/Sick/Other Codes";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.VACATIONSICKOTHERCODES;
								break;
							}
						case 6:
							{
								strTemp = "Check Recipients";
								LabelNumber[Strings.Asc("6")] = 6;
								lngFCode = modGlobalVariables.CHECKRECIPIENTSSECURITY;
								break;
							}
						case 7:
							{
								strTemp = "Check Recipients Categories";
								LabelNumber[Strings.Asc("7")] = 7;
								lngFCode = modGlobalVariables.CHECKRECIPIENTSCATEGORIES;
								break;
							}
						case 8:
							{
								strTemp = "Setup Payroll Accounts";
								LabelNumber[Strings.Asc("8")] = 8;
								lngFCode = modGlobalVariables.SETUPPAYROLLACCOUNTS;
								break;
							}
						case 9:
							{
								strTemp = "Object Codes for Sal/Ben";
                                boolDisable = true;
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							}
						case 10:
							{
								strTemp = "Full Set Report Selection";
								LabelNumber[Strings.Asc("A")] = 10;
								lngFCode = modGlobalVariables.REPORTSELECTIONS;
								break;
							}
						case 11:
							{
								strTemp = "Data Entry Selection";
								LabelNumber[Strings.Asc("B")] = 11;
								lngFCode = modGlobalVariables.DATAENTRYSELECTION;
								break;
							}
						case 12:
							{
								strTemp = "Update Pay Codes";
								LabelNumber[Strings.Asc("C")] = 12;
								lngFCode = modGlobalVariables.UPDATEPAYCODES;
								break;
							}
						case 13:
							{
								strTemp = "Clear LTD Deductions";
								LabelNumber[Strings.Asc("D")] = 13;
								lngFCode = modGlobalVariables.CLEARLTDDEDUCTIONS;
								break;
							}
						case 14:
							{
								// Grid.TextMatrix(intCount, 1) = "G. Reset Statutory Emp"
								strTemp = "Change Unemployment S-Y";
								LabelNumber[Strings.Asc("E")] = 14;
								lngFCode = modGlobalVariables.RESETUNEMPLOYMENT;
								break;
							}
						case 15:
							{
								strTemp = "Purge Database Data";
								LabelNumber[Strings.Asc("F")] = 15;
								lngFCode = modGlobalVariables.PURGEDATABASEDATA;
								break;
							}
						case 16:
							{
								strTemp = "Schedules";
								LabelNumber[Strings.Asc("G")] = 16;
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "PENOBSCOT COUNTY")
								{
									boolDisable = true;
                                    hidden = true;
								}
								break;
							}
						case 17:
							{
								strTemp = "Time Clock Setup";
								LabelNumber[Strings.Asc("H")] = 17;
								if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "PENOBSCOT COUNTY" && !modCoreysSweeterCode.Statics.gboolTimeClockPlus)
								{
									boolDisable = true;
								}
								break;
							}
						case 18:
							{
								strTemp = "ACA Coverage Setup";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
                    if (!hidden)
                    {
                        FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
                        switch (intCount)
                        {
                            case 16:
                                ScheduleCaptions(newItem);
                                break;
                            case 17:
                                TimeCardCaptions(newItem);
                                break;
                        }
                    }
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void ProcessCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PrintCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "PROCESS";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Payroll Processing]";
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblPayrollProcessSetup");
				if (rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gintCurrentPayRun = 1;
					modGlobalVariables.Statics.gdatCurrentPayDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					modGlobalVariables.Statics.gdatCurrentWeekEndingDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					modGlobalVariables.Statics.gintWeekNumber = 1;
				}
				else
				{
					modGlobalVariables.Statics.gintCurrentPayRun = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("PayRunID"))));
					modGlobalVariables.Statics.gdatCurrentPayDate = FCConvert.ToDateTime(Strings.Format(rsData.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy"));
					if (Information.IsDate(rsData.Get_Fields("weekenddate")))
					{
						modGlobalVariables.Statics.gdatCurrentWeekEndingDate = FCConvert.ToDateTime(Strings.Format(rsData.Get_Fields("weekenddate"), "MM/dd/yyyy"));
					}
					else
					{
						modGlobalVariables.Statics.gdatCurrentWeekEndingDate = modGlobalVariables.Statics.gdatCurrentPayDate;
					}
					modGlobalVariables.Statics.gintWeekNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("WeekNumber"))));
				}

                int menuNumber = 0;
				for (int intCount = 1; intCount <= 9; intCount++)
				{
					lngFCode = 0;
                    menuNumber = intCount;
					boolDisable = false;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "File Setup (1 Per Pay Period)";
								LabelNumber[Strings.Asc("1")] = 1;
								// Call DisableMenuOption(False, intCount)
								lngFCode = modGlobalVariables.FILESETUP;
								break;
							}
						case 2:
							{
								strTemp = "Data Entry";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.DATAENTRYSECURITY;
								break;
							}
						case 3:
							{
								strTemp = "Data Edit & Calculation";
								LabelNumber[Strings.Asc("3")] = 3;
								lngFCode = modGlobalVariables.DATAEDITANDCALCULATION;
								break;
							}
						case 4:
							{
								strTemp = "Verify & Accept Calculations";
								LabelNumber[Strings.Asc("4")] = 4;
								// Call DisableMenuOption(False, intCount)
								lngFCode = modGlobalVariables.VERIFYANDACCEPT;
								break;
							}
						case 5:
							{
								strTemp = "Checks";
								LabelNumber[Strings.Asc("5")] = 5;
								// Call DisableMenuOption(False, intCount)
								lngFCode = modGlobalVariables.CHECKS;
								break;
							}
						case 6:
							{
								strTemp = "Email Paychecks";
								LabelNumber[Strings.Asc("6")] = 6;
								lngFCode = modGlobalVariables.CHECKS;
								// Else
								// Call DisableMenuOption(False, intCount)
								// End If
								break;
							}
						//case 7:
						//	{
						//		strTemp = "ESP Create & Upload";
						//		LabelNumber[Strings.Asc("7")] = 7;
						//		if (!modCoreysSweeterCode.Statics.gboolESP && !(StaticSettings.gGlobalSettings.IsHarrisStaffComputer) || ssnViewPermission != SSNViewType.Full)
						//		{
						//			boolDisable = false;
						//		}
						//		else
						//		{
      //                             lngFCode = modGlobalVariables.CHECKS;
      //                          }
						//		break;
						//	}
						case 7:
                        {
                            menuNumber = 8;
								strTemp = "Create TA ACH Files";
								LabelNumber[Strings.Asc("8")] = 8;
                                if (bankAccountViewPermission != BankAccountViewType.Full)
                                {
                                    boolDisable = true;
                                }
                                else
                                {
                                    lngFCode = modGlobalVariables.CHECKS;
                                }
                                break;
							}
						case 8:
                        {
                            menuNumber = 9;
								strTemp = "Reports & Cleanup Routines";
								LabelNumber[Strings.Asc("9")] = 9;
								// Call DisableMenuOption(False, intCount)
								lngFCode = modGlobalVariables.PAYROLLPROCESSREPORTS;
								break;
							}
						case 9:
                        {
                            menuNumber = 10;
								strTemp = "Process Status";
								LabelNumber[Strings.Asc("A")] = 10;
								// Call DisableMenuOption(False, intCount)
								lngFCode = modGlobalVariables.PAYROLLPROCESSSTATUS;
								break;
							}
					}
					if (lngFCode != 0)
					{
						if (boolAllEmployeesAllowed || (menuNumber == 2 || menuNumber == 3 || menuNumber == 10 || menuNumber == 11))
						{
							boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
						}
						else
						{
							boolDisable = false;
						}
					}
					var menuItem = parent.SubItems.Add(strTemp, "Menu" + menuNumber, menu, !boolDisable, 2);
                    switch(menuNumber)
                    {
                        case 9:
                            ProcessReportCaptions(menuItem);
                            break;
                    }
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

        private void ProcessActions(int intMenu)
        {
            try
            {
                switch (intMenu)
                {
                    case 1:
                    {
                        // CHECK TO SEE if they have created check records but not print checks
                        clsDRWrapper rsLoad = new clsDRWrapper();
                        DateTime dtPayDate;
                        int intPRun = 0;
                        rsLoad.OpenRecordset("select * from tblpayrollprocesssetup", "twpy0000.vb1");
                        if (!rsLoad.EndOfFile())
                        {
                            if (Information.IsDate(rsLoad.Get_Fields("paydate")))
                            {
                                if (Convert.ToDateTime(rsLoad.Get_Fields("paydate")).ToOADate() != 0)
                                {
                                    dtPayDate = (DateTime) rsLoad.Get_Fields("paydate");
                                    intPRun = FCConvert.ToInt32(
                                        Math.Round(Conversion.Val(rsLoad.Get_Fields("payrunid"))));
                                    rsLoad.OpenRecordset(
                                        "select * from tblpayrollsteps where paydate = '" +
                                        FCConvert.ToString(dtPayDate) + "' and payrunid = " +
                                        FCConvert.ToString(intPRun), "twpy0000.vb1");
                                    if (!rsLoad.EndOfFile())
                                    {
                                        if (rsLoad.Get_Fields_Boolean("verifyaccept") &&
                                            !FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("checks")))
                                        {
                                            // check to see if there are any checks out there
                                            rsLoad.OpenRecordset(
                                                "select top 1 * from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and not adjustrecord = 1 and checknumber = 0 and paydate = '" +
                                                FCConvert.ToString(dtPayDate) + "' and payrunid = " +
                                                FCConvert.ToString(intPRun), "twpy0000.vb1");
                                            if (!rsLoad.EndOfFile())
                                            {
                                                // there are blank checks out there. Check to see if there are legit checks
                                                rsLoad.OpenRecordset(
                                                    "select top 1 * from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and not adjustrecord = 1 and checknumber > 0 and paydate = '" +
                                                    FCConvert.ToString(dtPayDate) + "' and payrunid = " +
                                                    FCConvert.ToString(intPRun), "twpy0000.vb1");
                                                if (rsLoad.EndOfFile())
                                                {
                                                    MessageBox.Show(
                                                        "You have completed verify and accept for the last payrun but have not run checks" +
                                                        "\r\n" + "Cannot continue", "Checks Not Run",
                                                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        frmPayrollProcessSetup.InstancePtr.Show(App.MainForm);
                        break;
                    }
                    case 2:
                    {
                        modGlobalConstants.Statics.gboolASKPermission = true;
                        if (modGlobalRoutines.CheckIfPayProcessCompleted("FileSetup", "DataEntry"))
                        {
                            if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "DataEntry", true))
                            {
                                MessageBox.Show(
                                    "Pending reports have not been run for the current pay date. Finish running reports or select file setup.",
                                    "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                modGlobalVariables.Statics.gstrEmployeeScreen = "frmPayrollDistribution";
                                modGlobalVariables.Statics.gboolDataEntry = true;
                                frmEmployeeListing.InstancePtr.Show(App.MainForm);
                            }
                        }

                        break;
                    }
                    case 3:
                    {
                        if(!WasPayrollProcessCompleted("DataEntry","EditTotals"))
                        {
                            FCMessageBox.Show("Step: Edit Totals cannot be done until Data Entry has been completed",MsgBoxStyle.Information | MsgBoxStyle.OkOnly,"TrioSoftware");
                            return;
                        }
                        if (WasPayrollProcessCompleted("VerifyAccept", "DataEntry"))
                        {
                            MessageBox.Show(
                                "Pending reports have not been run for the current pay date. Finish running reports or select file setup.",
                                "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                        }             
                            frmProcessEditTotals.InstancePtr.Unload();
                            frmProcessEditTotals.InstancePtr.Show(App.MainForm);
                        break;
                    }
                    case 4:
                    {
                        if (modGlobalRoutines.CheckIfPayProcessCompleted("Calculation", "VerifyAccept"))
                        {
                            if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "VerifyAccept", true))
                            {
                                MessageBox.Show("Verify and Accept has already been run for the current pay date.",
                                    "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                frmPayrollProcessSetup.InstancePtr.Unload();
                                frmEmployeeListing.InstancePtr.Unload();
                                frmProcessEditTotals.InstancePtr.Unload();
                                frmProcessDisplay.InstancePtr.Unload();
                                frmProcessVerify.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                            }
                        }

                        break;
                    }
                    case 5:
                    {
                        if (modGlobalRoutines.CheckIfPayProcessCompleted("VerifyAccept", "Checks"))
                        {
                            frmCheckSequence.InstancePtr.Show(App.MainForm);
                        }

                        break;
                    }
                    case 6:
                    {
                        frmEmailPayrollChecks.InstancePtr.Show(App.MainForm);
                        break;
                    }
                    case 7:
                    {
                        frmESHPExport.InstancePtr.Show(App.MainForm);
                        break;
                    }
                    case 8:
                    {
                        frmCreateACHFiles.InstancePtr.Init();
                        break;
                    }
                    case 9:
                    {
                        App.MainForm.NavigationMenu.CurrentItem.AllowExpand = true;
                        if (modGlobalRoutines.CheckIfPayProcessCompleted("Checks", "Reports", true))
                        {
                            if (SetCheckProcessMenuOptions())
                            {
                                ProcessReportCaptions(App.MainForm.NavigationMenu.CurrentItem);
                                //SetMenuOptions("PROCESSREPORTS");
                            }
                            else
                            {
                                //FC:FINAl:SBE - do not expand the node, if selection was canceled
                                App.MainForm.NavigationMenu.CurrentItem.AllowExpand = false;
                            }
                        }
                        else
                        {
                            if (SetCheckProcessMenuOptions(4))
                            {
                                ProcessReportCaptions(App.MainForm.NavigationMenu.CurrentItem);
                                //SetMenuOptions("PROCESSREPORTS");
                            }
                            else
                            {
                                //FC:FINAl:SBE - do not expand the node, if selection was canceled
                                App.MainForm.NavigationMenu.CurrentItem.AllowExpand = false;
                            }
                        }

                        break;
                    }
                    case 10:
                    {
                        frmPayrollSteps.InstancePtr.Show(App.MainForm);
                        break;
                    }
                    case 11:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
                    case 12:
                    {
                        break;
                    }
                }

                //end switch
            }
            catch (Exception ex)
            {
                var desc = ex.Message;
            }
        }

		public void DeductionCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "DeductionCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "DEDUCTIONREPORTS";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Payroll Processing]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Deduction Detail Listing";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "Custom Deduction Reports";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, boolDisable, 2);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void ProcessReportActions(ref int intMenuOption)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PrintActions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int lngYear = 0;
				modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet = false;
				modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName = string.Empty;
				modGlobalVariables.Statics.gtypeFullSetReports.strFullSetPort = string.Empty;
				switch (intMenuOption)
				{
					case 1:
						{
							modGlobalRoutines.RunFullSetOfReports();
							break;
						}
					case 2:
						{
							RptTrustAndAgencyCodes.InstancePtr.Init();
							break;
						}
					case 3:
						{
							frmCheckRegister.InstancePtr.Show(App.MainForm);
							break;
						}
					case 4:
						{
							modCustomReport.Statics.strPreSetReport = "PAYSUMMARY";
							frmCustomReport.InstancePtr.Show(App.MainForm);
							modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "PAYSUMMARY");
							frmCustomReport.InstancePtr.cmdPrint_Click();
							frmCustomReport.InstancePtr.Unload();
							break;
						}
					case 5:
						{
							modCustomReport.Statics.strPreSetReport = "EMPLOYEEDEDUCTIONS";
							// frmPrintParameters.Show , MDIParent
							rptEmployeeDeductionReport.InstancePtr.Init();
							break;
						}
					case 6:
						{
							rptDirectDeposit.InstancePtr.Init();
							break;
						}
					case 7:
						{
							// rptPayrollAccountingCharges.Init
							rptPayrollAccountingChargesPreview.InstancePtr.Init(false, false);
							break;
						}
					case 8:
						{
							modCustomReport.Statics.strPreSetReport = "DATAENTRYFORMS";
							modCustomReport.Statics.strCustomTitle = "**** Data Entry Forms ****";
							frmCustomReport.InstancePtr.Show(App.MainForm);
							modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DATAENTRYFORMS");
							frmCustomReport.InstancePtr.cmdPrint_Click();
							frmCustomReport.InstancePtr.Unload();
							break;
						}
					case 9:
						{
							modCustomReport.Statics.strPreSetReport = "EMPLOYEEVACATION";
							frmPrintParameters.InstancePtr.Show(App.MainForm);
							break;
						}
					case 10:
						{
							frmWarrant.InstancePtr.Show(App.MainForm);
							break;
						}
					case 11:
						{
							break;
						}
					case 12:
						{
							break;
						}
					case 13:
						{
							frmReportViewer.InstancePtr.Init(rptTempFlagReset.InstancePtr);
							//rptTempFlagReset.InstancePtr.Show(App.MainForm);
							break;
						}
					case 14:
						{
							rpt941.InstancePtr.Init();
							break;
						}
					case 15:
						{
							rpt940.InstancePtr.Init();
							break;
						}
					case 16:
						{
							frmMSRSSetup.InstancePtr.Show(App.MainForm);
							break;
						}
					case 17:
						{
							//SetMenuOptions("UNEMPLOYMENT");
							break;
						}
					case 18:
						{
							if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
							{
								//FC:FINAL:DSE:#i2421 Use date parameter by reference
								DateTime tmpArg = DateTime.FromOADate(0);
								frmSelectDateInfo.InstancePtr.Init4("Input FUTA Year", ref lngYear, -1, -1, ref tmpArg, -1, false);
							}
							else
							{
								lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
							}
							if (lngYear > 0)
							{
								rptFUTA940Report.InstancePtr.Init(lngYear);
							}
							break;
						}
					case 19:
						{
							//SetMenuOptions("PROCESS");
							break;
						}
				}
				if (lngFCode != 0)
				{
					DisableMenuOption(modSecurity.ValidPermissions(this, ref lngFCode, false), intMenuOption);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ProcessReportCaptions(FCMenuItem parent)
		{
			try
			{
                parent.Node.Nodes.Clear();
                if (strFullSetDesc == "")
                {
                    parent.Node.Nodes.Add("dummy");
                    return;
                }
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PrintCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "PROCESSREPORTS";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Reports & Cleanup Routines]";
				for (int intCount = 1; intCount <= 18; intCount++)
				{
					lngFCode = 0;
					boolDisable = false;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Full Set   (" + strFullSetDesc + ")";
								LabelNumber[Strings.Asc("1")] = 1;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.FULLSET];
								break;
							}
						case 2:
							{
								strTemp = "Trust & Agency Report";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.TRUST];
								break;
							}
						case 3:
							{
								strTemp = "Check Register";
								LabelNumber[Strings.Asc("3")] = 3;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.CHECKREG];
								break;
							}
						case 4:
							{
								strTemp = "Pay & Tax Summary";
								LabelNumber[Strings.Asc("4")] = 4;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.PAYTAX];
								break;
							}
						case 5:
							{
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("5")] = 5;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.DEDUCTION];
								break;
							}
						case 6:
							{
								strTemp = "Direct Deposit";
								LabelNumber[Strings.Asc("6")] = 6;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.DIRECTDEPOSIT] || bankAccountViewPermission != BankAccountViewType.Full;
								break;
							}
						case 7:
							{
								strTemp = "Payroll Accounting Charges";
								LabelNumber[Strings.Asc("7")] = 7;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.ACCOUNTING];
								break;
							}
						case 8:
							{
								strTemp = "Data Entry Forms";
								LabelNumber[Strings.Asc("8")] = 8;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.DATAENTRY];
								break;
							}
						case 9:
							{
								strTemp = "Vacation / Sick / Other";
								LabelNumber[Strings.Asc("9")] = 9;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.VACSICK];
								break;
							}
						case 10:
							{
								strTemp = "Payroll Warrant";
								LabelNumber[Strings.Asc("A")] = 10;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.WARRANT];
								break;
							}
						case 11:
							{
								strTemp = "Audit Report";
								LabelNumber[Strings.Asc("B")] = 11;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.AUDIT];
								break;
							}
						case 12:
							{
								strTemp = "Clean Up Routines";
								LabelNumber[Strings.Asc("C")] = 12;
								// Call DisableMenuOption(True, intCount)
								if (strFullSetDesc != "Individual" && strFullSetDesc != "Weekly")
								{
									boolDisable = true;
								}
								break;
							}
						case 13:
							{
								strTemp = "FTD (941) - FICA/Med/Fed";
								LabelNumber[Strings.Asc("D")] = 13;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941];
								break;
							}
						case 14:
							{
								strTemp = "FTD (940) - FUTA";
								LabelNumber[Strings.Asc("E")] = 14;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940];
								break;
							}
						case 15:
							{
								strTemp = "Retirement Reports";
								LabelNumber[Strings.Asc("F")] = 15;
								if (strFullSetDesc == "Yearly")
								{
									boolDisable = true;
								}
								break;
							}
						case 16:
							{
								strTemp = "Federal 941";
								LabelNumber[Strings.Asc("G")] = 16;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.WAGE941];
								break;
							}
						case 17:
							{
								strTemp = "State 941";
								LabelNumber[Strings.Asc("H")] = 17;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATE941] || ssnViewPermission != SSNViewType.Full;
								break;
							}
						case 18:
							{
								strTemp = "Unemployment Reports";
								LabelNumber[Strings.Asc("I")] = 18;
								if (strFullSetDesc == "Weekly" || strFullSetDesc == "Monthly")
								{
									// strFullSetDesc = "Yearly"
									boolDisable = true;
								}
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					//parent.SubItems.Add(strTemp, "Menu" + intCount, menu, boolDisable, 2);
                    FCMenuItem item = new FCMenuItem(strTemp, "Menu" + intCount, menu, !boolDisable, 2, "");
                    FCRigthTreeNode subNode = item.CreateTreeNode();
					//FC:FINAL:DSE:#i2431 Create dynamically added menu items
					switch (intCount)
					{
						case 18:
							UnemploymentReportCaptions(item);
							break;
						case 15:
							RetirementCaptions(item);
							break;
						case 12:
							CleanUpCaptions(item);
							break;
					}
					parent.Node.Nodes.Add(subNode);
					
                }
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void RetirementCaptions(FCMenuItem parent)
		{
			try
			{
				RetirementMenuCreated = true;
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				string menu = "RETIREMENT";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Retirement]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					boolDisable = false;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "MainePERS";
								LabelNumber[Strings.Asc("1")] = 1;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] || ssnViewPermission != SSNViewType.Full;
								break;
							}
						case 2:
							{
								strTemp = "Electronic ICMA";
								LabelNumber[Strings.Asc("2")] = 2;
                                boolDisable = ssnViewPermission != SSNViewType.Full;

                                break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem item = new FCMenuItem(strTemp, "Menu" + intCount, menu, !boolDisable, 3, "");
					FCRigthTreeNode subNode = item.CreateTreeNode();
					parent.Node.Nodes.Add(subNode);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RetirementCaptions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void ScheduleCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "     [Schedules]";
			string strTemp = "";
			string menu = "SCHEDULES";
			bool boolDisable = false;
			for (int intCounter = 1; intCounter <= 5; intCounter++)
			{
				lngFCode = 0;
				switch (intCounter)
				{
					case 1:
						{
							strTemp = "Shift Types";
							LabelNumber[Strings.Asc("1")] = 1;
							break;
						}
					case 2:
						{
							strTemp = "Holidays";
							LabelNumber[Strings.Asc("2")] = 2;
							break;
						}
					case 3:
						{
							strTemp = "Schedules";
							LabelNumber[Strings.Asc("3")] = 3;
							break;
						}
					case 4:
						{
							strTemp = "Advance Week Rotations";
							LabelNumber[Strings.Asc("4")] = 4;
							break;
						}
					case 5:
						{
							strTemp = "Reverse Week Rotations";
							LabelNumber[Strings.Asc("5")] = 5;
							break;
						}
				}
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				parent.SubItems.Add(strTemp, "Menu" + intCounter, menu, !boolDisable, 3);
			}
		}

		private void ScheduleActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmShiftTypes.InstancePtr.Init();
						break;
					}
				case 2:
					{
						frmHolidays.InstancePtr.Show(App.MainForm);
						break;
					}
				case 3:
					{
						frmScheduleWeeks.InstancePtr.Init();
						break;
					}
				case 4:
					{
						modSchedule.AdvanceWeekRotations();
						break;
					}
				case 5:
					{
						modSchedule.ReverseWeekRotations();
						break;
					}
				case 6:
					{
						//SetMenuOptions("MAIN");
						break;
					}
			}
			//end switch
		}

		private void FileActions(int intMenu)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				switch (intMenu)
				{
					case 1:
						{
							frmDefaultInformation.InstancePtr.Show(App.MainForm);
							break;
						}
					case 2:
						{
							cPayrollDB pDB = new cPayrollDB();
							cVersionInfo tVer;
							if (pDB.CheckVersion())
							{
								MessageBox.Show("Database Structure Check completed successfully.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								MessageBox.Show("Error Number " + FCConvert.ToString(pDB.LastErrorNumber) + "  " + pDB.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							tVer = pDB.GetVersion();
							MessageBox.Show("Current DB Version is " + tVer.VersionString);
							break;
						}
					case 3:
						{
							frmFieldLengths.InstancePtr.Show(App.MainForm);
							break;
						}
					case 4:
						{
							frmFrequencyCodes.InstancePtr.Show(App.MainForm);
							break;
						}
					case 5:
						{
							frmStateTownInfo.InstancePtr.Show(App.MainForm);
							break;
						}
					case 6:
						{
							// frmValidAccounts.Show , MDIParent
							string strTemp = "";
							frmLoadValidAccounts.InstancePtr.Init(strTemp);
							break;
						}
					case 7:
						{
							frmBanks.InstancePtr.Show(App.MainForm);
							// Case 8
							// frmCheckDetailAdjustment.Show , MDIParent
							// Case 9
							// frmAdjustUnemployment.Show , MDIParent
							break;
						}
					case 8:
						{
							frmTAAdjustments.InstancePtr.Show(App.MainForm);
							break;
						}
					case 9:
						{
							frmTaxStatusCodes.InstancePtr.Show(App.MainForm);
							break;
						}
					case 10:
						{
							frmPurgeTerminatedEmployees.InstancePtr.Show(App.MainForm);
							break;
						}
					case 11:
						{
							// Call PurgeArchiveInformation
							frmPurgeArchive.InstancePtr.Show(App.MainForm);
							break;
						}
					case 12:
						{
							//SetMenuOptions("HISTORYFIX");
							break;
						}
					case 13:
						{
							if (MessageBox.Show("This routine is intended for new setups only" + "\r\n" + "It is not intended to routinely adjust employee pay totals" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								if (fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == string.Empty)
								{
									frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
									if (fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == string.Empty)
										return;
								}
								frmLoadBack.InstancePtr.Init();
							}
							break;
						}
					case 14:
						{
							modCoreysSweeterCode.UndoCurrentPayRun();
							break;
						}
					case 15:
						{
							frmExportVacSick.InstancePtr.Show(App.MainForm);
							break;
						}
					case 16:
						{
							frmACH.InstancePtr.Show(App.MainForm);
							break;
						}
					case 17:
						{
							cPayrollDB maintDB = new cPayrollDB();
							if (maintDB.DoMaintenance())
							{
								MessageBox.Show("Database maintenance completed successfully.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								MessageBox.Show("Error running database maintenance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							break;
						}
					case 18:
						{
							//SetMenuOptions("MAIN");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FileActions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RetirementActions(int intMenu)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: dtPayDate As DateTime	OnWrite(DateTime, string)
				DateTime dtPayDate;
				// vbPorter upgrade warning: strPayDate As string	OnRead(DateTime)
				string strPayDate = "";
				switch (intMenu)
				{
					case 1:
						{
							frmMSRSSetup.InstancePtr.Show(App.MainForm);
							break;
						}
					case 2:
						{
							dtPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
							if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
							{
								// individual
								strPayDate = frmGetPayDate.InstancePtr.Init("Choose ICMA File Paydate");
								if (Information.IsDate(strPayDate))
								{
									dtPayDate = FCConvert.ToDateTime(strPayDate);
								}
								else
								{
									return;
								}
							}
							modCoreysSweeterCode.MakeIMCAFiles(ref dtPayDate);
							break;
						}
					case 3:
						{
							//SetMenuOptions("PROCESSREPORTS");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RetirementActions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Function PrintCaptions()
		// On Error GoTo CallErrorRoutine
		// gstrCurrentRoutine = "PrintCaptions"
		// Dim Mouse As clsMousePointer
		// Set Mouse = New clsMousePointer
		// GoTo ResumeCode
		// CallErrorRoutine:
		// Call SetErrorHandler
		// Exit Function
		// ResumeCode:
		//
		// MDIParent.Text = strTrio & "     [Print Maintenance]"
		// Grid.Clear
		//
		// For intCount = 1 To GRIDROWS
		// lngFCode = 0
		// Select Case intCount
		// Case 1
		// Grid.TextMatrix(intCount, 1) = "1.  Print Account List"
		// LabelNumber(Asc("1")) = 1
		//
		// Case 2
		// Grid.TextMatrix(intCount, 1) = "2.  Taxpayer Notices"
		// LabelNumber(Asc("2")) = 2
		//
		// Case 3
		// Grid.TextMatrix(intCount, 1) = "3.  Reimbursement Notices"
		// LabelNumber(Asc("3")) = 3
		//
		// Case 4
		// Grid.TextMatrix(intCount, 1) = "4.  Highest Assessment Listing"
		// LabelNumber(Asc("4")) = 4
		//
		// Case 5
		// Grid.TextMatrix(intCount, 1) = "5.  Deleted Accounts"
		// LabelNumber(Asc("5")) = 5
		//
		// Case 6
		// Grid.TextMatrix(intCount, 1) = "6.  Labels"
		// LabelNumber(Asc("6")) = 6
		//
		// Case 7
		// Grid.TextMatrix(intCount, 1) = "X. Return to Main"
		// LabelNumber(Asc("X")) = 7
		//
		// Case Else
		// Grid.TextMatrix(intCount, 1) = vbNullString
		// End Select
		//
		// If lngFCode <> 0 Then
		// Call DisableMenuOption(ValidPermissions(Me, lngFCode, False), intCount)
		// End If
		// Next
		// End Function
		public void TimeCardCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				string menu = "TIMECARD";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Time Clock Setup]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Time Clock Categories";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "Time Clock Shifts";
								LabelNumber[Strings.Asc("2")] = 2;
								if (!(fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county"))
								{
									boolDisable = true;
								}
								break;
							}
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 3);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In TimeCardCaptions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void FileCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FileCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "FILE";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [File Maintenance]";
				for (int intCount = 1; intCount <= 17; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Customize";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.CUSTOMIZE;
								break;
							}
						case 2:
							{
								strTemp = "Check Database Structure";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.CUSTOMIZE;
								break;
							}
						case 3:
							{
								strTemp = "Employee Lengths / etc.";
								LabelNumber[Strings.Asc("3")] = 3;
								lngFCode = modGlobalVariables.EMPLOYEELENGTHSETC;
								break;
							}
						case 4:
							{
								strTemp = "Frequency Codes";
								LabelNumber[Strings.Asc("4")] = 4;
								lngFCode = modGlobalVariables.FREQUENCYCODES;
								break;
							}
						case 5:
							{
								strTemp = "Edit States/Towns/Counties";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.EDITSTATESTOWNSCOUNTIES;
								break;
							}
						case 6:
							{
								strTemp = "Valid Account Numbers";
								LabelNumber[Strings.Asc("6")] = 6;
								lngFCode = modGlobalVariables.TESTVALIDACCOUNTS;
								break;
							}
						case 7:
							{
								strTemp = "Direct Deposit Banks";
								LabelNumber[Strings.Asc("7")] = 7;
								lngFCode = modGlobalVariables.DIRECTDEPOSITBANKS;
								break;
							}
						case 8:
							{
								strTemp = "Adjust T&A Records";
								LabelNumber[Strings.Asc("8")] = 8;
								lngFCode = modGlobalVariables.ADJUSTTARECORDS;
								break;
							}
						case 9:
							{
								strTemp = "Tax Status Codes";
								LabelNumber[Strings.Asc("9")] = 9;
								lngFCode = modGlobalVariables.TAXSTATUSCODES;
								break;
							}
						case 10:
							{
								strTemp = "Purge Terminated Employees";
								LabelNumber[Strings.Asc("A")] = 10;
								lngFCode = modGlobalVariables.PURGETERMINATEDEMPLOYEES;
								break;
							}
						case 11:
							{
								strTemp = "Purge Audit Information";
								LabelNumber[Strings.Asc("B")] = 11;
								lngFCode = modGlobalVariables.PURGEAUDIT;
								break;
							}
						case 12:
							{
								strTemp = "MainePERS / Unemp History Fix";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							}
						case 13:
							{
								strTemp = "Load of Back Information";
								LabelNumber[Strings.Asc("D")] = 13;
								lngFCode = modGlobalVariables.LOADBACK;
								// Call DisableMenuOption(False, intCount)
								break;
							}
						case 14:
							{
								strTemp = "Reverse Pay Run";
								LabelNumber[Strings.Asc("E")] = 14;
								lngFCode = modGlobalVariables.VERIFYANDACCEPT;
								break;
							}
						case 15:
							{
								strTemp = "Export Vac/Sic";
								LabelNumber[Strings.Asc("F")] = 15;
								break;
							}
						case 16:
							{
								strTemp = "ACH Setup";
								LabelNumber[Strings.Asc("G")] = 16;
                                if (bankAccountViewPermission == BankAccountViewType.Full)
                                {
                                    lngFCode = modGlobalVariables.CUSTOMIZE;
                                }
                                else
                                {
                                    boolDisable = true;
                                }
                                break;
							}
						case 17:
							{
								strTemp = "Database Maintenance";
								LabelNumber[Strings.Asc("H")] = 17;
								lngFCode = modGlobalVariables.CUSTOMIZE;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
					switch (intCount)
					{
						case 12:
							HistoryFixCaptions(newItem);
							break;
					}
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void HistoryFixCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FileCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "HISTORYFIX";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [MainePERS / Unemployment History Fix]";
				for (int intCount = 1; intCount <= 3; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Unemployment History Fix";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "MainePERS History Fix";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
						case 3:
							{
								strTemp = "Dist M Fix";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void UnemploymentReportCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				UnemploymentMenuCreated = true;
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FileCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "UNEMPLOYMENT";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Unemployment Reports]";
				for (int intCount = 1; intCount <= 5; intCount++)
				{
					lngFCode = 0;
					boolDisable = false;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "MMA Unemployment";
								LabelNumber[Strings.Asc("1")] = 1;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT];
								break;
							}
						case 2:
							{
								strTemp = "Non-MMA Unemployment";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA];
								break;
							}
						case 3:
							{
								strTemp = "ME UC-1";
								LabelNumber[Strings.Asc("3")] = 3;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATEUNEMPLOYMENT] || ssnViewPermission != SSNViewType.Full;
								break;
							}
						case 4:
							{
								strTemp = "FUTA (940)";
								LabelNumber[Strings.Asc("4")] = 4;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.FUTA940];
								// 
								// Case 5
								// Grid.TextMatrix(intCount, 1) = "5.  Multiple Worksite Report"
								// LabelNumber(Asc("5")) = 5
								// Call DisableMenuOption(ValidReports(MULTIPLEWORKSITEREPORT), intCount)
								break;
							}
						case 5:
							{
								strTemp = "Edit Unemployment Records";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.PAYROLLDISTRIBUTION;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem item = new FCMenuItem(strTemp, "Menu" + intCount, menu, !boolDisable, 3, "");
					FCRigthTreeNode subNode = item.CreateTreeNode();
					parent.Node.Nodes.Add(subNode);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void MQYCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MQYCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "MQYPROCESSING";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Payroll MTD / QTD / YTD Reports]";
				for (int intCount = 1; intCount <= 18; intCount++)
				{
					if (modGlobalVariables.Statics.gstrMQYProcessing == "MONTHLY")
					{
						switch (intCount)
						{
							case 1:
								strTemp = "Full Set";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							case 2:
								strTemp = "MONTHLY";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = true;
								break;
							case 3:
								strTemp = "ME State Retirement Rpt";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							case 4:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("4")] = 4;
								break;
							case 5:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("5")] = 5;
								break;
							case 6:
								strTemp = "FTD (8109) Fica & Holdings";
								LabelNumber[Strings.Asc("6")] = 6;
								break;
							case 7:
								strTemp = "FTD (8109) FUTA";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							case 8:
								strTemp = "QUARTERLY";
								LabelNumber[Strings.Asc("8")] = 8;
								boolDisable = true;
								break;
							case 9:
								strTemp = "MMA Unemployment Report";
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							case 10:
								strTemp = "Non-MMA Unemployment Rpt";
								LabelNumber[Strings.Asc("A")] = 10;
								break;
							case 11:
								strTemp = " Fed Qtrly Report (941)";
								LabelNumber[Strings.Asc("B")] = 11;
								break;
							case 12:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							case 13:
								strTemp = " State Unemployment (C-1)";
								LabelNumber[Strings.Asc("D")] = 13;
								break;
							case 14:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("E")] = 14;
								break;
							case 15:
								strTemp = "YTD";
								LabelNumber[Strings.Asc("F")] = 15;
								boolDisable = true;
								break;
							case 16:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("G")] = 16;
								break;
							case 17:
								strTemp = "Pay & Tax Sum Rpt";
								LabelNumber[Strings.Asc("H")] = 17;
								break;
							case 18:
								strTemp = "F.U.T.A. Report (940)";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
						}
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "QUARTERLY")
					{
						switch (intCount)
						{
							case 1:
								strTemp = "Full Set";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							case 2:
								strTemp = "MONTHLY";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = true;
								break;
							case 3:
								strTemp = "ME State Retirement Rpt";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							case 4:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("4")] = 4;
								break;
							case 5:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("5")] = 5;
								break;
							case 6:
								strTemp = "QUARTERLY";
								LabelNumber[Strings.Asc("6")] = 6;
								boolDisable = true;
								break;
							case 7:
								strTemp = "FTD (8109) Fica & Holdings";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							case 8:
								strTemp = "FTD (8109) FUTA";
								LabelNumber[Strings.Asc("8")] = 8;
								break;
							case 9:
								strTemp = "MMA Unemployment Report";
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							case 10:
								strTemp = "Non-MMA Unemployment Rpt";
								LabelNumber[Strings.Asc("A")] = 10;
								break;
							case 11:
								strTemp = "Fed Qtrly Report (941)";
								LabelNumber[Strings.Asc("B")] = 11;
								break;
							case 12:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							case 13:
								strTemp = "State Unemployment (C-1)";
								LabelNumber[Strings.Asc("D")] = 13;
								break;
							case 14:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("E")] = 14;
								break;
							case 15:
								strTemp = "CALENDER YTD";
								LabelNumber[Strings.Asc("F")] = 15;
								boolDisable = true;
								break;
							case 16:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("G")] = 16;
								break;
							case 17:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("H")] = 17;
								break;
							case 18:
								strTemp = "F.U.T.A. Report (940)";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
						}
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "YEARLY")
					{
						switch (intCount)
						{
							case 1:
								strTemp = "Full Set";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							case 2:
								strTemp = "MONTHLY";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = true;
								break;
							case 3:
								strTemp = "ME State Retirement Rpt";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							case 4:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("4")] = 4;
								break;
							case 5:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("5")] = 5;
								break;
							case 6:
								strTemp = "QUARTERLY";
								LabelNumber[Strings.Asc("6")] = 6;
								boolDisable = true;
								break;
							case 7:
								strTemp = "MMA Unemployment Report";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							case 8:
								strTemp = "Non-MMA Unemployment Rpt";
								LabelNumber[Strings.Asc("8")] = 8;
								break;
							case 9:
								strTemp = "Fed Qtrly Report (941)";
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							case 10:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("A")] = 10;
								break;
							case 11:
								strTemp = "State Unemployment (C-1)";
								LabelNumber[Strings.Asc("B")] = 11;
								break;
							case 12:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							case 13:
								strTemp = "CALENDER YTD";
								LabelNumber[Strings.Asc("D")] = 13;
								boolDisable = true;
								break;
							case 14:
								strTemp = "Deduction Report";
								LabelNumber[Strings.Asc("E")] = 14;
								break;
							case 15:
								strTemp = "Pay & Tax Sum Report";
								LabelNumber[Strings.Asc("F")] = 15;
								break;
							case 16:
								strTemp = "F.U.T.A. Rpt (940)";
								LabelNumber[Strings.Asc("G")] = 16;
								break;
							case 17:
								strTemp = "FTD (8109) Fica & Holdings";
								LabelNumber[Strings.Asc("H")] = 17;
								break;
							case 18:
								strTemp = "FTD (8109) FUTA";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
						}
					}
					else if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
					{
						switch (intCount)
						{
							case 1:
								strTemp = "Full Set";
								LabelNumber[Strings.Asc("1")] = 1;
								boolDisable = true;
								break;
							case 2:
								strTemp = "MONTHLY";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = true;
								break;
							case 3:
								strTemp = "ME State Retirement Rpt";
								LabelNumber[Strings.Asc("3")] = 3;
								break;
							case 4:
								strTemp = "Deduction Rpt";
								LabelNumber[Strings.Asc("4")] = 4;
								break;
							case 5:
								strTemp = "Pay & Tax Sum Rpt";
								LabelNumber[Strings.Asc("5")] = 5;
								break;
							case 6:
								strTemp = "FTD (8109) Fica & Holdings";
								LabelNumber[Strings.Asc("6")] = 6;
								break;
							case 7:
								strTemp = "FTD (8109) FUTA";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							case 8:
								strTemp = "QUARTERLY";
								LabelNumber[Strings.Asc("8")] = 8;
								boolDisable = true;
								break;
							case 9:
								strTemp = "MMA Unemployment Rpt";
								LabelNumber[Strings.Asc("9")] = 9;
								break;
							case 10:
								strTemp = "Non-MMA Unemployment Rpt";
								LabelNumber[Strings.Asc("A")] = 10;
								break;
							case 11:
								strTemp = "Fed Qtrly Rpt (941)";
								LabelNumber[Strings.Asc("B")] = 11;
								break;
							case 12:
								strTemp = "Deduction Rpt";
								LabelNumber[Strings.Asc("C")] = 12;
								break;
							case 13:
								strTemp = "State Unemployment (C-1)";
								LabelNumber[Strings.Asc("D")] = 13;
								break;
							case 14:
								strTemp = "Pay & Tax Sum Rpt";
								LabelNumber[Strings.Asc("E")] = 14;
								break;
							case 15:
								strTemp = "CALENDER YTD";
								LabelNumber[Strings.Asc("F")] = 15;
								boolDisable = true;
								break;
							case 16:
								strTemp = "Deduction Rpt";
								LabelNumber[Strings.Asc("G")] = 16;
								break;
							case 17:
								strTemp = "Pay & Tax Sum Rpt";
								LabelNumber[Strings.Asc("H")] = 17;
								break;
							case 18:
								strTemp = "F.U.T.A Rpt (940)";
								LabelNumber[Strings.Asc("I")] = 18;
								break;
						}
					}

					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, boolDisable, 2);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modBlockEntry.WriteYY();
				//Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
				modSubMain.Statics.rsBud = null;
				Application.Exit();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWBD0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCR0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCK0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCE0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWE90000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuFExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				modBlockEntry.WriteYY();
				//Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
				Application.Exit();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuFixedAssetsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWFA0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWGN0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show(App.MainForm);
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWMV0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

        //private void mnuOMaxForms_Click(object sender, System.EventArgs e)
        //{
        //	try
        //	{
        //		// On Error GoTo CallErrorRoutine
        //		fecherFoundation.Information.Err().Clear();
        //		modErrorHandler.Statics.gstrCurrentRoutine = "mnuOMaxForms_Click";
        //		clsMousePointer Mouse;
        //		Mouse = new clsMousePointer();
        //		modRegistry.RegCreateKeyWrp(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, 0);
        //		if (mnuOMaxForms.Checked)
        //		{
        //			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "False");
        //		}
        //		else
        //		{
        //			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "True");
        //		}
        //		modGlobalVariables.Statics.gboolMaxForms = !modGlobalVariables.Statics.gboolMaxForms;
        //		mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
        //	}
        //	catch (Exception ex)
        //	{
        //		// CallErrorRoutine:
        //		modErrorHandler.SetErrorHandler(ex);
        //		return;
        //	}
        //}

        //public void GRID_Enter(object sender, System.EventArgs e)
        //{
        //	int a;
        //}

        //private void GRID_KeyDownEvent(object sender, KeyEventArgs e)
        //{
        //	// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
        //	int intCounter;
        //	Keys KeyCode = e.KeyCode;
        //	if (KeyCode > (Keys)96 && KeyCode < (Keys)106)
        //	{
        //		KeyCode -= 48;
        //	}
        //	if (KeyCode == (Keys)13)
        //	{
        //		if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1) == MDIParent.InstancePtr.BackColor)
        //			return;
        //		if (fecherFoundation.Strings.Trim(GRID.TextMatrix(GRID.Row, 1)) == string.Empty)
        //			return;
        //		FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, GRID.Row, GRID.Col)), 1))[0])]), CallType.Method);
        //	}
        //	for (intCounter = 1; intCounter <= (GRID.Rows - 1); intCounter++)
        //	{
        //		if (Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(e.KeyCode)))
        //		{
        //			if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 1) == MDIParent.InstancePtr.BackColor)
        //				return;
        //			FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(intCounter), CallType.Method);
        //			KeyCode = 0;
        //			break;
        //		}
        //		GRID.Focus();
        //	}
        //}

        //private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	if (GRID.MouseCol != 1)
        //		return;
        //	// 
        //	/*? On Error Resume Next  */
        //	if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.MouseRow, 1) == MDIParent.InstancePtr.BackColor)
        //		return;
        //	FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(fecherFoundation.Strings.UCase(Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, GRID.Row, GRID.Col)), 1))[0])]), CallType.Method);
        //}

        //static int R = 0, c;

        //private void GRID_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	double I;
        //	ToolTip1.SetToolTip(GRID, GetGridToolTip());
        //	// avoid extra work
        //	if (GRID.MouseRow == R)
        //		return;
        //	R = GRID.MouseRow;
        //	if (R < 0)
        //		return;
        //	// move selection (even with no click)
        //	GRID.Select(R, 1);
        //	if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, R, 1) == MDIParent.InstancePtr.BackColor || R > LabelNumber[Convert.ToByte("X"[0])])
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        //		return;
        //	}
        //	else
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
        //	}
        //	// show cursor image if there is some text in column 5
        //	if (R == 0)
        //		return;
        //}
        //// vbPorter upgrade warning: intRowNumber As int	OnWriteFCConvert.ToInt32(
        //static int intRowNumber;

        //private void GRID_RowColChange(object sender, System.EventArgs e)
        //{
        //	/*? On Error Resume Next  */
        //	if (GRID.Row == 0)
        //	{
        //		GRID.Row = 1;
        //		// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0) = ImageList1.ListImages("Left").Picture
        //		return;
        //	}
        //	// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, intRowNumber, 0) = ImageList1.ListImages("Clear").Picture
        //	if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1) == MDIParent.InstancePtr.BackColor)
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        //		return;
        //	}
        //	else
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
        //	}
        //	// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, Grid.Row, 0) = ImageList1.ListImages("Left").Picture
        //	intRowNumber = GRID.Row;
        //}

        public void DisableMenuOption(bool boolState, int intRow)
        {
            //if (!boolState)
            //{
            //    MDIParent.InstancePtr.GRID.Select(intRow, 1);
            //    MDIParent.InstancePtr.GRID.CellForeColor = MDIParent.InstancePtr.BackColor;
            //}
        }
        // vbPorter upgrade warning: intPreset As object	OnWrite	OnRead
        public bool SetCheckProcessMenuOptions(int intPreset = 0)
		{
			bool SetCheckProcessMenuOptions = false;
			int intReturn = 0;
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			string strFTD940 = "";
			string strFTD941 = "";
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			// 0 = WEEKLY
			// 1 = MONTHLY
			// 2 = QUARTERLY
			// 3 = YEARLY
			// 4 = INDIVIDUAL
			// 
			SetCheckProcessMenuOptions = false;
			if (FCConvert.ToInt32(intPreset) == 0)
			{
				DateTime dtReturndate = DateTime.FromOADate(0);
				int lngReturnYear = 0;
				int intMonthQuarter = 0;
				int PayRunID = 0;
				intReturn = frmMQYFlag.InstancePtr.Init("Individual", "Monthly", "Quarterly", "Yearly", "Weekly", false, ref dtReturndate, ref lngReturnYear, ref intMonthQuarter, ref PayRunID);
			}
			else
			{
				if (FCConvert.ToInt32(intPreset) == 4)
				{
					modGlobalVariables.Statics.gstrMQYProcessing = "NONE";
				}
				intReturn = FCConvert.ToInt16(intPreset);
			}
			if (intReturn < 0)
			{
				// they cancelled
				return SetCheckProcessMenuOptions;
			}
			SetCheckProcessMenuOptions = true;
			if (intReturn < 0)
			{
			}
			else
			{
				switch (intReturn)
				{
					case 0:
						{
							strFullSetDesc = "Weekly";
							break;
						}
					case 1:
						{
							strFullSetDesc = "Monthly";
							break;
						}
					case 2:
						{
							strFullSetDesc = "Quarterly";
							break;
						}
					case 3:
						{
							strFullSetDesc = "Yearly";
							break;
						}
					case 4:
						{
							strFullSetDesc = "Individual";
							break;
						}
				}
				//end switch
				if (intReturn == 4)
				{
					// this indicating that they want to run individual reports
					// so all of them should be enabled
					for (intCounter = 0; intCounter <= 29; intCounter++)
					{
						// total number of reports
						modGlobalVariables.Statics.ValidReports[intCounter] = true;
					}
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.FULLSET] = false;
					// CHANGED FOR CALL ID 62852 MATTHEW 2/10/2005
					if (modGlobalVariables.Statics.gboolNoMSRS)
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = false;
					}
					else
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = (intReturn == 1 || intReturn == 4);
					}
					return SetCheckProcessMenuOptions;
				}
				else
				{
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.FULLSET] = true;
				}
				rsData.OpenRecordset("Select * from tblStandardLimits", modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
					strFTD940 = FCConvert.ToString(rsData.Get_Fields("FTD940"));
					strFTD941 = FCConvert.ToString(rsData.Get_Fields("FTD941"));
				}
				modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATE941] = intReturn == 2;
				rsData.OpenRecordset("Select * from tblReportSelection where ReportOption = " + FCConvert.ToString(intReturn), modGlobalVariables.DEFAULTDATABASE);
				rsDefaultInfo.OpenRecordset("SELECT * FROM tblDefaultInformation");
				if (rsData.EndOfFile())
				{
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.TRUST] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.CHECKREG] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DIRECTDEPOSIT] = (intReturn == 0 && bankAccountViewPermission == BankAccountViewType.Full);
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.ACCOUNTING] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DATAENTRY] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.AUDIT] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETTEMP] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETAUDIT] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.BACKUP] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.WARRANT] = intReturn == 0;
					if (fecherFoundation.Strings.UCase(strFTD940) == "WEEKLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 0;
					}
					else if (fecherFoundation.Strings.UCase(strFTD940) == "MONTHLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 1;
					}
					else if (fecherFoundation.Strings.UCase(strFTD940) == "QUARTERLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 2;
					}
					// MATTHEW 1/20/2004
					// this variable is actually for the quarterly state 941
					// ValidReports(FTD941) = intReturn = 2
					// If UCase(strFTD941) = "WEEKLY" Then
					// ValidReports(FTD941) = intReturn = 0
					// ElseIf UCase(strFTD941) = "MONTHLY" Then
					// ValidReports(FTD941) = intReturn = 1
					// End If
					// MATTHEW 5/10/2005 CALL ID 68315
					if (fecherFoundation.Strings.UCase(strFTD941) == "WEEKLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941] = intReturn == 0;
					}
					else if (fecherFoundation.Strings.UCase(strFTD941) == "MONTHLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941] = intReturn == 1;
					}
					// CHANGED FOR CALL ID 62852 MATTHEW 2/10/2005
					if (modGlobalVariables.Statics.gboolNoMSRS)
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = false;
					}
					else
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = intReturn == 1;
					}
					if (FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("MMAUnemployment")))
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT] = intReturn == 2;
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA] = false;
					}
					else
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA] = intReturn == 2;
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT] = false;
					}
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.WAGE941] = intReturn == 2;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATEUNEMPLOYMENT] = intReturn == 2;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.FUTA940] = intReturn == 3;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.PAYTAX] = true;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DEDUCTION] = true;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.VACSICK] = true;
				}
				else
				{
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.TRUST] = (intReturn == 0 && rsData.Get_Fields_Boolean("Trust"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.CHECKREG] = (intReturn == 0 && rsData.Get_Fields_Boolean("CheckRegister"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DIRECTDEPOSIT] = (intReturn == 0 && rsData.Get_Fields_Boolean("BankLists") && bankAccountViewPermission == BankAccountViewType.Full);
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.ACCOUNTING] = (intReturn == 0 && rsData.Get_Fields_Boolean("Accounting"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DATAENTRY] = (intReturn == 0 && rsData.Get_Fields_Boolean("DataEntryForms"));
					// I have set this to false so that it will not run with the
					// full set of reports as it really isn't finished and it is
					// very large and giving incorrect data. Deb from Topsham
					// requested this and I really agree with her until we can
					// clean up this report a bit. Matthew 1/13/03
					// ValidReports(AUDIT) = False
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.AUDIT] = (intReturn == 0 && rsData.Get_Fields_Boolean("Audit"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETTEMP] = (intReturn == 0 && rsData.Get_Fields_Boolean("TempList"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETAUDIT] = intReturn == 0;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.BACKUP] = (intReturn == 0 && rsData.Get_Fields_Boolean("Backup"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.WARRANT] = (intReturn == 0 && rsData.Get_Fields_Boolean("PayrollWarrant"));
					if (fecherFoundation.Strings.UCase(strFTD940) == "WEEKLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 0;
					}
					else if (fecherFoundation.Strings.UCase(strFTD940) == "MONTHLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 1;
					}
					else if (fecherFoundation.Strings.UCase(strFTD940) == "QUARTERLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD940] = intReturn == 2;
					}
					if (fecherFoundation.Strings.UCase(strFTD941) == "WEEKLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941] = intReturn == 0;
					}
					else if (fecherFoundation.Strings.UCase(strFTD941) == "MONTHLY")
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.FTD941] = intReturn == 1;
					}
					// CHANGED FOR CALL ID 62852 MATTHEW 2/10/2005
					if (modGlobalVariables.Statics.gboolNoMSRS)
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = false;
					}
					else
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MSRS] = intReturn == 1;
					}
					if (FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("MMAUnemployment")))
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT] = intReturn == 2;
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA] = false;
					}
					else
					{
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.NONMMA] = intReturn == 2;
						modGlobalVariables.Statics.ValidReports[modGlobalVariables.MMAUNEMPLOYMENT] = false;
					}
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.WAGE941] = (intReturn == 2);
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.STATEUNEMPLOYMENT] = (intReturn == 2) && rsData.Get_Fields_Boolean("C1");
					// ValidReports(MULTIPLEWORKSITEREPORT) = intReturn = 2
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.FUTA940] = intReturn == 3;
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.PAYTAX] = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("PayTaxSummary"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.DEDUCTION] = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Deductions"));
					modGlobalVariables.Statics.ValidReports[modGlobalVariables.VACSICK] = (rsData.Get_Fields_Boolean("Vacation") || rsData.Get_Fields_Boolean("Sick") || rsData.Get_Fields("Other"));
				}				
			}
			return SetCheckProcessMenuOptions;
		}

		public void CleanUpCaptions(FCMenuItem parent)
		{
			try
			{
				cleanUpRoutinesMenuCreated = true;
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FileCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "CLEANUP";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Clean Up Reports]";
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Backup/Delete Audit Rpt";
								LabelNumber[Strings.Asc("1")] = 1;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.BACKUP];
								break;
							}
						case 2:
							{
								strTemp = "Reset Temp / Dist";
								LabelNumber[Strings.Asc("2")] = 2;
								boolDisable = !modGlobalVariables.Statics.ValidReports[modGlobalVariables.RESETAUDIT];
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					FCMenuItem item = new FCMenuItem(strTemp, "Menu" + intCount, menu, !boolDisable, 3, "");
					FCRigthTreeNode subNode = item.CreateTreeNode();
					parent.Node.Nodes.Add(subNode);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWPY0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWPP0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWRE0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuRedbookHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWRB0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWBL0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCL0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWUT0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWVR0000.HLP", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void PurgeArchiveInformation()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PurgeArchiveInformation";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				FCFileSystem fso;
				clsDRWrapper rsExecute = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				fso = new FCFileSystem();
				// CHECK TO SEE IF THE ARCHIVE FOLDER DOES ACTUALLY EXIST
				// If fso.FolderExists("TWArchive") Then
				// Else
				// Call fso.CreateFolder("TWArchive")
				// End If
				// If FCFileSystem.FileExists("TWPY0000.vb1") Then
				// PY DATABASE DOES EXIST SO TRY AND COPY IT OVER TO THE ARCHIVE FOLDER
				// AddCYAEntry "PY", "Purge Audit Information", "Copy Database to Archive", gstrUser
				// Call fso.CopyFile("TWPY0000.vb1", CurDir & "\TWArchive\" & Format(Date, "MMddyy") & "TWArchive.vb1", True)
				// If FCFileSystem.FileExists(CurDir & "\TWArchive\" & Format(Date, "MMddyy") & "TWArchive.vb1") Then
				// COPY WAS SUCCESSFUL SO CLEAN OUT THE T
				modGlobalFunctions.AddCYAEntry_6("PY", "Purge Archive Information", "Delete Audit History", modGlobalVariables.Statics.gstrUser);
				rsExecute.Execute("Delete from AuditArchive", "TWPY0000.vb1");
				rsExecute.Execute("Delete from AuditHistory", "TWPY0000.vb1");
				rsExecute.Execute("Delete from AuditChanges", "TWPY0000.vb1");
				rsExecute.Execute("Delete from AuditChangesArchive", "TWPY0000.vb1");
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Purge of Audit History completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// End If
				// Else
				// Screen.MousePointer = vbDefault
				// MsgBox "Payroll Database could not be found. Purge of information could not be completed.", vbInformation + vbOKOnly, "TRIO Software"
				// End If
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private string GetGridToolTip()
		{
			string GetGridToolTip = "";
			string strTemp;
			int x = 0;
			//x = GRID.MouseRow;
			strTemp = "";
			if (fecherFoundation.Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu) == "CALENDAR")
			{
				switch (x)
				{
					case 10:
						{
							strTemp = "Create or update adjustment records for added W2 information affecting Federal,State,Fica,Medicare etc.";
							break;
						}
					case 11:
						{
							strTemp = "List employees whose Federal,State,Fica,Medicare etc. amounts were affected by added W2 information";
							break;
						}
					case 12:
						{
							strTemp = "Create paper or electronic W-2C and W-3C forms";
							break;
						}
				}
				//end switch
			}
			else if (fecherFoundation.Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu) == "SCHEDULES")
			{
				switch (x)
				{
					case 1:
						{
							strTemp = "Create or update shift types";
							break;
						}
					case 2:
						{
							strTemp = "Create or edit holidays";
							break;
						}
					case 3:
						{
							strTemp = "Create or edit schedules and week rotations";
							break;
						}
				}
				//end switch
			}
			GetGridToolTip = strTemp;
			return GetGridToolTip;
		}

		private void Update941W2Info()
		{
			// Check all of the additional w2 codes to see if any affect the 941 reports
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngYear = 0;
				string strLastENum;
				string strSQL = "";
				double dblFed = 0;
				double dblState = 0;
				double dblFica = 0;
				double dblMedicare = 0;
				double dblFedTot;
				double dblStateTot;
				double dblFicaTot;
				double dblMedicareTot;
				DateTime dtTemp;
				bool boolUpdateTotals;
				clsLoad.OpenRecordset("select top 1 * from tblw2edittable", "twpy0000.vb1");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No employees to adjust");
					return;
				}
				else
				{
					lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("year"))));
				}
				modGlobalFunctions.AddCYAEntry_6("PY", "W2 941 adjustments", "Tax Year " + FCConvert.ToString(lngYear));
				strLastENum = "";
				clsLoad.OpenRecordset("select * from tblW2AdditionalInfo inner join tblw2additionalinfodata on (9000+convert(int, Tblw2additionalinfo.ID) = tblw2additionalinfodata.additionaldeductionid) where (federal = 1 or fica = 1 or medicare = 1 or state = 1) order by employeenumber", "twpy0000.vb1");
				boolUpdateTotals = false;
				dtTemp = modCoreysSweeterCode.GetLastFiscalDay(FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngYear)));
				if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp, DateTime.Today) < 0)
				{
					boolUpdateTotals = true;
				}
				while (!clsLoad.EndOfFile())
				{
					dblFed = 0;
					dblState = 0;
					dblFica = 0;
					dblMedicare = 0;
					if (FCConvert.ToBoolean(clsLoad.Get_Fields("federal")))
					{
						dblFed = Conversion.Val(clsLoad.Get_Fields("amount"));
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields("Fica")))
					{
						dblFica = Conversion.Val(clsLoad.Get_Fields("amount"));
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields("state")))
					{
						dblState = Conversion.Val(clsLoad.Get_Fields("amount"));
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("Medicare")))
					{
						dblMedicare = Conversion.Val(clsLoad.Get_Fields("amount"));
					}
					if (dblFed != 0 || dblFica != 0 || dblState != 0 || dblMedicare != 0)
					{
						strSQL = "Insert into tblcheckdetail (adjustrecord,w2adjustment,totalrecord,employeenumber,paydate,federaltaxgross,statetaxgross,ficataxgross,medicaretaxgross,grosspay";
						strSQL += ",payrunid,federaltaxwh,statetaxwh,ficataxwh,medicaretaxwh,distgrosspay) values (";
						strSQL += "true,true,true";
						strSQL += ",'" + clsLoad.Get_Fields("EMPLOYEENUMBER") + "'";
						strSQL += ",'12/31/" + FCConvert.ToString(lngYear) + "'";
						strSQL += "," + FCConvert.ToString(dblFed);
						strSQL += "," + FCConvert.ToString(dblState);
						strSQL += "," + FCConvert.ToString(dblFica);
						strSQL += "," + FCConvert.ToString(dblMedicare);
						strSQL += ",0,0";
						strSQL += ",1,0,0,0,0,0";
						strSQL += "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("amount")));
						strSQL += ")";
						clsSave.Execute(strSQL, "twpy0000.vb1");
					}
					clsLoad.MoveNext();
				}
				MessageBox.Show("Records Adjusted", "Adjusted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Update941W2Info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void WorksiteReportCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "PrintingCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "WORKSITEREPORTS";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Worksite Reports]";
				modGlobalVariables.Statics.gboolGroupBy = false;
				for (int intCount = 1; intCount <= 2; intCount++)
				{
					lngFCode = 0;
					switch (intCount)
					{
						case 1:
							{
								strTemp = "BLS Worksite ID";
								LabelNumber[Strings.Asc("1")] = 1;
								break;
							}
						case 2:
							{
								strTemp = "Multiple Worksite Summary";
								LabelNumber[Strings.Asc("2")] = 2;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void WorksiteReportActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmReportViewer.InstancePtr.Init(rptBLSListing.InstancePtr, boolAllowEmail: true, strAttachmentName: "BLSListing");
						break;
					}
				case 2:
					{
						int lngYear = 0;
						int intQuarter = 0;
						modGlobalVariables.Statics.gboolCancelSelected = false;
						//FC:FINAL:DSE:#i2421 Use date parameter by reference
						DateTime tmpArg = DateTime.FromOADate(0);
						frmSelectDateInfo.InstancePtr.Init3("Choose Quarter and Year for Report", ref lngYear, ref intQuarter, -1, ref tmpArg , -1, false);
						if (!modGlobalVariables.Statics.gboolCancelSelected)
						{
							rptBLS.InstancePtr.Init(ref intQuarter, ref lngYear, false, "", false, false);
						}
						break;
					}
				case 3:
					{
						//SetMenuOptions("APPLICATIONPRINTING");
						break;
					}
			}
			//end switch
		}

		private void EmployeeActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmEmployeeMaster";
						// CHECK TO SE
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == "-1")
						{
							clsDRWrapper rsData = new clsDRWrapper();
							rsData.OpenRecordset("Select * from tblEmployeeMaster", "Payroll");
							if (rsData.EndOfFile())
							{
								frmEmployeeMaster.InstancePtr.Show(App.MainForm);
							}
							else
							{
								rsData.MoveLast();
								rsData.MoveFirst();
								if (rsData.RecordCount() < 2)
								{
									modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
									modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName = FCConvert.ToString(rsData.Get_Fields_String("FirstName"));
									modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName = FCConvert.ToString(rsData.Get_Fields("middlename"));
									modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName = FCConvert.ToString(rsData.Get_Fields_String("LastName"));
									modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig = FCConvert.ToString(rsData.Get_Fields_String("Desig"));
									modGlobalRoutines.SetCurrentEmployeeType(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig, false);
									frmEmployeeMaster.InstancePtr.Show(App.MainForm);
								}
								else
								{
									frmEmployeeSearch.InstancePtr.Show(App.MainForm);
								}
							}
						}
						else
						{
							frmEmployeeMaster.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 2:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmPayrollDistribution";
						modGlobalVariables.Statics.gboolDataEntry = false;
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							// Unload frmEmployeeSearch
							// Set frmEmployeeSearch = Nothing
							// DoEvents
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmPayrollDistribution.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 3:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmEmployeeDeductions";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmEmployeeDeductions.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 4:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmEmployersMatch";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmEmployersMatch.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 5:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmVacationSick";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmVacationSick.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 6:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmMiscUpdate";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmMiscUpdate.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 7:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmEmployeePayTotals";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmEmployeePayTotals.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 8:
					{
						modGlobalVariables.Statics.gstrEmployeeScreen = "frmDirectDeposit";
						if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty || Conversion.Val(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber) == -1)
						{
							frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							frmDirectDeposit.InstancePtr.Show(App.MainForm);
						}
						break;
					}
				case 9:
					{
						modGlobalRoutines.CloseChildForms();
						modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
						//App.DoEvents();
						frmEmployeeSearch.InstancePtr.Show(App.MainForm);
						break;
					}
				case 10:
					{
						// tropy-730
						string strChoices = "";
						strChoices = FCConvert.ToString(DateTime.Today.Year - 1) + ";" + FCConvert.ToString(DateTime.Today.Year - 1) + " tax year| " + FCConvert.ToString(DateTime.Today.Year) + " ;" + FCConvert.ToString(DateTime.Today.Year) + " tax year (Current)";
						int lngYearChoice = 0;
						int lngDefaultChoice = 0;
						if (DateTime.Today.Month > 1)
						{
							lngDefaultChoice = DateTime.Today.Year;
						}
						else
						{
							lngDefaultChoice = DateTime.Today.Year - 1;
						}
						frmDropDownChoice ddc = new frmDropDownChoice();
						lngYearChoice = ddc.GetChoiceByData(strChoices, lngDefaultChoice, "Tax Year", "Select the tax year the ACA data is for");
						/*- ddc = null; */
						if (lngYearChoice > 0)
						{
							frmACASetUp acaForm = new frmACASetUp();
							acaForm.Init(lngYearChoice);
						}
						break;
					}
				case 11:
					{
						if (DateTime.Today.Month > 1)
						{
							frmACADataImport.InstancePtr.Init(DateTime.Today.Year);
						}
						else
						{
							frmACADataImport.InstancePtr.Init((DateTime.Today.Year - 1));
						}
						break;
					}
				case 12:
					{
						frmNonPaidMSRS.InstancePtr.Show(App.MainForm);
						break;
					}
				case 13:
					{
						frmVoidCheck.InstancePtr.Show(App.MainForm);
						break;
					}
				case 14:
					{
						frmContractAddEdit.InstancePtr.Init("");
						break;
					}
				case 15:
					{
						frmEditCheckDetail.InstancePtr.Show(App.MainForm);
						break;
					}
				case 16:
					{
						modGlobalRoutines.ShowEmployeeInformation(false);
						//SetMenuOptions("MAIN");
						break;
					}
				case 17:
					{
						break;
					}
			}
			//end switch
		}

		public void ACAFilingCaptions(FCMenuItem parent)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ACAFilingCaptions";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strTemp = "";
				string menu = "ACAFILING";
				bool boolDisable = false;
				App.MainForm.Text = strTrio + "     [Health Care Filing]";
				for (int intCount = 1; intCount <= 7; intCount++)
				{
					switch (intCount)
					{
						case 1:
							{
								strTemp = "Employee Health Care";
								LabelNumber[Strings.Asc("1")] = 1;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 2:
							{
								strTemp = "Import Employee Health Care";
								LabelNumber[Strings.Asc("2")] = 2;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 3:
							{
								strTemp = "Print 1095-Bs";
								LabelNumber[Strings.Asc("3")] = 3;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 4:
							{
								strTemp = "Print 1094-B";
								LabelNumber[Strings.Asc("4")] = 4;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 5:
							{
								strTemp = "Print 1095-Cs";
								LabelNumber[Strings.Asc("5")] = 5;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 6:
							{
								strTemp = "Print 1094-C";
								LabelNumber[Strings.Asc("6")] = 6;
								lngFCode = modGlobalVariables.CNSTHealthcareSetup;
								break;
							}
						case 7:
							{
								strTemp = "Electronic Filing";
								LabelNumber[Strings.Asc("7")] = 7;
								break;
							}
					}
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
				}
			}
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		public void ACAFilingActions(int intOption)
		{
			switch (intOption)
			{
				case 1:
					{
						frmACASetUp acaSetupForm = new frmACASetUp();
						acaSetupForm.Init(GetCurrentW2Year());
						break;
					}
				case 2:
					{
						frmACADataImport.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 3:
					{
						frm1095Bs.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 4:
					{
						frm1094B.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 5:
					{
						frm1095Cs.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 6:
					{
						frm1094C.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 7:
					{
						frmACAElectronicFiling.InstancePtr.Init(GetCurrentW2Year());
						break;
					}
				case 8:
					{
						//SetMenuOptions("CALENDAR");
						break;
					}
			}
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToInt32(
		private int GetCurrentW2Year()
		{
			int GetCurrentW2Year = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("select distinct [year] from tblw2edittable", "Payroll");
			if (!rsData.EndOfFile())
			{
				GetCurrentW2Year = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("year"))));
			}
			else
			{
				rsData.OpenRecordset("select max([year]) as maxyear from tblw2original", "Payroll");
				if (!rsData.EndOfFile())
				{
					GetCurrentW2Year = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("maxyear"))));
				}
				else
				{
					GetCurrentW2Year = DateTime.Today.Year;
				}
			}
			return GetCurrentW2Year;
		}

        private bool WasPayrollProcessCompleted(string strPriorStep, string strNextStep)
        {
            try
            {
				var  rsData = new clsDRWrapper();
                rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + modGlobalVariables.Statics.gdatCurrentPayDate.FormatAndPadShortDate() + "' AND PayRunID = " + modGlobalVariables.Statics.gintCurrentPayRun, "Payroll");
                if (rsData.EndOfFile())
                {
                    return false;
                }

                if (rsData.Get_Fields_Boolean(strPriorStep))
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {

            }

            return false;
        }
    }
}

