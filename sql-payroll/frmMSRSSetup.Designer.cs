//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmMSRSSetup.
	/// </summary>
	partial class frmMSRSSetup
	{
		public fecherFoundation.FCComboBox cmbRange;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTeacherInfo;
		public System.Collections.Generic.List<T2KDateBox> t2kPaidDatesEnding;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid GridPremiums;
		public fecherFoundation.FCTextBox txtEmployerCode;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCTextBox txtPreparersName;
		public fecherFoundation.FCTextBox txtBasicRate;
		public fecherFoundation.FCTextBox txtDepARate;
		public fecherFoundation.FCTextBox txtDepBRate;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid GridCreditsDebits;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCFrame framTeachers;
		public fecherFoundation.FCTextBox txtPayrollPayments;
		public fecherFoundation.FCTextBox txtDaysWorked;
		public fecherFoundation.FCTextBox txtOldFedFundPerc;
		public fecherFoundation.FCTextBox txtEmployeesCoveredLast;
		public fecherFoundation.FCTextBox txtEmployeesSeparatedLast;
		public fecherFoundation.FCTextBox txtEmployeesSeparatedThis;
		public fecherFoundation.FCLabel lblTeacherInfo_0;
		public fecherFoundation.FCLabel lblTeacherInfo_1;
		public fecherFoundation.FCLabel lblTeacherInfo_2;
		public fecherFoundation.FCLabel lblTeacherInfo_3;
		public fecherFoundation.FCLabel lblTeacherInfo_4;
		public fecherFoundation.FCLabel lblTeacherInfo_5;
		public fecherFoundation.FCCheckBox chkAutoMatch;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtCompensationAdjustments;
		public fecherFoundation.FCTextBox txtEmployerContributionAdjustments;
		public fecherFoundation.FCTextBox txtPurchaseAgreements;
		public fecherFoundation.FCTextBox txtFedFundPerc;
		public fecherFoundation.FCLabel lblTeacherInfo_6;
		public fecherFoundation.FCLabel lblTeacherInfo_7;
		public fecherFoundation.FCLabel lblTeacherInfo_8;
		public fecherFoundation.FCLabel lblTeacherInfo_9;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCComboBox cmbReportType;
		public fecherFoundation.FCCheckBox chkElectronicFile;
		public Global.T2KPhoneNumberBox txtTelephone;
		public Global.T2KDateBox t2kPaidDatesEnding_0;
		public Global.T2KDateBox t2kPaidDatesEnding_1;
		public Global.T2KDateBox t2kPaidDatesEnding_2;
		public Global.T2KDateBox t2kPaidDatesEnding_3;
		public Global.T2KDateBox t2kPaidDatesEnding_4;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEditReview;
		public fecherFoundation.FCToolStripMenuItem mnusepar2;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnusepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.GridPremiums = new fecherFoundation.FCGrid();
			this.txtEmployerCode = new fecherFoundation.FCTextBox();
			this.txtEmployerName = new fecherFoundation.FCTextBox();
			this.txtPreparersName = new fecherFoundation.FCTextBox();
			this.txtBasicRate = new fecherFoundation.FCTextBox();
			this.txtDepARate = new fecherFoundation.FCTextBox();
			this.txtDepBRate = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.GridCreditsDebits = new fecherFoundation.FCGrid();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.framTeachers = new fecherFoundation.FCFrame();
			this.txtPayrollPayments = new fecherFoundation.FCTextBox();
			this.txtDaysWorked = new fecherFoundation.FCTextBox();
			this.txtOldFedFundPerc = new fecherFoundation.FCTextBox();
			this.txtEmployeesCoveredLast = new fecherFoundation.FCTextBox();
			this.txtEmployeesSeparatedLast = new fecherFoundation.FCTextBox();
			this.txtEmployeesSeparatedThis = new fecherFoundation.FCTextBox();
			this.lblTeacherInfo_0 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_1 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_2 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_3 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_4 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_5 = new fecherFoundation.FCLabel();
			this.chkAutoMatch = new fecherFoundation.FCCheckBox();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.txtCompensationAdjustments = new fecherFoundation.FCTextBox();
			this.txtEmployerContributionAdjustments = new fecherFoundation.FCTextBox();
			this.txtPurchaseAgreements = new fecherFoundation.FCTextBox();
			this.txtFedFundPerc = new fecherFoundation.FCTextBox();
			this.lblTeacherInfo_6 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_7 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_8 = new fecherFoundation.FCLabel();
			this.lblTeacherInfo_9 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.cmbReportType = new fecherFoundation.FCComboBox();
			this.chkElectronicFile = new fecherFoundation.FCCheckBox();
			this.txtTelephone = new Global.T2KPhoneNumberBox();
			this.t2kPaidDatesEnding_0 = new Global.T2KDateBox();
			this.t2kPaidDatesEnding_1 = new Global.T2KDateBox();
			this.t2kPaidDatesEnding_2 = new Global.T2KDateBox();
			this.t2kPaidDatesEnding_3 = new Global.T2KDateBox();
			this.t2kPaidDatesEnding_4 = new Global.T2KDateBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditReview = new fecherFoundation.FCToolStripMenuItem();
			this.mnusepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnusepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdEdit = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPremiums)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridCreditsDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framTeachers)).BeginInit();
			this.framTeachers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkElectronicFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(878, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.txtEmployerCode);
			this.ClientArea.Controls.Add(this.txtEmployerName);
			this.ClientArea.Controls.Add(this.txtPreparersName);
			this.ClientArea.Controls.Add(this.txtBasicRate);
			this.ClientArea.Controls.Add(this.txtDepARate);
			this.ClientArea.Controls.Add(this.txtDepBRate);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.framTeachers);
			this.ClientArea.Controls.Add(this.chkAutoMatch);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.chkElectronicFile);
			this.ClientArea.Controls.Add(this.txtTelephone);
			this.ClientArea.Controls.Add(this.t2kPaidDatesEnding_0);
			this.ClientArea.Controls.Add(this.t2kPaidDatesEnding_1);
			this.ClientArea.Controls.Add(this.t2kPaidDatesEnding_2);
			this.ClientArea.Controls.Add(this.t2kPaidDatesEnding_3);
			this.ClientArea.Controls.Add(this.t2kPaidDatesEnding_4);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Size = new System.Drawing.Size(878, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdEdit);
			this.TopPanel.Size = new System.Drawing.Size(878, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEdit, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(217, 30);
			this.HeaderText.Text = "MainePERS Setup";
			// 
			// cmbRange
			// 
			this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Range"});
			this.cmbRange.Location = new System.Drawing.Point(20, 56);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(119, 40);
			this.cmbRange.TabIndex = 3;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.GridPremiums);
			this.Frame1.Location = new System.Drawing.Point(30, 267);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(392, 145);
			this.Frame1.TabIndex = 10;
			this.Frame1.Text = "Premiums";
			// 
			// GridPremiums
			// 
			this.GridPremiums.Cols = 4;
			this.GridPremiums.Rows = 2;
			this.GridPremiums.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPremiums.FixedCols = 0;
			this.GridPremiums.Location = new System.Drawing.Point(0, 30);
			this.GridPremiums.Name = "GridPremiums";
			this.GridPremiums.ReadOnly = false;
			this.GridPremiums.RowHeadersVisible = false;
			this.GridPremiums.Size = new System.Drawing.Size(392, 94);
			this.GridPremiums.StandardTab = false;
			this.GridPremiums.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridPremiums.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridPremiums_ValidateEdit);
			this.GridPremiums.CurrentCellChanged += new System.EventHandler(this.GridPremiums_RowColChange);
			// 
			// txtEmployerCode
			// 
			this.txtEmployerCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployerCode.Location = new System.Drawing.Point(442, 67);
			this.txtEmployerCode.Name = "txtEmployerCode";
			this.txtEmployerCode.Size = new System.Drawing.Size(396, 40);
			this.txtEmployerCode.TabIndex = 3;
			this.txtEmployerCode.Text = "Text5";
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployerName.Location = new System.Drawing.Point(442, 117);
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Size = new System.Drawing.Size(396, 40);
			this.txtEmployerName.TabIndex = 5;
			this.txtEmployerName.Text = "Text5";
			// 
			// txtPreparersName
			// 
			this.txtPreparersName.BackColor = System.Drawing.SystemColors.Window;
			this.txtPreparersName.Location = new System.Drawing.Point(442, 167);
			this.txtPreparersName.Name = "txtPreparersName";
			this.txtPreparersName.Size = new System.Drawing.Size(396, 40);
			this.txtPreparersName.TabIndex = 7;
			this.txtPreparersName.Text = "Text5";
			// 
			// txtBasicRate
			// 
			this.txtBasicRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtBasicRate.Location = new System.Drawing.Point(676, 267);
			this.txtBasicRate.Name = "txtBasicRate";
			this.txtBasicRate.Size = new System.Drawing.Size(162, 40);
			this.txtBasicRate.TabIndex = 12;
			this.txtBasicRate.Text = "0.0000";
			this.txtBasicRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtDepARate
			// 
			this.txtDepARate.BackColor = System.Drawing.SystemColors.Window;
			this.txtDepARate.Location = new System.Drawing.Point(676, 317);
			this.txtDepARate.Name = "txtDepARate";
			this.txtDepARate.Size = new System.Drawing.Size(162, 40);
			this.txtDepARate.TabIndex = 14;
			this.txtDepARate.Text = "0.0000";
			this.txtDepARate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtDepBRate
			// 
			this.txtDepBRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtDepBRate.Location = new System.Drawing.Point(676, 367);
			this.txtDepBRate.Name = "txtDepBRate";
			this.txtDepBRate.Size = new System.Drawing.Size(162, 40);
			this.txtDepBRate.TabIndex = 16;
			this.txtDepBRate.Text = "0.0000";
			this.txtDepBRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.GridCreditsDebits);
			this.Frame2.Location = new System.Drawing.Point(442, 454);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(396, 182);
			this.Frame2.TabIndex = 19;
			this.Frame2.Text = "(Credits)/Debits";
			// 
			// GridCreditsDebits
			// 
			this.GridCreditsDebits.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridCreditsDebits.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridCreditsDebits.ExtendLastCol = true;
			this.GridCreditsDebits.FixedCols = 0;
			this.GridCreditsDebits.Location = new System.Drawing.Point(0, 30);
			this.GridCreditsDebits.Name = "GridCreditsDebits";
			this.GridCreditsDebits.ReadOnly = false;
			this.GridCreditsDebits.RowHeadersVisible = false;
			this.GridCreditsDebits.Rows = 3;
			this.GridCreditsDebits.Size = new System.Drawing.Size(396, 152);
			this.GridCreditsDebits.StandardTab = false;
			this.GridCreditsDebits.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridCreditsDebits.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridCreditsDebits_ValidateEdit);
			this.GridCreditsDebits.CurrentCellChanged += new System.EventHandler(this.GridCreditsDebits_RowColChange);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.txtStart);
			this.Frame4.Controls.Add(this.cmbRange);
			this.Frame4.Controls.Add(this.txtEnd);
			this.Frame4.Controls.Add(this.lblTo);
			this.Frame4.Controls.Add(this.lblSequence);
			this.Frame4.Location = new System.Drawing.Point(30, 721);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(392, 116);
			this.Frame4.TabIndex = 26;
			this.Frame4.Text = "Records";
			// 
			// txtStart
			// 
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.Location = new System.Drawing.Point(159, 56);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(80, 40);
			this.txtStart.TabIndex = 2;
			this.txtStart.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtStart.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.Location = new System.Drawing.Point(292, 56);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(80, 40);
			this.txtEnd.TabIndex = 4;
			this.txtEnd.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtEnd.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(255, 70);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(17, 16);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTo.Visible = false;
			// 
			// lblSequence
			// 
			this.lblSequence.Location = new System.Drawing.Point(159, 30);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(80, 16);
			this.lblSequence.TabIndex = 1;
			this.lblSequence.Text = "SEQUENCE";
			this.lblSequence.Visible = false;
			// 
			// framTeachers
			// 
			this.framTeachers.Controls.Add(this.txtPayrollPayments);
			this.framTeachers.Controls.Add(this.txtDaysWorked);
			this.framTeachers.Controls.Add(this.txtOldFedFundPerc);
			this.framTeachers.Controls.Add(this.txtEmployeesCoveredLast);
			this.framTeachers.Controls.Add(this.txtEmployeesSeparatedLast);
			this.framTeachers.Controls.Add(this.txtEmployeesSeparatedThis);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_0);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_1);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_2);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_3);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_4);
			this.framTeachers.Controls.Add(this.lblTeacherInfo_5);
			this.framTeachers.Location = new System.Drawing.Point(30, 847);
			this.framTeachers.Name = "framTeachers";
			this.framTeachers.Size = new System.Drawing.Size(808, 190);
			this.framTeachers.TabIndex = 38;
			this.framTeachers.Text = "Teacher Summary Page Information";
			this.framTeachers.Visible = false;
			// 
			// txtPayrollPayments
			// 
			this.txtPayrollPayments.BackColor = System.Drawing.SystemColors.Window;
			this.txtPayrollPayments.Location = new System.Drawing.Point(358, 30);
			this.txtPayrollPayments.Name = "txtPayrollPayments";
			this.txtPayrollPayments.Size = new System.Drawing.Size(80, 40);
			this.txtPayrollPayments.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtDaysWorked
			// 
			this.txtDaysWorked.BackColor = System.Drawing.SystemColors.Window;
			this.txtDaysWorked.Location = new System.Drawing.Point(358, 80);
			this.txtDaysWorked.Name = "txtDaysWorked";
			this.txtDaysWorked.Size = new System.Drawing.Size(80, 40);
			this.txtDaysWorked.TabIndex = 5;
			this.txtDaysWorked.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtOldFedFundPerc
			// 
			this.txtOldFedFundPerc.BackColor = System.Drawing.SystemColors.Window;
			this.txtOldFedFundPerc.Location = new System.Drawing.Point(358, 130);
			this.txtOldFedFundPerc.Name = "txtOldFedFundPerc";
			this.txtOldFedFundPerc.Size = new System.Drawing.Size(80, 40);
			this.txtOldFedFundPerc.TabIndex = 9;
			this.txtOldFedFundPerc.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtEmployeesCoveredLast
			// 
			this.txtEmployeesCoveredLast.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployeesCoveredLast.Location = new System.Drawing.Point(708, 30);
			this.txtEmployeesCoveredLast.Name = "txtEmployeesCoveredLast";
			this.txtEmployeesCoveredLast.Size = new System.Drawing.Size(80, 40);
			this.txtEmployeesCoveredLast.TabIndex = 3;
			this.txtEmployeesCoveredLast.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtEmployeesSeparatedLast
			// 
			this.txtEmployeesSeparatedLast.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployeesSeparatedLast.Location = new System.Drawing.Point(708, 80);
			this.txtEmployeesSeparatedLast.Name = "txtEmployeesSeparatedLast";
			this.txtEmployeesSeparatedLast.Size = new System.Drawing.Size(80, 40);
			this.txtEmployeesSeparatedLast.TabIndex = 7;
			this.txtEmployeesSeparatedLast.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtEmployeesSeparatedThis
			// 
			this.txtEmployeesSeparatedThis.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployeesSeparatedThis.Location = new System.Drawing.Point(708, 130);
			this.txtEmployeesSeparatedThis.Name = "txtEmployeesSeparatedThis";
			this.txtEmployeesSeparatedThis.Size = new System.Drawing.Size(80, 40);
			this.txtEmployeesSeparatedThis.TabIndex = 11;
			this.txtEmployeesSeparatedThis.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblTeacherInfo_0
			// 
			this.lblTeacherInfo_0.Location = new System.Drawing.Point(20, 44);
			this.lblTeacherInfo_0.Name = "lblTeacherInfo_0";
			this.lblTeacherInfo_0.Size = new System.Drawing.Size(320, 16);
			this.lblTeacherInfo_0.TabIndex = 1;
			this.lblTeacherInfo_0.Text = "NUMBER OF PAYROLL PAYMENTS IN CONTRACT YEAR";
			// 
			// lblTeacherInfo_1
			// 
			this.lblTeacherInfo_1.Location = new System.Drawing.Point(20, 94);
			this.lblTeacherInfo_1.Name = "lblTeacherInfo_1";
			this.lblTeacherInfo_1.Size = new System.Drawing.Size(320, 16);
			this.lblTeacherInfo_1.TabIndex = 4;
			this.lblTeacherInfo_1.Text = "NUMBER OF DAYS WORKED IN CONTRACT YEAR";
			// 
			// lblTeacherInfo_2
			// 
			this.lblTeacherInfo_2.Location = new System.Drawing.Point(20, 144);
			this.lblTeacherInfo_2.Name = "lblTeacherInfo_2";
			this.lblTeacherInfo_2.Size = new System.Drawing.Size(320, 16);
			this.lblTeacherInfo_2.TabIndex = 8;
			this.lblTeacherInfo_2.Text = "FED FUNDED PERCENT";
			// 
			// lblTeacherInfo_3
			// 
			this.lblTeacherInfo_3.Location = new System.Drawing.Point(458, 44);
			this.lblTeacherInfo_3.Name = "lblTeacherInfo_3";
			this.lblTeacherInfo_3.Size = new System.Drawing.Size(230, 16);
			this.lblTeacherInfo_3.TabIndex = 2;
			this.lblTeacherInfo_3.Text = "EMPLOYEES COVERED LAST REPORT";
			// 
			// lblTeacherInfo_4
			// 
			this.lblTeacherInfo_4.Location = new System.Drawing.Point(458, 94);
			this.lblTeacherInfo_4.Name = "lblTeacherInfo_4";
			this.lblTeacherInfo_4.Size = new System.Drawing.Size(230, 16);
			this.lblTeacherInfo_4.TabIndex = 6;
			this.lblTeacherInfo_4.Text = "EMPLOYEES SEPARATED LAST MONTH";
			// 
			// lblTeacherInfo_5
			// 
			this.lblTeacherInfo_5.Location = new System.Drawing.Point(458, 144);
			this.lblTeacherInfo_5.Name = "lblTeacherInfo_5";
			this.lblTeacherInfo_5.Size = new System.Drawing.Size(230, 16);
			this.lblTeacherInfo_5.TabIndex = 10;
			this.lblTeacherInfo_5.Text = "EMPLOYEES SEPARATED THIS MONTH";
			// 
			// chkAutoMatch
			// 
			this.chkAutoMatch.Location = new System.Drawing.Point(30, 417);
			this.chkAutoMatch.Name = "chkAutoMatch";
			this.chkAutoMatch.Size = new System.Drawing.Size(278, 27);
			this.chkAutoMatch.TabIndex = 17;
			this.chkAutoMatch.Text = "Auto Calc Employers Match Credit";
			this.chkAutoMatch.CheckedChanged += new System.EventHandler(this.chkAutoMatch_CheckedChanged);
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.txtCompensationAdjustments);
			this.Frame5.Controls.Add(this.txtEmployerContributionAdjustments);
			this.Frame5.Controls.Add(this.txtPurchaseAgreements);
			this.Frame5.Controls.Add(this.txtFedFundPerc);
			this.Frame5.Controls.Add(this.lblTeacherInfo_6);
			this.Frame5.Controls.Add(this.lblTeacherInfo_7);
			this.Frame5.Controls.Add(this.lblTeacherInfo_8);
			this.Frame5.Controls.Add(this.lblTeacherInfo_9);
			this.Frame5.Location = new System.Drawing.Point(30, 847);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(808, 190);
			this.Frame5.TabIndex = 27;
			this.Frame5.Text = "Teacher Summary Page Information";
			// 
			// txtCompensationAdjustments
			// 
			this.txtCompensationAdjustments.BackColor = System.Drawing.SystemColors.Window;
			this.txtCompensationAdjustments.Location = new System.Drawing.Point(497, 30);
			this.txtCompensationAdjustments.Name = "txtCompensationAdjustments";
			this.txtCompensationAdjustments.Size = new System.Drawing.Size(136, 40);
			this.txtCompensationAdjustments.TabIndex = 1;
			this.txtCompensationAdjustments.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtEmployerContributionAdjustments
			// 
			this.txtEmployerContributionAdjustments.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployerContributionAdjustments.Location = new System.Drawing.Point(497, 80);
			this.txtEmployerContributionAdjustments.Name = "txtEmployerContributionAdjustments";
			this.txtEmployerContributionAdjustments.Size = new System.Drawing.Size(136, 40);
			this.txtEmployerContributionAdjustments.TabIndex = 3;
			this.txtEmployerContributionAdjustments.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtPurchaseAgreements
			// 
			this.txtPurchaseAgreements.BackColor = System.Drawing.SystemColors.Window;
			this.txtPurchaseAgreements.Location = new System.Drawing.Point(497, 130);
			this.txtPurchaseAgreements.Name = "txtPurchaseAgreements";
			this.txtPurchaseAgreements.Size = new System.Drawing.Size(136, 40);
			this.txtPurchaseAgreements.TabIndex = 5;
			this.txtPurchaseAgreements.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtFedFundPerc
			// 
			this.txtFedFundPerc.BackColor = System.Drawing.SystemColors.Window;
			this.txtFedFundPerc.Location = new System.Drawing.Point(653, 130);
			this.txtFedFundPerc.Name = "txtFedFundPerc";
			this.txtFedFundPerc.Size = new System.Drawing.Size(135, 40);
			this.txtFedFundPerc.TabIndex = 7;
			this.txtFedFundPerc.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblTeacherInfo_6
			// 
			this.lblTeacherInfo_6.Location = new System.Drawing.Point(20, 44);
			this.lblTeacherInfo_6.Name = "lblTeacherInfo_6";
			this.lblTeacherInfo_6.Size = new System.Drawing.Size(450, 16);
			this.lblTeacherInfo_6.TabIndex = 8;
			this.lblTeacherInfo_6.Text = "ADJUSTMENTS TO PRIOR PERIOD GRANT FUNDED COMPENSATION";
			// 
			// lblTeacherInfo_7
			// 
			this.lblTeacherInfo_7.Location = new System.Drawing.Point(20, 94);
			this.lblTeacherInfo_7.Name = "lblTeacherInfo_7";
			this.lblTeacherInfo_7.Size = new System.Drawing.Size(450, 16);
			this.lblTeacherInfo_7.TabIndex = 2;
			this.lblTeacherInfo_7.Text = "ADJUSTMENTS TO PRIOR PERIOD GRANT FUNDED EMPLOYER CONTRIBUTIONS";
			// 
			// lblTeacherInfo_8
			// 
			this.lblTeacherInfo_8.Location = new System.Drawing.Point(20, 144);
			this.lblTeacherInfo_8.Name = "lblTeacherInfo_8";
			this.lblTeacherInfo_8.Size = new System.Drawing.Size(450, 16);
			this.lblTeacherInfo_8.TabIndex = 4;
			this.lblTeacherInfo_8.Text = "TOTAL OF PURCHASE AGREEMENTS (AUTHORIZED BY MAINEPERS)";
			// 
			// lblTeacherInfo_9
			// 
			this.lblTeacherInfo_9.Location = new System.Drawing.Point(653, 104);
			this.lblTeacherInfo_9.Name = "lblTeacherInfo_9";
			this.lblTeacherInfo_9.Size = new System.Drawing.Size(135, 16);
			this.lblTeacherInfo_9.TabIndex = 6;
			this.lblTeacherInfo_9.Text = "FED FUNDED PERCENT";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.cmbReportType);
			this.Frame3.Location = new System.Drawing.Point(30, 67);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(253, 90);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Report Type";
			// 
			// cmbReportType
			// 
			this.cmbReportType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbReportType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbReportType.Location = new System.Drawing.Point(20, 30);
			this.cmbReportType.Name = "cmbReportType";
			this.cmbReportType.Size = new System.Drawing.Size(213, 40);
			this.cmbReportType.Text = "Combo1";
			this.cmbReportType.SelectedIndexChanged += new System.EventHandler(this.cmbReportType_SelectedIndexChanged);
			// 
			// chkElectronicFile
			// 
			this.chkElectronicFile.Location = new System.Drawing.Point(30, 30);
			this.chkElectronicFile.Name = "chkElectronicFile";
			this.chkElectronicFile.Size = new System.Drawing.Size(273, 27);
			this.chkElectronicFile.TabIndex = 0;
			this.chkElectronicFile.Text = "Create Electronic MainePERS file";
			// 
			// txtTelephone
			// 
			this.txtTelephone.Location = new System.Drawing.Point(442, 217);
			this.txtTelephone.MaxLength = 13;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Size = new System.Drawing.Size(150, 40);
			this.txtTelephone.TabIndex = 9;
			// 
			// t2kPaidDatesEnding_0
			// 
			this.t2kPaidDatesEnding_0.Location = new System.Drawing.Point(30, 671);
			this.t2kPaidDatesEnding_0.MaxLength = 10;
			this.t2kPaidDatesEnding_0.Name = "t2kPaidDatesEnding_0";
			this.t2kPaidDatesEnding_0.Size = new System.Drawing.Size(115, 40);
			this.t2kPaidDatesEnding_0.TabIndex = 21;
			// 
			// t2kPaidDatesEnding_1
			// 
			this.t2kPaidDatesEnding_1.Location = new System.Drawing.Point(153, 671);
			this.t2kPaidDatesEnding_1.MaxLength = 10;
			this.t2kPaidDatesEnding_1.Name = "t2kPaidDatesEnding_1";
			this.t2kPaidDatesEnding_1.Size = new System.Drawing.Size(115, 40);
			this.t2kPaidDatesEnding_1.TabIndex = 22;
			// 
			// t2kPaidDatesEnding_2
			// 
			this.t2kPaidDatesEnding_2.Location = new System.Drawing.Point(276, 671);
			this.t2kPaidDatesEnding_2.MaxLength = 10;
			this.t2kPaidDatesEnding_2.Name = "t2kPaidDatesEnding_2";
			this.t2kPaidDatesEnding_2.Size = new System.Drawing.Size(115, 40);
			this.t2kPaidDatesEnding_2.TabIndex = 23;
			// 
			// t2kPaidDatesEnding_3
			// 
			this.t2kPaidDatesEnding_3.Location = new System.Drawing.Point(399, 671);
			this.t2kPaidDatesEnding_3.MaxLength = 10;
			this.t2kPaidDatesEnding_3.Name = "t2kPaidDatesEnding_3";
			this.t2kPaidDatesEnding_3.Size = new System.Drawing.Size(115, 40);
			this.t2kPaidDatesEnding_3.TabIndex = 24;
			// 
			// t2kPaidDatesEnding_4
			// 
			this.t2kPaidDatesEnding_4.Location = new System.Drawing.Point(522, 671);
			this.t2kPaidDatesEnding_4.MaxLength = 10;
			this.t2kPaidDatesEnding_4.Name = "t2kPaidDatesEnding_4";
			this.t2kPaidDatesEnding_4.Size = new System.Drawing.Size(115, 40);
			this.t2kPaidDatesEnding_4.TabIndex = 25;
			// 
			// Grid
			// 
			this.Grid.Cols = 3;
			this.Grid.ColumnHeadersHeight = 60;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.FixedCols = 0;
			this.Grid.FixedRows = 2;
			this.Grid.Location = new System.Drawing.Point(30, 454);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = false;
			this.Grid.RowHeadersVisible = false;
			this.Grid.Rows = 3;
			this.Grid.Size = new System.Drawing.Size(392, 182);
			this.Grid.StandardTab = false;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 18;
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
			this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(303, 81);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(110, 16);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "EMPLOYER CODE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(303, 131);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(110, 16);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "EMPLOYER NAME";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(303, 181);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(110, 16);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "PREPARER\'S NAME";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(303, 231);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(110, 16);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "TELEPHONE";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 646);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(122, 15);
			this.Label6.TabIndex = 20;
			this.Label6.Text = "PAID DATES ENDING";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(442, 281);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(210, 16);
			this.Label5.TabIndex = 11;
			this.Label5.Text = "EFFECTIVE BASIC INSURANCE RATE";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(442, 331);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(210, 16);
			this.Label7.TabIndex = 13;
			this.Label7.Text = "DEP. A RATE";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(442, 381);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(210, 16);
			this.Label8.TabIndex = 15;
			this.Label8.Text = "DEP. B RATE";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditReview,
            this.mnusepar2,
            this.mnuDelete,
            this.mnuSave,
            this.mnuSaveContinue,
            this.mnusepar,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuEditReview
			// 
			this.mnuEditReview.Index = 0;
			this.mnuEditReview.Name = "mnuEditReview";
			this.mnuEditReview.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuEditReview.Text = "Edit/Review MainePERS Records";
			this.mnuEditReview.Click += new System.EventHandler(this.mnuEditReview_Click);
			// 
			// mnusepar2
			// 
			this.mnusepar2.Index = 1;
			this.mnusepar2.Name = "mnusepar2";
			this.mnusepar2.Text = "-";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 2;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Current Code";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 4;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnusepar
			// 
			this.mnusepar.Index = 5;
			this.mnusepar.Name = "mnusepar";
			this.mnusepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdEdit
			// 
			this.cmdEdit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEdit.Location = new System.Drawing.Point(418, 29);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdEdit.Size = new System.Drawing.Size(215, 24);
			this.cmdEdit.TabIndex = 1;
			this.cmdEdit.Text = "Edit/Review MainePERS Records";
			this.cmdEdit.Click += new System.EventHandler(this.mnuEditReview_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(639, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(140, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete Current Code";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(346, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Location = new System.Drawing.Point(785, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSave.Size = new System.Drawing.Size(45, 24);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmMSRSSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(878, 688);
			this.FillColor = 0;
			this.Name = "frmMSRSSetup";
			this.Text = "MainePERS Setup";
			this.Load += new System.EventHandler(this.frmMSRSSetup_Load);
			this.Activated += new System.EventHandler(this.frmMSRSSetup_Activated);
			this.Resize += new System.EventHandler(this.frmMSRSSetup_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMSRSSetup_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridPremiums)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridCreditsDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framTeachers)).EndInit();
			this.framTeachers.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAutoMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkElectronicFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPaidDatesEnding_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSaveContinue;
		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdEdit;
	}
}