//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsTimeClockPlusToPayCategory
	{
		//=========================================================
		private clsDRWrapper rsLoad = new clsDRWrapper();

		public clsTimeClockPlusToPayCategory() : base()
		{
			string strSQL;
			strSQL = "Select * from TIMECLOCKCategories order by timecardcategory";
			rsLoad.OpenRecordset(strSQL, "Twpy0000.vb1");
		}

		public int GetPayrollCategory(string strTimeCategory)
		{
			int GetPayrollCategory = 0;
			if (!(rsLoad.EndOfFile() && rsLoad.BeginningOfFile()))
			{
				if (rsLoad.FindFirstRecord("timecardcategory", strTimeCategory))
				{
					GetPayrollCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("payrollcategory"))));
				}
				else
				{
					GetPayrollCategory = 0;
				}
			}
			else
			{
				GetPayrollCategory = 0;
			}
			return GetPayrollCategory;
		}
	}
}
