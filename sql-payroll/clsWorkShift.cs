﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class clsWorkShift
	{
		//=========================================================
		private int lngID;
		private int lngShiftType;
		private string strScheduledStart = string.Empty;
		private string strScheduledEnd = string.Empty;
		private string strActualStart = string.Empty;
		private string strActualEnd = string.Empty;
		private double dblLunchTime;
		private bool boolUnused;
		private string strAccount = string.Empty;

		public string Account
		{
			set
			{
				strAccount = fecherFoundation.Strings.Trim(Strings.Replace(value, "_", "X", 1, -1, CompareConstants.vbTextCompare));
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double LunchTime
		{
			set
			{
				dblLunchTime = value;
			}
			get
			{
				double LunchTime = 0;
				LunchTime = dblLunchTime;
				return LunchTime;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public int ShiftType
		{
			set
			{
				lngShiftType = value;
			}
			get
			{
				int ShiftType = 0;
				ShiftType = lngShiftType;
				return ShiftType;
			}
		}

		public string ScheduledStart
		{
			set
			{
				strScheduledStart = value;
			}
			get
			{
				string ScheduledStart = "";
				ScheduledStart = strScheduledStart;
				return ScheduledStart;
			}
		}

		public string ScheduledEnd
		{
			set
			{
				strScheduledEnd = value;
			}
			get
			{
				string ScheduledEnd = "";
				ScheduledEnd = strScheduledEnd;
				return ScheduledEnd;
			}
		}

		public string ActualEnd
		{
			set
			{
				strActualEnd = value;
			}
			get
			{
				string ActualEnd = "";
				ActualEnd = strActualEnd;
				return ActualEnd;
			}
		}

		public string ActualStart
		{
			set
			{
				strActualStart = value;
			}
			get
			{
				string ActualStart = "";
				ActualStart = strActualStart;
				return ActualStart;
			}
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public bool UnScheduled
		{
			get
			{
				bool UnScheduled = false;
				bool boolReturn;
				boolReturn = false;
				if (fecherFoundation.Strings.UCase(strScheduledStart) == "UNSCHEDULED" || fecherFoundation.Strings.UCase(strScheduledEnd) == "UNSCHEDULED" || fecherFoundation.Strings.Trim(strScheduledStart) == "" || fecherFoundation.Strings.Trim(strScheduledEnd) == "")
				{
					boolReturn = true;
				}
				else
				{
					if (Information.IsDate(strScheduledStart) && Information.IsDate(strScheduledEnd))
					{
						boolReturn = false;
					}
					else
					{
						boolReturn = true;
					}
				}
				UnScheduled = boolReturn;
				return UnScheduled;
			}
		}
	}
}
