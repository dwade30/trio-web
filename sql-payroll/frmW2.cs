//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2 : BaseForm
	{
		public frmW2()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2 InstancePtr
		{
			get
			{
				return (frmW2)Sys.GetInstance(typeof(frmW2));
			}
		}

		protected frmW2 _InstancePtr = null;
		//=========================================================
		private bool boolLoad;
		private bool boolSave;
		private string strYear = "";
		private bool pboolNewSave;
		private clsDRWrapper rsData = new clsDRWrapper();
		private bool boolOriginal;
		bool boolQuit;

        private void frmW2_Activated(object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:#2689 - moved code to load()
			//if (boolLoad)
			//{
			//	boolSave = false;
			//	this.ShowWait();
			//	FCUtils.StartTask(this, () =>
			//	{
			//		LoadGrid();
			//		//this.EndWait();

			//		if (boolQuit)
			//		{
			//			Close();
			//			this.EndWait();
			//			return;
			//		}
			//		if (!boolSave)
			//		{
			//			if (!boolOriginal)
			//			{
			//				rsData.Execute("Delete from tblW2EditTable", "Payroll");
			//				rsData.Execute("Delete from tblW2Deductions", "Payroll");
			//				rsData.Execute("Delete from tblW2Matches", "Payroll");
			//			}
			//			this.EndWait();
			//			Close();
			//		}
			//		else
			//		{
			//			boolLoad = false;
			//			if (modGlobalVariables.Statics.gstrW2Status == "SAVE")
			//			{
			//				boolSave = false;
			//				mnuProcess_Click();
			//				if (boolSave)
			//				{
			//					if (CheckSSN())
			//					{
			//					}
			//					else
			//					{
			//						rsData.Execute("Delete from tblW2EditTable", "Payroll");
			//						rsData.Execute("Delete from tblW2Deductions", "Payroll");
			//						rsData.Execute("Delete from tblW2Matches", "Payroll");
			//						this.EndWait();
			//						Close();
			//						return;
			//					}
			//					// CHECK TO SEE IF THERE ARE DUPLICATE EMPLOYEES WITH THE SAME
			//					// SSN AND IF SO THEN COMBINE THEM SO THAT THERE IS ONLY ONE.
			//					CombineSSN();
			//					// NOW ADD THE DATA TO THE NEW ORIGINAL ARCHIVE TABLE
			//					// MATTHEW 12/1/2004
			//					clsDRWrapper rsTable = new clsDRWrapper();
			//					string strFields = "";
			//					// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			//					int x;
			//				// CREATE A NEW TABLE IF IT DOESN'T EXIST
			//				FillTable:
			//					;
			//					if (pboolNewSave)
			//					{
			//						App.MainForm.ShowLoader = true;
			//						rsTable.OpenRecordset("SELECT * FROM tblW2EditTable");
			//						strFields = "";
			//						for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
			//						{
			//							if (rsTable.Get_FieldsIndexName(x) == "ID")
			//							{
			//							}
			//							else
			//							{
			//								strFields += rsTable.Get_FieldsIndexName(x) + ", ";
			//							}
			//						}
			//						strFields = Strings.Left(strFields, strFields.Length - 2);
			//						rsTable.Execute("Delete from tblW2Original", "Payroll");
			//						rsTable.Execute("INSERT INTO tblW2Original (" + strFields + ") SELECT " + strFields + " FROM tblW2EditTable where deleterecord <> 1", "Payroll");
			//						rsTable.OpenRecordset("SELECT * FROM tblW2Deductions");
			//						strFields = "";
			//						for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
			//						{
			//							if (rsTable.Get_FieldsIndexName(x) == "ID")
			//							{
			//							}
			//							else
			//							{
			//								strFields += rsTable.Get_FieldsIndexName(x) + ", ";
			//							}
			//						}
			//						strFields = Strings.Left(strFields, strFields.Length - 2);
			//						rsTable.Execute("Delete from tblW2OriginalDeductions", "Payroll");
			//						rsTable.Execute("INSERT INTO tblW2OriginalDeductions (" + strFields + ") SELECT " + strFields + " FROM tblW2Deductions", "Payroll");
			//						rsTable.OpenRecordset("SELECT * FROM tblW2Matches");
			//						strFields = "";
			//						for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
			//						{
			//							if (rsTable.Get_FieldsIndexName(x) == "ID")
			//							{
			//							}
			//							else
			//							{
			//								strFields += rsTable.Get_FieldsIndexName(x) + ", ";
			//							}
			//						}
			//						strFields = Strings.Left(strFields, strFields.Length - 2);
			//						rsTable.Execute("Delete from tblW2OriginalMatches", "Payroll");
			//						rsTable.Execute("INSERT INTO tblW2OriginalMatches (" + strFields + ") SELECT " + strFields + " FROM tblW2Matches", "Payroll");
			//					}
			//					clsDRWrapper rsExecute = new clsDRWrapper();
			//					clsDRWrapper rsEmployee = new clsDRWrapper();
			//					rsData.OpenRecordset("Select Distinct EmployeeNumber from tblW2EditTable", "TWPY0000.vb1");
			//					rsEmployee.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
			//					while (!rsData.EndOfFile())
			//					{
			//						if (rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
			//						{
			//							rsExecute.Execute("Update tblW2Original Set FirstName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("FirstName"))) + "', MiddleName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("MiddleName"))) + "', LastName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields_String("LastName"))))) + "' ,desig = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("desig"))))) + "' Where EmployeeNumber ='" + rsData.Get_Fields("EmployeeNumber") + "'", "Payroll");
			//							// Call rsExecute.Execute("Update tblW2EditTable Set FirstName ='" & FixQuote(rsEmployee.Fields("FirstName")) & "',MiddleName ='" & FixQuote(rsEmployee.Fields("MiddleName")) & "',LastName ='" & FixQuote(Trim(rsEmployee.Fields("LastName") & " " & rsEmployee.Fields("desig"))) & "' Where EmployeeNumber ='" & rsData.Fields("EmployeeNumber") & "'")
			//							rsExecute.Execute("Update tblW2EditTable Set FirstName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("FirstName"))) + "', MiddleName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("MiddleName"))) + "', LastName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields_String("LastName"))))) + "' ,desig = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("desig"))))) + "' Where EmployeeNumber ='" + rsData.Get_Fields("EmployeeNumber") + "'", "Payroll");
			//						}
			//						rsData.MoveNext();
			//					}
			//					rsData = null;
			//					rsEmployee = null;
			//					rsExecute = null;
			//					modGlobalRoutines.UpdateW2StatusData("Save");
			//					App.MainForm.ShowLoader = false;
			//					MessageBox.Show("Save of W-2 Information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//				}
			//				this.EndWait();
			//				Close();
			//			}
			//			else if (modGlobalVariables.Statics.gstrW2Status == "UPDATE")
			//			{
			//			}
			//			else if (modGlobalVariables.Statics.gstrW2Status == "EDITREPORT")
			//			{
			//				frmReportViewer.InstancePtr.Init(rptW2EditReport.InstancePtr, showModal: this.Modal);
			//				//rptW2EditReport.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			//				this.EndWait();
			//				Close();
			//			}
			//		}
			//		this.EndWait();
			//	});
			//}
		}

		private void CombineSSN()
		{
			string strSSN = "";
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDup = new clsDRWrapper();
			clsDRWrapper rsDeductionDup = new clsDRWrapper();
			clsDRWrapper rsMatchDup = new clsDRWrapper();
			clsDRWrapper rsDeduction = new clsDRWrapper();
			clsDRWrapper rsMatch = new clsDRWrapper();
			int lngDeductionNumber;
			double dblCYTDAmount;
			string strDupEmployee = "";
			string strCode = "";
			bool boolFoundMatch = false;

            try
            {
                rsData.OpenRecordset("Select * from tblW2EditTable Order by EmployeeNumber", "TWPY0000.vb1");
                rsDup.OpenRecordset("Select * from tblW2EditTable", "TWPY0000.vb1");
                rsDeductionDup.OpenRecordset("Select * from tblW2Deductions Order by EmployeeNumber", "TWPY0000.vb1");
                rsMatchDup.OpenRecordset("Select * from tblW2Matches Order by EmployeeNumber", "TWPY0000.vb1");
                while (!rsData.EndOfFile())
                {
                    // rsDup.FindFirstRecord2 "SSN, EmployeeNumber, DeleteRecord", rsData.Fields("SSN") & "," & rsData.Fields("EmployeeNumber") & ",0", ","
                    // rsDup.FindFirst ("SSN = '" & rsData.Fields("SSN") & "' and Employeenumber <> '" & rsData.Fields("EmployeeNumber") & "' and DeleteRecord <> 1")
                    boolFoundMatch = false;
                    // If rsDup.NoMatch Then
                    if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("deleterecord")))
                    {
                        if (!rsData.EndOfFile())
                            rsData.MoveNext();
                    }
                    else if (rsDup.FindFirst("employeenumber = '" + rsData.Get_Fields("employeenumber") + "' and deleterecord = 1"))
                        {
                            // skip this one, it was already processed
                            if (!rsData.EndOfFile())
                                rsData.MoveNext();
                        }
                        else if (!rsDup.FindFirst("SSN = '" + rsData.Get_Fields_String("SSN") + "' and Employeenumber <> '" + rsData.Get_Fields("EmployeeNumber") + "' and DeleteRecord <> 1"))
                            {
                                if (!rsData.EndOfFile())
                                    rsData.MoveNext();
                            }
                            else
                            {
                                if (rsData.Get_Fields_Boolean("DeleteRecord") == true)
                                {
                                }
                                else
                                {
                                    boolFoundMatch = true;
                                    strDupEmployee = FCConvert.ToString(rsDup.Get_Fields("EmployeeNumber"));
                                    rsData.Edit();
                                    rsData.Set_Fields("TotalGross", rsData.Get_Fields_Double("TotalGross") + rsDup.Get_Fields_Double("TotalGross"));
                                    rsData.Set_Fields("FederalWage", rsData.Get_Fields_Double("FederalWage") + rsDup.Get_Fields_Double("FederalWage"));
                                    rsData.Set_Fields("FICAWage", rsData.Get_Fields("FICAWage") + rsDup.Get_Fields("FICAWage"));
                                    rsData.Set_Fields("MedicareWage", rsData.Get_Fields_Double("MedicareWage") + rsDup.Get_Fields_Double("MedicareWage"));
                                    rsData.Set_Fields("StateWage", rsData.Get_Fields_Double("StateWage") + rsDup.Get_Fields_Double("StateWage"));
                                    rsData.Set_Fields("FederalTax", rsData.Get_Fields_Double("FederalTax") + rsDup.Get_Fields_Double("FederalTax"));
                                    rsData.Set_Fields("FICATax", rsData.Get_Fields("FICATax") + rsDup.Get_Fields("FICATax"));
                                    rsData.Set_Fields("MedicareTax", rsData.Get_Fields("MedicareTax") + rsDup.Get_Fields("MedicareTax"));
                                    rsData.Set_Fields("StateTax", rsData.Get_Fields_Double("StateTax") + rsDup.Get_Fields_Double("StateTax"));
                                    rsData.Update();
                                    rsDup.Edit();
                                    rsDup.Set_Fields("DeleteRecord", true);
                                    rsDup.Update();
                                    rsDup.OpenRecordset("Select * from tblW2EditTable", "TWPY0000.vb1");
                                    // CHECK FOR DUPLICATES OF DEDUCTIONS
                                    // MATTHEW 11/11/2004
                                    rsDeduction.OpenRecordset("Select * from tblW2Deductions Where EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    rsDeductionDup.OpenRecordset("Select * from tblW2Deductions Where EmployeeNumber = '" + strDupEmployee + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    while (!rsDeduction.EndOfFile())
                                    {
                                        rsDeductionDup.FindFirstRecord2("EmployeeNumber, DeductionNumber", rsData.Get_Fields("EmployeeNumber") + "," + rsDeduction.Get_Fields_Int32("DeductionNumber"), ",");
                                        if (rsDeductionDup.NoMatch)
                                        {
                                        }
                                        else
                                        {
                                            rsDeduction.Edit();
                                            rsDeduction.Set_Fields("CYTDAmount", rsDeduction.Get_Fields_Double("CYTDAmount") + rsDeductionDup.Get_Fields_Double("CYTDAmount"));
                                            rsDeduction.Update();
                                            rsDeductionDup.Delete();
                                        }
                                        rsDeduction.MoveNext();
                                    }
                                    rsDeductionDup.OpenRecordset("Select * from tblW2Deductions Where EmployeeNumber = '" + strDupEmployee + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    if (rsDeductionDup.EndOfFile())
                                    {
                                    }
                                    else
                                    {
                                        // need to add duplicate employees deductions to master employee
                                        while (!rsDeductionDup.EndOfFile())
                                        {
                                            rsDeduction.AddNew();
                                            rsDeduction.Set_Fields("DeductionNumber", rsDeductionDup.Get_Fields_Int32("DeductionNumber"));
                                            rsDeduction.Set_Fields("Code", rsDeductionDup.Get_Fields("Code"));
                                            rsDeduction.Set_Fields("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
                                            rsDeduction.Set_Fields("CYTDAmount", rsDeductionDup.Get_Fields_Double("CYTDAmount"));
                                            rsDeduction.Update();
                                            rsDeductionDup.MoveNext();
                                        }
                                    }
                                    rsDeductionDup.Execute("Delete from tblW2Deductions Where EmployeeNumber = '" + strDupEmployee + "'", "TWPY0000.vb1");
                                    // check for duplicate matches
                                    rsMatch.OpenRecordset("Select * from tblW2Matches Where EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    rsMatchDup.OpenRecordset("Select * from tblW2Matches Where EmployeeNumber = '" + strDupEmployee + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    while (!rsMatch.EndOfFile())
                                    {
                                        rsMatchDup.FindFirstRecord2("EmployeeNumber, DeductionNumber", rsData.Get_Fields("EmployeeNumber") + "," + rsMatch.Get_Fields_Int32("DeductionNumber"), ",");
                                        if (rsMatchDup.NoMatch)
                                        {
                                        }
                                        else
                                        {
                                            rsMatch.Edit();
                                            rsMatch.Set_Fields("CYTDAmount", rsMatch.Get_Fields_Double("CYTDAmount") + rsMatchDup.Get_Fields_Double("CYTDAmount"));
                                            rsMatch.Update();
                                            rsMatchDup.Delete();
                                        }
                                        rsMatch.MoveNext();
                                    }
                                    rsMatchDup.OpenRecordset("Select * from tblW2Matches Where EmployeeNumber = '" + strDupEmployee + "' Order by EmployeeNumber", "TWPY0000.vb1");
                                    if (rsMatchDup.EndOfFile())
                                    {
                                    }
                                    else
                                    {
                                        // need to add duplicate employees deductions to master employee
                                        while (!rsMatchDup.EndOfFile())
                                        {
                                            rsMatch.AddNew();
                                            rsMatch.Set_Fields("DeductionNumber", rsMatchDup.Get_Fields_Int32("DeductionNumber"));
                                            rsMatch.Set_Fields("Code", rsMatchDup.Get_Fields("Code"));
                                            rsMatch.Set_Fields("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
                                            rsMatch.Set_Fields("CYTDAmount", rsMatchDup.Get_Fields_Double("CYTDAmount"));
                                            rsMatch.Update();
                                            rsMatchDup.MoveNext();
                                        }
                                    }
                                    rsMatchDup.Execute("Delete from tblW2Matches Where EmployeeNumber = '" + strDupEmployee + "'", "TWPY0000.vb1");
                                }
                                if (!boolFoundMatch)
                                {
                                    if (!rsData.EndOfFile())
                                        rsData.MoveNext();
                                }
                            }
                }
                rsData.Execute("Delete from tblW2EditTable where DeleteRecord = 1", "TWPY0000.vb1");
            }
            finally
            {
                rsData.DisposeOf();
				rsDeduction.DisposeOf();
				rsDeductionDup.DisposeOf();
				rsDup.DisposeOf();
				rsMatch.DisposeOf();
				rsMatchDup.DisposeOf();
            }
		}

		private void frmW2_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2 properties;
			//frmW2.ScaleWidth	= 8985;
			//frmW2.ScaleHeight	= 7590;
			//frmW2.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolQuit = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				//this.lblStatus.WidthOriginal = this.WidthOriginal - 1000;
				//this.pbrStatus.WidthOriginal = lblStatus.WidthOriginal;
				boolLoad = true;
				if (boolLoad)
				{
					boolSave = false;
					this.ShowWait(showprogressBar:true);
					FCUtils.StartTask(this, () =>
					{
						LoadGrid();

						if (boolQuit)
						{
							this.EndWait();
							Close();
							return;
						}
						if (!boolSave)
						{
							if (!boolOriginal)
							{
								rsData.Execute("Delete from tblW2EditTable", "Payroll");
								rsData.Execute("Delete from tblW2Deductions", "Payroll");
								rsData.Execute("Delete from tblW2Matches", "Payroll");
							}
							this.EndWait();
							Close();
						}
						else
						{
							boolLoad = false;
							if (modGlobalVariables.Statics.gstrW2Status == "SAVE")
							{
								boolSave = false;
								mnuProcess_Click();
								if (boolSave)
								{
									if (CheckSSN())
									{
									}
									else
									{
										rsData.Execute("Delete from tblW2EditTable", "Payroll");
										rsData.Execute("Delete from tblW2Deductions", "Payroll");
										rsData.Execute("Delete from tblW2Matches", "Payroll");
										this.EndWait();
										Close();
										return;
									}
									// CHECK TO SEE IF THERE ARE DUPLICATE EMPLOYEES WITH THE SAME
									// SSN AND IF SO THEN COMBINE THEM SO THAT THERE IS ONLY ONE.
									CombineSSN();
									// NOW ADD THE DATA TO THE NEW ORIGINAL ARCHIVE TABLE
									// MATTHEW 12/1/2004
									clsDRWrapper rsTable = new clsDRWrapper();
									string strFields = "";
									// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
									int x;
								// CREATE A NEW TABLE IF IT DOESN'T EXIST
								FillTable:
									;
									if (pboolNewSave)
									{
										//App.MainForm.ShowLoader = true;
										rsTable.OpenRecordset("SELECT * FROM tblW2EditTable");
										strFields = "";
										for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
										{
											if (rsTable.Get_FieldsIndexName(x) == "ID")
											{
											}
											else
											{
												strFields += rsTable.Get_FieldsIndexName(x) + ", ";
											}
										}
										strFields = Strings.Left(strFields, strFields.Length - 2);
										rsTable.Execute("Delete from tblW2Original", "Payroll");
										rsTable.Execute("INSERT INTO tblW2Original (" + strFields + ") SELECT " + strFields + " FROM tblW2EditTable where deleterecord <> 1", "Payroll");
										rsTable.OpenRecordset("SELECT * FROM tblW2Deductions");
										strFields = "";
										for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
										{
											if (rsTable.Get_FieldsIndexName(x) == "ID")
											{
											}
											else
											{
												strFields += rsTable.Get_FieldsIndexName(x) + ", ";
											}
										}
										strFields = Strings.Left(strFields, strFields.Length - 2);
										rsTable.Execute("Delete from tblW2OriginalDeductions", "Payroll");
										rsTable.Execute("INSERT INTO tblW2OriginalDeductions (" + strFields + ") SELECT " + strFields + " FROM tblW2Deductions", "Payroll");
										rsTable.OpenRecordset("SELECT * FROM tblW2Matches");
										strFields = "";
										for (x = 0; x <= (rsTable.FieldsCount - 1); x++)
										{
											if (rsTable.Get_FieldsIndexName(x) == "ID")
											{
											}
											else
											{
												strFields += rsTable.Get_FieldsIndexName(x) + ", ";
											}
										}
										strFields = Strings.Left(strFields, strFields.Length - 2);
										rsTable.Execute("Delete from tblW2OriginalMatches", "Payroll");
										rsTable.Execute("INSERT INTO tblW2OriginalMatches (" + strFields + ") SELECT " + strFields + " FROM tblW2Matches", "Payroll");
									}
									clsDRWrapper rsExecute = new clsDRWrapper();
									clsDRWrapper rsEmployee = new clsDRWrapper();
									rsData.OpenRecordset("Select Distinct EmployeeNumber from tblW2EditTable", "TWPY0000.vb1");
									rsEmployee.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
									while (!rsData.EndOfFile())
									{
										if (rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
										{
											rsExecute.Execute("Update tblW2Original Set FirstName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("FirstName"))) + "', MiddleName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("MiddleName"))) + "', LastName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields_String("LastName"))))) + "' ,desig = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("desig"))))) + "' Where EmployeeNumber ='" + rsData.Get_Fields("EmployeeNumber") + "'", "Payroll");
											// Call rsExecute.Execute("Update tblW2EditTable Set FirstName ='" & FixQuote(rsEmployee.Fields("FirstName")) & "',MiddleName ='" & FixQuote(rsEmployee.Fields("MiddleName")) & "',LastName ='" & FixQuote(Trim(rsEmployee.Fields("LastName") & " " & rsEmployee.Fields("desig"))) & "' Where EmployeeNumber ='" & rsData.Fields("EmployeeNumber") & "'")
											rsExecute.Execute("Update tblW2EditTable Set FirstName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("FirstName"))) + "', MiddleName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(rsEmployee.Get_Fields_String("MiddleName"))) + "', LastName ='" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields_String("LastName"))))) + "' ,desig = '" + FCConvert.ToString(modGlobalRoutines.FixQuote(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields("desig"))))) + "' Where EmployeeNumber ='" + rsData.Get_Fields("EmployeeNumber") + "'", "Payroll");
										}
										rsData.MoveNext();
									}
									rsData = null;
									rsEmployee = null;
									rsExecute = null;
									modGlobalRoutines.UpdateW2StatusData("Save");
									//App.MainForm.ShowLoader = false;
									MessageBox.Show("Save of W-2 Information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								this.EndWait();
								Close();
							}
							else if (modGlobalVariables.Statics.gstrW2Status == "UPDATE")
							{
							}
							else if (modGlobalVariables.Statics.gstrW2Status == "EDITREPORT")
							{
								frmReportViewer.InstancePtr.Init(rptW2EditReport.InstancePtr, showModal: this.Modal);
								//rptW2EditReport.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								this.EndWait();
								Close();
							}
						}
						this.EndWait();
					});
				}

			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmW2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper rsTable = new clsDRWrapper();
			int intCounter = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsTotalWage = new clsDRWrapper();
			clsDRWrapper rsDeductions = new clsDRWrapper();
			clsDRWrapper rsDeductionsMatch = new clsDRWrapper();
			clsDRWrapper rsMatch = new clsDRWrapper();
			clsDRWrapper rsMatchDeductions = new clsDRWrapper();
			clsDRWrapper rsDeds = new clsDRWrapper();
			string strEmployee = "";
			string strWhere = "";
			string strWhere2 = "";
			clsDRWrapper rsDeductionSetup = new clsDRWrapper();
			clsDRWrapper rsW2Box12And14 = new clsDRWrapper();
			string strMatchBox1List = "";
			double dblMatchBox1 = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strFields = "";
			string strReturn;
			double dblTWage = 0;
			double dblTemp = 0;
			string strTemp = "";
			boolLoad = false;
			vsData.Cols = 13;
			vsData.Rows = 2;
			vsData.OutlineCol = 0;
			vsData.FixedRows = 1;
			vsData.FixedCols = 1;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsData.Redraw = false;
			vsData.SetFontSize(8);
			vsData.TextMatrix(0, 1, "Emp #");
			vsData.TextMatrix(0, 2, "Employee Name");
			vsData.TextMatrix(0, 3, "Address");
			vsData.TextMatrix(0, 4, "City/State/Zip");
			vsData.TextMatrix(0, 5, "SSN");
			vsData.TextMatrix(0, 6, "Retirement");
			vsData.TextMatrix(0, 7, "Deferred");
			vsData.ColDataType(12, FCGrid.DataTypeSettings.flexDTBoolean);
			// mqde
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.Visible = false;
			//Frame1.Visible = false;
			boolOriginal = false;
			this.UpdateWait(progressBarValue: 0);
			//pbrStatus.Value = 0;
			pboolNewSave = false;
			string strList;
			// strList = "Original data from prior save|1;Live data from pay totals|2"
			strList = "Live data from pay totals|2";
			// now load years from archive table
			rsData.OpenRecordset("select [year] from tblw2archive group by [year] having convert(int, isnull([year], 0)) > 0 order by [year] desc", "twpy0000.vb1");
			while (!rsData.EndOfFile())
			{
				strList += ";" + rsData.Get_Fields("year") + " archive|" + rsData.Get_Fields("year");
				rsData.MoveNext();
			}
			strReturn = frmListChoice.InstancePtr.Init(strList, "Load Data From", "Load Data From", false, "", true, true);
			//FC:FINAL:DSE form controls not displayed if opened second time
			frmListChoice.InstancePtr.Dispose();
			if (Conversion.Val(strReturn) < 0)
			{
				boolOriginal = true;
				boolQuit = true;
				Close();
				return;
			}
			if (Conversion.Val(strReturn) == 1)
			{
			// If rsData.UpdateDatabaseTable("tblW2Original", "twpy0000.vb1") Then
			RetrySave:
				;
				// If MsgBox("Do you wish to retrieve ORIGINAL data from prior save?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
				rsData.OpenRecordset("SELECT * FROM tblW2Original");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2EditTable", "Payroll");
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2EditTable (" + strFields + ") SELECT " + strFields + " FROM tblW2Original", "Payroll");
				rsData.OpenRecordset("SELECT * FROM tblW2OriginalDeductions");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2Deductions", "Payroll");
				rsData.Execute("Delete from tblW2OriginalDeductions where DeductionNumber >=9000", "Payroll");
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2Deductions (" + strFields + ") SELECT " + strFields + " FROM tblW2OriginalDeductions", "Payroll");
				rsData.OpenRecordset("SELECT * FROM tblW2OriginalMatches");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2Matches", "Payroll");
				rsData.Execute("Delete from tblW2OriginalMatches where DeductionNumber >=9000", "Payroll");
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2Matches (" + strFields + ") SELECT " + strFields + " FROM tblW2OriginalMatches", "Payroll");
				// CLEAR OUT THE MERGE FLAG
				rsData.Execute("Update tblW2StatusTable Set W2MergeDataDone = NULL", "Payroll");
				pbrStatus.Visible = false;
				lblStatus.Visible = false;
				modColorScheme.ColorGrid(vsData, 1);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				boolOriginal = true;
				return;
				// Else
				// If MsgBox("Do you wish to retrieve LIVE data from Pay Totals?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
				// GoTo GetLiveData
				// Else
				// boolQuit = True
				// Unload Me
				// Exit Sub
				// End If
				// 
				// End If
				// Else
				// With rsTable
				// CREATE A NEW TABLE IF IT DOESN'T EXIST
				// If .CreateNewDatabaseTable("tblW2Original", "TWPY0000.vb1") Then
				// table does not exist
				// create and insert into the deduction table
				// Call rsTable.Execute("SELECT tblW2EditTable.* INTO tblW2Original From tblW2EditTable")
				// Call rsTable.Execute("SELECT tblW2Deductions.* INTO tblW2OriginalDeductions From tblW2Deductions")
				// Call rsTable.Execute("SELECT tblW2Matches.* INTO tblW2OriginalMatches From tblW2Matches")
				// End If
				// End With
				// 
				// GoTo RetrySave
				// End If
				// End If
			}
			else if (Conversion.Val(strReturn) == 2)
			{
			GetLiveData:
				;
				pboolNewSave = true;
				strYear = Interaction.InputBox("Enter tax year to save data for.", "Save W-2 Information", FCConvert.ToString(DateTime.Today.Year));
				if (fecherFoundation.Strings.Trim(strYear) == string.Empty)
				{
					MessageBox.Show("W-2 Information was not saved.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolQuit = true;
					return;
				}
				else
				{
					if (!Information.IsDate("01/01/" + strYear))
					{
						MessageBox.Show("Invalid Tax year to report on.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						boolQuit = true;
						return;
					}
					rsData.OpenRecordset("Select * from tblW2EditTable WHERE [YEAR] = " + strYear, "TWPY0000.vb1");
					if (!rsData.EndOfFile())
					{
						if (MessageBox.Show("W-2 Information already exists in the Edit Table for year " + strYear + ". Are you sure you want to overwrite this data?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						{
						}
						else
						{
							MessageBox.Show("This process has been stopped.", "Process Stopped", MessageBoxButtons.OK, MessageBoxIcon.Information);
							boolQuit = true;
							return;
						}
					}
				}
				if (pboolNewSave)
				{
					rsTable.Execute("Delete from tblW2Original", "Payroll");
					rsTable.Execute("Delete from tblW2OriginalDeductions", "Payroll");
					rsTable.Execute("Delete from tblW2OriginalMatches", "Payroll");
					rsTable.Execute("delete from tblw2matches", "Payroll");
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				this.UpdateWait("Getting W-2 records from the database");
				//lblStatus.Text = "Getting W-2 records from the database";
				//FCUtils.ApplicationUpdate(lblStatus);
				// CLEAR THE SAVE OF THE CONTACT SCREEN SO THAT WE ARE SURE THAT THE CODES ARE
				// BROUGHT INTO THE DEDUCITON AND MATCH TABLES.
				rsData.Execute("Update tblDefaultInformation SET W2MasterSaveDone = 0", "TWPY0000.vb1");
				strMatchBox1List = "";
				rsData.OpenRecordset("select * from tbldefaultdeductions where type = 'M'", "twpy0000.vb1");
				while (!rsData.EndOfFile())
				{
					strMatchBox1List += FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Int32("DeductionID"))) + ",";
					rsData.MoveNext();
				}
				if (strMatchBox1List != "")
				{
					strMatchBox1List = Strings.Mid(strMatchBox1List, 1, strMatchBox1List.Length - 1);
				}
				rsData.OpenRecordset("SELECT DISTINCT tblEmployeeMaster.EmployeeNumber, W2StatutoryEmployee, FirstName, LastName, MiddleName, Desig, Address1, Address2, City, tblEmployeeMaster.State as StateNumber, Zip, SSN, W2Pen, W2DefIncome, States.State,tblemployeemaster.deptdiv,tblemployeemaster.seqnumber,mqge FROM (tblEmployeeMaster INNER JOIN tblCheckDetail ON tblEmployeeMaster.EmployeeNumber = tblCheckDetail.EmployeeNumber) LEFT JOIN States ON convert(int, tblEmployeeMaster.State) = States.ID WHERE (((tblCheckDetail.EmployeeNumber)<>'') AND ((Year([PayDate]))=" + strYear + ")) ORDER BY tblEmployeeMaster.EmployeeNumber", "TWPY0000.vb1", 4);
				rsW2Box12And14.OpenRecordset("Select * from tblW2Box12And14 Order by DeductionNumber");
				rsDeductions.OpenRecordset("select ID,description from tbldeductionsetup where description <> '' order by ID", "twpy0000.vb1");
				rsDeds.OpenRecordset("select distinct deddeductionnumber as deductioncode, employeenumber from tblcheckdetail where not checkvoid = 1 and deddeductionnumber > 0 and paydate between '1/1/" + strYear + "' and '12/31/" + strYear + "' order by employeenumber", "twpy0000.vb1");
				rsDeductionsMatch.OpenRecordset("SELECT distinct matchdeductionnumber as DeductionCode, EmployeeNumber from tblcheckdetail where not checkvoid = 1 and paydate between '1/1/" + strYear + "' and '12/31/" + strYear + "' order by employeenumber", "TWPY0000.vb1", 4);
				// 
				//FC:FINAL:DSE:#i2204 Prevent grid updates on client side during long-time loops
				vsData.Redraw = false;
                //FC:FINAL:AM:#4149 - don't show the loader
				//App.MainForm.ShowLoader = true;
				if (!rsData.EndOfFile())
				{
					rsData.MoveLast();
					rsData.MoveFirst();
					pbrStatus.Maximum = rsData.RecordCount() + 1;
					this.UpdateWait(progressBarMaximum: rsData.RecordCount() + 1);
					//lblStatus.Text = "Checking Employee Information Records";
					this.UpdateWait("Checking Employee Information Records");
					//pbrStatus.Value = 1;
					this.UpdateWait(progressBarValue:1);
					//FCUtils.ApplicationUpdate(pbrStatus);
					//FCUtils.ApplicationUpdate(lblStatus);
					intCounter = 0;
					while (!rsData.EndOfFile())
					{
						//App.DoEvents();
						intCounter += 1;
						// For intCounter = 1 To rsData.RecordCount
						// If rsData.Fields("EmployeeNumber") = "009" Then MsgBox "A"
						//lblStatus.Text = "Checking record  " + FCConvert.ToString(intCounter) + " of " + FCConvert.ToString(rsData.RecordCount() + 1);
						this.UpdateWait("Checking record  " + FCConvert.ToString(intCounter) + " of " + FCConvert.ToString(rsData.RecordCount() + 1));
						//FCUtils.ApplicationUpdate(lblStatus);
                        var employeeNumber = FCConvert.ToString(rsData.Get_Fields("employeenumber"));
                        var endTaxYear = FCConvert.ToDateTime("12/31/" + strYear);
                        dblTWage = modCoreysSweeterCode.GetCYTDPayTotal(-1, employeeNumber, endTaxYear);

                        if (dblTWage <= 0)
							goto NextEmployee;
						
						vsData.TextMatrix(vsData.Rows - 1, 1, employeeNumber);
						vsData.TextMatrix(vsData.Rows - 1, 2, rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields_String("Desig"));
						vsData.TextMatrix(vsData.Rows - 1, 3, rsData.Get_Fields_String("Address1") + "   " + rsData.Get_Fields_String("Address2"));
						vsData.TextMatrix(vsData.Rows - 1, 4, rsData.Get_Fields_String("City") + ";, ;" + rsData.Get_Fields("State") + ";" + rsData.Get_Fields("Zip"));
						vsData.TextMatrix(vsData.Rows - 1, 5, Strings.Format(rsData.Get_Fields_String("SSN"), "000-00-0000"));
						vsData.TextMatrix(vsData.Rows - 1, 6, FCConvert.ToString(rsData.Get_Fields_Boolean("W2Pen")));
						vsData.TextMatrix(vsData.Rows - 1, 7, FCConvert.ToString(rsData.Get_Fields_Boolean("W2DefIncome")));
						vsData.TextMatrix(vsData.Rows - 1, 8, FCConvert.ToString(rsData.Get_Fields_Boolean("W2DefIncome")));
						vsData.TextMatrix(vsData.Rows - 1, 9, FCConvert.ToString(rsData.Get_Fields_Boolean("W2StatutoryEmployee")));
						vsData.TextMatrix(vsData.Rows - 1, 10, FCConvert.ToString(rsData.Get_Fields("seqnumber")));
						vsData.TextMatrix(vsData.Rows - 1, 11, FCConvert.ToString(rsData.Get_Fields("deptdiv")));
						vsData.TextMatrix(vsData.Rows - 1, 12, FCConvert.ToString(rsData.Get_Fields_Boolean("mqge")));
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 5, vsData.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 6, vsData.Rows - 1, 9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 0);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE WAGES CAPTIONS
						vsData.TextMatrix(vsData.Rows - 1, 1, "Wages");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 1);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						vsData.TextMatrix(vsData.Rows - 1, 2, "Total Wage    Fed (Box 1) Wage");
						vsData.TextMatrix(vsData.Rows - 1, 3, "FICA Wage");
						vsData.TextMatrix(vsData.Rows - 1, 4, "Med Wage");
						vsData.TextMatrix(vsData.Rows - 1, 5, "State Wage");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 0, vsData.Rows - 1, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 2);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE WAGE DATA
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						// If Not rsTotalWage.NoMatch Then
						// vsData.TextMatrix(vsData.Rows - 1, 2) = Format(rsTotalWage.Fields("SumOfCalendarYTDTotal"), "#,##0.00")
						vsData.TextMatrix(vsData.Rows - 1, 2, Strings.Format(dblTWage, "#,##0.00"));
						// End If
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, employeeNumber, endTaxYear);
						dblMatchBox1 = modCoreysSweeterCode.GetCYTDBox1Matches(ref strMatchBox1List, employeeNumber, endTaxYear);
						dblTemp += dblMatchBox1;
                        const string currencyFormat = "0.00";
                        strTemp = Strings.Format(dblTemp, currencyFormat);
						// vsData.TextMatrix(vsData.Rows - 1, 2) = String(16 - Len(Trim(vsData.TextMatrix(vsData.Rows - 1, 2))), " ") & Trim(vsData.TextMatrix(vsData.Rows - 1, 2)) & String(18 - Len(strTemp), " ") & strTemp
						vsData.TextMatrix(vsData.Rows - 1, 2, fecherFoundation.Strings.Trim(vsData.TextMatrix(vsData.Rows - 1, 2)) + Strings.StrDup(21 - strTemp.Length, " ") + strTemp);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 3, strTemp);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, ".00");
						vsData.TextMatrix(vsData.Rows - 1, 4, strTemp);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, employeeNumber, endTaxYear);
						dblTemp += dblMatchBox1;
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 5, strTemp);
						// End If
						vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(2));
						// THIS IS A WAGE RECORD
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 6, vsData.Rows - 1, 7, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 3);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE TAX CAPTIONS
						vsData.TextMatrix(vsData.Rows - 1, 1, "Taxes");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 1);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						vsData.TextMatrix(vsData.Rows - 1, 2, "Fed Tax");
						vsData.TextMatrix(vsData.Rows - 1, 3, "FICA Tax");
						vsData.TextMatrix(vsData.Rows - 1, 4, "Med Tax");
						vsData.TextMatrix(vsData.Rows - 1, 5, "State Tax");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 0, vsData.Rows - 1, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 2);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE TAX DATA
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 2, strTemp);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 3, strTemp);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 4, strTemp);
						dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, employeeNumber, endTaxYear);
						strTemp = Strings.Format(dblTemp, currencyFormat);
						vsData.TextMatrix(vsData.Rows - 1, 5, strTemp);
						
						vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(3));
						// THIS IS A TAX RECORD
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 6, vsData.Rows - 1, 7, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 3);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ****************************************************************
						// ADD THE BOX 12 CAPTIONS
						vsData.TextMatrix(vsData.Rows - 1, 1, "Deductions");
						vsData.RowOutlineLevel(vsData.Rows - 1, 1);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						vsData.TextMatrix(vsData.Rows - 1, 2, "BOX 12");
						vsData.TextMatrix(vsData.Rows - 1, 3, "Code");
						vsData.TextMatrix(vsData.Rows - 1, 4, "Amount");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 2);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE BOX 12 DATA
						strEmployee = employeeNumber;
						if (strEmployee == "D280")
						{
							strEmployee = strEmployee;
						}
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						// If Not (rsDeductions.EndOfFile And rsDeductions.BeginningOfFile) Then
						rsDeductions.MoveFirst();
						// End If
						while (!rsDeductions.EndOfFile())
						{
							//App.DoEvents();
							// rsDeds.MoveLast
							// rsDeds.MoveFirst
							// Do While Not rsDeds.EndOfFile
							dblTemp = 0;
							// If rsDeds.FindFirst("employeenumber = '" & strEmployee & "' and deductionnumber = " & Val(rsDeductions.Fields("ID"))) Then
                            var deductionCode = rsDeds.Get_Fields("deductioncode");

                            if (rsDeds.FindFirst("employeenumber = '" + strEmployee + "' and deductioncode = " + FCConvert.ToString(Conversion.Val(rsDeductions.Get_Fields("ID")))))
							{
								// If rsDeds.FindFirstRecord2("employeenumber, deductionnumber", strEmployee & "," & rsDeductions.Fields("ID"), ",") Then
								dblTemp = modCoreysSweeterCode.GetCYTDDeductionTotal(deductionCode, strEmployee, endTaxYear);
							}
							if (dblTemp > 0)
							{
								rsW2Box12And14.FindFirstRecord("DeductionNumber", deductionCode);
								if (!rsW2Box12And14.NoMatch)
								{
									vsData.TextMatrix(vsData.Rows - 1, 3, FCConvert.ToString(rsW2Box12And14.Get_Fields("Code")));
								}
								// If rsDeductions.FindFirstRecord("ID", rsDeds.Fields("deductioncode")) Then
								vsData.TextMatrix(vsData.Rows - 1, 2, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))));
								// End If
								vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(dblTemp, currencyFormat));
								vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(deductionCode));
								vsData.RowOutlineLevel(vsData.Rows - 1, 3);
								vsData.IsSubtotal(vsData.Rows - 1, true);
								vsData.Rows += 1;
							}
							// rsDeds.MoveNext
							rsDeductions.MoveNext();
						}
					ExitDeds:
						;
						// ****************************************************************
						// ****************************************************************
						// ADD THE BOX 14 CAPTIONS
						vsData.TextMatrix(vsData.Rows - 1, 1, "Employer Match");
						vsData.RowOutlineLevel(vsData.Rows - 1, 1);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						vsData.TextMatrix(vsData.Rows - 1, 2, "BOX 14");
						vsData.TextMatrix(vsData.Rows - 1, 3, "Code");
						vsData.TextMatrix(vsData.Rows - 1, 4, "Amount");
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
						vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, vsData.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsData.RowOutlineLevel(vsData.Rows - 1, 2);
						vsData.IsSubtotal(vsData.Rows - 1, true);
						vsData.Rows += 1;
						// ADD THE BOX 14 ITEMS
						strEmployee = employeeNumber;
						vsData.TextMatrix(vsData.Rows - 1, 1, "");
						// If Not (rsDeductions.EndOfFile And rsDeductions.BeginningOfFile) Then
						rsDeductions.MoveFirst();
						// End If
						// 
						while (!rsDeductions.EndOfFile())
						{
							//App.DoEvents();
							// rsDeductionsMatch.MoveLast
							// rsDeductionsMatch.MoveFirst
							// Do While Not rsDeductionsMatch.EndOfFile
							dblTemp = 0;
							if (rsDeductionsMatch.FindFirstRecord2("employeenumber, deductioncode", strEmployee + "," + rsDeductions.Get_Fields("ID"), ","))
							{
								dblTemp = modCoreysSweeterCode.GetCYTDMatchTotal(rsDeductionsMatch.Get_Fields("deductioncode"), strEmployee, endTaxYear);
							}
							if (dblTemp > 0)
							{
								// If rsDeductions.FindFirstRecord("ID", rsDeductionsMatch.Fields("deductioncode")) Then
								vsData.TextMatrix(vsData.Rows - 1, 2, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("Description"))));
								// End If
								rsW2Box12And14.FindFirstRecord("DeductionNumber", rsDeductionsMatch.Get_Fields("deductioncode"));
								if (!rsW2Box12And14.NoMatch)
								{
									vsData.TextMatrix(vsData.Rows - 1, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Box12And14.Get_Fields("Code"))));
								}
								vsData.TextMatrix(vsData.Rows - 1, 4, Strings.Format(dblTemp, currencyFormat));
								vsData.TextMatrix(vsData.Rows - 1, 0, FCConvert.ToString(rsDeductionsMatch.Get_Fields("deductioncode")));
								vsData.RowOutlineLevel(vsData.Rows - 1, 3);
								vsData.IsSubtotal(vsData.Rows - 1, true);
								vsData.Rows += 1;
							}
							// rsDeductionsMatch.MoveNext
							rsDeductions.MoveNext();
						}
					ExitMatch:
						;
					// ****************************************************************
					NextEmployee:
						;
						this.UpdateWait(progressBarValue: pbrStatus.Value + 1);
						pbrStatus.Value = pbrStatus.Value + 1;
						//FCUtils.ApplicationUpdate(pbrStatus);
						rsData.MoveNext();
					}
				}
				//FC:FINAL:DSE:#i2204 Reenable grid updates on client side after long-time loop
				//App.MainForm.ShowLoader = false;
				vsData.Redraw = true;

				// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
				vsData.Outline(-1);
				vsData.Outline(1);
			SaveComplete:
				;
				// CLEAR OUT THE MERGE FLAG
				rsData.Execute("Update tblW2StatusTable Set W2MergeDataDone = NULL", "Payroll");
				pbrStatus.Visible = false;
				lblStatus.Visible = false;
				modColorScheme.ColorGrid(vsData, 1);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				boolSave = true;
			}
			else
			{
				// archive year
				rsData.OpenRecordset("SELECT * FROM tblW2Original");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					//App.DoEvents();
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2EditTable", "Payroll");
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2EditTable (" + strFields + ") SELECT " + strFields + " FROM tblW2archive where year = " + FCConvert.ToString(Conversion.Val(strReturn)), "Payroll");
				rsData.OpenRecordset("SELECT * FROM tblW2ARCHIVEDeductions");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					//App.DoEvents();
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2Deductions", "Payroll");
				// Call rsData.Execute("Delete from tblW2OriginalDeductions where DeductionNumber >=9000")
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2Deductions (" + strFields + ") SELECT " + strFields + " FROM tblW2ARCHIVEDeductions WHERE [YEAR] = " + FCConvert.ToString(Conversion.Val(strReturn)), "Payroll");
				rsData.OpenRecordset("SELECT * FROM tblW2archiveMatches");
				strFields = "";
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					//App.DoEvents();
					if (rsData.Get_FieldsIndexName(x) == "ID")
					{
					}
					else
					{
						strFields += rsData.Get_FieldsIndexName(x) + ", ";
					}
				}
				strFields = Strings.Left(strFields, strFields.Length - 2);
				rsData.Execute("Delete from tblW2Matches", "Payroll");
				// Call rsData.Execute("Delete from tblW2OriginalMatches where DeductionNumber >=9000")
				//App.DoEvents();
				rsData.Execute("INSERT INTO tblW2Matches (" + strFields + ") SELECT " + strFields + " FROM tblW2archiveMatches where [year] = " + FCConvert.ToString(Conversion.Val(strReturn)), "Payroll");
				// CLEAR OUT THE MERGE FLAG
				rsData.Execute("Update tblW2StatusTable Set W2MergeDataDone = NULL", "Payroll");
				pbrStatus.Visible = false;
				lblStatus.Visible = false;
				modColorScheme.ColorGrid(vsData, 1);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				boolOriginal = true;
				return;
			}
			NoSave:
				;
		}

		private bool CheckSSN()
		{
			bool CheckSSN = false;
			string strReturn = "";
			string strW2 = "";
			string strMaster = "";
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsEmployee = new clsDRWrapper();
			rsEmployee.OpenRecordset("Select * from tblEmployeeMaster");
			rsData.OpenRecordset("Select * from tblW2EditTable");
			while (!rsData.EndOfFile())
			{
				strW2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("SSN")));
				if (strW2.Length != 11)
				{
					// strReturn = InputBox("SSN for " & rsData.Fields("EmployeeName") & " does not seem to be in the format of 11 digits ###-##-####." & Chr(13) & "Enter correct SSN and click OK.", "InCorrect SSN")
					// If strReturn = vbNullString Then
					// MsgBox "Invalid SSN", vbCritical + vbOKOnly, "TRIO Software"
					// GoTo TRYAGAIN
					// Else
					// rsData.Edit
					// rsData.Fields("SSN") = strReturn
					// GoTo CheckLength
					// End If
					MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				else if (Strings.Mid(strW2, 4, 1) != "-")
				{
					MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				else if (Strings.Mid(strW2, 7, 1) != "-")
				{
					MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				else if (!Information.IsNumeric(strW2.Replace("-", "")))
				{
					MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				// Now check the Master SSN
				if (rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
				{
					strMaster = fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployee.Get_Fields_String("SSN")));
					if (strMaster.Length != 9)
					{
						MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CheckSSN;
					}
					else if (!Information.IsNumeric(strMaster.Replace("-", "")))
					{
						MessageBox.Show("Employee's SSN does not appear in a correct format for: " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CheckSSN;
					}
				}
				else
				{
					MessageBox.Show("SSN for " + rsData.Get_Fields_String("FullName") + " cannot be found.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				if (fecherFoundation.Strings.Trim(strW2.Replace("-", "")) != fecherFoundation.Strings.Trim(strMaster.Replace("-", "")))
				{
					MessageBox.Show("Employee's SSN and W2 SSN do not appear the same for employee " + rsData.Get_Fields_String("EmployeeName") + "\r" + "\r" + "Master: " + Strings.Format(strMaster, "000-00-0000") + "\r" + "W2:       " + strW2 + "\r" + "\r" + "Change the SSN on the Employee Add / Update screen and resave W2 information.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CheckSSN;
				}
				else
				{
				}
				rsData.MoveNext();
			}
			CheckSSN = true;
			return CheckSSN;
		}

		private void frmW2_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, vsData.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.07));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.25));
			vsData.ColWidth(3, FCConvert.ToInt32(vsData.WidthOriginal * 0.22));
			vsData.ColWidth(4, FCConvert.ToInt32(vsData.WidthOriginal * 0.16));
			vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.16));
			vsData.ColWidth(6, FCConvert.ToInt32(vsData.WidthOriginal * 0.16));
			vsData.ColWidth(7, FCConvert.ToInt32(vsData.WidthOriginal * 0.16));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptW2EditReport.InstancePtr);
			//rptW2EditReport.InstancePtr.Show(App.MainForm);
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDeductions = new clsDRWrapper();
			clsDRWrapper rsMatches = new clsDRWrapper();
			string strEmployeeNumber = "";
			int intRowOutLineLevel;
			double dblTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsData.Execute("Delete from tblW2EditTable", "Payroll");
				rsDeductions.Execute("Delete from tblW2Deductions", "Payroll");
				rsDeductions.OpenRecordset("Select * from tblW2Deductions", "TWPY0000.vb1");
				rsMatches.Execute("Delete from tblW2Matches ", "Payroll");
				rsMatches.OpenRecordset("Select * from tblW2Matches", "TWPY0000.vb1");
				rsData.Execute("Delete from tblW2OriginalDeductions", "Payroll");
				rsData.Execute("Delete from tblW2OriginalMatches", "Payroll");
				rsData.OpenRecordset("Select * from tblW2EditTable", "TWPY0000.vb1");
				// vbPorter upgrade warning: strTemp As object	OnWrite(string())
				string[] strTemp = null;
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				for (intRow = 1; intRow <= (vsData.Rows - 2); intRow++)
				{
					if (vsData.RowOutlineLevel(intRow) == 0)
					{
						if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, 1)) == string.Empty)
						{
						}
						else
						{
							rsData.AddNew();
							// If vsData.TextMatrix(intRow, 1) = "009" Then MsgBox "A"
							rsData.Set_Fields("deptdiv", vsData.TextMatrix(intRow, 11));
							rsData.Set_Fields("seqnumber", vsData.TextMatrix(intRow, 10));
							rsData.Set_Fields("mqge", FCConvert.CBool(vsData.TextMatrix(intRow, 12)));
							rsData.Set_Fields("EmployeeNumber", vsData.TextMatrix(intRow, 1));
							strEmployeeNumber = vsData.TextMatrix(intRow, 1);
							rsData.Set_Fields("EmployeeName", vsData.TextMatrix(intRow, 2));
							rsData.Set_Fields("Address", vsData.TextMatrix(intRow, 3));
							strTemp = Strings.Split(vsData.TextMatrix(intRow, 4), ";", -1, CompareConstants.vbBinaryCompare);
							for (intCounter = 0; intCounter <= (Information.UBound(strTemp, 1) - 1); intCounter++)
							{
								if (intCounter == 0)
									rsData.Set_Fields("City", strTemp[0]);
								if (intCounter == 1)
									rsData.Set_Fields("State", strTemp[2]);
								if (intCounter == 2)
									rsData.Set_Fields("Zip", strTemp[3]);
							}
							rsData.Set_Fields("CITYSTATEZIP", rsData.Get_Fields("CITY") + ", " + rsData.Get_Fields("state") + " " + rsData.Get_Fields_String("zip"));
							// rsData.Set_Fields("CityStateZip", Replace(vsData.TextMatrix(intRow, 4), ");", " ")
							rsData.Set_Fields("SSN", vsData.TextMatrix(intRow, 5));
							rsData.Set_Fields("W2Retirement", vsData.TextMatrix(intRow, 6));
							rsData.Set_Fields("W2Deferred", vsData.TextMatrix(intRow, 7));
							rsData.Set_Fields("W2StatutoryEmployee", vsData.TextMatrix(intRow, 9));
							intRow += 3;
							rsData.Set_Fields("TotalGross", FCConvert.ToString(CheckForBlank(fecherFoundation.Strings.Trim(Strings.Left(vsData.TextMatrix(intRow, 2), 16)))).Replace(",", ""));
							rsData.Set_Fields("FederalWage", FCConvert.ToString(CheckForBlank(fecherFoundation.Strings.Trim(Strings.Right(vsData.TextMatrix(intRow, 2), 16)))).Replace(",", ""));
							rsData.Set_Fields("FICAWage", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 3))).Replace(",", ""));
							rsData.Set_Fields("MedicareWage", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
							rsData.Set_Fields("StateWage", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 5))).Replace(",", ""));
							intRow += 3;
							rsData.Set_Fields("FederalTax", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 2))).Replace(",", ""));
							rsData.Set_Fields("FICATax", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 3))).Replace(",", ""));
							rsData.Set_Fields("MedicareTax", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 4))).Replace(",", ""));
							rsData.Set_Fields("StateTax", FCConvert.ToString(CheckForBlank(vsData.TextMatrix(intRow, 5))).Replace(",", ""));
							// rsData.Fields("EICAmount") = CheckForBlank(vsData.TextMatrix(intRow, 7))
							rsData.Set_Fields("Year", strYear);
							rsData.Set_Fields("deleterecord", false);
							rsData.Update();
							intRow += 3;
							// SAVE THE DEDUCTION RECORDS
							if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, 2)) == string.Empty)
							{
							}
							else
							{
								while (vsData.RowOutlineLevel(intRow) == 3)
								{
									dblTemp = FCConvert.ToDouble(CheckForBlank(vsData.TextMatrix(intRow, 4)));
									if (dblTemp != 0)
									{
										rsDeductions.AddNew();
										rsDeductions.Set_Fields("EmployeeNumber", strEmployeeNumber);
										rsDeductions.Set_Fields("Box12", true);
										rsDeductions.Set_Fields("DeductionNumber", vsData.TextMatrix(intRow, 0));
										rsDeductions.Set_Fields("Code", vsData.TextMatrix(intRow, 3));
										rsDeductions.Set_Fields("CYTDAmount", CheckForBlank(vsData.TextMatrix(intRow, 4)));
										if (rsDeductions.Get_Fields_Double("CYTDAmount") == 0)
										{
										}
										else
										{
											rsDeductions.Update();
										}
									}
									if (intRow + 1 < vsData.Rows - 1)
									{
										intRow += 1;
									}
									else
									{
										goto ExitDeductions;
									}
								}
							}
							ExitDeductions:
							;
							intRow += 2;
							// SAVE THE MATCH RECORDS
							if (fecherFoundation.Strings.Trim(vsData.TextMatrix(intRow, 2)) == string.Empty)
							{
							}
							else
							{
								while (vsData.RowOutlineLevel(intRow) == 3)
								{
									rsMatches.AddNew();
									rsMatches.Set_Fields("EmployeeNumber", strEmployeeNumber);
									rsMatches.Set_Fields("Box14", true);
									rsMatches.Set_Fields("DeductionNumber", vsData.TextMatrix(intRow, 0));
									rsMatches.Set_Fields("Code", vsData.TextMatrix(intRow, 3));
									rsMatches.Set_Fields("CYTDAmount", Strings.Format(CheckForBlank(vsData.TextMatrix(intRow, 4)), ".00"));
									if (rsMatches.Get_Fields_Double("CYTDAmount") == 0)
									{
									}
									else
									{
										rsMatches.Update();
									}
									if (intRow + 1 < vsData.Rows - 1)
									{
										intRow += 1;
									}
									else
									{
										goto ExitMatch;
									}
								}
							}
							ExitMatch:
							;
							intRow -= 1;
						}
					}
				}
				// MsgBox "Process of W2 information completed successfully.", vbInformation + vbOKOnly, "TRIO Software"
				boolSave = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void mnuProcess_Click()
		{
			mnuProcess_Click(mnuProcess, new System.EventArgs());
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public string CheckForBlank(string strData)
		{
			if (fecherFoundation.Strings.Trim(strData) == string.Empty)
			{
				return "0.00";
			}
			else
			{
				return Strings.Format(strData, "0.00");
			}
		}
	}
}
