//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmDefaultInformation.
	/// </summary>
	partial class frmDefaultInformation
	{
		public fecherFoundation.FCComboBox cmbW2Type;
		public fecherFoundation.FCComboBox cmbCheckImage;
		public FCGrid GridDeleteICMA;
		public fecherFoundation.FCFrame framPayrollDefaults;
		public fecherFoundation.FCComboBox cmbTrackLastCheckNumber;
		public fecherFoundation.FCCheckBox chkPayPeriod;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtRA1;
		public fecherFoundation.FCTextBox txtRA2;
		public fecherFoundation.FCTextBox txtRA3;
		public fecherFoundation.FCTextBox txtRA4;
		public fecherFoundation.FCTextBox txtCode2;
		public fecherFoundation.FCTextBox txtCode1;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCListBox lstMatchesBox1;
		public fecherFoundation.FCCheckBox chkFillAccount;
		public fecherFoundation.FCCheckBox chkContractEncumbrances;
		public fecherFoundation.FCCheckBox chkPrintSeparate;
		public fecherFoundation.FCCheckBox chkSignature;
		public fecherFoundation.FCCheckBox chkNegBalances;
		public fecherFoundation.FCButton cmdACHPrenote;
		public fecherFoundation.FCCheckBox chkNoMSRS;
		public fecherFoundation.FCComboBox cboEmployerIDPrefix;
		public fecherFoundation.FCCheckBox chkACHClearingHouse;
		public fecherFoundation.FCCheckBox chkWeeklyPR;
		public fecherFoundation.FCCheckBox chkSkipPages;
		public fecherFoundation.FCCheckBox chkMMAUnemployment;
		public fecherFoundation.FCCheckBox chkBalanced;
		public fecherFoundation.FCCheckBox chkElectronicC1;
		public fecherFoundation.FCComboBox cboW2CheckType;
		public fecherFoundation.FCComboBox cboTypeChecks;
		public fecherFoundation.FCComboBox cboDataEntry;
		public fecherFoundation.FCComboBox cboReportSequence;
		public fecherFoundation.FCComboBox cboPrintSequence;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCCheckBox chkMultiFunds;
		public fecherFoundation.FCCheckBox chkBankDD;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel lblEmployerID;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblDataEntry;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCFrame fraW2s;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCFrame framCheckOptions;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkPProcessReports;
		public fecherFoundation.FCCheckBox chkDefaultChecks;
		public fecherFoundation.FCCheckBox chkIgnorePrinterFonts;
		public fecherFoundation.FCFrame framLogo;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCPictureBox imgLogo;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtW2Horizontal;
		public fecherFoundation.FCButton cmdSampleW2;
		public fecherFoundation.FCButton cmdSampleCheck;
		public fecherFoundation.FCTextBox txtCheckLineAdjustment;
		public fecherFoundation.FCTextBox txtW2Adjustment;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCFrame framRetirement;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame Frame8;
		public FCGrid gridExtraMSRS;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCListBox lstPayback;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCListBox lstMSRS;
		public fecherFoundation.FCFrame fraW2Types;
		public fecherFoundation.FCListBox lstRetirement;
		public fecherFoundation.FCFrame framESHP;
		public fecherFoundation.FCTextBox txtESPExportPath;
		public fecherFoundation.FCTextBox txtESPW2Path;
		public fecherFoundation.FCTextBox txtESPCheckPath;
		public fecherFoundation.FCTextBox txtESPDataPath;
		public fecherFoundation.FCTextBox txtESPPasswordConfirm;
		public fecherFoundation.FCTextBox txtESPPassword;
		public fecherFoundation.FCTextBox txtESPUser;
		public fecherFoundation.FCTextBox txtEspHost;
		public fecherFoundation.FCTextBox txtEspPort;
		public fecherFoundation.FCTextBox txtEspOrg;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEditCheck;
		public fecherFoundation.FCToolStripMenuItem mnuEditPrint;
		public fecherFoundation.FCToolStripMenuItem mnuEditRetirementDeductions;
		public fecherFoundation.FCToolStripMenuItem mnuESHPOptions;
		public fecherFoundation.FCToolStripMenuItem mnuViewW2Types;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuClearWarrantLock;
		public fecherFoundation.FCToolStripMenuItem mnuCreateACH;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDefaultInformation));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images1"))));
            Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images2"))));
            Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images3"))));
            Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images4"))));
            this.cmbW2Type = new fecherFoundation.FCComboBox();
            this.cmbCheckImage = new fecherFoundation.FCComboBox();
            this.GridDeleteICMA = new fecherFoundation.FCGrid();
            this.framPayrollDefaults = new fecherFoundation.FCFrame();
            this.cmbTrackLastCheckNumber = new fecherFoundation.FCComboBox();
            this.chkPayPeriod = new fecherFoundation.FCCheckBox();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.txtRA1 = new fecherFoundation.FCTextBox();
            this.txtRA2 = new fecherFoundation.FCTextBox();
            this.txtRA3 = new fecherFoundation.FCTextBox();
            this.txtRA4 = new fecherFoundation.FCTextBox();
            this.txtCode2 = new fecherFoundation.FCTextBox();
            this.txtCode1 = new fecherFoundation.FCTextBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.lstMatchesBox1 = new fecherFoundation.FCListBox();
            this.chkFillAccount = new fecherFoundation.FCCheckBox();
            this.chkContractEncumbrances = new fecherFoundation.FCCheckBox();
            this.chkPrintSeparate = new fecherFoundation.FCCheckBox();
            this.chkSignature = new fecherFoundation.FCCheckBox();
            this.chkNegBalances = new fecherFoundation.FCCheckBox();
            this.cmdACHPrenote = new fecherFoundation.FCButton();
            this.chkNoMSRS = new fecherFoundation.FCCheckBox();
            this.cboEmployerIDPrefix = new fecherFoundation.FCComboBox();
            this.chkACHClearingHouse = new fecherFoundation.FCCheckBox();
            this.chkWeeklyPR = new fecherFoundation.FCCheckBox();
            this.chkSkipPages = new fecherFoundation.FCCheckBox();
            this.chkMMAUnemployment = new fecherFoundation.FCCheckBox();
            this.chkBalanced = new fecherFoundation.FCCheckBox();
            this.chkElectronicC1 = new fecherFoundation.FCCheckBox();
            this.cboW2CheckType = new fecherFoundation.FCComboBox();
            this.cboTypeChecks = new fecherFoundation.FCComboBox();
            this.cboDataEntry = new fecherFoundation.FCComboBox();
            this.cboReportSequence = new fecherFoundation.FCComboBox();
            this.cboPrintSequence = new fecherFoundation.FCComboBox();
            this.chkMultiFunds = new fecherFoundation.FCCheckBox();
            this.chkBankDD = new fecherFoundation.FCCheckBox();
            this.Label25 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.lblEmployerID = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblDataEntry = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.fraW2s = new fecherFoundation.FCFrame();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.framCheckOptions = new fecherFoundation.FCFrame();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkPProcessReports = new fecherFoundation.FCCheckBox();
            this.chkDefaultChecks = new fecherFoundation.FCCheckBox();
            this.chkIgnorePrinterFonts = new fecherFoundation.FCCheckBox();
            this.framLogo = new fecherFoundation.FCFrame();
            this.fileUpload = new Wisej.Web.Upload();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.imgLogo = new fecherFoundation.FCPictureBox();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtW2Horizontal = new fecherFoundation.FCTextBox();
            this.cmdSampleW2 = new fecherFoundation.FCButton();
            this.cmdSampleCheck = new fecherFoundation.FCButton();
            this.txtCheckLineAdjustment = new fecherFoundation.FCTextBox();
            this.txtW2Adjustment = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.framRetirement = new fecherFoundation.FCFrame();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Frame8 = new fecherFoundation.FCFrame();
            this.gridExtraMSRS = new fecherFoundation.FCGrid();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.lstPayback = new fecherFoundation.FCListBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.lstMSRS = new fecherFoundation.FCListBox();
            this.fraW2Types = new fecherFoundation.FCFrame();
            this.lstRetirement = new fecherFoundation.FCListBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.framESHP = new fecherFoundation.FCFrame();
            this.txtESPExportPath = new fecherFoundation.FCTextBox();
            this.txtESPW2Path = new fecherFoundation.FCTextBox();
            this.txtESPCheckPath = new fecherFoundation.FCTextBox();
            this.txtESPDataPath = new fecherFoundation.FCTextBox();
            this.txtESPPasswordConfirm = new fecherFoundation.FCTextBox();
            this.txtESPPassword = new fecherFoundation.FCTextBox();
            this.txtESPUser = new fecherFoundation.FCTextBox();
            this.txtEspHost = new fecherFoundation.FCTextBox();
            this.txtEspPort = new fecherFoundation.FCTextBox();
            this.txtEspOrg = new fecherFoundation.FCTextBox();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuViewW2Types = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCreateACH = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditCheck = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditRetirementDeductions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuESHPOptions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClearWarrantLock = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdESHPOptions = new fecherFoundation.FCButton();
            this.cmdEditRetirementDeductions = new fecherFoundation.FCButton();
            this.cmdEditPrint = new fecherFoundation.FCButton();
            this.cmdEditCheck = new fecherFoundation.FCButton();
            this.cmdClearWarrantLock = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleteICMA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayrollDefaults)).BeginInit();
            this.framPayrollDefaults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFillAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContractEncumbrances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSeparate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNegBalances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdACHPrenote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoMSRS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkACHClearingHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSkipPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMMAUnemployment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalanced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkElectronicC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMultiFunds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankDD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraW2s)).BeginInit();
            this.fraW2s.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framCheckOptions)).BeginInit();
            this.framCheckOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPProcessReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultChecks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIgnorePrinterFonts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLogo)).BeginInit();
            this.framLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSampleW2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSampleCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRetirement)).BeginInit();
            this.framRetirement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
            this.Frame8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridExtraMSRS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraW2Types)).BeginInit();
            this.fraW2Types.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framESHP)).BeginInit();
            this.framESHP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdESHPOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditRetirementDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearWarrantLock)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 968);
            this.BottomPanel.Size = new System.Drawing.Size(1000, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framCheckOptions);
            this.ClientArea.Controls.Add(this.framRetirement);
            this.ClientArea.Controls.Add(this.framPayrollDefaults);
            this.ClientArea.Controls.Add(this.framESHP);
            this.ClientArea.Controls.Add(this.fraW2s);
            this.ClientArea.Controls.Add(this.GridDeleteICMA);
            this.ClientArea.Size = new System.Drawing.Size(1020, 606);
            this.ClientArea.Controls.SetChildIndex(this.GridDeleteICMA, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraW2s, 0);
            this.ClientArea.Controls.SetChildIndex(this.framESHP, 0);
            this.ClientArea.Controls.SetChildIndex(this.framPayrollDefaults, 0);
            this.ClientArea.Controls.SetChildIndex(this.framRetirement, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.framCheckOptions, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdEditCheck);
            this.TopPanel.Controls.Add(this.cmdEditPrint);
            this.TopPanel.Controls.Add(this.cmdEditRetirementDeductions);
            this.TopPanel.Controls.Add(this.cmdESHPOptions);
            this.TopPanel.Controls.Add(this.cmdClearWarrantLock);
            this.TopPanel.Size = new System.Drawing.Size(1020, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClearWarrantLock, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdESHPOptions, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEditRetirementDeductions, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEditPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEditCheck, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(117, 28);
            this.HeaderText.Text = "Customize";
            // 
            // cmbW2Type
            // 
            this.cmbW2Type.Items.AddRange(new object[] {
            "Laser Form 5201",
            "Narrow Regular Form 7600",
            "Narrow 2 up",
            "Wide 2 up",
            "Laser 4 up Form 5205"});
            this.cmbW2Type.Location = new System.Drawing.Point(20, 30);
            this.cmbW2Type.Name = "cmbW2Type";
            this.cmbW2Type.Size = new System.Drawing.Size(271, 40);
            this.cmbW2Type.TabIndex = 1;
            this.cmbW2Type.SelectedIndexChanged += new System.EventHandler(this.optW2Type_CheckedChanged);
            // 
            // cmbCheckImage
            // 
            this.cmbCheckImage.Items.AddRange(new object[] {
            "None",
            "Use Town Seal",
            "Use Selected Image"});
            this.cmbCheckImage.Location = new System.Drawing.Point(20, 30);
            this.cmbCheckImage.Name = "cmbCheckImage";
            this.cmbCheckImage.Size = new System.Drawing.Size(300, 40);
            this.cmbCheckImage.TabIndex = 51;
            // 
            // GridDeleteICMA
            // 
            this.GridDeleteICMA.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeNone;
            this.GridDeleteICMA.Cols = 1;
            this.GridDeleteICMA.ColumnHeadersVisible = false;
            this.GridDeleteICMA.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDNone;
            this.GridDeleteICMA.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExNone;
            this.GridDeleteICMA.FixedCols = 0;
            this.GridDeleteICMA.FixedRows = 0;
            this.GridDeleteICMA.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusLight;
            this.GridDeleteICMA.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightAlways;
            this.GridDeleteICMA.Location = new System.Drawing.Point(0, 24);
            this.GridDeleteICMA.MergeCells = fecherFoundation.FCGrid.MergeCellsSettings.flexMergeNever;
            this.GridDeleteICMA.Name = "GridDeleteICMA";
            this.GridDeleteICMA.OutlineBar = fecherFoundation.FCGrid.OutlineBarSettings.flexOutlineBarNone;
            this.GridDeleteICMA.RowHeadersVisible = false;
            this.GridDeleteICMA.Rows = 0;
            this.GridDeleteICMA.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollBoth;
            this.GridDeleteICMA.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionFree;
            this.GridDeleteICMA.Size = new System.Drawing.Size(9, 10);
            this.GridDeleteICMA.TabIndex = 67;
            this.GridDeleteICMA.Visible = false;
            // 
            // framPayrollDefaults
            // 
            this.framPayrollDefaults.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.framPayrollDefaults.AppearanceKey = "groupBoxLeftBorder";
            this.framPayrollDefaults.Controls.Add(this.cmbTrackLastCheckNumber);
            this.framPayrollDefaults.Controls.Add(this.chkPayPeriod);
            this.framPayrollDefaults.Controls.Add(this.Frame9);
            this.framPayrollDefaults.Controls.Add(this.txtCode2);
            this.framPayrollDefaults.Controls.Add(this.txtCode1);
            this.framPayrollDefaults.Controls.Add(this.Frame7);
            this.framPayrollDefaults.Controls.Add(this.chkFillAccount);
            this.framPayrollDefaults.Controls.Add(this.chkContractEncumbrances);
            this.framPayrollDefaults.Controls.Add(this.chkPrintSeparate);
            this.framPayrollDefaults.Controls.Add(this.chkSignature);
            this.framPayrollDefaults.Controls.Add(this.chkNegBalances);
            this.framPayrollDefaults.Controls.Add(this.cmdACHPrenote);
            this.framPayrollDefaults.Controls.Add(this.chkNoMSRS);
            this.framPayrollDefaults.Controls.Add(this.cboEmployerIDPrefix);
            this.framPayrollDefaults.Controls.Add(this.chkACHClearingHouse);
            this.framPayrollDefaults.Controls.Add(this.chkWeeklyPR);
            this.framPayrollDefaults.Controls.Add(this.chkSkipPages);
            this.framPayrollDefaults.Controls.Add(this.chkMMAUnemployment);
            this.framPayrollDefaults.Controls.Add(this.chkBalanced);
            this.framPayrollDefaults.Controls.Add(this.chkElectronicC1);
            this.framPayrollDefaults.Controls.Add(this.cboW2CheckType);
            this.framPayrollDefaults.Controls.Add(this.cboTypeChecks);
            this.framPayrollDefaults.Controls.Add(this.cboDataEntry);
            this.framPayrollDefaults.Controls.Add(this.cboReportSequence);
            this.framPayrollDefaults.Controls.Add(this.cboPrintSequence);
            this.framPayrollDefaults.Controls.Add(this.chkMultiFunds);
            this.framPayrollDefaults.Controls.Add(this.chkBankDD);
            this.framPayrollDefaults.Controls.Add(this.Label25);
            this.framPayrollDefaults.Controls.Add(this.Label13);
            this.framPayrollDefaults.Controls.Add(this.Label12);
            this.framPayrollDefaults.Controls.Add(this.lblEmployerID);
            this.framPayrollDefaults.Controls.Add(this.Label1);
            this.framPayrollDefaults.Controls.Add(this.Label2);
            this.framPayrollDefaults.Controls.Add(this.lblDataEntry);
            this.framPayrollDefaults.Controls.Add(this.Label3);
            this.framPayrollDefaults.Controls.Add(this.Label18);
            this.framPayrollDefaults.Font3D = fecherFoundation.Font3DConstants._None;
            this.framPayrollDefaults.Location = new System.Drawing.Point(30, 30);
            this.framPayrollDefaults.Name = "framPayrollDefaults";
            this.framPayrollDefaults.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.framPayrollDefaults.Size = new System.Drawing.Size(864, 938);
            this.framPayrollDefaults.TabIndex = 1001;
            this.framPayrollDefaults.Text = "Payroll Defaults";
            // 
            // cmbTrackLastCheckNumber
            // 
            this.cmbTrackLastCheckNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTrackLastCheckNumber.Location = new System.Drawing.Point(243, 217);
            this.cmbTrackLastCheckNumber.Name = "cmbTrackLastCheckNumber";
            this.cmbTrackLastCheckNumber.Size = new System.Drawing.Size(175, 40);
            this.cmbTrackLastCheckNumber.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.cmbTrackLastCheckNumber, "Auto-populate next check number by bank or by check type per bank");
            // 
            // chkPayPeriod
            // 
            this.chkPayPeriod.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPayPeriod.Location = new System.Drawing.Point(20, 489);
            this.chkPayPeriod.Name = "chkPayPeriod";
            this.chkPayPeriod.Size = new System.Drawing.Size(181, 23);
            this.chkPayPeriod.TabIndex = 14;
            this.chkPayPeriod.Text = "Print pay period on stub";
            this.ToolTip1.SetToolTip(this.chkPayPeriod, "On a standard stub the check memo will be replaced with the pay period");
            // 
            // Frame9
            // 
            this.Frame9.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame9.Controls.Add(this.txtRA1);
            this.Frame9.Controls.Add(this.txtRA2);
            this.Frame9.Controls.Add(this.txtRA3);
            this.Frame9.Controls.Add(this.txtRA4);
            this.Frame9.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame9.Location = new System.Drawing.Point(448, 180);
            this.Frame9.Name = "Frame9";
            this.Frame9.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame9.Size = new System.Drawing.Size(396, 240);
            this.Frame9.TabIndex = 102;
            this.Frame9.Text = "Return Address";
            // 
            // txtRA1
            // 
            this.txtRA1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRA1.Cursor = Wisej.Web.Cursors.Default;
            this.txtRA1.Location = new System.Drawing.Point(20, 30);
            this.txtRA1.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtRA1.Name = "txtRA1";
            this.txtRA1.Size = new System.Drawing.Size(356, 40);
            this.txtRA1.TabIndex = 106;
            // 
            // txtRA2
            // 
            this.txtRA2.BackColor = System.Drawing.SystemColors.Window;
            this.txtRA2.Cursor = Wisej.Web.Cursors.Default;
            this.txtRA2.Location = new System.Drawing.Point(20, 80);
            this.txtRA2.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtRA2.Name = "txtRA2";
            this.txtRA2.Size = new System.Drawing.Size(356, 40);
            this.txtRA2.TabIndex = 105;
            // 
            // txtRA3
            // 
            this.txtRA3.BackColor = System.Drawing.SystemColors.Window;
            this.txtRA3.Cursor = Wisej.Web.Cursors.Default;
            this.txtRA3.Location = new System.Drawing.Point(20, 130);
            this.txtRA3.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtRA3.Name = "txtRA3";
            this.txtRA3.Size = new System.Drawing.Size(356, 40);
            this.txtRA3.TabIndex = 104;
            // 
            // txtRA4
            // 
            this.txtRA4.BackColor = System.Drawing.SystemColors.Window;
            this.txtRA4.Cursor = Wisej.Web.Cursors.Default;
            this.txtRA4.Location = new System.Drawing.Point(20, 180);
            this.txtRA4.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtRA4.Name = "txtRA4";
            this.txtRA4.Size = new System.Drawing.Size(356, 40);
            this.txtRA4.TabIndex = 103;
            // 
            // txtCode2
            // 
            this.txtCode2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode2.Cursor = Wisej.Web.Cursors.Default;
            this.txtCode2.Location = new System.Drawing.Point(172, 898);
            this.txtCode2.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCode2.Name = "txtCode2";
            this.txtCode2.Size = new System.Drawing.Size(220, 40);
            this.txtCode2.TabIndex = 19;
            this.txtCode2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCode2_KeyPress);
            // 
            // txtCode1
            // 
            this.txtCode1.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode1.Cursor = Wisej.Web.Cursors.Default;
            this.txtCode1.Location = new System.Drawing.Point(172, 848);
            this.txtCode1.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.Size = new System.Drawing.Size(220, 40);
            this.txtCode1.TabIndex = 18;
            this.txtCode1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCode1_KeyPress);
            // 
            // Frame7
            // 
            this.Frame7.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame7.Controls.Add(this.lstMatchesBox1);
            this.Frame7.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame7.Location = new System.Drawing.Point(448, 430);
            this.Frame7.Name = "Frame7";
            this.Frame7.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame7.Size = new System.Drawing.Size(396, 300);
            this.Frame7.TabIndex = 75;
            this.Frame7.Text = "Matches Affecting Box 1 And 16 Of W-2";
            this.ToolTip1.SetToolTip(this.Frame7, "Also affects reports such as the Federal 941");
            // 
            // lstMatchesBox1
            // 
            this.lstMatchesBox1.BackColor = System.Drawing.SystemColors.Window;
            this.lstMatchesBox1.CheckBoxes = true;
            this.lstMatchesBox1.Location = new System.Drawing.Point(20, 30);
            this.lstMatchesBox1.Name = "lstMatchesBox1";
            this.lstMatchesBox1.Size = new System.Drawing.Size(356, 250);
            this.lstMatchesBox1.Style = 1;
            this.lstMatchesBox1.TabIndex = 76;
            // 
            // chkFillAccount
            // 
            this.chkFillAccount.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFillAccount.Location = new System.Drawing.Point(20, 600);
            this.chkFillAccount.Name = "chkFillAccount";
            this.chkFillAccount.Size = new System.Drawing.Size(305, 23);
            this.chkFillAccount.TabIndex = 17;
            this.chkFillAccount.Text = "Use home dept/div for new distribution lines";
            // 
            // chkContractEncumbrances
            // 
            this.chkContractEncumbrances.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkContractEncumbrances.Location = new System.Drawing.Point(20, 563);
            this.chkContractEncumbrances.Name = "chkContractEncumbrances";
            this.chkContractEncumbrances.Size = new System.Drawing.Size(216, 23);
            this.chkContractEncumbrances.TabIndex = 16;
            this.chkContractEncumbrances.Text = "Contracts Use Encumbrances";
            // 
            // chkPrintSeparate
            // 
            this.chkPrintSeparate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPrintSeparate.Location = new System.Drawing.Point(20, 526);
            this.chkPrintSeparate.Name = "chkPrintSeparate";
            this.chkPrintSeparate.Size = new System.Drawing.Size(320, 23);
            this.chkPrintSeparate.TabIndex = 15;
            this.chkPrintSeparate.Text = "Print direct deposit and EFT checks separately";
            this.ToolTip1.SetToolTip(this.chkPrintSeparate, "Allow non-negotiable checks to be printed on different or blank stock");
            // 
            // chkSignature
            // 
            this.chkSignature.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSignature.Location = new System.Drawing.Point(20, 452);
            this.chkSignature.Name = "chkSignature";
            this.chkSignature.Size = new System.Drawing.Size(185, 23);
            this.chkSignature.TabIndex = 13;
            this.chkSignature.Text = "Use Electronic Signature";
            // 
            // chkNegBalances
            // 
            this.chkNegBalances.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkNegBalances.Location = new System.Drawing.Point(20, 415);
            this.chkNegBalances.Name = "chkNegBalances";
            this.chkNegBalances.Size = new System.Drawing.Size(232, 23);
            this.chkNegBalances.TabIndex = 12;
            this.chkNegBalances.Text = "Check for Neg Pay Cat Balances";
            // 
            // cmdACHPrenote
            // 
            this.cmdACHPrenote.AppearanceKey = "actionButton";
            this.cmdACHPrenote.Cursor = Wisej.Web.Cursors.Default;
            this.cmdACHPrenote.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdACHPrenote.Location = new System.Drawing.Point(20, 798);
            this.cmdACHPrenote.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdACHPrenote.Name = "cmdACHPrenote";
            this.cmdACHPrenote.Size = new System.Drawing.Size(220, 40);
            this.cmdACHPrenote.TabIndex = 27;
            this.cmdACHPrenote.Text = "Print ACH Prenote file";
            this.cmdACHPrenote.Visible = false;
            this.cmdACHPrenote.Click += new System.EventHandler(this.cmdACHPrenote_Click);
            // 
            // chkNoMSRS
            // 
            this.chkNoMSRS.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkNoMSRS.Location = new System.Drawing.Point(20, 267);
            this.chkNoMSRS.Name = "chkNoMSRS";
            this.chkNoMSRS.Size = new System.Drawing.Size(218, 23);
            this.chkNoMSRS.TabIndex = 7;
            this.chkNoMSRS.Text = "Location does NOT use MSRS";
            this.chkNoMSRS.CheckedChanged += new System.EventHandler(this.chkNoMSRS_CheckedChanged);
            // 
            // cboEmployerIDPrefix
            // 
            this.cboEmployerIDPrefix.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmployerIDPrefix.Location = new System.Drawing.Point(172, 748);
            this.cboEmployerIDPrefix.Name = "cboEmployerIDPrefix";
            this.cboEmployerIDPrefix.Size = new System.Drawing.Size(80, 40);
            this.cboEmployerIDPrefix.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.cboEmployerIDPrefix, "This should almost always be 1. Call TRIO otherwise.");
            // 
            // chkACHClearingHouse
            // 
            this.chkACHClearingHouse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkACHClearingHouse.Location = new System.Drawing.Point(20, 674);
            this.chkACHClearingHouse.Name = "chkACHClearingHouse";
            this.chkACHClearingHouse.Size = new System.Drawing.Size(239, 23);
            this.chkACHClearingHouse.TabIndex = 22;
            this.chkACHClearingHouse.Text = "Automated Clearing House (ACH)";
            this.chkACHClearingHouse.CheckedChanged += new System.EventHandler(this.chkACHClearingHouse_CheckedChanged);
            // 
            // chkWeeklyPR
            // 
            this.chkWeeklyPR.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkWeeklyPR.Location = new System.Drawing.Point(20, 304);
            this.chkWeeklyPR.Name = "chkWeeklyPR";
            this.chkWeeklyPR.Size = new System.Drawing.Size(197, 23);
            this.chkWeeklyPR.TabIndex = 8;
            this.chkWeeklyPR.Text = "Weekly P/R with Bi-Weekly";
            this.chkWeeklyPR.CheckedChanged += new System.EventHandler(this.chkWeeklyPR_CheckedChanged);
            // 
            // chkSkipPages
            // 
            this.chkSkipPages.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSkipPages.Location = new System.Drawing.Point(20, 637);
            this.chkSkipPages.Name = "chkSkipPages";
            this.chkSkipPages.Size = new System.Drawing.Size(229, 23);
            this.chkSkipPages.TabIndex = 21;
            this.chkSkipPages.Text = "Skip pages between deductions";
            // 
            // chkMMAUnemployment
            // 
            this.chkMMAUnemployment.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMMAUnemployment.Location = new System.Drawing.Point(20, 341);
            this.chkMMAUnemployment.Name = "chkMMAUnemployment";
            this.chkMMAUnemployment.Size = new System.Drawing.Size(160, 23);
            this.chkMMAUnemployment.TabIndex = 9;
            this.chkMMAUnemployment.Text = "MMA Unemployment";
            // 
            // chkBalanced
            // 
            this.chkBalanced.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBalanced.Location = new System.Drawing.Point(20, 711);
            this.chkBalanced.Name = "chkBalanced";
            this.chkBalanced.Size = new System.Drawing.Size(147, 23);
            this.chkBalanced.TabIndex = 24;
            this.chkBalanced.Text = "Balanced ACH File";
            this.chkBalanced.Visible = false;
            // 
            // chkElectronicC1
            // 
            this.chkElectronicC1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkElectronicC1.Location = new System.Drawing.Point(20, 378);
            this.chkElectronicC1.Name = "chkElectronicC1";
            this.chkElectronicC1.Size = new System.Drawing.Size(140, 23);
            this.chkElectronicC1.TabIndex = 10;
            this.chkElectronicC1.Text = "Electronic 941/C1";
            // 
            // cboW2CheckType
            // 
            this.cboW2CheckType.BackColor = System.Drawing.SystemColors.Window;
            this.cboW2CheckType.Items.AddRange(new object[] {
            "Laser Format",
            "Narrow 2 Up"});
            this.cboW2CheckType.Location = new System.Drawing.Point(619, 130);
            this.cboW2CheckType.Name = "cboW2CheckType";
            this.cboW2CheckType.Size = new System.Drawing.Size(225, 40);
            this.cboW2CheckType.TabIndex = 5;
            // 
            // cboTypeChecks
            // 
            this.cboTypeChecks.BackColor = System.Drawing.SystemColors.Window;
            this.cboTypeChecks.Items.AddRange(new object[] {
            "Laser",
            "Custom"});
            this.cboTypeChecks.Location = new System.Drawing.Point(619, 80);
            this.cboTypeChecks.Name = "cboTypeChecks";
            this.cboTypeChecks.Size = new System.Drawing.Size(225, 40);
            this.cboTypeChecks.TabIndex = 4;
            this.cboTypeChecks.SelectedIndexChanged += new System.EventHandler(this.cboTypeChecks_SelectedIndexChanged);
            // 
            // cboDataEntry
            // 
            this.cboDataEntry.BackColor = System.Drawing.SystemColors.Window;
            this.cboDataEntry.Items.AddRange(new object[] {
            "Employee Name",
            "Employee Number",
            "Sequence Number",
            "Department / Div",
            "Selected Dept / Div"});
            this.cboDataEntry.Location = new System.Drawing.Point(619, 30);
            this.cboDataEntry.Name = "cboDataEntry";
            this.cboDataEntry.Size = new System.Drawing.Size(225, 40);
            this.cboDataEntry.TabIndex = 3;
            this.cboDataEntry.SelectedIndexChanged += new System.EventHandler(this.cboDataEntry_SelectedIndexChanged);
            // 
            // cboReportSequence
            // 
            this.cboReportSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboReportSequence.Items.AddRange(new object[] {
            "Employee Name",
            "Employee Number",
            "Sequence Number",
            "Department / Div"});
            this.cboReportSequence.Location = new System.Drawing.Point(193, 80);
            this.cboReportSequence.Name = "cboReportSequence";
            this.cboReportSequence.Size = new System.Drawing.Size(226, 40);
            this.cboReportSequence.TabIndex = 2;
            this.cboReportSequence.SelectedIndexChanged += new System.EventHandler(this.cboReportSequence_SelectedIndexChanged);
            // 
            // cboPrintSequence
            // 
            this.cboPrintSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboPrintSequence.Items.AddRange(new object[] {
            "Employee Name",
            "Employee Number",
            "Sequence Number",
            "Department / Div"});
            this.cboPrintSequence.Location = new System.Drawing.Point(193, 30);
            this.cboPrintSequence.Name = "cboPrintSequence";
            this.cboPrintSequence.Size = new System.Drawing.Size(225, 40);
            this.cboPrintSequence.TabIndex = 1;
            this.cboPrintSequence.SelectedIndexChanged += new System.EventHandler(this.cboPrintSequence_SelectedIndexChanged);
            // 
            // chkMultiFunds
            // 
            this.chkMultiFunds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMultiFunds.Location = new System.Drawing.Point(20, 637);
            this.chkMultiFunds.Name = "chkMultiFunds";
            this.chkMultiFunds.Size = new System.Drawing.Size(132, 23);
            this.chkMultiFunds.TabIndex = 11;
            this.chkMultiFunds.Text = "Use Multi Funds";
            this.chkMultiFunds.Visible = false;
            // 
            // chkBankDD
            // 
            this.chkBankDD.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBankDD.Location = new System.Drawing.Point(20, 180);
            this.chkBankDD.Name = "chkBankDD";
            this.chkBankDD.Size = new System.Drawing.Size(221, 23);
            this.chkBankDD.TabIndex = 20;
            this.chkBankDD.Text = "Direct Deposit Checks to Bank";
            this.chkBankDD.Visible = false;
            this.chkBankDD.CheckedChanged += new System.EventHandler(this.chkBankDD_CheckedChanged);
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(20, 231);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(196, 15);
            this.Label25.TabIndex = 107;
            this.Label25.Text = "REMEMBER LAST CHECK NUMBER";
            this.ToolTip1.SetToolTip(this.Label25, "Auto-populate next check number by bank or by check type per bank");
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 912);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(129, 15);
            this.Label13.TabIndex = 80;
            this.Label13.Text = "CODE 2 DESCRIPTION";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 862);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(129, 15);
            this.Label12.TabIndex = 79;
            this.Label12.Text = "CODE 1 DESCRIPTION";
            // 
            // lblEmployerID
            // 
            this.lblEmployerID.Location = new System.Drawing.Point(20, 762);
            this.lblEmployerID.Name = "lblEmployerID";
            this.lblEmployerID.Size = new System.Drawing.Size(131, 15);
            this.lblEmployerID.TabIndex = 25;
            this.lblEmployerID.Text = "EMPLOYER ID PREFIX";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(145, 15);
            this.Label1.TabIndex = 31;
            this.Label1.Text = "CHECK PRINT SEQUENCE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(157, 15);
            this.Label2.TabIndex = 30;
            this.Label2.Text = "REPORT SEQUENCE";
            // 
            // lblDataEntry
            // 
            this.lblDataEntry.Location = new System.Drawing.Point(448, 44);
            this.lblDataEntry.Name = "lblDataEntry";
            this.lblDataEntry.Size = new System.Drawing.Size(139, 15);
            this.lblDataEntry.TabIndex = 29;
            this.lblDataEntry.Text = "DATA ENTRY SEQUENCE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(448, 94);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 17);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "TYPE OF CHECKS";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(448, 144);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(60, 15);
            this.Label18.TabIndex = 23;
            this.Label18.Text = "W2 TYPE";
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1,
            imageListEntry2,
            imageListEntry3,
            imageListEntry4,
            imageListEntry5});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 256);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // fraW2s
            // 
            this.fraW2s.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.fraW2s.Controls.Add(this.Image1);
            this.fraW2s.Controls.Add(this.cmbW2Type);
            this.fraW2s.Font3D = fecherFoundation.Font3DConstants._None;
            this.fraW2s.Location = new System.Drawing.Point(30, 30);
            this.fraW2s.Name = "fraW2s";
            this.fraW2s.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.fraW2s.Size = new System.Drawing.Size(615, 520);
            this.fraW2s.TabIndex = 40;
            this.fraW2s.Text = "W2 Types";
            this.fraW2s.Visible = false;
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Location = new System.Drawing.Point(20, 80);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(575, 420);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // framCheckOptions
            // 
            this.framCheckOptions.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.framCheckOptions.AppearanceKey = "groupBoxLeftBorder";
            this.framCheckOptions.Controls.Add(this.Frame5);
            this.framCheckOptions.Controls.Add(this.chkIgnorePrinterFonts);
            this.framCheckOptions.Controls.Add(this.framLogo);
            this.framCheckOptions.Controls.Add(this.Frame6);
            this.framCheckOptions.Font3D = fecherFoundation.Font3DConstants._None;
            this.framCheckOptions.Location = new System.Drawing.Point(30, 30);
            this.framCheckOptions.Name = "framCheckOptions";
            this.framCheckOptions.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.framCheckOptions.Size = new System.Drawing.Size(819, 668);
            this.framCheckOptions.TabIndex = 32;
            this.framCheckOptions.Text = "Printing Options";
            this.framCheckOptions.Visible = false;
            // 
            // Frame5
            // 
            this.Frame5.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame5.Controls.Add(this.chkPProcessReports);
            this.Frame5.Controls.Add(this.chkDefaultChecks);
            this.Frame5.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame5.Location = new System.Drawing.Point(576, 67);
            this.Frame5.Name = "Frame5";
            this.Frame5.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame5.Size = new System.Drawing.Size(243, 225);
            this.Frame5.TabIndex = 69;
            this.Frame5.Text = "Default Printer";
            // 
            // chkPProcessReports
            // 
            this.chkPProcessReports.Location = new System.Drawing.Point(20, 67);
            this.chkPProcessReports.Name = "chkPProcessReports";
            this.chkPProcessReports.Size = new System.Drawing.Size(182, 23);
            this.chkPProcessReports.TabIndex = 71;
            this.chkPProcessReports.Text = "Payroll Process Reports";
            // 
            // chkDefaultChecks
            // 
            this.chkDefaultChecks.Location = new System.Drawing.Point(20, 30);
            this.chkDefaultChecks.Name = "chkDefaultChecks";
            this.chkDefaultChecks.Size = new System.Drawing.Size(78, 23);
            this.chkDefaultChecks.TabIndex = 70;
            this.chkDefaultChecks.Text = "Checks";
            // 
            // chkIgnorePrinterFonts
            // 
            this.chkIgnorePrinterFonts.Location = new System.Drawing.Point(20, 30);
            this.chkIgnorePrinterFonts.Name = "chkIgnorePrinterFonts";
            this.chkIgnorePrinterFonts.Size = new System.Drawing.Size(186, 23);
            this.chkIgnorePrinterFonts.TabIndex = 68;
            this.chkIgnorePrinterFonts.Text = "Do Not Use Printer Fonts";
            // 
            // framLogo
            // 
            this.framLogo.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.framLogo.Controls.Add(this.fileUpload);
            this.framLogo.Controls.Add(this.cmbCheckImage);
            this.framLogo.Controls.Add(this.Label11);
            this.framLogo.Controls.Add(this.Label10);
            this.framLogo.Controls.Add(this.Label9);
            this.framLogo.Controls.Add(this.imgLogo);
            this.framLogo.Font3D = fecherFoundation.Font3DConstants._None;
            this.framLogo.Location = new System.Drawing.Point(20, 302);
            this.framLogo.Name = "framLogo";
            this.framLogo.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.framLogo.Size = new System.Drawing.Size(335, 350);
            this.framLogo.TabIndex = 47;
            this.framLogo.Text = "Logo";
            // 
            // fileUpload
            // 
            this.fileUpload.AllowedFileTypes = "image/*";
            this.fileUpload.Location = new System.Drawing.Point(20, 103);
            this.fileUpload.Name = "fileUpload";
            this.fileUpload.Size = new System.Drawing.Size(300, 40);
            this.fileUpload.TabIndex = 55;
            this.fileUpload.Text = "Pick Image";
            this.fileUpload.Uploaded += new Wisej.Web.UploadedEventHandler(this.fileUpload_Uploaded);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 80);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(300, 15);
            this.Label11.TabIndex = 53;
            this.Label11.Text = "PICK IMAGE FILE TO SHOW ON CHECKS";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(340, 87);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(205, 15);
            this.Label10.TabIndex = 52;
            this.Label10.Text = "PICK DIRECTORY";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label10.Visible = false;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(340, 30);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(205, 15);
            this.Label9.TabIndex = 51;
            this.Label9.Text = "PICK DRIVE";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label9.Visible = false;
            // 
            // imgLogo
            // 
            this.imgLogo.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgLogo.Location = new System.Drawing.Point(20, 154);
            this.imgLogo.Name = "imgLogo";
            this.imgLogo.Size = new System.Drawing.Size(175, 175);
            this.imgLogo.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // Frame6
            // 
            this.Frame6.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame6.Controls.Add(this.txtW2Horizontal);
            this.Frame6.Controls.Add(this.cmdSampleW2);
            this.Frame6.Controls.Add(this.cmdSampleCheck);
            this.Frame6.Controls.Add(this.txtCheckLineAdjustment);
            this.Frame6.Controls.Add(this.txtW2Adjustment);
            this.Frame6.Controls.Add(this.Label8);
            this.Frame6.Controls.Add(this.Label4);
            this.Frame6.Controls.Add(this.Label5);
            this.Frame6.Controls.Add(this.Label6);
            this.Frame6.Controls.Add(this.Label7);
            this.Frame6.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame6.Location = new System.Drawing.Point(20, 67);
            this.Frame6.Name = "Frame6";
            this.Frame6.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame6.Size = new System.Drawing.Size(522, 225);
            this.Frame6.TabIndex = 33;
            this.Frame6.Text = "Laser Printer Line Adjustment";
            // 
            // txtW2Horizontal
            // 
            this.txtW2Horizontal.BackColor = System.Drawing.SystemColors.Window;
            this.txtW2Horizontal.Cursor = Wisej.Web.Cursors.Default;
            this.txtW2Horizontal.Location = new System.Drawing.Point(200, 105);
            this.txtW2Horizontal.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtW2Horizontal.Name = "txtW2Horizontal";
            this.txtW2Horizontal.Size = new System.Drawing.Size(80, 40);
            this.txtW2Horizontal.TabIndex = 73;
            this.txtW2Horizontal.Text = "0";
            this.txtW2Horizontal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // cmdSampleW2
            // 
            this.cmdSampleW2.AppearanceKey = "actionButton";
            this.cmdSampleW2.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSampleW2.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdSampleW2.Location = new System.Drawing.Point(300, 105);
            this.cmdSampleW2.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdSampleW2.Name = "cmdSampleW2";
            this.cmdSampleW2.Size = new System.Drawing.Size(202, 40);
            this.cmdSampleW2.TabIndex = 72;
            this.cmdSampleW2.Text = "Print W2 Sample";
            this.ToolTip1.SetToolTip(this.cmdSampleW2, "Screen must be saved before changes will affect the sample print");
            this.cmdSampleW2.Click += new System.EventHandler(this.cmdSampleW2_Click);
            // 
            // cmdSampleCheck
            // 
            this.cmdSampleCheck.AppearanceKey = "actionButton";
            this.cmdSampleCheck.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSampleCheck.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdSampleCheck.Location = new System.Drawing.Point(300, 55);
            this.cmdSampleCheck.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdSampleCheck.Name = "cmdSampleCheck";
            this.cmdSampleCheck.Size = new System.Drawing.Size(202, 40);
            this.cmdSampleCheck.TabIndex = 46;
            this.cmdSampleCheck.Text = "Print Check Sample";
            this.ToolTip1.SetToolTip(this.cmdSampleCheck, "Screen must be saved before changes will affect the sample print");
            this.cmdSampleCheck.Click += new System.EventHandler(this.cmdSampleCheck_Click);
            // 
            // txtCheckLineAdjustment
            // 
            this.txtCheckLineAdjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtCheckLineAdjustment.Cursor = Wisej.Web.Cursors.Default;
            this.txtCheckLineAdjustment.Location = new System.Drawing.Point(100, 55);
            this.txtCheckLineAdjustment.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCheckLineAdjustment.Name = "txtCheckLineAdjustment";
            this.txtCheckLineAdjustment.Size = new System.Drawing.Size(80, 40);
            this.txtCheckLineAdjustment.TabIndex = 35;
            this.txtCheckLineAdjustment.Text = "0";
            this.txtCheckLineAdjustment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtW2Adjustment
            // 
            this.txtW2Adjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtW2Adjustment.Cursor = Wisej.Web.Cursors.Default;
            this.txtW2Adjustment.Location = new System.Drawing.Point(100, 105);
            this.txtW2Adjustment.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtW2Adjustment.Name = "txtW2Adjustment";
            this.txtW2Adjustment.Size = new System.Drawing.Size(80, 40);
            this.txtW2Adjustment.TabIndex = 34;
            this.txtW2Adjustment.Text = "0";
            this.txtW2Adjustment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(200, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(85, 15);
            this.Label8.TabIndex = 74;
            this.Label8.Text = "HORIZONTAL";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 69);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(53, 19);
            this.Label4.TabIndex = 39;
            this.Label4.Text = "CHECKS";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 155);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(482, 24);
            this.Label5.TabIndex = 38;
            this.Label5.Text = "ENTER THE NUMBER OF LINES THE DATA NEEDS TO BE MOVED. NEGATIVE AND FRACTIONAL NUM" +
    "BERS CAN BE ENTERED";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 190);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(159, 15);
            this.Label6.TabIndex = 37;
            this.Label6.Text = "EXAMPLES:   (-1,1,2.5)";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 119);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(29, 19);
            this.Label7.TabIndex = 36;
            this.Label7.Text = "W2\'S";
            // 
            // framRetirement
            // 
            this.framRetirement.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.framRetirement.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framRetirement.AppearanceKey = "groupBoxNoBorders";
            this.framRetirement.Controls.Add(this.Frame1);
            this.framRetirement.Controls.Add(this.Frame2);
            this.framRetirement.Font3D = fecherFoundation.Font3DConstants._None;
            this.framRetirement.Location = new System.Drawing.Point(30, 30);
            this.framRetirement.Name = "framRetirement";
            this.framRetirement.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.framRetirement.Size = new System.Drawing.Size(943, 668);
            this.framRetirement.TabIndex = 57;
            this.framRetirement.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.Frame8);
            this.Frame1.Controls.Add(this.Frame4);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.fraW2Types);
            this.Frame1.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame1.FormatCaption = false;
            this.Frame1.MinimumSize = new System.Drawing.Size(960, 433);
            this.Frame1.Name = "Frame1";
            this.Frame1.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame1.Size = new System.Drawing.Size(960, 433);
            this.Frame1.TabIndex = 60;
            this.Frame1.Text = "MainePERS";
            // 
            // Frame8
            // 
            this.Frame8.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame8.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame8.Controls.Add(this.gridExtraMSRS);
            this.Frame8.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame8.FormatCaption = false;
            this.Frame8.Location = new System.Drawing.Point(20, 266);
            this.Frame8.MinimumSize = new System.Drawing.Size(940, 157);
            this.Frame8.Name = "Frame8";
            this.Frame8.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame8.Size = new System.Drawing.Size(940, 157);
            this.Frame8.TabIndex = 77;
            this.Frame8.Text = "Extra MainePERS";
            // 
            // gridExtraMSRS
            // 
            this.gridExtraMSRS.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeNone;
            this.gridExtraMSRS.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridExtraMSRS.Cols = 4;
            this.gridExtraMSRS.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridExtraMSRS.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExNone;
            this.gridExtraMSRS.ExtendLastCol = true;
            this.gridExtraMSRS.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusLight;
            this.gridExtraMSRS.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightAlways;
            this.gridExtraMSRS.Location = new System.Drawing.Point(20, 30);
            this.gridExtraMSRS.MergeCells = fecherFoundation.FCGrid.MergeCellsSettings.flexMergeNever;
            this.gridExtraMSRS.Name = "gridExtraMSRS";
            this.gridExtraMSRS.OutlineBar = fecherFoundation.FCGrid.OutlineBarSettings.flexOutlineBarNone;
            this.gridExtraMSRS.ReadOnly = false;
            this.gridExtraMSRS.Rows = 4;
            this.gridExtraMSRS.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollBoth;
            this.gridExtraMSRS.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionFree;
            this.gridExtraMSRS.Size = new System.Drawing.Size(900, 107);
            this.gridExtraMSRS.TabIndex = 78;
            // 
            // Frame4
            // 
            this.Frame4.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame4.Controls.Add(this.lstPayback);
            this.Frame4.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame4.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);
            this.Frame4.Location = new System.Drawing.Point(640, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame4.Size = new System.Drawing.Size(290, 216);
            this.Frame4.TabIndex = 65;
            this.Frame4.Text = "Payback Ins Deductions";
            // 
            // lstPayback
            // 
            this.lstPayback.BackColor = System.Drawing.SystemColors.Window;
            this.lstPayback.CheckBoxes = true;
            this.lstPayback.Location = new System.Drawing.Point(20, 30);
            this.lstPayback.Name = "lstPayback";
            this.lstPayback.Size = new System.Drawing.Size(250, 166);
            this.lstPayback.Style = 1;
            this.lstPayback.TabIndex = 66;
            // 
            // Frame3
            // 
            this.Frame3.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame3.Controls.Add(this.lstMSRS);
            this.Frame3.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame3.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);
            this.Frame3.FormatCaption = false;
            this.Frame3.Location = new System.Drawing.Point(330, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame3.Size = new System.Drawing.Size(290, 216);
            this.Frame3.TabIndex = 63;
            this.Frame3.Text = "PERS Life Ins Deductions";
            // 
            // lstMSRS
            // 
            this.lstMSRS.BackColor = System.Drawing.SystemColors.Window;
            this.lstMSRS.CheckBoxes = true;
            this.lstMSRS.Location = new System.Drawing.Point(20, 30);
            this.lstMSRS.Name = "lstMSRS";
            this.lstMSRS.Size = new System.Drawing.Size(250, 166);
            this.lstMSRS.Style = 1;
            this.lstMSRS.TabIndex = 64;
            this.lstMSRS.SelectedIndexChanged += new System.EventHandler(this.lstMSRS_SelectedIndexChanged);
            // 
            // fraW2Types
            // 
            this.fraW2Types.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.fraW2Types.Controls.Add(this.lstRetirement);
            this.fraW2Types.Font3D = fecherFoundation.Font3DConstants._None;
            this.fraW2Types.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);
            this.fraW2Types.Location = new System.Drawing.Point(20, 30);
            this.fraW2Types.Name = "fraW2Types";
            this.fraW2Types.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.fraW2Types.Size = new System.Drawing.Size(290, 216);
            this.fraW2Types.TabIndex = 61;
            this.fraW2Types.Text = "Retirement Cont Deductions";
            // 
            // lstRetirement
            // 
            this.lstRetirement.BackColor = System.Drawing.SystemColors.Window;
            this.lstRetirement.CheckBoxes = true;
            this.lstRetirement.Location = new System.Drawing.Point(20, 30);
            this.lstRetirement.Name = "lstRetirement";
            this.lstRetirement.Size = new System.Drawing.Size(250, 166);
            this.lstRetirement.Style = 1;
            this.lstRetirement.TabIndex = 62;
            this.lstRetirement.SelectedIndexChanged += new System.EventHandler(this.lstRetirement_SelectedIndexChanged);
            // 
            // Frame2
            // 
            this.Frame2.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Font3D = fecherFoundation.Font3DConstants._None;
            this.Frame2.FormatCaption = false;
            this.Frame2.Location = new System.Drawing.Point(0, 433);
            this.Frame2.MinimumSize = new System.Drawing.Size(960, 219);
            this.Frame2.Name = "Frame2";
            this.Frame2.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.Frame2.Size = new System.Drawing.Size(960, 219);
            this.Frame2.TabIndex = 58;
            this.Frame2.Text = "ICMA Deductions";
            // 
            // Grid
            // 
            this.Grid.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeNone;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 8;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExNone;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusLight;
            this.Grid.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightAlways;
            this.Grid.Location = new System.Drawing.Point(20, 30);
            this.Grid.MergeCells = fecherFoundation.FCGrid.MergeCellsSettings.flexMergeNever;
            this.Grid.Name = "Grid";
            this.Grid.OutlineBar = fecherFoundation.FCGrid.OutlineBarSettings.flexOutlineBarNone;
            this.Grid.ReadOnly = false;
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollBoth;
            this.Grid.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionFree;
            this.Grid.Size = new System.Drawing.Size(920, 169);
            this.Grid.StandardTab = false;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 59;
            this.ToolTip1.SetToolTip(this.Grid, "Use Insert and Delete to add or delete rows");
            this.Grid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEdit);
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // framESHP
            // 
            this.framESHP.Alignment = fecherFoundation.FCFrame.AlignFrameTextConstants._LeftJustify;
            this.framESHP.Controls.Add(this.txtESPExportPath);
            this.framESHP.Controls.Add(this.txtESPW2Path);
            this.framESHP.Controls.Add(this.txtESPCheckPath);
            this.framESHP.Controls.Add(this.txtESPDataPath);
            this.framESHP.Controls.Add(this.txtESPPasswordConfirm);
            this.framESHP.Controls.Add(this.txtESPPassword);
            this.framESHP.Controls.Add(this.txtESPUser);
            this.framESHP.Controls.Add(this.txtEspHost);
            this.framESHP.Controls.Add(this.txtEspPort);
            this.framESHP.Controls.Add(this.txtEspOrg);
            this.framESHP.Controls.Add(this.Label24);
            this.framESHP.Controls.Add(this.Label23);
            this.framESHP.Controls.Add(this.Label22);
            this.framESHP.Controls.Add(this.Label21);
            this.framESHP.Controls.Add(this.Label20);
            this.framESHP.Controls.Add(this.Label19);
            this.framESHP.Controls.Add(this.Label17);
            this.framESHP.Controls.Add(this.Label16);
            this.framESHP.Controls.Add(this.Label15);
            this.framESHP.Controls.Add(this.Label14);
            this.framESHP.Font3D = fecherFoundation.Font3DConstants._None;
            this.framESHP.Location = new System.Drawing.Point(30, 30);
            this.framESHP.Name = "framESHP";
            this.framESHP.ShadowStyle = fecherFoundation.FCFrame.ShadowStyleConstants._Inset;
            this.framESHP.Size = new System.Drawing.Size(490, 540);
            this.framESHP.TabIndex = 81;
            this.framESHP.Text = "Esp";
            this.framESHP.Visible = false;
            // 
            // txtESPExportPath
            // 
            this.txtESPExportPath.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPExportPath.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPExportPath.Location = new System.Drawing.Point(175, 480);
            this.txtESPExportPath.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPExportPath.Name = "txtESPExportPath";
            this.txtESPExportPath.Size = new System.Drawing.Size(295, 40);
            this.txtESPExportPath.TabIndex = 101;
            this.txtESPExportPath.Text = "Data/Export";
            // 
            // txtESPW2Path
            // 
            this.txtESPW2Path.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPW2Path.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPW2Path.Location = new System.Drawing.Point(175, 430);
            this.txtESPW2Path.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPW2Path.Name = "txtESPW2Path";
            this.txtESPW2Path.Size = new System.Drawing.Size(295, 40);
            this.txtESPW2Path.TabIndex = 100;
            this.txtESPW2Path.Text = "PDF/W2";
            // 
            // txtESPCheckPath
            // 
            this.txtESPCheckPath.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPCheckPath.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPCheckPath.Location = new System.Drawing.Point(175, 380);
            this.txtESPCheckPath.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPCheckPath.Name = "txtESPCheckPath";
            this.txtESPCheckPath.Size = new System.Drawing.Size(295, 40);
            this.txtESPCheckPath.TabIndex = 99;
            this.txtESPCheckPath.Text = "PDF/Checks";
            // 
            // txtESPDataPath
            // 
            this.txtESPDataPath.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPDataPath.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPDataPath.Location = new System.Drawing.Point(175, 330);
            this.txtESPDataPath.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPDataPath.Name = "txtESPDataPath";
            this.txtESPDataPath.Size = new System.Drawing.Size(295, 40);
            this.txtESPDataPath.TabIndex = 98;
            this.txtESPDataPath.Text = "Data/Import";
            // 
            // txtESPPasswordConfirm
            // 
            this.txtESPPasswordConfirm.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPPasswordConfirm.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPPasswordConfirm.Location = new System.Drawing.Point(175, 280);
            this.txtESPPasswordConfirm.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPPasswordConfirm.Name = "txtESPPasswordConfirm";
            this.txtESPPasswordConfirm.Size = new System.Drawing.Size(295, 40);
            this.txtESPPasswordConfirm.TabIndex = 97;
            // 
            // txtESPPassword
            // 
            this.txtESPPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPPassword.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPPassword.Location = new System.Drawing.Point(175, 230);
            this.txtESPPassword.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPPassword.Name = "txtESPPassword";
            this.txtESPPassword.Size = new System.Drawing.Size(295, 40);
            this.txtESPPassword.TabIndex = 96;
            // 
            // txtESPUser
            // 
            this.txtESPUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtESPUser.Cursor = Wisej.Web.Cursors.Default;
            this.txtESPUser.Location = new System.Drawing.Point(175, 180);
            this.txtESPUser.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtESPUser.Name = "txtESPUser";
            this.txtESPUser.Size = new System.Drawing.Size(295, 40);
            this.txtESPUser.TabIndex = 95;
            // 
            // txtEspHost
            // 
            this.txtEspHost.BackColor = System.Drawing.SystemColors.Window;
            this.txtEspHost.Cursor = Wisej.Web.Cursors.Default;
            this.txtEspHost.Location = new System.Drawing.Point(175, 130);
            this.txtEspHost.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtEspHost.Name = "txtEspHost";
            this.txtEspHost.Size = new System.Drawing.Size(295, 40);
            this.txtEspHost.TabIndex = 94;
            // 
            // txtEspPort
            // 
            this.txtEspPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtEspPort.Cursor = Wisej.Web.Cursors.Default;
            this.txtEspPort.Location = new System.Drawing.Point(175, 80);
            this.txtEspPort.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtEspPort.Name = "txtEspPort";
            this.txtEspPort.Size = new System.Drawing.Size(295, 40);
            this.txtEspPort.TabIndex = 93;
            this.txtEspPort.Text = "22";
            // 
            // txtEspOrg
            // 
            this.txtEspOrg.BackColor = System.Drawing.SystemColors.Window;
            this.txtEspOrg.Cursor = Wisej.Web.Cursors.Default;
            this.txtEspOrg.Location = new System.Drawing.Point(175, 30);
            this.txtEspOrg.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtEspOrg.Name = "txtEspOrg";
            this.txtEspOrg.Size = new System.Drawing.Size(295, 40);
            this.txtEspOrg.TabIndex = 92;
            this.txtEspOrg.Text = "0";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(20, 294);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(136, 15);
            this.Label24.TabIndex = 91;
            this.Label24.Text = "CONFIRM PASSWORD";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(20, 494);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(100, 15);
            this.Label23.TabIndex = 90;
            this.Label23.Text = "EXPORT PATH";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(20, 444);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(105, 15);
            this.Label22.TabIndex = 89;
            this.Label22.Text = "W2 PATH";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(20, 394);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(140, 15);
            this.Label21.TabIndex = 88;
            this.Label21.Text = "CHECK PATH";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(20, 344);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(184, 15);
            this.Label20.TabIndex = 87;
            this.Label20.Text = "DATA PATH";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(20, 244);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(79, 15);
            this.Label19.TabIndex = 86;
            this.Label19.Text = "PASSWORD";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(20, 194);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(114, 15);
            this.Label17.TabIndex = 85;
            this.Label17.Text = "USER";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(20, 144);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(99, 15);
            this.Label16.TabIndex = 84;
            this.Label16.Text = "HOST";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 94);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(88, 15);
            this.Label15.TabIndex = 83;
            this.Label15.Text = "PORT";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 44);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(100, 15);
            this.Label14.TabIndex = 82;
            this.Label14.Text = "ORGANIZATION #";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuViewW2Types,
            this.mnuCreateACH});
            this.MainMenu1.Name = null;
            // 
            // mnuViewW2Types
            // 
            this.mnuViewW2Types.Index = 0;
            this.mnuViewW2Types.Name = "mnuViewW2Types";
            this.mnuViewW2Types.Text = "View W2 Types";
            this.mnuViewW2Types.Click += new System.EventHandler(this.mnuViewW2Types_Click);
            // 
            // mnuCreateACH
            // 
            this.mnuCreateACH.Index = 1;
            this.mnuCreateACH.Name = "mnuCreateACH";
            this.mnuCreateACH.Text = "Create TA ACH Files";
            this.mnuCreateACH.Visible = false;
            this.mnuCreateACH.Click += new System.EventHandler(this.mnuCreateACH_Click);
            // 
            // mnuEditCheck
            // 
            this.mnuEditCheck.Index = -1;
            this.mnuEditCheck.Name = "mnuEditCheck";
            this.mnuEditCheck.Text = "Edit Custom Check Format";
            this.mnuEditCheck.Click += new System.EventHandler(this.mnuEditCheck_Click);
            // 
            // mnuEditPrint
            // 
            this.mnuEditPrint.Index = -1;
            this.mnuEditPrint.Name = "mnuEditPrint";
            this.mnuEditPrint.Text = "Edit Printing Options";
            this.mnuEditPrint.Click += new System.EventHandler(this.mnuEditPrint_Click);
            // 
            // mnuEditRetirementDeductions
            // 
            this.mnuEditRetirementDeductions.Index = -1;
            this.mnuEditRetirementDeductions.Name = "mnuEditRetirementDeductions";
            this.mnuEditRetirementDeductions.Text = "Edit Retirement Deductions";
            this.mnuEditRetirementDeductions.Click += new System.EventHandler(this.mnuEditRetirementDeductions_Click);
            // 
            // mnuESHPOptions
            // 
            this.mnuESHPOptions.Index = -1;
            this.mnuESHPOptions.Name = "mnuESHPOptions";
            this.mnuESHPOptions.Text = "Edit ESP Options";
            this.mnuESHPOptions.Click += new System.EventHandler(this.mnuESHPOptions_Click);
            // 
            // mnuClearWarrantLock
            // 
            this.mnuClearWarrantLock.Index = -1;
            this.mnuClearWarrantLock.Name = "mnuClearWarrantLock";
            this.mnuClearWarrantLock.Text = "Clear Warrant/Journal Lock";
            this.mnuClearWarrantLock.Click += new System.EventHandler(this.mnuClearWarrantLock_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSepar,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 0;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                        ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit               ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSave.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdSave.Location = new System.Drawing.Point(471, 30);
            this.cmdSave.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdESHPOptions
            // 
            this.cmdESHPOptions.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdESHPOptions.Cursor = Wisej.Web.Cursors.Default;
            this.cmdESHPOptions.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdESHPOptions.Location = new System.Drawing.Point(682, 29);
            this.cmdESHPOptions.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdESHPOptions.Name = "cmdESHPOptions";
            this.cmdESHPOptions.Size = new System.Drawing.Size(120, 24);
            this.cmdESHPOptions.TabIndex = 1;
            this.cmdESHPOptions.Text = "Edit ESP Options";
            this.cmdESHPOptions.Click += new System.EventHandler(this.mnuESHPOptions_Click);
            // 
            // cmdEditRetirementDeductions
            // 
            this.cmdEditRetirementDeductions.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEditRetirementDeductions.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEditRetirementDeductions.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdEditRetirementDeductions.Location = new System.Drawing.Point(490, 29);
            this.cmdEditRetirementDeductions.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdEditRetirementDeductions.Name = "cmdEditRetirementDeductions";
            this.cmdEditRetirementDeductions.Size = new System.Drawing.Size(186, 24);
            this.cmdEditRetirementDeductions.TabIndex = 2;
            this.cmdEditRetirementDeductions.Text = "Edit Retirement Deductions";
            this.cmdEditRetirementDeductions.Click += new System.EventHandler(this.mnuEditRetirementDeductions_Click);
            // 
            // cmdEditPrint
            // 
            this.cmdEditPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEditPrint.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEditPrint.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdEditPrint.Location = new System.Drawing.Point(342, 29);
            this.cmdEditPrint.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdEditPrint.Name = "cmdEditPrint";
            this.cmdEditPrint.Size = new System.Drawing.Size(142, 24);
            this.cmdEditPrint.TabIndex = 3;
            this.cmdEditPrint.Text = "Edit Printing Options";
            this.cmdEditPrint.Click += new System.EventHandler(this.mnuEditPrint_Click);
            // 
            // cmdEditCheck
            // 
            this.cmdEditCheck.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEditCheck.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEditCheck.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdEditCheck.Location = new System.Drawing.Point(152, 29);
            this.cmdEditCheck.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdEditCheck.Name = "cmdEditCheck";
            this.cmdEditCheck.Size = new System.Drawing.Size(184, 24);
            this.cmdEditCheck.TabIndex = 4;
            this.cmdEditCheck.Text = "Edit Custom Check Format";
            this.cmdEditCheck.Click += new System.EventHandler(this.mnuEditCheck_Click);
            // 
            // cmdClearWarrantLock
            // 
            this.cmdClearWarrantLock.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClearWarrantLock.Cursor = Wisej.Web.Cursors.Default;
            this.cmdClearWarrantLock.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdClearWarrantLock.Location = new System.Drawing.Point(808, 29);
            this.cmdClearWarrantLock.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdClearWarrantLock.Name = "cmdClearWarrantLock";
            this.cmdClearWarrantLock.Size = new System.Drawing.Size(184, 24);
            this.cmdClearWarrantLock.TabIndex = 5;
            this.cmdClearWarrantLock.Text = "Clear Warrant/Journal Lock";
            this.cmdClearWarrantLock.Click += new System.EventHandler(this.mnuClearWarrantLock_Click);
            // 
            // frmDefaultInformation
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BorderStyleOriginal = fecherFoundation.FCForm.BorderStyleConstants.vbSizable;
            this.ClientSize = new System.Drawing.Size(1020, 666);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.FillStyle = fecherFoundation.FillStyleConstants.vbFSTransparent;
            this.KeyPreview = true;
            this.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.Menu = this.MainMenu1;
            this.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.Name = "frmDefaultInformation";
            this.PaletteMode = fecherFoundation.FCForm.PalleteModeConstants.vbPaletteModeHalfTone;
            this.ScaleMode = fecherFoundation.ScaleModeConstants.vbTwips;
            this.Text = "Customize";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDefaultInformation_Load);
            this.Activated += new System.EventHandler(this.frmDefaultInformation_Activated);
            this.Resize += new System.EventHandler(this.frmDefaultInformation_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDefaultInformation_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleteICMA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayrollDefaults)).EndInit();
            this.framPayrollDefaults.ResumeLayout(false);
            this.framPayrollDefaults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkFillAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContractEncumbrances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSeparate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNegBalances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdACHPrenote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoMSRS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkACHClearingHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSkipPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMMAUnemployment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalanced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkElectronicC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMultiFunds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankDD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraW2s)).EndInit();
            this.fraW2s.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framCheckOptions)).EndInit();
            this.framCheckOptions.ResumeLayout(false);
            this.framCheckOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPProcessReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultChecks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIgnorePrinterFonts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLogo)).EndInit();
            this.framLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSampleW2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSampleCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRetirement)).EndInit();
            this.framRetirement.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
            this.Frame8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridExtraMSRS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraW2Types)).EndInit();
            this.fraW2Types.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framESHP)).EndInit();
            this.framESHP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdESHPOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditRetirementDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearWarrantLock)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
		private Upload fileUpload;
        private FCButton cmdClearWarrantLock;
        private FCButton cmdEditCheck;
        private FCButton cmdEditPrint;
        private FCButton cmdEditRetirementDeductions;
        private FCButton cmdESHPOptions;
    }
}