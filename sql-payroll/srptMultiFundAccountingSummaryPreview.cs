//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMultiFundAccountingSummaryPreview.
	/// </summary>
	public partial class srptMultiFundAccountingSummaryPreview : FCSectionReport
	{
		public srptMultiFundAccountingSummaryPreview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptMultiFundAccountingSummaryPreview InstancePtr
		{
			get
			{
				return (srptMultiFundAccountingSummaryPreview)Sys.GetInstance(typeof(srptMultiFundAccountingSummaryPreview));
			}
		}

		protected srptMultiFundAccountingSummaryPreview _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo?.Dispose();
                rsAccountInfo = null;
				rsTaxAmountInfo?.Dispose();
                rsTaxAmountInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptMultiFundAccountingSummaryPreview	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Summary Type Binder Values
		// G - Gross Pay Section
		// T - Taxes Section
		// M - Employers Share of FICA and Medicare
		// D - Deductions Section
		// E - Employer's Match Section
		// P - Paid Section
		// C - Checking Account Section
		// X - Done witrh report
		private struct EncumCTLType
		{
			public double dblTotal;
			public bool boolSchool;
			public string strFund;
		};

		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		
		string strSummaryType = "";
		string strTaxType = "";
		bool blnFirstRecord;
		string strHeaderType;
		// vbPorter upgrade warning: curCredits As Decimal	OnWrite(int, Decimal)
		Decimal curCredits;
		// vbPorter upgrade warning: curDebits As Decimal	OnWrite(int, Decimal)
		Decimal curDebits;
		// vbPorter upgrade warning: curPaidTotal As Decimal	OnWrite(int, Decimal)
		Decimal curPaidTotal;
		// vbPorter upgrade warning: curGrossPay As Decimal	OnWrite(int, Decimal)
		Decimal curGrossPay;
		// vbPorter upgrade warning: curFederalTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFederalTaxWH;
		// vbPorter upgrade warning: curFICATaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curFICATaxWH;
		// vbPorter upgrade warning: curEmployerFicaTax As Decimal	OnWrite(int, Decimal)
		Decimal curEmployerFicaTax;
		// vbPorter upgrade warning: curMedicareTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curMedicareTaxWH;
		// vbPorter upgrade warning: curEmployerMedicareTax As Decimal	OnWrite(int, Decimal)
		Decimal curEmployerMedicareTax;
		// vbPorter upgrade warning: curStateTaxWH As Decimal	OnWrite(int, Decimal)
		Decimal curStateTaxWH;
		// vbPorter upgrade warning: curLocalTaxWH As Decimal	OnWrite(int, Decimal)
		// vbPorter upgrade warning: curDeductionsWH As Decimal	OnWrite(int, Decimal)
		Decimal curDeductionsWH;
		bool blnShowedPaidLabel;
		Decimal curDeductionMatch;
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		bool boolFICAMedSameAccount;
		bool boolSeparateAccounts;
		int intArrayID;
		// vbPorter upgrade warning: curTotalNet As Decimal	OnWrite
		Decimal curTotalNet;
		private EncumCTLType[] Encums = null;
		private double dblEncumCTL;
		// ***********************************************************
		// MATTHEW MULTI CALL ID 79810 10/27/2005
		bool pboolMuliFund;
		bool boolTaxesAgain;
		clsDRWrapper rsTaxAmountInfo = new clsDRWrapper();
		
		// ***********************************************************
		bool boolSchoolTaxes;
		string strTableToUse = "";
		// vbPorter upgrade warning: dtPayDat As DateTime	OnWrite(string, DateTime)
		DateTime dtPayDat;
		int intPayRunID;
		int lngBank;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			curTotalNet = 0;
			this.Fields.Add("SummaryTypeBinder");
			boolSchoolTaxes = false;
			modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
			modGlobalVariables.Statics.SortedAccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
			intArrayID = 0;
			if (modGlobalConstants.Statics.gboolBD)
			{
				modAccountTitle.SetAccountFormats();
			}
			lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank"))));
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				CheckRecord:
				;
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						goto CheckRecord;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				if (!boolTaxesAgain)
					rsAccountInfo.MoveNext();
				// ***********************************************************
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementSummaryType();
					if (strSummaryType != "X")
					{
						SetAccountInfoRecordset();
						eArgs.EOF = CheckRecord();
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["SummaryTypeBinder"].Value = strSummaryType;
			}
		}

		private bool CheckRecord()
		{
			if (rsAccountInfo.EndOfFile() != true)
			{
				return false;
			}
			else
			{
				IncrementSummaryType();
				if (strSummaryType != "X")
				{
					SetAccountInfoRecordset();
					return CheckRecord();
				}
				else
				{
					return true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strAry As string()	OnRead(string, DateTime)
			string[] strAry = null;
			if (modGlobalVariables.Statics.gintUseGroupMultiFund > 0)
			{
				pboolMuliFund = true;
			}
			else
			{
				pboolMuliFund = false;
			}
			if (FCConvert.ToString(this.UserData) != string.Empty)
			{
				strTableToUse = FCConvert.ToString(this.UserData);
				strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
				strTableToUse = strAry[0];
				dtPayDat = FCConvert.ToDateTime(strAry[1]);
				intPayRunID = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
				Label20.Visible = false;
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    if (fecherFoundation.Strings.UCase(strTableToUse) != "TBLTEMPPAYPROCESS")
                    {
                        rsLoad.OpenRecordset(
                            "select BANKNUMBER from tblpayrollsteps where payrunid = " +
                            FCConvert.ToString(intPayRunID) + " and paydate = '" + FCConvert.ToString(dtPayDat) + "'",
                            "twpy0000.vb1");
                        if (!rsLoad.EndOfFile())
                        {
                            if (Conversion.Val(rsLoad.Get_Fields("banknumber")) > 0)
                            {
                                lngBank = FCConvert.ToInt32(
                                    Math.Round(Conversion.Val(rsLoad.Get_Fields("banknumber"))));
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.Val(strAry[3]) > 0)
                        {
                            lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[3])));
                        }
                    }
                }
            }
			else
			{
				strTableToUse = "tblTempPayProcess";
				Label20.Visible = true;
				dtPayDat = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
			}
			strSummaryType = "G";
			strHeaderType = "G";
			blnFirstRecord = true;
			SetAccountInfoRecordset();
			curPaidTotal = 0;
			curCredits = 0;
			// dblEncumControl = 0
			curDebits = 0;
			curGrossPay = 0;
			curFederalTaxWH = 0;
			curStateTaxWH = 0;
			curFICATaxWH = 0;
			curEmployerFicaTax = 0;
			curEmployerMedicareTax = 0;
			curMedicareTaxWH = 0;
			curDeductionsWH = 0;
			dblEncumCTL = 0;
			FCUtils.EraseSafe(Encums);
			Encums = new EncumCTLType[1 + 1];
			blnShowedPaidLabel = false;
			boolSeparateAccounts = false;
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Detail.Visible = true;
			if (strSummaryType == "G")
			{
				ShowGrossPayRecord();
			}
			else if (strSummaryType == "T")
			{
				ShowTaxRecord();
			}
			else if ((strSummaryType == "M") || (strSummaryType == "SM"))
			{
				ShowEmployerMatchMedicareTaxRecord();
			}
			else if ((strSummaryType == "F") || (strSummaryType == "SF"))
			{
				ShowEmployerMatchFICATaxRecord();
			}
			else if (strSummaryType == "D")
			{
				ShowDeductionRecord();
			}
			else if (strSummaryType == "E")
			{
				ShowEmployerMatchRecord();
			}
			else if (strSummaryType == "P")
			{
				ShowTrustRecord();
			}
			else if (strSummaryType == "C")
			{
				ShowCheckingRecord();
			}

		}

		private void SetAccountInfoRecordset()
		{
			if (strSummaryType == "G")
			{
				rsAccountInfo.OpenRecordset("SELECT DistAccountNumber,boolencumbrance, SUM(DistGrossPay) as TotalPay FROM " + strTableToUse + " WHERE distributionrecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " GROUP BY DistAccountNumber,boolencumbrance HAVING SUM(DistGrossPay) <> 0 ORDER BY DistAccountNumber");
			}
			else if (strSummaryType == "T")
			{
				GetPayrollAccountsInfo();
			}
			else if (strSummaryType == "M")
			{
				rsAccountInfo.Execute("delete from tbltemptax", "Payroll");
				// SetupEmployerMatchInfo
				modGlobalRoutines.SetupTempTaxTable(dtPayDat, intPayRunID, strTableToUse);
				// If boolFICAMedSameAccount Then
				// rsAccountInfo.OpenRecordset "SELECT SUM(MedicareTax + FICATax) AS MedicareTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(MedicareTax) <> 0 ORDER BY DeptDiv"
				// Else
				rsAccountInfo.OpenRecordset("SELECT SUM(MedicareTax) AS MedicareTaxTotal,sum(employerMedicareTax) as EmployerMedicareTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING sum(MedicareTax) <> 0 or SUM(EmployerMedicareTax) <> 0 ORDER BY DeptDiv");
				// End If
			}
			else if (strSummaryType == "F")
			{
				// If Not boolFICAMedSameAccount Then
				rsAccountInfo.OpenRecordset("SELECT SUM(FICATax) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(FICATax) <> 0 or sum(employerFicaTax) <> 0 ORDER BY DeptDiv");
				// End If
				// Case "SM"
				// Call rsAccountInfo.Execute("delete from tbltemptax", "Payroll")
				// Call CreateSchoolFicaMedEntries(dtPayDat, intPayRunID, strTableToUse)
				// rsAccountInfo.OpenRecordset "SELECT SUM(MedicareTax) AS MedicareTaxTotal,sum(EmployerMedicareTax) as employermedicaretaxtotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(MedicareTax) <> 0 or sum(EmployerMedicareTax) <> 0 ORDER BY DeptDiv"
			}
			else if (strSummaryType == "SF")
			{
				rsAccountInfo.OpenRecordset("SELECT SUM(FICATax) AS FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal, DeptDiv FROM tblTempTax GROUP BY DeptDiv HAVING SUM(FICATax) <> 0 or sum(employerficatax) <> 0 ORDER BY DeptDiv");
			}
			else if (strSummaryType == "D")
			{
				rsAccountInfo.OpenRecordset("SELECT DeductionAccountNumber, SUM(DedAmount) as TotalPay, DeductionTitle FROM " + strTableToUse + " WHERE  deductionrecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " GROUP BY DeductionAccountNumber, DeductionTitle,deddeductionnumber HAVING SUM(DedAmount) <> 0 ORDER BY DeductionAccountNumber,deddeductionnumber");
			}
			else if (strSummaryType == "E")
			{
				rsAccountInfo.OpenRecordset("SELECT SUM(TotalMatchAmount) AS GrandTotalMatchAmount, MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber FROM (SELECT MatchGLAccountNumber as MatchAccountNumber, SUM(MatchAmount) * -1 as TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM " + strTableToUse + " WHERE  matchrecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchGLAccountNumber UNION ALL SELECT MatchAccountNumber, Sum(MatchAmount) AS TotalMatchAmount, MatchDeductionTitle, MatchDeductionNumber FROM " + strTableToUse + " WHERE matchrecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " GROUP BY MatchAccountNumber, MatchDeductionTitle, MatchDeductionNumber) as Temp GROUP BY MatchDeductionNumber, MatchDeductionTitle, MatchAccountNumber HAVING SUM(TOTALMATCHAMOUNT) <> 0 ORDER BY MatchAccountNumber, SUM(TotalMatchAmount)");
			}
			else if (strSummaryType == "P")
			{
				rsAccountInfo.OpenRecordset("SELECT TrustCategoryID, TrustDeductionAccountNumber, SUM(TrustAmount) as TrustAmountTotal, TrustDeductionDescription FROM " + strTableToUse + " WHERE TrustRecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " GROUP BY TrustCategoryID, TrustDeductionDescription, TrustDeductionAccountNumber HAVING SUM(TrustAmount) <> 0 ORDER BY TrustCategoryID");
			}
			else if (strSummaryType == "C")
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
			}
			else if (strSummaryType == "SC")
			{
				rsAccountInfo.OpenRecordset("select * from payrollschoolaccounts where code = 'CC'");
			}
		}

		private void IncrementSummaryType()
		{
			if (strSummaryType == "G")
			{
				strSummaryType = "T";
				strTaxType = "FT";
				// start with federal tax
			}
			else if (strSummaryType == "T")
			{
				
				strSummaryType = "M";
				
			}
			else if (strSummaryType == "M")
			{
				// If boolFICAMedSameAccount Then
				// strSummaryType = "D"
				// Else
				strSummaryType = "F";
				// End If
			}
			else if (strSummaryType == "F")
			{
                strSummaryType = "D";
            }
			else if (strSummaryType == "SM")
			{
				strSummaryType = "SF";
			}
			else if (strSummaryType == "SF")
			{
				strSummaryType = "D";
			}
			else if (strSummaryType == "D")
			{
				strSummaryType = "E";
			}
			else if (strSummaryType == "E")
			{
				strSummaryType = "P";
			}
			else if (strSummaryType == "P")
			{
				strSummaryType = "C";
			}
			else if (strSummaryType == "C")
			{
                strSummaryType = "X";
            }
			else if (strSummaryType == "SC")
			{
				strSummaryType = "X";
			}
		}

		private void ShowGrossPayRecord()
		{
			// - "AutoDim"
			fldDescription.Text = modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields_String("DistAccountNumber"));
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("DistAccountNumber"));
			fldCredits.Text = "";
			if (!rsAccountInfo.Get_Fields_Boolean("boolEncumbrance"))
			{
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")), "#,##0.00");
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")));
				curGrossPay += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")));
				Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
				intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
				modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("distaccountnumber"));
				modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
				modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")));
			}
			else
			{
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")) * -1, "#,##0.00") + " (E)";
				AddToEncumControl(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")), rsAccountInfo.Get_Fields("distaccountnumber"));
				Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
				intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
				modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("distaccountnumber"));
				modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "E";
				modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")));
			}
		}

		private void AddToEncumControl(ref double dblPay, ref string strAcct)
		{
			// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
			int intindex;
			string strFund;
			bool boolSchool;
			bool boolMatch;
			boolMatch = false;
			if (fecherFoundation.Strings.Trim(strAcct) == string.Empty)
				return;
			// If UCase(Left(strAcct, 1)) = "P" Or UCase(Left(strAcct, 1)) = "L" Then
			// boolSchool = True
			// strFund = GetSchoolFund(strAcct)
			// Else
			boolSchool = false;
			strFund = modBudgetaryAccounting.GetFund(ref strAcct);
			// End If
			// If Not IsEmpty(Encums) Then
			for (intindex = 0; intindex <= (Information.UBound(Encums, 1)); intindex++)
			{
				// If Not Encums(intIndex) Is Nothing Then
				if (Encums[intindex].strFund == strFund)
				{
					if (Encums[intindex].boolSchool && boolSchool)
					{
						boolMatch = true;
						Encums[intindex].dblTotal += dblPay;
					}
				}
				// End If
			}
			// intindex
			// End If
			if (!boolMatch)
			{
				Array.Resize(ref Encums, Information.UBound(Encums, 1) + 1 + 1);
				intindex = Information.UBound(Encums, 1);
				Encums[intindex].boolSchool = boolSchool;
				Encums[intindex].strFund = strFund;
				Encums[intindex].dblTotal = dblPay;
			}
			dblEncumCTL += dblPay;
		}

		private void ShowDeductionRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("DeductionTitle");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("DeductionAccountNumber"));
			if (Conversion.Val(rsAccountInfo.Get_Fields("totalpay")) >= 0)
			{
				fldCredits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")), "#,##0.00");
				fldDebits.Text = "";
				curCredits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")));
			}
			else
			{
				fldCredits.Text = "";
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")) * -1, "#,##0.00");
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("totalpay")) * -1);
			}
			curDeductionsWH += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TotalPay")));
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deductionaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("totalpay"))) * -1;
		}

		

		private void ShowCheckingRecord()
		{
            using (clsDRWrapper rsNetPay = new clsDRWrapper())
            {
                if (!modGlobalConstants.Statics.gboolBD)
                {
                    fldDescription.Text = "Checking";
                    fldAccount.Text = ShowAccountCorrectly(FCConvert.ToString(rsAccountInfo.Get_Fields("Account")));
                    //if (!(false && !modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms))
                    //{
                    rsNetPay.OpenRecordset("SELECT SUM(NetPay) as NetPayTotal,MultiFundNumber FROM " + strTableToUse +
                                           " WHERE TotalRecord = 1 and PayDate = '" + FCConvert.ToString(dtPayDat) +
                                           "' AND PayRunID = " + FCConvert.ToString(intPayRunID) +
                                           " Group By MultiFundNumber");
                    //}
                    //else
                    //{
                    //	rsNetPay.OpenRecordset("SELECT SUM(NetPay) as NetPayTotal,MultiFundNumber FROM " + strTableToUse + " WHERE not fromschool = 1 and TotalRecord = 1 and PayDate = '" + FCConvert.ToString(dtPayDat) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " Group By MultiFundNumber");
                    //}
                    if (rsNetPay.EndOfFile() != true && rsNetPay.BeginningOfFile() != true)
                    {
                        fldCredits.Text =
                            Strings.Format(Conversion.Val(rsNetPay.Get_Fields("NetPayTotal")) + curPaidTotal,
                                "#,##0.00");
                        curCredits += FCConvert.ToDecimal(Conversion.Val(rsNetPay.Get_Fields("NetPayTotal"))) +
                                      curPaidTotal;
                    }
                    else
                    {
                        fldCredits.Text = Strings.Format(curPaidTotal, "#,##0.00");
                        curCredits += curPaidTotal;
                    }

                    fldDebits.Text = "";
                    // ******************************************************************************
                    // MATTHEW MULTI CALL ID 79810 10/27/2005
                    string strText = "";
                    // If pboolMuliFund Then
                    if (Conversion.Val(rsNetPay.Get_Fields("multifundnumber")) > 0)
                    {
                        strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
                        // 01/03/2006 Need to format fund
                        Strings.MidSet(ref strText, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length,
                            Strings.Format(rsNetPay.Get_Fields_Int32("MultiFundNumber"),
                                Strings.StrDup(
                                    FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))),
                                    "0")));
                        fldAccount.Text = strText;
                    }

                    // ******************************************************************************
                }
                else
                {
                    GetDTDFRecords();
                }
            }
        }

		private void ShowTrustRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("TrustDeductionDescription");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("TrustDeductionAccountNumber"));
			if (Conversion.Val(rsAccountInfo.Get_Fields("trustamounttotal")) >= 0)
			{
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("TrustAmountTotal")), "#,##0.00");
				fldCredits.Text = "";
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TrustAmountTotal")));
			}
			else
			{
				fldDebits.Text = "";
				fldCredits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("trustamounttotal")) * -1, "#,##0.00");
				curCredits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("trustamounttotal")) * -1);
			}
			curPaidTotal += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("TrustAmountTotal")));
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("trustdeductionaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("trustamounttotal")));
		}

		private void ShowEmployerMatchRecord()
		{
			// - "AutoDim"
			fldDescription.Text = rsAccountInfo.Get_Fields_String("MatchDeductionTitle");
			fldAccount.Text = ShowAccountCorrectly(rsAccountInfo.Get_Fields_String("MatchAccountNumber"));
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("matchaccountnumber"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			if (rsAccountInfo.Get_Fields("GrandTotalMatchAmount") > 0)
			{
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("GrandTotalMatchAmount")), "#,##0.00");
				fldCredits.Text = "";
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("GrandTotalMatchAmount")));
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("grandtotalmatchamount")));
			}
			else
			{
				fldCredits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("GrandTotalMatchAmount")) * -1, "#,##0.00");
				fldDebits.Text = "";
				curCredits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("GrandTotalMatchAmount")) * -1);
				curDeductionMatch += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("GrandTotalMatchAmount")) * -1);
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("grandtotalmatchamount")));
			}
		}

		private void ShowTaxRecord()
		{
			// - "AutoDim"
			// rsTaxAmountInfo.OpenRecordset "SELECT SUM(FederalTaxWH) as FederalTaxTotal, SUM(StateTaxWH) as StateTaxTotal, SUM(LocalTaxWH) as LocalTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, SUM(FICATaxWH) as FICATaxTotal FROm " & strtabletouse & " WHERE TotalRecord = 1 AND PayDate = '" & dtPayDat & "' AND PayRunID = " & intpayrunid
			// ***********************************************************
			// MATTHEW MULTI CALL ID 79810 10/27/2005
			double dblTemp = 0;
			string strText = "";
			Detail.Visible = true;
			if (!boolTaxesAgain)
			{
				//if (!(modValidateAccount.Statics.gboolTownAccounts && modValidateAccount.Statics.gboolSchoolAccounts && !modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms))
				//{
					rsTaxAmountInfo.OpenRecordset("SELECT Sum(" + strTableToUse + ".FederalTaxWH) AS FederalTaxTotal, Sum(" + strTableToUse + ".StateTaxWH) AS StateTaxTotal, Sum(" + strTableToUse + ".LocalTaxWH) AS LocalTaxTotal, Sum(" + strTableToUse + ".MedicareTaxWH) AS MedicareTaxTotal, Sum(" + strTableToUse + ".FICATaxWH) AS FICATaxTotal, Sum(" + strTableToUse + ".EmployerFicaTax) AS EmployerFICATaxTotal, sum(" + strTableToUse + ".EmployerMedicareTax) as EmployerMedicareTaxTotal, " + strTableToUse + ".MultiFundNumber From " + strTableToUse + " Where (((" + strTableToUse + ".TotalRecord) = 1) And ((" + strTableToUse + ".PayDate) = '" + FCConvert.ToString(dtPayDat) + "') And ((" + strTableToUse + ".PayRunID) = " + FCConvert.ToString(intPayRunID) + ")) GROUP BY " + strTableToUse + ".MultiFundNumber");
					if (rsTaxAmountInfo.RecordCount() > 1)
					{
						boolTaxesAgain = true;
					}
					else
					{
						boolTaxesAgain = false;
					}
				//}
				//else
				//{
				//	rsTaxAmountInfo.OpenRecordset("SELECT Sum(" + strTableToUse + ".FederalTaxWH) AS FederalTaxTotal, Sum(" + strTableToUse + ".StateTaxWH) AS StateTaxTotal, Sum(" + strTableToUse + ".LocalTaxWH) AS LocalTaxTotal, Sum(" + strTableToUse + ".MedicareTaxWH) AS MedicareTaxTotal, Sum(" + strTableToUse + ".FICATaxWH) AS FICATaxTotal, Sum(" + strTableToUse + ".EmployerFicaTax) AS EmployerFICATaxTotal, sum(" + strTableToUse + ".EmployerMedicareTax) as EmployerMedicareTaxTotal, " + strTableToUse + ".MultiFundNumber From " + strTableToUse + " Where not fromschool = 1 and (((" + strTableToUse + ".TotalRecord) = 1) And ((" + strTableToUse + ".PayDate) = '" + FCConvert.ToString(dtPayDat) + "') And ((" + strTableToUse + ".PayRunID) = " + FCConvert.ToString(intPayRunID) + ")) GROUP BY " + strTableToUse + ".MultiFundNumber");
				//	rsSchoolTaxAmountInfo.OpenRecordset("SELECT Sum(" + strTableToUse + ".FederalTaxWH) AS FederalTaxTotal, Sum(" + strTableToUse + ".StateTaxWH) AS StateTaxTotal, Sum(" + strTableToUse + ".LocalTaxWH) AS LocalTaxTotal, Sum(" + strTableToUse + ".MedicareTaxWH) AS MedicareTaxTotal, Sum(" + strTableToUse + ".FICATaxWH) AS FICATaxTotal, Sum(" + strTableToUse + ".EmployerFicaTax) AS EmployerFICATaxTotal, " + strTableToUse + ".MultiFundNumber From " + strTableToUse + " Where fromschool = 1 and (((" + strTableToUse + ".TotalRecord) = 1) And ((" + strTableToUse + ".PayDate) = '" + FCConvert.ToString(dtPayDat) + "') And ((" + strTableToUse + ".PayRunID) = " + FCConvert.ToString(intPayRunID) + ")) GROUP BY " + strTableToUse + ".MultiFundNumber");
				//	if (!rsTaxAmountInfo.EndOfFile() && !rsSchoolTaxAmountInfo.EndOfFile())
				//	{
				//		boolTaxesAgain = true;
				//	}
				//	if (rsTaxAmountInfo.EndOfFile())
				//		boolSchoolTaxes = true;
				//}
			}
			// ***********************************************************
			double curAmt = 0;
			if (!boolSchoolTaxes)
			{
				string vbPorterVar = rsAccountInfo.Get_Fields("Code");
				// Select Case strTaxType
				if (vbPorterVar == "FT")
				{
					fldDescription.Text = "Fed Tax W/H";
					strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					// If gintUseGroupMultiFund <> 0 Then
					// Mid(strText, 3, Mid(Ledger, 1, 2)) = Format(gintUseGroupMultiFund, String(Val(Left(Ledger, 2)), "0"))
					// End If
					fldAccount.Text = ShowAccountCorrectly(strText);
					fldCredits.Text = Strings.Format(Conversion.Val(rsTaxAmountInfo.Get_Fields("FederalTaxTotal")), "#,##0.00");
					fldDebits.Text = "";
					curCredits += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("FederalTaxTotal")));
					curAmt = Conversion.Val(rsTaxAmountInfo.Get_Fields("federaltaxtotal"));
					curFederalTaxWH += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("FederalTaxTotal")));
				}
				else if (vbPorterVar == "ST")
				{
					fldDescription.Text = "State Tax W/H";
					strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					// If gintUseGroupMultiFund <> 0 Then
					// Mid(strText, 3, Mid(Ledger, 1, 2)) = Format(gintUseGroupMultiFund, String(Val(Left(Ledger, 2)), "0"))
					// End If
					fldAccount.Text = ShowAccountCorrectly(strText);
					fldCredits.Text = Strings.Format(Conversion.Val(rsTaxAmountInfo.Get_Fields("StateTaxTotal")), "#,##0.00");
					fldDebits.Text = "";
					curAmt = Conversion.Val(rsTaxAmountInfo.Get_Fields("statetaxtotal"));
					curCredits += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("StateTaxTotal")));
					curStateTaxWH += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("StateTaxTotal")));
				}
				else if (vbPorterVar == "MW")
				{
					fldDescription.Text = "Medicare W/H";
					strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					// If gintUseGroupMultiFund <> 0 Then
					// Mid(strText, 3, Mid(Ledger, 1, 2)) = Format(gintUseGroupMultiFund, String(Val(Left(Ledger, 2)), "0"))
					// End If
					fldAccount.Text = ShowAccountCorrectly(strText);
					dblTemp = Conversion.Val(rsTaxAmountInfo.Get_Fields("medicaretaxtotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EMployerMedicareTaxTotal"));
					if (dblTemp >= 0)
					{
						fldCredits.Text = Strings.Format(Conversion.Val(rsTaxAmountInfo.Get_Fields("MedicareTaxTotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerMedicareTaxTotal")), "#,##0.00");
						fldDebits.Text = "";
						curAmt = Conversion.Val(rsTaxAmountInfo.Get_Fields("medicaretaxtotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerMedicareTaxTotal"));
						curCredits += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("MedicareTaxTotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerMedicareTaxTotal")));
					}
					else
					{
						fldCredits.Text = "";
						fldDebits.Text = Strings.Format(dblTemp * -1, "#,##0.00");
						curAmt = dblTemp;
						curDebits += FCConvert.ToDecimal(-1 * dblTemp);
					}
					curMedicareTaxWH += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("MedicareTaxTotal")));
					curEmployerMedicareTax += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerMedicareTaxTotal")));
				}
				else if (vbPorterVar == "FW")
				{
					fldDescription.Text = "Soc Sec W/H";
					strText = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					// If gintUseGroupMultiFund <> 0 Then
					// Mid(strText, 3, Mid(Ledger, 1, 2)) = Format(gintUseGroupMultiFund, String(Val(Left(Ledger, 2)), "0"))
					// End If
					fldAccount.Text = ShowAccountCorrectly(strText);
					dblTemp = Conversion.Val(rsTaxAmountInfo.Get_Fields("ficataxtotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerFicaTaxTotal"));
					if (dblTemp >= 0)
					{
						// fldCredits = Format(Val(rsTaxAmountInfo.Fields("FICATaxTotal")) * 2, "#,##0.00")
						fldCredits.Text = Strings.Format(Conversion.Val(rsTaxAmountInfo.Get_Fields("FICATaxTotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerFicaTaxTotal")), "#,##0.00");
						fldDebits.Text = "";
						curAmt = Conversion.Val(rsTaxAmountInfo.Get_Fields("ficataxtotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerFicaTaxTotal"));
						curCredits += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("FICATaxTotal")) + Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerFicaTaxTotal")));
					}
					else
					{
						fldCredits.Text = "";
						fldDebits.Text = Strings.Format(dblTemp * -1, "#,##0.00");
						curAmt = dblTemp;
						curDebits += FCConvert.ToDecimal(-1 * dblTemp);
					}
					curFICATaxWH += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("FICATaxTotal")));
					curEmployerFicaTax += FCConvert.ToDecimal(Conversion.Val(rsTaxAmountInfo.Get_Fields("EmployerFicaTaxTotal")));
				}
				// If pboolMuliFund Then
				if (rsTaxAmountInfo.Get_Fields_Int32("MultiFundNumber") > 0)
				{
					strText = fldAccount.Text;
					// corey 01/03/2006 Need to format fund
					if (strText.Length > 3)
						Strings.MidSet(ref strText, 3, Strings.Left(modAccountTitle.Statics.Ledger, 2).Length, Strings.Format(rsTaxAmountInfo.Get_Fields_Int32("MultiFundNumber"), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
					fldAccount.Text = strText;
				}
				Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
				intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
				modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("account"));
				if (strText != string.Empty)
				{
					modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strText;
				}
				modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
				modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(curAmt) * -1;
			}


				if (!rsTaxAmountInfo.EndOfFile())
				{
					rsTaxAmountInfo.MoveNext();
					if (rsTaxAmountInfo.EndOfFile())
					{
						//if (!(modValidateAccount.Statics.gboolTownAccounts && modValidateAccount.Statics.gboolSchoolAccounts && !modCoreysSweeterCode.Statics.gboolUsesDueToDueFroms))
						//{
							boolTaxesAgain = false;
						//}
						//else
						//{
						//	boolTaxesAgain = true;
						//	boolSchoolTaxes = true;
						//}
					}
					else
					{
						boolTaxesAgain = true;
					}
				}

			
			// ***********************************************************
		}

		private void ShowEmployerMatchMedicareTaxRecord()
		{
			// - "AutoDim"
			// fldDescription = ReturnAccountDescription(rsAccountInfo.Fields("DeptDiv"))
			double dblTemp;
			fldDescription.Text = "Medicare Employer's Match";
			fldAccount.Text = ShowAccountCorrectly(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
			dblTemp = Conversion.Val(rsAccountInfo.Get_Fields("employermedicaretaxtotal"));
			if (dblTemp >= 0)
			{
				fldCredits.Text = "";
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("employerMedicareTaxTotal")), "#,##0.00");
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("employerMedicareTaxTotal")));
			}
			else
			{
				fldDebits.Text = "";
				fldCredits.Text = Strings.Format(dblTemp * -1, "#,##0.00");
				curCredits -= FCConvert.ToDecimal(dblTemp);
			}
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deptdiv"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("employermedicaretaxtotal")));
		}

		private void ShowEmployerMatchFICATaxRecord()
		{
			// - "AutoDim"
			double dblTemp;
			// If boolFICAMedSameAccount Then
			// Else
			// fldDescription = ReturnAccountDescription(rsAccountInfo.Fields("DeptDiv"))
			fldDescription.Text = "FICA Employer's Match";
			fldAccount.Text = ShowAccountCorrectly(FCConvert.ToString(rsAccountInfo.Get_Fields("DeptDiv")));
			// dblTemp = Val(rsAccountInfo.Fields("ficataxtotal"))
			dblTemp = Conversion.Val(rsAccountInfo.Get_Fields("EmployerFicaTaxTotal"));
			if (dblTemp >= 0)
			{
				fldCredits.Text = "";
				// fldDebits = Format(Val(rsAccountInfo.Fields("FICATaxTotal")), "#,##0.00")
				fldDebits.Text = Strings.Format(Conversion.Val(rsAccountInfo.Get_Fields("employerficataxtotal")), "#,##0.00");
				// curDebits = curDebits + Val(rsAccountInfo.Fields("FICATaxTotal"))
				curDebits += FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("employerficataxtotal")));
			}
			else
			{
				fldDebits.Text = "";
				fldCredits.Text = Strings.Format(dblTemp, "#,##0.00");
				curCredits -= FCConvert.ToDecimal(dblTemp);
			}
			Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
			intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
			modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
			modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rsAccountInfo.Get_Fields("deptdiv"));
			modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
			// AccountStrut(intArrayID).Amount = Val(rsAccountInfo.Fields("ficataxtotal"))
			modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Conversion.Val(rsAccountInfo.Get_Fields("employerficataxtotal")));
			// End If
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTotalDebits.Text = Strings.Format(curDebits + FCConvert.ToDecimal(dblEncumCTL), "#,##0.00");
			fldTotalCredits.Text = Strings.Format(curCredits, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (strSummaryType == "G")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Gross Pay" + Strings.StrDup(21, "-");
			}
			else if (strSummaryType == "T")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Taxes" + Strings.StrDup(25, "-");
			}
			else if (strSummaryType == "M")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "F")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "SM")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "SF")
			{
				lblTitle.Visible = false;
				lblTitle.Text = "";
			}
			else if (strSummaryType == "D")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Deductions" + Strings.StrDup(20, "-");
			}
			else if (strSummaryType == "E")
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Employer Match" + Strings.StrDup(16, "-");
			}
			else if (strSummaryType == "C")
			{
				if (blnShowedPaidLabel)
				{
					lblTitle.Visible = false;
					lblTitle.Text = "";
				}
				else
				{
					lblTitle.Visible = true;
					lblTitle.Text = "Paid" + Strings.StrDup(26, "-");
					blnShowedPaidLabel = true;
				}
			}
			else
			{
				lblTitle.Visible = true;
				lblTitle.Text = "Paid" + Strings.StrDup(26, "-");
				blnShowedPaidLabel = true;
			}
			strHeaderType = strSummaryType;
		}

		private void GetPayrollAccountsInfo()
		{
            using (clsDRWrapper rsAmountInfo = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "";

                rsAmountInfo.OpenRecordset(
                    "SELECT SUM(FederalTaxWH) as FederalTaxTotal, SUM(StateTaxWH) as StateTaxTotal, SUM(MedicareTaxWH) as MedicareTaxTotal, SUM(FICATaxWH) as FICATaxTotal,sum(EmployerFicaTax) as EmployerFicaTaxTotal,sum(EmployerMedicareTax) as EmployerMedicareTaxTotal FROm " +
                    strTableToUse + " WHERE TotalRecord = 1 AND PayDate = '" + FCConvert.ToString(dtPayDat) +
                    "' AND PayRunID = " + FCConvert.ToString(intPayRunID));
                if (Conversion.Val(rsAmountInfo.Get_Fields("FederalTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'FT' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("StateTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'ST' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("MedicareTaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'MW' ";
                }

                if (Conversion.Val(rsAmountInfo.Get_Fields("FICATaxTotal")) != 0)
                {
                    strSQL += "OR Code = 'FW' ";
                }

                // MATT 3/22/2005
                // CALL ID 65616
                if (fecherFoundation.Strings.Trim(strSQL).Length > 3)
                {
                    strSQL = Strings.Right(strSQL, strSQL.Length - 3);
                    rsAccountInfo.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE " + strSQL + "ORDER BY ID");
                }
            }
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// Dim rsJournalNumber As New clsDRWrapper
			fldGrossPay.Text = Strings.Format(curGrossPay + FCConvert.ToDecimal(dblEncumCTL), "#,##0.00");
			fldFederalTaxWH.Text = Strings.Format(curFederalTaxWH, "#,##0.00");
			fldStateTaxWH.Text = Strings.Format(curStateTaxWH, "#,##0.00");
			fldFICATaxWH.Text = Strings.Format(curFICATaxWH, "#,##0.00");
			fldMedicareTaxWH.Text = Strings.Format(curMedicareTaxWH, "#,##0.00");
			fldDeductionsWH.Text = Strings.Format(curDeductionsWH, "#,##0.00");
			fldNetPay.Text = Strings.Format((curGrossPay + FCConvert.ToDecimal(dblEncumCTL)) - curFederalTaxWH - curStateTaxWH - curFICATaxWH - curMedicareTaxWH - curDeductionsWH , "#,##0.00");
			fldFICAMatch.Text = Strings.Format(curEmployerFicaTax, "#,##0.00");
			fldMedicareMatch.Text = Strings.Format(curEmployerMedicareTax, "#,##0.00");
			fldDeductionMatch.Text = Strings.Format(curDeductionMatch, "#,##0.00");
		}
		
		private string ShowAccountCorrectly(string strAccount)
		{
			string ShowAccountCorrectly = "";
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			if (!modValidateAccount.AccountValidate(strAccount))
			{
				ShowAccountCorrectly = "**" + strAccount;
				rptPayrollAccountingChargesPreview.InstancePtr.blnShowBadAccountLabel = true;
			}
			else
			{
				ShowAccountCorrectly = strAccount;
			}
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
			return ShowAccountCorrectly;
		}

		private void GetDTDFRecords()
		{
            using (clsDRWrapper rspayrollaccounts = new clsDRWrapper())
            {

                double dblValue;
                string strCashAcct;
                string strSchoolCashAcct;
                string strTownEncAccount;
                string strSchoolEncAccount;
                dblValue = 0;
                strTownEncAccount = "";
                strSchoolEncAccount = "";
                rspayrollaccounts.OpenRecordset("select * from standardaccounts where CODE = 'EO'", "twbd0000.vb1");
                if (!rspayrollaccounts.EndOfFile())
                {
                    strTownEncAccount = FCConvert.ToString(rspayrollaccounts.Get_Fields("account"));
                }

                rspayrollaccounts.OpenRecordset("SELECT * FROM tblPayrollAccounts WHERE Code = 'CC'");
                // rsPayrollAccounts.FindFirstRecord "Description", "'Cash / Checking'"
                strCashAcct = "";
                strSchoolCashAcct = "";
                if (!rspayrollaccounts.EndOfFile())
                {
                    strCashAcct = FCConvert.ToString(rspayrollaccounts.Get_Fields("account"));
                }

                intArrayID = Information.UBound(modGlobalVariables.Statics.AccountStrut, 1);
                if (modGlobalConstants.Statics.gboolBD)
                {
                    // this sets up the cash accounts for the funds if the user has BD
                    modGlobalVariables.Statics.ftFundArray =
                        modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
                    // ftSchoolFundArray = CalcFundCash(AccountStrut(), True)
                }
                else
                {
                    // create a cash entry into the account M CASH for these accounts
                    modGlobalVariables.Statics.ftFundArray =
                        modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
                }

                int lngTownCashIndex = 0;
                int lngSchoolCashIndex;
                int lngCurrCashIndex;
                int lngTempIndex;
                string strCurrAcct = "";
                string strTempAcct = "";
                int lngSwapIndex;
                string strAcctOR = "";
                strAcctOR = "";
                if (modGlobalConstants.Statics.gboolBD)
                {
                    strAcctOR = modBudgetaryAccounting.GetBankOverride(ref lngBank, true);
                }

                if (strAcctOR != string.Empty)
                {
                    // had default cash account that may need to be split
                    modBudgetaryAccounting.CalcCashFundsFromOverride(ref strAcctOR,
                        ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false,
                        Strings.Format(dtPayDat, "MM/dd/yyyy") + " PY", "PY", false);
                }
                else
                {
                    if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut,
                        ref modGlobalVariables.Statics.ftFundArray, false,
                        Strings.Format(dtPayDat, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false,
                        lngTownCashIndex, true))
                    {
                    }
                }

                int x;
                string strDesc = "";
                string strCred = "";
                string strDeb = "";
                string strAcct = "";
                // If Not Encums Is Nothing Then
                for (x = 0; x <= Information.UBound(Encums, 1); x++)
                {
                    // If Not Encums(x) Is Nothing Then
                    if (Encums[x].dblTotal != 0)
                    {
                        strDesc += "Encumbrance CTL" + "\r\n";
                        strCred += "\r\n";
                        strDeb += Strings.Format(Encums[x].dblTotal, "#,###,##0.00") + "\r\n";
                        if (Encums[x].boolSchool)
                        {
                            strAcct += "L " + Encums[x].strFund + "-" + strSchoolEncAccount + "-00";
                        }
                        else
                        {
                            strAcct += "E " + Encums[x].strFund + "-" + strTownEncAccount + "-00";
                        }

                        strAcct += "\r\n";
                    }

                    // End If
                }

                // x
                // End If
                if (Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) > intArrayID)
                {
                    for (x = intArrayID + 1; x <= Information.UBound(modGlobalVariables.Statics.AccountStrut, 1); x++)
                    {
                        strDesc +=
                            fecherFoundation.Strings.Trim(modGlobalVariables.Statics.AccountStrut[x].Description) +
                            "\r\n";
                        if (Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount) < 0)
                        {
                            strCred += Strings.Format(
                                Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount) * -1,
                                "#,###,##0.00") + "\r\n";
                            curCredits +=
                                FCConvert.ToDecimal(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount)) *
                                -1;
                            strDeb += "\r\n";
                        }
                        else
                        {
                            strCred += "\r\n";
                            strDeb += Strings.Format(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount),
                                "#,###,##0.00") + "\r\n";
                            curDebits +=
                                FCConvert.ToDecimal(Conversion.Val(modGlobalVariables.Statics.AccountStrut[x].Amount));
                        }

                        strAcct += ShowAccountCorrectly(modGlobalVariables.Statics.AccountStrut[x]._account) + "\r\n";
                    }

                    // x
                }

                fldAccount.Text = strAcct;
                fldDescription.Text = strDesc;
                fldDebits.Text = strDeb;
                fldCredits.Text = strCred;
            }
        }
	}
}
