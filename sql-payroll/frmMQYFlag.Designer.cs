//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmMQYFlag.
	/// </summary>
	partial class frmMQYFlag
	{
		public fecherFoundation.FCComboBox cmbPer;
		public fecherFoundation.FCLabel lblPer;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbPer = new fecherFoundation.FCComboBox();
			this.lblPer = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 171);
			this.BottomPanel.Size = new System.Drawing.Size(485, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdContinue);
			this.ClientArea.Controls.Add(this.cmbPer);
			this.ClientArea.Controls.Add(this.lblPer);
			this.ClientArea.Location = new System.Drawing.Point(0, 0);
			this.ClientArea.Size = new System.Drawing.Size(485, 171);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(485, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbPer
			// 
			this.cmbPer.AutoSize = false;
			this.cmbPer.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPer.FormattingEnabled = true;
			this.cmbPer.Items.AddRange(new object[] {
            "Weekly",
            "Monthly",
            "Quarterly",
            "Yearly",
            "Individual"});
			this.cmbPer.Location = new System.Drawing.Point(294, 30);
			this.cmbPer.Name = "cmbPer";
			this.cmbPer.Size = new System.Drawing.Size(171, 40);
			this.cmbPer.TabIndex = 0;
			// 
			// lblPer
			// 
			this.lblPer.AutoSize = true;
			this.lblPer.Location = new System.Drawing.Point(30, 44);
			this.lblPer.Name = "lblPer";
			this.lblPer.Size = new System.Drawing.Size(265, 16);
			this.lblPer.TabIndex = 1;
			this.lblPer.Text = "WHICH PERIOD ARE YOU PROCESSING";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContinue,
            this.mnuSepar,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(30, 90);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(116, 48);
			this.cmdContinue.TabIndex = 2;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmMQYFlag
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(485, 171);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmMQYFlag";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Period Selection";
			this.Load += new System.EventHandler(this.frmMQYFlag_Load);
			this.Activated += new System.EventHandler(this.frmMQYFlag_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMQYFlag_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMQYFlag_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdContinue;
	}
}