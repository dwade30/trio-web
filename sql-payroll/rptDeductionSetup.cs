//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDeductionSetup.
	/// </summary>
	public partial class rptDeductionSetup : BaseSectionReport
	{
		public rptDeductionSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Deduction Setup";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeductionSetup InstancePtr
		{
			get
			{
				return (rptDeductionSetup)Sys.GetInstance(typeof(rptDeductionSetup));
			}
		}

		protected rptDeductionSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeductionSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 26,2001
		//
		// NOTES: One visible problem with this code is that it references
		// the form frmDeductionSetup directly. If this report is called from
		// any other form then this one then the data will not display correctly.
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter == frmDeductionSetup.InstancePtr.vsDeductions.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				txtLine.Text = frmDeductionSetup.InstancePtr.vsDeductions.TextMatrix(intCounter, 1);
				txtDescription.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
				txtFrequencyCode.Text = Strings.Left(FCConvert.ToString(frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)), 1);
				txtTaxStatusCode.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3);
				txtAmount.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4);
				if (FCConvert.ToString(frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) == "Percent")
				{
					txtAmountType.Text = "%";
				}
				else
				{
					txtAmountType.Text = "$";
				}
				txtLimit.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 8);
				txtLimitCode.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 9);
				txtAccount.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10);
				txtAcctDescription.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 11);
				txtStatus.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 12);
				txtPriority.Text = frmDeductionSetup.InstancePtr.vsDeductions.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 13);
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Deduction Setup ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (this.Document.Printer != null)
				{
					//this.Document.Printer.PrintQuality = ddPQMedium;
				}
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Payroll Deduction Setup Report";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			// lblPage.Caption = "Page " & intpage
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}