//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsScheduleList
	{
		//=========================================================
		private clsSchedule[] SchedList = null;
		private int intCurrentSchedule;

		public bool AdvanceWeekRotations()
		{
			bool AdvanceWeekRotations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn;
				boolReturn = true;
				AdvanceWeekRotations = false;
				MoveFirst();
				while (!(GetCurrentIndex() < 0))
				{
					if (!(SchedList[intCurrentSchedule] == null))
					{
						boolReturn = boolReturn && SchedList[intCurrentSchedule].AdvanceWeekRotations();
					}
					MoveNext();
				}
				AdvanceWeekRotations = boolReturn;
				return AdvanceWeekRotations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AdvanceWeekRotations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AdvanceWeekRotations;
		}

		public bool ReverseWeekRotations()
		{
			bool ReverseWeekRotations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolReturn;
				boolReturn = true;
				ReverseWeekRotations = false;
				MoveFirst();
				while (!(GetCurrentIndex() < 0))
				{
					if (!(SchedList[intCurrentSchedule] == null))
					{
						boolReturn = boolReturn && SchedList[intCurrentSchedule].ReverseWeekRotations();
					}
					MoveNext();
				}
				ReverseWeekRotations = boolReturn;
				return ReverseWeekRotations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReverseWeekRotations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ReverseWeekRotations;
		}

		public int InsertSchedule(ref clsSchedule tSchedule)
		{
			int InsertSchedule = 0;
			int intReturn;
			// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
			int intindex;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				intReturn = -1;
				InsertSchedule = -1;
				intindex = -1;
				if (!FCUtils.IsEmpty(SchedList))
				{
					intindex = Information.UBound(SchedList, 1);
					if (intindex < 0)
					{
						intindex = 0;
					}
					Array.Resize(ref SchedList, intindex + 1 + 1);
					intindex = Information.UBound(SchedList, 1);
				}
				else
				{
					SchedList = new clsSchedule[1 + 1];
					intindex = 0;
				}
				SchedList[intindex] = tSchedule;
				intReturn = intindex;
				intCurrentSchedule = intindex;
				InsertSchedule = intReturn;
				return InsertSchedule;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return InsertSchedule;
		}

		public int AddSchedule(string strName, modSchedule.SchedulePaidOption intPaidOption, int lngID = 0)
		{
			int AddSchedule = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSchedule tSchedule = new clsSchedule();
				int intReturn;
				intReturn = -1;
				AddSchedule = intReturn;
				tSchedule.ScheduleName = strName;
				tSchedule.ScheduleID = lngID;
				tSchedule.PaidOption = intPaidOption;
				tSchedule.AddWeek(1, "1");
				tSchedule.Unused = false;
				tSchedule.MoveFirst();
				intReturn = InsertSchedule(ref tSchedule);
				AddSchedule = intReturn;
				return AddSchedule;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddSchedule;
		}

		public clsSchedule GetCurrentSchedule()
		{
			clsSchedule GetCurrentSchedule = null;
			clsSchedule tSchedule;
			tSchedule = null;
			if (!FCUtils.IsEmpty(SchedList))
			{
				if (intCurrentSchedule >= 0)
				{
					if (!(SchedList[intCurrentSchedule] == null))
					{
						if (!SchedList[intCurrentSchedule].Unused)
						{
							tSchedule = SchedList[intCurrentSchedule];
						}
					}
				}
			}
			GetCurrentSchedule = tSchedule;
			return GetCurrentSchedule;
		}

		public clsSchedule GetScheduleByIndex(ref int intindex)
		{
			clsSchedule GetScheduleByIndex = null;
			clsSchedule tSchedule;
			tSchedule = null;
			if (!FCUtils.IsEmpty(SchedList) && intindex >= 0)
			{
				if (!(SchedList[intindex] == null))
				{
					if (!SchedList[intindex].Unused)
					{
						intCurrentSchedule = intindex;
						tSchedule = SchedList[intindex];
					}
				}
			}
			GetScheduleByIndex = tSchedule;
			return GetScheduleByIndex;
		}

		public clsSchedule GetScheduleById(ref int lngID)
		{
			clsSchedule GetScheduleById = null;
			clsSchedule tSchedule;
			tSchedule = null;
			if (!FCUtils.IsEmpty(SchedList) && lngID > 0)
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(SchedList, 1)); X++)
				{
					if (!(SchedList[X] == null))
					{
						if (!SchedList[X].Unused)
						{
							if (SchedList[X].ScheduleID == lngID)
							{
								intCurrentSchedule = X;
								tSchedule = SchedList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetScheduleById = tSchedule;
			return GetScheduleById;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentSchedule;
			return GetCurrentIndex;
		}

		public void MoveFirst()
		{
			if (!FCUtils.IsEmpty(SchedList))
			{
				if (Information.UBound(SchedList, 1) >= 0)
				{
					intCurrentSchedule = -1;
					MoveNext();
				}
				else
				{
					intCurrentSchedule = -1;
				}
			}
			else
			{
				intCurrentSchedule = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (!FCUtils.IsEmpty(SchedList))
			{
				// intCurrentWeek = intCurrentWeek + 1
				if (intCurrentSchedule > Information.UBound(SchedList, 1))
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentSchedule <= Information.UBound(SchedList, 1))
					{
						intCurrentSchedule += 1;
						if (intCurrentSchedule > Information.UBound(SchedList, 1))
						{
							intReturn = -1;
							break;
						}
						else if (SchedList[intCurrentSchedule] == null)
						{
						}
						else if (SchedList[intCurrentSchedule].Unused)
						{
						}
						else
						{
							intReturn = intCurrentSchedule;
							break;
						}
					}
				}
				intCurrentSchedule = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentSchedule = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool LoadSchedules()
		{
			bool LoadSchedules = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadSchedules = false;
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsSchedule tSchedule;
				FCUtils.EraseSafe(SchedList);
				intCurrentSchedule = -1;
				strSQL = "Select * from schedules order by name";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					tSchedule = new clsSchedule();
					tSchedule.PaidOption = (modSchedule.SchedulePaidOption)rsLoad.Get_Fields_Int16("PaidOption");
					tSchedule.ScheduleID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("id"))));
					tSchedule.ScheduleName = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					tSchedule.LoadSchedule(true);
					tSchedule.MoveFirst();
					if (tSchedule.GetCurrentIndex < 0)
					{
						// add a week
						tSchedule.AddWeek(1, "1");
					}
					InsertSchedule(ref tSchedule);
					rsLoad.MoveNext();
				}
				LoadSchedules = true;
				return LoadSchedules;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadSchedules", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadSchedules;
		}

		public bool SaveSchedules()
		{
			bool SaveSchedules = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveSchedules = false;
				bool boolReturn;
				boolReturn = true;
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				if (!FCUtils.IsEmpty(SchedList))
				{
					for (X = 0; X <= (Information.UBound(SchedList, 1)); X++)
					{
						if (!(SchedList[X] == null))
						{
							boolReturn = boolReturn && SchedList[X].SaveSchedule(true);
						}
					}
					// X
				}
				SaveSchedules = boolReturn;
				return SaveSchedules;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveSchedules", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSchedules;
		}
	}
}
