//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Part4.
	/// </summary>
	public partial class srptC1Part4 : FCSectionReport
	{
		public srptC1Part4()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1Part4 InstancePtr
		{
			get
			{
				return (srptC1Part4)Sys.GetInstance(typeof(srptC1Part4));
			}
		}

		protected srptC1Part4 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsC1?.Dispose();
				clsMonth1?.Dispose();
				clsMonth2?.Dispose();
				clsMonth3?.Dispose();
                clsC1 = null;
                clsMonth1 = null;
                clsMonth2 = null;
                clsMonth3 = null;
				rsUnemployment?.Dispose();
                rsUnemployment = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1Part4	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsMonth1 = new clsDRWrapper();
		clsDRWrapper clsMonth2 = new clsDRWrapper();
		clsDRWrapper clsMonth3 = new clsDRWrapper();
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		clsDRWrapper clsC1 = new clsDRWrapper();
		bool boolLastPage;
		double dblTotalWages;
		double dblTotalWH;
		double dblTotalExcess;
		int lngM1Workers;
		int lngM2Workers;
		int lngM3Workers;
		int lngM1Females;
		int lngM2Females;
		int lngM3Females;
		double dblLimit;
		clsDRWrapper rsUnemployment = new clsDRWrapper();
		int intpage;
		bool boolTestPrint;
		string strQTDWhere = "";
		string strYTDWhere = "";
		int intNumRecs;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear;
			strYear = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
			eArgs.EOF = clsC1.EndOfFile();
			if (!eArgs.EOF)
			{
				Barcode1.Text = Strings.Right(strYear, 2) + "0850" + Strings.Format(intpage, "0");
				if (intpage > 9)
					Barcode1.Left = 8040 / 1440F;
				intpage += 1;
			}
			else if (intpage == 2 && boolTestPrint)
			{
				eArgs.EOF = false;
				intpage += 1;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			int intQuarterCovered;
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear = "";
			boolTestPrint = modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest;
			intpage = 2;
			boolLastPage = false;
			lngM1Workers = 0;
			lngM2Workers = 0;
			lngM3Workers = 0;
			lngM1Females = 0;
			lngM2Females = 0;
			lngM3Females = 0;
			dblTotalExcess = 0;
			dblTotalWages = 0;
			dblTotalWH = 0;
			intNumRecs = 21;
			LoadInfo();
			if (!boolTestPrint)
			{
				txtName.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerName);
				txtWithholdingAccount.Text = Strings.Mid(EWRRecord.MRSWithholdingID, 1, 2) + " " + Strings.Mid(EWRRecord.MRSWithholdingID, 3);
				txtUCAccount.Text = EWRRecord.StateUCAccount;
				intQuarterCovered = modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter;
				strYear = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
				Label9.Text = "SCHEDULE 2/C1 (Form 941/C1-ME) " + strYear;
				txtYearStart.Text = strYear;
				txtYearEnd.Text = strYear;
				dblLimit = modCoreysSweeterCode.Statics.EWRWageTotals.dblLimit;
				switch (intQuarterCovered)
				{
					case 1:
						{
							// txtPeriodStart.Text = Format("01/01/" & strYear, "mm dd yyyy")
							txtPeriodStart.Text = "01 01";
							// txtPeriodEnd.Text = Format("03/31/" & strYear, "mm dd yyyy")
							txtPeriodEnd.Text = "03 31";
							strQTDWhere = " where paydate between '01/01/" + strYear + "' and '3/31/" + strYear + "' ";
							strYTDWhere = strQTDWhere;
							break;
						}
					case 2:
						{
							// txtPeriodStart.Text = Format("04/01/" & strYear, "MM/dd/yyyy")
							// txtPeriodEnd.Text = Format("06/30/" & strYear, "MM/dd/yyyy")
							txtPeriodStart.Text = "04 01";
							txtPeriodEnd.Text = "06 30";
							strQTDWhere = " where paydate between '04/01/" + strYear + "' and '6/30/" + strYear + "' ";
							strYTDWhere = " where paydate between '01/01/" + strYear + "' and '6/30/" + strYear + "' ";
							break;
						}
					case 3:
						{
							// txtPeriodStart.Text = Format("07/01/" & strYear, "mm dd yyyy")
							// txtPeriodEnd.Text = Format("09/30/" & strYear, "mm dd yyyy")
							txtPeriodStart.Text = "07 01";
							txtPeriodEnd.Text = "09 30";
							strQTDWhere = " where paydate between '07/01/" + strYear + "' and '9/30/" + strYear + "' ";
							strYTDWhere = " where paydate between '01/01/" + strYear + "' and '9/30/" + strYear + "' ";
							break;
						}
					case 4:
						{
							// txtPeriodStart.Text = Format("10/01/" & strYear, "mm dd yyyy")
							// txtPeriodEnd.Text = Format("12/31/" & strYear, "mm dd yyyy")
							txtPeriodStart.Text = "10 01";
							txtPeriodEnd.Text = "12 31";
							strQTDWhere = " where paydate between '10/1/" + strYear + "' and '12/31/" + strYear + "' ";
							strYTDWhere = " where paydate between '01/01/" + strYear + "' and '12/31/" + strYear + "' ";
							break;
						}
				}
				//end switch
				strQTDWhere += " and deductionrecord = 1 and uexempt = 1 and checkvoid = 0 ";
				strYTDWhere += " and deductionrecord = 1 and uexempt = 1 and checkvoid = 0 ";
				// MATTHEW CALL ID 72412 7/8/2005
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) and paydate between #" & frmElectronicWageReporting.txtStartDate(0).Text & "# and #" & frmElectronicWageReporting.txtEndDate(0).Text & "# group by employeenumber"
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between #" & frmElectronicWageReporting.txtStartDate(0).Text & "# and #" & frmElectronicWageReporting.txtEndDate(0).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M1Start) + "' and '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M1End) + "' group by employeenumber";
				clsMonth1.OpenRecordset(strSQL, "twpy0000.vb1");
				// MATTHEW CALL ID 72412 7/8/2005
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) and paydate between #" & frmElectronicWageReporting.txtStartDate(1).Text & "# and #" & frmElectronicWageReporting.txtEndDate(1).Text & "# group by employeenumber"
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') AND paydate between #" & frmElectronicWageReporting.txtStartDate(1).Text & "# and #" & frmElectronicWageReporting.txtEndDate(1).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') AND paydate between '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M2Start) + "' and '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M2End) + "' group by employeenumber";
				clsMonth2.OpenRecordset(strSQL, "twpy0000.vb1");
				// MATTHEW CALL ID 72412 7/8/2005
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) and paydate between #" & frmElectronicWageReporting.txtStartDate(2).Text & "# and #" & frmElectronicWageReporting.txtEndDate(2).Text & "# group by employeenumber"
				// strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between #" & frmElectronicWageReporting.txtStartDate(2).Text & "# and #" & frmElectronicWageReporting.txtEndDate(2).Text & "# group by employeenumber"
				// corey 11/14/2005
				strSQL = "select employeenumber from tblcheckdetail where (checkvoid = 0) AND (DistU <> 'N' AND DistU <> '') and paydate between '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M3Start) + "' and '" + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.M3End) + "' group by employeenumber";
				clsMonth3.OpenRecordset(strSQL, "twpy0000.vb1");
				rsUnemployment.OpenRecordset("Select * from tblMiscUpdate Order by EmployeeNumber", "TWPY0000.vb1");
				strSQL = FCConvert.ToString(this.UserData);
				clsC1.OpenRecordset(strSQL, "twpy0000.vb1");
				// Call SetBLS3020Data(strSQL, Month(frmElectronicWageReporting.cmbStartDate(0).Text), clsMonth1, clsMonth2, clsMonth3, rsUnemployment)
			}
			else
			{
				Label9.Text = Label9.Text + " " + FCConvert.ToString(DateTime.Today.Year);
			}
		}

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.StateUCAccount = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                }
            }
        }

		private void ClearBoxes()
		{
			int X;
			for (X = 1; X <= intNumRecs; X++)
			{
				(this.Detail.Controls["txtName" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtSSN" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtWage" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(this.Detail.Controls["txtSeasonal" + X] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(this.Detail.Controls["txtWH" + X] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			// X
			txtWagePageTotal.Text = "";
			txtWHPageTotal.Text = "";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblPageWage = 0;
			double dblPageWH = 0;
			double dblWage = 0;
			double dblWH = 0;
			double dblGross = 0;
			// vbPorter upgrade warning: dblExcess As double	OnWrite(int, Decimal)
			double dblExcess = 0;
			double dblQTDDed = 0;
			double dblYTDDed = 0;
			int intCurrentLine;
			string strTemp = "";
            using (clsDRWrapper rsUDed = new clsDRWrapper())
            {
                if (!boolTestPrint)
                {
                    if (!clsC1.EndOfFile())
                    {
                        ClearBoxes();
                        // up to intnumrecs on this page
                        dblPageWage = 0;
                        dblPageWH = 0;
                        dblExcess = 0;
                        intCurrentLine = 1;
                        if (intpage > 9)
                            Barcode1.Left = 8040 / 1440F;
                        for (intCurrentLine = 1; intCurrentLine <= intNumRecs; intCurrentLine++)
                        {
                            if (clsC1.EndOfFile())
                            {
                                boolLastPage = true;
                                break;
                            }

                            strTemp = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(
                                clsC1.Get_Fields("lastname") + " " + clsC1.Get_Fields("firstname") + " " +
                                fecherFoundation.Strings.Trim(Strings.Left(clsC1.Get_Fields("Middlename") + " ", 1)) +
                                " " + clsC1.Get_Fields("desig")));
                            (Detail.Controls["txtName" + intCurrentLine] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                            strTemp = Strings.Format(clsC1.Get_Fields("ssn"), "000000000");
                            // strTemp = Mid(strTemp, 1, 3) & "-" & Mid(strTemp, 4, 2) & "-" & Mid(strTemp, 6)
                            (Detail.Controls["txtSSN" + intCurrentLine] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                            dblWage = Conversion.Val(Strings.Format(clsC1.Get_Fields("qtdGrossPay"), "0.00"));
                            dblGross = Conversion.Val(Strings.Format(clsC1.Get_Fields("ytdgrosspay"), "0.00"));
                            rsUDed.OpenRecordset(
                                "select sum(dedamount) as totded from tblcheckdetail " + strYTDWhere +
                                " and employeenumber = '" + clsC1.Get_Fields("employeenumber") + "'", "twpy0000.vb1");
                            if (!rsUDed.EndOfFile())
                            {
                                dblYTDDed = Conversion.Val(rsUDed.Get_Fields("totded"));
                            }
                            else
                            {
                                dblYTDDed = 0;
                            }

                            if (dblYTDDed > 0)
                            {
                                rsUDed.OpenRecordset(
                                    "select sum(dedamount) as totded from tblcheckdetail " + strQTDWhere +
                                    " and employeenumber = '" + clsC1.Get_Fields("employeenumber") + "'",
                                    "twpy0000.vb1");
                                if (!rsUDed.EndOfFile())
                                {
                                    dblQTDDed = Conversion.Val(rsUDed.Get_Fields("totded"));
                                }
                                else
                                {
                                    dblQTDDed = 0;
                                }
                            }
                            else
                            {
                                dblQTDDed = 0;
                            }

                            dblWage -= dblQTDDed;
                            if (dblWage < 0)
                                dblWage = 0;
                            dblGross -= dblYTDDed;
                            if (dblGross < 0)
                                dblGross = 0;
                            // IS THIS PERSON SET TO "N" ON THE MSRS UNEMPLOYMENT SCREEN MATTHEW 04/09/04
                            // If .Fields("EmployeeNumber") = "041" Then MsgBox "A"
                            if (rsUnemployment.FindFirstRecord("EmployeeNumber", clsC1.Get_Fields("EmployeeNumber")))
                            {
                                if (FCConvert.ToString(rsUnemployment.Get_Fields("Unemployment")) == "N")
                                {
                                    dblGross = 0;
                                    dblWage = 0;
                                }
                            }

                            // If .Fields("seasonal") = "S" Then MsgBox "A"
                            // ADD T
                            dblPageWage += dblWage;
                            dblExcess = FCConvert.ToDouble(modDavesSweetCode.CalculateExcess(
                                FCConvert.ToDecimal(dblGross), FCConvert.ToDecimal(dblWage),
                                FCConvert.ToDecimal(dblLimit)));
                            dblTotalExcess += dblExcess;
                            (Detail.Controls["txtWage" + intCurrentLine] as
                                    GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                Strings.Format(dblWage, "0.00");
                            // MATTHEW 6/29/2004
                            // If UCase(.Fields("seasonal")) = "N" Then
                            if (fecherFoundation.Strings.UCase(
                                FCConvert.ToString(clsC1.Get_Fields_Boolean("seasonal"))) != "S")
                            {
                                (Detail.Controls["txtSeasonal" + intCurrentLine] as
                                    GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
                            }
                            else
                            {
                                (Detail.Controls["txtSeasonal" + intCurrentLine] as
                                    GrapeCity.ActiveReports.SectionReportModel.Label).Text = "X";
                            }

                            dblWH = Conversion.Val(Strings.Format(clsC1.Get_Fields("qtdstatewh"), "0.00"));
                            dblPageWH += dblWH;
                            (Detail.Controls["txtWH" + intCurrentLine] as
                                    GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                Strings.Format(dblWH, "0.00");
                            strTemp = fecherFoundation.Strings.UCase(Strings.Left(clsC1.Get_Fields("sex") + " ", 1));
                            if (!(clsMonth1.EndOfFile() && clsMonth1.BeginningOfFile()))
                            {
                                if (clsMonth1.FindFirstRecord("employeenumber", clsC1.Get_Fields("employeenumber")))
                                {
                                    if (dblWage == 0)
                                    {
                                    }
                                    else
                                    {
                                        // Debug.Print .Fields("eMPLOYEENUMBER")
                                        lngM1Workers += 1;
                                        if (strTemp == "F")
                                            lngM1Females += 1;
                                    }
                                }
                                else
                                {
                                    clsMonth1.MoveFirst();
                                }
                            }

                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsC1.Get_Fields("employeenumber"))) ==
                                string.Empty)
                                MessageBox.Show("A");
                            if (!(clsMonth2.EndOfFile() && clsMonth2.BeginningOfFile()))
                            {
                                if (clsMonth2.FindFirstRecord("employeenumber", clsC1.Get_Fields("employeenumber")))
                                {
                                    if (dblWage == 0)
                                    {
                                    }
                                    else
                                    {
                                        // MsgBox Trim(.Fields("lastname")) & " " & .Fields("firstname")
                                        lngM2Workers += 1;
                                        if (strTemp == "F")
                                            lngM2Females += 1;
                                    }
                                }
                                else
                                {
                                    clsMonth2.MoveFirst();
                                }
                            }

                            if (!(clsMonth3.EndOfFile() && clsMonth3.BeginningOfFile()))
                            {
                                if (clsMonth3.FindFirstRecord("employeenumber", clsC1.Get_Fields("employeenumber")))
                                {
                                    if (dblWage == 0)
                                    {
                                    }
                                    else
                                    {
                                        lngM3Workers += 1;
                                        if (strTemp == "F")
                                            lngM3Females += 1;
                                    }
                                }
                                else
                                {
                                    clsMonth3.MoveFirst();
                                }
                            }

                            clsC1.MoveNext();
                        }

                        // intCurrentLine
                        // make this check in case the last record is the intNumRecsth on this page
                        if (clsC1.EndOfFile())
                            boolLastPage = true;
                        // keep track of grandtotals
                        dblTotalWages += dblPageWage;
                        dblTotalWH += dblPageWH;
                        txtWagePageTotal.Text = Strings.Format(dblPageWage, "0.00");
                        txtWHPageTotal.Text = Strings.Format(dblPageWH, "0.00");
                        if (boolLastPage)
                        {
                            // show grand totals
                            txtWageTotal.Text = Strings.Format(dblTotalWages, "0.00");
                            txtWHTotal.Text = Strings.Format(dblTotalWH, "0.00");
                            // fill totals for next subreport
                            modCoreysSweeterCode.Statics.EWRWageTotals.dblExcessWages = dblTotalExcess;
                            modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalStateWithheld = dblTotalWH;
                            modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalGrossReportableWages = dblTotalWages;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthFemales = lngM1Females;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngFirstMonthWorkers = lngM1Workers;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthFemales = lngM2Females;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngSecondMonthWorkers = lngM2Workers;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthFemales = lngM3Females;
                            modCoreysSweeterCode.Statics.EWRWageTotals.lngThirdMonthWorkers = lngM3Workers;
                        }
                        else
                        {
                            txtWHTotal.Text = "";
                            txtWageTotal.Text = "";
                        }
                    }
                }
            }
        }

		
	}
}
