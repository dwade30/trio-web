//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmW3ME : BaseForm
	{
		public frmW3ME()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_10, 9);
			this.Label1.AddControlArrayElement(Label1_11, 10);
			this.Label1.AddControlArrayElement(Label1_12, 11);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW3ME InstancePtr
		{
			get
			{
				return (frmW3ME)Sys.GetInstance(typeof(frmW3ME));
			}
		}

		protected frmW3ME _InstancePtr = null;
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		int lngYearCovered;
		// vbPorter upgrade warning: dblReported As double	OnWrite(string)
		double dblReported;
		// vbPorter upgrade warning: dblWithheld As double	OnWrite(string)
		double dblWithheld;
		// vbPorter upgrade warning: dblThirdParty As double	OnWrite(string)
		double dblThirdParty;

		private void frmW3ME_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		public void Init(int lngYCovered, bool boolElectronic = true)
		{
			lngYearCovered = lngYCovered;
			if (boolElectronic)
			{
				cmbElectronic.Text = "Electronic";
				// optElectronic(1).Enabled = False
			}
			else
			{
				if (cmbElectronic.Items.Contains("Paper"))
				{
					cmbElectronic.Text = "Paper";
				}
				else
				{
					cmbElectronic.Text = "Electronic";
				}
				// optElectronic(0).Enabled = False
			}
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmW3ME_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW3ME properties;
			//frmW3ME.ScaleWidth	= 9225;
			//frmW3ME.ScaleHeight	= 8055;
			//frmW3ME.LinkTopic	= "Form1";
			//frmW3ME.LockControls	= -1  'True;
			//End Unmaped Properties
			int intTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, true);
			LoadInfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			double dblTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select sum(statetaxwh) as tottax from tblcheckdetail where checkvoid = 0 and paydate between '01/01/" + FCConvert.ToString(lngYearCovered) + "' and '12/31/" + FCConvert.ToString(lngYearCovered) + "' and totalrecord = 1", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					txtReported.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("tottax")), "0.00");
					txtWithheld.Text = txtReported.Text;
				}
				else
				{
					txtReported.Text = "0.00";
					txtWithheld.Text = "0.00";
				}
				dblTemp = 0;
				strSQL = "select sum(cytdamount) as tot from tblw2deductions where thirdpartysave = 1";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblTemp += Conversion.Val(clsLoad.Get_Fields("tot"));
				}
				strSQL = "select sum(cytdamount) as tot from tblw2matches where thirdpartysave = 1";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblTemp += Conversion.Val(clsLoad.Get_Fields("tot"));
				}
				txtThirdPartyAmount.Text = Strings.Format(dblTemp, "0.00");
				strSQL = "select * from tblemployerinfo";
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1, modCoreysSweeterCode.EWRReturnAddressLen);
					EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1, modCoreysSweeterCode.EWRReturnCityLen);
					EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")), 1, modCoreysSweeterCode.EWRReturnNameLen);
					EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
					EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
					EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
					EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
					EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
					EWRRecord.MRSWithholdingID = FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid"));
					EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
					EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
					EWRRecord.TransmitterName = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("transmittername")));
					EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
					EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")), 1, modCoreysSweeterCode.EWRTransmitterTitleLen);
					EWRRecord.ProcessorLicenseCode = FCConvert.ToString(clsLoad.Get_Fields_String("ProcessorLicenseCode"));
					txtThirdPartyName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("ThirdPartyName"));
					txtThirdPartyID.Text = FCConvert.ToString(clsLoad.Get_Fields_String("ThirdPartyID"));
				}
				else
				{
					EWRRecord.EmployerAddress = "";
					EWRRecord.EmployerCity = "";
					EWRRecord.EmployerName = "";
					EWRRecord.EmployerState = "ME";
					EWRRecord.EmployerStateCode = 23;
					EWRRecord.EmployerZip = "";
					EWRRecord.EmployerZip4 = "";
					EWRRecord.FederalEmployerID = "";
					EWRRecord.MRSWithholdingID = "";
					EWRRecord.StateUCAccount = "";
					EWRRecord.TransmitterExt = "";
					EWRRecord.TransmitterPhone = "0000000000";
					EWRRecord.TransmitterName = "";
					EWRRecord.TransmitterTitle = "";
					txtThirdPartyName.Text = "";
					txtThirdPartyID.Text = "";
				}
				txtFilename.Text = "W3ME";
				object strPhone = "";
				txtEmployerName.Text = fecherFoundation.Strings.Trim(EWRRecord.EmployerName);
				txtExtension.Text = EWRRecord.TransmitterExt;
				txtFederalID.Text = EWRRecord.FederalEmployerID;
				txtMRSID.Text = EWRRecord.MRSWithholdingID;
				txtContactName.Text = fecherFoundation.Strings.Trim(EWRRecord.TransmitterName);
				txtContactTitle.Text = fecherFoundation.Strings.Trim(EWRRecord.TransmitterTitle);
				strPhone = EWRRecord.TransmitterPhone;
				strPhone = FCConvert.ToString(modGlobalRoutines.PadToString(Conversion.Val(strPhone), 10));
				txtPhone.Text = "(" + Strings.Mid(strPhone.ToStringES(), 1, 3) + ")" + Strings.Mid(strPhone.ToStringES(), 4, 3) + "-" + Strings.Mid(strPhone.ToStringES(), 7);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			string strTemp;
			string strFile = "";
			bool boolElectronic = false;
			bool bool1099 = false;
			if (cmbElectronic.Text == "Electronic")
			{
				boolElectronic = true;
			}
			else
			{
				boolElectronic = false;
			}
			if (boolElectronic)
			{
				strFile = txtFilename.Text;
				if (fecherFoundation.Strings.Trim(strFile) == string.Empty)
				{
					MessageBox.Show("No filename was specified", "Missing Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!(fecherFoundation.Strings.UCase(Strings.Right("   " + strFile, 4)) == ".TXT"))
				{
					if (Strings.InStr(1, strFile, ".", CompareConstants.vbTextCompare) > 0)
					{
						MessageBox.Show("Invalid file extension" + "\r\n" + "File extension must be .txt or omitted", "Invalid File Extension", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strFile += ".txt";
				}
			}
			if (Conversion.Val(txtWithheld.Text) <= 0)
			{
				MessageBox.Show("Invalid withheld amount", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Conversion.Val(txtReported.Text) <= 0)
			{
				MessageBox.Show("Invalid withheld amount", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (fecherFoundation.Strings.Trim(txtEmployerName.Text) == string.Empty)
			{
				MessageBox.Show("No employer name specified", "Missing Employer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strTemp = Strings.Replace(txtFederalID.Text, "-", "", 1, -1, CompareConstants.vbTextCompare);
			if (strTemp.Length != modCoreysSweeterCode.EWRFederalIDLen)
			{
				MessageBox.Show("Invalid federal employer ID", "Invalid Federal EIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strTemp = Strings.Replace(txtMRSID.Text, "-", "", 1, -1, CompareConstants.vbTextCompare);
			if (strTemp.Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
			{
				MessageBox.Show("Invalid Maine state withholding ID", "Invalid State ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			dblReported = FCConvert.ToDouble(Strings.Format(Conversion.Val(txtReported.Text), "0.00"));
			dblWithheld = FCConvert.ToDouble(Strings.Format(Conversion.Val(txtWithheld.Text), "0.00"));
			dblThirdParty = FCConvert.ToDouble(Strings.Format(Conversion.Val(txtThirdPartyAmount.Text), "0.00"));
			if (chk1099.CheckState == Wisej.Web.CheckState.Checked)
			{
				bool1099 = true;
			}
			else
			{
				bool1099 = false;
			}
			if (SaveInfo())
			{
				if (boolElectronic)
				{
					if (CreateW3File(ref strFile))
					{
						Close();
						return;
					}
				}
				else
				{
					rptW3ME.InstancePtr.Init(ref dblWithheld, ref dblReported, ref bool1099, 0, ref dblThirdParty, txtThirdPartyName.Text, txtThirdPartyID.Text, this.Modal);
				}
			}
		}

		private void optElectronic_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				framFile.Visible = true;
			}
			else
			{
				framFile.Visible = false;
			}
		}

		private void optElectronic_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbElectronic.SelectedIndex;
			optElectronic_CheckedChanged(index, sender, e);
		}

		private void txtExtension_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFederalID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFilename_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Decimal)
			{
				KeyCode = (Keys)0;
			}
		}

		private void txtMRSID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (!KeyIsNumber(FCConvert.ToInt32(KeyAscii)))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: KeyAscii As int	OnWrite(Keys)
		private bool KeyIsNumber(int KeyAscii)
		{
			bool KeyIsNumber = false;
			KeyIsNumber = false;
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 27 || KeyAscii == 127 || KeyAscii == 8))
			{
				KeyIsNumber = true;
			}
			return KeyIsNumber;
		}

		private bool CreateW3File(ref string strFile)
		{
			bool CreateW3File = false;
			// creates the file "rtnwage" in the data directory
			FCFileSystem fso = new FCFileSystem();
			StreamWriter ts = null;
			bool boolFileOpened = false;
			string strRecord;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CreateW3File = false;
				boolFileOpened = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//strFile = Path.GetDirectoryName(strFile);
				// get rid of any existing file
				if (FCFileSystem.FileExists(strFile))
				{
					FCFileSystem.DeleteFile(strFile);
				}
				// create the file
				ts = FCFileSystem.CreateTextFile(strFile);
				// write the transmitter record (first record)
				strRecord = MakeTransmitterRecord();
				if (strRecord == "")
				{
					ts.Close();
					return CreateW3File;
				}
				ts.WriteLine(strRecord);
				// frmWait.IncrementProgress
				// write employer record
				strRecord = MakeEmployerRecord();
				if (strRecord == "")
				{
					ts.Close();
					return CreateW3File;
				}
				ts.WriteLine(strRecord);
				// write final record
				strRecord = MakeFinalRecord();
				if (strRecord == "")
				{
					ts.Close();
					return CreateW3File;
				}
				ts.WriteLine(strRecord);
				// close the file
				ts.Close();
				boolFileOpened = false;
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				ts = null;
				CreateW3File = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("File " + strFile + " created", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //FC:FINAL:BSE:#4166 process changed to prompt the user to download file
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, strFile), strFile);
				return CreateW3File;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				// Unload frmWait
				if (boolFileOpened)
				{
					ts.Close();
				}
				if (fecherFoundation.Information.Err(ex).Number == 52)
				{
					MessageBox.Show("Bad file name" + "\r\n" + "Make sure there are no illegal characters in the name such as / or *", "Bad File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateW3File", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return CreateW3File;
		}

		private string MakeTransmitterRecord()
		{
			string MakeTransmitterRecord = "";
			string strOut;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeTransmitterRecord = "";
				strOut = "A";
				strOut += Strings.Format(lngYearCovered, "0000");
				// four digit year
				strOut += "W3ME";
				strOut += EWRRecord.FederalEmployerID;
				// fed id. Already forced to correct size
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				// transmitting company name
				strOut += Strings.Mid(EWRRecord.TransmitterName + Strings.StrDup(30, " "), 1, 30);
				// person responsible for accuracy of this report
				// strOut = strOut & Mid(EWRRecord.TransmitterTitle & String(EWRTransmitterTitleLen, " "), 1, EWRTransmitterTitleLen)
				strOut += Strings.Format(EWRRecord.TransmitterPhone, "0000000000");
				object intValue = EWRRecord.TransmitterExt;
				strOut += FCConvert.ToString(modGlobalRoutines.PadToString(Conversion.Val(intValue), 4));
				EWRRecord.TransmitterExt = FCConvert.ToString(intValue);
				strOut += Strings.StrDup(128, " ");
				MakeTransmitterRecord = strOut;
				return MakeTransmitterRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeTransmitterRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeTransmitterRecord;
		}

		private string MakeEmployerRecord()
		{
			string MakeEmployerRecord = "";
			string strOut;
			int lngTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeEmployerRecord = "";
				strOut = "E";
				// employer record
				strOut += Strings.Format(lngYearCovered, "0000");
				// current year
				strOut += "W3ME";
				strOut += EWRRecord.MRSWithholdingID;
				// already forced to 11
				strOut += Strings.Mid(EWRRecord.EmployerName + Strings.StrDup(modCoreysSweeterCode.EWRNameLen, " "), 1, modCoreysSweeterCode.EWRNameLen);
				if (chk1099.CheckState == Wisej.Web.CheckState.Checked)
				{
					strOut += "Y";
				}
				else
				{
					strOut += "N";
				}
				strOut += Strings.Format(dblWithheld * 100, "00000000000000");
				strOut += Strings.Format(dblReported * 100, "00000000000000");
				strOut += Strings.StrDup(15, "0");
				// spaces
				strOut += Strings.Format(dblThirdParty * 100, "000000000000000");
				strOut += Strings.Left(txtThirdPartyName.Text + Strings.StrDup(51, " "), 51);
				strOut += Strings.Right(Strings.StrDup(9, "0") + FCConvert.ToString(Conversion.Val(txtThirdPartyID.Text)), 9);
				strOut += Strings.StrDup(51, " ");
				MakeEmployerRecord = strOut;
				return MakeEmployerRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeEmployerRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeEmployerRecord;
		}

		private string MakeFinalRecord()
		{
			string MakeFinalRecord = "";
			string strOut;
			double dblTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				MakeFinalRecord = "";
				strOut = "F";
				// final record
				strOut += "W3ME";
				strOut += Strings.Format(1, "00000");
				// total number of E records
				strOut += Strings.Format(dblWithheld * 100, "00000000000000");
				strOut += Strings.Format(dblReported * 100, "00000000000000");
				strOut += Strings.StrDup(202, " ");
				MakeFinalRecord = strOut;
				return MakeFinalRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeFinalRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeFinalRecord;
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			object strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				if (fecherFoundation.Strings.Trim(txtFederalID.Text).Length != modCoreysSweeterCode.EWRFederalIDLen)
				{
					MessageBox.Show("The federal ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRFederalIDLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFederalID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtThirdPartyID.Text).Length > 0 && fecherFoundation.Strings.Trim(txtThirdPartyID.Text).Length != modCoreysSweeterCode.EWRFederalIDLen)
				{
					MessageBox.Show("The third-party ID must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRFederalIDLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtThirdPartyID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtMRSID.Text).Length != modCoreysSweeterCode.EWRMRSWithholdingLen)
				{
					MessageBox.Show("The MRS Withholding Account must be of length " + FCConvert.ToString(modCoreysSweeterCode.EWRMRSWithholdingLen), "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtMRSID.Focus();
					return SaveInfo;
				}
				if (fecherFoundation.Strings.Trim(txtEmployerName.Text) == string.Empty)
				{
					MessageBox.Show("You must provide an employer name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerName.Focus();
					return SaveInfo;
				}
				EWRRecord.EmployerName = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtEmployerName.Text), modCoreysSweeterCode.EWRReturnNameLen));
				EWRRecord.FederalEmployerID = txtFederalID.Text;
				EWRRecord.MRSWithholdingID = txtMRSID.Text;
				EWRRecord.TransmitterExt = txtExtension.Text;
				strTemp = "";
				for (x = 1; x <= (txtPhone.Text.Length); x++)
				{
					if (Information.IsNumeric(Strings.Mid(txtPhone.Text, x, 1)))
					{
						strTemp += Strings.Mid(txtPhone.Text, x, 1);
					}
				}
				// x
				EWRRecord.TransmitterPhone = modGlobalRoutines.PadToString(Conversion.Val(strTemp), 10);
				EWRRecord.TransmitterName = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtContactName.Text), 30));
				EWRRecord.TransmitterTitle = FCConvert.ToString(modGlobalRoutines.AppendToString(fecherFoundation.Strings.Trim(txtContactTitle.Text), modCoreysSweeterCode.EWRTransmitterTitleLen));
				clsSave.OpenRecordset("select * from tblemployerinfo", "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("employername", EWRRecord.EmployerName);
				clsSave.Set_Fields("TransmitterTitle", EWRRecord.TransmitterTitle);
				clsSave.Set_Fields("transmitterextension", EWRRecord.TransmitterExt);
				clsSave.Set_Fields("transmitterphone", EWRRecord.TransmitterPhone);
				clsSave.Set_Fields("FederalEmployerID", EWRRecord.FederalEmployerID);
				clsSave.Set_Fields("MRSWithholdingID", EWRRecord.MRSWithholdingID);
				clsSave.Set_Fields("ThirdPartyName", fecherFoundation.Strings.Trim(txtThirdPartyName.Text));
				clsSave.Set_Fields("ThirdPartyID", txtThirdPartyID.Text);
				clsSave.Update();
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}
	}
}
