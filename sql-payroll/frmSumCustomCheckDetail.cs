﻿//Fecher vbPorter - Version 1.0.0.59
using Wisej.Web;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation;
using System;

namespace TWPY0000
{
	public partial class frmSumCustomCheckDetail : BaseForm
	{
		public frmSumCustomCheckDetail()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSumCustomCheckDetail InstancePtr
		{
			get
			{
				return (frmSumCustomCheckDetail)Sys.GetInstance(typeof(frmSumCustomCheckDetail));
			}
		}

		protected frmSumCustomCheckDetail _InstancePtr = null;
		//=========================================================
		private void cmdAll_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= this.lstTypes.Items.Count - 1; intCounter++)
			{
				if (cmdAll.Text == "Select All")
				{
					lstTypes.SetSelected(intCounter, true);
				}
				else
				{
					lstTypes.SetSelected(intCounter, false);
				}
			}
			if (cmdAll.Text == "Select All")
			{
				cmdAll.Text = "Deselect All";
			}
			else
			{
				cmdAll.Text = "Select All";
			}
		}

		private void frmSumCustomCheckDetail_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmSumCustomCheckDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSumCustomCheckDetail_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			lstTypes.AddItem("Gross Pay");
			lstTypes.AddItem("Net Pay");
			lstTypes.AddItem("Federal Tax WH");
			lstTypes.AddItem("State Tax WH");
			lstTypes.AddItem("FICA Tax WH");
			lstTypes.AddItem("Medicare Tax WH");
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

        private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strWhere = "";
			string strFields;
			int intCounter;
			modGlobalVariables.Statics.gstrTitle2Caption = string.Empty;
			strFields = string.Empty;
			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				if (lstTypes.Selected(intCounter))
				{
					strFields += " ,Sum(" + lstTypes.Items[intCounter].Text.Replace(" ", "") + ") AS SumOf" + lstTypes.Items[intCounter].Text.Replace(" ", "");
				}
			}
			// MATTHEW 8/18/2004
			if (fecherFoundation.Strings.Trim(strFields) != string.Empty)
			{
				strFields = " " + Strings.Mid(fecherFoundation.Strings.Trim(strFields), 2, fecherFoundation.Strings.Trim(strFields).Length - 1) + " ";
			}
			else
			{
				MessageBox.Show("Fields to report on must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			modGlobalConstants.Statics.gstrDeptDiv = string.Empty;
			if (cmbReportOn.Text == "Single Employee")
			{
				if (txtSingleEmployee.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtSingleEmployee.Focus();
					return;
				}
				else
				{
					modGlobalVariables.Statics.gstrTitle2Caption = "Single Employee #" + txtSingleEmployee.Text;
					strWhere = "Where tblCheckDetail.EmployeeNumber = '" + txtSingleEmployee.Text + "' AND TotalRecord = 1";
				}
			}
			else if (cmbReportOn.Text == "Emp / Date Range")
			{
				if (txtEmpSingle.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtEmpSingle.Focus();
					return;
				}
				else
				{
					if (txtEmpStartDate.Text == string.Empty)
					{
						MessageBox.Show("Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtEmpStartDate.Focus();
						return;
					}
					else if (!Information.IsDate(txtEmpStartDate.Text))
					{
						MessageBox.Show("Valid Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtEmpStartDate.Focus();
						return;
					}
					else if (txtEmpEndDate.Text == string.Empty)
					{
						modGlobalVariables.Statics.gstrTitle2Caption = "Single Employee #" + txtSingleEmployee.Text + " for the pay date of: " + txtEmpStartDate.Text;
						strWhere = "Where tblCheckDetail.EmployeeNumber = '" + txtEmpSingle.Text + "' AND TotalRecord = 1 AND PayDate = '" + txtEmpStartDate.Text + "'";
					}
					else if (txtEmpEndDate.Text != string.Empty && !Information.IsDate(txtEmpEndDate.Text))
					{
						MessageBox.Show("Valid End dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtEmpEndDate.Focus();
						return;
					}
					else
					{
						modGlobalVariables.Statics.gstrTitle2Caption = "Single Employee #" + txtSingleEmployee.Text + " for the pay date range from: " + txtEmpStartDate.Text + " to " + txtEmpEndDate.Text;
						strWhere = "Where tblCheckDetail.EmployeeNumber = '" + txtEmpSingle.Text + "' AND TotalRecord = 1 AND PayDate >= '" + txtEmpStartDate.Text + "' AND PayDate <= '" + txtEmpEndDate.Text + "'";
					}
				}
			}
			else if (cmbReportOn.Text == "Date Range")
			{
				if (txtStartDate.Text == string.Empty)
				{
					MessageBox.Show("Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtStartDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Valid Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtStartDate.Focus();
					return;
				}
				else if (txtEndDate.Text == string.Empty)
				{
					modGlobalVariables.Statics.gstrTitle2Caption = "Pay date of: " + txtStartDate.Text;
					strWhere = "Where TotalRecord = 1 AND PayDate = '" + txtStartDate.Text + "'";
				}
				else if (txtEndDate.Text != string.Empty && !Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("Valid End dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtEndDate.Focus();
					return;
				}
				else
				{
					modGlobalVariables.Statics.gstrTitle2Caption = "Pay date range from: " + txtStartDate.Text + " to " + txtEndDate.Text;
					strWhere = "Where TotalRecord = 1 AND PayDate >= '" + txtStartDate.Text + "' AND PayDate <= '" + txtEndDate.Text + "'";
				}
			}
			else if (cmbReportOn.Text == "Dept / Date Range")
			{
				if (txtDept.Text == string.Empty)
				{
					MessageBox.Show("Department must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtDept.Focus();
					return;
				}
				else
				{
					// THIS WILL HOLD T
					modGlobalConstants.Statics.gstrDeptDiv = fecherFoundation.Strings.Trim(txtDept.Text);
					if (txtDeptStartDate.Text == string.Empty)
					{
						MessageBox.Show("Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptStartDate.Focus();
						return;
					}
					else if (!Information.IsDate(txtDeptStartDate.Text))
					{
						MessageBox.Show("Valid Start dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptStartDate.Focus();
						return;
					}
					else if (txtDeptEndDate.Text == string.Empty)
					{
						modGlobalVariables.Statics.gstrTitle2Caption = "Department of: " + txtDept.Text + " and Pay date of: " + txtDeptStartDate.Text;
						strWhere = "Where TotalRecord = 1 AND PayDate = '" + txtDeptStartDate.Text + "'";
					}
					else if (txtDeptEndDate.Text != string.Empty && !Information.IsDate(txtDeptEndDate.Text))
					{
						MessageBox.Show("Valid End dates must be specified.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDeptEndDate.Focus();
						return;
					}
					else
					{
						modGlobalVariables.Statics.gstrTitle2Caption = "Department of: " + txtDept.Text + " and Pay date range from: " + txtDeptStartDate.Text + " to " + txtDeptEndDate.Text;
						strWhere = "Where TotalRecord = 1 AND PayDate >= '" + txtDeptStartDate.Text + "' AND PayDate <= '" + txtDeptEndDate.Text + "'";
					}
				}
			}
			if (strWhere == string.Empty)
			{
				strWhere = " Where CheckVoid = 0 ";
			}
			else
			{
				strWhere += " AND CheckVoid = 0 ";
			}
			modCustomReport.Statics.strCustomSQL = "Select" + strFields + ", tblCheckDetail.EmployeeNumber, tblEmployeeMaster.DeptDiv FROM tblCheckDetail INNER JOIN tblEmployeeMaster ON tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber " + strWhere + " Group By tblCheckDetail.EmployeeNumber,DeptDiv";
			frmReportViewer.InstancePtr.Init(rptSumCustomCheckDetail.InstancePtr, boolAllowEmail: true, strAttachmentName: "CustomCheckDetail");
		}

		private void optReportOn_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			txtEmpSingle.Visible = Index == 1;
			txtSingleEmployee.Visible = Index == 0;
			txtEmpStartDate.Visible = Index == 1;
			txtEmpEndDate.Visible = Index == 1;
			txtDept.Visible = Index == 3;
			txtDeptStartDate.Visible = Index == 3;
			txtDeptEndDate.Visible = Index == 3;
			txtStartDate.Visible = Index == 2;
			txtEndDate.Visible = Index == 2;
		}

		private void optReportOn_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReportOn.SelectedIndex;
			optReportOn_CheckedChanged(index, sender, e);
		}
	}
}
