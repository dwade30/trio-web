//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsPayTotalsByCat
	{
		//=========================================================
		private clsPayCatTotal[] PayCatTotals = null;
		private int intRegularIndex;
		private int intSickIndex;
		private int intVacationIndex;
		private int intOvertimeIndex;

		public void LoadCats()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsPayCatTotal tPCat;
				FCUtils.EraseSafe(PayCatTotals);
				rsLoad.OpenRecordset("select * from TBLpaycategories order by ID", "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					Array.Resize(ref PayCatTotals, FCConvert.ToInt32(rsLoad.Get_Fields("ID")) + 1);
					tPCat = new clsPayCatTotal();
					tPCat.ActualHours = 0;
					tPCat.AmountPaid = 0;
					tPCat.HoursPaid = 0;
					tPCat.PayCat = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					if (tPCat.PayCat == modCoreysSweeterCode.CNSTPAYCATREGULAR)
					{
						intRegularIndex = FCConvert.ToInt16(rsLoad.Get_Fields("ID"));
					}
					else if (tPCat.PayCat == modCoreysSweeterCode.CNSTPAYCATSICK)
					{
						intSickIndex = FCConvert.ToInt16(rsLoad.Get_Fields("ID"));
					}
					else if (tPCat.PayCat == modCoreysSweeterCode.CNSTPAYCATVACATION)
					{
						intVacationIndex = FCConvert.ToInt16(rsLoad.Get_Fields("ID"));
					}
					else if (tPCat.PayCat == modCoreysSweeterCode.CNSTPAYCATOVERTIME)
					{
						intOvertimeIndex = FCConvert.ToInt16(rsLoad.Get_Fields("ID"));
					}
					tPCat.ScheduledHours = 0;
					PayCatTotals[rsLoad.Get_Fields("ID")] = tPCat;
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadCats", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public int RegularIndex
		{
			get
			{
				int RegularIndex = 0;
				RegularIndex = intRegularIndex;
				return RegularIndex;
			}
		}

		public int SickIndex
		{
			get
			{
				int SickIndex = 0;
				SickIndex = intSickIndex;
				return SickIndex;
			}
		}

		public int VacationIndex
		{
			get
			{
				int VacationIndex = 0;
				VacationIndex = intVacationIndex;
				return VacationIndex;
			}
		}

		public int OvertimeIndex
		{
			get
			{
				int OvertimeIndex = 0;
				OvertimeIndex = intOvertimeIndex;
				return OvertimeIndex;
			}
		}

		public int MaxCat
		{
			get
			{
				int MaxCat = 0;
				int lngReturn;
				lngReturn = -1;
				if (!FCUtils.IsEmpty(PayCatTotals))
				{
					if (Information.UBound(PayCatTotals, 1) > 0)
					{
						lngReturn = Information.UBound(PayCatTotals, 1);
					}
				}
				MaxCat = lngReturn;
				return MaxCat;
			}
		}

		public void ClearCats()
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			if (!FCUtils.IsEmpty(PayCatTotals))
			{
				if (Information.UBound(PayCatTotals, 1) > 0)
				{
					for (X = 1; X <= (Information.UBound(PayCatTotals, 1)); X++)
					{
						if (!(PayCatTotals[X] == null))
						{
							PayCatTotals[X].Clear();
						}
					}
					// X
				}
			}
		}

		public void ClearDayTotals()
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			if (Information.UBound(PayCatTotals, 1) > 0)
			{
				for (X = 1; X <= (Information.UBound(PayCatTotals, 1)); X++)
				{
					if (!(PayCatTotals[X] == null))
					{
						PayCatTotals[X].ClearDay();
					}
				}
				// X
			}
		}

		public void ClearShiftTotals()
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			if (Information.UBound(PayCatTotals, 1) > 0)
			{
				for (X = 1; X <= (Information.UBound(PayCatTotals, 1)); X++)
				{
					if (!(PayCatTotals[X] == null))
					{
						PayCatTotals[X].ClearShift();
					}
				}
				// X
			}
		}

		public void AddToCat(clsPayCatTotal tCat)
		{
			if (Information.UBound(PayCatTotals, 1) >= tCat.PayCat)
			{
				if (PayCatTotals[tCat.PayCat] == null)
				{
					clsPayCatTotal tPCat = new clsPayCatTotal();
					tPCat.PayCat = tCat.PayCat;
					PayCatTotals[tCat.PayCat] = tPCat;
				}
			}
			else
			{
				Array.Resize(ref PayCatTotals, tCat.PayCat + 1);
				PayCatTotals[tCat.PayCat] = new clsPayCatTotal();
				PayCatTotals[tCat.PayCat].PayCat = tCat.PayCat;
			}
			PayCatTotals[tCat.PayCat].ActualHours += tCat.ActualHours;
			PayCatTotals[tCat.PayCat].AmountPaid += tCat.AmountPaid;
			PayCatTotals[tCat.PayCat].HoursPaid += tCat.HoursPaid;
			PayCatTotals[tCat.PayCat].ScheduledHours += tCat.ScheduledHours;
			PayCatTotals[tCat.PayCat].DayActualHours += tCat.DayActualHours;
			PayCatTotals[tCat.PayCat].DayAmountPaid += tCat.DayAmountPaid;
			PayCatTotals[tCat.PayCat].DayHoursPaid += tCat.DayHoursPaid;
			PayCatTotals[tCat.PayCat].DayScheduledHours += tCat.DayScheduledHours;
			PayCatTotals[tCat.PayCat].ShiftActualHours += tCat.ShiftActualHours;
			PayCatTotals[tCat.PayCat].ShiftAmountPaid += tCat.ShiftAmountPaid;
			PayCatTotals[tCat.PayCat].ShiftHoursPaid += tCat.ShiftHoursPaid;
			PayCatTotals[tCat.PayCat].ShiftScheduledHours += tCat.ShiftScheduledHours;
			if (tCat.BreakDownCount > 0)
			{
				int X;
				clsPayCatTotal tBreak;
				for (X = 1; X <= tCat.BreakDownCount; X++)
				{
					tBreak = (clsPayCatTotal)tCat.Get_BreakdownByIndex(X);
					if (!(tBreak == null))
					{
						PayCatTotals[tCat.PayCat].Set_BreakdownActualHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownActualHours(tBreak.Account) + tBreak.ActualHours);
						PayCatTotals[tCat.PayCat].Set_BreakdownAmountPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownAmountPaid(tBreak.Account) + tBreak.AmountPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownHoursPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownHoursPaid(tBreak.Account) + tBreak.HoursPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownScheduledHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownScheduledHours(tBreak.Account) + tBreak.ScheduledHours);
						PayCatTotals[tCat.PayCat].Set_BreakdownDayActualHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownDayActualHours(tBreak.Account) + tBreak.DayActualHours);
						PayCatTotals[tCat.PayCat].Set_BreakdownDayAmountPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownDayAmountPaid(tBreak.Account) + tBreak.DayAmountPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownDayHoursPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownDayHoursPaid(tBreak.Account) + tBreak.DayHoursPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownDayScheduledHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownDayScheduledHours(tBreak.Account) + tBreak.DayScheduledHours);
						PayCatTotals[tCat.PayCat].Set_BreakdownShiftActualHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownShiftActualHours(tBreak.Account) + tBreak.ShiftActualHours);
						PayCatTotals[tCat.PayCat].Set_BreakdownShiftAmountPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownShiftAmountPaid(tBreak.Account) + tBreak.ShiftAmountPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownShiftHoursPaid(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownShiftHoursPaid(tBreak.Account) + tBreak.ShiftHoursPaid);
						PayCatTotals[tCat.PayCat].Set_BreakdownShiftScheduledHours(tBreak.Account, PayCatTotals[tCat.PayCat].Get_BreakdownShiftScheduledHours(tBreak.Account) + tBreak.ShiftScheduledHours);
					}
				}
				// X
			}
		}

		public clsPayCatTotal Get_CategoryTotals(int lngCat)
		{
			clsPayCatTotal CategoryTotals = null;
			clsPayCatTotal tCat;
			tCat = null;
			// If Not PayCatTotals Is Nothing Then
			if (Information.UBound(PayCatTotals, 1) >= lngCat)
			{
				if (!(PayCatTotals[lngCat] == null))
				{
					tCat = PayCatTotals[lngCat];
				}
			}
			// End If
			CategoryTotals = tCat;
			return CategoryTotals;
		}
	}
}
