﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPYDistributionReportItem : cPayrollDistribution
	{
		//=========================================================
		private int lngRecordID;
		private int lngRecordNumber;
		private bool boolUpdated;
		private bool boolDeleted;
		private string strEmployeeNumber = string.Empty;
		private string strAccountCode = string.Empty;
		private string strAccountNumber = string.Empty;
		private string strWorkComp = string.Empty;
		private int lngCategory;
		private string strMSRS = string.Empty;
		private string strWC = string.Empty;
		private int lngTaxCode;
		private int lngCD;
		private double dblBaseRate;
		private double dblFactor;
		private int intNumberWeeks;
		private double dblDefaultHours;
		private double dblHoursWeek;
		private double dblGross;
		private string strDistU = string.Empty;
		private string strStatusCode = string.Empty;
		private string strGrantFunded = string.Empty;
		private int lngMSRSID;
		private int lngProject;
		private int intWeeksTaxWithheld;
		private int lngContractID;
		private string strCategoryDescription = string.Empty;
		private string strTaxStatusDescription = string.Empty;
		private string strPayCodeDescription = string.Empty;

		public string PayCodeDescription
		{
			set
			{
				strPayCodeDescription = value;
			}
			get
			{
				string PayCodeDescription = "";
				PayCodeDescription = strPayCodeDescription;
				return PayCodeDescription;
			}
		}

		public string TaxStatusDescription
		{
			set
			{
				strTaxStatusDescription = value;
			}
			get
			{
				string TaxStatusDescription = "";
				TaxStatusDescription = strTaxStatusDescription;
				return TaxStatusDescription;
			}
		}

		public string CategoryDescription
		{
			set
			{
				strCategoryDescription = value;
			}
			get
			{
				string CategoryDescription = "";
				CategoryDescription = strCategoryDescription;
				return CategoryDescription;
			}
		}
	}
}
