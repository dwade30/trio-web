//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmElectronicMSRS : BaseForm
	{
		public frmElectronicMSRS()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmElectronicMSRS InstancePtr
		{
			get
			{
				return (frmElectronicMSRS)Sys.GetInstance(typeof(frmElectronicMSRS));
			}
		}

		protected frmElectronicMSRS _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolCancel;
		private int intReportType;
		private bool boolFirstRun;
		const int CNSTMSRSFILINGTYPEORIGINAL = 0;
		const int CNSTMSRSFILINGTYPETEST = 1;
		const int CNSTMSRSFILINGTYPEREPLACEMENT = 2;
		const int CNSTMSRSPAYROLLCYCLEWEEKLY = 0;
		const int CNSTMSRSPAYROLLCYCLEBIWEEKLY = 1;
		const int CNSTMSRSPAYROLLCYCLEMONTHLY = 2;
		const int CNSTGRIDEMPMATCHCOLINCLUDE = 0;
		const int CNSTGRIDEMPMATCHCOLNUMBER = 1;
		const int CNSTGRIDEMPMATCHCOLDESCRIPTION = 2;
		// vbPorter upgrade warning: ReportType As int	OnWriteFCConvert.ToInt32(
		public bool Init(int ReportType, bool boolFirstRunOfMonth)
		{
			bool Init = false;
			boolCancel = true;
			boolFirstRun = boolFirstRunOfMonth;
			Init = false;
			intReportType = ReportType;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = !boolCancel;
			return Init;
		}

		private void frmElectronicMSRS_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmElectronicMSRS_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmElectronicMSRS properties;
			//frmElectronicMSRS.FillStyle	= 0;
			//frmElectronicMSRS.ScaleWidth	= 9300;
			//frmElectronicMSRS.ScaleHeight	= 7440;
			//frmElectronicMSRS.LinkTopic	= "Form2";
			//frmElectronicMSRS.LockControls	= -1  'True;
			//frmElectronicMSRS.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombos();
			// SetupGridEmpMatch
			LoadData();
		}

		private void FillCombos()
		{
			cmbFilingType.Clear();
			cmbFilingType.AddItem("Original Filing");
			cmbFilingType.ItemData(cmbFilingType.NewIndex, CNSTMSRSFILINGTYPEORIGINAL);
			cmbFilingType.AddItem("Test Filing");
			cmbFilingType.ItemData(cmbFilingType.NewIndex, CNSTMSRSFILINGTYPETEST);
			cmbFilingType.AddItem("Replacement Filing");
			cmbFilingType.ItemData(cmbFilingType.NewIndex, CNSTMSRSFILINGTYPEREPLACEMENT);
			cmbPayrollCycle.Clear();
			cmbPayrollCycle.AddItem("Weekly");
			cmbPayrollCycle.ItemData(cmbPayrollCycle.NewIndex, CNSTMSRSPAYROLLCYCLEWEEKLY);
			cmbPayrollCycle.AddItem("Bi-Weekly");
			cmbPayrollCycle.ItemData(cmbPayrollCycle.NewIndex, CNSTMSRSPAYROLLCYCLEBIWEEKLY);
			cmbPayrollCycle.AddItem("Monthly");
			cmbPayrollCycle.ItemData(cmbPayrollCycle.NewIndex, CNSTMSRSPAYROLLCYCLEMONTHLY);
		}
		// Private Sub Form_Resize()
		// ResizeGridEmpMatch
		// End Sub
		// Private Sub GridEmpMatch_BeforeRowColChange(ByVal OldRow As Long, ByVal OldCol As Long, ByVal NewRow As Long, ByVal NewCol As Long, Cancel As Boolean)
		// Select Case NewCol
		// Case CNSTGRIDEMPMATCHCOLINCLUDE
		// GridEmpMatch.Editable = flexEDKbdMouse
		// Case Else
		// GridEmpMatch.Editable = flexEDNone
		// End Select
		// End Sub
		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			boolCancel = true;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				boolCancel = false;
				Close();
			}
		}

		private void LoadData()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			clsLoad.OpenRecordset("select * from tblMSRSTable where reporttype = " + FCConvert.ToString(intReportType), "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (Conversion.Val(clsLoad.Get_Fields("submissionnumber")) > 0)
				{
					txtSubmissionNumber.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("submissionnumber")));
				}
				else
				{
					txtSubmissionNumber.Text = "1";
				}
				if (boolFirstRun)
				{
					txtSubmissionNumber.Text = "1";
				}
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("filingtype"))) == "T")
				{
					cmbFilingType.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("filingtype"))) == "R")
				{
					// cmbFilingType.ListIndex = 2
					cmbFilingType.SelectedIndex = 0;
				}
				else
				{
					cmbFilingType.SelectedIndex = 0;
					// O
				}
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("PayrollCycle"))) == "T")
				{
					cmbPayrollCycle.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("PayrollCycle"))) == "M")
				{
					cmbPayrollCycle.SelectedIndex = 2;
				}
				else
				{
					cmbPayrollCycle.SelectedIndex = 0;
				}
				txtTransmitterCode.Text = FCConvert.ToString(clsLoad.Get_Fields("transmittercode"));
				txtTransmitterName.Text = FCConvert.ToString(clsLoad.Get_Fields("transmittername"));
				txtAddress.Text = FCConvert.ToString(clsLoad.Get_Fields("transmitteraddress"));
				txtContactPerson.Text = FCConvert.ToString(clsLoad.Get_Fields("transmittercontactperson"));
				txtEmployerCode.Text = FCConvert.ToString(clsLoad.Get_Fields("employercode"));
				txtEmployerName.Text = FCConvert.ToString(clsLoad.Get_Fields("employername"));
				strTemp = FCConvert.ToString(clsLoad.Get_Fields("TELEPHONE"));
				strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Right("0000000000" + strTemp, 10);
				t2kPhone.Text = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
				// strTemp = .Fields("schedulenumber")
				// txtScheduleNumber.Text = Right("000000" & strTemp, 6)
			}
			else
			{
				txtSubmissionNumber.Text = "1";
				cmbFilingType.SelectedIndex = 0;
				// original
				cmbPayrollCycle.SelectedIndex = 0;
				// weekly
				// txtScheduleNumber.Text = "000001"
			}
			// LoadGridEmpMatch
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				string strTemp = "";
				int lngRow;
				SaveData = false;
				clsSave.OpenRecordset("select * from tblmsrsTable where reporttype = " + FCConvert.ToString(intReportType), "twpy0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("reporttype", intReportType);
				clsSave.Set_Fields("transmittercode", txtTransmitterCode.Text);
				clsSave.Set_Fields("transmittername", txtTransmitterName.Text);
				clsSave.Set_Fields("transmitteraddress", txtAddress.Text);
				clsSave.Set_Fields("transmittercontactperson", txtContactPerson.Text);
				clsSave.Set_Fields("employercode", txtEmployerCode.Text);
				clsSave.Set_Fields("employername", txtEmployerName.Text);
				// strTemp = Trim(txtScheduleNumber.Text)
				// strTemp = Right("000000" & strTemp, 6)
				// .Fields("schedulenumber") = strTemp
				strTemp = fecherFoundation.Strings.Trim(t2kPhone.Text);
				// strTemp = Replace(strTemp, "(", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, ")", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
				// strTemp = Right("0000000000" & strTemp, 10)
				clsSave.Set_Fields("telephone", strTemp);
				if (Conversion.Val(txtSubmissionNumber.Text) > 0)
				{
					clsSave.Set_Fields("submissionnumber", FCConvert.ToString(Conversion.Val(txtSubmissionNumber.Text)));
				}
				else
				{
					clsSave.Set_Fields("Submissionnumber", 1);
				}
				if (cmbFilingType.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("Filingtype", cmbFilingType.ItemData(cmbFilingType.SelectedIndex));
					switch (cmbFilingType.ItemData(cmbFilingType.SelectedIndex))
					{
						case CNSTMSRSFILINGTYPETEST:
							{
								clsSave.Set_Fields("Filingtype", "T");
								break;
							}
						case CNSTMSRSFILINGTYPEREPLACEMENT:
							{
								clsSave.Set_Fields("filingtype", "R");
								break;
							}
						default:
							{
								clsSave.Set_Fields("FilingType", "O");
								// original
								break;
							}
					}
					//end switch
				}
				else
				{
					clsSave.Set_Fields("FilingType", "O");
					// original
				}
				if (cmbPayrollCycle.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("PayrollCycle", cmbPayrollCycle.ItemData(cmbPayrollCycle.SelectedIndex));
					switch (cmbPayrollCycle.ItemData(cmbPayrollCycle.SelectedIndex))
					{
						case CNSTMSRSPAYROLLCYCLEBIWEEKLY:
							{
								clsSave.Set_Fields("PayrollCycle", "T");
								break;
							}
						case CNSTMSRSPAYROLLCYCLEMONTHLY:
							{
								clsSave.Set_Fields("PayrollCycle", "M");
								break;
							}
						default:
							{
								// weekly
								clsSave.Set_Fields("Payrollcycle", "W");
								break;
							}
					}
					//end switch
				}
				else
				{
					clsSave.Set_Fields("Payrollcycle", "W");
					// Weekly
				}
				clsSave.Update();
				// Call clsSave.Execute("delete from tbldefaultdeductions where type = 'M'", "twpy0000.vb1")
				// Call clsSave.OpenRecordset("select * from tbldefaultdeductions where deductionid = -1000", "twpy0000.vb1") 'don't get anything
				// For lngRow = 1 To GridEmpMatch.Rows - 1
				// If CBool(GridEmpMatch.TextMatrix(lngRow, CNSTGRIDEMPMATCHCOLINCLUDE)) Then
				// clsSave.AddNew
				// clsSave.Fields("deductionid") = GridEmpMatch.RowData(lngRow)
				// clsSave.Fields("Type") = "M"
				// clsSave.Fields("LastUserID") = FixQuote(gstrUser)
				// clsSave.Fields("LastUpdated") = Now
				// clsSave.Update
				// End If
				// Next lngRow
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(cmdSave, EventArgs.Empty);
        }
        // Private Sub SetupGridEmpMatch()
        // With GridEmpMatch
        // .Cols = 3
        // .ColDataType(CNSTGRIDEMPMATCHCOLINCLUDE) = flexDTBoolean
        // .TextMatrix(0, CNSTGRIDEMPMATCHCOLDESCRIPTION) = "Deduction"
        // End With
        // End Sub
        //
        // Private Sub ResizeGridEmpMatch()
        // Dim GridWidth As Long
        // With GridEmpMatch
        // GridWidth = .Width
        // .ColWidth(CNSTGRIDEMPMATCHCOLNUMBER) = 0.08 * GridWidth
        // .ColWidth(CNSTGRIDEMPMATCHCOLINCLUDE) = 0.1 * GridWidth
        // End With
        // End Sub
        //
        //
        // Private Sub LoadGridEmpMatch()
        // Dim clsLoad As New clsDRWrapper
        // Dim lngRow As Long
        //
        // With GridEmpMatch
        // .Rows = 1
        // Call clsLoad.OpenRecordset("select * from tbldeductionsetup order by deductionnumber", "twpy0000.vb1")
        // Do While Not clsLoad.EndOfFile
        // .Rows = .Rows + 1
        // lngRow = .Rows - 1
        // .TextMatrix(lngRow, CNSTGRIDEMPMATCHCOLNUMBER) = Val(clsLoad.Fields("DeductionNumber"))
        // .TextMatrix(lngRow, CNSTGRIDEMPMATCHCOLDESCRIPTION) = Trim(clsLoad.Fields("description"))
        // .RowData(lngRow) = Val(clsLoad.Fields("ID"))
        // .TextMatrix(lngRow, CNSTGRIDEMPMATCHCOLINCLUDE) = False
        // clsLoad.MoveNext
        // Loop
        // Call clsLoad.OpenRecordset("select * from tblDefaultDeductions where type = 'M'", "twpy0000.vb1")
        // Do While Not clsLoad.EndOfFile
        // lngRow = .FindRow(Val(clsLoad.Fields("deductionid")), 0)
        // If lngRow > 0 Then
        // .TextMatrix(lngRow, CNSTGRIDEMPMATCHCOLINCLUDE) = True
        // End If
        // clsLoad.MoveNext
        // Loop
        // End With
        // End Sub
    }
}
