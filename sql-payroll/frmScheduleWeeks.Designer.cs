//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmScheduleWeeks.
	/// </summary>
	partial class frmScheduleWeeks
	{
		public System.Collections.Generic.List<FCGrid> Grid;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCPanel framGrids;
		public FCGrid Grid_0;
		public FCGrid Grid_1;
		public FCGrid Grid_2;
		public FCGrid Grid_3;
		public FCGrid Grid_4;
		public FCGrid Grid_5;
		public FCGrid Grid_6;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public FCGrid GridWeeks;
		public FCGrid GridSchedule;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblWeek;
		public fecherFoundation.FCLabel lblSchedule;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddSchedule;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteSchedule;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuAddWeek;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteWeek;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.framGrids = new fecherFoundation.FCPanel();
            this.Grid_0 = new fecherFoundation.FCGrid();
            this.Grid_1 = new fecherFoundation.FCGrid();
            this.Grid_2 = new fecherFoundation.FCGrid();
            this.Grid_3 = new fecherFoundation.FCGrid();
            this.Grid_4 = new fecherFoundation.FCGrid();
            this.Grid_5 = new fecherFoundation.FCGrid();
            this.Grid_6 = new fecherFoundation.FCGrid();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.GridWeeks = new fecherFoundation.FCGrid();
            this.GridSchedule = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblWeek = new fecherFoundation.FCLabel();
            this.lblSchedule = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddSchedule = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteSchedule = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddWeek = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteWeek = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAddSchedule = new fecherFoundation.FCButton();
            this.cmdDeleteSchedule = new fecherFoundation.FCButton();
            this.cmdAddWeek = new fecherFoundation.FCButton();
            this.cmdDeleteWeek = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framGrids)).BeginInit();
            this.framGrids.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridWeeks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 624);
            this.BottomPanel.Size = new System.Drawing.Size(1198, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.GridWeeks);
            this.ClientArea.Controls.Add(this.GridSchedule);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblWeek);
            this.ClientArea.Controls.Add(this.lblSchedule);
            this.ClientArea.Size = new System.Drawing.Size(1198, 564);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteWeek);
            this.TopPanel.Controls.Add(this.cmdAddWeek);
            this.TopPanel.Controls.Add(this.cmdDeleteSchedule);
            this.TopPanel.Controls.Add(this.cmdAddSchedule);
            this.TopPanel.Size = new System.Drawing.Size(1198, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddSchedule, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteSchedule, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddWeek, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteWeek, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(127, 30);
            this.HeaderText.Text = "Schedules";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AutoScroll = true;
            this.Frame1.Controls.Add(this.framGrids);
            this.Frame1.Location = new System.Drawing.Point(30, 337);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1136, 194);
            this.Frame1.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // framGrids
            // 
            this.framGrids.AppearanceKey = "groupBoxNoBorders";
            this.framGrids.Controls.Add(this.Grid_0);
            this.framGrids.Controls.Add(this.Grid_1);
            this.framGrids.Controls.Add(this.Grid_2);
            this.framGrids.Controls.Add(this.Grid_3);
            this.framGrids.Controls.Add(this.Grid_4);
            this.framGrids.Controls.Add(this.Grid_5);
            this.framGrids.Controls.Add(this.Grid_6);
            this.framGrids.Controls.Add(this.Label1_6);
            this.framGrids.Controls.Add(this.Label1_5);
            this.framGrids.Controls.Add(this.Label1_4);
            this.framGrids.Controls.Add(this.Label1_3);
            this.framGrids.Controls.Add(this.Label1_2);
            this.framGrids.Controls.Add(this.Label1_1);
            this.framGrids.Controls.Add(this.Label1_0);
            this.framGrids.Name = "framGrids";
            this.framGrids.Size = new System.Drawing.Size(2520, 172);
            this.framGrids.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.framGrids, null);
            // 
            // Grid_0
            // 
            this.Grid_0.Cols = 4;
            this.Grid_0.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_0.ExtendLastCol = true;
            this.Grid_0.FixedCols = 0;
            this.Grid_0.Location = new System.Drawing.Point(0, 22);
            this.Grid_0.Name = "Grid_0";
            this.Grid_0.ReadOnly = false;
            this.Grid_0.RowHeadersVisible = false;
            this.Grid_0.Rows = 1;
            this.Grid_0.Size = new System.Drawing.Size(340, 144);
            this.Grid_0.StandardTab = false;
            this.Grid_0.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_0.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.Grid_0, null);
            this.Grid_0.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_0.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_0.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_1
            // 
            this.Grid_1.Cols = 4;
            this.Grid_1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_1.ExtendLastCol = true;
            this.Grid_1.FixedCols = 0;
            this.Grid_1.Location = new System.Drawing.Point(360, 22);
            this.Grid_1.Name = "Grid_1";
            this.Grid_1.ReadOnly = false;
            this.Grid_1.RowHeadersVisible = false;
            this.Grid_1.Rows = 1;
            this.Grid_1.Size = new System.Drawing.Size(340, 144);
            this.Grid_1.StandardTab = false;
            this.Grid_1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_1.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.Grid_1, null);
            this.Grid_1.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_1.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_1.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_2
            // 
            this.Grid_2.Cols = 4;
            this.Grid_2.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_2.ExtendLastCol = true;
            this.Grid_2.FixedCols = 0;
            this.Grid_2.Location = new System.Drawing.Point(720, 22);
            this.Grid_2.Name = "Grid_2";
            this.Grid_2.ReadOnly = false;
            this.Grid_2.RowHeadersVisible = false;
            this.Grid_2.Rows = 1;
            this.Grid_2.Size = new System.Drawing.Size(340, 144);
            this.Grid_2.StandardTab = false;
            this.Grid_2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_2.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.Grid_2, null);
            this.Grid_2.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_2.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_2.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_3
            // 
            this.Grid_3.Cols = 4;
            this.Grid_3.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_3.ExtendLastCol = true;
            this.Grid_3.FixedCols = 0;
            this.Grid_3.Location = new System.Drawing.Point(1080, 22);
            this.Grid_3.Name = "Grid_3";
            this.Grid_3.ReadOnly = false;
            this.Grid_3.RowHeadersVisible = false;
            this.Grid_3.Rows = 1;
            this.Grid_3.Size = new System.Drawing.Size(340, 144);
            this.Grid_3.StandardTab = false;
            this.Grid_3.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_3.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.Grid_3, null);
            this.Grid_3.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_3.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_3.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_4
            // 
            this.Grid_4.Cols = 4;
            this.Grid_4.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_4.ExtendLastCol = true;
            this.Grid_4.FixedCols = 0;
            this.Grid_4.Location = new System.Drawing.Point(1440, 22);
            this.Grid_4.Name = "Grid_4";
            this.Grid_4.ReadOnly = false;
            this.Grid_4.RowHeadersVisible = false;
            this.Grid_4.Rows = 1;
            this.Grid_4.Size = new System.Drawing.Size(340, 144);
            this.Grid_4.StandardTab = false;
            this.Grid_4.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_4.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.Grid_4, null);
            this.Grid_4.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_4.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_4.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_5
            // 
            this.Grid_5.Cols = 4;
            this.Grid_5.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_5.ExtendLastCol = true;
            this.Grid_5.FixedCols = 0;
            this.Grid_5.Location = new System.Drawing.Point(1800, 22);
            this.Grid_5.Name = "Grid_5";
            this.Grid_5.ReadOnly = false;
            this.Grid_5.RowHeadersVisible = false;
            this.Grid_5.Rows = 1;
            this.Grid_5.Size = new System.Drawing.Size(340, 144);
            this.Grid_5.StandardTab = false;
            this.Grid_5.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_5.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.Grid_5, null);
            this.Grid_5.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_5.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_5.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Grid_6
            // 
            this.Grid_6.Cols = 4;
            this.Grid_6.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid_6.ExtendLastCol = true;
            this.Grid_6.FixedCols = 0;
            this.Grid_6.Location = new System.Drawing.Point(2160, 22);
            this.Grid_6.Name = "Grid_6";
            this.Grid_6.ReadOnly = false;
            this.Grid_6.RowHeadersVisible = false;
            this.Grid_6.Rows = 1;
            this.Grid_6.Size = new System.Drawing.Size(340, 144);
            this.Grid_6.StandardTab = false;
            this.Grid_6.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid_6.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.Grid_6, null);
            this.Grid_6.CellButtonClick += new System.EventHandler(this.Grid_CellButtonClick);
            this.Grid_6.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
            this.Grid_6.KeyDown += new KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(2160, 0);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(143, 16);
            this.Label1_6.TabIndex = 18;
            this.Label1_6.Text = "SATURDAY";
            this.ToolTip1.SetToolTip(this.Label1_6, null);
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(1800, 0);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(143, 16);
            this.Label1_5.TabIndex = 17;
            this.Label1_5.Text = "FRIDAY";
            this.ToolTip1.SetToolTip(this.Label1_5, null);
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(1440, 0);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(143, 16);
            this.Label1_4.TabIndex = 16;
            this.Label1_4.Text = "THURSDAY";
            this.ToolTip1.SetToolTip(this.Label1_4, null);
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(1080, 0);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(143, 16);
            this.Label1_3.TabIndex = 15;
            this.Label1_3.Text = "WEDNESDAY";
            this.ToolTip1.SetToolTip(this.Label1_3, null);
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(720, 0);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(143, 16);
            this.Label1_2.TabIndex = 14;
            this.Label1_2.Text = "TUESDAY";
            this.ToolTip1.SetToolTip(this.Label1_2, null);
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(360, 0);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(57, 16);
            this.Label1_1.TabIndex = 13;
            this.Label1_1.Text = "MONDAY";
            this.ToolTip1.SetToolTip(this.Label1_1, null);
            // 
            // Label1_0
            // 
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(143, 16);
            this.Label1_0.TabIndex = 12;
            this.Label1_0.Text = "SUNDAY";
            this.ToolTip1.SetToolTip(this.Label1_0, null);
            // 
            // GridWeeks
            // 
            this.GridWeeks.AllowDrag = true;
            this.GridWeeks.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.GridWeeks.Cols = 5;
            this.GridWeeks.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridWeeks.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.GridWeeks.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.GridWeeks.ExtendLastCol = true;
            this.GridWeeks.Location = new System.Drawing.Point(785, 31);
            this.GridWeeks.Name = "GridWeeks";
            this.GridWeeks.ReadOnly = false;
            this.GridWeeks.Rows = 1;
            this.GridWeeks.Size = new System.Drawing.Size(381, 227);
            this.GridWeeks.StandardTab = false;
            this.GridWeeks.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridWeeks.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.GridWeeks, "Use Insert and Delete to add and delete weeks. Drag weeks to re-order");
            this.GridWeeks.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridWeeks_BeforeRowColChange);
            this.GridWeeks.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridWeeks_ValidateEdit);
            this.GridWeeks.CellFormatting+= new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridWeeks_MouseMoveEvent);
            this.GridWeeks.KeyDown += new Wisej.Web.KeyEventHandler(this.GridWeeks_KeyDownEvent);
            // 
            // GridSchedule
            // 
            this.GridSchedule.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSchedule.Cols = 4;
            this.GridSchedule.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridSchedule.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.GridSchedule.ExtendLastCol = true;
            this.GridSchedule.FixedCols = 0;
            this.GridSchedule.Location = new System.Drawing.Point(30, 30);
            this.GridSchedule.Name = "GridSchedule";
            this.GridSchedule.ReadOnly = false;
            this.GridSchedule.RowHeadersVisible = false;
            this.GridSchedule.Rows = 1;
            this.GridSchedule.Size = new System.Drawing.Size(737, 227);
            this.GridSchedule.StandardTab = false;
            this.GridSchedule.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridSchedule.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.GridSchedule, "Use Insert and Delete to add and delete schedules");
            this.GridSchedule.ComboCloseUp += new System.EventHandler(this.GridSchedule_ComboCloseUp);
            this.GridSchedule.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridSchedule_BeforeRowColChange);
            this.GridSchedule.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridSchedule_ValidateEdit);
            this.GridSchedule.KeyDown += new Wisej.Web.KeyEventHandler(this.GridSchedule_KeyDownEvent);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 306);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 15);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "WEEK";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 277);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(88, 15);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "SCHEDULE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // lblWeek
            // 
            this.lblWeek.Location = new System.Drawing.Point(125, 306);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(326, 15);
            this.lblWeek.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.lblWeek, null);
            // 
            // lblSchedule
            // 
            this.lblSchedule.Location = new System.Drawing.Point(125, 277);
            this.lblSchedule.Name = "lblSchedule";
            this.lblSchedule.Size = new System.Drawing.Size(326, 15);
            this.lblSchedule.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.lblSchedule, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddSchedule,
            this.mnuDeleteSchedule,
            this.mnuSepar3,
            this.mnuAddWeek,
            this.mnuDeleteWeek,
            this.mnuSepar1,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddSchedule
            // 
            this.mnuAddSchedule.Index = 0;
            this.mnuAddSchedule.Name = "mnuAddSchedule";
            this.mnuAddSchedule.Text = "Add Schedule";
            this.mnuAddSchedule.Click += new System.EventHandler(this.mnuAddSchedule_Click);
            // 
            // mnuDeleteSchedule
            // 
            this.mnuDeleteSchedule.Index = 1;
            this.mnuDeleteSchedule.Name = "mnuDeleteSchedule";
            this.mnuDeleteSchedule.Text = "Delete Schedule";
            this.mnuDeleteSchedule.Click += new System.EventHandler(this.mnuDeleteSchedule_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuAddWeek
            // 
            this.mnuAddWeek.Index = 3;
            this.mnuAddWeek.Name = "mnuAddWeek";
            this.mnuAddWeek.Text = "Add Week";
            this.mnuAddWeek.Click += new System.EventHandler(this.mnuAddWeek_Click);
            // 
            // mnuDeleteWeek
            // 
            this.mnuDeleteWeek.Index = 4;
            this.mnuDeleteWeek.Name = "mnuDeleteWeek";
            this.mnuDeleteWeek.Text = "Delete Week";
            this.mnuDeleteWeek.Click += new System.EventHandler(this.mnuDeleteWeek_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 5;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 6;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 7;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 8;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 9;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddSchedule
            // 
            this.cmdAddSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddSchedule.Location = new System.Drawing.Point(750, 30);
            this.cmdAddSchedule.Name = "cmdAddSchedule";
            this.cmdAddSchedule.Size = new System.Drawing.Size(104, 24);
            this.cmdAddSchedule.TabIndex = 1;
            this.cmdAddSchedule.Text = "Add Schedule";
            this.cmdAddSchedule.Click += new System.EventHandler(this.mnuAddSchedule_Click);
            // 
            // cmdDeleteSchedule
            // 
            this.cmdDeleteSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteSchedule.Location = new System.Drawing.Point(860, 30);
            this.cmdDeleteSchedule.Name = "cmdDeleteSchedule";
            this.cmdDeleteSchedule.Size = new System.Drawing.Size(120, 24);
            this.cmdDeleteSchedule.TabIndex = 2;
            this.cmdDeleteSchedule.Text = "Delete Schedule";
            this.cmdDeleteSchedule.Click += new System.EventHandler(this.mnuDeleteSchedule_Click);
            // 
            // cmdAddWeek
            // 
            this.cmdAddWeek.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddWeek.Location = new System.Drawing.Point(986, 30);
            this.cmdAddWeek.Name = "cmdAddWeek";
            this.cmdAddWeek.Size = new System.Drawing.Size(84, 24);
            this.cmdAddWeek.TabIndex = 3;
            this.cmdAddWeek.Text = "Add Week";
            this.cmdAddWeek.Click += new System.EventHandler(this.mnuAddWeek_Click);
            // 
            // cmdDeleteWeek
            // 
            this.cmdDeleteWeek.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteWeek.Location = new System.Drawing.Point(1076, 30);
            this.cmdDeleteWeek.Name = "cmdDeleteWeek";
            this.cmdDeleteWeek.Size = new System.Drawing.Size(100, 24);
            this.cmdDeleteWeek.TabIndex = 4;
            this.cmdDeleteWeek.Text = "Delete Week";
            this.cmdDeleteWeek.Click += new System.EventHandler(this.mnuDeleteWeek_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(485, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmScheduleWeeks
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1198, 732);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmScheduleWeeks";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Schedules";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmScheduleWeeks_Load);
            this.Resize += new System.EventHandler(this.frmScheduleWeeks_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmScheduleWeeks_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framGrids)).EndInit();
            this.framGrids.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridWeeks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDeleteSchedule;
		private FCButton cmdAddSchedule;
		private FCButton cmdDeleteWeek;
		private FCButton cmdAddWeek;
		private FCButton cmdSave;
	}
}