﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingDistribution.
	/// </summary>
	partial class rptCheckListingDistribution
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckListingDistribution));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistribution = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWarrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVoid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblSort = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblVoid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistGrossPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarrant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVoid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtPayRate,
            this.txtDistGrossPay,
            this.txtHours,
            this.txtDistribution,
            this.txtJournal,
            this.txtWarrant,
            this.txtTax,
            this.txtEmployee,
            this.txtDateField,
            this.txtType,
            this.txtCheckNumber,
            this.txtVoid});
            this.Detail.Height = 0.2708333F;
            this.Detail.Name = "Detail";
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.2083333F;
            this.txtAccount.Left = 6.71875F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtAccount.Text = "Account";
            this.txtAccount.Top = 0.04166667F;
            this.txtAccount.Width = 1.0625F;
            // 
            // txtPayRate
            // 
            this.txtPayRate.Height = 0.2083333F;
            this.txtPayRate.Left = 7.84375F;
            this.txtPayRate.Name = "txtPayRate";
            this.txtPayRate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPayRate.Text = "Pay Rate";
            this.txtPayRate.Top = 0.04166667F;
            this.txtPayRate.Width = 0.59375F;
            // 
            // txtDistGrossPay
            // 
            this.txtDistGrossPay.Height = 0.2083333F;
            this.txtDistGrossPay.Left = 9F;
            this.txtDistGrossPay.Name = "txtDistGrossPay";
            this.txtDistGrossPay.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDistGrossPay.Text = "Dist Gross Pay";
            this.txtDistGrossPay.Top = 0.04166667F;
            this.txtDistGrossPay.Width = 0.90625F;
            // 
            // txtHours
            // 
            this.txtHours.Height = 0.2083333F;
            this.txtHours.Left = 8.5625F;
            this.txtHours.Name = "txtHours";
            this.txtHours.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtHours.Text = "Hours";
            this.txtHours.Top = 0.04166667F;
            this.txtHours.Width = 0.34375F;
            // 
            // txtDistribution
            // 
            this.txtDistribution.Height = 0.2083333F;
            this.txtDistribution.Left = 5.34375F;
            this.txtDistribution.Name = "txtDistribution";
            this.txtDistribution.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtDistribution.Text = "Distribution";
            this.txtDistribution.Top = 0.04166667F;
            this.txtDistribution.Width = 1F;
            // 
            // txtJournal
            // 
            this.txtJournal.Height = 0.2083333F;
            this.txtJournal.Left = 4.0625F;
            this.txtJournal.Name = "txtJournal";
            this.txtJournal.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtJournal.Text = "Journal";
            this.txtJournal.Top = 0.04166667F;
            this.txtJournal.Width = 0.5F;
            // 
            // txtWarrant
            // 
            this.txtWarrant.Height = 0.2083333F;
            this.txtWarrant.Left = 4.65625F;
            this.txtWarrant.Name = "txtWarrant";
            this.txtWarrant.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtWarrant.Text = "Warrant";
            this.txtWarrant.Top = 0.04166667F;
            this.txtWarrant.Width = 0.65625F;
            // 
            // txtTax
            // 
            this.txtTax.Height = 0.2083333F;
            this.txtTax.Left = 6.375F;
            this.txtTax.Name = "txtTax";
            this.txtTax.Style = "font-family: \'Tahoma\'; text-align: center";
            this.txtTax.Text = "Taxes";
            this.txtTax.Top = 0.04166667F;
            this.txtTax.Width = 0.28125F;
            // 
            // txtEmployee
            // 
            this.txtEmployee.Height = 0.2333333F;
            this.txtEmployee.Left = 0.06666667F;
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.Style = "font-family: \'Tahoma\'";
            this.txtEmployee.Text = "Employee";
            this.txtEmployee.Top = 0.03333334F;
            this.txtEmployee.Width = 1.766667F;
            // 
            // txtDateField
            // 
            this.txtDateField.Height = 0.2333333F;
            this.txtDateField.Left = 1.866667F;
            this.txtDateField.Name = "txtDateField";
            this.txtDateField.Style = "font-family: \'Tahoma\'";
            this.txtDateField.Text = "Date";
            this.txtDateField.Top = 0.03333334F;
            this.txtDateField.Width = 0.8F;
            // 
            // txtType
            // 
            this.txtType.Height = 0.2F;
            this.txtType.Left = 2.666667F;
            this.txtType.Name = "txtType";
            this.txtType.Style = "font-family: \'Tahoma\'";
            this.txtType.Text = null;
            this.txtType.Top = 0.03333334F;
            this.txtType.Width = 0.3F;
            // 
            // txtCheckNumber
            // 
            this.txtCheckNumber.Height = 0.2083333F;
            this.txtCheckNumber.Left = 3.3125F;
            this.txtCheckNumber.Name = "txtCheckNumber";
            this.txtCheckNumber.Style = "font-family: \'Tahoma\'";
            this.txtCheckNumber.Text = null;
            this.txtCheckNumber.Top = 0.04166667F;
            this.txtCheckNumber.Width = 0.5625F;
            // 
            // txtVoid
            // 
            this.txtVoid.Height = 0.2F;
            this.txtVoid.Left = 3.066667F;
            this.txtVoid.Name = "txtVoid";
            this.txtVoid.Style = "font-family: \'Tahoma\'";
            this.txtVoid.Text = "Void";
            this.txtVoid.Top = 0.03333334F;
            this.txtVoid.Width = 0.2F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSort,
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.Label3,
            this.txtTime,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Field12,
            this.Field13,
            this.Line12,
            this.lblVoid});
            this.PageHeader.Height = 1.447917F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblSort
            // 
            this.lblSort.Height = 0.2083333F;
            this.lblSort.HyperLink = null;
            this.lblSort.Left = 0.0625F;
            this.lblSort.Name = "lblSort";
            this.lblSort.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 8.5pt; font" +
    "-weight: bold; text-align: center";
            this.lblSort.Text = null;
            this.lblSort.Top = 0.25F;
            this.lblSort.Width = 9.78125F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2083333F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.0625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 10pt; font-" +
    "weight: bold; text-align: center";
            this.Label1.Text = "PAYROLL CHECK HISTORY";
            this.Label1.Top = 0.04166667F;
            this.Label1.Width = 9.78125F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1666667F;
            this.txtMuniName.Left = 0.0625F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0.08333334F;
            this.txtMuniName.Width = 1.4375F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2083333F;
            this.txtDate.Left = 8.40625F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.04166667F;
            this.txtDate.Width = 1.4375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2083333F;
            this.txtPage.Left = 8.40625F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.25F;
            this.txtPage.Width = 1.4375F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2083333F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.0625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; font-size: 8.5pt; font" +
    "-weight: bold; text-align: center";
            this.Label3.Text = "Complete File";
            this.Label3.Top = 0.4583333F;
            this.Label3.Width = 9.78125F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.2083333F;
            this.txtTime.Left = 0.0625F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 1.4375F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.2083333F;
            this.Field1.Left = 0.0625F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'";
            this.Field1.Text = "Employee";
            this.Field1.Top = 1.208333F;
            this.Field1.Width = 0.9375F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.2333333F;
            this.Field2.Left = 1.866667F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \'Tahoma\'";
            this.Field2.Text = "Date";
            this.Field2.Top = 1.2F;
            this.Field2.Width = 0.8F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.2333333F;
            this.Field3.Left = 2.666667F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'";
            this.Field3.Text = "Type";
            this.Field3.Top = 1.2F;
            this.Field3.Width = 0.4F;
            // 
            // Field5
            // 
            this.Field5.Height = 0.2083333F;
            this.Field5.Left = 3.3125F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "font-family: \'Tahoma\'";
            this.Field5.Text = "Check #";
            this.Field5.Top = 1.208333F;
            this.Field5.Width = 0.65625F;
            // 
            // Field6
            // 
            this.Field6.Height = 0.2083333F;
            this.Field6.Left = 5.34375F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \'Tahoma\'; text-align: left";
            this.Field6.Text = "Pay Category";
            this.Field6.Top = 1.208333F;
            this.Field6.Width = 1F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.2083333F;
            this.Field7.Left = 4F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \'Tahoma\'; text-align: right";
            this.Field7.Text = "Journal #";
            this.Field7.Top = 1.208333F;
            this.Field7.Width = 0.625F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.2083333F;
            this.Field8.Left = 4.625F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-family: \'Tahoma\'; text-align: right";
            this.Field8.Text = "Warrant #";
            this.Field8.Top = 1.208333F;
            this.Field8.Width = 0.6875F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.2083333F;
            this.Field9.Left = 6.375F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-family: \'Tahoma\'; text-align: center";
            this.Field9.Text = "Tax";
            this.Field9.Top = 1.208F;
            this.Field9.Width = 0.3127503F;
            // 
            // Field10
            // 
            this.Field10.Height = 0.2083333F;
            this.Field10.Left = 6.71875F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "font-family: \'Tahoma\'; text-align: center";
            this.Field10.Text = "Account";
            this.Field10.Top = 1.208333F;
            this.Field10.Width = 0.6875F;
            // 
            // Field11
            // 
            this.Field11.Height = 0.2083333F;
            this.Field11.Left = 7.8125F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "font-family: \'Tahoma\'; text-align: right";
            this.Field11.Text = "Pay Rate";
            this.Field11.Top = 1.208333F;
            this.Field11.Width = 0.625F;
            // 
            // Field12
            // 
            this.Field12.Height = 0.2083333F;
            this.Field12.Left = 8.96875F;
            this.Field12.Name = "Field12";
            this.Field12.Style = "font-family: \'Tahoma\'; text-align: right";
            this.Field12.Text = "Dist Gross Pay";
            this.Field12.Top = 1.208333F;
            this.Field12.Width = 0.9375F;
            // 
            // Field13
            // 
            this.Field13.Height = 0.2083333F;
            this.Field13.Left = 8.5F;
            this.Field13.Name = "Field13";
            this.Field13.Style = "font-family: \'Tahoma\'; text-align: left";
            this.Field13.Text = "Hours";
            this.Field13.Top = 1.208333F;
            this.Field13.Width = 0.46875F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 1.416667F;
            this.Line12.Width = 9.875F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 9.875F;
            this.Line12.Y1 = 1.416667F;
            this.Line12.Y2 = 1.416667F;
            // 
            // lblVoid
            // 
            this.lblVoid.Height = 0.2F;
            this.lblVoid.Left = 3F;
            this.lblVoid.Name = "lblVoid";
            this.lblVoid.Style = "font-family: \'Tahoma\'";
            this.lblVoid.Text = "Void";
            this.lblVoid.Top = 1.2F;
            this.lblVoid.Width = 0.3666667F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptCheckListingDistribution
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.916667F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistGrossPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarrant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVoid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistribution;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVoid;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSort;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblVoid;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
