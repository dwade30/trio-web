//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTerminatedEmployees.
	/// </summary>
	public partial class rptTerminatedEmployees : BaseSectionReport
	{
		public rptTerminatedEmployees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Terminated Employee Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTerminatedEmployees InstancePtr
		{
			get
			{
				return (rptTerminatedEmployees)Sys.GetInstance(typeof(rptTerminatedEmployees));
			}
		}

		protected rptTerminatedEmployees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsPayTotals?.Dispose();
				rsEmployees?.Dispose();
                rsPayTotals = null;
                rsEmployees = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTerminatedEmployees	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       November 2nd, 2005
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;
		
		clsDRWrapper rsPayTotals = new clsDRWrapper();
		clsDRWrapper rsEmployees = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			TryAgain:
			;
			if (rsEmployees.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				chkDeductions.Checked = true;
				chkMatches.Checked = true;
				chkPayTotals.Checked = true;
				chkTerminate.Checked = false;
				if (modCoreysSweeterCode.GetCYTDDeductionTotal(-1, rsEmployees.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate) > 0 || modCoreysSweeterCode.GetFYTDDeductionTotal(-1, rsEmployees.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate) > 0)
				{
					chkDeductions.Checked = false;
				}
				Matches:
				;
				if (modCoreysSweeterCode.GetCYTDMatchTotal(-1, rsEmployees.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate) > 0 || modCoreysSweeterCode.GetFYTDMatchTotal(-1, rsEmployees.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate) > 0)
				{
					chkMatches.Checked = false;
				}
				PayTotals:
				;
				DateTime dtStart;
				// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
				DateTime dtDate;
				dtDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
				dtStart = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtStart) > 0)
				{
					dtStart = dtDate;
				}
				rsPayTotals.OpenRecordset("select top 1 ID from tblcheckdetail where checkvoid = 0 and employeenumber = '" + rsEmployees.Get_Fields("employeenumber") + "' and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ", "twpy0000.vb1");
				if (!rsPayTotals.EndOfFile())
				{
					chkPayTotals.Checked = false;
				}
				Finished:
				;
				txtNumber.Text = rsEmployees.Get_Fields_String("EmployeeNumber");
				txtName.Text = rsEmployees.Get_Fields_String("FirstName") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployees.Get_Fields_String("MiddleName"))) + " " + rsEmployees.Get_Fields_String("LastName");
				if (chkDeductions.Checked && chkMatches.Checked && chkPayTotals.Checked)
					chkTerminate.Checked = true;
				if (!rsEmployees.EndOfFile())
					rsEmployees.MoveNext();
				if (!rsEmployees.EndOfFile())
				{
					if (chkTerminate.Checked)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto TryAgain;
					}
				}
				else
				{
					if (chkTerminate.Checked)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, System.EventArgs e)
		{
			//this.ActiveReport_Initialize();
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsEmployees.OpenRecordset("Select * from tblEmployeeMaster Where Status = 'Terminated' or status = 'Resigned' Order by EmployeeNumber");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Purge Terminated/Resigned Employees";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

	
	}
}