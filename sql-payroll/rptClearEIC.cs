//TODO - Delete Report
//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptClearEIC.
	/// </summary>
	public partial class rptClearEIC : BaseSectionReport
	{
		public rptClearEIC()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Clear EIC Master Field";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptClearEIC InstancePtr
		{
			get
			{
				return (rptClearEIC)Sys.GetInstance(typeof(rptClearEIC));
			}
		}

		protected rptClearEIC _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMaster?.Dispose();
                rsMaster = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptClearEIC	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsMaster = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsMaster.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			NextRecord:
			;
			if (!rsMaster.EndOfFile())
			{
				if (FCConvert.ToString(rsMaster.Get_Fields_String("EICCode")) == "No")
				{
					if (!rsMaster.EndOfFile())
					{
						rsMaster.MoveNext();
						goto NextRecord;
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
				else
				{
					rsMaster.Edit();
					rsMaster.Set_Fields("EICCode", "No");
					rsMaster.Update();
					txtEmployeeNumber.Text = rsMaster.Get_Fields_String("EmployeeNumber");
					txtEmployeeName.Text = rsMaster.Get_Fields_String("FirstName") + " " + rsMaster.Get_Fields_String("LastName");
				}
			}
			if (!rsMaster.EndOfFile())
				rsMaster.MoveNext();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsMaster.OpenRecordset("Select * from tblEmployeeMaster Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// lblCaption.Caption = "Pay Date:  " & gdatCurrentPayDate & "    " & "Run ID: " & gintCurrentPayRun
			txtPage.Text = "Page 1";
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		
	}
}
