﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2C.
	/// </summary>
	partial class rptW2C
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2C));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtW2Type = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSNNameCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirstName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLastName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersFedEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIncorrectSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIncorrectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevAllocatedTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrAllocatedTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12A = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12A = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStatutory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevRetirementPlan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevThirdParty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStatutory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrRetirementPlan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrThirdParty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12B = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12B = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12C = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12C = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12D = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12D = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev14C = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr14C = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev14B = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr14B = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev14A = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr14A = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12ACode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12BCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12CCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrev12DCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12ACode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12BCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12CCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurr12DCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateID2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateID2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Type)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSNNameCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersFedEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncorrectSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncorrectName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevAllocatedTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrAllocatedTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12A)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12A)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStatutory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevRetirementPlan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevThirdParty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStatutory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrRetirementPlan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrThirdParty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12B)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12B)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12C)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12C)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12D)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12D)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14C)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14C)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14B)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14B)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14A)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14A)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12ACode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12BCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12CCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12DCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12ACode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12BCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12CCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12DCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateID2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateID2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtYear,
            this.txtW2Type,
            this.txtSSN,
            this.txtSSNNameCheck,
            this.txtFirstName,
            this.txtLastName,
            this.txtDesig,
            this.txtEmployersFedEIN,
            this.txtAddress1,
            this.txtAddress2,
            this.txtAddress3,
            this.txtEmployerAddress1,
            this.txtEmployerAddress2,
            this.txtEmployerAddress3,
            this.txtEmployerName,
            this.txtIncorrectSSN,
            this.txtIncorrectName,
            this.txtPrevWages,
            this.txtCurrWages,
            this.txtPrevSSWages,
            this.txtCurrSSWages,
            this.txtPrevMedicare,
            this.txtCurrMedicare,
            this.txtPrevSSTips,
            this.txtCurrSSTips,
            this.txtPrevNonQualifiedPlans,
            this.txtCurrNonQualifiedPlans,
            this.txtPrevFedTax,
            this.txtCurrFedTax,
            this.txtPrevSSTax,
            this.txtCurrSSTax,
            this.txtPrevMedTax,
            this.txtCurrMedTax,
            this.txtPrevAllocatedTips,
            this.txtCurrAllocatedTips,
            this.txtPrevDependentCare,
            this.txtCurrDependentCare,
            this.txtPrev12A,
            this.txtCurr12A,
            this.txtPrevStatutory,
            this.txtPrevRetirementPlan,
            this.txtPrevThirdParty,
            this.txtCurrStatutory,
            this.txtCurrRetirementPlan,
            this.txtCurrThirdParty,
            this.txtPrev12B,
            this.txtCurr12B,
            this.txtPrev12C,
            this.txtCurr12C,
            this.txtPrev12D,
            this.txtCurr12D,
            this.txtPrev14C,
            this.txtCurr14C,
            this.txtPrev14B,
            this.txtCurr14B,
            this.txtPrev14A,
            this.txtCurr14A,
            this.txtPrev12ACode,
            this.txtPrev12BCode,
            this.txtPrev12CCode,
            this.txtPrev12DCode,
            this.txtCurr12ACode,
            this.txtCurr12BCode,
            this.txtCurr12CCode,
            this.txtCurr12DCode,
            this.txtPrevStateName,
            this.txtCurrStateName,
            this.txtPrevStateID,
            this.txtCurrStateID,
            this.txtPrevStateWages,
            this.txtCurrStateWages,
            this.txtPrevStateTax,
            this.txtCurrStateTax,
            this.txtPrevStateName2,
            this.txtCurrStateName2,
            this.txtPrevStateID2,
            this.txtCurrStateID2,
            this.txtPrevStateWages2,
            this.txtCurrStateWages2,
            this.txtPrevStateTax2,
            this.txtCurrStateTax2});
			this.Detail.Height = 9.75F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1666667F;
			this.txtYear.Left = 4.0625F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "text-align: left";
			this.txtYear.Text = null;
			this.txtYear.Top = 0.8333333F;
			this.txtYear.Width = 0.6145833F;
			// 
			// txtW2Type
			// 
			this.txtW2Type.Height = 0.1666667F;
			this.txtW2Type.Left = 4.875F;
			this.txtW2Type.Name = "txtW2Type";
			this.txtW2Type.Text = null;
			this.txtW2Type.Top = 0.8333333F;
			this.txtW2Type.Width = 0.2395833F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1666667F;
			this.txtSSN.Left = 6.0625F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 0.8333333F;
			this.txtSSN.Width = 1.15625F;
			// 
			// txtSSNNameCheck
			// 
			this.txtSSNNameCheck.Height = 0.1666667F;
			this.txtSSNNameCheck.Left = 7.197917F;
			this.txtSSNNameCheck.Name = "txtSSNNameCheck";
			this.txtSSNNameCheck.Style = "text-align: center";
			this.txtSSNNameCheck.Text = "X";
			this.txtSSNNameCheck.Top = 1.145833F;
			this.txtSSNNameCheck.Visible = false;
			this.txtSSNNameCheck.Width = 0.2395833F;
			// 
			// txtFirstName
			// 
			this.txtFirstName.Height = 0.1666667F;
			this.txtFirstName.Left = 3.875F;
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Text = null;
			this.txtFirstName.Top = 2.229167F;
			this.txtFirstName.Width = 1.510417F;
			// 
			// txtLastName
			// 
			this.txtLastName.Height = 0.1666667F;
			this.txtLastName.Left = 5.6875F;
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Text = null;
			this.txtLastName.Top = 2.229167F;
			this.txtLastName.Width = 1.15625F;
			// 
			// txtDesig
			// 
			this.txtDesig.Height = 0.1666667F;
			this.txtDesig.Left = 7F;
			this.txtDesig.Name = "txtDesig";
			this.txtDesig.Style = "text-align: right";
			this.txtDesig.Text = null;
			this.txtDesig.Top = 2.229167F;
			this.txtDesig.Width = 0.40625F;
			// 
			// txtEmployersFedEIN
			// 
			this.txtEmployersFedEIN.Height = 0.1666667F;
			this.txtEmployersFedEIN.Left = 0.3125F;
			this.txtEmployersFedEIN.Name = "txtEmployersFedEIN";
			this.txtEmployersFedEIN.Text = null;
			this.txtEmployersFedEIN.Top = 1.944444F;
			this.txtEmployersFedEIN.Width = 1.84375F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.1666667F;
			this.txtAddress1.Left = 3.875F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 2.4375F;
			this.txtAddress1.Width = 2.78125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1666667F;
			this.txtAddress2.Left = 3.875F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 2.604167F;
			this.txtAddress2.Width = 2.78125F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.1666667F;
			this.txtAddress3.Left = 3.875F;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 2.770833F;
			this.txtAddress3.Width = 2.78125F;
			// 
			// txtEmployerAddress1
			// 
			this.txtEmployerAddress1.Height = 0.1666667F;
			this.txtEmployerAddress1.Left = 0.1875F;
			this.txtEmployerAddress1.Name = "txtEmployerAddress1";
			this.txtEmployerAddress1.Text = null;
			this.txtEmployerAddress1.Top = 0.9791667F;
			this.txtEmployerAddress1.Width = 2.78125F;
			// 
			// txtEmployerAddress2
			// 
			this.txtEmployerAddress2.Height = 0.1666667F;
			this.txtEmployerAddress2.Left = 0.1875F;
			this.txtEmployerAddress2.Name = "txtEmployerAddress2";
			this.txtEmployerAddress2.Text = null;
			this.txtEmployerAddress2.Top = 1.145833F;
			this.txtEmployerAddress2.Width = 2.78125F;
			// 
			// txtEmployerAddress3
			// 
			this.txtEmployerAddress3.Height = 0.1666667F;
			this.txtEmployerAddress3.Left = 0.1875F;
			this.txtEmployerAddress3.Name = "txtEmployerAddress3";
			this.txtEmployerAddress3.Text = null;
			this.txtEmployerAddress3.Top = 1.3125F;
			this.txtEmployerAddress3.Width = 2.78125F;
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.Height = 0.1666667F;
			this.txtEmployerName.Left = 0.1875F;
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Text = null;
			this.txtEmployerName.Top = 0.8125F;
			this.txtEmployerName.Width = 2.78125F;
			// 
			// txtIncorrectSSN
			// 
			this.txtIncorrectSSN.Height = 0.1666667F;
			this.txtIncorrectSSN.Left = 4.0625F;
			this.txtIncorrectSSN.Name = "txtIncorrectSSN";
			this.txtIncorrectSSN.Text = null;
			this.txtIncorrectSSN.Top = 1.625F;
			this.txtIncorrectSSN.Width = 1.260417F;
			// 
			// txtIncorrectName
			// 
			this.txtIncorrectName.Height = 0.1666667F;
			this.txtIncorrectName.Left = 4.0625F;
			this.txtIncorrectName.Name = "txtIncorrectName";
			this.txtIncorrectName.Text = null;
			this.txtIncorrectName.Top = 1.944444F;
			this.txtIncorrectName.Width = 2.510417F;
			// 
			// txtPrevWages
			// 
			this.txtPrevWages.Height = 0.1666667F;
			this.txtPrevWages.Left = 0.0625F;
			this.txtPrevWages.Name = "txtPrevWages";
			this.txtPrevWages.Style = "text-align: right";
			this.txtPrevWages.Text = null;
			this.txtPrevWages.Top = 3.3125F;
			this.txtPrevWages.Width = 1.760417F;
			// 
			// txtCurrWages
			// 
			this.txtCurrWages.Height = 0.1666667F;
			this.txtCurrWages.Left = 2.0625F;
			this.txtCurrWages.Name = "txtCurrWages";
			this.txtCurrWages.Style = "text-align: right";
			this.txtCurrWages.Text = null;
			this.txtCurrWages.Top = 3.3125F;
			this.txtCurrWages.Width = 1.697917F;
			// 
			// txtPrevSSWages
			// 
			this.txtPrevSSWages.Height = 0.1666667F;
			this.txtPrevSSWages.Left = 0.0625F;
			this.txtPrevSSWages.Name = "txtPrevSSWages";
			this.txtPrevSSWages.Style = "text-align: right";
			this.txtPrevSSWages.Text = null;
			this.txtPrevSSWages.Top = 3.583333F;
			this.txtPrevSSWages.Width = 1.760417F;
			// 
			// txtCurrSSWages
			// 
			this.txtCurrSSWages.Height = 0.1666667F;
			this.txtCurrSSWages.Left = 2.0625F;
			this.txtCurrSSWages.Name = "txtCurrSSWages";
			this.txtCurrSSWages.Style = "text-align: right";
			this.txtCurrSSWages.Text = null;
			this.txtCurrSSWages.Top = 3.583333F;
			this.txtCurrSSWages.Width = 1.697917F;
			// 
			// txtPrevMedicare
			// 
			this.txtPrevMedicare.Height = 0.1666667F;
			this.txtPrevMedicare.Left = 0.0625F;
			this.txtPrevMedicare.Name = "txtPrevMedicare";
			this.txtPrevMedicare.Style = "text-align: right";
			this.txtPrevMedicare.Text = null;
			this.txtPrevMedicare.Top = 3.916667F;
			this.txtPrevMedicare.Width = 1.760417F;
			// 
			// txtCurrMedicare
			// 
			this.txtCurrMedicare.Height = 0.1666667F;
			this.txtCurrMedicare.Left = 2.0625F;
			this.txtCurrMedicare.Name = "txtCurrMedicare";
			this.txtCurrMedicare.Style = "text-align: right";
			this.txtCurrMedicare.Text = null;
			this.txtCurrMedicare.Top = 3.916667F;
			this.txtCurrMedicare.Width = 1.697917F;
			// 
			// txtPrevSSTips
			// 
			this.txtPrevSSTips.Height = 0.1666667F;
			this.txtPrevSSTips.Left = 0.0625F;
			this.txtPrevSSTips.Name = "txtPrevSSTips";
			this.txtPrevSSTips.Style = "text-align: right";
			this.txtPrevSSTips.Text = null;
			this.txtPrevSSTips.Top = 4.25F;
			this.txtPrevSSTips.Width = 1.760417F;
			// 
			// txtCurrSSTips
			// 
			this.txtCurrSSTips.Height = 0.1666667F;
			this.txtCurrSSTips.Left = 2.0625F;
			this.txtCurrSSTips.Name = "txtCurrSSTips";
			this.txtCurrSSTips.Style = "text-align: right";
			this.txtCurrSSTips.Text = null;
			this.txtCurrSSTips.Top = 4.25F;
			this.txtCurrSSTips.Width = 1.697917F;
			// 
			// txtPrevNonQualifiedPlans
			// 
			this.txtPrevNonQualifiedPlans.Height = 0.1666667F;
			this.txtPrevNonQualifiedPlans.Left = 0.0625F;
			this.txtPrevNonQualifiedPlans.Name = "txtPrevNonQualifiedPlans";
			this.txtPrevNonQualifiedPlans.Style = "text-align: right";
			this.txtPrevNonQualifiedPlans.Text = null;
			this.txtPrevNonQualifiedPlans.Top = 4.833333F;
			this.txtPrevNonQualifiedPlans.Width = 1.760417F;
			// 
			// txtCurrNonQualifiedPlans
			// 
			this.txtCurrNonQualifiedPlans.Height = 0.1666667F;
			this.txtCurrNonQualifiedPlans.Left = 2.0625F;
			this.txtCurrNonQualifiedPlans.Name = "txtCurrNonQualifiedPlans";
			this.txtCurrNonQualifiedPlans.Style = "text-align: right";
			this.txtCurrNonQualifiedPlans.Text = null;
			this.txtCurrNonQualifiedPlans.Top = 4.833333F;
			this.txtCurrNonQualifiedPlans.Width = 1.697917F;
			// 
			// txtPrevFedTax
			// 
			this.txtPrevFedTax.Height = 0.1666667F;
			this.txtPrevFedTax.Left = 4.0625F;
			this.txtPrevFedTax.Name = "txtPrevFedTax";
			this.txtPrevFedTax.Style = "text-align: right";
			this.txtPrevFedTax.Text = null;
			this.txtPrevFedTax.Top = 3.3125F;
			this.txtPrevFedTax.Width = 1.572917F;
			// 
			// txtCurrFedTax
			// 
			this.txtCurrFedTax.Height = 0.1666667F;
			this.txtCurrFedTax.Left = 5.9375F;
			this.txtCurrFedTax.Name = "txtCurrFedTax";
			this.txtCurrFedTax.Style = "text-align: right";
			this.txtCurrFedTax.Text = null;
			this.txtCurrFedTax.Top = 3.3125F;
			this.txtCurrFedTax.Width = 1.447917F;
			// 
			// txtPrevSSTax
			// 
			this.txtPrevSSTax.Height = 0.1666667F;
			this.txtPrevSSTax.Left = 4.0625F;
			this.txtPrevSSTax.Name = "txtPrevSSTax";
			this.txtPrevSSTax.Style = "text-align: right";
			this.txtPrevSSTax.Text = null;
			this.txtPrevSSTax.Top = 3.583333F;
			this.txtPrevSSTax.Width = 1.572917F;
			// 
			// txtCurrSSTax
			// 
			this.txtCurrSSTax.Height = 0.1666667F;
			this.txtCurrSSTax.Left = 5.9375F;
			this.txtCurrSSTax.Name = "txtCurrSSTax";
			this.txtCurrSSTax.Style = "text-align: right";
			this.txtCurrSSTax.Text = null;
			this.txtCurrSSTax.Top = 3.583333F;
			this.txtCurrSSTax.Width = 1.447917F;
			// 
			// txtPrevMedTax
			// 
			this.txtPrevMedTax.Height = 0.1666667F;
			this.txtPrevMedTax.Left = 4.0625F;
			this.txtPrevMedTax.Name = "txtPrevMedTax";
			this.txtPrevMedTax.Style = "text-align: right";
			this.txtPrevMedTax.Text = null;
			this.txtPrevMedTax.Top = 3.916667F;
			this.txtPrevMedTax.Width = 1.572917F;
			// 
			// txtCurrMedTax
			// 
			this.txtCurrMedTax.Height = 0.1666667F;
			this.txtCurrMedTax.Left = 5.9375F;
			this.txtCurrMedTax.Name = "txtCurrMedTax";
			this.txtCurrMedTax.Style = "text-align: right";
			this.txtCurrMedTax.Text = null;
			this.txtCurrMedTax.Top = 3.916667F;
			this.txtCurrMedTax.Width = 1.447917F;
			// 
			// txtPrevAllocatedTips
			// 
			this.txtPrevAllocatedTips.Height = 0.1666667F;
			this.txtPrevAllocatedTips.Left = 4.0625F;
			this.txtPrevAllocatedTips.Name = "txtPrevAllocatedTips";
			this.txtPrevAllocatedTips.Style = "text-align: right";
			this.txtPrevAllocatedTips.Text = null;
			this.txtPrevAllocatedTips.Top = 4.25F;
			this.txtPrevAllocatedTips.Width = 1.572917F;
			// 
			// txtCurrAllocatedTips
			// 
			this.txtCurrAllocatedTips.Height = 0.1666667F;
			this.txtCurrAllocatedTips.Left = 5.9375F;
			this.txtCurrAllocatedTips.Name = "txtCurrAllocatedTips";
			this.txtCurrAllocatedTips.Style = "text-align: right";
			this.txtCurrAllocatedTips.Text = null;
			this.txtCurrAllocatedTips.Top = 4.25F;
			this.txtCurrAllocatedTips.Width = 1.447917F;
			// 
			// txtPrevDependentCare
			// 
			this.txtPrevDependentCare.Height = 0.1666667F;
			this.txtPrevDependentCare.Left = 4.0625F;
			this.txtPrevDependentCare.Name = "txtPrevDependentCare";
			this.txtPrevDependentCare.Style = "text-align: right";
			this.txtPrevDependentCare.Text = null;
			this.txtPrevDependentCare.Top = 4.541667F;
			this.txtPrevDependentCare.Width = 1.572917F;
			// 
			// txtCurrDependentCare
			// 
			this.txtCurrDependentCare.Height = 0.1666667F;
			this.txtCurrDependentCare.Left = 5.9375F;
			this.txtCurrDependentCare.Name = "txtCurrDependentCare";
			this.txtCurrDependentCare.Style = "text-align: right";
			this.txtCurrDependentCare.Text = null;
			this.txtCurrDependentCare.Top = 4.541667F;
			this.txtCurrDependentCare.Width = 1.447917F;
			// 
			// txtPrev12A
			// 
			this.txtPrev12A.Height = 0.1666667F;
			this.txtPrev12A.Left = 4.375F;
			this.txtPrev12A.Name = "txtPrev12A";
			this.txtPrev12A.Style = "text-align: right";
			this.txtPrev12A.Text = null;
			this.txtPrev12A.Top = 4.833333F;
			this.txtPrev12A.Width = 1.260417F;
			// 
			// txtCurr12A
			// 
			this.txtCurr12A.Height = 0.1666667F;
			this.txtCurr12A.Left = 6.25F;
			this.txtCurr12A.Name = "txtCurr12A";
			this.txtCurr12A.Style = "text-align: right";
			this.txtCurr12A.Text = null;
			this.txtCurr12A.Top = 4.833333F;
			this.txtCurr12A.Width = 1.135417F;
			// 
			// txtPrevStatutory
			// 
			this.txtPrevStatutory.Height = 0.1666667F;
			this.txtPrevStatutory.Left = 0.1666667F;
			this.txtPrevStatutory.Name = "txtPrevStatutory";
			this.txtPrevStatutory.Style = "text-align: center; vertical-align: top";
			this.txtPrevStatutory.Text = "X";
			this.txtPrevStatutory.Top = 5.21875F;
			this.txtPrevStatutory.Width = 0.2395833F;
			// 
			// txtPrevRetirementPlan
			// 
			this.txtPrevRetirementPlan.Height = 0.1666667F;
			this.txtPrevRetirementPlan.Left = 0.7291667F;
			this.txtPrevRetirementPlan.Name = "txtPrevRetirementPlan";
			this.txtPrevRetirementPlan.Style = "text-align: center";
			this.txtPrevRetirementPlan.Text = "X";
			this.txtPrevRetirementPlan.Top = 5.21875F;
			this.txtPrevRetirementPlan.Width = 0.2395833F;
			// 
			// txtPrevThirdParty
			// 
			this.txtPrevThirdParty.Height = 0.1666667F;
			this.txtPrevThirdParty.Left = 1.229167F;
			this.txtPrevThirdParty.Name = "txtPrevThirdParty";
			this.txtPrevThirdParty.Style = "text-align: right";
			this.txtPrevThirdParty.Text = "X";
			this.txtPrevThirdParty.Top = 5.21875F;
			this.txtPrevThirdParty.Width = 0.2395833F;
			// 
			// txtCurrStatutory
			// 
			this.txtCurrStatutory.Height = 0.1666667F;
			this.txtCurrStatutory.Left = 2.104167F;
			this.txtCurrStatutory.Name = "txtCurrStatutory";
			this.txtCurrStatutory.Style = "text-align: center";
			this.txtCurrStatutory.Text = "X";
			this.txtCurrStatutory.Top = 5.21875F;
			this.txtCurrStatutory.Width = 0.2395833F;
			// 
			// txtCurrRetirementPlan
			// 
			this.txtCurrRetirementPlan.Height = 0.1666667F;
			this.txtCurrRetirementPlan.Left = 2.625F;
			this.txtCurrRetirementPlan.Name = "txtCurrRetirementPlan";
			this.txtCurrRetirementPlan.Style = "text-align: center";
			this.txtCurrRetirementPlan.Text = "X";
			this.txtCurrRetirementPlan.Top = 5.21875F;
			this.txtCurrRetirementPlan.Width = 0.2395833F;
			// 
			// txtCurrThirdParty
			// 
			this.txtCurrThirdParty.Height = 0.1666667F;
			this.txtCurrThirdParty.Left = 3.166667F;
			this.txtCurrThirdParty.Name = "txtCurrThirdParty";
			this.txtCurrThirdParty.Style = "text-align: right";
			this.txtCurrThirdParty.Text = "X";
			this.txtCurrThirdParty.Top = 5.21875F;
			this.txtCurrThirdParty.Width = 0.2395833F;
			// 
			// txtPrev12B
			// 
			this.txtPrev12B.Height = 0.1666667F;
			this.txtPrev12B.Left = 4.375F;
			this.txtPrev12B.Name = "txtPrev12B";
			this.txtPrev12B.Style = "text-align: right";
			this.txtPrev12B.Text = null;
			this.txtPrev12B.Top = 5.1875F;
			this.txtPrev12B.Width = 1.260417F;
			// 
			// txtCurr12B
			// 
			this.txtCurr12B.Height = 0.1666667F;
			this.txtCurr12B.Left = 6.25F;
			this.txtCurr12B.Name = "txtCurr12B";
			this.txtCurr12B.Style = "text-align: right";
			this.txtCurr12B.Text = null;
			this.txtCurr12B.Top = 5.1875F;
			this.txtCurr12B.Width = 1.135417F;
			// 
			// txtPrev12C
			// 
			this.txtPrev12C.Height = 0.1666667F;
			this.txtPrev12C.Left = 4.375F;
			this.txtPrev12C.Name = "txtPrev12C";
			this.txtPrev12C.Style = "text-align: right";
			this.txtPrev12C.Text = null;
			this.txtPrev12C.Top = 5.479167F;
			this.txtPrev12C.Width = 1.260417F;
			// 
			// txtCurr12C
			// 
			this.txtCurr12C.Height = 0.1666667F;
			this.txtCurr12C.Left = 6.25F;
			this.txtCurr12C.Name = "txtCurr12C";
			this.txtCurr12C.Style = "text-align: right";
			this.txtCurr12C.Text = null;
			this.txtCurr12C.Top = 5.479167F;
			this.txtCurr12C.Width = 1.135417F;
			// 
			// txtPrev12D
			// 
			this.txtPrev12D.Height = 0.1666667F;
			this.txtPrev12D.Left = 4.375F;
			this.txtPrev12D.Name = "txtPrev12D";
			this.txtPrev12D.Style = "text-align: right";
			this.txtPrev12D.Text = null;
			this.txtPrev12D.Top = 5.770833F;
			this.txtPrev12D.Width = 1.260417F;
			// 
			// txtCurr12D
			// 
			this.txtCurr12D.Height = 0.1666667F;
			this.txtCurr12D.Left = 6.25F;
			this.txtCurr12D.Name = "txtCurr12D";
			this.txtCurr12D.Style = "text-align: right";
			this.txtCurr12D.Text = null;
			this.txtCurr12D.Top = 5.770833F;
			this.txtCurr12D.Width = 1.135417F;
			// 
			// txtPrev14C
			// 
			this.txtPrev14C.Height = 0.19F;
			this.txtPrev14C.Left = 0.1875F;
			this.txtPrev14C.Name = "txtPrev14C";
			this.txtPrev14C.Style = "text-align: left";
			this.txtPrev14C.Text = null;
			this.txtPrev14C.Top = 5.84375F;
			this.txtPrev14C.Width = 1.697917F;
			// 
			// txtCurr14C
			// 
			this.txtCurr14C.Height = 0.19F;
			this.txtCurr14C.Left = 2.0625F;
			this.txtCurr14C.Name = "txtCurr14C";
			this.txtCurr14C.Style = "text-align: left";
			this.txtCurr14C.Text = null;
			this.txtCurr14C.Top = 5.84375F;
			this.txtCurr14C.Width = 1.635417F;
			// 
			// txtPrev14B
			// 
			this.txtPrev14B.Height = 0.19F;
			this.txtPrev14B.Left = 0.1875F;
			this.txtPrev14B.Name = "txtPrev14B";
			this.txtPrev14B.Style = "text-align: left";
			this.txtPrev14B.Text = null;
			this.txtPrev14B.Top = 5.697917F;
			this.txtPrev14B.Width = 1.697917F;
			// 
			// txtCurr14B
			// 
			this.txtCurr14B.Height = 0.19F;
			this.txtCurr14B.Left = 2.0625F;
			this.txtCurr14B.Name = "txtCurr14B";
			this.txtCurr14B.Style = "text-align: left";
			this.txtCurr14B.Text = null;
			this.txtCurr14B.Top = 5.697917F;
			this.txtCurr14B.Width = 1.635417F;
			// 
			// txtPrev14A
			// 
			this.txtPrev14A.Height = 0.19F;
			this.txtPrev14A.Left = 0.1875F;
			this.txtPrev14A.Name = "txtPrev14A";
			this.txtPrev14A.Style = "text-align: left";
			this.txtPrev14A.Text = null;
			this.txtPrev14A.Top = 5.552083F;
			this.txtPrev14A.Width = 1.697917F;
			// 
			// txtCurr14A
			// 
			this.txtCurr14A.Height = 0.19F;
			this.txtCurr14A.Left = 2.0625F;
			this.txtCurr14A.Name = "txtCurr14A";
			this.txtCurr14A.Style = "text-align: left";
			this.txtCurr14A.Text = null;
			this.txtCurr14A.Top = 5.552083F;
			this.txtCurr14A.Width = 1.635417F;
			// 
			// txtPrev12ACode
			// 
			this.txtPrev12ACode.Height = 0.1666667F;
			this.txtPrev12ACode.Left = 4.0625F;
			this.txtPrev12ACode.Name = "txtPrev12ACode";
			this.txtPrev12ACode.Style = "text-align: left";
			this.txtPrev12ACode.Text = null;
			this.txtPrev12ACode.Top = 4.833333F;
			this.txtPrev12ACode.Width = 0.34375F;
			// 
			// txtPrev12BCode
			// 
			this.txtPrev12BCode.Height = 0.1666667F;
			this.txtPrev12BCode.Left = 4.0625F;
			this.txtPrev12BCode.Name = "txtPrev12BCode";
			this.txtPrev12BCode.Style = "text-align: left";
			this.txtPrev12BCode.Text = null;
			this.txtPrev12BCode.Top = 5.1875F;
			this.txtPrev12BCode.Width = 0.34375F;
			// 
			// txtPrev12CCode
			// 
			this.txtPrev12CCode.Height = 0.1666667F;
			this.txtPrev12CCode.Left = 4.0625F;
			this.txtPrev12CCode.Name = "txtPrev12CCode";
			this.txtPrev12CCode.Style = "text-align: left";
			this.txtPrev12CCode.Text = null;
			this.txtPrev12CCode.Top = 5.479167F;
			this.txtPrev12CCode.Width = 0.34375F;
			// 
			// txtPrev12DCode
			// 
			this.txtPrev12DCode.Height = 0.1666667F;
			this.txtPrev12DCode.Left = 4.0625F;
			this.txtPrev12DCode.Name = "txtPrev12DCode";
			this.txtPrev12DCode.Style = "text-align: left";
			this.txtPrev12DCode.Text = null;
			this.txtPrev12DCode.Top = 5.770833F;
			this.txtPrev12DCode.Width = 0.34375F;
			// 
			// txtCurr12ACode
			// 
			this.txtCurr12ACode.Height = 0.1666667F;
			this.txtCurr12ACode.Left = 5.9375F;
			this.txtCurr12ACode.Name = "txtCurr12ACode";
			this.txtCurr12ACode.Style = "text-align: left";
			this.txtCurr12ACode.Text = null;
			this.txtCurr12ACode.Top = 4.833333F;
			this.txtCurr12ACode.Width = 0.34375F;
			// 
			// txtCurr12BCode
			// 
			this.txtCurr12BCode.Height = 0.1666667F;
			this.txtCurr12BCode.Left = 5.9375F;
			this.txtCurr12BCode.Name = "txtCurr12BCode";
			this.txtCurr12BCode.Style = "text-align: left";
			this.txtCurr12BCode.Text = null;
			this.txtCurr12BCode.Top = 5.1875F;
			this.txtCurr12BCode.Width = 0.34375F;
			// 
			// txtCurr12CCode
			// 
			this.txtCurr12CCode.Height = 0.1666667F;
			this.txtCurr12CCode.Left = 5.9375F;
			this.txtCurr12CCode.Name = "txtCurr12CCode";
			this.txtCurr12CCode.Style = "text-align: left";
			this.txtCurr12CCode.Text = null;
			this.txtCurr12CCode.Top = 5.479167F;
			this.txtCurr12CCode.Width = 0.34375F;
			// 
			// txtCurr12DCode
			// 
			this.txtCurr12DCode.Height = 0.1666667F;
			this.txtCurr12DCode.Left = 5.9375F;
			this.txtCurr12DCode.Name = "txtCurr12DCode";
			this.txtCurr12DCode.Style = "text-align: left";
			this.txtCurr12DCode.Text = null;
			this.txtCurr12DCode.Top = 5.770833F;
			this.txtCurr12DCode.Width = 0.34375F;
			// 
			// txtPrevStateName
			// 
			this.txtPrevStateName.Height = 0.1666667F;
			this.txtPrevStateName.Left = 0.1875F;
			this.txtPrevStateName.Name = "txtPrevStateName";
			this.txtPrevStateName.Style = "text-align: left";
			this.txtPrevStateName.Text = null;
			this.txtPrevStateName.Top = 6.604167F;
			this.txtPrevStateName.Width = 1.697917F;
			// 
			// txtCurrStateName
			// 
			this.txtCurrStateName.Height = 0.1666667F;
			this.txtCurrStateName.Left = 2.125F;
			this.txtCurrStateName.Name = "txtCurrStateName";
			this.txtCurrStateName.Style = "text-align: left";
			this.txtCurrStateName.Text = null;
			this.txtCurrStateName.Top = 6.604167F;
			this.txtCurrStateName.Width = 1.697917F;
			// 
			// txtPrevStateID
			// 
			this.txtPrevStateID.Height = 0.1666667F;
			this.txtPrevStateID.Left = 0.1875F;
			this.txtPrevStateID.Name = "txtPrevStateID";
			this.txtPrevStateID.Style = "text-align: left";
			this.txtPrevStateID.Text = null;
			this.txtPrevStateID.Top = 6.916667F;
			this.txtPrevStateID.Width = 1.697917F;
			// 
			// txtCurrStateID
			// 
			this.txtCurrStateID.Height = 0.1666667F;
			this.txtCurrStateID.Left = 2.125F;
			this.txtCurrStateID.Name = "txtCurrStateID";
			this.txtCurrStateID.Style = "text-align: left";
			this.txtCurrStateID.Text = null;
			this.txtCurrStateID.Top = 6.916667F;
			this.txtCurrStateID.Width = 1.697917F;
			// 
			// txtPrevStateWages
			// 
			this.txtPrevStateWages.Height = 0.1666667F;
			this.txtPrevStateWages.Left = 0.1875F;
			this.txtPrevStateWages.Name = "txtPrevStateWages";
			this.txtPrevStateWages.Style = "text-align: right";
			this.txtPrevStateWages.Text = null;
			this.txtPrevStateWages.Top = 7.25F;
			this.txtPrevStateWages.Width = 1.697917F;
			// 
			// txtCurrStateWages
			// 
			this.txtCurrStateWages.Height = 0.1666667F;
			this.txtCurrStateWages.Left = 2.0625F;
			this.txtCurrStateWages.Name = "txtCurrStateWages";
			this.txtCurrStateWages.Style = "text-align: right";
			this.txtCurrStateWages.Text = null;
			this.txtCurrStateWages.Top = 7.25F;
			this.txtCurrStateWages.Width = 1.697917F;
			// 
			// txtPrevStateTax
			// 
			this.txtPrevStateTax.Height = 0.1666667F;
			this.txtPrevStateTax.Left = 0.1875F;
			this.txtPrevStateTax.Name = "txtPrevStateTax";
			this.txtPrevStateTax.Style = "text-align: right";
			this.txtPrevStateTax.Text = null;
			this.txtPrevStateTax.Top = 7.541667F;
			this.txtPrevStateTax.Width = 1.697917F;
			// 
			// txtCurrStateTax
			// 
			this.txtCurrStateTax.Height = 0.1666667F;
			this.txtCurrStateTax.Left = 2.0625F;
			this.txtCurrStateTax.Name = "txtCurrStateTax";
			this.txtCurrStateTax.Style = "text-align: right";
			this.txtCurrStateTax.Text = null;
			this.txtCurrStateTax.Top = 7.541667F;
			this.txtCurrStateTax.Width = 1.697917F;
			// 
			// txtPrevStateName2
			// 
			this.txtPrevStateName2.Height = 0.1666667F;
			this.txtPrevStateName2.Left = 4.0625F;
			this.txtPrevStateName2.Name = "txtPrevStateName2";
			this.txtPrevStateName2.Style = "text-align: left";
			this.txtPrevStateName2.Text = null;
			this.txtPrevStateName2.Top = 6.604167F;
			this.txtPrevStateName2.Width = 1.572917F;
			// 
			// txtCurrStateName2
			// 
			this.txtCurrStateName2.Height = 0.1666667F;
			this.txtCurrStateName2.Left = 5.9375F;
			this.txtCurrStateName2.Name = "txtCurrStateName2";
			this.txtCurrStateName2.Style = "text-align: left";
			this.txtCurrStateName2.Text = null;
			this.txtCurrStateName2.Top = 6.604167F;
			this.txtCurrStateName2.Width = 1.447917F;
			// 
			// txtPrevStateID2
			// 
			this.txtPrevStateID2.Height = 0.1666667F;
			this.txtPrevStateID2.Left = 4.0625F;
			this.txtPrevStateID2.Name = "txtPrevStateID2";
			this.txtPrevStateID2.Style = "text-align: left";
			this.txtPrevStateID2.Text = null;
			this.txtPrevStateID2.Top = 6.916667F;
			this.txtPrevStateID2.Width = 1.572917F;
			// 
			// txtCurrStateID2
			// 
			this.txtCurrStateID2.Height = 0.1666667F;
			this.txtCurrStateID2.Left = 5.9375F;
			this.txtCurrStateID2.Name = "txtCurrStateID2";
			this.txtCurrStateID2.Style = "text-align: left";
			this.txtCurrStateID2.Text = null;
			this.txtCurrStateID2.Top = 6.916667F;
			this.txtCurrStateID2.Width = 1.447917F;
			// 
			// txtPrevStateWages2
			// 
			this.txtPrevStateWages2.Height = 0.1666667F;
			this.txtPrevStateWages2.Left = 4.0625F;
			this.txtPrevStateWages2.Name = "txtPrevStateWages2";
			this.txtPrevStateWages2.Style = "text-align: right";
			this.txtPrevStateWages2.Text = null;
			this.txtPrevStateWages2.Top = 7.25F;
			this.txtPrevStateWages2.Width = 1.572917F;
			// 
			// txtCurrStateWages2
			// 
			this.txtCurrStateWages2.Height = 0.1666667F;
			this.txtCurrStateWages2.Left = 5.9375F;
			this.txtCurrStateWages2.Name = "txtCurrStateWages2";
			this.txtCurrStateWages2.Style = "text-align: right";
			this.txtCurrStateWages2.Text = null;
			this.txtCurrStateWages2.Top = 7.25F;
			this.txtCurrStateWages2.Width = 1.447917F;
			// 
			// txtPrevStateTax2
			// 
			this.txtPrevStateTax2.Height = 0.1666667F;
			this.txtPrevStateTax2.Left = 4.0625F;
			this.txtPrevStateTax2.Name = "txtPrevStateTax2";
			this.txtPrevStateTax2.Style = "text-align: right";
			this.txtPrevStateTax2.Text = null;
			this.txtPrevStateTax2.Top = 7.541667F;
			this.txtPrevStateTax2.Width = 1.572917F;
			// 
			// txtCurrStateTax2
			// 
			this.txtCurrStateTax2.Height = 0.1666667F;
			this.txtCurrStateTax2.Left = 5.9375F;
			this.txtCurrStateTax2.Name = "txtCurrStateTax2";
			this.txtCurrStateTax2.Style = "text-align: right";
			this.txtCurrStateTax2.Text = null;
			this.txtCurrStateTax2.Top = 7.541667F;
			this.txtCurrStateTax2.Width = 1.447917F;
			// 
			// rptW2C
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Type)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSNNameCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersFedEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncorrectSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncorrectName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevAllocatedTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrAllocatedTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12A)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12A)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStatutory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevRetirementPlan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevThirdParty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStatutory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrRetirementPlan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrThirdParty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12B)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12B)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12C)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12C)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12D)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12D)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14C)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14C)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14B)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14B)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev14A)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr14A)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12ACode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12BCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12CCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrev12DCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12ACode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12BCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12CCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurr12DCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateID2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateID2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtW2Type;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSNNameCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLastName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersFedEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncorrectSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncorrectName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevAllocatedTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrAllocatedTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12A;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12A;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStatutory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevRetirementPlan;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevThirdParty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStatutory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrRetirementPlan;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrThirdParty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12B;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12B;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12C;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12C;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12D;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12D;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev14C;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr14C;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev14B;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr14B;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev14A;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr14A;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12ACode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12BCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12CCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrev12DCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12ACode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12BCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12CCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurr12DCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateID2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateID2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateTax2;
	}
}
