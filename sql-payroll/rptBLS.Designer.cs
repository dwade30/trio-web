﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBLS.
	/// </summary>
	partial class rptBLS
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBLS));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReport = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWorksite = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTDSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM1Sub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM2Sub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM3Sub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorksite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Sub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Sub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Sub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtWorksite,
				this.txtQTD,
				this.txtMonth1,
				this.txtMonth2,
				this.txtMonth3
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.lblPage,
				this.txtTime,
				this.Field1,
				this.Field2,
				this.txtMonth1Label,
				this.Field4,
				this.txtMonth2Label,
				this.txtMonth3Label,
				this.Field7
			});
			this.PageHeader.Height = 0.8541667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtReport
			});
			this.GroupHeader1.Height = 0.1979167F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanGrow = false;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtQTDSub,
				this.txtM1Sub,
				this.txtM2Sub,
				this.txtM3Sub,
				this.Line1
			});
			this.GroupFooter1.Height = 0.2604167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.2291667F;
			this.txtCaption.Left = 2.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Multiple Worksite Summary";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 3.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 5.71875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.65625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.2083333F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.3125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold; text-align: right";
			this.Field1.Text = "Report";
			this.Field1.Top = 0.6875F;
			this.Field1.Width = 0.6145833F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 0.75F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold; text-align: right";
			this.Field2.Text = "Worksite";
			this.Field2.Top = 0.6875F;
			this.Field2.Width = 0.7395833F;
			// 
			// txtMonth1Label
			// 
			this.txtMonth1Label.Height = 0.1666667F;
			this.txtMonth1Label.Left = 1.6875F;
			this.txtMonth1Label.Name = "txtMonth1Label";
			this.txtMonth1Label.Style = "font-weight: bold; text-align: right";
			this.txtMonth1Label.Text = "Month 1";
			this.txtMonth1Label.Top = 0.6875F;
			this.txtMonth1Label.Width = 0.9895833F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2291667F;
			this.Field4.Left = 2.0625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.Field4.Text = null;
			this.Field4.Top = 0.2916667F;
			this.Field4.Width = 3.5625F;
			// 
			// txtMonth2Label
			// 
			this.txtMonth2Label.Height = 0.1666667F;
			this.txtMonth2Label.Left = 2.8125F;
			this.txtMonth2Label.Name = "txtMonth2Label";
			this.txtMonth2Label.Style = "font-weight: bold; text-align: right";
			this.txtMonth2Label.Text = "Month 2";
			this.txtMonth2Label.Top = 0.6875F;
			this.txtMonth2Label.Width = 0.9895833F;
			// 
			// txtMonth3Label
			// 
			this.txtMonth3Label.Height = 0.1666667F;
			this.txtMonth3Label.Left = 3.9375F;
			this.txtMonth3Label.Name = "txtMonth3Label";
			this.txtMonth3Label.Style = "font-weight: bold; text-align: right";
			this.txtMonth3Label.Text = "Month 3";
			this.txtMonth3Label.Top = 0.6875F;
			this.txtMonth3Label.Width = 0.9895833F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1666667F;
			this.Field7.Left = 5.0625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-weight: bold; text-align: right";
			this.Field7.Text = "Quarterly Wages";
			this.Field7.Top = 0.6875F;
			this.Field7.Width = 1.427083F;
			// 
			// txtReport
			// 
			this.txtReport.Height = 0.1666667F;
			this.txtReport.Left = 0.05208333F;
			this.txtReport.Name = "txtReport";
			this.txtReport.Style = "text-align: right";
			this.txtReport.Text = null;
			this.txtReport.Top = 0F;
			this.txtReport.Width = 0.6145833F;
			// 
			// txtWorksite
			// 
			this.txtWorksite.Height = 0.1666667F;
			this.txtWorksite.Left = 0.75F;
			this.txtWorksite.Name = "txtWorksite";
			this.txtWorksite.Style = "text-align: right";
			this.txtWorksite.Text = null;
			this.txtWorksite.Top = 0F;
			this.txtWorksite.Width = 0.7395833F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.1666667F;
			this.txtQTD.Left = 5.0625F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "text-align: right";
			this.txtQTD.Text = null;
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 1.427083F;
			// 
			// txtMonth1
			// 
			this.txtMonth1.Height = 0.1666667F;
			this.txtMonth1.Left = 1.6875F;
			this.txtMonth1.Name = "txtMonth1";
			this.txtMonth1.Style = "text-align: right";
			this.txtMonth1.Text = null;
			this.txtMonth1.Top = 0F;
			this.txtMonth1.Width = 0.9895833F;
			// 
			// txtMonth2
			// 
			this.txtMonth2.Height = 0.1666667F;
			this.txtMonth2.Left = 2.8125F;
			this.txtMonth2.Name = "txtMonth2";
			this.txtMonth2.Style = "text-align: right";
			this.txtMonth2.Text = null;
			this.txtMonth2.Top = 0F;
			this.txtMonth2.Width = 0.9895833F;
			// 
			// txtMonth3
			// 
			this.txtMonth3.Height = 0.1666667F;
			this.txtMonth3.Left = 3.9375F;
			this.txtMonth3.Name = "txtMonth3";
			this.txtMonth3.Style = "text-align: right";
			this.txtMonth3.Text = null;
			this.txtMonth3.Top = 0F;
			this.txtMonth3.Width = 0.9895833F;
			// 
			// txtQTDSub
			// 
			this.txtQTDSub.Height = 0.1666667F;
			this.txtQTDSub.Left = 5.0625F;
			this.txtQTDSub.Name = "txtQTDSub";
			this.txtQTDSub.Style = "text-align: right";
			this.txtQTDSub.Text = null;
			this.txtQTDSub.Top = 0.0625F;
			this.txtQTDSub.Width = 1.427083F;
			// 
			// txtM1Sub
			// 
			this.txtM1Sub.Height = 0.1666667F;
			this.txtM1Sub.Left = 1.6875F;
			this.txtM1Sub.Name = "txtM1Sub";
			this.txtM1Sub.Style = "text-align: right";
			this.txtM1Sub.Text = null;
			this.txtM1Sub.Top = 0.0625F;
			this.txtM1Sub.Width = 0.9895833F;
			// 
			// txtM2Sub
			// 
			this.txtM2Sub.Height = 0.1666667F;
			this.txtM2Sub.Left = 2.8125F;
			this.txtM2Sub.Name = "txtM2Sub";
			this.txtM2Sub.Style = "text-align: right";
			this.txtM2Sub.Text = null;
			this.txtM2Sub.Top = 0.0625F;
			this.txtM2Sub.Width = 0.9895833F;
			// 
			// txtM3Sub
			// 
			this.txtM3Sub.Height = 0.1666667F;
			this.txtM3Sub.Left = 3.9375F;
			this.txtM3Sub.Name = "txtM3Sub";
			this.txtM3Sub.Style = "text-align: right";
			this.txtM3Sub.Text = null;
			this.txtM3Sub.Top = 0.0625F;
			this.txtM3Sub.Width = 0.9895833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.6979167F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.03125F;
			this.Line1.Width = 5.8125F;
			this.Line1.X1 = 0.6979167F;
			this.Line1.X2 = 6.510417F;
			this.Line1.Y1 = 0.03125F;
			this.Line1.Y2 = 0.03125F;
			// 
			// rptBLS
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWorksite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTDSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM1Sub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM2Sub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM3Sub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWorksite;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth1Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth2Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth3Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReport;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTDSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM1Sub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM2Sub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM3Sub;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
