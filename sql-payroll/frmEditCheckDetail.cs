//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEditCheckDetail : BaseForm
	{
		public frmEditCheckDetail()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditCheckDetail InstancePtr
		{
			get
			{
				return (frmEditCheckDetail)Sys.GetInstance(typeof(frmEditCheckDetail));
			}
		}

		protected frmEditCheckDetail _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDEMPCOLEMPNO = 0;
		const int CNSTGRIDEMPCOLNAME = 1;
		const int CNSTGRIDEMPCOLSEQ = 2;
		const int CNSTGRIDEMPCOLDEPTDIV = 3;

		private void frmEditCheckDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditCheckDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditCheckDetail properties;
			//frmEditCheckDetail.FillStyle	= 0;
			//frmEditCheckDetail.ScaleWidth	= 9300;
			//frmEditCheckDetail.ScaleHeight	= 7530;
			//frmEditCheckDetail.LinkTopic	= "Form2";
			//frmEditCheckDetail.LockControls	= -1  'True;
			//frmEditCheckDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridEmployees();
			FillcmbPayDate();
			LoadGridEmployees();
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				mnuAddPayDate.Enabled = false;
				// wont let them add a date
			}
		}

		private void LoadGridEmployees()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			rsLoad.OpenRecordset("select * from tblemployeemaster order by employeenumber", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				GridEmployees.Rows += 1;
				lngRow = GridEmployees.Rows - 1;
				GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLEMPNO, FCConvert.ToString(rsLoad.Get_Fields("employeenumber")));
				GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLNAME, fecherFoundation.Strings.Trim(rsLoad.Get_Fields("lastname") + ", " + fecherFoundation.Strings.Trim(rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("middLENAME")) + " " + rsLoad.Get_Fields("desig")));
				GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLSEQ, FCConvert.ToString(rsLoad.Get_Fields("SeqNumber")));
				GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLDEPTDIV, FCConvert.ToString(rsLoad.Get_Fields("DeptDiv")));
				rsLoad.MoveNext();
			}
		}

		private void ResizeGridEmployees()
		{
			int GridWidth = 0;
			GridWidth = GridEmployees.WidthOriginal;
			GridEmployees.ColWidth(CNSTGRIDEMPCOLEMPNO, FCConvert.ToInt32(0.15 * GridWidth));
			GridEmployees.ColWidth(CNSTGRIDEMPCOLNAME, FCConvert.ToInt32(0.53 * GridWidth));
			GridEmployees.ColWidth(CNSTGRIDEMPCOLSEQ, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void frmEditCheckDetail_Resize(object sender, System.EventArgs e)
		{
			ResizeGridEmployees();
		}

		private void SetupGridEmployees()
		{
			GridEmployees.TextMatrix(0, CNSTGRIDEMPCOLEMPNO, "Employee #");
			GridEmployees.TextMatrix(0, CNSTGRIDEMPCOLNAME, "Full Name");
			GridEmployees.TextMatrix(0, CNSTGRIDEMPCOLSEQ, "Sequence");
			GridEmployees.TextMatrix(0, CNSTGRIDEMPCOLDEPTDIV, "Dept/Div");
			GridEmployees.ColAlignment(CNSTGRIDEMPCOLEMPNO, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridEmployees.ColAlignment(CNSTGRIDEMPCOLDEPTDIV, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridEmployees.ColAlignment(CNSTGRIDEMPCOLSEQ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void GridEmployees_DblClick(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			string[] strAry = null;
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string)
			DateTime dtTemp;
			if (GridEmployees.MouseRow < 1)
				return;
			if (cmbPayDate.SelectedIndex < 0)
			{
				MessageBox.Show("No Paydate selected", "No Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strAry = Strings.Split(cmbPayDate.Items[cmbPayDate.SelectedIndex].ToString(), "Run", -1, CompareConstants.vbTextCompare);
			dtTemp = FCConvert.ToDateTime(fecherFoundation.Strings.Trim(strAry[0]));
			lngRow = GridEmployees.MouseRow;
			frmEditPayRecords.InstancePtr.Init(GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLEMPNO), GridEmployees.TextMatrix(lngRow, CNSTGRIDEMPCOLNAME), ref dtTemp, FCConvert.ToInt16(Conversion.Val(fecherFoundation.Strings.Trim(strAry[1]))));
		}

		private void mnuAddPayDate_Click(object sender, System.EventArgs e)
		{
			ManualDate();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ManualDate()
		{
			string strDate = "";
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
			DateTime dtDate;
			dtDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			object temp = dtDate;
			if (frmInput.InstancePtr.Init(ref temp, "Pay Date", "Enter the new date", 2880, false, modGlobalConstants.InputDTypes.idtDate))
			{
				dtDate = Convert.ToDateTime(temp);
				if (Information.IsDate(dtDate))
				{
					if (dtDate.ToOADate() != 0)
					{
						if (dtDate != Convert.ToDateTime("12/30/1899"))
						{
							cmbPayDate.AddItem(FCConvert.ToString(dtDate) + "   Run 1", 0);
							cmbPayDate.SelectedIndex = 0;
						}
					}
				}
			}
		}

		private void FillcmbPayDate()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string, DateTime)
			DateTime dtStartDate;
			DateTime dtEndDate;
			DateTime dtTempDate;
			dtTempDate = modGlobalVariables.Statics.gdatCurrentPayDate;
			dtEndDate = modGlobalVariables.Statics.gdatCurrentPayDate;
			dtStartDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
			dtTempDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
			if (fecherFoundation.DateAndTime.DateDiff("d", dtTempDate, dtStartDate) > 0)
			{
				dtStartDate = dtTempDate;
			}
			cmbPayDate.Clear();
			strSQL = "select paydate,payrunid from tblcheckdetail where checkvoid = 0 group by paydate,payrunid order by paydate desc,payrunid";
			rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				cmbPayDate.AddItem(Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy") + "   Run " + rsLoad.Get_Fields("payrunid"));
				rsLoad.MoveNext();
			}
			if (cmbPayDate.Items.Count > 0)
			{
				cmbPayDate.SelectedIndex = 0;
			}
		}
	}
}
