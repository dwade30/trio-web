//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBankInfoListing.
	/// </summary>
	public partial class rptBankInfoListing : BaseSectionReport
	{
		public rptBankInfoListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Bank Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBankInfoListing InstancePtr
		{
			get
			{
				return (rptBankInfoListing)Sys.GetInstance(typeof(rptBankInfoListing));
			}
		}

		protected rptBankInfoListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBank_AutoInitialized?.Dispose();
                rsBank_AutoInitialized = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBankInfoListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       June 26,2001
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;
		//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
		//public clsDRWrapper RSExtract = new clsDRWrapper();
		clsDRWrapper rsBank_AutoInitialized = null;
		clsDRWrapper rsBank
		{
			get
			{
				if (rsBank_AutoInitialized == null)
				{
					rsBank_AutoInitialized = new clsDRWrapper();
				}
				return rsBank_AutoInitialized;
			}
			set
			{
				rsBank_AutoInitialized = value;
			}
		}
		string strSortField = "";

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "BankListing");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsBank.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				txtNumber.Text = Strings.Format(rsBank.Get_Fields("RECORDNUMBER"), "0000");
				txtName.Text = FCConvert.ToString(rsBank.Get_Fields_String("Name"));
				txtAddress1.Text = FCConvert.ToString(rsBank.Get_Fields_String("Address1"));
				txtAddress2.Text = FCConvert.ToString(rsBank.Get_Fields_String("Address2"));
				txtAddress3.Text = FCConvert.ToString(rsBank.Get_Fields("Address3"));
				if (!rsBank.EndOfFile())
					rsBank.MoveNext();
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsBank = new clsDRWrapper();
				if (modGlobalVariables.Statics.gboolSortBankName)
				{
					strSortField = "Name";
				}
				else
				{
					strSortField = "RecordNumber";
				}
				if (Conversion.Val(modGlobalVariables.Statics.gstrBankStart) != 0)
				{
					if (modGlobalVariables.Statics.gboolSortBankName)
					{
						rsBank.OpenRecordset("Select * from tblBanks where Name >= '" + modGlobalVariables.Statics.gstrBankStart + "' and Name <= '" + modGlobalVariables.Statics.gstrBankEnd + "' order by " + strSortField, "TWPY0000.vb1");
					}
					else
					{
						rsBank.OpenRecordset("Select * from tblBanks where ID >= " + Strings.Format(FCConvert.ToInt16(FCConvert.ToDouble(modGlobalVariables.Statics.gstrBankStart)), "0000") + " and ID <= " + Strings.Format(modGlobalVariables.Statics.gstrBankEnd, "0000") + " order by " + strSortField, "TWPY0000.vb1");
					}
				}
				else
				{
					rsBank.OpenRecordset("Select * from tblBanks order by " + strSortField, "TWPY0000.vb1");
				}
				lblAddress1.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				lblAddress2.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				lblAddress3.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				txtAddress1.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				txtAddress2.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				txtAddress3.Visible = frmPrintBankSetup.InstancePtr.chkBankName.CheckState == Wisej.Web.CheckState.Unchecked;
				intCounter = 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Payroll Bank Information";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm:ss tt");
		}

		
	}
}
