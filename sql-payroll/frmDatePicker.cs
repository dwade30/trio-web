﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmDatePicker : BaseForm
	{
		public frmDatePicker()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDatePicker InstancePtr
		{
			get
			{
				return (frmDatePicker)Sys.GetInstance(typeof(frmDatePicker));
			}
		}

		protected frmDatePicker _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: dtReturn As DateTime	OnWrite(int, string)
		DateTime dtReturn;
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(int, DateTime)
		public DateTime Init(string strCaption, int lngStartYear, int lngEndYear, DateTime dtDefault/*= DateTime.FromOADate(0)*/)
		{
			DateTime Init = System.DateTime.Now;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngYear = 0;
			DateTime dtStartDate;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp;
			Init = DateTime.FromOADate(0);
			if (dtDefault.ToOADate() != 0)
			{
				dtStartDate = dtDefault;
				lngYear = dtDefault.Year;
			}
			else
			{
				lngYear = DateTime.Today.Year;
				dtStartDate = DateTime.Today;
			}
			dtReturn = DateTime.FromOADate(0);
			this.Text = strCaption;
			cmbYear.Clear();
			intTemp = 0;
			for (x = lngStartYear; x <= lngEndYear; x++)
			{
				cmbYear.AddItem(x.ToString());
				if (x == lngYear)
				{
					intTemp = cmbYear.NewIndex;
				}
			}
			// x
			cmbYear.SelectedIndex = intTemp;
			cmbMonth.Clear();
			cmbMonth.AddItem("January");
			cmbMonth.AddItem("February");
			cmbMonth.AddItem("March");
			cmbMonth.AddItem("April");
			cmbMonth.AddItem("May");
			cmbMonth.AddItem("June");
			cmbMonth.AddItem("July");
			cmbMonth.AddItem("August");
			cmbMonth.AddItem("September");
			cmbMonth.AddItem("October");
			cmbMonth.AddItem("November");
			cmbMonth.AddItem("December");
			intTemp = dtStartDate.Month;
			cmbMonth.SelectedIndex = intTemp - 1;
			FillDay();
			cmbDay.SelectedIndex = dtStartDate.Day - 1;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = dtReturn;
			return Init;
		}

		private void FillDay()
		{
			// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string, DateTime)
			DateTime dtStart;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth;
			DateTime dtTemp;
			int intLastDay;
			string strDay = "";
			// vbPorter upgrade warning: intOldDay As int	OnWriteFCConvert.ToInt32(
			int intOldDay = 0;
			if (cmbMonth.SelectedIndex < 0)
				return;
			if (cmbDay.Items.Count > 0)
			{
				intOldDay = cmbDay.SelectedIndex;
			}
			dtStart = FCConvert.ToDateTime(FCConvert.ToString(cmbMonth.SelectedIndex + 1) + "/1/" + cmbYear.Text);
			intMonth = dtStart.Month;
			cmbDay.Clear();
			while (dtStart.Month == intMonth)
			{
				switch ((DayOfWeek)fecherFoundation.DateAndTime.Weekday(dtStart, DayOfWeek.Sunday))
				{
					case DayOfWeek.Sunday:
						{
							strDay = "Sunday";
							break;
						}
					case DayOfWeek.Monday:
						{
							strDay = "Monday";
							break;
						}
					case DayOfWeek.Tuesday:
						{
							strDay = "Tuesday";
							break;
						}
					case DayOfWeek.Wednesday:
						{
							strDay = "Wednesday";
							break;
						}
					case DayOfWeek.Thursday:
						{
							strDay = "Thursday";
							break;
						}
					case DayOfWeek.Friday:
						{
							strDay = "Friday";
							break;
						}
					case DayOfWeek.Saturday:
						{
							strDay = "Saturday";
							break;
						}
				}
				//end switch
				cmbDay.AddItem(FCConvert.ToString(dtStart.Day) + ",  " + strDay);
				dtStart = fecherFoundation.DateAndTime.DateAdd("d", 1, dtStart);
				if (intOldDay >= 0 && intOldDay <= (cmbDay.Items.Count - 1))
				{
					cmbDay.SelectedIndex = intOldDay;
				}
				else if (intOldDay > 0)
				{
					cmbDay.SelectedIndex = cmbDay.Items.Count - 1;
				}
				else
				{
					cmbDay.SelectedIndex = 0;
				}
			}
		}

		private void cmbMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillDay();
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillDay();
		}

		private void frmDatePicker_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDatePicker_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDatePicker properties;
			//frmDatePicker.FillStyle	= 0;
			//frmDatePicker.ScaleWidth	= 3885;
			//frmDatePicker.ScaleHeight	= 2100;
			//frmDatePicker.LinkTopic	= "Form2";
			//frmDatePicker.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			dtReturn = FCConvert.ToDateTime(FCConvert.ToString(cmbMonth.SelectedIndex + 1) + "/" + FCConvert.ToString(cmbDay.SelectedIndex + 1) + "/" + cmbYear.Text);
			Close();
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
