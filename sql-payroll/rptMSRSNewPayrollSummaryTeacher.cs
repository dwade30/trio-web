//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSNewPayrollSummaryTeacher.
	/// </summary>
	public partial class rptMSRSNewPayrollSummary : BaseSectionReport
	{
		public rptMSRSNewPayrollSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Monthly Payroll Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMSRSNewPayrollSummary InstancePtr
		{
			get
			{
				return (rptMSRSNewPayrollSummary)Sys.GetInstance(typeof(rptMSRSNewPayrollSummary));
			}
		}

		protected rptMSRSNewPayrollSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMSRSNewPayrollSummaryTeacher	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		bool boolNoData;
		bool boolElectronicMSRS;
		// vbPorter upgrade warning: clsMSRSObject As clsMSRS	OnWrite(object)
		clsMSRS clsMSRSObject;
		// vbPorter upgrade warning: MSRSObject As object	OnWrite(clsMSRS)	OnRead(clsMSRS)
		public void Init(bool boolUseElectronic/*= false*/, clsMSRS MSRSObject, List<string> batchReports = null)
		{
			boolElectronicMSRS = boolUseElectronic;
			if (boolElectronicMSRS)
			{
				clsMSRSObject = MSRSObject;
			}
			// frmReportViewer.Init Me, , 1
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modCoreysSweeterCode.CheckDefaultPrint(this, "", 1, boolAllowEmail: true, strAttachmentName: "MainePERS", boolDontAllowCloseTilReportDone: true);
			}
			else
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//GrapeCity.ActiveReports.Document.Section.Page pg = new GrapeCity.ActiveReports.Document.Section.Page();
			// vbPorter upgrade warning: intPageIndex As int	OnWrite(int, double)
			int intPageIndex;
			int X;
			string strCopy = "";
			int lngXCoord = 0;
			int lngYCoord = 0;
			if (boolElectronicMSRS)
			{
				clsMSRSObject.SetSummaryRecord();
			}
			if (boolNoData)
				return;
			intPageIndex = this.Document.Pages.Count;
			//FC:FINAL:DSE Endless loop if modifying collection during foreach loop
			//foreach (GrapeCity.ActiveReports.Document.Section.Page pg in this.Document.Pages)
			//{
			//	this.Document.Pages.Insert(intPageIndex, pg);
			//	intPageIndex += 1;
			//}
			int nrPages = this.Document.Pages.Count;
			for (int i = 0; i < nrPages; i++)
			{
				GrapeCity.ActiveReports.Document.Section.Page pg = Document.Pages[i];
				this.Document.Pages.Insert(intPageIndex, pg);
				intPageIndex += 1;
			}
			// pg
			intPageIndex = FCConvert.ToInt16(this.Document.Pages.Count / 2 - 1);
			X = 0;
			foreach (GrapeCity.ActiveReports.Document.Section.Page pg in this.Document.Pages)
			{
				if (X <= intPageIndex)
				{
					// first report
					strCopy = "MainePERS COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 4320;
					lngYCoord = 15120;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800, 270);
				}
				else
				{
					// second report
					strCopy = "EMPLOYER COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 4320;
					lngYCoord = 15120;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800, 270);
				}
				X += 1;
			}
			// pg
			//this.Document.Pages.Commit();
			//this.Refresh();
		}

		//private void ActiveReport_ReportStart(object sender, EventArgs e)
		//{
		//	string strSQL;
		//	string strTemp;
		//	string strTemp2;
		//	int X;
		//	double dblContributionAdjustment = 0;
		//	double dblCompensationAdjustment = 0;
		//	double dblPurchaseAgreements = 0;
		//	double dblPremiumTotal;
		//	double dblContributionTotal;
		//	double dblTotalC;
		//	double dblTotalD;
		//	double dblTotalRemittance;
		//	int intCurrentCode;
		//	double[] dblEmpRate = new double[6 + 1];
		//	double[] dblEarnComp = new double[6 + 1];
		//	double[] dblEmployerCont = new double[6 + 1];
		//	double[] dblCostOrCredit = new double[6 + 1];
		//	double[] dblEmployeeCont = new double[6 + 1];
		//	double[] dblTotcontributions = new double[6 + 1];
		//	double dblTotEmployerCont = 0;
		//	double dblTotCostOrCredit = 0;
		//	double dblTotEmployeeCont = 0;
		//	double dblBasicActives = 0;
		//	double dblBasicRetirees = 0;
		//	double dblSupplemental = 0;
		//	double dblDependent = 0;
		//	double dblDepARate = 0;
		//	double dblDepBRate = 0;
		//	double[] dblDebitOrCredit = new double[2 + 1];
		//	string[] strDebitOrCredit = new string[2 + 1];
		//	int intWeeks = 0;
		//	double dblMTDWithheld;
		//	string strMSRSCode = "";
		//	// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
		//	DateTime dtStartDate = DateTime.FromOADate(0);
		//	DateTime dtEndDate = DateTime.FromOADate(0);
		//	clsDRWrapper clsMSRS = new clsDRWrapper();
		//	double dblTotalEarnComp;
		//	double dblTotalFedEarnComp;
		//	double dblFedFundPerc = 0;
		//	double dblFedEmployerCont;
		//	int intCurAdjustment;
		//	string[] strAdjustment = new string[8 + 1];
		//	double[] dblAdjustment = new double[9 + 1];
		//	string[] strPlanCode = new string[6 + 1];
		//	double[] dblPlanCode = new double[6 + 1];
		//	double dblCurEComp = 0;
		//	double dblFedGrant = 0;
		//	// Dim lngNumCovered As Long
		//	// Dim lngNumAdded As Long
		//	double dblTotAdjustments;
		//	clsDRWrapper clsEmpCont = new clsDRWrapper();
		//	bool boolAutoAddCredit = false;
		//	// 
		//	boolNoData = false;
		//	dblTotAdjustments = 0;
		//	dblTotalFedEarnComp = 0;
		//	dblTotalEarnComp = 0;
		//	dblPremiumTotal = 0;
		//	dblContributionTotal = 0;
		//	dblTotalC = 0;
		//	dblTotalD = 0;
		//	dblDebitOrCredit[1] = 0;
		//	dblDebitOrCredit[2] = 0;
		//	strDebitOrCredit[1] = "";
		//	strDebitOrCredit[2] = "";
		//	dblTotalRemittance = 0;
		//	FCUtils.EraseSafe(dblAdjustment);
		//	FCUtils.EraseSafe(strAdjustment);
		//	FCUtils.EraseSafe(strPlanCode);
		//	FCUtils.EraseSafe(dblPlanCode);
		//	// 
		//	// lngNumCovered = 0
		//	// lngNumAdded = 0
		//	// 
		//	// strSQL = "select sum(earnablecompensation) as totEarnComp, sum(Employeecontributions) as TotEmpCont from tblmsrstempdetailreport where ((convert(int, isnull(statuscode, 0)) between 71 and 75) or (convert(int, isnull(statuscode, 0)) = 43)) and (earnablecompensation > 0 and employeecontributions > 0)"
		//	// corey 6/30/2005 call id
		//	// If gtypeFullSetReports.boolFullSet Then
		//	// lngReportType = Val(GetRegistryKey("MSRSReportType"))
		//	// End If
		//	dblTotalFedEarnComp = 0;
		//	strSQL = "select * from tblMSRSTable where reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
		//	clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
		//	strTemp = "";
		//	strTemp2 = "";
		//	if (!clsLoad.EndOfFile())
		//	{
		//		txtEmployerCode.Text = clsLoad.Get_Fields_String("EmployerCode");
		//		txtEmployerName.Text = clsLoad.Get_Fields_String("EmployerName");
		//		txtFedFundPerc.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fedfundperc")));
		//		dblFedFundPerc = Conversion.Val(clsLoad.Get_Fields("fedfundperc")) / 100;
		//		intWeeks = 0;
		//		for (X = 1; X <= 5; X++)
		//		{
		//			if (FCConvert.ToInt32(clsLoad.Get_Fields_DateTime("PaidDatesEnding" + FCConvert.ToString(X))) != 0)
		//			{
		//				intWeeks += 1;
		//				strTemp += Strings.Format(clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X)), "MM/dd/yyyy") + " ";
		//				if (X == 1)
		//					dtStartDate = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
		//				dtEndDate = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
		//			}
		//		}
		//		// X
		//		dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(dtStartDate.Month) + "/1/" + FCConvert.ToString(dtStartDate.Year));
		//		strTemp = fecherFoundation.Strings.Trim(strTemp);
		//		txtPaidDates1.Text = strTemp;
		//		txtPreparer.Text = clsLoad.Get_Fields_String("PreparersName");
		//		txtDateCompleted.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//		txtDateSigned.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//		strTemp = Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("telephone"))), "0000000000");
		//		txtTelephone.Text = FCConvert.ToString(clsLoad.Get_Fields("telephone"));
		//		dblBasicActives = Conversion.Val(clsLoad.Get_Fields("premiumactive"));
		//		dblSupplemental = Conversion.Val(clsLoad.Get_Fields("premiumsupplemental"));
		//		dblDependent = Conversion.Val(clsLoad.Get_Fields_Double("PremiumDependent"));
		//		dblDepARate = Conversion.Val(clsLoad.Get_Fields_Double("DepARate"));
		//		dblDepBRate = Conversion.Val(clsLoad.Get_Fields_Double("DepBRate"));
		//		dblBasicRetirees = 0;
		//		CalculatePremiums(ref dblBasicActives, ref dblBasicRetirees, ref dblSupplemental, ref dblDependent, ref dblDepARate, ref dblDepBRate, ref dtStartDate, ref dtEndDate, ref intWeeks);
		//		txtBasicPremiums.Text = Strings.Format(dblBasicActives, "#,###,##0.00");
		//		txtSupplemental.Text = Strings.Format(dblSupplemental, "#,###,##0.00");
		//		txtDependent.Text = Strings.Format(dblDependent, "#,###,##0.00");
		//		dblPremiumTotal = Conversion.Val(dblSupplemental + dblBasicActives + dblDependent);
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalActive = dblBasicActives;
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalDependent = dblDependent;
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalSupplemental = dblSupplemental;
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalLifeInsurancePremiums = dblPremiumTotal;
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetiree = dblBasicRetirees;
		//		txtTotalPremiums.Text = Strings.Format(dblPremiumTotal, "#,###,##0.00");
		//		dblDebitOrCredit[1] = Conversion.Val(clsLoad.Get_Fields("creditordebittaken1"));
		//		dblDebitOrCredit[2] = Conversion.Val(clsLoad.Get_Fields("creditordebittaken2"));
		//		strDebitOrCredit[1] = FCConvert.ToString(clsLoad.Get_Fields("creditordebitno1"));
		//		strDebitOrCredit[2] = FCConvert.ToString(clsLoad.Get_Fields("creditordebitno2"));
		//		if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("AutoAddEmployerCredit")))
		//		{
		//			boolAutoAddCredit = true;
		//		}
		//		else
		//		{
		//			boolAutoAddCredit = false;
		//		}
		//		dblCompensationAdjustment = Conversion.Val(clsLoad.Get_Fields_Double("PriorPeriodCompensationAdjustment"));
		//		dblContributionAdjustment = Conversion.Val(clsLoad.Get_Fields_Double("PriorPeriodContributionAdjustment"));
		//		dblPurchaseAgreements = Conversion.Val(clsLoad.Get_Fields_Double("PurchaseAgreements"));
		//		txtCompensationAdjustment.Text = Strings.Format(dblCompensationAdjustment, "#,###,##0.00");
		//		txtContributionAdjustment.Text = Strings.Format(dblContributionAdjustment, "#,###,##0.00");
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFundedEmployer = dblContributionAdjustment;
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFunded = dblCompensationAdjustment;
		//		txtPurchaseAgreements.Text = Strings.Format(dblPurchaseAgreements, "#,###,##0.00");
		//		modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalExcessPayback = dblPurchaseAgreements;
		//	}
		//	else
		//	{
		//		boolNoData = true;
		//		MessageBox.Show("There is no MainePERS information in the table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
		//		{
		//			this.Close();
		//			return;
		//		}
		//		else
		//		{
		//			return;
		//		}
		//	}
		//	if (!FCConvert.ToBoolean(clsLoad.Get_Fields("Range")))
		//	{
		//		strSQL = "select * from tblcheckdetail where reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER) + " and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "'";
		//	}
		//	else
		//	{
		//		strSQL = "select * from tblcheckdetail inner join tblemployeemaster on (tblemployeemaster.employeenumber = tblcheckdetail.employeenumber) where reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER) + " and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and seqnumber between " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("RANGESTART"))) + " and " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("rangeend")));
		//	}
		//	clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
		//	while (!clsMSRS.EndOfFile())
		//	{
		//		dblCurEComp = Conversion.Val(clsMSRS.Get_Fields("distgrosspay"));
		//		dblFedGrant = 0;
		//		if (Conversion.Val(dblCurEComp) > 0)
		//		{
		//			if (Conversion.Val(clsMSRS.Get_Fields("fedcompamount")) > 0)
		//			{
		//				if (Conversion.Val(clsMSRS.Get_Fields("fedcompDOLLARPERCENT")) == modCoreysSweeterCode.CNSTMSRSFEDCOMPPERCENT)
		//				{
		//					// percentage
		//					dblFedGrant = (Conversion.Val(clsMSRS.Get_Fields("fedcompamount")) / 100) * dblCurEComp;
		//				}
		//				else
		//				{
		//					// dollar
		//					dblFedGrant = Conversion.Val(clsMSRS.Get_Fields("fedcompamount"));
		//				}
		//			}
		//		}
		//		dblFedGrant = Conversion.Val(Strings.Format(dblFedGrant, "0.00"));
		//		dblTotalFedEarnComp += dblFedGrant;
		//		clsMSRS.MoveNext();
		//	}
		//	dblFedEmployerCont = dblFedFundPerc * dblTotalFedEarnComp;
		//	txtEmployerContributions.Text = Strings.Format(dblFedEmployerCont, "#,###,###,##0.00");
		//	txtGrantFundedCompensation.Text = Strings.Format(dblTotalFedEarnComp, "#,###,###,##0.00");
		//	modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFunded = dblTotalFedEarnComp;
		//	modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFundedEmployerContributions = dblFedEmployerCont;
		//	// **********************************************
		//	// strSQL = "select sum(grantfundedearnable) as totEarnComp, sum(Employeecontributions) as TotEmpCont from tblmsrstempdetailreport where (grantfundedearnable > 0 and employeecontributions > 0)"
		//	// Call clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1")
		//	// 
		//	// strSQL = "select * from tblMSRSTable"
		//	// Call clsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
		//	// 
		//	// strTemp = ""
		//	// strTemp2 = ""
		//	// If Not clsLoad.EndOfFile Then
		//	// With clsLoad
		//	// txtEmployerCode.Caption = .Fields("EmployerCode")
		//	// txtEmployerName.Caption = .Fields("EmployerName")
		//	// 
		//	// txtFedFundPerc.Caption = Val(.Fields("fedfundperc"))
		//	// dblFedFundPerc = Val(.Fields("fedfundperc")) / 100
		//	// dblFedEmployerCont = dblFedFundPerc * dblTotalFedEarnComp
		//	// txtEmployerContributions.Caption = Format(dblFedEmployerCont, "#,###,###,##0.00")
		//	// 
		//	// intWeeks = 0
		//	// For X = 1 To 5
		//	// If .Fields("PaidDatesEnding" & X) <> 0 Then
		//	// intWeeks = intWeeks + 1
		//	// 
		//	// strTemp = strTemp & Format(.Fields("paiddatesending" & X), "MM/dd/yyyy") & " "
		//	// 
		//	// If X = 1 Then dtStartDate = .Fields("paiddatesending" & X)
		//	// dtEndDate = .Fields("paiddatesending" & X)
		//	// End If
		//	// Next X
		//	// dtStartDate = Month(dtStartDate) & "/1/" & Year(dtStartDate)
		//	// strTemp = Trim(strTemp)
		//	// 
		//	// txtPaidDates1.Caption = strTemp
		//	// 
		//	// txtPreparer.Caption = .Fields("PreparersName")
		//	// txtDateCompleted.Caption = Format(Date, "MM/dd/yyyy")
		//	// txtDateSigned.Caption = Format(Date, "MM/dd/yyyy")
		//	// strTemp = Format(Val(.Fields("telephone")), "0000000000")
		//	// txtTelephone.Caption = .Fields("telephone")
		//	// dblBasicActives = Val(.Fields("premiumactive"))
		//	// dblSupplemental = Val(.Fields("premiumsupplemental"))
		//	// dblDependent = Val(.Fields("PremiumDependent"))
		//	// dblDepARate = Val(.Fields("DepARate"))
		//	// dblDepBRate = Val(.Fields("DepBRate"))
		//	// 
		//	// teachers only
		//	// dblBasicRetirees = 0
		//	// Call CalculatePremiums(dblBasicActives, dblBasicRetirees, dblSupplemental, dblDependent, dblDepARate, dblDepBRate, dtStartDate, dtEndDate, intWeeks)
		//	// txtBasicPremiums.Caption = Format(dblBasicActives, "#,###,##0.00")
		//	// txtSupplemental.Caption = Format(dblSupplemental, "#,###,##0.00")
		//	// 
		//	// txtDependent.Caption = Format(dblDependent, "#,###,##0.00")
		//	// dblPremiumTotal = Val(dblSupplemental + dblBasicActives + dblDependent)
		//	// txtTotalPremiums.Caption = Format(dblPremiumTotal, "#,###,##0.00")
		//	// dblDebitOrCredit(1) = Val(.Fields("creditordebittaken1"))
		//	// dblDebitOrCredit(2) = Val(.Fields("creditordebittaken2"))
		//	// strDebitOrCredit(1) = .Fields("creditordebitno1")
		//	// strDebitOrCredit(2) = .Fields("creditordebitno2")
		//	// If .Fields("AutoAddEmployerCredit") Then
		//	// boolAutoAddCredit = True
		//	// Else
		//	// boolAutoAddCredit = False
		//	// End If
		//	// dblCompensationAdjustment = Val(.Fields("PriorPeriodCompensationAdjustment"))
		//	// dblContributionAdjustment = Val(.Fields("PriorPeriodContributionAdjustment"))
		//	// dblPurchaseAgreements = Val(.Fields("PurchaseAgreements"))
		//	// txtCompensationAdjustment.Caption = Format(dblCompensationAdjustment, "#,###,##0.00")
		//	// txtContributionAdjustment.Caption = Format(dblContributionAdjustment, "#,###,##0.00")
		//	// txtPurchaseAgreements.Caption = Format(dblPurchaseAgreements, "#,###,##0.00")
		//	// End With
		//	// Else
		//	// boolNoData = True
		//	// MsgBox "There is no MSRS information in the table.", vbCritical, "Error"
		//	// If Not gtypeFullSetReports.boolFullSet Then
		//	// Unload Me
		//	// Else: Exit Sub
		//	// End If
		//	// End If
		//	// strSQL = "select count(ID) as thecount from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 and employeecontributions > 0"
		//	// Call clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1")
		//	// If Not clsMSRS.EndOfFile Then
		//	// If Not IsNull(clsMSRS.Fields("thecount")) Then
		//	// lngNumCovered = Val(clsMSRS.Fields("thecount"))
		//	// End If
		//	// End If
		//	// txtLine8.Caption = Format(lngNumCovered, "#,###,##0")
		//	// strSQL = "select * from tblmsrstable"
		//	strSQL = "select * from tblMSRSTable where reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
		//	clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
		//	// strSQL = "select count(ID) as thecount from (select * from tblmsrstempdetailreport where isdate(dateofhire ) and dateofhire  <> 0)  where year(dateofhire ) = " & Year(clsLoad.Fields("paiddatesending1")) & " and month(dateofhire ) = " & Month(clsLoad.Fields("paiddatesending1"))
		//	// Call clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1")
		//	// If Not clsMSRS.EndOfFile Then
		//	// If Not IsNull(clsMSRS.Fields("thecount")) Then
		//	// lngNumAdded = Val(clsMSRS.Fields("thecount"))
		//	// End If
		//	// End If
		//	// txtLine6.Caption = Format(lngNumAdded, "#,###,##0")
		//	// 
		//	// MATTHEW 9/21/2004
		//	// CHANGED FOR JAY SHE HAD AN EMPLOYEE THAT GOT PAID TWICE UNDER DIFFERENT POSITION CODES BUT THE SAME PLAN CODE AND THE
		//	// DISTINCT ON THIS LINE ELIMINATED ONE OF THOSE RECORDS. NOT SURE THE EFFECT ON OTHER TOWNS
		//	// BUT HIS SHOULD BE THE FIRST PLACE TO LOOK IF THE EMPLOYEE RETIREMENT CONTRIBUTIONS (LINE 14)
		//	// ON THE TEACHERS MONTHLY MSRS REPORT STARTS TO BE OFF. CALL ID #55808
		//	// WE NEED TO MAKE SURE THAT I DIDN'T MESS ANYTHING ELSE UP.
		//	// strSQL = "select sum(employeecontributions) as totEmpCont, plancode from (select distinct employeecontributions,employeenumber,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 ) group by plancode "
		//	// 
		//	strSQL = "select sum(employeecontributions) as totEmpCont, plancode from (select employeecontributions,employeenumber,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 ) group by plancode ";
		//	clsEmpCont.OpenRecordset(strSQL, "twpy0000.vb1");
		//	strSQL = "select sum(earnablecompensation) as totEarnComp, sum(Employeecontributions) as TotEmpCont,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 and employeecontributions > 0 group by plancode ";
		//	clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
		//	//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		//	strSQL = "select * from tblmsrsCodeRate WHERE reporttype = " + FCConvert.ToString(modCoreysSweeterCode.CNSTMSRSREPORTTYPETEACHER);
		//	clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
		//	intCurrentCode = 1;
		//	while (!clsMSRS.EndOfFile())
		//	{
		//		dblEmpRate[intCurrentCode] = 0;
		//		dblCostOrCredit[intCurrentCode] = 0;
		//		if (!clsLoad.EndOfFile())
		//		{
		//			if (clsLoad.FindFirstRecord("code", clsMSRS.Get_Fields("plancode")))
		//			{
		//				dblEmpRate[intCurrentCode] = Conversion.Val(clsLoad.Get_Fields("rate"));
		//				if (!boolAutoAddCredit)
		//				{
		//					dblCostOrCredit[intCurrentCode] = Conversion.Val(clsLoad.Get_Fields("costorcredit"));
		//				}
		//			}
		//		}
		//		dblEarnComp[intCurrentCode] = Conversion.Val(clsMSRS.Get_Fields("totearncomp"));
		//		dblEmployeeCont[intCurrentCode] = Conversion.Val(clsMSRS.Get_Fields("totempcont"));
		//		if (!(clsEmpCont.EndOfFile() && clsEmpCont.BeginningOfFile()))
		//		{
		//			if (clsEmpCont.FindFirstRecord("plancode", clsMSRS.Get_Fields("plancode")))
		//			{
		//				dblEmployeeCont[intCurrentCode] = Conversion.Val(clsEmpCont.Get_Fields("totempcont"));
		//			}
		//			else
		//			{
		//				dblEmployeeCont[intCurrentCode] = 0;
		//			}
		//		}
		//		else
		//		{
		//			dblEmployeeCont[intCurrentCode] = 0;
		//		}
		//		dblEmployerCont[intCurrentCode] = dblEarnComp[intCurrentCode] * dblEmpRate[intCurrentCode];
		//		if (boolAutoAddCredit)
		//		{
		//			dblCostOrCredit[intCurrentCode] = -dblEmployerCont[intCurrentCode];
		//		}
		//		// the credit cannot exceed the amount in employer contribution
		//		if (-1 * dblCostOrCredit[intCurrentCode] > dblEmployerCont[intCurrentCode])
		//			dblCostOrCredit[intCurrentCode] = dblEmployerCont[intCurrentCode] * -1;
		//		dblTotcontributions[intCurrentCode] = dblEmployerCont[intCurrentCode] + dblCostOrCredit[intCurrentCode] + dblEmployeeCont[intCurrentCode];
		//		if (intCurrentCode < 7)
		//		{
		//			strPlanCode[intCurrentCode] = "Plan Code " + clsMSRS.Get_Fields("plancode");
		//			dblPlanCode[intCurrentCode] = dblCostOrCredit[intCurrentCode];
		//		}
		//		else
		//		{
		//			strPlanCode[6] = "Other Codes";
		//			dblPlanCode[6] += dblCostOrCredit[6];
		//		}
		//		intCurrentCode += 1;
		//		clsMSRS.MoveNext();
		//	}
		//	// 
		//	// 01/23/2004 Lisa in Jay says this must be all, including fed funded teachers
		//	strSQL = "select sum(earnablecompensation) as totEarnComp, sum(Employeecontributions) as TotEmpCont from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53  and  (earnablecompensation > 0 and employeecontributions > 0)";
		//	clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
		//	if (!clsMSRS.EndOfFile())
		//	{
		//		dblTotalEarnComp = Conversion.Val(clsMSRS.Get_Fields("totearncomp"));
		//	}
		//	else
		//	{
		//		dblTotalEarnComp = 0;
		//	}
		//	txtEarnableCompensation.Text = Strings.Format(dblTotalEarnComp, "#,###,###,##0.00");
		//	modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalEarnableCompensation = dblTotalEarnComp;
		//	// txtLine12.Caption = Format(dblTotalFedEarnComp, "#,###,###,##0.00")
		//	// 
		//	for (X = 1; X <= 6; X++)
		//	{
		//		dblTotEmployerCont += dblEmployerCont[X];
		//		dblTotCostOrCredit += dblCostOrCredit[X];
		//		dblTotEmployeeCont += dblEmployeeCont[X];
		//		// showing credits and debits separate
		//		dblTotcontributions[X] = dblEmployerCont[X] + dblEmployeeCont[X];
		//		dblContributionTotal += dblTotcontributions[X];
		//	}
		//	// X
		//	modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetirementContributions = dblContributionTotal;
		//	txtTotalEmployeeContributions.Text = Strings.Format(dblContributionTotal, "#,###,###,##0.00");
		//	txtEmployerAndEmployeeCont.Text = Strings.Format(dblTotEmployeeCont + dblFedEmployerCont, "#,###,##0.00");
		//	// txtTotalEmployeeContributions.Caption = Format(dblTotEmployeeCont + dblFedEmployerCont, "#,###,###,##0.00")
		//	// txtEmployerAndEmployeeCont.Caption = Format(dblContributionTotal, "#,###,##0.00")
		//	// 
		//	// txtLine15.Caption = Format(dblTotEmployerCont + dblTotEmployeeCont + dblFedEmployerCont, "#,###,##0.00")
		//	// txtLine14.Caption = Format(dblTotEmployeeCont, "#,###,##0.00")
		//	// txtLine11.Caption = "NA"
		//	// 
		//	intCurAdjustment = 1;
		//	lblAdjustment1.Text = "";
		//	txtAdjustment1.Text = "";
		//	lblAdjustment2.Text = "";
		//	txtAdjustment2.Text = "";
		//	lblAdjustment3.Text = "";
		//	txtAdjustment3.Text = "";
		//	lblAdjustment4.Text = "";
		//	txtAdjustment4.Text = "";
		//	lblAdjustment5.Text = "";
		//	txtAdjustment5.Text = "";
		//	lblAdjustment6.Text = "";
		//	txtAdjustment6.Text = "";
		//	lblAdjustment7.Text = "";
		//	txtAdjustment7.Text = "";
		//	lblAdjustment8.Text = "";
		//	txtAdjustment8.Text = "";
		//	// do these on a separate page now.  They go under miscellaneous. Still need the total though
		//	intCurAdjustment = 1;
		//	for (X = 1; X <= 6; X++)
		//	{
		//		if (Conversion.Val(dblPlanCode[X]) != 0)
		//		{
		//			ReportFooter.Visible = true;
		//			FillAdjustmentLabels(ref intCurAdjustment, ref strPlanCode[X], Strings.Format(dblPlanCode[X], "#,###,##0.00"));
		//			intCurAdjustment += 1;
		//			dblTotAdjustments += dblPlanCode[X];
		//		}
		//	}
		//	// X
		//	if (intCurAdjustment > 1)
		//	{
		//		ReportFooter.Visible = true;
		//	}
		//	// 
		//	txtAdjustments.Text = Strings.Format(dblTotAdjustments, "#,###,###,##0.00");
		//	dblTotalD = dblDebitOrCredit[1] + dblDebitOrCredit[2];
		//	txtTotalDebitAndCredit.Text = Strings.Format(dblTotalD, "#,###,##0.00");
		//	// dblTotalC = dblPremiumTotal + dblContributionTotal
		//	dblTotalC = dblPremiumTotal + dblTotEmployeeCont + dblFedEmployerCont;
		//	// dblTotalRemittance = dblTotalC + dblTotalD + dblPurchaseAgreements + dblContributionAdjustment + dblTotAdjustments + dblCompensationAdjustment
		//	dblTotalRemittance = dblTotalC + dblTotalD + dblPurchaseAgreements + dblContributionAdjustment + dblTotAdjustments;
		//	// + dblCompensationAdjustment
		//	txtTotalRemittance.Text = Strings.Format(dblTotalRemittance, "#,###,##0.00");
		//	// 
		//	if (dblDebitOrCredit[1] != 0)
		//	{
		//		// If dblDebitOrCredit(1) < 0 Then
		//		// strTemp = "Credit Taken  (CM) # " & strDebitOrCredit(1)
		//		// Else
		//		// strTemp = "Debit Taken  DM # " & strDebitOrCredit(1)
		//		// End If
		//		strTemp = strDebitOrCredit[1];
		//		txtDebOrCredNum1.Text = strTemp;
		//		// strTemp2 = Format(dblDebitOrCredit(1), "#,###,##0.00")
		//		// Call FillAdjustmentLabels(intCurAdjustment, strTemp, strTemp2)
		//		// intCurAdjustment = intCurAdjustment + 1
		//		// dblTotAdjustments = dblTotAdjustments + dblDebitOrCredit(1)
		//	}
		//	// 
		//	if (dblDebitOrCredit[2] != 0)
		//	{
		//		// If dblDebitOrCredit(2) < 0 Then
		//		// strTemp = "Credit Taken  (CM) # " & strDebitOrCredit(2)
		//		// Else
		//		// strTemp = "Debit Taken  DM # " & strDebitOrCredit(2)
		//		// End If
		//		strTemp = strDebitOrCredit[2];
		//		txtDebOrCredNum2.Text = strTemp;
		//		// strTemp2 = Format(dblDebitOrCredit(2), "#,###,##0.00")
		//		// Call FillAdjustmentLabels(intCurAdjustment, strTemp, strTemp2)
		//		// intCurAdjustment = intCurAdjustment + 1
		//		// dblTotAdjustments = dblTotAdjustments + dblDebitOrCredit(2)
		//	}
		//}

		//private void FillAdjustmentLabels(ref int intAdjustment, ref string strDescription, string strAmount)
		//{
		//	switch (intAdjustment)
		//	{
		//		case 1:
		//			{
		//				lblAdjustment1.Text = strDescription;
		//				txtAdjustment1.Text = strAmount;
		//				break;
		//			}
		//		case 2:
		//			{
		//				lblAdjustment2.Text = strDescription;
		//				txtAdjustment2.Text = strAmount;
		//				break;
		//			}
		//		case 3:
		//			{
		//				lblAdjustment3.Text = strDescription;
		//				txtAdjustment3.Text = strAmount;
		//				break;
		//			}
		//		case 4:
		//			{
		//				lblAdjustment4.Text = strDescription;
		//				txtAdjustment4.Text = strAmount;
		//				break;
		//			}
		//		case 5:
		//			{
		//				lblAdjustment5.Text = strDescription;
		//				txtAdjustment5.Text = strAmount;
		//				break;
		//			}
		//		case 6:
		//			{
		//				lblAdjustment6.Text = strDescription;
		//				txtAdjustment6.Text = strAmount;
		//				break;
		//			}
		//		case 7:
		//			{
		//				lblAdjustment7.Text = strDescription;
		//				txtAdjustment7.Text = strAmount;
		//				break;
		//			}
		//		case 8:
		//			{
		//				lblAdjustment8.Text = strDescription;
		//				txtAdjustment8.Text = strAmount;
		//				break;
		//			}
		//	}
		//	//end switch
		//}
		//// vbPorter upgrade warning: dblBasicActives As object	OnWrite(double, int)
		//// vbPorter upgrade warning: dblBasicRetirees As object	OnWriteFCConvert.ToDouble(
		//// vbPorter upgrade warning: dblSupplemental As object	OnWrite(double, int)
		//// vbPorter upgrade warning: dblDependent As object	OnWrite(double, int)
		//// vbPorter upgrade warning: dblDepARate As object	OnWriteFCConvert.ToDouble(
		//// vbPorter upgrade warning: dblDepBRate As object	OnWriteFCConvert.ToDouble(
		//private void CalculatePremiums(ref double dblBasicActives, ref double dblBasicRetirees, ref double dblSupplemental, ref double dblDependent, ref double dblDepARate, ref double dblDepBRate, ref DateTime dtStartDate, ref DateTime dtEndDate, ref int intWeeks)
		//{
		//	double dblTemp = 0;
		//	double dblBasicActivesTemp = 0;
		//	double dblSupplementalTemp = 0;
		//	double dblDependentTemp = 0;
		//	clsDRWrapper clsPremium = new clsDRWrapper();
		//	string strSQL;
		//	string strSubQuery;
		//	string strNonPaidQuery;
		//	double dblMTDWithheld = 0;
		//	string strMSRSCode = "";
		//	string strWhere;
		//	string strComma;
		//	bool boolRange;
		//	string strRangeStart = "";
		//	string strRangeEnd = "";
		//	if (FCConvert.ToInt32(dblDepARate) == 0 && FCConvert.ToInt32(dblDepBRate) == 0)
		//		return;
		//	dblBasicActives = 0;
		//	dblDependent = 0;
		//	dblSupplemental = 0;
		//	strSQL = "select * from tbldefaultdeductions where type = 'L'";
		//	clsPremium.OpenRecordset(strSQL, "twpy0000.vb1");
		//	strWhere = "";
		//	strComma = "";
		//	if (!clsPremium.EndOfFile())
		//	{
		//		strWhere = " and deddeductionnumber in (";
		//		while (!clsPremium.EndOfFile())
		//		{
		//			strWhere += strComma + clsPremium.Get_Fields("deductionid");
		//			strComma = ",";
		//			clsPremium.MoveNext();
		//		}
		//		strWhere += ")";
		//	}
		//	// 
		//	// strSubQuery = "(select employeenumber,sum(dedamount) as Premiums  from tblcheckdetail where deductionrecord = 1  and paydate between '" & dtStartDate & "' and '" & dtEndDate & "'" & strWhere & "  group by employeenumber) as ChkQuery "
		//	strSubQuery = "(select tblcheckdetail.employeenumber as employeenumber,sum(dedamount) as Premiums  from tblcheckdetail  inner join (select distinct employeenumber from tblmsrstempdetailreport) as TempDetailQuery on (tblcheckdetail.employeenumber = tempdetailquery.employeenumber) where deductionrecord = 1  and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "'" + strWhere + "  group by tblcheckdetail.employeenumber) as ChkQuery ";
		//	strNonPaidQuery = "(select LifeInsuranceCode as lifeinscode,InsuranceContributions as PremiumsSum from tblMSRSNonPaid where include = 1 and status <> 'R'  and LEFT(isnull(positioncode, ' '),1) = 'Y') ";
		//	// inner join tblmiscupdate with with checkdetail for the regular employees
		//	// union with the same information from the MSRSNonPaid table
		//	strSQL = "(select lifeinscode,sum(premiums) as PremiumsSum from tblmiscupdate inner join " + strSubQuery + "  on (chkQuery.employeenumber = tblmiscupdate.employeenumber)  where tblmiscupdate.include = 1 group by tblmiscupdate.lifeinscode) ";
		//	strSQL += " union " + strNonPaidQuery;
		//	// too many layers of subqueries for access to handle in one statement
		//	// Call clsPremium.CreateStoredProcedure("MSRSCalcPremiumsQuery", strSQL, "twpy0000.vb1")
		//	// select the info we need from it
		//	// Call clsPremium.OpenRecordset("select lifeinscode,sum(premiumssum) as SumPremiums from msrscalcpremiumsquery group by lifeinscode", "twpy0000.vb1")
		//	clsPremium.OpenRecordset("select lifeinscode,sum(premiumssum) as SumPremiums from [ " + strSQL + "]. as msrscalcpremiumsquery group by lifeinscode", "twpy0000.vb1");
		//	while (!clsPremium.EndOfFile())
		//	{
		//		dblDependentTemp = 0;
		//		dblBasicActivesTemp = 0;
		//		dblSupplementalTemp = 0;
		//		dblMTDWithheld = Conversion.Val(clsPremium.Get_Fields("sumpremiums"));
		//		strMSRSCode = FCConvert.ToString(clsPremium.Get_Fields("Lifeinscode"));
		//		// MATTHEW 09/21/2004
		//		// DATA FROM LISA IN JAY FOR THE MONTH OF AUGUST COMES UP WITH A CODE OF "R"
		//		// WE SAY THAT THE CODE IS NOT RECONIZED BUT THEN
		//		if (fecherFoundation.Strings.UCase(strMSRSCode) == "HA")
		//		{
		//			dblDependentTemp = dblDepARate * intWeeks;
		//			dblBasicActivesTemp = dblMTDWithheld - dblDependentTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "HB")
		//		{
		//			dblDependentTemp = dblDepBRate * intWeeks;
		//			dblBasicActivesTemp = dblMTDWithheld - dblDependentTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S1")
		//		{
		//			dblSupplementalTemp = dblMTDWithheld / 2;
		//			dblBasicActivesTemp = dblMTDWithheld - dblSupplementalTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S2")
		//		{
		//			dblBasicActivesTemp = dblMTDWithheld / 3;
		//			dblSupplementalTemp = dblMTDWithheld - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S3")
		//		{
		//			dblBasicActivesTemp = dblMTDWithheld / 4;
		//			dblSupplementalTemp = dblMTDWithheld - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1A")
		//		{
		//			dblDependentTemp = dblDepARate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 2;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2A")
		//		{
		//			dblDependentTemp = dblDepARate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 3;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3A")
		//		{
		//			dblDependentTemp = dblDepARate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 4;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1B")
		//		{
		//			dblDependentTemp = dblDepBRate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 2;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2B")
		//		{
		//			dblDependentTemp = dblDepBRate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 3;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3B")
		//		{
		//			dblDependentTemp = dblDepBRate * intWeeks;
		//			dblTemp = dblMTDWithheld - dblDependentTemp;
		//			dblBasicActivesTemp = dblTemp / 4;
		//			dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "B")
		//		{
		//			dblBasicActivesTemp = dblMTDWithheld;
		//		}
		//		else if (fecherFoundation.Strings.UCase(strMSRSCode) == "I")
		//		{
		//			dblBasicActivesTemp = dblMTDWithheld;
		//		}
		//		else
		//		{
		//			dblBasicActivesTemp = dblMTDWithheld;
		//			MessageBox.Show("Unknown MainePERS Code " + strMSRSCode);
		//		}
		//		clsPremium.MoveNext();
		//		dblDependent += dblDependentTemp;
		//		dblBasicActives += dblBasicActivesTemp;
		//		dblSupplemental += dblSupplementalTemp;
		//	}
		//}

		//private void ActiveReport_Terminate(object sender, EventArgs e)
		//{
		//	if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
		//	{
		//		modDavesSweetCode.Statics.blnReportCompleted = true;
		//	}
		//}

		
	}
}
