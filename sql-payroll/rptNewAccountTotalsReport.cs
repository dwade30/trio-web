//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewAccountTotalsReport.
	/// </summary>
	public partial class rptNewAccountTotalsReport : BaseSectionReport
	{
		public rptNewAccountTotalsReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Account Totals";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewAccountTotalsReport InstancePtr
		{
			get
			{
				return (rptNewAccountTotalsReport)Sys.GetInstance(typeof(rptNewAccountTotalsReport));
			}
		}

		protected rptNewAccountTotalsReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
				rsPayCategories?.Dispose();
				rsEmployee?.Dispose();
				rsData?.Dispose();
                rsPayCategories = null;
                rsEmployee = null;
                rsData = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewAccountTotalsReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       July 22nd, 2005
		//
		// NOTES: WRITTEN FOR DEBBIE IN RUMFORD
		// I'M PRETTY SURE THAT RON WANTED THIS WITHIN THE FULL SET OF REPORTS BUT MY CONCERN THERE
		// IS THAT IF WE DID THAT TH
		//
		// *************************************************************************************************
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
		// private local variables
		int intpage;
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsEmployee = new clsDRWrapper();
		clsDRWrapper rsPayCategories = new clsDRWrapper();
		string strEmployeeNumber = "";
		string strAccountNumber = "";
		double dblTotal;
		double dblAmount;
		double dblHours;
		double dblTotalTotal;
		double dblAmountTotal;
		double dblHoursTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			eArgs.EOF = rsData.EndOfFile();
			if (!rsData.EndOfFile())
			{
				strAccountNumber = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
				this.Fields["grpHeader"].Value = strAccountNumber;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				this.Fields.Add("grpHeader");
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				rsData.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL);
				if (!rsData.EndOfFile())
				{
					this.Fields["grpHeader"].Value = rsData.Get_Fields_String("DistAccountNumber");
				}
				rsEmployee.OpenRecordset("Select * from tblEmployeeMaster");
				rsPayCategories.OpenRecordset("Select * from tblPayCategories");
				fldCaption2.Text = modGlobalVariables.Statics.gstrCheckListingSort;
				//this.Zoom = -1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// do nothing
				employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}
		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				if (strEmployeeNumber == FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")) && strAccountNumber == FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber")))
				{
					txtName.Visible = false;
					txtNumber.Visible = false;
				}
				else
				{
					txtName.Visible = true;
					txtNumber.Visible = true;
					dblTotal = 0;
				}
				strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
				strAccountNumber = FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber"));
				txtTotal.Visible = true;
				if (!rsData.EndOfFile())
				{
					rsData.MoveNext();
					if (!rsData.EndOfFile())
					{
						if (strEmployeeNumber == FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")) && strAccountNumber == FCConvert.ToString(rsData.Get_Fields_String("DistAccountNumber")))
						{
							txtTotal.Visible = false;
						}
					}
					rsData.MovePrevious();
				}
				dblHours = Conversion.Val(rsData.Get_Fields("SumOfDistHours"));
				dblAmount = Conversion.Val(rsData.Get_Fields("SumOfDistGrossPay"));
				dblTotal += Conversion.Val(rsData.Get_Fields("SumOFDistGrossPay"));
				dblTotalTotal += Conversion.Val(rsData.Get_Fields("SumOFDistGrossPay"));
				dblAmountTotal += dblAmount;
				dblHoursTotal += dblHours;
				// txtAccountNumber = strAccountNumber
				txtNumber.Text = strEmployeeNumber;
				txtHours.Text = Strings.Format(dblHours, "0.00");
				txtAmount.Text = Strings.Format(dblAmount, "0.00");
				txtTotal.Text = Strings.Format(dblTotal, "0.00");
				if (rsPayCategories.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields_Int32("DistPayCategory"))))
				{
					// txtType = rsData.Fields("DistPayCategory") & " - " & rsPayCategories.Fields("Description")
					txtType.Text = rsPayCategories.Get_Fields("categorynumber") + " - " + rsPayCategories.Get_Fields("Description");
				}
				else
				{
					txtType.Text = string.Empty;
				}
				if (rsEmployee.FindFirstRecord("EmployeeNumber", strEmployeeNumber))
				{
					txtName.Text = rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("MiddleName") + " " + rsEmployee.Get_Fields_String("LastName");
				}
				rsData.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtTotalHours.Text = Strings.Format(dblHoursTotal, "0.00");
			txtTotalAmount.Text = Strings.Format(dblAmountTotal, "0.00");
			txtTotalTotal.Text = Strings.Format(dblTotalTotal, "0.00");
			dblTotalTotal = 0;
			dblAmountTotal = 0;
			dblHoursTotal = 0;
			dblTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtAccountNumber.Text = strAccountNumber + "  " + modDavesSweetCode.GetAccountDescription(strAccountNumber);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		
	}
}