﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDistributionReport.
	/// </summary>
	public partial class rptDistributionReport : BaseSectionReport
	{
		public rptDistributionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDistributionReport InstancePtr
		{
			get
			{
				return (rptDistributionReport)Sys.GetInstance(typeof(rptDistributionReport));
			}
		}

		protected rptDistributionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDistributionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//clsDRWrapper clsReport = new clsDRWrapper();
		const int CNSTRTYPEEMPNUMBER = 0;
		const int CNSTRTYPEEMPNAME = 1;
		const int CNSTRTYPEPAYCAT = 2;
		const int CNSTRTYPEPAYDATE = 3;
		const int CNSTRTYPEACCOUNT = 4;
		const int CNSTRTYPEDEPTDIV = 5;
		private int intRepType;

		public void Init(ref int intRType, ref bool boolSummary, ref bool boolDetail, string strEmpNo/*= ""*/, DateTime dtStart/*= DateTime.FromOADate(0)*/, DateTime dtEnd/*= DateTime.FromOADate(0)*/, string strAcctStart = "", string strAcctEnd = "", string strgroup = "", string strDist = "")
		{
			string strSQL = "";
			string strWhere;
			string strOrderBy = "";
			string strGroupBy;
			strWhere = " where checkvoid = 0 and distributionrecord = 1";
			if (strEmpNo != string.Empty)
			{
				strWhere += " and employeenumber = '" + strEmpNo + "' ";
			}
			if (dtStart.ToOADate() != 0)
			{
				if (dtEnd.ToOADate() != 0)
				{
					strWhere += " and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) + "' ";
				}
				else
				{
					strWhere += " and paydate = '" + FCConvert.ToString(dtStart) + "' ";
				}
			}
			if (strAcctStart != string.Empty)
			{
				if (strAcctEnd != string.Empty)
				{
					strWhere += " and distaccountnumber between '" + strAcctStart + "' and '" + strAcctEnd + "' ";
				}
				else
				{
					strWhere += " and distaccountnumber = '" + strAcctStart + "' ";
				}
			}
			if (strgroup != string.Empty)
			{
				strWhere += " and groupid = '" + strgroup + "' ";
			}
			if (strDist != string.Empty)
			{
				strWhere += " " + strDist;
			}
			strGroupBy = "";
			intRepType = intRType;
			switch (intRepType)
			{
				case CNSTRTYPEEMPNUMBER:
					{
						if (boolSummary)
						{
							if (boolDetail)
							{
								strGroupBy = " group by tblcheckdetail.employeenumber,distpaycategory ";
							}
							else
							{
								strGroupBy = " group by tblcheckdetail.employeenumber ";
							}
							strOrderBy = " order by tblcheckdetail.employeenumber ";
						}
						else
						{
							strOrderBy = " order by tblcheckdetail.employeenumber,paydate ";
						}
						break;
					}
				case CNSTRTYPEEMPNAME:
					{
						break;
					}
				case CNSTRTYPEPAYCAT:
					{
						break;
					}
				case CNSTRTYPEPAYDATE:
					{
						break;
					}
				case CNSTRTYPEACCOUNT:
					{
						break;
					}
				case CNSTRTYPEDEPTDIV:
					{
						break;
					}
			}
			//end switch
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DistributionReport");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
