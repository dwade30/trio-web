﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsESHPFileCreator
	{
		//=========================================================
		private string strCheckPath = "";
		private string strDatPath = "";
		private string strW2Path = "";
		private int intOrgNumber;
		private string strPayrunPath = "";

		public delegate void CheckRecordAddedEventHandler(ref int intPercent);

		public event CheckRecordAddedEventHandler CheckRecordAdded;

		public delegate void EmployeeRecordAddedEventHandler(ref int intPercent);

		public event EmployeeRecordAddedEventHandler EmployeeRecordAdded;

		public delegate void EmployeeTaxRecordAddedEventHandler(ref int intPercent);

		public event EmployeeTaxRecordAddedEventHandler EmployeeTaxRecordAdded;

		public int OrganizationNumber
		{
			set
			{
				intOrgNumber = value;
			}
			get
			{
				int OrganizationNumber = 0;
				OrganizationNumber = intOrgNumber;
				return OrganizationNumber;
			}
		}

		public string CheckPath
		{
			get
			{
				string CheckPath = "";
				CheckPath = strCheckPath;
				return CheckPath;
			}
		}

		public string DataPath
		{
			get
			{
				string DataPath = "";
				DataPath = strDatPath;
				return DataPath;
			}
		}

		public string PayRunPath
		{
			get
			{
				string PayRunPath = "";
				PayRunPath = strPayrunPath;
				return PayRunPath;
			}
		}

		public bool CreateEmployeeTaxFile(ref clsESHPEmployeeTaxList clsEmployeeTaxes)
		{
			bool CreateEmployeeTaxFile = false;
			bool boolReturn;
			boolReturn = false;
			StreamWriter ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCFileSystem fs = new FCFileSystem();
				int intCount;
				int intTot;
				boolOpen = false;
				intCount = 0;
				string strFile;
				string strPath;
				string strLine = "";
				// vbPorter upgrade warning: intPercent As int	OnWriteFCConvert.ToDouble(
				int intPercent = 0;
				intTot = clsEmployeeTaxes.ItemCount();
				strFile = "O_" + FCConvert.ToString(intOrgNumber) + "_EmployeeTax";
				strFile += Strings.Format(clsEmployeeTaxes.PayDate, "MMddyyyy") + "_" + Strings.Format(DateTime.Now, "hhmmss") + ".dat";
				strPath = strDatPath + Strings.Format(clsEmployeeTaxes.PayDate, "MMddyyyy") + "_" + clsEmployeeTaxes.Payrun + "\\";
				strPayrunPath = Strings.Format(clsEmployeeTaxes.PayDate, "MMddyyyy") + "_" + clsEmployeeTaxes.Payrun + "\\";
				ts = FCFileSystem.CreateTextFile(strPath + strFile);
				clsESHPEmployeeTax tTaxRecord;
				clsEmployeeTaxes.MoveFirst();
				tTaxRecord = clsEmployeeTaxes.GetCurrentEmployeeTax();
				while (!(tTaxRecord == null))
				{
					strLine = GetEmployeeTaxLine(ref tTaxRecord);
					if (!(strLine == ""))
					{
						intCount += 1;
						ts.WriteLine(strLine);
						intPercent = FCConvert.ToInt16((FCConvert.ToDouble(intCount) / intTot) * 100);
						if (this.EmployeeTaxRecordAdded != null)
							this.EmployeeTaxRecordAdded(ref intPercent);
					}
					clsEmployeeTaxes.MoveNext();
					tTaxRecord = clsEmployeeTaxes.GetCurrentEmployeeTax();
				}
				strLine = "EMPTAX" + Strings.StrDup(8, " ") + "T" + Strings.Format(intCount, "000000");
				ts.WriteLine(strLine);
				ts.Close();
				if (!(ts == null))
				{
					ts.Close();
				}
				boolReturn = true;
				CreateEmployeeTaxFile = boolReturn;
				return CreateEmployeeTaxFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in CreateEmployeeTaxFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				if (boolOpen)
				{
					if (!(ts == null))
					{
						ts.Close();
					}
				}
			}
			return CreateEmployeeTaxFile;
		}

		public bool CreateEmployeeFile(ref clsESHPEmployeeList clsEmployees)
		{
			bool CreateEmployeeFile = false;
			bool boolReturn;
			boolReturn = false;
			FCFileSystem fs = new FCFileSystem();
			StreamWriter ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intCount;
				int intTot;
				boolOpen = false;
				intCount = 0;
				string strFile;
				string strPath;
				string strLine = "";
				// vbPorter upgrade warning: intPercent As int	OnWriteFCConvert.ToDouble(
				int intPercent = 0;
				intTot = clsEmployees.ItemCount();
				strFile = "O_" + FCConvert.ToString(intOrgNumber) + "_EmpDetail";
				strFile += Strings.Format(clsEmployees.PayDate, "MMddyyyy") + "_" + Strings.Format(DateTime.Now, "hhmmss") + ".dat";
				strPath = strDatPath + Strings.Format(clsEmployees.PayDate, "MMddyyyy") + "_" + clsEmployees.Payrun + "\\";
				strPayrunPath = Strings.Format(clsEmployees.PayDate, "MMddyyyy") + "_" + clsEmployees.Payrun + "\\";
				ts = FCFileSystem.CreateTextFile(strPath + strFile);
				clsESHPEmployeeDetail tEmpRecord;
				clsEmployees.MoveFirst();
				tEmpRecord = clsEmployees.GetCurrentEmployee();
				while (!(tEmpRecord == null))
				{
					strLine = GetEmployeeLine(ref tEmpRecord);
					if (!(strLine == ""))
					{
						intCount += 1;
						ts.WriteLine(strLine);
						intPercent = FCConvert.ToInt16((FCConvert.ToDouble(intCount) / intTot) * 100);
						if (this.EmployeeRecordAdded != null)
							this.EmployeeRecordAdded(ref intPercent);
					}
					clsEmployees.MoveNext();
					tEmpRecord = clsEmployees.GetCurrentEmployee();
				}
				// footer record
				strLine = "EMPDETAIL" + Strings.StrDup(5, " ") + "T" + Strings.Format(intCount, "000000");
				ts.WriteLine(strLine);
				ts.Close();
				if (!(ts == null))
				{
					ts.Close();
				}
				boolReturn = true;
				CreateEmployeeFile = boolReturn;
				return CreateEmployeeFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in CreateEmployeeFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				if (boolOpen)
				{
					if (!(ts == null))
					{
						ts.Close();
					}
				}
			}
			return CreateEmployeeFile;
		}

		public bool CreateCheckFile(ref clsESHPChecks clsChecks)
		{
			bool CreateCheckFile = false;
			bool boolReturn;
			boolReturn = false;
			FCFileSystem fs = new FCFileSystem();
			StreamWriter ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intCount;
				int intTot;
				boolOpen = false;
				intCount = 0;
				string strFile;
				string strPath;
				string strLine = "";
				// vbPorter upgrade warning: intPercent As int	OnWriteFCConvert.ToDouble(
				int intPercent = 0;
				intTot = clsChecks.ItemCount();
				strFile = "O_" + FCConvert.ToString(intOrgNumber) + "_PayCheck";
				strFile += Strings.Format(clsChecks.PayDate, "MMddyyyy") + "_" + Strings.Format(DateTime.Now, "hhmmss") + ".dat";
				strPath = strCheckPath + Strings.Format(clsChecks.PayDate, "MMddyyyy") + "_" + clsChecks.Payrun + "\\";
				strPayrunPath = Strings.Format(clsChecks.PayDate, "MMddyyyy") + "_" + clsChecks.Payrun + "\\";
				ts = FCFileSystem.CreateTextFile(strPath + strFile);
				clsESHPCheckRecord tCheckRecord;
				clsChecks.MoveFirst();
				tCheckRecord = clsChecks.GetCurrentCheck();
				while (!(tCheckRecord == null))
				{
					strLine = GetCheckLine(ref tCheckRecord);
					if (!(strLine == ""))
					{
						intCount += 1;
						ts.WriteLine(strLine);
						intPercent = FCConvert.ToInt16((FCConvert.ToDouble(intCount) / intTot) * 100);
						if (this.CheckRecordAdded != null)
							this.CheckRecordAdded(ref intPercent);
					}
					clsChecks.MoveNext();
					tCheckRecord = clsChecks.GetCurrentCheck();
				}
				// footer record
				strLine = "PAYCHECKDETAIL" + "T" + Strings.Format(intCount, "000000");
				ts.WriteLine(strLine);
				ts.Close();
				if (!(ts == null))
				{
					ts.Close();
				}
				boolReturn = true;
				CreateCheckFile = boolReturn;
				return CreateCheckFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in CreateCheckFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				if (boolOpen)
				{
					if (!(ts == null))
					{
						ts.Close();
					}
				}
			}
			return CreateCheckFile;
		}

		private string GetCheckLine(ref clsESHPCheckRecord tCheckRecord)
		{
			string GetCheckLine = "";
			string strReturn;
			string strTemp;
			strReturn = "";
			strReturn = "PAYCHECKDETAIL";
			strReturn += "D";
			strTemp = Strings.Left(tCheckRecord.SocialSecurityNumber + "         ", 9);
			strReturn += strTemp;
			strTemp = Strings.StrDup(10, "0");
			strReturn += strTemp;
			strTemp = Strings.Left(tCheckRecord.EMPLOYEENAME + Strings.StrDup(36, " "), 36);
			strReturn += strTemp;
			strTemp = FCConvert.ToString(tCheckRecord.CheckNumber);
			if (tCheckRecord.DIRECTDEPOSIT)
			{
				strTemp = "D" + strTemp;
			}
			strTemp = Strings.Left(strTemp + Strings.StrDup(11, " "), 11);
			strReturn += strTemp;
			strReturn += Strings.Format(tCheckRecord.CheckDate, "MMddyy");
			strTemp = Strings.Right(Strings.StrDup(10, "0") + FCConvert.ToString(tCheckRecord.CheckAmount * 100), 10);
			strReturn += strTemp;
			strReturn += Strings.Left(tCheckRecord.PDFFile + Strings.StrDup(20, " "), 20);
			GetCheckLine = strReturn;
			return GetCheckLine;
		}

		private string GetEmployeeTaxLine(ref clsESHPEmployeeTax tTaxRecord)
		{
			string GetEmployeeTaxLine = "";
			string strReturn;
			string strTemp;
			strReturn = "";
			strReturn = "EMPTAX" + Strings.StrDup(8, " ") + "D";
			strTemp = Strings.Left(tTaxRecord.SocialSecurityNumber + "         ", 9);
			strReturn += strTemp;
			strReturn += Strings.StrDup(10, "0");
			// employee number
			strTemp = Strings.Left(tTaxRecord.Description + Strings.StrDup(30, " "), 30);
			strReturn += strTemp;
			strTemp = Strings.Left(tTaxRecord.SortSequence + Strings.StrDup(1, " "), 1);
			strReturn += strTemp;
			strTemp = Strings.Format(tTaxRecord.StartDate, "MMddyyyy");
			if (strTemp != "")
			{
				// strTemp = Format(strTemp, "MMddyyyy")
			}
			else
			{
				strTemp = Strings.StrDup(8, " ");
			}
			strReturn += strTemp;
			strTemp = Strings.Left(tTaxRecord.FilingStatus + " ", 1);
			strReturn += strTemp;
			strTemp = Strings.Format(tTaxRecord.Exemptions, "000");
			strReturn += strTemp;
			strTemp = Strings.Format(tTaxRecord.AdditionalExemptions, "000");
			strReturn += strTemp;
			strTemp = Strings.Format(tTaxRecord.AdditionalAmount * 100, "0000000000");
			strReturn += strTemp;
			strTemp = Strings.Format(tTaxRecord.AdditionalPercent * 1000, "000000");
			strReturn += strTemp;
			GetEmployeeTaxLine = strReturn;
			return GetEmployeeTaxLine;
		}

		private string GetEmployeeLine(ref clsESHPEmployeeDetail tEmpRecord)
		{
			string GetEmployeeLine = "";
			string strReturn;
			string strTemp;
			strReturn = "";
			strReturn = "EMPDETAIL" + Strings.StrDup(5, " ") + "D";
			strTemp = Strings.Left(tEmpRecord.SocialSecurityNumber + "         ", 9);
			strReturn += strTemp;
			strReturn += Strings.StrDup(10, "0");
			// employee number, cant use
			strTemp = Strings.Left(tEmpRecord.FirstName + Strings.StrDup(15, " "), 15);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.MiddleInitial + " ", 1);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.LastName + Strings.StrDup(20, " "), 20);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.LegalFirstName + Strings.StrDup(15, " "), 15);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.LegalMiddleName + Strings.StrDup(15, " "), 15);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.LegalLastName + Strings.StrDup(20, " "), 20);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.Address1 + Strings.StrDup(30, " "), 30);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.Address2 + Strings.StrDup(30, " "), 30);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.City + Strings.StrDup(20, " "), 20);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.State + "  ", 2);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.ZipCode + Strings.StrDup(10, " "), 10);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.HomePhone + Strings.StrDup(12, " "), 12);
			strReturn += strTemp;
			strReturn += Strings.StrDup(12, " ");
			// cell phone
			strReturn += Strings.StrDup(12, " ");
			// work phone
			strReturn += Strings.StrDup(12, " ");
			// pager number
			strReturn += Strings.StrDup(12, " ");
			// fax number
			strTemp = Strings.Left(tEmpRecord.Email + Strings.StrDup(50, " "), 50);
			strReturn += strTemp;
			strReturn += Strings.Left(tEmpRecord.MaritalStatus + " ", 1);
			strTemp = Strings.Left(tEmpRecord.MainPosition + Strings.StrDup(30, " "), 30);
			strReturn += strTemp;
			strTemp = Strings.Left(FCConvert.ToString(intOrgNumber) + Strings.StrDup(10, " "), 10);
			strReturn += strTemp;
			strTemp = Strings.Left(tEmpRecord.LeaveClassLink + Strings.StrDup(10, " "), 10);
			strReturn += strTemp;
			GetEmployeeLine = strReturn;
			return GetEmployeeLine;
		}

		public clsESHPFileCreator() : base()
		{
			strCheckPath = Environment.CurrentDirectory;
			if (Strings.Right(Environment.CurrentDirectory, 1) != "\\")
			{
				strCheckPath += "\\";
			}
			strDatPath = strCheckPath;
			strCheckPath += "ESHPExport\\Checks\\";
			// strDatPath = strDatPath & "ESHPExport\DatFiles\"
			strDatPath = strCheckPath;
		}
	}
}
