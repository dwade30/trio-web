﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmailList.
	/// </summary>
	partial class rptEmailList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmailList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNumber,
				this.txtEmployeeName,
				this.txtCheck,
				this.txtAmount,
				this.txtEmail
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.txtDate,
				this.lblPage,
				this.txtTime,
				this.txtMuniName,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.PageHeader.Height = 0.8541667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtCaption.Text = null;
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 7.1875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.0625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.Left = 6.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2291667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: left";
			this.Label1.Text = "Employee";
			this.Label1.Top = 0.625F;
			this.Label1.Width = 0.84375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.2291667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Check";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 0.65625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2291667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.5625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Email";
			this.Label3.Top = 0.625F;
			this.Label3.Width = 2.864583F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2291667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Amount";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 0.71875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2291667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.9375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "Name";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 0.84375F;
			// 
			// txtEmployeeNumber
			// 
			this.txtEmployeeNumber.Height = 0.1979167F;
			this.txtEmployeeNumber.Left = 0.0625F;
			this.txtEmployeeNumber.Name = "txtEmployeeNumber";
			this.txtEmployeeNumber.Text = null;
			this.txtEmployeeNumber.Top = 0F;
			this.txtEmployeeNumber.Width = 0.84375F;
			// 
			// txtEmployeeName
			// 
			this.txtEmployeeName.Height = 0.1979167F;
			this.txtEmployeeName.Left = 0.9375F;
			this.txtEmployeeName.Name = "txtEmployeeName";
			this.txtEmployeeName.Text = null;
			this.txtEmployeeName.Top = 0F;
			this.txtEmployeeName.Width = 2.15625F;
			// 
			// txtCheck
			// 
			this.txtCheck.Height = 0.1979167F;
			this.txtCheck.Left = 3.125F;
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Text = null;
			this.txtCheck.Top = 0F;
			this.txtCheck.Width = 0.65625F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1979167F;
			this.txtAmount.Left = 3.8125F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.71875F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.1979167F;
			this.txtEmail.Left = 4.5625F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 0F;
			this.txtEmail.Width = 2.864583F;
			// 
			// rptEmailList
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
