//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBankListingReport.
	/// </summary>
	public partial class rptBankListingReport : BaseSectionReport
	{
		public rptBankListingReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bank Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsData = new FCGrid();
			vsData.Cols = 4;
			vsData.Rows = 1;
			vsSummary = new FCGrid();
			vsSummary.Cols = 4;
			vsSummary.Rows = 2;
			vsHeader = new FCGrid();
			vsHeader.Cols = 4;
			vsReportTotals = new FCGrid();
			vsReportTotals.Cols = 4;
			vsReportTotals.Rows = 2;
		}

		public static rptBankListingReport InstancePtr
		{
			get
			{
				return (rptBankListingReport)Sys.GetInstance(typeof(rptBankListingReport));
			}
		}

		protected rptBankListingReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				vsData?.Dispose();
                vsData = null;
				vsHeader?.Dispose();
                vsHeader = null;
				vsSummary?.Dispose();
                vsSummary = null;
				vsReportTotals?.Dispose();
                vsReportTotals = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBankListingReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		int intRow;
		int intCol;
		int intPageNumber;
		string strEmployeeNumber = "";
		double dblTotal;
		int intCounter;
		double dblGrandAmountTotal;
		double dblAmountTotal;
		int intEmployeeTotal;
		int intBankCode;
		string strDescription;
		int intGrandEmployeeTotal;
		FCGrid vsData;
		FCGrid vsSummary;
		FCGrid vsHeader;
		FCGrid vsReportTotals;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			NextRecord:
			;
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			this.Detail.Visible = true;
			if (intBankCode != FCConvert.ToInt32(rsData.Get_Fields_Int32("Bank")))
			{
				intBankCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("Bank"));
				GetBankAddress(intBankCode);
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("Bank"));
				this.Detail.Visible = false;
				eArgs.EOF = false;
				return;
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			strEmployeeNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblEmployeeMaster.EmployeeNumber")));
			vsData.TextMatrix(0, 0, fecherFoundation.Strings.Trim(strEmployeeNumber));
			vsData.TextMatrix(0, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))));
			intEmployeeTotal += 1;
			intGrandEmployeeTotal += 1;
			intBankCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("Bank"));
			GetBankAddress(intBankCode);
			vsData.TextMatrix(0, 2, rsData.Get_Fields("Account"));
			vsData.TextMatrix(0, 3, Strings.Format(rsData.Get_Fields("Amount"), "#,##0.00"));
			dblAmountTotal += Conversion.Val(rsData.Get_Fields("Amount"));
			dblGrandAmountTotal += Conversion.Val(rsData.Get_Fields("Amount"));
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void GetBankAddress(int intBankID)
		{
			strDescription = string.Empty;
			strDescription = FCConvert.ToString(intBankID) + " - " + rsData.Get_Fields_String("Name");
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblBanks.Address1"))) != string.Empty)
			{
				strDescription += "\r";
				strDescription += fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblBanks.Address1")));
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblBanks.Address2"))) != string.Empty)
			{
				strDescription += "\r";
				strDescription += fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblBanks.Address2")));
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address3"))) != string.Empty)
			{
				strDescription += "\r";
				strDescription += fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address3")));
			}
			// 12/07/2011
			// strDescription = strDescription & Chr(13)
			// strDescription = strDescription & Trim(rsData.Fields("tblBanks.City")) & ", " & Trim(rsData.Fields("tblBanks.State")) & " " & Trim(rsData.Fields("tblBanks.Zip"))
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			// intSetTotals = 0
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			Detail.Height = vsData.Height / 1440F + 30 / 1440F;
			if (!rsData.EndOfFile())
			{
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("Bank"));
				intBankCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("Bank"));
			}
			intRow = 0;
			//modPrintToFile.SetPrintProperties(this);
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			this.Fields["grpHeader"].Value = intBankCode.ToString();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			vsSummary.TextMatrix(0, 0, "Bank");
			vsSummary.TextMatrix(1, 0, "Totals");
			vsSummary.TextMatrix(1, 1, "Number Employees: " + FCConvert.ToString(intEmployeeTotal));
			vsSummary.TextMatrix(1, 3, Strings.Format(dblAmountTotal, "#,##0.00"));
			intEmployeeTotal = 0;
			dblAmountTotal = 0;
		}

		private void GroupHeader1_BeforePrint(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strDes As object	OnWrite(string())
			string[] strDes;
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			strDes = Strings.Split(strDescription, "\r", -1, CompareConstants.vbBinaryCompare);
			vsHeader.Rows = Information.UBound(strDes, 1) + 2;
			GroupHeader1.Height = (GroupHeader1.Height * (vsHeader.Rows - 1));
			vsHeader.Height = FCConvert.ToInt32((GroupHeader1.Height - 100 / 1440F));
			// Line1.X1 = vsHeader.Left
			// Line1.X2 = vsHeader.Width
			// Line1.Y1 = vsHeader.Top + vsHeader.Height + 20
			// Line1.Y2 = vsHeader.Top + vsHeader.Height + 20
			for (intCount = 0; intCount <= (Information.UBound(strDes, 1) - 1); intCount++)
			{
				vsHeader.TextMatrix(intCount, 0, string.Empty);
				vsHeader.TextMatrix(intCount, 1, strDes[intCount]);
				vsHeader.TextMatrix(intCount, 2, strDes[intCount]);
				vsHeader.TextMatrix(intCount, 3, strDes[intCount]);
				vsHeader.MergeRow(intCount, true);
			}
			vsHeader.TextMatrix(intCount + 1, 0, "Employee");
			vsHeader.TextMatrix(intCount + 1, 1, "Employee");
			vsHeader.TextMatrix(intCount + 1, 2, "Account");
			vsHeader.TextMatrix(intCount + 1, 3, "Amount");
			vsHeader.MergeRow(intCount + 1, true);
			vsHeader.ColWidth(0, 800);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(1, 4000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(2, 1500);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsSummary.ColWidth(0, vsHeader.ColWidth(0));
			vsSummary.ColWidth(1, vsHeader.ColWidth(1));
			vsSummary.ColWidth(2, vsHeader.ColWidth(2));
			vsSummary.ColWidth(3, vsHeader.ColWidth(3));
			vsReportTotals.ColWidth(0, vsHeader.ColWidth(0));
			vsReportTotals.ColWidth(1, vsHeader.ColWidth(1));
			vsReportTotals.ColWidth(2, vsHeader.ColWidth(2));
			vsReportTotals.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsSummary.ColAlignment(0, vsHeader.ColAlignment(0));
			vsSummary.ColAlignment(1, vsHeader.ColAlignment(1));
			vsSummary.ColAlignment(2, vsHeader.ColAlignment(2));
			vsSummary.ColAlignment(3, vsHeader.ColAlignment(3));
			vsReportTotals.ColAlignment(0, vsHeader.ColAlignment(0));
			vsReportTotals.ColAlignment(1, vsHeader.ColAlignment(1));
			vsReportTotals.ColAlignment(2, vsHeader.ColAlignment(2));
			vsReportTotals.ColAlignment(3, vsHeader.ColAlignment(3));
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strDes As object	OnWrite(string())
			string[] strDes;
			// vbPorter upgrade warning: intCount As int	OnWriteFCConvert.ToInt32(
			int intCount;
			strDes = Strings.Split(strDescription, "\r", -1, CompareConstants.vbBinaryCompare);
			vsHeader.Rows = Information.UBound(strDes, 1) + 2;
			GroupHeader1.Height = (GroupHeader1.Height * (vsHeader.Rows - 1));
			vsHeader.Height = FCConvert.ToInt32(GroupHeader1.Height - 100 / 1440F);
			Line1.X1 = vsHeader.Left;
			Line1.X2 = vsHeader.Width;
			Line1.Y1 = vsHeader.Top + vsHeader.Height + 20 / 1440F;
			Line1.Y2 = vsHeader.Top + vsHeader.Height + 20 / 1440F;
			for (intCount = 0; intCount <= (Information.UBound(strDes, 1) - 1); intCount++)
			{
				vsHeader.TextMatrix(intCount, 0, string.Empty);
				vsHeader.TextMatrix(intCount, 1, strDes[intCount]);
				vsHeader.TextMatrix(intCount, 2, strDes[intCount]);
				vsHeader.TextMatrix(intCount, 3, strDes[intCount]);
				vsHeader.MergeRow(intCount, true);
			}
			vsHeader.TextMatrix(intCount + 1, 0, "Employee");
			vsHeader.TextMatrix(intCount + 1, 1, "Employee");
			vsHeader.TextMatrix(intCount + 1, 2, "Account");
			vsHeader.TextMatrix(intCount + 1, 3, "Amount");
			vsHeader.MergeRow(intCount + 1, true);
			vsHeader.ColWidth(0, 800);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(1, 4000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(2, 1500);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsSummary.ColWidth(0, vsHeader.ColWidth(0));
			vsSummary.ColWidth(1, vsHeader.ColWidth(1));
			vsSummary.ColWidth(2, vsHeader.ColWidth(2));
			vsSummary.ColWidth(3, vsHeader.ColWidth(3));
			vsReportTotals.ColWidth(0, vsHeader.ColWidth(0));
			vsReportTotals.ColWidth(1, vsHeader.ColWidth(1));
			vsReportTotals.ColWidth(2, vsHeader.ColWidth(2));
			vsReportTotals.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsSummary.ColAlignment(0, vsHeader.ColAlignment(0));
			vsSummary.ColAlignment(1, vsHeader.ColAlignment(1));
			vsSummary.ColAlignment(2, vsHeader.ColAlignment(2));
			vsSummary.ColAlignment(3, vsHeader.ColAlignment(3));
			vsReportTotals.ColAlignment(0, vsHeader.ColAlignment(0));
			vsReportTotals.ColAlignment(1, vsHeader.ColAlignment(1));
			vsReportTotals.ColAlignment(2, vsHeader.ColAlignment(2));
			vsReportTotals.ColAlignment(3, vsHeader.ColAlignment(3));
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			vsReportTotals.TextMatrix(0, 0, "Final");
			vsReportTotals.TextMatrix(1, 0, "Totals");
			vsReportTotals.TextMatrix(1, 1, "Number Employees: " + FCConvert.ToString(intGrandEmployeeTotal));
			vsReportTotals.TextMatrix(1, 3, Strings.Format(dblGrandAmountTotal, "#,##0.00"));
		}

		

		private void ActiveReports_DataInitalize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
