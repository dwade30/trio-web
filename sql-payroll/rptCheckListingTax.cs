//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingTax.
	/// </summary>
	public partial class rptCheckListingTax : BaseSectionReport
	{
		public rptCheckListingTax()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Check Listing Tax";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCheckListingTax InstancePtr
		{
			get
			{
				return (rptCheckListingTax)Sys.GetInstance(typeof(rptCheckListingTax));
			}
		}

		protected rptCheckListingTax _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
				rsMain?.Dispose();
				rsMaster?.Dispose();
				rsWages?.Dispose();
				rsDeductions?.Dispose();
				rsVacSick?.Dispose();
				rsType?.Dispose();
                rsMain = null;
                rsMaster = null;
                rsWages = null;
                rsDeductions = null;
                rsVacSick = null;
                rsType = null;

            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckListingTax	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// **************************************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       JUNE 10TH, 2005
		//
		// MODIFIED BY:
		//
		// NOTES:
		//
		//
		//
		// **************************************************************************************************
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsMaster = new clsDRWrapper();
		private clsDRWrapper rsWages = new clsDRWrapper();
		private clsDRWrapper rsDeductions = new clsDRWrapper();
		private clsDRWrapper rsVacSick = new clsDRWrapper();
		private double dblTotal;
		private double dblHoursTotal;
		private clsDRWrapper rsType = new clsDRWrapper();
		private int intPageNumber;
		private double dblTotalAmount;
		private double dblTotalRegular;
		private double dblTotalOther;
		private double dblTotalFederal;
		private double dblTotalFICA;
		private double dblTotalMedicare;
		private double dblTotalState;
		private double dblGroupAmount;
		private double dblGroupRegular;
		private double dblGroupOther;
		private double dblGroupFederal;
		private double dblGroupFICA;
		private double dblGroupMedicare;
		private double dblGroupState;
		private bool boolClearGroupTotals;
		private bool boolWagesAsGross;
		private double dblGroupGross;
		private double dblTotalGross;

		public void Init(ref bool boolShowWagesAsGross)
		{
			boolWagesAsGross = boolShowWagesAsGross;
			if (boolWagesAsGross)
			{
				txtRegular.Visible = false;
				txtOther.Visible = false;
				txtGross.Visible = true;
				lblRegular.Visible = false;
				lblOther.Visible = false;
				lblGross.Visible = true;
				txtTotalRegular.Visible = false;
				txtTotalOther.Visible = false;
				txtTotGross.Visible = true;
				txtGroupGross.Visible = true;
				txtGroupOther.Visible = false;
				txtGroupRegular.Visible = false;
			}
			else
			{
				txtRegular.Visible = true;
				txtOther.Visible = true;
				txtGross.Visible = false;
				lblRegular.Visible = true;
				lblOther.Visible = true;
				lblGross.Visible = false;
				txtTotalRegular.Visible = true;
				txtTotalOther.Visible = true;
				txtTotGross.Visible = false;
				txtGroupGross.Visible = false;
				txtGroupOther.Visible = true;
				txtGroupRegular.Visible = true;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "CheckListing");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				double dblGross;
				NextRecord:
				;
				// CHECK TO SEE IF THE DATABASE IS DONE.
				while (!rsMaster.EndOfFile())
				{
					//Application.DoEvents();
					if (!employeeDict.ContainsKey(rsMaster.Get_Fields("EmployeeNumber")))
					{
						rsMaster.MoveNext();
					}
					else
					{
						break;
					}
				}
				if (rsMaster.EndOfFile())
				{
					this.Fields["grpHeader"].Value = "0";
					frmWait.InstancePtr.Unload();
					eArgs.EOF = true;
					return;
				}
				if (frmWait.InstancePtr.prgProgress.Value == 99)
				{
					frmWait.InstancePtr.prgProgress.Value = 0;
				}
				frmWait.InstancePtr.IncrementProgress();
				rsMain.OpenRecordset("SELECT tblCheckDetail.PayDate, tblCheckDetail.CheckVoid, tblCheckDetail.CheckNumber, tblCheckDetail.NetPay, tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.FederalTaxWH, tblCheckDetail.StateTaxWH, tblCheckDetail.FICATaxWH, tblCheckDetail.MedicareTaxWH From tblCheckDetail " + "GROUP BY tblCheckDetail.PayDate, tblCheckDetail.CheckVoid, tblCheckDetail.CheckNumber, tblCheckDetail.NetPay, tblCheckDetail.EmployeeNumber, tblCheckDetail.EmployeeName, tblCheckDetail.TotalRecord, tblCheckDetail.PayRunID, tblCheckDetail.FederalTaxWH, tblCheckDetail.StateTaxWH, tblCheckDetail.LocalTaxWH, tblCheckDetail.FICATaxWH, tblCheckDetail.MedicareTaxWH HAVING ((tblCheckDetail.TotalRecord = 1) AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND tblCheckDetail.CheckNumber = " + rsMaster.Get_Fields("CheckNumber") + ")", "Payroll");
				// corey 1/05/2006
				// Call rsWages.OpenRecordset("SELECT tblCheckDetail.DistributionRecord, tblCheckDetail.DistHours, tblCheckDetail.DistGrossPay, tblCheckDetail.CheckNumber, tblPayCategories.Description, tblPayCategories.ID FROM tblCheckDetail INNER JOIN tblPayCategories ON tblCheckDetail.DistPayCategory = tblPayCategories.ID GROUP BY tblCheckDetail.CheckNumber, tblCheckDetail.DistributionRecord, tblCheckDetail.DistHours, tblCheckDetail.DistGrossPay, tblPayCategories.Description, tblPayCategories.ID,tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate "
				// & "Having (((tblCheckDetail.DistributionRecord) = True) AND EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "' AND PayDate = '" & rsMaster.Fields("PayDate") & "' AND PayRunID = " & rsMaster.Fields("PayRunID") & " AND tblCheckDetail.CheckNumber = " & rsMaster.Fields("CheckNumber") & ") ORDER BY tblPayCategories.ID", DEFAULTDATABASE)
				rsWages.OpenRecordset("SELECT tblCheckDetail.DistributionRecord, Sum(tblCheckDetail.DistHours) as SumOfDistHours, Sum(tblCheckDetail.DistGrossPay) as SumOfDistGrossPay, tblCheckDetail.CheckNumber, tblPayCategories.Description, tblPayCategories.ID, tblPayCategories.Type FROM tblCheckDetail INNER JOIN tblPayCategories ON tblCheckDetail.DistPayCategory = tblPayCategories.ID GROUP BY tblCheckDetail.DistributionRecord, tblCheckDetail.DistHours, tblCheckDetail.DistGrossPay, tblCheckDetail.CheckNumber, tblPayCategories.Description, tblPayCategories.ID, tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate, tblPayCategories.Type " + "Having ((tblCheckDetail.DistributionRecord = 1) AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND tblCheckDetail.CheckNumber = " + rsMaster.Get_Fields("CheckNumber") + ") ORDER BY tblPayCategories.ID", modGlobalVariables.DEFAULTDATABASE);
				rsDeductions.OpenRecordset("SELECT Sum(tblCheckDetail.DedAmount) AS SumOfDedAmount, tblCheckDetail.CheckNumber, tblCheckDetail.DeductionRecord From tblCheckDetail GROUP BY tblCheckDetail.CheckNumber, tblCheckDetail.DeductionRecord,tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate Having (((tblCheckDetail.DeductionRecord) = 1) AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND CheckNumber = " + rsMaster.Get_Fields("CheckNumber") + ")", modGlobalVariables.DEFAULTDATABASE);
				rsVacSick.OpenRecordset("SELECT tblCheckDetail.CheckNumber, tblCheckDetail.VSRecord, Sum(tblCheckDetail.VSUsed) AS SumOfVSUsed, tblCheckDetail.VSTypeID From tblCheckDetail GROUP BY tblCheckDetail.CheckNumber, tblCheckDetail.VSRecord, tblCheckDetail.VSTypeID,tblCheckDetail.EmployeeNumber, tblCheckDetail.PayRunID, tblCheckDetail.PayDate Having (((tblCheckDetail.VSRecord) = 1) AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND CheckNumber = " + rsMaster.Get_Fields("CheckNumber") + ")", modGlobalVariables.DEFAULTDATABASE);
				if (!rsMain.EndOfFile())
				{
					this.Fields["grpHeader"].Value = rsMain.Get_Fields(modGlobalVariables.Statics.gstrCheckListingReportGroup);
					// If Trim(rsMain.Fields("EmployeeNumber")) > 5 Then
					// txtEmployee.Text = rsMain.Fields("EmployeeNumber") & " " & rsMain.Fields("EmployeeName")
					// Else
					// txtEmployee.Text = rsMain.Fields("EmployeeNumber") & String(5 - Len(Trim(rsMain.Fields("EmployeeNumber"))), " ") & rsMain.Fields("EmployeeName")
					// End If
					// 
					// txtDateField.Text = rsMain.Fields("PayDate")
					// 
					// Call rsType.OpenRecordset("SELECT tblCheckDetail.* From tblCheckDetail where TotalRecord = 1 AND EmployeeNumber = '" & rsMaster.Fields("EmployeeNumber") & "' AND PayDate = '" & rsMaster.Fields("PayDate") & "' AND PayRunID = " & rsMaster.Fields("PayRunID") & " AND CheckNumber = " & rsMaster.Fields("CheckNumber"), DEFAULTDATABASE)
					// If Val(rsType.Fields("NumberDirectDeposits")) <> 0 Then
					// txtType.Text = "  D"
					// ElseIf Val(rsType.Fields("NumberChecks")) <> 0 Then
					// txtType.Text = "  R"
					// ElseIf Val(rsType.Fields("NumberBoth")) <> 0 Then
					// txtType.Text = "  B"
					// Else
					// txtType.Text = vbNullString
					// End If
					// 
					// txtVoid.Text = IIf(rsMain.Fields("CheckVoid"), "  Y", "  N")
					// txtCheckNumber.Text = rsMain.Fields("CheckNumber")
					// txtAmount.Text = Format(rsMain.Fields("NetPay"), "0.00")
					// dblTotalAmount = dblTotalAmount + Format(rsMain.Fields("NetPay"), "0.00")
				}
				else
				{
					if (!rsMaster.EndOfFile())
						rsMaster.MoveNext();
					goto NextRecord;
				}
				
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// If gtypeFullSetReports.boolFullSet = False Then
			// Call UpdatePayrollStepTable("DEForms")
			// Else
			// blnReportCompleted = True
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intPageNumber = 0;
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsMaster.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, modGlobalVariables.DEFAULTDATABASE);
			lblSort.Text = modGlobalVariables.Statics.gstrCheckListingSort;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolClearGroupTotals = false;
			if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}
		private void ActiveReport_ReportEndedAndCanceled(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
		private void Detail_Format(object sender, EventArgs e)
		{
			double dblGross;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"))).Length > 5)
				{
					txtEmployee.Text = rsMain.Get_Fields_String("EmployeeNumber") + " " + rsMain.Get_Fields_String("EmployeeName");
				}
				else
				{
					txtEmployee.Text = rsMain.Get_Fields_String("EmployeeNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"))).Length, " ") + rsMain.Get_Fields_String("EmployeeName");
				}
				txtDateField.Text = FCConvert.ToString(rsMain.Get_Fields("PayDate"));
				rsType.OpenRecordset("SELECT tblCheckDetail.* From tblCheckDetail where TotalRecord = 1 AND EmployeeNumber = '" + rsMaster.Get_Fields("EmployeeNumber") + "' AND PayDate = '" + rsMaster.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + rsMaster.Get_Fields("PayRunID") + " AND CheckNumber = " + rsMaster.Get_Fields("CheckNumber"), modGlobalVariables.DEFAULTDATABASE);
				if (Conversion.Val(rsType.Get_Fields_Int32("NumberDirectDeposits")) != 0)
				{
					txtType.Text = "  D";
				}
				else if (Conversion.Val(rsType.Get_Fields_Int32("NumberChecks")) != 0)
				{
					txtType.Text = "  R";
				}
				else if (Conversion.Val(rsType.Get_Fields_Int32("NumberBoth")) != 0)
				{
					txtType.Text = "  B";
				}
				else
				{
					txtType.Text = string.Empty;
				}
				txtVoid.Text = (FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("CheckVoid")) ? "  Y" : "  N");
				txtCheckNumber.Text = FCConvert.ToString(rsMain.Get_Fields("CheckNumber"));
				dblTotal = 0;
				dblHoursTotal = 0;
				txtRegular.Text = Strings.Format(0, "0.00");
				dblGross = 0;
				txtGross.Text = "0.00";
				while (!rsWages.EndOfFile())
				{
					if (FCConvert.ToInt32(rsWages.Get_Fields("ID")) == 1)
					{
						// txtRegular.Text = CDbl(txtRegular.Text) + Format(rsWages.Fields("DistGrossPay"), "0.00")
						// dblTotalRegular = dblTotalRegular + Format(rsWages.Fields("DistGrossPay"), "0.00")
						txtRegular.Text = FCConvert.ToString(FCConvert.ToDouble(txtRegular.Text) + FCConvert.ToDouble(Strings.Format(rsWages.Get_Fields("sumofdistgrosspay"), "0.00")));
						dblTotalRegular += FCConvert.ToDouble(Strings.Format(rsWages.Get_Fields("sumofdistgrosspay"), "0.00"));
						dblGross += Conversion.Val(rsWages.Get_Fields("sumofdistgrosspay"));
					}
					else
					{
						// dblTotal = dblTotal + rsWages.Fields("DistGrossPay")
						dblTotal += Conversion.Val(rsWages.Get_Fields("sumofdistgrosspay"));
						dblGross += Conversion.Val(rsWages.Get_Fields("sumofdistgrosspay"));
					}
					// dblHoursTotal = dblHoursTotal + rsWages.Fields("DistHours")
					dblHoursTotal += Conversion.Val(rsWages.Get_Fields("sumofdisthours"));
					rsWages.MoveNext();
				}
				txtOther.Text = Strings.Format(dblTotal, "0.00");
				dblTotalOther += FCConvert.ToDouble(Strings.Format(dblTotal, "0.00"));
				txtGross.Text = Strings.Format(dblGross, "0.00");
				dblTotalGross += dblGross;
				if (rsMain.EndOfFile())
				{
					txtFederalWH.Text = "0.00";
					txtFICAWH.Text = "0.00";
					txtMedWH.Text = "0.00";
					txtStateWH.Text = "0.00";
					txtAmount.Text = Strings.Format(Conversion.Val(rsMain.Get_Fields_Decimal("NetPay")), "0.00");
					dblTotalAmount += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsMain.Get_Fields_Decimal("NetPay")), "0.00"));
				}
				else
				{
					double dblCurFed = 0;
					double dblCurFica = 0;
					double dblCurMed = 0;
					double dblCurState = 0;
					double dblCurNet = 0;
					dblCurNet = 0;
					dblCurFed = 0;
					dblCurFica = 0;
					dblCurMed = 0;
					dblCurState = 0;
					dblCurState = 0;
					while (!rsMain.EndOfFile())
					{
						dblCurNet += Conversion.Val(rsMain.Get_Fields("netpay"));
						dblCurFed += Conversion.Val(rsMain.Get_Fields("federaltaxwh"));
						dblCurFica += Conversion.Val(rsMain.Get_Fields("ficataxwh"));
						dblCurMed += Conversion.Val(rsMain.Get_Fields("medicaretaxwh"));
						dblCurState += Conversion.Val(rsMain.Get_Fields("statetaxwh"));
						rsMain.MoveNext();
					}
					txtFederalWH.Text = Strings.Format(dblCurFed, "0.00");
					dblTotalFederal += dblCurFed;
					txtAmount.Text = Strings.Format(dblCurNet, "0.00");
					dblTotalAmount += FCConvert.ToDouble(Strings.Format(dblCurNet, "0.00"));
					txtFICAWH.Text = Strings.Format(dblCurFica, "0.00");
					dblTotalFICA += dblCurFica;
					txtMedWH.Text = Strings.Format(dblCurMed, "0.00");
					dblTotalMedicare += dblCurMed;
					txtStateWH.Text = Strings.Format(dblCurState, "0.00");
					dblTotalState += dblCurState;
				}
				dblGroupAmount += Conversion.Val(txtAmount.Text);
				dblGroupRegular += Conversion.Val(txtRegular.Text);
				dblGroupOther += Conversion.Val(txtOther.Text);
				dblGroupGross = dblGroupRegular + dblGroupOther;
				dblGroupFederal += Conversion.Val(txtFederalWH.Text);
				dblGroupFICA += Conversion.Val(txtFICAWH.Text);
				dblGroupMedicare += Conversion.Val(txtMedWH.Text);
				dblGroupState += Conversion.Val(txtStateWH.Text);
				if (!rsMaster.EndOfFile())
					rsMaster.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Detail_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// If rsMaster.EndOfFile Then
			txtGroupAmount.Text = Strings.Format(dblGroupAmount, "0.00");
			txtGroupRegular.Text = Strings.Format(dblGroupRegular, "0.00");
			txtGroupOther.Text = Strings.Format(dblGroupOther, "0.00");
			txtGroupFederal.Text = Strings.Format(dblGroupFederal, "0.00");
			txtGroupFICA.Text = Strings.Format(dblGroupFICA, "0.00");
			txtGroupMedicare.Text = Strings.Format(dblGroupMedicare, "0.00");
			txtGroupState.Text = Strings.Format(dblGroupState, "0.00");
			txtGroupGross.Text = Strings.Format(dblGroupGross, "0.00");

			dblGroupAmount = 0;
			dblGroupRegular = 0;
			dblGroupOther = 0;
			dblGroupGross = 0;
			dblGroupFederal = 0;
			dblGroupFICA = 0;
			dblGroupMedicare = 0;
			dblGroupState = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// If boolClearGroupTotals Then
			// dblGroupAmount = txtAmount
			// dblGroupRegular = txtRegular
			// dblGroupOther = txtOther
			// dblGroupFederal = txtFederalWH
			// dblGroupFICA = txtFICAWH
			// dblGroupMedicare = txtMedWH
			// dblGroupState = txtStateWH
			// dblGroupLocal = txtLocalWH
			// dblGroupGross = txtGross
			// Else
			// boolClearGroupTotals = True
			// End If
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// MATTHEW 5/19/2005
			// 
			txtTotalAmount.Text = Strings.Format(dblTotalAmount, "0.00");
			txtTotalRegular.Text = Strings.Format(dblTotalRegular, "0.00");
			txtTotalOther.Text = Strings.Format(dblTotalOther, "0.00");
			txtTotalFederal.Text = Strings.Format(dblTotalFederal, "0.00");
			txtTotalFICA.Text = Strings.Format(dblTotalFICA, "0.00");
			txtTotalMedicare.Text = Strings.Format(dblTotalMedicare, "0.00");
			txtTotalState.Text = Strings.Format(dblTotalState, "0.00");
			txtTotGross.Text = Strings.Format(dblTotalGross, "0.00");
		}

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
