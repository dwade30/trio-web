//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsACHSetupController
	{
		//=========================================================
		public bool Load(ref clsACHSetup tACHSetup)
		{
			bool Load = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsLoad.OpenRecordset("select * from tblachinformation", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					tACHSetup.EmployerAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("employeraccount")));
					int intTemp = 0;
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("employerAccountType"))));
					if (intTemp < 20)
					{
						intTemp = 27;
					}
					tACHSetup.EmployerAccountType = intTemp;
					tACHSetup.EmployerID = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("employerid")));
					tACHSetup.EmployerName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("employername")));
					tACHSetup.EmployerRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("employerrt")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("immediateoriginname"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("immediateoriginname")));
					}
					else
					{
						tACHSetup.ImmediateOriginName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("employername")));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("odfinum"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginODFI = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("odfinum")));
					}
					else
					{
						tACHSetup.ImmediateOriginODFI = tACHSetup.EmployerRT;
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("immediateoriginrt"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("immediateoriginrt")));
					}
					else
					{
						tACHSetup.ImmediateOriginRT = tACHSetup.EmployerRT;
					}
					tACHSetup.ImmediateDestinationName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("achbankname")));
					tACHSetup.ImmediateDestinationRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("achbankrt")));
				}
				else
				{
					rsLoad.OpenRecordset("select * from GLOBALVARIABLES", "SystemSettings");
					tACHSetup.EmployerName = rsLoad.Get_Fields("citytown") + " of " + rsLoad.Get_Fields("muniname");
					tACHSetup.EmployerAccountType = 27;
					return Load;
				}
				Load = true;
				return Load;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return Load;
		}

		public bool Save(ref clsACHSetup tACHSetup)
		{
			bool Save = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from tblACHInformation");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("achbankrt", tACHSetup.ImmediateDestinationRT);
				rsSave.Set_Fields("achbankname", tACHSetup.ImmediateDestinationName);
				rsSave.Set_Fields("ImmediateOriginRT", tACHSetup.ImmediateOriginRT);
				rsSave.Set_Fields("odfinum", tACHSetup.ImmediateOriginODFI);
				rsSave.Set_Fields("immediateOriginName", tACHSetup.ImmediateOriginName);
				rsSave.Set_Fields("employeraccount", tACHSetup.EmployerAccount);
				rsSave.Set_Fields("employerAccountType", tACHSetup.EmployerAccountType);
				rsSave.Set_Fields("employerid", tACHSetup.EmployerID);
				rsSave.Set_Fields("EmployerName", tACHSetup.EmployerName);
				rsSave.Set_Fields("EmployerRT", tACHSetup.EmployerRT);
				rsSave.Update();
				Save = true;
				return Save;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return Save;
		}
	}
}
