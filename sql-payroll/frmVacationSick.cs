//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmVacationSick : BaseForm
	{
		public frmVacationSick()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVacationSick InstancePtr
		{
			get
			{
				return (frmVacationSick)Sys.GetInstance(typeof(frmVacationSick));
			}
		}

		protected frmVacationSick _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE AND DAN C. SOLTESZ
		// DATE:       APRIL 25,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ ON 6/12/01
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		// Private dbVacationSick      As DAO.Database
		private clsDRWrapper rsVacationSick = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, bool)
		private int intDataChanged;
		private int intSave;
		private string intCodeCaption = string.Empty;
		private string strPER = "";
		private int intCheckCount;
		// vbPorter upgrade warning: intRowSave As int	OnWriteFCConvert.ToInt32(
		private int intRowSave;
		private int intDelType;
		private bool boolSelect;
		private bool boolSave;
		private bool boolNoSave;
		private int intZeroRowId;
		// vbPorter upgrade warning: intZeroCodeId As int	OnWrite(int, string)
		private int intZeroCodeId;
		public bool boolFromVacSick;
		private int intID;
		// vbPorter upgrade warning: intChildID As int	OnRead(string)
		private int intChildID;
		private bool boolChange;
		// vbPorter upgrade warning: intTypeID As int	OnWriteFCConvert.ToInt32(
		private int intTypeID;
		private int intCodeID;
		private bool boolFromType;
		private bool boolGridSort;
		private bool boolCantEdit;
		const int TypeCode = 1;
		const int Code = 2;
		const int des = 3;
		const int Rate = 4;
		const int um = 5;
		const int typ = 6;
		const int ID = 7;
		const int md = 8;
		const int Max = 9;
		const int sel = 10;

		private struct Code0
		{
			// vbPorter upgrade warning: codes As int	OnWrite(string)
			public int codes;
			public string Descriptions;
			// vbPorter upgrade warning: Rates As double	OnWrite(string)
			public double Rates;
			public string ums;
			public string typs;
			public string dates;
			// vbPorter upgrade warning: maxs As double	OnWrite(string)
			public double maxs;
		};

		private Code0 UserCode0 = new Code0();
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		public bool FormDirty
		{
			get
			{
				bool FormDirty = false;
				FormDirty = intDataChanged != 0;
				return FormDirty;
			}
			set
			{
				if (!value)
				{
					intDataChanged = 0;
				}
			}
		}

		private void frmVacationSick_Resize(object sender, System.EventArgs e)
		{
			vsCodes.ColWidth(0, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.05));
			vsCodes.ColWidth(TypeCode, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.1));
			vsCodes.ColWidth(Code, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.07));
			vsCodes.ColWidth(des, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.2));
			vsCodes.ColWidth(Rate, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.1));
			vsCodes.ColWidth(um, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.1));
			vsCodes.ColWidth(typ, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.22));
			vsCodes.ColWidth(md, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.1));
			vsCodes.ColWidth(Max, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.1));
			// vsCodes.ColWidth(sel) = vsCodes.Width * 0.12
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void mnuSelectEmployee_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
			{
			}
			else
			{
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				modGlobalRoutines.SetEmployeeCaption(lblCurrentEmployee, modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
				LoadBalances();
				LoadGrid();
			}
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int x;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cboType_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				bool blnSelected = false;
				boolFromType = true;
				if (cboType.SelectedIndex < 0)
					intTypeID = 0;
				intTypeID = cboType.ItemData(cboType.SelectedIndex);
				txtEmployee.Text = txtEmployee.Tag + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				// & vbNewLine & cboType.List(cboType.ListIndex)
				intCodeID = 0;
				txtCode.Text = "Type: " + cboType.Text;
				LoadBalances();
				LoadGrid();
				if (cboType.SelectedIndex == 0)
				{
					txtAccruedCurrent.Enabled = false;
					txtAccruedFiscal.Enabled = false;
					txtAccruedCalendar.Enabled = false;
					txtUsedCurrent.Enabled = false;
					txtUsedFiscal.Enabled = false;
					txtUsedCalendar.Enabled = false;
					txtUsedBalance.Enabled = false;
				}
				else
				{
					blnSelected = false;
					for (intCounter = 1; intCounter <= (vsCodes.Rows - 1); intCounter++)
					{
						if (Conversion.Val(vsCodes.TextMatrix(intCounter, sel)) == 1)
						{
							blnSelected = true;
							break;
						}
					}
					if (!blnSelected)
					{
						txtAccruedCurrent.Enabled = false;
						txtAccruedFiscal.Enabled = false;
						txtAccruedCalendar.Enabled = false;
						txtUsedCurrent.Enabled = false;
						txtUsedFiscal.Enabled = false;
						txtUsedCalendar.Enabled = false;
						txtUsedBalance.Enabled = false;
					}
					else
					{
						txtAccruedCurrent.Enabled = true;
						txtAccruedFiscal.Enabled = true;
						txtAccruedCalendar.Enabled = true;
						txtUsedCurrent.Enabled = true;
						txtUsedFiscal.Enabled = true;
						txtUsedCalendar.Enabled = true;
						txtUsedBalance.Enabled = true;
					}
				}
				// Dave 12/14/2006--------------------------------------------
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cboType_Click()
		{
			cboType_SelectedIndexChanged(cboType, new System.EventArgs());
		}

		private void LoadBalances()
		{
			boolChange = false;
			rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID) + " order by selected desc", "TWPY0000.vb1");
			if (!rsVacationSick.EndOfFile())
			{
				rsVacationSick.MoveLast();
				rsVacationSick.MoveFirst();
				txtAccruedCurrent.Text = Strings.Format(rsVacationSick.Get_Fields_Double("AccruedCurrent"), "0.00");
				txtAccruedCurrent.Tag = txtAccruedCurrent.Text;
				txtAccruedFiscal.Text = Strings.Format(rsVacationSick.Get_Fields_Double("AccruedFiscal"), "0.00");
				txtAccruedFiscal.Tag = txtAccruedFiscal.Text;
				txtAccruedCalendar.Text = Strings.Format(rsVacationSick.Get_Fields_Double("AccruedCalendar"), "0.00");
				txtAccruedCalendar.Tag = txtAccruedCalendar.Text;
				txtUsedCurrent.Text = Strings.Format(rsVacationSick.Get_Fields_Double("UsedCurrent"), "0.00");
				txtUsedCurrent.Tag = txtUsedCurrent.Text;
				txtUsedFiscal.Text = Strings.Format(rsVacationSick.Get_Fields_Double("UsedFiscal"), "0.00");
				txtUsedFiscal.Tag = txtUsedFiscal.Text;
				txtUsedCalendar.Text = Strings.Format(rsVacationSick.Get_Fields_Double("UsedCalendar"), "0.00");
				txtUsedCalendar.Tag = txtUsedCalendar.Text;
				txtUsedBalance.Text = Strings.Format(rsVacationSick.Get_Fields_Double("UsedBalance"), "0.00");
				txtUsedBalance.Tag = txtUsedBalance.Text;
			}
			else
			{
				ClearBalances();
			}
			boolChange = true;
		}

		public void ClearBalances()
		{
			txtAccruedCurrent.Text = FCConvert.ToString(0);
			txtAccruedFiscal.Text = FCConvert.ToString(0);
			txtAccruedCalendar.Text = FCConvert.ToString(0);
			txtUsedCurrent.Text = FCConvert.ToString(0);
			txtUsedFiscal.Text = FCConvert.ToString(0);
			txtUsedCalendar.Text = FCConvert.ToString(0);
			txtUsedBalance.Text = FCConvert.ToString(0);
		}

		private void cboType_Enter(object sender, System.EventArgs e)
		{
			// If intDataChanged > 0 Then Call SaveChanges
		}

		private void frmVacationSick_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				lblCurrentEmployee.Text = "Employee:  " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
				txtEmployee.Text = txtEmployee.Tag + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				// & vbNewLine & cboType.List(cboType.ListIndex)
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVacationSick_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (KeyAscii == Keys.Escape)
					Close();
				if (KeyAscii == Keys.Return)
				{
					Support.SendKeys("{TAB}", false);
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVacationSick_Load(object sender, System.EventArgs e)
		{
            int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolCantEdit = false;
				modGlobalRoutines.Statics.gboolShowVerifyTotalsMessage = false;
				// vsElasticLight1.Enabled = True
				modGlobalRoutines.CheckCurrentEmployee();
				boolSave = true;
				boolSelect = false;
				boolFromVacSick = false;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				// set the global captions / titles for the form

				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == string.Empty)
				{
					MessageBox.Show("No current employee was selected. A new record must be added.", "Payroll Employee Add/Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// set the grid column headers/widths/etc....
				SetGridProperties();
				modMultiPay.Statics.gstrEmployeeCaption = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modMultiPay.SetMultiPayCaptions(this);
				// load the combo with the code types
				LoadCombo();
				if (cboType.Items.Count > 0)
					cboType.SelectedIndex = 0;
				if (vsCodes.Rows > 0)
					vsCodes.Select(0, 0);
				clsHistoryClass.SetGridIDColumn("PY", "vsCodes", 7);
				//FC:FINAL:AM:2570 - don't start edit mode
                //vsCodes.EditCell();
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					cmdSave.Enabled = false;
					cmdEdit.Enabled = false;
					vsCodes.Editable = FCGrid.EditableSettings.flexEDNone;
					txtAccruedCalendar.Enabled = false;
					txtAccruedCurrent.Enabled = false;
					txtAccruedFiscal.Enabled = false;
					txtAccruedSickCalendar.Enabled = false;
					txtAccruedSickCurrent.Enabled = false;
					txtAccruedSickFiscal.Enabled = false;
					txtCode.Enabled = false;
					txtEmployee.Enabled = false;
					txtUsedBalance.Enabled = false;
					txtUsedCalendar.Enabled = false;
					txtUsedCurrent.Enabled = false;
					txtUsedFiscal.Enabled = false;
					txtUsedSickBalance.Enabled = false;
					txtUsedSickCurrent.Enabled = false;
					txtUsedSickFiscal.Enabled = false;
				}
				intDataChanged = FCConvert.ToInt16(false);
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will use to enter data about controls that are not in a grid
				FillControlInformationClass();
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
					else
					{
						intDataChanged = 0;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (boolNoSave && !boolCantEdit)
				{
					e.Cancel = true;
					return;
				}
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuClear_Click()
		{
			// vbPorter upgrade warning: ControlName As Control	OnWrite(string)
			Control ControlName = new Control();
			foreach (Control ControlName_foreach in this.GetAllControls())
			{
				ControlName = ControlName_foreach;
				if (ControlName is FCTextBox)
				{
					if (ControlName.GetName() != "txtEmployee" && ControlName.GetName() != "txtCode" && FCConvert.ToString(ControlName.Tag) != "KEEP")
					{
						(ControlName as FCTextBox).Text = string.Empty;
					}
				}
				ControlName = null;
			}
		}

		private void mnuCaption1_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(1));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = FCConvert.ToBoolean(0);
			LoadGrid();
			lblCurrentEmployee.Text = "Employee " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "  -  " + fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeFirstName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeMiddleName) + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeLastName + " " + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeDesig;
		}

		private void mnuCaption2_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(2));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption3_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(3));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption4_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(4));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption5_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(5));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption6_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(6));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption7_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(7));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption8_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(8));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCaption9_Click(object sender, System.EventArgs e)
		{
			intChildID = FCConvert.ToInt16(modMultiPay.MultiPayCaption(9));
			modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(intChildID);
			modGlobalVariables.Statics.gtypeCurrentEmployee.ChildRecord = true;
			LoadGrid();
			lblCurrentEmployee.Text = modMultiPay.Statics.gstrEmployeeCaption + "   " + fecherFoundation.Strings.Trim(Strings.Mid(mnuCaption2.Text, 3, 50)) + " (2 of " + FCConvert.ToString(modMultiPay.Statics.gintNumberChildren) + ")";
		}

		private void mnuCodes_Click(object sender, System.EventArgs e)
		{
			frmVacationCodes.InstancePtr.Show();
			boolFromVacSick = true;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				if (boolNoSave)
				{
				}
				else
				{
					SaveChanges();
				}
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cboType_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int x;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				bool boolItemSelected;
				vsCodes.Select(0, 0);
				here:
				;
				boolNoSave = false;
				// get all of the records from the database
				if (cboType.Text == "All Types")
				{
					if (this.cboType.Text == "All Types")
					{
						intDataChanged = 0;
					}
					else
					{
						MessageBox.Show("Invalid Code. Please select grid row.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						boolNoSave = true;
					}
					return;
				}
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Vacation / Sick", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber, fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				rsVacationSick.Execute("update tblVacationSick set selected = 0 where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
				rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
				if (rsVacationSick.EndOfFile())
				{
					rsVacationSick.AddNew();
					rsVacationSick.SetData("AccruedCurrent", Strings.Format(Conversion.Val(txtAccruedCurrent.Text), "0.00"));
					rsVacationSick.SetData("AccruedFiscal", Strings.Format(Conversion.Val(txtAccruedFiscal.Text), "0.00"));
					rsVacationSick.SetData("AccruedCalendar", Strings.Format(Conversion.Val(txtAccruedCalendar.Text), "0.00"));
					rsVacationSick.SetData("UsedCurrent", Strings.Format(Conversion.Val(txtUsedCurrent.Text), "0.00"));
					rsVacationSick.SetData("UsedFiscal", Strings.Format(Conversion.Val(txtUsedFiscal.Text), "0.00"));
					rsVacationSick.SetData("UsedCalendar", Strings.Format(Conversion.Val(txtUsedCalendar.Text), "0.00"));
					rsVacationSick.SetData("UsedBalance", Strings.Format(Conversion.Val(txtUsedBalance.Text), "0.00"));
					// Else
					// rsVacationSick.Edit
				}
				// 
				// If txtAccruedCurrent.Tag <> txtAccruedCurrent Or
				// txtAccruedFiscal.Tag <> txtAccruedFiscal Or
				// txtAccruedCalendar.Tag <> txtAccruedCalendar Or
				// txtUsedCurrent.Tag <> txtUsedCurrent Or
				// txtUsedFiscal.Tag <> txtUsedFiscal Or
				// txtUsedCalendar.Tag <> txtUsedCalendar Or
				// txtUsedBalance.Tag <> txtUsedBalance Then
				// 
				// Call SetVerifyAdjustNeeded(True)
				// End If
				// corey 01/09/2006  The verify and adjust only works with cytd so should only show the message if those are what changed
				// If txtAccruedCalendar.Tag <> txtAccruedCalendar Or txtUsedCalendar.Tag <> txtUsedCalendar Then
				// Call SetVerifyAdjustNeeded(True)
				// End If
				// 
				// rsVacationSick.SetData "AccruedCurrent", Format(Val(txtAccruedCurrent), "0.00")
				// rsVacationSick.SetData "AccruedFiscal", Format(Val(txtAccruedFiscal), "0.00")
				// rsVacationSick.SetData "AccruedCalendar", Format(Val(txtAccruedCalendar), "0.00")
				// 
				// rsVacationSick.SetData "UsedCurrent", Format(Val(txtUsedCurrent), "0.00")
				// rsVacationSick.SetData "UsedFiscal", Format(Val(txtUsedFiscal), "0.00")
				// rsVacationSick.SetData "UsedCalendar", Format(Val(txtUsedCalendar), "0.00")
				// rsVacationSick.SetData "UsedBalance", Format(Val(txtUsedBalance), "0.00")
				// vbPorter upgrade warning: strTemp As object	OnWrite(object, string)
				object strTemp = null;
				boolItemSelected = false;
				for (intCounter = 1; intCounter <= (vsCodes.Rows - 1); intCounter++)
				{
					if (Conversion.Val(vsCodes.TextMatrix(intCounter, Code)) == 0)
					{
						// And (Val(vsCodes.TextMatrix(intCounter, sel)) * -1 = vbChecked Or Val(vsCodes.TextMatrix(intCounter, sel)) = vbChecked) Then
						// strTemp = Split(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, typ), "added")
						rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID) + " and codeid = 0", "TWPY0000.vb1");
						if (rsVacationSick.EndOfFile())
						{
							rsVacationSick.AddNew();
						}
						else
						{
							rsVacationSick.Edit();
						}
						rsVacationSick.SetData("AccruedCurrent", Strings.Format(Conversion.Val(txtAccruedCurrent.Text), "0.00"));
						rsVacationSick.SetData("AccruedFiscal", Strings.Format(Conversion.Val(txtAccruedFiscal.Text), "0.00"));
						rsVacationSick.SetData("AccruedCalendar", Strings.Format(Conversion.Val(txtAccruedCalendar.Text), "0.00"));
						rsVacationSick.SetData("UsedCurrent", Strings.Format(Conversion.Val(txtUsedCurrent.Text), "0.00"));
						rsVacationSick.SetData("UsedFiscal", Strings.Format(Conversion.Val(txtUsedFiscal.Text), "0.00"));
						rsVacationSick.SetData("UsedCalendar", Strings.Format(Conversion.Val(txtUsedCalendar.Text), "0.00"));
						rsVacationSick.SetData("UsedBalance", Strings.Format(Conversion.Val(txtUsedBalance.Text), "0.00"));
						strTemp = vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, typ);
						if (FCConvert.ToString(strTemp) == "")
							strTemp = "Pay Period";
						rsVacationSick.SetData("CodeID", 0);
						rsVacationSick.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
						rsVacationSick.SetData("TypeID", intTypeID);
						rsVacationSick.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
						rsVacationSick.SetData("des", vsCodes.TextMatrix(intCounter, des));
						rsVacationSick.SetData("Rates", Conversion.Val(vsCodes.TextMatrix(intCounter, Rate)));
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, um))) == string.Empty)
						{
							rsVacationSick.Set_Fields("um", "Hours");
						}
						else
						{
							rsVacationSick.SetData("um", vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, um));
						}
						rsVacationSick.SetData("Type", fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp)));
						if ((CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel)) * -1) == Wisej.Web.CheckState.Checked || (CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel))) == Wisej.Web.CheckState.Checked)
						{
							rsVacationSick.Set_Fields("selected", true);
						}
						else
						{
							rsVacationSick.Set_Fields("selected", false);
						}
						if (FCConvert.ToBoolean(rsVacationSick.Get_Fields_Boolean("selected")))
						{
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp)) == "Year")
							{
								if (Strings.InStr(1, vsCodes.TextMatrix(intCounter, md), "/", CompareConstants.vbBinaryCompare) == 0)
								{
									MessageBox.Show("Added field must be in the format of MM/W", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)).Length < 3)
								{
									MessageBox.Show("Added field must be in the format of MM/W", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)).Length == 3)
								{
									rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
								}
								else if (FCConvert.ToDouble(Strings.Left(fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)), 2)) > 12)
								{
									MessageBox.Show("Yearly month must be less then 13.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (FCConvert.ToDouble(Strings.Right(fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)), 1)) > 4)
								{
									MessageBox.Show("Week must be less then 5.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else
								{
									rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
								}
							}
							else if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp)) == "Month")
							{
								if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)).Length < 0)
								{
									MessageBox.Show("Added field must be in the format of MM", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md))) > 12)
								{
									MessageBox.Show("Month must be less then 13.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else
								{
									rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
								}
							}
							else if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp)) == "Quarter")
							{
								if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)).Length < 0)
								{
									MessageBox.Show("Added field must be in the format of MM", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md))) > 4)
								{
									MessageBox.Show("Quarter must be less then 5.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else
								{
									rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
								}
							}
							else if (fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp)) == "Anniversary")
							{
								if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md)).Length < 0)
								{
									MessageBox.Show("Added field must be in the format of MM", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, md))) > 5)
								{
									MessageBox.Show("Week must be less then 6.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
								else
								{
									rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
								}
							}
							else
							{
								rsVacationSick.SetData("md", vsCodes.TextMatrix(intCounter, md));
							}
						}
						else
						{
							rsVacationSick.Set_Fields("md", vsCodes.TextMatrix(intCounter, md));
						}
						rsVacationSick.SetData("max", Conversion.Val(vsCodes.TextMatrix(intCounter, Max)));
						rsVacationSick.Update();
						// boolItemSelected = True
						// GoTo Success
					}
					else if (Conversion.Val(vsCodes.TextMatrix(intCounter, Code)) != 0 && ((CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel)) * -1) == Wisej.Web.CheckState.Checked || (CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel))) == Wisej.Web.CheckState.Checked))
					{
						rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID) + " and codeid <> 0", "TWPY0000.vb1");
						if (rsVacationSick.EndOfFile())
						{
							rsVacationSick.AddNew();
						}
						else
						{
							rsVacationSick.Edit();
						}
						rsVacationSick.SetData("AccruedCurrent", Strings.Format(Conversion.Val(txtAccruedCurrent.Text), "0.00"));
						rsVacationSick.SetData("AccruedFiscal", Strings.Format(Conversion.Val(txtAccruedFiscal.Text), "0.00"));
						rsVacationSick.SetData("AccruedCalendar", Strings.Format(Conversion.Val(txtAccruedCalendar.Text), "0.00"));
						rsVacationSick.SetData("UsedCurrent", Strings.Format(Conversion.Val(txtUsedCurrent.Text), "0.00"));
						rsVacationSick.SetData("UsedFiscal", Strings.Format(Conversion.Val(txtUsedFiscal.Text), "0.00"));
						rsVacationSick.SetData("UsedCalendar", Strings.Format(Conversion.Val(txtUsedCalendar.Text), "0.00"));
						rsVacationSick.SetData("UsedBalance", Strings.Format(Conversion.Val(txtUsedBalance.Text), "0.00"));
						if (this.cboType.SelectedIndex == 0)
						{
							// then they are on the all types screen
							clsDRWrapper rsData = new clsDRWrapper();
							rsData.OpenRecordset("Select tblVacationSick.*,tblCodes.* from tblVacationSick LEFT JOIN tblCodes ON tblCodes.ID = tblVacationSick.CodeID where tblVacationSick.EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
							if (!rsData.EndOfFile())
							{
								rsVacationSick.SetData("CodeID", rsData.Get_Fields_Int32("CodeID"));
							}
							rsVacationSick.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
							rsVacationSick.SetData("TypeID", intTypeID);
							rsVacationSick.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
							// rsVacationSick.Update
							// GoTo Success
						}
						else
						{
							rsVacationSick.SetData("CodeID", vsCodes.TextMatrix(intCounter, ID));
							rsVacationSick.SetData("EmployeeNumber", modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber);
							rsVacationSick.SetData("TypeID", intTypeID);
							rsVacationSick.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
							// rsVacationSick.Update
							// GoTo Success
						}
						if ((CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel)) * -1) == Wisej.Web.CheckState.Checked || (CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel))) == Wisej.Web.CheckState.Checked)
						{
							rsVacationSick.Set_Fields("selected", true);
							boolItemSelected = true;
						}
						else
						{
							rsVacationSick.Set_Fields("selected", false);
						}
						rsVacationSick.Update();
					}
				}
				rsVacationSick.Execute("update tblvacationsick set usedbalance = " + FCConvert.ToString(Conversion.Val(txtUsedBalance.Text)) + ", usedcalendar = " + FCConvert.ToString(Conversion.Val(txtUsedCalendar.Text)) + ", usedfiscal = " + FCConvert.ToString(Conversion.Val(txtUsedFiscal.Text)) + ",usedcurrent = " + FCConvert.ToString(Conversion.Val(txtUsedCurrent.Text)) + " ,accruedfiscal = " + FCConvert.ToString(Conversion.Val(txtAccruedFiscal.Text)) + ", accruedcurrent = " + FCConvert.ToString(Conversion.Val(txtAccruedCurrent.Text)) + " where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
				// If Not boolItemSelected Then
				// If MsgBox("No items have been selected for this person and type. Continue with update?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
				// Call rsVacationSick.Execute("Delete from tblVacationSick where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "' and TypeID = " & intTypeID, "TWPY0000.vb1")
				// Else
				// Exit Sub
				// End If
				// End If
				Success:
				;
				// all changes have been updated
				intDataChanged = 0;
				clsHistoryClass.Compare();
				intTypeID = cboType.ItemData(cboType.SelectedIndex);
				LoadGrid();
				boolSelect = false;
				MessageBox.Show("Save was successful", "Payroll Employee Vacation/Sick", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(cmdSave, new System.EventArgs());
		}

		private void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intZeroCodeId), "TWPY0000.vb1");
				if (!rsVacationSick.EndOfFile())
				{
					txtAccruedCurrent.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("AccruedCurrent") + " ")), "0.00");
					txtAccruedFiscal.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("AccruedFiscal") + " ")), "0.00");
					txtAccruedCalendar.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("AccruedCalendar") + " ")), "0.00");
					// 
					// txtAccruedSickCurrent = Val(Trim(rsVacationSick.Fields("AccruedSickCurrent") & " "))
					// txtAccruedSickFiscal = Val(Trim(rsVacationSick.Fields("AccruedSickFiscal") & " "))
					// txtAccruedSickCalendar = Val(Trim(rsVacationSick.Fields("AccruedSickCalendar") & " "))
					// 
					txtUsedCurrent.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("UsedCurrent") + " ")), "0.00");
					txtUsedFiscal.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("UsedFiscal") + " ")), "0.00");
					txtUsedCalendar.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("UsedCalendar") + " ")), "0.00");
					txtUsedBalance.Text = Strings.Format(Conversion.Val(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("UsedBalance") + " ")), "0.00");
					// 
					// txtUsedSickCurrent = Val(Trim(rsVacationSick.Fields("UsedSickCurrent") & " "))
					// txtUsedSickFiscal = Val(Trim(rsVacationSick.Fields("UsedSickFiscal") & " "))
					// txtUsedSickCalendar = Val(Trim(rsVacationSick.Fields("UsedSickCalendar") & " "))
					// txtUsedSickBalance = Val(Trim(rsVacationSick.Fields("UsedSickBalance") & " "))
				}
				else
				{
					mnuClear_Click();
				}
				GetBalances();
				// txtCode = txtCode.Tag & intCodeCaption
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtAccruedCalendar_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedCurrent_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedFiscal_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedSickCalendar_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedSickCalendar_Enter(object sender, System.EventArgs e)
		{
			txtAccruedSickCalendar.SelectionStart = 0;
			txtAccruedSickCalendar.SelectionLength = 20;
		}

		private void txtAccruedSickCalendar_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtAccruedSickCurrent_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedSickCurrent_Enter(object sender, System.EventArgs e)
		{
			txtAccruedSickCurrent.SelectionStart = 0;
			txtAccruedSickCurrent.SelectionLength = 20;
		}

		private void txtAccruedSickCurrent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtAccruedSickFiscal_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtAccruedSickFiscal_Enter(object sender, System.EventArgs e)
		{
			txtAccruedSickFiscal.SelectionStart = 0;
			txtAccruedSickFiscal.SelectionLength = 20;
		}

		private void txtAccruedSickFiscal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtAccruedCalendar_Enter(object sender, System.EventArgs e)
		{
			txtAccruedCalendar.SelectionStart = 0;
			txtAccruedCalendar.SelectionLength = 20;
		}

		private void txtAccruedCalendar_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtAccruedCurrent_Enter(object sender, System.EventArgs e)
		{
			txtAccruedCurrent.SelectionStart = 0;
			txtAccruedCurrent.SelectionLength = 20;
		}

		private void txtAccruedCurrent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtAccruedFiscal_Enter(object sender, System.EventArgs e)
		{
			txtAccruedFiscal.SelectionStart = 0;
			txtAccruedFiscal.SelectionLength = 20;
		}

		private void txtAccruedFiscal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtUsedBalance_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedCalendar_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedCurrent_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedFiscal_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedSickBalance_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedSickCalendar_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedSickCalendar_Enter(object sender, System.EventArgs e)
		{
			txtUsedSickCalendar.SelectionStart = 0;
			txtUsedSickCalendar.SelectionLength = 20;
		}

		private void txtUsedSickCurrent_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedSickCurrent_Enter(object sender, System.EventArgs e)
		{
			txtUsedSickCurrent.SelectionStart = 0;
			txtUsedSickCurrent.SelectionLength = 20;
		}

		private void txtUsedSickFiscal_TextChanged(object sender, System.EventArgs e)
		{
			if (boolChange)
				intDataChanged += 1;
		}

		private void txtUsedSickFiscal_Enter(object sender, System.EventArgs e)
		{
			txtUsedSickFiscal.SelectionStart = 0;
			txtUsedSickFiscal.SelectionLength = 20;
		}

		private void GetBalances()
		{
			// THIS WILL FORCE THE DISPLAY TO HAVE TWO DECIMAL PLACES.
			txtUsedBalance.Text = Strings.Format(Conversion.Val(txtUsedBalance.Text), "0.00");
			txtUsedFiscal.Text = Strings.Format(Conversion.Val(txtUsedFiscal.Text), "0.00");
			txtUsedCurrent.Text = Strings.Format(Conversion.Val(txtUsedCurrent.Text), "0.00");
			txtUsedCalendar.Text = Strings.Format(Conversion.Val(txtUsedCalendar.Text), "0.00");
			txtAccruedFiscal.Text = Strings.Format(Conversion.Val(txtAccruedFiscal.Text), "0.00");
			txtAccruedCurrent.Text = Strings.Format(Conversion.Val(txtAccruedCurrent.Text), "0.00");
			txtAccruedCalendar.Text = Strings.Format(Conversion.Val(txtAccruedCalendar.Text), "0.00");
			// If intDataChanged > 0 Then Call SaveChanges
			// intDataChanged = 0
			// WHY ARE WE NOT RECALCULATING THE BALANCE
		}

		private void txtUsedBalance_Enter(object sender, System.EventArgs e)
		{
			txtUsedBalance.SelectionStart = 0;
			txtUsedBalance.SelectionLength = 20;
		}

		private void txtUsedCalendar_Enter(object sender, System.EventArgs e)
		{
			txtUsedCalendar.SelectionStart = 0;
			txtUsedCalendar.SelectionLength = 20;
		}

		private void txtUsedCalendar_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtUsedCurrent_Enter(object sender, System.EventArgs e)
		{
			txtUsedCurrent.SelectionStart = 0;
			txtUsedCurrent.SelectionLength = 20;
		}

		private void txtUsedCurrent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}

		private void txtUsedFiscal_Enter(object sender, System.EventArgs e)
		{
			txtUsedFiscal.SelectionStart = 0;
			txtUsedFiscal.SelectionLength = 20;
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
				vsCodes.ExtendLastCol = true;
				//vsCodes.WordWrap = true;
				vsCodes.Cols = 11;
				vsCodes.FixedRows = 1;
				vsCodes.ColHidden(0, true);
				vsCodes.ColHidden(TypeCode, true);
				vsCodes.ColHidden(ID, true);
                modGlobalRoutines.CenterGridCaptions(ref vsCodes);
				// set column 0 properties
				vsCodes.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(0) = 400
				// set column typecode properties
				vsCodes.ColAlignment(TypeCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(TypeCode) = 750
				vsCodes.TextMatrix(0, TypeCode, "Type");
				// set column code properties
				vsCodes.ColAlignment(Code, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(Code) = 700
				vsCodes.TextMatrix(0, Code, "Code");
				// set column des properties
				vsCodes.ColAlignment(des, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(des) = 2300
				vsCodes.TextMatrix(0, des, "Description");
				// set column rate properties
				vsCodes.ColAlignment(Rate, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsCodes.ColWidth(Rate) = 900
				vsCodes.TextMatrix(0, Rate, "Rate");
				// set column um properties
				vsCodes.ColAlignment(um, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(um) = 700
				vsCodes.TextMatrix(0, um, "U/M");
				vsCodes.ColComboList(um, "#1;Days|#2;Hours|#3;Weeks");
				// set column typ properties
				vsCodes.ColAlignment(typ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(typ) = 900
				vsCodes.TextMatrix(0, typ, "Per");
				vsCodes.ColComboList(typ, "#1;Pay Period" + "\t" + "Added Each Pay Period|#2;Month" + "\t" + "Added Monthly|#4;Quarter" + "\t" + "Added Quarterly|#5;Year" + "\t" + "Added Yearly|#6;Anniversary" + "\t" + "Added Anniversary Month|#8;Anniversary Date|#7;This Pay Period Only");
                var comboString = "#" + EarnedTimeAccrual.Hour.ToInteger() + ";"
                                  + EarnedTimeAccrual.Hour.ToName() + "\t"
                                  + EarnedTimeAccrual.Hour.ToDescription()
                                  + "|#" + EarnedTimeAccrual.PayPeriod.ToInteger() + ";" +
                                  EarnedTimeAccrual.PayPeriod.ToName() + "\t" +
                                  EarnedTimeAccrual.PayPeriod.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Month.ToInteger() + ";" +
                                  EarnedTimeAccrual.Month.ToName() + "\t" +
                                  EarnedTimeAccrual.Month.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Quarter.ToInteger() + ";" +
                                  EarnedTimeAccrual.Quarter.ToName() + "\t" +
                                  EarnedTimeAccrual.Quarter.ToDescription() +
                                  "|#" + EarnedTimeAccrual.Year.ToInteger() + ";" +
                                  EarnedTimeAccrual.Year.ToName() + "\t" +
                                  EarnedTimeAccrual.Year.ToDescription() +
                                  "|#" + EarnedTimeAccrual.AnniversaryMonth.ToInteger() + ";" +
                                  EarnedTimeAccrual.AnniversaryMonth.ToName() + "\t" +
                                  EarnedTimeAccrual.AnniversaryMonth.ToDescription() +
                                  "|#" + EarnedTimeAccrual.AnniversaryDate.ToInteger() + ";" +
                                  EarnedTimeAccrual.AnniversaryDate.ToName() + "\t" +
                                  EarnedTimeAccrual.AnniversaryDate.ToDescription() +
                                  "|#" + EarnedTimeAccrual.SinglePayPeriod.ToInteger() + ";" +
                                  EarnedTimeAccrual.SinglePayPeriod.ToName() + "\t" +
                                  EarnedTimeAccrual.SinglePayPeriod.ToDescription();

                //vsCodes.ColComboList(typ, comboString);
				// set column id properties
				vsCodes.ColAlignment(md, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// vsCodes.ColWidth(md) = 1000
				// vsCodes.TextMatrix(0, md) = "M/D/W"
				// Bug Call ID 4877
				vsCodes.TextMatrix(0, md, "Added");
				// set column max properties
				vsCodes.ColAlignment(Max, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsCodes.ColWidth(Max) = 1000
				vsCodes.TextMatrix(0, Max, "Max");
				// set column sel properties
				vsCodes.ColAlignment(sel, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vsCodes.ColWidth(sel) = 1000
				vsCodes.TextMatrix(0, sel, "Selected");
				vsCodes.ColDataType(sel, FCGrid.DataTypeSettings.flexDTBoolean);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadCombo()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadCombo";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				cboType.Clear();
				rsVacationSick.OpenRecordset("Select * from tblCodeTypes", "TWPY0000.vb1");
				if (!rsVacationSick.EndOfFile())
				{
					rsVacationSick.MoveLast();
					rsVacationSick.MoveFirst();
					// hard code the all types options
					cboType.AddItem("All Types");
					cboType.ItemData(cboType.NewIndex, 0);
					for (intCounter = 1; intCounter <= (rsVacationSick.RecordCount()); intCounter++)
					{
						cboType.AddItem(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVacationSick.Get_Fields("Description"))));
						cboType.ItemData(cboType.NewIndex, FCConvert.ToInt32(rsVacationSick.Get_Fields("ID")));
						rsVacationSick.MoveNext();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadDefaults()
		{
			UserCode0.Descriptions = fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Description") + " ");
			UserCode0.Rates = FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Rate") + " "));
			UserCode0.ums = fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("DHW") + " ");
			UserCode0.dates = fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("MonthDay") + " ");
			UserCode0.maxs = FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Max") + " "));
			UserCode0.typs = fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Type") + " ");
			UserCode0.codes = FCConvert.ToInt32(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Code") + " "));
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				intZeroCodeId = 0;
				vsCodes.Rows = 1;
				if (cboType.SelectedIndex < 0)
					return;
				string strCodeName = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper rsCodeTypes = new clsDRWrapper();
				if (cboType.SelectedIndex > 0)
				{
					// get all of the records from the database
					rsVacationSick.OpenRecordset("Select * from tblCodes where CodeType = " + FCConvert.ToString(intTypeID) + " Order by Code", "TWPY0000.vb1");
					if (!rsVacationSick.EndOfFile())
					{
						// this will make the recordcount property work correctly
						rsVacationSick.MoveLast();
						rsVacationSick.MoveFirst();
						// set the number of rows in the grid
						vsCodes.Rows = rsVacationSick.RecordCount() + 2;
						// set up a record for MANUAL data entry
						vsCodes.TextMatrix(1, 0, FCConvert.ToString(intCounter));
						vsCodes.TextMatrix(1, Code, "000");
						vsCodes.TextMatrix(1, des, "MANUAL");
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// fill the grid
						for (intCounter = 2; intCounter <= (rsVacationSick.RecordCount() + 1); intCounter++)
						{
							vsCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
							vsCodes.TextMatrix(intCounter, Code, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Code") + " "));
							// intID = Trim(rsVacationSick.Fields("ID") & " ")
							if (rsVacationSick.Get_Fields_String("Code").Trim().ToIntegerValue() == 0)
							{
								LoadDefaults();
								intZeroRowId = intCounter;
								intZeroCodeId = FCConvert.ToInt32(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("ID") + " "));
							}
							vsCodes.TextMatrix(intCounter, des, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Description") + " "));
							vsCodes.TextMatrix(intCounter, Rate, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Rate") + " "), "0.0000"));
							vsCodes.TextMatrix(intCounter, um, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("DHW") ));
							vsCodes.TextMatrix(intCounter, typ, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Type") + " "));
							vsCodes.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("ID") + " "));
							// vsCodes.TextMatrix(intCounter, id) = Trim(rsVacationSick.Fields("CodeType") & " ")
							vsCodes.TextMatrix(intCounter, md, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("MonthDay") + " "));
							vsCodes.TextMatrix(intCounter, Max, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Max") + " "), "0.00"));
							vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, intCounter, 9, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							rsVacationSick.MoveNext();
						}
						rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
						// & " and TypeID = " & intID)
						while (!rsVacationSick.EndOfFile())
						{
							for (intCounter = 1; intCounter <= (vsCodes.Rows - 1); intCounter++)
							{
								if (Conversion.Val(rsVacationSick.Get_Fields_Int32("CodeID")) == 0)
								{
									vsCodes.TextMatrix(intCounter, Rate, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("Rates") + " "), "0.0000"));
									vsCodes.TextMatrix(intCounter, um, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("UM") ));
									vsCodes.TextMatrix(intCounter, typ, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Type") + " "));
									vsCodes.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Int32("CodeID") + " "));
									vsCodes.TextMatrix(intCounter, md, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("MD") + " "));
									vsCodes.TextMatrix(intCounter, Max, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Max") + " "), "0.00"));
									if (FCConvert.ToBoolean(rsVacationSick.Get_Fields_Boolean("selected")))
									{
										vsCodes.TextMatrix(intCounter, sel, FCConvert.ToString(1));
										boolSelect = true;
									}
									break;
								}
								else
								{
									if (vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, ID) == FCConvert.ToString(rsVacationSick.Get_Fields_Int32("CodeID")))
									{
										if (FCConvert.ToBoolean(rsVacationSick.Get_Fields_Boolean("selected")))
										{
											vsCodes.TextMatrix(intCounter, sel, FCConvert.ToString(1));
											boolSelect = true;
										}
									}
								}
							}
							rsVacationSick.MoveNext();
						}
					}
					else
					{
						// set the number of rows in the grid
						vsCodes.Rows = 2;
						// set up a record for MANUAL data entry
						vsCodes.TextMatrix(1, 0, FCConvert.ToString(intCounter));
						vsCodes.TextMatrix(1, Code, "000");
						vsCodes.TextMatrix(1, des, "MANUAL");
						rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' and TypeID = " + FCConvert.ToString(intTypeID), "TWPY0000.vb1");
						if (!rsVacationSick.EndOfFile())
						{
							vsCodes.TextMatrix(1, Rate, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Double("Rates") + " "), "0.0000"));
							vsCodes.TextMatrix(1, um, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("UM") + " "));
							vsCodes.TextMatrix(1, typ, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Type") + " "));
							vsCodes.TextMatrix(1, ID, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Int32("CodeID") + " "));
							vsCodes.TextMatrix(1, md, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("MD") + " "));
							vsCodes.TextMatrix(1, Max, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Max") + " "), "0.00"));
							if (FCConvert.ToBoolean(rsVacationSick.Get_Fields_Boolean("Selected")))
							{
								vsCodes.TextMatrix(1, sel, FCConvert.ToString(1));
							}
						}
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					vsCodes.ColHidden(TypeCode, true);
					vsCodes.ColHidden(sel, false);
					if (!boolCantEdit)
					{
						vsCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						vsCodes.Editable = FCGrid.EditableSettings.flexEDNone;
					}
					//vsCodes.Editable = !boolCantEdit;
					for (intCounter = 1; intCounter <= (vsCodes.Rows - 1); intCounter++)
					{
						if (vsCodes.TextMatrix(intCounter, sel) != string.Empty)
						{
							// intCheckCount = intCheckCount + 1
							if (Conversion.Val(vsCodes.TextMatrix(intCounter, Code)) == 0)
							{
								vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, Rate, intCounter, sel, Color.White);
								vsCodes.TextMatrix(intCounter, des, "MANUAL");
							}
							// If Val(vsCodes.TextMatrix(intCounter, Code)) = 0 And vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, typ) <> "Pay Period" Then
							if (FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, typ)) == "Pay Period")
							{
								vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, md, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
							}
							else
							{
								// vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, md) = vbWhite
							}
						}
					}
				}
				else
				{
					// THIS SHOULD BE THE TYPE OF ALL ENTRIES
					vsCodes.Editable = FCGrid.EditableSettings.flexEDNone;
					rsVacationSick.OpenRecordset("Select tblVacationSick.ID as VacSickID, tblVacationSick.*,tblCodes.* from tblVacationSick LEFT JOIN tblCodes ON tblCodes.ID = tblVacationSick.CodeID where  tblVacationSick.EmployeeNumber = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber + "' order by tblvacationsick.typeid,selected desc", "TWPY0000.vb1");
					if (!rsVacationSick.EndOfFile())
					{
						// this will make the recordcount property work correctly
						rsVacationSick.MoveLast();
						rsVacationSick.MoveFirst();
						// set the number of rows in the grid
						// vsCodes.Rows = rsVacationSick.RecordCount + 1
						vsCodes.Rows = 1;
						// fill the grid
						int lngLastCode = 0;
						lngLastCode = -1;
						intCounter = 1;
						// For intCounter = 1 To rsVacationSick.RecordCount
						vsCodes.ColHidden(TypeCode, false);
						vsCodes.ColHidden(sel, true);
						rsCodeTypes.OpenRecordset("select * from tblcodetypes", "twpy0000.vb1");
						while (!rsVacationSick.EndOfFile())
						{
							if (lngLastCode != Conversion.Val(rsVacationSick.Get_Fields("typeid")))
							{
								vsCodes.Rows += 1;
								lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsVacationSick.Get_Fields("typeid"))));
								if (FCConvert.ToInt32(rsVacationSick.Get_Fields_Int32("CodeID")) == 0)
								{
									rsTemp.OpenRecordset("Select * from tblVacationSick where ID = " + FCConvert.ToString(Conversion.Val(rsVacationSick.Get_Fields("VacSickID"))), "TWPY0000.vb1");
									if (!rsTemp.EndOfFile())
									{
										vsCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
										vsCodes.TextMatrix(intCounter, Code, "000");
										vsCodes.TextMatrix(intCounter, des, "MANUAL");
										vsCodes.TextMatrix(intCounter, Rate, Strings.Format(fecherFoundation.Strings.Trim(rsTemp.Get_Fields_Double("Rates") + " "), "0.0000"));
										vsCodes.TextMatrix(intCounter, um, fecherFoundation.Strings.Trim(rsTemp.Get_Fields_String("UM") + " "));
										vsCodes.TextMatrix(intCounter, typ, fecherFoundation.Strings.Trim(rsTemp.Get_Fields("Type") + " "));
										vsCodes.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsTemp.Get_Fields_Int32("CodeID") + " "));
										vsCodes.TextMatrix(intCounter, md, fecherFoundation.Strings.Trim(rsTemp.Get_Fields_String("MD") + " "));
										vsCodes.TextMatrix(intCounter, Max, Strings.Format(fecherFoundation.Strings.Trim(rsTemp.Get_Fields("Max") + " "), "0.00"));
										vsCodes.TextMatrix(intCounter, sel, FCConvert.ToString(1));
										if (rsCodeTypes.FindFirstRecord("ID", lngLastCode))
										{
											vsCodes.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsCodeTypes.Get_Fields_String("description"))));
										}
									}
								}
								else
								{
									vsCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
									vsCodes.TextMatrix(intCounter, Code, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Code") + " "));
									vsCodes.TextMatrix(intCounter, des, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Description") + " "));
									vsCodes.TextMatrix(intCounter, Rate, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("Rate") + " "), "0.0000"));
									vsCodes.TextMatrix(intCounter, um, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("DHW") + " "));
									vsCodes.TextMatrix(intCounter, typ, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("tblCodes.Type") + " "));
									vsCodes.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_Int32("CodeID") + " "));
									vsCodes.TextMatrix(intCounter, md, fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields_String("MonthDay") + " "));
									vsCodes.TextMatrix(intCounter, Max, Strings.Format(fecherFoundation.Strings.Trim(rsVacationSick.Get_Fields("tblCodes.Max") + " "), "0.00"));
									vsCodes.TextMatrix(intCounter, sel, FCConvert.ToString(1));
									if (rsCodeTypes.FindFirstRecord("ID", lngLastCode))
									{
										vsCodes.TextMatrix(intCounter, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsCodeTypes.Get_Fields_String("description"))));
									}
								}
								vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, intCounter, 10, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								intCounter += 1;
							}
							rsVacationSick.MoveNext();
							// Next
						}
						// if all selected is true then open field so user knows what code goes with
						// Call rsVacationSick.OpenRecordset("SELECT tblCodeTypes.Description,tblCodeTypes.ID, tblVacationSick.TypeID FROM tblVacationSick INNER JOIN tblCodeTypes ON tblVacationSick.TypeID = tblCodeTypes.ID  where EmployeeNumber = '" & gtypeCurrentEmployee.EMPLOYEENUMBER & "'", "TWPY0000.vb1")
						// If Not rsVacationSick.EndOfFile Then
						// vsCodes.ColHidden(TypeCode) = False
						// vsCodes.ColHidden(sel) = True
						// For intCounter = 1 To vsCodes.Rows - 1
						// If Not rsVacationSick.EndOfFile Then
						// vsCodes.TextMatrix(intCounter, 1) = Trim(rsVacationSick.Fields("Description") & " ")
						// vsCodes.TextMatrix(intCounter, ID) = Trim(rsVacationSick.Fields("ID") & " ")
						// rsVacationSick.MoveNext
						// Else
						// Call vsCodes.RemoveItem(vsCodes.Rows - 1)
						// End If
						// Next
						// End If
					}
				}
				// If intZeroCodeId <> 0 Then
				// Call rsVacationSick.OpenRecordset("Select * from tblVacationSick where EmployeeNumber = '" & gtypeCurrentEmployee.EmployeeNumber & "'", "TWPY0000.vb1") ' & " and TypeID = " & intTypeID)
				// If Not rsVacationSick.EndOfFile Then
				// vsCodes.TextMatrix(intZeroRowId, des) = rsVacationSick.Fields("des")
				// vsCodes.TextMatrix(intZeroRowId, Rate) = Format(rsVacationSick.Fields("Rates"), "0.0000")
				// vsCodes.TextMatrix(intZeroRowId, um) = rsVacationSick.Fields("um")
				// vsCodes.TextMatrix(intZeroRowId, typ) = rsVacationSick.Fields("Type")
				// vsCodes.TextMatrix(intZeroRowId, md) = rsVacationSick.Fields("md")
				// vsCodes.TextMatrix(intZeroRowId, Max) = Format(rsVacationSick.Fields("Max"), "0.00")
				// End If
				// End If
				// Call vsCodes_AfterEdit(vsCodes.Row, vsCodes.Col)
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				vsCodes.Select(0, 0);
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtUsedFiscal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			GetBalances();
		}
		// vbPorter upgrade warning: Row As int	OnRead
		private void vsCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			intCheckCount = 0;
			bool blnSelected;
			blnSelected = false;
			for (intCounter = 1; intCounter <= (vsCodes.Rows - 1); intCounter++)
			{
				if ((CheckState)(FCConvert.ToInt32(vsCodes.TextMatrix(intCounter, sel)) * -1) == Wisej.Web.CheckState.Checked)
				{
					blnSelected = true;
					intCheckCount += 1;
					if (Conversion.Val(vsCodes.TextMatrix(intCounter, Code)) == 0 && vsCodes.Col == 10)
					{
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, Rate, intCounter, sel, Color.White);
						vsCodes.TextMatrix(intCounter, des, "MANUAL");
						if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, Rate)) == "")
						{
							vsCodes.TextMatrix(intCounter, Rate, "0");
						}
						if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, um)) == "")
						{
							vsCodes.TextMatrix(intCounter, um, "Hours");
						}
						if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, typ)) == "")
						{
							vsCodes.TextMatrix(intCounter, typ, "Pay Period");
						}
						if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(intCounter, Max)) == "")
						{
							vsCodes.TextMatrix(intCounter, Max, "9999.99");
						}
					}
					if (Conversion.Val(vsCodes.TextMatrix(intCounter, Code)) == 0 && FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, typ)) == "Year")
					{
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, md, Color.White);
					}
					else
					{
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, md, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
				if (intCounter != vsCodes.Row && cboType.SelectedIndex != 0 && vsCodes.Col == sel)
				{
					vsCodes.TextMatrix(intCounter, sel, FCConvert.ToString(FCConvert.ToInt32(Wisej.Web.CheckState.Unchecked)));
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, des, intCounter, Max, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					intCheckCount -= 1;
				}
				if (intCounter == vsCodes.Row)
				{
					intRowSave = vsCodes.Row;
				}
			}
			// For intCounter = 1 To vsCodes.Rows - 1
			// If Val(vsCodes.TextMatrix(intCounter, Code)) = 0 And Val(vsCodes.TextMatrix(intCounter, sel)) * -1 = vbUnchecked Then
			// If vsCodes.TextMatrix(intCounter, Code) = "000" Then
			// vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, Rate, intCounter, sel) = vbWhite
			// vsCodes.TextMatrix(intCounter, des) = "MANUAL"
			// vsCodes.TextMatrix(intCounter, Rate) = "0"
			// vsCodes.TextMatrix(intCounter, um) = "Hours"
			// vsCodes.TextMatrix(intCounter, typ) = "Pay Period"
			// vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 8) = TRIOCOLORGRAYBACKGROUND
			// vsCodes.TextMatrix(intCounter, Max) = "9999.99"
			// Else
			// 
			// vsCodes.TextMatrix(intCounter, des) = UserCode0.Descriptions
			// vsCodes.TextMatrix(intCounter, Rate) = UserCode0.Rates
			// vsCodes.TextMatrix(intCounter, um) = UserCode0.ums
			// vsCodes.TextMatrix(intCounter, typ) = UserCode0.typs
			// vsCodes.TextMatrix(intCounter, md) = UserCode0.dates
			// vsCodes.TextMatrix(intCounter, Max) = UserCode0.maxs
			// End If
			// End If
			// Next
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "2")
			{
				vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, md, Color.White);
				vsCodes.TextMatrix(vsCodes.Row, 8, FCConvert.ToString(1));
				// Format(Date, "DD")
				vsCodes.Select(vsCodes.Row, 8);
			}
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "4")
			{
				vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, md, Color.White);
				vsCodes.TextMatrix(vsCodes.Row, 8, FCConvert.ToString(1));
				vsCodes.Select(vsCodes.Row, 8);
			}
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "5")
			{
				vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, md, Color.White);
				vsCodes.TextMatrix(vsCodes.Row, 8, FCConvert.ToString(DateTime.Today.Month) + "/1");
				// Format(Date, "MM/W")
				// vsCodes.ColEditMask(8) = "##/#"
				vsCodes.Select(vsCodes.Row, 8);
			}
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "6")
			{
				vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, md, Color.White);
				vsCodes.TextMatrix(vsCodes.Row, 8, FCConvert.ToString(1));
				vsCodes.Select(vsCodes.Row, 8);
			}
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "7")
			{
				vsCodes.TextMatrix(vsCodes.Row, 8, "");
				vsCodes.Select(vsCodes.Row, 8);
			}
			if (vsCodes.Col == typ && vsCodes.TextMatrix(vsCodes.Row, vsCodes.Col) == "8")
			{
				vsCodes.TextMatrix(vsCodes.Row, 8, "");
				vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, md, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			if (intCheckCount > 1)
			{
				MessageBox.Show("Cannot Have More Than One Item Checked", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			intCheckCount = 0;
			if (!boolFromType)
			{
				intDataChanged += 1;
			}
			boolFromType = false;
			if (!blnSelected)
			{
				txtAccruedCurrent.Enabled = false;
				txtAccruedFiscal.Enabled = false;
				txtAccruedCalendar.Enabled = false;
				txtUsedCurrent.Enabled = false;
				txtUsedFiscal.Enabled = false;
				txtUsedCalendar.Enabled = false;
				txtUsedBalance.Enabled = false;
			}
			else
			{
				txtAccruedCurrent.Enabled = true;
				txtAccruedFiscal.Enabled = true;
				txtAccruedCalendar.Enabled = true;
				txtUsedCurrent.Enabled = true;
				txtUsedFiscal.Enabled = true;
				txtUsedCalendar.Enabled = true;
				txtUsedBalance.Enabled = true;
			}
		}

		private void vsCodes_ChangeEdit(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			object strTemp;
			boolFromType = false;
			strTemp = Strings.Split(vsCodes.EditText, "added", -1, CompareConstants.vbBinaryCompare);
			if (vsCodes.Col == 6)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[0])) == "Year")
				{
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, 8, Color.White);
					strPER = "Year";
				}
				else if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[0])) == "Month")
				{
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, 8, Color.White);
					strPER = "Month";
				}
				else if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[0])) == "Quarter")
				{
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, 8, Color.White);
					strPER = "Quarter";
				}
				else if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[0])) == "Anniversary")
				{
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, 8, Color.White);
					strPER = "Anniversary";
				}
				else if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[0])) == "This Pay Period Only")
				{
					if (FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCodes.Row, vsCodes.Col - 1)) == "Hours")
					{
					}
					else
					{
						MessageBox.Show("This Pay Period Only can only be chosen for a U/M of Hours.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.Row, vsCodes.Col, "Pay Period");
					}
				}
				else
				{
					strPER = "";
					if (vsCodes.Row > 0)
					{
						vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsCodes.TextMatrix(vsCodes.Row, 8, " ");
					}
					// If vsCodes.Col = 7 Then vsCodes.Col = 8
				}
			}
			else if (vsCodes.Col == 5)
			{
				if (FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsCodes.Row, vsCodes.Col + 1)) == "This Pay Period Only")
				{
					vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.Row, vsCodes.Col + 1, "Pay Period added Each Pay Period");
				}
			}
		}

		private void vsCodes_ClickEvent(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE:#4112 if grid is empty return
            if (vsCodes.Rows <= 1)
                return;
			if (cboType.SelectedIndex < 0)
				return;
			// if the type combo is 'All Selected Codes' then the ID field
			// in the grid is filled with the TYPE ID and not the CODE ID
			if (cboType.SelectedIndex == 0)
			{
				intTypeID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.MouseRow, ID))));
			}
			else
			{
				intCodeID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.MouseRow, ID))));
			}
			//intCodeCaption = FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.MouseRow, 1));
			intCodeCaption = FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpText, vsCodes.Row, 1));
			if (intCodeCaption == string.Empty)
			{
				intCodeCaption = cboType.Text;
			}
			txtCode.Text = "Type: " + intCodeCaption;
			if (cboType.SelectedIndex == 0)
			{
				LoadBalances();
			}
			if (vsCodes.MouseRow == 0)
			{
				if (vsCodes.Rows > 1)
				{
					vsCodes.Select(1, 0, vsCodes.Rows - 1, vsCodes.Cols - 1);
				}
				vsCodes.ColSel = vsCodes.MouseCol;
				vsCodes.Col = vsCodes.MouseCol;
				if (boolGridSort)
				{
					vsCodes.Sort = (FCGrid.SortSettings)1;
				}
				else
				{
					vsCodes.Sort = (FCGrid.SortSettings)2;
				}
				boolGridSort = !boolGridSort;
				vsCodes.Select(0, 0);
			}
		}

		private void vsCodes_Enter(object sender, System.EventArgs e)
		{
			if ((FCConvert.ToInt32(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, vsCodes.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) && !boolCantEdit)
			{
				vsCodes.EditCell();
			}
		}

		private void vsCodes_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (FCConvert.ToInt32(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, vsCodes.Col)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				KeyCode = 0;
				vsCodes.Select(vsCodes.Row, 1);
			}
		}

		private void vsCodes_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsCodes[e.ColumnIndex, e.RowIndex];
            int intMouseRow;
			int intMouseCol;
			intMouseRow = vsCodes.GetFlexRowIndex(e.RowIndex);
			intMouseCol = vsCodes.GetFlexColIndex(e.ColumnIndex);
			if (intMouseCol == md)
			{
				string vbPorterVar = FCConvert.ToString(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intMouseRow, typ));
				if (vbPorterVar == "Year")
				{
					//ToolTip1.SetToolTip(vsCodes, "Enter Month/Week of Month");
					cell.ToolTipText = "Enter Month/Week of Month";
				}
				else if (vbPorterVar == "Month")
				{
					//ToolTip1.SetToolTip(vsCodes, "Enter Week of Month");
					cell.ToolTipText = "Enter Week of Month";
				}
				else if (vbPorterVar == "Quarter")
				{
					//ToolTip1.SetToolTip(vsCodes, "Enter Month of Quarter (1-3) / Week of Month");
					cell.ToolTipText = "Enter Month of Quarter (1-3) / Week of Month";
				}
				else if (vbPorterVar == "Anniversary")
				{
					//ToolTip1.SetToolTip(vsCodes, "Enter Week of Month");
					cell.ToolTipText =  "Enter Week of Month";
				}
				else
				{
                    //ToolTip1.SetToolTip(vsCodes, "");
                    cell.ToolTipText = "";
				}
				// Select Case Val(vsCodes.TextMatrix(intMouseRow, 6))
				// Case 1
				// pay period
				// vsCodes.ToolTipText = ""
				// Case 2
				// vsCodes.ToolTipText = "Enter Week of Month"
				// Case 4
				// vsCodes.ToolTipText = "Enter Month of Quarter (1-3) / Week of Month"
				// Case 5
				// vsCodes.ToolTipText = "Enter Month/Week of Month"
				// Case 6
				// vsCodes.ToolTipText = "Enter Week of Month"
				// Case Else
				// vsCodes.ToolTipText = ""
				// End Select
			}
			else
			{
                //ToolTip1.SetToolTip(vsCodes, "");
                cell.ToolTipText = "";
			}
		}

		private void vsCodes_RowColChange(object sender, System.EventArgs e)
		{
			vsCodes.Editable = ((FCConvert.ToInt32(vsCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCodes.Row, vsCodes.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) && !boolCantEdit) ? FCGrid.EditableSettings.flexEDKbdMouse : FCGrid.EditableSettings.flexEDNone;
			if (vsCodes.Col == 9)
			{
				vsCodes.EditMask = "####.##";
			}
			else
			{
				vsCodes.EditMask = string.Empty;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			for (intRows = 1; intRows <= (vsCodes.Rows - 1); intRows++)
			{
				for (intCols = 4; intCols <= (vsCodes.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "vsCodes";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Row " + FCConvert.ToString(intRows) + " " + vsCodes.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtAccruedCurrent";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Accrued Current";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtAccruedFiscal";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Accrued Fiscal";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtAccruedCalendar";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Accrued Calendar";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUsedCurrent";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Used Current";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUsedFiscal";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Used Fiscal";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUsedCalendar";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Used Calendar";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUsedBalance";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Type: " + cboType.Text + "  Balance";
			intTotalNumberOfControls += 1;
		}
	}
}
