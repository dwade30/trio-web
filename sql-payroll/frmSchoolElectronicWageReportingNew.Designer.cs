//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmSchoolElectronicWageReportingNew.
	/// </summary>
	partial class frmSchoolElectronicWageReportingNew
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cmbEndDate;
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cmbStartDate;
		public System.Collections.Generic.List<T2KDateBox> txtStartDate;
		public System.Collections.Generic.List<T2KDateBox> txtEndDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblFrom;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMonth;
		public fecherFoundation.FCFrame Frame4;
		public Global.T2KDateBox T2KSeasonalFrom;
		public fecherFoundation.FCTextBox txtSeasonalCode;
		public Global.T2KDateBox T2KSeasonalTo;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtRemitted;
		public fecherFoundation.FCTextBox txtCredit;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCFrame fraDateInfo;
		public fecherFoundation.FCComboBox cmbEndDate_2;
		public fecherFoundation.FCComboBox cmbEndDate_1;
		public fecherFoundation.FCComboBox cmbStartDate_2;
		public fecherFoundation.FCComboBox cmbStartDate_1;
		public fecherFoundation.FCComboBox cmbEndDate_0;
		public fecherFoundation.FCComboBox cmbStartDate_0;
		public Global.T2KDateBox txtStartDate_0;
		public Global.T2KDateBox txtEndDate_0;
		public Global.T2KDateBox txtStartDate_1;
		public Global.T2KDateBox txtEndDate_1;
		public Global.T2KDateBox txtStartDate_2;
		public Global.T2KDateBox txtEndDate_2;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblFrom_2;
		public fecherFoundation.FCLabel lblMonth_2;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblFrom_1;
		public fecherFoundation.FCLabel lblMonth_1;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCLabel lblFrom_0;
		public fecherFoundation.FCLabel lblMonth_0;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCFrame framFile;
		public fecherFoundation.FCTextBox txtFilename;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtSchoolUCAccount;
		public fecherFoundation.FCTextBox txtSchoolMRSId;
		public fecherFoundation.FCTextBox txtProcessorLicenseCode;
		public fecherFoundation.FCTextBox txtMRSID;
		public fecherFoundation.FCTextBox txtUCAccount;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCLabel Label1_13;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtExtension;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCTextBox txttitle;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtStreetAddress;
		public fecherFoundation.FCTextBox txtEmployerName;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuCopy;
		public fecherFoundation.FCToolStripMenuItem mnuCopySchool;
		public fecherFoundation.FCToolStripMenuItem mnuReprintNonSchool;
		public fecherFoundation.FCToolStripMenuItem ReprintSchoolSummary;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSchoolC1Override;
		public fecherFoundation.FCToolStripMenuItem mnuBLS3020;
		public fecherFoundation.FCToolStripMenuItem mnuPrintTest;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame4 = new fecherFoundation.FCFrame();
            this.T2KSeasonalFrom = new Global.T2KDateBox();
            this.txtSeasonalCode = new fecherFoundation.FCTextBox();
            this.T2KSeasonalTo = new Global.T2KDateBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtRemitted = new fecherFoundation.FCTextBox();
            this.txtCredit = new fecherFoundation.FCTextBox();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.fraDateInfo = new fecherFoundation.FCFrame();
            this.cmbEndDate_2 = new fecherFoundation.FCComboBox();
            this.cmbEndDate_1 = new fecherFoundation.FCComboBox();
            this.cmbStartDate_2 = new fecherFoundation.FCComboBox();
            this.cmbStartDate_1 = new fecherFoundation.FCComboBox();
            this.cmbEndDate_0 = new fecherFoundation.FCComboBox();
            this.cmbStartDate_0 = new fecherFoundation.FCComboBox();
            this.txtStartDate_0 = new Global.T2KDateBox();
            this.txtEndDate_0 = new Global.T2KDateBox();
            this.txtStartDate_1 = new Global.T2KDateBox();
            this.txtEndDate_1 = new Global.T2KDateBox();
            this.txtStartDate_2 = new Global.T2KDateBox();
            this.txtEndDate_2 = new Global.T2KDateBox();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.lblFrom_2 = new fecherFoundation.FCLabel();
            this.lblMonth_2 = new fecherFoundation.FCLabel();
            this.lblTo_1 = new fecherFoundation.FCLabel();
            this.lblFrom_1 = new fecherFoundation.FCLabel();
            this.lblMonth_1 = new fecherFoundation.FCLabel();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.lblFrom_0 = new fecherFoundation.FCLabel();
            this.lblMonth_0 = new fecherFoundation.FCLabel();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.framFile = new fecherFoundation.FCFrame();
            this.txtFilename = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtSchoolUCAccount = new fecherFoundation.FCTextBox();
            this.txtSchoolMRSId = new fecherFoundation.FCTextBox();
            this.txtProcessorLicenseCode = new fecherFoundation.FCTextBox();
            this.txtMRSID = new fecherFoundation.FCTextBox();
            this.txtUCAccount = new fecherFoundation.FCTextBox();
            this.txtFederalID = new fecherFoundation.FCTextBox();
            this.Label1_15 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.Label1_13 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtExtension = new fecherFoundation.FCTextBox();
            this.txtPhone = new Global.T2KPhoneNumberBox();
            this.txttitle = new fecherFoundation.FCTextBox();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtStreetAddress = new fecherFoundation.FCTextBox();
            this.txtEmployerName = new fecherFoundation.FCTextBox();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopy = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopySchool = new fecherFoundation.FCToolStripMenuItem();
            this.mnuReprintNonSchool = new fecherFoundation.FCToolStripMenuItem();
            this.ReprintSchoolSummary = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSchoolC1Override = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBLS3020 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintTest = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KSeasonalFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KSeasonalTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).BeginInit();
            this.fraDateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framFile)).BeginInit();
            this.framFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(813, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.fraDateInfo);
            this.ClientArea.Controls.Add(this.framFile);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(813, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(813, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(305, 30);
            this.HeaderText.Text = "Electronic Wage Reporting";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.T2KSeasonalFrom);
            this.Frame4.Controls.Add(this.txtSeasonalCode);
            this.Frame4.Controls.Add(this.T2KSeasonalTo);
            this.Frame4.Controls.Add(this.Label5);
            this.Frame4.Controls.Add(this.Label4);
            this.Frame4.Controls.Add(this.Label3);
            this.Frame4.Location = new System.Drawing.Point(571, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(155, 240);
            this.Frame4.TabIndex = 63;
            this.Frame4.Text = "Seasonal";
            // 
            // T2KSeasonalFrom
            // 
            this.T2KSeasonalFrom.Location = new System.Drawing.Point(20, 105);
            this.T2KSeasonalFrom.Mask = "00/00/0000";
            this.T2KSeasonalFrom.Name = "T2KSeasonalFrom";
            this.T2KSeasonalFrom.Size = new System.Drawing.Size(115, 40);
            this.T2KSeasonalFrom.TabIndex = 7;
            // 
            // txtSeasonalCode
            // 
            this.txtSeasonalCode.AutoSize = false;
            this.txtSeasonalCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeasonalCode.LinkItem = null;
            this.txtSeasonalCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSeasonalCode.LinkTopic = null;
            this.txtSeasonalCode.Location = new System.Drawing.Point(86, 30);
            this.txtSeasonalCode.Name = "txtSeasonalCode";
            this.txtSeasonalCode.Size = new System.Drawing.Size(48, 40);
            this.txtSeasonalCode.TabIndex = 6;
            // 
            // T2KSeasonalTo
            // 
            this.T2KSeasonalTo.Location = new System.Drawing.Point(20, 180);
            this.T2KSeasonalTo.Mask = "00/00/0000";
            this.T2KSeasonalTo.Name = "T2KSeasonalTo";
            this.T2KSeasonalTo.Size = new System.Drawing.Size(115, 40);
            this.T2KSeasonalTo.TabIndex = 8;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 155);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(25, 15);
            this.Label5.TabIndex = 66;
            this.Label5.Text = "TO";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 80);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(114, 15);
            this.Label4.TabIndex = 65;
            this.Label4.Text = "SEASONAL PERIOD";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(33, 19);
            this.Label3.TabIndex = 64;
            this.Label3.Text = "CODE";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtRemitted);
            this.Frame5.Controls.Add(this.txtCredit);
            this.Frame5.Controls.Add(this.Label1_12);
            this.Frame5.Controls.Add(this.Label1_11);
            this.Frame5.Location = new System.Drawing.Point(30, 280);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(490, 90);
            this.Frame5.TabIndex = 60;
            this.Frame5.Text = "Credit / Remitted";
            // 
            // txtRemitted
            // 
            this.txtRemitted.AutoSize = false;
            this.txtRemitted.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemitted.LinkItem = null;
            this.txtRemitted.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtRemitted.LinkTopic = null;
            this.txtRemitted.Location = new System.Drawing.Point(365, 30);
            this.txtRemitted.Name = "txtRemitted";
            this.txtRemitted.Size = new System.Drawing.Size(104, 40);
            this.txtRemitted.TabIndex = 10;
            this.txtRemitted.Text = "0";
            // 
            // txtCredit
            // 
            this.txtCredit.AutoSize = false;
            this.txtCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txtCredit.LinkItem = null;
            this.txtCredit.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCredit.LinkTopic = null;
            this.txtCredit.Location = new System.Drawing.Point(95, 30);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(104, 40);
            this.txtCredit.TabIndex = 9;
            this.txtCredit.Text = "0";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(220, 44);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(120, 16);
            this.Label1_12.TabIndex = 62;
            this.Label1_12.Text = "AMOUNT REMITTED";
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(20, 44);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(45, 16);
            this.Label1_11.TabIndex = 61;
            this.Label1_11.Text = "CREDIT";
            // 
            // fraDateInfo
            // 
            this.fraDateInfo.Controls.Add(this.cmbEndDate_2);
            this.fraDateInfo.Controls.Add(this.cmbEndDate_1);
            this.fraDateInfo.Controls.Add(this.cmbStartDate_2);
            this.fraDateInfo.Controls.Add(this.cmbStartDate_1);
            this.fraDateInfo.Controls.Add(this.cmbEndDate_0);
            this.fraDateInfo.Controls.Add(this.cmbStartDate_0);
            this.fraDateInfo.Controls.Add(this.txtStartDate_0);
            this.fraDateInfo.Controls.Add(this.txtEndDate_0);
            this.fraDateInfo.Controls.Add(this.txtStartDate_1);
            this.fraDateInfo.Controls.Add(this.txtEndDate_1);
            this.fraDateInfo.Controls.Add(this.txtStartDate_2);
            this.fraDateInfo.Controls.Add(this.txtEndDate_2);
            this.fraDateInfo.Controls.Add(this.lblTo_2);
            this.fraDateInfo.Controls.Add(this.lblFrom_2);
            this.fraDateInfo.Controls.Add(this.lblMonth_2);
            this.fraDateInfo.Controls.Add(this.lblTo_1);
            this.fraDateInfo.Controls.Add(this.lblFrom_1);
            this.fraDateInfo.Controls.Add(this.lblMonth_1);
            this.fraDateInfo.Controls.Add(this.lblTo_0);
            this.fraDateInfo.Controls.Add(this.lblFrom_0);
            this.fraDateInfo.Controls.Add(this.lblMonth_0);
            this.fraDateInfo.Controls.Add(this.lblInstructions);
            this.fraDateInfo.Location = new System.Drawing.Point(30, 30);
            this.fraDateInfo.Name = "fraDateInfo";
            this.fraDateInfo.Size = new System.Drawing.Size(521, 240);
            this.fraDateInfo.TabIndex = 43;
            this.fraDateInfo.Text = "Date Information";
            // 
            // cmbEndDate_2
            // 
            this.cmbEndDate_2.AutoSize = false;
            this.cmbEndDate_2.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEndDate_2.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbEndDate_2.FormattingEnabled = true;
            this.cmbEndDate_2.Location = new System.Drawing.Point(329, 157);
            this.cmbEndDate_2.Name = "cmbEndDate_2";
            this.cmbEndDate_2.Size = new System.Drawing.Size(113, 40);
            this.cmbEndDate_2.TabIndex = 5;
            this.cmbEndDate_2.Text = "cmbStartDate";
            // 
            // cmbEndDate_1
            // 
            this.cmbEndDate_1.AutoSize = false;
            this.cmbEndDate_1.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEndDate_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbEndDate_1.FormattingEnabled = true;
            this.cmbEndDate_1.Location = new System.Drawing.Point(329, 107);
            this.cmbEndDate_1.Name = "cmbEndDate_1";
            this.cmbEndDate_1.Size = new System.Drawing.Size(113, 40);
            this.cmbEndDate_1.TabIndex = 3;
            this.cmbEndDate_1.Text = "cmbStartDate";
            // 
            // cmbStartDate_2
            // 
            this.cmbStartDate_2.AutoSize = false;
            this.cmbStartDate_2.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStartDate_2.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStartDate_2.FormattingEnabled = true;
            this.cmbStartDate_2.Location = new System.Drawing.Point(161, 157);
            this.cmbStartDate_2.Name = "cmbStartDate_2";
            this.cmbStartDate_2.Size = new System.Drawing.Size(113, 40);
            this.cmbStartDate_2.TabIndex = 4;
            this.cmbStartDate_2.Text = "cmbStartDate";
            // 
            // cmbStartDate_1
            // 
            this.cmbStartDate_1.AutoSize = false;
            this.cmbStartDate_1.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStartDate_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStartDate_1.FormattingEnabled = true;
            this.cmbStartDate_1.Location = new System.Drawing.Point(161, 107);
            this.cmbStartDate_1.Name = "cmbStartDate_1";
            this.cmbStartDate_1.Size = new System.Drawing.Size(113, 40);
            this.cmbStartDate_1.TabIndex = 2;
            this.cmbStartDate_1.Text = "cmbStartDate";
            // 
            // cmbEndDate_0
            // 
            this.cmbEndDate_0.AutoSize = false;
            this.cmbEndDate_0.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEndDate_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbEndDate_0.FormattingEnabled = true;
            this.cmbEndDate_0.Location = new System.Drawing.Point(329, 57);
            this.cmbEndDate_0.Name = "cmbEndDate_0";
            this.cmbEndDate_0.Size = new System.Drawing.Size(113, 40);
            this.cmbEndDate_0.TabIndex = 1;
            this.cmbEndDate_0.Text = "cmbStartDate";
            // 
            // cmbStartDate_0
            // 
            this.cmbStartDate_0.AutoSize = false;
            this.cmbStartDate_0.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStartDate_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStartDate_0.FormattingEnabled = true;
            this.cmbStartDate_0.Location = new System.Drawing.Point(161, 57);
            this.cmbStartDate_0.Name = "cmbStartDate_0";
            this.cmbStartDate_0.Size = new System.Drawing.Size(113, 40);
            this.cmbStartDate_0.TabIndex = 0;
            this.cmbStartDate_0.Text = "cmbStartDate";
            // 
            // txtStartDate_0
            // 
            this.txtStartDate_0.Location = new System.Drawing.Point(161, 57);
            this.txtStartDate_0.Mask = "00/00/0000";
            this.txtStartDate_0.MaxLength = 10;
            this.txtStartDate_0.Name = "txtStartDate_0";
            this.txtStartDate_0.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate_0.TabIndex = 44;
            this.txtStartDate_0.Visible = false;
            // 
            // txtEndDate_0
            // 
            this.txtEndDate_0.Location = new System.Drawing.Point(329, 57);
            this.txtEndDate_0.Mask = "00/00/0000";
            this.txtEndDate_0.MaxLength = 10;
            this.txtEndDate_0.Name = "txtEndDate_0";
            this.txtEndDate_0.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate_0.TabIndex = 45;
            this.txtEndDate_0.Visible = false;
            // 
            // txtStartDate_1
            // 
            this.txtStartDate_1.Location = new System.Drawing.Point(161, 107);
            this.txtStartDate_1.Mask = "00/00/0000";
            this.txtStartDate_1.MaxLength = 10;
            this.txtStartDate_1.Name = "txtStartDate_1";
            this.txtStartDate_1.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate_1.TabIndex = 46;
            this.txtStartDate_1.Visible = false;
            // 
            // txtEndDate_1
            // 
            this.txtEndDate_1.Location = new System.Drawing.Point(329, 107);
            this.txtEndDate_1.Mask = "00/00/0000";
            this.txtEndDate_1.MaxLength = 10;
            this.txtEndDate_1.Name = "txtEndDate_1";
            this.txtEndDate_1.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate_1.TabIndex = 47;
            this.txtEndDate_1.Visible = false;
            // 
            // txtStartDate_2
            // 
            this.txtStartDate_2.Location = new System.Drawing.Point(161, 157);
            this.txtStartDate_2.Mask = "00/00/0000";
            this.txtStartDate_2.MaxLength = 10;
            this.txtStartDate_2.Name = "txtStartDate_2";
            this.txtStartDate_2.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate_2.TabIndex = 48;
            this.txtStartDate_2.Visible = false;
            // 
            // txtEndDate_2
            // 
            this.txtEndDate_2.Location = new System.Drawing.Point(329, 157);
            this.txtEndDate_2.Mask = "00/00/0000";
            this.txtEndDate_2.MaxLength = 10;
            this.txtEndDate_2.Name = "txtEndDate_2";
            this.txtEndDate_2.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate_2.TabIndex = 49;
            this.txtEndDate_2.Visible = false;
            // 
            // lblTo_2
            // 
            this.lblTo_2.Location = new System.Drawing.Point(289, 171);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(25, 19);
            this.lblTo_2.TabIndex = 59;
            this.lblTo_2.Text = "TO";
            this.lblTo_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFrom_2
            // 
            this.lblFrom_2.Location = new System.Drawing.Point(93, 171);
            this.lblFrom_2.Name = "lblFrom_2";
            this.lblFrom_2.Size = new System.Drawing.Size(41, 19);
            this.lblFrom_2.TabIndex = 58;
            this.lblFrom_2.Text = "FROM";
            // 
            // lblMonth_2
            // 
            this.lblMonth_2.Location = new System.Drawing.Point(20, 171);
            this.lblMonth_2.Name = "lblMonth_2";
            this.lblMonth_2.Size = new System.Drawing.Size(60, 19);
            this.lblMonth_2.TabIndex = 57;
            this.lblMonth_2.Text = "MONTH 3";
            // 
            // lblTo_1
            // 
            this.lblTo_1.Location = new System.Drawing.Point(289, 121);
            this.lblTo_1.Name = "lblTo_1";
            this.lblTo_1.Size = new System.Drawing.Size(25, 19);
            this.lblTo_1.TabIndex = 56;
            this.lblTo_1.Text = "TO";
            this.lblTo_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFrom_1
            // 
            this.lblFrom_1.Location = new System.Drawing.Point(93, 121);
            this.lblFrom_1.Name = "lblFrom_1";
            this.lblFrom_1.Size = new System.Drawing.Size(41, 19);
            this.lblFrom_1.TabIndex = 55;
            this.lblFrom_1.Text = "FROM";
            // 
            // lblMonth_1
            // 
            this.lblMonth_1.Location = new System.Drawing.Point(20, 121);
            this.lblMonth_1.Name = "lblMonth_1";
            this.lblMonth_1.Size = new System.Drawing.Size(60, 19);
            this.lblMonth_1.TabIndex = 54;
            this.lblMonth_1.Text = "MONTH 2";
            // 
            // lblTo_0
            // 
            this.lblTo_0.Location = new System.Drawing.Point(289, 71);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(25, 19);
            this.lblTo_0.TabIndex = 53;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFrom_0
            // 
            this.lblFrom_0.Location = new System.Drawing.Point(93, 71);
            this.lblFrom_0.Name = "lblFrom_0";
            this.lblFrom_0.Size = new System.Drawing.Size(41, 19);
            this.lblFrom_0.TabIndex = 52;
            this.lblFrom_0.Text = "FROM";
            // 
            // lblMonth_0
            // 
            this.lblMonth_0.Location = new System.Drawing.Point(20, 71);
            this.lblMonth_0.Name = "lblMonth_0";
            this.lblMonth_0.Size = new System.Drawing.Size(60, 19);
            this.lblMonth_0.TabIndex = 51;
            this.lblMonth_0.Text = "MONTH 1";
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(500, 16);
            this.lblInstructions.TabIndex = 50;
            this.lblInstructions.Text = "ENTER DATE/RANGE TO COVER 12TH OF THE MONTH FOR EACH MONTH IN QUARTER";
            // 
            // framFile
            // 
            this.framFile.Controls.Add(this.txtFilename);
            this.framFile.Controls.Add(this.Label2);
            this.framFile.Location = new System.Drawing.Point(30, 900);
            this.framFile.Name = "framFile";
            this.framFile.Size = new System.Drawing.Size(313, 90);
            this.framFile.TabIndex = 41;
            this.framFile.Text = "File";
            // 
            // txtFilename
            // 
			this.txtFilename.MaxLength = 7;
            this.txtFilename.AutoSize = false;
            this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilename.LinkItem = null;
            this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFilename.LinkTopic = null;
            this.txtFilename.Location = new System.Drawing.Point(207, 30);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(86, 40);
            this.txtFilename.TabIndex = 26;
            this.txtFilename.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFilename_KeyDown);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(160, 16);
            this.Label2.TabIndex = 42;
            this.Label2.Text = "FILENAME (NO EXTENSION)";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtSchoolUCAccount);
            this.Frame3.Controls.Add(this.txtSchoolMRSId);
            this.Frame3.Controls.Add(this.txtProcessorLicenseCode);
            this.Frame3.Controls.Add(this.txtMRSID);
            this.Frame3.Controls.Add(this.txtUCAccount);
            this.Frame3.Controls.Add(this.txtFederalID);
            this.Frame3.Controls.Add(this.Label1_15);
            this.Frame3.Controls.Add(this.Label1_14);
            this.Frame3.Controls.Add(this.Label1_13);
            this.Frame3.Controls.Add(this.Label1_10);
            this.Frame3.Controls.Add(this.Label1_9);
            this.Frame3.Controls.Add(this.Label1_8);
            this.Frame3.Location = new System.Drawing.Point(30, 680);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(588, 210);
            this.Frame3.TabIndex = 37;
            this.Frame3.Text = "Employer Account & Id Numbers";
            // 
            // txtSchoolUCAccount
            // 
			this.txtSchoolUCAccount.MaxLength = 10;
            this.txtSchoolUCAccount.AutoSize = false;
            this.txtSchoolUCAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtSchoolUCAccount.LinkItem = null;
            this.txtSchoolUCAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSchoolUCAccount.LinkTopic = null;
            this.txtSchoolUCAccount.Location = new System.Drawing.Point(472, 150);
            this.txtSchoolUCAccount.Name = "txtSchoolUCAccount";
            this.txtSchoolUCAccount.Size = new System.Drawing.Size(96, 40);
            this.txtSchoolUCAccount.TabIndex = 25;
            // 
            // txtSchoolMRSId
            // 
			this.txtSchoolMRSId.MaxLength = 11;
            this.txtSchoolMRSId.AutoSize = false;
            this.txtSchoolMRSId.BackColor = System.Drawing.SystemColors.Window;
            this.txtSchoolMRSId.LinkItem = null;
            this.txtSchoolMRSId.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSchoolMRSId.LinkTopic = null;
            this.txtSchoolMRSId.Location = new System.Drawing.Point(472, 100);
            this.txtSchoolMRSId.Name = "txtSchoolMRSId";
            this.txtSchoolMRSId.Size = new System.Drawing.Size(96, 40);
            this.txtSchoolMRSId.TabIndex = 23;
            // 
            // txtProcessorLicenseCode
            // 
			this.txtProcessorLicenseCode.MaxLength = 7;
            this.txtProcessorLicenseCode.AutoSize = false;
            this.txtProcessorLicenseCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtProcessorLicenseCode.LinkItem = null;
            this.txtProcessorLicenseCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtProcessorLicenseCode.LinkTopic = null;
            this.txtProcessorLicenseCode.Location = new System.Drawing.Point(451, 30);
            this.txtProcessorLicenseCode.Name = "txtProcessorLicenseCode";
            this.txtProcessorLicenseCode.Size = new System.Drawing.Size(117, 40);
            this.txtProcessorLicenseCode.TabIndex = 21;
            // 
            // txtMRSID
            // 
			this.txtMRSID.MaxLength = 11;
            this.txtMRSID.AutoSize = false;
            this.txtMRSID.BackColor = System.Drawing.SystemColors.Window;
            this.txtMRSID.LinkItem = null;
            this.txtMRSID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMRSID.LinkTopic = null;
            this.txtMRSID.Location = new System.Drawing.Point(366, 100);
            this.txtMRSID.Name = "txtMRSID";
            this.txtMRSID.Size = new System.Drawing.Size(96, 40);
            this.txtMRSID.TabIndex = 22;
            this.txtMRSID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRSID_KeyPress);
            // 
            // txtUCAccount
            // 
			this.txtUCAccount.MaxLength = 10;
            this.txtUCAccount.AutoSize = false;
            this.txtUCAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtUCAccount.LinkItem = null;
            this.txtUCAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUCAccount.LinkTopic = null;
            this.txtUCAccount.Location = new System.Drawing.Point(366, 150);
            this.txtUCAccount.Name = "txtUCAccount";
            this.txtUCAccount.Size = new System.Drawing.Size(96, 40);
            this.txtUCAccount.TabIndex = 24;
            this.txtUCAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUCAccount_KeyPress);
            // 
            // txtFederalID
            // 
			this.txtFederalID.MaxLength = 9;
            this.txtFederalID.AutoSize = false;
            this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalID.LinkItem = null;
            this.txtFederalID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFederalID.LinkTopic = null;
            this.txtFederalID.Location = new System.Drawing.Point(183, 30);
            this.txtFederalID.Name = "txtFederalID";
            this.txtFederalID.Size = new System.Drawing.Size(86, 40);
            this.txtFederalID.TabIndex = 20;
            this.txtFederalID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalID_KeyPress);
            // 
            // Label1_15
            // 
            this.Label1_15.Location = new System.Drawing.Point(472, 80);
            this.Label1_15.Name = "Label1_15";
            this.Label1_15.Size = new System.Drawing.Size(91, 16);
            this.Label1_15.TabIndex = 69;
            this.Label1_15.Text = "SCHOOL";
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(366, 80);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(91, 16);
            this.Label1_14.TabIndex = 68;
            this.Label1_14.Text = "TOWN";
            // 
            // Label1_13
            // 
            this.Label1_13.Location = new System.Drawing.Point(289, 44);
            this.Label1_13.Name = "Label1_13";
            this.Label1_13.Size = new System.Drawing.Size(140, 16);
            this.Label1_13.TabIndex = 67;
            this.Label1_13.Text = "PROCESSOR LIC. CODE";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 114);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(320, 16);
            this.Label1_10.TabIndex = 40;
            this.Label1_10.Text = "MAINE REVENUE SERVICES WITHHOLDING ACCOUNT ID";
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(20, 164);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(320, 16);
            this.Label1_9.TabIndex = 39;
            this.Label1_9.Text = "STATE UC EMPLOYER ACCOUNT NUMBER";
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 44);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(140, 16);
            this.Label1_8.TabIndex = 38;
            this.Label1_8.Text = "FEDERAL EMPLOYER ID";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtExtension);
            this.Frame2.Controls.Add(this.txtPhone);
            this.Frame2.Controls.Add(this.txttitle);
            this.Frame2.Controls.Add(this.Label1_7);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Location = new System.Drawing.Point(30, 580);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(748, 90);
            this.Frame2.TabIndex = 33;
            this.Frame2.Text = "Contact Information For Individual Responsible For Accuracy Of Wage Report";
            // 
            // txtExtension
            // 
			this.txtExtension.MaxLength = 4;
            this.txtExtension.AutoSize = false;
            this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtension.LinkItem = null;
            this.txtExtension.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExtension.LinkTopic = null;
            this.txtExtension.Location = new System.Drawing.Point(680, 30);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(48, 40);
            this.txtExtension.TabIndex = 19;
            this.txtExtension.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExtension_KeyPress);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(406, 30);
            this.txtPhone.Mask = "(999)000-0000";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(120, 40);
            this.txtPhone.TabIndex = 18;
            this.txtPhone.Text = "(   )    -";
            // 
            // txttitle
            // 
			this.txttitle.MaxLength = 30;
            this.txttitle.AutoSize = false;
            this.txttitle.BackColor = System.Drawing.SystemColors.Window;
            this.txttitle.LinkItem = null;
            this.txttitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txttitle.LinkTopic = null;
            this.txttitle.Location = new System.Drawing.Point(83, 30);
            this.txttitle.Name = "txttitle";
            this.txttitle.Size = new System.Drawing.Size(230, 40);
            this.txttitle.TabIndex = 17;
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(547, 44);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(105, 16);
            this.Label1_7.TabIndex = 36;
            this.Label1_7.Text = "EXT. OR MAILBOX";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(333, 44);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(45, 16);
            this.Label1_6.TabIndex = 35;
            this.Label1_6.Text = "PHONE";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 44);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(35, 16);
            this.Label1_5.TabIndex = 34;
            this.Label1_5.Text = "TITLE";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtStreetAddress);
            this.Frame1.Controls.Add(this.txtEmployerName);
            this.Frame1.Controls.Add(this.Label1_4);
            this.Frame1.Controls.Add(this.Label1_3);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Location = new System.Drawing.Point(30, 380);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(630, 190);
            this.Frame1.TabIndex = 27;
            this.Frame1.Text = "Employer Name & Address";
            // 
            // txtZip4
            // 
			this.txtZip4.MaxLength = 4;
            this.txtZip4.AutoSize = false;
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.LinkItem = null;
            this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip4.LinkTopic = null;
            this.txtZip4.Location = new System.Drawing.Point(560, 130);
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(50, 40);
            this.txtZip4.TabIndex = 16;
            this.txtZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip4_KeyPress);
            // 
            // txtZip
            // 
			this.txtZip.MaxLength = 5;
            this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(494, 130);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(56, 40);
            this.txtZip.TabIndex = 15;
            this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
            // 
            // txtState
            // 
			this.txtState.MaxLength = 2;
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(387, 130);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(50, 40);
            this.txtState.TabIndex = 14;
            // 
            // txtCity
            // 
			this.txtCity.MaxLength = 20;
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(153, 130);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(159, 40);
            this.txtCity.TabIndex = 13;
            // 
            // txtStreetAddress
            // 
			this.txtStreetAddress.MaxLength = 35;
            this.txtStreetAddress.AutoSize = false;
            this.txtStreetAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetAddress.LinkItem = null;
            this.txtStreetAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStreetAddress.LinkTopic = null;
            this.txtStreetAddress.Location = new System.Drawing.Point(153, 80);
            this.txtStreetAddress.Name = "txtStreetAddress";
            this.txtStreetAddress.Size = new System.Drawing.Size(457, 40);
            this.txtStreetAddress.TabIndex = 12;
            // 
            // txtEmployerName
            // 
			this.txtEmployerName.MaxLength = 44;
            this.txtEmployerName.AutoSize = false;
            this.txtEmployerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployerName.LinkItem = null;
            this.txtEmployerName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmployerName.LinkTopic = null;
            this.txtEmployerName.Location = new System.Drawing.Point(153, 30);
            this.txtEmployerName.Name = "txtEmployerName";
            this.txtEmployerName.Size = new System.Drawing.Size(457, 40);
            this.txtEmployerName.TabIndex = 11;
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(458, 144);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(25, 16);
            this.Label1_4.TabIndex = 32;
            this.Label1_4.Text = "ZIP";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(332, 144);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(40, 16);
            this.Label1_3.TabIndex = 31;
            this.Label1_3.Text = "STATE";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 144);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(40, 16);
            this.Label1_2.TabIndex = 30;
            this.Label1_2.Text = "CITY";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(110, 16);
            this.Label1_1.TabIndex = 29;
            this.Label1_1.Text = "STREET ADDRESS";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 44);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 16);
            this.Label1_0.TabIndex = 28;
            this.Label1_0.Text = "EMPLOYER";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPreview,
            this.mnuSepar3,
            this.mnuCopy,
            this.mnuCopySchool,
            this.mnuReprintNonSchool,
            this.ReprintSchoolSummary,
            this.mnuPrintSchoolC1Override,
            this.mnuBLS3020,
            this.mnuPrintTest});
            this.MainMenu1.Name = null;
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 0;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print Preview C1 Report";
            this.mnuPrintPreview.Visible = false;
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            this.mnuSepar3.Visible = false;
            // 
            // mnuCopy
            // 
            this.mnuCopy.Index = 2;
            this.mnuCopy.Name = "mnuCopy";
            this.mnuCopy.Text = "Copy Existing File to Floppy";
            this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
            // 
            // mnuCopySchool
            // 
            this.mnuCopySchool.Index = 3;
            this.mnuCopySchool.Name = "mnuCopySchool";
            this.mnuCopySchool.Text = "Copy School File to Floppy";
            this.mnuCopySchool.Click += new System.EventHandler(this.mnuCopySchool_Click);
            // 
            // mnuReprintNonSchool
            // 
            this.mnuReprintNonSchool.Index = 4;
            this.mnuReprintNonSchool.Name = "mnuReprintNonSchool";
            this.mnuReprintNonSchool.Text = "Reprint Electronic C1 Summary";
            this.mnuReprintNonSchool.Click += new System.EventHandler(this.mnuReprintNonSchool_Click);
            // 
            // ReprintSchoolSummary
            // 
            this.ReprintSchoolSummary.Index = 5;
            this.ReprintSchoolSummary.Name = "ReprintSchoolSummary";
            this.ReprintSchoolSummary.Text = "Reprint School Electronic C1 Summary";
            this.ReprintSchoolSummary.Click += new System.EventHandler(this.ReprintSchoolSummary_Click);
            // 
            // mnuPrintSchoolC1Override
            // 
            this.mnuPrintSchoolC1Override.Index = 6;
            this.mnuPrintSchoolC1Override.Name = "mnuPrintSchoolC1Override";
            this.mnuPrintSchoolC1Override.Text = "Reprint School C1 With Rate Override";
            this.mnuPrintSchoolC1Override.Click += new System.EventHandler(this.mnuPrintSchoolC1Override_Click);
            // 
            // mnuBLS3020
            // 
            this.mnuBLS3020.Index = 7;
            this.mnuBLS3020.Name = "mnuBLS3020";
            this.mnuBLS3020.Text = "Set Multiple Worksite Report Info";
            this.mnuBLS3020.Visible = false;
            this.mnuBLS3020.Click += new System.EventHandler(this.mnuBLS3020_Click);
            // 
            // mnuPrintTest
            // 
            this.mnuPrintTest.Index = 8;
            this.mnuPrintTest.Name = "mnuPrintTest";
            this.mnuPrintTest.Text = "Print Test C1";
            this.mnuPrintTest.Click += new System.EventHandler(this.mnuPrintTest_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Continue";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(301, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(174, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmSchoolElectronicWageReportingNew
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(813, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmSchoolElectronicWageReportingNew";
            this.Text = "Electronic Wage Reporting";
            this.Load += new System.EventHandler(this.frmSchoolElectronicWageReportingNew_Load);
            this.Activated += new System.EventHandler(this.frmSchoolElectronicWageReportingNew_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSchoolElectronicWageReportingNew_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KSeasonalFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KSeasonalTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDateInfo)).EndInit();
            this.fraDateInfo.ResumeLayout(false);
            this.fraDateInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framFile)).EndInit();
            this.framFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
