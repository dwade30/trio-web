﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCustomRetirementDeductions.
	/// </summary>
	partial class frmCustomRetirementDeductions
	{
		public fecherFoundation.FCComboBox cmbReportOn;
		public fecherFoundation.FCLabel lblReportOn;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
        //public fecherFoundation.FCTextBox txtStartDate;
        //public fecherFoundation.FCTextBox txtEndDate;
        public Global.T2KDateBox txtStartDate;
        public Global.T2KDateBox txtEndDate;
        public fecherFoundation.FCTextBox txtEmployeeBoth;
		public fecherFoundation.FCLabel lblCaption;
		public fecherFoundation.FCLabel lblEmployeeBoth;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbReportOn = new fecherFoundation.FCComboBox();
			this.lblReportOn = new fecherFoundation.FCLabel();
			this.vsData = new fecherFoundation.FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.txtEmployeeBoth = new fecherFoundation.FCTextBox();
			this.lblCaption = new fecherFoundation.FCLabel();
			this.lblEmployeeBoth = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 311);
			this.BottomPanel.Size = new System.Drawing.Size(722, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsData);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbReportOn);
			this.ClientArea.Controls.Add(this.lblReportOn);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(742, 436);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblReportOn, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbReportOn, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsData, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(742, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(403, 28);
			this.HeaderText.Text = "Custom Retirement Deduction Report";
			// 
			// cmbReportOn
			// 
			this.cmbReportOn.Items.AddRange(new object[] {
            "Single Employee",
            "Emp/Date Range",
            "Date Range",
            "All Files"});
			this.cmbReportOn.Location = new System.Drawing.Point(200, 30);
			this.cmbReportOn.Name = "cmbReportOn";
			this.cmbReportOn.Size = new System.Drawing.Size(196, 40);
			this.cmbReportOn.TabIndex = 13;
			this.cmbReportOn.Text = "Single Employee";
			this.cmbReportOn.SelectedIndexChanged += new System.EventHandler(this.optReportOn_CheckedChanged);
			// 
			// lblReportOn
			// 
			this.lblReportOn.AutoSize = true;
			this.lblReportOn.Location = new System.Drawing.Point(30, 44);
			this.lblReportOn.Name = "lblReportOn";
			this.lblReportOn.Size = new System.Drawing.Size(122, 15);
			this.lblReportOn.TabIndex = 14;
			this.lblReportOn.Text = "REPORT CRITERIA";
			// 
			// vsData
			// 
			this.vsData.Cols = 9;
			this.vsData.ColumnHeadersVisible = false;
			this.vsData.FixedCols = 0;
			this.vsData.FixedRows = 0;
			this.vsData.Location = new System.Drawing.Point(30, 229);
			this.vsData.Name = "vsData";
			this.vsData.RowHeadersVisible = false;
			this.vsData.Size = new System.Drawing.Size(814, 82);
			this.vsData.TabIndex = 12;
			this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtEmployeeNumber);
			this.Frame2.Controls.Add(this.txtStartDate);
			this.Frame2.Controls.Add(this.txtEndDate);
			this.Frame2.Controls.Add(this.txtEmployeeBoth);
			this.Frame2.Controls.Add(this.lblCaption);
			this.Frame2.Controls.Add(this.lblEmployeeBoth);
			this.Frame2.Location = new System.Drawing.Point(30, 90);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(654, 90);
			this.Frame2.TabIndex = 8;
			this.Frame2.Text = "Parameters";
			// 
			// txtEmployeeNumber
			// 
			this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployeeNumber.Location = new System.Drawing.Point(201, 30);
			this.txtEmployeeNumber.Name = "txtEmployeeNumber";
			this.txtEmployeeNumber.Size = new System.Drawing.Size(80, 40);
			this.txtEmployeeNumber.TabIndex = 4;
			// 
			// txtStartDate
			// 
			this.txtStartDate.AutoSize = false;
			this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartDate.Location = new System.Drawing.Point(161, 30);
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(120, 40);
			this.txtStartDate.TabIndex = 5;
			this.txtStartDate.Visible = false;
			// 
			// txtEndDate
			// 
			this.txtEndDate.AutoSize = false;
			this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndDate.Location = new System.Drawing.Point(311, 30);
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(120, 40);
			this.txtEndDate.TabIndex = 6;
			this.txtEndDate.Visible = false;
			// 
			// txtEmployeeBoth
			// 
			this.txtEmployeeBoth.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmployeeBoth.Location = new System.Drawing.Point(534, 30);
			this.txtEmployeeBoth.Name = "txtEmployeeBoth";
			this.txtEmployeeBoth.Size = new System.Drawing.Size(80, 40);
			this.txtEmployeeBoth.TabIndex = 7;
			this.txtEmployeeBoth.Visible = false;
			// 
			// lblCaption
			// 
			this.lblCaption.Location = new System.Drawing.Point(20, 44);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(121, 15);
			this.lblCaption.TabIndex = 10;
			this.lblCaption.Text = "EMPLOYEE NUMBER";
			// 
			// lblEmployeeBoth
			// 
			this.lblEmployeeBoth.Location = new System.Drawing.Point(491, 44);
			this.lblEmployeeBoth.Name = "lblEmployeeBoth";
			this.lblEmployeeBoth.Size = new System.Drawing.Size(36, 15);
			this.lblEmployeeBoth.TabIndex = 9;
			this.lblEmployeeBoth.Text = "EMP";
			this.lblEmployeeBoth.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 200);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(211, 15);
			this.Label1.TabIndex = 13;
			this.Label1.Text = "DEDUCTION FIELD SELECTION";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Print / Preview";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(282, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(158, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Print / Preview";
			this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
			// 
			// frmCustomRetirementDeductions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(742, 496);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCustomRetirementDeductions";
			this.ShowInTaskbar = false;
			this.Text = "Custom Retirement Deduction Report";
			this.Load += new System.EventHandler(this.frmCustomRetirementDeductions_Load);
			this.Activated += new System.EventHandler(this.frmCustomRetirementDeductions_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomRetirementDeductions_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
