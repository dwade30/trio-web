//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmUpdateMatches.
	/// </summary>
	partial class frmUpdateMatches
	{
		public FCGrid GridEmployees;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cmbAccountType;
		public fecherFoundation.FCTextBox txtSegment1;
		public fecherFoundation.FCTextBox txtSegment2;
		public fecherFoundation.FCTextBox txtSegment3;
		public fecherFoundation.FCTextBox txtSegment4;
		public fecherFoundation.FCButton cmdUpdate;
		public fecherFoundation.FCTextBox txtMaxAmount;
		public fecherFoundation.FCComboBox cmbStatus;
		public fecherFoundation.FCComboBox cmbFrequency;
		public fecherFoundation.FCComboBox cmbMaxType;
		public fecherFoundation.FCComboBox cmbAmountType;
		public fecherFoundation.FCTextBox txtAmount;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbDepartment;
		public fecherFoundation.FCComboBox cmbCodeOne;
		public fecherFoundation.FCComboBox cmbCodeTwo;
		public fecherFoundation.FCComboBox cmbGroupIDs;
		public fecherFoundation.FCComboBox cmbSequence;
		public fecherFoundation.FCComboBox cmbFullTime;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCComboBox cmbMatchType;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.GridEmployees = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmbAccountType = new fecherFoundation.FCComboBox();
            this.txtSegment1 = new fecherFoundation.FCTextBox();
            this.txtSegment2 = new fecherFoundation.FCTextBox();
            this.txtSegment3 = new fecherFoundation.FCTextBox();
            this.txtSegment4 = new fecherFoundation.FCTextBox();
            this.cmdUpdate = new fecherFoundation.FCButton();
            this.txtMaxAmount = new fecherFoundation.FCTextBox();
            this.cmbStatus = new fecherFoundation.FCComboBox();
            this.cmbFrequency = new fecherFoundation.FCComboBox();
            this.cmbMaxType = new fecherFoundation.FCComboBox();
            this.cmbAmountType = new fecherFoundation.FCComboBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmbDepartment = new fecherFoundation.FCComboBox();
            this.cmbCodeOne = new fecherFoundation.FCComboBox();
            this.cmbCodeTwo = new fecherFoundation.FCComboBox();
            this.cmbGroupIDs = new fecherFoundation.FCComboBox();
            this.cmbSequence = new fecherFoundation.FCComboBox();
            this.cmbFullTime = new fecherFoundation.FCComboBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.cmbMatchType = new fecherFoundation.FCComboBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(898, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridEmployees);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmbMatchType);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Size = new System.Drawing.Size(898, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(302, 30);
            this.HeaderText.Text = "Update Employer Matches";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // GridEmployees
            // 
            this.GridEmployees.AllowSelection = false;
            this.GridEmployees.AllowUserToResizeColumns = false;
            this.GridEmployees.AllowUserToResizeRows = false;
            this.GridEmployees.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridEmployees.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorBkg = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.BackColorSel = System.Drawing.Color.Empty;
            this.GridEmployees.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridEmployees.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridEmployees.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridEmployees.ColumnHeadersHeight = 30;
            this.GridEmployees.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridEmployees.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridEmployees.DragIcon = null;
            this.GridEmployees.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridEmployees.ExtendLastCol = true;
            this.GridEmployees.FixedCols = 0;
            this.GridEmployees.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.FrozenCols = 0;
            this.GridEmployees.GridColor = System.Drawing.Color.Empty;
            this.GridEmployees.GridColorFixed = System.Drawing.Color.Empty;
            this.GridEmployees.Location = new System.Drawing.Point(544, 90);
            this.GridEmployees.Name = "GridEmployees";
            this.GridEmployees.OutlineCol = 0;
            this.GridEmployees.ReadOnly = true;
            this.GridEmployees.RowHeadersVisible = false;
            this.GridEmployees.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridEmployees.RowHeightMin = 0;
            this.GridEmployees.Rows = 1;
            this.GridEmployees.ScrollTipText = null;
            this.GridEmployees.ShowColumnVisibilityMenu = false;
            this.GridEmployees.Size = new System.Drawing.Size(319, 680);
            this.GridEmployees.StandardTab = true;
            this.GridEmployees.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridEmployees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridEmployees.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.GridEmployees, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmbAccountType);
            this.Frame2.Controls.Add(this.txtSegment1);
            this.Frame2.Controls.Add(this.txtSegment2);
            this.Frame2.Controls.Add(this.txtSegment3);
            this.Frame2.Controls.Add(this.txtSegment4);
            this.Frame2.Controls.Add(this.cmdUpdate);
            this.Frame2.Controls.Add(this.txtMaxAmount);
            this.Frame2.Controls.Add(this.cmbStatus);
            this.Frame2.Controls.Add(this.cmbFrequency);
            this.Frame2.Controls.Add(this.cmbMaxType);
            this.Frame2.Controls.Add(this.cmbAmountType);
            this.Frame2.Controls.Add(this.txtAmount);
            this.Frame2.Controls.Add(this.Label14);
            this.Frame2.Controls.Add(this.Label13);
            this.Frame2.Controls.Add(this.Label11);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.Controls.Add(this.Label8);
            this.Frame2.Location = new System.Drawing.Point(30, 430);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(470, 340);
            this.Frame2.TabIndex = 27;
            this.Frame2.Text = "Update";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.AutoSize = false;
            this.cmbAccountType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccountType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAccountType.FormattingEnabled = true;
            this.cmbAccountType.Location = new System.Drawing.Point(128, 80);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(80, 40);
            this.cmbAccountType.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.cmbAccountType, null);
            this.cmbAccountType.SelectedIndexChanged += new System.EventHandler(this.cmbAccountType_SelectedIndexChanged);
            // 
            // txtSegment1
            // 
            this.txtSegment1.AutoSize = false;
            this.txtSegment1.BackColor = System.Drawing.SystemColors.Window;
            this.txtSegment1.LinkItem = null;
            this.txtSegment1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSegment1.LinkTopic = null;
            this.txtSegment1.Location = new System.Drawing.Point(220, 80);
            this.txtSegment1.Name = "txtSegment1";
            this.txtSegment1.Size = new System.Drawing.Size(50, 40);
            this.txtSegment1.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtSegment1, null);
            this.txtSegment1.Visible = false;
            this.txtSegment1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment1_KeyPress);
            // 
            // txtSegment2
            // 
            this.txtSegment2.AutoSize = false;
            this.txtSegment2.BackColor = System.Drawing.SystemColors.Window;
            this.txtSegment2.LinkItem = null;
            this.txtSegment2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSegment2.LinkTopic = null;
            this.txtSegment2.Location = new System.Drawing.Point(280, 80);
            this.txtSegment2.Name = "txtSegment2";
            this.txtSegment2.Size = new System.Drawing.Size(50, 40);
            this.txtSegment2.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtSegment2, null);
            this.txtSegment2.Visible = false;
            this.txtSegment2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment2_KeyPress);
            // 
            // txtSegment3
            // 
            this.txtSegment3.AutoSize = false;
            this.txtSegment3.BackColor = System.Drawing.SystemColors.Window;
            this.txtSegment3.LinkItem = null;
            this.txtSegment3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSegment3.LinkTopic = null;
            this.txtSegment3.Location = new System.Drawing.Point(340, 80);
            this.txtSegment3.Name = "txtSegment3";
            this.txtSegment3.Size = new System.Drawing.Size(50, 40);
            this.txtSegment3.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtSegment3, null);
            this.txtSegment3.Visible = false;
            this.txtSegment3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment3_KeyPress);
            // 
            // txtSegment4
            // 
            this.txtSegment4.AutoSize = false;
            this.txtSegment4.BackColor = System.Drawing.SystemColors.Window;
            this.txtSegment4.LinkItem = null;
            this.txtSegment4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSegment4.LinkTopic = null;
            this.txtSegment4.Location = new System.Drawing.Point(400, 80);
            this.txtSegment4.Name = "txtSegment4";
            this.txtSegment4.Size = new System.Drawing.Size(50, 40);
            this.txtSegment4.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtSegment4, null);
            this.txtSegment4.Visible = false;
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.AppearanceKey = "actionButton";
            this.cmdUpdate.Location = new System.Drawing.Point(20, 280);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(102, 40);
            this.cmdUpdate.TabIndex = 17;
            this.cmdUpdate.Text = "Update";
            this.ToolTip1.SetToolTip(this.cmdUpdate, "Update matches with provided values");
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // txtMaxAmount
            // 
            this.txtMaxAmount.AutoSize = false;
            this.txtMaxAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxAmount.LinkItem = null;
            this.txtMaxAmount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMaxAmount.LinkTopic = null;
            this.txtMaxAmount.Location = new System.Drawing.Point(128, 130);
            this.txtMaxAmount.Name = "txtMaxAmount";
            this.txtMaxAmount.Size = new System.Drawing.Size(82, 40);
            this.txtMaxAmount.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtMaxAmount, null);
            this.txtMaxAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMaxAmount_KeyPress);
            // 
            // cmbStatus
            // 
            this.cmbStatus.AutoSize = false;
            this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(128, 230);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(322, 40);
            this.cmbStatus.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.cmbStatus, null);
            // 
            // cmbFrequency
            // 
            this.cmbFrequency.AutoSize = false;
            this.cmbFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFrequency.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFrequency.FormattingEnabled = true;
            this.cmbFrequency.Location = new System.Drawing.Point(128, 180);
            this.cmbFrequency.Name = "cmbFrequency";
            this.cmbFrequency.Size = new System.Drawing.Size(322, 40);
            this.cmbFrequency.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.cmbFrequency, null);
            // 
            // cmbMaxType
            // 
            this.cmbMaxType.AutoSize = false;
            this.cmbMaxType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMaxType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMaxType.FormattingEnabled = true;
            this.cmbMaxType.Location = new System.Drawing.Point(220, 130);
            this.cmbMaxType.Name = "cmbMaxType";
            this.cmbMaxType.Size = new System.Drawing.Size(230, 40);
            this.cmbMaxType.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.cmbMaxType, null);
            // 
            // cmbAmountType
            // 
            this.cmbAmountType.AutoSize = false;
            this.cmbAmountType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAmountType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAmountType.FormattingEnabled = true;
            this.cmbAmountType.Location = new System.Drawing.Point(220, 30);
            this.cmbAmountType.Name = "cmbAmountType";
            this.cmbAmountType.Size = new System.Drawing.Size(230, 40);
            this.cmbAmountType.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cmbAmountType, null);
            // 
            // txtAmount
            // 
            this.txtAmount.AutoSize = false;
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.LinkItem = null;
            this.txtAmount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAmount.LinkTopic = null;
            this.txtAmount.Location = new System.Drawing.Point(128, 30);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(82, 40);
            this.txtAmount.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtAmount, null);
            this.txtAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 244);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(90, 16);
            this.Label14.TabIndex = 32;
            this.Label14.Text = "STATUS";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 194);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(90, 16);
            this.Label13.TabIndex = 31;
            this.Label13.Text = "FREQUENCY";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 144);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(90, 16);
            this.Label11.TabIndex = 30;
            this.Label11.Text = "MAX AMOUNT";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 94);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(90, 16);
            this.Label10.TabIndex = 29;
            this.Label10.Text = "ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(90, 16);
            this.Label8.TabIndex = 28;
            this.Label8.Text = "AMOUNT";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmbDepartment);
            this.Frame1.Controls.Add(this.cmbCodeOne);
            this.Frame1.Controls.Add(this.cmbCodeTwo);
            this.Frame1.Controls.Add(this.cmbGroupIDs);
            this.Frame1.Controls.Add(this.cmbSequence);
            this.Frame1.Controls.Add(this.cmbFullTime);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Location = new System.Drawing.Point(30, 80);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(470, 340);
            this.Frame1.TabIndex = 19;
            this.Frame1.Text = "Filter";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.AutoSize = false;
            this.cmbDepartment.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDepartment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(145, 30);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(305, 40);
            this.cmbDepartment.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.cmbDepartment, null);
            this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.cmbDepartment_SelectedIndexChanged);
            // 
            // cmbCodeOne
            // 
            this.cmbCodeOne.AutoSize = false;
            this.cmbCodeOne.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeOne.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCodeOne.FormattingEnabled = true;
            this.cmbCodeOne.Location = new System.Drawing.Point(145, 80);
            this.cmbCodeOne.Name = "cmbCodeOne";
            this.cmbCodeOne.Size = new System.Drawing.Size(305, 40);
            this.cmbCodeOne.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbCodeOne, null);
            this.cmbCodeOne.SelectedIndexChanged += new System.EventHandler(this.cmbCodeOne_SelectedIndexChanged);
            // 
            // cmbCodeTwo
            // 
            this.cmbCodeTwo.AutoSize = false;
            this.cmbCodeTwo.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeTwo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCodeTwo.FormattingEnabled = true;
            this.cmbCodeTwo.Location = new System.Drawing.Point(145, 130);
            this.cmbCodeTwo.Name = "cmbCodeTwo";
            this.cmbCodeTwo.Size = new System.Drawing.Size(305, 40);
            this.cmbCodeTwo.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbCodeTwo, null);
            this.cmbCodeTwo.SelectedIndexChanged += new System.EventHandler(this.cmbCodeTwo_SelectedIndexChanged);
            // 
            // cmbGroupIDs
            // 
            this.cmbGroupIDs.AutoSize = false;
            this.cmbGroupIDs.BackColor = System.Drawing.SystemColors.Window;
            this.cmbGroupIDs.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbGroupIDs.FormattingEnabled = true;
            this.cmbGroupIDs.Location = new System.Drawing.Point(145, 180);
            this.cmbGroupIDs.Name = "cmbGroupIDs";
            this.cmbGroupIDs.Size = new System.Drawing.Size(305, 40);
            this.cmbGroupIDs.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbGroupIDs, null);
            this.cmbGroupIDs.SelectedIndexChanged += new System.EventHandler(this.cmbGroupIDs_SelectedIndexChanged);
            // 
            // cmbSequence
            // 
            this.cmbSequence.AutoSize = false;
            this.cmbSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSequence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSequence.FormattingEnabled = true;
            this.cmbSequence.Location = new System.Drawing.Point(145, 230);
            this.cmbSequence.Name = "cmbSequence";
            this.cmbSequence.Size = new System.Drawing.Size(305, 40);
            this.cmbSequence.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbSequence, null);
            this.cmbSequence.SelectedIndexChanged += new System.EventHandler(this.cmbSequence_SelectedIndexChanged);
            // 
            // cmbFullTime
            // 
            this.cmbFullTime.AutoSize = false;
            this.cmbFullTime.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFullTime.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFullTime.FormattingEnabled = true;
            this.cmbFullTime.Location = new System.Drawing.Point(145, 280);
            this.cmbFullTime.Name = "cmbFullTime";
            this.cmbFullTime.Size = new System.Drawing.Size(305, 40);
            this.cmbFullTime.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cmbFullTime, null);
            this.cmbFullTime.SelectedIndexChanged += new System.EventHandler(this.cmbFullTime_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 16);
            this.Label1.TabIndex = 25;
            this.Label1.Text = "DEPARTMENT";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 16);
            this.Label2.TabIndex = 24;
            this.Label2.Text = "CODE ONE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 16);
            this.Label3.TabIndex = 23;
            this.Label3.Text = "CODE TWO";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 194);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 16);
            this.Label4.TabIndex = 22;
            this.Label4.Text = "GROUP ID";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 244);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(100, 16);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "SEQUENCE";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 294);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(100, 16);
            this.Label6.TabIndex = 20;
            this.Label6.Text = "PART/FULL TIME";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // cmbMatchType
            // 
            this.cmbMatchType.AutoSize = false;
            this.cmbMatchType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMatchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMatchType.FormattingEnabled = true;
            this.cmbMatchType.Location = new System.Drawing.Point(170, 30);
            this.cmbMatchType.Name = "cmbMatchType";
            this.cmbMatchType.Size = new System.Drawing.Size(330, 40);
            this.cmbMatchType.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.cmbMatchType, null);
            this.cmbMatchType.SelectedIndexChanged += new System.EventHandler(this.cmbMatchType_SelectedIndexChanged);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(140, 19);
            this.Label7.TabIndex = 26;
            this.Label7.Text = "EMPLOYER MATCH";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(394, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmUpdateMatches
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmUpdateMatches";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Update Employer Matches";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmUpdateMatches_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUpdateMatches_KeyDown);
            this.Resize += new System.EventHandler(this.frmUpdateMatches_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridEmployees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
	}
}