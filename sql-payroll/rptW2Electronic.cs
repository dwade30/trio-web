﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Electronic.
	/// </summary>
	public partial class rptW2Electronic : BaseSectionReport
	{
		public rptW2Electronic()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W2 Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2Electronic InstancePtr
		{
			get
			{
				return (rptW2Electronic)Sys.GetInstance(typeof(rptW2Electronic));
			}
		}

		protected rptW2Electronic _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2Electronic	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		FCFileSystem fso = new FCFileSystem();
        StreamReader ts;
		string strRec = "";
		bool boolFileOpen;
		int lngTotalCount;
		double dblTotGrossFed;
		double dblTotFedTax;
		double dblTotGrossSocSec;
		double dblTotSocTax;
		double dblTotGrossMed;
		double dblTotMedTax;
		double dblTot401;
		double dblTot408;
		double dblTotBox12L;
		double dblTot457;
		double dblTot403;
		double dblTot501;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, false, showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = ts.EndOfStream;
			if (!eArgs.EOF)
			{
				strRec = ts.ReadLine();
				// If Left(strRec, 2) = "RT" Or Left(strRec, 2) = "RF" Then EOF = True
				if (Strings.Left(strRec, 2) == "RT")
				{
					ReadRTRecord();
					if (!ts.EndOfStream)
					{
						strRec = ts.ReadLine();
						if (Strings.Left(strRec, 2) == "RE")
						{
							this.Fields["grpHeader"].Value = Strings.Mid(strRec, 219, 1);
							if (!ts.EndOfStream)
							{
								strRec = ts.ReadLine();
							}
						}
					}
				}
				if (Strings.Left(strRec, 2) == "RF")
					eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp;
				boolFileOpen = false;
				lblMuni.Text = fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gstrMuniName);
				lblMuni2.Text = fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gstrMuniName);
				lblDate.Text = Strings.Format(DateTime.Today, "m/dd/yyyy");
				lblDate2.Text = lblDate.Text;
				lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
				lblTime2.Text = lblTime.Text;
				lblPage.Text = "Page 1";
				lngTotalCount = 0;
				dblTotGrossFed = 0;
				dblTotFedTax = 0;
				dblTotGrossSocSec = 0;
				dblTotSocTax = 0;
				dblTotGrossMed = 0;
				dblTotMedTax = 0;
				dblTot401 = 0;
				dblTot408 = 0;
				dblTotBox12L = 0;
				dblTot457 = 0;
				dblTot403 = 0;
				dblTot501 = 0;
				if (!FCFileSystem.FileExists("W2Report"))
				{
					MessageBox.Show("Cannot find file " + Environment.CurrentDirectory + "\\W2Report");
					this.Close();
					return;
				}
				ts = FCFileSystem.OpenText("W2Report");
				boolFileOpen = true;
				strRec = ts.ReadLine();
				// strrec has the RA record now
				ReadRARecord();
				strRec = ts.ReadLine();
				// this is the rerecord
				strTemp = Strings.Mid(strRec, 3, 4);
				// tax year
				this.Fields["grpHeader"].Value = Strings.Mid(strRec, 219, 1);
				txtYear.Text = strTemp;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				if (!ts.EndOfStream)
				{
					ReadRWRecord();
					if (!ts.EndOfStream)
					{
						// read the rsrecord
						strRec = ts.ReadLine();
						if (Strings.Left(strRec, 2) == "RS")
						{
							ReadRSRecord();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
			if (this.PageNumber > 1)
				PageHeader.Visible = true;
		}

		private void ReadRARecord()
		{
			// submitter record. first record in file
			string strTemp;
			strTemp = Strings.Mid(strRec, 3, 9);
			// employer id number
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			txtID.Text = strTemp;
			strTemp = Strings.Mid(strRec, 38, 57);
			// company name
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			txtCompany.Text = strTemp;
			strTemp = Strings.Mid(strRec, 95, 22);
			// location address
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			txtAddress1.Text = strTemp;
			strTemp = Strings.Mid(strRec, 117, 22);
			// delivery address
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			txtAddress2.Text = strTemp;
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 139, 22)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 161, 2)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 163, 5));
			txtCityStateZip.Text = strTemp;
			// city, state & zip
		}

		private void ReadRSRecord()
		{
			// employee state record
			string strTemp;
			double dblTemp;
			strTemp = Strings.Mid(strRec, 276, 11);
			dblTemp = Conversion.Val(strTemp) / 100;
			txtStateWages.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 287, 11);
			dblTemp = Conversion.Val(strTemp) / 100;
			txtStateTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
		}

		private void ReadRWRecord()
		{
			// employee record
			string strTemp;
			double dblTemp;
			strTemp = Strings.Mid(strRec, 3, 9);
			// Social Security Number
			txtSSN.Text = strTemp;
			strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(Strings.Mid(strRec, 12, 15)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 27, 15)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 42, 20)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 62, 4)));
			txtName.Text = strTemp;
			// Employee First, Middle, and last name
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 66, 22));
			// location address
			txtEmployeeAddress1.Text = strTemp;
			strTemp = fecherFoundation.Strings.Trim(Strings.Mid(strRec, 88, 22));
			// delivery address
			txtEmployeeAddress2.Text = strTemp;
			strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(Strings.Mid(strRec, 110, 22)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 132, 2)) + " " + fecherFoundation.Strings.Trim(Strings.Mid(strRec, 134, 5)));
			txtEmployeeCityStateZip.Text = strTemp;
			// city state zip
			strTemp = Strings.Mid(strRec, 188, 11);
			// wages, tips & other compensations
			dblTemp = Conversion.Val(strTemp) / 100;
			txtGrossFed.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 199, 11);
			// Federal Income Tax Withheld
			dblTemp = Conversion.Val(strTemp) / 100;
			txtFedTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 210, 11);
			// Social Security Wages
			dblTemp = Conversion.Val(strTemp) / 100;
			txtGrossSocSec.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 221, 11);
			// Social Security Tax Withheld
			dblTemp = Conversion.Val(strTemp) / 100;
			txtSocTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 232, 11);
			// Medicare Wages & tips
			dblTemp = Conversion.Val(strTemp) / 100;
			txtGrossMed.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 243, 11);
			// Medicare Tax Withheld
			dblTemp = Conversion.Val(strTemp) / 100;
			txtMedTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 408, 11);
			// Employer cost of premiums for group term over 50000
			dblTemp = Conversion.Val(strTemp) / 100;
			txtBox12L.Text = Strings.Format(dblTemp, "#,###,##0.00");
			strTemp = Strings.Mid(strRec, 488, 1);
			// Retirement plan indicator
			txtPen.Text = strTemp;
			strTemp = Strings.Mid(strRec, 287, 11);
			// 401k
			dblTemp = Conversion.Val(strTemp) / 100;
			if (dblTemp > 0)
			{
				txt401.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=401";
			}
			else
			{
				txt401.Text = "";
			}
			strTemp = Strings.Mid(strRec, 298, 11);
			// 403b
			dblTemp = Conversion.Val(strTemp) / 100;
			if (dblTemp > 0)
			{
				txt403.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=403";
			}
			else
			{
				txt403.Text = "";
			}
			strTemp = Strings.Mid(strRec, 309, 11);
			// 408k
			dblTemp = Conversion.Val(strTemp) / 100;
			if (dblTemp > 0)
			{
				txt408.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=408";
			}
			else
			{
				txt408.Text = "";
			}
			strTemp = Strings.Mid(strRec, 320, 11);
			// 457b
			dblTemp = Conversion.Val(Conversion.Val(FCConvert.ToInt32(strTemp) / 100));
			if (dblTemp > 0)
			{
				txt457.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=457";
			}
			else
			{
				txt457.Text = "";
			}
			strTemp = Strings.Mid(strRec, 331, 11);
			// 501c
			dblTemp = Conversion.Val(Conversion.Val(FCConvert.ToInt32(strTemp) / 100));
			if (dblTemp > 0)
			{
				txt501.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=501";
			}
			else
			{
				txt501.Text = "";
			}
		}

		private void ReadRTRecord()
		{
			// total record
			string strTemp;
			// vbPorter upgrade warning: dblTemp As string	OnWriteFCConvert.ToDouble(
			string dblTemp;
			strTemp = Strings.Mid(strRec, 3, 9);
			// number of employee records
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupCount.Text = Strings.Format(dblTemp, "0");
			lngTotalCount += FCConvert.ToInt32(dblTemp);
			strTemp = Strings.Mid(strRec, 10, 15);
			// wages, tips & other compensations
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupGrossFed.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotGrossFed += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 25, 15);
			// Federal Income Tax Withheld
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupFedTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotFedTax += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 40, 15);
			// Social Security Wages
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupGrossSocSec.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotGrossSocSec += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 55, 15);
			// Social Security Tax Withheld
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupSocTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotSocTax += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 70, 15);
			// Medicare Wages and Tips
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupGrossMed.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotGrossMed += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 85, 15);
			// Medicare Tax Withheld
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupMedTax.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotMedTax += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 310, 15);
			// Employer cost of premiums for group term life over 50000
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroupBox12L.Text = Strings.Format(dblTemp, "#,###,##0.00");
			dblTotBox12L += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 145, 15);
			// 401k
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroup401.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=401";
			dblTot401 += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 160, 15);
			// 403b
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroup403.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=403";
			dblTot403 += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 175, 15);
			// 408k
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroup408.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=408";
			dblTot408 += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 190, 15);
			// 457b
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroup457.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=457";
			dblTot457 += FCConvert.ToDouble(dblTemp);
			strTemp = Strings.Mid(strRec, 205, 15);
			// 501
			dblTemp = FCConvert.ToString(Conversion.Val(strTemp) / 100);
			txtGroup501.Text = Strings.Format(dblTemp, "#,###,##0.00") + "=501";
			dblTot501 += FCConvert.ToDouble(dblTemp);
		}

		private void ShowTotals()
		{
			// total record
			txtFinalCount.Text = Strings.Format(lngTotalCount, "0");
			txtTotGrossFed.Text = Strings.Format(dblTotGrossFed, "#,###,##0.00");
			txtTotFedTax.Text = Strings.Format(dblTotFedTax, "#,###,##0.00");
			txtTotGrossSocSec.Text = Strings.Format(dblTotGrossSocSec, "#,###,##0.00");
			txtTotGrossMed.Text = Strings.Format(dblTotGrossMed, "#,###,##0.00");
			txtTotSocTax.Text = Strings.Format(dblTotSocTax, "#,###,##0.00");
			txtTotMedTax.Text = Strings.Format(dblTotMedTax, "#,###,##0.00");
			txtTotBox12L.Text = Strings.Format(dblTotBox12L, "#,###,##0.00");
			txtTot401.Text = Strings.Format(dblTot401, "#,###,##0.00") + "=401";
			txtTot403.Text = Strings.Format(dblTot403, "#,###,##0.00") + "=403";
			txtTot408.Text = Strings.Format(dblTot408, "#,###,##0.00") + "=408";
			txtTot457.Text = Strings.Format(dblTot457, "#,###,##0.00") + "=457";
			txtTot501.Text = Strings.Format(dblTot501, "#,###,##0.00") + "=501";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// ReadRTRecord
			ShowTotals();
			ts.Close();
			boolFileOpen = false;
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
