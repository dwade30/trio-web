//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPayrollProcessSetup.
	/// </summary>
	partial class frmPayrollProcessSetup
	{
		public fecherFoundation.FCComboBox cmbNo;
		public fecherFoundation.FCTextBox txtFundNumber;
		public fecherFoundation.FCCheckBox chkBiWeeklyDeds;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdPeriodStartCalendar;
		public fecherFoundation.FCTextBox txtPayRun;
		public fecherFoundation.FCButton cmdCalendar2;
		public fecherFoundation.FCButton cmdCalendar1;
		public Global.T2KDateBox mebPayDate;
		public Global.T2KDateBox mebWeekEndDate;
		public Global.T2KDateBox t2kPeriodStartDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboFirstWeek;
		public fecherFoundation.FCComboBox cboWeekNumber;
		public fecherFoundation.FCLabel lblWeekMonth;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblFirstWeek;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCComboBox cboNumberPayWeeks;
		public fecherFoundation.FCLabel lblMonth;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCComboBox cboManualCheck;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCTextBox txtFirstFiscalMonth;
		public fecherFoundation.FCCheckBox chkBiWeekly;
		public fecherFoundation.FCCheckBox chkGroupID;
		public fecherFoundation.FCTextBox txtGroup;
		public fecherFoundation.FCFrame fraTrustAgency;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCLabel lblFundNumber;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbNo = new fecherFoundation.FCComboBox();
            this.txtFundNumber = new fecherFoundation.FCTextBox();
            this.chkBiWeeklyDeds = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdPeriodStartCalendar = new fecherFoundation.FCButton();
            this.txtPayRun = new fecherFoundation.FCTextBox();
            this.cmdCalendar2 = new fecherFoundation.FCButton();
            this.cmdCalendar1 = new fecherFoundation.FCButton();
            this.mebPayDate = new Global.T2KDateBox();
            this.mebWeekEndDate = new Global.T2KDateBox();
            this.t2kPeriodStartDate = new Global.T2KDateBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboFirstWeek = new fecherFoundation.FCComboBox();
            this.cboWeekNumber = new fecherFoundation.FCComboBox();
            this.lblWeekMonth = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblFirstWeek = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cboNumberPayWeeks = new fecherFoundation.FCComboBox();
            this.lblMonth = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.cboManualCheck = new fecherFoundation.FCComboBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.txtFirstFiscalMonth = new fecherFoundation.FCTextBox();
            this.chkBiWeekly = new fecherFoundation.FCCheckBox();
            this.chkGroupID = new fecherFoundation.FCCheckBox();
            this.txtGroup = new fecherFoundation.FCTextBox();
            this.fraTrustAgency = new fecherFoundation.FCFrame();
            this.lblWarning = new fecherFoundation.FCLabel();
            this.lblFundNumber = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeeklyDeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPeriodStartCalendar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebPayDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebWeekEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kPeriodStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTrustAgency)).BeginInit();
            this.fraTrustAgency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 731);
            this.BottomPanel.Size = new System.Drawing.Size(571, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.fraTrustAgency);
            this.ClientArea.Controls.Add(this.txtFundNumber);
            this.ClientArea.Controls.Add(this.chkBiWeeklyDeds);
            this.ClientArea.Controls.Add(this.txtFirstFiscalMonth);
            this.ClientArea.Controls.Add(this.chkBiWeekly);
            this.ClientArea.Controls.Add(this.chkGroupID);
            this.ClientArea.Controls.Add(this.txtGroup);
            this.ClientArea.Controls.Add(this.lblFundNumber);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Size = new System.Drawing.Size(591, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblFundNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtGroup, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkGroupID, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkBiWeekly, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFirstFiscalMonth, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkBiWeeklyDeds, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFundNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraTrustAgency, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(591, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(332, 30);
            this.HeaderText.Text = "File Setup (1 Per Pay Period)";
            // 
            // cmbNo
            // 
            this.cmbNo.Items.AddRange(new object[] {
            "Run T&A checks for group X only",
            "Run T&A checks for ALL employees",
            "Run T&A checks for weekly recipients only"});
            this.cmbNo.Location = new System.Drawing.Point(20, 30);
            this.cmbNo.Name = "cmbNo";
            this.cmbNo.Size = new System.Drawing.Size(471, 40);
            this.cmbNo.TabIndex = 38;
            // 
            // txtFundNumber
            // 
            this.txtFundNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFundNumber.Location = new System.Drawing.Point(351, 529);
            this.txtFundNumber.Name = "txtFundNumber";
            this.txtFundNumber.Size = new System.Drawing.Size(80, 40);
            this.txtFundNumber.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.txtFundNumber, "Enter single group description to run pay run on.");
            this.txtFundNumber.Visible = false;
            // 
            // chkBiWeeklyDeds
            // 
            this.chkBiWeeklyDeds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBiWeeklyDeds.Location = new System.Drawing.Point(30, 579);
            this.chkBiWeeklyDeds.Name = "chkBiWeeklyDeds";
            this.chkBiWeeklyDeds.Size = new System.Drawing.Size(268, 19);
            this.chkBiWeeklyDeds.TabIndex = 36;
            this.chkBiWeeklyDeds.Text = "Apply bi-weekly deductions / matches";
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.cmdPeriodStartCalendar);
            this.Frame2.Controls.Add(this.txtPayRun);
            this.Frame2.Controls.Add(this.cmdCalendar2);
            this.Frame2.Controls.Add(this.cmdCalendar1);
            this.Frame2.Controls.Add(this.mebPayDate);
            this.Frame2.Controls.Add(this.mebWeekEndDate);
            this.Frame2.Controls.Add(this.t2kPeriodStartDate);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label7);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(10, 79);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(544, 141);
            this.Frame2.TabIndex = 1;
            // 
            // cmdPeriodStartCalendar
            // 
            this.cmdPeriodStartCalendar.AppearanceKey = "imageButton";
            this.cmdPeriodStartCalendar.ImageSource = "icon - calendar?color=#707884";
            this.cmdPeriodStartCalendar.Location = new System.Drawing.Point(336, 1);
            this.cmdPeriodStartCalendar.Name = "cmdPeriodStartCalendar";
            this.cmdPeriodStartCalendar.Size = new System.Drawing.Size(40, 40);
            this.cmdPeriodStartCalendar.TabIndex = 2;
            this.cmdPeriodStartCalendar.Click += new System.EventHandler(this.cmdPeriodStartCalendar_Click);
            // 
            // txtPayRun
            // 
            this.txtPayRun.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayRun.Enabled = false;
            this.txtPayRun.Location = new System.Drawing.Point(481, 1);
            this.txtPayRun.LockedOriginal = true;
            this.txtPayRun.Name = "txtPayRun";
            this.txtPayRun.ReadOnly = true;
            this.txtPayRun.Size = new System.Drawing.Size(50, 40);
            this.txtPayRun.TabIndex = 22;
            // 
            // cmdCalendar2
            // 
            this.cmdCalendar2.AppearanceKey = "imageButton";
            this.cmdCalendar2.ImageSource = "icon - calendar?color=#707884";
            this.cmdCalendar2.Location = new System.Drawing.Point(336, 101);
            this.cmdCalendar2.Name = "cmdCalendar2";
            this.cmdCalendar2.Size = new System.Drawing.Size(40, 40);
            this.cmdCalendar2.TabIndex = 6;
            this.cmdCalendar2.Click += new System.EventHandler(this.cmdCalendar2_Click);
            // 
            // cmdCalendar1
            // 
            this.cmdCalendar1.AppearanceKey = "imageButton";
            this.cmdCalendar1.ImageSource = "icon - calendar?color=#707884";
            this.cmdCalendar1.Location = new System.Drawing.Point(336, 51);
            this.cmdCalendar1.Name = "cmdCalendar1";
            this.cmdCalendar1.Size = new System.Drawing.Size(40, 40);
            this.cmdCalendar1.TabIndex = 4;
            this.cmdCalendar1.Click += new System.EventHandler(this.cmdCalendar1_Click);
            // 
            // mebPayDate
            // 
            this.mebPayDate.Location = new System.Drawing.Point(204, 101);
            this.mebPayDate.Name = "mebPayDate";
            this.mebPayDate.Size = new System.Drawing.Size(115, 22);
            this.mebPayDate.TabIndex = 5;
            this.mebPayDate.TextChanged += new System.EventHandler(this.mebPayDate_Change);
            this.mebPayDate.Validating += new System.ComponentModel.CancelEventHandler(this.mebPayDate_Validate);
            // 
            // mebWeekEndDate
            // 
            this.mebWeekEndDate.Location = new System.Drawing.Point(204, 51);
            this.mebWeekEndDate.Name = "mebWeekEndDate";
            this.mebWeekEndDate.Size = new System.Drawing.Size(115, 22);
            this.mebWeekEndDate.TabIndex = 3;
            // 
            // t2kPeriodStartDate
            // 
            this.t2kPeriodStartDate.Location = new System.Drawing.Point(204, 1);
            this.t2kPeriodStartDate.Name = "t2kPeriodStartDate";
            this.t2kPeriodStartDate.Size = new System.Drawing.Size(115, 22);
            this.t2kPeriodStartDate.TabIndex = 1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 15);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(130, 16);
            this.Label2.TabIndex = 40;
            this.Label2.Text = "PERIOD START DATE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(396, 15);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 18);
            this.Label7.TabIndex = 23;
            this.Label7.Text = "PAY RUN";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 117);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(130, 16);
            this.Label4.TabIndex = 14;
            this.Label4.Text = "PAY DATE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 65);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(130, 16);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "WEEK ENDING DATE";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.cboFirstWeek);
            this.Frame1.Controls.Add(this.cboWeekNumber);
            this.Frame1.Controls.Add(this.lblWeekMonth);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.lblFirstWeek);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(10, 229);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(531, 90);
            this.Frame1.TabIndex = 9;
            // 
            // cboFirstWeek
            // 
            this.cboFirstWeek.BackColor = System.Drawing.SystemColors.Window;
            this.cboFirstWeek.Items.AddRange(new object[] {
            "Regular Month",
            "Quarter",
            "Fiscal Year",
            "Calendar year",
            "Both Fiscal and Calendar",
            "Both Quarter and Fiscal",
            " "});
            this.cboFirstWeek.Location = new System.Drawing.Point(204, 50);
            this.cboFirstWeek.Name = "cboFirstWeek";
            this.cboFirstWeek.Size = new System.Drawing.Size(327, 40);
            this.cboFirstWeek.TabIndex = 7;
            this.cboFirstWeek.Visible = false;
            this.cboFirstWeek.DropDown += new System.EventHandler(this.cboFirstWeek_DropDown);
            // 
            // cboWeekNumber
            // 
            this.cboWeekNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cboWeekNumber.Location = new System.Drawing.Point(204, 0);
            this.cboWeekNumber.Name = "cboWeekNumber";
            this.cboWeekNumber.Size = new System.Drawing.Size(171, 40);
            this.cboWeekNumber.TabIndex = 6;
            this.cboWeekNumber.SelectedIndexChanged += new System.EventHandler(this.cboWeekNumber_SelectedIndexChanged);
            // 
            // lblWeekMonth
            // 
            this.lblWeekMonth.Location = new System.Drawing.Point(433, 14);
            this.lblWeekMonth.Name = "lblWeekMonth";
            this.lblWeekMonth.Size = new System.Drawing.Size(98, 18);
            this.lblWeekMonth.TabIndex = 20;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(396, 14);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(17, 17);
            this.Label5.TabIndex = 16;
            this.Label5.Text = "OF";
            // 
            // lblFirstWeek
            // 
            this.lblFirstWeek.Location = new System.Drawing.Point(20, 64);
            this.lblFirstWeek.Name = "lblFirstWeek";
            this.lblFirstWeek.Size = new System.Drawing.Size(110, 16);
            this.lblFirstWeek.TabIndex = 11;
            this.lblFirstWeek.Text = "FIRST WEEK OF";
            this.lblFirstWeek.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 14);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(127, 17);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "PAY PERIOD NUMBER";
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupBoxNoBorders";
            this.Frame3.Controls.Add(this.cboNumberPayWeeks);
            this.Frame3.Controls.Add(this.lblMonth);
            this.Frame3.Location = new System.Drawing.Point(10, 329);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(531, 40);
            this.Frame3.TabIndex = 15;
            // 
            // cboNumberPayWeeks
            // 
            this.cboNumberPayWeeks.BackColor = System.Drawing.SystemColors.Window;
            this.cboNumberPayWeeks.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboNumberPayWeeks.Location = new System.Drawing.Point(204, 0);
            this.cboNumberPayWeeks.Name = "cboNumberPayWeeks";
            this.cboNumberPayWeeks.Size = new System.Drawing.Size(327, 40);
            this.cboNumberPayWeeks.TabIndex = 8;
            this.cboNumberPayWeeks.SelectedIndexChanged += new System.EventHandler(this.cboNumberPayWeeks_SelectedIndexChanged);
            // 
            // lblMonth
            // 
            this.lblMonth.Location = new System.Drawing.Point(20, 14);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(170, 26);
            this.lblMonth.TabIndex = 17;
            this.lblMonth.Text = "NUMBER OF PAY WEEKS IN";
            // 
            // Frame4
            // 
            this.Frame4.AppearanceKey = "groupBoxNoBorders";
            this.Frame4.Controls.Add(this.cboManualCheck);
            this.Frame4.Controls.Add(this.Label9);
            this.Frame4.Controls.Add(this.Label8);
            this.Frame4.Location = new System.Drawing.Point(10, 379);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(531, 40);
            this.Frame4.TabIndex = 24;
            // 
            // cboManualCheck
            // 
            this.cboManualCheck.BackColor = System.Drawing.SystemColors.Window;
            this.cboManualCheck.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboManualCheck.Location = new System.Drawing.Point(204, 0);
            this.cboManualCheck.Name = "cboManualCheck";
            this.cboManualCheck.Size = new System.Drawing.Size(327, 40);
            this.cboManualCheck.TabIndex = 25;
            this.cboManualCheck.SelectedIndexChanged += new System.EventHandler(this.cboManualCheck_SelectedIndexChanged);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 14);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(150, 15);
            this.Label9.TabIndex = 27;
            this.Label9.Text = "INDIVIDUAL CHECK";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(205, 21);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(98, 17);
            this.Label8.TabIndex = 26;
            // 
            // txtFirstFiscalMonth
            // 
            this.txtFirstFiscalMonth.Location = new System.Drawing.Point(214, 30);
            this.txtFirstFiscalMonth.Name = "txtFirstFiscalMonth";
            this.txtFirstFiscalMonth.Size = new System.Drawing.Size(327, 40);
            this.txtFirstFiscalMonth.TabIndex = 19;
            this.txtFirstFiscalMonth.TabStop = false;
            // 
            // chkBiWeekly
            // 
            this.chkBiWeekly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBiWeekly.Location = new System.Drawing.Point(30, 436);
            this.chkBiWeekly.Name = "chkBiWeekly";
            this.chkBiWeekly.Size = new System.Drawing.Size(230, 19);
            this.chkBiWeekly.TabIndex = 21;
            this.chkBiWeekly.Text = "Add bi-weekly pays to this week";
            // 
            // chkGroupID
            // 
            this.chkGroupID.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkGroupID.Location = new System.Drawing.Point(30, 486);
            this.chkGroupID.Name = "chkGroupID";
            this.chkGroupID.Size = new System.Drawing.Size(222, 19);
            this.chkGroupID.TabIndex = 28;
            this.chkGroupID.Text = "Run checks for selected group";
            this.chkGroupID.CheckedChanged += new System.EventHandler(this.chkGroupID_CheckedChanged);
            // 
            // txtGroup
            // 
            this.txtGroup.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroup.Location = new System.Drawing.Point(351, 479);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(80, 40);
            this.txtGroup.TabIndex = 29;
            this.ToolTip1.SetToolTip(this.txtGroup, "Enter single group description to run pay run on.");
            this.txtGroup.Visible = false;
            this.txtGroup.Validating += new System.ComponentModel.CancelEventHandler(this.txtGroup_Validating);
            // 
            // fraTrustAgency
            // 
            this.fraTrustAgency.Controls.Add(this.lblWarning);
            this.fraTrustAgency.Controls.Add(this.cmbNo);
            this.fraTrustAgency.Location = new System.Drawing.Point(30, 616);
            this.fraTrustAgency.Name = "fraTrustAgency";
            this.fraTrustAgency.Size = new System.Drawing.Size(511, 115);
            this.fraTrustAgency.TabIndex = 30;
            this.fraTrustAgency.Text = "Trust And Agency Checks";
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(20, 80);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(225, 15);
            this.lblWarning.TabIndex = 37;
            this.lblWarning.Text = "PAY RUN 1 WILL ALWAYS PRINT T & A\'S";
            // 
            // lblFundNumber
            // 
            this.lblFundNumber.Location = new System.Drawing.Point(30, 542);
            this.lblFundNumber.Name = "lblFundNumber";
            this.lblFundNumber.Size = new System.Drawing.Size(270, 15);
            this.lblFundNumber.TabIndex = 39;
            this.lblFundNumber.Text = " ENTER FUND NUMBER FOR SELECTED GROUP";
            this.lblFundNumber.Visible = false;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(150, 16);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "FIRST FISCAL MONTH IS ";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                  ";
            this.mnuSave.Visible = false;
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                 ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(239, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmPayrollProcessSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(591, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPayrollProcessSetup";
            this.Text = "File Setup (1 Per Pay Period)";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPayrollProcessSetup_Load);
            this.Activated += new System.EventHandler(this.frmPayrollProcessSetup_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPayrollProcessSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeeklyDeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPeriodStartCalendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebPayDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebWeekEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kPeriodStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTrustAgency)).EndInit();
            this.fraTrustAgency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}