//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	public partial class frmPayrollSteps : BaseForm
	{
		public frmPayrollSteps()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayrollSteps InstancePtr
		{
			get
			{
				return (frmPayrollSteps)Sys.GetInstance(typeof(frmPayrollSteps));
			}
		}

		protected frmPayrollSteps _InstancePtr = null;
		//=========================================================
		// vbPorter upgrade warning: intReportsRow As int	OnWriteFCConvert.ToInt32(
		private int intReportsRow;

		private void frmPayrollSteps_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmPayrollSteps_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPayrollSteps_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPayrollSteps properties;
			//frmPayrollSteps.ScaleWidth	= 9060;
			//frmPayrollSteps.ScaleHeight	= 7245;
			//frmPayrollSteps.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				LoadProcessGrid();
				//vsGrid.FixedCols = 0;
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
                // GRID COLUMN ZERO IS THE ID FOR
                // MAKE THE FIRST VISIBLE COLUMN A CHECK BOX
                //FC:FINAL:SGA #2763 - move next line to be first otherwise all style settings are lost when recreating the rows
                vsGrid.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
                //vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, vsGrid.Rows - 1, vsGrid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                vsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 0, 7, Color.White);
                vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 7, modGlobalConstants.Statics.TRIOCOLORBLUE);
				//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				// THIS IS TO COLOR THE REPORT LABEL ROW
				// THIS VARIABLE IS GOTTEN FROM THE LOAD PROCESS GRID FUNCTION
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intReportsRow, 0, intReportsRow, vsGrid.Cols - 1, Color.White);
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intReportsRow, 0, intReportsRow, vsGrid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORBLUE);
				vsGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				lblPayRun.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void LoadProcessGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadProcessGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				//DAO.Field FieldName = new DAO.Field();
				int intCounter;
				bool pboolPrinted = false;
				clsDRWrapper rsData = new clsDRWrapper();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				vsGrid.Clear();
				// SET THE GRID HEADINGS
				vsGrid.TextMatrix(0, 1, "Done");
				vsGrid.TextMatrix(0, 2, "Step");
				vsGrid.TextMatrix(0, 3, "UserID");
				vsGrid.TextMatrix(0, 4, "Date/Time");
				vsGrid.TextMatrix(0, 5, "");
				vsGrid.TextMatrix(0, 6, "Emp #");
				vsGrid.TextMatrix(0, 7, "Description");
				// HIDE THE COLUMN/RECORD IDENTIFIER COLUMN
				vsGrid.ColHidden(0, true);
				rsData.OpenRecordset("Select * from tblPayrollSteps where PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), modGlobalVariables.DEFAULTDATABASE);
				if (!rsData.EndOfFile())
				{
                    //FC:FINAL:AKV:#4264 - Payroll Processing - Process Status - Remove timestamp
                    //lblPayRun.Text = "Pay Date: " + rsData.Get_Fields_DateTime("PayDate") + "       Pay Run ID: " + rsData.Get_Fields("PayRunID");
                    lblPayRun.Text = "Pay Date: " + System.DateTime.Now.ToString("dd/mm/yyyy") + "       Pay Run ID: " + rsData.Get_Fields("PayRunID");
                    vsGrid.Rows += 1;
					for (x = 0; x <= (rsData.FieldsCount - 1); x++)
					{
						if (rsData.Get_FieldsIndexName(x) == "PayDate")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "PayRunID")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "EmployeeNumber")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "Description")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "DescriptionDateTime")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "MTDProcess")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "MTDProcessDateTime")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "QTDProcess")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "QTDProcessDateTime")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "YTDCalendarProcess")
						{
						}
						else if (rsData.Get_FieldsIndexName(x) == "YTDCalendarProcessDateTime")
						{
						}
						else
						{
							vsGrid.TextMatrix(vsGrid.Rows - 1, 0, FCConvert.ToString(rsData.Get_Fields("ID")));
							if (Strings.Right(rsData.Get_FieldsIndexName(x), 6) != "UserID" && Strings.Right(rsData.Get_FieldsIndexName(x), 8) != "DateTime")
							{
								vsGrid.TextMatrix(vsGrid.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))));
								vsGrid.TextMatrix(vsGrid.Rows - 1, 2, rsData.Get_FieldsIndexName(x));
                                int nr = 0;
                                bool val = false;
                                if (int.TryParse(FCConvert.ToString(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), out nr))
                                {
                                    val = (nr == 0 ? false : true);
                                }
                                else if (rsData.Get_Fields(rsData.Get_FieldsIndexName(x)) is Boolean)
                                {
                                    val = FCConvert.ToBoolean(rsData.Get_Fields(rsData.Get_FieldsIndexName(x)));
                                }                               
                               
								if (val == false && pboolPrinted == false)                               
                                {
                                    vsGrid.TextMatrix(vsGrid.Rows - 1, 6, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
									vsGrid.TextMatrix(vsGrid.Rows - 1, 7, FCConvert.ToString(rsData.Get_Fields("Description")));
									pboolPrinted = true;
								}
							}
							if (Strings.Right(rsData.Get_FieldsIndexName(x), 6) == "UserID")
							{
								vsGrid.TextMatrix(vsGrid.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))));
							}
							if (Strings.Right(rsData.Get_FieldsIndexName(x), 8) == "DateTime")
							{
								vsGrid.TextMatrix(vsGrid.Rows - 1, 4, FCConvert.ToString(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))));
								vsGrid.Rows += 1;
							}
						}
						// NEED THIS VARIABLE TO RECORD WHICH RECORD IS THE REPORTS
						// LABEL SO THAT IT CAN BE COLORED BLUE AS IS THE HEADER
						if (rsData.Get_FieldsIndexName(x) == "Reports")
							intReportsRow = (vsGrid.Rows - 1);
					}
					// x
					// For Each FieldName In rsData.AllFields
					// If FieldName.Name = "PayDate" Then
					// ElseIf FieldName.Name = "PayRunID" Then
					// ElseIf FieldName.Name = "EmployeeNumber" Then
					// ElseIf FieldName.Name = "Description" Then
					// ElseIf FieldName.Name = "DescriptionDateTime" Then
					// ElseIf FieldName.Name = "MTDProcess" Then
					// ElseIf FieldName.Name = "MTDProcessDateTime" Then
					// ElseIf FieldName.Name = "QTDProcess" Then
					// ElseIf FieldName.Name = "QTDProcessDateTime" Then
					// ElseIf FieldName.Name = "YTDCalendarProcess" Then
					// ElseIf FieldName.Name = "YTDCalendarProcessDateTime" Then
					// Else
					// .TextMatrix(.Rows - 1, 0) = rsData.Fields("ID")
					// 
					// If Right(FieldName.Name, 6) <> "UserID" And Right(FieldName.Name, 8) <> "DateTime" Then
					// .TextMatrix(.Rows - 1, 1) = rsData.Fields(FieldName.Name)
					// .TextMatrix(.Rows - 1, 2) = FieldName.Name
					// 
					// If rsData.Fields(FieldName.Name) = False And pboolPrinted = False Then
					// .TextMatrix(.Rows - 1, 6) = rsData.Fields("EmployeeNumber")
					// .TextMatrix(.Rows - 1, 7) = rsData.Fields("Description")
					// pboolPrinted = True
					// End If
					// End If
					// 
					// If Right(FieldName.Name, 6) = "UserID" Then
					// .TextMatrix(.Rows - 1, 3) = rsData.Fields(FieldName.Name)
					// End If
					// 
					// If Right(FieldName.Name, 8) = "DateTime" Then
					// .TextMatrix(.Rows - 1, 4) = rsData.Fields(FieldName.Name)
					// .Rows = .Rows + 1
					// End If
					// End If
					// 
					// NEED THIS VARIABLE TO RECORD WHICH RECORD IS THE REPORTS
					// LABEL SO THAT IT CAN BE COLORED BLUE AS IS THE HEADER
					// If FieldName.Name = "Reports" Then intReportsRow = .Rows - 1
					// Next
				}
				if (vsGrid.Rows > 1)
					vsGrid.Rows -= 1;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayrollSteps_Resize(object sender, System.EventArgs e)
		{
			// Adjust the widths of the columns to be a
			// percentage of the grid with itself
			vsGrid.ColWidth(0, vsGrid.WidthOriginal * 0);
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.06));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.11));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.21));
			vsGrid.ColWidth(5, vsGrid.WidthOriginal * 0);
			vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.29));
			//vsGrid.Height = vsGrid.Rows * vsGrid.RowHeight(0) + 60;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
