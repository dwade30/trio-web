﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptStandardDoubleStub.
	/// </summary>
	public partial class srptStandardDoubleStub : FCSectionReport
	{
		public srptStandardDoubleStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptStandardDoubleStub InstancePtr
		{
			get
			{
				return (srptStandardDoubleStub)Sys.GetInstance(typeof(srptStandardDoubleStub));
			}
		}

		protected srptStandardDoubleStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptStandardDoubleStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTMAXSTUBLINES = 23;
		private int intMaxPayLine;
		private int intMaxDedLine;
		private bool boolShowPayPeriod;
		private cEmployeeCheck eCheck;
		private cEarnedTimeService earnService = new cEarnedTimeService();

		private void CreateDynamicControls()
		{
			GrapeCity.ActiveReports.SectionReportModel.Label ctl;
			int CRow = 0;
			int x;
			float lngStartTop;
			string strTemp = "";
			if (eCheck == null)
			{
				return;
			}
			lngStartTop = txtPayDesc1.Top;
			intMaxPayLine = 1;
			intMaxDedLine = 1;
			// get the max number of pay lines and the max deduction lines
			// there won't be more since the parent report has already taken into
			// account what cnstmaxstublines is.  This can be changed to put the burden
			// on the stub but would have to be done to both stub sub reports.
			// With rptLaserCheck1.Grid
			// For x = 1 To CNSTMAXSTUBLINES
			// If Trim(.TextMatrix(x, 2)) <> vbNullString Then
			// intMaxPayLine = x + 1
			// End If
			// If Trim(.TextMatrix(x, 4)) <> vbNullString Then
			// intMaxDedLine = x + 1
			// End If
			// Next x
			// End With
			intMaxPayLine = eCheck.PayItems.ItemCount();
			if (intMaxPayLine > CNSTMAXSTUBLINES)
			{
				intMaxPayLine = CNSTMAXSTUBLINES;
			}
			intMaxDedLine = eCheck.DEDUCTIONS.ItemCount();
			if (intMaxDedLine > CNSTMAXSTUBLINES)
			{
				intMaxDedLine = CNSTMAXSTUBLINES;
			}
			// create the boxes for the pay info
			for (x = 2; x <= intMaxPayLine; x++)
			{
				CRow = x;
                strTemp = "txtPayDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtPayDesc1.Width;
				ctl.Left = txtPayDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtPayDesc1.Height;
				//strTemp = "txtPayDesc" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtPayDesc1.Font.Name, ctl.Font.Size);
                //Detail.Controls.Add(ctl);

                strTemp = "txtHours" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtHours1.Width;
				ctl.Left = txtHours1.Left;
				ctl.Height = txtHours1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = "txtHours" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtHours1.Font.Name, ctl.Font.Size);
                //Detail.Controls.Add(ctl);

                strTemp = "txtPayAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtPayAmount1.Width;
				ctl.Left = txtPayAmount1.Left;
				ctl.Height = txtPayAmount1.Height;
				ctl.Alignment = txtPayAmount1.Alignment;
				//strTemp = "txtPayAmount" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtPayAmount1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);
			}
			// x
			// put this stuff just below the last one we created
			lblTotalPay.Top = (240 * intMaxPayLine) / 1440F + lngStartTop;
			txtTotalAmount.Top = lblTotalPay.Top;
			txtTotalHours.Top = lblTotalPay.Top;
			// create the boxes for the deduction info
			lngStartTop = txtDedDesc1.Top;
			for (x = 2; x <= intMaxDedLine; x++)
			{
				CRow = x;
                strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedDesc1.Width;
				ctl.Left = txtDedDesc1.Left;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				ctl.Height = txtDedDesc1.Height;
				//strTemp = "txtDedDesc" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedDesc1.Font.Name, ctl.Font.Size);
                //Detail.Controls.Add(ctl);

                strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedAmount1.Width;
				ctl.Left = txtDedAmount1.Left;
				ctl.Height = txtDedAmount1.Height;
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = "txtDedAmount" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedAmount1.Font.Name, ctl.Font.Size);

                strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
                ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>(strTemp);
				ctl.Top = lngStartTop + ((CRow - 1) * 240) / 1440F;
				ctl.Width = txtDedYTD1.Width;
				ctl.Left = txtDedYTD1.Left;
				ctl.Height = txtDedYTD1.Height;
				ctl.Alignment = txtDedYTD1.Alignment;
				//strTemp = "txtDedYTD" + FCConvert.ToString(CRow);
				//ctl.Name = strTemp;
				Fields.Add(ctl.Name);
				// ctl.DataField = ctl.Name
				ctl.Font = new Font(txtDedYTD1.Font.Name, ctl.Font.Size);
				//Detail.Controls.Add(ctl);
			}
			// x
			// put this stuff just below the last one we created
			lblEMatch.Top = (240 * intMaxDedLine + 1) / 1440F + lngStartTop;
			txtEMatchCurrent.Top = lblEMatch.Top;
			txtEmatchYTD.Top = lblEMatch.Top;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			eCheck = (this.ParentReport as rptEmailPayrollCheck).GetCheckByIndex(this.UserData.ToString().ToIntegerValue());
			CreateDynamicControls();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            boolShowPayPeriod = (this.ParentReport as rptLaserCheck1).PrintPayPeriod;
			// Dim clsPrt As New clsReportPrinterFunctions
			// If Me.Tag <> vbNullString Then
			// Call clsPrt.SetReportFontsByTag(Me, "Text", Me.Tag)
			// Call clsPrt.SetReportFontsByTag(Me, "bold", Me.Tag, True)
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblDirectDeposit = 0;
			double dblDirectDepChk = 0;
			double dblDirectDepSav = 0;
			bool boolPrintTest = false;
			// vbPorter upgrade warning: tempTax As cTax	OnWrite(object)
			cTax tempTax;
			int intMaxPayLines;
			int intCurrentLine = 0;
			int intMaxDeductionLines;
			double dblTotalHours = 0;
			double dblTotalPayAmount = 0;
			int intMaxDepositLines;
			int lngVacID = 0;
			int lngSickID;
			cVacSickItem earnItem;
			int intMaxVacSickLines;
			intMaxPayLines = CNSTMAXSTUBLINES;
			intMaxDeductionLines = CNSTMAXSTUBLINES;
			intMaxDepositLines = 9;
			intMaxVacSickLines = 3;
			ClearFields();
			if (eCheck == null)
				return;
			if (!boolPrintTest)
			{
				// Line1.Visible = False
				cPayRun pRun;
				pRun = eCheck.PayRun;
				if (!boolShowPayPeriod)
				{
					lblCheckMessage.Text = eCheck.CheckMessage;
				}
				else
				{
					if (!(pRun == null))
					{
						if (Information.IsDate(pRun.PeriodStart) && Information.IsDate(pRun.PeriodEnd))
						{
							lblCheckMessage.Text = "Pay Period: " + Strings.Format(pRun.PeriodStart, "MM/dd/yyyy") + " to " + Strings.Format(pRun.PeriodEnd, "MM/dd/yyyy");
						}
					}
				}
				lngVacID = earnService.GetVacationID();
				lngSickID = earnService.GetSickID();
				intCurrentLine = 0;
				cPayStubItem payItem;
				double dblPayExcess = 0;
				double dblExcessHours = 0;
				dblTotalHours = 0;
				dblTotalPayAmount = 0;
				eCheck.PayItems.MoveFirst();
				while (eCheck.PayItems.IsCurrent())
				{
					payItem = (cPayStubItem)eCheck.PayItems.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalPayAmount += payItem.Amount;
					if (intCurrentLine < intMaxPayLines)
					{
						(Detail.Controls["txtPayDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = payItem.Description;
						(Detail.Controls["txtPayAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(payItem.Amount, "0.00");
						if (!payItem.IsDollars)
						{
							(Detail.Controls["txtHours" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(payItem.Hours, "0.00");
							dblTotalHours += payItem.Hours;
							if (payItem.Hours < 0)
							{
							}
							else
							{
							}
						}
						else
						{
						}
					}
					else
					{
						dblPayExcess += payItem.Amount;
						if (!payItem.IsDollars)
						{
							dblExcessHours += payItem.Hours;
							dblTotalHours += payItem.Hours;
							(Detail.Controls["txtHours" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblExcessHours, "0.00");
						}
						(Detail.Controls["txtPayAmount" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblPayExcess, "0.00");
						if (intCurrentLine == intMaxPayLines)
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = payItem.Description;
						}
						else
						{
							(Detail.Controls["txtPayDesc" + intMaxPayLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Other";
						}
					}
					eCheck.PayItems.MoveNext();
				}
				cPayStubItem dedItem;
				double dblDedExcess = 0;
				double dblDedYTDExcess = 0;
				double dblTotalDeductions = 0;
				eCheck.DEDUCTIONS.MoveFirst();
				intCurrentLine = 0;
				while (eCheck.DEDUCTIONS.IsCurrent())
				{
					dedItem = (cPayStubItem)eCheck.DEDUCTIONS.GetCurrentItem();
					intCurrentLine += 1;
					dblTotalDeductions += dedItem.Amount;
					if (intCurrentLine < intMaxDeductionLines)
					{
						(Detail.Controls["txtDedDesc" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = dedItem.Description;
						(Detail.Controls["txtDedAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dedItem.Amount, "0.00");
						(Detail.Controls["txtDedYTD" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dedItem.total, "0.00");
					}
					else
					{
						dblDedExcess += dedItem.Amount;
						dblDedYTDExcess += dedItem.total;
						(Detail.Controls["txtDedAmount" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblDedExcess, "0.00");
						(Detail.Controls["txtDedYTD" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblDedYTDExcess, "0.00");
						if (intCurrentLine == intMaxDeductionLines)
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = dedItem.Description;
						}
						else
						{
							(Detail.Controls["txtDedDesc" + intMaxDeductionLines] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Other";
						}
					}
					eCheck.DEDUCTIONS.MoveNext();
				}
				eCheck.Deposits.MoveFirst();
				intCurrentLine = 0;
				cCheckDeposit depItem;
				double dblExcessDeposit = 0;
				while (eCheck.Deposits.IsCurrent())
				{
					depItem = (cCheckDeposit)eCheck.Deposits.GetCurrentItem();
					intCurrentLine += 1;
					if (intCurrentLine < intMaxDepositLines)
					{
						(Detail.Controls["lblDeposit" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						(Detail.Controls["lblDepositAmount" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(depItem.Amount, "#,###,##0.00");
						Detail.Controls["lblDeposit" + intCurrentLine].Visible = true;
						Detail.Controls["lblDepositAmount" + intCurrentLine].Visible = true;
					}
					else
					{
						dblExcessDeposit += depItem.Amount;
						Detail.Controls["lblDeposit" + intMaxDepositLines].Visible = true;
						Detail.Controls["lblDepositAmount" + intMaxDepositLines].Visible = true;
						if (intCurrentLine == intMaxDepositLines)
						{
							(Detail.Controls["lblDeposit" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = depItem.Bank;
						}
						else
						{
							(Detail.Controls["lblDeposit" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = FCConvert.ToString(depItem.Bank == "Other");
						}
						(Detail.Controls["lblDepositAmount" + intMaxDepositLines] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(dblExcessDeposit, "#,###,##0.00");
					}
					eCheck.Deposits.MoveNext();
				}
				// With rptLaserCheck1.GridMisc
				dblDirectDepChk = eCheck.CheckingAmount;
				dblDirectDepSav = eCheck.SavingsAmount;
				dblDirectDeposit = dblDirectDepChk + dblDirectDepSav;
				txtDate.Text = "DATE " + Strings.Format(eCheck.PayRun.PayDate, "MM/dd/yyyy");
				txtCurrentGross.Text = Strings.Format(eCheck.CurrentGross, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["federal"];
				txtCurrentFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["fica"];
				txtCurrentFica.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["state"];
				txtCurrentState.Text = Strings.Format(tempTax.Tax, "0.00");
				txtCurrentDeductions.Text = Strings.Format(dblTotalDeductions, "0.00");
				// txtYTDDeds.Caption = Format(eCheck.YearToDateDeductions, "0.00")
				txtCurrentNet.Text = Strings.Format(eCheck.Net, "0.00");
				eCheck.EarnedTime.MoveFirst();
				intCurrentLine = 0;
				double dblOtherEarned = 0;
				dblOtherEarned = 0;
				while (eCheck.EarnedTime.IsCurrent())
				{
					earnItem = (cVacSickItem)eCheck.EarnedTime.GetCurrentItem();
					if (earnItem.TypeID == lngVacID)
					{
						txtVacationBalance.Text = Strings.Format(earnItem.Balance, "0.00");
					}
					else if (earnItem.TypeID == lngSickID)
					{
						txtSickBalance.Text = Strings.Format(earnItem.Balance, "0.00");
					}
					else
					{
						intCurrentLine += 1;
						if (intCurrentLine <= intMaxVacSickLines)
						{
							(Detail.Controls["lblCode" + intCurrentLine] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = earnItem.Name;
							(Detail.Controls["lblCode" + intCurrentLine + "Balance"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = Strings.Format(earnItem.Balance, "0.00");
						}
						else
						{
							dblOtherEarned += earnItem.Balance;
							txtOtherBalance.Text = Strings.Format(dblOtherEarned, "0.00");
						}
					}
					eCheck.EarnedTime.MoveNext();
				}
				txtYTDGross.Text = Strings.Format(eCheck.YearToDateGross, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfederal"];
				txtYTDFed.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdfica"];
				txtYTDFICA.Text = Strings.Format(tempTax.Tax, "0.00");
				tempTax = (cTax)eCheck.EmployeeTaxes["ytdstate"];
				txtYTDState.Text = Strings.Format(tempTax.Tax, "0.00");
				txtYTDNet.Text = Strings.Format(eCheck.YearToDateNet, "0.00");
				txtTotalHours.Text = Strings.Format(dblTotalHours, "0.00");
				txtTotalAmount.Text = Strings.Format(dblTotalPayAmount, "0.00");
				txtEMatchCurrent.Text = Strings.Format(eCheck.BenefitsTotal, "0.00");
				txtEmatchYTD.Text = Strings.Format(eCheck.YearToDateBenefitsTotal, "0.00");
				txtEmployeeNo.Text = eCheck.EmployeeNumber;
				txtName.Text = eCheck.EmployeeName;
				txtCheckNo.Text = eCheck.CheckNumber.ToString();
				if (dblDirectDeposit > 0)
				{
					// If LCase(MuniName) <> "rangeley" Then
					txtDirectDeposit.Visible = true;
					// txtDirectDepositLabel.Visible = True
					txtChkAmount.Visible = true;
					lblCheckAmount.Visible = true;
					txtDirectDepChkLabel.Visible = true;
					txtDirectDepositSavLabel.Visible = true;
					txtDirectDepSav.Visible = true;
					txtDirectDeposit.Text = Strings.Format(dblDirectDepChk, "0.00");
					txtDirectDepSav.Text = Strings.Format(dblDirectDepSav, "0.00");
					// End If
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				else
				{
					txtDirectDeposit.Visible = false;
					// txtDirectDepositLabel.Visible = False
					txtChkAmount.Visible = false;
					lblCheckAmount.Visible = false;
					txtChkAmount.Text = Strings.Format(eCheck.CheckAmount, "0.00");
				}
				if (eCheck.BasePayrate == 0)
				{
					lblPayRate.Visible = false;
					txtPayRate.Visible = false;
				}
				else
				{
					lblPayRate.Visible = true;
					txtPayRate.Visible = true;
					txtPayRate.Text = Strings.Format(eCheck.BasePayrate, "0.00");
				}
				if (fecherFoundation.Strings.Trim(eCheck.DepartmentDivision) == string.Empty)
				{
					lblDeptDiv.Visible = false;
					txtDeptDiv.Visible = false;
				}
				else
				{
					lblDeptDiv.Visible = true;
					txtDeptDiv.Visible = true;
					txtDeptDiv.Text = fecherFoundation.Strings.Trim(eCheck.DepartmentDivision);
				}
				// End With
				// Else
				// Line1.Visible = True
			}
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= 9; x++)
			{
				(Detail.Controls["lblDeposit" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				(Detail.Controls["lblDepositAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
				Detail.Controls["lblDeposit" + x].Visible = false;
				Detail.Controls["lblDepositAmount" + x].Visible = false;
			}
			// x
			for (x = 1; x <= intMaxPayLine; x++)
			{
				(Detail.Controls["txtPayDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtHours" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtPayAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			// x
			for (x = 1; x <= intMaxDedLine; x++)
			{
				(Detail.Controls["txtDedDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtDedAmount" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				(Detail.Controls["txtDedYTD" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			lblCode1.Text = "";
			lblCode2.Text = "";
			lblCode3.Text = "";
			lblCode1Balance.Text = "";
			lblCode2Balance.Text = "";
			lblCode3Balance.Text = "";
			txtVacationBalance.Text = "";
			txtSickBalance.Text = "";
			txtOtherBalance.Text = "";
		}

		
	}
}
