//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

using fecherFoundation.VisualBasicLayer;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmESHPCreateFiles.
	/// </summary>
	partial class frmESHPCreateFiles
	{
		public fecherFoundation.FCProgressBar pbPercentDone;
		public fecherFoundation.FCLabel lblUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbPercentDone = new fecherFoundation.FCProgressBar();
			this.lblUpdate = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 184);
			this.BottomPanel.Size = new System.Drawing.Size(371, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.pbPercentDone);
			this.ClientArea.Controls.Add(this.lblUpdate);
			this.ClientArea.Size = new System.Drawing.Size(371, 124);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(371, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(146, 30);
			this.HeaderText.Text = "ESP Upload";
			// 
			// pbPercentDone
			// 
			this.pbPercentDone.Location = new System.Drawing.Point(30, 80);
			this.pbPercentDone.Name = "pbPercentDone";
			this.pbPercentDone.Size = new System.Drawing.Size(310, 22);
			this.pbPercentDone.TabIndex = 0;
			// 
			// lblUpdate
			// 
			this.lblUpdate.Location = new System.Drawing.Point(30, 30);
			this.lblUpdate.Name = "lblUpdate";
			this.lblUpdate.Size = new System.Drawing.Size(310, 28);
			this.lblUpdate.TabIndex = 1;
			this.lblUpdate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(107, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(180, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save & Continue";
			// 
			// frmESHPCreateFiles
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(371, 292);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmESHPCreateFiles";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "ESP Upload";
			this.Load += new System.EventHandler(this.frmESHPCreateFiles_Load);
			this.Activated += new System.EventHandler(this.frmESHPCreateFiles_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmESHPCreateFiles_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}