//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmTempEmployeeSetup.
	/// </summary>
	partial class frmTempEmployeeSetup
	{
		public fecherFoundation.FCTextBox txtStateTaxes;
		public fecherFoundation.FCComboBox cboStateDollarPercent;
		public fecherFoundation.FCTextBox txtFederalTaxes;
		public fecherFoundation.FCComboBox cboFedDollarPercent;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid vsMatch;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid vsDeductions;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel lblEmployeeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSelectEmployee;
		public fecherFoundation.FCToolStripMenuItem mnusepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.txtStateTaxes = new fecherFoundation.FCTextBox();
            this.cboStateDollarPercent = new fecherFoundation.FCComboBox();
            this.txtFederalTaxes = new fecherFoundation.FCTextBox();
            this.cboFedDollarPercent = new fecherFoundation.FCComboBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.vsMatch = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsDeductions = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.lblEmployeeNumber = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectEmployee = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(708, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtStateTaxes);
            this.ClientArea.Controls.Add(this.cboStateDollarPercent);
            this.ClientArea.Controls.Add(this.txtFederalTaxes);
            this.ClientArea.Controls.Add(this.cboFedDollarPercent);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label18);
            this.ClientArea.Controls.Add(this.lblEmployeeNumber);
            this.ClientArea.Size = new System.Drawing.Size(708, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSelect);
            this.TopPanel.Size = new System.Drawing.Size(708, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelect, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(455, 30);
            this.HeaderText.Text = "Temporary Overrides (This pay run only)";
            // 
            // txtStateTaxes
            // 
			this.txtStateTaxes.MaxLength = 25;
            this.txtStateTaxes.AutoSize = false;
            this.txtStateTaxes.BackColor = System.Drawing.SystemColors.Window;
            this.txtStateTaxes.LinkItem = null;
            this.txtStateTaxes.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStateTaxes.LinkTopic = null;
            this.txtStateTaxes.Location = new System.Drawing.Point(320, 84);
            this.txtStateTaxes.Name = "txtStateTaxes";
            this.txtStateTaxes.Size = new System.Drawing.Size(80, 40);
            this.txtStateTaxes.TabIndex = 2;
            this.txtStateTaxes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStateTaxes_KeyPress);
            this.txtStateTaxes.Validating += new System.ComponentModel.CancelEventHandler(this.txtStateTaxes_Validating);
            // 
            // cboStateDollarPercent
            // 
            this.cboStateDollarPercent.AutoSize = false;
            this.cboStateDollarPercent.BackColor = System.Drawing.SystemColors.Window;
            this.cboStateDollarPercent.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboStateDollarPercent.FormattingEnabled = true;
            this.cboStateDollarPercent.Items.AddRange(new object[] {
            "Dollar",
            "Gross Percent",
            "Net Percent"});
            this.cboStateDollarPercent.Location = new System.Drawing.Point(410, 84);
            this.cboStateDollarPercent.Name = "cboStateDollarPercent";
            this.cboStateDollarPercent.Size = new System.Drawing.Size(170, 40);
            this.cboStateDollarPercent.TabIndex = 3;
            this.cboStateDollarPercent.Tag = "STAY";
            // 
            // txtFederalTaxes
            // 
			this.txtFederalTaxes.MaxLength = 25;
            this.txtFederalTaxes.AutoSize = false;
            this.txtFederalTaxes.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalTaxes.LinkItem = null;
            this.txtFederalTaxes.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFederalTaxes.LinkTopic = null;
            this.txtFederalTaxes.Location = new System.Drawing.Point(30, 84);
            this.txtFederalTaxes.Name = "txtFederalTaxes";
            this.txtFederalTaxes.Size = new System.Drawing.Size(80, 40);
            this.txtFederalTaxes.TabIndex = 0;
            this.txtFederalTaxes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFederalTaxes_KeyPress);
            this.txtFederalTaxes.Validating += new System.ComponentModel.CancelEventHandler(this.txtFederalTaxes_Validating);
            // 
            // cboFedDollarPercent
            // 
            this.cboFedDollarPercent.AutoSize = false;
            this.cboFedDollarPercent.BackColor = System.Drawing.SystemColors.Window;
            this.cboFedDollarPercent.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFedDollarPercent.FormattingEnabled = true;
            this.cboFedDollarPercent.Items.AddRange(new object[] {
            "Dollar",
            "Gross Percent",
            "Net Percent"});
            this.cboFedDollarPercent.Location = new System.Drawing.Point(120, 84);
            this.cboFedDollarPercent.Name = "cboFedDollarPercent";
            this.cboFedDollarPercent.Size = new System.Drawing.Size(170, 40);
            this.cboFedDollarPercent.TabIndex = 1;
            this.cboFedDollarPercent.Tag = "STAY";
            // 
            // Frame2
            // 
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.vsMatch);
            this.Frame2.Location = new System.Drawing.Point(30, 444);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(648, 280);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Matches";
            // 
            // vsMatch
            // 
            this.vsMatch.AllowSelection = false;
            this.vsMatch.AllowUserToResizeColumns = false;
            this.vsMatch.AllowUserToResizeRows = false;
            this.vsMatch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMatch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsMatch.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsMatch.BackColorBkg = System.Drawing.Color.Empty;
            this.vsMatch.BackColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.BackColorSel = System.Drawing.Color.Empty;
            this.vsMatch.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsMatch.Cols = 14;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsMatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsMatch.ColumnHeadersHeight = 30;
            this.vsMatch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsMatch.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsMatch.DragIcon = null;
            this.vsMatch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsMatch.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.FrozenCols = 0;
            this.vsMatch.GridColor = System.Drawing.Color.Empty;
            this.vsMatch.GridColorFixed = System.Drawing.Color.Empty;
            this.vsMatch.Location = new System.Drawing.Point(0, 30);
            this.vsMatch.Name = "vsMatch";
            this.vsMatch.OutlineCol = 0;
            this.vsMatch.ReadOnly = true;
            this.vsMatch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsMatch.RowHeightMin = 0;
            this.vsMatch.Rows = 1;
            this.vsMatch.ScrollTipText = null;
            this.vsMatch.ShowColumnVisibilityMenu = false;
            this.vsMatch.Size = new System.Drawing.Size(648, 250);
            this.vsMatch.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsMatch.TabIndex = 5;
            this.vsMatch.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsMatch_ChangeEdit);
            this.vsMatch.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMatch_ValidateEdit);
            this.vsMatch.CurrentCellChanged += new System.EventHandler(this.vsMatch_RowColChange);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsDeductions);
            this.Frame1.Location = new System.Drawing.Point(30, 144);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(648, 280);
            this.Frame1.TabIndex = 6;
            this.Frame1.Text = "Deductions";
            // 
            // vsDeductions
            // 
            this.vsDeductions.AllowSelection = false;
            this.vsDeductions.AllowUserToResizeColumns = false;
            this.vsDeductions.AllowUserToResizeRows = false;
            this.vsDeductions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDeductions.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsDeductions.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorBkg = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.BackColorSel = System.Drawing.Color.Empty;
            this.vsDeductions.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsDeductions.Cols = 17;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsDeductions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsDeductions.ColumnHeadersHeight = 30;
            this.vsDeductions.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsDeductions.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsDeductions.DragIcon = null;
            this.vsDeductions.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsDeductions.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.FrozenCols = 0;
            this.vsDeductions.GridColor = System.Drawing.Color.Empty;
            this.vsDeductions.GridColorFixed = System.Drawing.Color.Empty;
            this.vsDeductions.Location = new System.Drawing.Point(0, 30);
            this.vsDeductions.Name = "vsDeductions";
            this.vsDeductions.OutlineCol = 0;
            this.vsDeductions.ReadOnly = true;
            this.vsDeductions.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsDeductions.RowHeightMin = 0;
            this.vsDeductions.Rows = 1;
            this.vsDeductions.ScrollTipText = null;
            this.vsDeductions.ShowColumnVisibilityMenu = false;
            this.vsDeductions.Size = new System.Drawing.Size(648, 250);
            this.vsDeductions.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsDeductions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDeductions.TabIndex = 4;
            this.vsDeductions.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsDeductions_KeyPressEdit);
            this.vsDeductions.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_ChangeEdit);
            this.vsDeductions.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDeductions_AfterEdit);
            this.vsDeductions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsDeductions_ValidateEdit);
            this.vsDeductions.CurrentCellChanged += new System.EventHandler(this.vsDeductions_RowColChange);
            this.vsDeductions.KeyUp += new Wisej.Web.KeyEventHandler(this.vsDeductions_KeyUpEvent);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(320, 57);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(170, 18);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "ADDITIONAL STATE TAXES";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(30, 57);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(170, 18);
            this.Label18.TabIndex = 9;
            this.Label18.Text = "ADDITIONAL FEDERAL TAXES";
            // 
            // lblEmployeeNumber
            // 
            this.lblEmployeeNumber.Location = new System.Drawing.Point(30, 30);
            this.lblEmployeeNumber.Name = "lblEmployeeNumber";
            this.lblEmployeeNumber.Size = new System.Drawing.Size(614, 16);
            this.lblEmployeeNumber.TabIndex = 7;
            this.lblEmployeeNumber.Tag = "Employee #";
            this.lblEmployeeNumber.Text = "EMPLOYEE #";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelectEmployee,
            this.mnusepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSelectEmployee
            // 
            this.mnuSelectEmployee.Index = 0;
            this.mnuSelectEmployee.Name = "mnuSelectEmployee";
            this.mnuSelectEmployee.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuSelectEmployee.Text = "Select Employee";
            this.mnuSelectEmployee.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // mnusepar2
            // 
            this.mnusepar2.Index = 1;
            this.mnusepar2.Name = "mnusepar2";
            this.mnusepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelect.AppearanceKey = "toolbarButton";
            this.cmdSelect.Location = new System.Drawing.Point(553, 29);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdSelect.Size = new System.Drawing.Size(120, 24);
            this.cmdSelect.TabIndex = 1;
            this.cmdSelect.Text = "Select Employee";
            this.cmdSelect.Click += new System.EventHandler(this.mnuSelectEmployee_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(309, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmTempEmployeeSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(708, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmTempEmployeeSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Temporary Overrides (This pay run only)";
            this.Load += new System.EventHandler(this.frmTempEmployeeSetup_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTempEmployeeSetup_KeyDown);
            this.Resize += new System.EventHandler(this.frmTempEmployeeSetup_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSelect;
		private FCButton cmdSave;
	}
}
