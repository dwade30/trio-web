//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayTotals.
	/// </summary>
	public partial class rptPayTotals : BaseSectionReport
	{
		public rptPayTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Pay Totals";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayTotals InstancePtr
		{
			get
			{
				return (rptPayTotals)Sys.GetInstance(typeof(rptPayTotals));
			}
		}

		protected rptPayTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRep?.Dispose();
                rsRep = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsRep = new clsDRWrapper();

		public void Init(ref string strEmp)
		{
			rsRep.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + strEmp + "'", "twpy0000.vb1");
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PayTotals");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsRep.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			subreportPayCats.Report = new srptPayTotalsPayCats();
			SubReportMisc.Report = new srptPayTotalsMisc();
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsRep.EndOfFile())
			{
				subreportPayCats.Report.UserData = rsRep.Get_Fields("employeenumber");
				SubReportMisc.Report.UserData = rsRep.Get_Fields("employeenumber");
				rsRep.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
