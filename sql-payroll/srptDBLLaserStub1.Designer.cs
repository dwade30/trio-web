﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDBLLaserStub1.
	/// </summary>
	partial class srptDBLLaserStub1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDBLLaserStub1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtEmployeeNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrentState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHours1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDedYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDFICA = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYTDNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVacationBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSickBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblEMatch = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEMatchCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmatchYTD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepChkLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepSav = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDirectDepositSavLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode1Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode2Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode3Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNo,
				this.txtName,
				this.txtCheckNo,
				this.txtPay,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.txtCurrentGross,
				this.Label10,
				this.Label11,
				this.txtCurrentDeductions,
				this.txtCurrentNet,
				this.Label18,
				this.Label19,
				this.Label20,
				this.txtCurrentFed,
				this.txtCurrentFica,
				this.txtCurrentState,
				this.txtPayDesc1,
				this.txtHours1,
				this.txtPayAmount1,
				this.txtDedDesc1,
				this.txtDedAmount1,
				this.txtDedYTD1,
				this.lblTotalPay,
				this.txtTotalHours,
				this.txtTotalAmount,
				this.Label15,
				this.Label16,
				this.txtYTDGross,
				this.Label21,
				this.txtYTDFed,
				this.Label23,
				this.txtYTDFICA,
				this.Label25,
				this.txtYTDState,
				this.Label27,
				this.txtYTDNet,
				this.Label12,
				this.Label13,
				this.Label14,
				this.txtVacationBalance,
				this.txtSickBalance,
				this.lblEMatch,
				this.txtEMatchCurrent,
				this.txtEmatchYTD,
				this.txtDirectDepositLabel,
				this.txtDirectDeposit,
				this.txtChkAmount,
				this.lblCheckAmount,
				this.lblPayRate,
				this.txtPayRate,
				this.Label29,
				this.txtOtherBalance,
				this.txtDate,
				this.txtDirectDepChkLabel,
				this.txtDirectDepSav,
				this.txtDirectDepositSavLabel,
				this.lblCheckMessage,
				this.lblCode1,
				this.lblCode2,
				this.lblCode1Balance,
				this.lblCode2Balance,
				this.lblCode3,
				this.lblCode3Balance
			});
			this.Detail.Height = 6F;
			this.Detail.Name = "Detail";
			// 
			// txtEmployeeNo
			// 
			this.txtEmployeeNo.Height = 0.1666667F;
			this.txtEmployeeNo.HyperLink = null;
			this.txtEmployeeNo.Left = 0.0625F;
			this.txtEmployeeNo.Name = "txtEmployeeNo";
			this.txtEmployeeNo.Style = "text-align: right";
			this.txtEmployeeNo.Tag = "text";
			this.txtEmployeeNo.Text = "EMPLOYEE";
			this.txtEmployeeNo.Top = 0.1666667F;
			this.txtEmployeeNo.Width = 1.1875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 1.3125F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 3F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.1666667F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 5.875F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = "CHECK";
			this.txtCheckNo.Top = 0.1666667F;
			this.txtCheckNo.Width = 1.375F;
			// 
			// txtPay
			// 
			this.txtPay.Height = 0.1666667F;
			this.txtPay.HyperLink = null;
			this.txtPay.Left = 0.0625F;
			this.txtPay.Name = "txtPay";
			this.txtPay.Style = "text-align: left";
			this.txtPay.Tag = "text";
			this.txtPay.Text = "PAY";
			this.txtPay.Top = 0.5F;
			this.txtPay.Width = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "text-align: right";
			this.Label1.Tag = "text";
			this.Label1.Text = "HOURS";
			this.Label1.Top = 0.5F;
			this.Label1.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "text-align: right";
			this.Label2.Tag = "text";
			this.Label2.Text = "AMOUNT";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.8125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "text-align: left";
			this.Label3.Tag = "text";
			this.Label3.Text = "DEDUCTIONS";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 1.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.3125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "text-align: right";
			this.Label4.Tag = "text";
			this.Label4.Text = "CURRENT";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: center";
			this.Label5.Tag = "text";
			this.Label5.Text = "--YTD--";
			this.Label5.Top = 0.5F;
			this.Label5.Width = 0.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "text-align: right";
			this.Label6.Tag = "text";
			this.Label6.Text = "CURRENT";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 0.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.9375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "text-align: left";
			this.Label7.Tag = "text";
			this.Label7.Text = "GROSS";
			this.Label7.Top = 0.6666667F;
			this.Label7.Width = 0.75F;
			// 
			// txtCurrentGross
			// 
			this.txtCurrentGross.Height = 0.1666667F;
			this.txtCurrentGross.HyperLink = null;
			this.txtCurrentGross.Left = 6.6875F;
			this.txtCurrentGross.Name = "txtCurrentGross";
			this.txtCurrentGross.Style = "text-align: right";
			this.txtCurrentGross.Tag = "text";
			this.txtCurrentGross.Text = "0.00";
			this.txtCurrentGross.Top = 0.6666667F;
			this.txtCurrentGross.Width = 0.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "text-align: left";
			this.Label10.Tag = "text";
			this.Label10.Text = "DEDS.";
			this.Label10.Top = 1.5F;
			this.Label10.Width = 0.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "text-align: left";
			this.Label11.Tag = "text";
			this.Label11.Text = "NET";
			this.Label11.Top = 1.666667F;
			this.Label11.Width = 0.5625F;
			// 
			// txtCurrentDeductions
			// 
			this.txtCurrentDeductions.Height = 0.1666667F;
			this.txtCurrentDeductions.HyperLink = null;
			this.txtCurrentDeductions.Left = 6.6875F;
			this.txtCurrentDeductions.Name = "txtCurrentDeductions";
			this.txtCurrentDeductions.Style = "text-align: right";
			this.txtCurrentDeductions.Tag = "text";
			this.txtCurrentDeductions.Text = "0.00";
			this.txtCurrentDeductions.Top = 1.5F;
			this.txtCurrentDeductions.Width = 0.75F;
			// 
			// txtCurrentNet
			// 
			this.txtCurrentNet.Height = 0.1666667F;
			this.txtCurrentNet.HyperLink = null;
			this.txtCurrentNet.Left = 6.6875F;
			this.txtCurrentNet.Name = "txtCurrentNet";
			this.txtCurrentNet.Style = "text-align: right";
			this.txtCurrentNet.Tag = "text";
			this.txtCurrentNet.Text = "0.00";
			this.txtCurrentNet.Top = 1.666667F;
			this.txtCurrentNet.Width = 0.75F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.9375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "text-align: left";
			this.Label18.Tag = "text";
			this.Label18.Text = "FED. TAX";
			this.Label18.Top = 0.8333333F;
			this.Label18.Width = 0.75F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.9375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "text-align: left";
			this.Label19.Tag = "text";
			this.Label19.Text = "FICA/MED";
			this.Label19.Top = 1F;
			this.Label19.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 5.9375F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "text-align: left";
			this.Label20.Tag = "text";
			this.Label20.Text = "STATE";
			this.Label20.Top = 1.166667F;
			this.Label20.Width = 0.75F;
			// 
			// txtCurrentFed
			// 
			this.txtCurrentFed.Height = 0.1666667F;
			this.txtCurrentFed.HyperLink = null;
			this.txtCurrentFed.Left = 6.6875F;
			this.txtCurrentFed.Name = "txtCurrentFed";
			this.txtCurrentFed.Style = "text-align: right";
			this.txtCurrentFed.Tag = "text";
			this.txtCurrentFed.Text = "0.00";
			this.txtCurrentFed.Top = 0.8333333F;
			this.txtCurrentFed.Width = 0.75F;
			// 
			// txtCurrentFica
			// 
			this.txtCurrentFica.Height = 0.1666667F;
			this.txtCurrentFica.HyperLink = null;
			this.txtCurrentFica.Left = 6.6875F;
			this.txtCurrentFica.Name = "txtCurrentFica";
			this.txtCurrentFica.Style = "text-align: right";
			this.txtCurrentFica.Tag = "text";
			this.txtCurrentFica.Text = "0.00";
			this.txtCurrentFica.Top = 1F;
			this.txtCurrentFica.Width = 0.75F;
			// 
			// txtCurrentState
			// 
			this.txtCurrentState.Height = 0.1666667F;
			this.txtCurrentState.HyperLink = null;
			this.txtCurrentState.Left = 6.6875F;
			this.txtCurrentState.Name = "txtCurrentState";
			this.txtCurrentState.Style = "text-align: right";
			this.txtCurrentState.Tag = "text";
			this.txtCurrentState.Text = "0.00";
			this.txtCurrentState.Top = 1.166667F;
			this.txtCurrentState.Width = 0.75F;
			// 
			// txtPayDesc1
			// 
			this.txtPayDesc1.Height = 0.1666667F;
			this.txtPayDesc1.HyperLink = null;
			this.txtPayDesc1.Left = 0.0625F;
			this.txtPayDesc1.Name = "txtPayDesc1";
			this.txtPayDesc1.Style = "text-align: left";
			this.txtPayDesc1.Tag = "text";
			this.txtPayDesc1.Text = null;
			this.txtPayDesc1.Top = 0.6666667F;
			this.txtPayDesc1.Width = 1F;
			// 
			// txtHours1
			// 
			this.txtHours1.Height = 0.1666667F;
			this.txtHours1.HyperLink = null;
			this.txtHours1.Left = 1.125F;
			this.txtHours1.Name = "txtHours1";
			this.txtHours1.Style = "text-align: right";
			this.txtHours1.Tag = "text";
			this.txtHours1.Text = null;
			this.txtHours1.Top = 0.6666667F;
			this.txtHours1.Width = 0.6875F;
			// 
			// txtPayAmount1
			// 
			this.txtPayAmount1.Height = 0.1666667F;
			this.txtPayAmount1.HyperLink = null;
			this.txtPayAmount1.Left = 1.875F;
			this.txtPayAmount1.Name = "txtPayAmount1";
			this.txtPayAmount1.Style = "text-align: right";
			this.txtPayAmount1.Tag = "text";
			this.txtPayAmount1.Text = null;
			this.txtPayAmount1.Top = 0.6666667F;
			this.txtPayAmount1.Width = 0.6875F;
			// 
			// txtDedDesc1
			// 
			this.txtDedDesc1.Height = 0.1666667F;
			this.txtDedDesc1.HyperLink = null;
			this.txtDedDesc1.Left = 2.8125F;
			this.txtDedDesc1.Name = "txtDedDesc1";
			this.txtDedDesc1.Style = "text-align: left";
			this.txtDedDesc1.Tag = "text";
			this.txtDedDesc1.Text = null;
			this.txtDedDesc1.Top = 0.6666667F;
			this.txtDedDesc1.Width = 1.4375F;
			// 
			// txtDedAmount1
			// 
			this.txtDedAmount1.Height = 0.1666667F;
			this.txtDedAmount1.HyperLink = null;
			this.txtDedAmount1.Left = 4.3125F;
			this.txtDedAmount1.Name = "txtDedAmount1";
			this.txtDedAmount1.Style = "text-align: right";
			this.txtDedAmount1.Tag = "text";
			this.txtDedAmount1.Text = null;
			this.txtDedAmount1.Top = 0.6666667F;
			this.txtDedAmount1.Width = 0.75F;
			// 
			// txtDedYTD1
			// 
			this.txtDedYTD1.Height = 0.1666667F;
			this.txtDedYTD1.HyperLink = null;
			this.txtDedYTD1.Left = 5.0625F;
			this.txtDedYTD1.Name = "txtDedYTD1";
			this.txtDedYTD1.Style = "text-align: right";
			this.txtDedYTD1.Tag = "text";
			this.txtDedYTD1.Text = null;
			this.txtDedYTD1.Top = 0.6666667F;
			this.txtDedYTD1.Width = 0.6875F;
			// 
			// lblTotalPay
			// 
			this.lblTotalPay.Height = 0.1666667F;
			this.lblTotalPay.HyperLink = null;
			this.lblTotalPay.Left = 0.0625F;
			this.lblTotalPay.Name = "lblTotalPay";
			this.lblTotalPay.Style = "text-align: left";
			this.lblTotalPay.Tag = "text";
			this.lblTotalPay.Text = "TOTAL";
			this.lblTotalPay.Top = 4.479167F;
			this.lblTotalPay.Width = 0.5F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1666667F;
			this.txtTotalHours.HyperLink = null;
			this.txtTotalHours.Left = 1.0625F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "text-align: right";
			this.txtTotalHours.Tag = "text";
			this.txtTotalHours.Text = "0.00";
			this.txtTotalHours.Top = 4.479167F;
			this.txtTotalHours.Width = 0.75F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.HyperLink = null;
			this.txtTotalAmount.Left = 1.875F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "text-align: right";
			this.txtTotalAmount.Tag = "text";
			this.txtTotalAmount.Text = "0.00";
			this.txtTotalAmount.Top = 4.479167F;
			this.txtTotalAmount.Width = 0.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "text-align: left";
			this.Label15.Tag = "text";
			this.Label15.Text = "YEAR TO DATE:";
			this.Label15.Top = 5.3125F;
			this.Label15.Width = 2.125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "text-align: left";
			this.Label16.Tag = "text";
			this.Label16.Text = "GROSS";
			this.Label16.Top = 5.479167F;
			this.Label16.Width = 0.5625F;
			// 
			// txtYTDGross
			// 
			this.txtYTDGross.Height = 0.1666667F;
			this.txtYTDGross.HyperLink = null;
			this.txtYTDGross.Left = 0.6875F;
			this.txtYTDGross.Name = "txtYTDGross";
			this.txtYTDGross.Style = "text-align: right";
			this.txtYTDGross.Tag = "text";
			this.txtYTDGross.Text = "0.00";
			this.txtYTDGross.Top = 5.479167F;
			this.txtYTDGross.Width = 0.8125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1666667F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "text-align: left";
			this.Label21.Tag = "text";
			this.Label21.Text = "FED";
			this.Label21.Top = 5.479167F;
			this.Label21.Width = 0.375F;
			// 
			// txtYTDFed
			// 
			this.txtYTDFed.Height = 0.1666667F;
			this.txtYTDFed.HyperLink = null;
			this.txtYTDFed.Left = 2.0625F;
			this.txtYTDFed.Name = "txtYTDFed";
			this.txtYTDFed.Style = "text-align: right";
			this.txtYTDFed.Tag = "text";
			this.txtYTDFed.Text = "0.00";
			this.txtYTDFed.Top = 5.479167F;
			this.txtYTDFed.Width = 0.75F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2.9375F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "text-align: left";
			this.Label23.Tag = "text";
			this.Label23.Text = "FICA";
			this.Label23.Top = 5.479167F;
			this.Label23.Width = 0.5625F;
			// 
			// txtYTDFICA
			// 
			this.txtYTDFICA.Height = 0.1666667F;
			this.txtYTDFICA.HyperLink = null;
			this.txtYTDFICA.Left = 3.5625F;
			this.txtYTDFICA.Name = "txtYTDFICA";
			this.txtYTDFICA.Style = "text-align: right";
			this.txtYTDFICA.Tag = "text";
			this.txtYTDFICA.Text = "0.00";
			this.txtYTDFICA.Top = 5.479167F;
			this.txtYTDFICA.Width = 0.8125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1666667F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 4.5F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "text-align: left";
			this.Label25.Tag = "text";
			this.Label25.Text = "ST";
			this.Label25.Top = 5.479167F;
			this.Label25.Width = 0.3125F;
			// 
			// txtYTDState
			// 
			this.txtYTDState.Height = 0.1666667F;
			this.txtYTDState.HyperLink = null;
			this.txtYTDState.Left = 4.875F;
			this.txtYTDState.Name = "txtYTDState";
			this.txtYTDState.Style = "text-align: right";
			this.txtYTDState.Tag = "text";
			this.txtYTDState.Text = "0.00";
			this.txtYTDState.Top = 5.479167F;
			this.txtYTDState.Width = 0.8125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1666667F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.8125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "text-align: left";
			this.Label27.Tag = "text";
			this.Label27.Text = "NET";
			this.Label27.Top = 5.479167F;
			this.Label27.Width = 0.5625F;
			// 
			// txtYTDNet
			// 
			this.txtYTDNet.Height = 0.1666667F;
			this.txtYTDNet.HyperLink = null;
			this.txtYTDNet.Left = 6.4375F;
			this.txtYTDNet.Name = "txtYTDNet";
			this.txtYTDNet.Style = "text-align: right";
			this.txtYTDNet.Tag = "text";
			this.txtYTDNet.Text = "0.00";
			this.txtYTDNet.Top = 5.479167F;
			this.txtYTDNet.Width = 0.8125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.25F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: center";
			this.Label12.Tag = "text";
			this.Label12.Text = "BALANCE";
			this.Label12.Top = 1.833333F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: left";
			this.Label13.Tag = "text";
			this.Label13.Text = "VAC.";
			this.Label13.Top = 2F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "text-align: left";
			this.Label14.Tag = "text";
			this.Label14.Text = "SICK";
			this.Label14.Top = 2.166667F;
			this.Label14.Width = 0.4375F;
			// 
			// txtVacationBalance
			// 
			this.txtVacationBalance.Height = 0.1666667F;
			this.txtVacationBalance.HyperLink = null;
			this.txtVacationBalance.Left = 6.5625F;
			this.txtVacationBalance.Name = "txtVacationBalance";
			this.txtVacationBalance.Style = "text-align: right";
			this.txtVacationBalance.Tag = "text";
			this.txtVacationBalance.Text = "0.00";
			this.txtVacationBalance.Top = 2F;
			this.txtVacationBalance.Width = 0.625F;
			// 
			// txtSickBalance
			// 
			this.txtSickBalance.Height = 0.1666667F;
			this.txtSickBalance.HyperLink = null;
			this.txtSickBalance.Left = 6.5625F;
			this.txtSickBalance.Name = "txtSickBalance";
			this.txtSickBalance.Style = "text-align: right";
			this.txtSickBalance.Tag = "text";
			this.txtSickBalance.Text = "0.00";
			this.txtSickBalance.Top = 2.166667F;
			this.txtSickBalance.Width = 0.625F;
			// 
			// lblEMatch
			// 
			this.lblEMatch.Height = 0.1666667F;
			this.lblEMatch.HyperLink = null;
			this.lblEMatch.Left = 2.8125F;
			this.lblEMatch.Name = "lblEMatch";
			this.lblEMatch.Style = "text-align: left";
			this.lblEMatch.Tag = "text";
			this.lblEMatch.Text = "E/MATCH";
			this.lblEMatch.Top = 4.46875F;
			this.lblEMatch.Width = 0.9375F;
			// 
			// txtEMatchCurrent
			// 
			this.txtEMatchCurrent.Height = 0.1666667F;
			this.txtEMatchCurrent.HyperLink = null;
			this.txtEMatchCurrent.Left = 4.3125F;
			this.txtEMatchCurrent.Name = "txtEMatchCurrent";
			this.txtEMatchCurrent.Style = "text-align: right";
			this.txtEMatchCurrent.Tag = "text";
			this.txtEMatchCurrent.Text = "0.00";
			this.txtEMatchCurrent.Top = 4.479167F;
			this.txtEMatchCurrent.Width = 0.75F;
			// 
			// txtEmatchYTD
			// 
			this.txtEmatchYTD.Height = 0.1666667F;
			this.txtEmatchYTD.HyperLink = null;
			this.txtEmatchYTD.Left = 5.0625F;
			this.txtEmatchYTD.Name = "txtEmatchYTD";
			this.txtEmatchYTD.Style = "text-align: right";
			this.txtEmatchYTD.Tag = "text";
			this.txtEmatchYTD.Text = "0.00";
			this.txtEmatchYTD.Top = 4.479167F;
			this.txtEmatchYTD.Width = 0.6875F;
			// 
			// txtDirectDepositLabel
			// 
			this.txtDirectDepositLabel.Height = 0.1666667F;
			this.txtDirectDepositLabel.HyperLink = null;
			this.txtDirectDepositLabel.Left = 0.0625F;
			this.txtDirectDepositLabel.Name = "txtDirectDepositLabel";
			this.txtDirectDepositLabel.Style = "text-align: right";
			this.txtDirectDepositLabel.Tag = "text";
			this.txtDirectDepositLabel.Text = "DIRECT DEPOSIT";
			this.txtDirectDepositLabel.Top = 4.895833F;
			this.txtDirectDepositLabel.Visible = false;
			this.txtDirectDepositLabel.Width = 1.4375F;
			// 
			// txtDirectDeposit
			// 
			this.txtDirectDeposit.Height = 0.1666667F;
			this.txtDirectDeposit.HyperLink = null;
			this.txtDirectDeposit.Left = 2F;
			this.txtDirectDeposit.Name = "txtDirectDeposit";
			this.txtDirectDeposit.Style = "text-align: right";
			this.txtDirectDeposit.Tag = "text";
			this.txtDirectDeposit.Text = "0.00";
			this.txtDirectDeposit.Top = 4.895833F;
			this.txtDirectDeposit.Visible = false;
			this.txtDirectDeposit.Width = 0.75F;
			// 
			// txtChkAmount
			// 
			this.txtChkAmount.Height = 0.1666667F;
			this.txtChkAmount.HyperLink = null;
			this.txtChkAmount.Left = 5.1875F;
			this.txtChkAmount.Name = "txtChkAmount";
			this.txtChkAmount.Style = "text-align: right";
			this.txtChkAmount.Tag = "text";
			this.txtChkAmount.Text = "0.00";
			this.txtChkAmount.Top = 4.895833F;
			this.txtChkAmount.Width = 0.6875F;
			// 
			// lblCheckAmount
			// 
			this.lblCheckAmount.Height = 0.1666667F;
			this.lblCheckAmount.HyperLink = null;
			this.lblCheckAmount.Left = 4F;
			this.lblCheckAmount.Name = "lblCheckAmount";
			this.lblCheckAmount.Style = "text-align: right";
			this.lblCheckAmount.Tag = "text";
			this.lblCheckAmount.Text = "CHK AMOUNT";
			this.lblCheckAmount.Top = 4.895833F;
			this.lblCheckAmount.Width = 1.125F;
			// 
			// lblPayRate
			// 
			this.lblPayRate.Height = 0.1666667F;
			this.lblPayRate.HyperLink = null;
			this.lblPayRate.Left = 0.0625F;
			this.lblPayRate.Name = "lblPayRate";
			this.lblPayRate.Style = "text-align: left";
			this.lblPayRate.Tag = "text";
			this.lblPayRate.Text = "PAY RATE:";
			this.lblPayRate.Top = 5.645833F;
			this.lblPayRate.Width = 0.875F;
			// 
			// txtPayRate
			// 
			this.txtPayRate.Height = 0.1666667F;
			this.txtPayRate.HyperLink = null;
			this.txtPayRate.Left = 0.9375F;
			this.txtPayRate.Name = "txtPayRate";
			this.txtPayRate.Style = "text-align: right";
			this.txtPayRate.Tag = "text";
			this.txtPayRate.Text = null;
			this.txtPayRate.Top = 5.645833F;
			this.txtPayRate.Width = 0.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1666667F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 6.0625F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "text-align: left";
			this.Label29.Tag = "text";
			this.Label29.Text = "OTHER";
			this.Label29.Top = 2.333333F;
			this.Label29.Width = 0.5F;
			// 
			// txtOtherBalance
			// 
			this.txtOtherBalance.Height = 0.1666667F;
			this.txtOtherBalance.HyperLink = null;
			this.txtOtherBalance.Left = 6.5625F;
			this.txtOtherBalance.Name = "txtOtherBalance";
			this.txtOtherBalance.Style = "text-align: right";
			this.txtOtherBalance.Tag = "text";
			this.txtOtherBalance.Text = "0.00";
			this.txtOtherBalance.Top = 2.333333F;
			this.txtOtherBalance.Width = 0.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 4.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "text";
			this.txtDate.Text = "DATE";
			this.txtDate.Top = 0.1666667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtDirectDepChkLabel
			// 
			this.txtDirectDepChkLabel.Height = 0.1666667F;
			this.txtDirectDepChkLabel.HyperLink = null;
			this.txtDirectDepChkLabel.Left = 1.5625F;
			this.txtDirectDepChkLabel.Name = "txtDirectDepChkLabel";
			this.txtDirectDepChkLabel.Style = "text-align: right";
			this.txtDirectDepChkLabel.Tag = "text";
			this.txtDirectDepChkLabel.Text = "CHK";
			this.txtDirectDepChkLabel.Top = 4.895833F;
			this.txtDirectDepChkLabel.Visible = false;
			this.txtDirectDepChkLabel.Width = 0.375F;
			// 
			// txtDirectDepSav
			// 
			this.txtDirectDepSav.Height = 0.1666667F;
			this.txtDirectDepSav.HyperLink = null;
			this.txtDirectDepSav.Left = 3.25F;
			this.txtDirectDepSav.Name = "txtDirectDepSav";
			this.txtDirectDepSav.Style = "text-align: right";
			this.txtDirectDepSav.Tag = "text";
			this.txtDirectDepSav.Text = "0.00";
			this.txtDirectDepSav.Top = 4.895833F;
			this.txtDirectDepSav.Visible = false;
			this.txtDirectDepSav.Width = 0.75F;
			// 
			// txtDirectDepositSavLabel
			// 
			this.txtDirectDepositSavLabel.Height = 0.1666667F;
			this.txtDirectDepositSavLabel.HyperLink = null;
			this.txtDirectDepositSavLabel.Left = 2.8125F;
			this.txtDirectDepositSavLabel.Name = "txtDirectDepositSavLabel";
			this.txtDirectDepositSavLabel.Style = "text-align: right";
			this.txtDirectDepositSavLabel.Tag = "text";
			this.txtDirectDepositSavLabel.Text = "SAV";
			this.txtDirectDepositSavLabel.Top = 4.895833F;
			this.txtDirectDepositSavLabel.Visible = false;
			this.txtDirectDepositSavLabel.Width = 0.375F;
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Height = 0.1666667F;
			this.lblCheckMessage.HyperLink = null;
			this.lblCheckMessage.Left = 3.854167F;
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Style = "font-size: 10pt; text-align: right";
			this.lblCheckMessage.Text = null;
			this.lblCheckMessage.Top = 5.739583F;
			this.lblCheckMessage.Width = 3.40625F;
			// 
			// lblCode1
			// 
			this.lblCode1.Height = 0.1666667F;
			this.lblCode1.HyperLink = null;
			this.lblCode1.Left = 6.0625F;
			this.lblCode1.Name = "lblCode1";
			this.lblCode1.Style = "font-size: 10pt; text-align: left";
			this.lblCode1.Tag = "text";
			this.lblCode1.Text = "Code1";
			this.lblCode1.Top = 2.489583F;
			this.lblCode1.Width = 0.46875F;
			// 
			// lblCode2
			// 
			this.lblCode2.Height = 0.1666667F;
			this.lblCode2.HyperLink = null;
			this.lblCode2.Left = 6.0625F;
			this.lblCode2.Name = "lblCode2";
			this.lblCode2.Style = "font-size: 10pt; text-align: left";
			this.lblCode2.Tag = "text";
			this.lblCode2.Text = "Code2";
			this.lblCode2.Top = 2.65625F;
			this.lblCode2.Width = 0.46875F;
			// 
			// lblCode1Balance
			// 
			this.lblCode1Balance.Height = 0.1666667F;
			this.lblCode1Balance.HyperLink = null;
			this.lblCode1Balance.Left = 6.5625F;
			this.lblCode1Balance.Name = "lblCode1Balance";
			this.lblCode1Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode1Balance.Tag = "text";
			this.lblCode1Balance.Text = "0.00";
			this.lblCode1Balance.Top = 2.489583F;
			this.lblCode1Balance.Width = 0.625F;
			// 
			// lblCode2Balance
			// 
			this.lblCode2Balance.Height = 0.1666667F;
			this.lblCode2Balance.HyperLink = null;
			this.lblCode2Balance.Left = 6.5625F;
			this.lblCode2Balance.Name = "lblCode2Balance";
			this.lblCode2Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode2Balance.Tag = "text";
			this.lblCode2Balance.Text = "0.00";
			this.lblCode2Balance.Top = 2.65625F;
			this.lblCode2Balance.Width = 0.625F;
			// 
			// lblCode3
			// 
			this.lblCode3.Height = 0.1666667F;
			this.lblCode3.HyperLink = null;
			this.lblCode3.Left = 6.0625F;
			this.lblCode3.Name = "lblCode3";
			this.lblCode3.Style = "font-size: 10pt; text-align: left";
			this.lblCode3.Tag = "text";
			this.lblCode3.Text = "Code3";
			this.lblCode3.Top = 2.822917F;
			this.lblCode3.Width = 0.5F;
			// 
			// lblCode3Balance
			// 
			this.lblCode3Balance.Height = 0.1666667F;
			this.lblCode3Balance.HyperLink = null;
			this.lblCode3Balance.Left = 6.5625F;
			this.lblCode3Balance.Name = "lblCode3Balance";
			this.lblCode3Balance.Style = "font-size: 10pt; text-align: right";
			this.lblCode3Balance.Tag = "text";
			this.lblCode3Balance.Text = "0.00";
			this.lblCode3Balance.Top = 2.822917F;
			this.lblCode3Balance.Width = 0.625F;
			// 
			// srptDBLLaserStub1
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFICA;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDState;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEMatch;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEMatchCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmatchYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtOtherBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepChkLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepSav;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositSavLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Balance;
	}
}
