﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptWarrant.
	/// </summary>
	partial class rptWarrant
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarrant));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWarrant = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReprint = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldListedCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldWarrantSignature1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtIntoAP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalPayroll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalDD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFromAP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDirectDeposit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldListedCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIntoAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPayroll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheckAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFromAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPayee,
				this.fldCheck,
				this.fldNetPay,
				this.fldDirectDeposit,
				this.fldCheckAmount,
				this.txtGrossPay
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.CanShrink = true;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label13,
				this.Line1,
				this.lblPayDate,
				this.lblWarrant,
				this.lblReprint,
				this.Label9,
				this.Label10,
				this.Label21,
				this.Label22,
				this.Label27
			});
			this.PageHeader.Height = 1.15625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.Label17,
				this.Label18,
				this.fldListedCount,
				this.Line5,
				this.fldWarrantSignature1,
				this.fldWarrantSignature2,
				this.fldWarrantSignature3,
				this.fldWarrantSignature4,
				this.fldWarrantSignature5,
				this.fldWarrantSignature6,
				this.fldWarrantSignature7,
				this.fldWarrantSignature8,
				this.fldWarrantSignature9,
				this.fldWarrantSignature10,
				this.Label19,
				this.txtIntoAP,
				this.Label20,
				this.txtTotalPayroll,
				this.Line6,
				this.Label23,
				this.fldTotalDD,
				this.fldTotalCheckAmount,
				this.fldTotalNetPay,
				this.Label24,
				this.txtFromAP,
				this.fldTotalGrossPay
			});
			this.GroupFooter1.Height = 3.708333F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Payroll Warrant";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.933333F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label13.Text = "Employee";
			this.Label13.Top = 0.9333333F;
			this.Label13.Width = 1.833333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.06666667F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.133333F;
			this.Line1.Width = 7.466667F;
			this.Line1.X1 = 0.06666667F;
			this.Line1.X2 = 7.533333F;
			this.Line1.Y1 = 1.133333F;
			this.Line1.Y2 = 1.133333F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.1875F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = null;
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 4.6875F;
			// 
			// lblWarrant
			// 
			this.lblWarrant.Height = 0.1875F;
			this.lblWarrant.HyperLink = null;
			this.lblWarrant.Left = 1.5F;
			this.lblWarrant.Name = "lblWarrant";
			this.lblWarrant.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblWarrant.Text = null;
			this.lblWarrant.Top = 0.65625F;
			this.lblWarrant.Width = 4.6875F;
			// 
			// lblReprint
			// 
			this.lblReprint.Height = 0.1875F;
			this.lblReprint.HyperLink = null;
			this.lblReprint.Left = 1.5F;
			this.lblReprint.Name = "lblReprint";
			this.lblReprint.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblReprint.Text = "**** REPRINT ****";
			this.lblReprint.Top = 0.40625F;
			this.lblReprint.Visible = false;
			this.lblReprint.Width = 4.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.2F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.2666667F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Check";
			this.Label9.Top = 0.9333333F;
			this.Label9.Width = 0.5666667F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.2F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.8666667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "D / D";
			this.Label10.Top = 0.9333333F;
			this.Label10.Width = 0.8666667F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.2F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.733333F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label21.Text = "Check";
			this.Label21.Top = 0.9333333F;
			this.Label21.Width = 0.9333333F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1666667F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 4.770833F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label22.Text = "Amount";
			this.Label22.Top = 0.9375F;
			this.Label22.Visible = false;
			this.Label22.Width = 0.9375F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.2F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 6.4F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label27.Text = "Gross Pay";
			this.Label27.Top = 0.9333333F;
			this.Label27.Width = 0.9333333F;
			// 
			// fldPayee
			// 
			this.fldPayee.Height = 0.1666667F;
			this.fldPayee.Left = 2.9F;
			this.fldPayee.Name = "fldPayee";
			this.fldPayee.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPayee.Text = "yyyy";
			this.fldPayee.Top = 0F;
			this.fldPayee.Width = 2.6F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1666667F;
			this.fldCheck.Left = 0.1666667F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.6F;
			// 
			// fldNetPay
			// 
			this.fldNetPay.Height = 0.1666667F;
			this.fldNetPay.Left = 2.733333F;
			this.fldNetPay.Name = "fldNetPay";
			this.fldNetPay.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldNetPay.Text = "Field1";
			this.fldNetPay.Top = 0F;
			this.fldNetPay.Visible = false;
			this.fldNetPay.Width = 0.9F;
			// 
			// fldDirectDeposit
			// 
			this.fldDirectDeposit.Height = 0.1666667F;
			this.fldDirectDeposit.Left = 0.8666667F;
			this.fldDirectDeposit.Name = "fldDirectDeposit";
			this.fldDirectDeposit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDirectDeposit.Text = "Field1";
			this.fldDirectDeposit.Top = 0F;
			this.fldDirectDeposit.Width = 0.8666667F;
			// 
			// fldCheckAmount
			// 
			this.fldCheckAmount.Height = 0.1666667F;
			this.fldCheckAmount.Left = 1.733333F;
			this.fldCheckAmount.Name = "fldCheckAmount";
			this.fldCheckAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheckAmount.Text = "Field1";
			this.fldCheckAmount.Top = 0F;
			this.fldCheckAmount.Width = 0.9333333F;
			// 
			// txtGrossPay
			// 
			this.txtGrossPay.Height = 0.1666667F;
			this.txtGrossPay.Left = 6.4F;
			this.txtGrossPay.Name = "txtGrossPay";
			this.txtGrossPay.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtGrossPay.Text = "Field1";
			this.txtGrossPay.Top = 0F;
			this.txtGrossPay.Width = 0.9333333F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 3.033333F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.433333F;
			this.Line3.Width = 1.4F;
			this.Line3.X1 = 3.033333F;
			this.Line3.X2 = 4.433333F;
			this.Line3.Y1 = 1.433333F;
			this.Line3.Y2 = 1.433333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.4F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label17.Text = "Count";
			this.Label17.Top = 1.233333F;
			this.Label17.Width = 0.7333333F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.033333F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "Checks";
			this.Label18.Top = 1.466667F;
			this.Label18.Width = 0.6F;
			// 
			// fldListedCount
			// 
			this.fldListedCount.Height = 0.1666667F;
			this.fldListedCount.Left = 3.733333F;
			this.fldListedCount.Name = "fldListedCount";
			this.fldListedCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldListedCount.Text = "Field1";
			this.fldListedCount.Top = 1.466667F;
			this.fldListedCount.Width = 0.7F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.1875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0F;
			this.Line5.Width = 0.03125F;
			this.Line5.X1 = 4.1875F;
			this.Line5.X2 = 4.21875F;
			this.Line5.Y1 = 0F;
			this.Line5.Y2 = 0F;
			// 
			// fldWarrantSignature1
			// 
			this.fldWarrantSignature1.CanShrink = true;
			this.fldWarrantSignature1.Height = 0.1666667F;
			this.fldWarrantSignature1.Left = 0.3125F;
			this.fldWarrantSignature1.Name = "fldWarrantSignature1";
			this.fldWarrantSignature1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature1.Text = "Field3";
			this.fldWarrantSignature1.Top = 1.791667F;
			this.fldWarrantSignature1.Width = 6.9375F;
			// 
			// fldWarrantSignature2
			// 
			this.fldWarrantSignature2.CanShrink = true;
			this.fldWarrantSignature2.Height = 0.2083333F;
			this.fldWarrantSignature2.Left = 0.3125F;
			this.fldWarrantSignature2.Name = "fldWarrantSignature2";
			this.fldWarrantSignature2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature2.Text = "Field4";
			this.fldWarrantSignature2.Top = 1.958333F;
			this.fldWarrantSignature2.Width = 6.9375F;
			// 
			// fldWarrantSignature3
			// 
			this.fldWarrantSignature3.CanShrink = true;
			this.fldWarrantSignature3.Height = 0.1666667F;
			this.fldWarrantSignature3.Left = 0.3125F;
			this.fldWarrantSignature3.Name = "fldWarrantSignature3";
			this.fldWarrantSignature3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature3.Text = "Field5";
			this.fldWarrantSignature3.Top = 2.166667F;
			this.fldWarrantSignature3.Width = 6.9375F;
			// 
			// fldWarrantSignature4
			// 
			this.fldWarrantSignature4.CanShrink = true;
			this.fldWarrantSignature4.Height = 0.2083333F;
			this.fldWarrantSignature4.Left = 0.3125F;
			this.fldWarrantSignature4.Name = "fldWarrantSignature4";
			this.fldWarrantSignature4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature4.Text = "Field6";
			this.fldWarrantSignature4.Top = 2.333333F;
			this.fldWarrantSignature4.Width = 6.9375F;
			// 
			// fldWarrantSignature5
			// 
			this.fldWarrantSignature5.CanShrink = true;
			this.fldWarrantSignature5.Height = 0.1666667F;
			this.fldWarrantSignature5.Left = 0.3125F;
			this.fldWarrantSignature5.Name = "fldWarrantSignature5";
			this.fldWarrantSignature5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature5.Text = "Field7";
			this.fldWarrantSignature5.Top = 2.541667F;
			this.fldWarrantSignature5.Width = 6.9375F;
			// 
			// fldWarrantSignature6
			// 
			this.fldWarrantSignature6.CanShrink = true;
			this.fldWarrantSignature6.Height = 0.2083333F;
			this.fldWarrantSignature6.Left = 0.3125F;
			this.fldWarrantSignature6.Name = "fldWarrantSignature6";
			this.fldWarrantSignature6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature6.Text = "Field8";
			this.fldWarrantSignature6.Top = 2.708333F;
			this.fldWarrantSignature6.Width = 6.9375F;
			// 
			// fldWarrantSignature7
			// 
			this.fldWarrantSignature7.CanShrink = true;
			this.fldWarrantSignature7.Height = 0.1666667F;
			this.fldWarrantSignature7.Left = 0.3125F;
			this.fldWarrantSignature7.Name = "fldWarrantSignature7";
			this.fldWarrantSignature7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature7.Text = "Field9";
			this.fldWarrantSignature7.Top = 2.916667F;
			this.fldWarrantSignature7.Width = 6.9375F;
			// 
			// fldWarrantSignature8
			// 
			this.fldWarrantSignature8.CanShrink = true;
			this.fldWarrantSignature8.Height = 0.2083333F;
			this.fldWarrantSignature8.Left = 0.3125F;
			this.fldWarrantSignature8.Name = "fldWarrantSignature8";
			this.fldWarrantSignature8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature8.Text = "Field10";
			this.fldWarrantSignature8.Top = 3.083333F;
			this.fldWarrantSignature8.Width = 6.9375F;
			// 
			// fldWarrantSignature9
			// 
			this.fldWarrantSignature9.CanShrink = true;
			this.fldWarrantSignature9.Height = 0.1666667F;
			this.fldWarrantSignature9.Left = 0.3125F;
			this.fldWarrantSignature9.Name = "fldWarrantSignature9";
			this.fldWarrantSignature9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature9.Text = "Field11";
			this.fldWarrantSignature9.Top = 3.291667F;
			this.fldWarrantSignature9.Width = 6.9375F;
			// 
			// fldWarrantSignature10
			// 
			this.fldWarrantSignature10.CanShrink = true;
			this.fldWarrantSignature10.Height = 0.2083333F;
			this.fldWarrantSignature10.Left = 0.3125F;
			this.fldWarrantSignature10.Name = "fldWarrantSignature10";
			this.fldWarrantSignature10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldWarrantSignature10.Text = "Field12";
			this.fldWarrantSignature10.Top = 3.458333F;
			this.fldWarrantSignature10.Width = 6.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.2F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.1333333F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label19.Text = "Put into A/P";
			this.Label19.Top = 0.5333334F;
			this.Label19.Width = 0.8333333F;
			// 
			// txtIntoAP
			// 
			this.txtIntoAP.Height = 0.2F;
			this.txtIntoAP.Left = 1.6F;
			this.txtIntoAP.Name = "txtIntoAP";
			this.txtIntoAP.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.txtIntoAP.Text = "Field1";
			this.txtIntoAP.Top = 0.5333334F;
			this.txtIntoAP.Width = 1.066667F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.1333333F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label20.Text = "Total Payroll";
			this.Label20.Top = 1.033333F;
			this.Label20.Width = 0.8333333F;
			// 
			// txtTotalPayroll
			// 
			this.txtTotalPayroll.Height = 0.1666667F;
			this.txtTotalPayroll.Left = 1.6F;
			this.txtTotalPayroll.Name = "txtTotalPayroll";
			this.txtTotalPayroll.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.txtTotalPayroll.Text = "Field1";
			this.txtTotalPayroll.Top = 1F;
			this.txtTotalPayroll.Width = 1.066667F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 1.633333F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.9333333F;
			this.Line6.Width = 1.1F;
			this.Line6.X1 = 1.633333F;
			this.Line6.X2 = 2.733333F;
			this.Line6.Y1 = 0.9333333F;
			this.Line6.Y2 = 0.9333333F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.1666667F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Label23.Text = "Total";
			this.Label23.Top = 0.03333334F;
			this.Label23.Width = 0.5666667F;
			// 
			// fldTotalDD
			// 
			this.fldTotalDD.Height = 0.1666667F;
			this.fldTotalDD.Left = 0.8F;
			this.fldTotalDD.Name = "fldTotalDD";
			this.fldTotalDD.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalDD.Text = "Field1";
			this.fldTotalDD.Top = 0.03333334F;
			this.fldTotalDD.Width = 0.9333333F;
			// 
			// fldTotalCheckAmount
			// 
			this.fldTotalCheckAmount.Height = 0.1666667F;
			this.fldTotalCheckAmount.Left = 1.733333F;
			this.fldTotalCheckAmount.Name = "fldTotalCheckAmount";
			this.fldTotalCheckAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalCheckAmount.Text = "Field1";
			this.fldTotalCheckAmount.Top = 0.03333334F;
			this.fldTotalCheckAmount.Width = 0.9333333F;
			// 
			// fldTotalNetPay
			// 
			this.fldTotalNetPay.Height = 0.1666667F;
			this.fldTotalNetPay.Left = 2.733333F;
			this.fldTotalNetPay.Name = "fldTotalNetPay";
			this.fldTotalNetPay.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalNetPay.Text = "Field1";
			this.fldTotalNetPay.Top = 0.03333334F;
			this.fldTotalNetPay.Visible = false;
			this.fldTotalNetPay.Width = 0.9F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.2F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.1333333F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label24.Text = "Taken out of A/P";
			this.Label24.Top = 0.7333333F;
			this.Label24.Width = 1.333333F;
			// 
			// txtFromAP
			// 
			this.txtFromAP.Height = 0.2F;
			this.txtFromAP.Left = 1.633333F;
			this.txtFromAP.Name = "txtFromAP";
			this.txtFromAP.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.txtFromAP.Text = "Field1";
			this.txtFromAP.Top = 0.7333333F;
			this.txtFromAP.Width = 1.066667F;
			// 
			// fldTotalGrossPay
			// 
			this.fldTotalGrossPay.Height = 0.1666667F;
			this.fldTotalGrossPay.Left = 6.395833F;
			this.fldTotalGrossPay.Name = "fldTotalGrossPay";
			this.fldTotalGrossPay.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalGrossPay.Text = "Field1";
			this.fldTotalGrossPay.Top = 0.03125F;
			this.fldTotalGrossPay.Width = 0.9375F;
			// 
			// rptWarrant
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDirectDeposit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldListedCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIntoAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPayroll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheckAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFromAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReprint;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldListedCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIntoAP;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPayroll;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNetPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFromAP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalGrossPay;
	}
}
