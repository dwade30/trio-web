﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsACHFileInfo
	{
		//=========================================================
		private string strFileName = "";
		private string strEffectiveEntryDate = "";
		private bool boolBalanced;
		private bool boolRegular;
		private bool boolPreNote;
		private bool boolForcePreNote;
		// Private tACHSetup As New clsACHSetup
		private clsACHEmployerInfo eInfo = new clsACHEmployerInfo();
		private clsACHInfo destInfo = new clsACHInfo();
		private clsACHOriginatorInfo origInfo = new clsACHOriginatorInfo();

		public bool ForcePreNote
		{
			get
			{
				bool ForcePreNote = false;
				ForcePreNote = boolForcePreNote;
				return ForcePreNote;
			}
			set
			{
				boolForcePreNote = value;
			}
		}

		public bool Regular
		{
			set
			{
				boolRegular = value;
			}
			get
			{
				bool Regular = false;
				Regular = boolRegular;
				return Regular;
			}
		}

		public bool PreNote
		{
			set
			{
				boolPreNote = false;
			}
			get
			{
				bool PreNote = false;
				PreNote = boolPreNote;
				return PreNote;
			}
		}

		public clsACHOriginatorInfo OriginInfo()
		{
			clsACHOriginatorInfo OriginInfo = null;
			OriginInfo = origInfo;
			return OriginInfo;
		}

		public clsACHInfo DestinationInfo()
		{
			return destInfo;
		}

		public clsACHEmployerInfo EmployerInfo()
		{
			clsACHEmployerInfo EmployerInfo = null;
			EmployerInfo = eInfo;
			return EmployerInfo;
		}

		public string FileName
		{
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
			set
			{
				strFileName = value;
			}
		}

		public string EffectiveEntryDate
		{
			get
			{
				string EffectiveEntryDate = "";
				EffectiveEntryDate = strEffectiveEntryDate;
				return EffectiveEntryDate;
			}
			set
			{
				strEffectiveEntryDate = value;
			}
		}

		public bool Balanced
		{
			get
			{
				bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}
			set
			{
				boolBalanced = value;
			}
		}
	}
}
