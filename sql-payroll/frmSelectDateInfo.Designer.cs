//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmSelectDateInfo.
	/// </summary>
	partial class frmSelectDateInfo
	{
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cboDateSelect;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDateSelect;
		public fecherFoundation.FCComboBox cboDateSelect_2;
		public fecherFoundation.FCComboBox cboDateSelect_1;
		public fecherFoundation.FCComboBox cboDateSelect_0;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkAllPayRuns;
		public fecherFoundation.FCComboBox cboPayRunID;
		public fecherFoundation.FCComboBox cboPayDate;
		public fecherFoundation.FCComboBox cboPayDateYear;
		public fecherFoundation.FCLabel lblDateSelect_4;
		public fecherFoundation.FCLabel lblDateSelect_3;
		public fecherFoundation.FCLabel lblDateSelect_2;
		public fecherFoundation.FCLabel lblDateSelect_1;
		public fecherFoundation.FCLabel lblDateSelect_0;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cboDateSelect_2 = new fecherFoundation.FCComboBox();
            this.cboDateSelect_1 = new fecherFoundation.FCComboBox();
            this.cboDateSelect_0 = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkAllPayRuns = new fecherFoundation.FCCheckBox();
            this.cboPayRunID = new fecherFoundation.FCComboBox();
            this.cboPayDate = new fecherFoundation.FCComboBox();
            this.cboPayDateYear = new fecherFoundation.FCComboBox();
            this.lblDateSelect_4 = new fecherFoundation.FCLabel();
            this.lblDateSelect_3 = new fecherFoundation.FCLabel();
            this.lblDateSelect_2 = new fecherFoundation.FCLabel();
            this.lblDateSelect_1 = new fecherFoundation.FCLabel();
            this.lblDateSelect_0 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllPayRuns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 306);
            this.BottomPanel.Size = new System.Drawing.Size(372, 85);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cboDateSelect_2);
            this.ClientArea.Controls.Add(this.cboDateSelect_1);
            this.ClientArea.Controls.Add(this.cboDateSelect_0);
            this.ClientArea.Controls.Add(this.lblDateSelect_2);
            this.ClientArea.Controls.Add(this.lblDateSelect_1);
            this.ClientArea.Controls.Add(this.lblDateSelect_0);
            this.ClientArea.Size = new System.Drawing.Size(372, 246);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(372, 60);
            // 
            // cboDateSelect_2
            // 
            this.cboDateSelect_2.BackColor = System.Drawing.SystemColors.Window;
            this.cboDateSelect_2.Location = new System.Drawing.Point(188, 130);
            this.cboDateSelect_2.Name = "cboDateSelect_2";
            this.cboDateSelect_2.Size = new System.Drawing.Size(100, 40);
            this.cboDateSelect_2.TabIndex = 5;
            this.cboDateSelect_2.Visible = false;
            // 
            // cboDateSelect_1
            // 
            this.cboDateSelect_1.BackColor = System.Drawing.SystemColors.Window;
            this.cboDateSelect_1.Location = new System.Drawing.Point(188, 80);
            this.cboDateSelect_1.Name = "cboDateSelect_1";
            this.cboDateSelect_1.Size = new System.Drawing.Size(100, 40);
            this.cboDateSelect_1.TabIndex = 4;
            this.cboDateSelect_1.Visible = false;
            // 
            // cboDateSelect_0
            // 
            this.cboDateSelect_0.BackColor = System.Drawing.SystemColors.Window;
            this.cboDateSelect_0.Location = new System.Drawing.Point(188, 30);
            this.cboDateSelect_0.Name = "cboDateSelect_0";
            this.cboDateSelect_0.Size = new System.Drawing.Size(100, 40);
            this.cboDateSelect_0.TabIndex = 3;
            this.cboDateSelect_0.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkAllPayRuns);
            this.Frame1.Controls.Add(this.cboPayRunID);
            this.Frame1.Controls.Add(this.cboPayDate);
            this.Frame1.Controls.Add(this.cboPayDateYear);
            this.Frame1.Controls.Add(this.lblDateSelect_4);
            this.Frame1.Controls.Add(this.lblDateSelect_3);
            this.Frame1.Location = new System.Drawing.Point(30, 6);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(288, 238);
            this.Frame1.TabIndex = 6;
            this.Frame1.Text = "Select Pay Date";
            this.Frame1.Visible = false;
            // 
            // chkAllPayRuns
            // 
            this.chkAllPayRuns.Location = new System.Drawing.Point(20, 194);
            this.chkAllPayRuns.Name = "chkAllPayRuns";
            this.chkAllPayRuns.Size = new System.Drawing.Size(266, 27);
            this.chkAllPayRuns.TabIndex = 12;
            this.chkAllPayRuns.Text = "Print ALL Pay Runs for Pay Date";
            this.chkAllPayRuns.CheckedChanged += new System.EventHandler(this.chkAllPayRuns_CheckedChanged);
            // 
            // cboPayRunID
            // 
            this.cboPayRunID.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayRunID.Location = new System.Drawing.Point(168, 144);
            this.cboPayRunID.Name = "cboPayRunID";
            this.cboPayRunID.Size = new System.Drawing.Size(100, 40);
            this.cboPayRunID.TabIndex = 11;
            // 
            // cboPayDate
            // 
            this.cboPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDate.Location = new System.Drawing.Point(20, 144);
            this.cboPayDate.Name = "cboPayDate";
            this.cboPayDate.Size = new System.Drawing.Size(138, 40);
            this.cboPayDate.TabIndex = 9;
            this.cboPayDate.SelectedIndexChanged += new System.EventHandler(this.cboPayDate_SelectedIndexChanged);
            // 
            // cboPayDateYear
            // 
            this.cboPayDateYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDateYear.Location = new System.Drawing.Point(20, 57);
            this.cboPayDateYear.Name = "cboPayDateYear";
            this.cboPayDateYear.Size = new System.Drawing.Size(138, 40);
            this.cboPayDateYear.TabIndex = 7;
            this.cboPayDateYear.SelectedIndexChanged += new System.EventHandler(this.cboPayDateYear_SelectedIndexChanged);
            // 
            // lblDateSelect_4
            // 
            this.lblDateSelect_4.Location = new System.Drawing.Point(20, 117);
            this.lblDateSelect_4.Name = "lblDateSelect_4";
            this.lblDateSelect_4.Size = new System.Drawing.Size(185, 16);
            this.lblDateSelect_4.TabIndex = 10;
            this.lblDateSelect_4.Text = "SELECT PAY DATE AND RUN";
            // 
            // lblDateSelect_3
            // 
            this.lblDateSelect_3.Location = new System.Drawing.Point(20, 30);
            this.lblDateSelect_3.Name = "lblDateSelect_3";
            this.lblDateSelect_3.Size = new System.Drawing.Size(85, 16);
            this.lblDateSelect_3.TabIndex = 8;
            this.lblDateSelect_3.Text = "SELECT YEAR";
            // 
            // lblDateSelect_2
            // 
            this.lblDateSelect_2.Location = new System.Drawing.Point(30, 144);
            this.lblDateSelect_2.Name = "lblDateSelect_2";
            this.lblDateSelect_2.Size = new System.Drawing.Size(123, 21);
            this.lblDateSelect_2.TabIndex = 2;
            this.lblDateSelect_2.Visible = false;
            // 
            // lblDateSelect_1
            // 
            this.lblDateSelect_1.Location = new System.Drawing.Point(30, 94);
            this.lblDateSelect_1.Name = "lblDateSelect_1";
            this.lblDateSelect_1.Size = new System.Drawing.Size(123, 21);
            this.lblDateSelect_1.TabIndex = 1;
            this.lblDateSelect_1.Visible = false;
            // 
            // lblDateSelect_0
            // 
            this.lblDateSelect_0.Location = new System.Drawing.Point(30, 44);
            this.lblDateSelect_0.Name = "lblDateSelect_0";
            this.lblDateSelect_0.Size = new System.Drawing.Size(123, 21);
            this.lblDateSelect_0.TabIndex = 7;
            this.lblDateSelect_0.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(121, 22);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmSelectDateInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(372, 391);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSelectDateInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmSelectDateInfo_Load);
            this.Activated += new System.EventHandler(this.frmSelectDateInfo_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectDateInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllPayRuns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}