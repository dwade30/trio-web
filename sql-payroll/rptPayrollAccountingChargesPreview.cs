//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayrollAccountingChargesPreview.
	/// </summary>
	public partial class rptPayrollAccountingChargesPreview : BaseSectionReport
	{
		public rptPayrollAccountingChargesPreview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Payroll Accounting Charges";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayrollAccountingChargesPreview InstancePtr
		{
			get
			{
				return (rptPayrollAccountingChargesPreview)Sys.GetInstance(typeof(rptPayrollAccountingChargesPreview));
			}
		}

		protected rptPayrollAccountingChargesPreview _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayrollAccountingChargesPreview	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curEmployeeTotal As Decimal	OnWrite(int, Decimal)
		Decimal curEmployeeTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		bool blnSummaryPage;
		public DateTime datPayDate;
		public int intPayRunID;
		// vbPorter upgrade warning: intValidAcctHolder As int	OnWriteFCConvert.ToInt32(
		int intValidAcctHolder;
		public bool blnShowBadAccountLabel;
		private string strTableToUse = "";
		private bool boolFromPreview;
		private int lngBankNum;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("EmployeeBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsEmployeeInfo.MoveNext();
				eArgs.EOF = rsEmployeeInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["EmployeeBinder"].Value = rsEmployeeInfo.Get_Fields("EmployeeNumber");
			}
			else
			{
				blnSummaryPage = true;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!boolFromPreview)
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
				{
					modGlobalRoutines.UpdatePayrollStepTable("AccountingSummary");
				}
				else
				{
					modDavesSweetCode.Statics.blnReportCompleted = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			clsDRWrapper rsMaster = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnShowBadAccountLabel = false;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			modGlobalConstants.Statics.gboolPrintALLPayRuns = false;
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
			}
			else
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy");
			}
			blnFirstRecord = true;
			curEmployeeTotal = 0;
			curFinalTotal = 0;
			blnSummaryPage = false;
			rsMaster.OpenRecordset("Select * from tblDefaultInformation", modGlobalVariables.DEFAULTDATABASE);
			if (rsMaster.EndOfFile())
			{
				if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY " + strTableToUse + ".EmployeeNumber");
				}
				else
				{
					rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY " + strTableToUse + ".EmployeeNumber");
				}
			}
			else
			{
				// MATTHEW 10/26/2005 CALL ID 80543
				// Select Case rsMaster.Fields("DataEntrySequence")
				switch (rsMaster.Get_Fields_Int32("ReportSequence"))
				{
					case 0:
						{
							if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							else
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							break;
						}
					case 1:
						{
							if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY " + strTableToUse + ".EmployeeNumber");
							}
							else
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							break;
						}
					case 2:
						{
							// MATTHEW 10/26/2005 CALL ID 80543
							// rsEmployeeInfo.OpenRecordset "SELECT tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " & strtabletouse & ".EmployeeNumber as EmployeeNumber, " & strtabletouse & ".DistAccountNumber as DistAccountNumber, SUM(" & strtabletouse & ".DistGrossPay) as TotalPay FROM (" & strtabletouse & " INNER JOIN tblEmployeeMaster ON " & strtabletouse & ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" & datPayDate & "' AND PayRunID = " & intPayRunID & " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName, " & strtabletouse & ".EmployeeNumber, DistAccountNumber HAVING SUM(" & strtabletouse & ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName"
							if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							else
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.SeqNumber, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							break;
						}
					case 3:
						{
							if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.DeptDiv AS DeptDiv, tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							else
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.DeptDiv AS DeptDiv, tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY tblEmployeeMaster.DeptDiv, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName");
							}
							break;
						}
					default:
						{
							if (!modGlobalConstants.Statics.gboolPrintALLPayRuns)
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY " + strTableToUse + ".EmployeeNumber");
							}
							else
							{
								rsEmployeeInfo.OpenRecordset("SELECT tblemployeemaster.middlename as middlename,tblEmployeeMaster.LastName AS LastName, tblEmployeeMaster.FirstName AS FirstName, " + strTableToUse + ".EmployeeNumber as EmployeeNumber, " + strTableToUse + ".DistAccountNumber as DistAccountNumber, SUM(" + strTableToUse + ".DistGrossPay) as TotalPay FROM (" + strTableToUse + " INNER JOIN tblEmployeeMaster ON " + strTableToUse + ".EmployeeNumber = tblEmployeeMaster.EmployeeNumber) WHERE PayDate = '" + FCConvert.ToString(datPayDate) + "'  AND distributionrecord = 1 GROUP BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, " + strTableToUse + ".EmployeeNumber, DistAccountNumber HAVING SUM(" + strTableToUse + ".DistGrossPay) <> 0 ORDER BY " + strTableToUse + ".EmployeeNumber");
							}
							break;
						}
				}
				//end switch
			}
			rsMaster.Dispose();
			if (rsEmployeeInfo.EndOfFile() != true && rsEmployeeInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No information found for this pay date.", "No information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldAccount As object	OnWrite(string, object)
			fldAmount.Text = Strings.Format(Conversion.Val(rsEmployeeInfo.Get_Fields("TotalPay")), "#,##0.00");
			curEmployeeTotal += FCConvert.ToDecimal(Conversion.Val(rsEmployeeInfo.Get_Fields("TotalPay")));
			intValidAcctHolder = modValidateAccount.Statics.ValidAcctCheck;
			modValidateAccount.Statics.ValidAcctCheck = 3;
			if (!modValidateAccount.AccountValidate(rsEmployeeInfo.Get_Fields_String("DistAccountNumber")))
			{
				blnShowBadAccountLabel = true;
				fldAccount.Text = "**" + rsEmployeeInfo.Get_Fields_String("DistAccountNumber");
			}
			else
			{
				fldAccount.Text = rsEmployeeInfo.Get_Fields_String("DistAccountNumber");
			}
			modValidateAccount.Statics.ValidAcctCheck = intValidAcctHolder;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalTotal As object	OnWrite(string)
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
			curFinalTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployeeTotal As object	OnWrite(string)
			fldEmployeeTotal.Text = Strings.Format(curEmployeeTotal, "#,##0.00");
			curFinalTotal += curEmployeeTotal;
			curEmployeeTotal = 0;
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: SubReport1 As object	OnWrite(srptMultiFundAccountingSummaryPreview)
			// Set SubReport1 = New srptAccountingSummaryPreview
			SubReport1.Report = new srptMultiFundAccountingSummaryPreview();
			SubReport1.Report.UserData = strTableToUse + "," + FCConvert.ToString(datPayDate) + "," + FCConvert.ToString(intPayRunID) + "," + FCConvert.ToString(lngBankNum);
		}

		private void GroupFooter4_Format(object sender, EventArgs e)
		{
			if (blnShowBadAccountLabel == true)
			{
				lblBadAccount.Visible = true;
			}
			else
			{
				lblBadAccount.Visible = false;
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldEmployee As object	OnWrite(string)
			fldEmployee.Text = Strings.Format(rsEmployeeInfo.Get_Fields("EmployeeNumber"), "00") + "  " + rsEmployeeInfo.Get_Fields_String("LastName") + ", " + rsEmployeeInfo.Get_Fields_String("FirstName") + " " + rsEmployeeInfo.Get_Fields_String("MiddleName");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (blnSummaryPage)
			{
				Line2.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
			}
		}

		public void Init(bool modalDialog, bool boolPreview = true, int lngBank = 0, List<string> batchReports = null)
		{
			datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
			intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
			lngBankNum = lngBank;
			boolFromPreview = boolPreview;
			if (boolPreview)
			{
				strTableToUse = "tblTempPayProcess";
			}
			else
			{
				strTableToUse = "tblCheckDetail";
				if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
				{
					datPayDate = DateTime.Today;
					intPayRunID = 1;
					modGlobalVariables.Statics.gboolCancelSelected = false;
					frmSelectDateInfo.InstancePtr.Init2("Select Pay Date", -1, -1, -1, ref datPayDate, ref intPayRunID, false);
					if (modGlobalVariables.Statics.gboolCancelSelected)
					{
						this.Close();
						return;
					}
					if (datPayDate.ToOADate() != 0)
					{
						// do nothing
					}
					else
					{
						this.Close();
						return;
					}
				}
				else
				{
					datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
					intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
				}
				Label14.Visible = false;
			}
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "PayrollAccountingCharges", showModal: modalDialog);
			}
			else
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				this.PrintReport(false, batchReports: batchReports);
			}
			// rptPayrollAccountingChargesPreview.Show , MDIParent
		}

		private void rptPayrollAccountingChargesPreview_Load(object sender, System.EventArgs e)
		{
		}
	}
}
