﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cVacSickItem
	{
		//=========================================================
		private double dblAccrued;
		private double dblUsed;
		private string strTypeName = string.Empty;
		private int lngIDNumber;
		private double dblBalance;
		private int lngTypeID;
		private double dblCalendarAccrued;
		private double dblCalendarUsed;

		public double CalendarUsed
		{
			set
			{
				dblCalendarUsed = value;
			}
			get
			{
				double CalendarUsed = 0;
				CalendarUsed = dblCalendarUsed;
				return CalendarUsed;
			}
		}

		public double CalendarAccrued
		{
			set
			{
				dblCalendarAccrued = value;
			}
			get
			{
				double CalendarAccrued = 0;
				CalendarAccrued = dblCalendarAccrued;
				return CalendarAccrued;
			}
		}

		public int TypeID
		{
			set
			{
				lngTypeID = value;
			}
			get
			{
				int TypeID = 0;
				TypeID = lngTypeID;
				return TypeID;
			}
		}

		public double Accrued
		{
			set
			{
				dblAccrued = value;
			}
			get
			{
				double Accrued = 0;
				Accrued = dblAccrued;
				return Accrued;
			}
		}

		public double Used
		{
			set
			{
				dblUsed = value;
			}
			get
			{
				double Used = 0;
				Used = dblUsed;
				return Used;
			}
		}

		public string Name
		{
			set
			{
				strTypeName = value;
			}
			get
			{
				string Name = "";
				Name = strTypeName;
				return Name;
			}
		}

		public int ID
		{
			set
			{
				lngIDNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngIDNumber;
				return ID;
			}
		}

		public double Balance
		{
			set
			{
				dblBalance = value;
			}
			get
			{
				double Balance = 0;
				Balance = dblBalance;
				return Balance;
			}
		}
	}
}
