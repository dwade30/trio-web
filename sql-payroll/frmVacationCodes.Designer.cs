//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmVacationCodes.
	/// </summary>
	partial class frmVacationCodes
	{
		public fecherFoundation.FCListBox lstAss;
		public fecherFoundation.FCButton cmdAss;
		public fecherFoundation.FCComboBox cboType;
		public FCGrid vsVacationCodes;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblType;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuType;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuEditTypeDescription;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP4;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lstAss = new fecherFoundation.FCListBox();
            this.cmdAss = new fecherFoundation.FCButton();
            this.cboType = new fecherFoundation.FCComboBox();
            this.vsVacationCodes = new fecherFoundation.FCGrid();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblType = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuEditTypeDescription = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuType = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNewType = new fecherFoundation.FCButton();
            this.cmdNewCode = new fecherFoundation.FCButton();
            this.cmdDeleteType = new fecherFoundation.FCButton();
            this.cmdDeleteCode = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVacationCodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 603);
            this.BottomPanel.Size = new System.Drawing.Size(974, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstAss);
            this.ClientArea.Controls.Add(this.cmdAss);
            this.ClientArea.Controls.Add(this.cboType);
            this.ClientArea.Controls.Add(this.vsVacationCodes);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.cmdNew);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Size = new System.Drawing.Size(994, 606);
            this.ClientArea.Controls.SetChildIndex(this.cmdDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdNew, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdRefresh, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsVacationCodes, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdAss, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstAss, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewType);
            this.TopPanel.Controls.Add(this.cmdNewCode);
            this.TopPanel.Controls.Add(this.cmdDeleteType);
            this.TopPanel.Controls.Add(this.cmdDeleteCode);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Size = new System.Drawing.Size(994, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteType, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewType, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(319, 28);
            this.HeaderText.Text = "Vacation / Sick / Other Codes";
            // 
            // lstAss
            // 
            this.lstAss.BackColor = System.Drawing.SystemColors.Window;
            this.lstAss.Location = new System.Drawing.Point(30, 115);
            this.lstAss.Name = "lstAss";
            this.lstAss.Size = new System.Drawing.Size(264, 84);
            this.lstAss.TabIndex = 10;
            // 
            // cmdAss
            // 
            this.cmdAss.AppearanceKey = "actionButton";
            this.cmdAss.Location = new System.Drawing.Point(30, 218);
            this.cmdAss.Name = "cmdAss";
            this.cmdAss.Size = new System.Drawing.Size(244, 40);
            this.cmdAss.TabIndex = 9;
            this.cmdAss.Text = "Associate Pay Categories";
            this.cmdAss.Click += new System.EventHandler(this.cmdAss_Click);
            // 
            // cboType
            // 
            this.cboType.BackColor = System.Drawing.SystemColors.Window;
            this.cboType.Location = new System.Drawing.Point(92, 30);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(353, 40);
            this.cboType.TabIndex = 7;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.Enter += new System.EventHandler(this.cboType_Enter);
            this.cboType.TextChanged += new System.EventHandler(this.cboType_TextChanged);
            this.cboType.KeyDown += new Wisej.Web.KeyEventHandler(this.cboType_KeyDown);
            // 
            // vsVacationCodes
            // 
            this.vsVacationCodes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsVacationCodes.Cols = 10;
            this.vsVacationCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsVacationCodes.ExtendLastCol = true;
            this.vsVacationCodes.Location = new System.Drawing.Point(30, 278);
            this.vsVacationCodes.Name = "vsVacationCodes";
            this.vsVacationCodes.ReadOnly = false;
            this.vsVacationCodes.Rows = 50;
            this.vsVacationCodes.Size = new System.Drawing.Size(936, 325);
            this.vsVacationCodes.StandardTab = false;
            this.vsVacationCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsVacationCodes.TabIndex = 11;
            this.vsVacationCodes.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsVacationCodes_KeyDownEdit);
            this.vsVacationCodes.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsVacationCodes_KeyPressEdit);
            this.vsVacationCodes.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsVacationCodes_AfterEdit);
            this.vsVacationCodes.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsVacationCodes_ChangeEdit);
            this.vsVacationCodes.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsVacationCodes_MouseMoveEvent);
            this.vsVacationCodes.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsVacationCodes_ValidateEdit);
            this.vsVacationCodes.CurrentCellChanged += new System.EventHandler(this.vsVacationCodes_RowColChange);
            this.vsVacationCodes.Click += new System.EventHandler(this.vsVacationCodes_ClickEvent);
            this.vsVacationCodes.DoubleClick += new System.EventHandler(this.vsVacationCodes_DblClick);
            this.vsVacationCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVacationCodes_KeyDownEvent);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Location = new System.Drawing.Point(831, 467);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 24);
            this.cmdRefresh.TabIndex = 4;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Location = new System.Drawing.Point(719, 467);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(770, 467);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(812, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(48, 24);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(228, 19);
            this.Label1.TabIndex = 11;
            this.Label1.Text = "ASSOCIATED PAY CATEGORIES";
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(37, 19);
            this.lblType.TabIndex = 8;
            this.lblType.Text = "TYPE";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditTypeDescription});
            this.MainMenu1.Name = null;
            // 
            // mnuEditTypeDescription
            // 
            this.mnuEditTypeDescription.Index = 0;
            this.mnuEditTypeDescription.Name = "mnuEditTypeDescription";
            this.mnuEditTypeDescription.Text = "Edit Type Description";
            this.mnuEditTypeDescription.Click += new System.EventHandler(this.mnuEditTypeDescription_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuType
            // 
            this.mnuType.Index = -1;
            this.mnuType.Name = "mnuType";
            this.mnuType.Text = "New Type";
            this.mnuType.Click += new System.EventHandler(this.mnuType_Click);
            // 
            // mnuNew
            // 
            this.mnuNew.Index = -1;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Code";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Type";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = -1;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Text = "Delete Code";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                         ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit            ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP4
            // 
            this.mnuSP4.Index = -1;
            this.mnuSP4.Name = "mnuSP4";
            this.mnuSP4.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNewType
            // 
            this.cmdNewType.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewType.Location = new System.Drawing.Point(438, 29);
            this.cmdNewType.Name = "cmdNewType";
            this.cmdNewType.Size = new System.Drawing.Size(80, 24);
            this.cmdNewType.TabIndex = 1;
            this.cmdNewType.Text = "New Type";
            this.cmdNewType.Click += new System.EventHandler(this.mnuType_Click);
            // 
            // cmdNewCode
            // 
            this.cmdNewCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewCode.Location = new System.Drawing.Point(524, 29);
            this.cmdNewCode.Name = "cmdNewCode";
            this.cmdNewCode.Size = new System.Drawing.Size(82, 24);
            this.cmdNewCode.TabIndex = 2;
            this.cmdNewCode.Text = "New Code";
            this.cmdNewCode.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDeleteType
            // 
            this.cmdDeleteType.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteType.Location = new System.Drawing.Point(612, 29);
            this.cmdDeleteType.Name = "cmdDeleteType";
            this.cmdDeleteType.Size = new System.Drawing.Size(92, 24);
            this.cmdDeleteType.TabIndex = 3;
            this.cmdDeleteType.Text = "Delete Type";
            this.cmdDeleteType.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdDeleteCode
            // 
            this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteCode.Location = new System.Drawing.Point(710, 29);
            this.cmdDeleteCode.Name = "cmdDeleteCode";
            this.cmdDeleteCode.Size = new System.Drawing.Size(96, 24);
            this.cmdDeleteCode.TabIndex = 4;
            this.cmdDeleteCode.Text = "Delete Code";
            this.cmdDeleteCode.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(431, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.Location = new System.Drawing.Point(866, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(100, 24);
            this.cmdPrintPreview.TabIndex = 6;
            this.cmdPrintPreview.Text = "Print/Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmVacationCodes
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(994, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmVacationCodes";
            this.Text = "Vacation / Sick / Other Codes";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmVacationCodes_Load);
            this.Activated += new System.EventHandler(this.frmVacationCodes_Activated);
            this.Resize += new System.EventHandler(this.frmVacationCodes_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVacationCodes_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVacationCodes_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVacationCodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdNewCode;
		private FCButton cmdNewType;
		private FCButton cmdDeleteType;
		private FCButton cmdDeleteCode;
		private FCButton cmdSave;
        private FCButton cmdPrintPreview;
    }
}