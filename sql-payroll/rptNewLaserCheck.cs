﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewLaserCheck.
	/// </summary>
	public partial class rptNewLaserCheck : BaseSectionReport
	{
		public rptNewLaserCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Checks";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewLaserCheck InstancePtr
		{
			get
			{
				return (rptNewLaserCheck)Sys.GetInstance(typeof(rptNewLaserCheck));
			}
		}

		protected rptNewLaserCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewLaserCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
	}
}
