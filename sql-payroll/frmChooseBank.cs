//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmChooseBank : BaseForm
	{
		public frmChooseBank()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmChooseBank InstancePtr
		{
			get
			{
				return (frmChooseBank)Sys.GetInstance(typeof(frmChooseBank));
			}
		}

		protected frmChooseBank _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Corey Gray
		// Date         12/19/05
		// ********************************************************
		private int lngReturn;
		const int CNSTGRIDCOLNUMBER = 0;
		const int CNSTGRIDCOLBANK = 1;
		private bool boolUnload;

		public int Init()
		{
			int Init = 0;
			boolUnload = false;
			Init = 0;
			lngReturn = 0;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLNUMBER, true);
			Grid.TextMatrix(0, CNSTGRIDCOLBANK, "Bank");
		}

		private void FillGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				clsLoad.OpenRecordset("select * from banks where name <> ''", "twbd0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					while (!clsLoad.EndOfFile())
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNUMBER, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("ID"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLBANK, FCConvert.ToString(clsLoad.Get_Fields("name")));
						clsLoad.MoveNext();
					}
				}
				else
				{
					MessageBox.Show("You must set up your banks before you may continue with this process.", "No Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					boolUnload = true;
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmChooseBank_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsReportInfo = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			if (boolUnload)
			{
				Close();
				return;
			}
			// Call ForceFormToResize(Me)
		}

		private void frmChooseBank_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmChooseBank_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseBank properties;
			//frmChooseBank.FillStyle	= 0;
			//frmChooseBank.ScaleWidth	= 5880;
			//frmChooseBank.ScaleHeight	= 4635;
			//frmChooseBank.LinkTopic	= "Form2";
			//frmChooseBank.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLNUMBER))));
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNUMBER))));
				Close();
			}
			else
			{
				MessageBox.Show("You must choose a bank before continuing", "No Bank Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}      
    }
}
