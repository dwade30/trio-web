//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeePayTotals.
	/// </summary>
	public partial class srptEmployeePayTotals : FCSectionReport
	{
		public srptEmployeePayTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "sub Employee Deduction Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptEmployeePayTotals InstancePtr
		{
			get
			{
				return (srptEmployeePayTotals)Sys.GetInstance(typeof(srptEmployeePayTotals));
			}
		}

		protected srptEmployeePayTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsProcessing?.Dispose();
                rsProcessing = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptEmployeePayTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// private local variables
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		private int intCounter;
		private int intMaxCats;
		
		private string strEmployee = "";
		private double dblCurrent;
		private double dblMTD;
		private double dblQTD;
		private double dblFYTD;
		private double dblCYTD;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType = "";
				string strAmount = "";
				double dblTemp;
				if (rsProcessing.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				txtDescription.Text = rsProcessing.Get_Fields_String("description");
				dblTemp = modCoreysSweeterCode.GetCurrentPayTotal(rsProcessing.Get_Fields("ID"), ref strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtCurrent.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblCurrent += dblTemp;
				dblTemp = modCoreysSweeterCode.GetMTDPayTotal(rsProcessing.Get_Fields("ID"), ref strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtMTD.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblMTD += dblTemp;
				dblTemp = modCoreysSweeterCode.GetQTDPayTotal(rsProcessing.Get_Fields("ID"), ref strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtQTD.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblQTD += dblTemp;
				dblTemp = modCoreysSweeterCode.GetFYTDPayTotal(rsProcessing.Get_Fields("ID"), ref strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtFiscal.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblFYTD += dblTemp;
				dblTemp = modCoreysSweeterCode.GetCYTDPayTotal(rsProcessing.Get_Fields("ID"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
				txtCalendar.Text = Strings.Format(dblTemp, "$#,##0.00");
				dblCYTD += dblTemp;
				rsProcessing.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			dblCurrent = 0;
			dblMTD = 0;
			dblQTD = 0;
			dblCYTD = 0;
			dblFYTD = 0;
			strEmployee = FCConvert.ToString(this.UserData);
			rsProcessing.OpenRecordset("select * from tblpaycategories where not left(isnull(type, '   '),3) = 'Non' order by ID", "twpy0000.vb1");
			intCounter = 0;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			double dblTemp;
			txtTotalCalendar.Text = Strings.Format(dblCYTD, "$#,##0.00");
			txtTotalCurrent.Text = Strings.Format(dblCurrent, "$#,##0.00");
			txtTotalFYTD.Text = Strings.Format(dblFYTD, "$#,##0.00");
			txtTotalMTD.Text = Strings.Format(dblMTD, "$#,##0.00");
			txtTotalQTD.Text = Strings.Format(dblQTD, "$#,##0.00");
			txtTotalCalendarPaid.Text = Strings.Format(dblCYTD, "$#,##0.00");
			txtTotalCurrentPaid.Text = Strings.Format(dblCurrent, "$#,##0.00");
			txtTotalFYTDPaid.Text = Strings.Format(dblFYTD, "$#,##0.00");
			txtTotalMTDPaid.Text = Strings.Format(dblMTD, "$#,##0.00");
			txtTotalQTDPaid.Text = Strings.Format(dblQTD, "$#,##0.00");
			// taxwithheld
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentFedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentFicaWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentMedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentStateWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDFedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDFicaWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDMedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDStateWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDFedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDFicaWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDMedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDStateWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDFedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDFicaWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDMedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDStateWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDFedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDFicaWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDMedWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDStateWH.Text = Strings.Format(dblTemp, "$#,##0.00");
			// gross
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentFedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentFicaGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentMedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCurrentTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCurrentStateGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDFedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDFicaGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDMedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetMTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMTDStateGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDFedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDFicaGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDMedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetQTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtQTDStateGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDFedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDFicaGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDMedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetFYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFYTDStateGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDFedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDFicaGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDMedGross.Text = Strings.Format(dblTemp, "$#,##0.00");
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate);
			txtCYTDStateGross.Text = Strings.Format(dblTemp, "$#,##0.00");
		}
	}
}
