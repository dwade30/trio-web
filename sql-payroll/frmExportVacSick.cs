//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmExportVacSick : BaseForm
	{
		public frmExportVacSick()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmExportVacSick InstancePtr
		{
			get
			{
				return (frmExportVacSick)Sys.GetInstance(typeof(frmExportVacSick));
			}
		}

		protected frmExportVacSick _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Enter the file to export to";
			//FC:FINAL:DDU:#i2313 - download to client the file instead of showing ShowSave
			//if (fecherFoundation.Strings.Trim(txtExportpath.Text) == "")
			//{
			MDIParent.InstancePtr.CommonDialog1.FileName = "VacSickExport.csv";
			txtExportpath.Text = FCFileSystem.CurDir() + "\\" + MDIParent.InstancePtr.CommonDialog1.FileName;
			//}
			//else
			//{
			//MDIParent.InstancePtr.CommonDialog1.FileName = txtExportpath.Text;
			//}
			//MDIParent.InstancePtr.CommonDialog1.Filter = "CSV|*.csv;All|*.*";
			//MDIParent.InstancePtr.CommonDialog1.ShowSave();
			//txtExportpath.Text = MDIParent.InstancePtr.CommonDialog1.FileName;
		}

		private void frmExportVacSick_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExportVacSick_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExportVacSick properties;
			//frmExportVacSick.FillStyle	= 0;
			//frmExportVacSick.ScaleWidth	= 9300;
			//frmExportVacSick.ScaleHeight	= 7650;
			//frmExportVacSick.LinkTopic	= "Form2";
			//frmExportVacSick.LockControls	= -1  'True;
			//frmExportVacSick.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillLists();
			//FC:FINAL:DSE:#i2315 Export fill will download instead of save locally
			txtExportpath.Text = System.IO.Path.Combine(FCFileSystem.Statics.UserDataFolder, "VacSickExport.csv");
			this.Frame2.Visible = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillLists()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsDept = new clsDRWrapper();
			rsLoad.OpenRecordset("select department from tblemployeemaster where status <> 'Terminated' AND status <> 'Retired' group by department order by department", "twpy0000.vb1");
			if (modGlobalConstants.Statics.gboolBD)
			{
				rsDept.OpenRecordset("select * from deptdivtitles where convert(int, isnull(division, 0)) = 0 order by division", "twbd0000.vb1");
			}
			lstDepartment.Clear();
			while (!rsLoad.EndOfFile())
			{
				if (modGlobalConstants.Statics.gboolBD)
				{
					if (rsDept.FindFirstRecord("department", rsLoad.Get_Fields("department")))
					{
						lstDepartment.AddItem(rsDept.Get_Fields_String("LongDescription"));
						lstDepartment.ItemData(lstDepartment.NewIndex, FCConvert.ToInt32(rsDept.Get_Fields("ID")));
					}
					else
					{
						lstDepartment.AddItem(FCConvert.ToString(rsLoad.Get_Fields("department")));
						lstDepartment.ItemData(lstDepartment.NewIndex, 0);
					}
				}
				else
				{
					lstDepartment.AddItem(FCConvert.ToString(rsLoad.Get_Fields("department")));
					lstDepartment.ItemData(lstDepartment.NewIndex, 0);
				}
				lstDepartment.SetSelected(lstDepartment.NewIndex, true);
				rsLoad.MoveNext();
			}
			if (lstDepartment.Items.Count > 0)
			{
				lstDepartment.TopIndex = 0;
			}
			lstSequence.Clear();
			rsLoad.OpenRecordset("select seqNumber from tblemployeemaster where status <> 'Terminated' and status <> 'Retired' group by seqnumber order by seqnumber", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				lstSequence.AddItem(FCConvert.ToString(rsLoad.Get_Fields("seqnumber")));
				lstSequence.ItemData(lstSequence.NewIndex, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("seqNumber"))));
				lstSequence.SetSelected(lstSequence.NewIndex, true);
				rsLoad.MoveNext();
			}
			if (lstSequence.Items.Count > 0)
			{
				lstSequence.TopIndex = 0;
			}
			lstTypes.Clear();
			rsLoad.OpenRecordset("select * from tblCodeTypes order by description", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "penobscot county" || (FCConvert.ToInt32(rsLoad.Get_Fields("ID")) != 10))
				{
					lstTypes.AddItem(rsLoad.Get_Fields_String("description"));
					lstTypes.ItemData(lstTypes.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("ID")));
					lstTypes.SetSelected(lstTypes.NewIndex, true);
				}
				rsLoad.MoveNext();
			}
			if (lstTypes.Items.Count > 0)
			{
				lstTypes.TopIndex = 0;
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (CreateExport())
			{
				Close();
			}
		}

		private bool CreateExport()
		{
			bool CreateExport = false;
			StreamWriter ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolOpen = false;
				CreateExport = false;
				if (fecherFoundation.Strings.Trim(txtExportpath.Text) == "")
				{
					MessageBox.Show("You must specify a file to export.", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateExport;
				}
				FCFileSystem fso = new FCFileSystem();
				string strPath;
				string strFile;
				clsDRWrapper rsDept = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsDept.OpenRecordset("select * from deptdivtitles where convert(int, isnull(division, 0)) = 0 order by ID", "twbd0000.vb1");
				}
				strFile = fecherFoundation.Strings.Trim(txtExportpath.Text);
				strPath = Path.GetDirectoryName(strFile);
				//FC:FINAL:DSE:#i2315 Export file will download instead of save locally
				//if (!Directory.Exists(strPath))
				//{
				//	MessageBox.Show("Path " + strPath + " not found.", "Invalid Path", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//	return CreateExport;
				//}
				int x;
				clsDRWrapper rsTypes = new clsDRWrapper();
				bool boolFound;
				bool boolSeqAll;
				bool boolTypesAll;
				bool boolDeptAll;
				boolSeqAll = true;
				boolTypesAll = true;
				boolDeptAll = true;
				boolFound = false;
				for (x = 0; x <= lstDepartment.Items.Count - 1; x++)
				{
					if (lstDepartment.Selected(x))
					{
						boolFound = true;
					}
					else
					{
						boolDeptAll = false;
					}
				}
				// x
				if (!boolFound)
				{
					MessageBox.Show("You must select as least one department to export", "No Department Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateExport;
				}
				boolFound = false;
				for (x = 0; x <= lstSequence.Items.Count - 1; x++)
				{
					if (lstSequence.Selected(x))
					{
						boolFound = true;
					}
					else
					{
						boolSeqAll = false;
					}
				}
				// x
				if (!boolFound)
				{
					MessageBox.Show("You must select as least one sequence to export", "No Sequence Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateExport;
				}
				boolFound = false;
				for (x = 0; x <= lstTypes.Items.Count - 1; x++)
				{
					if (lstTypes.Selected(x))
					{
						boolFound = true;
					}
					else
					{
						boolTypesAll = false;
					}
				}
				// x
				if (!boolFound)
				{
					MessageBox.Show("You must select at least one type to export", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateExport;
				}
				//FC:FINAL:DSE:#i2315 Export file will download instead of save locally
				//if (FCFileSystem.FileExists(strFile))
				//{
				//	if (MessageBox.Show("That file already exists and will be overwritten." + "\r\n" + "Do you want to continue?", "File Overwrite Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
				//	{
				//		return CreateExport;
				//	}
				//	FCFileSystem.DeleteFile(strFile);
				//}
				string strSQL = "";
				if (boolTypesAll)
				{
					strSQL = "select * from tblcodetypes order by description";
				}
				else
				{
					strSQL = "select * from tblcodetypes where ";
					for (x = 0; x <= lstTypes.Items.Count - 1; x++)
					{
						if (lstTypes.Selected(x))
						{
							strSQL += " ID = " + FCConvert.ToString(lstTypes.ItemData(x)) + " or";
						}
					}
					// x
					strSQL = Strings.Mid(strSQL, 1, strSQL.Length - 2);
					strSQL += " order by description";
				}
				rsTypes.OpenRecordset(strSQL, "twpy0000.vb1");
				string strRecord = "";
				clsDRWrapper rsVals = new clsDRWrapper();
				rsVals.OpenRecordset("select * from tblvacationsick order by employeenumber,typeid,selected desc", "twpy0000.vb1");
				ts = FCFileSystem.CreateTextFile(strFile);
				boolOpen = true;
				string strWhere;
				string strDeptWhere;
				string strSeqWhere;
				int lngType = 0;
				clsDRWrapper rsEmp = new clsDRWrapper();
				string strOr;
				double dblTemp = 0;
				strOr = "";
				strDeptWhere = "";
				strSeqWhere = "";
				if (!boolDeptAll)
				{
					strDeptWhere = " and (";
					for (x = 0; x <= lstDepartment.Items.Count - 1; x++)
					{
						if (lstDepartment.Selected(x))
						{
							if (lstDepartment.ItemData(x) > 0)
							{
								if (rsDept.FindFirstRecord("ID", lstDepartment.ItemData(x)))
								{
									strDeptWhere += " " + strOr + " department = '" + rsDept.Get_Fields_String("Department") + "' ";
									strOr = " or ";
								}
							}
							else
							{
								strDeptWhere += " " + strOr + " department = '" + lstDepartment.Items[x].Text + "' ";
								strOr = " or ";
							}
						}
					}
					// x
					strDeptWhere += ") ";
				}
				if (!boolSeqAll)
				{
					strOr = "";
					strSeqWhere = " and (";
					for (x = 0; x <= lstSequence.Items.Count - 1; x++)
					{
						if (lstSequence.Selected(x))
						{
							strSeqWhere += " " + strOr + " seqnumber = " + FCConvert.ToString(lstSequence.ItemData(x));
							strOr = " or ";
						}
					}
					// x
					strSeqWhere += ") ";
				}
				strWhere = strDeptWhere + strSeqWhere;
				strSQL = "Select * from tblemployeemaster where status <> 'Terminated' and status <> 'Retired' " + strWhere + " order by employeenumber";
				rsEmp.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsEmp.EndOfFile())
				{
					strRecord = "Name,Employee,";
					while (!rsTypes.EndOfFile())
					{
						if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "penobscot county" || (FCConvert.ToInt32(rsTypes.Get_Fields("ID")) != 10))
						{
							strRecord += rsTypes.Get_Fields_String("description") + ",";
						}
						rsTypes.MoveNext();
					}
					strRecord = Strings.Mid(strRecord, 1, strRecord.Length - 1);
					ts.WriteLine(strRecord);
					while (!rsEmp.EndOfFile())
					{
						strRecord = fecherFoundation.Strings.Trim(rsEmp.Get_Fields_String("FirstName") + " " + rsEmp.Get_Fields_String("LastName") + " " + rsEmp.Get_Fields_String("Desig")) + ",";
						if (!(fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county"))
						{
							strRecord += rsEmp.Get_Fields("employeenumber");
						}
						else
						{
							strRecord += FCConvert.ToString(Conversion.Val(rsEmp.Get_Fields("employeenumber"))) + ",";
						}
						for (x = 0; x <= lstTypes.Items.Count - 1; x++)
						{
							if (lstTypes.Selected(x))
							{
								lngType = lstTypes.ItemData(x);
								if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) != "penobscot county" || (lngType != 1 && lngType != 10))
								{
									if (rsVals.FindFirstRecord2("employeenumber,typeid", rsEmp.Get_Fields("employeenumber") + "," + FCConvert.ToString(lngType), ","))
									{
										strRecord += FCConvert.ToString(Conversion.Val(rsVals.Get_Fields("usedbalance"))) + ",";
									}
									else
									{
										strRecord += "0,";
									}
								}
								else
								{
									// this is sick or sickbank
									if (lngType == 1)
									{
										// combine it under sick
										dblTemp = 0;
										if (rsVals.FindFirstRecord2("employeenumber,typeid", rsEmp.Get_Fields("employeenumber") + "," + FCConvert.ToString(lngType), ","))
										{
											dblTemp += Conversion.Val(rsVals.Get_Fields("usedbalance"));
										}
										if (rsVals.FindFirstRecord2("employeenumber,typeid", rsEmp.Get_Fields("employeenumber") + ",10", ","))
										{
											// sick bank
											dblTemp += Conversion.Val(rsVals.Get_Fields("usedbalance"));
										}
										strRecord += FCConvert.ToString(dblTemp) + ",";
									}
								}
							}
						}
						// x
						strRecord = Strings.Mid(strRecord, 1, strRecord.Length - 1);
						ts.WriteLine(strRecord);
						rsEmp.MoveNext();
					}
				}
				else
				{
					MessageBox.Show("No records found.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolOpen = false;
					ts.Close();
					return CreateExport;
				}
				boolOpen = false;
				ts.Close();
				CreateExport = true;
				//FC:FINAL:DDU:#i2213 - download to client the file
				FCUtils.Download(strFile, "VacSickExport.csv");
				MessageBox.Show("File exported successfully", "File Exported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return CreateExport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + " in CreateExport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateExport;
		}
	}
}
