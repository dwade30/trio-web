//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmContractAddEdit : BaseForm
	{
		public frmContractAddEdit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmContractAddEdit InstancePtr
		{
			get
			{
				return (frmContractAddEdit)Sys.GetInstance(typeof(frmContractAddEdit));
			}
		}

		protected frmContractAddEdit _InstancePtr = null;
		//=========================================================
		private clsContract conCurContract = new clsContract();
		const int CNSTGRIDCONTRACTSCOLID = 0;
		const int CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER = 1;
		const int CNSTGRIDCONTRACTSCOLDESCRIPTION = 2;
		const int CNSTGRIDCONTRACTSCOLORIGINALAMOUNT = 5;
		const int CNSTGRIDCONTRACTSCOLPAID = 6;
		const int CNSTGRIDCONTRACTSCOLLEFT = 7;
		const int CNSTGRIDCONTRACTSCOLSTARTDATE = 3;
		const int CNSTGRIDCONTRACTSCOLENDDATE = 4;
		const int CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL = 8;
		const int CNSTGRIDCONTRACTSCOLVENDOR = 9;
		const int CNSTGRIDACCOUNTSCOLID = 0;
		const int CNSTGRIDACCOUNTSCOLCONTRACTID = 1;
		const int CNSTGRIDACCOUNTSCOLACCOUNT = 2;
		const int CNSTGRIDACCOUNTSCOLORIGINALAMOUNT = 3;
		const int CNSTGRIDACCOUNTSCOLPAID = 4;
		const int CNSTGRIDACCOUNTSCOLLEFT = 5;
		const int CNSTGRIDCOLEMPLOYEE = 0;
		const int CNSTGRIDCOLEMPLOYEENAME = 1;
		const int CNSTGRIDCOLSEQUENCE = 2;
		const int CNSTGRIDCOLDEPTDIV = 3;
		const int CNSTPYTYPEGROUP = 0;
		const int CNSTPYTYPEIND = 1;
		const int CNSTPYTYPESEQ = 2;
		const int CNSTPYTYPEDEPTDIV = 3;
		const int CNSTPYTYPEDEPARTMENT = 4;
		private bool boolEncumber;

		public void Init(string strEmployee)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			boolEncumber = false;
			rsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				if (modGlobalConstants.Statics.gboolBD && rsLoad.Get_Fields_Boolean("EncumberContracts"))
				{
					boolEncumber = true;
				}
			}
			SetupGridContracts();
			SetupGridAccounts();
			SetupGrid();
			LoadEmployees();
			// LoadEmployee (strEmployee)
			this.Show(App.MainForm);
		}

		private void frmContractAddEdit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmContractAddEdit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmContractAddEdit properties;
			//frmContractAddEdit.FillStyle	= 0;
			//frmContractAddEdit.ScaleWidth	= 9300;
			//frmContractAddEdit.ScaleHeight	= 7680;
			//frmContractAddEdit.LinkTopic	= "Form2";
			//frmContractAddEdit.LockControls	= -1  'True;
			//frmContractAddEdit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void LoadEmployee(string strEmployee)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				int lngARow;
				// GridContracts.Rows = 1
				// GridAccounts.Rows = 1
				rsLoad.OpenRecordset("select * from contracts where employeenumber = '" + strEmployee + "' order by ID", "twpy0000.vb1");
				rsLoad.MoveFirst();
				rsLoad.MovePrevious();
				// Do While Not rsLoad.EndOfFile
				if (rsLoad.BeginningOfFile() && !rsLoad.EndOfFile())
				{
					if (rsLoad.FindFirstRecord("employeenumber", strEmployee))
					{
						if (conCurContract.LoadContract(FCConvert.ToInt32(rsLoad.Get_Fields("contractid")), rsLoad))
						{
							GridContracts.Rows += 1;
							lngRow = GridContracts.Rows - 1;
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID, FCConvert.ToString(conCurContract.ContractID));
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER, strEmployee);
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLDESCRIPTION, conCurContract.Description);
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.OriginalAmount, "#,###,###,##0.00"));
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLPAID, Strings.Format(conCurContract.AmountPaid, "###,###,##0.00"));
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLLEFT, Strings.Format(conCurContract.OriginalAmount - conCurContract.AmountPaid, "###,###,##0.00"));
							if (Information.IsDate(conCurContract.StartDate))
							{
								if (conCurContract.StartDate.ToOADate() != 0)
								{
									GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLSTARTDATE, Strings.Format(conCurContract.StartDate, "MM/dd/yyyy"));
								}
							}
							if (Information.IsDate(conCurContract.EndDate))
							{
								if (conCurContract.EndDate.ToOADate() != 0)
								{
									GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENDDATE, Strings.Format(conCurContract.EndDate, "MM/dd/yyyy"));
								}
							}
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL, FCConvert.ToString(conCurContract.EncumbranceJournal));
							GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLVENDOR, FCConvert.ToString(conCurContract.Vendor));
							GridContracts.RowHidden(lngRow, true);
							while (!conCurContract.EndOfAccounts())
							{
								GridAccounts.Rows += 1;
								lngARow = GridAccounts.Rows - 1;
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLID, FCConvert.ToString(conCurContract.CurAccountID));
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLACCOUNT, conCurContract.CurAccount);
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLCONTRACTID, FCConvert.ToString(conCurContract.ContractID));
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.CurAccountOriginalAmount, "###,###,##0.00"));
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLPAID, Strings.Format(conCurContract.CurAccountPaid, "###,###,##0.00"));
								if (conCurContract.CurAccountID >= 0)
								{
									GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, Strings.Format(conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid, "###,###,##0.00"));
								}
								else
								{
									GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
								}
								// GridAccounts.RowHidden(lngARow) = True
								conCurContract.MoveNextAccount();
							}
						}
					}
				}
				while (rsLoad.FindNextRecord("employeenumber", strEmployee))
				{
					if (conCurContract.LoadContract(FCConvert.ToInt32(rsLoad.Get_Fields("contractid")), rsLoad))
					{
						GridContracts.Rows += 1;
						lngRow = GridContracts.Rows - 1;
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID, FCConvert.ToString(conCurContract.ContractID));
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER, strEmployee);
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLDESCRIPTION, conCurContract.Description);
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.OriginalAmount, "#,###,###,##0.00"));
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLPAID, Strings.Format(conCurContract.AmountPaid, "###,###,##0.00"));
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLLEFT, Strings.Format(conCurContract.OriginalAmount - conCurContract.AmountPaid, "###,###,##0.00"));
						if (Information.IsDate(conCurContract.StartDate))
						{
							if (conCurContract.StartDate.ToOADate() != 0)
							{
								GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLSTARTDATE, Strings.Format(conCurContract.StartDate, "MM/dd/yyyy"));
							}
						}
						if (Information.IsDate(conCurContract.EndDate))
						{
							if (conCurContract.EndDate.ToOADate() != 0)
							{
								GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENDDATE, Strings.Format(conCurContract.EndDate, "MM/dd/yyyy"));
							}
						}
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL, FCConvert.ToString(conCurContract.EncumbranceJournal));
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLVENDOR, FCConvert.ToString(conCurContract.Vendor));
						// GridContracts.RowHidden(lngRow) = True
						while (!conCurContract.EndOfAccounts())
						{
							GridAccounts.Rows += 1;
							lngARow = GridAccounts.Rows - 1;
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLID, FCConvert.ToString(conCurContract.CurAccountID));
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLACCOUNT, conCurContract.CurAccount);
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLCONTRACTID, FCConvert.ToString(conCurContract.ContractID));
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.CurAccountOriginalAmount, "###,###,##0.00"));
							GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLPAID, Strings.Format(conCurContract.CurAccountPaid, "###,###,##0.00"));
							if (conCurContract.CurAccountID >= 0)
							{
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, Strings.Format(conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid, "###,###,##0.00"));
							}
							else
							{
								GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
							}
							// GridAccounts.RowHidden(lngARow) = True
							conCurContract.MoveNextAccount();
						}
					}
					// rsLoad.MoveNext
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadEmployee", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReLoadContract(int lngID)
		{
			int lngRow;
			int lngARow;
			if (conCurContract.LoadContract(lngID))
			{
				GridContracts.Rows += 1;
				lngRow = GridContracts.Rows - 1;
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID, FCConvert.ToString(conCurContract.ContractID));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER, conCurContract.EmployeeNumber);
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLDESCRIPTION, conCurContract.Description);
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.OriginalAmount, "#,###,###,##0.00"));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLPAID, Strings.Format(conCurContract.AmountPaid, "###,###,##0.00"));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLLEFT, Strings.Format(conCurContract.OriginalAmount - conCurContract.AmountPaid, "###,###,##0.00"));
				if (Information.IsDate(conCurContract.StartDate))
				{
					if (conCurContract.StartDate.ToOADate() != 0)
					{
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLSTARTDATE, Strings.Format(conCurContract.StartDate, "MM/dd/yyyy"));
					}
				}
				if (Information.IsDate(conCurContract.EndDate))
				{
					if (conCurContract.EndDate.ToOADate() != 0)
					{
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENDDATE, Strings.Format(conCurContract.EndDate, "MM/dd/yyyy"));
					}
				}
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL, FCConvert.ToString(conCurContract.EncumbranceJournal));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLVENDOR, FCConvert.ToString(conCurContract.Vendor));
				while (!conCurContract.EndOfAccounts())
				{
					GridAccounts.Rows += 1;
					lngARow = GridAccounts.Rows - 1;
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLID, FCConvert.ToString(conCurContract.CurAccountID));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLACCOUNT, conCurContract.CurAccount);
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLCONTRACTID, FCConvert.ToString(conCurContract.ContractID));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.CurAccountOriginalAmount, "###,###,##0.00"));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLPAID, Strings.Format(conCurContract.CurAccountPaid, "###,###,##0.00"));
					if (conCurContract.CurAccountID >= 0)
					{
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, Strings.Format(conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid, "###,###,##0.00"));
					}
					else
					{
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
					}
					conCurContract.MoveNextAccount();
				}
				GridContracts.Row = GridContracts.Rows - 1;
			}
		}

		private void SetupGridAccounts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridAccounts.Cols = 6;
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLID, true);
				GridAccounts.ColHidden(CNSTGRIDACCOUNTSCOLCONTRACTID, true);
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLACCOUNT, "Account");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, "Amount");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLPAID, "Paid");
				GridAccounts.TextMatrix(0, CNSTGRIDACCOUNTSCOLLEFT, "Remaining");
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLPAID, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridAccounts.ColAlignment(CNSTGRIDACCOUNTSCOLLEFT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridContracts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridContracts.Cols = 10;
				GridContracts.ColHidden(CNSTGRIDCONTRACTSCOLID, true);
				GridContracts.ColHidden(CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER, true);
				GridContracts.ColHidden(CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL, true);
				GridContracts.ColHidden(CNSTGRIDCONTRACTSCOLVENDOR, true);
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLDESCRIPTION, "Description");
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, "Amount");
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLSTARTDATE, "Start Date");
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLENDDATE, "End Date");
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLPAID, "Paid");
				GridContracts.TextMatrix(0, CNSTGRIDCONTRACTSCOLLEFT, "Remaining");
                GridContracts.ColAlignment(CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridContracts.ColAlignment(CNSTGRIDCONTRACTSCOLPAID, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridContracts.ColAlignment(CNSTGRIDCONTRACTSCOLLEFT, FCGrid.AlignmentSettings.flexAlignRightCenter);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridContracts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridContracts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int GridWidth = 0;
				GridWidth = GridContracts.WidthOriginal;
				GridContracts.ColWidth(CNSTGRIDCONTRACTSCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
				GridContracts.ColWidth(CNSTGRIDCONTRACTSCOLLEFT, FCConvert.ToInt32(0.14 * GridWidth));
				GridContracts.ColWidth(CNSTGRIDCONTRACTSCOLSTARTDATE, FCConvert.ToInt32(0.13 * GridWidth));
				GridContracts.ColWidth(CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.14 * GridWidth));
				GridContracts.ColWidth(CNSTGRIDCONTRACTSCOLPAID, FCConvert.ToInt32(0.14 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ResizeGridContracts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridAccounts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int GridWidth = 0;
				GridWidth = GridAccounts.WidthOriginal;
				GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLACCOUNT, FCConvert.ToInt32(0.39 * GridWidth));
				GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, FCConvert.ToInt32(0.2 * GridWidth));
				GridAccounts.ColWidth(CNSTGRIDACCOUNTSCOLPAID, FCConvert.ToInt32(0.2 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ResizeGridAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmContractAddEdit_Resize(object sender, System.EventArgs e)
		{
			ResizeGridContracts();
			ResizeGridAccounts();
			ResizeGrid();
		}

		private void GridContracts_DblClick(object sender, System.EventArgs e)
		{
			if (GridContracts.MouseRow > 0)
			{
				int lngID = 0;
				int lngRow = 0;
				lngRow = GridContracts.MouseRow;
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID))));
				frmEditContract.InstancePtr.Init(ref lngID);
				GridContracts.RemoveItem(lngRow);
				for (lngRow = GridAccounts.Rows - 1; lngRow >= 1; lngRow--)
				{
					if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLCONTRACTID)) == lngID)
					{
						GridAccounts.RemoveItem(lngRow);
					}
				}
				// lngRow
				ReLoadContract(lngID);
			}
		}

		private void GridContracts_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMouseRow = 0;
			switch (e.Button)
			{
				case MouseButtons.Right:
					{
						lngMouseRow = GridContracts.MouseRow;
						GridContracts.Row = lngMouseRow;
						//App.DoEvents();
						if (GridContracts.Row > 0)
						{
							if (!GridContracts.RowHidden(GridContracts.Row))
							{
								PopupMenu(mnuPopup);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridEmployee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (GridEmployee.Row > 0)
			{
				if (e.KeyCode == Keys.Insert)
				{
					mnuAddContract_Click();
				}
			}
		}

		private void GridEmployee_RowColChange(object sender, System.EventArgs e)
		{
			if (GridEmployee.Row > 0)
			{
				ReShowEmployee(GridEmployee.TextMatrix(GridEmployee.Row, CNSTGRIDCOLEMPLOYEE));
			}
			else
			{
				ReShowEmployee(FCConvert.ToString(0));
			}
		}
		// vbPorter upgrade warning: strEmployeeNumber As string	OnWrite(string, int)
		private void ReShowEmployee(string strEmployeeNumber)
		{
			int lngRow;
			int lngContractRow;
			int lngAccount;
			GridContracts.Rows = 1;
			GridAccounts.Rows = 1;
			LoadEmployee(strEmployeeNumber);
			lngContractRow = 0;
			for (lngRow = 1; lngRow <= GridContracts.Rows - 1; lngRow++)
			{
				if (GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER) == strEmployeeNumber)
				{
					GridContracts.RowHidden(lngRow, false);
					if (lngContractRow == 0)
					{
						lngContractRow = lngRow;
					}
				}
				else
				{
					GridContracts.RowHidden(lngRow, true);
				}
			}
			// lngRow
			GridContracts.Row = lngContractRow;
			// lngAccount = 0
			// For lngRow = 1 To GridAccounts.Rows - 1
			// If GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLCONTRACTID) = lngContract Then
			// GridAccounts.RowHidden(lngRow) = False
			// If lngAccount = 0 Then
			// lngAccount = lngRow
			// End If
			// Else
			// GridAccounts.RowHidden(lngRow) = True
			// End If
			// Next lngRow
		}

		private void GridContracts_RowColChange(object sender, System.EventArgs e)
		{
			int lngContract;
			int lngRow;
			lngContract = 0;
			if (GridContracts.Row > 0)
			{
				lngContract = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContracts.TextMatrix(GridContracts.Row, CNSTGRIDCONTRACTSCOLID))));
			}
			for (lngRow = 1; lngRow <= GridAccounts.Rows - 1; lngRow++)
			{
				if (FCConvert.ToDouble(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLCONTRACTID)) == lngContract)
				{
					GridAccounts.RowHidden(lngRow, false);
				}
				else
				{
					GridAccounts.RowHidden(lngRow, true);
				}
			}
			// lngRow
		}

		private void mnuAddContract_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			string strEmployee;
			int lngARow;
			if (GridEmployee.Row < 1)
				return;
			strEmployee = GridEmployee.TextMatrix(GridEmployee.Row, CNSTGRIDCOLEMPLOYEE);
			if (conCurContract.AddContract(GridEmployee.TextMatrix(GridEmployee.Row, CNSTGRIDCOLEMPLOYEE)) > 0)
			{
				GridContracts.Rows += 1;
				lngRow = GridContracts.Rows - 1;
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID, FCConvert.ToString(conCurContract.ContractID));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER, strEmployee);
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLDESCRIPTION, conCurContract.Description);
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.OriginalAmount, "#,###,###,##0.00"));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLPAID, Strings.Format(conCurContract.AmountPaid, "###,###,##0.00"));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLLEFT, Strings.Format(conCurContract.OriginalAmount - conCurContract.AmountPaid, "###,###,##0.00"));
				if (Information.IsDate(conCurContract.StartDate))
				{
					if (conCurContract.StartDate.ToOADate() != 0)
					{
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLSTARTDATE, Strings.Format(conCurContract.StartDate, "MM/dd/yyyy"));
					}
				}
				if (Information.IsDate(conCurContract.EndDate))
				{
					if (conCurContract.EndDate.ToOADate() != 0)
					{
						GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENDDATE, Strings.Format(conCurContract.EndDate, "MM/dd/yyyy"));
					}
				}
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLENCUMBRANCEJOURNAL, FCConvert.ToString(conCurContract.EncumbranceJournal));
				GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLVENDOR, FCConvert.ToString(conCurContract.Vendor));
				GridContracts.RowHidden(lngRow, false);
				while (!conCurContract.EndOfAccounts())
				{
					GridAccounts.Rows += 1;
					lngARow = GridAccounts.Rows - 1;
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLID, FCConvert.ToString(conCurContract.CurAccountID));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLACCOUNT, conCurContract.CurAccount);
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLCONTRACTID, FCConvert.ToString(conCurContract.ContractID));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLORIGINALAMOUNT, Strings.Format(conCurContract.CurAccountOriginalAmount, "###,###,##0.00"));
					GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLPAID, Strings.Format(conCurContract.CurAccountPaid, "###,###,##0.00"));
					if (conCurContract.CurAccountID >= 0)
					{
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, Strings.Format(conCurContract.CurAccountOriginalAmount - conCurContract.CurAccountPaid, "###,###,##0.00"));
					}
					else
					{
						GridAccounts.TextMatrix(lngARow, CNSTGRIDACCOUNTSCOLLEFT, "0.00");
					}
					GridAccounts.RowHidden(lngARow, false);
					conCurContract.MoveNextAccount();
				}
			}
		}

		public void mnuAddContract_Click()
		{
			mnuAddContract_Click(mnuAddContract, new System.EventArgs());
		}

		private void mnuEditContract_Click(object sender, System.EventArgs e)
		{
			if (GridContracts.Row > 0)
			{
				int lngID = 0;
				int lngRow = 0;
				lngRow = GridContracts.Row;
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID))));
				frmEditContract.InstancePtr.Init(ref lngID);
				GridContracts.RemoveItem(lngRow);
				for (lngRow = GridAccounts.Rows - 1; lngRow >= 1; lngRow--)
				{
					if (Conversion.Val(GridAccounts.TextMatrix(lngRow, CNSTGRIDACCOUNTSCOLCONTRACTID)) == lngID)
					{
						GridAccounts.RemoveItem(lngRow);
					}
				}
				// lngRow
				ReLoadContract(lngID);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadEmployees()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsSearch = new clsDRWrapper();
				string strFind;
				int lngRow;
				strFind = "";
				int lngID;
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				rsLoad.OpenRecordset("select * from payrollpermissions where UserID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					switch (rsLoad.Get_Fields_Int32("type"))
					{
						case CNSTPYTYPEDEPTDIV:
							{
								strFind += "not deptdiv = '" + rsLoad.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEGROUP:
							{
								strFind += "not groupid = '" + rsLoad.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPEIND:
							{
								strFind += "not employeenumber = '" + rsLoad.Get_Fields("val") + "' and ";
								break;
							}
						case CNSTPYTYPESEQ:
							{
								strFind += "not seqnumber = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("val"))) + " and ";
								break;
							}
						case CNSTPYTYPEDEPARTMENT:
							{
								strFind += "not convert(int, isnull(department, 0)) = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("val"))) + " and ";
								break;
							}
					}
					//end switch
					rsLoad.MoveNext();
				}
				if (strFind != string.Empty)
				{
					strFind = Strings.Mid(strFind, 1, strFind.Length - 4);
					strFind = "where " + strFind;
				}
				bool boolAllowed;
				GridEmployee.Rows = 1;
				rsSearch.OpenRecordset("SELECT * FROM TBLemployeemaster " + strFind + " order by employeenumber ", "twpy0000.vb1");
				// Call rsLoad.OpenRecordset("select contractid,employeenumber from contracts order by employeenumber,contractid", "twpy0000.vb1")
				rsLoad.OpenRecordset("select * from contracts order by employeenumber,ID", "twpy0000.vb1");
				rsLoad.MoveFirst();
				rsLoad.MovePrevious();
				while (!rsSearch.EndOfFile())
				{
					GridEmployee.Rows += 1;
					lngRow = GridEmployee.Rows - 1;
					GridEmployee.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEE, FCConvert.ToString(rsSearch.Get_Fields("employeenumber")));
					GridEmployee.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENAME, fecherFoundation.Strings.Trim(rsSearch.Get_Fields("lastname") + ", " + fecherFoundation.Strings.Trim(rsSearch.Get_Fields("firstname") + " " + rsSearch.Get_Fields("middlename")) + " " + rsSearch.Get_Fields("desig")));
					GridEmployee.TextMatrix(lngRow, CNSTGRIDCOLSEQUENCE, FCConvert.ToString(rsSearch.Get_Fields("seqnumber")));
					GridEmployee.TextMatrix(lngRow, CNSTGRIDCOLDEPTDIV, FCConvert.ToString(rsSearch.Get_Fields("deptdiv")));
					// Call LoadEmployee(rsSearch.Fields("employeenumber"), rsLoad)
					rsSearch.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadEmployees", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGrid()
		{
			GridEmployee.Cols = 4;
			GridEmployee.Rows = 1;
			GridEmployee.TextMatrix(0, CNSTGRIDCOLEMPLOYEE, "Employee #");
			GridEmployee.TextMatrix(0, CNSTGRIDCOLEMPLOYEENAME, "Name");
			GridEmployee.TextMatrix(0, CNSTGRIDCOLSEQUENCE, "Seq");
			GridEmployee.TextMatrix(0, CNSTGRIDCOLDEPTDIV, "Dept/Div");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = GridEmployee.WidthOriginal;
			GridEmployee.ColWidth(CNSTGRIDCOLEMPLOYEE, FCConvert.ToInt32(0.15 * GridWidth));
			GridEmployee.ColWidth(CNSTGRIDCOLEMPLOYEENAME, FCConvert.ToInt32(0.5 * GridWidth));
			GridEmployee.ColWidth(CNSTGRIDCOLSEQUENCE, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void mnuPayoff_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = GridContracts.Row;
			if (MessageBox.Show("This will pay off the remainder of the contract for employee " + GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER) + "\r\n" + "Continue?", "Pay Off Contract?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				PayOff(lngRow);
			}
		}

		private void PayOff(int lngRow)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngContract;
				string strList = "";
				lngContract = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID))));
				clsAuditChangesReporting clsA = new clsAuditChangesReporting();
				string strEmployee;
				strEmployee = GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER);
				clsContract conCT = new clsContract();
				int lngChecks = 0;
				// vbPorter upgrade warning: intCheck As int	OnWriteFCConvert.ToInt32(
				int intCheck;
				double dblTotal = 0;
				// vbPorter upgrade warning: dblWeek As double	OnWrite(string)
				double dblWeek = 0;
				double dblCurrent = 0;
				if (conCT.LoadContract(lngContract))
				{
					strList = "1|2|3|4|5|6|7|8|9|10";
					lngChecks = frmDropdown.InstancePtr.Init(ref strList, 1, "Contract Payoff", "Number of checks to print");
					if (lngChecks > 0)
					{
						clsDRWrapper rsSave = new clsDRWrapper();
						clsDRWrapper rsLoad = new clsDRWrapper();
						int lngMaxRec = 0;
						modGlobalFunctions.AddCYAEntry_6("PY", "Paid Off Contract " + FCConvert.ToString(lngContract), "Employee " + strEmployee);
						clsA.AddChange("Paid Off Contract " + FCConvert.ToString(lngContract));
						clsA.SaveToAuditChangesTable("Contracts", strEmployee, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "M/d/yyyy"), Conversion.Str(modGlobalVariables.Statics.gintCurrentPayRun));
						rsSave.Execute("UPDATE tblpayrolldistribution set hoursweek = 0 where employeenumber = '" + strEmployee + "' and contractid = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
						rsSave.OpenRecordset("select * from tblpayrolldistribution where ID = 0", "twpy0000.vb1");
						while (!conCT.EndOfAccounts())
						{
							if (conCT.CurAccountOriginalAmount - conCT.CurAccountPaid > 0)
							{
								rsLoad.OpenRecordset("select max(recordnumber) as maxrec from tblpayrolldistribution where employeenumber = '" + strEmployee + "'", "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									lngMaxRec = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("maxrec"))));
								}
								else
								{
									lngMaxRec = 0;
								}
								dblTotal = conCT.CurAccountOriginalAmount - conCT.CurAccountPaid;
								dblWeek = FCConvert.ToDouble(Strings.Format(dblTotal / lngChecks, "0.00"));
								for (intCheck = 1; intCheck <= lngChecks; intCheck++)
								{
									dblCurrent = dblWeek;
									if (dblCurrent > dblTotal)
									{
										dblCurrent = dblTotal;
									}
									if (intCheck == lngChecks)
									{
										dblCurrent = dblTotal;
									}
									dblTotal -= dblCurrent;
									if (dblCurrent > 0)
									{
										rsSave.AddNew();
										if (intCheck == 1)
										{
											if (lngChecks > 1)
											{
												rsSave.Set_Fields("accountcode", 2);
												// Weekly
											}
											else
											{
												rsSave.Set_Fields("accountcode", 3);
												// one week only
											}
										}
										else
										{
											rsSave.Set_Fields("accountcode", 2 + intCheck);
											// 4 is s(1), 5 is s(2) etc.
										}
										rsSave.Set_Fields("recordnumber", lngMaxRec + intCheck);
										rsSave.Set_Fields("employeenumber", strEmployee);
										rsSave.Set_Fields("accountnumber", conCT.CurAccount);
										rsSave.Set_Fields("baserate", dblCurrent);
										rsSave.Set_Fields("factor", 1);
										rsSave.Set_Fields("gross", dblCurrent);
										rsSave.Set_Fields("hoursweek", 1);
										rsSave.Set_Fields("contractid", lngContract);
										rsLoad.OpenRecordset("select * from tblpayrolldistribution where contractid = " + FCConvert.ToString(lngContract) + " and employeenumber = '" + strEmployee + "' and accountnumber = '" + conCT.CurAccount + "' order by recordnumber", "twpy0000.vb1");
										if (!rsLoad.EndOfFile())
										{
											rsSave.Set_Fields("workcomp", rsLoad.Get_Fields("workcomp"));
											if (Conversion.Val(rsLoad.Get_Fields("cat")) > 1 && Conversion.Val(rsLoad.Get_Fields("cat")) < 6)
											{
												rsSave.Set_Fields("cat", 1);
											}
											else
											{
												rsSave.Set_Fields("cat", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cat"))));
											}
											rsSave.Set_Fields("msrs", rsLoad.Get_Fields("msrs"));
											rsSave.Set_Fields("WC", rsLoad.Get_Fields_String("WC"));
											rsSave.Set_Fields("taxcode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("taxcode"))));
											rsSave.Set_Fields("cd", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cd"))));
											rsSave.Set_Fields("distu", rsLoad.Get_Fields("distu"));
											rsSave.Set_Fields("grantfunded", rsLoad.Get_Fields("grantfunded"));
											rsSave.Set_Fields("statuscode", rsLoad.Get_Fields("statuscode"));
											rsSave.Set_Fields("msrsid", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("msrsid"))));
											rsSave.Set_Fields("project", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("project"))));
										}
										else
										{
											rsSave.Set_Fields("workcomp", "Y");
											rsSave.Set_Fields("cat", 1);
											rsSave.Set_Fields("MSRS", "Y");
											rsSave.Set_Fields("WC", "Y");
											rsSave.Set_Fields("taxcode", 2);
											rsSave.Set_Fields("cd", 2);
											rsSave.Set_Fields("distu", "Y");
											rsSave.Set_Fields("grantfunded", 0);
											rsSave.Set_Fields("msrsid", modCoreysSweeterCode.CNSTMSRSREPORTTYPENONE);
											rsSave.Set_Fields("project", 0);
										}
										rsSave.Update();
									}
								}
								// intCheck
							}
							conCT.MoveNextAccount();
						}
						MessageBox.Show("The payoff check(s) have been created in the distribution screen" + "\r\n" + "Please check all settings for accuracy", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In PayOff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrintCustomContract_Click(object sender, System.EventArgs e)
		{
			frmCustomContractReport.InstancePtr.Show(App.MainForm);
		}

		private void mnuPrintSetup_Click(object sender, System.EventArgs e)
		{
			rptContract.InstancePtr.Init(false);
		}

		private void mnuPrintSummary_Click(object sender, System.EventArgs e)
		{
			rptContract.InstancePtr.Init(true);
		}

		private void mnuVoid_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to void this contract?", "Void Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				VoidContract(GridContracts.Row);
			}
		}

		private void VoidContract(int lngRow)
		{
			// VB6 Bad Scope Dim:
			int lngJournal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngContract;
				clsAuditChangesReporting clsA = new clsAuditChangesReporting();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strEmployee;
				clsContract conCT = new clsContract();
				int lngEncumbranceJournal;
				int lngEncumbranceID = 0;
				string strContractDescription;
				strEmployee = GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLEMPLOYEENUMBER);
				lngContract = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLID))));
				modGlobalFunctions.AddCYAEntry_6("PY", "Deleted Contract " + FCConvert.ToString(lngContract), "Employee " + strEmployee);
				clsA.AddChange("Deleted Contract " + FCConvert.ToString(lngContract));
				clsA.SaveToAuditChangesTable("Contracts", strEmployee, Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "M/d/yyyy"), Conversion.Str(modGlobalVariables.Statics.gintCurrentPayRun));
				strContractDescription = GridContracts.TextMatrix(lngRow, CNSTGRIDCONTRACTSCOLDESCRIPTION);
				if (boolEncumber)
				{
					double dblTotal = 0;
					// vbPorter upgrade warning: dblTemp As double	OnWrite(Decimal, double)
					double dblTemp = 0;
					bool boolChanged = false;
					clsDRWrapper rsJournal = new clsDRWrapper();
					lngJournal = 0;
					conCT.LoadContract(lngContract);
					lngEncumbranceJournal = conCT.EncumbranceJournal;
					lngEncumbranceID = conCT.EncumbranceID;
					rsSave.OpenRecordset("SELECT * FROM encumbrancedetail where recordnumber = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
					dblTotal = 0;
					boolChanged = false;
					rsJournal.OpenRecordset("select * from journalentries where ID = 0", "twbd0000.vb1");
					while (!rsSave.EndOfFile())
					{
						dblTemp = FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(rsSave.Get_Fields("amount")) - Conversion.Val(rsSave.Get_Fields("liquidated")) + Conversion.Val(rsSave.Get_Fields("adjustments"))));
						dblTemp *= -1;
						dblTotal += dblTemp;
						if (dblTemp != 0)
						{
							boolChanged = true;
							if (lngJournal == 0)
							{
								lngJournal = modCoreysSweeterCode.CreateAJournal("PY Void Contract " + FCConvert.ToString(lngContract), DateTime.Today.Month, "GJ");
							}
							rsJournal.AddNew();
							rsJournal.Set_Fields("amount", dblTemp);
							rsJournal.Set_Fields("Date", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							rsJournal.Set_Fields("Type", "G");
							rsJournal.Set_Fields("Description", fecherFoundation.Strings.Trim(Strings.Left("PY Void Contract Emp " + conCurContract.EmployeeNumber + Strings.StrDup(25, " "), 25)));
							rsJournal.Set_Fields("vendornumber", 0);
							rsJournal.Set_Fields("journalnumber", lngJournal);
							rsJournal.Set_Fields("account", rsSave.Get_Fields("account"));
							rsJournal.Set_Fields("warrantnumber", 0);
							rsJournal.Set_Fields("period", DateTime.Today.Month);
							rsJournal.Set_Fields("rcb", "E");
							rsJournal.Set_Fields("status", "E");
							rsJournal.Set_Fields("po", lngContract);
							rsJournal.Update();
						}
						rsSave.Edit();
						rsSave.Set_Fields("adjustments", Conversion.Val(rsSave.Get_Fields("adjustments")) + dblTemp);
						rsSave.Update();
						rsSave.MoveNext();
					}
					if (dblTotal != 0)
					{
						rsSave.OpenRecordset("select * from encumbrances where ID = " + FCConvert.ToString(lngEncumbranceID), "twbd0000.vb1");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
							rsSave.Set_Fields("Adjustments", FCConvert.ToDecimal(FCConvert.ToInt32(Conversion.Val(rsSave.Get_Fields("amount")) - Conversion.Val(rsSave.Get_Fields("liquidated"))) * -1));
							rsSave.Update();
						}
					}
					if (boolChanged)
					{
						rsJournal.OpenRecordset("select * from journalmaster where journalnumber = " + FCConvert.ToString(lngJournal), "twbd0000.vb1");
						if (!rsJournal.EndOfFile())
						{
							rsJournal.Edit();
							rsJournal.Set_Fields("totalamount", dblTotal);
							rsJournal.Update();
						}
					}
				}
				int lngCRow;
				for (lngCRow = GridAccounts.Rows - 1; lngCRow >= 1; lngCRow--)
				{
					GridAccounts.RemoveItem(lngCRow);
				}
				// lngCRow
				GridContracts.RemoveItem(lngRow);
				rsSave.Execute("DELETE from contractaccounts where contractid = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
				rsSave.Execute("delete from contracts where ID = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
				rsSave.Execute("update tblcheckdetail set contractid = 0 where contractid = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
				rsSave.Execute("update tbltemppayprocess set contractid = 0 where contractid = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
				rsSave.Execute("update tblpayrolldistribution set contractid = 0 where contractid = " + FCConvert.ToString(lngContract), "twpy0000.vb1");
				if (!boolEncumber || lngJournal == 0)
				{
					MessageBox.Show("Contract for employee " + strEmployee + " voided", "Voided", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Contract for employee " + strEmployee + " voided" + "\r\n" + "Created journal " + FCConvert.ToString(lngJournal), "Voided", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				ReShowEmployee(strEmployee);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In VoidContract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
