//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using SharedApplication.Extensions;
using System.Linq;

namespace TWPY0000
{
    public class cEmployeeService : IDisposable
    {
        //=========================================================
        const int CNSTPYTYPEGROUP = 0;
        const int CNSTPYTYPEIND = 1;
        const int CNSTPYTYPESEQ = 2;
        const int CNSTPYTYPEDEPTDIV = 3;
        const int CNSTPYTYPEDEPARTMENT = 4;
        private string strLastError = "";
        private int lngLastError;
        private Dictionary<object, object> dictStates = new Dictionary<object, object>();
        private cPYDistributionController distController = new cPYDistributionController();

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
        public object HadError
        {
            get
            {
                object HadError = null;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        private void SetError(int lngErrorNumber, string strErrorMessage)
        {
            lngLastError = lngErrorNumber;
            strLastError = strErrorMessage;
        }

        public cEmployee GetEmployeeByID(int lngID)
        {
            cEmployee GetEmployeeByID = null;
            clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select * from tblEmployeeMaster where id = " + FCConvert.ToString(lngID), "twpy0000.vb1");
            GetEmployeeByID = FillEmployee(ref rsLoad);
            rsLoad.Dispose();
            return GetEmployeeByID;
        }

        public cEmployee GetEmployeeByNumber(string strEmployeeNumber)
        {
            cEmployee GetEmployeeByNumber = null;
            clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
            GetEmployeeByNumber = FillEmployee(ref rsLoad);
            rsLoad.Dispose();
            return GetEmployeeByNumber;
        }

        private object FillStatesDictionary()
        {
            object FillStatesDictionary = null;
            clsDRWrapper rsLoad = new clsDRWrapper();
            if (dictStates == null)
            {
                dictStates = new Dictionary<object, object>();
            }
            dictStates.Clear();
            rsLoad.OpenRecordset("Select * from states order by id", "Payroll");
            while (!rsLoad.EndOfFile())
            {
                dictStates.Add(rsLoad.Get_Fields("id"), rsLoad.Get_Fields("State"));
                rsLoad.MoveNext();
            }
            rsLoad.Dispose();
            return FillStatesDictionary;
        }

        private cEmployee FillEmployee(ref clsDRWrapper rsLoad)
        {
            cEmployee FillEmployee = null;
            cEmployee retEmp = new cEmployee();
            // Dim rsStates As New clsDRWrapper
            if (!rsLoad.EndOfFile())
            {
                retEmp = new cEmployee();
                if (!dictStates.Any())
                {
                    FillStatesDictionary();
                }
                retEmp.Address1 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("address1"));
                retEmp.Address2 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("address2"));
                retEmp.AnniversaryDate = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("DateAnniversary"));
                if (Information.IsDate(rsLoad.Get_Fields("datehire")))
                {
                    if (Convert.ToDateTime(rsLoad.Get_Fields("datehire")).ToOADate() != 0)
                    {
                        retEmp.DateHired = Strings.Format(rsLoad.Get_Fields("datehire"), "MM/dd/yyyy");
                    }
                }

                if (Information.IsDate(rsLoad.Get_Fields_DateTime("datebirth")))
                {
                    if (!rsLoad.Get_Fields_DateTime("datebirth").IsEmptyDate())
                    {
                        retEmp.BirthDate = Strings.Format(rsLoad.Get_Fields("datebirth"), "MM/dd/yyyy");
                    }
                }

                retEmp.City = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("city"));
                retEmp.Designation = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("Desig"));
                retEmp.Email = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("email"));
                retEmp.EmployeeNumber = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("employeenumber"));
                retEmp.FirstName = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("FirstName"));
                retEmp.ID = rsLoad.Get_Fields("id");
                retEmp.LastName = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("lastname"));
                retEmp.MiddleName = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("middlename"));
                retEmp.Phone = rsLoad.Get_Fields("phone");
                if (Information.IsDate(rsLoad.Get_Fields_DateTime("DateRetired")))
                {
                    if (Convert.ToDateTime(rsLoad.Get_Fields("dateretired")).ToOADate() != 0)
                    {
                        retEmp.RetirementDate = Strings.Format(rsLoad.Get_Fields("dateretired"), "MM/dd/yyyy");
                    }
                }
                retEmp.Sex = rsLoad.Get_Fields_String("Sex");
                retEmp.SSN = rsLoad.Get_Fields("ssn");
                // Call rsStates.OpenRecordset("select * from states where id = " & Val(rsLoad.Fields("state")), "twpy0000.vb1")
                // If Not rsStates.EndOfFile Then
                // retEmp.State = rsStates.Fields("state")
                // End If
                if (dictStates.ContainsKey(rsLoad.Get_Fields_Int32("state")))
                {
                    retEmp.State = dictStates[rsLoad.Get_Fields_Int32("state")].ToString();
                }
                retEmp.Status = rsLoad.Get_Fields_String("status");
                if (Information.IsDate(rsLoad.Get_Fields("dateterminated")))
                {
                    if (Convert.ToDateTime(rsLoad.Get_Fields("dateterminated")).ToOADate() != 0)
                    {
                        retEmp.TerminationDate = Strings.Format(rsLoad.Get_Fields("dateterminated"), "MM/dd/yyyy");
                    }
                }
                retEmp.Zip = rsLoad.Get_Fields_String("zip");
                retEmp.Zip4 = rsLoad.Get_Fields_String("zip4");
                retEmp.AdditionalFederal = Conversion.Val(rsLoad.Get_Fields("addfed"));
                retEmp.AdditionalState = Conversion.Val(rsLoad.Get_Fields("addstate"));
                retEmp.BLSWorkSiteID = rsLoad.Get_Fields("blsworksiteid");
                retEmp.Check = rsLoad.Get_Fields_String("Check");
                retEmp.Code1 = rsLoad.Get_Fields_String("Code1");
                retEmp.Code2 = rsLoad.Get_Fields_String("Code2");
                retEmp.ContractActualPeriods = Conversion.Val(rsLoad.Get_Fields("contractactualperiods"));
                retEmp.ContractAmount = rsLoad.Get_Fields_Double("ContractAmount");
                retEmp.ContractPeriods = Conversion.Val(rsLoad.Get_Fields_Double("ContractPeriods"));
                retEmp.CurrentScheduleWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("CurrentScheduleWeek"))));
                retEmp.DeductionPercent = rsLoad.Get_Fields_Boolean("DeductionPercent");
                retEmp.Department = rsLoad.Get_Fields_String("Department");
                retEmp.DepartmentDivision = rsLoad.Get_Fields("DeptDiv");
                retEmp.FederalDollarPercent = rsLoad.Get_Fields_String("FedDollarPercent");
                retEmp.FederalFilingStatusID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("FedFilingStatusID"))));
                retEmp.FederalFirstCheckOnly = rsLoad.Get_Fields_Boolean("boolFedFirstCheckOnly");
                retEmp.FederalStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fedstatus"))));
                retEmp.FrequencyCodeID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("FreqCodeId"))));
                retEmp.GroupID = rsLoad.Get_Fields("GroupID");
                retEmp.HoursPerDay = Conversion.Val(rsLoad.Get_Fields_Double("HrsDay"));
                retEmp.HoursPerWeek = Conversion.Val(rsLoad.Get_Fields_Double("HrsWk"));
                retEmp.IsChildRecord = rsLoad.Get_Fields_Boolean("ChildRecord");
                retEmp.IsFicaExempt = rsLoad.Get_Fields_Boolean("FicaExempt");
                retEmp.IsMedicareExempt = rsLoad.Get_Fields_Boolean("MedicareExempt");
                retEmp.IsMQGE = rsLoad.Get_Fields_Boolean("Mqge");
                retEmp.IsPartTime = rsLoad.Get_Fields_Boolean("ftorpt");
                retEmp.IsUnemploymentExempt = rsLoad.Get_Fields_Boolean("UnemploymentExempt");
                retEmp.IsWorkersCompExempt = rsLoad.Get_Fields_Boolean("WorkersCompExempt");
                retEmp.MSRSType = rsLoad.Get_Fields_String("MSRSType");
                retEmp.PeriodDeductions = rsLoad.Get_Fields_Double("PeriodDeds");
                retEmp.PeriodTaxes = rsLoad.Get_Fields_Double("PeriodTaxes");
                retEmp.PrintPayRate = rsLoad.Get_Fields_Boolean("PrintPayRate");
                retEmp.ScheduleID = rsLoad.Get_Fields_Int32("ScheduleID");
                retEmp.SequenceNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("SeqNumber"))));
                retEmp.StateDollarPercent = rsLoad.Get_Fields_String("StateDollarPercent");
                retEmp.StateFilingStatusID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("StateFilingStatusID"))));
                retEmp.StateFirstCheckOnly = rsLoad.Get_Fields_Boolean("boolStateFirstCheckOnly");
                retEmp.StateStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("StateStatus"))));
                retEmp.UseParentDeductions = rsLoad.Get_Fields_Boolean("UseParentDeductions");
                retEmp.W2DeferredIncome = rsLoad.Get_Fields_Boolean("w2defincome");
                retEmp.W2Pension = rsLoad.Get_Fields_Boolean("w2pen");
                retEmp.W2StatutoryEmployee = rsLoad.Get_Fields_Boolean("W2StatutoryEmployee");
                retEmp.WorkersCompCode = rsLoad.Get_Fields_String("WorkCompCode");
            }
            FillEmployee = retEmp;
            return FillEmployee;
        }

        public cGenericCollection GetEmployeesForEmail()
        {
            cGenericCollection GetEmployeesForEmail = null;
            cGenericCollection emailEmployees = new cGenericCollection();
            clsDRWrapper rsLoad = new clsDRWrapper();
            cEmployee tempEmployee;
            rsLoad.OpenRecordset("select * from tblEmployeemaster where isnull(email,'') <> '' order by employeenumber", "Payroll");
            while (!rsLoad.EndOfFile())
            {
                //App.DoEvents();
                tempEmployee = FillEmployee(ref rsLoad);
                emailEmployees.AddItem(tempEmployee);
                rsLoad.MoveNext();
            }
            rsLoad.Dispose();
            GetEmployeesForEmail = emailEmployees;
            return GetEmployeesForEmail;
        }
        // vbPorter upgrade warning: boolDescending As object	OnWrite(bool)
        public cGenericCollection GetAllEmployees(string strOrderField, object boolDescending)
        {
            cGenericCollection GetAllEmployees = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cGenericCollection empList = new cGenericCollection();
                cEmployee emp;
                string strField = "";
                string strOrderBy = "";
                if (strOrderField != "")
                {
                    strOrderBy = " order by " + strOrderField;
                    if (FCConvert.ToBoolean(boolDescending))
                    {
                        strOrderBy += " desc";
                    }
                }
                rsLoad.OpenRecordset("select * from tblemployeemaster " + strOrderBy, "Payroll");
                while (!rsLoad.EndOfFile())
                {
                    //App.DoEvents();
                    emp = FillEmployee(ref rsLoad);
                    if (!(emp == null))
                    {
                        empList.AddItem(emp);
                    }
                    rsLoad.MoveNext();
                }
                GetAllEmployees = empList;
                return GetAllEmployees;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetAllEmployees;
        }

        public Dictionary<string, cEmployee> GetAllEmployeesAsDictionary()
        {
            Dictionary<string, cEmployee> GetAllEmployeesAsDictionary = null;
            ClearErrors();
            var rsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();


                Dictionary<string, cEmployee> empList = new Dictionary<string, cEmployee>();
                cEmployee emp;
                string strField = "";
                string strOrderBy = "";
                rsLoad.OpenRecordset("select * from tblemployeemaster order by employeenumber", "Payroll");
                while (!rsLoad.EndOfFile())
                {
                    //App.DoEvents();
                    emp = FillEmployee(ref rsLoad);
                    if (!(emp == null))
                    {
                        empList.Add(emp.EmployeeNumber, emp);
                    }

                    rsLoad.MoveNext();
                }

                GetAllEmployeesAsDictionary = empList;


                return GetAllEmployeesAsDictionary;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            finally
            {
                rsLoad.Dispose();
            }
            return GetAllEmployeesAsDictionary;
        }

        public void FillEmployeeDistributions(ref cGenericCollection listEmployees)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cEmployee tempEmp;
                cGenericCollection DistList;
                if (!(listEmployees == null))
                {
                    listEmployees.MoveFirst();
                    while (listEmployees.IsCurrent())
                    {
                        //App.DoEvents();
                        tempEmp = (cEmployee)listEmployees.GetCurrentItem();
                        tempEmp.DistributionRecords.ClearList();
                        DistList = distController.GetDistributionsByEmployeeNumber(tempEmp.EmployeeNumber);
                        if (FCConvert.ToBoolean(distController.HadError))
                        {
                            SetError(distController.LastErrorNumber, distController.LastErrorMessage);
                            return;
                        }
                        if (DistList.ItemCount() > 0)
                        {
                            DistList.MoveFirst();
                            while (DistList.IsCurrent())
                            {
                                //App.DoEvents();
                                tempEmp.DistributionRecords.AddItem(DistList.GetCurrentItem());
                                DistList.MoveNext();
                            }
                            DistList.ClearList();
                            DistList = null;
                        }
                        listEmployees.MoveNext();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
        }

        public bool AllEmployeesPermissable(int lngUserID)
        {
            bool AllEmployeesPermissable = false;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    bool boolReturn;
                    boolReturn = true;
                    rsLoad.OpenRecordset(
                        "select top 1 * from payrollpermissions where [userid] = " + FCConvert.ToString(lngUserID),
                        "Payroll");
                    if (!rsLoad.EndOfFile())
                    {
                        boolReturn = false;
                    }

                    AllEmployeesPermissable = boolReturn;
                }

                return AllEmployeesPermissable;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return AllEmployeesPermissable;
        }
        // vbPorter upgrade warning: strEmployeeNumber As object	OnWrite(string)
        public bool IsEmployeePermissable(int lngUserID, string strEmployeeNumber)
        {
            bool IsEmployeePermissable = false;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                Dictionary<string, cEmployee> listEmps = new Dictionary<string, cEmployee>();
                bool boolReturn;
                boolReturn = false;
                listEmps = GetEmployeesPermissableAsDictionary(lngUserID, "EmployeeNumber", false);
                if (listEmps.ContainsKey(strEmployeeNumber))
                {
                    boolReturn = true;
                }
                listEmps.Clear();
                listEmps = null;
                IsEmployeePermissable = boolReturn;
                return IsEmployeePermissable;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return IsEmployeePermissable;
        }

        public Dictionary<string, cEmployee> GetEmployeesPermissableAsDictionary(int lngUserID, string strOrderField, bool boolDescending)
        {
            Dictionary<string, cEmployee> GetEmployeesPermissableAsDictionary = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                clsDRWrapper rsLoad = new clsDRWrapper();
                cEmployee empl;
                Dictionary<string, cEmployee> listEmps = new Dictionary<string, cEmployee>();
                listEmps = GetAllEmployeesAsDictionary();
                if (!FCConvert.ToBoolean(HadError))
                {
                    Dictionary<string, cEmployee> listPermissable = new Dictionary<string, cEmployee>();
                    Dictionary<object, object> dictDeptDivs = new Dictionary<object, object>();
                    Dictionary<object, object> dictGroups = new Dictionary<object, object>();
                    Dictionary<object, object> dictEmployeeNumbers = new Dictionary<object, object>();
                    Dictionary<object, object> dictSequences = new Dictionary<object, object>();
                    Dictionary<object, object> dictDepartment = new Dictionary<object, object>();
                    rsLoad.OpenRecordset("Select * from payrollpermissions where [userid] = " + FCConvert.ToString(lngUserID), "Payroll");
                    if (!rsLoad.EndOfFile())
                    {
                        while (!rsLoad.EndOfFile())
                        {
                            //App.DoEvents();
                            switch (rsLoad.Get_Fields_Int32("type"))
                            {
                                case CNSTPYTYPEDEPTDIV:
                                    {
                                        dictDeptDivs.Add(rsLoad.Get_Fields("val"), true);
                                        break;
                                    }
                                case CNSTPYTYPEGROUP:
                                    {
                                        dictGroups.Add(rsLoad.Get_Fields("val"), true);
                                        break;
                                    }
                                case CNSTPYTYPEIND:
                                    {
                                        dictEmployeeNumbers.Add(rsLoad.Get_Fields("val"), true);
                                        break;
                                    }
                                case CNSTPYTYPESEQ:
                                    {
                                        dictSequences.Add(rsLoad.Get_Fields_String("Val"), true);
                                        break;
                                    }
                                case CNSTPYTYPEDEPARTMENT:
                                    {
                                        dictDepartment.Add(rsLoad.Get_Fields_String("Val"), true);
                                        break;
                                    }
                            }
                            //end switch
                            rsLoad.MoveNext();
                        }
                        // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
                        int x;
                        // listEmps.MoveFirst
                        cEmployee tempEmp;
                        // Do While listEmps.IsCurrent
                        // vbPorter upgrade warning: empArray As object	OnRead(cEmployee)
                        cEmployee[] empArray = new cEmployee[listEmps.Count];
                        int i = 0;
                        foreach (var item in listEmps.Values)
                        {
                            empArray[i] = item;
                            i++;
                        }
                        for (x = 0; x <= (Information.UBound(empArray, 1)); x++)
                        {
                            //App.DoEvents();
                            // Set tempEmp = listEmps.GetCurrentItem
                            tempEmp = empArray[x];
                            if (!dictDeptDivs.ContainsKey(tempEmp.DepartmentDivision))
                            {
                                if (!dictGroups.ContainsKey(tempEmp.GroupID))
                                {
                                    if (!dictEmployeeNumbers.ContainsKey(tempEmp.EmployeeNumber))
                                    {
                                        if (!dictSequences.ContainsKey(tempEmp.SequenceNumber))
                                        {
                                            if (!dictDepartment.ContainsKey(tempEmp.Department))
                                            {
                                                listPermissable.Add(tempEmp.EmployeeNumber, tempEmp);
                                            }
                                        }
                                    }
                                }
                            }
                            // listEmps.MoveNext
                            // Loop
                        }
                        listEmps.Clear();
                        GetEmployeesPermissableAsDictionary = listPermissable;
                    }
                    else
                    {
                        GetEmployeesPermissableAsDictionary = listEmps;
                    }
                }
                return GetEmployeesPermissableAsDictionary;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetEmployeesPermissableAsDictionary;
        }

        public cGenericCollection GetEmployeesPermissable(int lngUserID, string strOrderField, bool boolDescending)
        {
            cGenericCollection GetEmployeesPermissable = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    cEmployee empl;
                    cGenericCollection listEmps;
                    listEmps = GetAllEmployees(strOrderField, boolDescending);
                    if (!FCConvert.ToBoolean(HadError))
                    {
                        cGenericCollection listPermissable = new cGenericCollection();
                        Dictionary<object, object> dictDeptDivs = new Dictionary<object, object>();
                        Dictionary<object, object> dictGroups = new Dictionary<object, object>();
                        Dictionary<object, object> dictEmployeeNumbers = new Dictionary<object, object>();
                        Dictionary<object, object> dictSequences = new Dictionary<object, object>();
                        Dictionary<object, object> dictDepartment = new Dictionary<object, object>();
                        rsLoad.OpenRecordset(
                            "Select * from payrollpermissions where [userid] = " + FCConvert.ToString(lngUserID),
                            "Payroll");
                        if (!rsLoad.EndOfFile())
                        {
                            while (!rsLoad.EndOfFile())
                            {
                                //App.DoEvents();
                                if (Conversion.Val(rsLoad.Get_Fields("type")) == CNSTPYTYPEDEPTDIV)
                                {
                                    dictDeptDivs.Add(rsLoad.Get_Fields("val"), true);
                                }
                                else if (Conversion.Val(rsLoad.Get_Fields("type")) == CNSTPYTYPEGROUP)
                                {
                                    dictGroups.Add(rsLoad.Get_Fields("val"), true);
                                }
                                else if (Conversion.Val(rsLoad.Get_Fields("type")) == CNSTPYTYPEIND)
                                {
                                    dictEmployeeNumbers.Add(rsLoad.Get_Fields("val"), true);
                                }
                                else if (Conversion.Val(rsLoad.Get_Fields("type")) == CNSTPYTYPESEQ)
                                {
                                    dictSequences.Add(rsLoad.Get_Fields_String("Val"), true);
                                }
                                else if (Conversion.Val(rsLoad.Get_Fields("type")) == CNSTPYTYPEDEPARTMENT)
                                {
                                    dictDepartment.Add(rsLoad.Get_Fields_String("Val"), true);
                                }

                                rsLoad.MoveNext();
                            }

                            listEmps.MoveFirst();
                            cEmployee tempEmp;
                            while (listEmps.IsCurrent())
                            {
                                //App.DoEvents();
                                tempEmp = (cEmployee) listEmps.GetCurrentItem();
                                if (!dictDeptDivs.ContainsKey(tempEmp.DepartmentDivision))
                                {
                                    if (!dictGroups.ContainsKey(tempEmp.GroupID))
                                    {
                                        if (!dictEmployeeNumbers.ContainsKey(tempEmp.EmployeeNumber))
                                        {
                                            if (!dictSequences.ContainsKey(tempEmp.SequenceNumber))
                                            {
                                                if (!dictDepartment.ContainsKey(tempEmp.Department))
                                                {
                                                    listPermissable.AddItem(tempEmp);
                                                }
                                            }
                                        }
                                    }
                                }

                                listEmps.MoveNext();
                            }

                            listEmps.ClearList();
                            GetEmployeesPermissable = listPermissable;
                        }
                        else
                        {
                            GetEmployeesPermissable = listEmps;
                        }
                    }
                }

                return GetEmployeesPermissable;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetEmployeesPermissable;
        }

        public cGenericCollection GetDepartmentsUsed()
        {
            cGenericCollection GetDepartmentsUsed = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cGenericCollection collDepartments = new cGenericCollection();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select distinct [Department] from tblemployeemaster where not isnull([Department],'') = '' order by [Department]",
                        "Payroll");
                    while (!rsLoad.EndOfFile())
                    {
                        collDepartments.AddItem(rsLoad.Get_Fields_String("Department"));
                        rsLoad.MoveNext();
                    }
                }

                GetDepartmentsUsed = collDepartments;
                return GetDepartmentsUsed;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetDepartmentsUsed;
        }

        public cGenericCollection GetGroupIDsUsed()
        {
            cGenericCollection GetGroupIDsUsed = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cGenericCollection collGroups = new cGenericCollection();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "Select distinct [GroupID] from tblemployeemaster where not isnull([GroupID],'') =  '' order by [GroupID]",
                        "Payroll");
                    while (!rsLoad.EndOfFile())
                    {
                        collGroups.AddItem(FCConvert.ToString(rsLoad.Get_Fields("GroupID")));
                        rsLoad.MoveNext();
                    }
                }

                GetGroupIDsUsed = collGroups;
                return GetGroupIDsUsed;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetGroupIDsUsed;
        }

        public cGenericCollection GetCodeOnesUsed()
        {
            cGenericCollection GetCodeOnesUsed = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cGenericCollection collCodes = new cGenericCollection();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select distinct [Code1] from tblemployeemaster where not isnull([Code1],'') = '' order by [Code1]",
                        "Payroll");
                    while (!rsLoad.EndOfFile())
                    {
                        collCodes.AddItem(rsLoad.Get_Fields_String("Code1"));
                        rsLoad.MoveNext();
                    }
                }

                GetCodeOnesUsed = collCodes;
                return GetCodeOnesUsed;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetCodeOnesUsed;
        }

        public cGenericCollection GetCodeTwosUsed()
        {
            ClearErrors();
            cGenericCollection collCodes = new cGenericCollection();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select distinct [Code2] from tblemployeemaster where not isnull([Code2],'') = '' order by [Code2]",
                        "Payroll");
                    while (!rsLoad.EndOfFile())
                    {
                        collCodes.AddItem(rsLoad.Get_Fields_String("Code2"));
                        rsLoad.MoveNext();
                    }
                }

                return collCodes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return collCodes;
        }

        public cGenericCollection GetSequenceNumbersUsed()
        {
            cGenericCollection GetSequenceNumbersUsed = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cGenericCollection collSequences = new cGenericCollection();
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select distinct [SeqNumber] from tblemployeemaster where isnull([SeqNumber],0) > 0 order by [SeqNumber]",
                        "Payroll");
                    while (!rsLoad.EndOfFile())
                    {
                        collSequences.AddItem(FCConvert.ToString(rsLoad.Get_Fields("SeqNumber")));
                        rsLoad.MoveNext();
                    }
                }

                GetSequenceNumbersUsed = collSequences;
                return GetSequenceNumbersUsed;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            return GetSequenceNumbersUsed;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        public void Disposing(bool disposing)
        {
            if (disposing)
            {
                distController = null;
                dictStates?.Clear();
                dictStates = null;
            }
        }
    }
}
