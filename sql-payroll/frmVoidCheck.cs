//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmVoidCheck : BaseForm
	{
		public frmVoidCheck()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVoidCheck InstancePtr
		{
			get
			{
				return (frmVoidCheck)Sys.GetInstance(typeof(frmVoidCheck));
			}
		}

		protected frmVoidCheck _InstancePtr = null;
		//=========================================================
		public int lngCheckNumber;
		public int lngJournalNumber;
		private int lngPayrunID;
		// vbPorter upgrade warning: pdatCheckDate As DateTime	OnWrite(string)
		private DateTime pdatCheckDate;
		private int lngPeriodNumber;

		private void frmVoidCheck_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmVoidCheck_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				cmbAccountingPeriod.Clear();
				int x;
				for (x = 1; x <= 12; x++)
				{
					cmbAccountingPeriod.AddItem(x.ToString());
				}
				// x
				if (modGlobalConstants.Statics.gboolBD)
				{
					lblAccountingPeriod.Visible = true;
					cmbAccountingPeriod.Visible = true;
					cmbAccountingPeriod.SelectedIndex = DateTime.Today.Month - 1;
				}
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmVoidCheck_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmVoidCheck_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			if (!Information.IsDate(txtPayDate.Text))
				return;
			vsData.Clear();
			vsData.Rows = 1;
			vsData.Cols = 4;
			vsData.ColHidden(0, true);
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.TextMatrix(0, 1, "Check Number");
			vsData.TextMatrix(0, 2, "Pay Date");
			vsData.TextMatrix(0, 3, "Pay Run ID");
			// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
			rsData.OpenRecordset("Select distinct CheckNumber, PayDate, PayRunID from tblCheckDetail where PayDate = '" + txtPayDate.Text + "' and TrustRecord = 1 AND CheckVoid = 0 Order by CheckNumber", "TWPY0000.vb1");
			vsData.Rows = rsData.RecordCount() + 1;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				// vsData.TextMatrix(intCounter, 0) = .Fields("ID")
				vsData.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("CheckNumber")));
				vsData.TextMatrix(intCounter, 2, Strings.Format(rsData.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy"));
				vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields("PayRunID")));
                //FC:FINAL:BSE #4134 ColWidth should not be done with autosize 
                vsData.ColWidth(0, 0);
                vsData.ColWidth(1, 1700);
                vsData.ColWidth(2, 1700);
                vsData.ColWidth(3, 1700);
				vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, intCounter, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				rsData.MoveNext();
			}
		}

		private void frmVoidCheck_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(0, this.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(this.WidthOriginal * 0.3));
			vsData.ColWidth(2, FCConvert.ToInt32(this.WidthOriginal * 0.15));
			vsData.ColWidth(3, FCConvert.ToInt32(this.WidthOriginal * 0.15));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			DateTime dtTemp1;
			DateTime dtTemp2;
			DateTime dtDate;
			dtTemp1 = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(DateTime.Today);
			dtTemp1 = fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp1);
			dtTemp1 = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(dtTemp1);
			dtDate = dtTemp1;
			lngPeriodNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbAccountingPeriod.Text)));
			if (cmbType.Text == "Regular")
			{
				string strTempEmployeeNumber = "";
				if (FCConvert.ToBoolean(modGlobalRoutines.FormsExist(this)))
				{
					return;
				}
				strTempEmployeeNumber = modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber;
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = FCConvert.ToString(0);
				// clears out the last opened form so that none open when
				// a new employee has been chosen
				modGlobalVariables.Statics.gstrEmployeeScreen = string.Empty;
				frmEmployeeSearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber == "")
				{
				}
				else
				{
					frmCheckReturn.InstancePtr.Init(lngPeriodNumber);
					// frmCheckReturn.Show 1, MDIParent
				}
				modGlobalVariables.Statics.gtypeCurrentEmployee.EmployeeNumber = strTempEmployeeNumber;
			}
			else
			{
				if (Information.IsDate(txtPayDate.Text))
				{
					if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, FCConvert.ToDateTime(txtPayDate.Text)) >= 0)
					{
						vsData.Visible = true;
						lblHelp.Visible = true;
						LoadGrid();
					}
					else
					{
						MessageBox.Show("Only checks from the current or previous fiscal year can be voided", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					MessageBox.Show("Invalid Pay Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			lblPayDate.Visible = Index == 1;
			txtPayDate.Visible = Index == 1;
			vsData.Visible = false;
			lblHelp.Visible = false;
			if (Index == 1)
				txtPayDate.Focus();
			switch (Index)
			{
				case 0:
					{
						cmdProcess.Text = "Process";
						lblHelp.Visible = false;
						break;
					}
				case 1:
					{
						cmdProcess.Text = "Load";
						break;
					}
			}
			//end switch
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		private void vsData_DblClick(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			string strDBToUse = "";
			bool boolPreviousFiscalYear = false;
			DateTime dtDate;
			clsDRWrapper rsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (vsData.MouseRow > 0)
				{
					pdatCheckDate = FCConvert.ToDateTime(fecherFoundation.Strings.Trim(FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 2))));
					dtDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(DateTime.Today);
					if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, pdatCheckDate) < 0)
					{
                        rsTemp.OpenRecordset("select * from standardaccounts where code = 'AP'", "twbd0000.vb1");
                        if (!rsTemp.EndOfFile())
                        {
                            if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Account")))) != 0)
                            {
                                // do nothing
                            }
                            else
                            {
                                MessageBox.Show("Before you may void a check from a previous fiscal year you must set up your Town EOY Accounts Payable Account in Budgetary System > File Maintenance > G/L Ctrl / Stand Accts.", "Invalid Town EOY Accounts Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                return;
                            }
                        }

					}
					// VOID THE CHECK DETAIL TABLE  FOR THE CHOSEN CHECK
					rsData.Execute("Update tblCheckDetail Set CheckVoid = 1 where CheckNumber = " + vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 1) + " and PayDate = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 2))) + "' AND PayRunID = " + vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 3) + " AND TrustRecord = 1", "TWPY0000.vb1");
					// DAVE CALL YOUR ACCOUNTING SUMMARY REPORT FROM HERE
					lngCheckNumber = FCConvert.ToInt32(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 1));
					pdatCheckDate = FCConvert.ToDateTime(fecherFoundation.Strings.Trim(FCConvert.ToString(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 2))));
					lngPayrunID = FCConvert.ToInt32(vsData.Cell(FCGrid.CellPropertySettings.flexcpText, vsData.MouseRow, 3));
					// Create Reversing Entry for Budgetary
					boolPreviousFiscalYear = false;
					strDBToUse = "";
					if (modGlobalConstants.Statics.gboolBD)
					{
						dtDate = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(DateTime.Today);
						if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, pdatCheckDate) < 0)
						{
							boolPreviousFiscalYear = true;
							// find the budgetary database
							rsData.OpenRecordset("select top 1 * from archives where archivetype = 'Archive' order by archiveid desc", "SystemSettings");
							if (!rsData.EndOfFile())
							{
								strDBToUse = "Archive_" + rsData.Get_Fields("archiveID");
							}
							// With MDIParent.CommonDialog1
							// .DialogTitle = "Open Prior Year Database"
							// .Filter = "Budgetary Database|twbd0000.vb1"
							// .DefaultExt = "vb1"
							// .CancelError = True
							// .InitDir = CurDir
							// .FLAGS = cdlOFNExplorer + cdlOFNFileMustExist + cdlOFNLongNames + cdlOFNNoChangeDir
							// .ShowOpen
							// Dim fso As New FCFCFileSystem
							// strDBToUse = Path.GetDirectoryName(.FileName)
							// End With
						}
						GetJournalNumber(strDBToUse);
					}
					// Void Check out of Budgetary Check Rec Table
					if (modGlobalConstants.Statics.gboolBD)
					{
						if (boolPreviousFiscalYear)
						{
							rsCheckRec.GroupName = strDBToUse;
						}
						rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Type = '2' AND CheckNumber = " + FCConvert.ToString(lngCheckNumber), "TWBD0000.vb1");
						if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
						{
							rsCheckRec.Edit();
							rsCheckRec.Set_Fields("Status", "V");
							rsCheckRec.Update();
						}
						rsCheckRec.GroupName = "Live";
					}
					// Show Report With Reversed Charges
					rptPayrollAccountingChargesTrustReversal.InstancePtr.Init(ref lngCheckNumber, ref pdatCheckDate, ref lngPayrunID, ref boolPreviousFiscalYear, this.Modal);
					// CREATE A COPY OF THE RECORDS TO BE VOIDED
					// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
					int x;
					string strFields = "";
					rsCheckRec = new clsDRWrapper();
					rsCheckRec.OpenRecordset("SELECT * FROM tblCheckDetail");
					strFields = "";
					for (x = 0; x <= (rsCheckRec.FieldsCount - 1); x++)
					{
						if (rsCheckRec.Get_FieldsIndexName(x) == "ID")
						{
						}
						else
						{
							strFields += rsCheckRec.Get_FieldsIndexName(x) + ", ";
						}
					}
					strFields = Strings.Left(strFields, strFields.Length - 2);
					rsCheckRec.Execute("INSERT INTO tblCheckReturn (" + strFields + ") SELECT " + strFields + " From tblCheckDetail WHERE CheckNumber = " + FCConvert.ToString(lngCheckNumber), "Payroll");
					MessageBox.Show("Completed Successfully");
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 32755)
				{
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtPayDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtPayDate.Text.Length == 2 || txtPayDate.Text.Length == 5) && Strings.Right(txtPayDate.Text, 1) != "/")
        //	{
        //		txtPayDate.Text = txtPayDate.Text + "/";
        //		txtPayDate.SelectionStart = txtPayDate.Text.Length;
        //		txtPayDate.SelectionLength = txtPayDate.Text.Length;
        //	}
        //}

        //private void txtPayDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtPayDate.SelectionStart = 0;
        //	txtPayDate.SelectionLength = 10;
        //}

        //private void txtPayDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if (txtPayDate.SelectedText.Length == 10)
        //		txtPayDate.Text = string.Empty;
        //	if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
        //	{
        //		KeyAscii = (Keys)0;
        //	}
        //	if (fecherFoundation.Strings.Trim(txtPayDate.Text).Length >= 10 && KeyAscii != Keys.Back)
        //	{
        //		KeyAscii = (Keys)0;
        //		return;
        //	}
        //	if (KeyAscii == Keys.Back && Strings.Right(txtPayDate.Text, 1) == "/")
        //	{
        //		txtPayDate.Text = Strings.Left(txtPayDate.Text, txtPayDate.Text.Length - 2);
        //		txtPayDate.SelectionStart = txtPayDate.Text.Length;
        //		txtPayDate.SelectionLength = txtPayDate.Text.Length;
        //		KeyAscii = (Keys)0;
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void GetJournalNumber(string strDBToUse = "")
		{
			// - "AutoDim"
			int intArrayID = 0;
			string strAccount = "";
			// vbPorter upgrade warning: dblValue As double	OnWrite(double, string)
			double dblValue = 0;
			clsDRWrapper rspayrollaccounts = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			double dblCashAmount = 0;
			string strTemp = "";
			// NOW ADD THE DATA TO BUDGETARY
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				lngPeriodNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbAccountingPeriod.Text)));
				modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				intArrayID = 0;
				modAccountTitle.SetAccountFormats();
				// GET THE TRUST RECORDS FROM
				rsData.OpenRecordset("Select * from tblCheckDetail where CheckNumber = " + FCConvert.ToString(lngCheckNumber) + " and PayDate = '" + FCConvert.ToString(pdatCheckDate) + "' AND PayRunID = " + FCConvert.ToString(lngPayrunID) + " AND TrustRecord = 1 ORDER BY TrustDeductionAccountNumber", "Payroll");
				if (!rsData.EndOfFile())
				{
					modGlobalVariables.Statics.gintUseGroupMultiFund = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields_Int32("MultiFundNumber"))));
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("TrustDeductionAccountNumber"));
					for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("TrustDeductionAccountNumber")))
						{
							dblValue -= FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Double("TrustAmount"), "0.00"));
						}
						else
						{
							if (dblValue == 0)
							{
							}
							else
							{
								// MATTHEW 8/16/2005 CALL ID 75083
								// If intArrayID <> 0 Then
								// If AccountStrut(intArrayID).Account <> "" Then
								// ReDim Preserve AccountStrut(UBound(AccountStrut) + 1)
								// End If
								// Else
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
                                // End If
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                                modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                                modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
								dblCashAmount += (dblValue * -1);
								intArrayID += 1;
							}
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Double("TrustAmount") * -1, "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("TrustDeductionAccountNumber"));
						}
						rsData.MoveNext();
					}
					// save the last record
					if (dblValue == 0)
					{
					}
					else
					{
						if (intArrayID == 0)
						{
							if (modGlobalVariables.Statics.AccountStrut[intArrayID].Account != "")
							{
								Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
							}
						}
						else
						{
							Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						}
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						dblCashAmount += (dblValue * -1);
						intArrayID += 1;
					}
				}
				// NOW GET THE CHECKING SUM RECORD
				rspayrollaccounts.OpenRecordset("SELECT * FROM tblPayrollAccounts");
				rspayrollaccounts.FindFirstRecord("Description", "'Cash / Checking'");
				if (rspayrollaccounts.NoMatch)
				{
				}
				else
				{
					if (dblValue == 0)
					{
					}
					else
					{
						// this record is Medicare
						Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
						if (modGlobalVariables.Statics.gintUseGroupMultiFund == 0)
						{
                            modGlobalVariables.Statics.AccountStrut[intArrayID].Account = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
						}
						else
						{
							strTemp = FCConvert.ToString(rspayrollaccounts.Get_Fields("Account"));
							// corey 01/03/2006  Need to format the fund in case it is two digit fund
							if (Strings.Left(fecherFoundation.Strings.Trim(strTemp), 2) == "**")
							{
								Strings.MidSet(ref strTemp, 5, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
							}
							else
							{
								Strings.MidSet(ref strTemp, 3, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))), Strings.Format(modGlobalVariables.Statics.gintUseGroupMultiFund, Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")));
							}
                            modGlobalVariables.Statics.AccountStrut[intArrayID].Account = strTemp;
						}
                        modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblCashAmount, "0.00"));
                        modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " Void Ck " + FCConvert.ToString(lngCheckNumber);
						intArrayID += 1;
					}
				}
				// ****************************************************************************************
				// matthew 10/20/2004
				if (modGlobalConstants.Statics.gboolBD)
				{
					// this sets up the cash accounts for the funds if the user has BD
					modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
				}
				else
				{
					// create a cash entry into the account M CASH for these accounts
					modGlobalVariables.Statics.ftFundArray = modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
				}
				// Dave says should be safe to always do this 02/16/2006
				// If gintUseGroupMultiFund = 0 Then
				if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(DateTime.Today, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, 0, true))
				{
				}
				// End If
				// ****************************************************************************************
				lngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modGlobalVariables.Statics.AccountStrut, "PY", 0, pdatCheckDate, Strings.Format(pdatCheckDate, "MM/dd/yyyy") + " - Void " + FCConvert.ToString(lngCheckNumber), lngPeriodNumber, false, -1, strDBToUse);
			}
			// lngPeriodNumber = Left(Format(gdatCurrentPayDate, "MM/dd/yyyy"), 2)
		}
	}
}
