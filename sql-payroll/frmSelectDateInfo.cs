//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmSelectDateInfo : BaseForm
	{
		public frmSelectDateInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			lblDateSelect = new System.Collections.Generic.List<FCLabel>();
			lblDateSelect.AddControlArrayElement(lblDateSelect_4, 4);
			lblDateSelect.AddControlArrayElement(lblDateSelect_3, 3);
			lblDateSelect.AddControlArrayElement(lblDateSelect_2, 2);
			lblDateSelect.AddControlArrayElement(lblDateSelect_1, 1);
			lblDateSelect.AddControlArrayElement(lblDateSelect_0, 0);
			cboDateSelect = new System.Collections.Generic.List<FCComboBox>();
			cboDateSelect.AddControlArrayElement(cboDateSelect_2, 2);
			cboDateSelect.AddControlArrayElement(cboDateSelect_1, 1);
			cboDateSelect.AddControlArrayElement(cboDateSelect_0, 0);

		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSelectDateInfo InstancePtr
		{
			get
			{
				return (frmSelectDateInfo)Sys.GetInstance(typeof(frmSelectDateInfo));
			}
		}

		protected frmSelectDateInfo _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         3/19/2003
		// This form will be used to get year, quarter, and month
		// information in any place in payroll where it is required
		// ********************************************************
		bool blnReturnYear;
		bool blnReturnQuarter;
		bool blnReturnMonth;
		bool blnReturnDay;
		// vbPorter upgrade warning: lngSelectedYear As int	OnWrite(int, string)
		int lngSelectedYear;
		// vbPorter upgrade warning: intSelectedQuarter As int	OnWrite(int, string)
		int intSelectedQuarter;
		// vbPorter upgrade warning: intSelectedMonth As int	OnWrite(int, string)
		int intSelectedMonth;
		// vbPorter upgrade warning: datSelectedDay As DateTime	OnWrite(int, string)
		DateTime datSelectedDay;
		// vbPorter upgrade warning: intSelectedPayRunID As int	OnWriteFCConvert.ToInt32(
		int intSelectedPayRunID;
		bool boolCheckRegister;

		private void cboPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			rsPayDateInfo.OpenRecordset("SELECT DISTINCT PayRunID FROM tblCheckDetail WHERE PayDate = '" + cboPayDate.Text + "' And convert(int, isnull(payrunid, 0)) > 0 ORDER BY PayRunID");
			cboPayRunID.Clear();
			if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPayRunID.AddItem(FCConvert.ToString(rsPayDateInfo.Get_Fields("PayRunID")));
					rsPayDateInfo.MoveNext();
				}
				while (rsPayDateInfo.EndOfFile() != true);
				cboPayRunID.SelectedIndex = 0;
			}
		}

		private void cboPayDateYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsPayDateInfo = new clsDRWrapper();
			if (!boolCheckRegister)
			{
				rsPayDateInfo.OpenRecordset("SELECT DISTINCT PayDate FROM tblCheckDetail WHERE isnull(AdjustRecord, 0) = 0 AND Year(PayDate) = " + cboPayDateYear.Text + " ORDER BY PayDate DESC");
			}
			else
			{
				rsPayDateInfo.OpenRecordset("SELECT DISTINCT PayDate FROM tblpayrollsteps WHERE checkregister = 1 AND Year(PayDate) = " + cboPayDateYear.Text + " ORDER BY PayDate DESC");
			}
			cboPayDate.Clear();
			cboPayRunID.Clear();
			if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPayDate.AddItem(Strings.Format(rsPayDateInfo.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy"));
					rsPayDateInfo.MoveNext();
				}
				while (rsPayDateInfo.EndOfFile() != true);
				cboPayDate.SelectedIndex = 0;
			}
		}

		private void chkAllPayRuns_CheckedChanged(object sender, System.EventArgs e)
		{
			// If chkAllPayRuns Then
			// chkPrintOnlyDate = False
			// chkPrintOnlyDate.Visible = False
			// Else
			// chkPrintOnlyDate = False
			// chkPrintOnlyDate.Visible = True
			// End If
		}

		private void frmSelectDateInfo_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSelectDateInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectDateInfo properties;
			//frmSelectDateInfo.FillStyle	= 0;
			//frmSelectDateInfo.ScaleWidth	= 3285;
			//frmSelectDateInfo.ScaleHeight	= 2820;
			//frmSelectDateInfo.LinkTopic	= "Form2";
			//frmSelectDateInfo.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			chkAllPayRuns.CheckState = (modGlobalConstants.Statics.gboolPrintALLPayRuns ? CheckState.Checked : CheckState.Unchecked);
		}

		private void frmSelectDateInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuProcessQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
				modGlobalVariables.Statics.gboolCancelSelected = true;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			if (blnReturnYear)
			{
				lngSelectedYear = -1;
			}
			if (blnReturnQuarter)
			{
				intSelectedQuarter = -1;
			}
			if (blnReturnMonth)
			{
				intSelectedMonth = -1;
			}
			if (blnReturnDay)
			{
				datSelectedDay = DateTime.FromOADate(0);
			}
			modGlobalVariables.Statics.gboolCancelSelected = true;
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
		// vbPorter upgrade warning: datPayDate As DateTime	OnWrite(int, DateTime)
        public void Init2(string strFormCaption, int lngYear, int intQuarter, int intMonth, ref DateTime tempdatPayDate, ref int intPayRunID, bool boolFromCheckRegister)
        {           
            Init(strFormCaption, ref lngYear, ref intQuarter, ref intMonth, ref tempdatPayDate, ref intPayRunID, boolFromCheckRegister);
        }

        public void Init3(string strFormCaption, ref int lngYear, ref int intQuarter, int intMonth, ref DateTime tempdatPayDate, int intPayRunID, bool boolFromCheckRegister)
        {           
            Init(strFormCaption, ref lngYear, ref intQuarter, ref intMonth, ref tempdatPayDate, ref intPayRunID, boolFromCheckRegister);
        }
        public void Init4(string strFormCaption, ref int lngYear, int intQuarter, int intMonth, ref DateTime tempdatPayDate, int intPayRunID, bool boolFromCheckRegister)
        {
            Init(strFormCaption, ref lngYear, ref intQuarter, ref intMonth, ref tempdatPayDate, ref intPayRunID, boolFromCheckRegister);
        }
        public void Init5(string strFormCaption, ref int lngYear, int intQuarter, ref int intMonth, ref DateTime tempdatPayDate, int intPayRunID, bool boolFromCheckRegister)
        {
            Init(strFormCaption, ref lngYear, ref intQuarter, ref intMonth, ref tempdatPayDate, ref intPayRunID, boolFromCheckRegister);
        }
        public void Init(string strFormCaption, ref int lngYear /*= -1*/, ref int intQuarter /*= -1*/, ref int intMonth /*= -1*/, ref DateTime datPayDate /*= DateTime.FromOADate(0)*/, ref int intPayRunID /*= -1*/, bool boolFromCheckRegister = false)
		{
			int intindex;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			clsDRWrapper rsPayDateYear = new clsDRWrapper();
			boolCheckRegister = boolFromCheckRegister;
			blnReturnMonth = intMonth != -1;
			blnReturnQuarter = intQuarter != -1;
			blnReturnYear = lngYear != -1;
			blnReturnDay = datPayDate.ToOADate() != 0;
			intindex = 0;
			this.Text = strFormCaption;
			if (blnReturnDay)
			{
				cboPayDateYear.Clear();
				if (!boolFromCheckRegister)
				{
					rsPayDateYear.OpenRecordset("SELECT * FROM (SELECT DISTINCT Year(PayDate) as PayYear FROM tblCheckDetail) as Temp ORDER BY PayYear");
				}
				else
				{
					rsPayDateYear.OpenRecordset("SELECT  Year(PayDate) as PayYear FROM tblpayrollsteps where checkregister = 1 group by year(paydate) ORDER BY year(paydate)");
				}
				if (rsPayDateYear.EndOfFile() != true && rsPayDateYear.BeginningOfFile() != true)
				{
					do
					{
						cboPayDateYear.AddItem(FCConvert.ToString(rsPayDateYear.Get_Fields("PayYear")));
						rsPayDateYear.MoveNext();
					}
					while (rsPayDateYear.EndOfFile() != true);
					cboPayDateYear.SelectedIndex = cboPayDateYear.Items.Count - 1;
					Frame1.Visible = true;
				}
				else
				{
					datPayDate = DateTime.FromOADate(0);
					Close();
					return;
				}
			}
			else
			{
				if (blnReturnYear)
				{
					lblDateSelect[intindex].Visible = true;
					cboDateSelect[intindex].Visible = true;
					lblDateSelect[intindex].Text = "Select Year";
					cboDateSelect[intindex].Clear();
					for (counter = (DateTime.Now.Year - 5); counter <= (DateTime.Now.Year); counter++)
					{
						cboDateSelect[intindex].AddItem(counter.ToString());
					}
					cboDateSelect[intindex].SelectedIndex = cboDateSelect[intindex].Items.Count - 1;
					intindex += 1;
				}
				if (blnReturnQuarter)
				{
					lblDateSelect[intindex].Visible = true;
					cboDateSelect[intindex].Visible = true;
					lblDateSelect[intindex].Text = "Select Quarter";
					cboDateSelect[intindex].Clear();
					for (counter = 1; counter <= 4; counter++)
					{
						cboDateSelect[intindex].AddItem(counter.ToString());
					}
					if (DateTime.Now.Month <= 3)
					{
						cboDateSelect[intindex].SelectedIndex = 0;
					}
					else if (DateTime.Now.Month > 3 && DateTime.Now.Month <= 6)
					{
						cboDateSelect[intindex].SelectedIndex = 1;
					}
					else if (DateTime.Now.Month > 6 && DateTime.Now.Month <= 9)
					{
						cboDateSelect[intindex].SelectedIndex = 2;
					}
					else
					{
						cboDateSelect[intindex].SelectedIndex = 3;
					}
					intindex += 1;
				}
				if (blnReturnMonth)
				{
					lblDateSelect[intindex].Visible = true;
					cboDateSelect[intindex].Visible = true;
					lblDateSelect[intindex].Text = "Select Month";
					cboDateSelect[intindex].Clear();
					for (counter = 1; counter <= 12; counter++)
					{
						cboDateSelect[intindex].AddItem(counter.ToString());
					}
					cboDateSelect[intindex].SelectedIndex = DateTime.Now.Month - 1;
					intindex += 1;
				}
			}
			modGlobalVariables.Statics.gboolCancelSelected = false;
			frmSelectDateInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			// EXIT BU
			if (modGlobalVariables.Statics.gboolCancelSelected)
			{
				return;
			}
			if (blnReturnDay)
			{
				datPayDate = datSelectedDay;
				if (intSelectedPayRunID < 1)
				{
					intSelectedPayRunID = 1;
				}
				intPayRunID = intSelectedPayRunID;
			}
			if (blnReturnYear)
			{
				lngYear = lngSelectedYear;
			}
			if (blnReturnQuarter)
			{
				intQuarter = intSelectedQuarter;
			}
			if (blnReturnMonth)
			{
				intMonth = intSelectedMonth;
			}
			modGlobalVariables.Statics.gboolAllPayRuns = 0 != chkAllPayRuns.CheckState;
			Close();
			return;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int intindex;
			intindex = 0;
			if (blnReturnYear)
			{
				lngSelectedYear = FCConvert.ToInt32(cboDateSelect[intindex].Text);
				intindex += 1;
			}
			if (blnReturnQuarter)
			{
				intSelectedQuarter = FCConvert.ToInt32(cboDateSelect[intindex].Text);
				intindex += 1;
			}
			if (blnReturnMonth)
			{
				intSelectedMonth = FCConvert.ToInt32(cboDateSelect[intindex].Text);
				intindex += 1;
			}
			if (blnReturnDay)
			{
				datSelectedDay = FCConvert.ToDateTime(cboPayDate.Text);
				intSelectedPayRunID = (cboPayRunID.SelectedIndex + 1);
			}
			modGlobalConstants.Statics.gboolPrintALLPayRuns = 0 != chkAllPayRuns.CheckState;
			Close();
		}
	}
}
