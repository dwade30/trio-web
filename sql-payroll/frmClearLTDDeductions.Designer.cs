﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmClearLTDDeductions.
	/// </summary>
	partial class frmClearLTDDeductions
	{
		public fecherFoundation.FCFrame fraWarning;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboDeductions;
		public fecherFoundation.FCComboBox cboSequence;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblType;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fraWarning = new fecherFoundation.FCFrame();
            this.lblWarning = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboDeductions = new fecherFoundation.FCComboBox();
            this.cboSequence = new fecherFoundation.FCComboBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblType = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarning)).BeginInit();
            this.fraWarning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 360);
            this.BottomPanel.Size = new System.Drawing.Size(605, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.fraWarning);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(605, 300);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(605, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(334, 30);
            this.HeaderText.Text = "Clear Life to Date Deductions";
            // 
            // fraWarning
            // 
            this.fraWarning.AppearanceKey = "groupboxRedHeader";
            this.fraWarning.Controls.Add(this.lblWarning);
            this.fraWarning.Location = new System.Drawing.Point(30, 30);
            this.fraWarning.Name = "fraWarning";
            this.fraWarning.Size = new System.Drawing.Size(547, 97);
            this.fraWarning.Text = "Warning!!!!";
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(20, 44);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(507, 33);
            this.lblWarning.TabIndex = 1;
            this.lblWarning.Text = "CHANGES USING THIS SCREEN WILL AFFECT ALL LIFE TO DATE TOTALS  FOR EMPLOYEES WITH" +
    " THE SPECIFIED SEQUENCE AND DEDUCTION NUMBER";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.cboDeductions);
            this.Frame1.Controls.Add(this.cboSequence);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.lblType);
            this.Frame1.Location = new System.Drawing.Point(30, 147);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(547, 69);
            this.Frame1.TabIndex = 2;
            // 
            // cboDeductions
            // 
            this.cboDeductions.BackColor = System.Drawing.SystemColors.Window;
            this.cboDeductions.Location = new System.Drawing.Point(175, 29);
            this.cboDeductions.Name = "cboDeductions";
            this.cboDeductions.Size = new System.Drawing.Size(372, 40);
            this.cboDeductions.TabIndex = 6;
            // 
            // cboSequence
            // 
            this.cboSequence.BackColor = System.Drawing.SystemColors.Window;
            this.cboSequence.Location = new System.Drawing.Point(0, 29);
            this.cboSequence.Name = "cboSequence";
            this.cboSequence.Size = new System.Drawing.Size(145, 40);
            this.cboSequence.TabIndex = 5;
            // 
            // Label1
            // 
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(145, 15);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "SEQUENCE NUMBER";
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(175, 0);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(150, 15);
            this.lblType.TabIndex = 3;
            this.lblType.Text = "DEDUCTION NUMBER";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 236);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmClearLTDDeductions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(605, 468);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmClearLTDDeductions";
            this.Text = "Clear Life to Date Deductions";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmClearLTDDeductions_Load);
            this.Activated += new System.EventHandler(this.frmClearLTDDeductions_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmClearLTDDeductions_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarning)).EndInit();
            this.fraWarning.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSave;
    }
}
