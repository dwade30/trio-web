﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptHealthCareSetup.
	/// </summary>
	partial class rptHealthCareSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHealthCareSetup));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCoverageDeclined = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtJanBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFebBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAprBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMayBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJunBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJulBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAugBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSepBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOctBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNovBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDecBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJanBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFebBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAprBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMayBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJunBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJulBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAugBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSepBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOctBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNovBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDecBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJanBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFebBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAprBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMayBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJunBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtJulBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAugBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSepBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOctBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNovBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDecBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCoverageDeclined)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtEmployeeNumber,
            this.txtName,
            this.lblCoverageDeclined,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.txtJanBox14,
            this.txtFebBox14,
            this.txtMarBox14,
            this.txtAprBox14,
            this.txtMayBox14,
            this.txtJunBox14,
            this.txtJulBox14,
            this.txtAugBox14,
            this.txtSepBox14,
            this.txtOctBox14,
            this.txtNovBox14,
            this.txtDecBox14,
            this.txtJanBox16,
            this.txtFebBox16,
            this.txtMarBox16,
            this.txtAprBox16,
            this.txtMayBox16,
            this.txtJunBox16,
            this.txtJulBox16,
            this.txtAugBox16,
            this.txtSepBox16,
            this.txtOctBox16,
            this.txtNovBox16,
            this.txtDecBox16,
            this.txtJanBox15,
            this.txtFebBox15,
            this.txtMarBox15,
            this.txtAprBox15,
            this.txtMayBox15,
            this.txtJunBox15,
            this.txtJulBox15,
            this.txtAugBox15,
            this.txtSepBox15,
            this.txtOctBox15,
            this.txtNovBox15,
            this.txtDecBox15,
            this.Label15,
            this.Label16,
            this.Label17,
				this.SubReport1
			});
			this.Detail.Height = 1.96875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.txtSecondTitle
			});
			this.PageHeader.Height = 0.5416667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.23F;
			this.txtTitle.HyperLink = null;
			this.txtTitle.Left = 1.5625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 4.9375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.125F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtSecondTitle
			// 
			this.txtSecondTitle.Height = 0.1875F;
			this.txtSecondTitle.Left = 2.4375F;
			this.txtSecondTitle.Name = "txtSecondTitle";
			this.txtSecondTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtSecondTitle.Text = null;
			this.txtSecondTitle.Top = 0.25F;
			this.txtSecondTitle.Width = 3.125F;
			// 
            // Label1
            // 
            this.Label1.Height = 0.2393333F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.1145833F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "Employee";
            this.Label1.Top = 0.04166667F;
            this.Label1.Width = 0.8333333F;
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.Height = 0.1770833F;
            this.txtEmployeeNumber.Left = 1.010417F;
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Text = null;
            this.txtEmployeeNumber.Top = 0.04166667F;
            this.txtEmployeeNumber.Width = 1.197917F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1770833F;
            this.txtName.Left = 2.3125F;
            this.txtName.Name = "txtName";
            this.txtName.Text = null;
            this.txtName.Top = 0.04166667F;
            this.txtName.Width = 4.260417F;
            // 
            // lblCoverageDeclined
            // 
            this.lblCoverageDeclined.Height = 0.1770833F;
            this.lblCoverageDeclined.HyperLink = null;
            this.lblCoverageDeclined.Left = 1.010417F;
            this.lblCoverageDeclined.Name = "lblCoverageDeclined";
            this.lblCoverageDeclined.Style = "font-weight: bold";
            this.lblCoverageDeclined.Text = "Coverage Declined";
            this.lblCoverageDeclined.Top = 0.28125F;
            this.lblCoverageDeclined.Visible = false;
            this.lblCoverageDeclined.Width = 1.833333F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1975834F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.6979167F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold";
            this.Label3.Text = "Jan";
            this.Label3.Top = 0.6354167F;
            this.Label3.Width = 0.3333333F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2080001F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 1.25F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "Feb";
            this.Label4.Top = 0.625F;
            this.Label4.Width = 0.3333333F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2080001F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 1.822917F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "Mar";
            this.Label5.Top = 0.625F;
            this.Label5.Width = 0.3333333F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2080001F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 2.375F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "Apr";
            this.Label6.Top = 0.625F;
            this.Label6.Width = 0.3333333F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2080001F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 2.947917F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-weight: bold";
            this.Label7.Text = "May";
            this.Label7.Top = 0.625F;
            this.Label7.Width = 0.3333333F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2080001F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 3.5F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-weight: bold";
            this.Label8.Text = "Jun";
            this.Label8.Top = 0.625F;
            this.Label8.Width = 0.3333333F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2080001F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 4.072917F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold";
            this.Label9.Text = "Jul";
            this.Label9.Top = 0.625F;
            this.Label9.Width = 0.3333333F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2080001F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 4.625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "Aug";
            this.Label10.Top = 0.625F;
            this.Label10.Width = 0.3333333F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2080001F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.197917F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold";
            this.Label11.Text = "Sep";
            this.Label11.Top = 0.625F;
            this.Label11.Width = 0.3333333F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2080001F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.75F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold";
            this.Label12.Text = "Oct";
            this.Label12.Top = 0.625F;
            this.Label12.Width = 0.3333333F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2080001F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 6.322917F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-weight: bold";
            this.Label13.Text = "Nov";
            this.Label13.Top = 0.625F;
            this.Label13.Width = 0.3333333F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2080001F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.875F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold";
            this.Label14.Text = "Dec";
            this.Label14.Top = 0.625F;
            this.Label14.Width = 0.3333333F;
            // 
            // txtJanBox14
            // 
            this.txtJanBox14.Height = 0.1979167F;
            this.txtJanBox14.Left = 0.6979167F;
            this.txtJanBox14.Name = "txtJanBox14";
            this.txtJanBox14.Text = null;
            this.txtJanBox14.Top = 0.8333333F;
            this.txtJanBox14.Width = 0.5208333F;
            // 
            // txtFebBox14
            // 
            this.txtFebBox14.Height = 0.1979167F;
            this.txtFebBox14.Left = 1.25F;
            this.txtFebBox14.Name = "txtFebBox14";
            this.txtFebBox14.Text = null;
            this.txtFebBox14.Top = 0.8333333F;
            this.txtFebBox14.Width = 0.5208333F;
            // 
            // txtMarBox14
            // 
            this.txtMarBox14.Height = 0.1979167F;
            this.txtMarBox14.Left = 1.822917F;
            this.txtMarBox14.Name = "txtMarBox14";
            this.txtMarBox14.Text = null;
            this.txtMarBox14.Top = 0.8333333F;
            this.txtMarBox14.Width = 0.5208333F;
            // 
            // txtAprBox14
            // 
            this.txtAprBox14.Height = 0.1979167F;
            this.txtAprBox14.Left = 2.375F;
            this.txtAprBox14.Name = "txtAprBox14";
            this.txtAprBox14.Text = null;
            this.txtAprBox14.Top = 0.8333333F;
            this.txtAprBox14.Width = 0.5208333F;
            // 
            // txtMayBox14
            // 
            this.txtMayBox14.Height = 0.1979167F;
            this.txtMayBox14.Left = 2.947917F;
            this.txtMayBox14.Name = "txtMayBox14";
            this.txtMayBox14.Text = null;
            this.txtMayBox14.Top = 0.8333333F;
            this.txtMayBox14.Width = 0.5208333F;
            // 
            // txtJunBox14
            // 
            this.txtJunBox14.Height = 0.1979167F;
            this.txtJunBox14.Left = 3.5F;
            this.txtJunBox14.Name = "txtJunBox14";
            this.txtJunBox14.Text = null;
            this.txtJunBox14.Top = 0.8333333F;
            this.txtJunBox14.Width = 0.5208333F;
            // 
            // txtJulBox14
            // 
            this.txtJulBox14.Height = 0.1979167F;
            this.txtJulBox14.Left = 4.072917F;
            this.txtJulBox14.Name = "txtJulBox14";
            this.txtJulBox14.Text = null;
            this.txtJulBox14.Top = 0.8333333F;
            this.txtJulBox14.Width = 0.5208333F;
            // 
            // txtAugBox14
            // 
            this.txtAugBox14.Height = 0.1979167F;
            this.txtAugBox14.Left = 4.625F;
            this.txtAugBox14.Name = "txtAugBox14";
            this.txtAugBox14.Text = null;
            this.txtAugBox14.Top = 0.8333333F;
            this.txtAugBox14.Width = 0.5208333F;
            // 
            // txtSepBox14
            // 
            this.txtSepBox14.Height = 0.1979167F;
            this.txtSepBox14.Left = 5.197917F;
            this.txtSepBox14.Name = "txtSepBox14";
            this.txtSepBox14.Text = null;
            this.txtSepBox14.Top = 0.8333333F;
            this.txtSepBox14.Width = 0.5208333F;
            // 
            // txtOctBox14
            // 
            this.txtOctBox14.Height = 0.1979167F;
            this.txtOctBox14.Left = 5.75F;
            this.txtOctBox14.Name = "txtOctBox14";
            this.txtOctBox14.Text = null;
            this.txtOctBox14.Top = 0.8333333F;
            this.txtOctBox14.Width = 0.5208333F;
            // 
            // txtNovBox14
            // 
            this.txtNovBox14.Height = 0.1979167F;
            this.txtNovBox14.Left = 6.322917F;
            this.txtNovBox14.Name = "txtNovBox14";
            this.txtNovBox14.Text = null;
            this.txtNovBox14.Top = 0.8333333F;
            this.txtNovBox14.Width = 0.5208333F;
            // 
            // txtDecBox14
            // 
            this.txtDecBox14.Height = 0.1979167F;
            this.txtDecBox14.Left = 6.875F;
            this.txtDecBox14.Name = "txtDecBox14";
            this.txtDecBox14.Text = null;
            this.txtDecBox14.Top = 0.8333333F;
            this.txtDecBox14.Width = 0.5208333F;
            // 
            // txtJanBox16
            // 
            this.txtJanBox16.Height = 0.1979167F;
            this.txtJanBox16.Left = 0.6979167F;
            this.txtJanBox16.Name = "txtJanBox16";
            this.txtJanBox16.Text = null;
            this.txtJanBox16.Top = 1.333333F;
            this.txtJanBox16.Width = 0.5208333F;
            // 
            // txtFebBox16
            // 
            this.txtFebBox16.Height = 0.1979167F;
            this.txtFebBox16.Left = 1.25F;
            this.txtFebBox16.Name = "txtFebBox16";
            this.txtFebBox16.Text = null;
            this.txtFebBox16.Top = 1.333333F;
            this.txtFebBox16.Width = 0.5208333F;
            // 
            // txtMarBox16
            // 
            this.txtMarBox16.Height = 0.1979167F;
            this.txtMarBox16.Left = 1.822917F;
            this.txtMarBox16.Name = "txtMarBox16";
            this.txtMarBox16.Text = null;
            this.txtMarBox16.Top = 1.333333F;
            this.txtMarBox16.Width = 0.5208333F;
            // 
            // txtAprBox16
            // 
            this.txtAprBox16.Height = 0.1979167F;
            this.txtAprBox16.Left = 2.375F;
            this.txtAprBox16.Name = "txtAprBox16";
            this.txtAprBox16.Text = null;
            this.txtAprBox16.Top = 1.333333F;
            this.txtAprBox16.Width = 0.5208333F;
            // 
            // txtMayBox16
            // 
            this.txtMayBox16.Height = 0.1979167F;
            this.txtMayBox16.Left = 2.947917F;
            this.txtMayBox16.Name = "txtMayBox16";
            this.txtMayBox16.Text = null;
            this.txtMayBox16.Top = 1.333333F;
            this.txtMayBox16.Width = 0.5208333F;
            // 
            // txtJunBox16
            // 
            this.txtJunBox16.Height = 0.1979167F;
            this.txtJunBox16.Left = 3.5F;
            this.txtJunBox16.Name = "txtJunBox16";
            this.txtJunBox16.Text = null;
            this.txtJunBox16.Top = 1.333333F;
            this.txtJunBox16.Width = 0.5208333F;
            // 
            // txtJulBox16
            // 
            this.txtJulBox16.Height = 0.1979167F;
            this.txtJulBox16.Left = 4.072917F;
            this.txtJulBox16.Name = "txtJulBox16";
            this.txtJulBox16.Text = null;
            this.txtJulBox16.Top = 1.333333F;
            this.txtJulBox16.Width = 0.5208333F;
            // 
            // txtAugBox16
            // 
            this.txtAugBox16.Height = 0.1979167F;
            this.txtAugBox16.Left = 4.625F;
            this.txtAugBox16.Name = "txtAugBox16";
            this.txtAugBox16.Text = null;
            this.txtAugBox16.Top = 1.333333F;
            this.txtAugBox16.Width = 0.5208333F;
            // 
            // txtSepBox16
            // 
            this.txtSepBox16.Height = 0.1979167F;
            this.txtSepBox16.Left = 5.197917F;
            this.txtSepBox16.Name = "txtSepBox16";
            this.txtSepBox16.Text = null;
            this.txtSepBox16.Top = 1.333333F;
            this.txtSepBox16.Width = 0.5208333F;
            // 
            // txtOctBox16
            // 
            this.txtOctBox16.Height = 0.1979167F;
            this.txtOctBox16.Left = 5.75F;
            this.txtOctBox16.Name = "txtOctBox16";
            this.txtOctBox16.Text = null;
            this.txtOctBox16.Top = 1.333333F;
            this.txtOctBox16.Width = 0.5208333F;
            // 
            // txtNovBox16
            // 
            this.txtNovBox16.Height = 0.1979167F;
            this.txtNovBox16.Left = 6.322917F;
            this.txtNovBox16.Name = "txtNovBox16";
            this.txtNovBox16.Text = null;
            this.txtNovBox16.Top = 1.333333F;
            this.txtNovBox16.Width = 0.5208333F;
            // 
            // txtDecBox16
            // 
            this.txtDecBox16.Height = 0.1979167F;
            this.txtDecBox16.Left = 6.875F;
            this.txtDecBox16.Name = "txtDecBox16";
            this.txtDecBox16.Text = null;
            this.txtDecBox16.Top = 1.333333F;
            this.txtDecBox16.Width = 0.5208333F;
            // 
            // txtJanBox15
            // 
            this.txtJanBox15.Height = 0.1979167F;
            this.txtJanBox15.Left = 0.6979167F;
            this.txtJanBox15.Name = "txtJanBox15";
            this.txtJanBox15.Text = null;
            this.txtJanBox15.Top = 1.083333F;
            this.txtJanBox15.Width = 0.5208333F;
            // 
            // txtFebBox15
            // 
            this.txtFebBox15.Height = 0.1979167F;
            this.txtFebBox15.Left = 1.25F;
            this.txtFebBox15.Name = "txtFebBox15";
            this.txtFebBox15.Text = null;
            this.txtFebBox15.Top = 1.083333F;
            this.txtFebBox15.Width = 0.5208333F;
            // 
            // txtMarBox15
            // 
            this.txtMarBox15.Height = 0.1979167F;
            this.txtMarBox15.Left = 1.822917F;
            this.txtMarBox15.Name = "txtMarBox15";
            this.txtMarBox15.Text = null;
            this.txtMarBox15.Top = 1.083333F;
            this.txtMarBox15.Width = 0.5208333F;
            // 
            // txtAprBox15
            // 
            this.txtAprBox15.Height = 0.1979167F;
            this.txtAprBox15.Left = 2.375F;
            this.txtAprBox15.Name = "txtAprBox15";
            this.txtAprBox15.Text = null;
            this.txtAprBox15.Top = 1.083333F;
            this.txtAprBox15.Width = 0.5208333F;
            // 
            // txtMayBox15
            // 
            this.txtMayBox15.Height = 0.1979167F;
            this.txtMayBox15.Left = 2.947917F;
            this.txtMayBox15.Name = "txtMayBox15";
            this.txtMayBox15.Text = null;
            this.txtMayBox15.Top = 1.083333F;
            this.txtMayBox15.Width = 0.5208333F;
            // 
            // txtJunBox15
            // 
            this.txtJunBox15.Height = 0.1979167F;
            this.txtJunBox15.Left = 3.5F;
            this.txtJunBox15.Name = "txtJunBox15";
            this.txtJunBox15.Text = null;
            this.txtJunBox15.Top = 1.083333F;
            this.txtJunBox15.Width = 0.5208333F;
            // 
            // txtJulBox15
            // 
            this.txtJulBox15.Height = 0.1979167F;
            this.txtJulBox15.Left = 4.072917F;
            this.txtJulBox15.Name = "txtJulBox15";
            this.txtJulBox15.Text = null;
            this.txtJulBox15.Top = 1.083333F;
            this.txtJulBox15.Width = 0.5208333F;
            // 
            // txtAugBox15
            // 
            this.txtAugBox15.Height = 0.1979167F;
            this.txtAugBox15.Left = 4.625F;
            this.txtAugBox15.Name = "txtAugBox15";
            this.txtAugBox15.Text = null;
            this.txtAugBox15.Top = 1.083333F;
            this.txtAugBox15.Width = 0.5208333F;
            // 
            // txtSepBox15
            // 
            this.txtSepBox15.Height = 0.1979167F;
            this.txtSepBox15.Left = 5.197917F;
            this.txtSepBox15.Name = "txtSepBox15";
            this.txtSepBox15.Text = null;
            this.txtSepBox15.Top = 1.083333F;
            this.txtSepBox15.Width = 0.5208333F;
            // 
            // txtOctBox15
            // 
            this.txtOctBox15.Height = 0.1979167F;
            this.txtOctBox15.Left = 5.75F;
            this.txtOctBox15.Name = "txtOctBox15";
            this.txtOctBox15.Text = null;
            this.txtOctBox15.Top = 1.083333F;
            this.txtOctBox15.Width = 0.5208333F;
            // 
            // txtNovBox15
            // 
            this.txtNovBox15.Height = 0.1979167F;
            this.txtNovBox15.Left = 6.322917F;
            this.txtNovBox15.Name = "txtNovBox15";
            this.txtNovBox15.Text = null;
            this.txtNovBox15.Top = 1.083333F;
            this.txtNovBox15.Width = 0.5208333F;
            // 
            // txtDecBox15
            // 
            this.txtDecBox15.Height = 0.1979167F;
            this.txtDecBox15.Left = 6.875F;
            this.txtDecBox15.Name = "txtDecBox15";
            this.txtDecBox15.Text = null;
            this.txtDecBox15.Top = 1.083333F;
            this.txtDecBox15.Width = 0.5208333F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1666667F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0.0625F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-weight: bold";
            this.Label15.Text = "Box 14";
            this.Label15.Top = 0.8333333F;
            this.Label15.Width = 0.5833333F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1666667F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0.0625F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold";
            this.Label16.Text = "Box 15";
            this.Label16.Top = 1.083333F;
            this.Label16.Width = 0.5833333F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1666667F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.0625F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-weight: bold";
            this.Label17.Text = "Box 16";
            this.Label17.Top = 1.333333F;
            this.Label17.Width = 0.5833333F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.07291666F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.625F;
            this.SubReport1.Width = 7.416667F;
            // 
			// rptHealthCareSetup
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.4375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCoverageDeclined)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJanBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFebBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAprBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMayBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJunBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJulBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSepBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCoverageDeclined;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJanBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFebBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAprBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMayBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJunBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJulBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAugBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSepBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOctBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNovBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJanBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFebBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAprBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMayBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJunBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJulBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAugBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSepBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOctBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNovBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecBox16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJanBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFebBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAprBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMayBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJunBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJulBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAugBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSepBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOctBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNovBox15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecBox15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
