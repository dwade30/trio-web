﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEUC1EmployerRecord
	{
		//=========================================================
		private int intYear;
		private string strFederalID = string.Empty;
		private string strEmployerName = string.Empty;
		private string strStreetAddress = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZipExt = string.Empty;
		private string strUCAccount = string.Empty;
		private int intQuarter;
		private string strEIN = string.Empty;
		private string strProcessorLicenseCode = string.Empty;

		public int TaxYear
		{
			set
			{
				intYear = value;
			}
			get
			{
				int TaxYear = 0;
				TaxYear = intYear;
				return TaxYear;
			}
		}

		public string FederalID
		{
			set
			{
				strFederalID = value;
			}
			get
			{
				string FederalID = "";
				FederalID = strFederalID;
				return FederalID;
			}
		}

		public string EmployerName
		{
			set
			{
				strEmployerName = value;
			}
			get
			{
				string EmployerName = "";
				EmployerName = strEmployerName;
				return EmployerName;
			}
		}

		public string Address
		{
			set
			{
				strStreetAddress = value;
			}
			get
			{
				string Address = "";
				Address = strStreetAddress;
				return Address;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string ZipExtension
		{
			set
			{
				strZipExt = value;
			}
			get
			{
				string ZipExtension = "";
				ZipExtension = strZipExt;
				return ZipExtension;
			}
		}

		public string UCAccount
		{
			set
			{
				strUCAccount = value;
			}
			get
			{
				string UCAccount = "";
				UCAccount = strUCAccount;
				return UCAccount;
			}
		}

		public int Quarter
		{
			set
			{
				intQuarter = value;
			}
			get
			{
				int Quarter = 0;
				Quarter = intQuarter;
				return Quarter;
			}
		}

		public string PreparerEIN
		{
			set
			{
				strEIN = value;
			}
			get
			{
				string PreparerEIN = "";
				PreparerEIN = strEIN;
				return PreparerEIN;
			}
		}

		public string ProcessorLicenseCode
		{
			set
			{
				strProcessorLicenseCode = value;
			}
			get
			{
				string ProcessorLicenseCode = "";
				ProcessorLicenseCode = strProcessorLicenseCode;
				return ProcessorLicenseCode;
			}
		}

		public string ToString()
		{
			string ToString = "";
			string strReturn;
			strReturn = "E";
			strReturn += FCConvert.ToString(intYear);
			// 2 - 5
			strReturn += strFederalID;
			// 6 - 14
			strReturn += Strings.StrDup(9, " ");
			// not used by maine 15-23
			strReturn += Strings.Left(strEmployerName + Strings.StrDup(50, " "), 50);
			// 24 - 73
			strReturn += Strings.Left(strStreetAddress + Strings.StrDup(40, " "), 40);
			// 74 - 113
			strReturn += Strings.Left(strCity + Strings.StrDup(25, " "), 25);
			// 114 - 138
			strReturn += strState;
			// 139 - 140
			strReturn += Strings.StrDup(8, " ");
			// not used by maine 141-148
			if (fecherFoundation.Strings.Trim(strZipExt) != "")
			{
				// 149-153
				strReturn += "-" + Strings.Left(strZipExt + Strings.StrDup(4, " "), 4);
			}
			else
			{
				strReturn += Strings.StrDup(5, " ");
			}
			strReturn += Strings.Left(strZip + Strings.StrDup(5, " "), 5);
			// 154 - 158
			strReturn += Strings.StrDup(8, " ");
			// not used by maine 159-166
			strReturn += "WAGE";
			// 167-170
			strReturn += Strings.Format(modCoreysSweeterCode.GetStateCode(strState), "00");
			// 171-172
			strReturn += strUCAccount;
			// 173-182
			strReturn += Strings.StrDup(5, " ");
			// not used by maine
			strReturn += Strings.Format(3 * intQuarter, "00");
			// 188 - 189
			strReturn += "1";
			// 1 is true that employee s records are included position 190
			strReturn += Strings.StrDup(18, " ");
			// not used by maine  191-208
			strReturn += Strings.Left("000000000" + strEIN, 9);
			// 209-217
			strReturn += Strings.Left(strProcessorLicenseCode + Strings.StrDup(7, " "), 7);
			// 218-224
			strReturn += Strings.StrDup(51, " ");
			// not used by maine  225-275
			ToString = strReturn;
			return ToString;
		}
	}
}
