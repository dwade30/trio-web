//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTempFlagReset.
	/// </summary>
	public partial class rptTempFlagReset : BaseSectionReport
	{
		public rptTempFlagReset()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Temporary Flag Reset List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTempFlagReset InstancePtr
		{
			get
			{
				return (rptTempFlagReset)Sys.GetInstance(typeof(rptTempFlagReset));
			}
		}

		protected rptTempFlagReset _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsEmployees?.Dispose();
				rsDistributions?.Dispose();
				rsDistributionHours?.Dispose();
				rsDeductions?.Dispose();
				rsMatch?.Dispose();
				rsDeductionSetup?.Dispose();
				rsDeductionStatus?.Dispose();
				rsExecute?.Dispose();
				rsEmployeePeriods?.Dispose();
                rsEmployees = null;
                rsDistributions = null;
                rsDistributionHours = null;
                rsDeductions = null;
                rsMatch = null;
                rsDeductionSetup = null;
                rsDeductionStatus = null;
                rsExecute = null;
                rsEmployeePeriods = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTempFlagReset	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsEmployees = new clsDRWrapper();
		clsDRWrapper rsDistributions = new clsDRWrapper();
		clsDRWrapper rsDistributionHours = new clsDRWrapper();
		clsDRWrapper rsDeductions = new clsDRWrapper();
		clsDRWrapper rsMatch = new clsDRWrapper();
		clsDRWrapper rsDeductionSetup = new clsDRWrapper();
		clsDRWrapper rsDeductionStatus = new clsDRWrapper();
		clsDRWrapper rsExecute = new clsDRWrapper();
		clsDRWrapper rsEmployeePeriods = new clsDRWrapper();
		string strType = "";
		bool boolFirstRecordOfType;
		bool boolTaxes;
		bool boolFound;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeNextRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				txtEmployee.Text = string.Empty;
				txtStatus.Text = string.Empty;
				Line1.Visible = false;
				if (strType == "EMPLOYEE")
				{
					#region If1
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE EMPLOYEE STATUS SECTION
						txtEmployee.Text = "Employee";
						txtStatus.Text = "Old status          New status";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						if (!rsEmployees.EndOfFile())
						{
							txtEmployee.Text = fecherFoundation.Strings.Trim(rsEmployees.Get_Fields_String("FirstName") + " " + rsEmployees.Get_Fields("middlename")) + " " + rsEmployees.Get_Fields_String("LastName");
							txtStatus.Text = "Hold 1 Week         Active";
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblEmployeeMaster Set Status = 'Active' where ID = " + rsEmployees.Get_Fields("ID"), "TWPY0000.vb1");
						}
						if (!rsEmployees.EndOfFile())
						{
							rsEmployees.MoveNext();
						}
						else
						{
							strType = "SPACE-EMPLOYEE";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If1End
				}
				else if (strType == "DISTRIBUTION")
				{
					#region If2
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE STATUS OF DISTRIBUTIONS
						txtEmployee.Text = "Employee";
						txtStatus.Text = "Distributions";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						executeNextRecord = true;
						goto NextRecord;
					}
					eArgs.EOF = false;
					#endregion If2End
					/*? 160 */
				}
				else if (strType == "DISTRIBUTIONHOURS")
				{
					#region If3
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Employee Distributions";
						txtStatus.Text = "Distributions (Hours this week)  changed to (Default)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
						rsDistributionHours.OpenRecordset("Select tblPayrollDistribution.ID as tblPayrollDistributionID, tblPayrollDistribution.*, tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where DefaultHours <> HoursWeek Order by tblEmployeeMaster.EmployeeNumber,tblPayrollDistribution.RecordNumber", modGlobalVariables.DEFAULTDATABASE);
					}
					else
					{
						if (!rsDistributionHours.EndOfFile())
						{
							txtEmployee.Text = rsDistributionHours.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributionHours.Get_Fields_String("FirstName") + " " + rsDistributionHours.Get_Fields("middlename")) + " " + rsDistributionHours.Get_Fields_String("LastName");
							txtStatus.Text = Strings.StrDup(5, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(15 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Double("HoursWeek"))) + Strings.StrDup(27 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Double("HoursWeek"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Double("DefaultHours")));
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblPayrollDistribution Set HoursWeek = " + rsDistributionHours.Get_Fields_Double("DefaultHours") + " where ID = " + rsDistributionHours.Get_Fields("tblPayrollDistributionID"), "TWPY0000.vb1");
							// RECALCULATE THE NEW GROSS AMOUNTS
							rsExecute.Execute("UPDATE tblPayrollDistribution SET tblPayrollDistribution.Gross = [HoursWeek]*[BaseRate] WHERE (((tblPayrollDistribution.HoursWeek)<>0))", "TWPY0000.vb1");
						}
						if (!rsDistributionHours.EndOfFile())
						{
							rsDistributionHours.MoveNext();
						}
						else
						{
							strType = "SPACE-DISTRIBUTIONHOURS";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If3End
				}
				else if (strType == "DISTRIBUTIONTAXES")
				{
					#region If4
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Employee Distribution Tax # Per";
						txtStatus.Text = "Distributions (Tax # Per)  changed to (Tax # Per)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
						rsDistributionHours.OpenRecordset("Select tblPayrollDistribution.ID as tblPayrollDistributionID, tblPayrollDistribution.*, tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where NumberWeeks <> 0 Order by tblEmployeeMaster.EmployeeNumber,tblPayrollDistribution.RecordNumber", modGlobalVariables.DEFAULTDATABASE);
					}
					else
					{
						if (!rsDistributionHours.EndOfFile())
						{
							txtEmployee.Text = rsDistributionHours.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributionHours.Get_Fields_String("FirstName") + " " + rsDistributionHours.Get_Fields("middlename")) + " " + rsDistributionHours.Get_Fields_String("LastName");
							txtStatus.Text = Strings.StrDup(5, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(15 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("NumberWeeks"))) + Strings.StrDup(24 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("NumberWeeks"))).Length, " ") + "0";
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblPayrollDistribution Set NumberWeeks = 0 where ID = " + rsDistributionHours.Get_Fields("tblPayrollDistributionID"), "TWPY0000.vb1");
						}
						if (!rsDistributionHours.EndOfFile())
						{
							rsDistributionHours.MoveNext();
						}
						else
						{
							strType = "SPACE-DISTRIBUTIONTAXES";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If4End
				}
				else if (strType == "WEEKSWITHHELD")
				{
					#region If5
					if (boolFirstRecordOfType)
					{
						txtEmployee.Text = "Employee Distribution Weeks Withheld";
						txtStatus.Text = "Distributions (# Withheld) changed to (# Withheld)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
						rsDistributionHours.OpenRecordset("Select tblPayrollDistribution.*,tblPayrollDistribution.ID as tblPayrollDistributionID, tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber where WeeksTaxWithheld <> 0 Order by tblEmployeeMaster.EmployeeNumber,tblPayrollDistribution.RecordNumber", modGlobalVariables.DEFAULTDATABASE);
					}
					else
					{
						if (!rsDistributionHours.EndOfFile())
						{
							txtEmployee.Text = rsDistributionHours.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributionHours.Get_Fields_String("FirstName") + " " + rsDistributionHours.Get_Fields("middlename")) + " " + rsDistributionHours.Get_Fields_String("LastName");
							txtStatus.Text = Strings.StrDup(5, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(15 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int16("WeeksTaxWithheld"))) + Strings.StrDup(24 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributionHours.Get_Fields_Int16("WeeksTaxWithheld"))).Length, " ") + "0";
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblPayrollDistribution Set WeeksTaxWithheld = 0 where ID = " + rsDistributionHours.Get_Fields("tblPayrollDistributionID"), "TWPY0000.vb1");
						}
						if (!rsDistributionHours.EndOfFile())
						{
							rsDistributionHours.MoveNext();
						}
						else
						{
							strType = "SPACE-WEEKSWITHHELD";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If5End
				}
				else if (strType == "DEDUCTIONS")
				{
					#region If6
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Employee Deductions";
						txtStatus.Text = "Deductions          (Old Frequency)         (New Frequency)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						if (!rsDeductions.EndOfFile())
						{
							txtEmployee.Text = FCConvert.ToString(rsDeductions.Get_Fields("EmployeeNumber")) + " - " + fecherFoundation.Strings.Trim(rsDeductions.Get_Fields_String("FirstName") + " " + rsDeductions.Get_Fields("middlename")) + " " + rsDeductions.Get_Fields_String("LastName");
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))) == string.Empty)
							{
								// THIS DEDUCTION USES THE DEFAULT
								txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewDefaultFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewDefaultFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("OLDFrequency")));
							}
							else
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))) == "Default")
								{
									txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewDefaultFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewDefaultFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("OLDFrequency")));
								}
								else
								{
									if (Conversion.Val(rsDeductions.Get_Fields("OLDFrequencyid")) == 0 || Conversion.Val(rsDeductions.Get_Fields("oldfrequencyid")) == 10 || Conversion.Val(rsDeductions.Get_Fields("oldfrequencyid")) == 9)
									{
										txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewDefaultFrequencyName")));
									}
									else
									{
										txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("NewFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductions.Get_Fields("OLDFrequency")));
									}
								}
							}
							// UPDATE THE DATABASE
							if (Conversion.Val(rsDeductions.Get_Fields("OldFrequencyid")) == 0 || Conversion.Val(rsDeductions.Get_Fields("oldfrequencyid")) == 10 || Conversion.Val(rsDeductions.Get_Fields("oldfrequencyid")) == 9)
							{
								rsExecute.Execute("Update tblEmployeeDeductions Set FrequencyCode = " + rsDeductions.Get_Fields("NewDefaultFrequencyID") + " where ID = " + rsDeductions.Get_Fields("ID"), "TWPY0000.vb1");
							}
							else
							{
								rsExecute.Execute("Update tblEmployeeDeductions Set FrequencyCode = " + rsDeductions.Get_Fields("OldFrequencyID") + " where ID = " + rsDeductions.Get_Fields("ID"), "TWPY0000.vb1");
							}
						}
						if (!rsDeductions.EndOfFile())
						{
							rsDeductions.MoveNext();
						}
						else
						{
							strType = "SPACE-DEDUCTIONS";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If6End
				}
				else if (strType == "MATCH")
				{
					#region If7
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Employee Match";
						txtStatus.Text = "Match Deductions    (Old Frequency)         (New Frequency)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						if (!rsMatch.EndOfFile())
						{
							txtEmployee.Text = FCConvert.ToString(rsMatch.Get_Fields("EmployeeNumber")) + " - " + fecherFoundation.Strings.Trim(rsMatch.Get_Fields_String("FirstName") + " " + rsMatch.Get_Fields("middlename")) + " " + rsMatch.Get_Fields_String("LastName");
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))) == string.Empty)
							{
								// THIS DEDUCTION USES THE DEFAULT
								txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewDefaultFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewDefaultFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("OLDFrequency")));
							}
							else
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))) == "Default")
								{
									txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewDefaultFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewDefaultFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("OLDFrequency")));
								}
								else
								{
									if (Conversion.Val(rsMatch.Get_Fields("OLDFrequencyid")) == 0 || Conversion.Val(rsMatch.Get_Fields("oldfrequencyid")) == 10 || Conversion.Val(rsMatch.Get_Fields("oldfrequencyid")) == 9)
									{
										txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewDefaultFrequencyName")));
									}
									else
									{
										txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields_Int32("RecordNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("NewFrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsMatch.Get_Fields("OLDFrequency")));
									}
								}
							}
							// UPDATE THE DATABASE
							if (Conversion.Val(rsMatch.Get_Fields_Int32("OldFrequency")) == 0 || Conversion.Val(rsMatch.Get_Fields("OldFrequencyid")) == 10 || Conversion.Val(rsMatch.Get_Fields("OldFrequencyid")) == 9)
							{
								rsExecute.Execute("Update tblEmployersMatch Set FrequencyCode = " + rsMatch.Get_Fields("NewDefaultFrequencyID") + " where ID = " + rsMatch.Get_Fields("ID"), "TWPY0000.vb1");
							}
							else
							{
								rsExecute.Execute("Update tblEmployersMatch Set FrequencyCode = " + rsMatch.Get_Fields("OldFrequencyID") + " where ID = " + rsMatch.Get_Fields("ID"), "TWPY0000.vb1");
							}
						}
						if (!rsMatch.EndOfFile())
						{
							rsMatch.MoveNext();
						}
						else
						{
							strType = "SPACE-MATCH";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If7End
				}
				else if (strType == "DEDUCTIONSETUP")
				{
					#region If8
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Deduction Setup";
						txtStatus.Text = "Deduction Number    (Old Value)             (New Value)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						if (!rsDeductionSetup.EndOfFile())
						{
							txtEmployee.Text = string.Empty;
							txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionSetup.Get_Fields_Int32("DeductionNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionSetup.Get_Fields_Int32("DeductionNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionSetup.Get_Fields("FrequencyName"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionSetup.Get_Fields("FrequencyName"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionSetup.Get_Fields("OldFrequencyName")));
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblDeductionSetup Set FrequencyCode = " + rsDeductionSetup.Get_Fields("OldFrequencyID") + " where ID = " + rsDeductionSetup.Get_Fields("ID"), "TWPY0000.vb1");
						}
						if (!rsDeductionSetup.EndOfFile())
						{
							rsDeductionSetup.MoveNext();
						}
						else
						{
							strType = "SPACE-DEDUCTIONSETUP";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If8End
				}
				else if (strType == "DEDUCTIONSTATUS")
				{
					#region If9
					if (boolFirstRecordOfType)
					{
						txtEmployee.Text = "Deduction Status";
						txtStatus.Text = "Deduction Number    (Old Status)            (New Status)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						if (!rsDeductionStatus.EndOfFile())
						{
							txtEmployee.Text = string.Empty;
							txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionStatus.Get_Fields_Int32("DeductionNumber"))) + Strings.StrDup(20 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionStatus.Get_Fields_Int32("DeductionNumber"))).Length, " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionStatus.Get_Fields("status"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionStatus.Get_Fields("status"))).Length, " ") + "Active";
							// UPDATE THE DATABASE
							rsExecute.Execute("Update tblDeductionSetup Set status = 'Active' where ID = " + rsDeductionStatus.Get_Fields("ID"), "TWPY0000.vb1");
						}
						if (!rsDeductionStatus.EndOfFile())
						{
							rsDeductionStatus.MoveNext();
						}
						else
						{
							strType = "SPACE-DEDUCTIONSTATUS";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If9End
				}
				else if (strType == "DISTRIBUTIONPERIODS")
				{
					#region If10
					if (boolFirstRecordOfType)
					{
						// PRINT THE HEADING FOR THE RESET FOR THE DEFAULT HOURS FEILDS
						txtEmployee.Text = "Employee Distribution Periods";
						txtStatus.Text = "              (Old Value)             (New Value)";
						boolFirstRecordOfType = false;
						Line1.Visible = true;
					}
					else
					{
						boolTaxes = false;
						if (!rsEmployeePeriods.EndOfFile())
						{
							txtEmployee.Text = FCConvert.ToString(rsEmployeePeriods.Get_Fields("EmployeeNumber")) + " - " + fecherFoundation.Strings.Trim(rsEmployeePeriods.Get_Fields_String("FirstName") + " " + rsEmployeePeriods.Get_Fields("middlename")) + " " + rsEmployeePeriods.Get_Fields_String("LastName");
							if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodTaxes")))) != 1 || (FCConvert.ToDouble(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodTaxes")))) > 0 && FCConvert.ToDouble(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodTaxes")))) < 1))
							{
								txtStatus.Text = "Period Taxes: " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodTaxes"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodTaxes"))).Length, " ") + "1.0";
								// UPDATE THE DATABASE
								rsExecute.Execute("Update tblEmployeeMaster Set PeriodTaxes = 1.0 where ID = " + rsEmployeePeriods.Get_Fields("ID"), "TWPY0000.vb1");
								boolTaxes = true;
							}
							if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodDeds")))) != 1)
							{
								if (boolTaxes)
								{
									txtStatus.Text = txtStatus.Text + "\r" + "Period  Deds: " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodDeds"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodDeds"))).Length, " ") + "1.0";
								}
								else
								{
									txtStatus.Text = "Period  Deds: " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodDeds"))) + Strings.StrDup(25 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeePeriods.Get_Fields_Double("PeriodDeds"))).Length, " ") + "1.0";
								}
								// UPDATE THE DATABASE
								rsExecute.Execute("Update tblEmployeeMaster Set PeriodDeds = 1.0 where ID = " + rsEmployeePeriods.Get_Fields("ID"), "TWPY0000.vb1");
							}
						}
						if (!rsEmployeePeriods.EndOfFile())
						{
							rsEmployeePeriods.MoveNext();
						}
						else
						{
							strType = "SPACE-DISTRIBUTIONPERIODS";
							boolFirstRecordOfType = true;
						}
					}
					eArgs.EOF = false;
					#endregion If10End
					/*? 670 */
				}
				else if (Strings.Left(strType, 6) == "SPACE-")
				{
					#region If11
					// THIS WILL ALLOW A DOUBLE SPACE IN BETWEEN EACH SECTION
					if (Strings.Mid(strType, 7, strType.Length - 6) == "EMPLOYEE")
					{
						strType = "DISTRIBUTION";
						/*? 680 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DISTRIBUTION")
					{
						strType = "DISTRIBUTIONHOURS";
						/*? 685 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DISTRIBUTIONHOURS")
					{
						strType = "DISTRIBUTIONTAXES";
						/*? 695 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DISTRIBUTIONTAXES")
					{
						strType = "WEEKSWITHHELD";
						/*? 700 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "WEEKSWITHHELD")
					{
						strType = "DEDUCTIONS";
						/*? 705 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DEDUCTIONS")
					{
						strType = "MATCH";
						/*? 710 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "MATCH")
					{
						strType = "DEDUCTIONSETUP";
						/*? 715 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DEDUCTIONSETUP")
					{
						strType = "DEDUCTIONSTATUS";
						/*? 720 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DEDUCTIONSTATUS")
					{
						strType = "DISTRIBUTIONPERIODS";
						/*? 725 */
					}
					else if (Strings.Mid(strType, 7, strType.Length - 6) == "DISTRIBUTIONPERIODS")
					{
						strType = "DONE";
					}
					eArgs.EOF = false;
					#endregion If11End
				}
				else
				{
					#region If12
					// ************************************************************************************************
					// NOW REMOVE ALL ACTUAL PERIODS FROM THE MASTER TABLE
					// MATTHEW 7/20/2005
					// AD
					int intCounter;
					clsDRWrapper rsClear = new clsDRWrapper();
					rsClear.OpenRecordset("Select * from tblEmployeeMaster");
					while (!rsClear.EndOfFile())
					{
						if (Conversion.Val(rsClear.Get_Fields_Double("ContractActualPeriods")) != 0)
						{
							modGlobalFunctions.AddCYAEntry_6("PY", "Clear of Contract Pay Actual Periods", "EmployeeNumber: " + rsClear.Get_Fields("EmployeeNumber"), "Actual Periods: " + rsClear.Get_Fields_Double("ContractActualPeriods"));
							rsClear.Edit();
							rsClear.Set_Fields("ContractActualPeriods", 0);
							rsClear.Update();
						}
						rsClear.MoveNext();
					}
					// ************************************************************************************************
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PENOBSCOT COUNTY")
					{
						if (MessageBox.Show("Do you want to advance all employees on rotating schedules by one week?", "Advance Rotations?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							modSchedule.AdvanceWeekRotations();
						}
					}
					eArgs.EOF = true;
					#endregion If12End
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 3167)
				{
					fecherFoundation.Information.Err(ex).Clear();
					if (!rsDistributions.EndOfFile())
					{
						rsDistributions.MoveNext();
						if (!boolFound)
						{
							executeNextRecord = true;
							goto NextRecord;
						}
					}
					else
					{
						strType = "SPACE-DISTRIBUTION";
						boolFirstRecordOfType = true;
					}
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "Line " + fecherFoundation.Information.Erl(), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			NextRecord:
			;
			if (executeNextRecord)
			{
				try
				{
					executeNextRecord = false;
					// THIS WI
					boolFound = false;
					if (!rsDistributions.EndOfFile())
					{
						if (FCConvert.ToInt32(rsDistributions.Get_Fields_String("AccountCode")) == 3)
						{
							// one week only
							txtEmployee.Text = rsDistributions.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributions.Get_Fields_String("FirstName") + " " + rsDistributions.Get_Fields("middlename")) + " " + rsDistributions.Get_Fields_String("LastName");
							txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("RecordNumber"))).Length, " ") + " - Deleted";
							rsExecute.Execute("update tblpayrolldistribution set accountcode = 1 where ID = " + rsDistributions.Get_Fields("tblPayrollDistributionID"), "twpy0000.vb1");
							boolFound = true;
						}
						else
						{
							// THIS IS FOR THE CONTRACT PAY
							rsExecute.OpenRecordset("Select * from tblPayCodes where PayCode = '999'");
							if (!rsExecute.EndOfFile())
							{
								if (rsDistributions.Get_Fields("CD") == rsExecute.Get_Fields("ID"))
								{
									txtEmployee.Text = rsDistributions.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributions.Get_Fields_String("FirstName") + " " + rsDistributions.Get_Fields("middlename")) + " " + rsDistributions.Get_Fields_String("LastName");
									txtStatus.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("RecordNumber"))) + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Int32("RecordNumber"))).Length, " ") + " - Deleted Contract Pay S(" + FCConvert.ToString(Conversion.Val(rsDistributions.Get_Fields_String("AccountCode")) - 3) + ")";
									rsExecute.Execute("Delete from tblPayrollDistribution where ID = " + rsDistributions.Get_Fields("tblPayrollDistributionID"), "TWPY0000.vb1");
									boolFound = true;
								}
							}
							rsExecute.OpenRecordset("Select * from tblPayCodes where PayCode = '998'");
							if (!rsExecute.EndOfFile())
							{
								if (rsDistributions.Get_Fields("CD") == rsExecute.Get_Fields("ID"))
								{
									txtEmployee.Text = rsDistributions.Get_Fields("EmployeeNumber") + " - " + fecherFoundation.Strings.Trim(rsDistributions.Get_Fields_String("FirstName") + " " + rsDistributions.Get_Fields("middlename")) + " " + rsDistributions.Get_Fields_String("LastName");
									txtStatus.Text = "Contract hrs set to 0" + Strings.StrDup(3, " ") + "HoursWeek: " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Double("HoursWeek"))) + Strings.StrDup(8 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Double("HoursWeek"))).Length, " ") + "DefaultHrs: " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsDistributions.Get_Fields_Double("DefaultHours")));
									rsExecute.Execute("Update tblPayrollDistribution Set DefaultHours = 0, HoursWeek = 0  where ID = " + rsDistributions.Get_Fields("tblPayrollDistributionID"), "TWPY0000.vb1");
								}
							}
						}
					}
					DeletedRecord:
					;
					if (!rsDistributions.EndOfFile())
					{
						rsDistributions.MoveNext();
						if (!boolFound)
						{
							executeNextRecord = true;
							goto NextRecord;
						}
					}
					else
					{
						strType = "SPACE-DISTRIBUTION";
						boolFirstRecordOfType = true;
					}
					//FC:FINAL:DDU:#i2321 - missed to set EOF to false, to work as in original
					eArgs.EOF = false;
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					if (fecherFoundation.Information.Err(ex).Number == 3167)
					{
						fecherFoundation.Information.Err(ex).Clear();
						if (!rsDistributions.EndOfFile())
						{
							rsDistributions.MoveNext();
							if (!boolFound)
							{
								executeNextRecord = true;
								goto NextRecord;
							}
						}
						else
						{
							strType = "SPACE-DISTRIBUTION";
							boolFirstRecordOfType = true;
						}
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "Line " + fecherFoundation.Information.Erl(), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsPayDateInfo = new clsDRWrapper())
            {
                string strSQL;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                PageCounter = 0;
                Label2.Text = modGlobalConstants.Statics.MuniName;
                Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
                rsPayDateInfo.OpenRecordset("SELECT * FROM tblPayrollProcessSetup");
                if (rsPayDateInfo.EndOfFile() != true && rsPayDateInfo.BeginningOfFile() != true)
                {
                    lblPayDate.Text = "Pay Date: " + Strings.Format(rsPayDateInfo.Get_Fields("PayDate"), "MM/dd/yyyy");
                }
                else
                {
                    lblPayDate.Text = "Pay Date: UNKNOWN";
                }

                strType = "EMPLOYEE";
                boolFirstRecordOfType = true;
                rsEmployees.Execute("delete from deductionoverrides", "twpy0000.vb1");
                rsEmployees.Execute("delete from matchoverrides", "twpy0000.vb1");
                rsEmployees.Execute("delete from withholdingoverrides", "twpy0000.vb1");
                rsEmployees.OpenRecordset(
                    "Select * from tblEmployeeMaster where Status = 'Hold 1 Week' Order by EmployeeNumber");
                rsDistributions.OpenRecordset(
                    "Select tblPayrollDistribution.*, tblpayrolldistribution.id as tblPayrollDistributionID,tblemployeemaster.id as employeeMasterID, tblEmployeeMaster.* from tblPayrollDistribution INNER JOIN tblEmployeeMaster ON tblPayrollDistribution.EmployeeNumber = tblEmployeeMaster.EmployeeNumber Order by tblEmployeeMaster.EmployeeNumber,tblPayrollDistribution.RecordNumber",
                    modGlobalVariables.DEFAULTDATABASE);
                strSQL =
                    "SELECT tblDeductionSetup.ID as DeductionSetupID, tblFrequencyCodes_2.ID as OldFrequencyID, tblEmployeeDeductions.ID, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, tblEmployeeMaster.LastName, tblEmployeeMaster.EmployeeNumber, tblFrequencyCodes.FrequencyCode AS NewFrequencyCode, tblFrequencyCodes.Description AS NewFrequencyName, tblFrequencyCodes_1.Description AS NewDefaultFrequencyName, tblFrequencyCodes_1.FrequencyCode AS NewDefaultFrequencyCode,tblfrequencycodes_1.ID as NewDefaultFrequencyID, tblEmployeeDeductions.RecordNumber, tblFrequencyCodes_2.Description AS OldFrequency ";
                strSQL +=
                    "FROM ((((tblEmployeeMaster INNER JOIN tblEmployeeDeductions ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber) INNER JOIN tblDeductionSetup ON tblEmployeeDeductions.DeductionCode = tblDeductionSetup.ID) LEFT JOIN tblFrequencyCodes ON tblEmployeeDeductions.FrequencyCode = tblFrequencyCodes.ID) LEFT JOIN tblFrequencyCodes AS tblFrequencyCodes_1 ON tblDeductionSetup.FrequencyCode = tblFrequencyCodes_1.ID) LEFT JOIN tblFrequencyCodes AS tblFrequencyCodes_2 ON tblEmployeeDeductions.OldFrequency = tblFrequencyCodes_2.ID ";
                strSQL += "WHERE ((tblFrequencyCodes.FrequencyCode)='P') ";
                strSQL += "ORDER BY tblEmployeeMaster.EmployeeNumber, tblEmployeeDeductions.RecordNumber";
                rsDeductions.OpenRecordset(strSQL, "TWPY0000.vb1");
                rsMatch.OpenRecordset(
                    "SELECT tblDeductionSetup.ID as DeductionSetupID, tblFrequencyCodes_2.ID as OldFrequencyID, tblEmployersMatch.ID, tblEmployeeMaster.FirstName,tblemployeemaster.middlename, tblEmployeeMaster.LastName, tblEmployeeMaster.EmployeeNumber, tblFrequencyCodes.FrequencyCode AS NewFrequencyCode, tblFrequencyCodes.Description AS NewFrequencyName, tblFrequencyCodes_1.Description AS NewDefaultFrequencyName, tblFrequencyCodes_1.FrequencyCode AS NewDefaultFrequencyCode,tblfrequencycodes_1.ID as NewDefaultFrequencyID, tblEmployersMatch.RecordNumber, tblFrequencyCodes_2.Description AS OldFrequency " +
                    "FROM ((((tblEmployeeMaster INNER JOIN tblEmployersMatch ON tblEmployeeMaster.EmployeeNumber = tblEmployersMatch.EmployeeNumber) INNER JOIN tblDeductionSetup ON tblEmployersMatch.DeductionCode = tblDeductionSetup.ID) LEFT JOIN tblFrequencyCodes ON tblEmployersMatch.FrequencyCode = tblFrequencyCodes.ID) LEFT JOIN tblFrequencyCodes AS tblFrequencyCodes_1 ON tblDeductionSetup.FrequencyCode = tblFrequencyCodes_1.ID) LEFT JOIN tblFrequencyCodes AS tblFrequencyCodes_2 ON tblEmployersMatch.OldFrequency = tblFrequencyCodes_2.ID " +
                    "WHERE ((tblFrequencyCodes.FrequencyCode)='P') " +
                    "ORDER BY tblEmployeeMaster.EmployeeNumber, tblEmployersMatch.RecordNumber", "TWPY0000.vb1");
                rsDeductionSetup.OpenRecordset(
                    "SELECT tblFrequencyCodes.ID AS FrequencyID, tblFrequencyCodes.Description AS FrequencyName, tblFrequencyCodes_1.ID AS OldFrequencyID, tblFrequencyCodes_1.Description AS OldFrequencyName, tblDeductionSetup.ID, * " +
                    "FROM (tblDeductionSetup INNER JOIN tblFrequencyCodes ON tblDeductionSetup.FrequencyCode = tblFrequencyCodes.ID) INNER JOIN tblFrequencyCodes AS tblFrequencyCodes_1 ON tblDeductionSetup.OldFrequency = tblFrequencyCodes_1.ID " +
                    "WHERE (((tblDeductionSetup.FrequencyCode)=10))", "TWPY0000.vb1");
                rsDeductionStatus.OpenRecordset(
                    "select * from tbldeductionsetup where rtrim(isnull(status, '')) = 'Hold 1 Week'", "twpy0000.vb1");
                rsEmployeePeriods.OpenRecordset(
                    "Select * from tblEmployeeMaster where ((PeriodTaxes <> 1 and periodtaxes <> 0) or PeriodDeds <> 1) Order by EmployeeNumber");
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
			{
				modGlobalRoutines.UpdatePayrollStepTable("ResetTempData");
			}
			else
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
