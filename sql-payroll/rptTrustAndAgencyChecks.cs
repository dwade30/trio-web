//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTrustAndAgencyChecks.
	/// </summary>
	public partial class RptTrustAndAgencyCodes : BaseSectionReport
	{
		public RptTrustAndAgencyCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Trust & Agency Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static RptTrustAndAgencyCodes InstancePtr
		{
			get
			{
				return (RptTrustAndAgencyCodes)Sys.GetInstance(typeof(RptTrustAndAgencyCodes));
			}
		}

		protected RptTrustAndAgencyCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTACheckInfo?.Dispose();
                rsTACheckInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTrustAndAgencyChecks	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intFinalCount;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		clsDRWrapper rsTACheckInfo = new clsDRWrapper();
		DateTime datPayDate;
		int intPayRunID;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsTACheckInfo.MoveNext();
				eArgs.EOF = rsTACheckInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: PageCounter As object	OnWrite
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			int PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
			}
			else
			{
				lblPayDate.Text = "Pay Date: " + Strings.Format(datPayDate, "MM/dd/yyyy");
			}
			blnFirstRecord = true;
			intFinalCount = 0;
			curFinalTotal = 0;
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				rsTACheckInfo.OpenRecordset("SELECT SUM(TrustAmount) as TotalTAAmount, CheckNumber, PayDate,PayRunID, TrustRecipientID FROM tblCheckDetail WHERE TrustRecord = 1 AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND CheckVoid = 0 GROUP BY CheckNumber, TrustRecipientID, PayDate,PayRunID ORDER BY CheckNumber");
			}
			else
			{
				rsTACheckInfo.OpenRecordset("SELECT SUM(TrustAmount) as TotalTAAmount, CheckNumber, PayDate,PayRunID, TrustRecipientID FROM tblCheckDetail WHERE TrustRecord = 1 AND PayDate = '" + FCConvert.ToString(datPayDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID) + " AND CheckVoid = 0 GROUP BY CheckNumber, TrustRecipientID, PayDate,PayRunID ORDER BY CheckNumber");
			}
			if (rsTACheckInfo.EndOfFile() != true && rsTACheckInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No trust & agency information found for this pay run.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				this.Cancel();
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
				{
					this.Close();
					return;
				}
				else
				{
					modDavesSweetCode.Statics.blnReportCompleted = true;
				}
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
			{
				modGlobalRoutines.UpdatePayrollStepTable("TrustAgency");
			}
			else
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldRecipient As object	OnWrite(string)
            using (clsDRWrapper rsTARecipientInfo = new clsDRWrapper())
            {
                rsTARecipientInfo.OpenRecordset("SELECT * FROM tblRecipients WHERE ID = " +
                                                rsTACheckInfo.Get_Fields_Int32("TrustRecipientID"));
                if (rsTARecipientInfo.EndOfFile())
                {
                }
                else
                {
                    fldCheck.Text = FCConvert.ToString(rsTACheckInfo.Get_Fields("CheckNumber"));
                    fldDate.Text = Strings.Format(rsTACheckInfo.Get_Fields_DateTime("PayDate"), "MM/dd/yy") +
                                   "   Pay Run ID " + rsTACheckInfo.Get_Fields("PayRunID");
                    fldAmount.Text = Strings.Format(rsTACheckInfo.Get_Fields("TotalTAAmount"), "#,##0.00");
                    // fldRecipient = GetFormat(rsTARecipientInfo.Fields("RecptNumber"), 4) & "  " & rsTARecipientInfo.Fields("Name")
                    fldRecipient.Text = rsTARecipientInfo.Get_Fields_Int32("RecptNumber") + "  " +
                                        rsTARecipientInfo.Get_Fields_String("Name");
                    intFinalCount += 1;
                    curFinalTotal += FCConvert.ToDecimal(rsTACheckInfo.Get_Fields("TotalTAAmount"));
                }
            }
        }

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalCount As object	OnWrite
			// vbPorter upgrade warning: fldFinalTotal As object	OnWrite(string)
			fldFinalCount.Text = intFinalCount.ToString();
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: PageCounter As object	OnWrite
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			Label4.Text = "Page " + this.PageNumber;
		}

		public void Init(List<string> batchReports = null)
		{
			if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
			{
				datPayDate = DateTime.Today;
				intPayRunID = 1;
				// bug call id 5053
				modGlobalVariables.Statics.gboolCancelSelected = false;
				frmSelectDateInfo.InstancePtr.Init2("Date Selection", -1, -1, -1, ref datPayDate, ref intPayRunID, false);
				if (modGlobalVariables.Statics.gboolCancelSelected)
				{
					this.Close();
					return;
				}
			}
			else
			{
				datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// rptTrustAndAgencyChecks.PrintReport False
			}
			else
			{
				// frmReportViewer.Init Me
				modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: true, strAttachmentName: "TrustAndAgency");
			}
		}

		
	}
}
