//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmWarrant.
	/// </summary>
	partial class frmWarrant
	{
		public fecherFoundation.FCComboBox cmbReprint;
		public fecherFoundation.FCLabel lblReprint;
		public fecherFoundation.FCFrame fraSelectReport;
		public fecherFoundation.FCComboBox cboReport;
		public fecherFoundation.FCButton Command1;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCButton cmdProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbReprint = new fecherFoundation.FCComboBox();
            this.lblReprint = new fecherFoundation.FCLabel();
            this.fraSelectReport = new fecherFoundation.FCFrame();
            this.cboReport = new fecherFoundation.FCComboBox();
            this.Command1 = new fecherFoundation.FCButton();
            this.dlg1_Save = new fecherFoundation.FCCommonDialog();
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).BeginInit();
            this.fraSelectReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 263);
            this.BottomPanel.Size = new System.Drawing.Size(435, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbReprint);
            this.ClientArea.Controls.Add(this.lblReprint);
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.fraSelectReport);
            this.ClientArea.Size = new System.Drawing.Size(435, 203);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(435, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(227, 30);
            this.HeaderText.Text = "Payroll Warrant Rpt";
            // 
            // cmbReprint
            // 
            this.cmbReprint.Items.AddRange(new object[] {
            "Initial Run",
            "Reprint Warrant"});
            this.cmbReprint.Location = new System.Drawing.Point(175, 30);
            this.cmbReprint.Name = "cmbReprint";
            this.cmbReprint.Size = new System.Drawing.Size(212, 40);
            this.cmbReprint.Text = "Initial Run";
            // 
            // lblReprint
            // 
            this.lblReprint.AutoSize = true;
            this.lblReprint.Location = new System.Drawing.Point(30, 44);
            this.lblReprint.Name = "lblReprint";
            this.lblReprint.Size = new System.Drawing.Size(95, 15);
            this.lblReprint.TabIndex = 1;
            this.lblReprint.Text = "REPORT TYPE";
            // 
            // fraSelectReport
            // 
            this.fraSelectReport.Controls.Add(this.cboReport);
            this.fraSelectReport.Controls.Add(this.Command1);
            this.fraSelectReport.Location = new System.Drawing.Point(30, 22);
            this.fraSelectReport.Name = "fraSelectReport";
            this.fraSelectReport.Size = new System.Drawing.Size(377, 150);
            this.fraSelectReport.TabIndex = 5;
            this.fraSelectReport.Text = "Select Report";
            this.fraSelectReport.Visible = false;
            // 
            // cboReport
            // 
            this.cboReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboReport.Items.AddRange(new object[] {
            "1 - Most Recently Run Report",
            "2 - Next Oldest Report",
            "3 - 3rd Oldest Report",
            "4 - 4th Oldest Report",
            "5 - 5th Oldest Report",
            "6 - 6th Oldest Report",
            "7 - 7th Oldest Report",
            "8 - 8th Oldest Report",
            "9 - 9th Oldest Report"});
            this.cboReport.Location = new System.Drawing.Point(20, 30);
            this.cboReport.Name = "cboReport";
            this.cboReport.Size = new System.Drawing.Size(337, 40);
            this.cboReport.TabIndex = 8;
            this.cboReport.DropDown += new System.EventHandler(this.cboReport_DropDown);
            // 
            // Command1
            // 
            this.Command1.AppearanceKey = "actionButton";
            this.Command1.Location = new System.Drawing.Point(20, 90);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(98, 40);
            this.Command1.TabIndex = 7;
            this.Command1.Text = "Process";
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // dlg1_Save
            // 
            this.dlg1_Save.Name = "dlg1_Save";
            this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
            // 
            // dlg1
            // 
            this.dlg1.Name = "dlg1";
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 90);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(110, 48);
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmWarrant
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(435, 263);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmWarrant";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Payroll Warrant Rpt";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmWarrant_Load);
            this.Activated += new System.EventHandler(this.frmWarrant_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmWarrant_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).EndInit();
            this.fraSelectReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}