//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmReportYear.
	/// </summary>
	partial class frmReportYear
	{
		public fecherFoundation.FCComboBox cmbMonthly;
		public fecherFoundation.FCLabel lblMonthly;
		public fecherFoundation.FCButton cmdProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbMonthly = new fecherFoundation.FCComboBox();
            this.lblMonthly = new fecherFoundation.FCLabel();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 211);
            this.BottomPanel.Size = new System.Drawing.Size(398, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbMonthly);
            this.ClientArea.Controls.Add(this.lblMonthly);
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Size = new System.Drawing.Size(398, 151);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(398, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // cmbMonthly
            // 
            this.cmbMonthly.AutoSize = false;
            this.cmbMonthly.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonthly.FormattingEnabled = true;
            this.cmbMonthly.Items.AddRange(new object[] {
            "Calander",
            "Fiscal"});
            this.cmbMonthly.Location = new System.Drawing.Point(203, 30);
            this.cmbMonthly.Name = "cmbMonthly";
            this.cmbMonthly.Size = new System.Drawing.Size(167, 40);
            this.cmbMonthly.TabIndex = 0;
            this.cmbMonthly.SelectedIndexChanged += new System.EventHandler(this.optMonthly_CheckedChanged);
            // 
            // lblMonthly
            // 
            this.lblMonthly.AutoSize = true;
            this.lblMonthly.Location = new System.Drawing.Point(30, 44);
            this.lblMonthly.Name = "lblMonthly";
            this.lblMonthly.Size = new System.Drawing.Size(120, 15);
            this.lblMonthly.TabIndex = 1;
            this.lblMonthly.Text = "WHITCH YTD TYPE";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 90);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(64, 48);
            this.cmdProcess.TabIndex = 2;
            this.cmdProcess.Text = "Ok";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmReportYear
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(398, 211);
            this.FillColor = 0;
            this.Name = "frmReportYear";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reporting YTD Type";
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReportYear_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}