﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptMultiFundAccountingSummaryPreview.
	/// </summary>
	partial class srptMultiFundAccountingSummaryPreview
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMultiFundAccountingSummaryPreview));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFederalTaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFICATaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMedicareTaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStateTaxWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeductionsWH = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFICAMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMedicareMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeductionMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldTotalDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SummaryTypeBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalTaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFICATaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMedicareTaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateTaxWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeductionsWH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFICAMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMedicareMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeductionMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SummaryTypeBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDescription,
            this.fldAccount,
            this.fldDebits,
            this.fldCredits});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1666667F;
			this.fldDescription.Left = 0.25F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 1";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 3.34375F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1666667F;
			this.fldAccount.Left = 3.6875F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.6875F;
			// 
			// fldDebits
			// 
			this.fldDebits.Height = 0.1666667F;
			this.fldDebits.Left = 5.40625F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = "Field1";
			this.fldDebits.Top = 0F;
			this.fldDebits.Width = 0.875F;
			// 
			// fldCredits
			// 
			this.fldCredits.Height = 0.1666667F;
			this.fldCredits.Left = 6.34375F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = "Field1";
			this.fldCredits.Top = 0F;
			this.fldCredits.Width = 0.875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Line3});
			this.ReportHeader.Height = 0.2395833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label1.Text = "Description";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.71875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.03125F;
			this.Label2.Width = 0.59375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.40625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Debit";
			this.Label3.Top = 0.03125F;
			this.Label3.Width = 0.875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.34375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Credit";
			this.Label4.Top = 0.03125F;
			this.Label4.Width = 0.875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.21875F;
			this.Line3.Width = 7.5F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.5F;
			this.Line3.Y1 = 0.21875F;
			this.Line3.Y2 = 0.21875F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.fldGrossPay,
            this.fldFederalTaxWH,
            this.fldFICATaxWH,
            this.fldMedicareTaxWH,
            this.fldStateTaxWH,
            this.fldDeductionsWH,
            this.fldNetPay,
            this.fldFICAMatch,
            this.fldMedicareMatch,
            this.fldDeductionMatch,
            this.Label20});
			this.ReportFooter.Height = 3F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.25F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "Total:";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.46875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 2.84375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			this.Label7.Text = "Gross Pay";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 1.375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.84375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "Federal Tax W/H";
			this.Label8.Top = 0.5F;
			this.Label8.Width = 1.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "";
			this.Label9.Text = "FICA Tax W/H";
			this.Label9.Top = 0.6875F;
			this.Label9.Width = 1.375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.84375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "Medicare Tax W/H";
			this.Label10.Top = 0.875F;
			this.Label10.Width = 1.375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.84375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "";
			this.Label11.Text = "State Tax W/H";
			this.Label11.Top = 1.0625F;
			this.Label11.Width = 1.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.843333F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "";
			this.Label12.Text = "Deductions W/H";
			this.Label12.Top = 1.25F;
			this.Label12.Width = 1.375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.843333F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "";
			this.Label13.Text = "Net Pay";
			this.Label13.Top = 1.4375F;
			this.Label13.Width = 1.375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.843333F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "";
			this.Label14.Text = "FICA Match";
			this.Label14.Top = 1.71875F;
			this.Label14.Width = 1.375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.843333F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "";
			this.Label15.Text = "Medicare Match";
			this.Label15.Top = 1.90625F;
			this.Label15.Width = 1.375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.843333F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "";
			this.Label16.Text = "Employer Match";
			this.Label16.Top = 2.09375F;
			this.Label16.Width = 1.375F;
			// 
			// fldGrossPay
			// 
			this.fldGrossPay.Height = 0.1875F;
			this.fldGrossPay.Left = 4.3125F;
			this.fldGrossPay.Name = "fldGrossPay";
			this.fldGrossPay.Style = "text-align: right";
			this.fldGrossPay.Text = "Field6";
			this.fldGrossPay.Top = 0.3125F;
			this.fldGrossPay.Width = 1.125F;
			// 
			// fldFederalTaxWH
			// 
			this.fldFederalTaxWH.Height = 0.1875F;
			this.fldFederalTaxWH.Left = 4.3125F;
			this.fldFederalTaxWH.Name = "fldFederalTaxWH";
			this.fldFederalTaxWH.Style = "text-align: right";
			this.fldFederalTaxWH.Text = "Field6";
			this.fldFederalTaxWH.Top = 0.5F;
			this.fldFederalTaxWH.Width = 1.125F;
			// 
			// fldFICATaxWH
			// 
			this.fldFICATaxWH.Height = 0.1875F;
			this.fldFICATaxWH.Left = 4.3125F;
			this.fldFICATaxWH.Name = "fldFICATaxWH";
			this.fldFICATaxWH.Style = "text-align: right";
			this.fldFICATaxWH.Text = "Field6";
			this.fldFICATaxWH.Top = 0.6875F;
			this.fldFICATaxWH.Width = 1.125F;
			// 
			// fldMedicareTaxWH
			// 
			this.fldMedicareTaxWH.Height = 0.1875F;
			this.fldMedicareTaxWH.Left = 4.3125F;
			this.fldMedicareTaxWH.Name = "fldMedicareTaxWH";
			this.fldMedicareTaxWH.Style = "text-align: right";
			this.fldMedicareTaxWH.Text = "Field6";
			this.fldMedicareTaxWH.Top = 0.875F;
			this.fldMedicareTaxWH.Width = 1.125F;
			// 
			// fldStateTaxWH
			// 
			this.fldStateTaxWH.Height = 0.1875F;
			this.fldStateTaxWH.Left = 4.3125F;
			this.fldStateTaxWH.Name = "fldStateTaxWH";
			this.fldStateTaxWH.Style = "text-align: right";
			this.fldStateTaxWH.Text = "Field6";
			this.fldStateTaxWH.Top = 1.0625F;
			this.fldStateTaxWH.Width = 1.125F;
			// 
			// fldDeductionsWH
			// 
			this.fldDeductionsWH.Height = 0.1875F;
			this.fldDeductionsWH.Left = 4.312083F;
			this.fldDeductionsWH.Name = "fldDeductionsWH";
			this.fldDeductionsWH.Style = "text-align: right";
			this.fldDeductionsWH.Text = "Field6";
			this.fldDeductionsWH.Top = 1.25F;
			this.fldDeductionsWH.Width = 1.125F;
			// 
			// fldNetPay
			// 
			this.fldNetPay.Height = 0.1875F;
			this.fldNetPay.Left = 4.312083F;
			this.fldNetPay.Name = "fldNetPay";
			this.fldNetPay.Style = "text-align: right";
			this.fldNetPay.Text = "Field6";
			this.fldNetPay.Top = 1.4375F;
			this.fldNetPay.Width = 1.125F;
			// 
			// fldFICAMatch
			// 
			this.fldFICAMatch.Height = 0.1875F;
			this.fldFICAMatch.Left = 4.312083F;
			this.fldFICAMatch.Name = "fldFICAMatch";
			this.fldFICAMatch.Style = "text-align: right";
			this.fldFICAMatch.Text = "Field6";
			this.fldFICAMatch.Top = 1.71875F;
			this.fldFICAMatch.Width = 1.125F;
			// 
			// fldMedicareMatch
			// 
			this.fldMedicareMatch.Height = 0.1875F;
			this.fldMedicareMatch.Left = 4.312083F;
			this.fldMedicareMatch.Name = "fldMedicareMatch";
			this.fldMedicareMatch.Style = "text-align: right";
			this.fldMedicareMatch.Text = "Field6";
			this.fldMedicareMatch.Top = 1.90625F;
			this.fldMedicareMatch.Width = 1.125F;
			// 
			// fldDeductionMatch
			// 
			this.fldDeductionMatch.Height = 0.1875F;
			this.fldDeductionMatch.Left = 4.312083F;
			this.fldDeductionMatch.Name = "fldDeductionMatch";
			this.fldDeductionMatch.Style = "text-align: right";
			this.fldDeductionMatch.Text = "Field6";
			this.fldDeductionMatch.Top = 2.09375F;
			this.fldDeductionMatch.Width = 1.125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.635F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: center";
			this.Label20.Text = "This Report is Only a Preview";
			this.Label20.Top = 2.625F;
			this.Label20.Width = 2.21875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalDebits,
            this.fldTotalCredits,
            this.Line4,
            this.Label5});
			this.GroupFooter2.Height = 0.2395833F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// fldTotalDebits
			// 
			this.fldTotalDebits.Height = 0.1875F;
			this.fldTotalDebits.Left = 5.40625F;
			this.fldTotalDebits.Name = "fldTotalDebits";
			this.fldTotalDebits.Style = "font-weight: bold; text-align: right";
			this.fldTotalDebits.Text = "Field1";
			this.fldTotalDebits.Top = 0.03125F;
			this.fldTotalDebits.Width = 0.875F;
			// 
			// fldTotalCredits
			// 
			this.fldTotalCredits.Height = 0.1875F;
			this.fldTotalCredits.Left = 6.34375F;
			this.fldTotalCredits.Name = "fldTotalCredits";
			this.fldTotalCredits.Style = "font-weight: bold; text-align: right";
			this.fldTotalCredits.Text = "Field4";
			this.fldTotalCredits.Top = 0.03125F;
			this.fldTotalCredits.Width = 0.875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 2.6875F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 4.34375F;
			this.Line4.X1 = 2.6875F;
			this.Line4.X2 = 7.03125F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.46875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Totals";
			this.Label5.Top = 0.03125F;
			this.Label5.Width = 0.8125F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle,
            this.SummaryTypeBinder});
			this.GroupHeader1.DataField = "SummaryTypeBinder";
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 0F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTitle.Text = "Label5";
			this.lblTitle.Top = 0.03125F;
			this.lblTitle.Width = 2.8125F;
			// 
			// SummaryTypeBinder
			// 
			this.SummaryTypeBinder.Height = 0.1875F;
			this.SummaryTypeBinder.Left = 3.75F;
			this.SummaryTypeBinder.Name = "SummaryTypeBinder";
			this.SummaryTypeBinder.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.SummaryTypeBinder.Text = "Field5";
			this.SummaryTypeBinder.Top = 0.03125F;
			this.SummaryTypeBinder.Visible = false;
			this.SummaryTypeBinder.Width = 0.75F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptMultiFundAccountingSummaryPreview
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFederalTaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFICATaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMedicareTaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateTaxWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeductionsWH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFICAMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMedicareMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeductionMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SummaryTypeBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalTaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFICATaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMedicareTaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateTaxWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeductionsWH;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFICAMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMedicareMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeductionMatch;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox SummaryTypeBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
