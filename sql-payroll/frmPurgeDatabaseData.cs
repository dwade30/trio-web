﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPurgeDatabaseData : BaseForm
	{
		public frmPurgeDatabaseData()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPurgeDatabaseData InstancePtr
		{
			get
			{
				return (frmPurgeDatabaseData)Sys.GetInstance(typeof(frmPurgeDatabaseData));
			}
		}

		protected frmPurgeDatabaseData _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       March 17,2005
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsData = new clsDRWrapper();
		private int intCounter;

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			// MATTHEW 03/22/2005
			// THIS ROUTINE WILL PURGE DATA FROM THE AUDIT ARCHIVE TABLE AND THE CHECK DETAIL
			// TABLE ACCORDING TO THE DATE THAT THEY CHOOSE.
			if (FCConvert.ToDouble(cboYear.Text) >= DateTime.Today.Year)
			{
				MessageBox.Show("Must keep data including year: " + FCConvert.ToString(DateTime.Today.Year - 1), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				if (MessageBox.Show("Have you verified that ALL people are out of Payroll?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGlobalFunctions.AddCYAEntry_6("PY", "Verified all people are out of PY");
					// BACKUP THE DATABASE
					if (MessageBox.Show("Have you verified that your backup was completed successfully?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("PY", "Verified backup");
						if (MessageBox.Show("Are you sure you wish to purge ALL audit history and payroll detail prior to " + cboYear.Text + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							modGlobalFunctions.AddCYAEntry_6("PY", "Agree to Purge", "Year combo: " + cboYear.Text);
							// DELETE THE
							clsDRWrapper rsData = new clsDRWrapper();
							// Call rsData.Execute("Delete * from AuditArchive")
							rsData.Execute("Delete from tblCheckDetail where Year(PayDate) < " + cboYear.Text, "Payroll");
							MessageBox.Show("Purge of database completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			}
		}

		public void cmdPurge_Click()
		{
			cmdPurge_Click(cmdPurge, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdQuit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(mnuExit, new System.EventArgs());
		}
		// Private Sub Form_Activate()
		// On Error GoTo CallErrorRoutine
		// gstrCurrentRoutine = "Form_Activate"
		// Dim Mouse As clsMousePointer
		// Set Mouse = New clsMousePointer
		// GoTo ResumeCode
		// CallErrorRoutine:
		// Call SetErrorHandler
		// Exit Sub
		// ResumeCode:
		//
		// Call ForceFormToResize(Me)
		// verify that this form is not already open
		// If FormExist(Me) Then Exit Sub
		// End Sub
		private void frmPurgeDatabaseData_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_KeyUp";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (KeyCode == Keys.Escape)
				{
					KeyCode = (Keys)0;
					Close();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPurgeDatabaseData_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeDatabaseData properties;
			//frmPurgeDatabaseData.ScaleWidth	= 5505;
			//frmPurgeDatabaseData.ScaleHeight	= 4170;
			//frmPurgeDatabaseData.LinkTopic	= "Form2";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 2);
				cboYear.Clear();
				// vbPorter upgrade warning: lngYear As object	OnWriteFCConvert.ToInt32(
				for (int lngYear = 1995; lngYear <= DateTime.Today.Year - 1; lngYear++)
				{
					cboYear.AddItem(lngYear.ToString());
				}
				// lngYear
				// SET THE WARNING MESSAGE FOR A BACKUP HAS BEEN DONE
				txtWarning.Text = "Verify backup of database was completed successfully.";
				txtWarning2.Text = "File location should be: " + Environment.CurrentDirectory + "\\ Backup \\ TWPYArchive?.vb1";
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPurgeDatabaseData_Resize(object sender, System.EventArgs e)
		{
			//this.Label1.WidthOriginal = this.WidthOriginal;
			//this.cboYear.LeftOriginal = FCConvert.ToInt32((this.WidthOriginal / 2.0) - (this.cboYear.WidthOriginal / 2.0));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuPurge_Click(object sender, System.EventArgs e)
		{
			cmdPurge_Click();
		}
	}
}
