//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmEmployeeDeductionHistory.
	/// </summary>
	partial class frmEmployeeDeductionHistory
	{
		public FCGrid vsHistory;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSep1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsHistory = new fecherFoundation.FCGrid();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdRefresh = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdDelete);
            this.BottomPanel.Location = new System.Drawing.Point(0, 510);
            this.BottomPanel.Size = new System.Drawing.Size(968, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsHistory);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdNew);
            this.ClientArea.Controls.Add(this.cmdRefresh);
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Size = new System.Drawing.Size(968, 450);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(968, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(323, 30);
            this.HeaderText.Text = "Employee Deduction History";
            // 
            // vsHistory
            // 
            this.vsHistory.AllowSelection = false;
            this.vsHistory.AllowUserToResizeColumns = false;
            this.vsHistory.AllowUserToResizeRows = false;
            this.vsHistory.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsHistory.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsHistory.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsHistory.BackColorBkg = System.Drawing.Color.Empty;
            this.vsHistory.BackColorFixed = System.Drawing.Color.Empty;
            this.vsHistory.BackColorSel = System.Drawing.Color.Empty;
            this.vsHistory.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsHistory.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsHistory.ColumnHeadersHeight = 30;
            this.vsHistory.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsHistory.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsHistory.DragIcon = null;
            this.vsHistory.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHistory.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsHistory.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsHistory.FrozenCols = 0;
            this.vsHistory.GridColor = System.Drawing.Color.Empty;
            this.vsHistory.GridColorFixed = System.Drawing.Color.Empty;
            this.vsHistory.Location = new System.Drawing.Point(30, 30);
            this.vsHistory.Name = "vsHistory";
            this.vsHistory.OutlineCol = 0;
            this.vsHistory.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsHistory.RowHeightMin = 0;
            this.vsHistory.Rows = 50;
            this.vsHistory.ScrollTipText = null;
            this.vsHistory.ShowColumnVisibilityMenu = false;
            this.vsHistory.Size = new System.Drawing.Size(910, 401);
            this.vsHistory.StandardTab = true;
            this.vsHistory.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsHistory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsHistory.TabIndex = 0;
            this.vsHistory.CurrentCellChanged += new System.EventHandler(this.vsHistory_RowColChange);
            this.vsHistory.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsHistory_MouseDownEvent);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(874, 38);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(45, 40);
            this.cmdPrint.TabIndex = 5;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(273, 46);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(80, 40);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "acceptButton";
            this.cmdDelete.Location = new System.Drawing.Point(423, 30);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdDelete.Size = new System.Drawing.Size(92, 48);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.AppearanceKey = "actionButton";
            this.cmdNew.Location = new System.Drawing.Point(688, 43);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 40);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.AppearanceKey = "actionButton";
            this.cmdRefresh.Location = new System.Drawing.Point(808, 42);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 40);
            this.cmdRefresh.TabIndex = 4;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDelete,
            this.mnuSep1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 0;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSep1
            // 
            this.mnuSep1.Index = 1;
            this.mnuSep1.Name = "mnuSep1";
            this.mnuSep1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmEmployeeDeductionHistory
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(968, 618);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmployeeDeductionHistory";
            this.Text = "Employee Deduction History";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmEmployeeDeductionHistory_Load);
            this.Activated += new System.EventHandler(this.frmEmployeeDeductionHistory_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEmployeeDeductionHistory_KeyPress);
            this.Resize += new System.EventHandler(this.frmEmployeeDeductionHistory_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}