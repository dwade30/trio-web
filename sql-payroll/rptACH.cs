//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptACH.
	/// </summary>
	public partial class rptACH : BaseSectionReport
	{
		public rptACH()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ACH";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptACH InstancePtr
		{
			get
			{
				return (rptACH)Sys.GetInstance(typeof(rptACH));
			}
		}

		protected rptACH _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
				clsDDBanks?.Dispose();
                clsLoad = null;
                clsDDBanks = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptACH	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intpage;
		clsDRWrapper clsLoad = new clsDRWrapper();
		double lngTotal;
		int lngCount;
		clsDRWrapper clsDDBanks = new clsDRWrapper();
		DateTime dtDate;
		int intPayrun;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intpage = 1;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtPage.Text = "Page  1";
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		public void Init(bool modalDialog, DateTime? tempdtPDate = null, int intPRun = 0, List<string> batchReports = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtPDate == null)
			{
				tempdtPDate = DateTime.FromOADate(0);
			}
			DateTime dtPDate = tempdtPDate.Value;
			string strSQL;
			string strOrderBy = "";
			if (dtPDate.ToOADate() != 0)
			{
				dtDate = dtPDate;
				intPayrun = intPRun;
			}
			else
			{
				dtDate = modGlobalVariables.Statics.gdatCurrentPayDate;
				intPayrun = modGlobalVariables.Statics.gintCurrentPayRun;
			}
			clsDDBanks.OpenRecordset("select * from tblbanks order by ID", "twpy0000.vb1");
			lngTotal = 0;
			lngCount = 0;
			lblPayDate.Text = "Pay Date: " + Strings.Format(dtDate, "MM/dd/yyyy") + "   Pay Run ID: " + FCConvert.ToString(intPayrun);
			clsLoad.OpenRecordset("select reportsequence from tblDefaultInformation", "twpy0000.vb1");
			switch (clsLoad.Get_Fields_Int32("reportsequence"))
			{
				case 0:
					{
						// name
						strOrderBy = " lastname,firstname,tblemployeemaster.employeenumber ";
						break;
					}
				case 1:
					{
						// employee number
						strOrderBy = " tblemployeemaster.employeenumber ";
						break;
					}
				case 2:
					{
						// sequence
						strOrderBy = " seqnumber,lastname,firstname,tblemployeemaster.employeenumber ";
						break;
					}
				case 3:
					{
						// dept div
						strOrderBy = " deptdiv,lastname,firstname,tblemployeemaster.employeenumber ";
						break;
					}
			}
			//end switch
			clsLoad.OpenRecordset("select * from tblbanks where ACHBank = 1", "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				txtAddress1.Text = clsLoad.Get_Fields("address1");
				txtAddress2.Text = clsLoad.Get_Fields("address2");
				txtAddress3.Text = clsLoad.Get_Fields_String("address3");
			}
			strSQL = "(Select employeenumber,ddbanknumber,ddaccountnumber,DDAccountType,sum(ddamount) as TotAmount from tblcheckdetail where (checkvoid = 0) and  paydate = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "' and payrunid = " + FCConvert.ToString(intPayrun) + " and BankRecord = 1 and checknumber > 0 group by employeenumber,employeename,ddbanknumber,ddaccountnumber,DDAccountType) as ACHQuery ";
			strSQL = "Select * from tblemployeemaster inner join (" + strSQL + " inner join tblbanks on (tblbanks.ID = convert(int, isnull(achquery.ddbanknumber, 0))) ) on (achquery.employeenumber = tblemployeemaster.employeenumber) order by " + strOrderBy;
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
				// Me.PrintReport False
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, false, showModal: modalDialog);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
			{
				modGlobalRoutines.UpdatePayrollStepTable("BankList");
			}
			else
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			if (!clsLoad.EndOfFile())
			{
				lngCount += 1;
				dblTemp = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsLoad.Get_Fields("totamount")), "0.00"));
				lngTotal += dblTemp;
				fldEmployee.Text = clsLoad.Get_Fields("tblemployeemaster.employeenumber") + "  " + clsLoad.Get_Fields("lastname") + ", " + clsLoad.Get_Fields("firstname");
				fldAccount.Text = clsLoad.Get_Fields("ddaccountnumber");
				this.Field1.Text = clsLoad.Get_Fields_String("DDAccountType");
				// txtDFI.Text = .Fields("ddbanknumber")
				clsDDBanks.FindFirstRecord("ID", Conversion.Val(clsLoad.Get_Fields("ddbanknumber")));
				txtDFI.Text = clsDDBanks.Get_Fields("bankdepositid");
				fldAmount.Text = Strings.Format(clsLoad.Get_Fields("totamount"), "#,###,###,##0.00");
				txtBank.Text = clsLoad.Get_Fields("name");
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page  " + FCConvert.ToString(intpage);
			intpage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldFinalTotal.Text = Strings.Format(lngTotal, "#,###,###,##0.00");
			fldFinalCount.Text = FCConvert.ToString(lngCount);
		}

		
	}
}
