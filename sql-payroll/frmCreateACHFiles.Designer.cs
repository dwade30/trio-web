﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCreateACHFiles.
	/// </summary>
	partial class frmCreateACHFiles
	{
		public fecherFoundation.FCComboBox cmbPrenote;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDateSelect;
		public fecherFoundation.FCTextBox txtFileName;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdPrenote;
		public fecherFoundation.FCFrame framPayDate;
		public fecherFoundation.FCComboBox cboPayDateYear;
		public fecherFoundation.FCComboBox cboPayDate;
		public fecherFoundation.FCComboBox cboPayRunID;
		public fecherFoundation.FCLabel lblDateSelect_3;
		public fecherFoundation.FCLabel lblDateSelect_4;
		public Global.T2KDateBox T2KEffectiveEntryDate;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbPrenote = new fecherFoundation.FCComboBox();
            this.txtFileName = new fecherFoundation.FCTextBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cmdPrenote = new fecherFoundation.FCButton();
            this.framPayDate = new fecherFoundation.FCFrame();
            this.cboPayDateYear = new fecherFoundation.FCComboBox();
            this.cboPayDate = new fecherFoundation.FCComboBox();
            this.cboPayRunID = new fecherFoundation.FCComboBox();
            this.lblDateSelect_3 = new fecherFoundation.FCLabel();
            this.lblDateSelect_4 = new fecherFoundation.FCLabel();
            this.T2KEffectiveEntryDate = new Global.T2KDateBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrenote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayDate)).BeginInit();
            this.framPayDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(659, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtFileName);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.framPayDate);
            this.ClientArea.Controls.Add(this.T2KEffectiveEntryDate);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Size = new System.Drawing.Size(659, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(659, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(123, 30);
            this.HeaderText.Text = "ACH Files";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbPrenote
            // 
            this.cmbPrenote.AutoSize = false;
            this.cmbPrenote.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPrenote.FormattingEnabled = true;
            this.cmbPrenote.Items.AddRange(new object[] {
            "Only recipients marked Prenote",
            "All recipients"});
            this.cmbPrenote.Location = new System.Drawing.Point(20, 30);
            this.cmbPrenote.Name = "cmbPrenote";
            this.cmbPrenote.Size = new System.Drawing.Size(296, 40);
            this.cmbPrenote.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.cmbPrenote, null);
            // 
            // txtFileName
            // 
            this.txtFileName.AutoSize = false;
            this.txtFileName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileName.LinkItem = null;
            this.txtFileName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFileName.LinkTopic = null;
            this.txtFileName.Location = new System.Drawing.Point(268, 318);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(143, 40);
            this.txtFileName.TabIndex = 12;
            this.txtFileName.Text = "TAACHExp.txt";
            this.ToolTip1.SetToolTip(this.txtFileName, "The filename for entries not in separate files");
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.cmdPrenote);
            this.Frame3.Controls.Add(this.cmbPrenote);
            this.Frame3.Location = new System.Drawing.Point(30, 378);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(336, 150);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Prenote";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // cmdPrenote
            // 
            this.cmdPrenote.AppearanceKey = "actionButton";
            this.cmdPrenote.Location = new System.Drawing.Point(20, 90);
            this.cmdPrenote.Name = "cmdPrenote";
            this.cmdPrenote.Size = new System.Drawing.Size(198, 40);
            this.cmdPrenote.TabIndex = 9;
            this.cmdPrenote.Text = "Create Prenote File";
            this.ToolTip1.SetToolTip(this.cmdPrenote, null);
            this.cmdPrenote.Click += new System.EventHandler(this.cmdPrenote_Click);
            // 
            // framPayDate
            // 
            this.framPayDate.Controls.Add(this.cboPayDateYear);
            this.framPayDate.Controls.Add(this.cboPayDate);
            this.framPayDate.Controls.Add(this.cboPayRunID);
            this.framPayDate.Controls.Add(this.lblDateSelect_3);
            this.framPayDate.Controls.Add(this.lblDateSelect_4);
            this.framPayDate.Location = new System.Drawing.Point(30, 90);
            this.framPayDate.Name = "framPayDate";
            this.framPayDate.Size = new System.Drawing.Size(285, 208);
            this.framPayDate.TabIndex = 2;
            this.framPayDate.Text = "Select Pay Date";
            this.ToolTip1.SetToolTip(this.framPayDate, null);
            // 
            // cboPayDateYear
            // 
            this.cboPayDateYear.AutoSize = false;
            this.cboPayDateYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDateYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayDateYear.FormattingEnabled = true;
            this.cboPayDateYear.Location = new System.Drawing.Point(20, 59);
            this.cboPayDateYear.Name = "cboPayDateYear";
            this.cboPayDateYear.Size = new System.Drawing.Size(140, 40);
            this.cboPayDateYear.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cboPayDateYear, null);
            this.cboPayDateYear.SelectedIndexChanged += new System.EventHandler(this.cboPayDateYear_SelectedIndexChanged);
            // 
            // cboPayDate
            // 
            this.cboPayDate.AutoSize = false;
            this.cboPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayDate.FormattingEnabled = true;
            this.cboPayDate.Location = new System.Drawing.Point(20, 148);
            this.cboPayDate.Name = "cboPayDate";
            this.cboPayDate.Size = new System.Drawing.Size(140, 40);
            this.cboPayDate.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cboPayDate, null);
            this.cboPayDate.SelectedIndexChanged += new System.EventHandler(this.cboPayDate_SelectedIndexChanged);
            // 
            // cboPayRunID
            // 
            this.cboPayRunID.AutoSize = false;
            this.cboPayRunID.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayRunID.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayRunID.FormattingEnabled = true;
            this.cboPayRunID.Location = new System.Drawing.Point(180, 148);
            this.cboPayRunID.Name = "cboPayRunID";
            this.cboPayRunID.Size = new System.Drawing.Size(85, 40);
            this.cboPayRunID.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cboPayRunID, null);
            // 
            // lblDateSelect_3
            // 
            this.lblDateSelect_3.Location = new System.Drawing.Point(20, 30);
            this.lblDateSelect_3.Name = "lblDateSelect_3";
            this.lblDateSelect_3.Size = new System.Drawing.Size(100, 15);
            this.lblDateSelect_3.TabIndex = 7;
            this.lblDateSelect_3.Text = "SELECT YEAR";
            this.ToolTip1.SetToolTip(this.lblDateSelect_3, null);
            // 
            // lblDateSelect_4
            // 
            this.lblDateSelect_4.Location = new System.Drawing.Point(20, 119);
            this.lblDateSelect_4.Name = "lblDateSelect_4";
            this.lblDateSelect_4.Size = new System.Drawing.Size(195, 15);
            this.lblDateSelect_4.TabIndex = 6;
            this.lblDateSelect_4.Text = "SELECT PAY DATE AND RUN";
            this.ToolTip1.SetToolTip(this.lblDateSelect_4, null);
            // 
            // T2KEffectiveEntryDate
            // 
            this.T2KEffectiveEntryDate.Location = new System.Drawing.Point(233, 30);
            this.T2KEffectiveEntryDate.Mask = "00/00/0000";
            this.T2KEffectiveEntryDate.MaxLength = 10;
            this.T2KEffectiveEntryDate.Name = "T2KEffectiveEntryDate";
            this.T2KEffectiveEntryDate.Size = new System.Drawing.Size(115, 40);
            this.T2KEffectiveEntryDate.TabIndex = 0;
            this.T2KEffectiveEntryDate.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.T2KEffectiveEntryDate, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 332);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(190, 15);
            this.Label8.TabIndex = 13;
            this.Label8.Text = "GROUPED ENTRIES FILE NAME";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(159, 15);
            this.Label7.TabIndex = 1;
            this.Label7.Text = "EFFECTIVE ENTRY DATE";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(138, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.ToolTip1.SetToolTip(this.cmdSaveContinue, null);
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmCreateACHFiles
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(446, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCreateACHFiles";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "ACH Files";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCreateACHFiles_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreateACHFiles_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrenote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPayDate)).EndInit();
            this.framPayDate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.T2KEffectiveEntryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSaveContinue;
    }
}
