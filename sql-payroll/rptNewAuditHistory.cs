//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptNewAuditHistory.
	/// </summary>
	public partial class rptNewAuditHistory : BaseSectionReport
	{
		public rptNewAuditHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Audit History";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewAuditHistory InstancePtr
		{
			get
			{
				return (rptNewAuditHistory)Sys.GetInstance(typeof(rptNewAuditHistory));
			}
		}

		protected rptNewAuditHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				rsEmployeeInfo?.Dispose();
                rsEmployeeInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewAuditHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsEmployeeInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					rsData.MoveNext();
					eArgs.EOF = rsData.EndOfFile();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniname As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
			rsData.OpenRecordset("Select * from AuditChanges", modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				rsEmployeeInfo.OpenRecordset("SELECT * FROM tblEmployeeMaster");
			}
			else
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
				{
					MessageBox.Show("No Info Found");
					this.Cancel();
					this.Close();
				}
				this.Cancel();
			}
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			// Me.Zoom = -1
			// 
			// Call SetPrintProperties(Me)
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
			{
				modGlobalRoutines.UpdatePayrollStepTable("AuditReport");
			}
			else
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// If rsEmployeeInfo.FindFirstRecord("EmployeeNumber", EscapeQuotes(rsData.Fields("UserField1"))) Then
			if (rsEmployeeInfo.FindFirst("employeenumber = '" + modGlobalFunctions.EscapeQuotes(rsData.Get_Fields("userfield1")) + "'"))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) != "")
				{
					txtEmployee.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields_String("UserField1") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("MiddleName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig"))));
				}
				else
				{
					txtEmployee.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields_String("UserField1") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsEmployeeInfo.Get_Fields_String("Desig"))));
				}
			}
			else
			{
				txtEmployee.Text = rsData.Get_Fields_String("UserField1") + " UNKNOWN";
			}
			txtUser.Text = rsData.Get_Fields_String("UserID");
			txtDateTimeField.Text = FCConvert.ToString(rsData.Get_Fields_DateTime("DateUpdated")) + " " + FCConvert.ToString(rsData.Get_Fields("TimeUpdated"));
			fldLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Location")));
			fldDescription.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
