﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3C.
	/// </summary>
	partial class rptW3C
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW3C));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtW2Type = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chk941 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkThirdParty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chk943 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkMed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkMilitary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkHshld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkCT1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chk944 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrFedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrSSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrMedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevAllocatedTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrAllocatedTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrentBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkAdjustmentNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkAdjustmentYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtContactPerson = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAreaCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFaxAreaCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Type)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk941)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkThirdParty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk943)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMilitary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHshld)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCT1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk944)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrFedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevAllocatedTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrAllocatedTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustmentNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustmentYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPerson)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAreaCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFaxAreaCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtYear,
            this.txtW2Type,
            this.txtEmployerAddress1,
            this.txtEmployerAddress2,
            this.txtEmployerAddress3,
            this.txtEmployerName,
            this.chk941,
            this.chkThirdParty,
            this.chk943,
            this.chkMed,
            this.chkMilitary,
            this.chkHshld,
            this.chkCT1,
            this.chk944,
            this.txtPrevWages,
            this.txtCurrWages,
            this.txtPrevSSWages,
            this.txtCurrSSWages,
            this.txtPrevMedicare,
            this.txtCurrMedicare,
            this.txtPrevSSTips,
            this.txtCurrSSTips,
            this.txtPrevNonQualifiedPlans,
            this.txtCurrNonQualifiedPlans,
            this.txtPrevFedTax,
            this.txtCurrFedTax,
            this.txtPrevSSTax,
            this.txtCurrSSTax,
            this.txtPrevMedTax,
            this.txtCurrMedTax,
            this.txtPrevAllocatedTips,
            this.txtCurrAllocatedTips,
            this.txtPrevDependentCare,
            this.txtCurrDependentCare,
            this.txtPrevBox14,
            this.txtCurrentBox14,
            this.txtPrevBox12,
            this.txtCurrBox12,
            this.txtPrevStateWage,
            this.txtCurrStateWages,
            this.txtPrevStateTax,
            this.txtCurrStateTax,
            this.txtExplanation,
            this.chkAdjustmentNo,
            this.chkAdjustmentYes,
            this.txtDateFiled,
            this.txtTitle,
            this.txtDate,
            this.txtEmail,
            this.txtContactPerson,
            this.txtAreaCode,
            this.txtFaxAreaCode,
            this.txtPhone,
            this.txtFax});
			this.Detail.Height = 9.979167F;
			this.Detail.Name = "Detail";
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1666667F;
			this.txtYear.Left = 0.875F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "text-align: right";
			this.txtYear.Text = null;
			this.txtYear.Top = 0.1979167F;
			this.txtYear.Width = 0.6145833F;
			// 
			// txtW2Type
			// 
			this.txtW2Type.Height = 0.1666667F;
			this.txtW2Type.Left = 2.125F;
			this.txtW2Type.Name = "txtW2Type";
			this.txtW2Type.Text = null;
			this.txtW2Type.Top = 0.1979167F;
			this.txtW2Type.Width = 0.2395833F;
			// 
			// txtEmployerAddress1
			// 
			this.txtEmployerAddress1.Height = 0.1666667F;
			this.txtEmployerAddress1.Left = 0.3125F;
			this.txtEmployerAddress1.Name = "txtEmployerAddress1";
			this.txtEmployerAddress1.Text = null;
			this.txtEmployerAddress1.Top = 0.7291667F;
			this.txtEmployerAddress1.Width = 2.78125F;
			// 
			// txtEmployerAddress2
			// 
			this.txtEmployerAddress2.Height = 0.1666667F;
			this.txtEmployerAddress2.Left = 0.3125F;
			this.txtEmployerAddress2.Name = "txtEmployerAddress2";
			this.txtEmployerAddress2.Text = null;
			this.txtEmployerAddress2.Top = 0.8958333F;
			this.txtEmployerAddress2.Width = 2.78125F;
			// 
			// txtEmployerAddress3
			// 
			this.txtEmployerAddress3.Height = 0.1666667F;
			this.txtEmployerAddress3.Left = 0.3125F;
			this.txtEmployerAddress3.Name = "txtEmployerAddress3";
			this.txtEmployerAddress3.Text = null;
			this.txtEmployerAddress3.Top = 1.0625F;
			this.txtEmployerAddress3.Width = 2.78125F;
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.Height = 0.1666667F;
			this.txtEmployerName.Left = 0.3125F;
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Text = null;
			this.txtEmployerName.Top = 0.5625F;
			this.txtEmployerName.Width = 2.78125F;
			// 
			// chk941
			// 
			this.chk941.Height = 0.15625F;
			this.chk941.Left = 3.333333F;
			this.chk941.Name = "chk941";
			this.chk941.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chk941.Text = "X";
			this.chk941.Top = 0.71875F;
			this.chk941.Width = 0.15625F;
			// 
			// chkThirdParty
			// 
			this.chkThirdParty.Height = 0.15625F;
			this.chkThirdParty.Left = 7.041667F;
			this.chkThirdParty.Name = "chkThirdParty";
			this.chkThirdParty.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.chkThirdParty.Text = "X";
			this.chkThirdParty.Top = 0.71875F;
			this.chkThirdParty.Width = 0.15625F;
			// 
			// chk943
			// 
			this.chk943.Height = 0.15625F;
			this.chk943.Left = 4.333333F;
			this.chk943.Name = "chk943";
			this.chk943.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chk943.Text = "X";
			this.chk943.Top = 0.71875F;
			this.chk943.Width = 0.15625F;
			// 
			// chkMed
			// 
			this.chkMed.Height = 0.15625F;
			this.chkMed.Left = 4.333333F;
			this.chkMed.Name = "chkMed";
			this.chkMed.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkMed.Text = "X";
			this.chkMed.Top = 1.145833F;
			this.chkMed.Width = 0.15625F;
			// 
			// chkMilitary
			// 
			this.chkMilitary.Height = 0.15625F;
			this.chkMilitary.Left = 3.833333F;
			this.chkMilitary.Name = "chkMilitary";
			this.chkMilitary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkMilitary.Text = "X";
			this.chkMilitary.Top = 0.71875F;
			this.chkMilitary.Width = 0.15625F;
			// 
			// chkHshld
			// 
			this.chkHshld.Height = 0.15625F;
			this.chkHshld.Left = 3.833333F;
			this.chkHshld.Name = "chkHshld";
			this.chkHshld.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkHshld.Text = "X";
			this.chkHshld.Top = 1.145833F;
			this.chkHshld.Width = 0.15625F;
			// 
			// chkCT1
			// 
			this.chkCT1.Height = 0.15625F;
			this.chkCT1.Left = 3.333333F;
			this.chkCT1.Name = "chkCT1";
			this.chkCT1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkCT1.Text = "X";
			this.chkCT1.Top = 1.145833F;
			this.chkCT1.Width = 0.15625F;
			// 
			// chk944
			// 
			this.chk944.Height = 0.15625F;
			this.chk944.Left = 4.833333F;
			this.chk944.Name = "chk944";
			this.chk944.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.chk944.Text = "X";
			this.chk944.Top = 0.71875F;
			this.chk944.Width = 0.15625F;
			// 
			// txtPrevWages
			// 
			this.txtPrevWages.Height = 0.1666667F;
			this.txtPrevWages.Left = 0F;
			this.txtPrevWages.Name = "txtPrevWages";
			this.txtPrevWages.Style = "text-align: right";
			this.txtPrevWages.Text = null;
			this.txtPrevWages.Top = 2.479167F;
			this.txtPrevWages.Width = 1.760417F;
			// 
			// txtCurrWages
			// 
			this.txtCurrWages.Height = 0.1666667F;
			this.txtCurrWages.Left = 2F;
			this.txtCurrWages.Name = "txtCurrWages";
			this.txtCurrWages.Style = "text-align: right";
			this.txtCurrWages.Text = null;
			this.txtCurrWages.Top = 2.479167F;
			this.txtCurrWages.Width = 1.697917F;
			// 
			// txtPrevSSWages
			// 
			this.txtPrevSSWages.Height = 0.1666667F;
			this.txtPrevSSWages.Left = 0F;
			this.txtPrevSSWages.Name = "txtPrevSSWages";
			this.txtPrevSSWages.Style = "text-align: right";
			this.txtPrevSSWages.Text = null;
			this.txtPrevSSWages.Top = 2.791667F;
			this.txtPrevSSWages.Width = 1.760417F;
			// 
			// txtCurrSSWages
			// 
			this.txtCurrSSWages.Height = 0.1666667F;
			this.txtCurrSSWages.Left = 2F;
			this.txtCurrSSWages.Name = "txtCurrSSWages";
			this.txtCurrSSWages.Style = "text-align: right";
			this.txtCurrSSWages.Text = null;
			this.txtCurrSSWages.Top = 2.791667F;
			this.txtCurrSSWages.Width = 1.697917F;
			// 
			// txtPrevMedicare
			// 
			this.txtPrevMedicare.Height = 0.1666667F;
			this.txtPrevMedicare.Left = 0F;
			this.txtPrevMedicare.Name = "txtPrevMedicare";
			this.txtPrevMedicare.Style = "text-align: right";
			this.txtPrevMedicare.Text = null;
			this.txtPrevMedicare.Top = 3.104167F;
			this.txtPrevMedicare.Width = 1.760417F;
			// 
			// txtCurrMedicare
			// 
			this.txtCurrMedicare.Height = 0.1666667F;
			this.txtCurrMedicare.Left = 2F;
			this.txtCurrMedicare.Name = "txtCurrMedicare";
			this.txtCurrMedicare.Style = "text-align: right";
			this.txtCurrMedicare.Text = null;
			this.txtCurrMedicare.Top = 3.104167F;
			this.txtCurrMedicare.Width = 1.697917F;
			// 
			// txtPrevSSTips
			// 
			this.txtPrevSSTips.Height = 0.1666667F;
			this.txtPrevSSTips.Left = 0F;
			this.txtPrevSSTips.Name = "txtPrevSSTips";
			this.txtPrevSSTips.Style = "text-align: right";
			this.txtPrevSSTips.Text = null;
			this.txtPrevSSTips.Top = 3.416667F;
			this.txtPrevSSTips.Width = 1.760417F;
			// 
			// txtCurrSSTips
			// 
			this.txtCurrSSTips.Height = 0.1666667F;
			this.txtCurrSSTips.Left = 2F;
			this.txtCurrSSTips.Name = "txtCurrSSTips";
			this.txtCurrSSTips.Style = "text-align: right";
			this.txtCurrSSTips.Text = null;
			this.txtCurrSSTips.Top = 3.416667F;
			this.txtCurrSSTips.Width = 1.697917F;
			// 
			// txtPrevNonQualifiedPlans
			// 
			this.txtPrevNonQualifiedPlans.Height = 0.1666667F;
			this.txtPrevNonQualifiedPlans.Left = 0F;
			this.txtPrevNonQualifiedPlans.Name = "txtPrevNonQualifiedPlans";
			this.txtPrevNonQualifiedPlans.Style = "text-align: right";
			this.txtPrevNonQualifiedPlans.Text = null;
			this.txtPrevNonQualifiedPlans.Top = 4F;
			this.txtPrevNonQualifiedPlans.Width = 1.760417F;
			// 
			// txtCurrNonQualifiedPlans
			// 
			this.txtCurrNonQualifiedPlans.Height = 0.1666667F;
			this.txtCurrNonQualifiedPlans.Left = 2F;
			this.txtCurrNonQualifiedPlans.Name = "txtCurrNonQualifiedPlans";
			this.txtCurrNonQualifiedPlans.Style = "text-align: right";
			this.txtCurrNonQualifiedPlans.Text = null;
			this.txtCurrNonQualifiedPlans.Top = 4F;
			this.txtCurrNonQualifiedPlans.Width = 1.697917F;
			// 
			// txtPrevFedTax
			// 
			this.txtPrevFedTax.Height = 0.1666667F;
			this.txtPrevFedTax.Left = 4.0625F;
			this.txtPrevFedTax.Name = "txtPrevFedTax";
			this.txtPrevFedTax.Style = "text-align: right";
			this.txtPrevFedTax.Text = null;
			this.txtPrevFedTax.Top = 2.479167F;
			this.txtPrevFedTax.Width = 1.572917F;
			// 
			// txtCurrFedTax
			// 
			this.txtCurrFedTax.Height = 0.1666667F;
			this.txtCurrFedTax.Left = 5.9375F;
			this.txtCurrFedTax.Name = "txtCurrFedTax";
			this.txtCurrFedTax.Style = "text-align: right";
			this.txtCurrFedTax.Text = null;
			this.txtCurrFedTax.Top = 2.479167F;
			this.txtCurrFedTax.Width = 1.447917F;
			// 
			// txtPrevSSTax
			// 
			this.txtPrevSSTax.Height = 0.1666667F;
			this.txtPrevSSTax.Left = 4.0625F;
			this.txtPrevSSTax.Name = "txtPrevSSTax";
			this.txtPrevSSTax.Style = "text-align: right";
			this.txtPrevSSTax.Text = null;
			this.txtPrevSSTax.Top = 2.791667F;
			this.txtPrevSSTax.Width = 1.572917F;
			// 
			// txtCurrSSTax
			// 
			this.txtCurrSSTax.Height = 0.1666667F;
			this.txtCurrSSTax.Left = 5.9375F;
			this.txtCurrSSTax.Name = "txtCurrSSTax";
			this.txtCurrSSTax.Style = "text-align: right";
			this.txtCurrSSTax.Text = null;
			this.txtCurrSSTax.Top = 2.791667F;
			this.txtCurrSSTax.Width = 1.447917F;
			// 
			// txtPrevMedTax
			// 
			this.txtPrevMedTax.Height = 0.1666667F;
			this.txtPrevMedTax.Left = 4.0625F;
			this.txtPrevMedTax.Name = "txtPrevMedTax";
			this.txtPrevMedTax.Style = "text-align: right";
			this.txtPrevMedTax.Text = null;
			this.txtPrevMedTax.Top = 3.104167F;
			this.txtPrevMedTax.Width = 1.572917F;
			// 
			// txtCurrMedTax
			// 
			this.txtCurrMedTax.Height = 0.1666667F;
			this.txtCurrMedTax.Left = 5.9375F;
			this.txtCurrMedTax.Name = "txtCurrMedTax";
			this.txtCurrMedTax.Style = "text-align: right";
			this.txtCurrMedTax.Text = null;
			this.txtCurrMedTax.Top = 3.104167F;
			this.txtCurrMedTax.Width = 1.447917F;
			// 
			// txtPrevAllocatedTips
			// 
			this.txtPrevAllocatedTips.Height = 0.1666667F;
			this.txtPrevAllocatedTips.Left = 4.0625F;
			this.txtPrevAllocatedTips.Name = "txtPrevAllocatedTips";
			this.txtPrevAllocatedTips.Style = "text-align: right";
			this.txtPrevAllocatedTips.Text = null;
			this.txtPrevAllocatedTips.Top = 3.416667F;
			this.txtPrevAllocatedTips.Width = 1.572917F;
			// 
			// txtCurrAllocatedTips
			// 
			this.txtCurrAllocatedTips.Height = 0.1666667F;
			this.txtCurrAllocatedTips.Left = 5.9375F;
			this.txtCurrAllocatedTips.Name = "txtCurrAllocatedTips";
			this.txtCurrAllocatedTips.Style = "text-align: right";
			this.txtCurrAllocatedTips.Text = null;
			this.txtCurrAllocatedTips.Top = 3.416667F;
			this.txtCurrAllocatedTips.Width = 1.447917F;
			// 
			// txtPrevDependentCare
			// 
			this.txtPrevDependentCare.Height = 0.1666667F;
			this.txtPrevDependentCare.Left = 4.0625F;
			this.txtPrevDependentCare.Name = "txtPrevDependentCare";
			this.txtPrevDependentCare.Style = "text-align: right";
			this.txtPrevDependentCare.Text = null;
			this.txtPrevDependentCare.Top = 3.729167F;
			this.txtPrevDependentCare.Width = 1.572917F;
			// 
			// txtCurrDependentCare
			// 
			this.txtCurrDependentCare.Height = 0.1666667F;
			this.txtCurrDependentCare.Left = 5.9375F;
			this.txtCurrDependentCare.Name = "txtCurrDependentCare";
			this.txtCurrDependentCare.Style = "text-align: right";
			this.txtCurrDependentCare.Text = null;
			this.txtCurrDependentCare.Top = 3.729167F;
			this.txtCurrDependentCare.Width = 1.447917F;
			// 
			// txtPrevBox14
			// 
			this.txtPrevBox14.Height = 0.1666667F;
			this.txtPrevBox14.Left = 0F;
			this.txtPrevBox14.Name = "txtPrevBox14";
			this.txtPrevBox14.Style = "text-align: right";
			this.txtPrevBox14.Text = null;
			this.txtPrevBox14.Top = 4.3125F;
			this.txtPrevBox14.Width = 1.760417F;
			// 
			// txtCurrentBox14
			// 
			this.txtCurrentBox14.Height = 0.1666667F;
			this.txtCurrentBox14.Left = 2F;
			this.txtCurrentBox14.Name = "txtCurrentBox14";
			this.txtCurrentBox14.Style = "text-align: right";
			this.txtCurrentBox14.Text = null;
			this.txtCurrentBox14.Top = 4.3125F;
			this.txtCurrentBox14.Width = 1.697917F;
			// 
			// txtPrevBox12
			// 
			this.txtPrevBox12.Height = 0.1666667F;
			this.txtPrevBox12.Left = 4.0625F;
			this.txtPrevBox12.Name = "txtPrevBox12";
			this.txtPrevBox12.Style = "text-align: right";
			this.txtPrevBox12.Text = null;
			this.txtPrevBox12.Top = 4F;
			this.txtPrevBox12.Width = 1.572917F;
			// 
			// txtCurrBox12
			// 
			this.txtCurrBox12.Height = 0.1666667F;
			this.txtCurrBox12.Left = 5.9375F;
			this.txtCurrBox12.Name = "txtCurrBox12";
			this.txtCurrBox12.Style = "text-align: right";
			this.txtCurrBox12.Text = null;
			this.txtCurrBox12.Top = 4F;
			this.txtCurrBox12.Width = 1.447917F;
			// 
			// txtPrevStateWage
			// 
			this.txtPrevStateWage.Height = 0.1666667F;
			this.txtPrevStateWage.Left = 0F;
			this.txtPrevStateWage.Name = "txtPrevStateWage";
			this.txtPrevStateWage.Style = "text-align: right";
			this.txtPrevStateWage.Text = null;
			this.txtPrevStateWage.Top = 4.625F;
			this.txtPrevStateWage.Width = 1.760417F;
			// 
			// txtCurrStateWages
			// 
			this.txtCurrStateWages.Height = 0.1666667F;
			this.txtCurrStateWages.Left = 2F;
			this.txtCurrStateWages.Name = "txtCurrStateWages";
			this.txtCurrStateWages.Style = "text-align: right";
			this.txtCurrStateWages.Text = null;
			this.txtCurrStateWages.Top = 4.625F;
			this.txtCurrStateWages.Width = 1.697917F;
			// 
			// txtPrevStateTax
			// 
			this.txtPrevStateTax.Height = 0.1666667F;
			this.txtPrevStateTax.Left = 4.0625F;
			this.txtPrevStateTax.Name = "txtPrevStateTax";
			this.txtPrevStateTax.Style = "text-align: right";
			this.txtPrevStateTax.Text = null;
			this.txtPrevStateTax.Top = 4.625F;
			this.txtPrevStateTax.Width = 1.572917F;
			// 
			// txtCurrStateTax
			// 
			this.txtCurrStateTax.Height = 0.1666667F;
			this.txtCurrStateTax.Left = 5.9375F;
			this.txtCurrStateTax.Name = "txtCurrStateTax";
			this.txtCurrStateTax.Style = "text-align: right";
			this.txtCurrStateTax.Text = null;
			this.txtCurrStateTax.Top = 4.625F;
			this.txtCurrStateTax.Width = 1.447917F;
			// 
			// txtExplanation
			// 
			this.txtExplanation.Height = 0.1666667F;
			this.txtExplanation.Left = 1.0625F;
			this.txtExplanation.Name = "txtExplanation";
			this.txtExplanation.OutputFormat = resources.GetString("txtExplanation.OutputFormat");
			this.txtExplanation.Text = null;
			this.txtExplanation.Top = 5.270833F;
			this.txtExplanation.Width = 6.270833F;
			// 
			// chkAdjustmentNo
			// 
			this.chkAdjustmentNo.Height = 0.15625F;
			this.chkAdjustmentNo.Left = 6.40625F;
			this.chkAdjustmentNo.Name = "chkAdjustmentNo";
			this.chkAdjustmentNo.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.chkAdjustmentNo.Text = "X";
			this.chkAdjustmentNo.Top = 5.451389F;
			this.chkAdjustmentNo.Width = 0.15625F;
			// 
			// chkAdjustmentYes
			// 
			this.chkAdjustmentYes.Height = 0.15625F;
			this.chkAdjustmentYes.Left = 5.833333F;
			this.chkAdjustmentYes.Name = "chkAdjustmentYes";
			this.chkAdjustmentYes.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.chkAdjustmentYes.Text = "X";
			this.chkAdjustmentYes.Top = 5.451389F;
			this.chkAdjustmentYes.Width = 0.15625F;
			// 
			// txtDateFiled
			// 
			this.txtDateFiled.Height = 0.1666667F;
			this.txtDateFiled.Left = 2.802083F;
			this.txtDateFiled.Name = "txtDateFiled";
			this.txtDateFiled.Text = null;
			this.txtDateFiled.Top = 5.652778F;
			this.txtDateFiled.Width = 1.59375F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1666667F;
			this.txtTitle.Left = 3.427083F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 6.041667F;
			this.txtTitle.Width = 2.40625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.177083F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Text = null;
			this.txtDate.Top = 6.041667F;
			this.txtDate.Width = 1.21875F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.1666667F;
			this.txtEmail.Left = 3.6875F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 6.666667F;
			this.txtEmail.Width = 2.78125F;
			// 
			// txtContactPerson
			// 
			this.txtContactPerson.Height = 0.1666667F;
			this.txtContactPerson.Left = 0.375F;
			this.txtContactPerson.Name = "txtContactPerson";
			this.txtContactPerson.Text = null;
			this.txtContactPerson.Top = 6.375F;
			this.txtContactPerson.Width = 2.78125F;
			// 
			// txtAreaCode
			// 
			this.txtAreaCode.Height = 0.1666667F;
			this.txtAreaCode.Left = 3.677083F;
			this.txtAreaCode.Name = "txtAreaCode";
			this.txtAreaCode.Text = null;
			this.txtAreaCode.Top = 6.375F;
			this.txtAreaCode.Width = 0.40625F;
			// 
			// txtFaxAreaCode
			// 
			this.txtFaxAreaCode.Height = 0.1666667F;
			this.txtFaxAreaCode.Left = 0.3645833F;
			this.txtFaxAreaCode.Name = "txtFaxAreaCode";
			this.txtFaxAreaCode.Text = null;
			this.txtFaxAreaCode.Top = 6.666667F;
			this.txtFaxAreaCode.Width = 0.40625F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1666667F;
			this.txtPhone.Left = 4.177083F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 6.375F;
			this.txtPhone.Width = 1.71875F;
			// 
			// txtFax
			// 
			this.txtFax.Height = 0.1666667F;
			this.txtFax.Left = 0.8645833F;
			this.txtFax.Name = "txtFax";
			this.txtFax.Text = null;
			this.txtFax.Top = 6.666667F;
			this.txtFax.Width = 1.71875F;
			// 
			// rptW3C
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtW2Type)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk941)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkThirdParty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk943)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMilitary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHshld)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCT1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk944)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrFedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrSSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrMedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevAllocatedTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrAllocatedTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrentBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustmentNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustmentYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPerson)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAreaCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFaxAreaCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtW2Type;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chk941;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkThirdParty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chk943;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkMed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkMilitary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkHshld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkCT1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chk944;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrFedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrSSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrMedTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevAllocatedTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrAllocatedTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrentBox14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevBox12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrBox12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkAdjustmentNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox chkAdjustmentYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactPerson;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAreaCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFaxAreaCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFax;
	}
}
