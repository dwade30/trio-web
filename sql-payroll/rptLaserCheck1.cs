//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptLaserCheck1.
	/// </summary>
	public partial class rptLaserCheck1 : BaseSectionReport
	{
		public rptLaserCheck1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
        {
            this.Name = "Checks";
            if (_InstancePtr == null)
                _InstancePtr = this;
            Grid = new FCGrid();
            Grid.Cols = 12;
            Grid.Rows = 1;
            GridMisc = new FCGrid();
            GridMisc.CellCharacterCasing = CharacterCasing.Normal;
            Grid.CellCharacterCasing = CharacterCasing.Normal;
            GridMisc.FixedRows = 0;
            Grid.FixedRows = 0;
            GridMisc.FixedCols = 0;
            Grid.FixedCols = 0;
        }

        public static rptLaserCheck1 InstancePtr
        {
            get
            {
                return (rptLaserCheck1)Sys.GetInstance(typeof(rptLaserCheck1));
            }
        }

        protected rptLaserCheck1 _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	rptLaserCheck1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        //=========================================================
        private bool boolPrintValidChecksSeparate;
        int lngVacCodeID;
        int lngSickCodeID;
        clsDRWrapper rsVacSickCodeTypes = new clsDRWrapper();

        //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
        //public clsDRWrapper clsLoad = new clsDRWrapper();
        public clsDRWrapper clsLoad_AutoInitialized = null;
        public clsDRWrapper clsLoad
        {
            get
            {
                if (clsLoad_AutoInitialized == null)
                {
                    clsLoad_AutoInitialized = new clsDRWrapper();
                }
                return clsLoad_AutoInitialized;
            }
            set
            {
                clsLoad_AutoInitialized = value;
            }
        }
        public bool boolPrintSig;
        public bool boolCustomCheck;
        public string strReturnAddress = string.Empty;
        public string strLogoPath = "";
        string strTag;
        clsDRWrapper clsExecute = new clsDRWrapper();
        bool boolReprinting;
        int lngCurrentCheckNumber;
        DateTime dtPayDate;
        bool boolMoreThanNinePays;
        // if there are more than 9 pay categories
        bool boolMoreThanNineDeds;
        // if there are more than 9 deduction categories
        clsDRWrapper clsDeductions = new clsDRWrapper();
        clsDRWrapper clsPay = new clsDRWrapper();
        clsDRWrapper clsVacSick = new clsDRWrapper();
        clsDRWrapper clsSick = new clsDRWrapper();
        clsDRWrapper clsVacSickOther1 = new clsDRWrapper();
        clsDRWrapper clsVacSickOther2 = new clsDRWrapper();
        clsDRWrapper clsVacSickOther3 = new clsDRWrapper();
        clsDRWrapper clsVacation = new clsDRWrapper();
        clsDRWrapper clsEMatch = new clsDRWrapper();
        public FCGrid Grid;
        public FCGrid GridMisc;
        // vbPorter upgrade warning: intNumPays As int	OnWriteFCConvert.ToInt32(
        int intNumPays;
        // vbPorter upgrade warning: intNumDeds As int	OnWriteFCConvert.ToInt32(
        int intNumDeds;
        int[] arPayCodes = new int[100 + 1];
        int[] arDedCodes = null;
        string[] arPayDescriptions = new string[100 + 1];
        string[] arPayTypes = new string[100 + 1];
        string[] arDedDescriptions = null;
        //clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
        int intCheckType;
        string strPrintFont = "";
        int lngWarrantNumber;
        int intChecksToPrint;
        int intNumStubs;
        int intCheckBodyPosition;
        float sngLaserAlignment;
        clsDRWrapper clsDDBanks = new clsDRWrapper();
        clsDRWrapper clsTandAChecks = new clsDRWrapper();
        clsDRWrapper clsStates = new clsDRWrapper();
        bool boolACH;
        clsDRWrapper clsYTDDeductions = new clsDRWrapper();
        string strPFont;
        bool boolDoubleStub;
        int intMaxStubLines;
        int intMaxStubDDLines;
        clsDRWrapper clsDDDeductions = new clsDRWrapper();
        clsDRWrapper clsBanks = new clsDRWrapper();
        int plngStartingCheckNumber;
        bool boolIgnorePrinterFonts;
        bool boolPrintValidChecksOnly;
        // Real checks
        bool boolPrintInvalidChecksOnly;
        // 100% DD and EFT Checks that print as Void
        bool[] boolAryInvalidCheck = null;
        public System.Collections.Generic.List<GrapeCity.ActiveReports.Document.Section.Page> invalidCheckPages = new System.Collections.Generic.List<GrapeCity.ActiveReports.Document.Section.Page>();
        int lngCheckPage;
        int lngInvalidCheckNumber;
        int lngValidCheckNumber;
        public int lngSignatureID;
        private int lngOrigStartCheckNo;
        private int lngOrigEndCheckNo;
        private string strESPFolder = string.Empty;
        private string strESPChecks = string.Empty;
        //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
        //public clsDRWrapper clsFormat = new clsDRWrapper();
        public clsDRWrapper clsFormat_AutoInitialized = null;
        public clsDRWrapper clsFormat
        {
            get
            {
                if (clsFormat_AutoInitialized == null)
                {
                    clsFormat_AutoInitialized = new clsDRWrapper();
                }
                return clsFormat_AutoInitialized;
            }
            set
            {
                clsFormat_AutoInitialized = value;
            }
        }
        private bool boolMailer;
        private int intMaxMatchLines;
        private bool boolShowIndividualPayRates;
        const int CNSTCheckColBank = 55;
        const int CNSTCheckColBankName = 56;
        const int CNSTCheckColBankRouting = 57;
        const int CNSTCheckColBankAccount = 58;
        const int CNSTFICACOL = 46;
        const int CNSTMEDCOL = 47;
        const int CNSTYTDFICACOL = 48;
        const int CNSTYTDMEDCOL = 49;
        const int CNSTDEPTDIVDESCCOL = 50;
        const int CNSTYTDFedGrossCol = 51;
        const int CNSTYTDFicaGrossCol = 52;
        const int CNSTYTDMedGrossCol = 53;
        const int CNSTYTDStateGrossCol = 54;
        const int CNSTMAXGRIDCOLS = 59;
        const int CNSTMatchDescCol = 8;
        const int CNSTMatchAmountCol = 9;
        const int CNSTMatchYTDAmountCol = 10;
        private bool boolLaserDblStub;
        public FCCollection VacSickHistoryColl = new FCCollection();
        private int intNonNegotiableFormat;
        private bool boolPrintPayPeriod;
        private bool boolNonNegotiable;
        private cPayRun pRun;
        private int lngHighestPrintedValidCheck;
        private int lngHighestPrintedInvalidCheck;
        private int lngCheckBankID;
        private clsDRWrapper rsLeave = new clsDRWrapper();
        private Dictionary<int, string> VacSickDescriptions = new Dictionary<int, string>();
        private bool showRemoveChecks = false;

        #region Public Properties

        public cPayRun PayRun
        {
            get
            {
                cPayRun PayRun = null;
                PayRun = pRun;
                return PayRun;
            }
        }

        public bool PrintPayPeriod
        {
            get
            {
                bool PrintPayPeriod = false;
                PrintPayPeriod = boolPrintPayPeriod;
                return PrintPayPeriod;
            }
        }

        public bool NonNegotiable
        {
            get
            {
                bool NonNegotiable = false;
                NonNegotiable = boolNonNegotiable;
                return NonNegotiable;
            }
        }

        public int NonNegotiableFormat
        {
            get
            {
                int NonNegotiableFormat = 0;
                NonNegotiableFormat = intNonNegotiableFormat;
                return NonNegotiableFormat;
            }
        }

        #endregion

        public bool ShowIndividualPayRates
        {
            get
            {
                bool ShowIndividualPayRates = false;
                ShowIndividualPayRates = boolShowIndividualPayRates;
                return ShowIndividualPayRates;
            }
        }

        private void ClearVacSickHistory()
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                while (VacSickHistoryColl.Count > 0)
                {
                    VacSickHistoryColl.Remove(1);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "In ClearVacSickHistory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
        {
            int const_printtoolid;
            int cnt;
            boolNonNegotiable = false;

            intChecksToPrint = 0;
            lngCheckPage = 0;
            lngHighestPrintedValidCheck = 0;
            lngHighestPrintedInvalidCheck = 0;
            // start with paychecks
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                eArgs.EOF = clsLoad.EndOfFile();
                if (eArgs.EOF == true)
                {
                    eArgs.EOF = clsDDBanks.EndOfFile();
                    if (eArgs.EOF == true)
                    {
                        eArgs.EOF = clsTandAChecks.EndOfFile();
                    }
                }
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check FetchData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            int lngEnd = 0;
            int lngOrigPages = 0;
            int lngOrigSize = 0;

            try
            {
                fecherFoundation.Information.Err().Clear();
                int x;
                cBankService bServ = new cBankService();
                cSettingsController setCont = new cSettingsController();
                string strTemp;
                string strCheckType = "";
                strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
                var useBankLastCheckNumber = fecherFoundation.Strings.LCase(strTemp);

                switch (useBankLastCheckNumber)
                {
                    case "no":
                    case "bank":
                        strCheckType = "";

                        break;
                    case "check type":
                        strCheckType = "Payroll";

                        break;
                }

                if (lngHighestPrintedValidCheck > 0 && lngCheckBankID > 0 && modGlobalConstants.Statics.gboolBD)
                {
                    bServ.UpdateLastCheckNumberForBankCheckType(lngCheckBankID, lngHighestPrintedValidCheck, strCheckType);
                }

                if (lngHighestPrintedInvalidCheck > 0 && lngCheckBankID > 0 && modGlobalConstants.Statics.gboolBD)
                {
                    bServ.UpdateLastCheckNumberForBankCheckType(lngCheckBankID, lngHighestPrintedInvalidCheck, "Payroll NonNegotiable");
                }

                if (!boolPrintValidChecksSeparate || this.Document.Pages.Count <= 0) return;

                lngEnd = this.Document.Pages.Count;
                lngOrigPages = lngEnd;
                lngOrigSize = Information.UBound(boolAryInvalidCheck, 1);

                for (x = this.Document.Pages.Count - 1; x >= 1; x--)
                {
                    if (Information.UBound(boolAryInvalidCheck, 1) < x || !boolAryInvalidCheck[x]) continue;

                    invalidCheckPages.Add(this.Document.Pages[x]);
                    this.Document.Pages.Insert(lngEnd, this.Document.Pages[x]);
                    lngEnd -= 1;
                    this.Document.Pages.RemoveAt(x);
                }

                // x
                if (boolAryInvalidCheck[0])
                {

                    invalidCheckPages.Add(this.Document.Pages[0]);
                    this.Document.Pages.Insert(lngEnd, this.Document.Pages[0]);
                    RemoveFirstPage();
                }

                frmWait.InstancePtr.Unload();
                fecherFoundation.Information.Err().Clear();
                frmCheckViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, boolOverRideDuplex: true, boolDontAllowPagesChoice: true);
            }
            catch (Exception ex)
            {
                frmWait.InstancePtr.Unload();
                var errNumber = FCConvert.ToString(fecherFoundation.Information.Err(ex).Number);

                var errDesc = fecherFoundation.Information.Err(ex).Description;

                MessageBox.Show(fecherFoundation.Information.Erl() != 40
                                    ? $"Error Number {errNumber}  {errDesc}\r\nOn line {fecherFoundation.Information.Erl()} in Check ReportEnd"
                                    : $"Error Number {errNumber}  {errDesc}\r\nPages: {FCConvert.ToString(lngOrigPages)} Orig. Size: {FCConvert.ToString(lngOrigSize)} index: {FCConvert.ToString(ex)}\r\nIn Check ReportEnd", "Error"
                              , MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

        }

        private void RemoveFirstPage()
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                //this.Refresh();
                //Application.DoEvents();
                this.Document.Pages.RemoveAt(0);
                //this.Document.Pages.Commit();
                //this.Refresh();
                //Application.DoEvents();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check RemoveFirstPage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                boolAryInvalidCheck = new bool[1 + 1];
                if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais" || boolMailer)
                {
                    SubReport1.Height = 5220 / 1440F;
                    // 3 and 5 eighths
                    SubReport2.Height = 5220 / 1440F;
                    SubReport2.Top = SubReport1.Top + SubReport1.Height + 1;
                    SubReport3.Top = SubReport2.Top + SubReport2.Height + 1;
                    SubReport3.Height = 5220 / 1440F;
                    SubReport1.CanShrink = false;
                    SubReport2.CanShrink = false;
                }
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            modGlobalRoutines.UpdatePayrollStepTable("Checks");

        }

        private void Detail_Format(object sender, EventArgs e)
        {
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            int intRow = 0;
            double dblTotGross = 0;
            double dblTotHours = 0;
            double dblTotDeductions = 0;
            double dblNet = 0;
            string strSQL = "";
            int intTemp = 0;
            int intCurrentPayRow = 0;
            int intCurrentDedRow = 0;
            int intCurrentMatchRow = 0;
            double dblExcessAmount = 0;
            double dblExcessHours = 0;
            double dblExcessYTD = 0;
            double dblDirectDeposit = 0;
            double dblDirectDepositChk = 0;
            double dblDirectDepositSav = 0;
            double dblTrustAmount = 0;
            string strState = "";
            string strTemp = "";
            bool boolTemp = false;
            double dblTemp = 0;
            double dblYTDGross = 0;
            double dblYTDFicaGross = 0;
            double dblYTDMedGross = 0;
            double dblYTDFedGross = 0;
            double dblYTDStateGross = 0;
            clsDRWrapper rsYTDBenefits = new clsDRWrapper();
            clsDRWrapper rsBenefits = new clsDRWrapper();
            // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
            DateTime dtStart;

            const float c3And5Eighths = 5220 / 1440F;
            const float cFullHeight = 5040 * 2 / 1440F;

            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                strTag = "";
                boolNonNegotiable = false;
                dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));

                if (boolPrintValidChecksSeparate)
                {
                    UpdateProgressBar();
                }

                const string numericFormat = "0.00";

                if (!clsLoad.EndOfFile())
                {
                    if (intCheckType == 0 && intNumStubs < 2)
                    {
                        SubReport3.Report = null;
                        SubReport3.Visible = false;
                    }

                    if (intChecksToPrint == 0)
                    {
                        // paychecks
                        dblTotGross = 0;
                        dblTotHours = 0;
                        dblTotDeductions = 0;
                        dblExcessAmount = 0;
                        dblExcessHours = 0;
                        dblExcessYTD = 0;
                        intTemp = intNumPays;
                        if (intNumPays > intMaxStubLines) intTemp = intMaxStubLines;

                        for (x = 1; x <= intMaxStubLines; x++)
                        {
                            //Application.DoEvents();
                            Grid.TextMatrix(x - 1, 0, "");
                            Grid.TextMatrix(x - 1, 1, "");
                            Grid.TextMatrix(x - 1, 2, "");
                            Grid.TextMatrix(x - 1, 11, "");
                        }

                        // x
                        intTemp = intNumDeds;
                        if (intTemp > intMaxStubLines) intTemp = intMaxStubLines;

                        for (x = 1; x <= intMaxStubLines; x++)
                        {
                            //Application.DoEvents();
                            Grid.TextMatrix(x - 1, 3, "");
                            Grid.TextMatrix(x - 1, 4, "");
                            Grid.TextMatrix(x - 1, 5, "");
                            Grid.TextMatrix(x - 1, CNSTMatchAmountCol, "");
                            Grid.TextMatrix(x - 1, CNSTMatchDescCol, "");
                            Grid.TextMatrix(x - 1, CNSTMatchYTDAmountCol, "");
                        }

                        // x
                        intCurrentPayRow = 0;
                        intCurrentDedRow = 0;
                        intCurrentMatchRow = 0;

                        // do pay categories first
                        string loadedEmployeeNumber = clsLoad.Get_Fields("employeenumber");
                        string loadedMasterRecord = clsLoad.Get_Fields_String("MasterRecord");

                        if (clsPay.FindFirst($"employeenumber = '{loadedEmployeeNumber}' and masterrecord = '{loadedMasterRecord}'"))
                        {
                            dblYTDGross = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, loadedEmployeeNumber, dtPayDate);
                            dblYTDFedGross = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, loadedEmployeeNumber, dtPayDate);
                            dblYTDFicaGross = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, loadedEmployeeNumber, dtPayDate);
                            dblYTDMedGross = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, loadedEmployeeNumber, dtPayDate);
                            dblYTDStateGross = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, loadedEmployeeNumber, dtPayDate);

                            while (clsPay.Get_Fields("employeenumber") == loadedEmployeeNumber && clsPay.Get_Fields("masterrecord") == loadedMasterRecord)
                            {
                                intRow = GetPayRow();
                                intCurrentPayRow += 1;

                                var payType = fecherFoundation.Strings.UCase(arPayTypes[intRow]);
                                double distributionHours = Conversion.Val(clsPay.Get_Fields("distributionhours"));
                                double grossPay = Conversion.Val(clsPay.Get_Fields("DGrossPay"));
                                double distpayrate = Conversion.Val(clsPay.Get_Fields("distpayrate"));

                                if (intCurrentPayRow < intMaxStubLines + 1)
                                {
                                    dblTotGross += grossPay;

                                    var currentGridRowIndex = intCurrentPayRow - 1;

                                    Grid.TextMatrix(currentGridRowIndex, 0, arPayDescriptions[intRow]);
                                    Grid.TextMatrix(currentGridRowIndex, 2, Strings.Format(grossPay, numericFormat));

                                    if (distpayrate > 0)
                                    {
                                        Grid.TextMatrix(currentGridRowIndex, 11, Strings.Format(distpayrate, numericFormat));
                                    }

                                    switch (payType)
                                    {
                                        case "DOLLARS":
                                            Grid.TextMatrix(currentGridRowIndex, 1, "");

                                            break;
                                        case "NON-PAY (HOURS)" when distributionHours < 0:
                                            Grid.TextMatrix(currentGridRowIndex, 1, Strings.Format(-1 * distributionHours, numericFormat));

                                            break;
                                        case "NON-PAY (HOURS)":
                                            Grid.TextMatrix(currentGridRowIndex, 1, "+" + Strings.Format(distributionHours, numericFormat));

                                            break;
                                        default:
                                            dblTotHours += distributionHours;
                                            Grid.TextMatrix(currentGridRowIndex, 1, Strings.Format(distributionHours, numericFormat));

                                            break;
                                    }

                                    if (intCurrentPayRow == intMaxStubLines)
                                    {
                                        dblExcessAmount = grossPay;
                                        dblExcessHours = payType == "DOLLARS" ? 0 : distributionHours;
                                    }
                                }
                                else
                                {
                                    if (grossPay > 0 || distributionHours > 0)
                                    {
                                        dblTotGross += grossPay;
                                        dblExcessAmount += grossPay;

                                        if (payType != "DOLLARS")
                                        {
                                            dblTotHours += distributionHours;
                                            dblExcessHours += distributionHours;
                                        }

                                        Grid.TextMatrix(intMaxStubLines - 1, 0, "Other");
                                        Grid.TextMatrix(intMaxStubLines - 1, 1, Strings.Format(dblExcessHours, numericFormat));
                                        Grid.TextMatrix(intMaxStubLines - 1, 2, Strings.Format(dblExcessAmount, numericFormat));
                                    }
                                }

                                clsPay.MoveNext();

                                if (clsPay.EndOfFile()) break;
                            }
                        }


                        // ** YTD

                        dblExcessAmount = 0;
                        double dblTotalYTDDeductions = 0;

                        string loadedEmpMasterEmployeeNumber = clsLoad.Get_Fields("EmployeeMasterEmployeeNumber");
                        string loadedCheckDetailMasterRecord = clsLoad.Get_Fields("tblCheckDetailMasterRecord");

                        if (clsDeductions.FindFirst($"EmployeeNumber = '{loadedEmpMasterEmployeeNumber}' and masterrecord = '{loadedMasterRecord}'"))
                        {

                            while (!clsDeductions.EndOfFile() && (clsDeductions.Get_Fields("Query1EmployeeNumber") == loadedEmpMasterEmployeeNumber
                                                                  && (loadedCheckDetailMasterRecord == loadedMasterRecord
                                                                      || (loadedMasterRecord == "" && loadedCheckDetailMasterRecord.ToUpper() == "P0")
                                                                      )
                                                                  ))
                            {
                                intRow = 0;
                                double query3DedCode = Conversion.Val(clsDeductions.Get_Fields("Query3deductioncode"));

                                for (x = 1; x <= (Information.UBound(arDedCodes, 1)); x++)
                                {

                                    if (arDedCodes[x] == query3DedCode)
                                    {
                                        intRow = x;

                                        break;
                                    }
                                }

                                double totDeductions = Conversion.Val(clsDeductions.Get_Fields("deductionamount"));
                                double ytdDeductions = Conversion.Val(clsDeductions.Get_Fields("ytddeduction"));

                                if (totDeductions != 0 || ytdDeductions != 0)
                                {
                                    intCurrentDedRow += 1;

                                    if (intCurrentDedRow < intMaxStubLines + 1)
                                    {
                                        dblTotDeductions += totDeductions;
                                        dblTotalYTDDeductions += ytdDeductions;

                                        if (intCurrentDedRow == intMaxStubLines)
                                        {
                                            dblExcessAmount = totDeductions;
                                            dblExcessYTD = ytdDeductions;
                                        }

                                        Grid.TextMatrix(intCurrentDedRow - 1, 3, arDedDescriptions[intRow]);
                                        Grid.TextMatrix(intCurrentDedRow - 1, 4, Strings.Format(totDeductions, numericFormat));
                                        Grid.TextMatrix(intCurrentDedRow - 1, 5, Strings.Format(ytdDeductions, numericFormat));
                                    }
                                    else
                                    {
                                        dblTotDeductions += totDeductions;
                                        dblTotalYTDDeductions += ytdDeductions;
                                        dblExcessAmount += totDeductions;
                                        dblExcessYTD += ytdDeductions;

                                        Grid.TextMatrix(intMaxStubLines - 1, 3, "Other");
                                        Grid.TextMatrix(intMaxStubLines - 1, 4, Strings.Format(dblExcessAmount, numericFormat));
                                        Grid.TextMatrix(intMaxStubLines - 1, 5, Strings.Format(dblExcessYTD, numericFormat));
                                    }
                                }

                                clsDeductions.MoveNext();
                            }
                        }

                        dblTotalYTDDeductions = 0;

                        if (!(clsYTDDeductions.EndOfFile() && clsDeductions.BeginningOfFile()))
                        {
                            clsDeductions.MoveFirst();

                            // still need to get ytd total to figure ytd net
                            if (clsYTDDeductions.FindFirst($"Query1EmployeeNumber = '{loadedEmpMasterEmployeeNumber}'"))
                            {
                                while (!clsYTDDeductions.EndOfFile() && clsYTDDeductions.Get_Fields("Query1EmployeeNumber") == loadedEmpMasterEmployeeNumber)
                                {
                                    dblTotalYTDDeductions += Conversion.Val(clsYTDDeductions.Get_Fields("YTDDeduction"));
                                    clsYTDDeductions.MoveNext();
                                }
                            }
                        }

                        GridMisc.TextMatrix(0, 42, Strings.Format(dblTotalYTDDeductions, numericFormat));
                        double dblBenAmount = 0;
                        rsYTDBenefits.OpenRecordset($"select sum(matchamount) as YTDBenefit,matchdeductionnumber as benefitcode,employeenumber  from tblcheckdetail where matchrecord = 1  and employeenumber = '{loadedEmpMasterEmployeeNumber}' and checkvoid = 0 and paydate between '{FCConvert.ToString(dtStart)}' and '{FCConvert.ToString(dtPayDate)}' group by employeenumber,matchdeductionnumber", "twpy0000.vb1");

                        while (!rsYTDBenefits.EndOfFile())
                        {
                            intRow = 0;
                            int dedCode = rsYTDBenefits.Get_Fields_Int32("benefitcode");
                            for (x = 1; x <= (Information.UBound(arDedCodes, 1)); x++)
                            {
                                if (arDedCodes[x] == dedCode)
                                {
                                    intRow = x;

                                    break;
                                }
                            }

                            dblBenAmount = 0;
                            rsBenefits.OpenRecordset($"Select sum(matchamount) as tot from tblcheckdetail where isnull(checkvoid,0) = 0 and masterrecord = '{loadedMasterRecord}' and employeenumber = '{loadedEmpMasterEmployeeNumber}' and matchrecord = 1 and paydate = '{FCConvert.ToString(dtPayDate)}' and matchdeductionnumber = {rsYTDBenefits.Get_Fields("benefitcode")}", "twpy0000.vb1");

                            if (!rsBenefits.EndOfFile())
                            {
                                dblBenAmount = rsBenefits.Get_Fields_Double("tot");
                            }

                            intCurrentMatchRow += 1;

                            if (intCurrentMatchRow < intMaxMatchLines)
                            {
                                Grid.TextMatrix(intCurrentMatchRow - 1, CNSTMatchDescCol, intRow > 0 ? arDedDescriptions[intRow] : "");
                                Grid.TextMatrix(intCurrentMatchRow - 1, CNSTMatchYTDAmountCol, Strings.Format(rsYTDBenefits.Get_Fields_Double("YTDBenefit"), numericFormat));
                                Grid.TextMatrix(intCurrentMatchRow - 1, CNSTMatchAmountCol, Strings.Format(dblBenAmount, numericFormat));
                            }
                            else
                            {
                                Grid.TextMatrix(intMaxMatchLines - 1, CNSTMatchDescCol, "Other");
                            }

                            rsYTDBenefits.MoveNext();
                        }

                        ClearVacSickHistory();
                        cVacSickHistoryItem vsHistory;

                        if (clsVacation.FindFirst("employeenumber = '" + loadedEmpMasterEmployeeNumber + "'"))
                        {
                            double vacationBalance = clsVacation.Get_Fields_Double("vacationbalance");

                            GridMisc.TextMatrix(0, 0, Strings.Format(vacationBalance, numericFormat));
                            vsHistory = new cVacSickHistoryItem();
                            vsHistory.Balance = vacationBalance;
                            vsHistory.Description = "Vacation";
                            vsHistory.Accrued = clsVacation.Get_Fields_Double("AccruedCurrent");
                            vsHistory.Used = clsVacation.Get_Fields_Double("UsedCurrent");
                            vsHistory.CalendarAccrued = clsVacation.Get_Fields_Double("accruedcalendarvacation");
                            vsHistory.CalendarUsed = clsVacation.Get_Fields_Double("UsedCalendarVacation");
                            vsHistory.ID = lngVacCodeID;
                            VacSickHistoryColl.Add(vsHistory);
                        }
                        else
                        {
                            GridMisc.TextMatrix(0, 0, numericFormat);
                        }

                        if (clsSick.FindFirst("employeenumber = '" + loadedEmpMasterEmployeeNumber + "'"))
                        {
                            var sickBalance = clsSick.Get_Fields_Double("sickbalance");

                            GridMisc.TextMatrix(0, 1, Strings.Format(sickBalance, numericFormat));

                            vsHistory = new cVacSickHistoryItem();
                            vsHistory.Balance = sickBalance;
                            vsHistory.Description = "Sick";
                            vsHistory.Accrued = clsSick.Get_Fields_Double("AccruedCurrent");
                            vsHistory.Used = clsSick.Get_Fields_Double("UsedCurrent");
                            vsHistory.CalendarAccrued = clsSick.Get_Fields_Double("AccruedCalendarSick");
                            vsHistory.CalendarUsed = clsSick.Get_Fields_Double("UsedCalendarSick");
                            vsHistory.ID = lngSickCodeID;
                            VacSickHistoryColl.Add(vsHistory);
                        }
                        else
                        {
                            GridMisc.TextMatrix(0, 1, numericFormat);
                        }

                        // 36 is code1
                        GridMisc.TextMatrix(0, 36, clsVacSickOther1.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}'") ? Strings.Format(clsVacSickOther1.Get_Fields_Double("UsedBalance"), numericFormat) : numericFormat);

                        // 37 is code2
                        GridMisc.TextMatrix(0, 37, clsVacSickOther2.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}'") ? Strings.Format(clsVacSickOther2.Get_Fields_Double("UsedBalance"), numericFormat) : numericFormat);

                        // 38 is code3
                        GridMisc.TextMatrix(0, 38, clsVacSickOther3.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}'") ? Strings.Format(clsVacSickOther3.Get_Fields_Double("UsedBalance"), numericFormat) : numericFormat);

                        GridMisc.TextMatrix(0, 33, clsVacSick.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}'") ? clsVacSick.Get_Fields_Double("OtherBalance").FormatAsMoney() : numericFormat);

                        AddNonVacSickLeaveToCollection(clsLoad.Get_Fields_String("EmployeeMasterEmployeeNumber"));

                        if (clsEMatch.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}' and masterrecord = '{loadedCheckDetailMasterRecord}'"))
                        {
                            GridMisc.TextMatrix(0, 25, Strings.Format(clsEMatch.Get_Fields_Double("CurrentMatchAmount"), numericFormat));
                            GridMisc.TextMatrix(0, 26, Strings.Format(clsEMatch.Get_Fields_Double("YTDMatch"), numericFormat));
                        }
                        else
                        {

                            if (clsEMatch.FindFirst($"employeenumber = '{loadedEmpMasterEmployeeNumber}'"))
                            {
                                GridMisc.TextMatrix(0, 25, Strings.Format(clsEMatch.Get_Fields_Double("CurrentMatchAmount"), numericFormat));
                                GridMisc.TextMatrix(0, 26, Strings.Format(clsEMatch.Get_Fields_Double("YTDMatch"), numericFormat));
                            }
                            else
                            {
                                GridMisc.TextMatrix(0, 25, numericFormat);
                                GridMisc.TextMatrix(0, 26, numericFormat);
                            }
                        }

                        GridMisc.TextMatrix(0, 2, "EMPLOYEE " + loadedEmpMasterEmployeeNumber);
                        GridMisc.TextMatrix(0, 3, Strings.Format(dblTotHours, numericFormat));
                        GridMisc.TextMatrix(0, 4, Strings.Format(dblTotGross, numericFormat));
                        GridMisc.TextMatrix(0, 5, Strings.Format(dblTotGross, numericFormat));
                        GridMisc.TextMatrix(0, 6, Strings.Format(clsLoad.Get_Fields_Double("FederalTaxWH"), numericFormat));
                        GridMisc.TextMatrix(0, 7, Strings.Format(clsLoad.Get_Fields_Double("FICATaxWH") + clsLoad.Get_Fields_Double("MedicareTaxWH"), numericFormat));
                        GridMisc.TextMatrix(0, CNSTFICACOL, Strings.Format(clsLoad.Get_Fields_Double("ficataxwh"), numericFormat));
                        GridMisc.TextMatrix(0, CNSTMEDCOL, Strings.Format(clsLoad.Get_Fields_Double("medicaretaxwh"), numericFormat));
                        GridMisc.TextMatrix(0, 8, Strings.Format(clsLoad.Get_Fields_Double("statetaxwh"), numericFormat));

                        GridMisc.TextMatrix(0, 23, Strings.Format(dblTotDeductions, numericFormat));
                        dblNet = dblTotGross - clsLoad.Get_Fields_Double("federaltaxwh") - clsLoad.Get_Fields_Double("medicaretaxwh") - clsLoad.Get_Fields_Double("ficataxwh") - clsLoad.Get_Fields_Double("statetaxwh");
                        dblNet -= dblTotDeductions;
                        if (dblNet != clsLoad.Get_Fields_Decimal("netpay").ToDouble())
                        {
                            dblNet = clsLoad.Get_Fields_Decimal("netpay").ToDouble();
                        }

                        dblDirectDepositChk = clsLoad.Get_Fields_Double("ddepositchkamount");
                        dblDirectDepositSav = clsLoad.Get_Fields_Double("ddepositsavamount");
                        dblDirectDeposit = dblDirectDepositChk + dblDirectDepositSav;

                        for (x = 1; x <= intMaxStubDDLines; x++)
                        {
                            Grid.TextMatrix(x - 1, 6, 0);
                            Grid.TextMatrix(x - 1, 7, "");
                        }

                        // x
                        if (dblDirectDeposit > 0)
                        {
                            boolTemp = false;

                            if (!(clsDDDeductions.EndOfFile() && clsDDDeductions.BeginningOfFile()))
                            {
                                if (!clsDDDeductions.BeginningOfFile() && clsDDDeductions.EndOfFile())
                                {
                                    clsDDDeductions.MoveFirst();
                                }

                                if (!((clsDDDeductions.Get_Fields_String("employeenumber") == loadedEmpMasterEmployeeNumber) && (clsDDDeductions.Get_Fields_String("masterrecord") == loadedCheckDetailMasterRecord)))
                                {
                                    if (clsDDDeductions.FindFirstRecord2("Employeenumber,masterrecord", loadedEmpMasterEmployeeNumber + "," + loadedCheckDetailMasterRecord, ","))
                                    {
                                        boolTemp = true;
                                    }
                                }
                                else
                                {
                                    boolTemp = true;
                                }
                            }

                            if (boolTemp)
                            {
                                x = 1;
                                dblTemp = 0;

                                while (!clsDDDeductions.EndOfFile())
                                {
                                    if (clsDDDeductions.Get_Fields_String("employeenumber") != loadedEmpMasterEmployeeNumber || clsDDDeductions.Get_Fields_String("masterrecord") != loadedCheckDetailMasterRecord)
                                    {
                                        // not the same check any more
                                        break;
                                    }

                                    double dDepositAmount = clsDDDeductions.Get_Fields_Double("ddepositamount");

                                    if (x <= intMaxStubDDLines)
                                    {
                                        Grid.TextMatrix(x - 1, 6, Strings.Format(dDepositAmount, numericFormat));
                                        if (x == intMaxStubDDLines) dblTemp = dDepositAmount;
                                        boolTemp = false;

                                        if (!(clsBanks.EndOfFile() && clsBanks.BeginningOfFile()))
                                        {
                                            int ddBankNumber = clsDDDeductions.Get_Fields_Int32("ddbanknumber");

                                            if (clsBanks.Get_Fields("ID") != ddBankNumber)
                                            {
                                                if (clsBanks.FindFirstRecord("ID", ddBankNumber))
                                                {
                                                    boolTemp = true;
                                                }
                                            }
                                            else
                                            {
                                                boolTemp = true;
                                            }
                                        }

                                        if (boolTemp)
                                        {
                                            Grid.TextMatrix(x - 1, 7, clsBanks.Get_Fields_String("SHORTDESCRIPTION").Trim());
                                        }
                                        else
                                        {
                                            if (x <= intMaxStubLines)
                                            {
                                                Grid.TextMatrix(x - 1, 7, "Deposit #" + FCConvert.ToString(x));
                                            }
                                            else
                                            {
                                                Grid.TextMatrix(intMaxStubDDLines - 1, 7, "Other");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Grid.TextMatrix(intMaxStubDDLines - 1, 7, "Other");
                                        dblTemp += dDepositAmount;
                                        Grid.TextMatrix(intMaxStubDDLines - 1, 7, Strings.Format(dblTemp, numericFormat));
                                    }

                                    x += 1;
                                    clsDDDeductions.MoveNext();
                                }
                            }
                        }

                        if (dblNet - dblDirectDeposit < 0.01)
                        {
                            GridMisc.TextMatrix(0, 10, "** VOID *** VOID *** VOID *** VOID *** VOID *** VOID *** VOID **");
                            strTag = "INVALID CHECK";
                            boolNonNegotiable = true;

                            GridMisc.TextMatrix(0, 11, Strings.Format(dblNet, numericFormat) == Strings.Format(dblDirectDeposit, numericFormat)
                                                    ? Strings.Format(dblDirectDeposit, numericFormat)
                                                    : Strings.Format(dblNet - dblDirectDeposit, numericFormat));

                            GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
                        }
                        else
                        {
                            GridMisc.TextMatrix(0, 10, modConvertAmountToString.ConvertAmountToString(FCConvert.ToDecimal(dblNet - dblDirectDeposit)) + " ");

                            if (GridMisc.TextMatrix(0, 10).Length <= 64)
                            {
                                GridMisc.TextMatrix(0, 10, GridMisc.TextMatrix(0, 10) + Strings.StrDup(65 - GridMisc.TextMatrix(0, 10).Length, "*"));
                            }


                            GridMisc.TextMatrix(0, 11, Strings.Format(dblNet - dblDirectDeposit, numericFormat));
                            GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
                        }

                        GridMisc.TextMatrix(0, 9, Strings.Format(dblNet, numericFormat));

                        if (!boolPrintValidChecksSeparate || strTag == string.Empty)
                        {
                            lngCurrentCheckNumber = lngValidCheckNumber;
                            lngHighestPrintedValidCheck = lngValidCheckNumber;
                            lngValidCheckNumber += 1;
                        }
                        else
                        {
                            lngCurrentCheckNumber = lngInvalidCheckNumber;
                            lngHighestPrintedInvalidCheck = lngInvalidCheckNumber;
                            lngInvalidCheckNumber += 1;
                        }

                        GridMisc.TextMatrix(0, 12, "CHECK " + FCConvert.ToString(lngCurrentCheckNumber));
                        GridMisc.TextMatrix(0, 13, Strings.Format(dblYTDGross, numericFormat));
                        GridMisc.TextMatrix(0, 14, Strings.Format(clsLoad.Get_Fields_Double("YTDFedTax"), numericFormat));
                        GridMisc.TextMatrix(0, 15, Strings.Format(clsLoad.Get_Fields_Double("YTDFICA") + clsLoad.Get_Fields_Double("ytdmedicare"), numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDFICACOL, Strings.Format(clsLoad.Get_Fields_Double("ytdfica"), numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDMEDCOL, Strings.Format(clsLoad.Get_Fields_Double("ytdmedicare"), numericFormat));
                        GridMisc.TextMatrix(0, 16, Strings.Format(clsLoad.Get_Fields_Double("YTDStateTax"), numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDFedGrossCol, Strings.Format(dblYTDFedGross, numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDFicaGrossCol, Strings.Format(dblYTDFicaGross, numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDMedGrossCol, Strings.Format(dblYTDMedGross, numericFormat));
                        GridMisc.TextMatrix(0, CNSTYTDStateGrossCol, Strings.Format(dblYTDStateGross, numericFormat));
                        GridMisc.TextMatrix(0, 17, Strings.Format(dblYTDGross - clsLoad.Get_Fields_Double("ytdfedtax") - clsLoad.Get_Fields_Double("ytdfica") - clsLoad.Get_Fields_Double("ytdmedicare") - clsLoad.Get_Fields_Double("ytdstatetax") - dblTotalYTDDeductions, numericFormat));
                        GridMisc.TextMatrix(0, 18, $"{clsLoad.Get_Fields("firstname")} {clsLoad.Get_Fields_String("MiddleName")} {clsLoad.Get_Fields_String("Lastname")} {clsLoad.Get_Fields_String("DESIG")}");
                        GridMisc.TextMatrix(0, 19, clsLoad.Get_Fields_String("address1"));
                        GridMisc.TextMatrix(0, 20, clsLoad.Get_Fields_String("address2"));

                        strState = clsStates.FindFirstRecord("id", clsLoad.Get_Fields_String("state").ToIntegerValue()) ? clsStates.Get_Fields_String("State") : "";

                        GridMisc.TextMatrix(0, 21, $"{clsLoad.Get_Fields_String("city")} {strState} {clsLoad.Get_Fields_String("zip")}");

                        if (GridMisc.TextMatrix(0, 20).Trim() == "")
                        {
                            GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                            GridMisc.TextMatrix(0, 21, "");
                        }

                        if (GridMisc.TextMatrix(0, 19).Trim() == "")
                        {
                            GridMisc.TextMatrix(0, 19, GridMisc.TextMatrix(0, 20));
                            GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                            GridMisc.TextMatrix(0, 21, "");
                        }

                        string deptDiv = clsLoad.Get_Fields_String("deptdiv");

                        GridMisc.TextMatrix(0, 43, deptDiv);

                        if (modGlobalVariables.Statics.gboolBudgetary)
                        {
                            strTemp = "";

                            if (fecherFoundation.Strings.Trim(deptDiv) != "")
                            {
                                strTemp = Strings.InStr(1, deptDiv, "-", CompareConstants.vbBinaryCompare) > 0
                                    ? Strings.Left(deptDiv, Strings.InStr(1, deptDiv, "-", CompareConstants.vbBinaryCompare) - 1)
                                    : deptDiv;
                            }

                            deptDiv = modAccountTitle.GetDepartmentName(strTemp, true);
                        }

                        GridMisc.TextMatrix(0, CNSTDEPTDIVDESCCOL, deptDiv);


                        GridMisc.TextMatrix(0, 22, loadedEmpMasterEmployeeNumber);
                        GridMisc.TextMatrix(0, 27, "R");

                        GridMisc.TextMatrix(0, 29, dblDirectDepositChk);
                        GridMisc.TextMatrix(0, 34, dblDirectDepositSav);
                        GridMisc.TextMatrix(0, 35, clsLoad.Get_Fields_Double("EICAmount"));

                        if (clsLoad.Get_Fields_Boolean("PrintPayRate"))
                        {
                            GridMisc.TextMatrix(0, 31, Strings.Format(clsLoad.Get_Fields_Double("BasePayRate"), numericFormat));
                        }
                        else
                        {
                            GridMisc.TextMatrix(0, 31, "");
                        }

                        // 
                        if (boolReprinting)
                        {
                            int checkNumber = clsLoad.Get_Fields_Int32("checknumber");

                            if (checkNumber != lngCurrentCheckNumber)
                            {
                                if ((checkNumber < plngStartingCheckNumber) || (lngOrigStartCheckNo > (lngOrigEndCheckNo - lngOrigStartCheckNo) + plngStartingCheckNumber))
                                {
                                    // if original check # is less than the new number or the original numbers have no overlap at all
                                    strSQL = "Insert into tblCheckHistory ";
                                    strSQL += "(CheckNumber,ReplacedByCheckNumber,EmployeeNumber,EmployeeName,Amount,CheckDate,CheckRun,CheckType,Status,ReportNumber,WarrantNumber) values ";
                                    strSQL += "(" + FCConvert.ToString(checkNumber) + "," + FCConvert.ToString(lngCurrentCheckNumber) + ",'" + loadedEmpMasterEmployeeNumber + "'";
                                    strSQL += ",'" + GridMisc.TextMatrix(0, 18).Replace("'", "''") + "'," + FCConvert.ToString(dblNet - dblDirectDeposit) + ",'" + FCConvert.ToString(dtPayDate) + "'," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + ",'R','V',0," + FCConvert.ToString(lngWarrantNumber) + ")";
                                    clsExecute.Execute(strSQL, "twpy0000.vb1");
                                }
                            }
                        }

                        strSQL = $"update tblCheckDetail set checknumber = {lngCurrentCheckNumber} where employeenumber = '{loadedEmpMasterEmployeeNumber}' and paydate = '{FCConvert.ToString(dtPayDate)}' and PayRunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and masterrecord = '{loadedCheckDetailMasterRecord}' and not bankrecord = 1 and trustrecord <> 1";
                        clsExecute.Execute(strSQL, "twpy0000.vb1");
                    }

                    if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais" || boolMailer)
                    {
                        SubReport1.Height = c3And5Eighths;

                        // 3 and 5 eighths
                        SubReport2.Height = c3And5Eighths;
                        SubReport2.Top = SubReport1.Top + SubReport1.Height + 1;
                        SubReport3.Top = SubReport2.Top + SubReport2.Height + 1;
                        SubReport3.Height = c3And5Eighths;
                        SubReport1.CanShrink = false;
                        SubReport2.CanShrink = false;
                    }
                    
                   
                    if (boolMailer)
                    {
                        SubReport1.Report = new srptLaserMailerPayStub();
                        SubReport2.Report = new srptLaserCheckBody1();
                        SubReport3.Report = new srptAddressStub();
                    }
                    else
                    {
                        if (intCheckBodyPosition == 0)
                        {
                            if (boolDoubleStub)
                            {
                                SubReport2.CanGrow = true;
                                SubReport3.CanShrink = true;
                                SubReport2.Height = cFullHeight;

                                // two * regular stub height
                                SubReport2.Report = boolLaserDblStub ? (SectionReport) new srptNewDBLLaserStub2() : new srptDBLLaserStub1();

                                SubReport3.Height = 0;
                            }
                            else
                            {
                                SubReport2.Report = new srptLaserStub1();
                            }

                            SubReport1.Report = new srptLaserCheckBody1();
                            SubReport1.Report.UserData = strPrintFont;
                            SubReport2.Report.UserData = strPrintFont;

                            if (intNumStubs > 1)
                            {
                                SubReport3.Report = new srptLaserStub1();

                                // Set SubReport3 = New srptAddressStub
                                SubReport3.Report.UserData = strPrintFont;
                            }
                        }
                        else
                        {
                            if ((intCheckBodyPosition == 1) || (intNumStubs < 2 && intCheckType == 0))
                            {
                                if (boolDoubleStub)
                                {
                                    SubReport1.CanGrow = true;
                                    SubReport2.CanShrink = true;
                                    SubReport1.Height = cFullHeight;

                                    SubReport2.Report = boolLaserDblStub ? (SectionReport) new srptNewDBLLaserStub2() : new srptDBLLaserStub1();

                                    SubReport1.Report = new srptDBLLaserStub1();
                                    SubReport2.Height = 0;
                                    SubReport1.Report.UserData = strPrintFont;
                                    SubReport3.Report = new srptLaserCheckBody1();
                                    SubReport3.Report.UserData = strPrintFont;
                                }
                                else
                                {
                                    SubReport1.Report = new srptLaserStub1();
                                    SubReport1.Report.UserData = strPrintFont;
                                    SubReport2.Report = new srptLaserCheckBody1();
                                    SubReport2.Report.UserData = strPrintFont;
                                }

                                if (intNumStubs > 1)
                                {
                                    SubReport3.Report = new srptLaserStub1();

                                    // Set SubReport3 = New srptAddressStub
                                    SubReport3.Report.UserData = strPrintFont;
                                }
                            }
                            else
                            {
                                if (boolDoubleStub)
                                {
                                    SubReport1.CanGrow = true;
                                    SubReport2.CanShrink = true;
                                    SubReport1.Height = cFullHeight;

                                    // Set SubReport1 = New srptDBLLaserStub1
                                    SubReport1.Report = boolLaserDblStub ? (SectionReport) new srptNewDBLLaserStub2() : new srptDBLLaserStub1();

                                    SubReport1.Report.UserData = strPrintFont;
                                    SubReport2.Height = 0;
                                    SubReport3.Report = new srptLaserCheckBody1();
                                    SubReport3.Report.UserData = strPrintFont;
                                }
                                else
                                {
                                    if (intNumStubs > 1)
                                    {
                                        SubReport1.Report = new srptLaserStub1();
                                        SubReport1.Report.UserData = strPrintFont;
                                    }

                                    SubReport2.Report = new srptLaserStub1();
                                    SubReport3.Report = new srptLaserCheckBody1();
                                    SubReport2.Report.UserData = strPrintFont;
                                    SubReport3.Report.UserData = strPrintFont;
                                }
                            }
                        }
                    }

                    clsLoad.MoveNext();
                    if (clsLoad.EndOfFile()) intChecksToPrint = 1;

                    // signal that we are printing ddbank checks now
                }
                else
                {
                    if (!clsDDBanks.EndOfFile())
                    {
                        double totDDAmount = Conversion.Val(clsDDBanks.Get_Fields("totddamount"));

                        GridMisc.TextMatrix(0, 2, "BANK " + clsDDBanks.Get_Fields_Int32("recordnumber"));
                        GridMisc.TextMatrix(0, 9, Strings.Format(totDDAmount, numericFormat));
                        dblDirectDeposit = totDDAmount;

                        if (boolACH)
                        {
                            GridMisc.TextMatrix(0, 10, "** ACH *** ACH *** ACH *** ACH *** ACH *** ACH *** ACH *** ACH **");
                            strTag = "INVALID CHECK";
                            boolNonNegotiable = true;
                        }
                        else
                        {
                            if (clsDDBanks.Get_Fields_Boolean("EFT"))
                            {
                                GridMisc.TextMatrix(0, 10, "** EFT *** EFT *** EFT *** EFT *** EFT *** EFT *** EFT *** EFT **");
                                strTag = "INVALID CHECK";
                                boolNonNegotiable = true;
                            }
                            else
                            {
                                GridMisc.TextMatrix(0, 10, modConvertAmountToString.ConvertAmountToString(FCConvert.ToDecimal(dblDirectDeposit)) + " ");
                                GridMisc.TextMatrix(0, 10, GridMisc.TextMatrix(0, 10) + Strings.StrDup(65 - GridMisc.TextMatrix(0, 10).Length, "*"));
                            }
                        }

                        if (strTag == string.Empty || !boolPrintValidChecksSeparate)
                        {
                            lngCurrentCheckNumber = lngValidCheckNumber;
                            lngHighestPrintedValidCheck = lngValidCheckNumber;
                            lngValidCheckNumber += 1;
                        }
                        else
                        {
                            lngCurrentCheckNumber = lngInvalidCheckNumber;
                            lngHighestPrintedInvalidCheck = lngInvalidCheckNumber;
                            lngInvalidCheckNumber += 1;
                        }

                        GridMisc.TextMatrix(0, 11, Strings.Format(dblDirectDeposit, numericFormat));
                        GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
                        GridMisc.TextMatrix(0, 12, "CHECK " + FCConvert.ToString(lngCurrentCheckNumber));
                        GridMisc.TextMatrix(0, 18, clsDDBanks.Get_Fields_String("Name"));
                        GridMisc.TextMatrix(0, 19, clsDDBanks.Get_Fields("address1"));
                        GridMisc.TextMatrix(0, 20, clsDDBanks.Get_Fields("address2"));

                        // 12/07/2011
                        GridMisc.TextMatrix(0, 21, clsDDBanks.Get_Fields_String("address3").Trim());

                        if (GridMisc.TextMatrix(0, 20).Trim() == "")
                        {
                            GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                            GridMisc.TextMatrix(0, 21, "");
                        }

                        if (GridMisc.TextMatrix(0, 19).Trim() == "")
                        {
                            GridMisc.TextMatrix(0, 19, GridMisc.TextMatrix(0, 20));
                            GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                            GridMisc.TextMatrix(0, 21, "");
                        }

                        GridMisc.TextMatrix(0, 22, clsDDBanks.Get_Fields_Int32("recordnumber"));
                        GridMisc.TextMatrix(0, 27, "D");

                        // directdeposit
                        if (boolReprinting)
                        {
                            int checkNumber = clsDDBanks.Get_Fields_Int32("checknumber");

                            if (checkNumber != lngCurrentCheckNumber)
                            {
                                if (checkNumber < plngStartingCheckNumber)
                                {
                                    strTemp = Strings.Replace(clsDDBanks.Get_Fields_Int32("checknumber").ToString(), "'", "", 1, -1, CompareConstants.vbTextCompare);
                                    strSQL = "Insert into tblCheckHistory ";
                                    strSQL += "(CheckNumber,ReplacedByCheckNumber,EmployeeNumber,EmployeeName,Amount,CheckDate,CheckRun,CheckType,Status,ReportNumber,WarrantNumber) values ";
                                    strSQL += $"({FCConvert.ToString(checkNumber)},{FCConvert.ToString(lngCurrentCheckNumber)},'{clsDDBanks.Get_Fields("ddbanknumber")}'";
                                    strSQL += $",'{strTemp}',{FCConvert.ToString(dblDirectDeposit)},'{FCConvert.ToString(dtPayDate)}',{FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)},'D','V',0,{FCConvert.ToString(lngWarrantNumber)})";
                                    clsExecute.Execute(strSQL, "twpy0000.vb1");
                                }
                            }
                        }

                        strSQL = boolACH 
                            ? $"update tblCheckDetail set checknumber = {lngCurrentCheckNumber} where paydate = '{FCConvert.ToString(dtPayDate)}' and PayRunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)} and BankRecord = 1" 
                            : $"update tblCheckDetail set checknumber = {lngCurrentCheckNumber} where convert(int, isnull(DDBankNumber, 0)) = '{clsDDBanks.Get_Fields("ddbanknumber")}' and paydate = '{FCConvert.ToString(dtPayDate)}' and PayRunID = {FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun)}";

                        clsExecute.Execute(strSQL, "twpy0000.vb1");

                        if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais" || boolMailer)
                        {
                            SubReport1.Height = c3And5Eighths;

                            // 3 and 5 eighths
                            SubReport2.Height = c3And5Eighths;
                            SubReport2.Top = SubReport1.Top + SubReport1.Height + 1;
                            SubReport3.Top = SubReport2.Top + SubReport2.Height + 1;
                            SubReport3.Height = c3And5Eighths;
                            SubReport1.CanShrink = false;
                            SubReport2.CanShrink = false;
                        }

                        if (boolMailer)
                        {
                            SubReport1.Report = new srptDDStub1();
                            SubReport2.Report = new srptLaserCheckBody1();
                            SubReport3.Report = new srptAddressStub();
                        }
                        else
                            if (intCheckBodyPosition == 0)
                            {
                                SubReport1.Report = new srptLaserCheckBody1();
                                SubReport2.Report = new srptDDStub1();
                                SubReport1.Report.UserData = strPrintFont;
                                SubReport2.Report.UserData = strPrintFont;

                                if (boolDoubleStub)
                                {
                                    SubReport2.CanGrow = true;
                                    SubReport2.Height = cFullHeight;
                                    SubReport3.CanShrink = true;
                                    SubReport3.Height = 0;
                                }

                                if (intNumStubs > 1)
                                {
                                    SubReport3.Report = new srptDDStub1();
                                    SubReport3.Report.UserData = strPrintFont;
                                }
                            }
                            else
                                if ((intCheckBodyPosition == 1) || (intNumStubs < 2 && intCheckType == 0))
                                {
                                    //Application.DoEvents();
                                    SubReport1.Report = new srptDDStub1();

                                    if (boolDoubleStub)
                                    {
                                        SubReport1.CanGrow = true;
                                        SubReport1.Height = cFullHeight;
                                        SubReport2.CanShrink = true;
                                        SubReport2.Height = 0;
                                        SubReport3.Report = new srptLaserCheckBody1();
                                        SubReport3.Report.UserData = strPrintFont;
                                    }
                                    else
                                    {
                                        SubReport2.Report = new srptLaserCheckBody1();
                                        SubReport2.Report.UserData = strPrintFont;
                                    }

                                    SubReport1.Report.UserData = strPrintFont;

                                    if (intNumStubs > 1)
                                    {
                                        SubReport3.Report = new srptDDStub1();
                                        SubReport3.Report.UserData = strPrintFont;
                                    }
                                }
                                else
                                {
                                    //Application.DoEvents();
                                    if (intNumStubs > 1)
                                    {
                                        SubReport1.Report = new srptDDStub1();
                                        SubReport1.Report.UserData = strPrintFont;
                                    }

                                    if (boolDoubleStub)
                                    {
                                        SubReport1.CanGrow = true;
                                        SubReport2.CanShrink = true;
                                        SubReport2.Height = 0;
                                        SubReport1.Height = cFullHeight;
                                        SubReport1.Report = new srptDDStub1();
                                        SubReport1.Report.UserData = strPrintFont;
                                    }
                                    else
                                    {
                                        SubReport2.Report = new srptDDStub1();
                                        SubReport2.Report.UserData = strPrintFont;
                                    }

                                    SubReport3.Report = new srptLaserCheckBody1();
                                    SubReport3.Report.UserData = strPrintFont;
                                }

                        clsDDBanks.MoveNext();
                        if (clsDDBanks.EndOfFile()) intChecksToPrint = 2;
                    }
                    else
                    {
                        if (!clsTandAChecks.EndOfFile())
                        {
                            dblTrustAmount = Conversion.Val(clsTandAChecks.Get_Fields("tottrustamount"));

                            GridMisc.TextMatrix(0, 2, "Recipient " + clsTandAChecks.Get_Fields_Int32("recptnumber"));
                            GridMisc.TextMatrix(0, 9, Strings.Format(dblTrustAmount, numericFormat));

                            if (clsTandAChecks.Get_Fields_Boolean("EFT") == true)
                            {
                                GridMisc.TextMatrix(0, 10, "** EFT *** EFT *** EFT *** EFT *** EFT *** EFT *** EFT *** EFT **");
                                strTag = "INVALID CHECK";
                                boolNonNegotiable = true;
                                GridMisc.TextMatrix(0, 11, Strings.Format(dblTrustAmount, numericFormat));
                                GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
                            }
                            else
                            {
                                GridMisc.TextMatrix(0, 10, modConvertAmountToString.ConvertAmountToString(dblTrustAmount.ToDecimal()) + " ");
                                GridMisc.TextMatrix(0, 10, GridMisc.TextMatrix(0, 10) + Strings.StrDup(65 - GridMisc.TextMatrix(0, 10).Length, "*"));
                                GridMisc.TextMatrix(0, 11, Strings.Format(dblTrustAmount, numericFormat));
                                GridMisc.TextMatrix(0, 11, "$" + Strings.StrDup(13 - GridMisc.TextMatrix(0, 11).Length, "*") + GridMisc.TextMatrix(0, 11));
                            }

                            if (strTag == string.Empty || !boolPrintValidChecksSeparate)
                            {
                                lngCurrentCheckNumber = lngValidCheckNumber;
                                lngHighestPrintedValidCheck = lngValidCheckNumber;
                                lngValidCheckNumber += 1;
                            }
                            else
                            {
                                lngCurrentCheckNumber = lngInvalidCheckNumber;
                                lngHighestPrintedInvalidCheck = lngInvalidCheckNumber;
                                lngInvalidCheckNumber += 1;
                            }

                            GridMisc.TextMatrix(0, 12, "Check " + FCConvert.ToString(lngCurrentCheckNumber));
                            GridMisc.TextMatrix(0, 18, clsTandAChecks.Get_Fields_String("Name"));
                            GridMisc.TextMatrix(0, 19, clsTandAChecks.Get_Fields_String("Address1"));
                            GridMisc.TextMatrix(0, 20, clsTandAChecks.Get_Fields("address2"));
                            GridMisc.TextMatrix(0, 21, clsTandAChecks.Get_Fields_String("address3"));

                            if (GridMisc.TextMatrix(0, 20).Trim() == "")
                            {
                                GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                                GridMisc.TextMatrix(0, 21, "");
                            }

                            if (GridMisc.TextMatrix(0, 19).Trim() == "")
                            {
                                GridMisc.TextMatrix(0, 19, GridMisc.TextMatrix(0, 20));
                                GridMisc.TextMatrix(0, 20, GridMisc.TextMatrix(0, 21));
                                GridMisc.TextMatrix(0, 21, "");
                            }

                            GridMisc.TextMatrix(0, 22, clsTandAChecks.Get_Fields_Int32("recptnumber"));
                            GridMisc.TextMatrix(0, 27, "T");

                            // Trust and Agency
                            GridMisc.TextMatrix(0, 6, clsTandAChecks.Get_Fields_Double("fedtrust"));
                            GridMisc.TextMatrix(0, 7, clsTandAChecks.Get_Fields_Double("ficatrust"));
                            GridMisc.TextMatrix(0, 8, clsTandAChecks.Get_Fields_Double("statetrust"));
                            GridMisc.TextMatrix(0, 32, clsTandAChecks.Get_Fields_Double("MedTrust"));

                            if (boolReprinting)
                            {
                                int pqCheckNumber = clsTandAChecks.Get_Fields_Int32("parentquery1.checknumber");

                                if (pqCheckNumber != lngCurrentCheckNumber)
                                {
                                    if (pqCheckNumber < plngStartingCheckNumber)
                                    {
                                        strTemp = Strings.Replace(FCConvert.ToString(clsTandAChecks.Get_Fields_String("Name")), "'", "", 1, -1, CompareConstants.vbTextCompare);
                                        strSQL = "Insert into tblCheckHistory ";
                                        strSQL += "(CheckNumber,ReplacedByCheckNumber,EmployeeNumber,EmployeeName,Amount,CheckDate,CheckRun,CheckType,Status,ReportNumber,WarrantNumber) values ";
                                        strSQL += "(" + FCConvert.ToString(pqCheckNumber) + "," + FCConvert.ToString(lngCurrentCheckNumber) + ",'" + clsTandAChecks.Get_Fields_Int32("parentquery1trustrecipientid") + "'";
                                        strSQL += ",'" + strTemp + "'," + FCConvert.ToString(dblTrustAmount) + ",'" + FCConvert.ToString(dtPayDate) + "'," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + ",'T','V',0," + FCConvert.ToString(lngWarrantNumber) + ")";
                                        clsExecute.Execute(strSQL, "twpy0000.vb1");
                                    }
                                }
                            }

                            strSQL = "update tblCheckDetail set checknumber = " + FCConvert.ToString(lngCurrentCheckNumber) + " where convert(int, isnull(trustrecipientid, 0)) = '" + clsTandAChecks.Get_Fields("parentquery1trustrecipientid") + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
                            clsExecute.Execute(strSQL, "twpy0000.vb1");

                            if (fecherFoundation.Strings.LCase(modGlobalVariables.Statics.gstrMuniName) == "calais" || boolMailer)
                            {
                                SubReport1.Height = c3And5Eighths;

                                // 3 and 5 eighths
                                SubReport2.Height = c3And5Eighths;
                                SubReport2.Top = SubReport1.Top + SubReport1.Height + 1;
                                SubReport3.Top = SubReport2.Top + SubReport2.Height + 1;
                                SubReport3.Height = c3And5Eighths;
                                SubReport1.CanShrink = false;
                                SubReport2.CanShrink = false;
                            }

                            if (boolMailer)
                            {
                                SubReport1.Report = new srptDDStub1();
                                SubReport2.Report = new srptLaserCheckBody1();
                                SubReport3.Report = new srptAddressStub();
                            }
                            else
                            {
                                if (intCheckBodyPosition == 0)
                                {
                                    SubReport1.Report = new srptLaserCheckBody1();
                                    SubReport2.Report = new srptDDStub1();
                                    SubReport1.Report.UserData = strPrintFont;
                                    SubReport2.Report.UserData = strPrintFont;

                                    if (boolDoubleStub)
                                    {
                                        SubReport2.CanGrow = true;
                                        SubReport3.CanShrink = true;
                                        SubReport2.Height = cFullHeight;
                                        SubReport3.Height = 0;
                                    }

                                    if (intNumStubs > 1)
                                    {
                                        SubReport3.Report = new srptDDStub1();
                                        SubReport3.Report.UserData = strPrintFont;
                                    }
                                }
                                else
                                    if ((intCheckBodyPosition == 1) || (intNumStubs < 2 && intCheckType == 0))
                                    {
                                        SubReport1.Report = new srptDDStub1();
                                        SubReport1.Report.UserData = strPrintFont;

                                        if (boolDoubleStub)
                                        {
                                            SubReport1.CanGrow = true;
                                            SubReport2.CanShrink = true;
                                            SubReport1.Height = cFullHeight;
                                            SubReport2.Height = 0;
                                            SubReport3.Report = new srptLaserCheckBody1();
                                            SubReport3.Report.UserData = strPrintFont;
                                        }
                                        else
                                        {
                                            SubReport2.Report = new srptLaserCheckBody1();
                                            SubReport2.Report.UserData = strPrintFont;
                                        }

                                        if (intNumStubs > 1)
                                        {
                                            SubReport3.Report = new srptDDStub1();
                                            SubReport3.Report.UserData = strPrintFont;
                                        }
                                    }
                                    else
                                    {
                                        if (intNumStubs > 1)
                                        {
                                            SubReport1.Report = new srptDDStub1();
                                            SubReport1.Report.UserData = strPrintFont;
                                        }

                                        if (boolDoubleStub)
                                        {
                                            SubReport1.CanGrow = true;
                                            SubReport2.CanShrink = true;
                                            SubReport1.Height = cFullHeight;
                                            SubReport2.Height = 0;
                                            SubReport1.Report = new srptDDStub1();
                                            SubReport1.Report.UserData = strPrintFont;
                                        }
                                        else
                                        {
                                            SubReport2.Report = new srptDDStub1();
                                            SubReport2.Report.UserData = strPrintFont;
                                        }

                                        SubReport3.Report = new srptLaserCheckBody1();
                                        SubReport3.Report.UserData = strPrintFont;
                                    }
                            }

                            //Application.DoEvents();
                            clsTandAChecks.MoveNext();
                        }
                    }
                }

                if (Information.UBound(boolAryInvalidCheck, 1) < lngCheckPage)
                {
                    Array.Resize(ref boolAryInvalidCheck, lngCheckPage + 1);
                }

                if (strTag != string.Empty)
                {
                    boolAryInvalidCheck[lngCheckPage] = true;
                    boolNonNegotiable = true;
                }

                lngCheckPage += 1;

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Check DetailFormat");
            }
            finally
            {
                rsBenefits.DisposeOf();
                rsYTDBenefits.DisposeOf();
            }
        }

        private int GetPayRow()
        {
            int intRow = 0;
            int distPayCategory = clsPay.Get_Fields_Int32("distPayCategory");

            for (var x = 1; x <= intNumPays; x++)
            {
                if (arPayCodes[x] == distPayCategory)
                {
                    intRow = x;

                    break;
                }
            }

            if (intRow == 0) intRow = intMaxStubLines;

            return intRow;
        }

        private static void UpdateProgressBar()
        {
            if (frmWait.InstancePtr.prgProgress.Value >= 100)
            {
                frmWait.InstancePtr.prgProgress.Value = 0;
            }
            else
            {
                frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Value + 1;
            }

            frmWait.InstancePtr.prgProgress.Refresh();
        }

        public void Init(bool boolReprint, int lngStartingCheckNumber, int lngWarrant, bool boolPrintSignature, int lngStartingReprintNumber = 0, int lngEndingReprintNumber = 0, string strPrinterName = "", int lngBankNumber = 0, int lngSigID = 0, int lngNonNegStart = 0)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSQL = "";
                string strPayTemp = "";
                string strDedTemp = "";
                string strFonttoUse = "";
                int intReturn = 0;
                string strOrderBy = "";
                string strWhere = "";
                showRemoveChecks = true;
                // vbPorter upgrade warning: lngAlignment As int	OnWriteFCConvert.ToSingle(
                decimal lngAlignment = 0;
                string strVac1 = "";
                string strVac2 = "";
                string strVac3 = "";
                string strDed1;
                string strDed2;
                string strPayQuery1;
                string strPayQuery1a;
                string strPayQuery1b = "";
                string strPayQuery2;
                string strPayQuery3;
                string strPayQuery4;
                string strPayQuery5;
                string strPayQuery6;
                string strDDQuery1 = "";
                string strTandAQuery;
                string strTandAFed;
                string strTandAFica;
                string strTandAMed;
                string strTandAState;
                string strTandALocal = "";
                int lngACHBank = 0;
                bool boolTwoStubs;
                // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
                DateTime dtStart;
                DateTime dtEnd;
                arDedCodes = new int[150 + 1];
                arDedDescriptions = new string[150 + 1];
                lngCheckBankID = lngBankNumber;

                strESPFolder = Environment.CurrentDirectory + "\\eshpexport\\";
                strESPChecks = strESPFolder + "Checks\\";
                lngOrigEndCheckNo = lngEndingReprintNumber;
                lngOrigStartCheckNo = lngStartingReprintNumber;
                boolPrintValidChecksSeparate = false;
                boolIgnorePrinterFonts = true;
                intMaxStubLines = 9;
                intMaxMatchLines = 12;
                boolPrintSig = boolPrintSignature;
                lngSignatureID = lngSigID;
                // Call clsBanks.OpenRecordset("select * from tblbanks order by ID", "twpy0000.vb1")
                clsStates.OpenRecordset("Select ID,state from states order by id", "twpy0000.vb1");
                clsLoad.OpenRecordset("select printsequence,ignoreprinterfonts,PrintInvalidSeparate,ShowPayPeriodOnStub from tbldefaultINFORMATION", "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    boolIgnorePrinterFonts = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ignoreprinterfonts"));
                    boolPrintValidChecksSeparate = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("printinvalidseparate"));
                    boolPrintPayPeriod = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("showpayperiodonstub"));
                    switch (clsLoad.Get_Fields_Int32("printsequence"))
                    {
                        case 0:
                            {
                                // name order
                                strOrderBy = " LastName,FirstName,tblcheckdetail.masterrecord ";
                                break;
                            }
                        case 1:
                            {
                                // employee number
                                strOrderBy = " tblcheckdetail.employeenumber,lastname,firstname,tblcheckdetail.masterrecord";
                                break;
                            }
                        case 2:
                            {
                                // sequence number
                                strOrderBy = " seqnumber,lastname,firstname,tblcheckdetail.masterrecord ";
                                break;
                            }
                        case 3:
                            {
                                // dept division
                                strOrderBy = " deptdiv,lastname,firstname,tblcheckdetail.masterrecord ";
                                break;
                            }
                    }
                    //end switch
                }
                // checktype 0 = paychecks, 1 = directdeposit to bank,2 = T&A checks
                lngWarrantNumber = lngWarrant;

                modGlobalVariables.Statics.boolCancelButtonSelected = false;

                strPrintFont = "";
                strPFont = "";

                cPayRunController prc = new cPayRunController();
                pRun = prc.GetPayRun(modGlobalVariables.Statics.gdatCurrentPayDate, modGlobalVariables.Statics.gintCurrentPayRun);
                clsLoad.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                strReturnAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress1")));
                if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2"))) != "")
                {
                    if (strReturnAddress != "")
                        strReturnAddress += "\r\n";
                    strReturnAddress += fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress2")));
                }
                if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3"))) != "")
                {
                    if (strReturnAddress != "")
                        strReturnAddress += "\r\n";
                    strReturnAddress += fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress3")));
                }
                if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4"))) != "")
                {
                    if (strReturnAddress != "")
                        strReturnAddress += "\r\n";
                    strReturnAddress += fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ReturnAddress4")));
                }
                if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 0)
                {
                    strLogoPath = "";
                }
                else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 1)
                {
                    // town seal
                    if (Strings.Left(Environment.CurrentDirectory, 1) != "\\")
                    {
                        strLogoPath = Environment.CurrentDirectory + "\\TownSeal.pic";
                    }
                    else
                    {
                        strLogoPath = Environment.CurrentDirectory + "TownSeal.pic";
                    }
                }
                else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int16("UseCheckImage")) == 2)
                {
                    // custom image
                    strLogoPath = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("checklogo")));
                }
                // 0 is standard 1 is laser
                if (!boolPrintInvalidChecksOnly)
                {
                    if (!clsLoad.EndOfFile())
                    {
                        intCheckType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("checktype"))));
                    }
                    else
                    {
                        intCheckType = 0;
                    }
                }
                else
                {
                    intCheckType = 1;
                    // laser
                }
                if (intCheckType == 0)
                {
                    intCheckBodyPosition = 1;
                }
                else if (intCheckType == 1)
                {
                    intCheckBodyPosition = 1;
                }
                boolCustomCheck = false;
                boolTwoStubs = false;
                if (intCheckType == 2)
                {
                    // custom
                    boolCustomCheck = true;
                    // choose the custom check format
                    // frmSelectCustomCheckFormat.Init
                    if (!boolPrintInvalidChecksOnly && !boolPrintValidChecksOnly)
                    {
                        intNonNegotiableFormat = FCConvert.ToInt32(frmSelectCustomCheckFormat.InstancePtr.Init(true));
                    }
                    else
                    {
                        intNonNegotiableFormat = FCConvert.ToInt32(frmSelectCustomCheckFormat.InstancePtr.Init(false));
                    }
                    clsLoad.OpenRecordset("select * from customchecks where id = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), "twpy0000.vb1");
                    if (clsLoad.EndOfFile())
                    {
                        frmWait.InstancePtr.Unload();
                        MessageBox.Show("Default parameters for custom check were not found. Set these parameters in the customize screen.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        modGlobalVariables.Statics.boolCancelButtonSelected = true;
                        return;
                    }
                    else
                    {
                        if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("checktype"))) == "D")
                        {
                            intCheckType = 0;
                        }
                        else if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields("checktype"))) == "M")
                        {
                            intCheckType = 1;
                            boolMailer = true;
                        }
                        else
                        {
                            intCheckType = 1;
                        }
                        if (FCConvert.ToString(clsLoad.Get_Fields("checkposition")) == "T")
                        {
                            intCheckBodyPosition = 0;
                        }
                        else if (clsLoad.Get_Fields("Checkposition") == "M")
                        {
                            intCheckBodyPosition = 1;
                        }
                        else
                        {
                            intCheckBodyPosition = 2;
                        }
                        boolDoubleStub = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("UseDoubleStub"));
                        boolTwoStubs = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("UseTwoStubs"));
                    }
                }
                boolShowIndividualPayRates = false;
                if (boolDoubleStub)
                {
                    intMaxStubLines = 23;
                    intMaxStubDDLines = 9;
                    boolLaserDblStub = true;
                }
                else
                {
                    intMaxStubLines = 9;
                    intMaxStubDDLines = 9;
                    boolLaserDblStub = false;
                }
                if (intCheckType == 0)
                {
                    intNumStubs = 1;
                    if (boolTwoStubs)
                    {
                        intNumStubs = 2;
                    }
                    this.PageSettings.Margins.Bottom = 0;
                    frmWait.InstancePtr.Unload();

                }
                else
                {
                    if (!boolDoubleStub)
                    {
                        intNumStubs = 2;
                    }
                    else
                    {
                        intNumStubs = 1;
                        if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowIndividualPayrates")))
                        {
                            boolShowIndividualPayRates = true;
                        }
                    }
                }
                //Application.DoEvents();
                frmWait.InstancePtr.Init("Loading Check Information");
                clsFormat.OpenRecordset("select * from tblCheckFormat", "twpy0000.vb1");
                if (!clsFormat.EndOfFile())
                {
                    sngLaserAlignment = FCConvert.ToSingle(Conversion.Val(clsFormat.Get_Fields_Double("LaserAlignment")));
                    // 
                    // If intCheckType = 0 And intNumStubs <= 2 And intCheckBodyPosition = 2 Then intCheckBodyPosition = 1
                }
                else
                {
                    sngLaserAlignment = 0;
                    intCheckBodyPosition = 1;
                }
                if (fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("CheckAlignment", "PY"))) != string.Empty)
                {
                    sngLaserAlignment = FCConvert.ToSingle(Conversion.Val(modRegistry.GetRegistryKey("CheckAlignment", "PY")));
                }
                if (intCheckType == 1 && sngLaserAlignment != 0)
                {
                    // don't allow more than a half inch adjustment if positive
                    if (sngLaserAlignment < 0)
                    {
                        if (this.PageSettings.Margins.Top + FCConvert.ToInt32(sngLaserAlignment * 240 / 1440F) < 0)
                        {
                            this.PageSettings.Margins.Top = 0;
                        }
                        else
                        {
                            this.PageSettings.Margins.Top += FCConvert.ToInt32(sngLaserAlignment * 240 / 1440F);
                        }
                    }
                    else
                    {
                        if (sngLaserAlignment > 3)
                            sngLaserAlignment = 3;
                        lngAlignment = Convert.ToDecimal(sngLaserAlignment * 240 / 1440F);
                        SubReport1.Top += Convert.ToSingle(lngAlignment);
                        SubReport2.Top += Convert.ToSingle(lngAlignment);
                        SubReport3.Top += Convert.ToSingle(lngAlignment);
                    }
                }
                Grid.Rows = 100;
                clsLoad.OpenRecordset("select max (deductionnumber) as maxnum from tbldeductionsetup", "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    if (clsLoad.Get_Fields_Int32("maxnum") > Grid.Rows)
                    {
                        Grid.Rows = clsLoad.Get_Fields_Int32("maxnum");
                    }
                }
                GridMisc.Rows = 1;
                GridMisc.Cols = CNSTMAXGRIDCOLS;
                GridMisc.TextMatrix(0, 30, intCheckType);
                boolReprinting = boolReprint;
                lngCurrentCheckNumber = lngStartingCheckNumber;
                plngStartingCheckNumber = lngStartingCheckNumber;
                lngValidCheckNumber = lngCurrentCheckNumber;
                intChecksToPrint = 0;
                dtPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
                GridMisc.TextMatrix(0, 24, dtPayDate);
                GridMisc.TextMatrix(0, 28, intChecksToPrint);
                GridMisc.TextMatrix(0, CNSTCheckColBank, lngCheckBankID);
                GridMisc.TextMatrix(0, CNSTCheckColBankName, "");
                GridMisc.TextMatrix(0, CNSTCheckColBankAccount, "");
                GridMisc.TextMatrix(0, CNSTCheckColBankRouting, "");
                if (!modGlobalConstants.Statics.gboolBD)
                {
                    clsBanks.OpenRecordset("select * from tblbanks order by id", "twpy0000.vb1");
                    if (lngCheckBankID > 0)
                    {
                        if (clsBanks.FindFirstRecord("id", lngCheckBankID))
                        {
                            GridMisc.TextMatrix(0, CNSTCheckColBankName, clsBanks.Get_Fields("name"));
                            GridMisc.TextMatrix(0, CNSTCheckColBankAccount, clsBanks.Get_Fields_String("CheckingAccount"));
                            GridMisc.TextMatrix(0, CNSTCheckColBankRouting, clsBanks.Get_Fields_String("BankDepositID"));
                        }
                    }
                }
                else if (lngBankNumber > 0)
                {
                    clsBanks.OpenRecordset("select * from banks where banknumber = " + FCConvert.ToString(lngBankNumber), "twbd0000.vb1");
                    if (!clsBanks.EndOfFile())
                    {
                        GridMisc.TextMatrix(0, CNSTCheckColBankName, clsBanks.Get_Fields("name"));
                        GridMisc.TextMatrix(0, CNSTCheckColBankAccount, clsBanks.Get_Fields_String("BankAccountNumber"));
                        GridMisc.TextMatrix(0, CNSTCheckColBankRouting, clsBanks.Get_Fields_String("RoutingNumber"));
                    }
                    clsBanks.OpenRecordset("select * from tblbanks order by id", "twpy0000.vb1");
                }
                // get descriptions for pay categories
                clsLoad.OpenRecordset("select * from tblpaycategories", "twpy0000.vb1");
                intNumPays = clsLoad.RecordCount();
                while (!clsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    arPayDescriptions[clsLoad.Get_Fields("categorynumber")] = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                    arPayCodes[clsLoad.Get_Fields("categorynumber")] = FCConvert.ToInt32(clsLoad.Get_Fields("ID"));
                    arPayTypes[clsLoad.Get_Fields("categorynumber")] = FCConvert.ToString(clsLoad.Get_Fields("Type"));
                    Grid.TextMatrix(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("categorynumber")) - 1), 0, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
                    clsLoad.MoveNext();
                }
                // get descriptions for deductions
                clsLoad.OpenRecordset("select * from tbldeductionsetup", "twpy0000.vb1");
                intNumDeds = clsLoad.RecordCount();
                while (!clsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    if (Conversion.Val(clsLoad.Get_Fields("deductionnumber")) > Information.UBound(arDedCodes, 1))
                    {
                        Array.Resize(ref arDedCodes, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("Deductionnumber"))) + 1);
                    }
                    if (Conversion.Val(clsLoad.Get_Fields("deductionnumber")) > Information.UBound(arDedDescriptions, 1))
                    {
                        Array.Resize(ref arDedDescriptions, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("deductionnumber"))) + 1);
                    }
                    arDedCodes[clsLoad.Get_Fields("deductionnumber")] = FCConvert.ToInt32(clsLoad.Get_Fields("ID"));
                    arDedDescriptions[clsLoad.Get_Fields("deductionnumber")] = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                    Grid.TextMatrix(clsLoad.Get_Fields_Int32("DeductionNumber") - 1, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
                    clsLoad.MoveNext();
                }
                // open the recordsets
                if (boolReprint)
                {
                    strWhere = " checknumber between " + FCConvert.ToString(lngStartingReprintNumber) + " and " + FCConvert.ToString(lngEndingReprintNumber) + " ";
                }
                else
                {
                    strWhere = " checknumber = 0 ";
                }
                GridMisc.TextMatrix(0, 45, strWhere);
                if (boolReprint)
                {
                    // strSQL = "select employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail where paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & " and convert(int, isnull(ddamount, 0)) > 0  order by employeenumber,masterrecord"
                    strSQL = "select tblcheckdetail.employeenumber as employeenumber,masterrecord,ddbanknumber, isnull(ddamount, 0) as DDepositAmount from tblcheckdetail inner join tbldirectdeposit on ( tblcheckdetail.ddbanknumber = tbldirectdeposit.bank) and  (tblcheckdetail.ddaccountnumber = tbldirectdeposit.account) and (tblcheckdetail.employeenumber = tbldirectdeposit.employeenumber) where checkvoid = 0 and  paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + "  and isnull(ddamount, 0) > 0 order by tblcheckdetail.employeenumber,masterrecord,rownumber";
                }
                else
                {
                    // strSQL = "select employeenumber,masterrecord,ddbanknumber,convert(int, isnull(ddamount, 0)) as DDepositAmount from tblcheckdetail where paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & " and " & strWhere & " and convert(int, isnull(ddamount, 0)) > 0 order by employeenumber,masterrecord"
                    strSQL = "select tblcheckdetail.employeenumber as employeenumber,masterrecord,ddbanknumber, isnull(ddamount, 0) as DDepositAmount from tblcheckdetail inner join tbldirectdeposit on ( tblcheckdetail.ddbanknumber = tbldirectdeposit.bank) and (tblcheckdetail.ddaccountnumber = tbldirectdeposit.account) and  (tblcheckdetail.employeenumber = tbldirectdeposit.employeenumber) where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and  isnull(ddamount, 0) > 0 order by tblcheckdetail.employeenumber,masterrecord,rownumber";
                }
                clsDDDeductions.OpenRecordset(strSQL, "twpy0000.vb1");
                // get only total records to join with
                if (boolReprint)
                {
                    // strSQL = "select employeenumber,masterrecord,sum( isnull(ddamount, 0)) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & "  and (left(DDACCOUNTTYPE + '  ',2) = '22' or left(rtrim(isnull(ddaccounttype, ''))  + '  ',2) = '  ') group by employeenumber,masterrecord"
                    strSQL = "select employeenumber,masterrecord,sum( ddamount) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + "  and (isnull(DDACCOUNTTYPE ,'') = '22' or isnull(ddaccounttype, '')  = '  ') group by employeenumber,masterrecord";
                }
                else
                {
                    // strSQL = "select employeenumber,masterrecord,sum( isnull(ddamount, 0)) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & " and " & strWhere & " and (left(DDACCOUNTTYPE + '  ',2) = '22' or left(rtrim(isnull(ddaccounttype, '')) + '  ',2) = '  ') group by employeenumber,masterrecord"
                    strSQL = "select employeenumber,masterrecord,sum( ddamount) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and (isnull(DDACCOUNTTYPE ,'') = '22' or isnull(ddaccounttype, '') = '  ') group by employeenumber,masterrecord";
                }
                strPayQuery1 = "(" + strSQL + ") as CheckPayQuery1 ";
                if (boolReprint)
                {
                    // strSQL = "select employeenumber,masterrecord as masterrecorda,sum( isnull(ddamount, 0)) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & " and left(DDACCOUNTTYPE + '  ',2) = '32' group by employeenumber,masterrecord"
                    strSQL = "select employeenumber,masterrecord as masterrecorda,sum( ddamount) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and isnull(DDACCOUNTTYPE ,'') = '32' group by employeenumber,masterrecord";
                }
                else
                {
                    // strSQL = "select employeenumber,masterrecord as masterrecorda,sum( isnull(ddamount, 0)) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" & dtPayDate & "' and payrunid = " & gintCurrentPayRun & " and " & strWhere & " and left(DDACCOUNTTYPE + '  ',2) = '32' group by employeenumber,masterrecord"
                    strSQL = "select employeenumber,masterrecord as masterrecorda,sum( ddamount) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and isnull(DDACCOUNTTYPE ,'') = '32' group by employeenumber,masterrecord";
                }
                strPayQuery1a = "(" + strSQL + ") as CheckPayQuery1a ";
                // now combine these two as one so we can right join wiht the rest
                dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
                // strSQL = "select employeenumber,sum(isnull(distgrosspay, 0)) as YTDGross from tblcheckdetail where checkvoid = 0  and paydate between '" & dtStart & "' and '" & dtPayDate & "' group by employeenumber HAVING SUM( isnull(distgrosspay, 0)) > 0"
                strSQL = "select employeenumber,sum(distgrosspay) as YTDGross from tblcheckdetail where checkvoid = 0  and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber HAVING SUM( distgrosspay) > 0";
                // strSQL = "select employeenumber,sum(CalendarYTDTotal) as YTDGross from tblpaytotals where paycategory > 0 group by employeenumber"
                strPayQuery2 = "(" + strSQL + ") as CheckPayQuery2 ";
                // strSQL = "select employeenumber,sum( isnull(FederalTaxWH, 0)) as YTDFedTax from tblcheckdetail where checkvoid = 0 and paydate between '" & dtStart & "' and '" & dtPayDate & "' group by employeenumber "
                strSQL = "select employeenumber,sum( FederalTaxWH) as YTDFedTax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
                // strSQL = "select employeenumber,CalendarYTDTotal as YTDFedTax from tblpaytotals where standardpaytotal = 1 "
                strPayQuery3 = "(" + strSQL + ") as CheckPayQuery3 ";
                // strSQL = "select employeenumber,sum( isnull(ficataxwh, 0) +  isnull(medicaretaxwh, 0)) as YTDFica from tblcheckdetail where checkvoid = 0 and paydate between '" & dtStart & "' and '" & dtPayDate & "' group by employeenumber "
                strSQL = "select employeenumber,sum( ficataxwh) as YTDFica, sum(medicaretaxwh) as YTDMedicare from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
                // strSQL = "select employeenumber,sum(CalendarYTDTotal) as YTDFICA from tblpaytotals where standardpaytotal = 2 or standardpaytotal = 3 group by employeenumber"
                strPayQuery4 = "(" + strSQL + ") as CheckPayQuery4 ";
                // strSQL = "select employeenumber,sum( isnull(statetaxwh, 0)) as ytdstatetax from tblcheckdetail where checkvoid = 0 and paydate between '" & dtStart & "' and '" & dtPayDate & "' group by employeenumber "
                strSQL = "select employeenumber,sum( statetaxwh) as ytdstatetax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
                // strSQL = "select employeenumber,CalendarYTDTotal as YTDStateTax from tblpaytotals where standardpaytotal = 4 "
                strPayQuery5 = "(" + strSQL + ") as CheckPayQuery5 ";
                strSQL = "select MasterRecord, CheckPayQuery5.EmployeeNumber as CheckPayQuery5EmployeeNumber, DDepositSavAmount, DDepositChkAmount, YTDGross, YTDFedTax, YTDFICA, ytdmedicare, YTDStateTax  from " + strPayQuery1a + " right join (" + strPayQuery1 + " right join (" + strPayQuery2 + " inner join (" + strPayQuery3 + " inner join (" + strPayQuery4 + " inner join " + strPayQuery5 + " on (checkpayquery4.employeenumber = checkpayquery5.employeenumber)) on (checkpayquery3.employeenumber = checkpayquery4.employeenumber))";
                strSQL += " on (checkpayquery2.employeenumber = checkpayquery3.employeenumber)) on (checkpayquery1.employeenumber = checkpayquery2.employeenumber)) on (checkpayquery1a.employeenumber = checkpayquery1.employeenumber) and (checkpayquery1a.masterrecorda = checkpayquery1.masterrecord";
                strPayQuery6 = "(" + strSQL + ")) as CheckPayQuery6 ";
                if (!boolPrintInvalidChecksOnly && !boolPrintValidChecksOnly)
                {
                    strSQL = "select tblCheckDetail.MasterRecord as tblCheckDetailMasterRecord, *, tblemployeemaster.EmployeeNumber as EmployeeMasterEmployeeNumber from " + strPayQuery6 + " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.CheckPayQuery5EmployeeNumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and TotalRecord = 1 and " + strWhere + "  order by " + strOrderBy;
                }
                else if (boolPrintInvalidChecksOnly)
                {
                    // get only 100% dd checks
                    // strSQL = "select tblCheckDetail.MasterRecord as tblCheckDetailMasterRecord, *, tblemployeemaster.EmployeeNumber as EmployeeMasterEmployeeNumber from " & strPayQuery6 & " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.CheckPayQuery5EmployeeNumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" & dtPayDate & "' and PayRunID = " & gintCurrentPayRun & " and TotalRecord = 1 and " & strWhere & " AND (convert(int, isnull(DDEPOSITSAVAMOUNT, 0))  + convert(int, isnull(DDEPOSITCHKAMOUNT, 0)) >= netpay) order by " & strOrderBy
                    strSQL = "select tblCheckDetail.MasterRecord as tblCheckDetailMasterRecord, *, tblemployeemaster.EmployeeNumber as EmployeeMasterEmployeeNumber from " + strPayQuery6 + " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.CheckPayQuery5EmployeeNumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and TotalRecord = 1 and " + strWhere + " AND (DDEPOSITSAVAMOUNT  + DDEPOSITCHKAMOUNT >= netpay) order by " + strOrderBy;
                }
                else
                {
                    // get only <100% dd checks				
                    strSQL = "select tblCheckDetail.MasterRecord as tblCheckDetailMasterRecord, *, tblemployeemaster.EmployeeNumber as EmployeeMasterEmployeeNumber from " + strPayQuery6 + " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (checkpayquery6.CheckPayQuery5EmployeeNumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and TotalRecord = 1 and " + strWhere + " AND (DDEPOSITSAVAMOUNT + DDEPOSITCHKAMOUNT < netpay) order by " + strOrderBy;
                }
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!boolShowIndividualPayRates)
                {
                    strSQL = "select employeenumber,masterrecord,distpaycategory,sum(distgrosspay) as dgrosspay,sum(disthours) as distributionhours , 0 as distpayrate from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and distributionrecord = 1 group by employeenumber,masterrecord,distpaycategory order by employeenumber,masterrecord,distpaycategory";
                }
                else
                {
                    strSQL = "select employeenumber,masterrecord,distpaycategory,sum(distgrosspay) as dgrosspay,sum(disthours) as distributionhours ,distpayrate from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and distributionrecord = 1 group by employeenumber,masterrecord,distpaycategory,distpayrate order by employeenumber,masterrecord,distpaycategory";
                }
                clsPay.OpenRecordset(strSQL, "twpy0000.vb1");
                strSQL = "select employeenumber,masterrecord,sum(matchamount) as CurrentMatchAmount from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and  matchrecord = 1 group by employeenumber,masterrecord";
                strDed2 = "(" + strSQL + ") AS CheckDedQuery2 ";
                strSQL = "select * from " + strDed2 + " right join (select sum(matchamount) as YTDMatch,employeenumber from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber) as CheckDedQuery1 ON  (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;";
                clsEMatch.OpenRecordset(strSQL, "twpy0000.vb1");
                // *************************************************************************************************
                // *************************************************************************************************
                strSQL = "select sum(dedamount) as YTDDeduction,deddeductionnumber as Query3deductioncode,employeenumber as Query1EmployeeNumber from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber,deddeductionnumber";
                strDed1 = "(" + strSQL + ") as CheckDedQuery1 ";
                clsYTDDeductions.OpenRecordset(strSQL + " order by employeenumber,deddeductionnumber ", "twpy0000.vb1");
                strSQL = "select employeenumber,masterrecord,deddeductionnumber,sum(dedamount) as deductionamount,empdeductionid from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and  deductionrecord = 1 group by employeenumber,masterrecord,deddeductionnumber,empdeductionid";
                strDed2 = "(" + strSQL + ") as CheckDedQuery2 ";
                strSQL = "select * from (select * from " + strDed2 + " right join " + strDed1 + " on (Query1EmployeeNumber = checkdedquery2.employeenumber) and (checkdedquery2.deddeductionnumber = Query3deductioncode)) as checkdedquery3  LEFT join tblemployeedeductions on (checkdedquery3.empdeductionid = tblemployeedeductions.ID) order by Query1EmployeeNumber,masterrecord DESC,recordnumber";
                clsDeductions.OpenRecordset(strSQL, "twpy0000.vb1");
                // Vacation sick information
                rsVacSickCodeTypes.OpenRecordset("Select * from tblCodeTypes", "Payroll");
                while (!rsVacSickCodeTypes.EndOfFile())
                {
                    VacSickDescriptions.Add(rsVacSickCodeTypes.Get_Fields_Int32("id"), rsVacSickCodeTypes.Get_Fields_String("Description").Trim());
                    rsVacSickCodeTypes.MoveNext();
                }
                rsVacSickCodeTypes.FindFirstRecord("Description", "Vacation");
                if (rsVacSickCodeTypes.NoMatch)
                {
                    MessageBox.Show("Error finding Vacation ID code", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    lngVacCodeID = FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID"));
                    strSQL = "Select accruedcurrent,usedcurrent,accruedCalendar as accruedCalendarvacation,usedCalendar as usedCalendarvacation,usedbalance as vacationbalance,employeenumber from tblvacationsick where selected = 1 and typeid = " + FCConvert.ToString(lngVacCodeID);
                }
                clsVacation.OpenRecordset(strSQL, "twpy0000.vb1");
                rsVacSickCodeTypes.FindFirstRecord("Description", "Sick");
                if (rsVacSickCodeTypes.NoMatch)
                {
                    MessageBox.Show("Error finding Sick ID code", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    lngSickCodeID = FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID"));
                    strSQL = "select accruedcurrent,usedcurrent,accruedCalendar as accruedCalendarsick,usedCalendar as usedCalendarsick,usedbalance as sickbalance,employeenumber from tblvacationsick where selected = 1 and typeid = " + FCConvert.ToString(lngSickCodeID);
                }
                clsSick.OpenRecordset(strSQL, "twpy0000.vb1");

                // now get the other vacation sick codes
                int intTypeNumber;
                int intFound;
                int intCode1 = 0;
                int intCode2 = 0;
                int intCode3 = 0;
                intFound = 0;
                rsVacSickCodeTypes.MoveFirst();
                GridMisc.TextMatrix(0, 39, string.Empty);
                GridMisc.TextMatrix(0, 40, string.Empty);
                GridMisc.TextMatrix(0, 41, string.Empty);
                strSQL = "select accruedcurrent,usedcurrent,accruedCalendar,usedCalendar,usedbalance,employeenumber from tblvacationsick where typeid = 99999999";
                clsVacSickOther1.OpenRecordset(strSQL, "twpy0000.vb1");
                clsVacSickOther2.OpenRecordset(strSQL, "twpy0000.vb1");
                clsVacSickOther3.OpenRecordset(strSQL, "twpy0000.vb1");
                for (intTypeNumber = 1; intTypeNumber <= 3; intTypeNumber++)
                {
                NextTry:
                    ;
                    if (FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID")) == lngSickCodeID)
                    {
                        if (!rsVacSickCodeTypes.EndOfFile())
                            rsVacSickCodeTypes.MoveNext();
                        if (!rsVacSickCodeTypes.EndOfFile())
                            goto NextTry;
                    }
                    else if (FCConvert.ToInt32(rsVacSickCodeTypes.Get_Fields("ID")) == lngVacCodeID)
                    {
                        if (!rsVacSickCodeTypes.EndOfFile())
                            rsVacSickCodeTypes.MoveNext();
                        if (!rsVacSickCodeTypes.EndOfFile())
                            goto NextTry;
                    }
                    else
                    {
                        strSQL = "select accruedcurrent,usedcurrent,accruedCalendar,usedCalendar,usedbalance,employeenumber,typeid from tblvacationsick where selected = 1 and  typeid = " + rsVacSickCodeTypes.Get_Fields("ID");
                        if (intTypeNumber == 1)
                        {
                            clsVacSickOther1.OpenRecordset(strSQL, "twpy0000.vb1");
                            intCode1 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length > 3 && !boolLaserDblStub)
                            {
                                GridMisc.TextMatrix(0, 39, Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")), 4));
                            }
                            else
                            {
                                // MATTHEW 8/2/2005
                                GridMisc.TextMatrix(0, 39, rsVacSickCodeTypes.Get_Fields("Description"));
                            }
                            intFound = 1;
                        }
                        else if (intTypeNumber == 2)
                        {
                            clsVacSickOther2.OpenRecordset(strSQL, "twpy0000.vb1");
                            intCode2 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length > 3 && !boolLaserDblStub)
                            {
                                GridMisc.TextMatrix(0, 40, Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")), 4));
                            }
                            else
                            {
                                // MATTHEW 8/2/2005
                                GridMisc.TextMatrix(0, 40, rsVacSickCodeTypes.Get_Fields("Description"));
                            }
                            intFound = 2;
                        }
                        else if (intTypeNumber == 3)
                        {
                            clsVacSickOther3.OpenRecordset(strSQL, "twpy0000.vb1");
                            intCode3 = FCConvert.ToInt16(rsVacSickCodeTypes.Get_Fields("ID"));
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description"))).Length > 3 && !boolLaserDblStub)
                            {
                                GridMisc.TextMatrix(0, 41, Strings.Left(FCConvert.ToString(rsVacSickCodeTypes.Get_Fields("Description")), 4));
                            }
                            else
                            {
                                // MATTHEW 8/2/2005
                                GridMisc.TextMatrix(0, 41, rsVacSickCodeTypes.Get_Fields("Description"));
                            }
                            intFound = 3;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (!rsVacSickCodeTypes.EndOfFile())
                        rsVacSickCodeTypes.MoveNext();
                    if (rsVacSickCodeTypes.EndOfFile())
                        break;
                }

                switch (intFound)
                {
                    // ***********************************************************************************************************************
                    case 1:
                        strSQL = "select sum(AccruedCurrent) as AccruedCurrentOther, sum(UsedCurrent) as UsedCurrentOther, SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " + FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) + " AND TypeID <> " + FCConvert.ToString(intCode1) + " group by employeenumber";

                        break;
                    case 2:
                        strSQL = "select sum(AccruedCurrent) as AccruedCurrentOther, sum(UsedCurrent) as UsedCurrentOther,SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " + FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) + " AND TypeID <> " + FCConvert.ToString(intCode1) + " AND TypeID <> " + FCConvert.ToString(intCode2) + " group by employeenumber";

                        break;
                    case 3:
                        strSQL = "select sum(AccruedCurrent) as AccruedCurrentOther, sum(UsedCurrent) as UsedCurrentOther,SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " + FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) + " AND TypeID <> " + FCConvert.ToString(intCode1) + " AND TypeID <> " + FCConvert.ToString(intCode2) + " AND TypeID <> " + FCConvert.ToString(intCode3) + " group by employeenumber";

                        break;
                    default:
                        strSQL = "select sum(AccruedCurrent) as AccruedCurrentOther, sum(UsedCurrent) as UsedCurrentOther,SUM(accruedcalendar) as AccruedCalendarOther,SUM(usedCalendar) as usedCalendarOTher,sum(usedbalance) as OtherBalance,employeenumber from tblvacationsick  where selected = 1 and typeid <> " + FCConvert.ToString(lngSickCodeID) + " AND TypeID <> " + FCConvert.ToString(lngVacCodeID) + " group by employeenumber";

                        break;
                }
                clsVacSick.OpenRecordset(strSQL, "twpy0000.vb1");
                // check if we print the direct deposit checks to banks
                //Application.DoEvents();
                // open the direct deposit to banks info now
                // CHECK to see if sending to an ach bank or individual direct deposit banks
                clsDDBanks.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
                if (FCConvert.ToBoolean(clsDDBanks.Get_Fields_Boolean("achclearinghouse")))
                {
                    // don't do reg. dd stuff
                    // sum up all ddamounts
                    boolACH = true;
                    clsDDBanks.OpenRecordset("select * from tblbanks where achbank = 1", "twpy0000.vb1");
                    if (!clsDDBanks.EndOfFile())
                    {
                        lngACHBank = FCConvert.ToInt32(Math.Round(Conversion.Val(clsDDBanks.Get_Fields("ID"))));
                        strSQL = "select " + FCConvert.ToString(lngACHBank) + " as banknum ,checknumber,sum(DDAmount) as TotDDAmount from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and BankRecord = 1 and checkvoid = 0 group by checknumber";
                        strDDQuery1 = "(" + strSQL + ") as DDBankQuery1 ";
                        // ***********************************************************************
                        // MATTHEW 12/14/2005 CALL ID 73485
                        // strSQL = "select * ,ID as ddBankNumber from tblbanks inner join " & strDDQuery1 & " on (ddbankquery1.banknum = tblbanks.ID) where tblbanks.ID = " & lngACHBank "
                        // ***********************************************************************
                        strSQL = "select * ,ID as ddBankNumber from tblbanks inner join " + strDDQuery1 + " on (ddbankquery1.banknum = tblbanks.ID) where tblbanks.ID = " + FCConvert.ToString(lngACHBank) + " AND TotDDAmount > 0";
                        clsDDBanks.OpenRecordset(strSQL, "twpy0000.vb1");
                    }
                }
                else
                {
                    strSQL = "select ddBankNumber,checknumber,sum(DDAmount) as TotDDAmount from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " and BankRecord = 1 and checkvoid = 0 group by ddbanknumber,checknumber";
                    strDDQuery1 = "(" + strSQL + ") as DDBankQuery1 ";
                    strSQL = "select * from tblbanks inner join " + strDDQuery1 + " on (tblbanks.ID = convert(int, isnull(ddbankquery1.ddbanknumber, 0))) order by tblbanks.name";
                    clsDDBanks.OpenRecordset(strSQL, "twpy0000.vb1");
                }
                if (!boolReprint)
                {
                    // take care of any voided amounts to trust and agency checks
                    // do it before we create a check so we don't print a zero check
                    // but only do it for recipients that otherwise will be getting a check this time
                    CheckForTACorrections();
                }
                // now open the T and A checks info
                strSQL = "select trustrecipientid,checknumber,sum(trustamount) as TotTrustAmount from tblCheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " group by trustrecipientid,checknumber having sum(trustamount) > 0";
                strTandAQuery = "(" + strSQL + ") as TandACheckQuery1 ";
                strSQL = "select trustrecipientid,checknumber,sum(trustamount) as FedTrust from tblcheckdetail where trustcategoryid = 1 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " group by trustrecipientid,checknumber ";
                strTandAFed = "(" + strSQL + ") as TandAFedQuery";
                strSQL = "select trustrecipientid,checknumber,sum(trustamount) as FicaTrust from tblcheckdetail where trustcategoryid = 2 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " group by trustrecipientid,checknumber ";
                strTandAFica = "(" + strSQL + ") as TandAFicaQuery";
                strSQL = "select trustrecipientid,checknumber,sum(trustamount) as MedTrust from tblcheckdetail where trustcategoryid = 3 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " group by trustrecipientid,checknumber ";
                strTandAMed = "(" + strSQL + ") as TandAMedQuery";
                strSQL = "select trustrecipientid,checknumber,sum(trustamount) as StateTrust from tblcheckdetail where trustcategoryid = 4 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and " + strWhere + " group by trustrecipientid,checknumber ";
                strTandAState = "(" + strSQL + ") as TandAStateQuery";
                strSQL = "select *, parentquery1.trustrecipientid as parentquery1trustrecipientid from " + strTandAState + " right join (" + strTandAMed + " right join (" + strTandAFica + " right join (" + strTandAFed + " right join (select * from tblrecipients inner join " + strTandAQuery + " on (tblrecipients.ID = tandacheckquery1.trustrecipientid)) as parentquery1 ";
                strSQL += " on (tandafedquery.trustrecipientid = parentquery1.trustrecipientid) and (tandafedquery.checknumber = parentquery1.checknumber)) ";
                strSQL += " on (tandaficaquery.trustrecipientid = parentquery1.trustrecipientid) and (tandaficaquery.checknumber = parentquery1.checknumber)) ";
                strSQL += " on (tandamedquery.trustrecipientid = parentquery1.trustrecipientid) and (tandamedquery.checknumber = parentquery1.checknumber)) ";
                strSQL += " on (tandastatequery.trustrecipientid = parentquery1.trustrecipientid) and (tandastatequery.checknumber = parentquery1.checknumber) ";
                strSQL += " order by name ";
                clsTandAChecks.OpenRecordset(strSQL, "twpy0000.vb1");
                if (intCheckType == 0 && intNumStubs < 2)
                {
                    // this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(215900/1440f), FCConvert.ToInt32(177800/1440f));
                    this.Detail.Height = 7F;
                }
                if (modGlobalVariables.Statics.gboolBudgetary && !boolReprint)
                {
                    clsDRWrapper clsTemp = new clsDRWrapper();
                    int lngLastCheck = 0;
                    lngLastCheck = lngStartingCheckNumber;
                    if (clsLoad.RecordCount() > 0)
                    {
                        lngLastCheck = clsLoad.RecordCount() + lngLastCheck - 1;
                    }
                    if (clsDDBanks.RecordCount() > 0)
                    {
                        lngLastCheck = clsDDBanks.RecordCount() + lngLastCheck - 1;
                    }
                    if (clsTandAChecks.RecordCount() > 0)
                    {
                        lngLastCheck = clsTandAChecks.RecordCount() + lngLastCheck - 1;
                    }
                    strSQL = "select * from checkrecmaster where banknumber = " + FCConvert.ToString(lngBankNumber) + " and checknumber between " + FCConvert.ToString(lngStartingCheckNumber) + " and " + FCConvert.ToString(lngLastCheck);
                    clsTemp.OpenRecordset(strSQL, "twbd0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        frmWait.InstancePtr.Unload();
                        if (MessageBox.Show("At least some of the check numbers to be printed match check numbers already recorded for this bank" + "\r\n" + "Do you wish to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            modGlobalFunctions.AddCYAEntry("PY", "Printed check nums already in Bud", "Bank " + FCConvert.ToString(lngBankNumber), "Checks " + FCConvert.ToString(lngStartingCheckNumber) + " to " + FCConvert.ToString(lngLastCheck), "Paydate " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate), "Payrun " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun));
                            frmWait.InstancePtr.Init("Loading Check Information");
                        }
                        else
                        {
                            // Unload Me
                            frmWait.InstancePtr.Unload();
                            return;
                        }
                    }
                }
                if (!boolPrintValidChecksSeparate)
                {
                    frmWait.InstancePtr.Unload();
                    frmReportViewer.InstancePtr.Init(this, "", boolOverRideDuplex: true, boolAllowEmail: false);
                }
                else
                {
                    // check ahead of time to determine the check numbers
                    frmWait.InstancePtr.Init("Creating Checks");
                    lngValidCheckNumber = lngCurrentCheckNumber;
                    while (!clsLoad.EndOfFile())
                    {
                        if (!(Conversion.Val(clsLoad.Get_Fields("netpay")) - Conversion.Val(clsLoad.Get_Fields("ddepositchkamount")) - Conversion.Val(clsLoad.Get_Fields("ddepositsavamount")) < 0.01))
                        {
                            lngValidCheckNumber += 1;
                        }
                        clsLoad.MoveNext();
                    }
                    clsLoad.MoveFirst();
                    while (!clsDDBanks.EndOfFile())
                    {
                        if (!FCConvert.ToBoolean(clsDDBanks.Get_Fields_Boolean("EFT")) && !boolACH)
                        {
                            lngValidCheckNumber += 1;
                        }
                        clsDDBanks.MoveNext();
                    }
                    clsDDBanks.MoveFirst();
                    while (!clsTandAChecks.EndOfFile())
                    {
                        if (!FCConvert.ToBoolean(clsTandAChecks.Get_Fields_Boolean("EFT")))
                        {
                            lngValidCheckNumber += 1;
                        }
                        clsTandAChecks.MoveNext();
                    }
                    clsTandAChecks.MoveFirst();
                    lngInvalidCheckNumber = lngValidCheckNumber;
                    if (lngNonNegStart > 0)
                        lngInvalidCheckNumber = lngNonNegStart;
                    lngValidCheckNumber = lngCurrentCheckNumber;
                    this.Run(false);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init");
            }
        }

		private void CheckForTACorrections()
		{
			clsDRWrapper clsTATotals = new clsDRWrapper();
			// will hold the summed totals for each recipient from tblCheckDetail
			clsDRWrapper clsTACorrections = new clsDRWrapper();
			// will hold the correction amounts to be subtracted
			clsDRWrapper clsExecute = new clsDRWrapper();
			// will be used to create new TA records in tblCheckDetail
			string strSQL;
			int CurrentRecipient;
			// vbPorter upgrade warning: dblRecipientTotal As double	OnWrite(int, string, double)
			double dblRecipientTotal;
			// vbPorter upgrade warning: dblAmountToReduce As double	OnWrite(string, double)
			double dblAmountToReduce = 0;
			string strAccount = "";
			int lngCatID = 0;
			int lngCheckNumber = 0;
			int lngJournal;
			int lngWarrant;
			int intAccountingPer;
			// vbPorter upgrade warning: dblAdj2 As double	OnWrite(string)
			double dblAdj2 = 0;
			clsDRWrapper rsTAAdjust = new clsDRWrapper();
			int lngReportType = 0;
			int lngCurrentCategory;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsTAAdjust.OpenRecordset("Select * from tblCheckDetail where CheckNumber = 99999999999");
				strSQL = "select * from tblTAReduction order by recipientid,checknumber";
				clsTACorrections.OpenRecordset(strSQL, "twpy0000.vb1");
				if (clsTACorrections.EndOfFile())
					return;
				// nothing to process
				strSQL = "select sum(trustamount) as trusttotal,TrustRecipientid from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and (TrustRecord = 1) group by trustrecipientid ";
				// strSQL = "select sum(trustamount) as trusttotal,TRUSTcategoryid,TrustRecipientid from tblcheckdetail where checkvoid = 0 and paydate = '" & gdatCurrentPayDate & "' and payrunid = " & gintCurrentPayRun & " and (TrustRecord = 1) group by trustrecipientid,TRUSTcategoryid "
				clsTATotals.OpenRecordset(strSQL, "twpy0000.vb1");
				if (clsTATotals.EndOfFile())
					return;
				// nothing to process
				strSQL = "select top 1 JournalNumber,WarrantNumber,AccountingPeriod from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " and (TrustRecord = 1) ";
				clsExecute.OpenRecordset(strSQL, "twpy0000.vb1");
				lngJournal = clsExecute.Get_Fields_Int32("JournalNumber");
				lngWarrant = clsExecute.Get_Fields_Int32("WarrantNumber");
				intAccountingPer = clsExecute.Get_Fields_Int32("AccountingPeriod");
				CurrentRecipient = 0;
				dblRecipientTotal = 0;
				lngCurrentCategory = 0;
				while (!clsTACorrections.EndOfFile())
				{
					//Application.DoEvents();
					if (clsTACorrections.Get_Fields_Int32("recipientid") != CurrentRecipient)
					{
						// Or (lngCurrentCategory <> clsTACorrections.Fields("CATEGORYID")) Then
						CurrentRecipient = clsTACorrections.Get_Fields_Int32("recipientid");
						lngCurrentCategory = clsTACorrections.Get_Fields_Int32("categoryid");
						if (clsTATotals.FindFirstRecord("trustrecipientid", clsTACorrections.Get_Fields_Int32("recipientid")))
						{
							// If clsTATotals.FindFirstRecord(, , "trustrecipientid = " & clsTACorrections.Fields("recipientid") & " and trustcategoryid = " & lngCurrentCategory) Then
							dblRecipientTotal = FCConvert.ToDouble(Strings.Format(clsTATotals.Get_Fields_Double("trusttotal"), "0.00"));
						}
						else
						{
							dblRecipientTotal = 0;
							// takes care of the no match
						}
					}
					if (dblRecipientTotal > 0)
					{
						strAccount = clsTACorrections.Get_Fields_String("Deductionaccountnumber");
						lngCatID = FCConvert.ToInt32(clsTACorrections.Get_Fields_Int32("CategoryID"));
						lngCheckNumber = clsTACorrections.Get_Fields_Int32("CheckNumber");
						if (clsTACorrections.Get_Fields_Double("amount") < 0)
						{
							dblAdj2 = FCConvert.ToDouble(Strings.Format(clsTACorrections.Get_Fields_Double("amount") * -1, "0.00"));
						}
						lngReportType = clsTACorrections.Get_Fields_Int32("reporttype");
						if (dblRecipientTotal >= dblAdj2 || clsTACorrections.Get_Fields_Double("amount") > 0)
						{
							dblAmountToReduce = FCConvert.ToDouble(Strings.Format(clsTACorrections.Get_Fields_Double("amount"), "0.00"));
							dblRecipientTotal += dblAmountToReduce;
							strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
							strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
							strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
							strSQL += "," + FCConvert.ToString(CurrentRecipient);
							strSQL += "," + FCConvert.ToString(lngCurrentCategory);
							strSQL += "," + clsTACorrections.Get_Fields_Int32("deductionid");
							strSQL += "," + FCConvert.ToString(-dblAmountToReduce);
							strSQL += "," + FCConvert.ToString(lngCheckNumber);
							strSQL += ",'" + clsTACorrections.Get_Fields_String("deductionaccountNUMBER") + "'";
							strSQL += "," + clsTACorrections.Get_Fields_Int32("reporttype");
							strSQL += ")";
							clsExecute.Execute(strSQL, "Payroll");
							clsTACorrections.Edit();
							clsTACorrections.Delete();
							clsTACorrections.Update();
						}
						else
						{
							// recipient total < amount
							dblAmountToReduce = dblRecipientTotal;
							dblRecipientTotal = 0;
							strSQL = "Insert into TAReductionAdjust(Paydate,Payrun,RecipientID,CategoryID,DeductionID,Amount,CheckNumber,DeductionAccount,Reporttype) values (";
							strSQL += "'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
							strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
							strSQL += "," + FCConvert.ToString(CurrentRecipient);
							strSQL += "," + FCConvert.ToString(lngCurrentCategory);
							strSQL += "," + clsTACorrections.Get_Fields("deductionid");
							strSQL += "," + FCConvert.ToString(dblAmountToReduce);
							strSQL += "," + FCConvert.ToString(lngCheckNumber);
							strSQL += ",'" + clsTACorrections.Get_Fields("deductionaccountnumber") + "'";
							strSQL += "," + clsTACorrections.Get_Fields("reporttype");
							strSQL += ")";
							clsExecute.Execute(strSQL, "Payroll");
							clsTACorrections.Edit();
							// clsTACorrections.Fields("amount") = clsTACorrections.Fields("amount") - dblAmountToReduce
							clsTACorrections.Set_Fields("amount", clsTACorrections.Get_Fields_Double("amount").RoundToMoney() + dblAmountToReduce);
							clsTACorrections.Update();
							dblAmountToReduce = -dblAmountToReduce;
						}
						if (dblAmountToReduce != 0)
						{
							strSQL = "Insert into tblcheckdetail (taadjustrecord,AdjustRecord, checknumber,Paydate,PayrunID,JournalNumber,WarrantNumber,AccountingPeriod,TrustRecord,TrustRecipientid,TrustAmount,TrustCategoryID,TrustDeductionDescription,TrustDeductionAccountNumber,reporttype) values (";
							strSQL += "1,1, 0";
							strSQL += ",'" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "'";
							strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
							strSQL += "," + FCConvert.ToString(lngJournal);
							strSQL += "," + FCConvert.ToString(lngWarrant);
							strSQL += "," + FCConvert.ToString(intAccountingPer);
							strSQL += ",1";
							strSQL += "," + FCConvert.ToString(CurrentRecipient);
							strSQL += "," + FCConvert.ToString(dblAmountToReduce);
							strSQL += "," + FCConvert.ToString(lngCatID);
							strSQL += ",'Voided Check Number " + FCConvert.ToString(lngCheckNumber) + "'";
							strSQL += ",'" + strAccount + "'";
							strSQL += "," + FCConvert.ToString(lngReportType);
							strSQL += ")";
							clsExecute.Execute(strSQL, "Payroll");
							modGlobalFunctions.AddCYAEntry_6("PY", "TA Adjustment Applied on " + FCConvert.ToString(DateTime.Now), "PayDate: " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + " Pay Run ID: " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "CheckNumber " + FCConvert.ToString(lngCheckNumber), "Recipient " + FCConvert.ToString(CurrentRecipient) + " Amount " + FCConvert.ToString(dblAmountToReduce));
						}
					}
					clsTACorrections.MoveNext();
				}
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					// vbPorter upgrade warning: intArrayID As int	OnWriteFCConvert.ToInt32(
					int intArrayID = 0;
					clsDRWrapper rsData = new clsDRWrapper();
					// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
					int intCounter;
					int lngBank = 0;
					int lngTownCashIndex = 0;
					int lngSchoolCashIndex;
					int lngCurrCashIndex;
					int lngTempIndex;
					string strCurrAcct = "";
					string strTempAcct = "";
					int lngSwapIndex;
					modGlobalVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
					intArrayID = 0;
					lngBank = 0;
					rsData.OpenRecordset("select * from  tblPayrollSteps where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun), "Payroll");
					if (!rsData.EndOfFile())
					{
						lngBank = rsData.Get_Fields_Int32("banknumber");
					}
					if (lngBank == 0)
					{
						lngBank = modBudgetaryAccounting.GetBankVariable("PayrollBank").ToString().ToIntegerValue();
					}
					// TA ADJUSTMENT RECORDS. THESE ARE THE RECORDS THAT AT ONE TIME EXISTED IN THE
					// TBLTAREDUCTION TABLE AND WHEN CHECKS WHERE RUN THEN OFFSETTING ENTRIES WHERE
					// CREATED INTO THE CHECK DETAIL TABLE.
					rsData.OpenRecordset("SELECT * from tblCheckDetail WHERE AdjustRecord = 1 AND CheckNumber = 0 and JournalNumber = " + lngJournal + " AND WarrantNumber = " + lngWarrant + " AND PayDate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' AND PayRunID = " + modGlobalVariables.Statics.gintCurrentPayRun, "Payroll");
					if (!rsData.EndOfFile())
					{
						for (intCounter = 0; intCounter <= (rsData.RecordCount() - 1); intCounter++)
						{
							//Application.DoEvents();
							Array.Resize(ref modGlobalVariables.Statics.AccountStrut, Information.UBound(modGlobalVariables.Statics.AccountStrut, 1) + 1 + 1);
							modGlobalVariables.Statics.AccountStrut[intArrayID].Account = rsData.Get_Fields_String("TrustDeductionAccountNumber");
							modGlobalVariables.Statics.AccountStrut[intArrayID].AcctType = "A";
							modGlobalVariables.Statics.AccountStrut[intArrayID].RCB = "R";
							modGlobalVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(rsData.Get_Fields_Double("TrustAmount"), "0.00"));
							modGlobalVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yy") + " Payroll (TA Adj)";
							intArrayID += 1;
							rsData.MoveNext();
						}
					}
					// THIS WILL CREATE A SEPERATE OFFSETTING CASH ENTRY FOR THE NEW JOURNAL ENTRIES
					if (modGlobalVariables.Statics.gboolBudgetary)
					{
						// this sets up the cash accounts for the funds if the user has BD
						modGlobalVariables.Statics.ftFundArray = modBudgetaryAccounting.CalcFundCash(ref modGlobalVariables.Statics.AccountStrut);
						// ftSchoolFundArray = CalcFundCash(AccountStrut(), True)
					}
					else
					{
						// create a cash entry into the account M CASH for these accounts
						modGlobalVariables.Statics.ftFundArray = modGlobalRoutines.CashAccountForM(ref modGlobalVariables.Statics.AccountStrut);
					}
					string strAcctOR = "";
                    strAcctOR = "";
                    if (modGlobalVariables.Statics.gboolBudgetary)
                    {
                        strAcctOR = modBudgetaryAccounting.GetBankOverride(ref lngBank, true);
                    }
                    if (strAcctOR != string.Empty)
                    {
                        // had default cash account that may need to be split
                        modBudgetaryAccounting.CalcCashFundsFromOverride(ref strAcctOR, ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(DateTime.Today, "MM/dd/yyyy") + " PY", "PY", false);
                    }
                    else
                    {
                        if (modBudgetaryAccounting.CalcDueToFrom(ref modGlobalVariables.Statics.AccountStrut, ref modGlobalVariables.Statics.ftFundArray, false, Strings.Format(DateTime.Today, "MM/dd/yyyy") + " PYDTDF", false, false, "PY", "", false, lngTownCashIndex, true))
                        {
                        }
                    }
					// ADD THESE JOURNAL ENTRIES TO THE EXISTING JOURNAL.
					intArrayID = modBudgetaryAccounting.AddToJournal(ref modGlobalVariables.Statics.AccountStrut, "PY", lngJournal, Convert.ToDateTime(Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy")), Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy") + " Payroll");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckForTACorrections", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


        private void AddNonVacSickLeaveToCollection(string employeeNumber)
        {
            var strSQL = "select AccruedCurrent,UsedCurrent, accruedcalendar, usedCalendar, usedbalance,employeenumber, typeid from tblvacationsick where selected = 1 and typeid <> " + lngSickCodeID + " and TypeID <> " + lngVacCodeID + " and employeenumber = '" + employeeNumber + "'";

            rsLeave.OpenRecordset(strSQL, "Payroll");
            while (!rsLeave.EndOfFile())
            {
                var vsHistory = new cVacSickHistoryItem();
                vsHistory.Balance = rsLeave.Get_Fields_Double("UsedBalance");
                vsHistory.Accrued = rsLeave.Get_Fields_Double("AccruedCurrent");
                vsHistory.Used = rsLeave.Get_Fields_Double("UsedCurrent");
                vsHistory.CalendarAccrued = rsLeave.Get_Fields_Double("AccuredCalendar");
                vsHistory.CalendarUsed = rsLeave.Get_Fields_Double("UsedCalendar");
                vsHistory.ID = rsLeave.Get_Fields_Int32("typeid");
                if (VacSickDescriptions.ContainsKey(vsHistory.ID))
                {
                    vsHistory.Description = VacSickDescriptions[vsHistory.ID];
                }
                VacSickHistoryColl.Add(vsHistory);
                rsLeave.MoveNext();
            }
        }
    }
}
