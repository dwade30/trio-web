﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cPayrollBank
	{
		//=========================================================
		// vbPorter upgrade warning: lngRecordNumber As int	OnWriteFCConvert.ToInt32(
		private int lngRecordNumber;
		private int lngIDNumber;
		private string strBankName = string.Empty;
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strAddress3 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strBankDepositID = string.Empty;
		private bool boolEFT;
		private bool boolACH;
		private string strShortDescription = string.Empty;
		private string strCheckingAccount = string.Empty;
		private DateTime dtLastUpdate;
		private string strLastUserID = string.Empty;
		// vbPorter upgrade warning: lngRecord As int	OnRead
		public int RecordNumber
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int RecordNumber = 0;
				RecordNumber = lngRecordNumber;
				return RecordNumber;
			}
		}

		public int ID
		{
			set
			{
				lngIDNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngIDNumber;
				return ID;
			}
		}

		public string Name
		{
			set
			{
				strBankName = value;
			}
			get
			{
				string Name = "";
				Name = strBankName;
				return Name;
			}
		}

		public string Address1
		{
			set
			{
				strAddress1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddress2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}

		public string Address3
		{
			set
			{
				strAddress3 = value;
			}
			get
			{
				string Address3 = "";
				Address3 = strAddress3;
				return Address3;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string BankDepositID
		{
			set
			{
				strBankDepositID = value;
			}
			get
			{
				string BankDepositID = "";
				BankDepositID = strBankDepositID;
				return BankDepositID;
			}
		}

		public bool EFT
		{
			set
			{
				boolEFT = value;
			}
			get
			{
				bool EFT = false;
				EFT = boolEFT;
				return EFT;
			}
		}

		public bool ACH
		{
			set
			{
				boolACH = value;
			}
			get
			{
				bool ACH = false;
				ACH = boolACH;
				return ACH;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string CheckingAccount
		{
			set
			{
				strCheckingAccount = value;
			}
			get
			{
				string CheckingAccount = "";
				CheckingAccount = strCheckingAccount;
				return CheckingAccount;
			}
		}

		public DateTime LastUpdate
		{
			set
			{
				dtLastUpdate = value;
			}
			get
			{
				DateTime LastUpdate = System.DateTime.Now;
				LastUpdate = dtLastUpdate;
				return LastUpdate;
			}
		}

		public string LastUserID
		{
			set
			{
				strLastUserID = value;
			}
			get
			{
				string LastUserID = "";
				LastUserID = strLastUserID;
				return LastUserID;
			}
		}
	}
}
