//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmClearLTDDeductions : BaseForm
	{
		public frmClearLTDDeductions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmClearLTDDeductions InstancePtr
		{
			get
			{
				return (frmClearLTDDeductions)Sys.GetInstance(typeof(frmClearLTDDeductions));
			}
		}

		protected frmClearLTDDeductions _InstancePtr = null;
		//=========================================================
		// ****************************************************************************
		// ***************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTTHEW LARRABEE
		// DATE:       June 3, 2004
		//
		// NOTES:
		//
		// ****************************************************************************
		// ****************************************************************************
		// PRIVATE LOCAL VARIABLES
		private int intCounter;
		private clsHistory clsHistoryClass = new clsHistory();
		private clsDRWrapper rsData = new clsDRWrapper();

		private void frmClearLTDDeductions_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// DO NOT ALLOW MULTIPLE INSTANCES OF THIS FORM TO EXIST
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmClearLTDDeductions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// THIS IS WHERE W
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void SaveContinue()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveContinue";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				double dblRate;
				int intCounter;
				// CHECK THE VALIDITY OF THE SEQUENCE VALUE.
				if (cboSequence.SelectedIndex < 0)
				{
					MessageBox.Show("Invalid Sequence Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboSequence.Focus();
					return;
				}
				// CHECK THE VALIDITY OF THE DEDUCTION NUMBER
				if (cboDeductions.SelectedIndex < 0)
				{
					MessageBox.Show("Invalid Deduction Number.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboDeductions.Focus();
					return;
				}
				// UPDATE THE DATABASE SO THAT WE HAVE CLEARED OUT THE LIFE TO DATE
				if (MessageBox.Show("Are you sure you wish to update all records with the sequence number of: " + cboSequence.Text + "\r" + "and a deduction number of: " + cboDeductions.Text + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGlobalFunctions.AddCYAEntry_6("PY", "Clear LTD Deduction Totals", "Sequence: " + cboSequence.Text, "DeductionNumber: " + cboDeductions.Text);
					clsDRWrapper rsExecute = new clsDRWrapper();
					// Call rsExecute.Execute("UPDATE tblEmployeeMaster INNER JOIN tblEmployeeDeductions ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber SET tblEmployeeDeductions.LTDTotal = 0 WHERE (((tblEmployeeMaster.SeqNumber)=" & cboSequence & ") AND ((tblEmployeeDeductions.DeductionCode)=" & cboDeductions.ItemData(cboDeductions.ListIndex) & "))", "TWPY0000.vb1")
					modGlobalVariables.Statics.gintSequenceNumber = FCConvert.ToInt32(cboSequence.Text);
					modGlobalVariables.Statics.gintDeductionNumber = cboDeductions.ItemData(cboDeductions.SelectedIndex);
					rptClearLTDDeductions.InstancePtr.Init(this.Modal);
					// MsgBox "Update of database completed successfully.", vbOKOnly, "TRIO Software"
					// Unload Me
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmClearLTDDeductions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClearLTDDeductions properties;
			//frmClearLTDDeductions.ScaleWidth	= 8145;
			//frmClearLTDDeductions.ScaleHeight	= 4815;
			//frmClearLTDDeductions.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// SET THE SIZE OF THE FORM
				modGlobalFunctions.SetFixedSize(this, 1);
				fraWarning.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				LoadCombos();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// UNLOAD THE FORM
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// SET FOCUS BACK TO THE MENU OPTIONS OF THE MDIPARENT
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveContinue();
		}

		private void LoadCombos()
		{
			rsData.OpenRecordset("Select distinct SeqNumber from tblEmployeeMaster Order by SeqNumber Asc", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				cboSequence.AddItem(FCConvert.ToString(rsData.Get_Fields("SeqNumber")));
				rsData.MoveNext();
			}
			rsData.OpenRecordset("Select * from tblDeductionSetup Order by DeductionNumber Asc", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber"))).Length > 4)
				{
					cboDeductions.AddItem(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")) + " - " + rsData.Get_Fields_String("Description"), rsData.Get_Fields("ID"));
				}
				else
				{
					cboDeductions.AddItem(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")) + Strings.StrDup(4 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber"))).Length, " ") + "- " + rsData.Get_Fields_String("Description"));
					cboDeductions.ItemData(cboDeductions.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				}
				rsData.MoveNext();
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(cmdSave, EventArgs.Empty);
        }
    }
}
