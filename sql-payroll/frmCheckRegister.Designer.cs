﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCheckRegister.
	/// </summary>
	partial class frmCheckRegister
	{
		public fecherFoundation.FCComboBox cmbReprint;
		public fecherFoundation.FCLabel lblReprint;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCComboBox cboBanks;
		public fecherFoundation.FCButton cmdProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbReprint = new fecherFoundation.FCComboBox();
            this.lblReprint = new fecherFoundation.FCLabel();
            this.fraBanks = new fecherFoundation.FCFrame();
            this.Command3 = new fecherFoundation.FCButton();
            this.cboBanks = new fecherFoundation.FCComboBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
            this.fraBanks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 248);
            this.BottomPanel.Size = new System.Drawing.Size(442, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbReprint);
            this.ClientArea.Controls.Add(this.lblReprint);
            this.ClientArea.Controls.Add(this.fraBanks);
            this.ClientArea.Size = new System.Drawing.Size(442, 188);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(442, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(180, 30);
            this.HeaderText.Text = "Check Register";
            // 
            // cmbReprint
            // 
            this.cmbReprint.AutoSize = false;
            this.cmbReprint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReprint.FormattingEnabled = true;
            this.cmbReprint.Items.AddRange(new object[] {
            "Initial Run",
            "Reprint Register" });
            this.cmbReprint.Location = new System.Drawing.Point(212, 30);
            this.cmbReprint.Name = "cmbReprint";
            this.cmbReprint.Size = new System.Drawing.Size(202, 40);
            this.cmbReprint.TabIndex = 6;
            this.cmbReprint.Text = "Initial Run";
            // 
            // lblReprint
            // 
            this.lblReprint.AutoSize = true;
            this.lblReprint.Location = new System.Drawing.Point(30, 44);
            this.lblReprint.Name = "lblReprint";
            this.lblReprint.Size = new System.Drawing.Size(95, 15);
            this.lblReprint.TabIndex = 7;
            this.lblReprint.Text = "REPORT TYPE";
            // 
            // fraBanks
            // 
            this.fraBanks.Controls.Add(this.Command3);
            this.fraBanks.Controls.Add(this.cboBanks);
            this.fraBanks.Location = new System.Drawing.Point(30, 30);
            this.fraBanks.Name = "fraBanks";
            this.fraBanks.Size = new System.Drawing.Size(325, 150);
            this.fraBanks.TabIndex = 5;
            this.fraBanks.Text = "Select A Bank";
            this.fraBanks.Visible = false;
            // 
            // Command3
            // 
            this.Command3.AppearanceKey = "actionButton";
            this.Command3.Location = new System.Drawing.Point(20, 90);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(104, 40);
            this.Command3.TabIndex = 9;
            this.Command3.Text = "Process";
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // cboBanks
            // 
            this.cboBanks.AutoSize = false;
            this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
            this.cboBanks.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboBanks.FormattingEnabled = true;
            this.cboBanks.Items.AddRange(new object[] {
            "1 - Most Recently Run Report",
            "2 - Next Oldest Report",
            "3 - 3rd Oldest Report",
            "4 - 4th Oldest Report",
            "5 - 5th Oldest Report",
            "6 - 6th Oldest Report",
            "7 - 7th Oldest Report",
            "8 - 8th Oldest Report",
            "9 - 9th Oldest Report"});
            this.cboBanks.Location = new System.Drawing.Point(20, 30);
            this.cboBanks.Name = "cboBanks";
            this.cboBanks.Size = new System.Drawing.Size(285, 40);
            this.cboBanks.TabIndex = 8;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(165, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(105, 48);
            this.cmdProcess.TabIndex = 1;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmCheckRegister
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(442, 356);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCheckRegister";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Check Register";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCheckRegister_Load);
            this.Activated += new System.EventHandler(this.frmCheckRegister_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckRegister_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
            this.fraBanks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Command3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton Command3;
    }
}
