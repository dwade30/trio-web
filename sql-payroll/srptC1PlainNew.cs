//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Plain.
	/// </summary>
	public partial class srptC1Plain : FCSectionReport
	{
		public srptC1Plain()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "ActiveReport1";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptC1Plain InstancePtr
		{
			get
			{
				return (srptC1Plain)Sys.GetInstance(typeof(srptC1Plain));
			}
		}

		protected srptC1Plain _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptC1Plain	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCoreysSweeterCode.EWRInfo EWRRecord = new modCoreysSweeterCode.EWRInfo();
		bool boolTestPrint;

		private void LoadInfo()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    EWRRecord.EmployerAddress = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("address1")), 1,
                        modCoreysSweeterCode.EWRReturnAddressLen);
                    EWRRecord.EmployerCity = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("city")), 1,
                        modCoreysSweeterCode.EWRReturnCityLen);
                    EWRRecord.EmployerName = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("EmployerName")),
                        1, modCoreysSweeterCode.EWRReturnNameLen);
                    EWRRecord.EmployerState = FCConvert.ToString(clsLoad.Get_Fields("state"));
                    EWRRecord.EmployerStateCode = modCoreysSweeterCode.GetStateCode(EWRRecord.EmployerState);
                    EWRRecord.EmployerZip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
                    EWRRecord.EmployerZip4 = FCConvert.ToString(clsLoad.Get_Fields("zip4"));
                    EWRRecord.FederalEmployerID = FCConvert.ToString(clsLoad.Get_Fields_String("FederalEmployerID"));
                    EWRRecord.MRSWithholdingID =
                        Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid")), 1, 2) + " " +
                        Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("mrswithholdingid")), 3);
                    EWRRecord.StateUCAccount = FCConvert.ToString(clsLoad.Get_Fields_String("StateUCAccount"));
                    EWRRecord.TransmitterExt = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterExtension"));
                    EWRRecord.TransmitterPhone = FCConvert.ToString(clsLoad.Get_Fields_String("TransmitterPhone"));
                    EWRRecord.TransmitterTitle = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields("transmittertitle")),
                        1, modCoreysSweeterCode.EWRTransmitterTitleLen);
                    EWRRecord.TransmitterEmail = FCConvert.ToString(clsLoad.Get_Fields("transmitteremail"));
                }
                else
                {
                    EWRRecord.EmployerAddress = "";
                    EWRRecord.EmployerCity = "";
                    EWRRecord.EmployerName = "";
                    EWRRecord.EmployerState = "ME";
                    EWRRecord.EmployerStateCode = 23;
                    EWRRecord.EmployerZip = "";
                    EWRRecord.EmployerZip4 = "";
                    EWRRecord.FederalEmployerID = "";
                    EWRRecord.MRSWithholdingID = "";
                    EWRRecord.TransmitterExt = "";
                    EWRRecord.TransmitterPhone = "0000000000";
                    EWRRecord.TransmitterTitle = "";
                    EWRRecord.TransmitterEmail = "";
                    EWRRecord.StateUCAccount = "";
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intQuarterCovered = 0;
			string strTemp = "";
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL = "";
                int x;
                double dblTotalWithheld;
                double dblTotalPaid;
                double dblWithheldSubtotalA;
                double dblWithheldSubtotalB;
                double dblWithheldSubtotalC;
                double dblPaymentSubtotalA;
                double dblPaymentSubtotalB;
                double dblPaymentSubtotalC;
                int intTemp;
                int intMonthOne = 0;
                // vbPorter upgrade warning: dtStart As DateTime	OnWrite(string)
                DateTime dtStart = DateTime.Now;
                // vbPorter upgrade warning: dtEnd As DateTime	OnWrite(string)
                DateTime dtEnd = DateTime.Now;
                boolTestPrint = modCoreysSweeterCode.Statics.EWRWageTotals.PrintTest;
                dblWithheldSubtotalA = 0;
                dblWithheldSubtotalB = 0;
                dblWithheldSubtotalC = 0;
                dblPaymentSubtotalA = 0;
                dblPaymentSubtotalB = 0;
                dblPaymentSubtotalC = 0;
                dblTotalWithheld = 0;
                dblTotalPaid = 0;
                LoadInfo();
                if (!boolTestPrint)
                {
                    txtMaineLicense.Visible = false;
                    txtDate.Text = Strings.Format(DateTime.Today, "mm dd yy");
                    txtAddress1.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerAddress);
                    txtCity.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerCity);
                    txtState.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerState);
                    txtZip.Text = EWRRecord.EmployerZip;
                    txtEmail.Text = EWRRecord.TransmitterEmail;
                    txtEIN.Text = "";
                    txtName.Text = fecherFoundation.Strings.UCase(EWRRecord.EmployerName);
                    txtWithholdingAccount.Text = EWRRecord.MRSWithholdingID;
                    strTemp = EWRRecord.TransmitterPhone;
                    strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 3) + "-" +
                              Strings.Mid(strTemp, 7);
                    txtPhone.Text = strTemp;
                    Label4.Text = "SCHEDULE 1 (Form 941ME) " +
                                  FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                    intQuarterCovered = modCoreysSweeterCode.Statics.EWRWageTotals.intQuarter;
                    txtQuarterNum.Text = FCConvert.ToString(intQuarterCovered);
                    strTemp = Strings.Right(FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear), 2);
                    // vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string)
                    DateTime dtTemp = DateTime.Now;
                    switch (intQuarterCovered)
                    {
                        case 1:
                        {
                            txtPeriodStart.Text =
                                "01 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtPeriodEnd.Text =
                                "03 31  " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtMonthStart.Text = "01";
                            txtMonthEnd.Text = "03";
                            txtDayStart.Text = "01";
                            txtDayEnd.Text = "31";
                            dtStart = FCConvert.ToDateTime("1/1/" + strTemp);
                            dtEnd = FCConvert.ToDateTime("3/31/" + strTemp);
                            dtTemp = FCConvert.ToDateTime("05/01/" + strTemp);
                            // txtDueBy.Text = "04-" & Format(Month(DateAdd("d", -1, dtTemp)), "00")
                            txtDueMonth.Text = "04";
                            // txtDueYear.Text = strTemp
                            txtDueYear.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtDueDay.Text = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp).Day,
                                "00");
                            txtDueDate.Text =
                                "04 " +
                                Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp).Day, "00") + "  " +
                                FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            intMonthOne = 1;
                            break;
                        }
                        case 2:
                        {
                            txtPeriodStart.Text =
                                "04 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtPeriodEnd.Text =
                                "06 30  " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // txtDueBy.Text = "07-31-" & strTemp
                            txtDueMonth.Text = "07";
                            txtDueDay.Text = "31";
                            // txtDueYear.Text = strTemp
                            txtDueYear.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            intMonthOne = 4;
                            dtStart = FCConvert.ToDateTime("4/1/" + strTemp);
                            dtEnd = FCConvert.ToDateTime("6/30/" + strTemp);
                            txtMonthStart.Text = "04";
                            txtMonthEnd.Text = "06";
                            txtDayStart.Text = "01";
                            txtDayEnd.Text = "30";
                            txtDueDate.Text =
                                "07 " +
                                Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp).Day, "00") + "  " +
                                FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            break;
                        }
                        case 3:
                        {
                            txtPeriodStart.Text =
                                "07 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtPeriodEnd.Text =
                                "09 30  " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // txtDueBy.Text = "10-31-" & strTemp
                            txtDueMonth.Text = "10";
                            txtDueDay.Text = "31";
                            // txtDueYear.Text = strTemp
                            txtDueYear.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            intMonthOne = 7;
                            dtStart = FCConvert.ToDateTime("7/1/" + strTemp);
                            dtEnd = FCConvert.ToDateTime("9/30/" + strTemp);
                            txtMonthStart.Text = "07";
                            txtMonthEnd.Text = "09";
                            txtDayStart.Text = "01";
                            txtDayEnd.Text = "30";
                            txtDueDate.Text =
                                "10 " +
                                Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, dtTemp).Day, "00") + "  " +
                                FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            break;
                        }
                        case 4:
                        {
                            txtPeriodStart.Text =
                                "10 01 " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            txtPeriodEnd.Text =
                                "12 31  " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                            // txtDueBy.Text = "01-31-" & Format(Val(strTemp) + 1, "00")
                            txtDueMonth.Text = "01";
                            txtDueDay.Text = "31";
                            txtDueYear.Text = Strings.Format(Conversion.Val(strTemp) + 1, "00");
                            txtDueYear.Text =
                                FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear + 1);
                            intMonthOne = 10;
                            dtStart = FCConvert.ToDateTime("10/1/" + strTemp);
                            dtEnd = FCConvert.ToDateTime("12/31/" + strTemp);
                            txtMonthStart.Text = "10";
                            txtMonthEnd.Text = "12";
                            txtDayStart.Text = "01";
                            txtDayEnd.Text = "31";
                            txtDueDate.Text =
                                "01 31  " + FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear + 1);
                            break;
                        }
                    }

                    //end switch
                    txtPeriodStart2.Text = txtPeriodStart.Text;
                    txtPeriodEnd2.Text = txtPeriodEnd.Text;
                    txtYearStart.Text = strTemp;
                    txtYearEnd.Text = strTemp;
                    txtPeriodMonthStart.Text = txtMonthStart.Text;
                    txtPeriodDayStart.Text = txtDayStart.Text;
                    txtPeriodYearStart.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                    txtPeriodMonthEnd.Text = txtMonthEnd.Text;
                    txtPeriodDayEnd.Text = txtDayEnd.Text;
                    txtPeriodYearEnd.Text = FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                    // txtDateStart.Text = txtPeriodStart.Text
                    // txtDateEnd.Text = txtPeriodEnd.Text
                    txtName2.Text = txtName.Text;
                    txtWAccount2.Text = txtWithholdingAccount.Text;
                    txtMagnetic.Visible = false;
                    txtAmendedReturn.Visible = false;
                    txtCloseAccount.Visible = false;
                    txtEnclosingAmendedw3me.Visible = false;
                    txtOverpaymentCheckbox.Visible = false;
                    txtPayeeStatementsSent.Visible = false;
                    strSQL =
                        "select count(employeenumber) as thecount from (select employeenumber from tblcheckdetail where statetaxwh > 0 and paydate between '" +
                        FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) +
                        "' group by employeenumber) tbl1";
                    clsLoad.OpenRecordset(strSQL, "Payroll");
                    if (!clsLoad.EndOfFile())
                    {
                        txtNumberofPayees.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("thecount")));
                    }
                    else
                    {
                        txtNumberofPayees.Text = "0";
                    }

                    for (x = 1; x <= 63; x++)
                    {
                        (this.ReportFooter.Controls["txtDatePaid" + x] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                        // Me.ReportFooter.Controls("txtwithheld" & x).Text = ""
                        (this.ReportFooter.Controls["txtAmount" + x] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                    }

                    // x
                    // strSQL = "select * from tblStatePayments where paid > 0 order by datepaid"
                    strSQL = "select * from tblStatePayments where paid > 0 or withheld > 0 order by datepaid";
                    clsLoad.OpenRecordset(strSQL, "Payroll");
                    x = 1;
                    while (!clsLoad.EndOfFile())
                    {
                        if (x > 63)
                            break;
                        if (Convert.ToDateTime(clsLoad.Get_Fields("datepaid")).Month > intMonthOne)
                        {
                            if (Convert.ToDateTime(clsLoad.Get_Fields("datepaid")).Month == intMonthOne + 1)
                            {
                                if (x < 22)
                                    x = 22;
                            }
                            else
                            {
                                if (x < 43)
                                    x = 43;
                            }
                        }

                        (this.ReportFooter.Controls["txtDatePaid" + x] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            Strings.Format(clsLoad.Get_Fields("datepaid"), "mm dd yy");
                        // Me.ReportFooter.Controls("txtwithheld" & x).Text = Format(Val(clsLoad.Fields("withheld")), "0.00")
                        (this.ReportFooter.Controls["txtAmount" + x] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            Strings.Format(Conversion.Val(clsLoad.Get_Fields("paid")), "0.00");
                        dblTotalWithheld += Conversion.Val(clsLoad.Get_Fields("withheld"));
                        dblTotalPaid += Conversion.Val(clsLoad.Get_Fields("paid"));
                        if (x < 22)
                        {
                            dblWithheldSubtotalA += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalA += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }
                        else if (x < 43)
                        {
                            dblWithheldSubtotalB += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalB += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }
                        else
                        {
                            dblWithheldSubtotalC += Conversion.Val(clsLoad.Get_Fields("withheld"));
                            dblPaymentSubtotalC += Conversion.Val(clsLoad.Get_Fields("paid"));
                        }

                        x += 1;
                        clsLoad.MoveNext();
                    }

                    // txtCheckMagneticTape.Text = ""
                    // Field28.Text = ""
                    // Field29.Text = ""
                    // Field30.Text = ""
                    // Field31.Text = ""
                    // Field37.Text = ""
                    // txtLastPayrollDate.Text = ""
                    // txtDateSold.Text = ""
                    // txtLine5.Text = Format(dblTotalWithheld, "0.00")
                    txtLine1.Text = Strings.Format(dblTotalWithheld, "0.00");
                    txtLine6.Text = Strings.Format(dblTotalPaid, "0.00");
                    txtLine2.Text = txtLine6.Text;
                    txtLine2b.Text = "0.00";
                    txtLine2c.Text = txtLine6.Text;
                    if (dblTotalWithheld - dblTotalPaid >= 0)
                    {
                        txtLine3a.Text = Strings.Format(dblTotalWithheld - dblTotalPaid, "0.00");
                        txtLine3b.Text = "";
                    }
                    else
                    {
                        txtLine3a.Text = "";
                        txtLine3b.Text = Strings.Format(dblTotalPaid - dblTotalWithheld, "0.00");
                    }

                    txtPaymentSub1.Text = Strings.Format(dblPaymentSubtotalA, "0.00");
                    // txtPaymentSubA.Text = Format(dblPaymentSubtotalA, "0.00")
                    // txtPaymentSubB.Text = Format(dblPaymentSubtotalB, "0.00")
                    if (dblPaymentSubtotalB > 0)
                    {
                        txtPaymentSub2.Text = Strings.Format(dblPaymentSubtotalB, "0.00");
                    }
                    else
                    {
                        txtPaymentSub2.Text = "";
                    }

                    // txtPaymentSubC.Text = Format(dblPaymentSubtotalC, "0.00")
                    if (dblPaymentSubtotalC > 0)
                    {
                        txtPaymentSub3.Text = Strings.Format(dblPaymentSubtotalC, "0.00");
                    }
                    else
                    {
                        txtPaymentSub3.Text = "";
                    }

                    // txtWithheldSubA.Text = Format(dblWithheldSubtotalA, "0.00")
                    // txtWithheldSub1.Text = Format(dblWithheldSubtotalA, "0.00")
                    // txtWithheldSubB.Text = Format(dblWithheldSubtotalB, "0.00")
                    // If dblWithheldSubtotalB > 0 Then
                    // txtWithheldSub2.Text = Format(dblWithheldSubtotalB, "0.00")
                    // Else
                    // txtWithheldSub2.Text = ""
                    // End If
                    // txtWithheldSubC.Text = Format(dblWithheldSubtotalC, "0.00")
                    // If dblWithheldSubtotalC > 0 Then
                    // txtWithheldSub3.Text = Format(dblWithheldSubtotalC, "0.00")
                    // Else
                    // txtWithheldSub3.Text = ""
                    // End If
                    // 
                    // save the info so it can be used by other subreports
                    modCoreysSweeterCode.Statics.EWRWageTotals.dblTotalPayments = dblTotalPaid;
                }
                else
                {
                    Label4.Text = "SCHEDULE 1 (Form 941ME) " +
                                  FCConvert.ToString(modCoreysSweeterCode.Statics.EWRWageTotals.lngYear);
                }
            }
        }

		
	}
}
