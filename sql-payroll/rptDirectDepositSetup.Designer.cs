﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDirectDepositSetup.
	/// </summary>
	partial class rptDirectDepositSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDirectDepositSetup));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.BankNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBankName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccountType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.BankNumData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BankNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BankNumData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee,
				this.txtBankName,
				this.txtAccount,
				this.txtAmount,
				this.txtAmountType,
				this.txtAccountType,
				this.BankNumData
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.Label4,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.Line12,
				this.Label15,
				this.Label16,
				this.Label17,
				this.BankNum,
				this.Label9
			});
			this.PageHeader.Height = 0.7708333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.666667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label3.Text = "Account #";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.78125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 8.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label4.Text = "Amount";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 0.75F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.708333F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "DIRECT DEPOSIT SETUP REPORT";
			this.Label1.Top = 0.08333334F;
			this.Label1.Width = 5.15625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.04166667F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2.6875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 8.96875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.96875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.04166667F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0.1333333F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.7666667F;
			this.Line12.Width = 10.26667F;
			this.Line12.X1 = 0.1333333F;
			this.Line12.X2 = 10.4F;
			this.Line12.Y1 = 0.7666667F;
			this.Line12.Y2 = 0.7666667F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.04166667F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label15.Text = "Employee";
			this.Label15.Top = 0.5625F;
			this.Label15.Width = 1.0625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 9.302083F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.Label16.Text = "Amount Type";
			this.Label16.Top = 0.5625F;
			this.Label16.Width = 1.114583F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.770833F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.Label17.Text = "Account Type";
			this.Label17.Top = 0.5625F;
			this.Label17.Width = 1.229167F;
			// 
			// BankNum
			// 
			this.BankNum.Height = 0.1875F;
			this.BankNum.HyperLink = null;
			this.BankNum.Left = 2.479167F;
			this.BankNum.Name = "BankNum";
			this.BankNum.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.BankNum.Text = "Bank#";
			this.BankNum.Top = 0.5625F;
			this.BankNum.Width = 0.5833333F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label9.Text = "Bank Name ";
			this.Label9.Top = 0.5625F;
			this.Label9.Width = 1F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.1875F;
			this.txtEmployee.Left = 0.04166667F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtEmployee.Text = null;
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 2.375F;
			// 
			// txtBankName
			// 
			this.txtBankName.Height = 0.1875F;
			this.txtBankName.Left = 3.0625F;
			this.txtBankName.Name = "txtBankName";
			this.txtBankName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtBankName.Text = null;
			this.txtBankName.Top = 0F;
			this.txtBankName.Width = 2.541667F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 5.666667F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.03125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 8.28125F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.96875F;
			// 
			// txtAmountType
			// 
			this.txtAmountType.Height = 0.1875F;
			this.txtAmountType.Left = 9.302083F;
			this.txtAmountType.Name = "txtAmountType";
			this.txtAmountType.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtAmountType.Text = null;
			this.txtAmountType.Top = 0F;
			this.txtAmountType.Width = 1.104167F;
			// 
			// txtAccountType
			// 
			this.txtAccountType.Height = 0.1875F;
			this.txtAccountType.Left = 6.770833F;
			this.txtAccountType.Name = "txtAccountType";
			this.txtAccountType.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtAccountType.Text = null;
			this.txtAccountType.Top = 0F;
			this.txtAccountType.Width = 1.427083F;
			// 
			// BankNumData
			// 
			this.BankNumData.Height = 0.1875F;
			this.BankNumData.Left = 2.479167F;
			this.BankNumData.Name = "BankNumData";
			this.BankNumData.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.BankNumData.Text = null;
			this.BankNumData.Top = 0F;
			this.BankNumData.Width = 0.5F;
			// 
			// rptDirectDepositSetup
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.4375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BankNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BankNumData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBankName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox BankNumData;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label BankNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
