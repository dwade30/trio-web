﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCustomMatchReports.
	/// </summary>
	partial class frmCustomMatchReports
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCComboBox cmbReportOn;
		public fecherFoundation.FCLabel lblReportOn;
		public fecherFoundation.FCFrame fraSummary;
		public fecherFoundation.FCFrame Frame2;
		public T2KDateBox txtStartDate;
		public T2KDateBox txtEndDate;
		public fecherFoundation.FCTextBox txtEmployeeBoth;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
		public fecherFoundation.FCLabel lblCaption;
		public fecherFoundation.FCLabel lblDash;
		public fecherFoundation.FCLabel lblEmployeeBoth;
		public fecherFoundation.FCListBox lstDeductions;
		public fecherFoundation.FCFrame fraSelect;
		public fecherFoundation.FCCheckBox ChkAll;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbType = new fecherFoundation.FCComboBox();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.cmbReportOn = new fecherFoundation.FCComboBox();
            this.lblReportOn = new fecherFoundation.FCLabel();
            this.fraSummary = new fecherFoundation.FCFrame();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtStartDate = new T2KDateBox();
            this.txtEndDate = new T2KDateBox();
            this.txtEmployeeBoth = new fecherFoundation.FCTextBox();
            this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
            this.lblCaption = new fecherFoundation.FCLabel();
            this.lblDash = new fecherFoundation.FCLabel();
            this.lblEmployeeBoth = new fecherFoundation.FCLabel();
            this.lstDeductions = new fecherFoundation.FCListBox();
            this.fraSelect = new fecherFoundation.FCFrame();
            this.ChkAll = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelect)).BeginInit();
            this.fraSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(671, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Controls.Add(this.cmbReportOn);
            this.ClientArea.Controls.Add(this.lblReportOn);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.lstDeductions);
            this.ClientArea.Controls.Add(this.fraSelect);
            this.ClientArea.Size = new System.Drawing.Size(671, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(671, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(247, 30);
            this.HeaderText.Text = "Employers Match Rpt";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Detail",
            "Summary",
            "Emp Match Setup Information"});
            this.cmbType.Location = new System.Drawing.Point(20, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(312, 40);
            this.cmbType.TabIndex = 1;
            // 
            // cmbSort
            // 
            this.cmbSort.Items.AddRange(new object[] {
            "Employee Number",
            "Employee Name",
            "Deduction Number",
            "Pay Date"});
            this.cmbSort.Location = new System.Drawing.Point(207, 90);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(245, 40);
            this.cmbSort.TabIndex = 22;
            this.cmbSort.Text = "Employee Number";
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 104);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(127, 15);
            this.lblSort.TabIndex = 23;
            this.lblSort.Text = "GROUP REPORT BY";
            // 
            // cmbReportOn
            // 
            this.cmbReportOn.Items.AddRange(new object[] {
            "Single Employee",
            "Emp / Date Range",
            "Date Range",
            "All Files"});
            this.cmbReportOn.Location = new System.Drawing.Point(207, 30);
            this.cmbReportOn.Name = "cmbReportOn";
            this.cmbReportOn.Size = new System.Drawing.Size(245, 40);
            this.cmbReportOn.TabIndex = 24;
            this.cmbReportOn.Text = "Single Employee";
            this.cmbReportOn.SelectedIndexChanged += new System.EventHandler(this.optReportOn_CheckedChanged);
            // 
            // lblReportOn
            // 
            this.lblReportOn.AutoSize = true;
            this.lblReportOn.Location = new System.Drawing.Point(30, 44);
            this.lblReportOn.Name = "lblReportOn";
            this.lblReportOn.Size = new System.Drawing.Size(120, 15);
            this.lblReportOn.TabIndex = 25;
            this.lblReportOn.Text = "REPORT CRITERIA";
            // 
            // fraSummary
            // 
            this.fraSummary.Controls.Add(this.cmbType);
            this.fraSummary.Location = new System.Drawing.Point(283, 260);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(352, 90);
            this.fraSummary.TabIndex = 21;
            this.fraSummary.Text = "Type Of Report";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtStartDate);
            this.Frame2.Controls.Add(this.txtEndDate);
            this.Frame2.Controls.Add(this.txtEmployeeBoth);
            this.Frame2.Controls.Add(this.txtEmployeeNumber);
            this.Frame2.Controls.Add(this.lblCaption);
            this.Frame2.Controls.Add(this.lblDash);
            this.Frame2.Controls.Add(this.lblEmployeeBoth);
            this.Frame2.Location = new System.Drawing.Point(30, 150);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(605, 90);
            this.Frame2.TabIndex = 15;
            this.Frame2.Text = "Parameters";
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartDate.Location = new System.Drawing.Point(302, 30);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(120, 40);
            this.txtStartDate.TabIndex = 10;
            this.txtStartDate.Visible = false;
            //this.txtStartDate.Enter += new System.EventHandler(this.txtStartDate_Enter);
            //this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_TextChanged);
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEndDate.Location = new System.Drawing.Point(465, 30);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(120, 40);
            this.txtEndDate.TabIndex = 11;
            this.txtEndDate.Visible = false;
            //this.txtEndDate.Enter += new System.EventHandler(this.txtEndDate_Enter);
            //this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_TextChanged);
            // 
            // txtEmployeeBoth
            // 
            this.txtEmployeeBoth.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeBoth.Location = new System.Drawing.Point(202, 30);
            this.txtEmployeeBoth.Name = "txtEmployeeBoth";
            this.txtEmployeeBoth.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeBoth.TabIndex = 9;
            this.txtEmployeeBoth.Visible = false;
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeNumber.Location = new System.Drawing.Point(202, 30);
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Size = new System.Drawing.Size(80, 40);
            this.txtEmployeeNumber.TabIndex = 8;
            // 
            // lblCaption
            // 
            this.lblCaption.Location = new System.Drawing.Point(20, 44);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(143, 26);
            this.lblCaption.TabIndex = 18;
            this.lblCaption.Text = "EMPLOYEE NUMBER";
            // 
            // lblDash
            // 
            this.lblDash.Location = new System.Drawing.Point(442, 44);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(3, 15);
            this.lblDash.TabIndex = 17;
            this.lblDash.Text = "-";
            this.lblDash.Visible = false;
            // 
            // lblEmployeeBoth
            // 
            this.lblEmployeeBoth.Location = new System.Drawing.Point(20, 44);
            this.lblEmployeeBoth.Name = "lblEmployeeBoth";
            this.lblEmployeeBoth.Size = new System.Drawing.Size(37, 15);
            this.lblEmployeeBoth.TabIndex = 16;
            this.lblEmployeeBoth.Text = "EMP";
            this.lblEmployeeBoth.Visible = false;
            // 
            // lstDeductions
            // 
            this.lstDeductions.BackColor = System.Drawing.SystemColors.Window;
            this.lstDeductions.CheckBoxes = true;
            this.lstDeductions.Location = new System.Drawing.Point(30, 370);
            this.lstDeductions.Name = "lstDeductions";
            this.lstDeductions.Size = new System.Drawing.Size(422, 202);
            this.lstDeductions.Style = 1;
            this.lstDeductions.TabIndex = 13;
            // 
            // fraSelect
            // 
            this.fraSelect.Controls.Add(this.ChkAll);
            this.fraSelect.Location = new System.Drawing.Point(30, 260);
            this.fraSelect.Name = "fraSelect";
            this.fraSelect.Size = new System.Drawing.Size(223, 90);
            this.fraSelect.TabIndex = 14;
            this.fraSelect.Text = "Select";
            // 
            // ChkAll
            // 
            this.ChkAll.Location = new System.Drawing.Point(20, 30);
            this.ChkAll.Name = "ChkAll";
            this.ChkAll.Size = new System.Drawing.Size(183, 27);
            this.ChkAll.TabIndex = 12;
            this.ChkAll.Text = "Select All Deductions";
            this.ChkAll.CheckedChanged += new System.EventHandler(this.ChkAll_CheckedChanged);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Print / Preview";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(256, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(154, 48);
            this.cmdSaveContinue.Text = "Print / Preview";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // frmCustomMatchReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(671, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomMatchReports";
            this.ShowInTaskbar = false;
            this.Text = "Employers Match Rpt";
            this.Load += new System.EventHandler(this.frmCustomMatchReports_Load);
            this.Activated += new System.EventHandler(this.frmCustomMatchReports_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomMatchReports_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelect)).EndInit();
            this.fraSelect.ResumeLayout(false);
            this.fraSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSaveContinue;
    }
}
