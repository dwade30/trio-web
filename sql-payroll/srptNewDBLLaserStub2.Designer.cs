﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewDBLLaserStub2.
	/// </summary>
	partial class srptNewDBLLaserStub2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptNewDBLLaserStub2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtEmployeeNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentState = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDFICA = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDState = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtVacationAccrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSickAccrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode1Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode2Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode3Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode4Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDFedTaxable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDFicaTaxable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDStateTaxable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtVacationUsed = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSickTaken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode1Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode2Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode3Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode4Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtVacationBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSickBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode1Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode3Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode4Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCurrentMedicare = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDMedicare = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDMedicareTaxable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPayDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode2Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTotalYTDDeductions = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label206 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label209 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label210 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label223 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label224 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label226 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label227 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label230 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblCode5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode5Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode6Accrued = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode5Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode6Taken = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode5Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode6Balance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDNet = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYTDGross = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDirectDepositLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDirectDeposit = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCheckAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDirectDepChkLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDirectDepSav = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDirectDepositSavLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblDepartmentLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDepartment = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBankAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBank6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayDesc12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHours12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPayAmount12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedDesc20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedAmount20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDedYTD20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenefit12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenAmount12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBenYTD12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEMatchCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmatchYTD = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label225 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lineDirectDepositBottom = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lineDirectDepositLeft = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lineDirectDepositRight = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblIndRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRate12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationAccrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickAccrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFedTaxable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFicaTaxable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDStateTaxable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedicare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDMedicare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDMedicareTaxable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalYTDDeductions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label206)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Accrued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Taken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Balance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartmentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIndRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployeeNo,
            this.txtName,
            this.txtCheckNo,
            this.txtPay,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.txtCurrentGross,
            this.txtCurrentDeductions,
            this.txtCurrentNet,
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtCurrentFed,
            this.txtCurrentFica,
            this.txtCurrentState,
            this.txtPayDesc1,
            this.txtHours1,
            this.txtPayAmount1,
            this.txtDedDesc1,
            this.txtDedAmount1,
            this.txtDedYTD1,
            this.txtYTDFed,
            this.txtYTDFICA,
            this.txtYTDState,
            this.Label13,
            this.Label14,
            this.txtVacationAccrued,
            this.txtSickAccrued,
            this.lblPayRate,
            this.lblCode1,
            this.lblCode1Accrued,
            this.txtDate,
            this.lblCode2,
            this.lblCode3,
            this.lblCode2Accrued,
            this.lblCode3Accrued,
            this.lblCode4,
            this.lblCode4Accrued,
            this.Label30,
            this.Label31,
            this.Label32,
            this.txtYTDFedTaxable,
            this.txtYTDFicaTaxable,
            this.txtYTDStateTaxable,
            this.Label39,
            this.Label40,
            this.txtVacationUsed,
            this.txtSickTaken,
            this.lblCode1Taken,
            this.lblCode2Taken,
            this.lblCode3Taken,
            this.lblCode4Taken,
            this.txtVacationBalance,
            this.txtSickBalance,
            this.lblCode1Balance,
            this.lblCode3Balance,
            this.lblCode4Balance,
            this.Label53,
            this.Label57,
            this.txtCurrentMedicare,
            this.txtYTDMedicare,
            this.txtYTDMedicareTaxable,
            this.Line2,
            this.txtPayDesc2,
            this.txtHours2,
            this.txtPayAmount2,
            this.txtPayDesc3,
            this.txtHours3,
            this.txtPayAmount3,
            this.txtPayDesc4,
            this.txtHours4,
            this.txtPayAmount4,
            this.txtPayDesc5,
            this.txtHours5,
            this.txtPayAmount5,
            this.txtPayDesc6,
            this.txtHours6,
            this.txtPayAmount6,
            this.txtDedDesc2,
            this.txtDedAmount2,
            this.txtDedYTD2,
            this.txtDedDesc3,
            this.txtDedAmount3,
            this.txtDedYTD3,
            this.txtDedDesc4,
            this.txtDedAmount4,
            this.txtDedYTD4,
            this.txtDedDesc5,
            this.txtDedAmount5,
            this.txtDedYTD5,
            this.txtDedDesc6,
            this.txtDedAmount6,
            this.txtDedYTD6,
            this.txtDedDesc7,
            this.txtDedAmount7,
            this.txtDedYTD7,
            this.txtDedDesc8,
            this.txtDedAmount8,
            this.txtDedYTD8,
            this.txtDedDesc9,
            this.txtDedAmount9,
            this.txtDedYTD9,
            this.txtDedDesc10,
            this.txtDedAmount10,
            this.txtDedYTD10,
            this.txtDedDesc11,
            this.txtDedAmount11,
            this.txtDedYTD11,
            this.txtDedDesc12,
            this.txtDedAmount12,
            this.txtDedYTD12,
            this.txtDedDesc13,
            this.txtDedAmount13,
            this.txtDedYTD13,
            this.txtDedDesc14,
            this.txtDedAmount14,
            this.txtDedYTD14,
            this.txtDedDesc15,
            this.txtDedAmount15,
            this.txtDedYTD15,
            this.txtDedDesc16,
            this.txtDedAmount16,
            this.txtDedYTD16,
            this.txtTotalHours,
            this.txtTotalAmount,
            this.lblCheckMessage,
            this.lblCode2Balance,
            this.Line3,
            this.txtTotalYTDDeductions,
            this.Line4,
            this.Label206,
            this.Line5,
            this.Label209,
            this.Label210,
            this.txtPayDesc7,
            this.txtHours7,
            this.txtPayAmount7,
            this.txtPayDesc8,
            this.txtHours8,
            this.txtPayAmount8,
            this.txtPayDesc9,
            this.txtHours9,
            this.txtPayAmount9,
            this.txtPayDesc10,
            this.txtHours10,
            this.txtPayAmount10,
            this.txtPayRate,
            this.Label223,
            this.Line10,
            this.Label224,
            this.Line11,
            this.Label226,
            this.Label227,
            this.Line12,
            this.Label230,
            this.Line13,
            this.lblCode5,
            this.lblCode5Accrued,
            this.lblCode6,
            this.lblCode6Accrued,
            this.lblCode5Taken,
            this.lblCode6Taken,
            this.lblCode5Balance,
            this.lblCode6Balance,
            this.txtYTDNet,
            this.txtYTDGross,
            this.txtDirectDepositLabel,
            this.txtDirectDeposit,
            this.txtChkAmount,
            this.lblCheckAmount,
            this.txtDirectDepChkLabel,
            this.txtDirectDepSav,
            this.txtDirectDepositSavLabel,
            this.txtBankAmount1,
            this.txtBankAmount3,
            this.txtBank3,
            this.txtBank1,
            this.txtBankAmount2,
            this.txtBank2,
            this.Line9,
            this.lblDepartmentLabel,
            this.txtDepartment,
            this.txtBankAmount5,
            this.txtBank5,
            this.txtBankAmount4,
            this.txtBank4,
            this.txtBankAmount7,
            this.txtBank7,
            this.txtBankAmount6,
            this.txtBank6,
            this.txtPayDesc11,
            this.txtHours11,
            this.txtPayAmount11,
            this.txtPayDesc12,
            this.txtHours12,
            this.txtPayAmount12,
            this.txtDedDesc17,
            this.txtDedAmount17,
            this.txtDedYTD17,
            this.txtDedDesc18,
            this.txtDedAmount18,
            this.txtDedYTD18,
            this.txtDedDesc19,
            this.txtDedAmount19,
            this.txtDedYTD19,
            this.txtDedDesc20,
            this.txtDedAmount20,
            this.txtDedYTD20,
            this.Label141,
            this.Label142,
            this.Label143,
            this.lblBenefit1,
            this.lblBenAmount1,
            this.lblBenYTD1,
            this.lblBenefit2,
            this.lblBenAmount2,
            this.lblBenYTD2,
            this.lblBenefit3,
            this.lblBenAmount3,
            this.lblBenYTD3,
            this.lblBenefit4,
            this.lblBenAmount4,
            this.lblBenYTD4,
            this.lblBenefit5,
            this.lblBenAmount5,
            this.lblBenYTD5,
            this.lblBenefit6,
            this.lblBenAmount6,
            this.lblBenYTD6,
            this.lblBenefit7,
            this.lblBenAmount7,
            this.lblBenYTD7,
            this.lblBenefit8,
            this.lblBenAmount8,
            this.lblBenYTD8,
            this.lblBenefit9,
            this.lblBenAmount9,
            this.lblBenYTD9,
            this.lblBenefit10,
            this.lblBenAmount10,
            this.lblBenYTD10,
            this.lblBenefit11,
            this.lblBenAmount11,
            this.lblBenYTD11,
            this.lblBenefit12,
            this.lblBenAmount12,
            this.lblBenYTD12,
            this.txtEMatchCurrent,
            this.txtEmatchYTD,
            this.Line7,
            this.Line8,
            this.Label225,
            this.Line14,
            this.Line15,
            this.Line16,
            this.Line17,
            this.Line18,
            this.Line19,
            this.Line20,
            this.Line21,
            this.Line22,
            this.Line23,
            this.Line24,
            this.Line25,
            this.Line26,
            this.lineDirectDepositBottom,
            this.lineDirectDepositLeft,
            this.lineDirectDepositRight,
            this.lblIndRate,
            this.txtRate1,
            this.txtRate2,
            this.txtRate3,
            this.txtRate4,
            this.txtRate5,
            this.txtRate6,
            this.txtRate7,
            this.txtRate8,
            this.txtRate9,
            this.txtRate10,
            this.txtRate11,
            this.txtRate12,
            this.Line6});
            this.Detail.Height = 6.84375F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtEmployeeNo
            // 
            this.txtEmployeeNo.Height = 0.1354167F;
            this.txtEmployeeNo.HyperLink = null;
            this.txtEmployeeNo.Left = 0.9375F;
            this.txtEmployeeNo.Name = "txtEmployeeNo";
            this.txtEmployeeNo.Style = "text-align: left";
            this.txtEmployeeNo.Tag = "text";
            this.txtEmployeeNo.Text = "1234";
            this.txtEmployeeNo.Top = 0.59375F;
            this.txtEmployeeNo.Width = 2.135417F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1666667F;
            this.txtName.HyperLink = null;
            this.txtName.Left = 0.1041667F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "";
            this.txtName.Tag = "text";
            this.txtName.Text = "George Washington";
            this.txtName.Top = 0.40625F;
            this.txtName.Width = 3F;
            // 
            // txtCheckNo
            // 
            this.txtCheckNo.Height = 0.1666667F;
            this.txtCheckNo.HyperLink = null;
            this.txtCheckNo.Left = 2.177083F;
            this.txtCheckNo.Name = "txtCheckNo";
            this.txtCheckNo.Style = "font-size: 10pt; text-align: left";
            this.txtCheckNo.Tag = "text";
            this.txtCheckNo.Text = "1234";
            this.txtCheckNo.Top = 0.03125F;
            this.txtCheckNo.Width = 1.010417F;
            // 
            // txtPay
            // 
            this.txtPay.Height = 0.1666667F;
            this.txtPay.HyperLink = null;
            this.txtPay.Left = 3.5625F;
            this.txtPay.Name = "txtPay";
            this.txtPay.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.txtPay.Tag = "text";
            this.txtPay.Text = "Pay";
            this.txtPay.Top = 0.08333334F;
            this.txtPay.Width = 1F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1666667F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 5.958333F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label1.Tag = "text";
            this.Label1.Text = "Hours";
            this.Label1.Top = 0.08333334F;
            this.Label1.Width = 0.6875F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 6.645833F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label2.Tag = "text";
            this.Label2.Text = "Amount";
            this.Label2.Top = 0.08333334F;
            this.Label2.Width = 0.6875F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1666667F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 3.552083F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label3.Tag = "text";
            this.Label3.Text = "Deduction";
            this.Label3.Top = 2.229167F;
            this.Label3.Width = 1.4375F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1666667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 5.895833F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label4.Tag = "text";
            this.Label4.Text = "Amount";
            this.Label4.Top = 2.229167F;
            this.Label4.Width = 0.75F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1666667F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 6.645833F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label5.Tag = "text";
            this.Label5.Text = "YTD";
            this.Label5.Top = 2.229167F;
            this.Label5.Width = 0.6875F;
            // 
            // txtCurrentGross
            // 
            this.txtCurrentGross.Height = 0.1388889F;
            this.txtCurrentGross.HyperLink = null;
            this.txtCurrentGross.Left = 5.104167F;
            this.txtCurrentGross.Name = "txtCurrentGross";
            this.txtCurrentGross.Style = "font-size: 8.5pt; text-align: right";
            this.txtCurrentGross.Tag = "text";
            this.txtCurrentGross.Text = "0.00";
            this.txtCurrentGross.Top = 6.302083F;
            this.txtCurrentGross.Width = 0.6875F;
            // 
            // txtCurrentDeductions
            // 
            this.txtCurrentDeductions.Height = 0.1354167F;
            this.txtCurrentDeductions.HyperLink = null;
            this.txtCurrentDeductions.Left = 5.895833F;
            this.txtCurrentDeductions.Name = "txtCurrentDeductions";
            this.txtCurrentDeductions.Style = "text-align: right";
            this.txtCurrentDeductions.Tag = "text";
            this.txtCurrentDeductions.Text = "0.00";
            this.txtCurrentDeductions.Top = 5.072917F;
            this.txtCurrentDeductions.Width = 0.75F;
            // 
            // txtCurrentNet
            // 
            this.txtCurrentNet.Height = 0.1354167F;
            this.txtCurrentNet.HyperLink = null;
            this.txtCurrentNet.Left = 5.041667F;
            this.txtCurrentNet.Name = "txtCurrentNet";
            this.txtCurrentNet.Style = "font-size: 8.5pt; text-align: right";
            this.txtCurrentNet.Tag = "text";
            this.txtCurrentNet.Text = "0.00";
            this.txtCurrentNet.Top = 6.4375F;
            this.txtCurrentNet.Width = 0.75F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1388889F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 3.552083F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 8pt; text-align: left";
            this.Label18.Tag = "text";
            this.Label18.Text = "FIT";
            this.Label18.Top = 5.708333F;
            this.Label18.Width = 0.75F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1388889F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 3.552083F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 8pt; text-align: left";
            this.Label19.Tag = "text";
            this.Label19.Text = "FICA";
            this.Label19.Top = 5.833333F;
            this.Label19.Width = 0.75F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1388889F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 3.552083F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 8pt; text-align: left";
            this.Label20.Tag = "text";
            this.Label20.Text = "SWT-ME";
            this.Label20.Top = 6.083333F;
            this.Label20.Width = 0.75F;
            // 
            // txtCurrentFed
            // 
            this.txtCurrentFed.Height = 0.1388889F;
            this.txtCurrentFed.HyperLink = null;
            this.txtCurrentFed.Left = 5.041667F;
            this.txtCurrentFed.Name = "txtCurrentFed";
            this.txtCurrentFed.Style = "text-align: right";
            this.txtCurrentFed.Tag = "text";
            this.txtCurrentFed.Text = "0.00";
            this.txtCurrentFed.Top = 5.708333F;
            this.txtCurrentFed.Width = 0.75F;
            // 
            // txtCurrentFica
            // 
            this.txtCurrentFica.Height = 0.1354167F;
            this.txtCurrentFica.HyperLink = null;
            this.txtCurrentFica.Left = 5.041667F;
            this.txtCurrentFica.Name = "txtCurrentFica";
            this.txtCurrentFica.Style = "text-align: right";
            this.txtCurrentFica.Tag = "text";
            this.txtCurrentFica.Text = "0.00";
            this.txtCurrentFica.Top = 5.833333F;
            this.txtCurrentFica.Width = 0.75F;
            // 
            // txtCurrentState
            // 
            this.txtCurrentState.Height = 0.1354167F;
            this.txtCurrentState.HyperLink = null;
            this.txtCurrentState.Left = 5.041667F;
            this.txtCurrentState.Name = "txtCurrentState";
            this.txtCurrentState.Style = "text-align: right";
            this.txtCurrentState.Tag = "text";
            this.txtCurrentState.Text = "0.00";
            this.txtCurrentState.Top = 6.083333F;
            this.txtCurrentState.Width = 0.75F;
            // 
            // txtPayDesc1
            // 
            this.txtPayDesc1.Height = 0.1388889F;
            this.txtPayDesc1.HyperLink = null;
            this.txtPayDesc1.Left = 3.5625F;
            this.txtPayDesc1.Name = "txtPayDesc1";
            this.txtPayDesc1.Style = "text-align: left";
            this.txtPayDesc1.Tag = "text";
            this.txtPayDesc1.Text = "Regular";
            this.txtPayDesc1.Top = 0.25F;
            this.txtPayDesc1.Width = 1.489583F;
            // 
            // txtHours1
            // 
            this.txtHours1.Height = 0.1354167F;
            this.txtHours1.HyperLink = null;
            this.txtHours1.Left = 5.958333F;
            this.txtHours1.Name = "txtHours1";
            this.txtHours1.Style = "text-align: right";
            this.txtHours1.Tag = "text";
            this.txtHours1.Text = "0.00";
            this.txtHours1.Top = 0.25F;
            this.txtHours1.Width = 0.6875F;
            // 
            // txtPayAmount1
            // 
            this.txtPayAmount1.Height = 0.1354167F;
            this.txtPayAmount1.HyperLink = null;
            this.txtPayAmount1.Left = 6.645833F;
            this.txtPayAmount1.Name = "txtPayAmount1";
            this.txtPayAmount1.Style = "text-align: right";
            this.txtPayAmount1.Tag = "text";
            this.txtPayAmount1.Text = "0.00";
            this.txtPayAmount1.Top = 0.25F;
            this.txtPayAmount1.Width = 0.6875F;
            // 
            // txtDedDesc1
            // 
            this.txtDedDesc1.Height = 0.1388889F;
            this.txtDedDesc1.HyperLink = null;
            this.txtDedDesc1.Left = 3.552083F;
            this.txtDedDesc1.Name = "txtDedDesc1";
            this.txtDedDesc1.Style = "text-align: left";
            this.txtDedDesc1.Tag = "text";
            this.txtDedDesc1.Text = "Deduction Name";
            this.txtDedDesc1.Top = 2.458333F;
            this.txtDedDesc1.Width = 2.25F;
            // 
            // txtDedAmount1
            // 
            this.txtDedAmount1.Height = 0.1388889F;
            this.txtDedAmount1.HyperLink = null;
            this.txtDedAmount1.Left = 5.895833F;
            this.txtDedAmount1.Name = "txtDedAmount1";
            this.txtDedAmount1.Style = "text-align: right";
            this.txtDedAmount1.Tag = "text";
            this.txtDedAmount1.Text = "0.00";
            this.txtDedAmount1.Top = 2.458333F;
            this.txtDedAmount1.Width = 0.75F;
            // 
            // txtDedYTD1
            // 
            this.txtDedYTD1.Height = 0.1354167F;
            this.txtDedYTD1.HyperLink = null;
            this.txtDedYTD1.Left = 6.645833F;
            this.txtDedYTD1.Name = "txtDedYTD1";
            this.txtDedYTD1.Style = "text-align: right";
            this.txtDedYTD1.Tag = "text";
            this.txtDedYTD1.Text = "0.00";
            this.txtDedYTD1.Top = 2.458333F;
            this.txtDedYTD1.Width = 0.6875F;
            // 
            // txtYTDFed
            // 
            this.txtYTDFed.Height = 0.1388889F;
            this.txtYTDFed.HyperLink = null;
            this.txtYTDFed.Left = 5.802083F;
            this.txtYTDFed.Name = "txtYTDFed";
            this.txtYTDFed.Style = "text-align: right";
            this.txtYTDFed.Tag = "text";
            this.txtYTDFed.Text = "0.00";
            this.txtYTDFed.Top = 5.708333F;
            this.txtYTDFed.Width = 0.6875F;
            // 
            // txtYTDFICA
            // 
            this.txtYTDFICA.Height = 0.1354167F;
            this.txtYTDFICA.HyperLink = null;
            this.txtYTDFICA.Left = 5.802083F;
            this.txtYTDFICA.Name = "txtYTDFICA";
            this.txtYTDFICA.Style = "text-align: right";
            this.txtYTDFICA.Tag = "text";
            this.txtYTDFICA.Text = "0.00";
            this.txtYTDFICA.Top = 5.833333F;
            this.txtYTDFICA.Width = 0.6875F;
            // 
            // txtYTDState
            // 
            this.txtYTDState.Height = 0.1354167F;
            this.txtYTDState.HyperLink = null;
            this.txtYTDState.Left = 5.802083F;
            this.txtYTDState.Name = "txtYTDState";
            this.txtYTDState.Style = "text-align: right";
            this.txtYTDState.Tag = "text";
            this.txtYTDState.Text = "0.00";
            this.txtYTDState.Top = 6.083333F;
            this.txtYTDState.Width = 0.6875F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1388889F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0.1041667F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 8pt; text-align: left";
            this.Label13.Tag = "text";
            this.Label13.Text = "Vacation";
            this.Label13.Top = 4.270833F;
            this.Label13.Width = 1.197917F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1388889F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.1041667F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 8pt; text-align: left";
            this.Label14.Tag = "text";
            this.Label14.Text = "Sick";
            this.Label14.Top = 4.395833F;
            this.Label14.Width = 1.197917F;
            // 
            // txtVacationAccrued
            // 
            this.txtVacationAccrued.Height = 0.1388889F;
            this.txtVacationAccrued.HyperLink = null;
            this.txtVacationAccrued.Left = 1.333333F;
            this.txtVacationAccrued.Name = "txtVacationAccrued";
            this.txtVacationAccrued.Style = "text-align: right";
            this.txtVacationAccrued.Tag = "text";
            this.txtVacationAccrued.Text = "0.00";
            this.txtVacationAccrued.Top = 4.270833F;
            this.txtVacationAccrued.Width = 0.625F;
            // 
            // txtSickAccrued
            // 
            this.txtSickAccrued.Height = 0.1388889F;
            this.txtSickAccrued.HyperLink = null;
            this.txtSickAccrued.Left = 1.333333F;
            this.txtSickAccrued.Name = "txtSickAccrued";
            this.txtSickAccrued.Style = "text-align: right";
            this.txtSickAccrued.Tag = "text";
            this.txtSickAccrued.Text = "0.00";
            this.txtSickAccrued.Top = 4.395833F;
            this.txtSickAccrued.Width = 0.625F;
            // 
            // lblPayRate
            // 
            this.lblPayRate.Height = 0.1388889F;
            this.lblPayRate.HyperLink = null;
            this.lblPayRate.Left = 0.1041667F;
            this.lblPayRate.Name = "lblPayRate";
            this.lblPayRate.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.lblPayRate.Tag = "text";
            this.lblPayRate.Text = "Base Rate";
            this.lblPayRate.Top = 0.71875F;
            this.lblPayRate.Width = 0.75F;
            // 
            // lblCode1
            // 
            this.lblCode1.Height = 0.1388889F;
            this.lblCode1.HyperLink = null;
            this.lblCode1.Left = 0.1041667F;
            this.lblCode1.Name = "lblCode1";
            this.lblCode1.Style = "font-size: 8pt; text-align: left";
            this.lblCode1.Tag = "text";
            this.lblCode1.Text = "Personal";
            this.lblCode1.Top = 4.520833F;
            this.lblCode1.Width = 1.197917F;
            // 
            // lblCode1Accrued
            // 
            this.lblCode1Accrued.Height = 0.1388889F;
            this.lblCode1Accrued.HyperLink = null;
            this.lblCode1Accrued.Left = 1.333333F;
            this.lblCode1Accrued.Name = "lblCode1Accrued";
            this.lblCode1Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode1Accrued.Tag = "text";
            this.lblCode1Accrued.Text = "0.00";
            this.lblCode1Accrued.Top = 4.520833F;
            this.lblCode1Accrued.Width = 0.625F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1666667F;
            this.txtDate.HyperLink = null;
            this.txtDate.Left = 0.1666667F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-size: 10pt; text-align: left";
            this.txtDate.Tag = "text";
            this.txtDate.Text = "00/00/0000";
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 1.010417F;
            // 
            // lblCode2
            // 
            this.lblCode2.Height = 0.1388889F;
            this.lblCode2.HyperLink = null;
            this.lblCode2.Left = 0.1041667F;
            this.lblCode2.Name = "lblCode2";
            this.lblCode2.Style = "font-size: 8pt; text-align: left";
            this.lblCode2.Tag = "text";
            this.lblCode2.Text = "Comp Time";
            this.lblCode2.Top = 4.645833F;
            this.lblCode2.Width = 1.197917F;
            // 
            // lblCode3
            // 
            this.lblCode3.Height = 0.1388889F;
            this.lblCode3.HyperLink = null;
            this.lblCode3.Left = 0.1041667F;
            this.lblCode3.Name = "lblCode3";
            this.lblCode3.Style = "font-size: 8pt; text-align: left";
            this.lblCode3.Tag = "text";
            this.lblCode3.Text = "Code2";
            this.lblCode3.Top = 4.770833F;
            this.lblCode3.Width = 1.197917F;
            // 
            // lblCode2Accrued
            // 
            this.lblCode2Accrued.Height = 0.1388889F;
            this.lblCode2Accrued.HyperLink = null;
            this.lblCode2Accrued.Left = 1.333333F;
            this.lblCode2Accrued.Name = "lblCode2Accrued";
            this.lblCode2Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode2Accrued.Tag = "text";
            this.lblCode2Accrued.Text = "0.00";
            this.lblCode2Accrued.Top = 4.645833F;
            this.lblCode2Accrued.Width = 0.625F;
            // 
            // lblCode3Accrued
            // 
            this.lblCode3Accrued.Height = 0.1388889F;
            this.lblCode3Accrued.HyperLink = null;
            this.lblCode3Accrued.Left = 1.333333F;
            this.lblCode3Accrued.Name = "lblCode3Accrued";
            this.lblCode3Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode3Accrued.Tag = "text";
            this.lblCode3Accrued.Text = "0.00";
            this.lblCode3Accrued.Top = 4.770833F;
            this.lblCode3Accrued.Width = 0.625F;
            // 
            // lblCode4
            // 
            this.lblCode4.Height = 0.1388889F;
            this.lblCode4.HyperLink = null;
            this.lblCode4.Left = 0.1041667F;
            this.lblCode4.Name = "lblCode4";
            this.lblCode4.Style = "font-size: 8pt; text-align: left";
            this.lblCode4.Tag = "text";
            this.lblCode4.Text = "Code3";
            this.lblCode4.Top = 4.895833F;
            this.lblCode4.Width = 1.197917F;
            // 
            // lblCode4Accrued
            // 
            this.lblCode4Accrued.Height = 0.1388889F;
            this.lblCode4Accrued.HyperLink = null;
            this.lblCode4Accrued.Left = 1.333333F;
            this.lblCode4Accrued.Name = "lblCode4Accrued";
            this.lblCode4Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode4Accrued.Tag = "text";
            this.lblCode4Accrued.Text = "0.00";
            this.lblCode4Accrued.Top = 4.895833F;
            this.lblCode4Accrued.Width = 0.625F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1770833F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 5.041667F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label30.Text = "Amount";
            this.Label30.Top = 5.479167F;
            this.Label30.Width = 0.75F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1770833F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 5.510417F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label31.Text = "YTD";
            this.Label31.Top = 5.479167F;
            this.Label31.Width = 0.9791667F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.1770833F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 6.40625F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label32.Text = "YTD Taxable";
            this.Label32.Top = 5.479167F;
            this.Label32.Width = 0.9791667F;
            // 
            // txtYTDFedTaxable
            // 
            this.txtYTDFedTaxable.Height = 0.1388889F;
            this.txtYTDFedTaxable.HyperLink = null;
            this.txtYTDFedTaxable.Left = 6.583333F;
            this.txtYTDFedTaxable.Name = "txtYTDFedTaxable";
            this.txtYTDFedTaxable.Style = "text-align: right";
            this.txtYTDFedTaxable.Tag = "text";
            this.txtYTDFedTaxable.Text = "0.00";
            this.txtYTDFedTaxable.Top = 5.708333F;
            this.txtYTDFedTaxable.Width = 0.75F;
            // 
            // txtYTDFicaTaxable
            // 
            this.txtYTDFicaTaxable.Height = 0.1354167F;
            this.txtYTDFicaTaxable.HyperLink = null;
            this.txtYTDFicaTaxable.Left = 6.583333F;
            this.txtYTDFicaTaxable.Name = "txtYTDFicaTaxable";
            this.txtYTDFicaTaxable.Style = "text-align: right";
            this.txtYTDFicaTaxable.Tag = "text";
            this.txtYTDFicaTaxable.Text = "0.00";
            this.txtYTDFicaTaxable.Top = 5.833333F;
            this.txtYTDFicaTaxable.Width = 0.75F;
            // 
            // txtYTDStateTaxable
            // 
            this.txtYTDStateTaxable.Height = 0.1354167F;
            this.txtYTDStateTaxable.HyperLink = null;
            this.txtYTDStateTaxable.Left = 6.583333F;
            this.txtYTDStateTaxable.Name = "txtYTDStateTaxable";
            this.txtYTDStateTaxable.Style = "text-align: right";
            this.txtYTDStateTaxable.Tag = "text";
            this.txtYTDStateTaxable.Text = "0.00";
            this.txtYTDStateTaxable.Top = 6.083333F;
            this.txtYTDStateTaxable.Width = 0.75F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1770833F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 1.625F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label39.Text = "Taken";
            this.Label39.Top = 4.083333F;
            this.Label39.Width = 0.9791667F;
            // 
            // Label40
            // 
            this.Label40.Height = 0.1354167F;
            this.Label40.HyperLink = null;
            this.Label40.Left = 2.302083F;
            this.Label40.Name = "Label40";
            this.Label40.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label40.Text = "Balance";
            this.Label40.Top = 4.083333F;
            this.Label40.Width = 0.9791667F;
            // 
            // txtVacationUsed
            // 
            this.txtVacationUsed.Height = 0.1388889F;
            this.txtVacationUsed.HyperLink = null;
            this.txtVacationUsed.Left = 1.979167F;
            this.txtVacationUsed.Name = "txtVacationUsed";
            this.txtVacationUsed.Style = "text-align: right";
            this.txtVacationUsed.Tag = "text";
            this.txtVacationUsed.Text = "0.00";
            this.txtVacationUsed.Top = 4.270833F;
            this.txtVacationUsed.Width = 0.625F;
            // 
            // txtSickTaken
            // 
            this.txtSickTaken.Height = 0.1354167F;
            this.txtSickTaken.HyperLink = null;
            this.txtSickTaken.Left = 1.979167F;
            this.txtSickTaken.Name = "txtSickTaken";
            this.txtSickTaken.Style = "text-align: right";
            this.txtSickTaken.Tag = "text";
            this.txtSickTaken.Text = "0.00";
            this.txtSickTaken.Top = 4.395833F;
            this.txtSickTaken.Width = 0.625F;
            // 
            // lblCode1Taken
            // 
            this.lblCode1Taken.Height = 0.1354167F;
            this.lblCode1Taken.HyperLink = null;
            this.lblCode1Taken.Left = 1.979167F;
            this.lblCode1Taken.Name = "lblCode1Taken";
            this.lblCode1Taken.Style = "text-align: right";
            this.lblCode1Taken.Tag = "text";
            this.lblCode1Taken.Text = "0.00";
            this.lblCode1Taken.Top = 4.520833F;
            this.lblCode1Taken.Width = 0.625F;
            // 
            // lblCode2Taken
            // 
            this.lblCode2Taken.Height = 0.1354167F;
            this.lblCode2Taken.HyperLink = null;
            this.lblCode2Taken.Left = 1.979167F;
            this.lblCode2Taken.Name = "lblCode2Taken";
            this.lblCode2Taken.Style = "font-size: 8pt; text-align: right";
            this.lblCode2Taken.Tag = "text";
            this.lblCode2Taken.Text = "0.00";
            this.lblCode2Taken.Top = 4.645833F;
            this.lblCode2Taken.Width = 0.625F;
            // 
            // lblCode3Taken
            // 
            this.lblCode3Taken.Height = 0.1354167F;
            this.lblCode3Taken.HyperLink = null;
            this.lblCode3Taken.Left = 1.979167F;
            this.lblCode3Taken.Name = "lblCode3Taken";
            this.lblCode3Taken.Style = "font-size: 8pt; text-align: right";
            this.lblCode3Taken.Tag = "text";
            this.lblCode3Taken.Text = "0.00";
            this.lblCode3Taken.Top = 4.770833F;
            this.lblCode3Taken.Width = 0.625F;
            // 
            // lblCode4Taken
            // 
            this.lblCode4Taken.Height = 0.1354167F;
            this.lblCode4Taken.HyperLink = null;
            this.lblCode4Taken.Left = 1.979167F;
            this.lblCode4Taken.Name = "lblCode4Taken";
            this.lblCode4Taken.Style = "font-size: 8pt; text-align: right";
            this.lblCode4Taken.Tag = "text";
            this.lblCode4Taken.Text = "0.00";
            this.lblCode4Taken.Top = 4.895833F;
            this.lblCode4Taken.Width = 0.625F;
            // 
            // txtVacationBalance
            // 
            this.txtVacationBalance.Height = 0.1354167F;
            this.txtVacationBalance.HyperLink = null;
            this.txtVacationBalance.Left = 2.65625F;
            this.txtVacationBalance.Name = "txtVacationBalance";
            this.txtVacationBalance.Style = "text-align: right";
            this.txtVacationBalance.Tag = "text";
            this.txtVacationBalance.Text = "0.00";
            this.txtVacationBalance.Top = 4.270833F;
            this.txtVacationBalance.Width = 0.625F;
            // 
            // txtSickBalance
            // 
            this.txtSickBalance.Height = 0.1354167F;
            this.txtSickBalance.HyperLink = null;
            this.txtSickBalance.Left = 2.65625F;
            this.txtSickBalance.Name = "txtSickBalance";
            this.txtSickBalance.Style = "text-align: right";
            this.txtSickBalance.Tag = "text";
            this.txtSickBalance.Text = "0.00";
            this.txtSickBalance.Top = 4.395833F;
            this.txtSickBalance.Width = 0.625F;
            // 
            // lblCode1Balance
            // 
            this.lblCode1Balance.Height = 0.1354167F;
            this.lblCode1Balance.HyperLink = null;
            this.lblCode1Balance.Left = 2.65625F;
            this.lblCode1Balance.Name = "lblCode1Balance";
            this.lblCode1Balance.Style = "text-align: right";
            this.lblCode1Balance.Tag = "text";
            this.lblCode1Balance.Text = "0.00";
            this.lblCode1Balance.Top = 4.520833F;
            this.lblCode1Balance.Width = 0.625F;
            // 
            // lblCode3Balance
            // 
            this.lblCode3Balance.Height = 0.1354167F;
            this.lblCode3Balance.HyperLink = null;
            this.lblCode3Balance.Left = 2.65625F;
            this.lblCode3Balance.Name = "lblCode3Balance";
            this.lblCode3Balance.Style = "font-size: 8pt; text-align: right";
            this.lblCode3Balance.Tag = "text";
            this.lblCode3Balance.Text = "0.00";
            this.lblCode3Balance.Top = 4.770833F;
            this.lblCode3Balance.Width = 0.625F;
            // 
            // lblCode4Balance
            // 
            this.lblCode4Balance.Height = 0.1354167F;
            this.lblCode4Balance.HyperLink = null;
            this.lblCode4Balance.Left = 2.65625F;
            this.lblCode4Balance.Name = "lblCode4Balance";
            this.lblCode4Balance.Style = "font-size: 8pt; text-align: right";
            this.lblCode4Balance.Tag = "text";
            this.lblCode4Balance.Text = "0.00";
            this.lblCode4Balance.Top = 4.895833F;
            this.lblCode4Balance.Width = 0.625F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.1354167F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 1.395833F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.Label53.Text = "Accrued";
            this.Label53.Top = 4.083333F;
            this.Label53.Width = 0.5520833F;
            // 
            // Label57
            // 
            this.Label57.Height = 0.1388889F;
            this.Label57.HyperLink = null;
            this.Label57.Left = 3.552083F;
            this.Label57.Name = "Label57";
            this.Label57.Style = "font-size: 8pt; text-align: left";
            this.Label57.Tag = "text";
            this.Label57.Text = "MC";
            this.Label57.Top = 5.958333F;
            this.Label57.Width = 0.75F;
            // 
            // txtCurrentMedicare
            // 
            this.txtCurrentMedicare.Height = 0.1354167F;
            this.txtCurrentMedicare.HyperLink = null;
            this.txtCurrentMedicare.Left = 5.041667F;
            this.txtCurrentMedicare.Name = "txtCurrentMedicare";
            this.txtCurrentMedicare.Style = "text-align: right";
            this.txtCurrentMedicare.Tag = "text";
            this.txtCurrentMedicare.Text = "0.00";
            this.txtCurrentMedicare.Top = 5.958333F;
            this.txtCurrentMedicare.Width = 0.75F;
            // 
            // txtYTDMedicare
            // 
            this.txtYTDMedicare.Height = 0.1354167F;
            this.txtYTDMedicare.HyperLink = null;
            this.txtYTDMedicare.Left = 5.802083F;
            this.txtYTDMedicare.Name = "txtYTDMedicare";
            this.txtYTDMedicare.Style = "text-align: right";
            this.txtYTDMedicare.Tag = "text";
            this.txtYTDMedicare.Text = "0.00";
            this.txtYTDMedicare.Top = 5.958333F;
            this.txtYTDMedicare.Width = 0.6875F;
            // 
            // txtYTDMedicareTaxable
            // 
            this.txtYTDMedicareTaxable.Height = 0.1354167F;
            this.txtYTDMedicareTaxable.HyperLink = null;
            this.txtYTDMedicareTaxable.Left = 6.583333F;
            this.txtYTDMedicareTaxable.Name = "txtYTDMedicareTaxable";
            this.txtYTDMedicareTaxable.Style = "text-align: right";
            this.txtYTDMedicareTaxable.Tag = "text";
            this.txtYTDMedicareTaxable.Text = "0.00";
            this.txtYTDMedicareTaxable.Top = 5.958333F;
            this.txtYTDMedicareTaxable.Width = 0.75F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 3.520833F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 5.6875F;
            this.Line2.Width = 3.885417F;
            this.Line2.X1 = 3.520833F;
            this.Line2.X2 = 7.40625F;
            this.Line2.Y1 = 5.6875F;
            this.Line2.Y2 = 5.6875F;
            // 
            // txtPayDesc2
            // 
            this.txtPayDesc2.Height = 0.1388889F;
            this.txtPayDesc2.HyperLink = null;
            this.txtPayDesc2.Left = 3.5625F;
            this.txtPayDesc2.Name = "txtPayDesc2";
            this.txtPayDesc2.Style = "text-align: left";
            this.txtPayDesc2.Tag = "text";
            this.txtPayDesc2.Text = "Regular";
            this.txtPayDesc2.Top = 0.375F;
            this.txtPayDesc2.Width = 1.489583F;
            // 
            // txtHours2
            // 
            this.txtHours2.Height = 0.1354167F;
            this.txtHours2.HyperLink = null;
            this.txtHours2.Left = 5.958333F;
            this.txtHours2.Name = "txtHours2";
            this.txtHours2.Style = "text-align: right";
            this.txtHours2.Tag = "text";
            this.txtHours2.Text = "0.00";
            this.txtHours2.Top = 0.375F;
            this.txtHours2.Width = 0.6875F;
            // 
            // txtPayAmount2
            // 
            this.txtPayAmount2.Height = 0.1354167F;
            this.txtPayAmount2.HyperLink = null;
            this.txtPayAmount2.Left = 6.645833F;
            this.txtPayAmount2.Name = "txtPayAmount2";
            this.txtPayAmount2.Style = "text-align: right";
            this.txtPayAmount2.Tag = "text";
            this.txtPayAmount2.Text = "0.00";
            this.txtPayAmount2.Top = 0.375F;
            this.txtPayAmount2.Width = 0.6875F;
            // 
            // txtPayDesc3
            // 
            this.txtPayDesc3.Height = 0.1388889F;
            this.txtPayDesc3.HyperLink = null;
            this.txtPayDesc3.Left = 3.5625F;
            this.txtPayDesc3.Name = "txtPayDesc3";
            this.txtPayDesc3.Style = "text-align: left";
            this.txtPayDesc3.Tag = "text";
            this.txtPayDesc3.Text = "Regular";
            this.txtPayDesc3.Top = 0.5F;
            this.txtPayDesc3.Width = 1.489583F;
            // 
            // txtHours3
            // 
            this.txtHours3.Height = 0.1354167F;
            this.txtHours3.HyperLink = null;
            this.txtHours3.Left = 5.958333F;
            this.txtHours3.Name = "txtHours3";
            this.txtHours3.Style = "text-align: right";
            this.txtHours3.Tag = "text";
            this.txtHours3.Text = "0.00";
            this.txtHours3.Top = 0.5F;
            this.txtHours3.Width = 0.6875F;
            // 
            // txtPayAmount3
            // 
            this.txtPayAmount3.Height = 0.1354167F;
            this.txtPayAmount3.HyperLink = null;
            this.txtPayAmount3.Left = 6.645833F;
            this.txtPayAmount3.Name = "txtPayAmount3";
            this.txtPayAmount3.Style = "text-align: right";
            this.txtPayAmount3.Tag = "text";
            this.txtPayAmount3.Text = "0.00";
            this.txtPayAmount3.Top = 0.5F;
            this.txtPayAmount3.Width = 0.6875F;
            // 
            // txtPayDesc4
            // 
            this.txtPayDesc4.Height = 0.1388889F;
            this.txtPayDesc4.HyperLink = null;
            this.txtPayDesc4.Left = 3.5625F;
            this.txtPayDesc4.Name = "txtPayDesc4";
            this.txtPayDesc4.Style = "text-align: left";
            this.txtPayDesc4.Tag = "text";
            this.txtPayDesc4.Text = "Regular";
            this.txtPayDesc4.Top = 0.625F;
            this.txtPayDesc4.Width = 1.489583F;
            // 
            // txtHours4
            // 
            this.txtHours4.Height = 0.1354167F;
            this.txtHours4.HyperLink = null;
            this.txtHours4.Left = 5.958333F;
            this.txtHours4.Name = "txtHours4";
            this.txtHours4.Style = "text-align: right";
            this.txtHours4.Tag = "text";
            this.txtHours4.Text = "0.00";
            this.txtHours4.Top = 0.625F;
            this.txtHours4.Width = 0.6875F;
            // 
            // txtPayAmount4
            // 
            this.txtPayAmount4.Height = 0.1354167F;
            this.txtPayAmount4.HyperLink = null;
            this.txtPayAmount4.Left = 6.645833F;
            this.txtPayAmount4.Name = "txtPayAmount4";
            this.txtPayAmount4.Style = "text-align: right";
            this.txtPayAmount4.Tag = "text";
            this.txtPayAmount4.Text = "0.00";
            this.txtPayAmount4.Top = 0.625F;
            this.txtPayAmount4.Width = 0.6875F;
            // 
            // txtPayDesc5
            // 
            this.txtPayDesc5.Height = 0.1388889F;
            this.txtPayDesc5.HyperLink = null;
            this.txtPayDesc5.Left = 3.5625F;
            this.txtPayDesc5.Name = "txtPayDesc5";
            this.txtPayDesc5.Style = "text-align: left";
            this.txtPayDesc5.Tag = "text";
            this.txtPayDesc5.Text = "Regular";
            this.txtPayDesc5.Top = 0.75F;
            this.txtPayDesc5.Width = 1.489583F;
            // 
            // txtHours5
            // 
            this.txtHours5.Height = 0.1354167F;
            this.txtHours5.HyperLink = null;
            this.txtHours5.Left = 5.958333F;
            this.txtHours5.Name = "txtHours5";
            this.txtHours5.Style = "text-align: right";
            this.txtHours5.Tag = "text";
            this.txtHours5.Text = "0.00";
            this.txtHours5.Top = 0.75F;
            this.txtHours5.Width = 0.6875F;
            // 
            // txtPayAmount5
            // 
            this.txtPayAmount5.Height = 0.1354167F;
            this.txtPayAmount5.HyperLink = null;
            this.txtPayAmount5.Left = 6.645833F;
            this.txtPayAmount5.Name = "txtPayAmount5";
            this.txtPayAmount5.Style = "text-align: right";
            this.txtPayAmount5.Tag = "text";
            this.txtPayAmount5.Text = "0.00";
            this.txtPayAmount5.Top = 0.75F;
            this.txtPayAmount5.Width = 0.6875F;
            // 
            // txtPayDesc6
            // 
            this.txtPayDesc6.Height = 0.1388889F;
            this.txtPayDesc6.HyperLink = null;
            this.txtPayDesc6.Left = 3.5625F;
            this.txtPayDesc6.Name = "txtPayDesc6";
            this.txtPayDesc6.Style = "text-align: left";
            this.txtPayDesc6.Tag = "text";
            this.txtPayDesc6.Text = "Regular";
            this.txtPayDesc6.Top = 0.875F;
            this.txtPayDesc6.Width = 1.489583F;
            // 
            // txtHours6
            // 
            this.txtHours6.Height = 0.1354167F;
            this.txtHours6.HyperLink = null;
            this.txtHours6.Left = 5.958333F;
            this.txtHours6.Name = "txtHours6";
            this.txtHours6.Style = "text-align: right";
            this.txtHours6.Tag = "text";
            this.txtHours6.Text = "0.00";
            this.txtHours6.Top = 0.875F;
            this.txtHours6.Width = 0.6875F;
            // 
            // txtPayAmount6
            // 
            this.txtPayAmount6.Height = 0.1354167F;
            this.txtPayAmount6.HyperLink = null;
            this.txtPayAmount6.Left = 6.645833F;
            this.txtPayAmount6.Name = "txtPayAmount6";
            this.txtPayAmount6.Style = "text-align: right";
            this.txtPayAmount6.Tag = "text";
            this.txtPayAmount6.Text = "0.00";
            this.txtPayAmount6.Top = 0.875F;
            this.txtPayAmount6.Width = 0.6875F;
            // 
            // txtDedDesc2
            // 
            this.txtDedDesc2.Height = 0.1354167F;
            this.txtDedDesc2.HyperLink = null;
            this.txtDedDesc2.Left = 3.552083F;
            this.txtDedDesc2.Name = "txtDedDesc2";
            this.txtDedDesc2.Style = "text-align: left";
            this.txtDedDesc2.Tag = "text";
            this.txtDedDesc2.Text = "Deduction Name";
            this.txtDedDesc2.Top = 2.583333F;
            this.txtDedDesc2.Width = 2.25F;
            // 
            // txtDedAmount2
            // 
            this.txtDedAmount2.Height = 0.1388889F;
            this.txtDedAmount2.HyperLink = null;
            this.txtDedAmount2.Left = 5.895833F;
            this.txtDedAmount2.Name = "txtDedAmount2";
            this.txtDedAmount2.Style = "text-align: right";
            this.txtDedAmount2.Tag = "text";
            this.txtDedAmount2.Text = "0.00";
            this.txtDedAmount2.Top = 2.583333F;
            this.txtDedAmount2.Width = 0.75F;
            // 
            // txtDedYTD2
            // 
            this.txtDedYTD2.Height = 0.1354167F;
            this.txtDedYTD2.HyperLink = null;
            this.txtDedYTD2.Left = 6.645833F;
            this.txtDedYTD2.Name = "txtDedYTD2";
            this.txtDedYTD2.Style = "text-align: right";
            this.txtDedYTD2.Tag = "text";
            this.txtDedYTD2.Text = "0.00";
            this.txtDedYTD2.Top = 2.583333F;
            this.txtDedYTD2.Width = 0.6875F;
            // 
            // txtDedDesc3
            // 
            this.txtDedDesc3.Height = 0.1354167F;
            this.txtDedDesc3.HyperLink = null;
            this.txtDedDesc3.Left = 3.552083F;
            this.txtDedDesc3.Name = "txtDedDesc3";
            this.txtDedDesc3.Style = "text-align: left";
            this.txtDedDesc3.Tag = "text";
            this.txtDedDesc3.Text = "Deduction Name";
            this.txtDedDesc3.Top = 2.708333F;
            this.txtDedDesc3.Width = 2.25F;
            // 
            // txtDedAmount3
            // 
            this.txtDedAmount3.Height = 0.1388889F;
            this.txtDedAmount3.HyperLink = null;
            this.txtDedAmount3.Left = 5.895833F;
            this.txtDedAmount3.Name = "txtDedAmount3";
            this.txtDedAmount3.Style = "text-align: right";
            this.txtDedAmount3.Tag = "text";
            this.txtDedAmount3.Text = "0.00";
            this.txtDedAmount3.Top = 2.708333F;
            this.txtDedAmount3.Width = 0.75F;
            // 
            // txtDedYTD3
            // 
            this.txtDedYTD3.Height = 0.1354167F;
            this.txtDedYTD3.HyperLink = null;
            this.txtDedYTD3.Left = 6.645833F;
            this.txtDedYTD3.Name = "txtDedYTD3";
            this.txtDedYTD3.Style = "text-align: right";
            this.txtDedYTD3.Tag = "text";
            this.txtDedYTD3.Text = "0.00";
            this.txtDedYTD3.Top = 2.708333F;
            this.txtDedYTD3.Width = 0.6875F;
            // 
            // txtDedDesc4
            // 
            this.txtDedDesc4.Height = 0.1354167F;
            this.txtDedDesc4.HyperLink = null;
            this.txtDedDesc4.Left = 3.552083F;
            this.txtDedDesc4.Name = "txtDedDesc4";
            this.txtDedDesc4.Style = "text-align: left";
            this.txtDedDesc4.Tag = "text";
            this.txtDedDesc4.Text = "Deduction Name";
            this.txtDedDesc4.Top = 2.833333F;
            this.txtDedDesc4.Width = 2.25F;
            // 
            // txtDedAmount4
            // 
            this.txtDedAmount4.Height = 0.1388889F;
            this.txtDedAmount4.HyperLink = null;
            this.txtDedAmount4.Left = 5.895833F;
            this.txtDedAmount4.Name = "txtDedAmount4";
            this.txtDedAmount4.Style = "text-align: right";
            this.txtDedAmount4.Tag = "text";
            this.txtDedAmount4.Text = "0.00";
            this.txtDedAmount4.Top = 2.833333F;
            this.txtDedAmount4.Width = 0.75F;
            // 
            // txtDedYTD4
            // 
            this.txtDedYTD4.Height = 0.1354167F;
            this.txtDedYTD4.HyperLink = null;
            this.txtDedYTD4.Left = 6.645833F;
            this.txtDedYTD4.Name = "txtDedYTD4";
            this.txtDedYTD4.Style = "text-align: right";
            this.txtDedYTD4.Tag = "text";
            this.txtDedYTD4.Text = "0.00";
            this.txtDedYTD4.Top = 2.833333F;
            this.txtDedYTD4.Width = 0.6875F;
            // 
            // txtDedDesc5
            // 
            this.txtDedDesc5.Height = 0.1354167F;
            this.txtDedDesc5.HyperLink = null;
            this.txtDedDesc5.Left = 3.552083F;
            this.txtDedDesc5.Name = "txtDedDesc5";
            this.txtDedDesc5.Style = "text-align: left";
            this.txtDedDesc5.Tag = "text";
            this.txtDedDesc5.Text = "Deduction Name";
            this.txtDedDesc5.Top = 2.958333F;
            this.txtDedDesc5.Width = 2.25F;
            // 
            // txtDedAmount5
            // 
            this.txtDedAmount5.Height = 0.1388889F;
            this.txtDedAmount5.HyperLink = null;
            this.txtDedAmount5.Left = 5.895833F;
            this.txtDedAmount5.Name = "txtDedAmount5";
            this.txtDedAmount5.Style = "text-align: right";
            this.txtDedAmount5.Tag = "text";
            this.txtDedAmount5.Text = "0.00";
            this.txtDedAmount5.Top = 2.958333F;
            this.txtDedAmount5.Width = 0.75F;
            // 
            // txtDedYTD5
            // 
            this.txtDedYTD5.Height = 0.1354167F;
            this.txtDedYTD5.HyperLink = null;
            this.txtDedYTD5.Left = 6.645833F;
            this.txtDedYTD5.Name = "txtDedYTD5";
            this.txtDedYTD5.Style = "text-align: right";
            this.txtDedYTD5.Tag = "text";
            this.txtDedYTD5.Text = "0.00";
            this.txtDedYTD5.Top = 2.958333F;
            this.txtDedYTD5.Width = 0.6875F;
            // 
            // txtDedDesc6
            // 
            this.txtDedDesc6.Height = 0.1354167F;
            this.txtDedDesc6.HyperLink = null;
            this.txtDedDesc6.Left = 3.552083F;
            this.txtDedDesc6.Name = "txtDedDesc6";
            this.txtDedDesc6.Style = "text-align: left";
            this.txtDedDesc6.Tag = "text";
            this.txtDedDesc6.Text = "Deduction Name";
            this.txtDedDesc6.Top = 3.083333F;
            this.txtDedDesc6.Width = 2.25F;
            // 
            // txtDedAmount6
            // 
            this.txtDedAmount6.Height = 0.1388889F;
            this.txtDedAmount6.HyperLink = null;
            this.txtDedAmount6.Left = 5.895833F;
            this.txtDedAmount6.Name = "txtDedAmount6";
            this.txtDedAmount6.Style = "text-align: right";
            this.txtDedAmount6.Tag = "text";
            this.txtDedAmount6.Text = "0.00";
            this.txtDedAmount6.Top = 3.083333F;
            this.txtDedAmount6.Width = 0.75F;
            // 
            // txtDedYTD6
            // 
            this.txtDedYTD6.Height = 0.1354167F;
            this.txtDedYTD6.HyperLink = null;
            this.txtDedYTD6.Left = 6.645833F;
            this.txtDedYTD6.Name = "txtDedYTD6";
            this.txtDedYTD6.Style = "text-align: right";
            this.txtDedYTD6.Tag = "text";
            this.txtDedYTD6.Text = "0.00";
            this.txtDedYTD6.Top = 3.083333F;
            this.txtDedYTD6.Width = 0.6875F;
            // 
            // txtDedDesc7
            // 
            this.txtDedDesc7.Height = 0.1354167F;
            this.txtDedDesc7.HyperLink = null;
            this.txtDedDesc7.Left = 3.552083F;
            this.txtDedDesc7.Name = "txtDedDesc7";
            this.txtDedDesc7.Style = "text-align: left";
            this.txtDedDesc7.Tag = "text";
            this.txtDedDesc7.Text = "Deduction Name";
            this.txtDedDesc7.Top = 3.208333F;
            this.txtDedDesc7.Width = 2.25F;
            // 
            // txtDedAmount7
            // 
            this.txtDedAmount7.Height = 0.1388889F;
            this.txtDedAmount7.HyperLink = null;
            this.txtDedAmount7.Left = 5.895833F;
            this.txtDedAmount7.Name = "txtDedAmount7";
            this.txtDedAmount7.Style = "text-align: right";
            this.txtDedAmount7.Tag = "text";
            this.txtDedAmount7.Text = "0.00";
            this.txtDedAmount7.Top = 3.208333F;
            this.txtDedAmount7.Width = 0.75F;
            // 
            // txtDedYTD7
            // 
            this.txtDedYTD7.Height = 0.1354167F;
            this.txtDedYTD7.HyperLink = null;
            this.txtDedYTD7.Left = 6.645833F;
            this.txtDedYTD7.Name = "txtDedYTD7";
            this.txtDedYTD7.Style = "text-align: right";
            this.txtDedYTD7.Tag = "text";
            this.txtDedYTD7.Text = "0.00";
            this.txtDedYTD7.Top = 3.208333F;
            this.txtDedYTD7.Width = 0.6875F;
            // 
            // txtDedDesc8
            // 
            this.txtDedDesc8.Height = 0.1354167F;
            this.txtDedDesc8.HyperLink = null;
            this.txtDedDesc8.Left = 3.552083F;
            this.txtDedDesc8.Name = "txtDedDesc8";
            this.txtDedDesc8.Style = "text-align: left";
            this.txtDedDesc8.Tag = "text";
            this.txtDedDesc8.Text = "Deduction Name";
            this.txtDedDesc8.Top = 3.333333F;
            this.txtDedDesc8.Width = 2.25F;
            // 
            // txtDedAmount8
            // 
            this.txtDedAmount8.Height = 0.1388889F;
            this.txtDedAmount8.HyperLink = null;
            this.txtDedAmount8.Left = 5.895833F;
            this.txtDedAmount8.Name = "txtDedAmount8";
            this.txtDedAmount8.Style = "text-align: right";
            this.txtDedAmount8.Tag = "text";
            this.txtDedAmount8.Text = "0.00";
            this.txtDedAmount8.Top = 3.333333F;
            this.txtDedAmount8.Width = 0.75F;
            // 
            // txtDedYTD8
            // 
            this.txtDedYTD8.Height = 0.1354167F;
            this.txtDedYTD8.HyperLink = null;
            this.txtDedYTD8.Left = 6.645833F;
            this.txtDedYTD8.Name = "txtDedYTD8";
            this.txtDedYTD8.Style = "text-align: right";
            this.txtDedYTD8.Tag = "text";
            this.txtDedYTD8.Text = "0.00";
            this.txtDedYTD8.Top = 3.333333F;
            this.txtDedYTD8.Width = 0.6875F;
            // 
            // txtDedDesc9
            // 
            this.txtDedDesc9.Height = 0.1354167F;
            this.txtDedDesc9.HyperLink = null;
            this.txtDedDesc9.Left = 3.552083F;
            this.txtDedDesc9.Name = "txtDedDesc9";
            this.txtDedDesc9.Style = "text-align: left";
            this.txtDedDesc9.Tag = "text";
            this.txtDedDesc9.Text = "Deduction Name";
            this.txtDedDesc9.Top = 3.458333F;
            this.txtDedDesc9.Width = 2.25F;
            // 
            // txtDedAmount9
            // 
            this.txtDedAmount9.Height = 0.1388889F;
            this.txtDedAmount9.HyperLink = null;
            this.txtDedAmount9.Left = 5.895833F;
            this.txtDedAmount9.Name = "txtDedAmount9";
            this.txtDedAmount9.Style = "text-align: right";
            this.txtDedAmount9.Tag = "text";
            this.txtDedAmount9.Text = "0.00";
            this.txtDedAmount9.Top = 3.458333F;
            this.txtDedAmount9.Width = 0.75F;
            // 
            // txtDedYTD9
            // 
            this.txtDedYTD9.Height = 0.1354167F;
            this.txtDedYTD9.HyperLink = null;
            this.txtDedYTD9.Left = 6.645833F;
            this.txtDedYTD9.Name = "txtDedYTD9";
            this.txtDedYTD9.Style = "text-align: right";
            this.txtDedYTD9.Tag = "text";
            this.txtDedYTD9.Text = "0.00";
            this.txtDedYTD9.Top = 3.458333F;
            this.txtDedYTD9.Width = 0.6875F;
            // 
            // txtDedDesc10
            // 
            this.txtDedDesc10.Height = 0.1354167F;
            this.txtDedDesc10.HyperLink = null;
            this.txtDedDesc10.Left = 3.552083F;
            this.txtDedDesc10.Name = "txtDedDesc10";
            this.txtDedDesc10.Style = "text-align: left";
            this.txtDedDesc10.Tag = "text";
            this.txtDedDesc10.Text = "Deduction Name";
            this.txtDedDesc10.Top = 3.583333F;
            this.txtDedDesc10.Width = 2.25F;
            // 
            // txtDedAmount10
            // 
            this.txtDedAmount10.Height = 0.1388889F;
            this.txtDedAmount10.HyperLink = null;
            this.txtDedAmount10.Left = 5.895833F;
            this.txtDedAmount10.Name = "txtDedAmount10";
            this.txtDedAmount10.Style = "text-align: right";
            this.txtDedAmount10.Tag = "text";
            this.txtDedAmount10.Text = "0.00";
            this.txtDedAmount10.Top = 3.583333F;
            this.txtDedAmount10.Width = 0.75F;
            // 
            // txtDedYTD10
            // 
            this.txtDedYTD10.Height = 0.1354167F;
            this.txtDedYTD10.HyperLink = null;
            this.txtDedYTD10.Left = 6.645833F;
            this.txtDedYTD10.Name = "txtDedYTD10";
            this.txtDedYTD10.Style = "text-align: right";
            this.txtDedYTD10.Tag = "text";
            this.txtDedYTD10.Text = "0.00";
            this.txtDedYTD10.Top = 3.583333F;
            this.txtDedYTD10.Width = 0.6875F;
            // 
            // txtDedDesc11
            // 
            this.txtDedDesc11.Height = 0.1354167F;
            this.txtDedDesc11.HyperLink = null;
            this.txtDedDesc11.Left = 3.552083F;
            this.txtDedDesc11.Name = "txtDedDesc11";
            this.txtDedDesc11.Style = "text-align: left";
            this.txtDedDesc11.Tag = "text";
            this.txtDedDesc11.Text = "Deduction Name";
            this.txtDedDesc11.Top = 3.708333F;
            this.txtDedDesc11.Width = 2.25F;
            // 
            // txtDedAmount11
            // 
            this.txtDedAmount11.Height = 0.1388889F;
            this.txtDedAmount11.HyperLink = null;
            this.txtDedAmount11.Left = 5.895833F;
            this.txtDedAmount11.Name = "txtDedAmount11";
            this.txtDedAmount11.Style = "text-align: right";
            this.txtDedAmount11.Tag = "text";
            this.txtDedAmount11.Text = "0.00";
            this.txtDedAmount11.Top = 3.708333F;
            this.txtDedAmount11.Width = 0.75F;
            // 
            // txtDedYTD11
            // 
            this.txtDedYTD11.Height = 0.1354167F;
            this.txtDedYTD11.HyperLink = null;
            this.txtDedYTD11.Left = 6.645833F;
            this.txtDedYTD11.Name = "txtDedYTD11";
            this.txtDedYTD11.Style = "text-align: right";
            this.txtDedYTD11.Tag = "text";
            this.txtDedYTD11.Text = "0.00";
            this.txtDedYTD11.Top = 3.708333F;
            this.txtDedYTD11.Width = 0.6875F;
            // 
            // txtDedDesc12
            // 
            this.txtDedDesc12.Height = 0.1354167F;
            this.txtDedDesc12.HyperLink = null;
            this.txtDedDesc12.Left = 3.552083F;
            this.txtDedDesc12.Name = "txtDedDesc12";
            this.txtDedDesc12.Style = "text-align: left";
            this.txtDedDesc12.Tag = "text";
            this.txtDedDesc12.Text = "Deduction Name";
            this.txtDedDesc12.Top = 3.833333F;
            this.txtDedDesc12.Width = 2.25F;
            // 
            // txtDedAmount12
            // 
            this.txtDedAmount12.Height = 0.1388889F;
            this.txtDedAmount12.HyperLink = null;
            this.txtDedAmount12.Left = 5.895833F;
            this.txtDedAmount12.Name = "txtDedAmount12";
            this.txtDedAmount12.Style = "text-align: right";
            this.txtDedAmount12.Tag = "text";
            this.txtDedAmount12.Text = "0.00";
            this.txtDedAmount12.Top = 3.833333F;
            this.txtDedAmount12.Width = 0.75F;
            // 
            // txtDedYTD12
            // 
            this.txtDedYTD12.Height = 0.1354167F;
            this.txtDedYTD12.HyperLink = null;
            this.txtDedYTD12.Left = 6.645833F;
            this.txtDedYTD12.Name = "txtDedYTD12";
            this.txtDedYTD12.Style = "text-align: right";
            this.txtDedYTD12.Tag = "text";
            this.txtDedYTD12.Text = "0.00";
            this.txtDedYTD12.Top = 3.833333F;
            this.txtDedYTD12.Width = 0.6875F;
            // 
            // txtDedDesc13
            // 
            this.txtDedDesc13.Height = 0.1354167F;
            this.txtDedDesc13.HyperLink = null;
            this.txtDedDesc13.Left = 3.552083F;
            this.txtDedDesc13.Name = "txtDedDesc13";
            this.txtDedDesc13.Style = "text-align: left";
            this.txtDedDesc13.Tag = "text";
            this.txtDedDesc13.Text = "Deduction Name";
            this.txtDedDesc13.Top = 3.958333F;
            this.txtDedDesc13.Width = 2.25F;
            // 
            // txtDedAmount13
            // 
            this.txtDedAmount13.Height = 0.1388889F;
            this.txtDedAmount13.HyperLink = null;
            this.txtDedAmount13.Left = 5.895833F;
            this.txtDedAmount13.Name = "txtDedAmount13";
            this.txtDedAmount13.Style = "text-align: right";
            this.txtDedAmount13.Tag = "text";
            this.txtDedAmount13.Text = "0.00";
            this.txtDedAmount13.Top = 3.958333F;
            this.txtDedAmount13.Width = 0.75F;
            // 
            // txtDedYTD13
            // 
            this.txtDedYTD13.Height = 0.1354167F;
            this.txtDedYTD13.HyperLink = null;
            this.txtDedYTD13.Left = 6.645833F;
            this.txtDedYTD13.Name = "txtDedYTD13";
            this.txtDedYTD13.Style = "text-align: right";
            this.txtDedYTD13.Tag = "text";
            this.txtDedYTD13.Text = "0.00";
            this.txtDedYTD13.Top = 3.958333F;
            this.txtDedYTD13.Width = 0.6875F;
            // 
            // txtDedDesc14
            // 
            this.txtDedDesc14.Height = 0.1354167F;
            this.txtDedDesc14.HyperLink = null;
            this.txtDedDesc14.Left = 3.552083F;
            this.txtDedDesc14.Name = "txtDedDesc14";
            this.txtDedDesc14.Style = "text-align: left";
            this.txtDedDesc14.Tag = "text";
            this.txtDedDesc14.Text = "Deduction Name";
            this.txtDedDesc14.Top = 4.083333F;
            this.txtDedDesc14.Width = 2.25F;
            // 
            // txtDedAmount14
            // 
            this.txtDedAmount14.Height = 0.1388889F;
            this.txtDedAmount14.HyperLink = null;
            this.txtDedAmount14.Left = 5.895833F;
            this.txtDedAmount14.Name = "txtDedAmount14";
            this.txtDedAmount14.Style = "text-align: right";
            this.txtDedAmount14.Tag = "text";
            this.txtDedAmount14.Text = "0.00";
            this.txtDedAmount14.Top = 4.083333F;
            this.txtDedAmount14.Width = 0.75F;
            // 
            // txtDedYTD14
            // 
            this.txtDedYTD14.Height = 0.1354167F;
            this.txtDedYTD14.HyperLink = null;
            this.txtDedYTD14.Left = 6.645833F;
            this.txtDedYTD14.Name = "txtDedYTD14";
            this.txtDedYTD14.Style = "text-align: right";
            this.txtDedYTD14.Tag = "text";
            this.txtDedYTD14.Text = "0.00";
            this.txtDedYTD14.Top = 4.083333F;
            this.txtDedYTD14.Width = 0.6875F;
            // 
            // txtDedDesc15
            // 
            this.txtDedDesc15.Height = 0.1354167F;
            this.txtDedDesc15.HyperLink = null;
            this.txtDedDesc15.Left = 3.552083F;
            this.txtDedDesc15.Name = "txtDedDesc15";
            this.txtDedDesc15.Style = "text-align: left";
            this.txtDedDesc15.Tag = "text";
            this.txtDedDesc15.Text = "Deduction Name";
            this.txtDedDesc15.Top = 4.208333F;
            this.txtDedDesc15.Width = 2.25F;
            // 
            // txtDedAmount15
            // 
            this.txtDedAmount15.Height = 0.1388889F;
            this.txtDedAmount15.HyperLink = null;
            this.txtDedAmount15.Left = 5.895833F;
            this.txtDedAmount15.Name = "txtDedAmount15";
            this.txtDedAmount15.Style = "text-align: right";
            this.txtDedAmount15.Tag = "text";
            this.txtDedAmount15.Text = "0.00";
            this.txtDedAmount15.Top = 4.208333F;
            this.txtDedAmount15.Width = 0.75F;
            // 
            // txtDedYTD15
            // 
            this.txtDedYTD15.Height = 0.1354167F;
            this.txtDedYTD15.HyperLink = null;
            this.txtDedYTD15.Left = 6.645833F;
            this.txtDedYTD15.Name = "txtDedYTD15";
            this.txtDedYTD15.Style = "text-align: right";
            this.txtDedYTD15.Tag = "text";
            this.txtDedYTD15.Text = "0.00";
            this.txtDedYTD15.Top = 4.208333F;
            this.txtDedYTD15.Width = 0.6875F;
            // 
            // txtDedDesc16
            // 
            this.txtDedDesc16.Height = 0.1354167F;
            this.txtDedDesc16.HyperLink = null;
            this.txtDedDesc16.Left = 3.552083F;
            this.txtDedDesc16.Name = "txtDedDesc16";
            this.txtDedDesc16.Style = "text-align: left";
            this.txtDedDesc16.Tag = "text";
            this.txtDedDesc16.Text = "Deduction Name";
            this.txtDedDesc16.Top = 4.333333F;
            this.txtDedDesc16.Width = 2.25F;
            // 
            // txtDedAmount16
            // 
            this.txtDedAmount16.Height = 0.1388889F;
            this.txtDedAmount16.HyperLink = null;
            this.txtDedAmount16.Left = 5.895833F;
            this.txtDedAmount16.Name = "txtDedAmount16";
            this.txtDedAmount16.Style = "text-align: right";
            this.txtDedAmount16.Tag = "text";
            this.txtDedAmount16.Text = "0.00";
            this.txtDedAmount16.Top = 4.333333F;
            this.txtDedAmount16.Width = 0.75F;
            // 
            // txtDedYTD16
            // 
            this.txtDedYTD16.Height = 0.1354167F;
            this.txtDedYTD16.HyperLink = null;
            this.txtDedYTD16.Left = 6.645833F;
            this.txtDedYTD16.Name = "txtDedYTD16";
            this.txtDedYTD16.Style = "text-align: right";
            this.txtDedYTD16.Tag = "text";
            this.txtDedYTD16.Text = "0.00";
            this.txtDedYTD16.Top = 4.333333F;
            this.txtDedYTD16.Width = 0.6875F;
            // 
            // txtTotalHours
            // 
            this.txtTotalHours.Height = 0.1354167F;
            this.txtTotalHours.HyperLink = null;
            this.txtTotalHours.Left = 5.895833F;
            this.txtTotalHours.Name = "txtTotalHours";
            this.txtTotalHours.Style = "text-align: right";
            this.txtTotalHours.Tag = "text";
            this.txtTotalHours.Text = "0.00";
            this.txtTotalHours.Top = 1.8125F;
            this.txtTotalHours.Width = 0.75F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Height = 0.1354167F;
            this.txtTotalAmount.HyperLink = null;
            this.txtTotalAmount.Left = 6.645833F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Style = "text-align: right";
            this.txtTotalAmount.Tag = "text";
            this.txtTotalAmount.Text = "0.00";
            this.txtTotalAmount.Top = 1.8125F;
            this.txtTotalAmount.Width = 0.6875F;
            // 
            // lblCheckMessage
            // 
            this.lblCheckMessage.Height = 0.1666667F;
            this.lblCheckMessage.HyperLink = null;
            this.lblCheckMessage.Left = 0.08333334F;
            this.lblCheckMessage.Name = "lblCheckMessage";
            this.lblCheckMessage.Style = "font-size: 10pt; text-align: center; vertical-align: top";
            this.lblCheckMessage.Text = null;
            this.lblCheckMessage.Top = 6.625F;
            this.lblCheckMessage.Width = 7.21875F;
            // 
            // lblCode2Balance
            // 
            this.lblCode2Balance.Height = 0.1354167F;
            this.lblCode2Balance.HyperLink = null;
            this.lblCode2Balance.Left = 2.65625F;
            this.lblCode2Balance.Name = "lblCode2Balance";
            this.lblCode2Balance.Style = "text-align: right";
            this.lblCode2Balance.Tag = "text";
            this.lblCode2Balance.Text = "0.00";
            this.lblCode2Balance.Top = 4.645833F;
            this.lblCode2Balance.Width = 0.625F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 3.520833F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 5.020833F;
            this.Line3.Width = 3.885417F;
            this.Line3.X1 = 3.520833F;
            this.Line3.X2 = 7.40625F;
            this.Line3.Y1 = 5.020833F;
            this.Line3.Y2 = 5.020833F;
            // 
            // txtTotalYTDDeductions
            // 
            this.txtTotalYTDDeductions.Height = 0.1354167F;
            this.txtTotalYTDDeductions.HyperLink = null;
            this.txtTotalYTDDeductions.Left = 6.645833F;
            this.txtTotalYTDDeductions.Name = "txtTotalYTDDeductions";
            this.txtTotalYTDDeductions.Style = "text-align: right";
            this.txtTotalYTDDeductions.Tag = "text";
            this.txtTotalYTDDeductions.Text = "0.00";
            this.txtTotalYTDDeductions.Top = 5.072917F;
            this.txtTotalYTDDeductions.Width = 0.6875F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 3.520833F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 2.423611F;
            this.Line4.Width = 3.885417F;
            this.Line4.X1 = 3.520833F;
            this.Line4.X2 = 7.40625F;
            this.Line4.Y1 = 2.423611F;
            this.Line4.Y2 = 2.423611F;
            // 
            // Label206
            // 
            this.Label206.Height = 0.1666667F;
            this.Label206.HyperLink = null;
            this.Label206.Left = 3.552083F;
            this.Label206.Name = "Label206";
            this.Label206.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label206.Tag = "text";
            this.Label206.Text = "Total";
            this.Label206.Top = 5.0625F;
            this.Label206.Width = 1.4375F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 3.520833F;
            this.Line5.LineWeight = 2F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 1.78125F;
            this.Line5.Width = 3.885417F;
            this.Line5.X1 = 3.520833F;
            this.Line5.X2 = 7.40625F;
            this.Line5.Y1 = 1.78125F;
            this.Line5.Y2 = 1.78125F;
            // 
            // Label209
            // 
            this.Label209.Height = 0.1354167F;
            this.Label209.HyperLink = null;
            this.Label209.Left = 3.5625F;
            this.Label209.Name = "Label209";
            this.Label209.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label209.Tag = "text";
            this.Label209.Text = "Total";
            this.Label209.Top = 1.8125F;
            this.Label209.Width = 1.4375F;
            // 
            // Label210
            // 
            this.Label210.Height = 0.1770833F;
            this.Label210.HyperLink = null;
            this.Label210.Left = 3.552083F;
            this.Label210.Name = "Label210";
            this.Label210.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label210.Text = "Tax";
            this.Label210.Top = 5.479167F;
            this.Label210.Width = 0.7083333F;
            // 
            // txtPayDesc7
            // 
            this.txtPayDesc7.Height = 0.1388889F;
            this.txtPayDesc7.HyperLink = null;
            this.txtPayDesc7.Left = 3.5625F;
            this.txtPayDesc7.Name = "txtPayDesc7";
            this.txtPayDesc7.Style = "text-align: left";
            this.txtPayDesc7.Tag = "text";
            this.txtPayDesc7.Text = "Regular";
            this.txtPayDesc7.Top = 1F;
            this.txtPayDesc7.Width = 1.489583F;
            // 
            // txtHours7
            // 
            this.txtHours7.Height = 0.1354167F;
            this.txtHours7.HyperLink = null;
            this.txtHours7.Left = 5.958333F;
            this.txtHours7.Name = "txtHours7";
            this.txtHours7.Style = "text-align: right";
            this.txtHours7.Tag = "text";
            this.txtHours7.Text = "0.00";
            this.txtHours7.Top = 1F;
            this.txtHours7.Width = 0.6875F;
            // 
            // txtPayAmount7
            // 
            this.txtPayAmount7.Height = 0.1354167F;
            this.txtPayAmount7.HyperLink = null;
            this.txtPayAmount7.Left = 6.645833F;
            this.txtPayAmount7.Name = "txtPayAmount7";
            this.txtPayAmount7.Style = "text-align: right";
            this.txtPayAmount7.Tag = "text";
            this.txtPayAmount7.Text = "0.00";
            this.txtPayAmount7.Top = 1F;
            this.txtPayAmount7.Width = 0.6875F;
            // 
            // txtPayDesc8
            // 
            this.txtPayDesc8.Height = 0.1388889F;
            this.txtPayDesc8.HyperLink = null;
            this.txtPayDesc8.Left = 3.5625F;
            this.txtPayDesc8.Name = "txtPayDesc8";
            this.txtPayDesc8.Style = "text-align: left";
            this.txtPayDesc8.Tag = "text";
            this.txtPayDesc8.Text = "Regular";
            this.txtPayDesc8.Top = 1.125F;
            this.txtPayDesc8.Width = 1.489583F;
            // 
            // txtHours8
            // 
            this.txtHours8.Height = 0.1354167F;
            this.txtHours8.HyperLink = null;
            this.txtHours8.Left = 5.958333F;
            this.txtHours8.Name = "txtHours8";
            this.txtHours8.Style = "text-align: right";
            this.txtHours8.Tag = "text";
            this.txtHours8.Text = "0.00";
            this.txtHours8.Top = 1.125F;
            this.txtHours8.Width = 0.6875F;
            // 
            // txtPayAmount8
            // 
            this.txtPayAmount8.Height = 0.1354167F;
            this.txtPayAmount8.HyperLink = null;
            this.txtPayAmount8.Left = 6.645833F;
            this.txtPayAmount8.Name = "txtPayAmount8";
            this.txtPayAmount8.Style = "text-align: right";
            this.txtPayAmount8.Tag = "text";
            this.txtPayAmount8.Text = "0.00";
            this.txtPayAmount8.Top = 1.125F;
            this.txtPayAmount8.Width = 0.6875F;
            // 
            // txtPayDesc9
            // 
            this.txtPayDesc9.Height = 0.1388889F;
            this.txtPayDesc9.HyperLink = null;
            this.txtPayDesc9.Left = 3.5625F;
            this.txtPayDesc9.Name = "txtPayDesc9";
            this.txtPayDesc9.Style = "text-align: left";
            this.txtPayDesc9.Tag = "text";
            this.txtPayDesc9.Text = "Regular";
            this.txtPayDesc9.Top = 1.25F;
            this.txtPayDesc9.Width = 1.489583F;
            // 
            // txtHours9
            // 
            this.txtHours9.Height = 0.1354167F;
            this.txtHours9.HyperLink = null;
            this.txtHours9.Left = 5.958333F;
            this.txtHours9.Name = "txtHours9";
            this.txtHours9.Style = "text-align: right";
            this.txtHours9.Tag = "text";
            this.txtHours9.Text = "0.00";
            this.txtHours9.Top = 1.25F;
            this.txtHours9.Width = 0.6875F;
            // 
            // txtPayAmount9
            // 
            this.txtPayAmount9.Height = 0.1354167F;
            this.txtPayAmount9.HyperLink = null;
            this.txtPayAmount9.Left = 6.645833F;
            this.txtPayAmount9.Name = "txtPayAmount9";
            this.txtPayAmount9.Style = "text-align: right";
            this.txtPayAmount9.Tag = "text";
            this.txtPayAmount9.Text = "0.00";
            this.txtPayAmount9.Top = 1.25F;
            this.txtPayAmount9.Width = 0.6875F;
            // 
            // txtPayDesc10
            // 
            this.txtPayDesc10.Height = 0.1388889F;
            this.txtPayDesc10.HyperLink = null;
            this.txtPayDesc10.Left = 3.5625F;
            this.txtPayDesc10.Name = "txtPayDesc10";
            this.txtPayDesc10.Style = "text-align: left";
            this.txtPayDesc10.Tag = "text";
            this.txtPayDesc10.Text = "Regular";
            this.txtPayDesc10.Top = 1.375F;
            this.txtPayDesc10.Width = 1.489583F;
            // 
            // txtHours10
            // 
            this.txtHours10.Height = 0.1354167F;
            this.txtHours10.HyperLink = null;
            this.txtHours10.Left = 5.958333F;
            this.txtHours10.Name = "txtHours10";
            this.txtHours10.Style = "text-align: right";
            this.txtHours10.Tag = "text";
            this.txtHours10.Text = "0.00";
            this.txtHours10.Top = 1.375F;
            this.txtHours10.Width = 0.6875F;
            // 
            // txtPayAmount10
            // 
            this.txtPayAmount10.Height = 0.1354167F;
            this.txtPayAmount10.HyperLink = null;
            this.txtPayAmount10.Left = 6.645833F;
            this.txtPayAmount10.Name = "txtPayAmount10";
            this.txtPayAmount10.Style = "text-align: right";
            this.txtPayAmount10.Tag = "text";
            this.txtPayAmount10.Text = "0.00";
            this.txtPayAmount10.Top = 1.375F;
            this.txtPayAmount10.Width = 0.6875F;
            // 
            // txtPayRate
            // 
            this.txtPayRate.Height = 0.1354167F;
            this.txtPayRate.HyperLink = null;
            this.txtPayRate.Left = 0.9375F;
            this.txtPayRate.Name = "txtPayRate";
            this.txtPayRate.Style = "text-align: left";
            this.txtPayRate.Tag = "text";
            this.txtPayRate.Text = "0.00";
            this.txtPayRate.Top = 0.71875F;
            this.txtPayRate.Width = 0.5625F;
            // 
            // Label223
            // 
            this.Label223.Height = 0.1388889F;
            this.Label223.HyperLink = null;
            this.Label223.Left = 0.1041667F;
            this.Label223.Name = "Label223";
            this.Label223.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label223.Tag = "text";
            this.Label223.Text = "Employee";
            this.Label223.Top = 0.59375F;
            this.Label223.Width = 0.75F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0.0625F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 1.78125F;
            this.Line10.Width = 3.270833F;
            this.Line10.X1 = 0.0625F;
            this.Line10.X2 = 3.333333F;
            this.Line10.Y1 = 1.78125F;
            this.Line10.Y2 = 1.78125F;
            // 
            // Label224
            // 
            this.Label224.Height = 0.1666667F;
            this.Label224.HyperLink = null;
            this.Label224.Left = 1.541667F;
            this.Label224.Name = "Label224";
            this.Label224.Style = "font-size: 10pt; font-weight: bold; text-align: left";
            this.Label224.Tag = "text";
            this.Label224.Text = "Check";
            this.Label224.Top = 0.03125F;
            this.Label224.Width = 0.5625F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.0625F;
            this.Line11.LineWeight = 2F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.25F;
            this.Line11.Width = 3.260417F;
            this.Line11.X1 = 0.0625F;
            this.Line11.X2 = 3.322917F;
            this.Line11.Y1 = 0.25F;
            this.Line11.Y2 = 0.25F;
            // 
            // Label226
            // 
            this.Label226.Height = 0.1354167F;
            this.Label226.HyperLink = null;
            this.Label226.Left = 3.552083F;
            this.Label226.Name = "Label226";
            this.Label226.Style = "font-size: 8.5pt; text-align: left";
            this.Label226.Tag = "text";
            this.Label226.Text = "Net";
            this.Label226.Top = 6.4375F;
            this.Label226.Width = 0.75F;
            // 
            // Label227
            // 
            this.Label227.Height = 0.1388889F;
            this.Label227.HyperLink = null;
            this.Label227.Left = 3.552083F;
            this.Label227.Name = "Label227";
            this.Label227.Style = "font-size: 8.5pt; text-align: left";
            this.Label227.Tag = "text";
            this.Label227.Text = "Earnings";
            this.Label227.Top = 6.302083F;
            this.Label227.Width = 0.75F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 3.520833F;
            this.Line12.LineWeight = 2F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 6.25F;
            this.Line12.Width = 3.885417F;
            this.Line12.X1 = 3.520833F;
            this.Line12.X2 = 7.40625F;
            this.Line12.Y1 = 6.25F;
            this.Line12.Y2 = 6.25F;
            // 
            // Label230
            // 
            this.Label230.Height = 0.1354167F;
            this.Label230.HyperLink = null;
            this.Label230.Left = 0.1041667F;
            this.Label230.Name = "Label230";
            this.Label230.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label230.Tag = "text";
            this.Label230.Text = "Leave";
            this.Label230.Top = 4.083333F;
            this.Label230.Width = 0.7708333F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 0.0625F;
            this.Line13.LineWeight = 2F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 4.229167F;
            this.Line13.Width = 3.260417F;
            this.Line13.X1 = 0.0625F;
            this.Line13.X2 = 3.322917F;
            this.Line13.Y1 = 4.229167F;
            this.Line13.Y2 = 4.229167F;
            // 
            // lblCode5
            // 
            this.lblCode5.Height = 0.1388889F;
            this.lblCode5.HyperLink = null;
            this.lblCode5.Left = 0.1041667F;
            this.lblCode5.Name = "lblCode5";
            this.lblCode5.Style = "font-size: 8pt; text-align: left";
            this.lblCode5.Tag = "text";
            this.lblCode5.Text = "Code 4";
            this.lblCode5.Top = 5.020833F;
            this.lblCode5.Width = 1.197917F;
            // 
            // lblCode5Accrued
            // 
            this.lblCode5Accrued.Height = 0.1388889F;
            this.lblCode5Accrued.HyperLink = null;
            this.lblCode5Accrued.Left = 1.333333F;
            this.lblCode5Accrued.Name = "lblCode5Accrued";
            this.lblCode5Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode5Accrued.Tag = "text";
            this.lblCode5Accrued.Text = "0.00";
            this.lblCode5Accrued.Top = 5.020833F;
            this.lblCode5Accrued.Width = 0.625F;
            // 
            // lblCode6
            // 
            this.lblCode6.Height = 0.1388889F;
            this.lblCode6.HyperLink = null;
            this.lblCode6.Left = 0.1041667F;
            this.lblCode6.Name = "lblCode6";
            this.lblCode6.Style = "font-size: 8pt; text-align: left";
            this.lblCode6.Tag = "text";
            this.lblCode6.Text = "Code 5";
            this.lblCode6.Top = 5.145833F;
            this.lblCode6.Width = 1.197917F;
            // 
            // lblCode6Accrued
            // 
            this.lblCode6Accrued.Height = 0.1388889F;
            this.lblCode6Accrued.HyperLink = null;
            this.lblCode6Accrued.Left = 1.333333F;
            this.lblCode6Accrued.Name = "lblCode6Accrued";
            this.lblCode6Accrued.Style = "font-size: 8pt; text-align: right";
            this.lblCode6Accrued.Tag = "text";
            this.lblCode6Accrued.Text = "0.00";
            this.lblCode6Accrued.Top = 5.145833F;
            this.lblCode6Accrued.Width = 0.625F;
            // 
            // lblCode5Taken
            // 
            this.lblCode5Taken.Height = 0.1354167F;
            this.lblCode5Taken.HyperLink = null;
            this.lblCode5Taken.Left = 1.979167F;
            this.lblCode5Taken.Name = "lblCode5Taken";
            this.lblCode5Taken.Style = "font-size: 8pt; text-align: right";
            this.lblCode5Taken.Tag = "text";
            this.lblCode5Taken.Text = "0.00";
            this.lblCode5Taken.Top = 5.020833F;
            this.lblCode5Taken.Width = 0.625F;
            // 
            // lblCode6Taken
            // 
            this.lblCode6Taken.Height = 0.1354167F;
            this.lblCode6Taken.HyperLink = null;
            this.lblCode6Taken.Left = 1.979167F;
            this.lblCode6Taken.Name = "lblCode6Taken";
            this.lblCode6Taken.Style = "font-size: 8pt; text-align: right";
            this.lblCode6Taken.Tag = "text";
            this.lblCode6Taken.Text = "0.00";
            this.lblCode6Taken.Top = 5.145833F;
            this.lblCode6Taken.Width = 0.625F;
            // 
            // lblCode5Balance
            // 
            this.lblCode5Balance.Height = 0.1354167F;
            this.lblCode5Balance.HyperLink = null;
            this.lblCode5Balance.Left = 2.65625F;
            this.lblCode5Balance.Name = "lblCode5Balance";
            this.lblCode5Balance.Style = "font-size: 8pt; text-align: right";
            this.lblCode5Balance.Tag = "text";
            this.lblCode5Balance.Text = "0.00";
            this.lblCode5Balance.Top = 5.020833F;
            this.lblCode5Balance.Width = 0.625F;
            // 
            // lblCode6Balance
            // 
            this.lblCode6Balance.Height = 0.1354167F;
            this.lblCode6Balance.HyperLink = null;
            this.lblCode6Balance.Left = 2.65625F;
            this.lblCode6Balance.Name = "lblCode6Balance";
            this.lblCode6Balance.Style = "font-size: 8pt; text-align: right";
            this.lblCode6Balance.Tag = "text";
            this.lblCode6Balance.Text = "0.00";
            this.lblCode6Balance.Top = 5.145833F;
            this.lblCode6Balance.Width = 0.625F;
            // 
            // txtYTDNet
            // 
            this.txtYTDNet.Height = 0.1388889F;
            this.txtYTDNet.HyperLink = null;
            this.txtYTDNet.Left = 5.802083F;
            this.txtYTDNet.Name = "txtYTDNet";
            this.txtYTDNet.Style = "text-align: right";
            this.txtYTDNet.Tag = "text";
            this.txtYTDNet.Text = "0.00";
            this.txtYTDNet.Top = 6.4375F;
            this.txtYTDNet.Width = 0.6875F;
            // 
            // txtYTDGross
            // 
            this.txtYTDGross.Height = 0.1388889F;
            this.txtYTDGross.HyperLink = null;
            this.txtYTDGross.Left = 5.802083F;
            this.txtYTDGross.Name = "txtYTDGross";
            this.txtYTDGross.Style = "text-align: right";
            this.txtYTDGross.Tag = "text";
            this.txtYTDGross.Text = "0.00";
            this.txtYTDGross.Top = 6.302083F;
            this.txtYTDGross.Width = 0.6875F;
            // 
            // txtDirectDepositLabel
            // 
            this.txtDirectDepositLabel.Height = 0.1666667F;
            this.txtDirectDepositLabel.HyperLink = null;
            this.txtDirectDepositLabel.Left = 0.07291666F;
            this.txtDirectDepositLabel.Name = "txtDirectDepositLabel";
            this.txtDirectDepositLabel.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.txtDirectDepositLabel.Tag = "text";
            this.txtDirectDepositLabel.Text = "Direct Deposit";
            this.txtDirectDepositLabel.Top = 5.479167F;
            this.txtDirectDepositLabel.Width = 1.4375F;
            // 
            // txtDirectDeposit
            // 
            this.txtDirectDeposit.Height = 0.1354167F;
            this.txtDirectDeposit.HyperLink = null;
            this.txtDirectDeposit.Left = 0.6666667F;
            this.txtDirectDeposit.Name = "txtDirectDeposit";
            this.txtDirectDeposit.Style = "text-align: right";
            this.txtDirectDeposit.Tag = "text";
            this.txtDirectDeposit.Text = "0.00";
            this.txtDirectDeposit.Top = 5.697917F;
            this.txtDirectDeposit.Width = 0.625F;
            // 
            // txtChkAmount
            // 
            this.txtChkAmount.Height = 0.1354167F;
            this.txtChkAmount.HyperLink = null;
            this.txtChkAmount.Left = 0.6666667F;
            this.txtChkAmount.Name = "txtChkAmount";
            this.txtChkAmount.Style = "text-align: right";
            this.txtChkAmount.Tag = "text";
            this.txtChkAmount.Text = "0.00";
            this.txtChkAmount.Top = 5.96875F;
            this.txtChkAmount.Width = 0.625F;
            // 
            // lblCheckAmount
            // 
            this.lblCheckAmount.Height = 0.1354167F;
            this.lblCheckAmount.HyperLink = null;
            this.lblCheckAmount.Left = 0.1041667F;
            this.lblCheckAmount.Name = "lblCheckAmount";
            this.lblCheckAmount.Style = "text-align: left";
            this.lblCheckAmount.Tag = "text";
            this.lblCheckAmount.Text = "Negotiable";
            this.lblCheckAmount.Top = 5.96875F;
            this.lblCheckAmount.Width = 0.6458333F;
            // 
            // txtDirectDepChkLabel
            // 
            this.txtDirectDepChkLabel.Height = 0.1354167F;
            this.txtDirectDepChkLabel.HyperLink = null;
            this.txtDirectDepChkLabel.Left = 0.1041667F;
            this.txtDirectDepChkLabel.Name = "txtDirectDepChkLabel";
            this.txtDirectDepChkLabel.Style = "text-align: left";
            this.txtDirectDepChkLabel.Tag = "text";
            this.txtDirectDepChkLabel.Text = "Checking";
            this.txtDirectDepChkLabel.Top = 5.697917F;
            this.txtDirectDepChkLabel.Width = 0.625F;
            // 
            // txtDirectDepSav
            // 
            this.txtDirectDepSav.Height = 0.1354167F;
            this.txtDirectDepSav.HyperLink = null;
            this.txtDirectDepSav.Left = 0.6666667F;
            this.txtDirectDepSav.Name = "txtDirectDepSav";
            this.txtDirectDepSav.Style = "text-align: right";
            this.txtDirectDepSav.Tag = "text";
            this.txtDirectDepSav.Text = "0.00";
            this.txtDirectDepSav.Top = 5.822917F;
            this.txtDirectDepSav.Width = 0.625F;
            // 
            // txtDirectDepositSavLabel
            // 
            this.txtDirectDepositSavLabel.Height = 0.1354167F;
            this.txtDirectDepositSavLabel.HyperLink = null;
            this.txtDirectDepositSavLabel.Left = 0.1041667F;
            this.txtDirectDepositSavLabel.Name = "txtDirectDepositSavLabel";
            this.txtDirectDepositSavLabel.Style = "text-align: left";
            this.txtDirectDepositSavLabel.Tag = "text";
            this.txtDirectDepositSavLabel.Text = "Savings";
            this.txtDirectDepositSavLabel.Top = 5.833333F;
            this.txtDirectDepositSavLabel.Width = 0.625F;
            // 
            // txtBankAmount1
            // 
            this.txtBankAmount1.Height = 0.1354167F;
            this.txtBankAmount1.HyperLink = null;
            this.txtBankAmount1.Left = 2.71875F;
            this.txtBankAmount1.Name = "txtBankAmount1";
            this.txtBankAmount1.Style = "text-align: right";
            this.txtBankAmount1.Tag = "text";
            this.txtBankAmount1.Text = "0.00";
            this.txtBankAmount1.Top = 5.697917F;
            this.txtBankAmount1.Width = 0.5625F;
            // 
            // txtBankAmount3
            // 
            this.txtBankAmount3.Height = 0.1354167F;
            this.txtBankAmount3.HyperLink = null;
            this.txtBankAmount3.Left = 2.71875F;
            this.txtBankAmount3.Name = "txtBankAmount3";
            this.txtBankAmount3.Style = "text-align: right";
            this.txtBankAmount3.Tag = "text";
            this.txtBankAmount3.Text = "0.00";
            this.txtBankAmount3.Top = 5.947917F;
            this.txtBankAmount3.Width = 0.5625F;
            // 
            // txtBank3
            // 
            this.txtBank3.Height = 0.1388889F;
            this.txtBank3.HyperLink = null;
            this.txtBank3.Left = 1.34375F;
            this.txtBank3.Name = "txtBank3";
            this.txtBank3.Style = "text-align: left";
            this.txtBank3.Tag = "text";
            this.txtBank3.Text = "Bank Three";
            this.txtBank3.Top = 5.96875F;
            this.txtBank3.Width = 1.333333F;
            // 
            // txtBank1
            // 
            this.txtBank1.Height = 0.1388889F;
            this.txtBank1.HyperLink = null;
            this.txtBank1.Left = 1.34375F;
            this.txtBank1.Name = "txtBank1";
            this.txtBank1.Style = "text-align: left";
            this.txtBank1.Tag = "text";
            this.txtBank1.Text = "Bank One";
            this.txtBank1.Top = 5.697917F;
            this.txtBank1.Width = 1.3125F;
            // 
            // txtBankAmount2
            // 
            this.txtBankAmount2.Height = 0.1354167F;
            this.txtBankAmount2.HyperLink = null;
            this.txtBankAmount2.Left = 2.71875F;
            this.txtBankAmount2.Name = "txtBankAmount2";
            this.txtBankAmount2.Style = "text-align: right";
            this.txtBankAmount2.Tag = "text";
            this.txtBankAmount2.Text = "0.00";
            this.txtBankAmount2.Top = 5.822917F;
            this.txtBankAmount2.Width = 0.5625F;
            // 
            // txtBank2
            // 
            this.txtBank2.Height = 0.1388889F;
            this.txtBank2.HyperLink = null;
            this.txtBank2.Left = 1.34375F;
            this.txtBank2.Name = "txtBank2";
            this.txtBank2.Style = "text-align: left";
            this.txtBank2.Tag = "text";
            this.txtBank2.Text = "Bank Two";
            this.txtBank2.Top = 5.833333F;
            this.txtBank2.Width = 1.3125F;
            // 
            // Line9
            // 
            this.Line9.Height = 0F;
            this.Line9.Left = 0.0625F;
            this.Line9.LineWeight = 2F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 5.645833F;
            this.Line9.Width = 3.260417F;
            this.Line9.X1 = 0.0625F;
            this.Line9.X2 = 3.322917F;
            this.Line9.Y1 = 5.645833F;
            this.Line9.Y2 = 5.645833F;
            // 
            // lblDepartmentLabel
            // 
            this.lblDepartmentLabel.Height = 0.1388889F;
            this.lblDepartmentLabel.HyperLink = null;
            this.lblDepartmentLabel.Left = 0.1041667F;
            this.lblDepartmentLabel.Name = "lblDepartmentLabel";
            this.lblDepartmentLabel.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.lblDepartmentLabel.Tag = "text";
            this.lblDepartmentLabel.Text = "Department";
            this.lblDepartmentLabel.Top = 0.84375F;
            this.lblDepartmentLabel.Width = 0.75F;
            // 
            // txtDepartment
            // 
            this.txtDepartment.Height = 0.1354167F;
            this.txtDepartment.HyperLink = null;
            this.txtDepartment.Left = 0.9375F;
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Style = "text-align: left";
            this.txtDepartment.Tag = "text";
            this.txtDepartment.Text = "Municipal Office";
            this.txtDepartment.Top = 0.84375F;
            this.txtDepartment.Width = 2.260417F;
            // 
            // txtBankAmount5
            // 
            this.txtBankAmount5.Height = 0.1354167F;
            this.txtBankAmount5.HyperLink = null;
            this.txtBankAmount5.Left = 2.71875F;
            this.txtBankAmount5.Name = "txtBankAmount5";
            this.txtBankAmount5.Style = "text-align: right";
            this.txtBankAmount5.Tag = "text";
            this.txtBankAmount5.Text = "0.00";
            this.txtBankAmount5.Top = 6.21875F;
            this.txtBankAmount5.Width = 0.5625F;
            // 
            // txtBank5
            // 
            this.txtBank5.Height = 0.1388889F;
            this.txtBank5.HyperLink = null;
            this.txtBank5.Left = 1.34375F;
            this.txtBank5.Name = "txtBank5";
            this.txtBank5.Style = "text-align: left";
            this.txtBank5.Tag = "text";
            this.txtBank5.Text = "Bank Five";
            this.txtBank5.Top = 6.21875F;
            this.txtBank5.Width = 1.333333F;
            // 
            // txtBankAmount4
            // 
            this.txtBankAmount4.Height = 0.1354167F;
            this.txtBankAmount4.HyperLink = null;
            this.txtBankAmount4.Left = 2.71875F;
            this.txtBankAmount4.Name = "txtBankAmount4";
            this.txtBankAmount4.Style = "text-align: right";
            this.txtBankAmount4.Tag = "text";
            this.txtBankAmount4.Text = "0.00";
            this.txtBankAmount4.Top = 6.09375F;
            this.txtBankAmount4.Width = 0.5625F;
            // 
            // txtBank4
            // 
            this.txtBank4.Height = 0.1354167F;
            this.txtBank4.HyperLink = null;
            this.txtBank4.Left = 1.34375F;
            this.txtBank4.Name = "txtBank4";
            this.txtBank4.Style = "text-align: left";
            this.txtBank4.Tag = "text";
            this.txtBank4.Text = "Bank Four";
            this.txtBank4.Top = 6.09375F;
            this.txtBank4.Width = 1.3125F;
            // 
            // txtBankAmount7
            // 
            this.txtBankAmount7.Height = 0.1354167F;
            this.txtBankAmount7.HyperLink = null;
            this.txtBankAmount7.Left = 2.71875F;
            this.txtBankAmount7.Name = "txtBankAmount7";
            this.txtBankAmount7.Style = "text-align: right";
            this.txtBankAmount7.Tag = "text";
            this.txtBankAmount7.Text = "0.00";
            this.txtBankAmount7.Top = 6.46875F;
            this.txtBankAmount7.Width = 0.5625F;
            // 
            // txtBank7
            // 
            this.txtBank7.Height = 0.1388889F;
            this.txtBank7.HyperLink = null;
            this.txtBank7.Left = 1.34375F;
            this.txtBank7.Name = "txtBank7";
            this.txtBank7.Style = "text-align: left";
            this.txtBank7.Tag = "text";
            this.txtBank7.Text = "Bank Seven";
            this.txtBank7.Top = 6.46875F;
            this.txtBank7.Width = 1.333333F;
            // 
            // txtBankAmount6
            // 
            this.txtBankAmount6.Height = 0.1354167F;
            this.txtBankAmount6.HyperLink = null;
            this.txtBankAmount6.Left = 2.71875F;
            this.txtBankAmount6.Name = "txtBankAmount6";
            this.txtBankAmount6.Style = "text-align: right";
            this.txtBankAmount6.Tag = "text";
            this.txtBankAmount6.Text = "0.00";
            this.txtBankAmount6.Top = 6.34375F;
            this.txtBankAmount6.Width = 0.5625F;
            // 
            // txtBank6
            // 
            this.txtBank6.Height = 0.1354167F;
            this.txtBank6.HyperLink = null;
            this.txtBank6.Left = 1.34375F;
            this.txtBank6.Name = "txtBank6";
            this.txtBank6.Style = "text-align: left";
            this.txtBank6.Tag = "text";
            this.txtBank6.Text = "Bank Six";
            this.txtBank6.Top = 6.34375F;
            this.txtBank6.Width = 1.3125F;
            // 
            // txtPayDesc11
            // 
            this.txtPayDesc11.Height = 0.1388889F;
            this.txtPayDesc11.HyperLink = null;
            this.txtPayDesc11.Left = 3.5625F;
            this.txtPayDesc11.Name = "txtPayDesc11";
            this.txtPayDesc11.Style = "text-align: left";
            this.txtPayDesc11.Tag = "text";
            this.txtPayDesc11.Text = "Regular";
            this.txtPayDesc11.Top = 1.5F;
            this.txtPayDesc11.Width = 1.489583F;
            // 
            // txtHours11
            // 
            this.txtHours11.Height = 0.1354167F;
            this.txtHours11.HyperLink = null;
            this.txtHours11.Left = 5.958333F;
            this.txtHours11.Name = "txtHours11";
            this.txtHours11.Style = "text-align: right";
            this.txtHours11.Tag = "text";
            this.txtHours11.Text = "0.00";
            this.txtHours11.Top = 1.5F;
            this.txtHours11.Width = 0.6875F;
            // 
            // txtPayAmount11
            // 
            this.txtPayAmount11.Height = 0.1354167F;
            this.txtPayAmount11.HyperLink = null;
            this.txtPayAmount11.Left = 6.645833F;
            this.txtPayAmount11.Name = "txtPayAmount11";
            this.txtPayAmount11.Style = "text-align: right";
            this.txtPayAmount11.Tag = "text";
            this.txtPayAmount11.Text = "0.00";
            this.txtPayAmount11.Top = 1.5F;
            this.txtPayAmount11.Width = 0.6875F;
            // 
            // txtPayDesc12
            // 
            this.txtPayDesc12.Height = 0.1388889F;
            this.txtPayDesc12.HyperLink = null;
            this.txtPayDesc12.Left = 3.5625F;
            this.txtPayDesc12.Name = "txtPayDesc12";
            this.txtPayDesc12.Style = "text-align: left";
            this.txtPayDesc12.Tag = "text";
            this.txtPayDesc12.Text = "Regular";
            this.txtPayDesc12.Top = 1.625F;
            this.txtPayDesc12.Width = 1.489583F;
            // 
            // txtHours12
            // 
            this.txtHours12.Height = 0.1354167F;
            this.txtHours12.HyperLink = null;
            this.txtHours12.Left = 5.958333F;
            this.txtHours12.Name = "txtHours12";
            this.txtHours12.Style = "text-align: right";
            this.txtHours12.Tag = "text";
            this.txtHours12.Text = "0.00";
            this.txtHours12.Top = 1.625F;
            this.txtHours12.Width = 0.6875F;
            // 
            // txtPayAmount12
            // 
            this.txtPayAmount12.Height = 0.1354167F;
            this.txtPayAmount12.HyperLink = null;
            this.txtPayAmount12.Left = 6.645833F;
            this.txtPayAmount12.Name = "txtPayAmount12";
            this.txtPayAmount12.Style = "text-align: right";
            this.txtPayAmount12.Tag = "text";
            this.txtPayAmount12.Text = "0.00";
            this.txtPayAmount12.Top = 1.625F;
            this.txtPayAmount12.Width = 0.6875F;
            // 
            // txtDedDesc17
            // 
            this.txtDedDesc17.Height = 0.1354167F;
            this.txtDedDesc17.HyperLink = null;
            this.txtDedDesc17.Left = 3.552083F;
            this.txtDedDesc17.Name = "txtDedDesc17";
            this.txtDedDesc17.Style = "text-align: left";
            this.txtDedDesc17.Tag = "text";
            this.txtDedDesc17.Text = "Deduction Name";
            this.txtDedDesc17.Top = 4.458333F;
            this.txtDedDesc17.Width = 2.25F;
            // 
            // txtDedAmount17
            // 
            this.txtDedAmount17.Height = 0.1388889F;
            this.txtDedAmount17.HyperLink = null;
            this.txtDedAmount17.Left = 5.895833F;
            this.txtDedAmount17.Name = "txtDedAmount17";
            this.txtDedAmount17.Style = "text-align: right";
            this.txtDedAmount17.Tag = "text";
            this.txtDedAmount17.Text = "0.00";
            this.txtDedAmount17.Top = 4.458333F;
            this.txtDedAmount17.Width = 0.75F;
            // 
            // txtDedYTD17
            // 
            this.txtDedYTD17.Height = 0.1354167F;
            this.txtDedYTD17.HyperLink = null;
            this.txtDedYTD17.Left = 6.645833F;
            this.txtDedYTD17.Name = "txtDedYTD17";
            this.txtDedYTD17.Style = "text-align: right";
            this.txtDedYTD17.Tag = "text";
            this.txtDedYTD17.Text = "0.00";
            this.txtDedYTD17.Top = 4.458333F;
            this.txtDedYTD17.Width = 0.6875F;
            // 
            // txtDedDesc18
            // 
            this.txtDedDesc18.Height = 0.1354167F;
            this.txtDedDesc18.HyperLink = null;
            this.txtDedDesc18.Left = 3.552083F;
            this.txtDedDesc18.Name = "txtDedDesc18";
            this.txtDedDesc18.Style = "text-align: left";
            this.txtDedDesc18.Tag = "text";
            this.txtDedDesc18.Text = "Deduction Name";
            this.txtDedDesc18.Top = 4.583333F;
            this.txtDedDesc18.Width = 2.25F;
            // 
            // txtDedAmount18
            // 
            this.txtDedAmount18.Height = 0.1388889F;
            this.txtDedAmount18.HyperLink = null;
            this.txtDedAmount18.Left = 5.895833F;
            this.txtDedAmount18.Name = "txtDedAmount18";
            this.txtDedAmount18.Style = "text-align: right";
            this.txtDedAmount18.Tag = "text";
            this.txtDedAmount18.Text = "0.00";
            this.txtDedAmount18.Top = 4.583333F;
            this.txtDedAmount18.Width = 0.75F;
            // 
            // txtDedYTD18
            // 
            this.txtDedYTD18.Height = 0.1354167F;
            this.txtDedYTD18.HyperLink = null;
            this.txtDedYTD18.Left = 6.645833F;
            this.txtDedYTD18.Name = "txtDedYTD18";
            this.txtDedYTD18.Style = "text-align: right";
            this.txtDedYTD18.Tag = "text";
            this.txtDedYTD18.Text = "0.00";
            this.txtDedYTD18.Top = 4.583333F;
            this.txtDedYTD18.Width = 0.6875F;
            // 
            // txtDedDesc19
            // 
            this.txtDedDesc19.Height = 0.1354167F;
            this.txtDedDesc19.HyperLink = null;
            this.txtDedDesc19.Left = 3.552083F;
            this.txtDedDesc19.Name = "txtDedDesc19";
            this.txtDedDesc19.Style = "text-align: left";
            this.txtDedDesc19.Tag = "text";
            this.txtDedDesc19.Text = "Deduction Name";
            this.txtDedDesc19.Top = 4.708333F;
            this.txtDedDesc19.Width = 2.25F;
            // 
            // txtDedAmount19
            // 
            this.txtDedAmount19.Height = 0.1388889F;
            this.txtDedAmount19.HyperLink = null;
            this.txtDedAmount19.Left = 5.895833F;
            this.txtDedAmount19.Name = "txtDedAmount19";
            this.txtDedAmount19.Style = "text-align: right";
            this.txtDedAmount19.Tag = "text";
            this.txtDedAmount19.Text = "0.00";
            this.txtDedAmount19.Top = 4.708333F;
            this.txtDedAmount19.Width = 0.75F;
            // 
            // txtDedYTD19
            // 
            this.txtDedYTD19.Height = 0.1354167F;
            this.txtDedYTD19.HyperLink = null;
            this.txtDedYTD19.Left = 6.645833F;
            this.txtDedYTD19.Name = "txtDedYTD19";
            this.txtDedYTD19.Style = "text-align: right";
            this.txtDedYTD19.Tag = "text";
            this.txtDedYTD19.Text = "0.00";
            this.txtDedYTD19.Top = 4.708333F;
            this.txtDedYTD19.Width = 0.6875F;
            // 
            // txtDedDesc20
            // 
            this.txtDedDesc20.Height = 0.1354167F;
            this.txtDedDesc20.HyperLink = null;
            this.txtDedDesc20.Left = 3.552083F;
            this.txtDedDesc20.Name = "txtDedDesc20";
            this.txtDedDesc20.Style = "text-align: left";
            this.txtDedDesc20.Tag = "text";
            this.txtDedDesc20.Text = "Deduction Name";
            this.txtDedDesc20.Top = 4.833333F;
            this.txtDedDesc20.Width = 2.25F;
            // 
            // txtDedAmount20
            // 
            this.txtDedAmount20.Height = 0.1388889F;
            this.txtDedAmount20.HyperLink = null;
            this.txtDedAmount20.Left = 5.895833F;
            this.txtDedAmount20.Name = "txtDedAmount20";
            this.txtDedAmount20.Style = "text-align: right";
            this.txtDedAmount20.Tag = "text";
            this.txtDedAmount20.Text = "0.00";
            this.txtDedAmount20.Top = 4.833333F;
            this.txtDedAmount20.Width = 0.75F;
            // 
            // txtDedYTD20
            // 
            this.txtDedYTD20.Height = 0.1354167F;
            this.txtDedYTD20.HyperLink = null;
            this.txtDedYTD20.Left = 6.645833F;
            this.txtDedYTD20.Name = "txtDedYTD20";
            this.txtDedYTD20.Style = "text-align: right";
            this.txtDedYTD20.Tag = "text";
            this.txtDedYTD20.Text = "0.00";
            this.txtDedYTD20.Top = 4.833333F;
            this.txtDedYTD20.Width = 0.6875F;
            // 
            // Label141
            // 
            this.Label141.Height = 0.1666667F;
            this.Label141.HyperLink = null;
            this.Label141.Left = 0.09375F;
            this.Label141.Name = "Label141";
            this.Label141.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label141.Tag = "text";
            this.Label141.Text = "Benefit";
            this.Label141.Top = 1.961806F;
            this.Label141.Width = 1.4375F;
            // 
            // Label142
            // 
            this.Label142.Height = 0.1666667F;
            this.Label142.HyperLink = null;
            this.Label142.Left = 1.84375F;
            this.Label142.Name = "Label142";
            this.Label142.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label142.Tag = "text";
            this.Label142.Text = "Amount";
            this.Label142.Top = 1.958333F;
            this.Label142.Width = 0.75F;
            // 
            // Label143
            // 
            this.Label143.Height = 0.1666667F;
            this.Label143.HyperLink = null;
            this.Label143.Left = 2.59375F;
            this.Label143.Name = "Label143";
            this.Label143.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label143.Tag = "text";
            this.Label143.Text = "YTD";
            this.Label143.Top = 1.958333F;
            this.Label143.Width = 0.6875F;
            // 
            // lblBenefit1
            // 
            this.lblBenefit1.Height = 0.1388889F;
            this.lblBenefit1.HyperLink = null;
            this.lblBenefit1.Left = 0.09375F;
            this.lblBenefit1.Name = "lblBenefit1";
            this.lblBenefit1.Style = "text-align: left";
            this.lblBenefit1.Tag = "text";
            this.lblBenefit1.Text = "Benefit Name";
            this.lblBenefit1.Top = 2.15625F;
            this.lblBenefit1.Width = 1.4375F;
            // 
            // lblBenAmount1
            // 
            this.lblBenAmount1.Height = 0.1388889F;
            this.lblBenAmount1.HyperLink = null;
            this.lblBenAmount1.Left = 1.84375F;
            this.lblBenAmount1.Name = "lblBenAmount1";
            this.lblBenAmount1.Style = "text-align: right";
            this.lblBenAmount1.Tag = "text";
            this.lblBenAmount1.Text = "0.00";
            this.lblBenAmount1.Top = 2.15625F;
            this.lblBenAmount1.Width = 0.75F;
            // 
            // lblBenYTD1
            // 
            this.lblBenYTD1.Height = 0.1388889F;
            this.lblBenYTD1.HyperLink = null;
            this.lblBenYTD1.Left = 2.59375F;
            this.lblBenYTD1.Name = "lblBenYTD1";
            this.lblBenYTD1.Style = "text-align: right";
            this.lblBenYTD1.Tag = "text";
            this.lblBenYTD1.Text = "0.00";
            this.lblBenYTD1.Top = 2.15625F;
            this.lblBenYTD1.Width = 0.6875F;
            // 
            // lblBenefit2
            // 
            this.lblBenefit2.Height = 0.1388889F;
            this.lblBenefit2.HyperLink = null;
            this.lblBenefit2.Left = 0.09375F;
            this.lblBenefit2.Name = "lblBenefit2";
            this.lblBenefit2.Style = "text-align: left";
            this.lblBenefit2.Tag = "text";
            this.lblBenefit2.Text = "Benefit Name";
            this.lblBenefit2.Top = 2.28125F;
            this.lblBenefit2.Width = 1.4375F;
            // 
            // lblBenAmount2
            // 
            this.lblBenAmount2.Height = 0.1388889F;
            this.lblBenAmount2.HyperLink = null;
            this.lblBenAmount2.Left = 1.84375F;
            this.lblBenAmount2.Name = "lblBenAmount2";
            this.lblBenAmount2.Style = "text-align: right";
            this.lblBenAmount2.Tag = "text";
            this.lblBenAmount2.Text = "0.00";
            this.lblBenAmount2.Top = 2.28125F;
            this.lblBenAmount2.Width = 0.75F;
            // 
            // lblBenYTD2
            // 
            this.lblBenYTD2.Height = 0.1388889F;
            this.lblBenYTD2.HyperLink = null;
            this.lblBenYTD2.Left = 2.59375F;
            this.lblBenYTD2.Name = "lblBenYTD2";
            this.lblBenYTD2.Style = "text-align: right";
            this.lblBenYTD2.Tag = "text";
            this.lblBenYTD2.Text = "0.00";
            this.lblBenYTD2.Top = 2.28125F;
            this.lblBenYTD2.Width = 0.6875F;
            // 
            // lblBenefit3
            // 
            this.lblBenefit3.Height = 0.1388889F;
            this.lblBenefit3.HyperLink = null;
            this.lblBenefit3.Left = 0.09375F;
            this.lblBenefit3.Name = "lblBenefit3";
            this.lblBenefit3.Style = "text-align: left";
            this.lblBenefit3.Tag = "text";
            this.lblBenefit3.Text = "Benefit Name";
            this.lblBenefit3.Top = 2.40625F;
            this.lblBenefit3.Width = 1.4375F;
            // 
            // lblBenAmount3
            // 
            this.lblBenAmount3.Height = 0.1388889F;
            this.lblBenAmount3.HyperLink = null;
            this.lblBenAmount3.Left = 1.84375F;
            this.lblBenAmount3.Name = "lblBenAmount3";
            this.lblBenAmount3.Style = "text-align: right";
            this.lblBenAmount3.Tag = "text";
            this.lblBenAmount3.Text = "0.00";
            this.lblBenAmount3.Top = 2.40625F;
            this.lblBenAmount3.Width = 0.75F;
            // 
            // lblBenYTD3
            // 
            this.lblBenYTD3.Height = 0.1388889F;
            this.lblBenYTD3.HyperLink = null;
            this.lblBenYTD3.Left = 2.59375F;
            this.lblBenYTD3.Name = "lblBenYTD3";
            this.lblBenYTD3.Style = "text-align: right";
            this.lblBenYTD3.Tag = "text";
            this.lblBenYTD3.Text = "0.00";
            this.lblBenYTD3.Top = 2.40625F;
            this.lblBenYTD3.Width = 0.6875F;
            // 
            // lblBenefit4
            // 
            this.lblBenefit4.Height = 0.1388889F;
            this.lblBenefit4.HyperLink = null;
            this.lblBenefit4.Left = 0.09375F;
            this.lblBenefit4.Name = "lblBenefit4";
            this.lblBenefit4.Style = "text-align: left";
            this.lblBenefit4.Tag = "text";
            this.lblBenefit4.Text = "Benefit Name";
            this.lblBenefit4.Top = 2.53125F;
            this.lblBenefit4.Width = 1.4375F;
            // 
            // lblBenAmount4
            // 
            this.lblBenAmount4.Height = 0.1388889F;
            this.lblBenAmount4.HyperLink = null;
            this.lblBenAmount4.Left = 1.84375F;
            this.lblBenAmount4.Name = "lblBenAmount4";
            this.lblBenAmount4.Style = "text-align: right";
            this.lblBenAmount4.Tag = "text";
            this.lblBenAmount4.Text = "0.00";
            this.lblBenAmount4.Top = 2.53125F;
            this.lblBenAmount4.Width = 0.75F;
            // 
            // lblBenYTD4
            // 
            this.lblBenYTD4.Height = 0.1388889F;
            this.lblBenYTD4.HyperLink = null;
            this.lblBenYTD4.Left = 2.59375F;
            this.lblBenYTD4.Name = "lblBenYTD4";
            this.lblBenYTD4.Style = "text-align: right";
            this.lblBenYTD4.Tag = "text";
            this.lblBenYTD4.Text = "0.00";
            this.lblBenYTD4.Top = 2.53125F;
            this.lblBenYTD4.Width = 0.6875F;
            // 
            // lblBenefit5
            // 
            this.lblBenefit5.Height = 0.1388889F;
            this.lblBenefit5.HyperLink = null;
            this.lblBenefit5.Left = 0.09375F;
            this.lblBenefit5.Name = "lblBenefit5";
            this.lblBenefit5.Style = "text-align: left";
            this.lblBenefit5.Tag = "text";
            this.lblBenefit5.Text = "Benefit Name";
            this.lblBenefit5.Top = 2.65625F;
            this.lblBenefit5.Width = 1.4375F;
            // 
            // lblBenAmount5
            // 
            this.lblBenAmount5.Height = 0.1388889F;
            this.lblBenAmount5.HyperLink = null;
            this.lblBenAmount5.Left = 1.84375F;
            this.lblBenAmount5.Name = "lblBenAmount5";
            this.lblBenAmount5.Style = "text-align: right";
            this.lblBenAmount5.Tag = "text";
            this.lblBenAmount5.Text = "0.00";
            this.lblBenAmount5.Top = 2.65625F;
            this.lblBenAmount5.Width = 0.75F;
            // 
            // lblBenYTD5
            // 
            this.lblBenYTD5.Height = 0.1388889F;
            this.lblBenYTD5.HyperLink = null;
            this.lblBenYTD5.Left = 2.59375F;
            this.lblBenYTD5.Name = "lblBenYTD5";
            this.lblBenYTD5.Style = "text-align: right";
            this.lblBenYTD5.Tag = "text";
            this.lblBenYTD5.Text = "0.00";
            this.lblBenYTD5.Top = 2.65625F;
            this.lblBenYTD5.Width = 0.6875F;
            // 
            // lblBenefit6
            // 
            this.lblBenefit6.Height = 0.1388889F;
            this.lblBenefit6.HyperLink = null;
            this.lblBenefit6.Left = 0.09375F;
            this.lblBenefit6.Name = "lblBenefit6";
            this.lblBenefit6.Style = "text-align: left";
            this.lblBenefit6.Tag = "text";
            this.lblBenefit6.Text = "Benefit Name";
            this.lblBenefit6.Top = 2.78125F;
            this.lblBenefit6.Width = 1.4375F;
            // 
            // lblBenAmount6
            // 
            this.lblBenAmount6.Height = 0.1388889F;
            this.lblBenAmount6.HyperLink = null;
            this.lblBenAmount6.Left = 1.84375F;
            this.lblBenAmount6.Name = "lblBenAmount6";
            this.lblBenAmount6.Style = "text-align: right";
            this.lblBenAmount6.Tag = "text";
            this.lblBenAmount6.Text = "0.00";
            this.lblBenAmount6.Top = 2.78125F;
            this.lblBenAmount6.Width = 0.75F;
            // 
            // lblBenYTD6
            // 
            this.lblBenYTD6.Height = 0.1388889F;
            this.lblBenYTD6.HyperLink = null;
            this.lblBenYTD6.Left = 2.59375F;
            this.lblBenYTD6.Name = "lblBenYTD6";
            this.lblBenYTD6.Style = "text-align: right";
            this.lblBenYTD6.Tag = "text";
            this.lblBenYTD6.Text = "0.00";
            this.lblBenYTD6.Top = 2.78125F;
            this.lblBenYTD6.Width = 0.6875F;
            // 
            // lblBenefit7
            // 
            this.lblBenefit7.Height = 0.1388889F;
            this.lblBenefit7.HyperLink = null;
            this.lblBenefit7.Left = 0.09375F;
            this.lblBenefit7.Name = "lblBenefit7";
            this.lblBenefit7.Style = "text-align: left";
            this.lblBenefit7.Tag = "text";
            this.lblBenefit7.Text = "Benefit Name";
            this.lblBenefit7.Top = 2.90625F;
            this.lblBenefit7.Width = 1.4375F;
            // 
            // lblBenAmount7
            // 
            this.lblBenAmount7.Height = 0.1388889F;
            this.lblBenAmount7.HyperLink = null;
            this.lblBenAmount7.Left = 1.84375F;
            this.lblBenAmount7.Name = "lblBenAmount7";
            this.lblBenAmount7.Style = "text-align: right";
            this.lblBenAmount7.Tag = "text";
            this.lblBenAmount7.Text = "0.00";
            this.lblBenAmount7.Top = 2.90625F;
            this.lblBenAmount7.Width = 0.75F;
            // 
            // lblBenYTD7
            // 
            this.lblBenYTD7.Height = 0.1388889F;
            this.lblBenYTD7.HyperLink = null;
            this.lblBenYTD7.Left = 2.59375F;
            this.lblBenYTD7.Name = "lblBenYTD7";
            this.lblBenYTD7.Style = "text-align: right";
            this.lblBenYTD7.Tag = "text";
            this.lblBenYTD7.Text = "0.00";
            this.lblBenYTD7.Top = 2.90625F;
            this.lblBenYTD7.Width = 0.6875F;
            // 
            // lblBenefit8
            // 
            this.lblBenefit8.Height = 0.1388889F;
            this.lblBenefit8.HyperLink = null;
            this.lblBenefit8.Left = 0.09375F;
            this.lblBenefit8.Name = "lblBenefit8";
            this.lblBenefit8.Style = "text-align: left";
            this.lblBenefit8.Tag = "text";
            this.lblBenefit8.Text = "Benefit Name";
            this.lblBenefit8.Top = 3.03125F;
            this.lblBenefit8.Width = 1.4375F;
            // 
            // lblBenAmount8
            // 
            this.lblBenAmount8.Height = 0.1388889F;
            this.lblBenAmount8.HyperLink = null;
            this.lblBenAmount8.Left = 1.84375F;
            this.lblBenAmount8.Name = "lblBenAmount8";
            this.lblBenAmount8.Style = "text-align: right";
            this.lblBenAmount8.Tag = "text";
            this.lblBenAmount8.Text = "0.00";
            this.lblBenAmount8.Top = 3.03125F;
            this.lblBenAmount8.Width = 0.75F;
            // 
            // lblBenYTD8
            // 
            this.lblBenYTD8.Height = 0.1388889F;
            this.lblBenYTD8.HyperLink = null;
            this.lblBenYTD8.Left = 2.59375F;
            this.lblBenYTD8.Name = "lblBenYTD8";
            this.lblBenYTD8.Style = "text-align: right";
            this.lblBenYTD8.Tag = "text";
            this.lblBenYTD8.Text = "0.00";
            this.lblBenYTD8.Top = 3.03125F;
            this.lblBenYTD8.Width = 0.6875F;
            // 
            // lblBenefit9
            // 
            this.lblBenefit9.Height = 0.1388889F;
            this.lblBenefit9.HyperLink = null;
            this.lblBenefit9.Left = 0.09375F;
            this.lblBenefit9.Name = "lblBenefit9";
            this.lblBenefit9.Style = "text-align: left";
            this.lblBenefit9.Tag = "text";
            this.lblBenefit9.Text = "Benefit Name";
            this.lblBenefit9.Top = 3.15625F;
            this.lblBenefit9.Width = 1.4375F;
            // 
            // lblBenAmount9
            // 
            this.lblBenAmount9.Height = 0.1388889F;
            this.lblBenAmount9.HyperLink = null;
            this.lblBenAmount9.Left = 1.84375F;
            this.lblBenAmount9.Name = "lblBenAmount9";
            this.lblBenAmount9.Style = "text-align: right";
            this.lblBenAmount9.Tag = "text";
            this.lblBenAmount9.Text = "0.00";
            this.lblBenAmount9.Top = 3.15625F;
            this.lblBenAmount9.Width = 0.75F;
            // 
            // lblBenYTD9
            // 
            this.lblBenYTD9.Height = 0.1388889F;
            this.lblBenYTD9.HyperLink = null;
            this.lblBenYTD9.Left = 2.59375F;
            this.lblBenYTD9.Name = "lblBenYTD9";
            this.lblBenYTD9.Style = "text-align: right";
            this.lblBenYTD9.Tag = "text";
            this.lblBenYTD9.Text = "0.00";
            this.lblBenYTD9.Top = 3.15625F;
            this.lblBenYTD9.Width = 0.6875F;
            // 
            // lblBenefit10
            // 
            this.lblBenefit10.Height = 0.1388889F;
            this.lblBenefit10.HyperLink = null;
            this.lblBenefit10.Left = 0.09375F;
            this.lblBenefit10.Name = "lblBenefit10";
            this.lblBenefit10.Style = "text-align: left";
            this.lblBenefit10.Tag = "text";
            this.lblBenefit10.Text = "Benefit Name";
            this.lblBenefit10.Top = 3.28125F;
            this.lblBenefit10.Width = 1.4375F;
            // 
            // lblBenAmount10
            // 
            this.lblBenAmount10.Height = 0.1388889F;
            this.lblBenAmount10.HyperLink = null;
            this.lblBenAmount10.Left = 1.84375F;
            this.lblBenAmount10.Name = "lblBenAmount10";
            this.lblBenAmount10.Style = "text-align: right";
            this.lblBenAmount10.Tag = "text";
            this.lblBenAmount10.Text = "0.00";
            this.lblBenAmount10.Top = 3.28125F;
            this.lblBenAmount10.Width = 0.75F;
            // 
            // lblBenYTD10
            // 
            this.lblBenYTD10.Height = 0.1388889F;
            this.lblBenYTD10.HyperLink = null;
            this.lblBenYTD10.Left = 2.59375F;
            this.lblBenYTD10.Name = "lblBenYTD10";
            this.lblBenYTD10.Style = "text-align: right";
            this.lblBenYTD10.Tag = "text";
            this.lblBenYTD10.Text = "0.00";
            this.lblBenYTD10.Top = 3.28125F;
            this.lblBenYTD10.Width = 0.6875F;
            // 
            // lblBenefit11
            // 
            this.lblBenefit11.Height = 0.1388889F;
            this.lblBenefit11.HyperLink = null;
            this.lblBenefit11.Left = 0.09375F;
            this.lblBenefit11.Name = "lblBenefit11";
            this.lblBenefit11.Style = "text-align: left";
            this.lblBenefit11.Tag = "text";
            this.lblBenefit11.Text = "Benefit Name";
            this.lblBenefit11.Top = 3.40625F;
            this.lblBenefit11.Width = 1.4375F;
            // 
            // lblBenAmount11
            // 
            this.lblBenAmount11.Height = 0.1388889F;
            this.lblBenAmount11.HyperLink = null;
            this.lblBenAmount11.Left = 1.84375F;
            this.lblBenAmount11.Name = "lblBenAmount11";
            this.lblBenAmount11.Style = "text-align: right";
            this.lblBenAmount11.Tag = "text";
            this.lblBenAmount11.Text = "0.00";
            this.lblBenAmount11.Top = 3.40625F;
            this.lblBenAmount11.Width = 0.75F;
            // 
            // lblBenYTD11
            // 
            this.lblBenYTD11.Height = 0.1388889F;
            this.lblBenYTD11.HyperLink = null;
            this.lblBenYTD11.Left = 2.59375F;
            this.lblBenYTD11.Name = "lblBenYTD11";
            this.lblBenYTD11.Style = "text-align: right";
            this.lblBenYTD11.Tag = "text";
            this.lblBenYTD11.Text = "0.00";
            this.lblBenYTD11.Top = 3.40625F;
            this.lblBenYTD11.Width = 0.6875F;
            // 
            // lblBenefit12
            // 
            this.lblBenefit12.Height = 0.1388889F;
            this.lblBenefit12.HyperLink = null;
            this.lblBenefit12.Left = 0.09375F;
            this.lblBenefit12.Name = "lblBenefit12";
            this.lblBenefit12.Style = "text-align: left";
            this.lblBenefit12.Tag = "text";
            this.lblBenefit12.Text = "Benefit Name";
            this.lblBenefit12.Top = 3.53125F;
            this.lblBenefit12.Width = 1.4375F;
            // 
            // lblBenAmount12
            // 
            this.lblBenAmount12.Height = 0.1388889F;
            this.lblBenAmount12.HyperLink = null;
            this.lblBenAmount12.Left = 1.84375F;
            this.lblBenAmount12.Name = "lblBenAmount12";
            this.lblBenAmount12.Style = "text-align: right";
            this.lblBenAmount12.Tag = "text";
            this.lblBenAmount12.Text = "0.00";
            this.lblBenAmount12.Top = 3.53125F;
            this.lblBenAmount12.Width = 0.75F;
            // 
            // lblBenYTD12
            // 
            this.lblBenYTD12.Height = 0.1388889F;
            this.lblBenYTD12.HyperLink = null;
            this.lblBenYTD12.Left = 2.59375F;
            this.lblBenYTD12.Name = "lblBenYTD12";
            this.lblBenYTD12.Style = "text-align: right";
            this.lblBenYTD12.Tag = "text";
            this.lblBenYTD12.Text = "0.00";
            this.lblBenYTD12.Top = 3.53125F;
            this.lblBenYTD12.Width = 0.6875F;
            // 
            // txtEMatchCurrent
            // 
            this.txtEMatchCurrent.Height = 0.1354167F;
            this.txtEMatchCurrent.HyperLink = null;
            this.txtEMatchCurrent.Left = 1.84375F;
            this.txtEMatchCurrent.Name = "txtEMatchCurrent";
            this.txtEMatchCurrent.Style = "text-align: right";
            this.txtEMatchCurrent.Tag = "text";
            this.txtEMatchCurrent.Text = "0.00";
            this.txtEMatchCurrent.Top = 3.729167F;
            this.txtEMatchCurrent.Width = 0.75F;
            // 
            // txtEmatchYTD
            // 
            this.txtEmatchYTD.Height = 0.1354167F;
            this.txtEmatchYTD.HyperLink = null;
            this.txtEmatchYTD.Left = 2.583333F;
            this.txtEmatchYTD.Name = "txtEmatchYTD";
            this.txtEmatchYTD.Style = "text-align: right";
            this.txtEmatchYTD.Tag = "text";
            this.txtEmatchYTD.Text = "0.00";
            this.txtEmatchYTD.Top = 3.729167F;
            this.txtEmatchYTD.Width = 0.6875F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0.0625F;
            this.Line7.LineWeight = 2F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 3.684028F;
            this.Line7.Width = 3.260417F;
            this.Line7.X1 = 0.0625F;
            this.Line7.X2 = 3.322917F;
            this.Line7.Y1 = 3.684028F;
            this.Line7.Y2 = 3.684028F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0.0625F;
            this.Line8.LineWeight = 2F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 2.138889F;
            this.Line8.Width = 3.260417F;
            this.Line8.X1 = 0.0625F;
            this.Line8.X2 = 3.322917F;
            this.Line8.Y1 = 2.138889F;
            this.Line8.Y2 = 2.138889F;
            // 
            // Label225
            // 
            this.Label225.Height = 0.1666667F;
            this.Label225.HyperLink = null;
            this.Label225.Left = 0.09375F;
            this.Label225.Name = "Label225";
            this.Label225.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
            this.Label225.Tag = "text";
            this.Label225.Text = "Total";
            this.Label225.Top = 3.729167F;
            this.Label225.Width = 1.4375F;
            // 
            // Line14
            // 
            this.Line14.Height = 1.53125F;
            this.Line14.Left = 0.0625F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0.25F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 0.0625F;
            this.Line14.X2 = 0.0625F;
            this.Line14.Y1 = 0.25F;
            this.Line14.Y2 = 1.78125F;
            // 
            // Line15
            // 
            this.Line15.Height = 1.53125F;
            this.Line15.Left = 3.322917F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.25F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 3.322917F;
            this.Line15.X2 = 3.322917F;
            this.Line15.Y1 = 0.25F;
            this.Line15.Y2 = 1.78125F;
            // 
            // Line16
            // 
            this.Line16.Height = 1.545139F;
            this.Line16.Left = 0.0625F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 2.138889F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 0.0625F;
            this.Line16.X2 = 0.0625F;
            this.Line16.Y1 = 2.138889F;
            this.Line16.Y2 = 3.684028F;
            // 
            // Line17
            // 
            this.Line17.Height = 1.545139F;
            this.Line17.Left = 3.322917F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 2.138889F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 3.322917F;
            this.Line17.X2 = 3.322917F;
            this.Line17.Y1 = 2.138889F;
            this.Line17.Y2 = 3.684028F;
            // 
            // Line18
            // 
            this.Line18.Height = 1.083333F;
            this.Line18.Left = 0.0625F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 4.229167F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 0.0625F;
            this.Line18.X2 = 0.0625F;
            this.Line18.Y1 = 4.229167F;
            this.Line18.Y2 = 5.3125F;
            // 
            // Line19
            // 
            this.Line19.Height = 1.083333F;
            this.Line19.Left = 3.322917F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 4.229167F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 3.322917F;
            this.Line19.X2 = 3.322917F;
            this.Line19.Y1 = 4.229167F;
            this.Line19.Y2 = 5.3125F;
            // 
            // Line20
            // 
            this.Line20.Height = 0F;
            this.Line20.Left = 0.0625F;
            this.Line20.LineWeight = 2F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 5.3125F;
            this.Line20.Width = 3.260417F;
            this.Line20.X1 = 0.0625F;
            this.Line20.X2 = 3.322917F;
            this.Line20.Y1 = 5.3125F;
            this.Line20.Y2 = 5.3125F;
            // 
            // Line21
            // 
            this.Line21.Height = 1.53125F;
            this.Line21.Left = 3.520833F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0.25F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 3.520833F;
            this.Line21.X2 = 3.520833F;
            this.Line21.Y1 = 0.25F;
            this.Line21.Y2 = 1.78125F;
            // 
            // Line22
            // 
            this.Line22.Height = 1.53125F;
            this.Line22.Left = 7.40625F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0.25F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 7.40625F;
            this.Line22.X2 = 7.40625F;
            this.Line22.Y1 = 0.25F;
            this.Line22.Y2 = 1.78125F;
            // 
            // Line23
            // 
            this.Line23.Height = 2.597222F;
            this.Line23.Left = 3.520833F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 2.423611F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 3.520833F;
            this.Line23.X2 = 3.520833F;
            this.Line23.Y1 = 2.423611F;
            this.Line23.Y2 = 5.020833F;
            // 
            // Line24
            // 
            this.Line24.Height = 2.597222F;
            this.Line24.Left = 7.40625F;
            this.Line24.LineWeight = 1F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 2.423611F;
            this.Line24.Width = 0F;
            this.Line24.X1 = 7.40625F;
            this.Line24.X2 = 7.40625F;
            this.Line24.Y1 = 2.423611F;
            this.Line24.Y2 = 5.020833F;
            // 
            // Line25
            // 
            this.Line25.Height = 0.5625F;
            this.Line25.Left = 3.520833F;
            this.Line25.LineWeight = 1F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 5.6875F;
            this.Line25.Width = 0F;
            this.Line25.X1 = 3.520833F;
            this.Line25.X2 = 3.520833F;
            this.Line25.Y1 = 5.6875F;
            this.Line25.Y2 = 6.25F;
            // 
            // Line26
            // 
            this.Line26.Height = 0.5625F;
            this.Line26.Left = 7.40625F;
            this.Line26.LineWeight = 1F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 5.6875F;
            this.Line26.Width = 0F;
            this.Line26.X1 = 7.40625F;
            this.Line26.X2 = 7.40625F;
            this.Line26.Y1 = 5.6875F;
            this.Line26.Y2 = 6.25F;
            // 
            // lineDirectDepositBottom
            // 
            this.lineDirectDepositBottom.Height = 0F;
            this.lineDirectDepositBottom.Left = 0.0625F;
            this.lineDirectDepositBottom.LineWeight = 2F;
            this.lineDirectDepositBottom.Name = "lineDirectDepositBottom";
            this.lineDirectDepositBottom.Top = 6.625F;
            this.lineDirectDepositBottom.Width = 3.260417F;
            this.lineDirectDepositBottom.X1 = 0.0625F;
            this.lineDirectDepositBottom.X2 = 3.322917F;
            this.lineDirectDepositBottom.Y1 = 6.625F;
            this.lineDirectDepositBottom.Y2 = 6.625F;
            // 
            // lineDirectDepositLeft
            // 
            this.lineDirectDepositLeft.Height = 0.979167F;
            this.lineDirectDepositLeft.Left = 0.0625F;
            this.lineDirectDepositLeft.LineWeight = 1F;
            this.lineDirectDepositLeft.Name = "lineDirectDepositLeft";
            this.lineDirectDepositLeft.Top = 5.645833F;
            this.lineDirectDepositLeft.Width = 0F;
            this.lineDirectDepositLeft.X1 = 0.0625F;
            this.lineDirectDepositLeft.X2 = 0.0625F;
            this.lineDirectDepositLeft.Y1 = 5.645833F;
            this.lineDirectDepositLeft.Y2 = 6.625F;
            // 
            // lineDirectDepositRight
            // 
            this.lineDirectDepositRight.Height = 0.979167F;
            this.lineDirectDepositRight.Left = 3.322917F;
            this.lineDirectDepositRight.LineWeight = 1F;
            this.lineDirectDepositRight.Name = "lineDirectDepositRight";
            this.lineDirectDepositRight.Top = 5.645833F;
            this.lineDirectDepositRight.Width = 0F;
            this.lineDirectDepositRight.X1 = 3.322917F;
            this.lineDirectDepositRight.X2 = 3.322917F;
            this.lineDirectDepositRight.Y1 = 5.645833F;
            this.lineDirectDepositRight.Y2 = 6.625F;
            // 
            // lblIndRate
            // 
            this.lblIndRate.Height = 0.1666667F;
            this.lblIndRate.HyperLink = null;
            this.lblIndRate.Left = 5.270833F;
            this.lblIndRate.Name = "lblIndRate";
            this.lblIndRate.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblIndRate.Tag = "text";
            this.lblIndRate.Text = "Rate";
            this.lblIndRate.Top = 0.08333334F;
            this.lblIndRate.Width = 0.6875F;
            // 
            // txtRate1
            // 
            this.txtRate1.Height = 0.1354167F;
            this.txtRate1.HyperLink = null;
            this.txtRate1.Left = 5.270833F;
            this.txtRate1.Name = "txtRate1";
            this.txtRate1.Style = "text-align: right";
            this.txtRate1.Tag = "text";
            this.txtRate1.Text = "0.00";
            this.txtRate1.Top = 0.25F;
            this.txtRate1.Width = 0.6875F;
            // 
            // txtRate2
            // 
            this.txtRate2.Height = 0.1354167F;
            this.txtRate2.HyperLink = null;
            this.txtRate2.Left = 5.270833F;
            this.txtRate2.Name = "txtRate2";
            this.txtRate2.Style = "text-align: right";
            this.txtRate2.Tag = "text";
            this.txtRate2.Text = "0.00";
            this.txtRate2.Top = 0.375F;
            this.txtRate2.Width = 0.6875F;
            // 
            // txtRate3
            // 
            this.txtRate3.Height = 0.1354167F;
            this.txtRate3.HyperLink = null;
            this.txtRate3.Left = 5.270833F;
            this.txtRate3.Name = "txtRate3";
            this.txtRate3.Style = "text-align: right";
            this.txtRate3.Tag = "text";
            this.txtRate3.Text = "0.00";
            this.txtRate3.Top = 0.5F;
            this.txtRate3.Width = 0.6875F;
            // 
            // txtRate4
            // 
            this.txtRate4.Height = 0.1354167F;
            this.txtRate4.HyperLink = null;
            this.txtRate4.Left = 5.270833F;
            this.txtRate4.Name = "txtRate4";
            this.txtRate4.Style = "text-align: right";
            this.txtRate4.Tag = "text";
            this.txtRate4.Text = "0.00";
            this.txtRate4.Top = 0.625F;
            this.txtRate4.Width = 0.6875F;
            // 
            // txtRate5
            // 
            this.txtRate5.Height = 0.1354167F;
            this.txtRate5.HyperLink = null;
            this.txtRate5.Left = 5.270833F;
            this.txtRate5.Name = "txtRate5";
            this.txtRate5.Style = "text-align: right";
            this.txtRate5.Tag = "text";
            this.txtRate5.Text = "0.00";
            this.txtRate5.Top = 0.75F;
            this.txtRate5.Width = 0.6875F;
            // 
            // txtRate6
            // 
            this.txtRate6.Height = 0.1354167F;
            this.txtRate6.HyperLink = null;
            this.txtRate6.Left = 5.270833F;
            this.txtRate6.Name = "txtRate6";
            this.txtRate6.Style = "text-align: right";
            this.txtRate6.Tag = "text";
            this.txtRate6.Text = "0.00";
            this.txtRate6.Top = 0.875F;
            this.txtRate6.Width = 0.6875F;
            // 
            // txtRate7
            // 
            this.txtRate7.Height = 0.1354167F;
            this.txtRate7.HyperLink = null;
            this.txtRate7.Left = 5.270833F;
            this.txtRate7.Name = "txtRate7";
            this.txtRate7.Style = "text-align: right";
            this.txtRate7.Tag = "text";
            this.txtRate7.Text = "0.00";
            this.txtRate7.Top = 1F;
            this.txtRate7.Width = 0.6875F;
            // 
            // txtRate8
            // 
            this.txtRate8.Height = 0.1354167F;
            this.txtRate8.HyperLink = null;
            this.txtRate8.Left = 5.270833F;
            this.txtRate8.Name = "txtRate8";
            this.txtRate8.Style = "text-align: right";
            this.txtRate8.Tag = "text";
            this.txtRate8.Text = "0.00";
            this.txtRate8.Top = 1.125F;
            this.txtRate8.Width = 0.6875F;
            // 
            // txtRate9
            // 
            this.txtRate9.Height = 0.1354167F;
            this.txtRate9.HyperLink = null;
            this.txtRate9.Left = 5.270833F;
            this.txtRate9.Name = "txtRate9";
            this.txtRate9.Style = "text-align: right";
            this.txtRate9.Tag = "text";
            this.txtRate9.Text = "0.00";
            this.txtRate9.Top = 1.25F;
            this.txtRate9.Width = 0.6875F;
            // 
            // txtRate10
            // 
            this.txtRate10.Height = 0.1354167F;
            this.txtRate10.HyperLink = null;
            this.txtRate10.Left = 5.270833F;
            this.txtRate10.Name = "txtRate10";
            this.txtRate10.Style = "text-align: right";
            this.txtRate10.Tag = "text";
            this.txtRate10.Text = "0.00";
            this.txtRate10.Top = 1.375F;
            this.txtRate10.Width = 0.6875F;
            // 
            // txtRate11
            // 
            this.txtRate11.Height = 0.1354167F;
            this.txtRate11.HyperLink = null;
            this.txtRate11.Left = 5.270833F;
            this.txtRate11.Name = "txtRate11";
            this.txtRate11.Style = "text-align: right";
            this.txtRate11.Tag = "text";
            this.txtRate11.Text = "0.00";
            this.txtRate11.Top = 1.5F;
            this.txtRate11.Width = 0.6875F;
            // 
            // txtRate12
            // 
            this.txtRate12.Height = 0.1354167F;
            this.txtRate12.HyperLink = null;
            this.txtRate12.Left = 5.270833F;
            this.txtRate12.Name = "txtRate12";
            this.txtRate12.Style = "text-align: right";
            this.txtRate12.Tag = "text";
            this.txtRate12.Text = "0.00";
            this.txtRate12.Top = 1.625F;
            this.txtRate12.Width = 0.6875F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 3.520833F;
            this.Line6.LineWeight = 2F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.25F;
            this.Line6.Width = 3.885417F;
            this.Line6.X1 = 3.520833F;
            this.Line6.X2 = 7.40625F;
            this.Line6.Y1 = 0.25F;
            this.Line6.Y2 = 0.25F;
            // 
            // srptNewDBLLaserStub2
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.447917F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentGross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentFed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentFica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFICA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationAccrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickAccrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPayRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFedTaxable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDFicaTaxable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDStateTaxable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVacationBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSickBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentMedicare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDMedicare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDMedicareTaxable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalYTDDeductions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label206)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Accrued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Taken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6Balance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYTDGross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDeposit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepChkLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepSav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirectDepositSavLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartmentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAmount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDesc12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayAmount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedDesc20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedAmount20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDedYTD20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenefit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenAmount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBenYTD12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMatchCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmatchYTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIndRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployeeNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFICA;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDState;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationAccrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickAccrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode4Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFedTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDFicaTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDStateTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationUsed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickTaken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode4Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtVacationBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSickBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode4Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCurrentMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDMedicareTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD14;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD16;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalYTDDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label206;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label209;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label210;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label223;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label224;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label226;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label227;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label230;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode5Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode6Accrued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode5Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode6Taken;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode5Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode6Balance;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYTDGross;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDeposit;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepChkLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepSav;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDirectDepositSavLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDepartmentLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDepartment;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBank6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayDesc12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtHours12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPayAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD17;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD18;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedDesc20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedAmount20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDedYTD20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenefit12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBenYTD12;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEMatchCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmatchYTD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label225;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line lineDirectDepositBottom;
		private GrapeCity.ActiveReports.SectionReportModel.Line lineDirectDepositLeft;
		private GrapeCity.ActiveReports.SectionReportModel.Line lineDirectDepositRight;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIndRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate9;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
	}
}
