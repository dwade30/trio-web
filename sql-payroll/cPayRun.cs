﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cPayRun
	{
		//=========================================================
		private DateTime dtPeriodStart;
		private DateTime dtPeriodEnd;
		private DateTime dtPayDate;
		private int intPayrunID;
		private int lngBankID;

		public int BankID
		{
			set
			{
				lngBankID = value;
			}
			get
			{
				int BankID = 0;
				BankID = lngBankID;
				return BankID;
			}
		}

		public DateTime PeriodStart
		{
			set
			{
				dtPeriodStart = value;
			}
			get
			{
				DateTime PeriodStart = System.DateTime.Now;
				PeriodStart = dtPeriodStart;
				return PeriodStart;
			}
		}

		public DateTime PeriodEnd
		{
			set
			{
				dtPeriodEnd = value;
			}
			get
			{
				DateTime PeriodEnd = System.DateTime.Now;
				PeriodEnd = dtPeriodEnd;
				return PeriodEnd;
			}
		}

		public DateTime PayDate
		{
			set
			{
				dtPayDate = value;
			}
			get
			{
				DateTime PayDate = System.DateTime.Now;
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public int PayRunID
		{
			set
			{
				intPayrunID = value;
			}
			get
			{
				int PayRunID = 0;
				PayRunID = intPayrunID;
				return PayRunID;
			}
		}
	}
}
