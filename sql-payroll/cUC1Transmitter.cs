﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cUC1Transmitter
	{
		//=========================================================
		private string strTitle = string.Empty;
		private string strEmail = string.Empty;
		private string strPhone = string.Empty;
		private string strExt = string.Empty;

		public string Title
		{
			set
			{
				strTitle = value;
			}
			get
			{
				string Title = "";
				Title = strTitle;
				return Title;
			}
		}

		public string Email
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
		}

		public string Phone
		{
			set
			{
				strPhone = value;
			}
			get
			{
				string Phone = "";
				Phone = strPhone;
				return Phone;
			}
		}

		public string Extension
		{
			set
			{
				strExt = value;
			}
			get
			{
				string Extension = "";
				Extension = strExt;
				return Extension;
			}
		}
	}
}
