//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmACH : BaseForm
	{
		public frmACH()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmACH InstancePtr
		{
			get
			{
				return (frmACH)Sys.GetInstance(typeof(frmACH));
			}
		}

		protected frmACH _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmACH_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmACH_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmACH properties;
			//frmACH.FillStyle	= 0;
			//frmACH.ScaleWidth	= 5880;
			//frmACH.ScaleHeight	= 4980;
			//frmACH.LinkTopic	= "Form2";
			//frmACH.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			cmbAccountType.Clear();
			cmbAccountType.AddItem("Checking");
			cmbAccountType.ItemData(0, 27);
			cmbAccountType.AddItem("Savings");
			cmbAccountType.ItemData(1, 37);
			LoadInfo();
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// THIS WILL L
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select * from tblachinformation", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					txtBankName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("achbankname")));
					txtACHRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("achbankrt")));
					txtEmployerName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employername")));
					txtEmpRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerrt")));
					txtEmpAccount.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employeraccount")));
					txtEmployerID.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerid")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginname"))) != string.Empty)
					{
						txtImmediateOriginName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginname")));
					}
					else
					{
						txtImmediateOriginName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employername")));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginrt"))) != string.Empty)
					{
						txtImmediateOriginRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginrt")));
					}
					else
					{
						txtImmediateOriginRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerrt")));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ODFINum"))) != string.Empty)
					{
						txtODFI.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("odfinum")));
					}
					else
					{
						txtODFI.Text = txtImmediateOriginRT.Text;
					}
					int intTemp = 0;
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("employerAccountType"))));
					if (intTemp < 20)
					{
						intTemp = 27;
					}
					if (intTemp == 37)
					{
						cmbAccountType.SelectedIndex = 1;
					}
					else
					{
						cmbAccountType.SelectedIndex = 0;
					}
				}
				else
				{
					clsLoad.OpenRecordset("select * from GLOBALVARIABLES", "SystemSettings");
					txtEmployerName.Text = clsLoad.Get_Fields("citytown") + " of " + clsLoad.Get_Fields("muniname");
					cmbAccountType.SelectedIndex = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click()
		{
			if (SaveInfo())
			{
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				clsSave.OpenRecordset("select * from tblACHInformation", "twpy0000.vb1");
				if (clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				strTemp = fecherFoundation.Strings.Trim(txtBankName.Text);
				if (strTemp == string.Empty)
				{
					MessageBox.Show("You must provide a bank name");
					txtBankName.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("EmployerAccountType", cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
				// pad with spaces to the correct length
				strTemp += Strings.StrDup(modCoreysSweeterCode.ACHNameLen, " ");
				strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHNameLen);
				clsSave.Set_Fields("achbankname", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtACHRT.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHRTLen)
				{
					MessageBox.Show("You must provide an ACH RT of length " + FCConvert.ToString(modCoreysSweeterCode.ACHRTLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtACHRT.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("achbankrt", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtEmployerName.Text);
				if (strTemp == string.Empty)
				{
					MessageBox.Show("You must provide an employer name");
					txtEmployerName.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				strTemp += Strings.StrDup(modCoreysSweeterCode.ACHEmployerNameLen, " ");
				strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHEmployerNameLen);
				clsSave.Set_Fields("employername", strTemp);
				if (fecherFoundation.Strings.Trim(txtImmediateOriginName.Text) == string.Empty)
				{
					clsSave.Set_Fields("ImmediateOriginName", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtImmediateOriginName.Text);
					strTemp += Strings.StrDup(modCoreysSweeterCode.ACHNameLen, " ");
					strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHNameLen);
					clsSave.Set_Fields("ImmediateOriginName", strTemp);
				}
				strTemp = fecherFoundation.Strings.Trim(txtEmpRT.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHEmployerRTLen)
				{
					MessageBox.Show("You must provide an Employer RT of length " + FCConvert.ToString(modCoreysSweeterCode.ACHEmployerRTLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmpRT.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("employerrt", strTemp);
				if (fecherFoundation.Strings.Trim(txtImmediateOriginRT.Text) == string.Empty)
				{
					clsSave.Set_Fields("ImmediateOriginRT", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtImmediateOriginRT.Text);
					if (strTemp.Length < 10)
					{
						strTemp += Strings.StrDup(modCoreysSweeterCode.ACHRTLen, " ");
						strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHRTLen);
					}
					clsSave.Set_Fields("ImmediateOriginRT", strTemp);
				}
				if (fecherFoundation.Strings.Trim(txtODFI.Text) == string.Empty)
				{
					clsSave.Set_Fields("odfinum", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtODFI.Text);
					strTemp += Strings.StrDup(modCoreysSweeterCode.ACHRTLen, " ");
					strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHRTLen);
					clsSave.Set_Fields("ODFINum", strTemp);
				}
				strTemp = fecherFoundation.Strings.Trim(txtEmpAccount.Text);
				if (strTemp.Length < 1)
				{
					// If Len(strTemp) <> ACHEmployerAccountLen Then
					MessageBox.Show("You must provide an employer account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// MsgBox "You must provide an Employer Account # of length " & ACHEmployerAccountLen, vbExclamation
					txtEmpAccount.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("EmployerAccount", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtEmployerID.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHEmployerIDLen)
				{
					MessageBox.Show("You must provide an Employer ID of length " + FCConvert.ToString(modCoreysSweeterCode.ACHEmployerIDLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerID.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("employerid", strTemp);
				clsSave.Update();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Close();
			}
		}
	}
}
