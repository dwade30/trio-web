﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmailList.
	/// </summary>
	public partial class rptEmailList : BaseSectionReport
	{
		public rptEmailList()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Emailed Stubs";
        }

		protected override void Dispose(bool disposing)
		{
            base.Dispose(disposing);
		}

		private cEmployeeChecksReport theReport;

		public void Init(ref cEmployeeChecksReport checksReport)
		{
			theReport = checksReport;
            frmReportViewer.InstancePtr.Init(this);
        }

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			if (!(theReport == null))
			{
				theReport.employeeChecks.MoveFirst();
				txtCaption.Text = "Emails sent for pay date " + FCConvert.ToString(theReport.PayDate) + " run " + FCConvert.ToString(theReport.PayrunNumber);
				txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				txtMuniName.Text = modGlobalConstants.Statics.MuniName;
				txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !theReport.employeeChecks.IsCurrent();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (theReport.employeeChecks.IsCurrent())
			{
				cEmployeeCheck eCheck;
				eCheck = (cEmployeeCheck)theReport.employeeChecks.GetCurrentItem();
				txtAmount.Text = Strings.Format(eCheck.Net, "0.00");
				if (eCheck.Net > 0 && eCheck.CheckAmount == 0)
				{
					txtCheck.Text = "D" + FCConvert.ToString(eCheck.CheckNumber);
				}
				else
				{
					txtCheck.Text = FCConvert.ToString(eCheck.CheckNumber);
				}
				txtEmployeeNumber.Text = eCheck.EmployeeNumber;
				txtEmployeeName.Text = eCheck.EmployeeName;
				txtEmail.Text = eCheck.Employee.Email;
				theReport.employeeChecks.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}

		
	}
}
