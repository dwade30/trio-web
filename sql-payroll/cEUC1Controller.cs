﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Wisej.Web;
using OfficeOpenXml;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public class cEUC1Controller
	{
		//=========================================================
		private const string CNSTSheetName = "Sheet1";

		public bool CreateFile(string strFileName, ref cUC1Report ucRep)
		{
			try
            {
                using (var fs = new FileStream(strFileName, FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new HSSFWorkbook();
                    ISheet excelSheet = workbook.CreateSheet(CNSTSheetName);

                    List<String> columns = new List<string>();
                    IRow row = excelSheet.CreateRow(0);
                    int columnIndex;

                    for (columnIndex = 0; columnIndex < 5; columnIndex++)
                    {
                        columns.Add("");
                    }

                    int rowIndex = 0;

                    ucRep.Details.MoveFirst();

                    while (ucRep.Details.IsCurrent())
                    {
                        var schedDet = ucRep.Details.GetCurrentDetail();

                        row = excelSheet.CreateRow(rowIndex);

                        row.CreateCell(0, CellType.String).SetCellValue(schedDet.SSN);
                        row.CreateCell(1, CellType.Numeric).SetCellValue(schedDet.UCWages);
                        row.CreateCell(2, CellType.String).SetCellValue("");
                        row.CreateCell(3, CellType.String).SetCellValue(schedDet.LastFirstNoSuffix + new string(' ', 70).Left(70).Trim());
                        row.CreateCell(4, CellType.String).SetCellValue("");

                        rowIndex++;
                        ucRep.Details.MoveNext();
                    }

                    workbook.Write(fs);
                    fs.Close();
                }

                return true;
            }
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
    }
}
