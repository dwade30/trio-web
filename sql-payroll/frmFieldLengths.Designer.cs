//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmFieldLengths.
	/// </summary>
	partial class frmFieldLengths
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label4;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtDivLength;
		public fecherFoundation.FCTextBox txtDeptLength;
		public fecherFoundation.FCLabel Label4_2;
		public fecherFoundation.FCLabel Label4_1;
		public fecherFoundation.FCLabel Label4_0;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkBiWeekly;
		public fecherFoundation.FCCheckBox chkWeekly;
		public fecherFoundation.FCComboBox cboType;
		public fecherFoundation.FCTextBox txtEmployeeNumber;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdExit;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame5 = new fecherFoundation.FCFrame();
            this.cboMonth = new fecherFoundation.FCComboBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtDivLength = new fecherFoundation.FCTextBox();
            this.txtDeptLength = new fecherFoundation.FCTextBox();
            this.Label4_2 = new fecherFoundation.FCLabel();
            this.Label4_1 = new fecherFoundation.FCLabel();
            this.Label4_0 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkBiWeekly = new fecherFoundation.FCCheckBox();
            this.chkWeekly = new fecherFoundation.FCCheckBox();
            this.cboType = new fecherFoundation.FCComboBox();
            this.txtEmployeeNumber = new fecherFoundation.FCTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkRequired = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdExit = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeekly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 545);
            this.BottomPanel.Size = new System.Drawing.Size(576, 47);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Size = new System.Drawing.Size(576, 485);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(576, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(276, 30);
            this.HeaderText.Text = "Employee Lengths / etc.";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.cboMonth);
            this.Frame5.Controls.Add(this.Label5);
            this.Frame5.Location = new System.Drawing.Point(30, 217);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(189, 117);
            this.Frame5.TabIndex = 20;
            this.Frame5.Text = "Fiscal Month";
            // 
            // cboMonth
            // 
            this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboMonth.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboMonth.Location = new System.Drawing.Point(20, 57);
            this.cboMonth.Name = "cboMonth";
            this.cboMonth.Size = new System.Drawing.Size(149, 40);
            this.cboMonth.TabIndex = 5;
            this.cboMonth.TextChanged += new System.EventHandler(this.cboMonth_TextChanged);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(130, 15);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "FIRST FISCAL MONTH";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtDivLength);
            this.Frame4.Controls.Add(this.txtDeptLength);
            this.Frame4.Controls.Add(this.Label4_2);
            this.Frame4.Controls.Add(this.Label4_1);
            this.Frame4.Controls.Add(this.Label4_0);
            this.Frame4.Location = new System.Drawing.Point(346, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(192, 177);
            this.Frame4.TabIndex = 16;
            this.Frame4.Text = "Department / Division";
            // 
            // txtDivLength
            // 
            this.txtDivLength.BackColor = System.Drawing.SystemColors.Window;
            this.txtDivLength.Location = new System.Drawing.Point(84, 107);
            this.txtDivLength.Name = "txtDivLength";
            this.txtDivLength.Size = new System.Drawing.Size(88, 40);
            this.txtDivLength.TabIndex = 4;
            this.txtDivLength.Enter += new System.EventHandler(this.txtDivLength_Enter);
            this.txtDivLength.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDivLength_KeyPress);
            // 
            // txtDeptLength
            // 
            this.txtDeptLength.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeptLength.Location = new System.Drawing.Point(84, 57);
            this.txtDeptLength.Name = "txtDeptLength";
            this.txtDeptLength.Size = new System.Drawing.Size(88, 40);
            this.txtDeptLength.TabIndex = 3;
            this.txtDeptLength.Enter += new System.EventHandler(this.txtDeptLength_Enter);
            this.txtDeptLength.Validating += new System.ComponentModel.CancelEventHandler(this.txtDeptLength_Validating);
            this.txtDeptLength.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDeptLength_KeyPress);
            // 
            // Label4_2
            // 
            this.Label4_2.Location = new System.Drawing.Point(20, 121);
            this.Label4_2.Name = "Label4_2";
            this.Label4_2.Size = new System.Drawing.Size(40, 16);
            this.Label4_2.TabIndex = 19;
            this.Label4_2.Text = "DIV";
            // 
            // Label4_1
            // 
            this.Label4_1.Location = new System.Drawing.Point(20, 71);
            this.Label4_1.Name = "Label4_1";
            this.Label4_1.Size = new System.Drawing.Size(40, 16);
            this.Label4_1.TabIndex = 18;
            this.Label4_1.Text = "DEPT";
            // 
            // Label4_0
            // 
            this.Label4_0.Location = new System.Drawing.Point(84, 30);
            this.Label4_0.Name = "Label4_0";
            this.Label4_0.Size = new System.Drawing.Size(50, 16);
            this.Label4_0.TabIndex = 17;
            this.Label4_0.Text = "LENGTH";
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupboxRedHeader";
            this.Frame3.Controls.Add(this.Label3);
            this.Frame3.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Frame3.Location = new System.Drawing.Point(30, 345);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(508, 86);
            this.Frame3.TabIndex = 14;
            this.Frame3.Text = "Warning!!!!";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(405, 50);
            this.Label3.TabIndex = 15;
            this.Label3.Text = "CHANGES TO ANY LENGTH OR FORMAT COULD HAVE MAJOR IMPLICATIONS TO THE OVERALL FUNC" +
    "TIONALITY OF THIS APPLICATION";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkBiWeekly);
            this.Frame2.Controls.Add(this.chkWeekly);
            this.Frame2.Location = new System.Drawing.Point(239, 217);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(141, 118);
            this.Frame2.TabIndex = 13;
            this.Frame2.Text = "Pay Frequency";
            // 
            // chkBiWeekly
            // 
            this.chkBiWeekly.Location = new System.Drawing.Point(20, 67);
            this.chkBiWeekly.Name = "chkBiWeekly";
            this.chkBiWeekly.Size = new System.Drawing.Size(101, 27);
            this.chkBiWeekly.TabIndex = 7;
            this.chkBiWeekly.Text = "Bi-Weekly";
            this.chkBiWeekly.CheckedChanged += new System.EventHandler(this.chkBiWeekly_CheckedChanged);
            // 
            // chkWeekly
            // 
            this.chkWeekly.Location = new System.Drawing.Point(20, 30);
            this.chkWeekly.Name = "chkWeekly";
            this.chkWeekly.Size = new System.Drawing.Size(81, 27);
            this.chkWeekly.TabIndex = 6;
            this.chkWeekly.Text = "Weekly";
            this.chkWeekly.CheckedChanged += new System.EventHandler(this.chkWeekly_CheckedChanged);
            // 
            // cboType
            // 
            this.cboType.BackColor = System.Drawing.SystemColors.Window;
            this.cboType.Items.AddRange(new object[] {
            "Numeric",
            "Alphanumeric"});
            this.cboType.Location = new System.Drawing.Point(103, 80);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(173, 40);
            this.cboType.TabIndex = 1;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // txtEmployeeNumber
            // 
            this.txtEmployeeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeNumber.Location = new System.Drawing.Point(103, 30);
            this.txtEmployeeNumber.Name = "txtEmployeeNumber";
            this.txtEmployeeNumber.Size = new System.Drawing.Size(173, 40);
            this.txtEmployeeNumber.TabIndex = 13;
            this.txtEmployeeNumber.Enter += new System.EventHandler(this.txtEmployeeNumber_Enter);
            this.txtEmployeeNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmployeeNumber_KeyPress);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkRequired);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.cboType);
            this.Frame1.Controls.Add(this.txtEmployeeNumber);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(296, 177);
            this.Frame1.TabIndex = 10;
            this.Frame1.Text = "Employee";
            // 
            // chkRequired
            // 
            this.chkRequired.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRequired.Location = new System.Drawing.Point(20, 130);
            this.chkRequired.Name = "chkRequired";
            this.chkRequired.Size = new System.Drawing.Size(149, 27);
            this.chkRequired.TabIndex = 2;
            this.chkRequired.Text = "Length Required";
            this.chkRequired.CheckedChanged += new System.EventHandler(this.chkRequired_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(60, 16);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "LENGTH";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(60, 16);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "FORMAT";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 449);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 8;
            this.cmdSave.TabStop = false;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(37, 54);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(82, 25);
            this.cmdExit.TabIndex = 9;
            this.cmdExit.TabStop = false;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                            ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit          ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmFieldLengths
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(576, 592);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmFieldLengths";
            this.Text = "Employee Lengths / etc.";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmFieldLengths_Load);
            this.Activated += new System.EventHandler(this.frmFieldLengths_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFieldLengths_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBiWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeekly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}