﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	public class modGlobalVariables
	{
		//=========================================================
		// Public gboolESP As Boolean
		public const int ENTERPAYROLL = 1;
		public const int EMPLOYEEFILEUPDATE = 2;
		public const int PAYROLLPROCESSING = 3;
		public const int CALENDARENDOFYEAR = 4;
		public const int MAINPRINTING = 5;
		public const int TABLEANDFILESETUPS = 6;
		public const int FILEMAINTENANCE = 7;
		public const int EMPLOYEEADDUPDATE = 8;
		public const int PAYROLLDISTRIBUTION = 9;
		public const int DEDUCTIONS = 10;
		public const int EMPLOYERSMATCH = 11;
		public const int VACATIONSICK = 12;
		public const int MSRSUNEMPLOYMENTINFO = 13;
		public const int PayTotals = 14;
		public const int DIRECTDEPOSITSECURITY = 15;
		public const int SELECTEMPLOYEE = 16;
		public const int NONPAIDMSRSINFO = 17;
		public const int CHECKRETURN = 18;
		public const int FILESETUP = 19;
		public const int DATAENTRYSECURITY = 20;
		public const int DATAEDITANDCALCULATION = 21;
		public const int VERIFYANDACCEPT = 22;
		public const int CHECKS = 23;
		public const int PAYROLLPROCESSREPORTS = 24;
		public const int PAYROLLPROCESSSTATUS = 25;
		public const int CLEAREICCODES = 26;
		public const int DEDUCTIONSETUP = 27;
		public const int TAXTABLEMAINTENANCE = 28;
		public const int PAYCATEGORYSETUP = 29;
		public const int STANDARDLIMITSRATES = 30;
		public const int VACATIONSICKOTHERCODES = 31;
		public const int CHECKRECIPIENTSSECURITY = 32;
		public const int CHECKRECIPIENTSCATEGORIES = 33;
		public const int SETUPPAYROLLACCOUNTS = 34;
		public const int REPORTSELECTIONS = 35;
		public const int DATAENTRYSELECTION = 36;
		public const int CREATETAXTABLEDATABSE = 37;
		public const int LOADNEWTAXTABLES = 38;
		public const int UPDATEPAYCODES = 39;
		public const int FREQUENCYCODES = 40;
		public const int TAXSTATUSCODES = 41;
		public const int EMPLOYEELENGTHSETC = 42;
		public const int CUSTOMIZE = 43;
		public const int EDITSTATESTOWNSCOUNTIES = 44;
		public const int TESTVALIDACCOUNTS = 45;
		public const int DIRECTDEPOSITBANKS = 46;
		public const int VERIFYADJUSTPAYTOTALS = 47;
		public const int ADJUSTTARECORDS = 48;
		public const int EMPLOYEEEDITSAVE = 59;
		public const int CONTRACTWAGES = 60;
		public const int EDITPAYRECORDS = 62;
		public const int LOADBACK = 63;
		public const int PURGETERMINATEDEMPLOYEES = 64;
		public const int PURGEAUDIT = 65;
		public const int CLEARLTDDEDUCTIONS = 67;
		public const int RESETUNEMPLOYMENT = 68;
		public const int PURGEDATABASEDATA = 69;
		public const int CNSTSecurityPOSTPYJOURNALS = 70;
		public const int CNSTHealthcareSetup = 71;
        public const int CNSTViewSocialSecurityNumbers = 72;
        public const int CNSTViewBankAccountNumbers = 73;
		public const int CNSTW2TYPELASER = 0;
		public const int CNSTW2TYPENARROWREGULAR = 1;
		public const int CNSTW2TYPENARROW2UP = 2;
		public const int CNSTW2TYPEWIDE2UP = 3;
		public const int CNSTW2TYPELASER2X2 = 4;

		public struct TrustAgencyChecks
		{
			public int RecipientNumber;
			public int DeductionNumber;
			public int CheckRecipCatID;
			public string Description;
			public string Frequency;
			public string ColumnToAdd;
			public double TotalAmount;
			public string DeductionDescription;
			public string DeductionAccountNumber;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public TrustAgencyChecks (int unusedParam)
			{
				this.RecipientNumber = 0;
				this.DeductionNumber = 0;
				this.CheckRecipCatID = 0;
				this.Description = String.Empty;
				this.Frequency = String.Empty;
				this.ColumnToAdd = String.Empty;
				this.TotalAmount = 0;
				this.DeductionDescription = String.Empty;
				this.DeductionAccountNumber = String.Empty;
			}
		};

		public struct DefaultInformation
		{
			public string City;
			public string State;
			public string Zip;
			public int StateID;

			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public DefaultInformation (int unusedParam)
			{
				this.City = String.Empty;
				this.State = String.Empty;
				this.Zip = String.Empty;
				this.StateID = 0;
			}
		};

		public struct VacationSickCodes
		{
			// vbPorter upgrade warning: ID As int	OnRead(string)
			public int ID;
			// vbPorter upgrade warning: Code As string	OnWriteFCConvert.ToInt32(
			public string Code;
			public int CodeType;
			public string Description;
			public Decimal Rate;
			public string DHW;
			public string Type;
			public string MonthDay;
			public double Max;
			public string Cleared;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public VacationSickCodes (int unusedParam)
			{
				this.ID = 0;
				this.Code = String.Empty;
				this.CodeType = 0;
				this.Description = String.Empty;
				this.Rate = 0;
				this.DHW = String.Empty;
				this.Type = String.Empty;
				this.MonthDay = String.Empty;
				this.Max = 0;
				this.Cleared = String.Empty;
			}
		};

		public struct GlobalW3Type
		{
			public double dblTotalWages;
			public double dblFederalTax;
			public double dblFicaWages;
			public double dblFicaTax;
			public double dblMedWages;
			public double dblMedTaxes;
			public double dblStateWages;
			public double dblStateTax;
			// vbPorter upgrade warning: dblThirdPartySick As double	OnWrite(int, string)
			public double dblThirdPartySick;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public GlobalW3Type (int unusedParam)
			{
				this.dblTotalWages = 0;
				this.dblFederalTax = 0;
				this.dblFicaWages = 0;
				this.dblFicaTax = 0;
				this.dblMedWages = 0;
				this.dblMedTaxes = 0;
				this.dblStateWages = 0;
				this.dblStateTax = 0;
				this.dblThirdPartySick = 0;
			}
		};

		public struct gtypeFullSet
		{
			public string strFullSetName;
			public string strFullSetPort;
			public bool boolFullSet;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public gtypeFullSet(int unusedParam)
			{
				this.strFullSetName = String.Empty;
				this.strFullSetPort = String.Empty;
				this.boolFullSet = false;
			}
		};

		public struct gtypeEmployee
		{
			// vbPorter upgrade warning: EmployeeNumber As string	OnWrite(string, int)
			public string EmployeeNumber;
			public string EmployeeLastName;
			public string EmployeeFirstName;
			public string EmployeeMiddleName;
			public string EmployeeDesig;
			// vbPorter upgrade warning: ChildRecord As bool	OnWrite(int, bool)
			public bool ChildRecord;
			public string ParentID;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public gtypeEmployee(int unusedParam)
			{
				this.EmployeeNumber = String.Empty;
				this.EmployeeLastName = String.Empty;
				this.EmployeeFirstName = String.Empty;
				this.EmployeeMiddleName = String.Empty;
				this.EmployeeDesig = String.Empty;
				this.ChildRecord = false;
				this.ParentID = String.Empty;
			}
		};

		public enum gEnumCodeValidation
		{
			Frequency = 1,
			TaxStatus = 2,
			PayCategory = 3,
		}

		public const string PAYROLLDATABASE = "Twpy0000.vb1";
		public const string DEFAULTDATABASE = "TWPY0000.vb1";
		public const int GRIDROWS = 20;
		public const int TRUST = 0;
		public const int CHECKREG = 1;
		public const int DIRECTDEPOSIT = 2;
		public const int ACCOUNTING = 3;
		public const int DATAENTRY = 4;
		public const int AUDIT = 5;
		public const int RESETTEMP = 6;
		public const int RESETAUDIT = 7;
		public const int WARRANT = 8;
		public const int FTD941 = 9;
		public const int FTD940 = 10;
		public const int MSRS = 11;
		public const int MMAUNEMPLOYMENT = 12;
		public const int NONMMA = 13;
		// Global Const FEDQTR941 = 14
		public const int STATEUNEMPLOYMENT = 15;
		public const int FUTA940 = 16;
		public const int PAYTAX = 17;
		public const int DEDUCTION = 18;
		public const int VACSICK = 19;
		public const int BACKUP = 20;
		public const int FULLSET = 21;
		public const int WAGE941 = 22;
		public const int STATE941 = 23;

		public struct DefaultDeduction
		{
			public string des;
			public string TaxStatusCode;
			public string Amount;
			public string AmountType;
			public string FreqCode;
			public string Limit;
			public string LimitType;
			public string Priority;
			public string DeductionNumber;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public DefaultDeduction(int unusedParam)
			{
				this.des = String.Empty;
				this.TaxStatusCode = String.Empty;
				this.Amount = String.Empty;
				this.AmountType = String.Empty;
				this.FreqCode = String.Empty;
				this.Limit = String.Empty;
				this.LimitType = String.Empty;
				this.Priority = String.Empty;
				this.DeductionNumber = String.Empty;
			}
		};

		public struct StandardPayTotals
		{
			public string des;
			public string Key;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public StandardPayTotals(int unusedParam)
			{
				this.des = String.Empty;
				this.Key = String.Empty;
			}
		};

		public enum ACAEmployeeIdentifierType
		{
			SSN = 0,
			EmployeeNumber = 1,
		}

		public class StaticVariables
		{
			public DefaultInformation DefaultInfo = new DefaultInformation();
			public VacationSickCodes[] VacationSickCodeInfo = null;
			public TrustAgencyChecks[] CHECKRECIPIENTS = null;
			public TrustAgencyChecks[] TrustCheck = null;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsDeductionID = new clsDRWrapper();
			public clsDRWrapper rsDeductionID_AutoInitialized = null;
			public clsDRWrapper rsDeductionID
			{
				get
				{
					if ( rsDeductionID_AutoInitialized == null)
					{
						 rsDeductionID_AutoInitialized = new clsDRWrapper();
					}
					return rsDeductionID_AutoInitialized;
				}
				set
				{
					 rsDeductionID_AutoInitialized = value;
				}
			}
			// vbPorter upgrade warning: glngW2EditTableID As int	OnWrite(int, string)
			public int glngW2EditTableID;
			public bool gboolAutoSelectEmployee;
			public int glngCheckNumberToVoid;
			public int gintDistributionLisitingField;
			public bool gboolPrintOnlyPayDate;
			// vbPorter upgrade warning: gstrPrintAllW2s As string	OnWrite(int, string)
			public string gstrPrintAllW2s = "";
			public bool gboolCancelACHForm;
			public bool gboolCancelW3;
			// Global variables for the application
			public bool[] ValidReports = new bool[30 + 1];
			public modBudgetaryAccounting.FundType[] AccountStrut = null;
			public modBudgetaryAccounting.FundType[] ftFundArray = null;
            public modBudgetaryAccounting.FundType[] SortedAccountStrut = null;
			public bool gboolGroupByEmployeeNumber;
			public bool gboolGroupBy;
			public string gstrTitle2Caption = string.Empty;
			public double gdblBox12Total;
			public double gdblBox14Total;
			// vbPorter upgrade warning: gboolNoMSRS As bool	OnWrite(CheckState, bool)
			public bool gboolNoMSRS;
			public bool gboolCheckNegBalances;
			// vbPorter upgrade warning: gboolAllPayRuns As bool	OnWrite(CheckState)
			public bool gboolAllPayRuns;
			public int gintUseGroupMultiFund;
			public string gstrSelectedAccount = string.Empty;
			public gtypeFullSet gtypeFullSetReports = new gtypeFullSet();
			public gtypeEmployee gtypeCurrentEmployee = new gtypeEmployee(0);
			public DefaultDeduction[] aryDefautlDeduction = null;
            public GlobalW3Type W3Information = new GlobalW3Type();
			public FCRecordset WPP = null;
			public clsDRWrapper dbVariables;
			public FCRecordset WWK = null;
			// Public rsVariables              As DAO.RecordSet
			public bool gboolSameAsC1WithHolding;
			public string gstrLastDEEmployee = "";
			// vbPorter upgrade warning: gintSequenceNumber As int	OnWrite(string)
			public int gintSequenceNumber;
			// vbPorter upgrade warning: gintDeductionNumber As int	OnWriteFCConvert.ToInt32(
			public int gintDeductionNumber;
			public int gintVacationCodeType;
			// vbPorter upgrade warning: gintRowNumber As int	OnWriteFCConvert.ToInt32(
			public int gintRowNumber;
			public int gintFirstFiscalMonth;
			// vbPorter upgrade warning: gdatCurrentPayDate As DateTime	OnWrite(string)
			public DateTime gdatCurrentPayDate;
			// vbPorter upgrade warning: gdatCurrentWeekEndingDate As DateTime	OnWrite(string, DateTime)
			public DateTime gdatCurrentWeekEndingDate;
			// vbPorter upgrade warning: gdatStart As DateTime	OnWrite(string)
			public DateTime gdatStart;
			// vbPorter upgrade warning: gdatEnd As DateTime	OnWrite(string)
			public DateTime gdatEnd;
			// vbPorter upgrade warning: gintCurrentPayRun As int	OnWrite(string, int)
			public int gintCurrentPayRun;
			public int gintWeekNumber;
			public string gstrCheckListingSort = string.Empty;
			public string gstrActiveTaxTableCode = "";
			// Public gstrTrioSDrive           As String
			// Public gstrReturn               As String
			// Public gstrNetworkFlag          As String
			public string gstrMuniName = "";
			public string gstrUser = "";
			public string gstrPayrollName = string.Empty;
			public string gstrReceiptPrinter = string.Empty;
			public string gstrReceiptWidth = string.Empty;
			public string gstrMvr3Printer = string.Empty;
			public string gstrCtaPrinter = "";
			public string gstrHelpFieldName = string.Empty;
			public string gstrEmployeeScreen = "";
			public string gstrSinglePersonRecord = "";
			public string gstrBankStart = "";
			public string gstrBankEnd = "";
			public string gstrMQYProcessing = "";
			public string gstrDepartmentNum = "";
			public string gstrDivisionNum = "";
			// vbPorter upgrade warning: gstrVacType As string	OnWrite(int, string)
			public string gstrVacType = "";
			public string gstrEmployeeToGet = string.Empty;
			public int gintSequenceBy;
			public bool gboolCancelSelected;
			public bool pboolUnloadProcessGrid;
			public bool gboolSummaryDedCustomReport;
			public string gstrDeductionSummaryType = "";
			public string gstrShowCYTDDeductionReport = "";
			public bool gboolSummaryDistCustomReport;
			public string gstrDistributionSummaryType = "";
			public bool gboolBudgetary;
			public bool gboolHaveDivision;
			public bool gboolAdditionalStateTaxNetPercent;
			public bool gboolAdditionalFederalTaxNetPercent;
			public double gdblAdditionalStateTaxNetPercent;
			public double gdblAdditionalFederalTaxNetPercent;
			public bool gboolCloseVerificationReport;


			// vbPorter upgrade warning: gobjEditFormName As object	OnWrite(frmPayCategories, frmVacationCodes, frmPayStatuses)
			public object gobjEditFormName;
			public bool gboolDontChangeMenus;

			public bool gboolDosFlag;
			// Public gboolBbort                   As Boolean
			public bool gboolEmployeeTypeNumeric;
			public bool gboolShowDeductionSetup;

			public bool gboolSortBankName;
			public bool gboolDataEntryOpen;
            public bool gboolDataEntry;
			public bool gboolSamePage;
			public bool gboolPrintAllTypes;
			public string gstrW2Status = "";
			public string gstrPrintProcessDetailReport = string.Empty;
			public string gstrCheckListingSQL = string.Empty;
			// used on the reports for rptCheckListingRegular.rpt
			public string gstrCheckListingWhere = "";
			public double gdblBothRegularTotal;
			public double gdblBothDirectDepositTotal;
			public double gdblRegularTotal;
			public double gdblDirectDepositTotal;
			public int gintMQFCType;

			public bool boolCancelButtonSelected;
			public bool gboolExitWithoutShowingReport;
            public string gstrCheckMessage = "";
			public string gstrCheckListingReportGroup = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
