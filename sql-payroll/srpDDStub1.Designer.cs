﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptDDStub1.
	/// </summary>
	partial class srptDDStub1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDDStub1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtBankNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtChkAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFica = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblState = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFedAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFedAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFicaAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFicaAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMedAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMedAdjust = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFica)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFedAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFicaAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMedAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtBankNumber,
				this.txtName,
				this.txtCheckNo,
				this.txtChkAmount,
				this.Label32,
				this.txtFed,
				this.lblFed,
				this.txtFica,
				this.lblFica,
				this.txtMed,
				this.lblMed,
				this.txtState,
				this.lblState,
				this.txtDate,
				this.lblCheckMessage,
				this.txtFedAdjust,
				this.lblFedAdjust,
				this.txtFicaAdjust,
				this.lblFicaAdjust,
				this.txtMedAdjust,
				this.lblMedAdjust
			});
			this.Detail.Height = 2.895833F;
			this.Detail.Name = "Detail";
			// 
			// txtBankNumber
			// 
			this.txtBankNumber.Height = 0.1666667F;
			this.txtBankNumber.HyperLink = null;
			this.txtBankNumber.Left = 0.1388889F;
			this.txtBankNumber.Name = "txtBankNumber";
			this.txtBankNumber.Style = "text-align: right";
			this.txtBankNumber.Tag = "text";
			this.txtBankNumber.Text = "Bank";
			this.txtBankNumber.Top = 1F;
			this.txtBankNumber.Width = 1.1875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.HyperLink = null;
			this.txtName.Left = 1.388889F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "";
			this.txtName.Tag = "text";
			this.txtName.Text = "EMPLOYEE";
			this.txtName.Top = 1F;
			this.txtName.Width = 3.6875F;
			// 
			// txtCheckNo
			// 
			this.txtCheckNo.Height = 0.1666667F;
			this.txtCheckNo.HyperLink = null;
			this.txtCheckNo.Left = 5.4375F;
			this.txtCheckNo.Name = "txtCheckNo";
			this.txtCheckNo.Style = "text-align: right";
			this.txtCheckNo.Tag = "text";
			this.txtCheckNo.Text = "CHECK";
			this.txtCheckNo.Top = 0.3333333F;
			this.txtCheckNo.Width = 1.375F;
			// 
			// txtChkAmount
			// 
			this.txtChkAmount.Height = 0.1666667F;
			this.txtChkAmount.HyperLink = null;
			this.txtChkAmount.Left = 6.3125F;
			this.txtChkAmount.Name = "txtChkAmount";
			this.txtChkAmount.Style = "text-align: right";
			this.txtChkAmount.Tag = "text";
			this.txtChkAmount.Text = "0.00";
			this.txtChkAmount.Top = 1F;
			this.txtChkAmount.Width = 1F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1666667F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 5.138889F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "text-align: right";
			this.Label32.Tag = "text";
			this.Label32.Text = "CHK AMOUNT";
			this.Label32.Top = 1F;
			this.Label32.Width = 1.125F;
			// 
			// txtFed
			// 
			this.txtFed.Height = 0.1666667F;
			this.txtFed.HyperLink = null;
			this.txtFed.Left = 6.3125F;
			this.txtFed.Name = "txtFed";
			this.txtFed.Style = "text-align: right";
			this.txtFed.Tag = "text";
			this.txtFed.Text = "0.00";
			this.txtFed.Top = 1.166667F;
			this.txtFed.Visible = false;
			this.txtFed.Width = 1F;
			// 
			// lblFed
			// 
			this.lblFed.Height = 0.1666667F;
			this.lblFed.HyperLink = null;
			this.lblFed.Left = 5.6875F;
			this.lblFed.Name = "lblFed";
			this.lblFed.Style = "text-align: left";
			this.lblFed.Tag = "text";
			this.lblFed.Text = "Fed";
			this.lblFed.Top = 1.166667F;
			this.lblFed.Visible = false;
			this.lblFed.Width = 0.5625F;
			// 
			// txtFica
			// 
			this.txtFica.Height = 0.1666667F;
			this.txtFica.HyperLink = null;
			this.txtFica.Left = 6.3125F;
			this.txtFica.Name = "txtFica";
			this.txtFica.Style = "text-align: right";
			this.txtFica.Tag = "text";
			this.txtFica.Text = "0.00";
			this.txtFica.Top = 1.333333F;
			this.txtFica.Visible = false;
			this.txtFica.Width = 1F;
			// 
			// lblFica
			// 
			this.lblFica.Height = 0.1666667F;
			this.lblFica.HyperLink = null;
			this.lblFica.Left = 5.6875F;
			this.lblFica.Name = "lblFica";
			this.lblFica.Style = "text-align: left";
			this.lblFica.Tag = "text";
			this.lblFica.Text = "Fica";
			this.lblFica.Top = 1.333333F;
			this.lblFica.Visible = false;
			this.lblFica.Width = 0.5625F;
			// 
			// txtMed
			// 
			this.txtMed.Height = 0.1666667F;
			this.txtMed.HyperLink = null;
			this.txtMed.Left = 6.3125F;
			this.txtMed.Name = "txtMed";
			this.txtMed.Style = "text-align: right";
			this.txtMed.Tag = "text";
			this.txtMed.Text = "0.00";
			this.txtMed.Top = 1.5F;
			this.txtMed.Visible = false;
			this.txtMed.Width = 1F;
			// 
			// lblMed
			// 
			this.lblMed.Height = 0.1666667F;
			this.lblMed.HyperLink = null;
			this.lblMed.Left = 5.6875F;
			this.lblMed.Name = "lblMed";
			this.lblMed.Style = "text-align: left";
			this.lblMed.Tag = "text";
			this.lblMed.Text = "Med";
			this.lblMed.Top = 1.5F;
			this.lblMed.Visible = false;
			this.lblMed.Width = 0.5625F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1666667F;
			this.txtState.HyperLink = null;
			this.txtState.Left = 6.3125F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "text-align: right";
			this.txtState.Tag = "text";
			this.txtState.Text = "0.00";
			this.txtState.Top = 1.666667F;
			this.txtState.Visible = false;
			this.txtState.Width = 1F;
			// 
			// lblState
			// 
			this.lblState.Height = 0.1666667F;
			this.lblState.HyperLink = null;
			this.lblState.Left = 5.6875F;
			this.lblState.Name = "lblState";
			this.lblState.Style = "text-align: left";
			this.lblState.Tag = "text";
			this.lblState.Text = "State";
			this.lblState.Top = 1.666667F;
			this.lblState.Visible = false;
			this.lblState.Width = 0.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 3.625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "text";
			this.txtDate.Text = "DATE";
			this.txtDate.Top = 0.3333333F;
			this.txtDate.Width = 1.375F;
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Height = 0.1666667F;
			this.lblCheckMessage.HyperLink = null;
			this.lblCheckMessage.Left = 3.40625F;
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Style = "font-size: 10pt; text-align: right";
			this.lblCheckMessage.Text = null;
			this.lblCheckMessage.Top = 0.5F;
			this.lblCheckMessage.Visible = false;
			this.lblCheckMessage.Width = 3.40625F;
			// 
			// txtFedAdjust
			// 
			this.txtFedAdjust.Height = 0.1666667F;
			this.txtFedAdjust.HyperLink = null;
			this.txtFedAdjust.Left = 6.3125F;
			this.txtFedAdjust.Name = "txtFedAdjust";
			this.txtFedAdjust.Style = "text-align: right";
			this.txtFedAdjust.Tag = "text";
			this.txtFedAdjust.Text = "0.00";
			this.txtFedAdjust.Top = 1.833333F;
			this.txtFedAdjust.Visible = false;
			this.txtFedAdjust.Width = 1F;
			// 
			// lblFedAdjust
			// 
			this.lblFedAdjust.Height = 0.1666667F;
			this.lblFedAdjust.HyperLink = null;
			this.lblFedAdjust.Left = 5.25F;
			this.lblFedAdjust.Name = "lblFedAdjust";
			this.lblFedAdjust.Style = "text-align: left";
			this.lblFedAdjust.Tag = "text";
			this.lblFedAdjust.Text = "Fed Adjust";
			this.lblFedAdjust.Top = 1.833333F;
			this.lblFedAdjust.Visible = false;
			this.lblFedAdjust.Width = 1F;
			// 
			// txtFicaAdjust
			// 
			this.txtFicaAdjust.Height = 0.1666667F;
			this.txtFicaAdjust.HyperLink = null;
			this.txtFicaAdjust.Left = 6.3125F;
			this.txtFicaAdjust.Name = "txtFicaAdjust";
			this.txtFicaAdjust.Style = "text-align: right";
			this.txtFicaAdjust.Tag = "text";
			this.txtFicaAdjust.Text = "0.00";
			this.txtFicaAdjust.Top = 2F;
			this.txtFicaAdjust.Visible = false;
			this.txtFicaAdjust.Width = 1F;
			// 
			// lblFicaAdjust
			// 
			this.lblFicaAdjust.Height = 0.1666667F;
			this.lblFicaAdjust.HyperLink = null;
			this.lblFicaAdjust.Left = 5.25F;
			this.lblFicaAdjust.Name = "lblFicaAdjust";
			this.lblFicaAdjust.Style = "text-align: left";
			this.lblFicaAdjust.Tag = "text";
			this.lblFicaAdjust.Text = "Fica Adjust";
			this.lblFicaAdjust.Top = 2F;
			this.lblFicaAdjust.Visible = false;
			this.lblFicaAdjust.Width = 1F;
			// 
			// txtMedAdjust
			// 
			this.txtMedAdjust.Height = 0.1666667F;
			this.txtMedAdjust.HyperLink = null;
			this.txtMedAdjust.Left = 6.3125F;
			this.txtMedAdjust.Name = "txtMedAdjust";
			this.txtMedAdjust.Style = "text-align: right";
			this.txtMedAdjust.Tag = "text";
			this.txtMedAdjust.Text = "0.00";
			this.txtMedAdjust.Top = 2.166667F;
			this.txtMedAdjust.Visible = false;
			this.txtMedAdjust.Width = 1F;
			// 
			// lblMedAdjust
			// 
			this.lblMedAdjust.Height = 0.1666667F;
			this.lblMedAdjust.HyperLink = null;
			this.lblMedAdjust.Left = 5.25F;
			this.lblMedAdjust.Name = "lblMedAdjust";
			this.lblMedAdjust.Style = "text-align: left";
			this.lblMedAdjust.Tag = "text";
			this.lblMedAdjust.Text = "Med Adjust";
			this.lblMedAdjust.Top = 2.166667F;
			this.lblMedAdjust.Visible = false;
			this.lblMedAdjust.Width = 1F;
			// 
			// srptDDStub1
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChkAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFica)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFedAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFicaAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFicaAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMedAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCheckNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtChkAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFica;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMed;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMed;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtState;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblState;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFedAdjust;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFedAdjust;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtFicaAdjust;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFicaAdjust;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMedAdjust;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMedAdjust;
	}
}
