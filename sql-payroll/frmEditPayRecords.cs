//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmEditPayRecords : BaseForm
	{
		public frmEditPayRecords()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:AM:#4142 - set the disabled color
            GridDeds.BackColorFixed = System.Drawing.ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            GridMatch.BackColorFixed = System.Drawing.ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            GridDist.BackColorFixed = System.Drawing.ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);

        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditPayRecords InstancePtr
		{
			get
			{
				return (frmEditPayRecords)Sys.GetInstance(typeof(frmEditPayRecords));
			}
		}

		protected frmEditPayRecords _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolCantEdit;
		const int CNSTGRIDTOTALSCOLDESC = 0;
		const int CNSTGRIDTOTALSCOLFED = 1;
		const int CNSTGRIDTOTALSCOLFICA = 2;
		const int CNSTGRIDTOTALSCOLMEDICARE = 3;
		const int CNSTGRIDTOTALSCOLSTATE = 4;
		const int CNSTGRIDDEDCOLNUMBER = 0;
		const int CNSTGRIDDEDCOLDESC = 1;
		const int CNSTGRIDDEDCOLAMOUNT = 3;
		const int CNSTGRIDDEDCOLACCOUNT = 2;
		const int CNSTGRIDDEDCOLRECID = 4;
		const int CNSTGRIDDEDCOLDEDID = 5;
		const int CNSTGRIDDEDCOLEDITABLE = 6;
		const int CNSTGRIDMATCHCOLNUMBER = 0;
		const int CNSTGRIDMATCHCOLDESC = 1;
		const int CNSTGRIDMatchCOLAmount = 4;
		const int CNSTGRIDMATCHCOLACCOUNT = 2;
		const int CNSTGRIDMATCHCOLGLACCOUNT = 3;
		const int CNSTGRIDMATCHCOLRECID = 5;
		const int CNSTGRIDMATCHCOLDEDID = 6;
		const int CNSTGRIDMATCHCOLEDITABLE = 7;
		const int CNSTGRIDDISTCOLID = 0;
		const int CNSTGRIDDISTCOLPAYCAT = 1;
		const int CNSTGRIDDISTCOLPAYCATDESC = 2;
		const int CNSTGRIDDISTCOLGROSSPAY = 3;
		const int CNSTGRIDDISTCOLHOURS = 4;
		const int CNSTGRIDDISTCOLWC = 5;
		const int CNSTGRIDDISTCOLWCCODE = 6;
		const int CNSTGRIDDISTCOLADJUSTMENT = 7;
		const int CNSTGRIDVACSICKCOLID = 0;
		const int CNSTGRIDVACSICKCOLTYPE = 1;
		const int CNSTGRIDVACSICKCOLACCRUED = 2;
		const int CNSTGRIDVACSICKCOLUSED = 3;
		const int CNSTGRIDVACSICKCOLBALANCE = 4;
		private string strEmployeeNumber = string.Empty;
		private DateTime dtPayDate;
		private int intPayRun;
		private bool boolFromSchool;
		private string strEmpName = "";
		private bool blnDataChanged;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		// vbPorter upgrade warning: intPRun As int	OnWriteFCConvert.ToDouble(
		public void Init(string strEmpNumber, string strEmployeeName, ref DateTime dtPDate, int intPRun)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			strEmployeeNumber = strEmpNumber;
			rsLoad.OpenRecordset("select * from tblemployeemaster where employeenumber = '" + strEmpNumber + "'", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				strEmpName = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("middlename")) + " " + rsLoad.Get_Fields("lastname") + " " + rsLoad.Get_Fields("desig");
			}
			else
			{
				strEmpName = "";
			}
			dtPayDate = dtPDate;
			intPayRun = intPRun;
			lblEmployee.Text = strEmpNumber + "  " + strEmployeeName + "     PayDate " + FCConvert.ToString(dtPDate) + " Payrun " + FCConvert.ToString(intPRun);
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmEditPayRecords_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditPayRecords_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditPayRecords properties;
			//frmEditPayRecords.FillStyle	= 0;
			//frmEditPayRecords.ScaleWidth	= 9300;
			//frmEditPayRecords.ScaleHeight	= 7620;
			//frmEditPayRecords.LinkTopic	= "Form2";
			//frmEditPayRecords.LockControls	= -1  'True;
			//frmEditPayRecords.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int x;
			boolCantEdit = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTotals();
			SetupGridDeds();
			SetupGridMatch();
			SetupGridPay();
			SetupGridDist();
			SetupGridVacSick();
			SetupGridCheck();
			Reload(0);
			// GridTotals.Tag = 0
			// txtCheckNumber.Text = ""
			// LoadTotals
			// LoadGridDeds
			// LoadGridMatch
			// LoadGridPay
			// LoadGridDist
			// LoadGridVacSick
			// If clsSecurityClass.Check_Permissions(EMPLOYEEEDITSAVE) <> "F" Then
			// boolCantEdit = True
			// mnuSave.Enabled = False
			// mnuSaveExit.Enabled = False
			// GridDeds.Editable = flexEDNone
			// GridTotals.Enabled = False
			// GridMatch.Editable = flexEDNone
			// GridPay.Enabled = False
			// GridDist.Editable = flexEDNone
			// GridVacSick.Editable = flexEDNone
			// End If
			// 
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			// FillControlInformationClassFromGrid
			// This will initialize the old data so we have somethign to compare the new data against when you save
			// For X = 0 To intTotalNumberOfControls - 1
			// clsControlInfo(X).FillOldValue Me
			// Next
			// ---------------------------------------------------------------------
			blnDataChanged = false;
		}

		private void SetupGridCheck()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp;
			bool boolChoose;
			// boolChoose = False
			// Call rsLoad.OpenRecordset("select masterrecord from tblcheckdetail where checkvoid = 0 and employeenumber = '" & strEmployeeNumber & "' and paydate = '" & dtPayDate & "' and payrunid = " & intPayRun & " and masterrecord <> '' group by masterrecord order by masterrecord", "twpy0000.vb1")
			// If Not rsLoad.EndOfFile Then
			// Do While Not rsLoad.EndOfFile
			// Select Case Val(Mid(rsLoad.Fields("masterrecord"), 2))
			// Case 0
			// strTemp = strTemp & "#0;Regular|"
			// Case Else
			// strTemp = strTemp & "#" & Val(Mid(rsLoad.Fields("masterrecord"), 2)) & ";S" & Val(Mid(rsLoad.Fields("masterrecord"), 2)) & "|"
			// boolChoose = True
			// End Select
			// rsLoad.MoveNext
			// Loop
			// If strTemp <> vbNullString Then
			// strTemp = Mid(strTemp, 1, Len(strTemp) - 1)
			// End If
			// End If
			// If Not boolChoose Then
			// gridCheck.Rows = 1
			// gridCheck.TextMatrix(0, 0) = 0
			// gridCheck.Visible = False
			// Else
			strTemp = "#0;Regular|#1;S1|#2;S2|#3;S3|#4;S4|#5;S5|#6;S6|#7;S7|#8;S8|#9;S9";
			gridCheck.ColComboList(0, strTemp);
			gridCheck.Visible = true;
			gridCheck.TextMatrix(0, 0, FCConvert.ToString(0));
			gridCheck.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			// End If
		}

		private void Reload(int intChk)
		{
			int x;
			GridTotals.Tag = (System.Object)(0);
			txtCheckNumber.Text = "";
			LoadTotals(intChk);
			LoadGridDeds(intChk);
			LoadGridMatch(intChk);
			LoadGridPay(intChk);
			LoadGridDist(intChk);
			LoadGridVacSick(intChk);
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
			{
				boolCantEdit = true;
				mnuSave.Enabled = false;
				mnuSaveExit.Enabled = false;
				GridDeds.Editable = FCGrid.EditableSettings.flexEDNone;
				GridTotals.Enabled = false;
				GridMatch.Editable = FCGrid.EditableSettings.flexEDNone;
				GridPay.Enabled = false;
				GridDist.Editable = FCGrid.EditableSettings.flexEDNone;
				GridVacSick.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnDataChanged)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (SaveInfo())
					{
						// do nothing
					}
					else
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmEditPayRecords_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTotals();
			ResizeGridDeds();
			ResizeGridMatch();
			ResizeGridPay();
			ResizeGridDist();
			ResizeGridVacSick();
		}

		private void gridCheck_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intChk As int	OnWrite(string)
			int intChk;
			intChk = FCConvert.ToInt32(gridCheck.ComboData(gridCheck.ComboIndex));
			Reload(intChk);
		}

		private void GridDeds_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (GridDeds.Row < 1)
				return;
			GridDeds.RowData(GridDeds.Row, true);
			switch (GridDeds.Col)
			{
				case CNSTGRIDDEDCOLAMOUNT:
					{
						RecalcTotals();
						break;
					}
				case CNSTGRIDDEDCOLNUMBER:
					{
						GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLDESC, FCConvert.ToString(Conversion.Val(GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLNUMBER))));
						GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLDEDID, FCConvert.ToString(Conversion.Val(GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLNUMBER))));
						break;
					}
			}
			//end switch
		}

		private void GridDeds_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewRow == 0)
				return;
			//FC:FINAL:DDU:#i2182 - check correct
			if (FCConvert.ToString(GridDeds.TextMatrix(e.NewRow, CNSTGRIDDEDCOLEDITABLE)) == "0" ||
				FCConvert.ToString(GridDeds.TextMatrix(e.NewRow, CNSTGRIDDEDCOLEDITABLE)).ToLower() == "false" || boolCantEdit)
			{
				GridDeds.Editable = FCGrid.EditableSettings.flexEDNone;
				return;
			}
			switch (e.NewCol)
			{
				case CNSTGRIDDEDCOLACCOUNT:
				case CNSTGRIDDEDCOLDESC:
					{
						GridDeds.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				default:
					{
						GridDeds.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
			}
			//end switch
		}

		private void GridDeds_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

		private void GridDeds_ComboDropDown(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLDEDID))));
			if (lngID > 0)
			{
				for (x = 0; x <= (GridDeds.ComboCount - 1); x++)
				{
					if (Conversion.Val(GridDeds.ComboData(x)) == lngID)
					{
						GridDeds.ComboIndex = x;
					}
				}
				// x
			}
		}

		private void GridDeds_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int lngRow = 0;
			// vbPorter upgrade warning: intanswer As int	OnWrite(DialogResult)
			DialogResult intanswer = 0;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						GridDeds.Rows += 1;
						lngRow = GridDeds.Rows - 1;
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Added Row to Deductions");
						// ------------------------------------------------------------------
						GridDeds.TopRow = lngRow;
						GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLAMOUNT, "0.00");
						GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLRECID, "0");
						GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLEDITABLE, FCConvert.ToString(true));
						GridDeds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDEDCOLACCOUNT, GridDeds.BackColorFixed);
						GridDeds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDEDCOLDESC, GridDeds.BackColorFixed);
						GridDeds.RowData(lngRow, true);
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridDeds.Row < 1)
							return;
						if (GridDeds.TextMatrix(GridDeds.Row, CNSTGRIDDEDCOLRECID) == "0")
						{
							intanswer = MessageBox.Show("Are you sure you wish to delete this record?", "Delete Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intanswer == DialogResult.Yes)
							{
								// Dave 12/14/2006---------------------------------------------------
								// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
								modAuditReporting.RemoveGridRow_8("GridDeds", intTotalNumberOfControls - 1, GridDeds.Row, clsControlInfo);
								// We then add a change record saying the row was deleted
								clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(GridDeds.Row) + " from Deductions");
								// -------------------------------------------------------------------
								GridDeds.RemoveItem(GridDeds.Row);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridDist_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (GridDist.Row < 1)
				return;
			GridDist.RowData(GridDist.Row, true);
			switch (GridDist.Col)
			{
				case CNSTGRIDDISTCOLGROSSPAY:
					{
						RecalcTotals();
						break;
					}
				case CNSTGRIDDISTCOLPAYCAT:
					{
						GridDist.TextMatrix(GridDist.Row, CNSTGRIDDISTCOLPAYCATDESC, FCConvert.ToString(Conversion.Val(GridDist.TextMatrix(GridDist.Row, CNSTGRIDDISTCOLPAYCAT))));
						break;
					}
			}
			//end switch
		}

		private void GridDist_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewRow == 0)
				return;
			if (boolCantEdit)
				return;
			switch (e.NewCol)
			{
				case CNSTGRIDDISTCOLHOURS:
				case CNSTGRIDDISTCOLWC:
					{
						GridDist.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case CNSTGRIDDISTCOLPAYCATDESC:
					{
						GridDist.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case CNSTGRIDDISTCOLGROSSPAY:
				case CNSTGRIDDISTCOLPAYCAT:
					{
						//FC:FINAL:DDU:#i2182 - check correct
						if (FCConvert.ToString(GridDist.TextMatrix(e.NewRow, CNSTGRIDDISTCOLADJUSTMENT)).ToLower() == "true" ||
							FCConvert.ToString(GridDist.TextMatrix(e.NewRow, CNSTGRIDDISTCOLADJUSTMENT)) == "-1")
						{
							GridDist.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							GridDist.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case CNSTGRIDDISTCOLWCCODE:
					{
						//FC:FINAL:DDU:#i2182 - check correct
						if (FCConvert.ToString(GridDist.TextMatrix(e.NewRow, CNSTGRIDDISTCOLWC)).ToLower() == "true" ||
							FCConvert.ToString(GridDist.TextMatrix(e.NewRow, CNSTGRIDDISTCOLWC)).ToLower() == "-1")
						{
							GridDist.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							GridDist.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
			}
			//end switch
		}

		private void GridDist_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

		private void GridDist_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int lngRow = 0;
			if (boolCantEdit)
				return;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						GridDist.Rows += 1;
						lngRow = GridDist.Rows - 1;
						GridDist.TopRow = lngRow;
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Added Row to Distribution");
						// ------------------------------------------------------------------
						GridDist.RowData(lngRow, true);
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLADJUSTMENT, FCConvert.ToString(true));
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLID, FCConvert.ToString(0));
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLGROSSPAY, "0.00");
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLHOURS, "0");
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWC, FCConvert.ToString(false));
						GridDist.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDISTCOLPAYCATDESC, GridDist.BackColorFixed);
						break;
					}
				case Keys.Delete:
					{
						if (GridDist.Row < 1)
							return;
						lngRow = GridDist.Row;
						if (Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLID)) != 0)
							return;
						// Dave 12/14/2006---------------------------------------------------
						// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
						modAuditReporting.RemoveGridRow_8("GridDist", intTotalNumberOfControls - 1, GridDist.Row, clsControlInfo);
						// We then add a change record saying the row was deleted
						clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(GridDist.Row) + " from Distribution");
						// -------------------------------------------------------------------
						GridDist.RemoveItem(lngRow);
						RecalcTotals();
						break;
					}
			}
			//end switch
		}

		private void GridMatch_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (GridDist.Row < 1)
				return;
			GridMatch.RowData(GridDist.Row, true);
			switch (GridDist.Col)
			{
				case CNSTGRIDMatchCOLAmount:
					{
						RecalcTotals();
						break;
					}
				case CNSTGRIDMATCHCOLNUMBER:
					{
						GridMatch.TextMatrix(GridDist.Row, CNSTGRIDMATCHCOLDESC, FCConvert.ToString(Conversion.Val(GridMatch.TextMatrix(GridDist.Row, CNSTGRIDMATCHCOLNUMBER))));
						GridMatch.TextMatrix(GridDist.Row, CNSTGRIDMATCHCOLDEDID, FCConvert.ToString(Conversion.Val(GridMatch.TextMatrix(GridDist.Row, CNSTGRIDMATCHCOLNUMBER))));
						break;
					}
			}
			//end switch
		}

		private void GridMatch_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewRow < 1)
				return;
			//FC:FINAL:DDU:#i2182 - check correct
			if (FCConvert.ToString(GridMatch.TextMatrix(e.NewRow, CNSTGRIDMATCHCOLEDITABLE)).ToLower() == "false" ||
				FCConvert.ToString(GridMatch.TextMatrix(e.NewRow, CNSTGRIDMATCHCOLEDITABLE)) == "0" || boolCantEdit)
			{
				GridMatch.Editable = FCGrid.EditableSettings.flexEDNone;
				return;
			}
			switch (e.NewCol)
			{
				case CNSTGRIDMatchCOLAmount:
				case CNSTGRIDMATCHCOLNUMBER:
					{
						GridMatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						GridMatch.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void GridMatch_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

		private void GridMatch_ComboDropDown(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridMatch.TextMatrix(GridMatch.Row, CNSTGRIDMATCHCOLDEDID))));
			if (lngID > 0)
			{
				for (x = 0; x <= (GridMatch.ComboCount - 1); x++)
				{
					if (Conversion.Val(GridMatch.ComboData(x)) == lngID)
					{
						GridMatch.ComboIndex = x;
					}
				}
				// x
			}
		}

		private void GridMatch_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int lngRow = 0;
			// vbPorter upgrade warning: intanswer As int	OnWrite(DialogResult)
			DialogResult intanswer = 0;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						GridMatch.Rows += 1;
						lngRow = GridMatch.Rows - 1;
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Added Row to Employee Match");
						// ------------------------------------------------------------------
						GridMatch.RowData(lngRow, true);
						GridMatch.TextMatrix(lngRow, CNSTGRIDMatchCOLAmount, "0.00");
						GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLRECID, "0");
						GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLEDITABLE, FCConvert.ToString(true));
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLDESC, GridMatch.BackColorFixed);
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLGLACCOUNT, GridMatch.BackColorFixed);
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLACCOUNT, GridMatch.BackColorFixed);
						GridMatch.TopRow = lngRow;
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridMatch.Row < 1)
							return;
						if (GridMatch.TextMatrix(GridMatch.Row, CNSTGRIDMATCHCOLRECID) == "0")
						{
							intanswer = MessageBox.Show("Are you sure you wish to delete this record?", "Delete Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intanswer == DialogResult.Yes)
							{
								// Dave 12/14/2006---------------------------------------------------
								// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
								modAuditReporting.RemoveGridRow_8("GridMatch", intTotalNumberOfControls - 1, GridMatch.Row, clsControlInfo);
								// We then add a change record saying the row was deleted
								clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(GridMatch.Row) + " from Employee Match");
								// -------------------------------------------------------------------
								GridMatch.RemoveItem(GridMatch.Row);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridTotals_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

		private void GridVacSick_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (GridVacSick.Row < 1)
				return;
			GridVacSick.RowData(GridVacSick.Row, true);
		}

		private void GridVacSick_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

		private void GridVacSick_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int lngRow = 0;
			if (boolCantEdit)
				return;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						GridVacSick.Rows += 1;
						lngRow = GridVacSick.Rows - 1;
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Added Row to Vacation / Sick");
						// ------------------------------------------------------------------
						GridVacSick.RowData(lngRow, true);
						GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLACCRUED, "0");
						GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLUSED, "0");
						GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLBALANCE, "0");
						break;
					}
				case Keys.Delete:
					{
						if (GridVacSick.Row < 1)
							return;
						lngRow = GridVacSick.Row;
						if (Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLID)) == 0)
						{
							// Dave 12/14/2006---------------------------------------------------
							// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
							modAuditReporting.RemoveGridRow_8("GridVacSick", intTotalNumberOfControls - 1, GridVacSick.Row, clsControlInfo);
							// We then add a change record saying the row was deleted
							clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(GridVacSick.Row) + " from Vacation / Sick");
							// -------------------------------------------------------------------
							GridMatch.RemoveItem(GridMatch.Row);
							GridVacSick.RemoveItem(lngRow);
						}
						break;
					}
			}
			//end switch
		}

		private void GridVacSick_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridVacSick[e.ColumnIndex, e.RowIndex];
            int lngCol;
			lngCol = GridVacSick.GetFlexColIndex(e.ColumnIndex);
			switch (lngCol)
			{
				case CNSTGRIDVACSICKCOLBALANCE:
					{
						//ToolTip1.SetToolTip(GridVacSick, "Balance is balance as of the chosen pay date (after used and accrued)");
						cell.ToolTipText = "Balance is balance as of the chosen pay date (after used and accrued)";
						break;
					}
				default:
					{
						//ToolTip1.SetToolTip(GridVacSick, "Use Insert key to add new adjustment records");
						cell.ToolTipText = "Use Insert key to add new adjustment records";
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridPay()
		{
			GridPay.TextMatrix(0, 1, "Amount");
			GridPay.TextMatrix(1, 0, "Gross");
			GridPay.TextMatrix(2, 0, "Net");
            //FC:FINAL:BSE:#4143 column should be right aligned 
            GridPay.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void SetupGridTotals()
		{
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLFED, "Federal");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLFICA, "Fica");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLMEDICARE, "Medicare");
			GridTotals.TextMatrix(0, CNSTGRIDTOTALSCOLSTATE, "State");
			GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLDESC, "Taxable Gross");
			GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLDESC, "Tax Withheld");
			GridTotals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDTOTALSCOLFED, 0, CNSTGRIDTOTALSCOLSTATE, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //FC:FINAL:BSE:#4143 columns should be right aligned 
            GridTotals.ColAlignment(CNSTGRIDTOTALSCOLFED, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GridTotals.ColAlignment(CNSTGRIDTOTALSCOLFICA, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GridTotals.ColAlignment(CNSTGRIDTOTALSCOLMEDICARE, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GridTotals.ColAlignment(CNSTGRIDTOTALSCOLSTATE, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGridTotals()
		{
			int GridWidth = 0;
			GridWidth = GridTotals.WidthOriginal;
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLDESC, FCConvert.ToInt32(0.22 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLFED, FCConvert.ToInt32(0.16 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLFICA, FCConvert.ToInt32(0.16 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLMEDICARE, FCConvert.ToInt32(0.16 * GridWidth));
			GridTotals.ColWidth(CNSTGRIDTOTALSCOLSTATE, FCConvert.ToInt32(0.16 * GridWidth));
			//GridTotals.Height = GridTotals.RowHeight(0) * 3 + 40;
		}

		private void LoadTotals(int intChk)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select checknumber from tblcheckdetail where totalrecord = 1 and  masterrecord = 'P" + FCConvert.ToString(intChk) + "' and  checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " order by checknumber desc", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				txtCheckNumber.Text = FCConvert.ToString(rsLoad.Get_Fields("checknumber"));
			}
			else
			{
				txtCheckNumber.Text = "";
			}
			rsLoad.OpenRecordset("select sum(federaltaxWH) as totFed,sum(stateTaxWH) as totState,sum(ficataxwh) as totfica,sum(medicaretaxwh) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totfed"))), "0.00"));
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totstate"))), "0.00"));
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totfica"))), "0.00"));
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totmed"))), "0.00"));
			}
			else
			{
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED, "0.00");
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE, "0.00");
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA, "0.00");
				GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE, "0.00");
			}
			rsLoad.OpenRecordset("select sum(federaltaxgross) as totFed,sum(stateTaxgross) as totState,sum(ficataxgross) as totfica,sum(medicaretaxgross) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFED, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totfed"))), "0.00"));
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLSTATE, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totstate"))), "0.00"));
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFICA, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totfica"))), "0.00"));
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLMEDICARE, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totmed"))), "0.00"));
			}
			else
			{
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFED, "0.00");
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLSTATE, "0.00");
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFICA, "0.00");
				GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLMEDICARE, "0.00");
			}
		}

		private void SetupGridDeds()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			int[] aryEDeds = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int intCount = 0;
			string strTemp = "";
			string strList;
			string strList2;
			string strList3;
			string strMList;
			string strMList2;
			string strMList3;
			rsLoad.OpenRecordset("select ID,deductionnumber,description from TBLDEDUCTIONSETUP order by deductionnumber", "twpy0000.vb1");
			rsTemp.OpenRecordset("select * from tblemployeedeductions where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
			rsTemp2.OpenRecordset("select * from tblemployerSmatch where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
			strList = "";
			strList2 = "";
			strList3 = "";
			strMList = "";
			strMList2 = "";
			strMList3 = "";
			aryEDeds = new int[1 + 1];
			while (!rsTemp.EndOfFile())
			{
				if (rsLoad.FindFirstRecord("ID", rsTemp.Get_Fields("deductioncode")))
				{
					intCount = 0;
					for (x = 1; x <= (Information.UBound(aryEDeds, 1)); x++)
					{
						if (aryEDeds[x] == FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("deductionnumber"))))
						{
							intCount += 1;
						}
					}
					// x
					Array.Resize(ref aryEDeds, Information.UBound(aryEDeds, 1) + 1 + 1);
					aryEDeds[Information.UBound(aryEDeds)] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("deductionnumber"))));
					switch (intCount)
					{
						case 0:
							{
								break;
							}
						case 1:
							{
								strTemp = "2nd Occurence";
								break;
							}
						case 2:
							{
								strTemp = "3rd Occurence";
								break;
							}
						case 3:
							{
								strTemp = "4th Occurence";
								break;
							}
						default:
							{
								strTemp = FCConvert.ToString(intCount) + "th Occurence";
								break;
							}
					}
					//end switch
					if (intCount < 1)
					{
						strList += "#" + rsTemp.Get_Fields("ID") + ";" + rsLoad.Get_Fields("deductionnumber") + "\t" + rsLoad.Get_Fields_String("description") + "|";
					}
					else
					{
						strList += "#" + rsTemp.Get_Fields("ID") + ";" + rsLoad.Get_Fields("deductionnumber") + "\t" + rsLoad.Get_Fields_String("description") + "\t" + strTemp + "|";
					}
					strList2 += "#" + rsTemp.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
					strList3 += "#" + rsTemp.Get_Fields("ID") + ";" + rsLoad.Get_Fields("ID") + "|";
				}
				rsTemp.MoveNext();
			}
			aryEDeds = new int[1 + 1];
			while (!rsTemp2.EndOfFile())
			{
				if (rsLoad.FindFirstRecord("ID", rsTemp2.Get_Fields("deductioncode")))
				{
					intCount = 0;
					for (x = 1; x <= (Information.UBound(aryEDeds, 1)); x++)
					{
						if (aryEDeds[x] == FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("deductionnumber"))))
						{
							intCount += 1;
						}
					}
					// x
					Array.Resize(ref aryEDeds, Information.UBound(aryEDeds, 1) + 1 + 1);
					aryEDeds[Information.UBound(aryEDeds)] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("deductionnumber"))));
					switch (intCount)
					{
						case 0:
							{
								break;
							}
						case 1:
							{
								strTemp = "2nd Occurence";
								break;
							}
						case 2:
							{
								strTemp = "3rd Occurence";
								break;
							}
						case 3:
							{
								strTemp = "4th Occurence";
								break;
							}
						default:
							{
								strTemp = FCConvert.ToString(intCount) + "th Occurence";
								break;
							}
					}
					//end switch
					if (intCount < 1)
					{
						strMList += "#" + rsTemp2.Get_Fields("ID") + ";" + rsLoad.Get_Fields("deductionnumber") + "\t" + rsLoad.Get_Fields_String("description") + "|";
					}
					else
					{
						strMList += "#" + rsTemp2.Get_Fields("ID") + ";" + rsLoad.Get_Fields("deductionnumber") + "\t" + rsLoad.Get_Fields_String("description") + "\t" + strTemp + "|";
					}
					strMList2 += "#" + rsTemp2.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
					strMList3 += "#" + rsTemp2.Get_Fields("ID") + ";" + rsLoad.Get_Fields("ID") + "|";
				}
				rsTemp2.MoveNext();
			}
			if (strList != string.Empty)
			{
				strList = Strings.Mid(strList, 1, strList.Length - 1);
				strList2 = Strings.Mid(strList2, 1, strList2.Length - 1);
				strList3 = Strings.Mid(strList3, 1, strList3.Length - 1);
			}
			if (strMList != string.Empty)
			{
				strMList = Strings.Mid(strMList, 1, strMList.Length - 1);
				strMList2 = Strings.Mid(strMList2, 1, strMList2.Length - 1);
				strMList3 = Strings.Mid(strMList3, 1, strMList3.Length - 1);
			}
			GridDeds.ColHidden(CNSTGRIDDEDCOLDEDID, true);
			GridDeds.ColHidden(CNSTGRIDDEDCOLRECID, true);
			GridDeds.ColComboList(CNSTGRIDDEDCOLNUMBER, strList);
			GridDeds.ColComboList(CNSTGRIDDEDCOLDESC, strList2);
			GridDeds.ColComboList(CNSTGRIDDEDCOLDEDID, strList3);
			GridDeds.TextMatrix(0, CNSTGRIDDEDCOLACCOUNT, "Account");
			GridDeds.TextMatrix(0, CNSTGRIDDEDCOLAMOUNT, "Amount");
			// .TextMatrix(0, CNSTGRIDDEDCOLNUMBER) = "ID"
			GridDeds.TextMatrix(0, CNSTGRIDDEDCOLDESC, "Deduction Description");
			GridDeds.ColAlignment(CNSTGRIDDEDCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridDeds.ColHidden(CNSTGRIDDEDCOLEDITABLE, true);
			GridDeds.ColDataType(CNSTGRIDDEDCOLEDITABLE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridMatch.ColComboList(CNSTGRIDMATCHCOLNUMBER, strMList);
			GridMatch.ColComboList(CNSTGRIDMATCHCOLDESC, strMList2);
			GridMatch.ColComboList(CNSTGRIDMATCHCOLDEDID, strMList3);
		}

		private void SetupGridMatch()
		{
			GridMatch.ColHidden(CNSTGRIDMATCHCOLDEDID, true);
			GridMatch.ColHidden(CNSTGRIDMATCHCOLRECID, true);
			GridMatch.TextMatrix(0, CNSTGRIDMATCHCOLACCOUNT, "Account");
			GridMatch.TextMatrix(0, CNSTGRIDMATCHCOLGLACCOUNT, "GL Account");
			// .TextMatrix(0, CNSTGRIDMATCHCOLNUMBER) = "ID"
			GridMatch.TextMatrix(0, CNSTGRIDMATCHCOLDESC, "Match Description");
			GridMatch.TextMatrix(0, CNSTGRIDMatchCOLAmount, "Amount");
            //FC:FINAL:BSE:#4143 column should be right aligned
            GridMatch.ColAlignment(CNSTGRIDMatchCOLAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridMatch.ColHidden(CNSTGRIDMATCHCOLEDITABLE, true);
			GridMatch.ColDataType(CNSTGRIDMATCHCOLEDITABLE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridMatch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDMatchCOLAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGridDeds()
		{
			int GridWidth = 0;
			GridWidth = GridDeds.WidthOriginal;
			GridDeds.ColWidth(CNSTGRIDDEDCOLNUMBER, FCConvert.ToInt32(0.06 * GridWidth));
			GridDeds.ColWidth(CNSTGRIDDEDCOLDESC, FCConvert.ToInt32(0.44 * GridWidth));
			GridDeds.ColWidth(CNSTGRIDDEDCOLACCOUNT, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void ResizeGridMatch()
		{
			int GridWidth = 0;
			GridWidth = GridMatch.WidthOriginal;
			GridMatch.ColWidth(CNSTGRIDMATCHCOLNUMBER, FCConvert.ToInt32(0.06 * GridWidth));
			GridMatch.ColWidth(CNSTGRIDMATCHCOLDESC, FCConvert.ToInt32(0.32 * GridWidth));
			GridMatch.ColWidth(CNSTGRIDMATCHCOLGLACCOUNT, FCConvert.ToInt32(0.24 * GridWidth));
			GridMatch.ColWidth(CNSTGRIDMATCHCOLACCOUNT, FCConvert.ToInt32(0.24 * GridWidth));
		}

		private void ResizeGridPay()
		{
			int GridWidth = 0;
			GridWidth = GridPay.WidthOriginal;
			GridPay.ColWidth(0, FCConvert.ToInt32(0.35 * GridWidth));
			//GridPay.Height = 3 * GridPay.RowHeight(0) + 40;
		}

		private void LoadGridDeds(int intChk)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridDeds.Rows = 1;
				rsLoad.OpenRecordset("select * from tblcheckdetail where checkvoid = 0 and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and deductionrecord = 1 order by adjustrecord desc,ID", "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					GridDeds.Rows += 1;
					lngRow = GridDeds.Rows - 1;
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLAMOUNT, Strings.Format(rsLoad.Get_Fields("dedamount"), "0.00"));
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLACCOUNT, FCConvert.ToString(rsLoad.Get_Fields("deductionaccountnumber")));
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLDEDID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("empdeductionid"))));
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLRECID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ID"))));
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLDESC, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("EMPDEDUCTIONID"))));
					GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLNUMBER, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("empdeductionid"))));
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("adjustrecord")))
					{
						GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLEDITABLE, FCConvert.ToString(false));
						GridDeds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridDeds.Cols - 1, GridDeds.BackColorFixed);
						// .FixedRows = .Rows
					}
					else
					{
						GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLEDITABLE, FCConvert.ToString(true));
						GridDeds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDEDCOLACCOUNT, GridDeds.BackColorFixed);
						GridDeds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDEDCOLDESC, GridDeds.BackColorFixed);
					}
					GridDeds.RowData(lngRow, false);
					// not edited
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridDeds", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGridMatch(int intChk)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridMatch.Rows = 1;
				rsLoad.OpenRecordset("Select * from tblcheckdetail where checkvoid = 0 and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and matchrecord = 1 order by adjustrecord desc,ID", "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					GridMatch.Rows += 1;
					lngRow = GridMatch.Rows - 1;
					GridMatch.TextMatrix(lngRow, CNSTGRIDMatchCOLAmount, Strings.Format(rsLoad.Get_Fields("matchamount"), "0.00"));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLACCOUNT, FCConvert.ToString(rsLoad.Get_Fields("matchaccountnumber")));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLDEDID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("empdeductionid"))));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLRECID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ID"))));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLDESC, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("empdeductionid"))));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLGLACCOUNT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("matchglaccountnumber"))));
					GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLNUMBER, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("empdeductionid"))));
					if (!FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("adjustrecord")))
					{
						GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLEDITABLE, FCConvert.ToString(false));
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridMatch.Cols - 1, GridMatch.BackColorFixed);
						// .FixedRows = .Rows
					}
					else
					{
						GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLEDITABLE, FCConvert.ToString(true));
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLACCOUNT, GridMatch.BackColorFixed);
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLDESC, GridMatch.BackColorFixed);
						GridMatch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDMATCHCOLGLACCOUNT, GridMatch.BackColorFixed);
					}
					GridMatch.RowData(lngRow, false);
					// not edited
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridMatch", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGridPay(int intChk)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			double dblGross = 0;
			double dblNet = 0;
			rsLoad.OpenRecordset("select sum(distgrosspay) as totgross from tblcheckdetail where checkvoid = 0 and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and distributionrecord = 1", "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				dblGross = Conversion.Val(rsLoad.Get_Fields("totgross"));
			}
			else
			{
				dblGross = 0;
			}
			dblNet = dblGross;
			rsLoad.OpenRecordset("select sum(dedamount) as totded,sum(federaltaxwh) as totfed,sum(ficataxwh) as totfica,sum(medicaretaxwh) as totmed,sum(statetaxwh) as totstate from tblcheckdetail where checkvoid = 0 and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun), "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				dblNet -= Conversion.Val(rsLoad.Get_Fields("totded"));
				dblNet -= Conversion.Val(rsLoad.Get_Fields("totfed"));
				dblNet -= Conversion.Val(rsLoad.Get_Fields("totfica"));
				dblNet -= Conversion.Val(rsLoad.Get_Fields("totmed"));
				dblNet -= Conversion.Val(rsLoad.Get_Fields("totstate"));
			}
			GridPay.TextMatrix(1, 1, Strings.Format(dblGross, "0.00"));
			GridPay.TextMatrix(2, 1, Strings.Format(dblNet, "0.00"));
		}

		private void SetupGridDist()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strList;
			string strList2 = "";
			rsLoad.OpenRecordset("select ID,categorynumber,description from tblpaycategories order by categorynumber", "twpy0000.vb1");
			strList = "";
			while (!rsLoad.EndOfFile())
			{
				strList += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields("categorynumber") + "\t" + rsLoad.Get_Fields_String("description") + "|";
				strList2 += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strList != string.Empty)
			{
				strList = Strings.Mid(strList, 1, strList.Length - 1);
				strList2 = Strings.Mid(strList2, 1, strList2.Length - 1);
			}
			GridDist.Rows = 1;
			GridDist.ColComboList(CNSTGRIDDISTCOLPAYCAT, strList);
			GridDist.ColComboList(CNSTGRIDDISTCOLPAYCATDESC, strList2);
			GridDist.ColHidden(CNSTGRIDDISTCOLID, true);
			GridDist.ColHidden(CNSTGRIDDISTCOLADJUSTMENT, true);
			GridDist.ColDataType(CNSTGRIDDISTCOLADJUSTMENT, FCGrid.DataTypeSettings.flexDTBoolean);
			GridDist.ColAlignment(CNSTGRIDDISTCOLHOURS, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDist.ColAlignment(CNSTGRIDDISTCOLWC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDist.ColAlignment(CNSTGRIDDISTCOLWCCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDist.ColAlignment(CNSTGRIDDISTCOLGROSSPAY, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridDist.ColDataType(CNSTGRIDDISTCOLWC, FCGrid.DataTypeSettings.flexDTBoolean);
			GridDist.TextMatrix(0, CNSTGRIDDISTCOLPAYCATDESC, "Pay Category");
			GridDist.TextMatrix(0, CNSTGRIDDISTCOLGROSSPAY, "Gross");
			GridDist.TextMatrix(0, CNSTGRIDDISTCOLHOURS, "Hours");
			GridDist.TextMatrix(0, CNSTGRIDDISTCOLWC, "W/C");
			GridDist.TextMatrix(0, CNSTGRIDDISTCOLWCCODE, "W/C Code");
		}

		private void ResizeGridDist()
		{
			int GridWidth = 0;
			GridWidth = GridDist.WidthOriginal;
			GridDist.ColWidth(CNSTGRIDDISTCOLPAYCAT, FCConvert.ToInt32(0.07 * GridWidth));
			GridDist.ColWidth(CNSTGRIDDISTCOLPAYCATDESC, FCConvert.ToInt32(0.32 * GridWidth));
			GridDist.ColWidth(CNSTGRIDDISTCOLGROSSPAY, FCConvert.ToInt32(0.15 * GridWidth));
			GridDist.ColWidth(CNSTGRIDDISTCOLHOURS, FCConvert.ToInt32(0.1 * GridWidth));
			GridDist.ColWidth(CNSTGRIDDISTCOLWC, FCConvert.ToInt32(0.08 * GridWidth));
			GridDist.ColWidth(CNSTGRIDDISTCOLWCCODE, FCConvert.ToInt32(0.12 * GridWidth));
		}

		private void LoadGridDist(int intChk)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				GridDist.Rows = 1;
				rsLoad.OpenRecordset("select * from tblcheckdetail where distributionrecord = 1 and checkvoid = 0 and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " order by ID", "twpy0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					GridDist.Rows += 1;
					lngRow = GridDist.Rows - 1;
					GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("ID"))));
					GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLPAYCAT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("distpaycategory"))));
					GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLPAYCATDESC, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("distpaycategory"))));
					GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLHOURS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("disthours"))));
					GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLGROSSPAY, Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("distgrosspay"))), "0.00"));
					
					boolFromSchool = false;
					
					if ((fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("DISTWCCODE"))) + " ", 1)) == "N") || (fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("DISTWCCODE"))) + " ", 1)) == ""))
					{
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWC, FCConvert.ToString(false));
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWCCODE, "");
					}
					else if (fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("DISTWCCODE"))) + " ", 1)) == "Y")
					{
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWC, FCConvert.ToString(true));
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWCCODE, "");
					}
					else
					{
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWC, FCConvert.ToString(true));
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWCCODE, FCConvert.ToString(rsLoad.Get_Fields("distwccode")));
					}
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("adjustrecord")))
					{
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLADJUSTMENT, FCConvert.ToString(true));
						GridDist.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDISTCOLPAYCATDESC, GridDist.BackColorFixed);
					}
					else
					{
						GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLADJUSTMENT, FCConvert.ToString(false));
						GridDist.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDDISTCOLPAYCAT, lngRow, CNSTGRIDDISTCOLGROSSPAY, GridDist.BackColorFixed);
					}
					GridDist.RowData(lngRow, false);
					// not edited
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in LoadGridDist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RecalcTotals()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				double dblFedTax = 0;
				double dblFicaTax = 0;
				double dblMedTax = 0;
				double dblStateTax = 0;
				double dblGross;
				double dblDed;
				double dblNet;
				int x;
				// GridTotals.Row = 0
				// GridDeds.Row = 0
				// GridMatch.Row = 0
				//App.DoEvents();
				dblFedTax = Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED));
				dblFicaTax = Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA));
				dblMedTax = Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE));
				dblStateTax = Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE));
				dblDed = 0;
				for (x = 1; x <= GridDeds.Rows - 1; x++)
				{
					dblDed += Conversion.Val(GridDeds.TextMatrix(x, CNSTGRIDDEDCOLAMOUNT));
				}
				// x
				dblGross = 0;
				for (x = 1; x <= GridDist.Rows - 1; x++)
				{
					dblGross += Conversion.Val(GridDist.TextMatrix(x, CNSTGRIDDISTCOLGROSSPAY));
				}
				// x
				dblNet = dblGross;
				dblNet -= dblDed;
				dblNet -= dblFedTax;
				dblNet -= dblFicaTax;
				dblNet -= dblMedTax;
				dblNet -= dblStateTax;
				GridPay.TextMatrix(1, 1, Strings.Format(dblGross, "0.00"));
				GridPay.TextMatrix(2, 1, Strings.Format(dblNet, "0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RecalcTotals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				int lngID = 0;
				double dblTemp = 0;
				double dblFedTax = 0;
				double dblFedGross = 0;
				double dblFicaTax = 0;
				double dblFicaGross = 0;
				double dblMedTax = 0;
				double dblMedGross = 0;
				double dblStateTax = 0;
				double dblStateGross = 0;
				double dblNet = 0;
				double dblTotalGross;
				int x;
				int intChk;
				GridTotals.Row = 0;
				GridDeds.Row = 0;
				GridMatch.Row = 0;
				GridDist.Row = 0;
				gridCheck.Row = 0;
				//App.DoEvents();
				RecalcTotals();
				intChk = FCConvert.ToInt32(Math.Round(Conversion.Val(gridCheck.TextMatrix(0, 0))));
				for (lngRow = 1; lngRow <= GridDeds.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLNUMBER)) <= 0)
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no deduction type chosen" + "\r\n" + "Cannot continue", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridMatch.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLNUMBER)) <= 0)
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no match type chosen" + "\r\n" + "Cannot continue", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridVacSick.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLTYPE)) <= 0)
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no type chosen", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridDist.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLPAYCAT)) <= 0)
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no pay category chosen" + "\r\n" + "Cannot continue", "Invalid Pay Category", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				// Dave 12/14/2006------------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Edit Pay Totals", strEmployeeNumber, fecherFoundation.Strings.Trim(dtPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(intPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (lngRow = 1; lngRow <= GridDeds.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLNUMBER)) > 0)
					{
						if (FCConvert.CBool(GridDeds.RowData(lngRow)))
						{
							// has been edited
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLRECID))));
							rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("employeename", strEmpName);
								rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(Conversion.Val(gridCheck.TextMatrix(0, 0))));
							}
							rsSave.Set_Fields("adjustrecord", true);
							rsSave.Set_Fields("deductionrecord", true);
							rsSave.Set_Fields("employeenumber", strEmployeeNumber);
							rsSave.Set_Fields("Paydate", dtPayDate);
							rsSave.Set_Fields("payrunid", intPayRun);
							dblTemp = Conversion.Val(GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLAMOUNT));
							rsSave.Set_Fields("dedamount", dblTemp);
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(GridDeds.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDDEDCOLDESC))) != string.Empty)
							{
								rsSave.Set_Fields("deductiontitle", GridDeds.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDDEDCOLDESC));
							}
							else
							{
								rsSave.Set_Fields("deductiontitle", " ");
							}
							// rsSave.Fields("deddeductionnumber") = Val(.TextMatrix(lngRow, CNSTGRIDDEDCOLNUMBER)) 'not the visible number, but the id
							rsSave.Set_Fields("deddeductionnumber", FCConvert.ToString(Conversion.Val(GridDeds.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDDEDCOLDEDID))));
							rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLNUMBER))));
							// rsSave.Fields("netpay") = -dblTemp
							lngID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							GridDeds.TextMatrix(lngRow, CNSTGRIDDEDCOLRECID, FCConvert.ToString(lngID));
							rsSave.Update();
						}
						GridDeds.RowData(lngRow, false);
					}
					else
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no deduction type chosen" + "\r\n" + "Cannot continue", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridMatch.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLNUMBER)) > 0)
					{
						if (FCConvert.CBool(GridMatch.RowData(lngRow)))
						{
							// has been edited
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLRECID))));
							rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("employeename", strEmpName);
								rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(Conversion.Val(gridCheck.TextMatrix(0, 0))));
							}
							rsSave.Set_Fields("adjustrecord", true);
							rsSave.Set_Fields("matchrecord", true);
							rsSave.Set_Fields("employeenumber", strEmployeeNumber);
							rsSave.Set_Fields("paydate", dtPayDate);
							rsSave.Set_Fields("payrunid", intPayRun);
							dblTemp = Conversion.Val(GridMatch.TextMatrix(lngRow, CNSTGRIDMatchCOLAmount));
							rsSave.Set_Fields("matchamount", dblTemp);
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(GridMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDMATCHCOLDESC))) != string.Empty)
							{
								rsSave.Set_Fields("matchdeductiontitle", GridMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDMATCHCOLDESC));
							}
							else
							{
								rsSave.Set_Fields("matchdeductiontitle", " ");
							}
							// rsSave.Fields("matchdeductionnumber") = Val(.TextMatrix(lngRow, CNSTGRIDMATCHCOLNUMBER)) 'not the visible number, but the id
							rsSave.Set_Fields("matchdeductionnumber", FCConvert.ToString(Conversion.Val(GridMatch.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDMATCHCOLDEDID))));
							rsSave.Set_Fields("empdeductionid", FCConvert.ToString(Conversion.Val(GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLNUMBER))));
							rsSave.Update();
							lngID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							GridMatch.TextMatrix(lngRow, CNSTGRIDMATCHCOLRECID, FCConvert.ToString(lngID));
						}
						GridMatch.RowData(lngRow, false);
					}
					else
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no match type chosen" + "\r\n" + "Cannot continue", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridVacSick.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLTYPE)) > 0)
					{
						if (FCConvert.CBool(GridVacSick.RowData(lngRow)))
						{
							// has been edited
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLID))));
							rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("employeename", strEmpName);
								rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(Conversion.Val(gridCheck.TextMatrix(0, 0))));
								rsSave.Set_Fields("Adjustrecord", true);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("paydate", dtPayDate);
								rsSave.Set_Fields("payrunid", intPayRun);
								rsSave.Set_Fields("vsrecord", true);
							}
							rsSave.Set_Fields("vsaccrued", FCConvert.ToString(Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLACCRUED))));
							rsSave.Set_Fields("vsused", FCConvert.ToString(Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLUSED))));
							rsSave.Set_Fields("vsbalance", FCConvert.ToString(Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLBALANCE))));
							rsSave.Set_Fields("vstypeid", FCConvert.ToString(Conversion.Val(GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLTYPE))));
							rsSave.Update();
							lngID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLID, FCConvert.ToString(lngID));
						}
						GridVacSick.RowData(lngRow, false);
					}
					else
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no type chosen", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridDist.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLPAYCAT)) > 0)
					{
						if (FCConvert.CBool(GridDist.RowData(lngRow)))
						{
							// has been edited
							lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLID))));
							rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(lngID), "twpy0000.vb1");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("employeename", strEmpName);
								rsSave.Set_Fields("adjustrecord", true);
								rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(Conversion.Val(gridCheck.TextMatrix(0, 0))));
								GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLADJUSTMENT, FCConvert.ToString(true));
								rsSave.Set_Fields("paydate", dtPayDate);
								rsSave.Set_Fields("payrunid", intPayRun);
								rsSave.Set_Fields("employeenumber", strEmployeeNumber);
								rsSave.Set_Fields("distributionrecord", true);
								rsSave.Set_Fields("fromschool", boolFromSchool);
							}
							if (FCConvert.CBool(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLADJUSTMENT)))
							{
								dblTemp = Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLGROSSPAY));
								rsSave.Set_Fields("grosspay", dblTemp);
								rsSave.Set_Fields("distgrosspay", dblTemp);
								// rsSave.Fields("netpay") = dblTemp
								rsSave.Set_Fields("distpaycategory", FCConvert.ToString(Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLPAYCAT))));
							}
							rsSave.Set_Fields("disthours", FCConvert.ToString(Conversion.Val(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLHOURS))));
							if (!FCConvert.CBool(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWC)))
							{
								rsSave.Set_Fields("distwccode", "N");
							}
							else
							{
								if (fecherFoundation.Strings.Trim(GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWCCODE)) != string.Empty)
								{
									rsSave.Set_Fields("distwccode", GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLWCCODE));
								}
								else
								{
									rsSave.Set_Fields("distwccode", "Y");
								}
							}
							rsSave.Update();
							lngID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							GridDist.TextMatrix(lngRow, CNSTGRIDDISTCOLID, FCConvert.ToString(lngID));
						}
						GridDist.RowData(lngRow, false);
					}
					else
					{
						MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no pay category chosen" + "\r\n" + "Cannot continue", "Invalid Pay Category", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				// lngRow
				// now calculate differences in totals
				if (Conversion.Val(GridTotals.Tag) > 0)
				{
					rsSave.OpenRecordset("select sum(federaltaxWH) as totFed,sum(stateTaxWH) as totState,sum(ficataxwh) as totfica,sum(medicaretaxwh) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and ID <> " + FCConvert.ToString(Conversion.Val(GridTotals.Tag)) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				}
				else
				{
					rsSave.OpenRecordset("select sum(federaltaxWH) as totFed,sum(stateTaxWH) as totState,sum(ficataxwh) as totfica,sum(medicaretaxwh) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				}
				if (!rsSave.EndOfFile())
				{
					dblFedTax = Conversion.Val(rsSave.Get_Fields("totfed"));
					dblFicaTax = Conversion.Val(rsSave.Get_Fields("totfica"));
					dblMedTax = Conversion.Val(rsSave.Get_Fields("totmed"));
					dblStateTax = Conversion.Val(rsSave.Get_Fields("totstate"));
				}
				else
				{
					dblFedTax = 0;
					dblFicaTax = 0;
					dblMedTax = 0;
					dblStateTax = 0;
				}
				bool boolChanges = false;
				boolChanges = false;
				if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED)) != dblFedTax)
				{
					boolChanges = true;
				}
				else if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA)) != dblFicaTax)
				{
					boolChanges = true;
				}
				else if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE)) != dblMedTax)
				{
					boolChanges = true;
				}
				else if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE)) != dblStateTax)
				{
					boolChanges = true;
				}
				
				if (boolChanges)
				{
					rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(Conversion.Val(GridTotals.Tag)), "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(intChk));
						rsSave.Set_Fields("paydate", dtPayDate);
						rsSave.Set_Fields("payrunid", intPayRun);
						rsSave.Set_Fields("employeenumber", strEmployeeNumber);
						rsSave.Set_Fields("employeename", strEmpName);
						rsSave.Set_Fields("totalrecord", true);
						rsSave.Set_Fields("adjustrecord", true);
					}
					if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED)) != dblFedTax)
					{
						rsSave.Set_Fields("federaltaxwh", Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFED)) - dblFedTax);
					}
					if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA)) != dblFicaTax)
					{
						rsSave.Set_Fields("ficataxwh", Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLFICA)) - dblFicaTax);
					}
					if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE)) != dblMedTax)
					{
						rsSave.Set_Fields("medicaretaxwh", Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLMEDICARE)) - dblMedTax);
					}
					if (Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE)) != dblStateTax)
					{
						rsSave.Set_Fields("statetaxwh", Conversion.Val(GridTotals.TextMatrix(2, CNSTGRIDTOTALSCOLSTATE)) - dblStateTax);
					}
					
					rsSave.Update();
					GridTotals.Tag = (System.Object)(rsSave.Get_Fields("ID"));
				}
				boolChanges = false;
				if (Conversion.Val(GridTotals.Tag) > 0)
				{
					rsSave.OpenRecordset("select sum(federaltaxgross) as totFed,sum(stateTaxgross) as totState,sum(ficataxgross) as totfica,sum(medicaretaxgross) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and ID <> " + FCConvert.ToString(Conversion.Val(GridTotals.Tag)), "twpy0000.vb1");
				}
				else
				{
					rsSave.OpenRecordset("select sum(federaltaxgross) as totFed,sum(stateTaxgross) as totState,sum(ficataxgross) as totfica,sum(medicaretaxgross) as totmed from tblcheckdetail where totalrecord = 1 and checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				}
				if (!rsSave.EndOfFile())
				{
					dblFedGross = Conversion.Val(rsSave.Get_Fields("totfed"));
					dblFicaGross = Conversion.Val(rsSave.Get_Fields("totfica"));
					dblMedGross = Conversion.Val(rsSave.Get_Fields("totmed"));
					dblStateGross = Conversion.Val(rsSave.Get_Fields("totstate"));
				}
				else
				{
					dblFedGross = 0;
					dblFicaGross = 0;
					dblMedGross = 0;
					dblStateGross = 0;
				}
				if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFED)) != dblFedGross)
				{
					boolChanges = true;
				}
				if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFICA)) != dblFicaGross)
				{
					boolChanges = true;
				}
				if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLMEDICARE)) != dblMedGross)
				{
					boolChanges = true;
				}
				if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLSTATE)) != dblStateGross)
				{
					boolChanges = true;
				}
				
				if (Conversion.Val(GridTotals.Tag) > 0)
				{
					rsSave.OpenRecordset("select sum(netpay) as totnet from tblcheckdetail where checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and ID <> " + FCConvert.ToString(Conversion.Val(GridTotals.Tag)) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				}
				else
				{
					rsSave.OpenRecordset("select sum(netpay) as totnet from tblcheckdetail where checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				}
				if (!rsSave.EndOfFile())
				{
					dblNet = Conversion.Val(rsSave.Get_Fields("totnet"));
				}
				else
				{
					dblNet = 0;
				}
				if (Conversion.Val(GridPay.TextMatrix(2, 1)) != dblNet)
				{
					if (!(Conversion.Val(GridTotals.Tag) > 0 && dblNet == 0))
					{
						boolChanges = true;
					}
				}
				if (boolChanges)
				{
					rsSave.OpenRecordset("select * from tblcheckdetail where ID = " + FCConvert.ToString(Conversion.Val(GridTotals.Tag)), "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("masterrecord", "P" + FCConvert.ToString(intChk));
						rsSave.Set_Fields("paydate", dtPayDate);
						rsSave.Set_Fields("payrunid", intPayRun);
						rsSave.Set_Fields("employeenumber", strEmployeeNumber);
						rsSave.Set_Fields("employeename", strEmpName);
						rsSave.Set_Fields("adjustrecord", true);
						rsSave.Set_Fields("totalrecord", true);
						rsSave.Set_Fields("fromschool", boolFromSchool);
						if (Conversion.Val(GridPay.TextMatrix(2, 1)) != dblNet)
						{
							// If dblNet <> 0 Or Val(GridTotals.Tag) = 0 Then
							rsSave.Set_Fields("netpay", Conversion.Val(GridPay.TextMatrix(2, 1)) - dblNet);
							// End If
						}
					}
					if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFED)) != dblFedGross)
					{
						rsSave.Set_Fields("federaltaxgross", Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFED)) - dblFedGross);
					}
					if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFICA)) != dblFicaGross)
					{
						rsSave.Set_Fields("ficataxgross", Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLFICA)) - dblFicaGross);
					}
					if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLMEDICARE)) != dblMedGross)
					{
						rsSave.Set_Fields("medicaretaxgross", Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLMEDICARE)) - dblMedGross);
					}
					if (Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLSTATE)) != dblStateGross)
					{
						rsSave.Set_Fields("statetaxgross", Conversion.Val(GridTotals.TextMatrix(1, CNSTGRIDTOTALSCOLSTATE)) - dblStateGross);
					}
					
					if (Conversion.Val(GridPay.TextMatrix(2, 1)) != dblNet)
					{
						if (dblNet != 0 || Conversion.Val(GridTotals.Tag) == 0)
						{
							rsSave.Set_Fields("netpay", Conversion.Val(GridPay.TextMatrix(2, 1)) - dblNet);
						}
					}
					rsSave.Update();
					GridTotals.Tag = (System.Object)(rsSave.Get_Fields("ID"));
				}
				rsSave.Execute("update tblcheckdetail set checknumber = " + FCConvert.ToString(Conversion.Val(txtCheckNumber.Text)) + " where checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayRun) + "  and masterrecord = 'P" + FCConvert.ToString(intChk) + "'", "twpy0000.vb1");
				SaveInfo = true;
				blnDataChanged = false;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void SetupGridVacSick()
		{
			string strList = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			GridVacSick.Rows = 1;
			GridVacSick.ColHidden(CNSTGRIDVACSICKCOLID, true);
			GridVacSick.TextMatrix(0, CNSTGRIDVACSICKCOLTYPE, "Type");
			GridVacSick.TextMatrix(0, CNSTGRIDVACSICKCOLACCRUED, "Accrued");
			GridVacSick.TextMatrix(0, CNSTGRIDVACSICKCOLUSED, "Used");
			GridVacSick.TextMatrix(0, CNSTGRIDVACSICKCOLBALANCE, "Balance");
			rsLoad.OpenRecordset("select * from tblcodetypes order by ID", "twpy0000.vb1");
			strList = "";
			while (!rsLoad.EndOfFile())
			{
				strList += "#" + rsLoad.Get_Fields("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strList != string.Empty)
			{
				strList = Strings.Mid(strList, 1, strList.Length - 1);
			}
			GridVacSick.ColComboList(CNSTGRIDVACSICKCOLTYPE, strList);
		}

		private void ResizeGridVacSick()
		{
			int GridWidth = 0;
			GridWidth = GridVacSick.WidthOriginal;
			GridVacSick.ColWidth(CNSTGRIDVACSICKCOLTYPE, FCConvert.ToInt32(0.42 * GridWidth));
			GridVacSick.ColWidth(CNSTGRIDVACSICKCOLACCRUED, FCConvert.ToInt32(0.18 * GridWidth));
			GridVacSick.ColWidth(CNSTGRIDVACSICKCOLUSED, FCConvert.ToInt32(0.18 * GridWidth));
		}

		private void LoadGridVacSick(int intChk)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			GridVacSick.Rows = 1;
			rsLoad.OpenRecordset("select * from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and masterrecord = 'P" + FCConvert.ToString(intChk) + "' and PAYRUNID = " + FCConvert.ToString(intPayRun) + " and vsrecord = 1 and employeenumber = '" + strEmployeeNumber + "' and checkvoid = 0 order by vstypeid", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				GridVacSick.Rows += 1;
				lngRow = GridVacSick.Rows - 1;
				GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLID, FCConvert.ToString(rsLoad.Get_Fields("ID")));
				GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLTYPE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("vstypeid"))));
				GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLACCRUED, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("vsaccrued"))));
				GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLUSED, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("vsused"))));
				GridVacSick.TextMatrix(lngRow, CNSTGRIDVACSICKCOLBALANCE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("vsbalance"))));
				GridVacSick.RowData(lngRow, false);
				rsLoad.MoveNext();
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (GridDist.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (GridDist.Cols - 1); intCols++)
				{
					if (intCols != 2)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "GridDist";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Distribution Grid  Row " + FCConvert.ToString(intRows) + " " + GridDist.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
			for (intRows = 1; intRows <= (GridDeds.Rows - 1); intRows++)
			{
				for (intCols = 0; intCols <= (GridDeds.Cols - 1); intCols++)
				{
					if (intCols == 0 || intCols == 3)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "GridDeds";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Deductions Grid  Row " + FCConvert.ToString(intRows) + " " + GridDeds.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
			for (intRows = 1; intRows <= (GridMatch.Rows - 1); intRows++)
			{
				for (intCols = 0; intCols <= (GridMatch.Cols - 1); intCols++)
				{
					if (intCols == 0 || intCols == 4)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "GridMatch";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Employee Match Grid  Row " + FCConvert.ToString(intRows) + " " + GridMatch.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
			for (intRows = 1; intRows <= (GridVacSick.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (GridVacSick.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "GridVacSick";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Vacation Sick Grid  Row " + FCConvert.ToString(intRows) + " " + GridVacSick.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
			for (intRows = 1; intRows <= (GridTotals.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (GridTotals.Cols - 1); intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "GridTotals";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Totals Grid  " + GridTotals.TextMatrix(0, intCols) + " " + GridTotals.TextMatrix(intRows, 0);
					intTotalNumberOfControls += 1;
				}
			}
		}

		private void txtCheckNumber_TextChanged(object sender, System.EventArgs e)
		{
			blnDataChanged = true;
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }
    }
}
