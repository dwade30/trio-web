﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cPayCategory
	{
		//=========================================================
		private int lngRecordID;
		private int lngCategoryNumber;
		private string strDescription = string.Empty;
		private double dblMulti;
		private string strType = string.Empty;
		private int lngTaxStatus;
		private bool boolMSR;
		private bool boolUnemployment;
		private bool boolWorkersComp;
		private int lngExemptCode1;
		private string strExemptType1 = string.Empty;
		private int lngExemptCode2;
		private string strExemptType2 = string.Empty;
		private int lngExemptCode3;
		private string strExemptType3 = string.Empty;
		private int lngExemptCode4;
		private string strExemptType4 = string.Empty;
		private int lngExemptCode5;
		private string strExemptType5 = string.Empty;
		private string strLastUserID = string.Empty;
		private string strLastUpdate = "";

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int CategoryNumber
		{
			set
			{
				lngCategoryNumber = value;
			}
			get
			{
				int CategoryNumber = 0;
				CategoryNumber = lngCategoryNumber;
				return CategoryNumber;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public double Multiplier
		{
			set
			{
				dblMulti = value;
			}
			get
			{
				double Multiplier = 0;
				Multiplier = dblMulti;
				return Multiplier;
			}
		}

		public string UnitType
		{
			set
			{
				strType = value;
			}
			get
			{
				string UnitType = "";
				UnitType = strType;
				return UnitType;
			}
		}

		public int TaxStatus
		{
			set
			{
				lngTaxStatus = value;
			}
			get
			{
				int TaxStatus = 0;
				TaxStatus = lngTaxStatus;
				return TaxStatus;
			}
		}

		public bool MSR
		{
			set
			{
				boolMSR = value;
			}
			get
			{
				bool MSR = false;
				MSR = boolMSR;
				return MSR;
			}
		}

		public bool Unemployment
		{
			set
			{
				boolUnemployment = value;
			}
			get
			{
				bool Unemployment = false;
				Unemployment = boolUnemployment;
				return Unemployment;
			}
		}

		public bool WorkersComp
		{
			set
			{
				boolWorkersComp = value;
			}
			get
			{
				bool WorkersComp = false;
				WorkersComp = boolWorkersComp;
				return WorkersComp;
			}
		}

		public int ExemptCode1
		{
			set
			{
				lngExemptCode1 = value;
			}
			get
			{
				int ExemptCode1 = 0;
				ExemptCode1 = lngExemptCode1;
				return ExemptCode1;
			}
		}

		public string ExemptType1
		{
			set
			{
				strExemptType1 = value;
			}
			get
			{
				string ExemptType1 = "";
				ExemptType1 = strExemptType1;
				return ExemptType1;
			}
		}

		public int ExemptCode2
		{
			set
			{
				lngExemptCode2 = value;
			}
			get
			{
				int ExemptCode2 = 0;
				ExemptCode2 = lngExemptCode2;
				return ExemptCode2;
			}
		}

		public string ExemptType2
		{
			set
			{
				strExemptType2 = value;
			}
			get
			{
				string ExemptType2 = "";
				ExemptType2 = strExemptType2;
				return ExemptType2;
			}
		}

		public int ExemptCode3
		{
			set
			{
				lngExemptCode3 = value;
			}
			get
			{
				int ExemptCode3 = 0;
				ExemptCode3 = lngExemptCode3;
				return ExemptCode3;
			}
		}

		public string ExemptType3
		{
			set
			{
				strExemptType3 = value;
			}
			get
			{
				string ExemptType3 = "";
				ExemptType3 = strExemptType3;
				return ExemptType3;
			}
		}

		public int ExemptCode4
		{
			set
			{
				lngExemptCode4 = value;
			}
			get
			{
				int ExemptCode4 = 0;
				ExemptCode4 = lngExemptCode4;
				return ExemptCode4;
			}
		}

		public string ExemptType4
		{
			set
			{
				strExemptType4 = value;
			}
			get
			{
				string ExemptType4 = "";
				ExemptType4 = strExemptType4;
				return ExemptType4;
			}
		}

		public int ExemptCode5
		{
			set
			{
				lngExemptCode5 = value;
			}
			get
			{
				int ExemptCode5 = 0;
				ExemptCode5 = lngExemptCode5;
				return ExemptCode5;
			}
		}

		public string ExemptType5
		{
			set
			{
				strExemptType5 = value;
			}
			get
			{
				string ExemptType5 = "";
				ExemptType5 = strExemptType5;
				return ExemptType5;
			}
		}

		public string LastUserID
		{
			set
			{
				strLastUserID = value;
			}
			get
			{
				string LastUserID = "";
				LastUserID = strLastUserID;
				return LastUserID;
			}
		}

		public string LastUpdate
		{
			set
			{
				if (fecherFoundation.Information.IsDate(value))
				{
					strLastUpdate = value;
				}
				else
				{
					strLastUpdate = "";
				}
			}
			get
			{
				string LastUpdate = "";
				LastUpdate = strLastUpdate;
				return LastUpdate;
			}
		}
	}
}
