//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptFedStateFilingStatus.
	/// </summary>
	public partial class rptFedStateFilingStatus : BaseSectionReport
	{
		public rptFedStateFilingStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Federal / State Filing Status";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptFedStateFilingStatus InstancePtr
		{
			get
			{
				return (rptFedStateFilingStatus)Sys.GetInstance(typeof(rptFedStateFilingStatus));
			}
		}

		protected rptFedStateFilingStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptFedStateFilingStatus	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       June 26,2001
		//
		// **************************************************
		// private local variables
		int intpage;
		int intCounter;

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "FedStateFilingStatus");
			// Me.Show , MDIParent
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (intCounter >= Information.UBound(modGlobalRoutines.Statics.aryFedStateFilingStatus, 1))
			{
				eArgs.EOF = true;
			}
			else
			{
				txtName.Text = modGlobalRoutines.Statics.aryFedStateFilingStatus[intCounter].strData;
				chkFederal.Checked = modGlobalRoutines.Statics.aryFedStateFilingStatus[intCounter].boolType == "FEDERAL" ? true : false;
				chkState.Checked = modGlobalRoutines.Statics.aryFedStateFilingStatus[intCounter].boolType == "STATE";
				if (intCounter != Information.UBound(modGlobalRoutines.Statics.aryFedStateFilingStatus, 1))
				{
					if (txtName.Text == modGlobalRoutines.Statics.aryFedStateFilingStatus[intCounter + 1].strData)
					{
						chkState.Checked = modGlobalRoutines.Statics.aryFedStateFilingStatus[intCounter + 1].boolType == "STATE";
						intCounter += 1;
					}
				}
				intCounter += 1;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Vacation Codes ActiveReport_Initialize";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				intCounter = 2;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtCaption.Text = "Federal / State Filing Status Change";
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		
	}
}