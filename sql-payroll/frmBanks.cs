//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmBanks : BaseForm
	{
		public frmBanks()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBanks InstancePtr
		{
			get
			{
				return (frmBanks)Sys.GetInstance(typeof(frmBanks));
			}
		}

		protected frmBanks _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 08,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		//
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsBanks = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private int intDataChanged;
		private bool boolNew;
		private clsHistory clsHistoryClass = new clsHistory();
		private int lngCurACHBank;
		private bool boolASC;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// if there is no active current row then we cannot delete anything
			if (vsBanks.Row <= 0)
			{
				MessageBox.Show("Row to delete must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			if (vsBanks.Col <= 0)
			{
				MessageBox.Show("Row to delete must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			if (MessageBox.Show("This action will delete record #" + vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsBanks.Row, 1) + ". Continue?", "Payroll Bank Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				// NEED TO CHECK TO MAKE SURE THAT NO EMPLOYEE HAS USE THAT BANK
				rsBanks.OpenRecordset("Select * from tblDirectDeposit where Bank = " + vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsBanks.Row, 4));
				if (rsBanks.EndOfFile())
				{
				}
				else
				{
					MessageBox.Show("This bank is currently being used and cannot be removed." + "\r" + "One person assigned to this bank is Employee Number: " + rsBanks.Get_Fields("EmployeeNumber"));
					return;
				}
				// delete the record from the database
				rsBanks.Execute("Delete from tblBanks where ID = " + vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsBanks.Row, 4), "Payroll");
				// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
				clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsBanks);
				// Dave 12/14/2006---------------------------------------------------
				// This function will go through the control information class and set the control type to DeletedControl for every item in this grid that was on the line
				modAuditReporting.RemoveGridRow_8("vsBanks", intTotalNumberOfControls - 1, vsBanks.Row, clsControlInfo);
				// We then add a change record saying the row was deleted
				clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsBanks.Row) + " from Direct Deposit Banks");
				// -------------------------------------------------------------------
				// delete the record from the grid
				vsBanks.RemoveItem(vsBanks.Row);
				// 
				LoadGrid();
				// decrement the counter as to how many records are dirty\
				intDataChanged -= 1;
				MessageBox.Show("Record Deleted successfully.", "Payroll Banks", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// unload the form
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// have any rows been altered?
			if (intDataChanged > 0)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Banks", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// save all changes
					cmdSave_Click();
				}
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			int lngBankNum;
			int x;
			lngBankNum = 0;
			for (x = 1; x <= vsBanks.Rows - 1; x++)
			{
				if (Conversion.Val(vsBanks.TextMatrix(x, 1)) > lngBankNum)
				{
					lngBankNum = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBanks.TextMatrix(x, 1))));
				}
			}
			// x
			// append a new row to the end of the grid
			vsBanks.AddItem(FCConvert.ToString(vsBanks.Rows));
			// increment the counter as to how many records are dirty
			intDataChanged += 1;
			// assign a row number to this new record
			vsBanks.TextMatrix(vsBanks.Rows - 1, 0, vsBanks.TextMatrix(vsBanks.Rows - 1, 0) + ".");
			vsBanks.TextMatrix(vsBanks.Rows - 1, 1, FCConvert.ToString(lngBankNum + 1));
			// vsBanks.TextMatrix(vsBanks.Rows - 1, 1) = Format(Val(vsBanks.TextMatrix(vsBanks.Rows - 2, 1)) + 1, "00000")
			vsBanks.Select(vsBanks.Rows - 1, 2);
			vsBanks.EditCell();
			vsBanks.LeftCol = 2;
			// Dave 12/14/2006---------------------------------------------------
			// Add change record for adding a row to the grid
			clsReportChanges.AddChange("Added Row to Direct Deposit Banks");
			// ------------------------------------------------------------------
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// display the frequency code report
			// rptBanks.Show
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// verify there are not 'dirty' record
			SaveChanges();
			// load the grid of current data from the database
			LoadGrid();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CallErrorRoutine = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				vOnErrorGoToLabel = curOnErrorGoToLabel_CallErrorRoutine;
				/* On Error GoTo CallErrorRoutine */
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsBanks.Select(0, 0);
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Direct Deposit Banks", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (intCounter = 1; intCounter <= (vsBanks.Rows - 1); intCounter++)
				{
					rsBanks.OpenRecordset("Select * from tblBanks where ID = " + FCConvert.ToString(Conversion.Val(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 4))), "TWPY0000.vb1");
					if (Strings.Right(FCConvert.ToString(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
					{
						boolNew = false;
						rsBanks.Edit();
					}
					else if (Strings.Right(FCConvert.ToString(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
					{
						boolNew = true;
						rsBanks.AddNew();
					}
					else
					{
						goto NextRecord;
					}
					// rsBanks.SetData "RecordNumber", Val(Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & " "))
					rsBanks.Set_Fields("RecordNumber", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(vsBanks.TextMatrix(intCounter, 1)))));
					rsBanks.SetData("Name", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + " "));
					rsBanks.SetData("Address1", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3) + " "));
					rsBanks.SetData("Address2", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5) + " "));
					rsBanks.SetData("Address3", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6) + " "));
					rsBanks.SetData("ShortDescription", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 10) + " "));
					// NEW ADDED 5/20/2003 MATTHEW
					rsBanks.SetData("BankDepositID", fecherFoundation.Strings.Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7) + " "));
					rsBanks.Set_Fields("eft", FCConvert.CBool(Conversion.Val(vsBanks.TextMatrix(intCounter, 8))) == true);
					if (vsBanks.TextMatrix(intCounter, 9) == string.Empty)
					{
						rsBanks.Set_Fields("achbank", false);
					}
					else
					{
						rsBanks.Set_Fields("achbank", FCConvert.CBool(vsBanks.TextMatrix(intCounter, 9)) == true);
					}
					if (FCConvert.ToBoolean(rsBanks.Get_Fields_Boolean("achbank")))
					{
						if (lngCurACHBank != FCConvert.ToInt32(rsBanks.Get_Fields("ID")))
						{
							// change the ach info in the table since the bank changed
							clsSave.OpenRecordset("select * from tblachinformation", "twpy0000.vb1");
							if (clsSave.EndOfFile())
							{
								clsSave.AddNew();
							}
							else
							{
								clsSave.Edit();
							}
							clsSave.Set_Fields("ACHBankRT", rsBanks.Get_Fields_String("BankDepositID"));
							clsSave.Set_Fields("ACHBankName", Strings.Mid(fecherFoundation.Strings.Trim(FCConvert.ToString(rsBanks.Get_Fields("name"))) + Strings.StrDup(23, " "), 1, 23));
							clsSave.Update();
						}
					}
					rsBanks.SetData("LastUserID", modGlobalVariables.Statics.gstrUser);
					if (FCConvert.ToString(rsBanks.Get_Fields_String("Name")) == string.Empty && FCConvert.ToString(rsBanks.Get_Fields_String("Address1")) == string.Empty && FCConvert.ToString(rsBanks.Get_Fields_String("Address2")) == string.Empty && FCConvert.ToString(rsBanks.Get_Fields("Address3")) == string.Empty)
					{
					}
					else
					{
						rsBanks.Update();
						/*? On Error Resume Next  */
						if (boolNew)
						{
							// clear the new symbol from the row number
							vsBanks.TextMatrix(intCounter, 0, Strings.Mid(vsBanks.TextMatrix(intCounter, 0), 1, vsBanks.TextMatrix(intCounter, 0).Length - 1));
							// get the new ID from the table for the record that was just added and place it into column 3
							rsBanks.OpenRecordset("Select Max(ID) as NewID from tblBanks", "TWPY0000.vb1");
							vsBanks.TextMatrix(intCounter, 1, Strings.Format(rsBanks.Get_Fields("NewID"), "00000"));
						}
						else
						{
							// clear the edit symbol from the row number
							vsBanks.TextMatrix(intCounter, 0, Strings.Mid(vsBanks.TextMatrix(intCounter, 0), 1, vsBanks.TextMatrix(intCounter, 0).Length - 2));
						}
						vOnErrorGoToLabel = curOnErrorGoToLabel_CallErrorRoutine;
						/* On Error GoTo CallErrorRoutine */
						fecherFoundation.Information.Err().Clear();
					}
					NextRecord:
					;
				}
				clsHistoryClass.Compare();
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Banks", MessageBoxButtons.OK, MessageBoxIcon.Information);
				LoadGrid();
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				return;
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Banks", MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					case curOnErrorGoToLabel_CallErrorRoutine:
						modErrorHandler.SetErrorHandler(ex);
						break;
				}
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmBanks_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// Call ForceFormToResize(Me)
			// verify that this form is not already open
			// If FormExist(Me) Then Exit Sub
		}

		private void frmBanks_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// if the insert key is pressed
			if (KeyCode == Keys.Insert)
			{
				cmdNew_Click();
			}
		}

		private void frmBanks_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBanks_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBanks properties;
			//frmBanks.ScaleWidth	= 9300;
			//frmBanks.ScaleHeight	= 7695;
			//frmBanks.LinkTopic	= "Form1";
			//frmBanks.LockControls	= -1  'True;
			//End Unmaped Properties
			int counter;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// vsElasticLight1.Enabled = True
			// set the size of the form
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			// open the forms global database connection
			rsBanks.DefaultDB = "TWPY0000.vb1";

			// set the grid column headers/widths/etc....
			SetGridProperties();
			// Load the grid with data
			LoadGrid();
			clsHistoryClass.SetGridIDColumn("PY", "vsBanks", 1);
			this.vsBanks.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsBanks.FrozenCols = 1;
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// get all of the records from the database
			vsBanks.Rows = 1;
			lngCurACHBank = -1;
			// can't match -1 to a bank
			rsBanks.OpenRecordset("Select * from tblBanks Order by RecordNumber", "TWPY0000.vb1");
			if (!rsBanks.EndOfFile())
			{
				// this will make the recordcount property work correctly
				rsBanks.MoveLast();
				rsBanks.MoveFirst();
				// set the number of rows in the grid
				vsBanks.Rows = rsBanks.RecordCount() + 1;
				// THIS IS THE FACT THAT
				// fill the grid
				for (intCounter = 1; intCounter <= (rsBanks.RecordCount()); intCounter++)
				{
					vsBanks.TextMatrix(intCounter, 0, FCConvert.ToString(Conversion.Val(rsBanks.Get_Fields_Int32("RecordNumber"))));
					// If intCounter <> Val(Trim(rsBanks.Fields("RecordNumber") & " ")) Then
					// rsBanks.Execute ("Update tblBanks Set RecordNumber = " & intCounter & " where ID = " & Trim(rsBanks.Fields("ID") & " "))
					// End If
					vsBanks.TextMatrix(intCounter, 1, Strings.Format(fecherFoundation.Strings.Trim(rsBanks.Get_Fields_Int32("RecordNumber") + " "), "00000"));
					vsBanks.TextMatrix(intCounter, 1, FCConvert.ToString(Conversion.Val(rsBanks.Get_Fields_Int32("RecordNumber"))));
					vsBanks.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsBanks.Get_Fields_String("Name") + " "));
					vsBanks.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsBanks.Get_Fields_String("Address1") + " "));
					vsBanks.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsBanks.Get_Fields("ID") + " "));
					vsBanks.TextMatrix(intCounter, 5, fecherFoundation.Strings.Trim(rsBanks.Get_Fields_String("Address2") + " "));
					vsBanks.TextMatrix(intCounter, 6, fecherFoundation.Strings.Trim(rsBanks.Get_Fields("Address3") + " "));
					vsBanks.TextMatrix(intCounter, 7, fecherFoundation.Strings.Trim(rsBanks.Get_Fields_String("BankDepositID") + " "));
					vsBanks.TextMatrix(intCounter, 8, FCConvert.ToString(rsBanks.Get_Fields_Boolean("eft") == true));
					vsBanks.TextMatrix(intCounter, 9, FCConvert.ToString(rsBanks.Get_Fields_Boolean("achbank") == true));
					if (FCConvert.CBool(vsBanks.TextMatrix(intCounter, 9)))
					{
						vsBanks.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					vsBanks.TextMatrix(intCounter, 10, FCConvert.ToString(rsBanks.Get_Fields("ShortDescription")));
					if (FCConvert.ToBoolean(rsBanks.Get_Fields_Boolean("achbank")))
						lngCurACHBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBanks.Get_Fields("ID"))));
					rsBanks.MoveNext();
				}
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
			}
			clsHistoryClass.Init = this;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			vsBanks.Cols = 11;
			GridWidth = vsBanks.WidthOriginal;
			vsBanks.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			vsBanks.ColWidth(2, FCConvert.ToInt32(0.3 * GridWidth));
			vsBanks.ColWidth(3, FCConvert.ToInt32(0.4 * GridWidth));
			vsBanks.ColWidth(5, FCConvert.ToInt32(0.4 * GridWidth));
			vsBanks.ColWidth(6, FCConvert.ToInt32(0.4 * GridWidth));
			vsBanks.ColWidth(7, FCConvert.ToInt32(0.2 * GridWidth));
			vsBanks.ColWidth(8, FCConvert.ToInt32(0.08 * GridWidth));
			vsBanks.ColWidth(9, FCConvert.ToInt32(0.11 * GridWidth));
			vsBanks.ColWidth(10, FCConvert.ToInt32(0.3 * GridWidth));
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// set grid properties
			vsBanks.Cols = 11;
			vsBanks.ColHidden(4, true);
			vsBanks.ColHidden(0, true);
			vsBanks.FixedCols = 1;
			// set column 0 properties
			vsBanks.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set column 1 properties
			vsBanks.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:AM:#2664 - don't set the data type
			//vsBanks.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
			vsBanks.TextMatrix(0, 1, "Bank #");
			vsBanks.ColSort(1, FCGrid.SortSettings.flexSortNumericAscending);
			// set column 2 properties
			vsBanks.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 2, "Name");
			vsBanks.ColDataType(2, FCGrid.DataTypeSettings.flexDTString);
			vsBanks.ColSort(2, FCGrid.SortSettings.flexSortStringAscending);
			// set column 3 properties
			vsBanks.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 3, "Address 1");
			// set column 5 properties
			vsBanks.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 5, "Address 2");
			// set column 6 properties
			vsBanks.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 6, "Address 3");
			// set column 7 properties
			vsBanks.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 7, "DFI Number");
			vsBanks.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set column 8 properties
			vsBanks.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.ColDataType(8, FCGrid.DataTypeSettings.flexDTBoolean);
			vsBanks.TextMatrix(0, 8, "EFT");
			// set column 9 properties
			vsBanks.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.ColDataType(9, FCGrid.DataTypeSettings.flexDTBoolean);
			vsBanks.TextMatrix(0, 9, "ACH Bank");
			// set column 10 properties
			vsBanks.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBanks.TextMatrix(0, 10, "Short Name");
			//vsBanks.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 10, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// vsBanks.ColEditMask(10) = "&&&&&&&&&&&&&"
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			vsBanks.Select(0, 0);
			SaveChanges();
		}

		private void frmBanks_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			// vsBanks.ColWidth(0) = vsBanks.Width * 0.05
			// vsBanks.ColWidth(1) = vsBanks.Width * 0.1
			// vsBanks.ColWidth(2) = vsBanks.Width * 0.4
			// vsBanks.ColWidth(3) = vsBanks.Width * 0.4
			// vsBanks.ColWidth(5) = vsBanks.Width * 0.4
			// vsBanks.ColWidth(6) = vsBanks.Width * 0.4
			// vsBanks.ColWidth(7) = vsBanks.Width * 0.4
			// vsBanks.ColWidth(8) = vsBanks.Width * 0
			// vsBanks.ColWidth(9) = vsBanks.Width * 0
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			if (frmDirectDeposit.InstancePtr.boolFromDirectDeposit == true)
			{
				frmDirectDeposit.InstancePtr.Show();
				frmDirectDeposit.InstancePtr.LoadCombo();
			}
			else
			{
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			frmDirectDeposit.InstancePtr.boolFromDirectDeposit = false;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			frmPrintBankSetup.InstancePtr.Show(App.MainForm);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsBanks_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsBanks_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// if the current row is already marked as needing a save then do not mark it again
			if (Strings.Right(" " + vsBanks.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsBanks.Row, 0), 1) != ".")
			{
				// increment the counter as to how many rows are dirty
				intDataChanged += 1;
				// Change the row number in the first column to indicate that this record has been edited
				vsBanks.TextMatrix(vsBanks.Row, 0, vsBanks.TextMatrix(vsBanks.Row, 0) + "..");
			}
			if (vsBanks.Col == 9)
			{
                // ach column
                //FC:FINAL:AKV - Convert numeric string to bool
                //if (FCConvert.ToBoolean(vsBanks.TextMatrix(vsBanks.Row, vsBanks.Col)) == true)
                if (FCConvert.CBool(vsBanks.TextMatrix(vsBanks.Row, vsBanks.Col)) == true)
                {
					// mark all others as false
					for (x = 1; x <= (vsBanks.Rows - 1); x++)
					{
						vsBanks.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 8, Color.White);
						if (x != vsBanks.Row)
						{
							vsBanks.TextMatrix(x, vsBanks.Col, FCConvert.ToString(false));
							if (Strings.Right(" " + vsBanks.TextMatrix(x, 0), 1) != ".")
							{
								intDataChanged += 1;
								vsBanks.TextMatrix(x, 0, vsBanks.TextMatrix(x, 0) + "..");
							}
						}
					}
					// x
					vsBanks.TextMatrix(vsBanks.Row, vsBanks.Col - 1, FCConvert.ToString(false));
					vsBanks.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsBanks.Row, vsBanks.Col - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					vsBanks.TextMatrix(vsBanks.Row, vsBanks.Col - 1, FCConvert.ToString(false));
					vsBanks.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsBanks.Row, vsBanks.Col - 1, Color.White);
				}
			}
			if (vsBanks.Col == 8)
			{
				// ach column
				if (FCConvert.ToInt32(vsBanks.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsBanks.Row, 8)) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsBanks.TextMatrix(vsBanks.Row, 8, FCConvert.ToString(false));
				}
			}
		}

		private void vsBanks_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsBanks.MouseRow == 0)
			{
                // If vsBanks.MouseCol = 2 Then
                // If boolASC Then
                // vsBanks.Sort = flexSortGenericDescending
                // Else
                // vsBanks.Sort = flexSortGenericAscending
                // End If
                // 
                // boolASC = Not boolASC
                // ElseIf vsBanks.MouseCol = 1 Then
                // vsbanks.ColSort(0) =
                // End If
                //FC:FINAL:AM:#i2236 - sort by the settings set in ColSort
                vsBanks.Sort = FCGrid.SortSettings.flexSortUseColSort;
                if (vsBanks.MouseCol == 1)
                {
                    vsBanks.ColSort(1, FCGrid.SortSettings.flexSortNumericAscending);
                    vsBanks.Select(1, 1);
                }
                else if (vsBanks.MouseCol == 2)
                {
                    vsBanks.ColSort(2, FCGrid.SortSettings.flexSortStringAscending);
                    vsBanks.Select(1, 2);
                }
			}
		}

		private void vsBanks_Enter(object sender, System.EventArgs e)
		{
			if (vsBanks.Rows > 1)
				vsBanks.Select(1, 1);
		}

		private void vsBanks_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// if the key is the TAB then make it work like the right arrow
			if (KeyCode == (Keys)9)
				KeyCode = (Keys)39;
			if (KeyCode == (Keys)13)
				KeyCode = (Keys)39;
			if (vsBanks.Col == 2)
			{
				if (KeyCode == (Keys)37)
				{
					// left key
					if (vsBanks.Row > 1)
					{
						vsBanks.Col = 8;
						vsBanks.LeftCol = 8;
						vsBanks.Row -= 1;
					}
					KeyCode = 0;
				}
			}
			if (vsBanks.Col == 8)
			{
				if (KeyCode == (Keys)39)
				{
					// right key
					if (vsBanks.Row < vsBanks.Rows - 1)
					{
						vsBanks.Col = 2;
						vsBanks.LeftCol = 2;
						vsBanks.Row += 1;
						KeyCode = 0;
					}
					else
					{
						cmdNew_Click();
						KeyCode = 0;
					}
				}
			}
		}

		private void vsBanks_KeyDownEdit(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsBanks_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// if this is the return key then make it function as a tab
			if (e.KeyCode == Keys.Return)
			{
				// KeyCode = 0
				// if this is the last column then begin in the 1st column of the next row
				// If vsBanks.Col = 3 Then
				// If vsBanks.Row < vsBanks.Rows - 1 Then
				// vsBanks.Row = vsBanks.Row + 1
				// vsBanks.Col = 2
				// Else
				// if there is no other row then create a new one
				// Call cmdNew_Click
				// End If
				// Else
				// move to the next column
				// SendKeys "{RIGHT}"
				// End If
			}
		}

		private void vsBanks_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsBanks[e.ColumnIndex, e.RowIndex];
            if (vsBanks.GetFlexRowIndex(e.RowIndex) == 10)
			{
				//ToolTip1.SetToolTip(vsBanks, "Shortened version of the bank name for checks and reports");
				cell.ToolTipText = "Shortened version of the bank name for checks and reports";
			}
			else
			{
				//ToolTip1.SetToolTip(vsBanks, "Press Insert Key To Add New Row");
				cell.ToolTipText = "Press Insert Key To Add New Row";
			}
		}

		private void vsBanks_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsBanks_RowColChange";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// If vsBanks.Col = 1 Then vsBanks.Col = 2
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsBanks.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (vsBanks.Cols - 1); intCols++)
				{
					if (intCols != 4)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                        clsControlInfo[intTotalNumberOfControls].ControlName = "vsBanks";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsBanks.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}
	}
}
