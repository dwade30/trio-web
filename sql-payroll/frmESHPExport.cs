//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmESHPExport : BaseForm
	{
		public frmESHPExport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmESHPExport InstancePtr
		{
			get
			{
				return (frmESHPExport)Sys.GetInstance(typeof(frmESHPExport));
			}
		}

		protected frmESHPExport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsDRWrapper rsLoad = new clsDRWrapper();
		bool boolLoading;

		private void cmbPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				FillcmbPayRun();
			}
		}

		private void frmESHPExport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmESHPExport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmESHPExport properties;
			//frmESHPExport.FillStyle	= 0;
			//frmESHPExport.ScaleWidth	= 5880;
			//frmESHPExport.ScaleHeight	= 4050;
			//frmESHPExport.LinkTopic	= "Form2";
			//frmESHPExport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			rsLoad.OpenRecordset("select distinct paydate,payrunid from tblcheckdetail WHERE checkvoid = 0 order by paydate desc, payrunid", "twpy0000.vb1");
			FillcmbPayDate();
		}

		private void FillcmbPayRun()
		{
			rsLoad.MoveFirst();
			cmbPayrun.Clear();
			string strPayDate = "";
			if (cmbPayDate.SelectedIndex >= 0)
			{
				strPayDate = cmbPayDate.Text;
				while (!rsLoad.EndOfFile())
				{
					if (strPayDate == Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy"))
					{
						cmbPayrun.AddItem(FCConvert.ToString(rsLoad.Get_Fields("Payrunid")));
					}
					rsLoad.MoveNext();
				}
			}
			if (cmbPayrun.Items.Count > 0)
			{
				cmbPayrun.SelectedIndex = 0;
			}
		}

		private void FillcmbPayDate()
		{
			rsLoad.MoveFirst();
			boolLoading = true;
			cmbPayDate.Clear();
			string strLastDate;
			strLastDate = "";
			while (!rsLoad.EndOfFile())
			{
				if (!(Strings.Format(rsLoad.Get_Fields("paydate"), "MM/dd/yyyy") == strLastDate))
				{
					strLastDate = Strings.Format(rsLoad.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy");
					cmbPayDate.AddItem(strLastDate);
					cmbPayDate.ItemData(cmbPayDate.ListCount - 1, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("payrunid"))));
				}
				else
				{
					cmbPayDate.ItemData(cmbPayDate.ListCount - 1, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("payrunid"))));
				}
				rsLoad.MoveNext();
			}
			if (cmbPayDate.Items.Count > 0)
			{
				cmbPayDate.SelectedIndex = 0;
				FillcmbPayRun();
			}
			boolLoading = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbPayDate.SelectedIndex >= 0 && cmbPayrun.SelectedIndex >= 0)
			{
				CreateExportFiles();
			}
			else
			{
				MessageBox.Show("You must select a valid pay date and pay run to continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void CreateExportFiles()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strESHPDir;
				// Dim strESHPDatDir As String
				// Dim strDatExportDir As String
				string strESHPBaseDir;
				strESHPBaseDir = Environment.CurrentDirectory + "\\" + "ESHPExport\\";
				strESHPDir = strESHPBaseDir + "Checks\\";
				// strESHPDatDir = CurDir & "\" & "ESHPExport\DatFiles\"
				string strExportDir;
				strExportDir = strESHPDir + Strings.Format(cmbPayDate.Text, "MMddyyyy") + "_" + FCConvert.ToString(Conversion.Val(cmbPayrun.Text));
				// strDatExportDir = strESHPDatDir & Format(cmbPayDate.Text, "MMddyyyy") & "_" & Val(cmbPayrun.Text)
				FCFileSystem fs = new FCFileSystem();
				DirectoryInfo fld;
				if (!Directory.Exists(strESHPBaseDir))
				{
                    Directory.CreateDirectory(strESHPBaseDir);
				}
				if (!Directory.Exists(strESHPDir))
				{
                    Directory.CreateDirectory(strESHPDir);
				}
				// If Not fs.FolderExists(strESHPDatDir) Then
				// Call fs.CreateFolder(strESHPDatDir)
				// End If
				// If Not fs.FolderExists(strDatExportDir) Then
				// Call fs.CreateFolder(strDatExportDir)
				// End If
				if (Directory.Exists(strExportDir))
				{
					Directory.Delete(strExportDir, true);
				}
                Directory.CreateDirectory(strExportDir);
				strExportDir += "\\";
				rptESHPExport.InstancePtr.Init(Convert.ToDateTime(cmbPayDate.Text), FCConvert.ToInt16(Conversion.Val(cmbPayrun.Text)), strExportDir);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateExportFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
