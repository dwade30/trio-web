//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;

namespace TWPY0000
{
	public partial class frmCheckRecipients : BaseForm
	{
		public frmCheckRecipients()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCheckRecipients InstancePtr
		{
			get
			{
				return (frmCheckRecipients)Sys.GetInstance(typeof(frmCheckRecipients));
			}
		}

		protected frmCheckRecipients _InstancePtr = null;
		private clsDRWrapper rsRecipients = new clsDRWrapper();
		private clsHistory clsHistoryClass = new clsHistory();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		// used for grid recipients
		const int RECPT = 0;
		const int FREQ = 1;
		const int NAME1 = 2;
		const int Address1 = 3;
		const int HIDDEN = 4;
		const int ID = 5;
		const int Address2 = 6;
		const int Address3 = 7;
		const int EFT = 8;
		const int CNSTCOLACH = 9;
		const int CNSTCOLEFTSeparate = 10;
		const int CNSTCOLEFTBank = 11;
		const int CNSTCOLEFTAccount = 12;
		const int CNSTCOLEFTAccountType = 13;
		const int CNSTCOLEFTPrenote = 14;
		const int CNSTCOLEFTFileName = 15;
		const int CNSTCOLEFTFileExt = 16;
		private bool boolSaveSuccessful;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
        private BankAccountViewType bankAccountViewPermission = BankAccountViewType.None;


        private void frmCheckRecipients_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// Call ForceFormToResize(Me)
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
				return;
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			// set grid properties
			vsRecipients.Cols = 17;
			vsRecipients.FixedCols = 1;
			vsRecipients.ColHidden(HIDDEN, true);
			vsRecipients.ColHidden(ID, true);
			// vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsRecipients.Cols - 1) = True
			// set column 0 properties
			vsRecipients.ColAlignment(RECPT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(RECPT, 900);
			vsRecipients.TextMatrix(RECPT, RECPT, "#");
			// set column 1 properties
			vsRecipients.ColAlignment(FREQ, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(FREQ, 1000);
			vsRecipients.TextMatrix(0, FREQ, "Frequency");
			vsRecipients.ColComboList(FREQ, "#0;Weekly|#1;Monthly|#2;Quarterly");
            if (bankAccountViewPermission != BankAccountViewType.Full)
            {
                vsRecipients.Columns[CNSTCOLEFTAccount - 1].ReadOnly = true;
            }

            // set column 2 properties
            vsRecipients.ColAlignment(NAME1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(NAME1, 2200);
			vsRecipients.TextMatrix(0, NAME1, "Name");
			// set column 3 properties
			vsRecipients.ColAlignment(Address1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(Address1, 1400);
			vsRecipients.TextMatrix(0, Address1, "Address Line 1");
			// set column 4 properties
			vsRecipients.ColAlignment(HIDDEN, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(HIDDEN, 300);
			vsRecipients.ColAlignment(Address2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(Address2, 1400);
			vsRecipients.TextMatrix(0, Address2, "Address Line 2");
			vsRecipients.ColAlignment(Address3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(Address3, 1400);
			vsRecipients.TextMatrix(0, Address3, "Address Line 3");
			vsRecipients.ColAlignment(EFT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.ColWidth(EFT, 800);
			vsRecipients.TextMatrix(0, EFT, "EFT");
			vsRecipients.ColDataType(EFT, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRecipients.ColAlignment(CNSTCOLACH, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// vsRecipients.ColWidth(CNSTCOLACH) = 800
			vsRecipients.TextMatrix(0, CNSTCOLACH, "ACH");
			vsRecipients.ColDataType(CNSTCOLACH, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRecipients.ColAlignment(CNSTCOLEFTAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRecipients.TextMatrix(0, CNSTCOLEFTAccount, "Account");
			vsRecipients.TextMatrix(0, CNSTCOLEFTAccountType, "Type");
			vsRecipients.TextMatrix(0, CNSTCOLEFTBank, "Bank");
			vsRecipients.TextMatrix(0, CNSTCOLEFTFileName, "File Prefix");
			vsRecipients.TextMatrix(0, CNSTCOLEFTPrenote, "Prenote");
			vsRecipients.TextMatrix(0, CNSTCOLEFTSeparate, "Sep. File");
			vsRecipients.TextMatrix(0, CNSTCOLEFTFileExt, "File Extension");
			vsRecipients.ColComboList(CNSTCOLEFTBank, string.Empty);
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			rsTemp.OpenRecordset("Select * from tblBanks order by Name", "TWPY0000.vb1");
			strTemp = "";
			if (!rsTemp.EndOfFile())
			{
				rsTemp.MoveLast();
				rsTemp.MoveFirst();
				string tempname = "";
				string tempaddress = "";
				string tempcity = "";
				// makes this column work like a combo box.
				// this fills the box with the values from the tblBanks table
				// This forces this column to work as a combo box
				// frmWait.Init "Loading Direct Deposit Information", True, rsTemp.RecordCount, True
				for (intCounter = 1; intCounter <= (rsTemp.RecordCount()); intCounter++)
				{
					//App.DoEvents();
					tempname = FCConvert.ToString(rsTemp.Get_Fields_String("Name"));
					tempaddress = FCConvert.ToString(rsTemp.Get_Fields_String("Address1"));
					// tempcity = rsTemp.Fields("City")
					// make data look nice in combo box
					// looks like there are columns in the combo box
					if (tempname.Length < 25)
					{
						tempname = FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(tempname, 25, false));
					}
					if (tempaddress.Length < 20)
					{
						tempaddress = FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(tempaddress, FCConvert.ToInt16(false)));
					}
					if (intCounter == 1)
					{
						strTemp += "#" + rsTemp.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsTemp.Get_Fields_Int32("RecordNumber"), "00000") : "");
					}
					else
					{
						strTemp += "|#" + rsTemp.Get_Fields("ID") + ";" + tempname + "\t" + tempaddress + "\t" + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_Int32("RecordNumber"))) != "" ? Strings.Format(rsTemp.Get_Fields_Int32("RecordNumber"), "00000") : "");
					}
					rsTemp.MoveNext();
					// frmWait.IncrementProgress
				}
				vsRecipients.ColComboList(CNSTCOLEFTBank, strTemp);
			}
			vsRecipients.ColComboList(CNSTCOLEFTAccountType, "#" + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPECHECKING) + ";Checking|#" + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPESAVINGS) + ";Savings");
			vsRecipients.ColDataType(CNSTCOLEFTPrenote, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRecipients.ColDataType(CNSTCOLEFTSeparate, FCGrid.DataTypeSettings.flexDTBoolean);
			// CEN
			modGlobalRoutines.CenterGridCaptions(ref vsRecipients);
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
			}
			vsRecipients.Rows = 1;
			rsRecipients.OpenRecordset("SELECT * FROM tblRecipients ORDER BY RecptNumber", "TWPY0000.vb1");
			if (!rsRecipients.EndOfFile())
			{
				rsRecipients.MoveLast();
				rsRecipients.MoveFirst();
				for (intCounter = 1; intCounter <= (rsRecipients.RecordCount()); intCounter++)
				{
					//App.DoEvents();
					vsRecipients.AddItem("", vsRecipients.Rows);
					if (intCounter != FCConvert.ToDouble(fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_Int32("RecptNumber") + " ")))
					{
						rsRecipients.Execute("Update tblRecipients Set RecptNumber = " + FCConvert.ToString(intCounter) + " where ID = " + fecherFoundation.Strings.Trim(rsRecipients.Get_Fields("ID") + " "), "Payroll");
						vsRecipients.TextMatrix(intCounter, RECPT, FCConvert.ToString(intCounter));
					}
					else
					{
						vsRecipients.TextMatrix(intCounter, RECPT, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_Int32("RecptNumber") + " "));
					}
					vsRecipients.TextMatrix(intCounter, FREQ, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields("Freq") + " "));
					vsRecipients.TextMatrix(intCounter, NAME1, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_String("Name") + " "));
					vsRecipients.TextMatrix(intCounter, Address1, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_String("Address1") + " "));
					vsRecipients.TextMatrix(intCounter, Address2, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_String("Address2") + " "));
					vsRecipients.TextMatrix(intCounter, Address3, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields("Address3") + " "));
					vsRecipients.TextMatrix(intCounter, ID, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields("ID") + " "));
					vsRecipients.TextMatrix(intCounter, EFT, fecherFoundation.Strings.Trim(rsRecipients.Get_Fields_Boolean("EFT") + " "));
					vsRecipients.TextMatrix(intCounter, CNSTCOLACH, FCConvert.ToString(rsRecipients.Get_Fields_Boolean("ACH")));
                    var bankAccount = "";
                    if (!string.IsNullOrWhiteSpace(rsRecipients.Get_Fields_String("EFTAccount")))
                    {
                        switch (bankAccountViewPermission)
                        {
                            case BankAccountViewType.Full:
                                bankAccount = rsRecipients.Get_Fields_String("EFTAccount");
                                break;
                            case BankAccountViewType.Masked:
                                bankAccount = "******" + rsRecipients.Get_Fields_String("EFTAccount").Right(4);                               
                                break;
                            default:
                                bankAccount = "**********";
                                break;
                        }
                    }

                    vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccount, bankAccount);
					if (Conversion.Val(rsRecipients.Get_Fields_Int32("EFTAccountType")) > 0)
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccountType, FCConvert.ToString(rsRecipients.Get_Fields_Int32("EFTAccountType")));
					}
					else
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccountType, FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPECHECKING));
					}
					if (Conversion.Val(rsRecipients.Get_Fields_Int32("EFTBank")) > 0)
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTBank, FCConvert.ToString(rsRecipients.Get_Fields_Int32("EFTBank")));
					}
					vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileName, FCConvert.ToString(rsRecipients.Get_Fields_String("EFTFilePrefix")));
					if (FCConvert.CBool(rsRecipients.Get_Fields_Boolean("EftPrenote")))
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTPrenote, FCConvert.ToString(true));
					}
					else
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTPrenote, FCConvert.ToString(false));
					}
					if (FCConvert.CBool(rsRecipients.Get_Fields_Boolean("EftSeparateFile")))
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTSeparate, FCConvert.ToString(true));
					}
					else
					{
						vsRecipients.TextMatrix(intCounter, CNSTCOLEFTSeparate, FCConvert.ToString(false));
					}
					vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileExt, FCConvert.ToString(rsRecipients.Get_Fields_String("EFTFileExtension")));
					UpdateEFTCells(intCounter);
					rsRecipients.MoveNext();
				}
			}
			// If vsRecipients.Rows > 1 Then
			// vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpFont, 1, 0, vsRecipients.Rows - 1, vsRecipients.Cols - 1) = vsRecipients.Font
			// End If
			//vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRecipients.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			clsHistoryClass.Init = this;
		}

		private void frmCheckRecipients_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCheckRecipients_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
                switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                    .ViewBankAccountNumbers.ToInteger()))
                {
                    case "F":
                        bankAccountViewPermission = BankAccountViewType.Full;
                        break;
                    case "P":
                        bankAccountViewPermission = BankAccountViewType.Masked;
                        break;
                    default:
                        bankAccountViewPermission = BankAccountViewType.None;
                        break;
                }
            }
			catch (Exception ex)
			{
				modErrorHandler.SetErrorHandler(ex);
			}
			int counter;
			modGlobalFunctions.SetFixedSize(this, 0);
			rsRecipients.DefaultDB = "TWPY0000.vb1";
			// set grid width/captions etc...
			SetGridProperties();
			// fill grid
			LoadGrid();
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
			clsHistoryClass.SetGridIDColumn("PY", "vsRecipients", ID);
			intDataChanged = 0;
		}

		private void cmdExit_Click()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
					else
					{
						intDataChanged = 0;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmCheckRecipients_Resize(object sender, System.EventArgs e)
		{
			if (vsRecipients.Cols < 17)
			{
				vsRecipients.Cols = 17;
			}
			vsRecipients.FixedCols = 1;
			vsRecipients.ColHidden(HIDDEN, true);
			vsRecipients.ColHidden(ID, true);
			vsRecipients.ColWidth(RECPT, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.07));
			vsRecipients.ColWidth(FREQ, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.1));
			vsRecipients.ColWidth(NAME1, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.22));
			vsRecipients.ColWidth(HIDDEN, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.03));
			vsRecipients.ColWidth(Address1, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.17));
			vsRecipients.ColWidth(Address2, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.17));
			vsRecipients.ColWidth(Address3, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.17));
			vsRecipients.ColWidth(EFT, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.08));
			vsRecipients.ColWidth(CNSTCOLACH, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.08));
			vsRecipients.ColWidth(CNSTCOLEFTPrenote, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.08));
			vsRecipients.ColWidth(CNSTCOLEFTSeparate, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.08));
			vsRecipients.ColWidth(CNSTCOLEFTAccount, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.1));
			vsRecipients.ColWidth(CNSTCOLEFTAccountType, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.1));
			vsRecipients.ColWidth(CNSTCOLEFTBank, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.2));
			vsRecipients.ColWidth(CNSTCOLEFTFileExt, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.1));
			vsRecipients.ColWidth(CNSTCOLEFTFileName, FCConvert.ToInt32(vsRecipients.WidthOriginal * 0.12));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			this.vsRecipients.Select(0, 0);
			SaveChanges();
			if (intDataChanged == (DialogResult)2)
			{
				e.Cancel = true;
				return;
			}
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuDeleteRecipient_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuDeleteRecipient_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				// if there is no active current row then we cannot delete anything
				if (vsRecipients.Row < 0)
					return;
				if (vsRecipients.Col < 0)
					return;
				// vbPorter upgrade warning: TempRow As int	OnWrite(string)
				int TempRow = 0;
				int intCounter2;
				TempRow = FCConvert.ToInt32(vsRecipients.TextMatrix(vsRecipients.Row, RECPT));
				if (MessageBox.Show("This action will delete recipient #" + FCConvert.ToString(TempRow) + ". Continue?", "Check Recipients", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsData.OpenRecordset("Select * from tblCategories where Include = " + FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(vsRecipients.Row, ID))), "TWPY0000.vb1");
					if (rsData.EndOfFile() || Conversion.Val(vsRecipients.TextMatrix(vsRecipients.Row, ID)) == 0)
					{
					}
					else
					{
						// we can 't delete recipient if its being used by a category
						MessageBox.Show("Recipient code is being used by a recipient category and cannot be deleted", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
					int intColNumber = 0;
					intColNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRecipients.TextMatrix(vsRecipients.Row, 0))));
					// if new row and not saved then just delete from grid and don't worry about database
					if (Strings.Right(FCConvert.ToString(vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsRecipients.Row, HIDDEN)), 2) == "..")
					{
						vsRecipients.RemoveItem(vsRecipients.Row);
						for (intCounter = 1; intCounter <= (vsRecipients.Rows - 1); intCounter++)
						{
							if (Conversion.Val(vsRecipients.TextMatrix(intCounter, 0)) > intColNumber)
							{
								vsRecipients.TextMatrix(intCounter, 0, FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(intCounter, 0)) - 1));
							}
						}
						return;
					}
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsRecipients.Row) + "from Check Recipients");
					// ------------------------------------------------------------------
					// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
					clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsRecipients);
					modGlobalFunctions.AddCYAEntry_8("PY", "Deleted Check Recipient " + FCConvert.ToString(TempRow), vsRecipients.TextMatrix(vsRecipients.Row, NAME1));
					// need to clean up grid and database
					vsRecipients.RemoveItem(vsRecipients.Row);
					// after we delete we have to adjust row numbers
					for (intCounter = 1; intCounter <= (vsRecipients.Rows - 1); intCounter++)
					{
						if (Conversion.Val(vsRecipients.TextMatrix(intCounter, 0)) > intColNumber)
						{
							vsRecipients.TextMatrix(intCounter, 0, FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(intCounter, 0)) - 1));
						}
					}
					// SetGridHeight
					rsRecipients.Execute("Delete from tblRecipients where RecptNumber = " + FCConvert.ToString(TempRow), "Payroll");
					rsRecipients.OpenRecordset("Select * From tblRecipients ORDER BY RecptNumber", "TWPY0000.vb1");
					if (!rsRecipients.EndOfFile())
					{
						rsRecipients.MoveLast();
						rsRecipients.MoveFirst();
						// intRecordNumber = rsRecipients.Fields("RecptNumber")
						for (intCounter = 1; intCounter <= (rsRecipients.RecordCount()); intCounter++)
						{
							if (rsRecipients.Get_Fields_Int32("RecptNumber") > TempRow)
							{
								rsRecipients.Edit();
								rsRecipients.SetData("RecptNumber", rsRecipients.Get_Fields_Int32("RecptNumber") - 1);
								rsRecipients.Update();
							}
							rsRecipients.MoveNext();
						}
					}
					// load the grid with the new data to show the 'clean out' of the current row
					LoadGrid();
					// decrement the counter as to how many records are dirty
					intDataChanged -= 1;
					MessageBox.Show("Record Deleted successfully.", "Check Recipient", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuNew";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsRecipients.AddItem("", vsRecipients.Rows);
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, RECPT, FCConvert.ToString(vsRecipients.Rows - 1));
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, FREQ, "Weekly");
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, HIDDEN, "..");
				vsRecipients.Select(vsRecipients.Rows - 1, FREQ);
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, CNSTCOLEFTAccountType, FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTTYPECHECKING));
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, CNSTCOLEFTPrenote, FCConvert.ToString(false));
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, CNSTCOLEFTSeparate, FCConvert.ToString(false));
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, EFT, FCConvert.ToString(false));
				vsRecipients.TextMatrix(vsRecipients.Rows - 1, CNSTCOLACH, FCConvert.ToString(false));
				intDataChanged += 1;
				clsReportChanges.AddChange("Added Row to Check Recipients");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private int convertit(bool boolValue)
		{
			int convertit = 0;
			if (boolValue)
			{
				convertit = 1;
			}
			else
			{
				convertit = 0;
			}
			return convertit;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "mnuSave";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				boolSaveSuccessful = false;
				vsRecipients.Select(0, 0);
				if (NotValidData() == true)
					return;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Check Recipients", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				rsRecipients.OpenRecordset("SELECT * FROM tblRecipients ORDER BY RecptNumber", "TWPY0000.vb1");
				if (!rsRecipients.EndOfFile())
				{
					rsRecipients.MoveLast();
					rsRecipients.MoveFirst();
					for (intCounter = 1; intCounter <= (vsRecipients.Rows - 1); intCounter++)
					{
						if (vsRecipients.TextMatrix(intCounter, NAME1) == string.Empty && vsRecipients.TextMatrix(intCounter, Address1) == string.Empty && vsRecipients.TextMatrix(intCounter, Address2) == string.Empty && vsRecipients.TextMatrix(intCounter, Address3) == string.Empty)
						{
						}
						else
						{
							if (FCConvert.CBool(vsRecipients.TextMatrix(intCounter, EFT)) == true)
							{
								if (FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLACH)) == true)
								{
									if (Conversion.Val(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTBank)) <= 0)
									{
										MessageBox.Show("You have chosen to send funds by ACH to " + vsRecipients.TextMatrix(intCounter, NAME1) + " but have not chosen a bank.", "No Bank Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
									if (fecherFoundation.Strings.Trim(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccount)) == string.Empty)
									{
										MessageBox.Show("You have chosen to send funds by ACH to " + vsRecipients.TextMatrix(intCounter, NAME1) + " but have not specified an account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
									if (FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTSeparate)) == true)
									{
										if (fecherFoundation.Strings.Trim(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileName)) == string.Empty)
										{
											MessageBox.Show("You have chosen to send funds by separate ACH file to " + vsRecipients.TextMatrix(intCounter, NAME1) + " but have not specified a file prefix.", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											return;
										}
									}
								}
							}
							if (Strings.Right(FCConvert.ToString(vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, HIDDEN)), 1) == ".")
							{
								if (fecherFoundation.Strings.Trim(vsRecipients.TextMatrix(intCounter, NAME1)) == string.Empty)
								{
								}
								else
								{
									rsRecipients.FindFirstRecord("RecptNumber", vsRecipients.TextMatrix(intCounter, RECPT));
									if (rsRecipients.NoMatch)
									{
										// if user added a new record to grid then add a record to database also
										rsRecipients.AddNew();
									}
									else
									{
										// just saving data that was there on load
										rsRecipients.Edit();
									}
									rsRecipients.SetData("RecptNumber", vsRecipients.TextMatrix(intCounter, RECPT));
									rsRecipients.SetData("Freq", vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, FREQ));
									rsRecipients.SetData("Name", vsRecipients.TextMatrix(intCounter, NAME1));
									rsRecipients.SetData("Address1", vsRecipients.TextMatrix(intCounter, Address1));
									rsRecipients.SetData("Address2", vsRecipients.TextMatrix(intCounter, Address2));
									rsRecipients.SetData("Address3", vsRecipients.TextMatrix(intCounter, Address3));
									rsRecipients.SetData("EFT", FCConvert.CBool((vsRecipients.TextMatrix(intCounter, EFT) == string.Empty ? "0" : vsRecipients.TextMatrix(intCounter, EFT))));
									if (vsRecipients.TextMatrix(intCounter, CNSTCOLACH) == string.Empty)
									{
										rsRecipients.Set_Fields("ACH", false);
									}
									else
									{
										rsRecipients.Set_Fields("ACH", FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLACH)));
									}
									// rsRecipients.Fields("ACH") = vsRecipients.TextMatrix(intCounter, CNSTCOLACH)
									rsRecipients.Set_Fields("eftbANK", FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTBank))));
                                    if (bankAccountViewPermission == BankAccountViewType.Full)
                                    {
                                        rsRecipients.Set_Fields("EFTAccount",
                                            vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccount));
                                    }
                                    rsRecipients.Set_Fields("EFTAccountType", FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccountType))));
									rsRecipients.Set_Fields("EftPrenote", FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTPrenote)));
									rsRecipients.Set_Fields("EftFilePrefix", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileName));
									rsRecipients.Set_Fields("EftSeparateFile", FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTSeparate)));
									rsRecipients.Set_Fields("EFTFileExtension", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileExt));
									vsRecipients.TextMatrix(intCounter, HIDDEN, "");
									rsRecipients.Update();
								}
							}
						}
					}
				}
				else
				{
					for (intCounter = 1; intCounter <= (vsRecipients.Rows - 1); intCounter++)
					{
						if (Strings.Right(FCConvert.ToString(vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, HIDDEN)), 1) == ".")
						{
							// if user added a new record to grid then add a record to database also
							rsRecipients.AddNew();
						}
						else
						{
							// just saving data that was there on load
							rsRecipients.Edit();
						}
						rsRecipients.SetData("RecptNumber", vsRecipients.TextMatrix(intCounter, RECPT));
						rsRecipients.SetData("Freq", vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, FREQ));
						rsRecipients.SetData("Name", vsRecipients.TextMatrix(intCounter, NAME1) + " ");
						rsRecipients.SetData("Address1", vsRecipients.TextMatrix(intCounter, Address1));
						rsRecipients.SetData("Address2", vsRecipients.TextMatrix(intCounter, Address2));
						rsRecipients.SetData("Address3", vsRecipients.TextMatrix(intCounter, Address3));
						rsRecipients.SetData("EFT", FCConvert.CBool(fecherFoundation.Strings.Trim(vsRecipients.TextMatrix(intCounter, EFT)) == string.Empty ? "0" : vsRecipients.TextMatrix(intCounter, EFT)));
						rsRecipients.Set_Fields("eftbANK", FCConvert.ToString(Conversion.Val(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTBank))));
						rsRecipients.Set_Fields("EFTAccount", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccount));
						rsRecipients.Set_Fields("EFTAccountType", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTAccountType));
						rsRecipients.Set_Fields("EftPrenote", FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTPrenote)));
						rsRecipients.Set_Fields("EftFilePrefix", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileName));
						rsRecipients.Set_Fields("EftSeparateFile", FCConvert.CBool(vsRecipients.TextMatrix(intCounter, CNSTCOLEFTSeparate)));
						rsRecipients.Set_Fields("EFTFileExtension", vsRecipients.TextMatrix(intCounter, CNSTCOLEFTFileExt));
						vsRecipients.TextMatrix(intCounter, HIDDEN, "");
						rsRecipients.Update();
					}
				}
				intDataChanged = 0;
				boolSaveSuccessful = true;
				//clsHistoryClass.Compare();
				MessageBox.Show("Save was successful", "Check Recipients", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// Call SetGridProperties
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			boolSaveSuccessful = false;
			mnuSave_Click();
			if (boolSaveSuccessful)
			{
				mnuExit_Click();
			}
		}
		// vbPorter upgrade warning: Row As int	OnRead
		// vbPorter upgrade warning: Col As int	OnRead
		private void vsRecipients_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Strings.Right(FCConvert.ToString(vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsRecipients.Row, HIDDEN)), 1) != ".")
			{
				// increment the counter as to how many rows are dirty
				intDataChanged += 1;
				// Change column to indicate that this record has been edited
				vsRecipients.TextMatrix(vsRecipients.Row, HIDDEN, vsRecipients.TextMatrix(vsRecipients.Row, HIDDEN) + ".");
			}
			intDataChanged += 1;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			intRow = vsRecipients.Row;
			intCol = vsRecipients.Col;
			if (intRow > 0)
			{
				switch (intCol)
				{
					case EFT:
						{
							UpdateEFTCells(intRow);
							break;
						}
					case CNSTCOLACH:
						{
							UpdateEFTCells(intRow);
							break;
						}
					case CNSTCOLEFTSeparate:
						{
							UpdateEFTCells(intRow);
							break;
						}
				}
				//end switch
			}
		}

		private void UpdateEFTCells(int intRow)
		{
			if (!FCConvert.CBool(vsRecipients.TextMatrix(intRow, EFT)))
			{
				vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLACH, intRow, CNSTCOLEFTFileExt, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			else
			{
				vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLACH, Color.White);
				if (!FCConvert.CBool(vsRecipients.TextMatrix(intRow, CNSTCOLACH)))
				{
					vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLACH + 1, intRow, CNSTCOLEFTFileExt, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				else
				{
					vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLACH + 1, intRow, CNSTCOLEFTFileExt, Color.White);
					if (!FCConvert.CBool(vsRecipients.TextMatrix(intRow, CNSTCOLEFTSeparate)))
					{
						vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLEFTFileExt, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLEFTFileName, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					else
					{
						vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLEFTFileExt, Color.White);
						vsRecipients.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intRow, CNSTCOLEFTFileName, Color.White);
					}
				}
			}
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			for (intCounter = 1; intCounter <= (vsRecipients.Rows - 1); intCounter++)
			{
				// If vsRecipients.TextMatrix(intCounter, DESC) = vbNullString Then
				// MsgBox "Please Enter Description", , "Error"
				// vsRecipients.Select vsRecipients.Row, vsRecipients.Col
				// NotValidData = True
				// Exit Function
				// End If
				if (vsRecipients.TextMatrix(intCounter, FREQ) == string.Empty)
				{
					MessageBox.Show("Please Make Selection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsRecipients.Select(vsRecipients.Row, vsRecipients.Col);
					NotValidData = true;
					return NotValidData;
				}
			}
			NotValidData = false;
			return NotValidData;
		}

		private void vsRecipients_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			intRow = vsRecipients.Row;
			intCol = vsRecipients.Col;
			if (intRow > 0)
			{
				if (intCol > EFT)
				{
					if (FCConvert.CBool(vsRecipients.TextMatrix(intRow, EFT)) == true)
					{
						if (intCol == CNSTCOLACH)
						{
							// vsRecipients.Editable = True
						}
						else
						{
							if (FCConvert.CBool(vsRecipients.TextMatrix(intRow, CNSTCOLACH)))
							{
								// vsRecipients.Editable = True
								if (FCConvert.CBool(vsRecipients.TextMatrix(intRow, CNSTCOLEFTSeparate)) == true)
								{
								}
								else
								{
									if (intCol == CNSTCOLEFTFileExt || intCol == CNSTCOLEFTFileName)
									{
										e.Cancel = true;
									}
								}
							}
							else
							{
								// vsRecipients.Editable = False
								e.Cancel = true;
							}
						}
					}
					else
					{
						// vsRecipients.Editable = False
						e.Cancel = true;
					}
				}
				else
				{
					// vsRecipients.Editable = True
				}
			}
		}

		private void vsRecipients_DblClick(object sender, System.EventArgs e)
		{
			vsRecipients.EditCell();
		}

		private void vsRecipients_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsRecipients_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if this is the return key then make it function as a tab
				if (KeyCode == Keys.Return || KeyCode == Keys.Tab)
				{
					KeyCode = 0;
					// if this is the last column then begin in the 1st column of the next row
					if (vsRecipients.Col == 8)
					{
						if (vsRecipients.Row < vsRecipients.Rows - 1)
						{
							vsRecipients.Row += 1;
							vsRecipients.Col = 1;
						}
						else
						{
							// if there is no other row then create a new one
							mnuNew_Click();
						}
					}
					else
					{
						// move to the next column
						Support.SendKeys("{RIGHT}", false);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			// vbPorter upgrade warning: intCols As int	OnWriteFCConvert.ToInt32(
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsRecipients.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= (vsRecipients.Cols - 1); intCols++)
				{
					if (intCols != 4 && intCols != 12)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                        clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
						clsControlInfo[intTotalNumberOfControls].ControlName = "vsRecipients";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsRecipients.TextMatrix(0, intCols);
						intTotalNumberOfControls += 1;
					}
				}
			}
		}

		private void vsRecipients_RowColChange(object sender, System.EventArgs e)
		{
			
		}
	}
}
