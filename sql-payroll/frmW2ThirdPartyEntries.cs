//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmW2ThirdPartyEntries : BaseForm
	{
		public frmW2ThirdPartyEntries()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmW2ThirdPartyEntries InstancePtr
		{
			get
			{
				return (frmW2ThirdPartyEntries)Sys.GetInstance(typeof(frmW2ThirdPartyEntries));
			}
		}

		protected frmW2ThirdPartyEntries _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:          NOVEMBER 4TH, 2004
		//
		// NOTES:
		//
		//
		//
		// **************************************************
		// private local variables
		// Private dbLengths       As DAO.Database
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsEmployee = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		private bool boolLoading;
		private bool pboolDataChanged;
		const int ID = 0;
		const int EmployeeNumber = 1;
		const int EmployeeName = 2;
		const int TYPE1 = 3;
		const int TYPE2 = 4;
		const int TYPE3 = 5;
		const int AMOUNT1 = 3;
		const int AMOUNT2 = 4;
		const int AMOUNT3 = 5;

		public bool SaveChanges()
		{
			bool SaveChanges = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				int intAmountType = 0;
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsExecute = new clsDRWrapper();
				int intTry;
				vsData.Select(0, 0);
				SaveChanges = false;
				rsData.OpenRecordset("Select * from tblW2AdditionalInfoData");
				for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
				{
					for (intTry = 1; intTry <= 3; intTry++)
					{
						intAmountType = 0;
						switch (intTry)
						{
							case 1:
								{
									// SET THE AMOUNT FOR TH
									// If Val(vsData.TextMatrix(intCounter, AMOUNT1)) <> 0 Then
									if (cboType1.SelectedIndex != 0 && cboType1.Text == string.Empty)
									{
										MessageBox.Show("Code for Type 1 has not been selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										vsData.Select(intCounter, AMOUNT1);
										return SaveChanges;
									}
									intAmountType = AMOUNT1;
									goto SaveType;
									// Else
									// intAmountType = 0
									// GoTo NextTry
									// GoTo SaveType
									// End If
									break;
								}
							case 2:
								{
									// If Val(vsData.TextMatrix(intCounter, AMOUNT2)) <> 0 Then
									if (cboType2.SelectedIndex != 0 && cboType2.Text == string.Empty)
									{
										MessageBox.Show("Code for Type 2 has not been selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										vsData.Select(intCounter, AMOUNT2);
										return SaveChanges;
									}
									intAmountType = AMOUNT2;
									goto SaveType;
									// Else
									// GoTo NextTry
									// End If
									break;
								}
							case 3:
								{
									// If Val(vsData.TextMatrix(intCounter, AMOUNT3)) <> 0 Then
									if (cboType3.SelectedIndex != 0 && cboType3.Text == string.Empty)
									{
										MessageBox.Show("Code for Type 3 has not been selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										vsData.Select(intCounter, AMOUNT3);
										return SaveChanges;
									}
									intAmountType = AMOUNT3;
									goto SaveType;
									// Else
									// GoTo NextTry
									// End If
									break;
								}
						}
						//end switch
						SaveType:
						;
						// we need to save this data
						int lngDeductionID = 0;
						switch (intAmountType)
						{
							case 0:
								{
									lngDeductionID = 0;
									break;
								}
							case AMOUNT1:
								{
									lngDeductionID = cboType1.ItemData(cboType1.SelectedIndex);
									break;
								}
							case AMOUNT2:
								{
									lngDeductionID = cboType2.ItemData(cboType2.SelectedIndex);
									break;
								}
							case AMOUNT3:
								{
									lngDeductionID = cboType3.ItemData(cboType3.SelectedIndex);
									break;
								}
						}
						//end switch
						if (lngDeductionID == 0)
						{
						}
						else
						{
							if (Conversion.Val(vsData.TextMatrix(intCounter, intAmountType)) == 0)
							{
								rsExecute.Execute("Delete from tblW2AdditionalInfoData Where EmployeeNumber='" + vsData.TextMatrix(intCounter, EmployeeNumber) + "' AND AdditionalDeductionID = " + FCConvert.ToString(lngDeductionID + 9000), "Payroll");
							}
							else
							{
								if (rsData.FindFirstRecord2("EmployeeNumber,AdditionalDeductionID", vsData.TextMatrix(intCounter, EmployeeNumber) + "," + FCConvert.ToString(lngDeductionID + 9000), ","))
								{
									rsData.Edit();
									rsData.Set_Fields("Amount", vsData.TextMatrix(intCounter, intAmountType));
									rsData.Update();
								}
								else
								{
									rsData.AddNew();
									rsData.Set_Fields("EmployeeNumber", vsData.TextMatrix(intCounter, EmployeeNumber));
									rsData.Set_Fields("AdditionalDeductionID", lngDeductionID + 9000);
									rsData.Set_Fields("Amount", vsData.TextMatrix(intCounter, intAmountType));
									rsData.Update();
								}
							}
						}
						NextTry:
						;
					}
				}
				MessageBox.Show("Save completed Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveChanges = true;
				pboolDataChanged = false;
				return SaveChanges;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return SaveChanges;
			}
		}

		private void SetEmployeeAmountsInGrid()
		{
			rsData.OpenRecordset("Select * from tblW2AdditionalInfoData");
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (cboType1.SelectedIndex >= 0)
				{
					if (rsData.FindFirstRecord2("EmployeeNumber,AdditionalDeductionID", vsData.TextMatrix(intCounter, EmployeeNumber) + "," + FCConvert.ToString(cboType1.ItemData(cboType1.SelectedIndex) + 9000), ","))
					{
						vsData.TextMatrix(intCounter, AMOUNT1, FCConvert.ToString(rsData.Get_Fields("Amount")));
					}
					else
					{
						vsData.TextMatrix(intCounter, AMOUNT1, string.Empty);
					}
				}
				if (cboType2.SelectedIndex >= 0)
				{
					if (rsData.FindFirstRecord2("EmployeeNumber,AdditionalDeductionID", vsData.TextMatrix(intCounter, EmployeeNumber) + "," + FCConvert.ToString(cboType1.ItemData(cboType2.SelectedIndex) + 9000), ","))
					{
						vsData.TextMatrix(intCounter, AMOUNT2, FCConvert.ToString(rsData.Get_Fields("Amount")));
					}
					else
					{
						vsData.TextMatrix(intCounter, AMOUNT2, string.Empty);
					}
				}
				if (cboType3.SelectedIndex >= 0)
				{
					if (rsData.FindFirstRecord2("EmployeeNumber,AdditionalDeductionID", vsData.TextMatrix(intCounter, EmployeeNumber) + "," + FCConvert.ToString(cboType1.ItemData(cboType3.SelectedIndex) + 9000), ","))
					{
						vsData.TextMatrix(intCounter, AMOUNT3, FCConvert.ToString(rsData.Get_Fields("Amount")));
					}
					else
					{
						vsData.TextMatrix(intCounter, AMOUNT3, string.Empty);
					}
				}
			}
			pboolDataChanged = false;
		}

		private void cboType1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			vsData.TextMatrix(0, TYPE1, cboType1.Text);
			if (vsData.TextMatrix(0, TYPE1) != string.Empty)
			{
				if (vsData.TextMatrix(0, TYPE1) == vsData.TextMatrix(0, TYPE2) || vsData.TextMatrix(0, TYPE1) == vsData.TextMatrix(0, TYPE3))
				{
					MessageBox.Show("Duplicate types cannot be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboType1.SelectedIndex = 0;
					return;
				}
			}
			SetEmployeeAmountsInGrid();
		}

		private void cboType1_DropDown(object sender, System.EventArgs e)
		{
			if (pboolDataChanged)
			{
				if (MessageBox.Show("Data has changed. Save changes before showing new Deduction?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					SaveChanges();
				}
				else
				{
				}
				pboolDataChanged = false;
			}
		}

		private void cboType2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			vsData.TextMatrix(0, TYPE2, cboType2.Text);
			if (vsData.TextMatrix(0, TYPE2) != string.Empty)
			{
				if (vsData.TextMatrix(0, TYPE2) == vsData.TextMatrix(0, TYPE1) || vsData.TextMatrix(0, TYPE2) == vsData.TextMatrix(0, TYPE3))
				{
					MessageBox.Show("Duplicate types cannot be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboType2.SelectedIndex = 0;
					return;
				}
			}
			SetEmployeeAmountsInGrid();
		}

		private void cboType3_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			vsData.TextMatrix(0, TYPE3, cboType3.Text);
			if (vsData.TextMatrix(0, TYPE3) != string.Empty)
			{
				if (vsData.TextMatrix(0, TYPE3) == vsData.TextMatrix(0, TYPE1) || vsData.TextMatrix(0, TYPE3) == vsData.TextMatrix(0, TYPE2))
				{
					MessageBox.Show("Duplicate types cannot be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					cboType3.SelectedIndex = 0;
					return;
				}
			}
			SetEmployeeAmountsInGrid();
		}

		private void frmW2ThirdPartyEntries_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2ThirdPartyEntries_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			if (KeyAscii == Keys.Escape)
			{
				mnuExit_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmW2ThirdPartyEntries_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmW2ThirdPartyEntries properties;
			//frmW2ThirdPartyEntries.ScaleWidth	= 10770;
			//frmW2ThirdPartyEntries.ScaleHeight	= 7080;
			//frmW2ThirdPartyEntries.LinkTopic	= "Form1";
			//frmW2ThirdPartyEntries.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				SetGridProperties();
				LoadCombos();
				cboType1.SelectedIndex = 0;
				cboType2.SelectedIndex = 0;
				cboType3.SelectedIndex = 0;
				// open the forms global database connection
				// Set dbLengths = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsData.DefaultDB = "TWPY0000.vb1";
				// Get the data from the database
				boolLoading = true;
				ShowData();
				boolLoading = false;
				pboolDataChanged = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				string strSort = "";
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblDefaultInformation");
				// 0=EmployeeNumbe,r 1=Employee Name, 2=Sequence #, 3=DeptDiv
				if (!rsData.EndOfFile())
				{
					switch (rsData.Get_Fields_Int32("DataEntrySequence"))
					{
						case 0:
							{
								strSort = "LastName,FirstName";
								break;
							}
						case 1:
							{
								strSort = "EmployeeNumber";
								break;
							}
						case 2:
							{
								strSort = "SeqNumber";
								break;
							}
						case 3:
							{
								strSort = "DeptDiv";
								break;
							}
					}
					//end switch
				}
				else
				{
					strSort = "EmployeeNumber";
				}
				rsData.OpenRecordset("Select * from tblEmployeeMaster Order by LastName ");
				// & strSort
				if (!rsData.EndOfFile())
				{
					rsData.MoveLast();
					rsData.MoveFirst();
				}
				for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
				{
					vsData.Rows += 1;
					vsData.TextMatrix(intCounter, ID, FCConvert.ToString(rsData.Get_Fields("ID")));
					vsData.TextMatrix(intCounter, EmployeeNumber, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
					vsData.TextMatrix(intCounter, EmployeeName, rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName"));
					rsData.MoveNext();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmW2ThirdPartyEntries_Resize(object sender, System.EventArgs e)
		{
			vsData.Cols = 6;
			vsData.ColWidth(0, vsData.WidthOriginal * 0);
			vsData.ColWidth(1, FCConvert.ToInt32(vsData.WidthOriginal * 0.1));
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.4));
			vsData.ColWidth(3, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
			vsData.ColWidth(4, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
			vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsData.Select(0, 0);
				if (pboolDataChanged)
				{
					if (SaveChanges())
					{
						//MDIParent.InstancePtr.Show();
						// set focus back to the menu options of the MDIParent
						// CallByName MDIParent, "Grid_GotFocus", VbMethod
					}
					else
					{
						e.Cancel = true;
					}
				}
				else
				{
					//MDIParent.InstancePtr.Show();
					// set focus back to the menu options of the MDIParent
					// CallByName MDIParent, "Grid_GotFocus", VbMethod
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuReport_Click(object sender, System.EventArgs e)
		{
			// rptW2AdditionalInfoData.Show , MDIParent
			frmReportViewer.InstancePtr.Init(rptW2AdditionalInfoData.InstancePtr, boolAllowEmail: true, strAttachmentName: "W2AdditionalInfo");
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveChanges();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveChanges())
			{
				mnuExit_Click();
			}
		}

		private void SetGridProperties()
		{
			vsData.Cols = 6;
			vsData.Rows = 2;
			vsData.FixedCols = 3;
			vsData.ColHidden(0, true);
			vsData.FixedRows = 1;
			vsData.Sort = FCGrid.SortSettings.flexSortStringNoCaseAscending;
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.ColAlignment(EmployeeNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.TextMatrix(0, ID, "ID");
			vsData.TextMatrix(0, EmployeeNumber, "Emp #");
			vsData.TextMatrix(0, EmployeeName, "Employee Name");
			vsData.TextMatrix(0, TYPE1, string.Empty);
			vsData.TextMatrix(0, TYPE2, string.Empty);
			vsData.TextMatrix(0, TYPE3, string.Empty);
			vsData.ColWidth(EmployeeNumber, 800);
			vsData.ColWidth(EmployeeName, 3000);
			vsData.ColWidth(TYPE1, 2000);
			vsData.ColWidth(TYPE2, 2000);
			vsData.ColWidth(TYPE3, 2000);
		}

		private void LoadCombos()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblW2AdditionalInfo Order by ID");
			cboType1.AddItem("");
			cboType1.ItemData(cboType1.NewIndex, 0);
			while (!rsData.EndOfFile())
			{
				cboType1.AddItem(FCConvert.ToString(rsData.Get_Fields("Description")));
				cboType1.ItemData(cboType1.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}
			rsData.MoveFirst();
			cboType2.AddItem("");
			cboType2.ItemData(cboType2.NewIndex, 0);
			while (!rsData.EndOfFile())
			{
				cboType2.AddItem(FCConvert.ToString(rsData.Get_Fields("Description")));
				cboType2.ItemData(cboType2.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}
			rsData.MoveFirst();
			cboType3.AddItem("");
			cboType3.ItemData(cboType3.NewIndex, 0);
			while (!rsData.EndOfFile())
			{
				cboType3.AddItem(FCConvert.ToString(rsData.Get_Fields("Description")));
				cboType3.ItemData(cboType3.NewIndex, FCConvert.ToInt32(rsData.Get_Fields("ID")));
				rsData.MoveNext();
			}
		}

		private void vsData_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			pboolDataChanged = true;
		}

		private void vsData_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			int lngRow;
			lngRow = vsData.MouseRow;
			intCol = vsData.MouseCol;
			if (lngRow == 0)
			{
				if (intCol == EmployeeNumber || intCol == EmployeeName)
				{
					vsData.Col = intCol;
					if (Conversion.Val(vsData.ColData(intCol)) == 0)
					{
						vsData.Sort = FCGrid.SortSettings.flexSortStringAscending;
						vsData.ColData(intCol, 1);
					}
					else
					{
						vsData.Sort = FCGrid.SortSettings.flexSortStringDescending;
						vsData.ColData(intCol, 0);
					}
				}
			}
		}
	}
}
