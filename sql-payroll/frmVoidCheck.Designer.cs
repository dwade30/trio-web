//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmVoidCheck.
	/// </summary>
	partial class frmVoidCheck
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbAccountingPeriod;
		public T2KDateBox txtPayDate;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCLabel lblAccountingPeriod;
		public fecherFoundation.FCLabel lblHelp;
		public fecherFoundation.FCLabel lblPayDate;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.cmbAccountingPeriod = new fecherFoundation.FCComboBox();
            this.txtPayDate = new T2KDateBox();
            this.vsData = new fecherFoundation.FCGrid();
            this.lblAccountingPeriod = new fecherFoundation.FCLabel();
            this.lblHelp = new fecherFoundation.FCLabel();
            this.lblPayDate = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(690, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbAccountingPeriod);
            this.ClientArea.Controls.Add(this.txtPayDate);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Controls.Add(this.lblAccountingPeriod);
            this.ClientArea.Controls.Add(this.lblHelp);
            this.ClientArea.Controls.Add(this.lblPayDate);
            this.ClientArea.Size = new System.Drawing.Size(690, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(690, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(162, 30);
            this.HeaderText.Text = "Check Return";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbType
            // 
            this.cmbType.AutoSize = false;
            this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Regular",
            "Trust and Agency"});
            this.cmbType.Location = new System.Drawing.Point(220, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(217, 40);
            this.cmbType.TabIndex = 8;
            this.cmbType.Text = "Regular";
            this.ToolTip1.SetToolTip(this.cmbType, null);
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(141, 15);
            this.lblType.TabIndex = 9;
            this.lblType.Text = "CHECK RETURN TYPE";
            this.ToolTip1.SetToolTip(this.lblType, null);
            // 
            // cmbAccountingPeriod
            // 
            this.cmbAccountingPeriod.AutoSize = false;
            this.cmbAccountingPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccountingPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAccountingPeriod.FormattingEnabled = true;
            this.cmbAccountingPeriod.Location = new System.Drawing.Point(220, 90);
            this.cmbAccountingPeriod.Name = "cmbAccountingPeriod";
            this.cmbAccountingPeriod.Size = new System.Drawing.Size(217, 40);
            this.cmbAccountingPeriod.TabIndex = 7;
            this.cmbAccountingPeriod.Text = "Combo1";
            this.ToolTip1.SetToolTip(this.cmbAccountingPeriod, "Accounting period to use in journal entries");
            this.cmbAccountingPeriod.Visible = false;
            // 
            // txtPayDate
            // 
            this.txtPayDate.AutoSize = false;
            this.txtPayDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayDate.Location = new System.Drawing.Point(220, 150);
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.Size = new System.Drawing.Size(217, 40);
            this.txtPayDate.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtPayDate, null);
            this.txtPayDate.Visible = false;
            //this.txtPayDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPayDate_KeyPress);
            //this.txtPayDate.Enter += new System.EventHandler(this.txtPayDate_Enter);
            //this.txtPayDate.TextChanged += new System.EventHandler(this.txtPayDate_TextChanged);
            // 
            // vsData
            // 
            this.vsData.AllowSelection = false;
            this.vsData.AllowUserToResizeColumns = false;
            this.vsData.AllowUserToResizeRows = false;
            this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsData.BackColorBkg = System.Drawing.Color.Empty;
            this.vsData.BackColorFixed = System.Drawing.Color.Empty;
            this.vsData.BackColorSel = System.Drawing.Color.Empty;
            this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsData.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsData.ColumnHeadersHeight = 30;
            this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsData.DragIcon = null;
            this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsData.FixedCols = 0;
            this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsData.FrozenCols = 0;
            this.vsData.GridColor = System.Drawing.Color.Empty;
            this.vsData.GridColorFixed = System.Drawing.Color.Empty;
            this.vsData.Location = new System.Drawing.Point(30, 239);
            this.vsData.Name = "vsData";
            this.vsData.OutlineCol = 0;
            this.vsData.ReadOnly = true;
            this.vsData.RowHeadersVisible = false;
            this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsData.RowHeightMin = 0;
            this.vsData.Rows = 50;
            this.vsData.ScrollTipText = null;
            this.vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsData.ShowColumnVisibilityMenu = false;
            this.vsData.Size = new System.Drawing.Size(932, 372);
            this.vsData.StandardTab = true;
            this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsData.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.vsData, "Double-Click on a row to process it");
            this.vsData.Visible = false;
            this.vsData.DoubleClick += new System.EventHandler(this.vsData_DblClick);
            // 
            // lblAccountingPeriod
            // 
            this.lblAccountingPeriod.Location = new System.Drawing.Point(30, 104);
            this.lblAccountingPeriod.Name = "lblAccountingPeriod";
            this.lblAccountingPeriod.Size = new System.Drawing.Size(121, 22);
            this.lblAccountingPeriod.TabIndex = 8;
            this.lblAccountingPeriod.Text = "RETURN PERIOD";
            this.ToolTip1.SetToolTip(this.lblAccountingPeriod, "Accounting period to use in journal entries");
            this.lblAccountingPeriod.Visible = false;
            // 
            // lblHelp
            // 
            this.lblHelp.Location = new System.Drawing.Point(30, 210);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(351, 15);
            this.lblHelp.TabIndex = 6;
            this.lblHelp.Text = "DOUBLE-CLICK AN ENTRY TO PROCESS IT";
            this.ToolTip1.SetToolTip(this.lblHelp, null);
            this.lblHelp.Visible = false;
            // 
            // lblPayDate
            // 
            this.lblPayDate.Location = new System.Drawing.Point(30, 164);
            this.lblPayDate.Name = "lblPayDate";
            this.lblPayDate.Size = new System.Drawing.Size(62, 22);
            this.lblPayDate.TabIndex = 5;
            this.lblPayDate.Text = "PAY DATE";
            this.ToolTip1.SetToolTip(this.lblPayDate, null);
            this.lblPayDate.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process            ";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(289, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(106, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.ToolTip1.SetToolTip(this.cmdProcess, null);
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // frmVoidCheck
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(690, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmVoidCheck";
            this.Text = "Check Return";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmVoidCheck_Load);
            this.Activated += new System.EventHandler(this.frmVoidCheck_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVoidCheck_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVoidCheck_KeyPress);
            this.Resize += new System.EventHandler(this.frmVoidCheck_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
	}
}