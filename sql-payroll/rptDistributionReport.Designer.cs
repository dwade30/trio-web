﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDistributionReport.
	/// </summary>
	partial class rptDistributionReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDistributionReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblHours = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGrossPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroup1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIDNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmpNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIDNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtIDNo,
				this.txtDescription,
				this.txtHours,
				this.txtGrossPay,
				this.txtPayDate,
				this.txtEmpNo,
				this.txtEmployeeName
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.lblHours,
				this.lblGrossPay,
				this.Label1
			});
			this.PageHeader.Height = 0.75F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup1
			});
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.270833F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "CUSTOM  DISTRIBUTION  REPORT";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.395833F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.270833F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// lblHours
			// 
			this.lblHours.Height = 0.1666667F;
			this.lblHours.HyperLink = null;
			this.lblHours.Left = 5.666667F;
			this.lblHours.Name = "lblHours";
			this.lblHours.Style = "font-weight: bold; text-align: right";
			this.lblHours.Text = "Hours";
			this.lblHours.Top = 0.5833333F;
			this.lblHours.Width = 0.6666667F;
			// 
			// lblGrossPay
			// 
			this.lblGrossPay.Height = 0.1666667F;
			this.lblGrossPay.HyperLink = null;
			this.lblGrossPay.Left = 6.416667F;
			this.lblGrossPay.Name = "lblGrossPay";
			this.lblGrossPay.Style = "font-weight: bold; text-align: right";
			this.lblGrossPay.Text = "Gross Pay";
			this.lblGrossPay.Top = 0.5833333F;
			this.lblGrossPay.Width = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 4.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: left";
			this.Label1.Text = "Pay Date";
			this.Label1.Top = 0.5833333F;
			this.Label1.Width = 0.84375F;
			// 
			// txtGroup1
			// 
			this.txtGroup1.DataField = "Group1DField";
			this.txtGroup1.Height = 0.1666667F;
			this.txtGroup1.Left = 0F;
			this.txtGroup1.Name = "txtGroup1";
			this.txtGroup1.Style = "font-weight: bold";
			this.txtGroup1.Text = "Field1";
			this.txtGroup1.Top = 0F;
			this.txtGroup1.Width = 4F;
			// 
			// txtIDNo
			// 
			this.txtIDNo.Height = 0.1666667F;
			this.txtIDNo.Left = 0.1666667F;
			this.txtIDNo.Name = "txtIDNo";
			this.txtIDNo.Text = "Field1";
			this.txtIDNo.Top = 0F;
			this.txtIDNo.Width = 0.5833333F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 0.8333333F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = "Field1";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 3.833333F;
			// 
			// txtHours
			// 
			this.txtHours.Height = 0.1666667F;
			this.txtHours.Left = 5.666667F;
			this.txtHours.Name = "txtHours";
			this.txtHours.Style = "text-align: right";
			this.txtHours.Text = "Field1";
			this.txtHours.Top = 0F;
			this.txtHours.Width = 0.6666667F;
			// 
			// txtGrossPay
			// 
			this.txtGrossPay.Height = 0.1666667F;
			this.txtGrossPay.Left = 6.416667F;
			this.txtGrossPay.Name = "txtGrossPay";
			this.txtGrossPay.Style = "text-align: right";
			this.txtGrossPay.Text = "Field1";
			this.txtGrossPay.Top = 0F;
			this.txtGrossPay.Width = 1F;
			// 
			// txtPayDate
			// 
			this.txtPayDate.Height = 0.1666667F;
			this.txtPayDate.Left = 4.75F;
			this.txtPayDate.Name = "txtPayDate";
			this.txtPayDate.Style = "text-align: left";
			this.txtPayDate.Text = null;
			this.txtPayDate.Top = 0F;
			this.txtPayDate.Width = 0.8333333F;
			// 
			// txtEmpNo
			// 
			this.txtEmpNo.Height = 0.1666667F;
			this.txtEmpNo.Left = 2.166667F;
			this.txtEmpNo.Name = "txtEmpNo";
			this.txtEmpNo.Style = "text-align: right";
			this.txtEmpNo.Text = "Field1";
			this.txtEmpNo.Top = 0F;
			this.txtEmpNo.Visible = false;
			this.txtEmpNo.Width = 0.5F;
			// 
			// txtEmployeeName
			// 
			this.txtEmployeeName.Height = 0.1666667F;
			this.txtEmployeeName.Left = 2.75F;
			this.txtEmployeeName.Name = "txtEmployeeName";
			this.txtEmployeeName.Text = "Field1";
			this.txtEmployeeName.Top = 0F;
			this.txtEmployeeName.Visible = false;
			this.txtEmployeeName.Width = 1.666667F;
			// 
			// rptDistributionReport
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIDNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIDNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmpNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHours;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
