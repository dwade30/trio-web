﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cTaxAdj941
	{
		//=========================================================
		private string strPayDate = string.Empty;
		private int intPayRunID;
		private double dblActualAmount;
		private bool boolAdjustment;
		private string strEffectiveDate = string.Empty;

		public string PayDate
		{
			set
			{
				strPayDate = value;
			}
			get
			{
				string PayDate = "";
				PayDate = strPayDate;
				return PayDate;
			}
		}

		public int PayRunID
		{
			set
			{
				intPayRunID = value;
			}
			get
			{
				int PayRunID = 0;
				PayRunID = intPayRunID;
				return PayRunID;
			}
		}

		public double Amount
		{
			set
			{
				dblActualAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblActualAmount;
				return Amount;
			}
		}

		public bool IsAdjustment
		{
			set
			{
				boolAdjustment = value;
			}
			get
			{
				bool IsAdjustment = false;
				IsAdjustment = boolAdjustment;
				return IsAdjustment;
			}
		}

		public string EffectiveDate
		{
			set
			{
				strEffectiveDate = value;
			}
			get
			{
				string EffectiveDate = "";
				EffectiveDate = strEffectiveDate;
				return EffectiveDate;
			}
		}
	}
}
