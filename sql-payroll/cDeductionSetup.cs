﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	public class cDeductionSetup
	{
		//=========================================================
		private int lngDeductionNumber;
		private int lngRecordID;
		private string strDescription = string.Empty;
		private int intFrequencyCode;
		private int intTaxStatusCode;
		private double dblDefaultAmount;
		private string strAmountType = string.Empty;
		private double dblLimit;
		private string strLimitType = string.Empty;
		private string strStatus = string.Empty;
		private int intPriority;
		private string strAccountID = string.Empty;
		private string strAccountDescription = string.Empty;
		private string strPercentType = string.Empty;
		private string strLastUserID = string.Empty;
		private string strLastUpdate = "";
		private int intOldFrequency;
		private bool boolUExempt;
		private bool boolUpdated;
		private bool boolDeleted;

		public int DeductionNumber
		{
			set
			{
				lngDeductionNumber = value;
				IsUpdated = true;
			}
			get
			{
				int DeductionNumber = 0;
				DeductionNumber = lngDeductionNumber;
				return DeductionNumber;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int FrequencyCode
		{
			set
			{
				intFrequencyCode = value;
				IsUpdated = true;
			}
			get
			{
				int FrequencyCode = 0;
				FrequencyCode = intFrequencyCode;
				return FrequencyCode;
			}
		}

		public int TaxStatusCode
		{
			set
			{
				intTaxStatusCode = value;
				IsUpdated = true;
			}
			get
			{
				int TaxStatusCode = 0;
				TaxStatusCode = intTaxStatusCode;
				return TaxStatusCode;
			}
		}

		public double Amount
		{
			set
			{
				dblDefaultAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblDefaultAmount;
				return Amount;
			}
		}

		public string AmountType
		{
			set
			{
				strAmountType = value;
				IsUpdated = true;
			}
			get
			{
				string AmountType = "";
				AmountType = strAmountType;
				return AmountType;
			}
		}

		public double Limit
		{
			set
			{
				dblLimit = value;
				IsUpdated = true;
			}
			get
			{
				double Limit = 0;
				Limit = dblLimit;
				return Limit;
			}
		}

		public string LimitType
		{
			set
			{
				strLimitType = value;
				IsUpdated = true;
			}
			get
			{
				string LimitType = "";
				LimitType = strLimitType;
				return LimitType;
			}
		}

		public string Status
		{
			set
			{
				strStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}

		public int Priority
		{
			set
			{
				intPriority = value;
				IsUpdated = true;
			}
			get
			{
				int Priority = 0;
				Priority = intPriority;
				return Priority;
			}
		}

		public string AccountID
		{
			set
			{
				strAccountID = value;
				IsUpdated = true;
			}
			get
			{
				string AccountID = "";
				AccountID = strAccountID;
				return AccountID;
			}
		}

		public string AccountDescription
		{
			set
			{
				strAccountDescription = value;
				IsUpdated = true;
			}
			get
			{
				string AccountDescription = "";
				AccountDescription = strAccountDescription;
				return AccountDescription;
			}
		}

		public string Percent
		{
			set
			{
				strPercentType = value;
				IsUpdated = true;
			}
			get
			{
				string Percent = "";
				Percent = strPercentType;
				return Percent;
			}
		}

		public string LastUserID
		{
			set
			{
				IsUpdated = true;
				strLastUserID = value;
			}
			get
			{
				string LastUserID = "";
				LastUserID = strLastUserID;
				return LastUserID;
			}
		}

		public string LastUpdate
		{
			set
			{
				IsUpdated = true;
				if (Information.IsDate(value))
				{
					strLastUpdate = value;
				}
				else
				{
					strLastUpdate = "";
				}
			}
			get
			{
				string LastUpdate = "";
				LastUpdate = strLastUpdate;
				return LastUpdate;
			}
		}

		public int OldFrequency
		{
			set
			{
				IsUpdated = true;
				intOldFrequency = value;
			}
			get
			{
				int OldFrequency = 0;
				OldFrequency = intOldFrequency;
				return OldFrequency;
			}
		}

		public bool UExempt
		{
			set
			{
				IsUpdated = true;
				boolUExempt = value;
			}
			get
			{
				bool UExempt = false;
				UExempt = boolUExempt;
				return UExempt;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool Deleted
		{
			set
			{
				IsUpdated = true;
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}
	}
}
