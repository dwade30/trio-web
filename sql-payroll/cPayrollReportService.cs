﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cPayrollReportService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private Dictionary<object, object> dictStates = new Dictionary<object, object>();
		private cEmployeeService empServ = new cEmployeeService();
		private cPYDistributionController distServ = new cPYDistributionController();
		private cPYTaxStatusCodeController tsController = new cPYTaxStatusCodeController();
		private cPayCategoryController pcController = new cPayCategoryController();
		private cPayCodeController payCodeController = new cPayCodeController();

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public void FillCustomDistributionSetupReport(ref cPYDistributionSetupReport theReport)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (theReport == null)
				{
					fecherFoundation.Information.Err().Raise(9999, "", "Report object is null", null, null);
					return;
				}
				cGenericCollection listEmps;
				string strOrderBy = "";
				bool boolUseSpecificEmployees = false;
				bool boolUseSpecificAccounts = false;
				bool boolUseRangeAccounts = false;
				bool boolUseSpecificGroup = false;
				if (theReport.EmployeesToReport.Count > 0)
				{
					boolUseSpecificEmployees = true;
				}
				else
				{
					boolUseSpecificEmployees = false;
				}
				if (theReport.Accounts.Count > 0)
				{
					boolUseSpecificAccounts = true;
				}
				else
				{
					boolUseSpecificAccounts = false;
				}
				if (theReport.StartAccount != "" && theReport.EndAccount != "")
				{
					boolUseRangeAccounts = true;
				}
				if (theReport.GroupID != "")
				{
					boolUseSpecificGroup = true;
				}
				else
				{
					boolUseSpecificGroup = false;
				}
				switch (theReport.OrderByItem)
				{
					case 1:
						{
							strOrderBy = "lastname,firstname,employeenumber";
							break;
						}
					default:
						{
							strOrderBy = "EmployeeNumber";
							break;
						}
				}
				//end switch
				int lngUserID;
				lngUserID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				listEmps = empServ.GetEmployeesPermissable(lngUserID, strOrderBy, false);
				if (FCConvert.ToBoolean(empServ.HadError))
				{
					SetError(empServ.LastErrorNumber, empServ.LastErrorMessage);
					return;
				}
				listEmps.MoveFirst();
				cEmployee tempEmp;
				bool boolUse = false;
				cGenericCollection listDist;
				cPayrollDistribution tempDist;
				cPYDistributionReportItem distItem;
				Dictionary<object, object> dictTaxStatuses = new Dictionary<object, object>();
				// vbPorter upgrade warning: tempTaxStatusCode As cPYTaxStatusCode	OnWrite(object)
				cPYTaxStatusCode tempTaxStatusCode;
				Dictionary<object, object> dictPayCategories = new Dictionary<object, object>();
				// vbPorter upgrade warning: tempPayCategory As cPayCategory	OnWrite(object)
				cPayCategory tempPayCategory;
				Dictionary<object, object> dictPayCodes = new Dictionary<object, object>();
				// vbPorter upgrade warning: tempPayCode As cPayCode	OnWrite(object)
				cPayCode tempPayCode;
				dictTaxStatuses = tsController.GetTaxStatusCodesAsDictionary();
				if (FCConvert.ToBoolean(tsController.HadError))
				{
					SetError(tsController.LastErrorNumber, tsController.LastErrorMessage);
					return;
				}
				dictPayCategories = pcController.GetPayCategoriesAsDictionary();
				if (FCConvert.ToBoolean(pcController.HadError))
				{
					SetError(pcController.LastErrorNumber, pcController.LastErrorMessage);
					return;
				}
				dictPayCodes = payCodeController.GetPayCodesAsDictionary();
				if (FCConvert.ToBoolean(payCodeController.HadError))
				{
					SetError(payCodeController.LastErrorNumber, payCodeController.LastErrorMessage);
					return;
				}
				while (listEmps.IsCurrent())
				{
					//App.DoEvents();
					tempEmp = (cEmployee)listEmps.GetCurrentItem();
					boolUse = true;
					if (boolUseSpecificEmployees)
					{
						if (!theReport.EmployeesToReport.ContainsKey(tempEmp.EmployeeNumber))
						{
							boolUse = false;
						}
					}
					if (boolUse)
					{
						if (boolUseSpecificGroup)
						{
							if (tempEmp.GroupID != theReport.GroupID)
							{
								boolUse = false;
							}
						}
					}
					if (boolUse)
					{
						listDist = distServ.GetDistributionsByEmployeeNumber(tempEmp.EmployeeNumber);
						if (!(listDist == null))
						{
							listDist.MoveFirst();
							while (listDist.IsCurrent())
							{
								//App.DoEvents();
								tempDist = (cPayrollDistribution)listDist.GetCurrentItem();
								boolUse = true;
								if (!theReport.PayCodes.ContainsKey(tempDist.Category))
								{
									boolUse = false;
								}
								if (boolUse && boolUseRangeAccounts)
								{
									if (!(FCConvert.ToInt32(tempDist.AccountNumber) >= FCConvert.ToInt32(theReport.StartAccount) && FCConvert.ToInt32(tempDist.AccountNumber) <= FCConvert.ToInt32(theReport.EndAccount)))
									{
										boolUse = false;
									}
								}
								if (boolUse && boolUseSpecificAccounts)
								{
									if (!theReport.Accounts.ContainsKey(tempDist.AccountNumber))
									{
										boolUse = false;
									}
								}
								if (boolUse)
								{
									distItem = new cPYDistributionReportItem();
									distItem.AccountCode = tempDist.AccountCode;
									distItem.AccountNumber = tempDist.AccountNumber;
									distItem.BaseRate = tempDist.BaseRate;
									distItem.Category = tempDist.Category;
									distItem.CD = tempDist.CD;
									distItem.ContractID = tempDist.ContractID;
									distItem.DefaultHours = tempDist.DefaultHours;
									distItem.Deleted = tempDist.Deleted;
									distItem.DistU = tempDist.DistU;
									distItem.EmployeeNumber = tempDist.EmployeeNumber;
									distItem.Factor = tempDist.Factor;
									distItem.GrantFunded = tempDist.GrantFunded;
									distItem.Gross = tempDist.Gross;
									distItem.HoursWeek = tempDist.HoursWeek;
									distItem.ID = tempDist.ID;
									distItem.MSRS = tempDist.MSRS;
									distItem.MSRSID = tempDist.MSRSID;
									distItem.NumberWeeks = tempDist.NumberWeeks;
									distItem.Project = tempDist.Project;
									distItem.RecordNumber = tempDist.RecordNumber;
									distItem.StatusCode = tempDist.StatusCode;
									distItem.TaxCode = tempDist.TaxCode;
									distItem.WC = tempDist.WC;
									distItem.WeeksTaxWithheld = tempDist.WeeksTaxWithheld;
									distItem.WorkComp = tempDist.WorkComp;
									if (tempDist.Category > 0)
									{
										if (dictPayCategories.ContainsKey(tempDist.Category))
										{
											tempPayCategory = (cPayCategory)dictPayCategories[tempDist.Category];
											distItem.CategoryDescription = tempPayCategory.Description;
											if (dictTaxStatuses.ContainsKey(tempPayCategory.TaxStatus))
											{
												tempTaxStatusCode = (cPYTaxStatusCode)dictTaxStatuses[tempPayCategory.TaxStatus];
												distItem.TaxStatusDescription = tempTaxStatusCode.Description;
											}
										}
									}
									if (tempDist.CD > 0)
									{
										if (dictPayCodes.ContainsKey(tempDist.CD))
										{
											tempPayCode = (cPayCode)dictPayCodes[tempDist.CD];
											distItem.PayCodeDescription = tempPayCode.Description;
										}
									}
									tempEmp.DistributionRecords.AddItem(distItem);
								}
								listDist.MoveNext();
							}
						}
						if (tempEmp.DistributionRecords.ItemCount() > 0)
						{
							theReport.Employees.AddItem(tempEmp);
						}
					}
					listEmps.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}
		// gstrCheckListingSQL = "SELECT tblEmployeeMaster.*,tblPayrollDistribution.* from tblEmployeeMaster INNER JOIN tblPayrollDistribution on tblEmployeeMaster.EmployeeNumber = tblPayrollDistribution.EmployeeNumber where CAT <> 0 "
	}
}
