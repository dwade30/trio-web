//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Commands;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
    public partial class frmW2CList : BaseForm
    {
        public frmW2CList()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmW2CList InstancePtr
        {
            get
            {
                return (frmW2CList)Sys.GetInstance(typeof(frmW2CList));
            }
        }

        protected frmW2CList _InstancePtr = null;
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By
        // Date
        // ********************************************************
        const int CNSTGridColUse = 0;
        const int CNSTGRIDCOLEMPLOYEENUMBER = 1;
        const int CNSTGRIDCOLEMPLOYEENAME = 2;
        const int CNSTGRIDCOLSSN = 3;
        const int CNSTGRIDCOLDEPTDIV = 4;
        const int CNSTGRIDCOLSEQUENCE = 5;

        private void frmW2CList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmW2CList_Load(object sender, System.EventArgs e)
        {
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            SetupGrid();
            LoadGrid();
        }

        private void frmW2CList_Resize(object sender, System.EventArgs e)
        {
            ResizeGrid();
        }
        // vbPorter upgrade warning: NewCol As int	OnRead
        private void Grid_BeforeRowColChange(object sender, DataGridViewCellEventArgs e)
        {
            // vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
            int intCol;
            int lngRow;
            intCol = Grid.Col;
            lngRow = Grid.Row;
            if (intCol == CNSTGridColUse && lngRow > 0)
            {
                if (Grid.Editable != FCGrid.EditableSettings.flexEDKbdMouse)
                {
                    Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                    if (FCConvert.CBool(Grid.TextMatrix(lngRow, intCol)))
                    {
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpChecked, lngRow, intCol, false);
                    }
                    else
                    {
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpChecked, lngRow, intCol, true);
                    }
                    // Grid.TextMatrix(lngRow, intCol) = Not (CBool(Grid.TextMatrix(lngRow, intCol)))
                }
            }
            else
            {
                Grid.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private void Grid_DblClick(object sender, System.EventArgs e)
        {
            int lngYear;
            int lngRow;
            int recordId = 0;

            if (Grid.MouseRow < 1)
                return;
            if (Conversion.Val(txtYear.Text) < 2000 || Conversion.Val(txtYear.Text) > DateTime.Today.Year)
            {
                MessageBox.Show("Invalid Year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
            lngRow = Grid.MouseRow;

            recordId = StaticSettings.GlobalCommandDispatcher.Send(new InitializeW2CInfo
            {
                SocialSecurityNumber = Grid.TextMatrix(lngRow, CNSTGRIDCOLSSN),
                TaxYear = lngYear,
                EmployeeNumber = Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENUMBER),
                DeptDiv = Grid.TextMatrix(lngRow, CNSTGRIDCOLDEPTDIV),
                SequenceNumber = Grid.TextMatrix(lngRow, CNSTGRIDCOLSEQUENCE).ToIntegerValue()
            }).Result;

            frmW2CEdit.InstancePtr.Init(ref lngYear, Grid.TextMatrix(lngRow, CNSTGRIDCOLSSN), recordId);
        }

        private void Grid_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int x = 0;
            x = x;
        }

        private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int x = 0;
            x = x;
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void SetupGrid()
        {
            Grid.Rows = 1;
            Grid.Cols = 6;
            Grid.ColDataType(CNSTGridColUse, FCGrid.DataTypeSettings.flexDTBoolean);
            Grid.ExtendLastCol = true;
            Grid.TextMatrix(0, CNSTGRIDCOLEMPLOYEENUMBER, "Employee");
            Grid.TextMatrix(0, CNSTGRIDCOLEMPLOYEENAME, "Name");
            Grid.TextMatrix(0, CNSTGRIDCOLSSN, "SSN");
            Grid.TextMatrix(0, CNSTGRIDCOLDEPTDIV, "Dept-Div");
            Grid.TextMatrix(0, CNSTGRIDCOLSEQUENCE, "Sequence");
        }

        private void ResizeGrid()
        {
            int GridWidth = 0;
            GridWidth = Grid.WidthOriginal;
            Grid.ColWidth(CNSTGridColUse, FCConvert.ToInt32(0.06 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToInt32(0.11 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLEMPLOYEENAME, FCConvert.ToInt32(0.43 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLSSN, FCConvert.ToInt32(0.15 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLDEPTDIV, FCConvert.ToInt32(0.1 * GridWidth));
        }

        private void LoadGrid()
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSQL;
                string strOrderBy = "";
                int lngRow;
                string strTemp = "";
                int lngTemp;
                lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("W2CYear", "PY"))));
                if (lngTemp == 0)
                {
                    lngTemp = DateTime.Today.Year;
                }
                txtYear.Text = FCConvert.ToString(lngTemp);
                Grid.Rows = 1;
                strSQL = "select * from tblemployeemaster order by ";
                switch (modCoreysSweeterCode.GetReportSequence())
                {
                    case 0:
                        {
                            // name
                            strOrderBy = " lastname,firstname ";
                            break;
                        }
                    case 1:
                        {
                            // number
                            strOrderBy = " employeenumber ";
                            break;
                        }
                    case 2:
                        {
                            // seqnumber
                            strOrderBy = " seqnumber,lastname,firstname ";
                            break;
                        }
                    case 3:
                        {
                            // deptdiv
                            strOrderBy = " deptdiv,lastname,firstname ";
                            break;
                        }
                    default:
                        {
                            strOrderBy = " lastname,firstname ";
                            break;
                        }
                }
                //end switch
                strSQL += strOrderBy;
                rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                while (!rsLoad.EndOfFile())
                {
                    Grid.Rows += 1;
                    lngRow = Grid.Rows - 1;
                    Grid.TextMatrix(lngRow, CNSTGRIDCOLDEPTDIV, FCConvert.ToString(rsLoad.Get_Fields("deptdiv")));
                    Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENAME, fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsLoad.Get_Fields("lastname") + ", " + rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("middlename")) + " " + rsLoad.Get_Fields("desig")));
                    Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENUMBER, FCConvert.ToString(rsLoad.Get_Fields("employeenumber")));
                    Grid.TextMatrix(lngRow, CNSTGRIDCOLSEQUENCE, FCConvert.ToString(rsLoad.Get_Fields("seqnumber")));
                    Grid.TextMatrix(lngRow, CNSTGridColUse, FCConvert.ToString(false));
                    strTemp = Strings.Right(Strings.StrDup(9, "0") + rsLoad.Get_Fields("ssn"), 9);
                    strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
                    Grid.TextMatrix(lngRow, CNSTGRIDCOLSSN, strTemp);
                    rsLoad.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReloadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuLoadList_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngRow;
            rsLoad.OpenRecordset("select * from w2clist", "twpy0000.vb1");
            for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
            {
                if (rsLoad.FindFirstRecord("employeenumber", Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENUMBER)))
                {
                    Grid.TextMatrix(lngRow, CNSTGridColUse, FCConvert.ToString(true));
                }
                else
                {
                }
            }
            // lngRow
            MessageBox.Show("List Loaded", "Loaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void mnuPrintTestPage_Click(object sender, System.EventArgs e)
        {
            clsW2C clsW2CSummary = new clsW2C();
            clsW2CSummary.TaxYear = DateTime.Today.Year;
            rptW2C.InstancePtr.Init(DateTime.Today.Year, clsW2CSummary, this.Modal, true, 0, 0, true);
        }

        private void mnuPrintW3CTestPage_Click(object sender, System.EventArgs e)
        {
            clsW2C clsW2CSummary = new clsW2C();
            clsW2CSummary.TaxYear = DateTime.Today.Year;
            rptW3C.InstancePtr.Init(ref clsW2CSummary, this.Modal, true);
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                Grid.Row = 0;
                clsDRWrapper rsLoad = new clsDRWrapper();
                int lngYear;
                int lngRow;
                string strTemp = "";
                clsDRWrapper rsSave = new clsDRWrapper();
                bool boolW2 = false;
                if (Conversion.Val(txtYear.Text) < 2000 || Conversion.Val(txtYear.Text) > DateTime.Today.Year)
                {
                    MessageBox.Show("Invalid Year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
                modRegistry.SaveRegistryKey("W2CYear", FCConvert.ToString(lngYear), "PY");
                int lngTemp;
                lngRow = Grid.FindRow(true, 1, CNSTGridColUse);
                if (lngRow < 1)
                {
                    MessageBox.Show("You must choose at least one employee to correct", "No Employees Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                rsSave.Execute("update w2c set [current] = 0", "twpy0000.vb1");
                rsSave.OpenRecordset("select * from w2c where taxyear = " + FCConvert.ToString(lngYear), "twpy0000.vb1");
                while (lngRow > 0)
                {
                    strTemp = Grid.TextMatrix(lngRow, CNSTGRIDCOLSSN);
                    strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
                    if (!rsSave.FindFirstRecord("ssn", Grid.TextMatrix(lngRow, CNSTGRIDCOLSSN)))
                    {
                        MessageBox.Show("Employee " + Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENUMBER) + " has no W-2C information for year " + FCConvert.ToString(lngYear) + "\r\n" + "Either uncheck this employee or add W-2C information", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    rsSave.Edit();
                    rsSave.Set_Fields("current", true);
                    rsSave.Set_Fields("orderno", lngRow);
                    rsSave.Update();
                    if (lngRow < Grid.Rows - 1)
                    {
                        lngRow = Grid.FindRow(true, lngRow + 1, CNSTGridColUse);
                    }
                    else
                    {
                        lngRow = -1;
                    }
                }
                if (cmbW2.Text == "W-2")
                {
                    boolW2 = true;
                }
                else
                {
                    boolW2 = false;
                }
                if (cmbType.Text == "Paper")
                {
                    // printed
                    clsW2C clsW2CSummary = new clsW2C();
                    clsW2CSummary.TaxYear = lngYear;
                    rptW2C.InstancePtr.Init(lngYear, clsW2CSummary, true, boolW2);
                    if (frmW3C.InstancePtr.Init(clsW2CSummary))
                    {
                        rptW3C.InstancePtr.Init(ref clsW2CSummary, this.Modal);
                    }
                }
                else
                {
                    // electronic
                    clsW2C clsW2CFile = new clsW2C();
                    clsW2CFile.TaxYear = lngYear;
                    clsW2CFile.CreateElectronicW2C();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuSaveExit_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuSaveList_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsSave = new clsDRWrapper();
            rsSave.Execute("delete from w2clist", "twpy0000.vb1");
            int lngRow;
            rsSave.OpenRecordset("select * from w2clist", "twpy0000.vb1");
            for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
            {
                if (FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGridColUse)))
                {
                    rsSave.AddNew();
                    rsSave.Set_Fields("Employeenumber", Grid.TextMatrix(lngRow, CNSTGRIDCOLEMPLOYEENUMBER));
                    rsSave.Update();
                }
            }
            // lngRow
            MessageBox.Show("List Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Grid_Click(object sender, EventArgs e)
        {

        }
    }
}
