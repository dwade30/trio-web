﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cEmployeeChecksReport
	{
		//=========================================================
		private DateTime dtPayDate;
		private int intPayrun;
		private cGenericCollection colEmployeeChecks = new cGenericCollection();
		private int lngCheckFormatID;
		private cCustomCheckFormat ckFormat = new cCustomCheckFormat();
		private int lngBankID;
		private string strBankName = string.Empty;
		private cPayRun payRunInfo = new cPayRun();
		private bool boolAllNonNegotiable;
        private bool boolShowPayPeriod;

		public bool AllNonNegotiable
		{
			set
			{
				boolAllNonNegotiable = value;
			}
			get
			{
				bool AllNonNegotiable = false;
				AllNonNegotiable = boolAllNonNegotiable;
				return AllNonNegotiable;
			}
		}

        public bool ShowPayPeriodOnStub
		{
            set
            {
                boolShowPayPeriod = value;
            }
            get
            {
                return boolShowPayPeriod;
            }
        }

		public cPayRun Payrun
		{
			set
			{
				payRunInfo = value;
			}
			get
			{
				cPayRun Payrun = null;
				Payrun = payRunInfo;
				return Payrun;
			}
		}

		public int BankID
		{
			set
			{
				lngBankID = value;
			}
			get
			{
				int BankID = 0;
				BankID = lngBankID;
				return BankID;
			}
		}

		public string BankName
		{
			set
			{
				strBankName = value;
			}
			get
			{
				string BankName = "";
				BankName = strBankName;
				return BankName;
			}
		}

		public cCustomCheckFormat checkFormat
		{
			set
			{
				ckFormat = value;
			}
			get
			{
				cCustomCheckFormat checkFormat = null;
				checkFormat = ckFormat;
				return checkFormat;
			}
		}

		public int CheckFormatID
		{
			set
			{
				lngCheckFormatID = value;
			}
			get
			{
				int CheckFormatID = 0;
				CheckFormatID = lngCheckFormatID;
				return CheckFormatID;
			}
		}

		public DateTime PayDate
		{
			set
			{
				dtPayDate = value;
			}
			get
			{
				DateTime PayDate = System.DateTime.Now;
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public int PayrunNumber
		{
			set
			{
				intPayrun = value;
			}
			get
			{
				int PayrunNumber = 0;
				PayrunNumber = intPayrun;
				return PayrunNumber;
			}
		}

		public cGenericCollection employeeChecks
		{
			get
			{
				cGenericCollection employeeChecks = null;
				employeeChecks = colEmployeeChecks;
				return employeeChecks;
			}
		}
	}
}
