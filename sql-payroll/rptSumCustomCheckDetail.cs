//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSumCustomCheckDetail.
	/// </summary>
	public partial class rptSumCustomCheckDetail : BaseSectionReport
	{
		public rptSumCustomCheckDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Check Detail Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSumCustomCheckDetail InstancePtr
		{
			get
			{
				return (rptSumCustomCheckDetail)Sys.GetInstance(typeof(rptSumCustomCheckDetail));
			}
		}

		protected rptSumCustomCheckDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsEmployee?.Dispose();
                rsData = null;
                rsEmployee = null;
				employeeDict?.Clear();
                employeeDict = null;
				employeeService?.Dispose();
                employeeService = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSumCustomCheckDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       August 17,2004
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		int intpage;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsEmployee = new clsDRWrapper();
		int intField;
		object FieldName;
		double dblField1Total;
		double dblField2Total;
		double dblField3Total;
		double dblField4Total;
		double dblField5Total;
		double dblField6Total;
		double dblField7Total;
		double dblField8Total;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			while (!rsData.EndOfFile())
			{
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				if (modGlobalConstants.Statics.gstrDeptDiv == string.Empty)
				{
				}
				else
				{
					TryNextEmployee:
					;
					if (Strings.Left(FCConvert.ToString(rsData.Get_Fields("DeptDiv")), modGlobalConstants.Statics.gstrDeptDiv.Length) == modGlobalConstants.Statics.gstrDeptDiv)
					{
						// print this employee
					}
					else
					{
						if (!rsData.EndOfFile())
							rsData.MoveNext();
						if (!rsData.EndOfFile())
						{
							goto TryNextEmployee;
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				// display the data
				rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
				if (rsEmployee.NoMatch)
				{
					txtEmployee.Text = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
				}
				else
				{
					txtEmployee.Text = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")) + " - " + rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("LastName");
				}
				intField = 1;
				for (x = 0; x <= (rsData.FieldsCount - 1); x++)
				{
					if (Strings.Left(rsData.Get_FieldsIndexName(x), 5) == "SumOf")
					{
						switch (intField)
						{
							case 1:
								{
									Field1.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField1Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 2:
								{
									Field2.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField2Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 3:
								{
									Field3.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField3Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 4:
								{
									Field4.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField4Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 5:
								{
									Field5.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField5Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 6:
								{
									Field6.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField6Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 7:
								{
									Field7.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField7Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							case 8:
								{
									Field8.Text = Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00");
									dblField8Total += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields(rsData.Get_FieldsIndexName(x))), "0.00"));
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
						intField += 1;
					}
				}
				// For Each FieldName In rsData.AllFields
				// If Left(FieldName.Name, 5) = "SumOf" Then
				// Select Case intField
				// Case 1
				// Field1 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField1Total = dblField1Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 2
				// Field2 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField2Total = dblField2Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 3
				// Field3 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField3Total = dblField3Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 4
				// Field4 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField4Total = dblField4Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 5
				// Field5 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField5Total = dblField5Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 6
				// Field6 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField6Total = dblField6Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 7
				// Field7 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField7Total = dblField7Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case 8
				// Field8 = Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// dblField8Total = dblField8Total + Format(Val(rsData.Fields(FieldName.Name)), "0.00")
				// Case Else
				// End Select
				// 
				// intField = intField + 1
				// End If
				// Next
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page #" + FCConvert.ToString(intpage);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, "TWPY0000.vb1");
			rsEmployee.OpenRecordset("Select * from tblEmployeeMaster", "TWPY0000.vb1");
			intpage = 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			txtCaption.Text = "Check Detail Summary";
			// M
			// THIS VARIABLE IS SET DURING THE CLICK EVENT OF THE REPORT ON
			// OPTION ON THE FORM FRMSUMCUSTOMCHECKDETAIL.
			txtCaption2.Text = modGlobalVariables.Statics.gstrTitle2Caption;
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			intField = 1;
			for (x = 0; x <= (rsData.FieldsCount - 1); x++)
			{
				if (Strings.Left(rsData.Get_FieldsIndexName(x), 5) == "SumOf")
				{
					switch (intField)
					{
						case 1:
							{
								Label1.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 2:
							{
								Label2.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 3:
							{
								Label3.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 4:
							{
								Label4.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 5:
							{
								Label5.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 6:
							{
								Label6.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 7:
							{
								Label7.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						case 8:
							{
								Label8.Text = Strings.Mid(rsData.Get_FieldsIndexName(x), 6, rsData.Get_FieldsIndexName(x).Length - 5);
								break;
							}
						default:
							{
								break;
							}
					}
					//end switch
					intField += 1;
				}
			}
			// For Each FieldName In rsData.AllFields
			// If Left(FieldName.Name, 5) = "SumOf" Then
			// Select Case intField
			// Case 1
			// Label1.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 2
			// Label2.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 3
			// Label3.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 4
			// Label4.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 5
			// Label5.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 6
			// Label6.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 7
			// Label7.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case 8
			// Label8.Caption = Mid(FieldName.Name, 6, Len(FieldName.Name) - 5)
			// Case Else
			// End Select
			// 
			// intField = intField + 1
			// End If
			// Next
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (dblField1Total != 0)
				txtField1Total.Text = Strings.Format(dblField1Total, "0.00");
			if (dblField2Total != 0)
				txtField2Total.Text = Strings.Format(dblField2Total, "0.00");
			if (dblField3Total != 0)
				txtField3Total.Text = Strings.Format(dblField3Total, "0.00");
			if (dblField4Total != 0)
				txtField4Total.Text = Strings.Format(dblField4Total, "0.00");
			if (dblField5Total != 0)
				txtField5Total.Text = Strings.Format(dblField5Total, "0.00");
			if (dblField6Total != 0)
				txtField6Total.Text = Strings.Format(dblField6Total, "0.00");
			if (dblField7Total != 0)
				txtField7Total.Text = Strings.Format(dblField7Total, "0.00");
			if (dblField8Total != 0)
				txtField8Total.Text = Strings.Format(dblField8Total, "0.00");
		}

		
	}
}
