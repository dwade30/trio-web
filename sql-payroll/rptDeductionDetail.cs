//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDeductionDetail.
	/// </summary>
	public partial class rptDeductionDetail : BaseSectionReport
	{
		public rptDeductionDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Deduction Detail Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeductionDetail InstancePtr
		{
			get
			{
				return (rptDeductionDetail)Sys.GetInstance(typeof(rptDeductionDetail));
			}
		}

		protected rptDeductionDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsSummary?.Dispose();
				rsDeductionInfo?.Dispose();
                rsDeductionInfo = null;
                rsSummary = null;
                rsData = null;
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeductionDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double dblAmountTotal;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsSummary = new clsDRWrapper();
		clsDRWrapper rsDeductionInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// **************************************************************************************************
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// If blnFirstRecord Then
			// EOF = False
			// Else
			eArgs.EOF = false;
			if (!blnFirstRecord)
			{
				rsData.MoveNext();
			}
			else
			{
				blnFirstRecord = false;
			}
			while (!rsData.EndOfFile())
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			while (rsData.EndOfFile() && !rsSummary.EndOfFile())
			{
				rsSummary.MoveNext();
				if (rsSummary.EndOfFile() != true)
				{
					// Call rsData.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, SUM(tblEmployeeDeductions.CalendarYTDTotal) as CalendarYTDTotal FROM tblEmployeeMaster INNER JOIN tblEmployeeDeductions ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber WHERE tblEmployeeDeductions.DeductionCode = " & rsSummary.Fields("DeductionNumber") & " GROUP BY tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName")
					rsData.OpenRecordset("select tblemployeemaster.employeenumber, tblemployeemaster.middlename,tblemployeemaster.firstname, tblemployeemaster.lastname, sum(tblcheckdetail.dedamount) as calendarytdtotal from tblemployeemaster inner join tblcheckdetail on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and deddeductionnumber = " + rsSummary.Get_Fields("ID") + " AND PAYDATE between '1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by tblemployeemaster.employeenumber,tblemployeemaster.firstname,tblemployeemaster.lastname,tblemployeemaster.middlename");
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
					break;
				}
			}
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
			}
			// If rsData.EndOfFile Then
			// rsSummary.MoveNext
			// If rsSummary.EndOfFile <> True Then
			// Call rsData.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, SUM(tblEmployeeDeductions.CalendarYTDTotal) as CalendarYTDTotal FROM tblEmployeeMaster INNER JOIN tblEmployeeDeductions ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber WHERE tblEmployeeDeductions.DeductionCode = " & rsSummary.Fields("DeductionNumber") & " GROUP BY tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName")
			// Call rsData.OpenRecordset("select tblemployeemaster.employeenumber, tblemployeemaster.firstname, tblemployeemaster.lastname, sum(tblcheckdetail.dedamount) as calendarytdtotal from tblemployeemaster inner join tblcheckdetail on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and deddeductionnumber = " & rsSummary.Fields("deductionnumber") & " AND PAYDATE between '1/1/" & Year(gdatCurrentPayDate) & "' and '" & gdatCurrentPayDate & "' group by tblemployeemaster.employeenumber,tblemployeemaster.firstname,tblemployeemaster.lastname")
			// EOF = False
			// Else
			// EOF = True
			// End If
			// Else
			// EOF = False
			// End If
			// End If
			if (eArgs.EOF == false)
			{
				this.Fields["Binder"].Value = rsSummary.Get_Fields_Int32("DeductionNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = "Deduction Detail Report";
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			txtPayDate.Text = string.Empty;
			// "Pay Date: " & gdatCurrentPayDate
			rsSummary.OpenRecordset("SELECT  DeductionNumber,ID FROM tblDeductionSetup WHERE ID IN (SELECT DeductionCode FROM tblEmployeeDeductions) ORDER BY DeductionNumber");
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				// Call rsData.OpenRecordset("SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName, SUM(tblEmployeeDeductions.CalendarYTDTotal) as CalendarYTDTotal FROM tblEmployeeMaster INNER JOIN tblEmployeeDeductions ON tblEmployeeMaster.EmployeeNumber = tblEmployeeDeductions.EmployeeNumber WHERE tblEmployeeDeductions.DeductionCode = " & rsSummary.Fields("DeductionNumber") & " GROUP BY tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName, tblEmployeeMaster.LastName")
				rsData.OpenRecordset("select tblemployeemaster.employeenumber,tblemployeemaster.middlename, tblemployeemaster.firstname, tblemployeemaster.lastname, sum(tblcheckdetail.dedamount) as calendarytdtotal from tblemployeemaster inner join tblcheckdetail on (tblcheckdetail.employeenumber = tblemployeemaster.employeenumber) where checkvoid = 0 and deddeductionnumber = " + rsSummary.Get_Fields("ID") + " AND PAYDATE between '1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' group by tblemployeemaster.employeenumber,tblemployeemaster.firstname,tblemployeemaster.lastname,tblemployeemaster.middlename");
				// If rsData.EndOfFile Then
				// MsgBox "No Data Found", vbInformation, "No Information"
				// Me.Cancel
				// Unload Me
				// End If
			}
			else
			{
				MessageBox.Show("No Data Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + fecherFoundation.Strings.Trim(rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields("middlename") + " " + rsData.Get_Fields_String("LastName"));
			txtAmount.Text = Strings.Format(rsData.Get_Fields("CalendarYTDTotal"), "#,##0.00");
			dblAmountTotal += FCConvert.ToDouble(rsData.Get_Fields("CalendarYTDTotal"));
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtAmountTotal.Text = Strings.Format(dblAmountTotal, "#,##0.00");
			dblAmountTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			rsDeductionInfo.OpenRecordset("SELECT * FROM tblDeductionSetup WHERE ID = " + rsSummary.Get_Fields("ID"));
			if (rsDeductionInfo.EndOfFile() != true && rsDeductionInfo.BeginningOfFile() != true)
			{
				txtDeduction.Text = FCConvert.ToString(rsDeductionInfo.Get_Fields_Int32("DeductionNumber")) + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsDeductionInfo.Get_Fields_Int32("DeductionNumber"))).Length, " ") + " - " + rsDeductionInfo.Get_Fields("Description");
			}
			else
			{
				txtDeduction.Text = "UNKNOWN";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
