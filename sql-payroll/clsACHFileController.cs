//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;

namespace TWPY0000
{
	public class clsACHFileController
	{
		//=========================================================
		private string strLastError = "";

		public string LastError
		{
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public clsACHFileInfo NewFileInfo()
		{
			clsACHFileInfo NewFileInfo = null;
			clsACHFileInfo tInfo = new clsACHFileInfo();
			NewFileInfo = tInfo;
			return NewFileInfo;
		}

		public bool FillFileInfo(ref clsACHFile OutFile)
		{
			bool FillFileInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				OutFile.FileInfo().EffectiveEntryDate = DateTime.Now.ToString();
				clsACHSetup tSetup = new clsACHSetup();
				clsACHSetupController tCon = new clsACHSetupController();
				strLastError = "";
				if (!tCon.Load(ref tSetup))
				{
					strLastError = "Unable to load ACH setup information.";
					return FillFileInfo;
				}
				OutFile.FileInfo().OriginInfo().ImmediateOriginName = tSetup.ImmediateOriginName;
				OutFile.FileInfo().OriginInfo().ImmediateOriginRT = tSetup.ImmediateOriginRT;
				OutFile.FileInfo().OriginInfo().OriginatingDFI = tSetup.ImmediateOriginODFI;
				OutFile.FileInfo().EmployerInfo().EmployerName = tSetup.EmployerName;
				OutFile.FileInfo().EmployerInfo().EmployerAccount = tSetup.EmployerAccount;
				OutFile.FileInfo().EmployerInfo().EmployerRT = tSetup.EmployerRT;
				OutFile.FileInfo().EmployerInfo().EmployerID = tSetup.EmployerID;
				OutFile.FileInfo().EmployerInfo().AccountType = FCConvert.ToString(tSetup.EmployerAccountType);
				OutFile.FileInfo().DestinationInfo().ACHBankName = tSetup.ImmediateDestinationName;
				OutFile.FileInfo().DestinationInfo().ACHBankRT = tSetup.ImmediateDestinationRT;
				FillFileInfo = true;
				return FillFileInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return FillFileInfo;
		}

		public bool FillHeaderRec(ref clsACHFile OutFile)
		{
			bool FillHeaderRec = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLastError = "";
				OutFile.FileHeader().FileIDModifier = "A";
				OutFile.FileHeader().ImmediateDestination = OutFile.FileInfo().DestinationInfo().ACHBankRT;
				OutFile.FileHeader().ImmediateDestinationName = OutFile.FileInfo().DestinationInfo().ACHBankName;
				OutFile.FileHeader().ImmediateOriginRT = OutFile.FileInfo().OriginInfo().ImmediateOriginRT;
				OutFile.FileHeader().ImmediateOriginName = OutFile.FileInfo().OriginInfo().ImmediateOriginName;
				FillHeaderRec = true;
				return FillHeaderRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return FillHeaderRec;
		}

		public bool FillBatchRecs(ref clsACHFile OutFile, ref clsACHBatch tBatch)
		{
			bool FillBatchRecs = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLastError = "";
				tBatch.BatchHeader().CompanyID = OutFile.FileInfo().EmployerInfo().EmployerID;
				tBatch.BatchHeader().CompanyName = OutFile.FileInfo().EmployerInfo().EmployerName;
				tBatch.BatchHeader().EffectiveEntryDate = OutFile.FileInfo().EffectiveEntryDate;
				tBatch.BatchHeader().DescriptiveDate = Strings.Format(DateTime.Now, "YYMMDD");
				tBatch.BatchHeader().OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
				tBatch.BatchControl().CompanyID = OutFile.FileInfo().EmployerInfo().EmployerID;
				tBatch.BatchControl().OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
				FillBatchRecs = true;
				return FillBatchRecs;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return FillBatchRecs;
		}

		public bool WriteACHFile(ref clsACHFile OutFile)
		{
			bool WriteACHFile = false;
			bool boolOpen = false;
            StreamWriter ts;
			FCFileSystem fso = new FCFileSystem();
			//ts = FCFileSystem.CreateTextFile(OutFile.FileInfo().FileName, true, false);
            ts = FCFileSystem.CreateTextFile(OutFile.FileInfo().FileName);
            try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strLastError = "";
				int intRecNum;
				intRecNum = 0;
				boolOpen = true;
				string strReturn;
				strReturn = "";
				strReturn = fecherFoundation.Strings.UCase(OutFile.FileHeader().OutputLine());
				ts.WriteLine(strReturn);
				FCCollection tBatches = new FCCollection();
				// create batch header
				tBatches = OutFile.Batches();
				if (!(tBatches == null))
				{
					foreach (clsACHBatch tBatch in tBatches)
					{
						strReturn = fecherFoundation.Strings.UCase(tBatch.BatchHeader().OutputLine());
						ts.WriteLine(strReturn);
						foreach (clsACHDetailRecord tDet in tBatch.Details())
						{
							strReturn = fecherFoundation.Strings.UCase(tDet.OutputLine());
							if (strReturn != "")
							{
								ts.WriteLine(strReturn);
							}
							else
							{
								strLastError = tDet.LastError;
								ts.Close();
								return WriteACHFile;
							}
						}
						// tDet
						foreach (clsACHOffsetRecord tOff in tBatch.Offsets())
						{
							strReturn = fecherFoundation.Strings.UCase(tOff.OutputLine());
							ts.WriteLine(strReturn);
						}
						// tOff
						strReturn = tBatch.BatchControl().OutputLine();
						ts.WriteLine(strReturn);
					}
					// tBatch
				}
				else
				{
					strLastError = "No batches found.";
					ts.Close();
					return WriteACHFile;
				}
				strReturn = fecherFoundation.Strings.UCase(OutFile.FileControl().OutputLine());
				ts.WriteLine(strReturn);
				// fillers
				foreach (clsACHBlockFiller tFill in OutFile.Fillers())
				{
					strReturn = fecherFoundation.Strings.UCase(tFill.OutputLine());
					ts.WriteLine(strReturn);
				}
				// tFill
				boolOpen = false;
				ts.Close();
				WriteACHFile = true;
				return WriteACHFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolOpen)
				{
					ts.Close();
				}
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return WriteACHFile;
		}
		// vbPorter upgrade warning: dtEntryDate As DateTime	OnWrite(string)
		// vbPorter upgrade warning: lngRecipientID As int	OnWrite(int, double)
		public clsACHFile CreateACHFile(bool boolRegular, bool boolPrenotes, bool boolDD, bool boolTandA, bool boolBalanced, DateTime dtEntryDate, int lngRecipientID, bool boolForcePreNote)
		{
			clsACHFile CreateACHFile = null;
			strLastError = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsACHFile OutFile = new clsACHFile();
				OutFile.FileInfo().Balanced = boolBalanced;
				OutFile.FileInfo().Regular = boolRegular;
				OutFile.FileInfo().PreNote = boolPrenotes;
				OutFile.FileInfo().ForcePreNote = boolForcePreNote;
				if (boolForcePreNote)
				{
					OutFile.FileInfo().Regular = false;
					OutFile.FileInfo().PreNote = true;
				}
				if (!FillFileInfo(ref OutFile))
				{
					return CreateACHFile;
				}
				OutFile.FileInfo().EffectiveEntryDate = FCConvert.ToString(dtEntryDate);
				if (!FillHeaderRec(ref OutFile))
				{
					return CreateACHFile;
				}
				clsACHBatch tBat = new clsACHBatch();
				FillBatchRecs(ref OutFile, ref tBat);
				OutFile.AddBatch(ref tBat);
				if (boolDD)
				{
				}
				if (boolTandA)
				{
					tBat.BatchHeader().EntryClassCode = "CCD";
					if (!CreateTrustAndAgencyChecks(ref OutFile, lngRecipientID))
					{
						return CreateACHFile;
					}
				}
				// If boolRegular Then
				// If Not createregularrecs(outFile) Then
				// Exit Function
				// End If
				// End If
				// If boolPrenotes Then
				// If Not createprenotes(outFile) Then
				// Exit Function
				// End If
				// End If
				clsACHOffsetRecord tOff;
				foreach (clsACHBatch tBatch in OutFile.Batches())
				{
					if (OutFile.FileInfo().Balanced)
					{
						tOff = new clsACHOffsetRecord();
						tOff.PreNote = !boolRegular;
						tOff.AccountType = OutFile.FileInfo().EmployerInfo().AccountType;
						tOff.TotalAmount = tBatch.CalcTotalCredits();
						tOff.EmployerRT = OutFile.FileInfo().EmployerInfo().EmployerRT;
						tOff.EmployerID = OutFile.FileInfo().EmployerInfo().EmployerID;
						tOff.EmployerAccount = OutFile.FileInfo().EmployerInfo().EmployerAccount;
						tOff.Name = OutFile.FileInfo().EmployerInfo().EmployerName;
						tOff.OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
						tBatch.AddOffset(ref tOff);
					}
					tBatch.BuildBatchControlRecord();
				}
				// tBatch
				OutFile.BuildFileControl();
				CreateACHFile = OutFile;
				return CreateACHFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return CreateACHFile;
		}
		// vbPorter upgrade warning: 'Return' As bool	OnWrite
		public bool CreateTrustAndAgencyChecks(ref clsACHFile OutFile, int lngRecipientID)
		{
			bool CreateTrustAndAgencyChecks = false;
			strLastError = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL = "";
				string strWhere = "";
				string strPrenoteWhere;
				clsDRWrapper clsRecips = new clsDRWrapper();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsBanks = new clsDRWrapper();
				string strRecipWhere;
				strRecipWhere = "";
				strPrenoteWhere = "";
				if (OutFile.FileInfo().PreNote && !OutFile.FileInfo().Regular && !OutFile.FileInfo().ForcePreNote)
				{
					strRecipWhere = " and eftprenote = 1 ";
				}
				else if (OutFile.FileInfo().Regular && !OutFile.FileInfo().PreNote)
				{
					strRecipWhere = " and eftprenote = 0 ";
				}
				rsBanks.OpenRecordset("select * from tblbanks", "twpy0000.vb1");
				if (lngRecipientID > 0)
				{
					strWhere = " and trustrecipientid = " + FCConvert.ToString(lngRecipientID);
					strPrenoteWhere = " and ID = " + FCConvert.ToString(lngRecipientID);
					clsRecips.OpenRecordset("SELECT * from tblRecipients where eft = 1 and ach = 1 " + strRecipWhere + " and ID = " + FCConvert.ToString(lngRecipientID), "twpy0000.vb1");
				}
				else
				{
					clsRecips.OpenRecordset("SELECT * from tblRecipients where eft = 1 and ach = 1 " + strRecipWhere + " and eftseparatefile = 0", "twpy0000.vb1");
				}
				if (OutFile.FileInfo().Regular)
				{
					strSQL = "select trustrecipientid,sum(trustamount) as TotTrustAmount from tblCheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and payrunid = " + FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun) + " " + strWhere + " group by trustrecipientid having sum(trustamount) > 0";
				}
				else if (OutFile.FileInfo().ForcePreNote)
				{
					strSQL = "select ID as trustrecipientid,0 as tottrustamount from tblrecipients where eft = 1 and ach = 1 " + strPrenoteWhere;
				}
				else
				{
					// just prenotes
					strSQL = "select ID as trustrecipientid,0 as tottrustamount from tblrecipients where eft = 1 and ach = 1 and eftprenote = 1 " + strPrenoteWhere;
				}
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (!clsRecips.EndOfFile())
					{
						clsACHBatch tBatch;
						clsACHDetailRecord tRec;
						tBatch = OutFile.Batches()[OutFile.Batches().Count];
						while (!rsLoad.EndOfFile())
						{
							if (clsRecips.FindFirstRecord("ID", Conversion.Val(rsLoad.Get_Fields("trustrecipientid"))))
							{
								tRec = new clsACHDetailRecord();
								if (rsBanks.FindFirstRecord("ID", Conversion.Val(clsRecips.Get_Fields("eftbank"))))
								{
									tRec.BankID = FCConvert.ToString(rsBanks.Get_Fields_String("BankDepositID"));
								}
								else
								{
									strLastError = "Bank not found for recipient " + clsRecips.Get_Fields("name");
									return CreateTrustAndAgencyChecks;
								}
								tRec.PreNote = clsRecips.Get_Fields_Boolean("eftprenote") || OutFile.FileInfo().ForcePreNote;
								tRec.AccountNumber = FCConvert.ToString(clsRecips.Get_Fields("eftaccount"));
								tRec.AccountType = FCConvert.ToString(clsRecips.Get_Fields("eftaccounttype"));
								tRec.Name = FCConvert.ToString(clsRecips.Get_Fields_String("Name"));
								if (!tRec.PreNote)
								{
									tRec.TotalAmount = Conversion.Val(rsLoad.Get_Fields("tottrustamount"));
								}
								else
								{
									tRec.TotalAmount = 0;
								}
								tRec.ImmediateOriginRT = OutFile.FileHeader().ImmediateOriginRT;
								tBatch.AddDetail(ref tRec);
							}
							else
							{
								// strLastError = "Could not find recipient " & rsLoad.Fields("trustrecipientid")
								// Exit Function
							}
							rsLoad.MoveNext();
						}
					}
					else
					{
						// If lngRecipientID <> 0 Then strLastError = "No matching recipients found."
						return CreateTrustAndAgencyChecks;
					}
				}
				CreateTrustAndAgencyChecks = FCConvert.ToBoolean(1);
				return CreateTrustAndAgencyChecks;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
			}
			return CreateTrustAndAgencyChecks;
		}
	}
}
