﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayTotals.
	/// </summary>
	partial class rptPayTotals
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPayTotals));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.subreportPayCats = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReportMisc = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.subreportPayCats,
            this.SubReportMisc});
            this.Detail.Height = 0.25F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // subreportPayCats
            // 
            this.subreportPayCats.CloseBorder = false;
            this.subreportPayCats.Height = 0.05208333F;
            this.subreportPayCats.Left = 0F;
            this.subreportPayCats.Name = "subreportPayCats";
            this.subreportPayCats.Report = null;
            this.subreportPayCats.Top = 0.03125F;
            this.subreportPayCats.Width = 7.46875F;
            // 
            // SubReportMisc
            // 
            this.SubReportMisc.CloseBorder = false;
            this.SubReportMisc.Height = 0.05208333F;
            this.SubReportMisc.Left = 0F;
            this.SubReportMisc.Name = "SubReportMisc";
            this.SubReportMisc.Report = null;
            this.SubReportMisc.Top = 0.1458333F;
            this.SubReportMisc.Width = 7.46875F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtTime,
            this.txtPage,
            this.txtDesc,
            this.txtCurrent,
            this.txtMTD,
            this.txtQTD,
            this.txtFYTD,
            this.txtCYTD});
            this.PageHeader.Height = 0.75F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.270833F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Pay Totals";
            this.Label1.Top = 0F;
            this.Label1.Width = 2.958333F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0F;
            this.txtMuniName.Width = 2.270833F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 6.0625F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.4375F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.4375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 6.0625F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1.4375F;
            // 
            // txtDesc
            // 
            this.txtDesc.Height = 0.1666667F;
            this.txtDesc.Left = 0.02083333F;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Style = "font-weight: bold";
            this.txtDesc.Text = "Description";
            this.txtDesc.Top = 0.5625F;
            this.txtDesc.Width = 2.229167F;
            // 
            // txtCurrent
            // 
            this.txtCurrent.Height = 0.1666667F;
            this.txtCurrent.Left = 2.375F;
            this.txtCurrent.Name = "txtCurrent";
            this.txtCurrent.Style = "font-weight: bold; text-align: right";
            this.txtCurrent.Text = "Current";
            this.txtCurrent.Top = 0.5625F;
            this.txtCurrent.Width = 0.9166667F;
            // 
            // txtMTD
            // 
            this.txtMTD.Height = 0.1666667F;
            this.txtMTD.Left = 3.375F;
            this.txtMTD.Name = "txtMTD";
            this.txtMTD.Style = "font-weight: bold; text-align: right";
            this.txtMTD.Text = "MTD";
            this.txtMTD.Top = 0.5625F;
            this.txtMTD.Width = 0.9166667F;
            // 
            // txtQTD
            // 
            this.txtQTD.Height = 0.1666667F;
            this.txtQTD.Left = 4.375F;
            this.txtQTD.Name = "txtQTD";
            this.txtQTD.Style = "font-weight: bold; text-align: right";
            this.txtQTD.Text = "QTD";
            this.txtQTD.Top = 0.5625F;
            this.txtQTD.Width = 0.9166667F;
            // 
            // txtFYTD
            // 
            this.txtFYTD.Height = 0.1666667F;
            this.txtFYTD.Left = 5.375F;
            this.txtFYTD.Name = "txtFYTD";
            this.txtFYTD.Style = "font-weight: bold; text-align: right";
            this.txtFYTD.Text = "FYTD";
            this.txtFYTD.Top = 0.5625F;
            this.txtFYTD.Width = 0.9166667F;
            // 
            // txtCYTD
            // 
            this.txtCYTD.Height = 0.1666667F;
            this.txtCYTD.Left = 6.375F;
            this.txtCYTD.Name = "txtCYTD";
            this.txtCYTD.Style = "font-weight: bold; text-align: right";
            this.txtCYTD.Text = "CYTD";
            this.txtCYTD.Top = 0.5625F;
            this.txtCYTD.Width = 0.9166667F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptPayTotals
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFYTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCYTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subreportPayCats;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReportMisc;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCYTD;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
